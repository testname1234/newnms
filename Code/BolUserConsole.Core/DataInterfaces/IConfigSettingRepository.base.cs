﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IConfigSettingRepositoryBase
	{
        
        Dictionary<string, string> GetConfigSettingBasicSearchColumns();
        List<SearchColumn> GetConfigSettingSearchColumns();
        List<SearchColumn> GetConfigSettingAdvanceSearchColumns();
        

		ConfigSetting GetConfigSetting(System.Int32 ConfigSettingId);
		ConfigSetting UpdateConfigSetting(ConfigSetting entity);
		bool DeleteConfigSetting(System.Int32 ConfigSettingId);
		ConfigSetting DeleteConfigSetting(ConfigSetting entity);
		List<ConfigSetting> GetPagedConfigSetting(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<ConfigSetting> GetAllConfigSetting();
		ConfigSetting InsertConfigSetting(ConfigSetting entity);	}
	
	
}
