﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IExceptionLogRepositoryBase
	{
        
        Dictionary<string, string> GetExceptionLogBasicSearchColumns();
        List<SearchColumn> GetExceptionLogSearchColumns();
        List<SearchColumn> GetExceptionLogAdvanceSearchColumns();
        

		ExceptionLog GetExceptionLog(System.Int32 ExceptionLogId);
		ExceptionLog UpdateExceptionLog(ExceptionLog entity);
		bool DeleteExceptionLog(System.Int32 ExceptionLogId);
		ExceptionLog DeleteExceptionLog(ExceptionLog entity);
		List<ExceptionLog> GetPagedExceptionLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<ExceptionLog> GetAllExceptionLog();
		ExceptionLog InsertExceptionLog(ExceptionLog entity);	}
	
	
}
