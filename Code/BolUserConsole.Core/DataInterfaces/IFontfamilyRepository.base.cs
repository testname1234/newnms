﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IFontfamilyRepositoryBase
	{
        
        Dictionary<string, string> GetFontfamilyBasicSearchColumns();
        List<SearchColumn> GetFontfamilySearchColumns();
        List<SearchColumn> GetFontfamilyAdvanceSearchColumns();
        

		Fontfamily GetFontfamily(System.Int32 FontfamilyId);
		Fontfamily UpdateFontfamily(Fontfamily entity);
		bool DeleteFontfamily(System.Int32 FontfamilyId);
		Fontfamily DeleteFontfamily(Fontfamily entity);
		List<Fontfamily> GetPagedFontfamily(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<Fontfamily> GetAllFontfamily();
		Fontfamily InsertFontfamily(Fontfamily entity);	}
	
	
}
