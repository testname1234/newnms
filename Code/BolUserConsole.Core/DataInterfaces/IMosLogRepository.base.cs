﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IMosLogRepositoryBase
	{
        
        Dictionary<string, string> GetMosLogBasicSearchColumns();
        List<SearchColumn> GetMosLogSearchColumns();
        List<SearchColumn> GetMosLogAdvanceSearchColumns();
        

		List<MosLog> GetMosLogByCommandTypeId(System.Int32 CommandTypeId);
		MosLog GetMosLog(System.Int32 MosLogId);
		MosLog UpdateMosLog(MosLog entity);
		bool DeleteMosLog(System.Int32 MosLogId);
		MosLog DeleteMosLog(MosLog entity);
		List<MosLog> GetPagedMosLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<MosLog> GetAllMosLog();
		MosLog InsertMosLog(MosLog entity);	}
	
	
}
