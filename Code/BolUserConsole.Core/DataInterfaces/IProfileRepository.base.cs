﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IProfileRepositoryBase
	{
        
        Dictionary<string, string> GetProfileBasicSearchColumns();
        List<SearchColumn> GetProfileSearchColumns();
        List<SearchColumn> GetProfileAdvanceSearchColumns();
        

		List<Profile> GetProfileByFontfamilyId(System.Int32 FontfamilyId);
		Profile GetProfile(System.Int32 ProfileId);
		Profile UpdateProfile(Profile entity);
		bool DeleteProfile(System.Int32 ProfileId);
		Profile DeleteProfile(Profile entity);
		List<Profile> GetPagedProfile(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<Profile> GetAllProfile();
		Profile InsertProfile(Profile entity);	}
	
	
}
