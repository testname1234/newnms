﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IPrompterActionRepositoryBase
	{
        
        Dictionary<string, string> GetPrompterActionBasicSearchColumns();
        List<SearchColumn> GetPrompterActionSearchColumns();
        List<SearchColumn> GetPrompterActionAdvanceSearchColumns();
        

		PrompterAction GetPrompterAction(System.Int32 ActionId);
		PrompterAction UpdatePrompterAction(PrompterAction entity);
		bool DeletePrompterAction(System.Int32 ActionId);
		PrompterAction DeletePrompterAction(PrompterAction entity);
		List<PrompterAction> GetPagedPrompterAction(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<PrompterAction> GetAllPrompterAction();
		PrompterAction InsertPrompterAction(PrompterAction entity);	}
	
	
}
