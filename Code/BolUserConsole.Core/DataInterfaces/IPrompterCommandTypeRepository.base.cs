﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IPrompterCommandTypeRepositoryBase
	{
        
        Dictionary<string, string> GetPrompterCommandTypeBasicSearchColumns();
        List<SearchColumn> GetPrompterCommandTypeSearchColumns();
        List<SearchColumn> GetPrompterCommandTypeAdvanceSearchColumns();
        

		PrompterCommandType GetPrompterCommandType(System.Int32 CommandTypeId);
		PrompterCommandType UpdatePrompterCommandType(PrompterCommandType entity);
		bool DeletePrompterCommandType(System.Int32 CommandTypeId);
		PrompterCommandType DeletePrompterCommandType(PrompterCommandType entity);
		List<PrompterCommandType> GetPagedPrompterCommandType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<PrompterCommandType> GetAllPrompterCommandType();
		PrompterCommandType InsertPrompterCommandType(PrompterCommandType entity);	}
	
	
}
