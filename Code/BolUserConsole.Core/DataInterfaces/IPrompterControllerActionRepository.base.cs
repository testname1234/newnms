﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IPrompterControllerActionRepositoryBase
	{
        
        Dictionary<string, string> GetPrompterControllerActionBasicSearchColumns();
        List<SearchColumn> GetPrompterControllerActionSearchColumns();
        List<SearchColumn> GetPrompterControllerActionAdvanceSearchColumns();
        

		PrompterControllerAction GetPrompterControllerAction(System.Int32 PrompterControllerAction);
		PrompterControllerAction UpdatePrompterControllerAction(PrompterControllerAction entity);
		bool DeletePrompterControllerAction(System.Int32 PrompterControllerAction);
		PrompterControllerAction DeletePrompterControllerAction(PrompterControllerAction entity);
		List<PrompterControllerAction> GetPagedPrompterControllerAction(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<PrompterControllerAction> GetAllPrompterControllerAction();
		PrompterControllerAction InsertPrompterControllerAction(PrompterControllerAction entity);	}
	
	
}
