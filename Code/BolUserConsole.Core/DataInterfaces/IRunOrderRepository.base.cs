﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IRunOrderRepositoryBase
	{
        
        Dictionary<string, string> GetRunOrderBasicSearchColumns();
        List<SearchColumn> GetRunOrderSearchColumns();
        List<SearchColumn> GetRunOrderAdvanceSearchColumns();
        

		RunOrder GetRunOrder(System.Int32 RoId);
		RunOrder UpdateRunOrder(RunOrder entity);
		bool DeleteRunOrder(System.Int32 RoId);
		RunOrder DeleteRunOrder(RunOrder entity);
		List<RunOrder> GetPagedRunOrder(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<RunOrder> GetAllRunOrder();
		RunOrder InsertRunOrder(RunOrder entity);	}
	
	
}
