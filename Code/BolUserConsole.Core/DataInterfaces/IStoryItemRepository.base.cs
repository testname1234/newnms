﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IStoryItemRepositoryBase
	{
        
        Dictionary<string, string> GetStoryItemBasicSearchColumns();
        List<SearchColumn> GetStoryItemSearchColumns();
        List<SearchColumn> GetStoryItemAdvanceSearchColumns();
        

		List<StoryItem> GetStoryItemByStoryId(System.Int32 StoryId);
		StoryItem GetStoryItem(System.Int32 StoryItemId);
		StoryItem UpdateStoryItem(StoryItem entity);
		bool DeleteStoryItem(System.Int32 StoryItemId);
		StoryItem DeleteStoryItem(StoryItem entity);
		List<StoryItem> GetPagedStoryItem(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<StoryItem> GetAllStoryItem();
		StoryItem InsertStoryItem(StoryItem entity);	}
	
	
}
