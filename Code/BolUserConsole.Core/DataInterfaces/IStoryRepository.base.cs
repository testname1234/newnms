﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IStoryRepositoryBase
	{
        
        Dictionary<string, string> GetStoryBasicSearchColumns();
        List<SearchColumn> GetStorySearchColumns();
        List<SearchColumn> GetStoryAdvanceSearchColumns();
        

		List<Story> GetStoryByRoId(System.Int32 RoId);
		Story GetStory(System.Int32 StoryId);
		Story UpdateStory(Story entity);
		bool DeleteStory(System.Int32 StoryId);
		Story DeleteStory(Story entity);
		List<Story> GetPagedStory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<Story> GetAllStory();
		Story InsertStory(Story entity);	}
	
	
}
