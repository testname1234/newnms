﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IStoryRepository: IStoryRepositoryBase
	{
        bool DeleteStoryByRoId(System.Int32 RoId);
        bool UpdateSeqenceByStoryId(System.Int32 StoryId, System.Int32 UpdatedSequenceId);
	}
	
	
}
