﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IUserActionRepositoryBase
	{
        
        Dictionary<string, string> GetUserActionBasicSearchColumns();
        List<SearchColumn> GetUserActionSearchColumns();
        List<SearchColumn> GetUserActionAdvanceSearchColumns();
        

		List<UserAction> GetUserActionByActionId(System.Int32 ActionId);
		List<UserAction> GetUserActionByRoId(System.Int32 RoId);
		//List<UserAction> GetUserActionByStoryId(System.Int32 StoryId);
		List<UserAction> GetUserActionByUserId(System.Int32 UserId);
		UserAction GetUserAction(System.Int32 UserActionId);
		UserAction UpdateUserAction(UserAction entity);
		bool DeleteUserAction(System.Int32 UserActionId);
		UserAction DeleteUserAction(UserAction entity);
		List<UserAction> GetPagedUserAction(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<UserAction> GetAllUserAction();
		UserAction InsertUserAction(UserAction entity);	}
	
	
}
