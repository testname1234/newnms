﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;

namespace BolUserConsole.Core.DataInterfaces
{
		
	public interface IUserRepositoryBase
	{
        
        Dictionary<string, string> GetUserBasicSearchColumns();
        List<SearchColumn> GetUserSearchColumns();
        List<SearchColumn> GetUserAdvanceSearchColumns();
        

		User GetUser(System.Int32 UserId);
		User UpdateUser(User entity);
		bool DeleteUser(System.Int32 UserId);
		User DeleteUser(User entity);
		List<User> GetPagedUser(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns);
		List<User> GetAllUser();
		User InsertUser(User entity);	}
	
	
}
