﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.ConfigSetting
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ConfigSettingId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String SettingKey{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String SettingValue{ get; set; }

	}	
}
