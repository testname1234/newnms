﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.Fontfamily
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string FontfamilyId{ get; set; }

		[FieldLength(MaxLength = 50)]
		[DataMember (EmitDefaultValue=false)]
		public string Name{ get; set; }

	}	
}
