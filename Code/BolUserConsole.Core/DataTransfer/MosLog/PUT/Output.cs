﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.MosLog
{
    [DataContract]
	public class PutOutput
	{
			
		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.String Data{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Ip{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsSent{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CommandTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 Port{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MosLogId{ get; set; }

	}	
}
