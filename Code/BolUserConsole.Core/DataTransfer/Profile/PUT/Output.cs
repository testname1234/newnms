﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.Profile
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ProfileId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FontfamilyId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FontSize{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double ScrollSpeed{ get; set; }

	}	
}
