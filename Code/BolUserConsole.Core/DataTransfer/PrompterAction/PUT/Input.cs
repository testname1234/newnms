﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.PrompterAction
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string ActionId{ get; set; }

		[FieldLength(MaxLength = 100)]
		[DataMember (EmitDefaultValue=false)]
		public string Name{ get; set; }

	}	
}
