﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.PrompterCommandType
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CommandTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Type{ get; set; }

	}	
}
