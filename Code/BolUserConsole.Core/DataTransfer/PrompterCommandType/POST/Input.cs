﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.PrompterCommandType
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string CommandTypeId{ get; set; }

		[FieldLength(MaxLength = 100)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string Type{ get; set; }

	}	
}
