﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.PrompterControllerAction
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 PrompterControllerAction{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String CommandName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MappedValue{ get; set; }

	}	
}
