﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.RunOrder
{
    [DataContract]
	public class PostInput
	{
			
		[FieldLength(MaxLength = 500)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string StartTime{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string Duration{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string RoId{ get; set; }

	}	
}
