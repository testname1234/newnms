﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.RunOrder
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[IgnoreDataMember]
		public System.DateTime StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string StartTimeStr
		{
			 get { return StartTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime Duration{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string DurationStr
		{
			 get { return Duration.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Duration = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RoId{ get; set; }

	}	
}
