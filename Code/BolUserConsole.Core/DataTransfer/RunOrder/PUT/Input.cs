﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.RunOrder
{
    [DataContract]
	public class PutInput
	{
			
		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string StartTime{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string Duration{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string RoId{ get; set; }

	}	
}
