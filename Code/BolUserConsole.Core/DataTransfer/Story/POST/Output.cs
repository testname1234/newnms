﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.Story
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RoId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsPlayed{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsSkipped{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 StoryId{ get; set; }

	}	
}
