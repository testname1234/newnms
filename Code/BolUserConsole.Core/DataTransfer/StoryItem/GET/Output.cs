﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.StoryItem
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 StoryItemId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 StoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Detail{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Instructions{ get; set; }

	}	
}
