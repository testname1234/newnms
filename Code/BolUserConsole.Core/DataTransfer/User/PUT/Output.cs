﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.User
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 UserId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String SessionId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? LastLoginTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastLoginTimeStr
		{
			 get {if(LastLoginTime.HasValue) return LastLoginTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastLoginTime = date.ToUniversalTime();  }  } 
		}

	}	
}
