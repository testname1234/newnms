﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace BolUserConsole.Core.DataTransfer.UserAction
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ActionId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RoId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 StoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 UserId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 UserActionId{ get; set; }

	}	
}
