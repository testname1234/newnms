﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class ConfigSettingBase:EntityBase, IEquatable<ConfigSettingBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ConfigSettingId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ConfigSettingId{ get; set; }

		[FieldNameAttribute("SettingKey",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SettingKey{ get; set; }

		[FieldNameAttribute("SettingValue",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SettingValue{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ConfigSettingBase> Members

        public virtual bool Equals(ConfigSettingBase other)
        {
			if(this.ConfigSettingId==other.ConfigSettingId  && this.SettingKey==other.SettingKey  && this.SettingValue==other.SettingValue )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ConfigSetting other)
        {
			if(other!=null)
			{
				this.ConfigSettingId=other.ConfigSettingId;
				this.SettingKey=other.SettingKey;
				this.SettingValue=other.SettingValue;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
