﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class ExceptionLogBase:EntityBase, IEquatable<ExceptionLogBase>
	{
			
		[FieldNameAttribute("CreationDate",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Message",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Message{ get; set; }

		[PrimaryKey]
		[FieldNameAttribute("ExceptionLogId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ExceptionLogId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ExceptionLogBase> Members

        public virtual bool Equals(ExceptionLogBase other)
        {
			if(this.CreationDate==other.CreationDate  && this.Message==other.Message  && this.ExceptionLogId==other.ExceptionLogId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ExceptionLog other)
        {
			if(other!=null)
			{
				this.CreationDate=other.CreationDate;
				this.Message=other.Message;
				this.ExceptionLogId=other.ExceptionLogId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
