﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class FontfamilyBase:EntityBase, IEquatable<FontfamilyBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FontfamilyId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FontfamilyId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FontfamilyBase> Members

        public virtual bool Equals(FontfamilyBase other)
        {
			if(this.FontfamilyId==other.FontfamilyId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Fontfamily other)
        {
			if(other!=null)
			{
				this.FontfamilyId=other.FontfamilyId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
