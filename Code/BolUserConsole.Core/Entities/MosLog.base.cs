﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class MosLogBase:EntityBase, IEquatable<MosLogBase>
	{
			
		[FieldNameAttribute("CreationDate",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Data",false,false,4000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Data{ get; set; }

		[FieldNameAttribute("Ip",false,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Ip{ get; set; }

		[FieldNameAttribute("IsSent",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsSent{ get; set; }

		[FieldNameAttribute("CommandTypeId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CommandTypeId{ get; set; }

		[FieldNameAttribute("Port",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 Port{ get; set; }

		[PrimaryKey]
		[FieldNameAttribute("MosLogId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MosLogId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<MosLogBase> Members

        public virtual bool Equals(MosLogBase other)
        {
			if(this.CreationDate==other.CreationDate  && this.Data==other.Data  && this.Ip==other.Ip  && this.IsSent==other.IsSent  && this.CommandTypeId==other.CommandTypeId  && this.Port==other.Port  && this.MosLogId==other.MosLogId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(MosLog other)
        {
			if(other!=null)
			{
				this.CreationDate=other.CreationDate;
				this.Data=other.Data;
				this.Ip=other.Ip;
				this.IsSent=other.IsSent;
				this.CommandTypeId=other.CommandTypeId;
				this.Port=other.Port;
				this.MosLogId=other.MosLogId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
