﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class ProfileBase:EntityBase, IEquatable<ProfileBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProfileId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProfileId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("FontfamilyId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FontfamilyId{ get; set; }

		[FieldNameAttribute("FontSize",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FontSize{ get; set; }

		[FieldNameAttribute("ScrollSpeed",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double ScrollSpeed{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProfileBase> Members

        public virtual bool Equals(ProfileBase other)
        {
			if(this.ProfileId==other.ProfileId  && this.Name==other.Name  && this.FontfamilyId==other.FontfamilyId  && this.FontSize==other.FontSize  && this.ScrollSpeed==other.ScrollSpeed )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Profile other)
        {
			if(other!=null)
			{
				this.ProfileId=other.ProfileId;
				this.Name=other.Name;
				this.FontfamilyId=other.FontfamilyId;
				this.FontSize=other.FontSize;
				this.ScrollSpeed=other.ScrollSpeed;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
