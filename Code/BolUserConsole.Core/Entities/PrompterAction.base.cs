﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class PrompterActionBase:EntityBase, IEquatable<PrompterActionBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ActionId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ActionId{ get; set; }

		[FieldNameAttribute("Name",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<PrompterActionBase> Members

        public virtual bool Equals(PrompterActionBase other)
        {
			if(this.ActionId==other.ActionId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(PrompterAction other)
        {
			if(other!=null)
			{
				this.ActionId=other.ActionId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
