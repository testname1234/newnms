﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class PrompterCommandTypeBase:EntityBase, IEquatable<PrompterCommandTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CommandTypeId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CommandTypeId{ get; set; }

		[FieldNameAttribute("Type",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<PrompterCommandTypeBase> Members

        public virtual bool Equals(PrompterCommandTypeBase other)
        {
			if(this.CommandTypeId==other.CommandTypeId  && this.Type==other.Type )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(PrompterCommandType other)
        {
			if(other!=null)
			{
				this.CommandTypeId=other.CommandTypeId;
				this.Type=other.Type;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
