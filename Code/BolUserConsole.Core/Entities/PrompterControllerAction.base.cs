﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class PrompterControllerActionBase:EntityBase, IEquatable<PrompterControllerActionBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("PrompterControllerAction",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 PrompterControllerAction{ get; set; }

		[FieldNameAttribute("CommandName",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String CommandName{ get; set; }

		[FieldNameAttribute("MappedValue",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MappedValue{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<PrompterControllerActionBase> Members

        public virtual bool Equals(PrompterControllerActionBase other)
        {
			if(this.PrompterControllerAction==other.PrompterControllerAction  && this.CommandName==other.CommandName  && this.MappedValue==other.MappedValue )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(PrompterControllerAction other)
        {
			if(other!=null)
			{
				this.PrompterControllerAction=other.PrompterControllerAction;
				this.CommandName=other.CommandName;
				this.MappedValue=other.MappedValue;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
