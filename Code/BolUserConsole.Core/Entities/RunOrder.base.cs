﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class RunOrderBase:EntityBase, IEquatable<RunOrderBase>
	{
			
		[FieldNameAttribute("Slug",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("StartTime",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string StartTimeStr
		{
			 get { return StartTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Duration",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime Duration{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string DurationStr
		{
			 get { return Duration.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Duration = date.ToUniversalTime();  }  } 
		}

		[PrimaryKey]
		[FieldNameAttribute("RoId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RoId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<RunOrderBase> Members

        public virtual bool Equals(RunOrderBase other)
        {
			if(this.Slug==other.Slug  && this.StartTime==other.StartTime  && this.Duration==other.Duration  && this.RoId==other.RoId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(RunOrder other)
        {
			if(other!=null)
			{
				this.Slug=other.Slug;
				this.StartTime=other.StartTime;
				this.Duration=other.Duration;
				this.RoId=other.RoId;
			}
			
		
		}

        #endregion
		
		
	}
	
	
}
