﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public partial class RunOrder : RunOrderBase 
	{

        public List<Story> Stories { get; set; }
        public List<UserAction> UserActionsPerformed { get; set; }
    }
}
