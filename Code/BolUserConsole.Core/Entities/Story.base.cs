﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class StoryBase:EntityBase, IEquatable<StoryBase>
	{
			
		[FieldNameAttribute("RoId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RoId{ get; set; }

		[FieldNameAttribute("SequenceId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceId{ get; set; }

		[FieldNameAttribute("IsPlayed",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsPlayed{ get; set; }

		[FieldNameAttribute("IsSkipped",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsSkipped{ get; set; }

		[FieldNameAttribute("Slug",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[PrimaryKey]
		[FieldNameAttribute("StoryId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StoryId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<StoryBase> Members

        public virtual bool Equals(StoryBase other)
        {
			if(this.RoId==other.RoId  && this.SequenceId==other.SequenceId  && this.IsPlayed==other.IsPlayed  && this.IsSkipped==other.IsSkipped  && this.Slug==other.Slug  && this.StoryId==other.StoryId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Story other)
        {
			if(other!=null)
			{
				this.RoId=other.RoId;
				this.SequenceId=other.SequenceId;
				this.IsPlayed=other.IsPlayed;
				this.IsSkipped=other.IsSkipped;
				this.Slug=other.Slug;
				this.StoryId=other.StoryId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
