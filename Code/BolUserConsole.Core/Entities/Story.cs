﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public partial class Story : StoryBase 
	{

        public List<StoryItem> StoryItems { get; set; }
		
	}
}
