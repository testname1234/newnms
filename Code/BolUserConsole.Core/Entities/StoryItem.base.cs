﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class StoryItemBase:EntityBase, IEquatable<StoryItemBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("StoryItemId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StoryItemId{ get; set; }

		[FieldNameAttribute("StoryId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StoryId{ get; set; }

		[FieldNameAttribute("SequenceId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceId{ get; set; }

		[FieldNameAttribute("Slug",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("Detail",false,false,4000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Detail{ get; set; }

		[FieldNameAttribute("Instructions",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Instructions{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<StoryItemBase> Members

        public virtual bool Equals(StoryItemBase other)
        {
			if(this.StoryItemId==other.StoryItemId  && this.StoryId==other.StoryId  && this.SequenceId==other.SequenceId  && this.Slug==other.Slug  && this.Detail==other.Detail  && this.Instructions==other.Instructions )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(StoryItem other)
        {
			if(other!=null)
			{
				this.StoryItemId=other.StoryItemId;
				this.StoryId=other.StoryId;
				this.SequenceId=other.SequenceId;
				this.Slug=other.Slug;
				this.Detail=other.Detail;
				this.Instructions=other.Instructions;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
