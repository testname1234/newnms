﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class UserBase:EntityBase, IEquatable<UserBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("UserId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		[FieldNameAttribute("SessionId",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SessionId{ get; set; }

		[FieldNameAttribute("LastLoginTime",true,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastLoginTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastLoginTimeStr
		{
			 get {if(LastLoginTime.HasValue) return LastLoginTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastLoginTime = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<UserBase> Members

        public virtual bool Equals(UserBase other)
        {
			if(this.UserId==other.UserId  && this.SessionId==other.SessionId  && this.LastLoginTime==other.LastLoginTime )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(User other)
        {
			if(other!=null)
			{
				this.UserId=other.UserId;
				this.SessionId=other.SessionId;
				this.LastLoginTime=other.LastLoginTime;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
