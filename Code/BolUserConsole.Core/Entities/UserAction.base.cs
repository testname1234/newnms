﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace BolUserConsole.Core.Entities
{
    [DataContract]
	public abstract partial class UserActionBase:EntityBase, IEquatable<UserActionBase>
	{
			
		[FieldNameAttribute("ActionId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ActionId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("RoId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RoId{ get; set; }

		[FieldNameAttribute("StoryId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StoryId{ get; set; }

		[FieldNameAttribute("UserId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		[PrimaryKey]
		[FieldNameAttribute("UserActionId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserActionId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<UserActionBase> Members

        public virtual bool Equals(UserActionBase other)
        {
			if(this.ActionId==other.ActionId  && this.CreationDate==other.CreationDate  && this.RoId==other.RoId  && this.StoryId==other.StoryId  && this.UserId==other.UserId  && this.UserActionId==other.UserActionId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(UserAction other)
        {
			if(other!=null)
			{
				this.ActionId=other.ActionId;
				this.CreationDate=other.CreationDate;
				this.RoId=other.RoId;
				this.StoryId=other.StoryId;
				this.UserId=other.UserId;
				this.UserActionId=other.UserActionId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
