﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;

namespace BolUserConsole.Core.Helper
{
    public class ExceptionLogger
    {

        public static string lastExceptionType = string.Empty;
        public static DateTime lastExceptionDateTime = DateTime.UtcNow.AddDays(-1);
        public static void Log(Exception exp)
        {
            List<SerializableException> exceptions = new List<SerializableException>();
            if (exp.GetType().ToString() != lastExceptionType || DateTime.UtcNow.Subtract(lastExceptionDateTime).TotalSeconds > 60)
            {
                try
                {
                    Console.WriteLine(exp.Message);
                    lastExceptionType = exp.GetType().ToString();
                    lastExceptionDateTime = DateTime.UtcNow;
                    SerializableException serializableExp = new SerializableException(exp);
                    exceptions.Add(serializableExp);
                }
                catch { }
                ThreadPool.QueueUserWorkItem(new WaitCallback(SendMail), exceptions);
            }
        }

        public static void Log(Exception exp, SqlParameter[] parameters, string query)
        {
            List<SerializableException> exceptions = new List<SerializableException>();
            try
            {
                SerializableException serializableExp = new SerializableException(exp);
                StringBuilder strBuilder = new StringBuilder();
                foreach (SqlParameter par in parameters)
                {
                    strBuilder.AppendLine(par.ParameterName + ":" + par.Value);
                }
                serializableExp.Detail += "\n" + query + "\n" + strBuilder.ToString();
                exceptions.Add(serializableExp);
            }
            catch { }
            ThreadPool.QueueUserWorkItem(new WaitCallback(SendMail), exceptions);
        }

        public static void SendMail(object obj)
        {
            try
            {
                string pcName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                List<SerializableException> exceptions = (obj as List<SerializableException>);
                if (exceptions != null && exceptions.Count > 0)
                {
                    //ITrackingClient client = IoC.Resolve<ITrackingClient>("TrackingClient");
                    //for (int i = 0; i < exceptions.Count; i++)
                    //{
                    //    exceptions[i].Host = string.Format("{0} ({1})", pcName, AppSettings.ProductVersion);
                    //}
                        
                    //client.PostExceptions(exceptions);
                }
            }
            catch { }
        }

        private static void FetchInnerExceptions(SerializableException exception, List<SerializableException> Exceptions,string pcName)
        {
            //if (exception != null)
            //{
            //    exception.Host = string.Format("{0} ({1})", pcName, AppSettings.ProductVersion);
            //    Exceptions.Add(exception);
            //    if (exception.InnerException != null)
            //        FetchInnerExceptions(exception.InnerException, Exceptions, pcName);
            //}
        }

        static void smtp_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)
            {
                try
                {
                    if (File.Exists("errorLog.xml"))
                        File.Delete("errorLog.xml");
                }
                catch { }
                try
                {
                    if (File.Exists("errorLog.txt"))
                        File.Delete("errorLog.txt");
                }
                catch { }
            }
        }
    }
}
