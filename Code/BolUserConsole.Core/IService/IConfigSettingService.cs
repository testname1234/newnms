﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.ConfigSetting;

namespace BolUserConsole.Core.IService
{
		
	public interface IConfigSettingService
	{
        Dictionary<string, string> GetConfigSettingBasicSearchColumns();
        
        List<SearchColumn> GetConfigSettingAdvanceSearchColumns();

		ConfigSetting GetConfigSetting(System.Int32 ConfigSettingId);
		DataTransfer<List<GetOutput>> GetAll();
		ConfigSetting UpdateConfigSetting(ConfigSetting entity);
		bool DeleteConfigSetting(System.Int32 ConfigSettingId);
		List<ConfigSetting> GetAllConfigSetting();
		ConfigSetting InsertConfigSetting(ConfigSetting entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
