﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.ExceptionLog;

namespace BolUserConsole.Core.IService
{
		
	public interface IExceptionLogService
	{
        Dictionary<string, string> GetExceptionLogBasicSearchColumns();
        
        List<SearchColumn> GetExceptionLogAdvanceSearchColumns();

		ExceptionLog GetExceptionLog(System.Int32 ExceptionLogId);
		DataTransfer<List<GetOutput>> GetAll();
		ExceptionLog UpdateExceptionLog(ExceptionLog entity);
		bool DeleteExceptionLog(System.Int32 ExceptionLogId);
		List<ExceptionLog> GetAllExceptionLog();
		ExceptionLog InsertExceptionLog(ExceptionLog entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
