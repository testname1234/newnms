﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.Fontfamily;

namespace BolUserConsole.Core.IService
{
		
	public interface IFontfamilyService
	{
        Dictionary<string, string> GetFontfamilyBasicSearchColumns();
        
        List<SearchColumn> GetFontfamilyAdvanceSearchColumns();

		Fontfamily GetFontfamily(System.Int32 FontfamilyId);
		DataTransfer<List<GetOutput>> GetAll();
		Fontfamily UpdateFontfamily(Fontfamily entity);
		bool DeleteFontfamily(System.Int32 FontfamilyId);
		List<Fontfamily> GetAllFontfamily();
		Fontfamily InsertFontfamily(Fontfamily entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
