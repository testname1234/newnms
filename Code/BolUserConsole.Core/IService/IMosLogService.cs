﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.MosLog;

namespace BolUserConsole.Core.IService
{
		
	public interface IMosLogService
	{
        Dictionary<string, string> GetMosLogBasicSearchColumns();
        
        List<SearchColumn> GetMosLogAdvanceSearchColumns();

		List<MosLog> GetMosLogByCommandTypeId(System.Int32 CommandTypeId);
		MosLog GetMosLog(System.Int32 MosLogId);
		DataTransfer<List<GetOutput>> GetAll();
		MosLog UpdateMosLog(MosLog entity);
		bool DeleteMosLog(System.Int32 MosLogId);
		List<MosLog> GetAllMosLog();
		MosLog InsertMosLog(MosLog entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
