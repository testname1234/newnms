﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.Profile;

namespace BolUserConsole.Core.IService
{
		
	public interface IProfileService
	{
        Dictionary<string, string> GetProfileBasicSearchColumns();
        
        List<SearchColumn> GetProfileAdvanceSearchColumns();

		List<Profile> GetProfileByFontfamilyId(System.Int32 FontfamilyId);
		Profile GetProfile(System.Int32 ProfileId);
		DataTransfer<List<GetOutput>> GetAll();
		Profile UpdateProfile(Profile entity);
		bool DeleteProfile(System.Int32 ProfileId);
		List<Profile> GetAllProfile();
		Profile InsertProfile(Profile entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
