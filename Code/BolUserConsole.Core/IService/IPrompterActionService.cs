﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.PrompterAction;

namespace BolUserConsole.Core.IService
{
		
	public interface IPrompterActionService
	{
        Dictionary<string, string> GetPrompterActionBasicSearchColumns();
        
        List<SearchColumn> GetPrompterActionAdvanceSearchColumns();

		PrompterAction GetPrompterAction(System.Int32 ActionId);
		DataTransfer<List<GetOutput>> GetAll();
		PrompterAction UpdatePrompterAction(PrompterAction entity);
		bool DeletePrompterAction(System.Int32 ActionId);
		List<PrompterAction> GetAllPrompterAction();
		PrompterAction InsertPrompterAction(PrompterAction entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
