﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.PrompterCommandType;

namespace BolUserConsole.Core.IService
{
		
	public interface IPrompterCommandTypeService
	{
        Dictionary<string, string> GetPrompterCommandTypeBasicSearchColumns();
        
        List<SearchColumn> GetPrompterCommandTypeAdvanceSearchColumns();

		PrompterCommandType GetPrompterCommandType(System.Int32 CommandTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		PrompterCommandType UpdatePrompterCommandType(PrompterCommandType entity);
		bool DeletePrompterCommandType(System.Int32 CommandTypeId);
		List<PrompterCommandType> GetAllPrompterCommandType();
		PrompterCommandType InsertPrompterCommandType(PrompterCommandType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
