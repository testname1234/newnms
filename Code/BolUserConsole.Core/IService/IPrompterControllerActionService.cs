﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.PrompterControllerAction;

namespace BolUserConsole.Core.IService
{
		
	public interface IPrompterControllerActionService
	{
        Dictionary<string, string> GetPrompterControllerActionBasicSearchColumns();
        
        List<SearchColumn> GetPrompterControllerActionAdvanceSearchColumns();

		PrompterControllerAction GetPrompterControllerAction(System.Int32 PrompterControllerAction);
		DataTransfer<List<GetOutput>> GetAll();
		PrompterControllerAction UpdatePrompterControllerAction(PrompterControllerAction entity);
		bool DeletePrompterControllerAction(System.Int32 PrompterControllerAction);
		List<PrompterControllerAction> GetAllPrompterControllerAction();
		PrompterControllerAction InsertPrompterControllerAction(PrompterControllerAction entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
