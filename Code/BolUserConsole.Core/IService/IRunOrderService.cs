﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.RunOrder;

namespace BolUserConsole.Core.IService
{
		
	public interface IRunOrderService
	{
        Dictionary<string, string> GetRunOrderBasicSearchColumns();
        
        List<SearchColumn> GetRunOrderAdvanceSearchColumns();

		RunOrder GetRunOrder(System.Int32 RoId);
		DataTransfer<List<GetOutput>> GetAll();
		RunOrder UpdateRunOrder(RunOrder entity);
		bool DeleteRunOrder(System.Int32 RoId);
		List<RunOrder> GetAllRunOrder();
		RunOrder InsertRunOrder(RunOrder entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
