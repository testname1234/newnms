﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.StoryItem;

namespace BolUserConsole.Core.IService
{
		
	public interface IStoryItemService
	{
        Dictionary<string, string> GetStoryItemBasicSearchColumns();
        
        List<SearchColumn> GetStoryItemAdvanceSearchColumns();

		List<StoryItem> GetStoryItemByStoryId(System.Int32 StoryId);
		StoryItem GetStoryItem(System.Int32 StoryItemId);
		DataTransfer<List<GetOutput>> GetAll();
		StoryItem UpdateStoryItem(StoryItem entity);
		bool DeleteStoryItem(System.Int32 StoryItemId);
		List<StoryItem> GetAllStoryItem();
		StoryItem InsertStoryItem(StoryItem entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
