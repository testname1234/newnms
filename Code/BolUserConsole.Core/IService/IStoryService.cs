﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.Story;

namespace BolUserConsole.Core.IService
{
		
	public interface IStoryService
	{
        Dictionary<string, string> GetStoryBasicSearchColumns();
        
        List<SearchColumn> GetStoryAdvanceSearchColumns();

		List<Story> GetStoryByRoId(System.Int32 RoId);
		Story GetStory(System.Int32 StoryId);
		DataTransfer<List<GetOutput>> GetAll();
		Story UpdateStory(Story entity);
		bool DeleteStory(System.Int32 StoryId);
		List<Story> GetAllStory();
		Story InsertStory(Story entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
