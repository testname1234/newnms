﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.UserAction;

namespace BolUserConsole.Core.IService
{
		
	public interface IUserActionService
	{
        Dictionary<string, string> GetUserActionBasicSearchColumns();
        
        List<SearchColumn> GetUserActionAdvanceSearchColumns();

		List<UserAction> GetUserActionByActionId(System.Int32 ActionId);
		List<UserAction> GetUserActionByRoId(System.Int32 RoId);
        //List<UserAction> GetUserActionByStoryId(System.Int32 StoryId);
		List<UserAction> GetUserActionByUserId(System.Int32 UserId);
		UserAction GetUserAction(System.Int32 UserActionId);
		DataTransfer<List<GetOutput>> GetAll();
		UserAction UpdateUserAction(UserAction entity);
		bool DeleteUserAction(System.Int32 UserActionId);
		List<UserAction> GetAllUserAction();
		UserAction InsertUserAction(UserAction entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
