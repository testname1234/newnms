﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BolUserConsole.Core
{
    [Serializable]
    public class SerializableException
    {
        [XmlAttribute("time")]
        public DateTime TimeUtc { get; set; }

        [XmlAttribute("message")]
        public string Message { get; set; }

        [XmlAttribute("detail")]
        public string Detail { get; set; }

        public SerializableException InnerException { get; set; }

         [XmlAttribute("host")]
        public string Host { get; set; }

         [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("source")]
        public string Source { get; set; }

         [XmlAttribute("user")]
        public string User { get; set; }

        public string AllXml { get; set; }

        public SerializableException()
        {
 
        }

        public SerializableException(Exception exp)
        {
            this.Message = exp.Message;
            this.TimeUtc = DateTime.UtcNow;
            this.Detail = exp.StackTrace;
            this.Type = exp.GetType().ToString();
            this.Source = exp.Source.ToString();
            this.User = Environment.UserName;            
            if(exp.InnerException!=null)
                this.InnerException = new SerializableException(exp.InnerException);
        }
    }
}
