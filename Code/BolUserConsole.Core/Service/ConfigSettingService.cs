﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.ConfigSetting;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class ConfigSettingService : IConfigSettingService 
	{
		private IConfigSettingRepository _iConfigSettingRepository;
        
		public ConfigSettingService(IConfigSettingRepository iConfigSettingRepository)
		{
			this._iConfigSettingRepository = iConfigSettingRepository;
		}
        
        public Dictionary<string, string> GetConfigSettingBasicSearchColumns()
        {
            
            return this._iConfigSettingRepository.GetConfigSettingBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetConfigSettingAdvanceSearchColumns()
        {
            
            return this._iConfigSettingRepository.GetConfigSettingAdvanceSearchColumns();
           
        }
        

		public ConfigSetting GetConfigSetting(System.Int32 ConfigSettingId)
		{
			return _iConfigSettingRepository.GetConfigSetting(ConfigSettingId);
		}

		public ConfigSetting UpdateConfigSetting(ConfigSetting entity)
		{
			return _iConfigSettingRepository.UpdateConfigSetting(entity);
		}

		public bool DeleteConfigSetting(System.Int32 ConfigSettingId)
		{
			return _iConfigSettingRepository.DeleteConfigSetting(ConfigSettingId);
		}

		public List<ConfigSetting> GetAllConfigSetting()
		{
			return _iConfigSettingRepository.GetAllConfigSetting();
		}

		public ConfigSetting InsertConfigSetting(ConfigSetting entity)
		{
			 return _iConfigSettingRepository.InsertConfigSetting(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 configsettingid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out configsettingid))
            {
				ConfigSetting configsetting = _iConfigSettingRepository.GetConfigSetting(configsettingid);
                if(configsetting!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(configsetting);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ConfigSetting> configsettinglist = _iConfigSettingRepository.GetAllConfigSetting();
            if (configsettinglist != null && configsettinglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(configsettinglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ConfigSetting configsetting = new ConfigSetting();
                PostOutput output = new PostOutput();
                configsetting.CopyFrom(Input);
                configsetting = _iConfigSettingRepository.InsertConfigSetting(configsetting);
                output.CopyFrom(configsetting);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ConfigSetting configsettinginput = new ConfigSetting();
                ConfigSetting configsettingoutput = new ConfigSetting();
                PutOutput output = new PutOutput();
                configsettinginput.CopyFrom(Input);
                ConfigSetting configsetting = _iConfigSettingRepository.GetConfigSetting(configsettinginput.ConfigSettingId);
                if (configsetting!=null)
                {
                    configsettingoutput = _iConfigSettingRepository.UpdateConfigSetting(configsettinginput);
                    if(configsettingoutput!=null)
                    {
                        output.CopyFrom(configsettingoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 configsettingid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out configsettingid))
            {
				 bool IsDeleted = _iConfigSettingRepository.DeleteConfigSetting(configsettingid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
