﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.ExceptionLog;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class ExceptionLogService : IExceptionLogService 
	{
		private IExceptionLogRepository _iExceptionLogRepository;
        
		public ExceptionLogService(IExceptionLogRepository iExceptionLogRepository)
		{
			this._iExceptionLogRepository = iExceptionLogRepository;
		}
        
        public Dictionary<string, string> GetExceptionLogBasicSearchColumns()
        {
            
            return this._iExceptionLogRepository.GetExceptionLogBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetExceptionLogAdvanceSearchColumns()
        {
            
            return this._iExceptionLogRepository.GetExceptionLogAdvanceSearchColumns();
           
        }
        

		public ExceptionLog GetExceptionLog(System.Int32 ExceptionLogId)
		{
			return _iExceptionLogRepository.GetExceptionLog(ExceptionLogId);
		}

		public ExceptionLog UpdateExceptionLog(ExceptionLog entity)
		{
			return _iExceptionLogRepository.UpdateExceptionLog(entity);
		}

		public bool DeleteExceptionLog(System.Int32 ExceptionLogId)
		{
			return _iExceptionLogRepository.DeleteExceptionLog(ExceptionLogId);
		}

		public List<ExceptionLog> GetAllExceptionLog()
		{
			return _iExceptionLogRepository.GetAllExceptionLog();
		}

		public ExceptionLog InsertExceptionLog(ExceptionLog entity)
		{
			 return _iExceptionLogRepository.InsertExceptionLog(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 exceptionlogid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out exceptionlogid))
            {
				ExceptionLog exceptionlog = _iExceptionLogRepository.GetExceptionLog(exceptionlogid);
                if(exceptionlog!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(exceptionlog);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ExceptionLog> exceptionloglist = _iExceptionLogRepository.GetAllExceptionLog();
            if (exceptionloglist != null && exceptionloglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(exceptionloglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ExceptionLog exceptionlog = new ExceptionLog();
                PostOutput output = new PostOutput();
                exceptionlog.CopyFrom(Input);
                exceptionlog = _iExceptionLogRepository.InsertExceptionLog(exceptionlog);
                output.CopyFrom(exceptionlog);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ExceptionLog exceptionloginput = new ExceptionLog();
                ExceptionLog exceptionlogoutput = new ExceptionLog();
                PutOutput output = new PutOutput();
                exceptionloginput.CopyFrom(Input);
                ExceptionLog exceptionlog = _iExceptionLogRepository.GetExceptionLog(exceptionloginput.ExceptionLogId);
                if (exceptionlog!=null)
                {
                    exceptionlogoutput = _iExceptionLogRepository.UpdateExceptionLog(exceptionloginput);
                    if(exceptionlogoutput!=null)
                    {
                        output.CopyFrom(exceptionlogoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 exceptionlogid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out exceptionlogid))
            {
				 bool IsDeleted = _iExceptionLogRepository.DeleteExceptionLog(exceptionlogid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
