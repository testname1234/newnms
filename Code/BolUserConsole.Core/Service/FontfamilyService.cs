﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.Fontfamily;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class FontfamilyService : IFontfamilyService 
	{
		private IFontfamilyRepository _iFontfamilyRepository;
        
		public FontfamilyService(IFontfamilyRepository iFontfamilyRepository)
		{
			this._iFontfamilyRepository = iFontfamilyRepository;
		}
        
        public Dictionary<string, string> GetFontfamilyBasicSearchColumns()
        {
            
            return this._iFontfamilyRepository.GetFontfamilyBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFontfamilyAdvanceSearchColumns()
        {
            
            return this._iFontfamilyRepository.GetFontfamilyAdvanceSearchColumns();
           
        }
        

		public Fontfamily GetFontfamily(System.Int32 FontfamilyId)
		{
			return _iFontfamilyRepository.GetFontfamily(FontfamilyId);
		}

		public Fontfamily UpdateFontfamily(Fontfamily entity)
		{
			return _iFontfamilyRepository.UpdateFontfamily(entity);
		}

		public bool DeleteFontfamily(System.Int32 FontfamilyId)
		{
			return _iFontfamilyRepository.DeleteFontfamily(FontfamilyId);
		}

		public List<Fontfamily> GetAllFontfamily()
		{
			return _iFontfamilyRepository.GetAllFontfamily();
		}

		public Fontfamily InsertFontfamily(Fontfamily entity)
		{
			 return _iFontfamilyRepository.InsertFontfamily(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 fontfamilyid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out fontfamilyid))
            {
				Fontfamily fontfamily = _iFontfamilyRepository.GetFontfamily(fontfamilyid);
                if(fontfamily!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(fontfamily);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Fontfamily> fontfamilylist = _iFontfamilyRepository.GetAllFontfamily();
            if (fontfamilylist != null && fontfamilylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(fontfamilylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Fontfamily fontfamily = new Fontfamily();
                PostOutput output = new PostOutput();
                fontfamily.CopyFrom(Input);
                fontfamily = _iFontfamilyRepository.InsertFontfamily(fontfamily);
                output.CopyFrom(fontfamily);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Fontfamily fontfamilyinput = new Fontfamily();
                Fontfamily fontfamilyoutput = new Fontfamily();
                PutOutput output = new PutOutput();
                fontfamilyinput.CopyFrom(Input);
                Fontfamily fontfamily = _iFontfamilyRepository.GetFontfamily(fontfamilyinput.FontfamilyId);
                if (fontfamily!=null)
                {
                    fontfamilyoutput = _iFontfamilyRepository.UpdateFontfamily(fontfamilyinput);
                    if(fontfamilyoutput!=null)
                    {
                        output.CopyFrom(fontfamilyoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 fontfamilyid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out fontfamilyid))
            {
				 bool IsDeleted = _iFontfamilyRepository.DeleteFontfamily(fontfamilyid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
