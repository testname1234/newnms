﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.MosLog;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class MosLogService : IMosLogService 
	{
		private IMosLogRepository _iMosLogRepository;
        
		public MosLogService(IMosLogRepository iMosLogRepository)
		{
			this._iMosLogRepository = iMosLogRepository;
		}
        
        public Dictionary<string, string> GetMosLogBasicSearchColumns()
        {
            
            return this._iMosLogRepository.GetMosLogBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMosLogAdvanceSearchColumns()
        {
            
            return this._iMosLogRepository.GetMosLogAdvanceSearchColumns();
           
        }
        

		public virtual List<MosLog> GetMosLogByCommandTypeId(System.Int32 CommandTypeId)
		{
			return _iMosLogRepository.GetMosLogByCommandTypeId(CommandTypeId);
		}

		public MosLog GetMosLog(System.Int32 MosLogId)
		{
			return _iMosLogRepository.GetMosLog(MosLogId);
		}

		public MosLog UpdateMosLog(MosLog entity)
		{
			return _iMosLogRepository.UpdateMosLog(entity);
		}

		public bool DeleteMosLog(System.Int32 MosLogId)
		{
			return _iMosLogRepository.DeleteMosLog(MosLogId);
		}

		public List<MosLog> GetAllMosLog()
		{
			return _iMosLogRepository.GetAllMosLog();
		}

		public MosLog InsertMosLog(MosLog entity)
		{
			 return _iMosLogRepository.InsertMosLog(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 moslogid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out moslogid))
            {
				MosLog moslog = _iMosLogRepository.GetMosLog(moslogid);
                if(moslog!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(moslog);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<MosLog> mosloglist = _iMosLogRepository.GetAllMosLog();
            if (mosloglist != null && mosloglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mosloglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                MosLog moslog = new MosLog();
                PostOutput output = new PostOutput();
                moslog.CopyFrom(Input);
                moslog = _iMosLogRepository.InsertMosLog(moslog);
                output.CopyFrom(moslog);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                MosLog mosloginput = new MosLog();
                MosLog moslogoutput = new MosLog();
                PutOutput output = new PutOutput();
                mosloginput.CopyFrom(Input);
                MosLog moslog = _iMosLogRepository.GetMosLog(mosloginput.MosLogId);
                if (moslog!=null)
                {
                    moslogoutput = _iMosLogRepository.UpdateMosLog(mosloginput);
                    if(moslogoutput!=null)
                    {
                        output.CopyFrom(moslogoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 moslogid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out moslogid))
            {
				 bool IsDeleted = _iMosLogRepository.DeleteMosLog(moslogid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
