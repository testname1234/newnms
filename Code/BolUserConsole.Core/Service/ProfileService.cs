﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.Profile;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class ProfileService : IProfileService 
	{
		private IProfileRepository _iProfileRepository;
        
		public ProfileService(IProfileRepository iProfileRepository)
		{
			this._iProfileRepository = iProfileRepository;
		}
        
        public Dictionary<string, string> GetProfileBasicSearchColumns()
        {
            
            return this._iProfileRepository.GetProfileBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetProfileAdvanceSearchColumns()
        {
            
            return this._iProfileRepository.GetProfileAdvanceSearchColumns();
           
        }
        

		public virtual List<Profile> GetProfileByFontfamilyId(System.Int32 FontfamilyId)
		{
			return _iProfileRepository.GetProfileByFontfamilyId(FontfamilyId);
		}

		public Profile GetProfile(System.Int32 ProfileId)
		{
			return _iProfileRepository.GetProfile(ProfileId);
		}

		public Profile UpdateProfile(Profile entity)
		{
			return _iProfileRepository.UpdateProfile(entity);
		}

		public bool DeleteProfile(System.Int32 ProfileId)
		{
			return _iProfileRepository.DeleteProfile(ProfileId);
		}

		public List<Profile> GetAllProfile()
		{
			return _iProfileRepository.GetAllProfile();
		}

		public Profile InsertProfile(Profile entity)
		{
			 return _iProfileRepository.InsertProfile(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 profileid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out profileid))
            {
				Profile profile = _iProfileRepository.GetProfile(profileid);
                if(profile!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(profile);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Profile> profilelist = _iProfileRepository.GetAllProfile();
            if (profilelist != null && profilelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(profilelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Profile profile = new Profile();
                PostOutput output = new PostOutput();
                profile.CopyFrom(Input);
                profile = _iProfileRepository.InsertProfile(profile);
                output.CopyFrom(profile);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Profile profileinput = new Profile();
                Profile profileoutput = new Profile();
                PutOutput output = new PutOutput();
                profileinput.CopyFrom(Input);
                Profile profile = _iProfileRepository.GetProfile(profileinput.ProfileId);
                if (profile!=null)
                {
                    profileoutput = _iProfileRepository.UpdateProfile(profileinput);
                    if(profileoutput!=null)
                    {
                        output.CopyFrom(profileoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 profileid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out profileid))
            {
				 bool IsDeleted = _iProfileRepository.DeleteProfile(profileid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
