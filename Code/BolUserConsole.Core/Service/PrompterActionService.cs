﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.PrompterAction;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class PrompterActionService : IPrompterActionService 
	{
		private IPrompterActionRepository _iPrompterActionRepository;
        
		public PrompterActionService(IPrompterActionRepository iPrompterActionRepository)
		{
			this._iPrompterActionRepository = iPrompterActionRepository;
		}
        
        public Dictionary<string, string> GetPrompterActionBasicSearchColumns()
        {
            
            return this._iPrompterActionRepository.GetPrompterActionBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetPrompterActionAdvanceSearchColumns()
        {
            
            return this._iPrompterActionRepository.GetPrompterActionAdvanceSearchColumns();
           
        }
        

		public PrompterAction GetPrompterAction(System.Int32 ActionId)
		{
			return _iPrompterActionRepository.GetPrompterAction(ActionId);
		}

		public PrompterAction UpdatePrompterAction(PrompterAction entity)
		{
			return _iPrompterActionRepository.UpdatePrompterAction(entity);
		}

		public bool DeletePrompterAction(System.Int32 ActionId)
		{
			return _iPrompterActionRepository.DeletePrompterAction(ActionId);
		}

		public List<PrompterAction> GetAllPrompterAction()
		{
			return _iPrompterActionRepository.GetAllPrompterAction();
		}

		public PrompterAction InsertPrompterAction(PrompterAction entity)
		{
			 return _iPrompterActionRepository.InsertPrompterAction(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 actionid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out actionid))
            {
				PrompterAction prompteraction = _iPrompterActionRepository.GetPrompterAction(actionid);
                if(prompteraction!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(prompteraction);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<PrompterAction> prompteractionlist = _iPrompterActionRepository.GetAllPrompterAction();
            if (prompteractionlist != null && prompteractionlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(prompteractionlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                PrompterAction prompteraction = new PrompterAction();
                PostOutput output = new PostOutput();
                prompteraction.CopyFrom(Input);
                prompteraction = _iPrompterActionRepository.InsertPrompterAction(prompteraction);
                output.CopyFrom(prompteraction);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                PrompterAction prompteractioninput = new PrompterAction();
                PrompterAction prompteractionoutput = new PrompterAction();
                PutOutput output = new PutOutput();
                prompteractioninput.CopyFrom(Input);
                PrompterAction prompteraction = _iPrompterActionRepository.GetPrompterAction(prompteractioninput.ActionId);
                if (prompteraction!=null)
                {
                    prompteractionoutput = _iPrompterActionRepository.UpdatePrompterAction(prompteractioninput);
                    if(prompteractionoutput!=null)
                    {
                        output.CopyFrom(prompteractionoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 actionid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out actionid))
            {
				 bool IsDeleted = _iPrompterActionRepository.DeletePrompterAction(actionid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
