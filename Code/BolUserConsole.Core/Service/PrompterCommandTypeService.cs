﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.PrompterCommandType;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class PrompterCommandTypeService : IPrompterCommandTypeService 
	{
		private IPrompterCommandTypeRepository _iPrompterCommandTypeRepository;
        
		public PrompterCommandTypeService(IPrompterCommandTypeRepository iPrompterCommandTypeRepository)
		{
			this._iPrompterCommandTypeRepository = iPrompterCommandTypeRepository;
		}
        
        public Dictionary<string, string> GetPrompterCommandTypeBasicSearchColumns()
        {
            
            return this._iPrompterCommandTypeRepository.GetPrompterCommandTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetPrompterCommandTypeAdvanceSearchColumns()
        {
            
            return this._iPrompterCommandTypeRepository.GetPrompterCommandTypeAdvanceSearchColumns();
           
        }
        

		public PrompterCommandType GetPrompterCommandType(System.Int32 CommandTypeId)
		{
			return _iPrompterCommandTypeRepository.GetPrompterCommandType(CommandTypeId);
		}

		public PrompterCommandType UpdatePrompterCommandType(PrompterCommandType entity)
		{
			return _iPrompterCommandTypeRepository.UpdatePrompterCommandType(entity);
		}

		public bool DeletePrompterCommandType(System.Int32 CommandTypeId)
		{
			return _iPrompterCommandTypeRepository.DeletePrompterCommandType(CommandTypeId);
		}

		public List<PrompterCommandType> GetAllPrompterCommandType()
		{
			return _iPrompterCommandTypeRepository.GetAllPrompterCommandType();
		}

		public PrompterCommandType InsertPrompterCommandType(PrompterCommandType entity)
		{
			 return _iPrompterCommandTypeRepository.InsertPrompterCommandType(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 commandtypeid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out commandtypeid))
            {
				PrompterCommandType promptercommandtype = _iPrompterCommandTypeRepository.GetPrompterCommandType(commandtypeid);
                if(promptercommandtype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(promptercommandtype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<PrompterCommandType> promptercommandtypelist = _iPrompterCommandTypeRepository.GetAllPrompterCommandType();
            if (promptercommandtypelist != null && promptercommandtypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(promptercommandtypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                PrompterCommandType promptercommandtype = new PrompterCommandType();
                PostOutput output = new PostOutput();
                promptercommandtype.CopyFrom(Input);
                promptercommandtype = _iPrompterCommandTypeRepository.InsertPrompterCommandType(promptercommandtype);
                output.CopyFrom(promptercommandtype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                PrompterCommandType promptercommandtypeinput = new PrompterCommandType();
                PrompterCommandType promptercommandtypeoutput = new PrompterCommandType();
                PutOutput output = new PutOutput();
                promptercommandtypeinput.CopyFrom(Input);
                PrompterCommandType promptercommandtype = _iPrompterCommandTypeRepository.GetPrompterCommandType(promptercommandtypeinput.CommandTypeId);
                if (promptercommandtype!=null)
                {
                    promptercommandtypeoutput = _iPrompterCommandTypeRepository.UpdatePrompterCommandType(promptercommandtypeinput);
                    if(promptercommandtypeoutput!=null)
                    {
                        output.CopyFrom(promptercommandtypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 commandtypeid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out commandtypeid))
            {
				 bool IsDeleted = _iPrompterCommandTypeRepository.DeletePrompterCommandType(commandtypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
