﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.PrompterControllerAction;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class PrompterControllerActionService : IPrompterControllerActionService 
	{
		private IPrompterControllerActionRepository _iPrompterControllerActionRepository;
        
		public PrompterControllerActionService(IPrompterControllerActionRepository iPrompterControllerActionRepository)
		{
			this._iPrompterControllerActionRepository = iPrompterControllerActionRepository;
		}
        
        public Dictionary<string, string> GetPrompterControllerActionBasicSearchColumns()
        {
            
            return this._iPrompterControllerActionRepository.GetPrompterControllerActionBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetPrompterControllerActionAdvanceSearchColumns()
        {
            
            return this._iPrompterControllerActionRepository.GetPrompterControllerActionAdvanceSearchColumns();
           
        }
        

		public PrompterControllerAction GetPrompterControllerAction(System.Int32 PrompterControllerAction)
		{
			return _iPrompterControllerActionRepository.GetPrompterControllerAction(PrompterControllerAction);
		}

		public PrompterControllerAction UpdatePrompterControllerAction(PrompterControllerAction entity)
		{
			return _iPrompterControllerActionRepository.UpdatePrompterControllerAction(entity);
		}

		public bool DeletePrompterControllerAction(System.Int32 PrompterControllerAction)
		{
			return _iPrompterControllerActionRepository.DeletePrompterControllerAction(PrompterControllerAction);
		}

		public List<PrompterControllerAction> GetAllPrompterControllerAction()
		{
			return _iPrompterControllerActionRepository.GetAllPrompterControllerAction();
		}

		public PrompterControllerAction InsertPrompterControllerAction(PrompterControllerAction entity)
		{
			 return _iPrompterControllerActionRepository.InsertPrompterControllerAction(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 promptercontrolleraction=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out promptercontrolleraction))
            {
				//PrompterControllerAction promptercontrolleraction = _iPrompterControllerActionRepository.GetPrompterControllerAction(promptercontrolleraction);
                if(promptercontrolleraction!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(promptercontrolleraction);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<PrompterControllerAction> promptercontrolleractionlist = _iPrompterControllerActionRepository.GetAllPrompterControllerAction();
            if (promptercontrolleractionlist != null && promptercontrolleractionlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(promptercontrolleractionlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                PrompterControllerAction promptercontrolleraction = new PrompterControllerAction();
                PostOutput output = new PostOutput();
                promptercontrolleraction.CopyFrom(Input);
                promptercontrolleraction = _iPrompterControllerActionRepository.InsertPrompterControllerAction(promptercontrolleraction);
                output.CopyFrom(promptercontrolleraction);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                PrompterControllerAction promptercontrolleractioninput = new PrompterControllerAction();
                PrompterControllerAction promptercontrolleractionoutput = new PrompterControllerAction();
                PutOutput output = new PutOutput();
                promptercontrolleractioninput.CopyFrom(Input);
                PrompterControllerAction promptercontrolleraction = _iPrompterControllerActionRepository.GetPrompterControllerAction(promptercontrolleractioninput.PrompterControllerAction);
                if (promptercontrolleraction!=null)
                {
                    promptercontrolleractionoutput = _iPrompterControllerActionRepository.UpdatePrompterControllerAction(promptercontrolleractioninput);
                    if(promptercontrolleractionoutput!=null)
                    {
                        output.CopyFrom(promptercontrolleractionoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 promptercontrolleraction=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out promptercontrolleraction))
            {
				 bool IsDeleted = _iPrompterControllerActionRepository.DeletePrompterControllerAction(promptercontrolleraction);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
