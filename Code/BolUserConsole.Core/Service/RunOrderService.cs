﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.RunOrder;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class RunOrderService : IRunOrderService 
	{
		private IRunOrderRepository _iRunOrderRepository;
        
		public RunOrderService(IRunOrderRepository iRunOrderRepository)
		{
			this._iRunOrderRepository = iRunOrderRepository;
		}
        
        public Dictionary<string, string> GetRunOrderBasicSearchColumns()
        {
            
            return this._iRunOrderRepository.GetRunOrderBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRunOrderAdvanceSearchColumns()
        {
            
            return this._iRunOrderRepository.GetRunOrderAdvanceSearchColumns();
           
        }
        

		public RunOrder GetRunOrder(System.Int32 RoId)
		{
			return _iRunOrderRepository.GetRunOrder(RoId);
		}

		public RunOrder UpdateRunOrder(RunOrder entity)
		{
			return _iRunOrderRepository.UpdateRunOrder(entity);
		}

		public bool DeleteRunOrder(System.Int32 RoId)
		{
			return _iRunOrderRepository.DeleteRunOrder(RoId);
		}

		public List<RunOrder> GetAllRunOrder()
		{
			return _iRunOrderRepository.GetAllRunOrder();
		}

		public RunOrder InsertRunOrder(RunOrder entity)
		{
			 return _iRunOrderRepository.InsertRunOrder(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 roid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out roid))
            {
				RunOrder runorder = _iRunOrderRepository.GetRunOrder(roid);
                if(runorder!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(runorder);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<RunOrder> runorderlist = _iRunOrderRepository.GetAllRunOrder();
            if (runorderlist != null && runorderlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(runorderlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                RunOrder runorder = new RunOrder();
                PostOutput output = new PostOutput();
                runorder.CopyFrom(Input);
                runorder = _iRunOrderRepository.InsertRunOrder(runorder);
                output.CopyFrom(runorder);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                RunOrder runorderinput = new RunOrder();
                RunOrder runorderoutput = new RunOrder();
                PutOutput output = new PutOutput();
                runorderinput.CopyFrom(Input);
                RunOrder runorder = _iRunOrderRepository.GetRunOrder(runorderinput.RoId);
                if (runorder!=null)
                {
                    runorderoutput = _iRunOrderRepository.UpdateRunOrder(runorderinput);
                    if(runorderoutput!=null)
                    {
                        output.CopyFrom(runorderoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 roid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out roid))
            {
				 bool IsDeleted = _iRunOrderRepository.DeleteRunOrder(roid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
