﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.StoryItem;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class StoryItemService : IStoryItemService 
	{
		private IStoryItemRepository _iStoryItemRepository;
        
		public StoryItemService(IStoryItemRepository iStoryItemRepository)
		{
			this._iStoryItemRepository = iStoryItemRepository;
		}
        
        public Dictionary<string, string> GetStoryItemBasicSearchColumns()
        {
            
            return this._iStoryItemRepository.GetStoryItemBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetStoryItemAdvanceSearchColumns()
        {
            
            return this._iStoryItemRepository.GetStoryItemAdvanceSearchColumns();
           
        }
        

		public virtual List<StoryItem> GetStoryItemByStoryId(System.Int32 StoryId)
		{
			return _iStoryItemRepository.GetStoryItemByStoryId(StoryId);
		}

		public StoryItem GetStoryItem(System.Int32 StoryItemId)
		{
			return _iStoryItemRepository.GetStoryItem(StoryItemId);
		}

		public StoryItem UpdateStoryItem(StoryItem entity)
		{
			return _iStoryItemRepository.UpdateStoryItem(entity);
		}

		public bool DeleteStoryItem(System.Int32 StoryItemId)
		{
			return _iStoryItemRepository.DeleteStoryItem(StoryItemId);
		}

		public List<StoryItem> GetAllStoryItem()
		{
			return _iStoryItemRepository.GetAllStoryItem();
		}

		public StoryItem InsertStoryItem(StoryItem entity)
		{
			 return _iStoryItemRepository.InsertStoryItem(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 storyitemid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out storyitemid))
            {
				StoryItem storyitem = _iStoryItemRepository.GetStoryItem(storyitemid);
                if(storyitem!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(storyitem);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<StoryItem> storyitemlist = _iStoryItemRepository.GetAllStoryItem();
            if (storyitemlist != null && storyitemlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(storyitemlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                StoryItem storyitem = new StoryItem();
                PostOutput output = new PostOutput();
                storyitem.CopyFrom(Input);
                storyitem = _iStoryItemRepository.InsertStoryItem(storyitem);
                output.CopyFrom(storyitem);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                StoryItem storyiteminput = new StoryItem();
                StoryItem storyitemoutput = new StoryItem();
                PutOutput output = new PutOutput();
                storyiteminput.CopyFrom(Input);
                StoryItem storyitem = _iStoryItemRepository.GetStoryItem(storyiteminput.StoryItemId);
                if (storyitem!=null)
                {
                    storyitemoutput = _iStoryItemRepository.UpdateStoryItem(storyiteminput);
                    if(storyitemoutput!=null)
                    {
                        output.CopyFrom(storyitemoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 storyitemid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out storyitemid))
            {
				 bool IsDeleted = _iStoryItemRepository.DeleteStoryItem(storyitemid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
