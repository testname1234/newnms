﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.Story;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class StoryService : IStoryService 
	{
		private IStoryRepository _iStoryRepository;
        
		public StoryService(IStoryRepository iStoryRepository)
		{
			this._iStoryRepository = iStoryRepository;
		}
        
        public Dictionary<string, string> GetStoryBasicSearchColumns()
        {
            
            return this._iStoryRepository.GetStoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetStoryAdvanceSearchColumns()
        {
            
            return this._iStoryRepository.GetStoryAdvanceSearchColumns();
           
        }
        

		public virtual List<Story> GetStoryByRoId(System.Int32 RoId)
		{
			return _iStoryRepository.GetStoryByRoId(RoId);
		}

		public Story GetStory(System.Int32 StoryId)
		{
			return _iStoryRepository.GetStory(StoryId);
		}

		public Story UpdateStory(Story entity)
		{
			return _iStoryRepository.UpdateStory(entity);
		}

		public bool DeleteStory(System.Int32 StoryId)
		{
			return _iStoryRepository.DeleteStory(StoryId);
		}

		public List<Story> GetAllStory()
		{
			return _iStoryRepository.GetAllStory();
		}

		public Story InsertStory(Story entity)
		{
			 return _iStoryRepository.InsertStory(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 storyid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out storyid))
            {
				Story story = _iStoryRepository.GetStory(storyid);
                if(story!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(story);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Story> storylist = _iStoryRepository.GetAllStory();
            if (storylist != null && storylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(storylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Story story = new Story();
                PostOutput output = new PostOutput();
                story.CopyFrom(Input);
                story = _iStoryRepository.InsertStory(story);
                output.CopyFrom(story);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Story storyinput = new Story();
                Story storyoutput = new Story();
                PutOutput output = new PutOutput();
                storyinput.CopyFrom(Input);
                Story story = _iStoryRepository.GetStory(storyinput.StoryId);
                if (story!=null)
                {
                    storyoutput = _iStoryRepository.UpdateStory(storyinput);
                    if(storyoutput!=null)
                    {
                        output.CopyFrom(storyoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 storyid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out storyid))
            {
				 bool IsDeleted = _iStoryRepository.DeleteStory(storyid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
