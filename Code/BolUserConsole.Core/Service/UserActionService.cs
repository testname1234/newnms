﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.UserAction;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class UserActionService : IUserActionService 
	{
		private IUserActionRepository _iUserActionRepository;
        
		public UserActionService(IUserActionRepository iUserActionRepository)
		{
			this._iUserActionRepository = iUserActionRepository;
		}
        
        public Dictionary<string, string> GetUserActionBasicSearchColumns()
        {
            
            return this._iUserActionRepository.GetUserActionBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetUserActionAdvanceSearchColumns()
        {
            
            return this._iUserActionRepository.GetUserActionAdvanceSearchColumns();
           
        }
        

		public virtual List<UserAction> GetUserActionByActionId(System.Int32 ActionId)
		{
			return _iUserActionRepository.GetUserActionByActionId(ActionId);
		}

		public virtual List<UserAction> GetUserActionByRoId(System.Int32 RoId)
		{
			return _iUserActionRepository.GetUserActionByRoId(RoId);
		}

        //public virtual List<UserAction> GetUserActionByStoryId(System.Int32 StoryId)
        //{
        //    //return _iUserActionRepository.GetUserActionByStoryId(StoryId);
        //}

		public virtual List<UserAction> GetUserActionByUserId(System.Int32 UserId)
		{
			return _iUserActionRepository.GetUserActionByUserId(UserId);
		}

		public UserAction GetUserAction(System.Int32 UserActionId)
		{
			return _iUserActionRepository.GetUserAction(UserActionId);
		}

		public UserAction UpdateUserAction(UserAction entity)
		{
			return _iUserActionRepository.UpdateUserAction(entity);
		}

		public bool DeleteUserAction(System.Int32 UserActionId)
		{
			return _iUserActionRepository.DeleteUserAction(UserActionId);
		}

		public List<UserAction> GetAllUserAction()
		{
			return _iUserActionRepository.GetAllUserAction();
		}

		public UserAction InsertUserAction(UserAction entity)
		{
			 return _iUserActionRepository.InsertUserAction(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 useractionid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out useractionid))
            {
				UserAction useraction = _iUserActionRepository.GetUserAction(useractionid);
                if(useraction!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(useraction);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<UserAction> useractionlist = _iUserActionRepository.GetAllUserAction();
            if (useractionlist != null && useractionlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(useractionlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                UserAction useraction = new UserAction();
                PostOutput output = new PostOutput();
                useraction.CopyFrom(Input);
                useraction = _iUserActionRepository.InsertUserAction(useraction);
                output.CopyFrom(useraction);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                UserAction useractioninput = new UserAction();
                UserAction useractionoutput = new UserAction();
                PutOutput output = new PutOutput();
                useractioninput.CopyFrom(Input);
                UserAction useraction = _iUserActionRepository.GetUserAction(useractioninput.UserActionId);
                if (useraction!=null)
                {
                    useractionoutput = _iUserActionRepository.UpdateUserAction(useractioninput);
                    if(useractionoutput!=null)
                    {
                        output.CopyFrom(useractionoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 useractionid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out useractionid))
            {
				 bool IsDeleted = _iUserActionRepository.DeleteUserAction(useractionid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
