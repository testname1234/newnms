﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using BolUserConsole.Core.IService;
using BolUserConsole.Core.DataTransfer;
using BolUserConsole.Core.DataTransfer.User;
using Validation;
using System.Linq;

namespace BolUserConsole.Core.Service
{
		
	public class UserService : IUserService 
	{
		private IUserRepository _iUserRepository;
        
		public UserService(IUserRepository iUserRepository)
		{
			this._iUserRepository = iUserRepository;
		}
        
        public Dictionary<string, string> GetUserBasicSearchColumns()
        {
            
            return this._iUserRepository.GetUserBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetUserAdvanceSearchColumns()
        {
            
            return this._iUserRepository.GetUserAdvanceSearchColumns();
           
        }
        

		public User GetUser(System.Int32 UserId)
		{
			return _iUserRepository.GetUser(UserId);
		}

		public User UpdateUser(User entity)
		{
			return _iUserRepository.UpdateUser(entity);
		}

		public bool DeleteUser(System.Int32 UserId)
		{
			return _iUserRepository.DeleteUser(UserId);
		}

		public List<User> GetAllUser()
		{
			return _iUserRepository.GetAllUser();
		}

		public User InsertUser(User entity)
		{
			 return _iUserRepository.InsertUser(entity);
		}


        public DataTransfer<GetOutput> Get(string id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 userid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out userid))
            {
				User user = _iUserRepository.GetUser(userid);
                if(user!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(user);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<User> userlist = _iUserRepository.GetAllUser();
            if (userlist != null && userlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(userlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                User user = new User();
                PostOutput output = new PostOutput();
                user.CopyFrom(Input);
                user = _iUserRepository.InsertUser(user);
                output.CopyFrom(user);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                User userinput = new User();
                User useroutput = new User();
                PutOutput output = new PutOutput();
                userinput.CopyFrom(Input);
                User user = _iUserRepository.GetUser(userinput.UserId);
                if (user!=null)
                {
                    useroutput = _iUserRepository.UpdateUser(userinput);
                    if(useroutput!=null)
                    {
                        output.CopyFrom(useroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 userid=0;
            if (!string.IsNullOrEmpty(id) && System.Int32.TryParse(id,out userid))
            {
				 bool IsDeleted = _iUserRepository.DeleteUser(userid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
