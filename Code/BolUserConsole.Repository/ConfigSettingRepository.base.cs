﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class ConfigSettingRepositoryBase : Repository, IConfigSettingRepositoryBase 
	{
        
        public ConfigSettingRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ConfigSettingId",new SearchColumn(){Name="ConfigSettingId",Title="ConfigSettingId",SelectClause="ConfigSettingId",WhereClause="AllRecords.ConfigSettingId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SettingKey",new SearchColumn(){Name="SettingKey",Title="SettingKey",SelectClause="SettingKey",WhereClause="AllRecords.SettingKey",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SettingValue",new SearchColumn(){Name="SettingValue",Title="SettingValue",SelectClause="SettingValue",WhereClause="AllRecords.SettingValue",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetConfigSettingSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetConfigSettingBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetConfigSettingAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetConfigSettingSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ConfigSetting."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",ConfigSetting."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ConfigSetting GetConfigSetting(System.Int32 ConfigSettingId)
		{

			string sql=GetConfigSettingSelectClause();
			sql+="from ConfigSetting where ConfigSettingId=@ConfigSettingId ";
			SqlParameter parameter=new SqlParameter("@ConfigSettingId",ConfigSettingId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ConfigSettingFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<ConfigSetting> GetAllConfigSetting()
		{

			string sql=GetConfigSettingSelectClause();
			sql+="from ConfigSetting";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ConfigSetting>(ds, ConfigSettingFromDataRow);
		}

		public virtual List<ConfigSetting> GetPagedConfigSetting(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetConfigSettingCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ConfigSettingId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ConfigSettingId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ConfigSettingId] ";
            tempsql += " FROM [ConfigSetting] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ConfigSettingId"))
					tempsql += " , (AllRecords.[ConfigSettingId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ConfigSettingId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetConfigSettingSelectClause()+@" FROM [ConfigSetting], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ConfigSetting].[ConfigSettingId] = PageIndex.[ConfigSettingId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ConfigSetting>(ds, ConfigSettingFromDataRow);
			}else{ return null;}
		}

		private int GetConfigSettingCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ConfigSetting as AllRecords ";
			else
				sql = "SELECT Count(*) FROM ConfigSetting as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ConfigSetting))]
		public virtual ConfigSetting InsertConfigSetting(ConfigSetting entity)
		{

			ConfigSetting other=new ConfigSetting();
			other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into ConfigSetting ( [SettingKey]
				,[SettingValue] )
				Values
				( @SettingKey
				, @SettingValue )";
				
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@SettingKey",entity.SettingKey)
					, new SqlParameter("@SettingValue",entity.SettingValue)};
               var identity= SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
               return GetConfigSetting(identity);
            }
            return other;
		}

		[MOLog(AuditOperations.Update,typeof(ConfigSetting))]
		public virtual ConfigSetting UpdateConfigSetting(ConfigSetting entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update ConfigSetting set  [SettingKey]=@SettingKey
							, [SettingValue]=@SettingValue 
							 where ConfigSettingId=@ConfigSettingId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SettingKey",entity.SettingKey)
					, new SqlParameter("@SettingValue",entity.SettingValue)
					, new SqlParameter("@ConfigSettingId",entity.ConfigSettingId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteConfigSetting(System.Int32 ConfigSettingId)
		{

			string sql="delete from ConfigSetting where ConfigSettingId=@ConfigSettingId";
			SqlParameter parameter=new SqlParameter("@ConfigSettingId",ConfigSettingId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ConfigSetting))]
		public virtual ConfigSetting DeleteConfigSetting(ConfigSetting entity)
		{
			this.DeleteConfigSetting(entity.ConfigSettingId);
			return entity;
		}


		public virtual ConfigSetting ConfigSettingFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ConfigSetting entity=new ConfigSetting();
			entity.ConfigSettingId = (System.Int32)dr["ConfigSettingId"];
			entity.SettingKey = dr["SettingKey"].ToString();
			entity.SettingValue = dr["SettingValue"].ToString();
			return entity;
		}

	}
	
	
}