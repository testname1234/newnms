﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using BolUserConsole.Core;

namespace BolUserConsole.Repository
{
		
	public partial class ConfigSettingRepository: ConfigSettingRepositoryBase, IConfigSettingRepository
	{

        public virtual bool UpdateConfigSettingValueById(System.Int32 CSId, System.String Value)
        {
            string sql = @"Update ConfigSetting set [SettingValue]=@Value
							 where ConfigSettingId=@CSId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@CSId",CSId)
					, new SqlParameter("@Value",Value)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return true;
        }
		
	}
	
	
}
