﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class ExceptionLogRepositoryBase : Repository, IExceptionLogRepositoryBase 
	{
        
        public ExceptionLogRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Message",new SearchColumn(){Name="Message",Title="Message",SelectClause="Message",WhereClause="AllRecords.Message",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ExceptionLogId",new SearchColumn(){Name="ExceptionLogId",Title="ExceptionLogId",SelectClause="ExceptionLogId",WhereClause="AllRecords.ExceptionLogId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetExceptionLogSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetExceptionLogBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetExceptionLogAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetExceptionLogSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ExceptionLog."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",ExceptionLog."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ExceptionLog GetExceptionLog(System.Int32 ExceptionLogId)
		{

			string sql=GetExceptionLogSelectClause();
			sql+="from ExceptionLog where ExceptionLogId=@ExceptionLogId ";
			SqlParameter parameter=new SqlParameter("@ExceptionLogId",ExceptionLogId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ExceptionLogFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<ExceptionLog> GetAllExceptionLog()
		{

			string sql=GetExceptionLogSelectClause();
			sql+="from ExceptionLog";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ExceptionLog>(ds, ExceptionLogFromDataRow);
		}

		public virtual List<ExceptionLog> GetPagedExceptionLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetExceptionLogCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ExceptionLogId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ExceptionLogId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ExceptionLogId] ";
            tempsql += " FROM [ExceptionLog] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ExceptionLogId"))
					tempsql += " , (AllRecords.[ExceptionLogId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ExceptionLogId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetExceptionLogSelectClause()+@" FROM [ExceptionLog], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ExceptionLog].[ExceptionLogId] = PageIndex.[ExceptionLogId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ExceptionLog>(ds, ExceptionLogFromDataRow);
			}else{ return null;}
		}

		private int GetExceptionLogCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ExceptionLog as AllRecords ";
			else
				sql = "SELECT Count(*) FROM ExceptionLog as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ExceptionLog))]
		public virtual ExceptionLog InsertExceptionLog(ExceptionLog entity)
		{

			ExceptionLog other=new ExceptionLog();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ExceptionLog ( [CreationDate]
				,[Message] )
				Values
				( @CreationDate
				, @Message )";
				
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Message",entity.Message)};
                var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return GetExceptionLog(identity);
            }
            return other;

		}

		[MOLog(AuditOperations.Update,typeof(ExceptionLog))]
		public virtual ExceptionLog UpdateExceptionLog(ExceptionLog entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update ExceptionLog set  [CreationDate]=@CreationDate
							, [Message]=@Message 
							 where ExceptionLogId=@ExceptionLogId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Message",entity.Message)
					, new SqlParameter("@ExceptionLogId",entity.ExceptionLogId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteExceptionLog(System.Int32 ExceptionLogId)
		{

			string sql="delete from ExceptionLog where ExceptionLogId=@ExceptionLogId";
			SqlParameter parameter=new SqlParameter("@ExceptionLogId",ExceptionLogId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ExceptionLog))]
		public virtual ExceptionLog DeleteExceptionLog(ExceptionLog entity)
		{
			this.DeleteExceptionLog(entity.ExceptionLogId);
			return entity;
		}


		public virtual ExceptionLog ExceptionLogFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ExceptionLog entity=new ExceptionLog();
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			entity.Message = dr["Message"].ToString();
			entity.ExceptionLogId = (System.Int32)dr["ExceptionLogId"];
			return entity;
		}

	}
	
	
}