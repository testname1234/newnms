﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class FontfamilyRepositoryBase : Repository, IFontfamilyRepositoryBase 
	{
        
        public FontfamilyRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FontfamilyId",new SearchColumn(){Name="FontfamilyId",Title="FontfamilyId",SelectClause="FontfamilyId",WhereClause="AllRecords.FontfamilyId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFontfamilySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFontfamilyBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFontfamilyAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFontfamilySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "Fontfamily."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",Fontfamily."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Fontfamily GetFontfamily(System.Int32 FontfamilyId)
		{

			string sql=GetFontfamilySelectClause();
			sql+="from Fontfamily where FontfamilyId=@FontfamilyId ";
			SqlParameter parameter=new SqlParameter("@FontfamilyId",FontfamilyId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FontfamilyFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<Fontfamily> GetAllFontfamily()
		{

			string sql=GetFontfamilySelectClause();
			sql+="from Fontfamily";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Fontfamily>(ds, FontfamilyFromDataRow);
		}

		public virtual List<Fontfamily> GetPagedFontfamily(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFontfamilyCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FontfamilyId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FontfamilyId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FontfamilyId] ";
            tempsql += " FROM [Fontfamily] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FontfamilyId"))
					tempsql += " , (AllRecords.[FontfamilyId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FontfamilyId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetFontfamilySelectClause()+@" FROM [Fontfamily], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Fontfamily].[FontfamilyId] = PageIndex.[FontfamilyId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Fontfamily>(ds, FontfamilyFromDataRow);
			}else{ return null;}
		}

		private int GetFontfamilyCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Fontfamily as AllRecords ";
			else
				sql = "SELECT Count(*) FROM Fontfamily as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Fontfamily))]
		public virtual Fontfamily InsertFontfamily(Fontfamily entity)
		{

			Fontfamily other=new Fontfamily();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Fontfamily ( [Name] )
				Values
				( @Name );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)};
                var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return GetFontfamily(identity);
            }
            return other;
		}

		[MOLog(AuditOperations.Update,typeof(Fontfamily))]
		public virtual Fontfamily UpdateFontfamily(Fontfamily entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update Fontfamily set  [Name]=@Name 
							 where FontfamilyId=@FontfamilyId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@FontfamilyId",entity.FontfamilyId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteFontfamily(System.Int32 FontfamilyId)
		{

			string sql="delete from Fontfamily where FontfamilyId=@FontfamilyId";
			SqlParameter parameter=new SqlParameter("@FontfamilyId",FontfamilyId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Fontfamily))]
		public virtual Fontfamily DeleteFontfamily(Fontfamily entity)
		{
			this.DeleteFontfamily(entity.FontfamilyId);
			return entity;
		}


		public virtual Fontfamily FontfamilyFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Fontfamily entity=new Fontfamily();
			entity.FontfamilyId = (System.Int32)dr["FontfamilyId"];
			entity.Name = dr["Name"].ToString();
			return entity;
		}

	}
	
	
}