﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class MosLogRepositoryBase : Repository, IMosLogRepositoryBase 
	{
        
        public MosLogRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Data",new SearchColumn(){Name="Data",Title="Data",SelectClause="Data",WhereClause="AllRecords.Data",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Ip",new SearchColumn(){Name="Ip",Title="Ip",SelectClause="Ip",WhereClause="AllRecords.Ip",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsSent",new SearchColumn(){Name="IsSent",Title="IsSent",SelectClause="IsSent",WhereClause="AllRecords.IsSent",DataType="System.Boolean",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CommandTypeId",new SearchColumn(){Name="CommandTypeId",Title="CommandTypeId",SelectClause="CommandTypeId",WhereClause="AllRecords.CommandTypeId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Port",new SearchColumn(){Name="Port",Title="Port",SelectClause="Port",WhereClause="AllRecords.Port",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MosLogId",new SearchColumn(){Name="MosLogId",Title="MosLogId",SelectClause="MosLogId",WhereClause="AllRecords.MosLogId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMosLogSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMosLogBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMosLogAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMosLogSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "MosLog."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",MosLog."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<MosLog> GetMosLogByCommandTypeId(System.Int32 CommandTypeId)
		{

			string sql=GetMosLogSelectClause();
			sql+="from MosLog where CommandTypeId=@CommandTypeId  ";
			SqlParameter parameter=new SqlParameter("@CommandTypeId",CommandTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosLog>(ds,MosLogFromDataRow);
		}

		public virtual MosLog GetMosLog(System.Int32 MosLogId)
		{

			string sql=GetMosLogSelectClause();
			sql+="from MosLog where MosLogId=@MosLogId ";
			SqlParameter parameter=new SqlParameter("@MosLogId",MosLogId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return MosLogFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<MosLog> GetAllMosLog()
		{

			string sql=GetMosLogSelectClause();
			sql+="from MosLog";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosLog>(ds, MosLogFromDataRow);
		}

		public virtual List<MosLog> GetPagedMosLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMosLogCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MosLogId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MosLogId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MosLogId] ";
            tempsql += " FROM [MosLog] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MosLogId"))
					tempsql += " , (AllRecords.[MosLogId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MosLogId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetMosLogSelectClause()+@" FROM [MosLog], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MosLog].[MosLogId] = PageIndex.[MosLogId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosLog>(ds, MosLogFromDataRow);
			}else{ return null;}
		}

		private int GetMosLogCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MosLog as AllRecords ";
			else
				sql = "SELECT Count(*) FROM MosLog as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(MosLog))]
		public virtual MosLog InsertMosLog(MosLog entity)
		{

			MosLog other=new MosLog();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MosLog ( [CreationDate]
				,[Data]
				,[Ip]
				,[IsSent]
				,[CommandTypeId]
				,[Port] )
				Values
				( @CreationDate
				, @Data
				, @Ip
				, @IsSent
				, @CommandTypeId
				, @Port );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Data",entity.Data)
					, new SqlParameter("@Ip",entity.Ip)
					, new SqlParameter("@IsSent",entity.IsSent)
					, new SqlParameter("@CommandTypeId",entity.CommandTypeId)
					, new SqlParameter("@Port",entity.Port)};
                var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return GetMosLog(identity);
            }
            return other;

		}

		[MOLog(AuditOperations.Update,typeof(MosLog))]
		public virtual MosLog UpdateMosLog(MosLog entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update MosLog set  [CreationDate]=@CreationDate
							, [Data]=@Data
							, [Ip]=@Ip
							, [IsSent]=@IsSent
							, [CommandTypeId]=@CommandTypeId
							, [Port]=@Port 
							 where MosLogId=@MosLogId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Data",entity.Data)
					, new SqlParameter("@Ip",entity.Ip)
					, new SqlParameter("@IsSent",entity.IsSent)
					, new SqlParameter("@CommandTypeId",entity.CommandTypeId)
					, new SqlParameter("@Port",entity.Port)
					, new SqlParameter("@MosLogId",entity.MosLogId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteMosLog(System.Int32 MosLogId)
		{

			string sql="delete from MosLog where MosLogId=@MosLogId";
			SqlParameter parameter=new SqlParameter("@MosLogId",MosLogId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(MosLog))]
		public virtual MosLog DeleteMosLog(MosLog entity)
		{
			this.DeleteMosLog(entity.MosLogId);
			return entity;
		}


		public virtual MosLog MosLogFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			MosLog entity=new MosLog();
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			entity.Data = dr["Data"].ToString();
			entity.Ip = dr["Ip"].ToString();
			entity.IsSent = (System.Boolean)dr["IsSent"];
			entity.CommandTypeId = (System.Int32)dr["CommandTypeId"];
			entity.Port = (System.Int32)dr["Port"];
			entity.MosLogId = (System.Int32)dr["MosLogId"];
			return entity;
		}

	}
	
	
}