﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class ProfileRepositoryBase : Repository, IProfileRepositoryBase 
	{
        
        public ProfileRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProfileId",new SearchColumn(){Name="ProfileId",Title="ProfileId",SelectClause="ProfileId",WhereClause="AllRecords.ProfileId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FontfamilyId",new SearchColumn(){Name="FontfamilyId",Title="FontfamilyId",SelectClause="FontfamilyId",WhereClause="AllRecords.FontfamilyId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FontSize",new SearchColumn(){Name="FontSize",Title="FontSize",SelectClause="FontSize",WhereClause="AllRecords.FontSize",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScrollSpeed",new SearchColumn(){Name="ScrollSpeed",Title="ScrollSpeed",SelectClause="ScrollSpeed",WhereClause="AllRecords.ScrollSpeed",DataType="System.Double",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProfileSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProfileBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProfileAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProfileSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "Profile."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",Profile."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Profile> GetProfileByFontfamilyId(System.Int32 FontfamilyId)
		{

			string sql=GetProfileSelectClause();
			sql+="from Profile where FontfamilyId=@FontfamilyId  ";
			SqlParameter parameter=new SqlParameter("@FontfamilyId",FontfamilyId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Profile>(ds,ProfileFromDataRow);
		}

		public virtual Profile GetProfile(System.Int32 ProfileId)
		{

			string sql=GetProfileSelectClause();
			sql+="from Profile where ProfileId=@ProfileId ";
			SqlParameter parameter=new SqlParameter("@ProfileId",ProfileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProfileFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<Profile> GetAllProfile()
		{

			string sql=GetProfileSelectClause();
			sql+="from Profile";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Profile>(ds, ProfileFromDataRow);
		}

		public virtual List<Profile> GetPagedProfile(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProfileCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProfileId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProfileId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProfileId] ";
            tempsql += " FROM [Profile] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProfileId"))
					tempsql += " , (AllRecords.[ProfileId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProfileId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetProfileSelectClause()+@" FROM [Profile], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Profile].[ProfileId] = PageIndex.[ProfileId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Profile>(ds, ProfileFromDataRow);
			}else{ return null;}
		}

		private int GetProfileCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Profile as AllRecords ";
			else
				sql = "SELECT Count(*) FROM Profile as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Profile))]
		public virtual Profile InsertProfile(Profile entity)
		{

			Profile other=new Profile();
			other = entity;
			if(entity.IsTransient())
			{
                string sql = @"Insert into Profile ( [Name]
				,[FontfamilyId]
				,[FontSize]
				,[ScrollSpeed] )
				Values
				( @Name
				, @FontfamilyId
				, @FontSize
				, @ScrollSpeed )";

                string sqlId = @"SELECT @@IDENTITY";
//scope_identity()";


				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@FontfamilyId",entity.FontfamilyId)
					, new SqlParameter("@FontSize",entity.FontSize)
					, new SqlParameter("@ScrollSpeed",entity.ScrollSpeed)};

                System.Data.SqlServerCe.SqlCeConnection connection= SqlHelper.OpenConnectionForSQLCEIdentity(this.ConnectionString);
                SqlHelper.ExecuteNonQuery(connection, CommandType.Text, sql, parameterArray);
                var identity = SqlHelper.ExecuteScalar(connection, CommandType.Text, sqlId);
                return GetProfile(Convert.ToInt32(identity));
            }
            return other;

		}

		[MOLog(AuditOperations.Update,typeof(Profile))]
		public virtual Profile UpdateProfile(Profile entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update Profile set  [Name]=@Name
							, [FontfamilyId]=@FontfamilyId
							, [FontSize]=@FontSize
							, [ScrollSpeed]=@ScrollSpeed 
							 where ProfileId=@ProfileId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@FontfamilyId",entity.FontfamilyId)
					, new SqlParameter("@FontSize",entity.FontSize)
					, new SqlParameter("@ScrollSpeed",entity.ScrollSpeed)
					, new SqlParameter("@ProfileId",entity.ProfileId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteProfile(System.Int32 ProfileId)
		{

			string sql="delete from Profile where ProfileId=@ProfileId";
			SqlParameter parameter=new SqlParameter("@ProfileId",ProfileId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Profile))]
		public virtual Profile DeleteProfile(Profile entity)
		{
			this.DeleteProfile(entity.ProfileId);
			return entity;
		}


		public virtual Profile ProfileFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Profile entity=new Profile();
			entity.ProfileId = (System.Int32)dr["ProfileId"];
			entity.Name = dr["Name"].ToString();
			entity.FontfamilyId = (System.Int32)dr["FontfamilyId"];
			entity.FontSize = (System.Int32)dr["FontSize"];
			entity.ScrollSpeed = (System.Double)dr["ScrollSpeed"];
			return entity;
		}

	}
	
	
}