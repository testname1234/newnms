﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class PrompterActionRepositoryBase : Repository, IPrompterActionRepositoryBase 
	{
        
        public PrompterActionRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ActionId",new SearchColumn(){Name="ActionId",Title="ActionId",SelectClause="ActionId",WhereClause="AllRecords.ActionId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetPrompterActionSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetPrompterActionBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetPrompterActionAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetPrompterActionSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "PrompterAction."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",PrompterAction."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual PrompterAction GetPrompterAction(System.Int32 ActionId)
		{

			string sql=GetPrompterActionSelectClause();
			sql+="from PrompterAction where ActionId=@ActionId ";
			SqlParameter parameter=new SqlParameter("@ActionId",ActionId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return PrompterActionFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<PrompterAction> GetAllPrompterAction()
		{

			string sql=GetPrompterActionSelectClause();
			sql+="from PrompterAction";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<PrompterAction>(ds, PrompterActionFromDataRow);
		}

		public virtual List<PrompterAction> GetPagedPrompterAction(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetPrompterActionCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ActionId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ActionId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ActionId] ";
            tempsql += " FROM [PrompterAction] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ActionId"))
					tempsql += " , (AllRecords.[ActionId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ActionId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetPrompterActionSelectClause()+@" FROM [PrompterAction], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [PrompterAction].[ActionId] = PageIndex.[ActionId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<PrompterAction>(ds, PrompterActionFromDataRow);
			}else{ return null;}
		}

		private int GetPrompterActionCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM PrompterAction as AllRecords ";
			else
				sql = "SELECT Count(*) FROM PrompterAction as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(PrompterAction))]
		public virtual PrompterAction InsertPrompterAction(PrompterAction entity)
		{

			PrompterAction other=new PrompterAction();
			other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into PrompterAction ( [Name] )
				Values
				( @Name );
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)};
                var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return GetPrompterAction(identity);
            }
            return other;
		}

		[MOLog(AuditOperations.Update,typeof(PrompterAction))]
		public virtual PrompterAction UpdatePrompterAction(PrompterAction entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update PrompterAction set  [Name]=@Name 
							 where ActionId=@ActionId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ActionId",entity.ActionId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeletePrompterAction(System.Int32 ActionId)
		{

			string sql="delete from PrompterAction where ActionId=@ActionId";
			SqlParameter parameter=new SqlParameter("@ActionId",ActionId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(PrompterAction))]
		public virtual PrompterAction DeletePrompterAction(PrompterAction entity)
		{
			this.DeletePrompterAction(entity.ActionId);
			return entity;
		}


		public virtual PrompterAction PrompterActionFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			PrompterAction entity=new PrompterAction();
			entity.ActionId = (System.Int32)dr["ActionId"];
			entity.Name = dr["Name"].ToString();
			return entity;
		}

	}
	
	
}