﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class PrompterCommandTypeRepositoryBase : Repository, IPrompterCommandTypeRepositoryBase 
	{
        
        public PrompterCommandTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CommandTypeId",new SearchColumn(){Name="CommandTypeId",Title="CommandTypeId",SelectClause="CommandTypeId",WhereClause="AllRecords.CommandTypeId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetPrompterCommandTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetPrompterCommandTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetPrompterCommandTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetPrompterCommandTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "PrompterCommandType."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",PrompterCommandType."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual PrompterCommandType GetPrompterCommandType(System.Int32 CommandTypeId)
		{

			string sql=GetPrompterCommandTypeSelectClause();
			sql+="from PrompterCommandType where CommandTypeId=@CommandTypeId ";
			SqlParameter parameter=new SqlParameter("@CommandTypeId",CommandTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return PrompterCommandTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<PrompterCommandType> GetAllPrompterCommandType()
		{

			string sql=GetPrompterCommandTypeSelectClause();
			sql+="from PrompterCommandType";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<PrompterCommandType>(ds, PrompterCommandTypeFromDataRow);
		}

		public virtual List<PrompterCommandType> GetPagedPrompterCommandType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetPrompterCommandTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CommandTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CommandTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CommandTypeId] ";
            tempsql += " FROM [PrompterCommandType] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CommandTypeId"))
					tempsql += " , (AllRecords.[CommandTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CommandTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetPrompterCommandTypeSelectClause()+@" FROM [PrompterCommandType], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [PrompterCommandType].[CommandTypeId] = PageIndex.[CommandTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<PrompterCommandType>(ds, PrompterCommandTypeFromDataRow);
			}else{ return null;}
		}

		private int GetPrompterCommandTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM PrompterCommandType as AllRecords ";
			else
				sql = "SELECT Count(*) FROM PrompterCommandType as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(PrompterCommandType))]
		public virtual PrompterCommandType InsertPrompterCommandType(PrompterCommandType entity)
		{

			PrompterCommandType other=new PrompterCommandType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into PrompterCommandType ( [Type] )
				Values
				( @Type );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Type",entity.Type)};
                var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return GetPrompterCommandType(identity);
            }
            return other;


		}

		[MOLog(AuditOperations.Update,typeof(PrompterCommandType))]
		public virtual PrompterCommandType UpdatePrompterCommandType(PrompterCommandType entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update PrompterCommandType set  [Type]=@Type 
							 where CommandTypeId=@CommandTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Type",entity.Type)
					, new SqlParameter("@CommandTypeId",entity.CommandTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeletePrompterCommandType(System.Int32 CommandTypeId)
		{

			string sql="delete from PrompterCommandType where CommandTypeId=@CommandTypeId";
			SqlParameter parameter=new SqlParameter("@CommandTypeId",CommandTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(PrompterCommandType))]
		public virtual PrompterCommandType DeletePrompterCommandType(PrompterCommandType entity)
		{
			this.DeletePrompterCommandType(entity.CommandTypeId);
			return entity;
		}


		public virtual PrompterCommandType PrompterCommandTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			PrompterCommandType entity=new PrompterCommandType();
			entity.CommandTypeId = (System.Int32)dr["CommandTypeId"];
			entity.Type = dr["Type"].ToString();
			return entity;
		}

	}
	
	
}