﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class PrompterControllerActionRepositoryBase : Repository, IPrompterControllerActionRepositoryBase 
	{
        
        public PrompterControllerActionRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("PrompterControllerAction",new SearchColumn(){Name="PrompterControllerAction",Title="PrompterControllerAction",SelectClause="PrompterControllerAction",WhereClause="AllRecords.PrompterControllerAction",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CommandName",new SearchColumn(){Name="CommandName",Title="CommandName",SelectClause="CommandName",WhereClause="AllRecords.CommandName",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MappedValue",new SearchColumn(){Name="MappedValue",Title="MappedValue",SelectClause="MappedValue",WhereClause="AllRecords.MappedValue",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetPrompterControllerActionSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetPrompterControllerActionBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetPrompterControllerActionAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetPrompterControllerActionSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "PrompterControllerAction."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",PrompterControllerAction."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual PrompterControllerAction GetPrompterControllerAction(System.Int32 PrompterControllerAction)
		{

			string sql=GetPrompterControllerActionSelectClause();
			sql+="from PrompterControllerAction where PrompterControllerAction=@PrompterControllerAction ";
			SqlParameter parameter=new SqlParameter("@PrompterControllerAction",PrompterControllerAction);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return PrompterControllerActionFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<PrompterControllerAction> GetAllPrompterControllerAction()
		{

			string sql=GetPrompterControllerActionSelectClause();
			sql+="from PrompterControllerAction";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<PrompterControllerAction>(ds, PrompterControllerActionFromDataRow);
		}

		public virtual List<PrompterControllerAction> GetPagedPrompterControllerAction(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetPrompterControllerActionCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [PrompterControllerAction] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([PrompterControllerAction])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [PrompterControllerAction] ";
            tempsql += " FROM [PrompterControllerAction] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("PrompterControllerAction"))
					tempsql += " , (AllRecords.[PrompterControllerAction])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[PrompterControllerAction])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetPrompterControllerActionSelectClause()+@" FROM [PrompterControllerAction], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [PrompterControllerAction].[PrompterControllerAction] = PageIndex.[PrompterControllerAction] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<PrompterControllerAction>(ds, PrompterControllerActionFromDataRow);
			}else{ return null;}
		}

		private int GetPrompterControllerActionCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM PrompterControllerAction as AllRecords ";
			else
				sql = "SELECT Count(*) FROM PrompterControllerAction as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(PrompterControllerAction))]
		public virtual PrompterControllerAction InsertPrompterControllerAction(PrompterControllerAction entity)
		{

			PrompterControllerAction other=new PrompterControllerAction();
			other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into PrompterControllerAction ( [CommandName]
				,[MappedValue] )
				Values
				( @CommandName
				, @MappedValue )";
				
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@CommandName",entity.CommandName)
					, new SqlParameter("@MappedValue",entity.MappedValue)};
              var identity=  SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return GetPrompterControllerAction(identity);
            }
            return other;
		}

		[MOLog(AuditOperations.Update,typeof(PrompterControllerAction))]
		public virtual PrompterControllerAction UpdatePrompterControllerAction(PrompterControllerAction entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update PrompterControllerAction set  [CommandName]=@CommandName
							, [MappedValue]=@MappedValue 
							 where PrompterControllerAction=@PrompterControllerAction";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CommandName",entity.CommandName)
					, new SqlParameter("@MappedValue",entity.MappedValue)
					, new SqlParameter("@PrompterControllerAction",entity.PrompterControllerAction)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeletePrompterControllerAction(System.Int32 PrompterControllerAction)
		{

			string sql="delete from PrompterControllerAction where PrompterControllerAction=@PrompterControllerAction";
			SqlParameter parameter=new SqlParameter("@PrompterControllerAction",PrompterControllerAction);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(PrompterControllerAction))]
		public virtual PrompterControllerAction DeletePrompterControllerAction(PrompterControllerAction entity)
		{
			this.DeletePrompterControllerAction(entity.PrompterControllerAction);
			return entity;
		}


		public virtual PrompterControllerAction PrompterControllerActionFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			PrompterControllerAction entity=new PrompterControllerAction();
			entity.PrompterControllerAction = (System.Int32)dr["PrompterControllerAction"];
			entity.CommandName = dr["CommandName"].ToString();
			entity.MappedValue = (System.Int32)dr["MappedValue"];
			return entity;
		}

	}
	
	
}