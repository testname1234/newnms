﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using BolUserConsole.Core;

namespace BolUserConsole.Repository
{
		
	public partial class PrompterControllerActionRepository: PrompterControllerActionRepositoryBase, IPrompterControllerActionRepository
	{
        public virtual bool MappControllerActionById(System.Int32 PAId, System.Int32 MappedValue)
        {
            string sql = @"Update PrompterControllerAction set [MappedValue]=@MappedValue
							 where PrompterControllerAction=@PAId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@PAId",PAId)
					, new SqlParameter("@MappedValue",MappedValue)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return true;
        }
		
	}
	
	
}
