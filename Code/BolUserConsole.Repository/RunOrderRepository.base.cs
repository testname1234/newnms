﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using System.Data.SqlServerCe;

namespace BolUserConsole.Repository
{
		
	public abstract partial class RunOrderRepositoryBase : Repository, IRunOrderRepositoryBase 
	{
        
        public RunOrderRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("Slug",new SearchColumn(){Name="Slug",Title="Slug",SelectClause="Slug",WhereClause="AllRecords.Slug",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StartTime",new SearchColumn(){Name="StartTime",Title="StartTime",SelectClause="StartTime",WhereClause="AllRecords.StartTime",DataType="System.DateTime",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.DateTime",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RoId",new SearchColumn(){Name="RoId",Title="RoId",SelectClause="RoId",WhereClause="AllRecords.RoId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetRunOrderSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetRunOrderBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetRunOrderAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetRunOrderSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "RunOrder."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",RunOrder."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual RunOrder GetRunOrder(System.Int32 RoId)
		{

			string sql=GetRunOrderSelectClause();
			sql+="from RunOrder where RoId=@RoId ";
			SqlParameter parameter=new SqlParameter("@RoId",RoId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return RunOrderFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<RunOrder> GetAllRunOrder()
		{

			string sql=GetRunOrderSelectClause();
			sql+="from RunOrder";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrder>(ds, RunOrderFromDataRow);
		}

		public virtual List<RunOrder> GetPagedRunOrder(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetRunOrderCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [RoId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([RoId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [RoId] ";
            tempsql += " FROM [RunOrder] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("RoId"))
					tempsql += " , (AllRecords.[RoId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[RoId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetRunOrderSelectClause()+@" FROM [RunOrder], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [RunOrder].[RoId] = PageIndex.[RoId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrder>(ds, RunOrderFromDataRow);
			}else{ return null;}
		}

		private int GetRunOrderCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM RunOrder as AllRecords ";
			else
				sql = "SELECT Count(*) FROM RunOrder as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(RunOrder))]
		public virtual RunOrder InsertRunOrder(RunOrder entity)
		{

           // SqlCeEngine engine = new SqlCeEngine(@"Data Source=E:\Sqlce_DB\MyDatabase#1.sdf;Password=Axact123;");
           // engine.Upgrade(@"Data Source=E:\Sqlce_DB\MyDatabase#1.sdf;Password=Axact123;");

			RunOrder other=new RunOrder();
			other = entity;
				string sql=@"Insert into RunOrder ( [RoId]
				,[Slug]
				,[StartTime]
				,[Duration] )
				Values
				( @RoId
				, @Slug
				, @StartTime
				, @Duration );
";
				SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@RoId",entity.RoId)
					, new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@StartTime",entity.StartTime)
					, new SqlParameter("@Duration",entity.Duration)};
				SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		[MOLog(AuditOperations.Update,typeof(RunOrder))]
		public virtual RunOrder UpdateRunOrder(RunOrder entity)
		{

			string sql=@"Update RunOrder set  [Slug]=@Slug
							, [StartTime]=@StartTime
							, [Duration]=@Duration 
							 where RoId=@RoId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@RoId",entity.RoId)
					, new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@StartTime",entity.StartTime)
					, new SqlParameter("@Duration",entity.Duration)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteRunOrder(System.Int32 RoId)
		{

			string sql="delete from RunOrder where RoId=@RoId";
			SqlParameter parameter=new SqlParameter("@RoId",RoId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(RunOrder))]
		public virtual RunOrder DeleteRunOrder(RunOrder entity)
		{
			this.DeleteRunOrder(entity.RoId);
			return entity;
		}


		public virtual RunOrder RunOrderFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			RunOrder entity=new RunOrder();
			entity.Slug = dr["Slug"].ToString();
			entity.StartTime = (System.DateTime)dr["StartTime"];
			entity.Duration = (System.DateTime)dr["Duration"];
			entity.RoId = (System.Int32)dr["RoId"];
			return entity;
		}

	}
	
	
}