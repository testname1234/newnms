﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace BolUserConsole.Repository
{
		
	public partial class RunOrderRepository: RunOrderRepositoryBase, IRunOrderRepository
	{
        public int getMaxId() 
        {
            string sql = @"SELECT Max(RoId) FROM RunOrder";
            string d  = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql).ToString();
            if (!String.IsNullOrEmpty(d))
            {
                return int.Parse(d);
            }
            else 
            {
                return 0;
            }
        }
	}
	
	
}
