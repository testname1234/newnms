﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{

    public abstract partial class StoryItemRepositoryBase : Repository, IStoryItemRepositoryBase
    {

        public StoryItemRepositoryBase()
        {
            this.SearchColumns = new Dictionary<string, SearchColumn>();

            this.SearchColumns.Add("StoryItemId", new SearchColumn() { Name = "StoryItemId", Title = "StoryItemId", SelectClause = "StoryItemId", WhereClause = "AllRecords.StoryItemId", DataType = "System.Int32", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("StoryId", new SearchColumn() { Name = "StoryId", Title = "StoryId", SelectClause = "StoryId", WhereClause = "AllRecords.StoryId", DataType = "System.Int32", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("SequenceId", new SearchColumn() { Name = "SequenceId", Title = "SequenceId", SelectClause = "SequenceId", WhereClause = "AllRecords.SequenceId", DataType = "System.Int32", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Slug", new SearchColumn() { Name = "Slug", Title = "Slug", SelectClause = "Slug", WhereClause = "AllRecords.Slug", DataType = "System.String", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Detail", new SearchColumn() { Name = "Detail", Title = "Detail", SelectClause = "Detail", WhereClause = "AllRecords.Detail", DataType = "System.String", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Instructions", new SearchColumn() { Name = "Instructions", Title = "Instructions", SelectClause = "Instructions", WhereClause = "AllRecords.Instructions", DataType = "System.String", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
        }

        public virtual List<SearchColumn> GetStoryItemSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }



        public virtual Dictionary<string, string> GetStoryItemBasicSearchColumns()
        {
            Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetStoryItemAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }


        public virtual string GetStoryItemSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery = string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
                    if (keyValuePair.Value.IsForeignColumn)
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                        else
                        {
                            selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "StoryItem." + keyValuePair.Key;
                        }
                        else
                        {
                            selectQuery += ",StoryItem." + keyValuePair.Key;
                        }
                    }
                }
            }
            return "Select " + selectQuery + " ";
        }


        public virtual List<StoryItem> GetStoryItemByStoryId(System.Int32 StoryId)
        {

            string sql = GetStoryItemSelectClause();
            sql += "from StoryItem where StoryId=@StoryId  ";
            SqlParameter parameter = new SqlParameter("@StoryId", StoryId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<StoryItem>(ds, StoryItemFromDataRow);
        }

        public virtual StoryItem GetStoryItem(System.Int32 StoryItemId)
        {

            string sql = GetStoryItemSelectClause();
            sql += "from StoryItem where StoryItemId=@StoryItemId ";
            SqlParameter parameter = new SqlParameter("@StoryItemId", StoryItemId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return StoryItemFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual List<StoryItem> GetAllStoryItem()
        {

            string sql = GetStoryItemSelectClause();
            sql += "from StoryItem";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<StoryItem>(ds, StoryItemFromDataRow);
        }

        public virtual List<StoryItem> GetPagedStoryItem(string orderByClause, int pageSize, int startIndex, out int count, List<SearchColumn> searchColumns)
        {

            string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
            if (!String.IsNullOrEmpty(orderByClause))
            {
                KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
            }

            count = GetStoryItemCount(whereClause, searchColumns);
            if (count > 0)
            {
                if (count < startIndex) startIndex = (count / pageSize) * pageSize;

                int PageLowerBound = startIndex;
                int PageUpperBound = PageLowerBound + pageSize;
                string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [StoryItemId] int				   
				            );";

                //Insert into the temp table
                string tempsql = "INSERT INTO #PageIndex ([StoryItemId])";
                tempsql += " SELECT ";
                if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
                tempsql += " [StoryItemId] ";
                tempsql += " FROM [StoryItem] AllRecords";
                if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
                if (orderByClause.Length > 0)
                {
                    tempsql += " ORDER BY " + orderByClause;
                    if (!orderByClause.Contains("StoryItemId"))
                        tempsql += " , (AllRecords.[StoryItemId])";
                }
                else
                {
                    tempsql += " ORDER BY (AllRecords.[StoryItemId])";
                }

                // Return paged results
                string pagedResultsSql =
                    GetStoryItemSelectClause() + @" FROM [StoryItem], #PageIndex PageIndex WHERE ";
                pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString();
                pagedResultsSql += @" AND [StoryItem].[StoryItemId] = PageIndex.[StoryItemId] 
				                  ORDER BY PageIndex.IndexId;";
                pagedResultsSql += " drop table #PageIndex";
                sql = sql + tempsql + pagedResultsSql;
                sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<StoryItem>(ds, StoryItemFromDataRow);
            }
            else { return null; }
        }

        private int GetStoryItemCount(string whereClause, List<SearchColumn> searchColumns)
        {

            string sql = string.Empty;
            if (string.IsNullOrEmpty(whereClause))
                sql = "SELECT Count(*) FROM StoryItem as AllRecords ";
            else
                sql = "SELECT Count(*) FROM StoryItem as AllRecords where  " + whereClause;
            var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
            return rowCount == DBNull.Value ? 0 : (int)rowCount;
        }

        [MOLog(AuditOperations.Create, typeof(StoryItem))]
        public virtual StoryItem InsertStoryItem(StoryItem entity)
        {

            StoryItem other = new StoryItem();
            other = entity;
            string sql = @"Insert into StoryItem ( [StoryItemId]
				,[StoryId]
				,[SequenceId]
				,[Slug]
				,[Detail]
				,[Instructions] )
				Values
				( @StoryItemId
				, @StoryId
				, @SequenceId
				, @Slug
				, @Detail
				, @Instructions )";

            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@StoryItemId",entity.StoryItemId)
					, new SqlParameter("@StoryId",entity.StoryId)
					, new SqlParameter("@SequenceId",entity.SequenceId)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@Detail",entity.Detail)
					, new SqlParameter("@Instructions",entity.Instructions)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return null;
        }

        [MOLog(AuditOperations.Update, typeof(StoryItem))]
        public virtual StoryItem UpdateStoryItem(StoryItem entity)
        {

            string sql = @"Update StoryItem set  [StoryId]=@StoryId
							, [SequenceId]=@SequenceId
							, [Slug]=@Slug
							, [Detail]=@Detail
							, [Instructions]=@Instructions 
							 where StoryItemId=@StoryItemId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@StoryItemId",entity.StoryItemId)
					, new SqlParameter("@StoryId",entity.StoryId)
					, new SqlParameter("@SequenceId",entity.SequenceId)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@Detail",entity.Detail)
					, new SqlParameter("@Instructions",entity.Instructions)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return null;
        }

        public virtual bool DeleteStoryItem(System.Int32 StoryItemId)
        {

            string sql = "delete from StoryItem where StoryItemId=@StoryItemId";
            SqlParameter parameter = new SqlParameter("@StoryItemId", StoryItemId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        [MOLog(AuditOperations.Delete, typeof(StoryItem))]
        public virtual StoryItem DeleteStoryItem(StoryItem entity)
        {
            this.DeleteStoryItem(entity.StoryItemId);
            return entity;
        }


        public virtual StoryItem StoryItemFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            StoryItem entity = new StoryItem();
            entity.StoryItemId = (System.Int32)dr["StoryItemId"];
            entity.StoryId = (System.Int32)dr["StoryId"];
            entity.SequenceId = (System.Int32)dr["SequenceId"];
            entity.Slug = dr["Slug"].ToString();
            entity.Detail = dr["Detail"].ToString();
            entity.Instructions = dr["Instructions"].ToString();
            return entity;
        }

    }
	
		
}