﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using BolUserConsole.Core;





namespace BolUserConsole.Repository
{
		
	public partial class StoryItemRepository: StoryItemRepositoryBase, IStoryItemRepository
	{

        public virtual bool DeleteStoryItemByStoryId(System.Int32 StoryId)
        {
            string sql = "delete from StoryItem where StoryId=@StoryId";
            SqlParameter parameter = new SqlParameter("@StoryId", StoryId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public int getMaxId()
        {
            string sql = @"SELECT Max(StoryItemId) FROM StoryItem";
            string d = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql).ToString();
            if (!String.IsNullOrEmpty(d))
            {
                return int.Parse(d);
            }
            else
            {
                return 0;
            }
        }
		
	}
	
	
}
