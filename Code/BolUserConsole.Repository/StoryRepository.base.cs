﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{

    public abstract partial class StoryRepositoryBase : Repository, IStoryRepositoryBase
    {

        public StoryRepositoryBase()
        {
            this.SearchColumns = new Dictionary<string, SearchColumn>();

            this.SearchColumns.Add("RoId", new SearchColumn() { Name = "RoId", Title = "RoId", SelectClause = "RoId", WhereClause = "AllRecords.RoId", DataType = "System.Int32", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("SequenceId", new SearchColumn() { Name = "SequenceId", Title = "SequenceId", SelectClause = "SequenceId", WhereClause = "AllRecords.SequenceId", DataType = "System.Int32", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("IsPlayed", new SearchColumn() { Name = "IsPlayed", Title = "IsPlayed", SelectClause = "IsPlayed", WhereClause = "AllRecords.IsPlayed", DataType = "System.Boolean?", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("IsSkipped", new SearchColumn() { Name = "IsSkipped", Title = "IsSkipped", SelectClause = "IsSkipped", WhereClause = "AllRecords.IsSkipped", DataType = "System.Boolean", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Slug", new SearchColumn() { Name = "Slug", Title = "Slug", SelectClause = "Slug", WhereClause = "AllRecords.Slug", DataType = "System.String", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("StoryId", new SearchColumn() { Name = "StoryId", Title = "StoryId", SelectClause = "StoryId", WhereClause = "AllRecords.StoryId", DataType = "System.Int32", IsForeignColumn = false, IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
        }

        public virtual List<SearchColumn> GetStorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }



        public virtual Dictionary<string, string> GetStoryBasicSearchColumns()
        {
            Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetStoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }


        public virtual string GetStorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery = string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
                    if (keyValuePair.Value.IsForeignColumn)
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                        else
                        {
                            selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "Story." + keyValuePair.Key;
                        }
                        else
                        {
                            selectQuery += ",Story." + keyValuePair.Key;
                        }
                    }
                }
            }
            return "Select " + selectQuery + " ";
        }


        public virtual List<Story> GetStoryByRoId(System.Int32 RoId)
        {

            string sql = GetStorySelectClause();
            sql += "from Story where RoId=@RoId  ";
            SqlParameter parameter = new SqlParameter("@RoId", RoId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Story>(ds, StoryFromDataRow);
        }

        public virtual Story GetStory(System.Int32 StoryId)
        {

            string sql = GetStorySelectClause();
            sql += "from Story where StoryId=@StoryId ";
            SqlParameter parameter = new SqlParameter("@StoryId", StoryId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return StoryFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual List<Story> GetAllStory()
        {

            string sql = GetStorySelectClause();
            sql += "from Story";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Story>(ds, StoryFromDataRow);
        }

        public virtual List<Story> GetPagedStory(string orderByClause, int pageSize, int startIndex, out int count, List<SearchColumn> searchColumns)
        {

            string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
            if (!String.IsNullOrEmpty(orderByClause))
            {
                KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
            }

            count = GetStoryCount(whereClause, searchColumns);
            if (count > 0)
            {
                if (count < startIndex) startIndex = (count / pageSize) * pageSize;

                int PageLowerBound = startIndex;
                int PageUpperBound = PageLowerBound + pageSize;
                string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [StoryId] int				   
				            );";

                //Insert into the temp table
                string tempsql = "INSERT INTO #PageIndex ([StoryId])";
                tempsql += " SELECT ";
                if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
                tempsql += " [StoryId] ";
                tempsql += " FROM [Story] AllRecords";
                if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
                if (orderByClause.Length > 0)
                {
                    tempsql += " ORDER BY " + orderByClause;
                    if (!orderByClause.Contains("StoryId"))
                        tempsql += " , (AllRecords.[StoryId])";
                }
                else
                {
                    tempsql += " ORDER BY (AllRecords.[StoryId])";
                }

                // Return paged results
                string pagedResultsSql =
                    GetStorySelectClause() + @" FROM [Story], #PageIndex PageIndex WHERE ";
                pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString();
                pagedResultsSql += @" AND [Story].[StoryId] = PageIndex.[StoryId] 
				                  ORDER BY PageIndex.IndexId;";
                pagedResultsSql += " drop table #PageIndex";
                sql = sql + tempsql + pagedResultsSql;
                sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<Story>(ds, StoryFromDataRow);
            }
            else { return null; }
        }

        private int GetStoryCount(string whereClause, List<SearchColumn> searchColumns)
        {

            string sql = string.Empty;
            if (string.IsNullOrEmpty(whereClause))
                sql = "SELECT Count(*) FROM Story as AllRecords ";
            else
                sql = "SELECT Count(*) FROM Story as AllRecords where  " + whereClause;
            var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
            return rowCount == DBNull.Value ? 0 : (int)rowCount;
        }

        [MOLog(AuditOperations.Create, typeof(Story))]
        public virtual Story InsertStory(Story entity)
        {

            Story other = new Story();
            other = entity;
            string sql = @"Insert into Story ( [StoryId]
				,[RoId]
				,[SequenceId]
				,[IsPlayed]
				,[IsSkipped]
				,[Slug] )
				Values
				( @StoryId
				, @RoId
				, @SequenceId
				, @IsPlayed
				, @IsSkipped
				, @Slug );
";
            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@StoryId",entity.StoryId)
					, new SqlParameter("@RoId",entity.RoId)
					, new SqlParameter("@SequenceId",entity.SequenceId)
					, new SqlParameter("@IsPlayed",entity.IsPlayed ?? (object)DBNull.Value)
					, new SqlParameter("@IsSkipped",entity.IsSkipped)
					, new SqlParameter("@Slug",entity.Slug)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return null;
        }

        [MOLog(AuditOperations.Update, typeof(Story))]
        public virtual Story UpdateStory(Story entity)
        {

            string sql = @"Update Story set  [RoId]=@RoId
							, [SequenceId]=@SequenceId
							, [IsPlayed]=@IsPlayed
							, [IsSkipped]=@IsSkipped
							, [Slug]=@Slug 
							 where StoryId=@StoryId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@StoryId",entity.StoryId)
					, new SqlParameter("@RoId",entity.RoId)
					, new SqlParameter("@SequenceId",entity.SequenceId)
					, new SqlParameter("@IsPlayed",entity.IsPlayed ?? (object)DBNull.Value)
					, new SqlParameter("@IsSkipped",entity.IsSkipped)
					, new SqlParameter("@Slug",entity.Slug)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return null;
        }

        public virtual bool DeleteStory(System.Int32 StoryId)
        {

            string sql = "delete from Story where StoryId=@StoryId";
            SqlParameter parameter = new SqlParameter("@StoryId", StoryId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        [MOLog(AuditOperations.Delete, typeof(Story))]
        public virtual Story DeleteStory(Story entity)
        {
            this.DeleteStory(entity.StoryId);
            return entity;
        }


        public virtual Story StoryFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            Story entity = new Story();
            entity.RoId = (System.Int32)dr["RoId"];
            entity.SequenceId = (System.Int32)dr["SequenceId"];
            entity.IsPlayed = dr["IsPlayed"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsPlayed"];
            entity.IsSkipped = (System.Boolean)dr["IsSkipped"];
            entity.Slug = dr["Slug"].ToString();
            entity.StoryId = (System.Int32)dr["StoryId"];
            return entity;
        }

    }
		
}