﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using BolUserConsole.Core;


namespace BolUserConsole.Repository
{
		
	public partial class StoryRepository: StoryRepositoryBase, IStoryRepository
	{

        public virtual bool DeleteStoryByRoId(System.Int32 RoId)
        {
            string sql = "delete from Story where RoId=@RoId";
            SqlParameter parameter = new SqlParameter("@RoId", RoId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual bool UpdateSeqenceByStoryId(System.Int32 StoryId, System.Int32 UpdatedSequenceId)
        {
            string sql = @"Update Story set [SequenceId]=@UpdatedSequenceId
							 where StoryId=@StoryId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@StoryId",StoryId)
					, new SqlParameter("@UpdatedSequenceId",UpdatedSequenceId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return true;
        }
        public int getMaxId()
        {
            string sql = @"SELECT Max(StoryId) FROM Story";
            string d = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql).ToString();
            if (!String.IsNullOrEmpty(d))
            {
                return int.Parse(d);
            }
            else
            {
                return 0;
            }
        }
		
	}
	
	
}
    