﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class UserActionRepositoryBase : Repository, IUserActionRepositoryBase 
	{
        
        public UserActionRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ActionId",new SearchColumn(){Name="ActionId",Title="ActionId",SelectClause="ActionId",WhereClause="AllRecords.ActionId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RoId",new SearchColumn(){Name="RoId",Title="RoId",SelectClause="RoId",WhereClause="AllRecords.RoId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StoryId",new SearchColumn(){Name="StoryId",Title="StoryId",SelectClause="StoryId",WhereClause="AllRecords.StoryId",DataType="System.Int32?",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserActionId",new SearchColumn(){Name="UserActionId",Title="UserActionId",SelectClause="UserActionId",WhereClause="AllRecords.UserActionId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetUserActionSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetUserActionBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetUserActionAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetUserActionSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "UserAction."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",UserAction."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<UserAction> GetUserActionByActionId(System.Int32 ActionId)
		{

			string sql=GetUserActionSelectClause();
			sql+="from UserAction where ActionId=@ActionId  ";
			SqlParameter parameter=new SqlParameter("@ActionId",ActionId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserAction>(ds,UserActionFromDataRow);
		}

		public virtual List<UserAction> GetUserActionByRoId(System.Int32 RoId)
		{

			string sql=GetUserActionSelectClause();
			sql+="from UserAction where RoId=@RoId  ";
			SqlParameter parameter=new SqlParameter("@RoId",RoId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserAction>(ds,UserActionFromDataRow);
		}

		public virtual List<UserAction> GetUserActionByStoryId(System.Int32? StoryId)
		{

			string sql=GetUserActionSelectClause();
			sql+="from UserAction where StoryId=@StoryId  ";
			SqlParameter parameter=new SqlParameter("@StoryId",StoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserAction>(ds,UserActionFromDataRow);
		}

		public virtual List<UserAction> GetUserActionByUserId(System.Int32 UserId)
		{

			string sql=GetUserActionSelectClause();
			sql+="from UserAction where UserId=@UserId  ";
			SqlParameter parameter=new SqlParameter("@UserId",UserId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserAction>(ds,UserActionFromDataRow);
		}

		public virtual UserAction GetUserAction(System.Int32 UserActionId)
		{

			string sql=GetUserActionSelectClause();
			sql+="from UserAction where UserActionId=@UserActionId ";
			SqlParameter parameter=new SqlParameter("@UserActionId",UserActionId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return UserActionFromDataRow(ds.Tables[0].Rows[0]);
		}

		public virtual List<UserAction> GetAllUserAction()
		{

			string sql=GetUserActionSelectClause();
			sql+="from UserAction";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserAction>(ds, UserActionFromDataRow);
		}

		public virtual List<UserAction> GetPagedUserAction(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetUserActionCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [UserActionId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([UserActionId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [UserActionId] ";
            tempsql += " FROM [UserAction] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("UserActionId"))
					tempsql += " , (AllRecords.[UserActionId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[UserActionId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetUserActionSelectClause()+@" FROM [UserAction], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [UserAction].[UserActionId] = PageIndex.[UserActionId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserAction>(ds, UserActionFromDataRow);
			}else{ return null;}
		}

		private int GetUserActionCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM UserAction as AllRecords ";
			else
				sql = "SELECT Count(*) FROM UserAction as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(UserAction))]
		public virtual UserAction InsertUserAction(UserAction entity)
		{

			UserAction other=new UserAction();
			other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into UserAction ( [ActionId]
				,[CreationDate]
				,[RoId]
				,[StoryId]
				,[UserId] )
				Values
				( @ActionId
				, @CreationDate
				, @RoId
				, @StoryId
				, @UserId )";

                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@ActionId",entity.ActionId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@RoId",entity.RoId)
					, new SqlParameter("@StoryId",entity.StoryId ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId)};
              var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return GetUserAction(identity);
            }
            return other;
		}

		[MOLog(AuditOperations.Update,typeof(UserAction))]
		public virtual UserAction UpdateUserAction(UserAction entity)
		{

			if (entity.IsTransient()) return entity;
			string sql=@"Update UserAction set  [ActionId]=@ActionId
							, [CreationDate]=@CreationDate
							, [RoId]=@RoId
							, [StoryId]=@StoryId
							, [UserId]=@UserId 
							 where UserActionId=@UserActionId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ActionId",entity.ActionId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@RoId",entity.RoId)
					, new SqlParameter("@StoryId",entity.StoryId ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@UserActionId",entity.UserActionId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteUserAction(System.Int32 UserActionId)
		{

			string sql="delete from UserAction where UserActionId=@UserActionId";
			SqlParameter parameter=new SqlParameter("@UserActionId",UserActionId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(UserAction))]
		public virtual UserAction DeleteUserAction(UserAction entity)
		{
			this.DeleteUserAction(entity.UserActionId);
			return entity;
		}

		public virtual UserAction UserActionFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			UserAction entity=new UserAction();
			entity.ActionId = (System.Int32)dr["ActionId"];
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			entity.RoId = (System.Int32)dr["RoId"];
			entity.StoryId = dr["StoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["StoryId"];
			entity.UserId = (System.Int32)dr["UserId"];
			entity.UserActionId = (System.Int32)dr["UserActionId"];
			return entity;
		}


	}
		
}