﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using BolUserConsole.Core;

namespace BolUserConsole.Repository
{
		
	public partial class UserActionRepository: UserActionRepositoryBase, IUserActionRepository
	{
        public virtual bool DeleteUserActionByRoId(System.Int32 RoId)
        {
            string sql = "delete from UserAction where RoId=@RoId";
            SqlParameter parameter = new SqlParameter("@RoId", RoId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

	}
	
	
}
