﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using BolUserConsole.Core;
using BolUserConsole.Core.Entities;
using BolUserConsole.Core.DataInterfaces;

namespace BolUserConsole.Repository
{
		
	public abstract partial class UserRepositoryBase : Repository, IUserRepositoryBase 
	{
        
        public UserRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SessionId",new SearchColumn(){Name="SessionId",Title="SessionId",SelectClause="SessionId",WhereClause="AllRecords.SessionId",DataType="System.String",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastLoginTime",new SearchColumn(){Name="LastLoginTime",Title="LastLoginTime",SelectClause="LastLoginTime",WhereClause="AllRecords.LastLoginTime",DataType="System.DateTime?",IsForeignColumn=false,IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetUserSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetUserBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetUserAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetUserSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[User]."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",[User]."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual User GetUser(System.Int32 UserId)
		{
			string sql=GetUserSelectClause();
			sql+="from [User] where UserId=@UserId ";
			SqlParameter parameter=new SqlParameter("@UserId",UserId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return UserFromDataRow(ds.Tables[0].Rows[0]);

		}

		public virtual List<User> GetAllUser()
		{

			string sql=GetUserSelectClause();
			sql+="from [User]";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<User>(ds, UserFromDataRow);
		}

		public virtual List<User> GetPagedUser(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetUserCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [UserId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([UserId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [UserId] ";
            tempsql += " FROM [User] AllRecords";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("UserId"))
					tempsql += " , (AllRecords.[UserId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[UserId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                GetUserSelectClause()+@" FROM [User], #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [User].[UserId] = PageIndex.[UserId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<User>(ds, UserFromDataRow);
			}else{ return null;}
		}

		private int GetUserCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM User as AllRecords ";
			else
				sql = "SELECT Count(*) FROM User as AllRecords where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(User))]
		public virtual User InsertUser(User entity)
		{

			User other=new User();
			other = entity;
				string sql=@"Insert into [User] ( [UserId]
				,[SessionId]
				,[LastLoginTime] )
				Values
				( @UserId
				, @SessionId
				, @LastLoginTime )";

				SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@SessionId",entity.SessionId)
					, new SqlParameter("@LastLoginTime",entity.LastLoginTime ?? (object)DBNull.Value)};
				SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		[MOLog(AuditOperations.Update,typeof(User))]
		public virtual User UpdateUser(User entity)
		{

			string sql=@"Update [User] set  [SessionId]=@SessionId
							, [LastLoginTime]=@LastLoginTime 
							 where UserId=@UserId";

			SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@SessionId",entity.SessionId)
					, new SqlParameter("@LastLoginTime",entity.LastLoginTime ?? (object)DBNull.Value)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return null;
		}

		public virtual bool DeleteUser(System.Int32 UserId)
		{

			string sql="delete from [User] where UserId=@UserId";
			SqlParameter parameter=new SqlParameter("@UserId",UserId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(User))]
		public virtual User DeleteUser(User entity)
		{
			this.DeleteUser(entity.UserId);
			return entity;
		}


		public virtual User UserFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			User entity=new User();
			entity.UserId = (System.Int32)dr["UserId"];
			entity.SessionId = dr["SessionId"].ToString();
			entity.LastLoginTime = dr["LastLoginTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastLoginTime"];
			return entity;
		}

	}
	
	
}