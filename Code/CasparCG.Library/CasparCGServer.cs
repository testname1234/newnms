﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using CasparCGLib.Entities;
using CasparCGLib.Enums;
using System.Net;
using MOSProtocol.Lib.Entities;
using MOSProtocol.Lib;
using Common;

namespace CasparCGLib
{
    public class CasparCGServer
    {
        #region private proprties

        private string _hostName { get; set; }
        private int _port { get; set; }

        private string PlayCommand = "Play [CHANNELID]-[LAYER] \"[URL]\" [LOOP]";
        private string CGAddCommand = "CG [CHANNELID]-[LAYER] ADD [FLASHLAYER] [URL] [PLAY_ON_LOAD]";
        private string TransformationCommand = "MIXER [CHANNELID]-[LAYER] FILL [X] [Y] [X_SCALE] [Y_SCALE]";
        private string CropCommand = "MIXER [CHANNELID]-[LAYER] CLIP [CROP_LEFT] [CROP_RIGHT] [CROP_TOP] [CROP_BOTTOM]";
        private string DecklinkCommand = "Play [CHANNELID]-[LAYER] Decklink [DECKLINK_ID]";
        private string ChromaCommand = "MIXER [CHANNELID]-[LAYER] CHROMA [KEY] [THRESHOLD_LOWER] [THRESHOLD_UPPER]";

        private TcpClient client = new TcpClient();
        private NetworkStream networkStream;

        #endregion

        #region public properties

        public bool IsConnected
        {
            get
            {
                if (client != null)
                {
                    return client.Connected;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        #region public methods

        public CasparCGServer(string HostName, int Port)
        {
            _hostName = HostName;
            _port = Port;
        }

        public void Connect()
        {
            if (!client.Connected)
            {
                client.Connect(_hostName, _port);
                networkStream = client.GetStream();
            }
        }

        public void Disconnect()
        {
            if (client.Connected)
            {
                client.Close();
                networkStream = null;
            }
        }

        public void ClearChannel()
        {
            Common.Helper.SendData(networkStream, string.Format("Clear 1 \r\n"), System.Text.Encoding.UTF8);
        }

        public void LoadRundown(string path)
        {
            string message = string.Empty;
            List<item> itemList = new List<item>();
            using (StreamReader reader = new StreamReader(path))
            {
                string xml = reader.ReadToEnd();
                itemList = Common.Helper.DeserializeFromXml<List<item>>(xml, "items");
            }

            PlayRundown(itemList);
        }

        public void PlayItem(item _item)
        {
            string message = string.Empty;
            switch (_item.ItemType)
            {
                case CasparItemType.TEMPLATE:
                    Common.Helper.SendData(networkStream, GetAMCPCommandForItem(_item, CGAddCommand), System.Text.Encoding.UTF8);
                    break;
                case CasparItemType.MOVIE:
                    Common.Helper.SendData(networkStream, GetAMCPCommandForItem(_item, PlayCommand), System.Text.Encoding.UTF8);
                    break;
                case CasparItemType.STILL:
                    Common.Helper.SendData(networkStream, GetAMCPCommandForItem(_item, PlayCommand), System.Text.Encoding.UTF8);
                    break;
                case CasparItemType.TRANSFORMATION:
                    Common.Helper.SendData(networkStream, GetAMCPCommandForItem(_item, TransformationCommand), System.Text.Encoding.UTF8);
                    break;
                case CasparItemType.CROP:
                    Common.Helper.SendData(networkStream, GetAMCPCommandForItem(_item, CropCommand), System.Text.Encoding.UTF8);
                    break;
                case CasparItemType.DECKLINKINPUT:
                    Common.Helper.SendData(networkStream, GetAMCPCommandForItem(_item, DecklinkCommand), System.Text.Encoding.UTF8);
                    break;
                case CasparItemType.CHROMAKEY:
                    Common.Helper.SendData(networkStream, GetAMCPCommandForItem(_item, ChromaCommand), System.Text.Encoding.UTF8);
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region private method

        public void PlayRundown(List<item> list)
        {
            ClearChannel();
            foreach (item _item in list)
            {
                PlayItem(_item);
            }
        }

        private string GetAMCPCommandForItem(item _item, string command)
        {
            command = command.Replace("[CHANNELID]", _item.Channel).Replace("[LAYER]", _item.Videolayer)
                     .Replace("[URL]", _item.Name).Replace("[LOOP]", _item.Loop == "True" ? "LOOP" : string.Empty)
                     .Replace("[FLASHLAYER]", _item.Flashlayer).Replace("[PLAY_ON_LOAD]", "1").Replace("[DATA]", "")
                     .Replace("[X]", _item.Positionx).Replace("[Y]", _item.Positiony).Replace("[X_SCALE]", _item.Scalex)
                     .Replace("[Y_SCALE]", _item.Scaley).Replace("[DECKLINK_ID]", _item.Device).Replace("[KEY]", _item.Key)
                     .Replace("[THRESHOLD_LOWER]", _item.Threshold).Replace("[THRESHOLD_UPPER]", "0.20").Replace("[CROP_LEFT]", _item.CropLeft)
                     .Replace("[CROP_RIGHT]", _item.CropRight).Replace("[CROP_TOP]", _item.CropTop).Replace("[CROP_BOTTOM]", _item.CropBottom);
            command = string.Format("{0}\r\n", command);
            return command;
        }

        #endregion
    }
}
