﻿using System;

namespace CasparCGLib.Enums
{
    public enum CasparItemType
    {
        TEMPLATE = 1,
        MOVIE = 2,
        TRANSFORMATION = 3,
        STILL = 4,
        AUDIO = 5,
        CROP = 6,
        DECKLINKINPUT = 7,
        CHROMAKEY =8
    }
}
