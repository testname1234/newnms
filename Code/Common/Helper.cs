﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Xml.Serialization;

namespace Common
{
    public static class Helper
    {
        public static T DeserializeFromXml<T>(string xml,string rootAttr=null)
        {
            T result;
            XmlSerializer ser ;
            if (!string.IsNullOrEmpty(rootAttr))
            {
                XmlRootAttribute root = new XmlRootAttribute("items");
                ser = new XmlSerializer(typeof(T),root);
            }
            else
            {
                ser = new XmlSerializer(typeof(T));
            }
            
            using (TextReader tr = new StringReader(xml))
            {
                try
                {
                    result = (T)ser.Deserialize(tr);
                }
                catch
                {
                    result = default(T);
                }
            }
            return result;
        }

        public static void SendData(NetworkStream networkStream, string data,System.Text.Encoding encoding)
        {
            byte[] outStream = encoding.GetBytes(data);
            networkStream.Write(outStream, 0, outStream.Length);
            networkStream.Flush();
        }
    }
}
