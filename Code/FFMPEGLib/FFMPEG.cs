﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using FFMPEGLib.Helper;
using Newtonsoft.Json;

namespace FFMPEGLib
{
    public class FFMPEG
    {
        public static string InitialPath = AppDomain.CurrentDomain.BaseDirectory + "/ffmpeg";

        public static double GetDuration(string file)
        {
            return TimeSpan.FromMilliseconds(GetDurationInMilliSeconds(file)).TotalSeconds;
        }

        public static void ApplyRgb(string source, string destinition, double Red, double Green, double Blue)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", "-i \"" + source + "\" -vf mp=eq2=1:1:0:1:" + Red + ":" + Green + ":" + Blue + " -y \"" + destinition + "\"");
        }

        public static void BrightenVideo(string source, string destinition, double Brightness)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", "-i \"" + source + "\" -vf mp=eq2=1:1:" + Brightness + ":1:1:1:1 -y \"" + destinition + "\"");
        }
        public static void ContrastVideo(string source, string destinition, double contrast)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", "-i \"" + source + "\" -vf mp=eq2=1:" + contrast + ":0:1:1:1:1 -y \"" + destinition + "\"");
        }

        public static void SaturateVideo(string source, string destinition, double saturation)
        {
            //-vf mp=eq2=gamma:contrast:brightness:saturation:rg:gg:bg:weight
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", "-i \"" + source + "\" -vf mp=eq2=1:1:0:" + saturation + ":1:1:1 -y \"" + destinition + "\"");
        }

        public static void blurVideo(string source, string destinition, TimeSpan from, TimeSpan to)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", "-i \"" + source + "\" -vf \"boxblur=enable='between(t," + from.Seconds + "," + to.Seconds + ")'\" -codec:a -y \"" + destinition + "\"");
        }

        public static void HueVideo(string source, string destinition, int degree)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", "-i \"" + source + "\" -vf hue=h=" + degree + ":s=1 -y \"" + destinition + "\"");
        }

        public static void MotionVideo(string source, string destinition, double speed)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", "-i \"" + source + "\" -filter:v \"setpts=" + speed + "*PTS\" -y \"" + destinition + "\"");
        }

        public static void ClipVideo(string source, TimeSpan from, TimeSpan to, string destination)
        {
            TimeSpan duration = to - from;
            string toTime = string.Empty;
            if (to.TotalMilliseconds > 0)
            {
                toTime = " -t " + duration.ToString() + " ";
            }
            //ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-ss " + from.ToString() + " -i \"" + source + "\"  -movflags +faststart -ar 22050 -vcodec libx264 -y \"" + destination + "\""));
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-ss " + from.ToString() + " -i \"" + source + "\" -movflags +faststart -vcodec libx264 " + toTime + " -y \"" + destination + "\""));
        }

        public static void ClipVideoWith420pix(string source, TimeSpan from, TimeSpan to, string destination, bool enableEncoding = false)
        {
            TimeSpan duration = to - from;
            string toTime = string.Empty;
            if (to.TotalMilliseconds > 0)
            {
                toTime = " -t " + duration.ToString() + " ";
            }
            toTime = " -pix_fmt yuv420p -vf \"setsar=1/1\" " + toTime;
            if (enableEncoding)
                toTime = toTime + " -vcodec libx264 ";
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-ss " + from.ToString() + " -i \"" + source + "\" " + toTime + " -y \"" + destination + "\""));
        }

        public static void ClipVideo(string source, int from, int to, string destination, int fps)
        {
            TimeSpan fromTS = TimeSpan.FromMilliseconds(((1 / from) * fps) * 1000);
            TimeSpan toTS = TimeSpan.FromMilliseconds(((1 / to) * fps) * 1000);
            TimeSpan duration = toTS - fromTS;
            string toTime = string.Empty;
            if (toTS.TotalMilliseconds > 0)
            {
                toTime = " -t " + duration.ToString() + " ";
            }
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-ss " + fromTS.ToString() + " -i \"" + source + "\" -c copy " + toTime + destination));
        }

        public static void CreateThumb(string source, string destination, string thumbPath, TimeSpan? ts = null)
        {
            if (!Directory.Exists(thumbPath))
            {
                Directory.CreateDirectory(thumbPath);
            }
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\"" + (!ts.HasValue ? "" : " -ss " + ts.Value.ToString()) + " -vf  \"thumbnail,scale=300:168\" -frames:v 1 -y \"" + thumbPath + destination + "\""));
        }

        public static void CreateVideofromThumb(string source, string destination, string thumbPath, TimeSpan ts)
        {
            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-loop 1 -i " + source + " -r 25 -t " + ts.ToString() + " -pix_fmt yuv420p -y " + destination + thumbPath));
        }


        public static void CreateThumbs(string source, double fps, string thumbsFolderPath)
        {
            if (!Directory.Exists(thumbsFolderPath))
            {
                Directory.CreateDirectory(thumbsFolderPath);
                ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -f image2 -s 200x200 -vf fps=fps=" + fps + " " + thumbsFolderPath + "/thumb%d.png"));
            }
        }

        public static void ConvertToMp4(string source, string destination)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -movflags +faststart -qscale 0 -ar 22050 -vcodec libx264 -y \"" + destination + "\""));
        }

        public static void ConvertToCustomCodec(string source, string destination, string codec)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" " + codec + " -y \"" + destination + "\""));
        }

        public static void ConvertToAudioHighResCodec(string source, string destination, string codec)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -acodec libmp3lame -ab 256k \"" + destination + "\""));
        }

        public static void ConvertToAudioLowResCodec(string source, string destination, string codec)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -acodec libmp3lame -ab 56k \"" + destination + "\""));
        }

        public static void ConvertToMP4ByHandBrake(string source, string destination)
        {
            ExecuteProcess(InitialPath + @"/HandBrakeCLI.exe", string.Format(" -i \"" + source + "\" -o \"" + destination + "\"  -e x264 -r 15 -b 128 -B 32 -R44.1 -X 720 -Y 576"));
        }

        public static void ConvertToMP4ForChrome(string source, string destination)
        {
            //ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format(" -i \"" + source + "\" -movflags +faststart -y -strict experimental -acodec aac -ac 2 -ab 160k -vcodec libx264 -pix_fmt yuv420p -preset slow -profile:v baseline \"" + destination + "\""));
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format(" -i \"" + source + "\" -movflags +faststart -qscale 0 -ar 22050 -vcodec libx264 -s 432x408 \"" + destination + "\""));
        }

        public static void CreateSnapshots(string source, double fps, string snapshotsFolderPath, int quality = 35)
        {
            if (!Directory.Exists(snapshotsFolderPath))
            {
                Directory.CreateDirectory(snapshotsFolderPath);
                ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -q " + quality + " -vf fps=fps=" + fps + " \"" + snapshotsFolderPath + "/snap%d.png\""));
            }
        }

        public static void CreateSnapshotsAfterThisSecond(string source, double fps, string snapshotsFolderPath, int second, int quality = 35)
        {
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -ss " + second + " -q " + quality + " -s 720x576 -vf fps=fps=" + fps + " \"" + snapshotsFolderPath + "/%d.jpg\""));
        }

        public static void CreateSnapshot(string source, TimeSpan frameTime, string snapshotsFolderPath, string fileName)
        {
            if (!Directory.Exists(snapshotsFolderPath))
            {
                Directory.CreateDirectory(snapshotsFolderPath);
            }
            if (!File.Exists(snapshotsFolderPath + fileName))
            {
                ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-ss " + frameTime.ToString() + " -i \"" + source + "\" -vframes:v 1 " + snapshotsFolderPath + fileName));
            }
        }

        public static double GetDurationInMilliSeconds(string source)
        {
            string output = ExecuteProcessWithOutputReadAsync(InitialPath + @"/ffprobe.exe", string.Format("-i \"" + source + "\" -show_format"));
            if (!string.IsNullOrEmpty(output))
            {
                var duration = output.Split('\n').Where(x => x.StartsWith("duration=")).FirstOrDefault();
                return Convert.ToDouble(duration.Split('=')[1]) * 1000;
            }
            return -1;
        }

        public static FfProbeInformation GetMediaInformation(string source)
        {
            string output = ExecuteProcessWithOutput(InitialPath + @"/ffprobe.exe", string.Format(" -v quiet -print_format json -show_format -show_streams \"" + source + "\""));
            return JsonConvert.DeserializeObject<FfProbeInformation>(output);
        }

        public static List<string> BlendVideos(string inp1, TimeSpan from1, TimeSpan to1, TimeSpan from2, TimeSpan to2, string inp2, string output, TimeSpan clip1Total, TimeSpan transtime, TransitionTypes type, string format, bool enableEncoding = false)
        {
            string fromFormat = "mp4";
            List<string> clipUrls = new List<string>();
            TimeSpan timezero = new TimeSpan(0);
            TimeSpan timenegative = new TimeSpan(-1);

            if (!File.Exists(inp1.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from1.TotalMilliseconds + "-" + to1.TotalMilliseconds + "-f." + format)))
                ClipVideoWith420pix(inp1, to1 - transtime, to1, inp1.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from1.TotalMilliseconds + "-" + to1.TotalMilliseconds + "-f." + format), enableEncoding);
            if (!File.Exists(inp2.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from2.TotalMilliseconds + "-" + to2.TotalMilliseconds + "-s." + format)))
                ClipVideoWith420pix(inp2, from2, from2 + transtime, inp2.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from2.TotalMilliseconds + "-" + to2.TotalMilliseconds + "-s." + format), enableEncoding);
            if (!File.Exists(inp1.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from1.TotalMilliseconds + "-" + to1.TotalMilliseconds + "-ff." + format)))
                ClipVideoWith420pix(inp1, from1, to1 - transtime, inp1.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from1.TotalMilliseconds + "-" + to1.TotalMilliseconds + "-ff." + format), enableEncoding);
            if (!File.Exists(inp2.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from2.TotalMilliseconds + "-" + to2.TotalMilliseconds + "-ss." + format)))
                ClipVideoWith420pix(inp2, from2 + transtime, to2, inp2.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from2.TotalMilliseconds + "-" + to2.TotalMilliseconds + "-ss." + format), enableEncoding);

            clipUrls.Add(inp1.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from1.TotalMilliseconds + "-" + to1.TotalMilliseconds + "-ff." + format));
            clipUrls.Add(output);
            clipUrls.Add(inp2.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from2.TotalMilliseconds + "-" + to2.TotalMilliseconds + "-ss." + format));

            if (File.Exists(output))
                File.Delete(output);

            if (type == TransitionTypes.Fade)
            {
                ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i " + inp1.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from1.TotalMilliseconds + "-" + to1.TotalMilliseconds + "-f." + format) + " -i " + inp2.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from2.TotalMilliseconds + "-" + to2.TotalMilliseconds + "-s." + format) + " -filter_complex \"blend=all_expr='A*(1-(if(gte(T," + transtime.TotalSeconds + "),1,T/" + transtime.TotalSeconds + ")))+B*((if(gte(T," + transtime.TotalSeconds + "),1,T/" + transtime.TotalSeconds + ")))'\" -t 6  -y " + output));
            }
            else if (type == TransitionTypes.UnCoverLeft)
            {
                ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-y -i " + inp1.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from1.TotalMilliseconds + "-" + to1.TotalMilliseconds + "-f." + format) + " -i " + inp2.Replace("/Videos/", "/Clips/").Replace("." + fromFormat, from2.TotalMilliseconds + "-" + to2.TotalMilliseconds + "-s." + format) + " -filter_complex \"blend=all_expr='if(gte(0,X-N*SH*T*10),B,A)'\" -y " + output));
            }
            return clipUrls;
        }

        public static void EmbedImage(string source, string overlayImage, string output, double top, double left, double right, double bottom, TimeSpan from, TimeSpan to)
        {
            if (File.Exists(output))
                File.Delete(output);

            double width = right - left;
            double height = bottom - top;
            //-i \"" + source + "\" -i \"" + overlayImage + "\" -filter_complex "[1:v]scale=" + width + ":" + height + " [ovrl],[0:v][ovrl] overlay=" + top + ":" + left + ":enable='between(t,0,20)'" -y \"" + output +"\""
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -i \"" + overlayImage + "\" -filter_complex \"[1:v]scale=" + width + ":" + height + " [ovrl],[0:v][ovrl] overlay=" + left + ":" + top + ":enable='between(t," + from.TotalSeconds.ToString() + "," + to.TotalSeconds.ToString() + ")'\" -y \"" + output + "\""));

        }

        public static void EmbedVideo(string source, string overlayVideo, string output, double top, double left, double right, double bottom, TimeSpan from, TimeSpan to)
        {
            if (File.Exists(output))
                File.Delete(output);

            double width = right - left;
            double height = bottom - top;
            //-i out.mp4 -i 1.mp4 -filter_complex "[1:v]scale=320:240 [ovrl],[0:v][ovrl] overlay=25:25:enable='between(t,0,20)'" -y output.mp4
            //ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -i \"" + overlayVideo + "\" -filter_complex \"[1:v]scale=320:240 [ovrl],[0:v][ovrl] overlay=25:25:enable='between(t,0,20)'\" -y D:\\Thumbs\\output9.mp4"));
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i \"" + source + "\" -i \"" + overlayVideo + "\" -filter_complex \"[1:v]scale=" + width + ":" + height + " [ovrl],[0:v][ovrl] overlay=" + left + ":" + top + ":enable='between(t," + from.TotalSeconds.ToString() + "," + to.TotalSeconds.ToString() + ")'\" -y \"" + output + "\""));
            //
        }

        public static void ConcatenateVideosForPreview(List<string> clipUrls, string output)
        {
            //-i concat:"part1.ts|part2.ts" -codec copy output.mp4
            if (File.Exists(output))
                File.Delete(output);

            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i concat:\"" + string.Join("|", clipUrls) + "\" -movflags +faststart -qscale 0 -ar 22050 -codec copy \"" + output + "\""));

        }

        public static void ConcatenateVideosForPreviewWithEncoding(List<string> clipUrls, string output)
        {
            //-i concat:"part1.ts|part2.ts" -codec copy output.mp4
            if (File.Exists(output))
                File.Delete(output);

            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-i concat:\"" + string.Join("|", clipUrls) + "\"  -movflags +faststart -qscale 0 -ar 22050 -vcodec libx264 \"" + output + "\""));

        }

        public static void ConcatenateVideos(List<string> clipUrls, string output, bool isAudio = false)
        {
            if (File.Exists(output))
                File.Delete(output);
            ExecuteProcess(InitialPath + @"/ffmpeg.exe", string.Format("-y -i " + string.Join(" -i ", clipUrls) + " -filter_complex concat=n=" + clipUrls.Count + ":v=" + (isAudio == true ? 0 : 1).ToString() + ":a=1 -f MOV -y " + output));
        }

        private static string ExecuteProcessWithOutputReadAsync(string exePath, string parameter)
        {
            try
            {
                StringBuilder output = new StringBuilder();

                using (Process process = new Process())
                {

                    process.StartInfo = new System.Diagnostics.ProcessStartInfo(exePath, parameter);
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;



                    AutoResetEvent outputWaitHandle = new AutoResetEvent(false);
                    process.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            outputWaitHandle.Set();
                        }
                        else
                        {
                            output.AppendLine(e.Data);
                        }
                    };

                    process.Start();

                    process.BeginOutputReadLine();

                    if (process.WaitForExit(1000) &&
                        outputWaitHandle.WaitOne(1000))
                    {
                        // Process completed. Check process.ExitCode here.
                    }
                    else
                    {
                        // Timed out.
                    }
                }

                return output.ToString();
            }
            catch (System.IO.IOException ex)
            {
            }
            return null;
        }

        private static string ExecuteProcessWithOutput(string exePath, string parameter)
        {
            DateTime dt = DateTime.Now;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            try
            {
                process.StartInfo = new System.Diagnostics.ProcessStartInfo
                    (exePath, parameter);
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                StringBuilder str = new StringBuilder();
                while (!process.HasExited)
                {
                    str.Append(process.StandardOutput.ReadToEnd());
                }
                str.Append(process.StandardOutput.ReadToEnd());
                return str.ToString();
            }
            catch (System.IO.IOException ex)
            {
                process.Kill();
            }
            Console.WriteLine(DateTime.Now.Subtract(dt).TotalMilliseconds);
            return null;
        }

        private static void ExecuteProcess(string exePath, string parameter)
        {
            DateTime dt = DateTime.Now;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            try
            {
                process.StartInfo = new System.Diagnostics.ProcessStartInfo
                    (exePath, parameter);
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                process.WaitForExit();
            }
            catch (System.IO.IOException ex)
            {
                process.Kill();
            }
            Console.WriteLine(DateTime.Now.Subtract(dt).TotalMilliseconds);
        }
    }
}
