﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FFMPEGLib.Helper
{
    public class FfProbeInformation
    {
        public List<Stream> streams { get; set; }
        public Format format { get; set; }
    }
}
