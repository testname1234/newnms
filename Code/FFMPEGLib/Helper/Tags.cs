﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FFMPEGLib.Helper
{
    public class Tags
    {
        public string language { get; set; }
        public string SfOriginalFPS { get; set; }
        public string WMFSDKVersion { get; set; }
        public string WMFSDKNeeded { get; set; }
        public string comment { get; set; }
        public string title { get; set; }
        public string copyright { get; set; }
        public string IsVBR { get; set; }
        public string DeviceConformanceTemplate { get; set; }
    }
}
