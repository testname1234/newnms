﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class AppearanceTypeBase:EntityBase, IEquatable<AppearanceTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("AppearanceTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 AppearanceTypeId{ get; set; }

		[FieldNameAttribute("Type",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<AppearanceTypeBase> Members

        public virtual bool Equals(AppearanceTypeBase other)
        {
			if(this.AppearanceTypeId==other.AppearanceTypeId  && this.Type==other.Type  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(AppearanceType other)
        {
			if(other!=null)
			{
				this.AppearanceTypeId=other.AppearanceTypeId;
				this.Type=other.Type;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
