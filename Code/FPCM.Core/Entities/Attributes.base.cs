﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class AttributesBase:EntityBase, IEquatable<AttributesBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("AttributeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 AttributeId{ get; set; }

		[FieldNameAttribute("Name",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("GenreId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GenreId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<AttributesBase> Members

        public virtual bool Equals(AttributesBase other)
        {
			if(this.AttributeId==other.AttributeId  && this.Name==other.Name  && this.GenreId==other.GenreId  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Attributes other)
        {
			if(other!=null)
			{
				this.AttributeId=other.AttributeId;
				this.Name=other.Name;
				this.GenreId=other.GenreId;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
