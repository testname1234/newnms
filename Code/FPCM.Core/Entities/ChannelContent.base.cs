﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelContentBase:EntityBase, IEquatable<ChannelContentBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelContentId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelContentId{ get; set; }

		[FieldNameAttribute("WeekdayShowtimeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WeekdayShowtimeId{ get; set; }

		[FieldNameAttribute("ContentTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ContentTypeId{ get; set; }

		[FieldNameAttribute("TypeId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TypeId{ get; set; }

		[FieldNameAttribute("Time",true,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual TimeSpan? Time{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelContentBase> Members

        public virtual bool Equals(ChannelContentBase other)
        {
			if(this.ChannelContentId==other.ChannelContentId  && this.WeekdayShowtimeId==other.WeekdayShowtimeId  && this.ContentTypeId==other.ContentTypeId  && this.TypeId==other.TypeId  && this.Time==other.Time  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ChannelContent other)
        {
			if(other!=null)
			{
				this.ChannelContentId=other.ChannelContentId;
				this.WeekdayShowtimeId=other.WeekdayShowtimeId;
				this.ContentTypeId=other.ContentTypeId;
				this.TypeId=other.TypeId;
				this.Time=other.Time;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
