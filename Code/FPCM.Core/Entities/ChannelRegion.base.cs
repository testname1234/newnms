﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelRegionBase:EntityBase, IEquatable<ChannelRegionBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelRegionId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelRegionId{ get; set; }

		[FieldNameAttribute("ChannelId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelId{ get; set; }

		[FieldNameAttribute("RegionId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RegionId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelRegionBase> Members

        public virtual bool Equals(ChannelRegionBase other)
        {
			if(this.ChannelRegionId==other.ChannelRegionId  && this.ChannelId==other.ChannelId  && this.RegionId==other.RegionId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ChannelRegion other)
        {
			if(other!=null)
			{
				this.ChannelRegionId=other.ChannelRegionId;
				this.ChannelId=other.ChannelId;
				this.RegionId=other.RegionId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
