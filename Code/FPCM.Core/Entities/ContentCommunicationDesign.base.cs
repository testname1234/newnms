﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ContentCommunicationDesignBase:EntityBase, IEquatable<ContentCommunicationDesignBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CommunicationDesignId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CommunicationDesignId{ get; set; }

		[FieldNameAttribute("ChannelContentId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelContentId{ get; set; }

		[FieldNameAttribute("FileName",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FileName{ get; set; }

		[FieldNameAttribute("Status",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Status{ get; set; }

		[FieldNameAttribute("AssetId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AssetId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ContentCommunicationDesignBase> Members

        public virtual bool Equals(ContentCommunicationDesignBase other)
        {
			if(this.CommunicationDesignId==other.CommunicationDesignId  && this.ChannelContentId==other.ChannelContentId  && this.FileName==other.FileName  && this.Status==other.Status  && this.AssetId==other.AssetId  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ContentCommunicationDesign other)
        {
			if(other!=null)
			{
				this.CommunicationDesignId=other.CommunicationDesignId;
				this.ChannelContentId=other.ChannelContentId;
				this.FileName=other.FileName;
				this.Status=other.Status;
				this.AssetId=other.AssetId;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
