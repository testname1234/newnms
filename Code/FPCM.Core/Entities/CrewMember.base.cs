﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class CrewMemberBase:EntityBase, IEquatable<CrewMemberBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CrewMemberId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CrewMemberId{ get; set; }

		[FieldNameAttribute("CrewTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CrewTypeId{ get; set; }

		[FieldNameAttribute("Name",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Rating",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Rating{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CrewMemberBase> Members

        public virtual bool Equals(CrewMemberBase other)
        {
			if(this.CrewMemberId==other.CrewMemberId  && this.CrewTypeId==other.CrewTypeId  && this.Name==other.Name  && this.Rating==other.Rating  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CrewMember other)
        {
			if(other!=null)
			{
				this.CrewMemberId=other.CrewMemberId;
				this.CrewTypeId=other.CrewTypeId;
				this.Name=other.Name;
				this.Rating=other.Rating;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
