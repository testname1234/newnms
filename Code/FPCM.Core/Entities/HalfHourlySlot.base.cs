﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class HalfHourlySlotBase:EntityBase, IEquatable<HalfHourlySlotBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("HalfHourlySlotId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 HalfHourlySlotId{ get; set; }

		[FieldNameAttribute("HourlySlotId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 HourlySlotId{ get; set; }

		[FieldNameAttribute("Slot",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slot{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<HalfHourlySlotBase> Members

        public virtual bool Equals(HalfHourlySlotBase other)
        {
			if(this.HalfHourlySlotId==other.HalfHourlySlotId  && this.HourlySlotId==other.HourlySlotId  && this.Slot==other.Slot  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(HalfHourlySlot other)
        {
			if(other!=null)
			{
				this.HalfHourlySlotId=other.HalfHourlySlotId;
				this.HourlySlotId=other.HourlySlotId;
				this.Slot=other.Slot;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
