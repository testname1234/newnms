﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class OgCountryBase:EntityBase, IEquatable<OgCountryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CountryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CountryId{ get; set; }

		[FieldNameAttribute("Name",false,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("ShortCode",false,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ShortCode{ get; set; }

		[FieldNameAttribute("PhoneCode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? PhoneCode{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<OgCountryBase> Members

        public virtual bool Equals(OgCountryBase other)
        {
			if(this.CountryId==other.CountryId  && this.Name==other.Name  && this.ShortCode==other.ShortCode  && this.PhoneCode==other.PhoneCode  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(OgCountry other)
        {
			if(other!=null)
			{
				this.CountryId=other.CountryId;
				this.Name=other.Name;
				this.ShortCode=other.ShortCode;
				this.PhoneCode=other.PhoneCode;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
