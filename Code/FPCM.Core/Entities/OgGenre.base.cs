﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class OgGenreBase:EntityBase, IEquatable<OgGenreBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("GenreId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GenreId{ get; set; }

		[FieldNameAttribute("Genre",false,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Genre{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime IsActive{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string IsActiveStr
		{
			 get { return IsActive.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { IsActive = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<OgGenreBase> Members

        public virtual bool Equals(OgGenreBase other)
        {
			if(this.GenreId==other.GenreId  && this.Genre==other.Genre  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(OgGenre other)
        {
			if(other!=null)
			{
				this.GenreId=other.GenreId;
				this.Genre=other.Genre;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
