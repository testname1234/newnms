﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class OgShowTimeBase:EntityBase, IEquatable<OgShowTimeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ShowTimeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ShowTimeId{ get; set; }

		[FieldNameAttribute("ShowTime",false,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ShowTime{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<OgShowTimeBase> Members

        public virtual bool Equals(OgShowTimeBase other)
        {
			if(this.ShowTimeId==other.ShowTimeId  && this.ShowTime==other.ShowTime  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(OgShowTime other)
        {
			if(other!=null)
			{
				this.ShowTimeId=other.ShowTimeId;
				this.ShowTime=other.ShowTime;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
