﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class OgSpecialDayBase:EntityBase, IEquatable<OgSpecialDayBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SpecialDayId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SpecialDayId{ get; set; }

		[FieldNameAttribute("Title",false,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("DayType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? DayType{ get; set; }

		[FieldNameAttribute("OccuranceDay",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OccuranceDay{ get; set; }

		[FieldNameAttribute("OccuranceMonth",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OccuranceMonth{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<OgSpecialDayBase> Members

        public virtual bool Equals(OgSpecialDayBase other)
        {
			if(this.SpecialDayId==other.SpecialDayId  && this.Title==other.Title  && this.DayType==other.DayType  && this.OccuranceDay==other.OccuranceDay  && this.OccuranceMonth==other.OccuranceMonth  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(OgSpecialDay other)
        {
			if(other!=null)
			{
				this.SpecialDayId=other.SpecialDayId;
				this.Title=other.Title;
				this.DayType=other.DayType;
				this.OccuranceDay=other.OccuranceDay;
				this.OccuranceMonth=other.OccuranceMonth;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
