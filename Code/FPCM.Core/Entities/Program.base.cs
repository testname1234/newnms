﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramBase:EntityBase, IEquatable<ProgramBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("Name",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("GenreId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GenreId{ get; set; }

		[FieldNameAttribute("SubGenreId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SubGenreId{ get; set; }

		[FieldNameAttribute("SegmentsCount",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SegmentsCount{ get; set; }

		[FieldNameAttribute("ProgramTimeFrom",false,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual TimeSpan ProgramTimeFrom{ get; set; }

		[FieldNameAttribute("ProgramTimeTo",false,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual TimeSpan ProgramTimeTo{ get; set; }

		[FieldNameAttribute("TransmissionTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TransmissionTypeId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramBase> Members

        public virtual bool Equals(ProgramBase other)
        {
			if(this.ProgramId==other.ProgramId  && this.Name==other.Name  && this.GenreId==other.GenreId  && this.SubGenreId==other.SubGenreId  && this.SegmentsCount==other.SegmentsCount && this.ProgramTimeFrom==other.ProgramTimeFrom  && this.ProgramTimeTo==other.ProgramTimeTo  && this.TransmissionTypeId==other.TransmissionTypeId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Program other)
        {
			if(other!=null)
			{
				this.ProgramId=other.ProgramId;
				this.Name=other.Name;
				this.GenreId=other.GenreId;
				this.SubGenreId=other.SubGenreId;
				this.SegmentsCount=other.SegmentsCount;
				this.ProgramTimeFrom=other.ProgramTimeFrom;
				this.ProgramTimeTo=other.ProgramTimeTo;
				this.TransmissionTypeId=other.TransmissionTypeId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
