﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace FPCM.Core.Entities
{
    [DataContract]
	public partial class Program : ProgramBase 
	{
        [FieldNameAttribute("BreakCount", false, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 BreakCount { get; set; }	
	}
}
