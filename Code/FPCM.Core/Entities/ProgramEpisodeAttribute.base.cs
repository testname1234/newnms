﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramEpisodeAttributeBase:EntityBase, IEquatable<ProgramEpisodeAttributeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramEpisodeAttributeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramEpisodeAttributeId{ get; set; }

		[FieldNameAttribute("ProgramEpisodeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramEpisodeId{ get; set; }

		[FieldNameAttribute("AttributeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 AttributeId{ get; set; }

		[FieldNameAttribute("Value",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Value{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramEpisodeAttributeBase> Members

        public virtual bool Equals(ProgramEpisodeAttributeBase other)
        {
			if(this.ProgramEpisodeAttributeId==other.ProgramEpisodeAttributeId  && this.ProgramEpisodeId==other.ProgramEpisodeId  && this.AttributeId==other.AttributeId  && this.Value==other.Value  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramEpisodeAttribute other)
        {
			if(other!=null)
			{
				this.ProgramEpisodeAttributeId=other.ProgramEpisodeAttributeId;
				this.ProgramEpisodeId=other.ProgramEpisodeId;
				this.AttributeId=other.AttributeId;
				this.Value=other.Value;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
