﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FPCM.Core.Entities
{
    [DataContract]
    public class ProgramEpisodeProducer
    {
        [FieldNameAttribute("ProgramEpisodeId", false, true, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ProgramEpisodeId { get; set; }

        [FieldNameAttribute("ProducerId", false, true, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ProducerId { get; set; }
    }
}
