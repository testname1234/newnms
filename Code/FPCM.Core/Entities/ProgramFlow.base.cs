﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramFlowBase:EntityBase, IEquatable<ProgramFlowBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramFlowId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramFlowId{ get; set; }

		[FieldNameAttribute("ProgramEpisodeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramEpisodeId{ get; set; }

		[FieldNameAttribute("FlowTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FlowTypeId{ get; set; }

		[FieldNameAttribute("SequenceOrder",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceOrder{ get; set; }

		[FieldNameAttribute("DurationInSec",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DurationInSec{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramFlowBase> Members

        public virtual bool Equals(ProgramFlowBase other)
        {
			if(this.ProgramFlowId==other.ProgramFlowId  && this.ProgramEpisodeId==other.ProgramEpisodeId  && this.FlowTypeId==other.FlowTypeId  && this.SequenceOrder==other.SequenceOrder  && this.DurationInSec==other.DurationInSec  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramFlow other)
        {
			if(other!=null)
			{
				this.ProgramFlowId=other.ProgramFlowId;
				this.ProgramEpisodeId=other.ProgramEpisodeId;
				this.FlowTypeId=other.FlowTypeId;
				this.SequenceOrder=other.SequenceOrder;
				this.DurationInSec=other.DurationInSec;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
