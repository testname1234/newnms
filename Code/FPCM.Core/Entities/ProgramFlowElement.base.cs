﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramFlowElementBase:EntityBase, IEquatable<ProgramFlowElementBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramFlowElementId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramFlowElementId{ get; set; }

		[FieldNameAttribute("ProgramFlowId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramFlowId{ get; set; }

		[FieldNameAttribute("ProgramElementId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramElementId{ get; set; }

		[FieldNameAttribute("DurationInSec",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DurationInSec{ get; set; }

		[FieldNameAttribute("SequenceOrder",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceOrder{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramFlowElementBase> Members

        public virtual bool Equals(ProgramFlowElementBase other)
        {
			if(this.ProgramFlowElementId==other.ProgramFlowElementId  && this.ProgramFlowId==other.ProgramFlowId  && this.ProgramElementId==other.ProgramElementId  && this.DurationInSec==other.DurationInSec  && this.SequenceOrder==other.SequenceOrder  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramFlowElement other)
        {
			if(other!=null)
			{
				this.ProgramFlowElementId=other.ProgramFlowElementId;
				this.ProgramFlowId=other.ProgramFlowId;
				this.ProgramElementId=other.ProgramElementId;
				this.DurationInSec=other.DurationInSec;
				this.SequenceOrder=other.SequenceOrder;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
