﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class ScreenElementBase:EntityBase, IEquatable<ScreenElementBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ScreenElementId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenElementId{ get; set; }

		[FieldNameAttribute("Name",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,8)]
		[IgnoreDataMember]
		public virtual System.Boolean IsActive{ get; set;}
		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ScreenElementBase> Members

        public virtual bool Equals(ScreenElementBase other)
        {
			if(this.ScreenElementId==other.ScreenElementId  && this.Name==other.Name  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ScreenElement other)
        {
			if(other!=null)
			{
				this.ScreenElementId=other.ScreenElementId;
				this.Name=other.Name;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
