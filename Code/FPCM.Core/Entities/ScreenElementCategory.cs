﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FPCM.Core.Entities
{
    [DataContract]
    public class ScreenElementCategory
    {
        [PrimaryKey]
        [FieldNameAttribute("ScreenElementCategoryId", false, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ScreenElementCategoryId { get; set; }

        [FieldNameAttribute("Name", false, false, 500)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Name { get; set; }

        [FieldNameAttribute("ShortCode", false, false, 500)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String ShortCode { get; set; }

        [FieldNameAttribute("CreationDate", false, false, 8)]
        [IgnoreDataMember]
        public virtual System.DateTime CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public virtual string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [FieldNameAttribute("IsActive", false, false, 8)]
        [IgnoreDataMember]
        public virtual System.Boolean IsActive { get; set; }

        public virtual void CopyFrom(ScreenElementCategory other)
        {
            if (other != null)
            {
                this.ScreenElementCategoryId = other.ScreenElementCategoryId;
                this.Name = other.Name;
                this.ShortCode = other.ShortCode;
                this.CreationDate = other.CreationDate;
                this.IsActive = other.IsActive;
            }
        }
    }
}
