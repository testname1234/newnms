﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FPCM.Core.Entities
{
    [DataContract]
    public class ScreenTemplatePosition
    {
        [PrimaryKey]
        [FieldNameAttribute("ScreenTemplatePositionId", false, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ScreenTemplatePositionId { get; set; }

        [FieldNameAttribute("ScreenElementId", false, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ScreenElementId { get; set; }

        [FieldNameAttribute("ScreenPositionId", false, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ScreenPositionId { get; set; }

        [FieldNameAttribute("ScreenTemplateId", false, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ScreenTemplateId { get; set; }

        [FieldNameAttribute("CreationDate", false, false, 8)]
        [IgnoreDataMember]
        public virtual System.DateTime CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [FieldNameAttribute("IsActive", false, false, 1)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean IsActive { get; set; }
    }
}
