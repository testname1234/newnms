﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class SubGenreBase:EntityBase, IEquatable<SubGenreBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SubGenreId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SubGenreId{ get; set; }

		[FieldNameAttribute("GenreId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GenreId{ get; set; }

		[FieldNameAttribute("SubGenre",true,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SubGenre{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SubGenreBase> Members

        public virtual bool Equals(SubGenreBase other)
        {
			if(this.SubGenreId==other.SubGenreId  && this.GenreId==other.GenreId  && this.SubGenre==other.SubGenre  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SubGenre other)
        {
			if(other!=null)
			{
				this.SubGenreId=other.SubGenreId;
				this.GenreId=other.GenreId;
				this.SubGenre=other.SubGenre;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
