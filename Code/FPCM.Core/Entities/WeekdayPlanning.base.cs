﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class WeekdayPlanningBase:EntityBase, IEquatable<WeekdayPlanningBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("WeekdayPlanningId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WeekdayPlanningId{ get; set; }

		[FieldNameAttribute("ChannelRegionId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelRegionId{ get; set; }

		[FieldNameAttribute("Weekday",false,false,3)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Weekday{ get; set; }

		[FieldNameAttribute("SpecialDayId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SpecialDayId{ get; set; }

		[FieldNameAttribute("Date",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime Date{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string DateStr
		{
			 get { return Date.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Date = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<WeekdayPlanningBase> Members

        public virtual bool Equals(WeekdayPlanningBase other)
        {
			if(this.WeekdayPlanningId==other.WeekdayPlanningId  && this.ChannelRegionId==other.ChannelRegionId  && this.Weekday==other.Weekday  && this.SpecialDayId==other.SpecialDayId  && this.Date==other.Date  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(WeekdayPlanning other)
        {
			if(other!=null)
			{
				this.WeekdayPlanningId=other.WeekdayPlanningId;
				this.ChannelRegionId=other.ChannelRegionId;
				this.Weekday=other.Weekday;
				this.SpecialDayId=other.SpecialDayId;
				this.Date=other.Date;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
