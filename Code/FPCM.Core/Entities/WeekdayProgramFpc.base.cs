﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class WeekdayProgramFpcBase:EntityBase, IEquatable<WeekdayProgramFpcBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("WeekdayProgramFpcId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WeekdayProgramFpcId{ get; set; }

		[FieldNameAttribute("WeekdayShowtimeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WeekdayShowtimeId{ get; set; }

		[FieldNameAttribute("ProgramEpisodeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramEpisodeId{ get; set; }

		[FieldNameAttribute("StartTime",false,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual TimeSpan StartTime{ get; set; }

		[FieldNameAttribute("DurationInSec",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DurationInSec{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<WeekdayProgramFpcBase> Members

        public virtual bool Equals(WeekdayProgramFpcBase other)
        {
			if(this.WeekdayProgramFpcId==other.WeekdayProgramFpcId  && this.WeekdayShowtimeId==other.WeekdayShowtimeId  && this.ProgramEpisodeId==other.ProgramEpisodeId  && this.StartTime==other.StartTime  && this.DurationInSec==other.DurationInSec  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(WeekdayProgramFpc other)
        {
			if(other!=null)
			{
				this.WeekdayProgramFpcId=other.WeekdayProgramFpcId;
				this.WeekdayShowtimeId=other.WeekdayShowtimeId;
				this.ProgramEpisodeId=other.ProgramEpisodeId;
				this.StartTime=other.StartTime;
				this.DurationInSec=other.DurationInSec;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
