﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace FPCM.Core.Entities
{
    [DataContract]
	public abstract partial class WeekdayShowtimeBase:EntityBase, IEquatable<WeekdayShowtimeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("WeekdayShowtimeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WeekdayShowtimeId{ get; set; }

		[FieldNameAttribute("WeekdayPlanningId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WeekdayPlanningId{ get; set; }

		[FieldNameAttribute("ShowTimeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ShowTimeId{ get; set; }

		[FieldNameAttribute("StartTime",false,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual TimeSpan StartTime{ get; set; }

		[FieldNameAttribute("DurationInSec",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DurationInSec{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<WeekdayShowtimeBase> Members

        public virtual bool Equals(WeekdayShowtimeBase other)
        {
			if(this.WeekdayShowtimeId==other.WeekdayShowtimeId  && this.WeekdayPlanningId==other.WeekdayPlanningId  && this.ShowTimeId==other.ShowTimeId  && this.StartTime==other.StartTime  && this.DurationInSec==other.DurationInSec  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(WeekdayShowtime other)
        {
			if(other!=null)
			{
				this.WeekdayShowtimeId=other.WeekdayShowtimeId;
				this.WeekdayPlanningId=other.WeekdayPlanningId;
				this.ShowTimeId=other.ShowTimeId;
				this.StartTime=other.StartTime;
				this.DurationInSec=other.DurationInSec;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
