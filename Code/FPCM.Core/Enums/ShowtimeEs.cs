﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCM.Core.Enums
{
    public enum ShowtimeEs
    {
        PrimeTime=1,
        LateTime,
        RepeatTime,
        DayTime
    }
}
