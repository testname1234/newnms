﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCM.Core.Enums
{
    public enum SpecialDayEs
    {
        Eid = 1,
        IndependenceDay,
        ResolutionDay
    }
}
