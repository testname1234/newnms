﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCM.Core.Enums
{
    public enum TransmissionTypeEs
    {
        Live =1,
        AsLive,
        Recorded,
    }
}
