﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using FPCM.Core.Entities;
using FPCM.Core.Enums;

namespace FPCM.Core
{

    public class MockEntityDataGenerator
    {
        public virtual AppearanceType GetMockAppearanceType(bool isSaved)
        {
            AppearanceType entity = new AppearanceType();
            entity.Type = TestUtility.Instance.RandomString(49, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            return entity;
        }

        public virtual Attributes GetMockAttributes(bool isSaved)
        {
            Attributes entity = new Attributes();
            entity.Name = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.GenreId = GetMockOgGenre(true).GenreId;
            return entity;
        }

        public virtual Channel GetMockChannel()
        {
            Channel entity = new Channel();
            entity.CreationDate = DateTime.Now;
            entity.LastUpdateDate = DateTime.Now; ;
            entity.IsActive = true;
            entity.ChannelId = 1;
            entity.GenreId = (int)GenreEs.News;
            entity.Name = "Bol News";
            entity.Philosophy = "Bol News will be focusing on showing soft face of Pakistan";
            return entity;
        }

        public virtual ChannelContent GetMockChannelContent(bool isSaved)
        {
            ChannelContent entity = new ChannelContent();
            entity.Time = TestUtility.Instance.RandomDateTime().TimeOfDay;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ContentTypeId = GetMockContentType(true).ContentTypeId;
            entity.TypeId = GetMockOgType(true).TypeId;
            entity.WeekdayShowtimeId = GetMockWeekdayShowtime(true).WeekdayShowtimeId;
            return entity;
        }

        public virtual ChannelCountry GetMockChannelCountry(bool isSaved)
        {
            ChannelCountry entity = new ChannelCountry();
            entity.ChannelId = TestUtility.Instance.RandomNumber();
            entity.CountryId = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            return entity;
        }

        public virtual ChannelRegion GetMockChannelRegion(bool isSaved)
        {
            ChannelRegion entity = new ChannelRegion();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.RegionId = GetMockRegion(true).RegionId;
            return entity;
        }

        public virtual ColorTheme GetMockColorTheme(bool isSaved)
        {
            ColorTheme entity = new ColorTheme();
            entity.Name = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            return entity;
        }

        public virtual ContentCommunicationDesign GetMockContentCommunicationDesign(bool isSaved)
        {
            ContentCommunicationDesign entity = new ContentCommunicationDesign();
            entity.FileName = TestUtility.Instance.RandomString(249, false); ;
            entity.Status = TestUtility.Instance.RandomString(49, false); ;
            entity.AssetId = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ChannelContentId = GetMockChannelContent(true).ChannelContentId;
            return entity;
        }

        public virtual ContentType GetMockContentType(bool isSaved)
        {
            ContentType entity = new ContentType();
            entity.Name = TestUtility.Instance.RandomString(124, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual CrewMember GetMockCrewMember(bool isSaved)
        {
            CrewMember entity = new CrewMember();
            entity.Name = TestUtility.Instance.RandomString(249, false); ;
            entity.Rating = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.CrewTypeId = GetMockCrewType(true).CrewTypeId;

            return entity;
        }

        public virtual CrewType GetMockCrewType(bool isSaved)
        {
            CrewType entity = new CrewType();
            entity.Type = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual FlowType GetMockFlowType(bool isSaved)
        {
            FlowType entity = new FlowType();
            entity.Type = TestUtility.Instance.RandomString(49, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual Format GetMockFormat(bool isSaved)
        {
            Format entity = new Format();
            entity.Name = TestUtility.Instance.RandomString(124, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual GuestType GetMockGuestType(bool isSaved)
        {
            GuestType entity = new GuestType();
            entity.Type = TestUtility.Instance.RandomString(124, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual HalfHourlySlot GetMockHalfHourlySlot(bool isSaved)
        {
            HalfHourlySlot entity = new HalfHourlySlot();
            entity.Slot = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.HourlySlotId = GetMockHourlySlot(true).HourlySlotId;

            return entity;
        }

        public virtual HourlySlot GetMockHourlySlot(bool isSaved)
        {
            HourlySlot entity = new HourlySlot();
            entity.Slot = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual Language GetMockLanguage(bool isSaved)
        {
            Language entity = new Language();
            entity.Name = TestUtility.Instance.RandomString(49, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual Location GetMockLocation(bool isSaved)
        {
            Location entity = new Location();
            entity.Name = TestUtility.Instance.RandomString(249, false); ;
            entity.LocationType = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual MediumType GetMockMediumType(bool isSaved)
        {
            MediumType entity = new MediumType();
            entity.Type = TestUtility.Instance.RandomString(124, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual OgCountry GetMockOgCountry(bool isSaved)
        {
            OgCountry entity = new OgCountry();
            entity.Name = TestUtility.Instance.RandomString(124, false); ;
            entity.ShortCode = TestUtility.Instance.RandomString(5, false); ;
            entity.PhoneCode = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual OgGenre GetMockOgGenre(bool isSaved)
        {
            OgGenre entity = new OgGenre();
            entity.Genre = TestUtility.Instance.RandomString(124, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomDateTime();

            return entity;
        }

        public virtual OgShowTime GetMockOgShowTime(bool isSaved)
        {
            OgShowTime entity = new OgShowTime();
            entity.ShowTime = TestUtility.Instance.RandomString(124, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual OgSpecialDay GetMockOgSpecialDay(bool isSaved)
        {
            OgSpecialDay entity = new OgSpecialDay();
            entity.Title = TestUtility.Instance.RandomString(124, false); ;
            entity.DayType = TestUtility.Instance.RandomNumber();
            entity.OccuranceDay = TestUtility.Instance.RandomNumber();
            entity.OccuranceMonth = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual OgType GetMockOgType(bool isSaved)
        {
            OgType entity = new OgType();
            entity.Name = TestUtility.Instance.RandomString(124, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual Program GetMockProgram(bool isSaved)
        {
            Program entity = new Program();
            entity.Name = TestUtility.Instance.RandomString(249, false); ;
            entity.SegmentsCount = TestUtility.Instance.RandomNumber();
            entity.ProgramTimeFrom = TestUtility.Instance.RandomDateTime().TimeOfDay;
            entity.ProgramTimeTo = TestUtility.Instance.RandomDateTime().TimeOfDay;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.GenreId = GetMockOgGenre(true).GenreId;
            entity.SubGenreId = GetMockSubGenre(true).SubGenreId;
            entity.TransmissionTypeId = GetMockTransmissionType(true).TransmissionTypeId;

            return entity;
        }

        public virtual ProgramCrew GetMockProgramCrew(bool isSaved)
        {
            ProgramCrew entity = new ProgramCrew();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.CrewMemberId = GetMockCrewMember(true).CrewMemberId;
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;

            return entity;
        }

        public virtual ProgramElement GetMockProgramElement(bool isSaved)
        {
            ProgramElement entity = new ProgramElement();
            entity.Name = TestUtility.Instance.RandomString(124, false); ;
            entity.Description = TestUtility.Instance.RandomString(2, false); ;
            entity.FileName = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual ProgramElementTemplate GetMockProgramElementTemplate(bool isSaved)
        {
            ProgramElementTemplate entity = new ProgramElementTemplate();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;
            entity.ScreenTemplateId = GetMockScreenTemplate(true).ScreenTemplateId;

            return entity;
        }

        public virtual ProgramEpisode GetMockProgramEpisode(bool isSaved)
        {
            ProgramEpisode entity = new ProgramEpisode();
            entity.EpisodeTitle = TestUtility.Instance.RandomString(2, false); ;
            entity.StartTime = TestUtility.Instance.RandomDateTime().TimeOfDay;
            entity.DurationInSec = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ProgramId = GetMockProgram(true).ProgramId;

            return entity;
        }

        public virtual ProgramEpisodeAttribute GetMockProgramEpisodeAttribute(bool isSaved)
        {
            ProgramEpisodeAttribute entity = new ProgramEpisodeAttribute();
            entity.Value = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.AttributeId = GetMockAttributes(true).AttributeId;
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;

            return entity;
        }

        public virtual ProgramFlow GetMockProgramFlow(bool isSaved)
        {
            ProgramFlow entity = new ProgramFlow();
            entity.SequenceOrder = TestUtility.Instance.RandomNumber();
            entity.DurationInSec = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.FlowTypeId = GetMockFlowType(true).FlowTypeId;
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;

            return entity;
        }

        public virtual ProgramFlowElement GetMockProgramFlowElement(bool isSaved)
        {
            ProgramFlowElement entity = new ProgramFlowElement();
            entity.DurationInSec = TestUtility.Instance.RandomNumber();
            entity.SequenceOrder = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ProgramElementId = GetMockProgramElement(true).ProgramElementId;
            entity.ProgramFlowId = GetMockProgramFlow(true).ProgramFlowId;

            return entity;
        }

        public virtual ProgramGuestAppearance GetMockProgramGuestAppearance(bool isSaved)
        {
            ProgramGuestAppearance entity = new ProgramGuestAppearance();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.AppearanceTypeId = GetMockAppearanceType(true).AppearanceTypeId;
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;

            return entity;
        }

        public virtual ProgramGuestMedium GetMockProgramGuestMedium(bool isSaved)
        {
            ProgramGuestMedium entity = new ProgramGuestMedium();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.MediumTypeId = GetMockMediumType(true).MediumTypeId;
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;

            return entity;
        }

        public virtual ProgramGuestType GetMockProgramGuestType(bool isSaved)
        {
            ProgramGuestType entity = new ProgramGuestType();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.GuestTypeId = GetMockGuestType(true).GuestTypeId;
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;

            return entity;
        }

        public virtual ProgramLanguage GetMockProgramLanguage(bool isSaved)
        {
            ProgramLanguage entity = new ProgramLanguage();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.LanguageId = GetMockLanguage(true).LanguageId;
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;

            return entity;
        }

        public virtual ProgramSlot GetMockProgramSlot(bool isSaved)
        {
            ProgramSlot entity = new ProgramSlot();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.HalfHourlySlotId = GetMockHalfHourlySlot(true).HalfHourlySlotId;
            entity.HourlySlotId = GetMockHourlySlot(true).HourlySlotId;
            entity.WeekdayProgramFpcId = GetMockWeekdayProgramFpc(true).WeekdayProgramFpcId;

            return entity;
        }

        public virtual Region GetMockRegion(bool isSaved)
        {
            Region entity = new Region();
            entity.Name = TestUtility.Instance.RandomString(10, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual RegionLocation GetMockRegionLocation(bool isSaved)
        {
            RegionLocation entity = new RegionLocation();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.LocationId = GetMockLocation(true).LocationId;
            entity.RegionId = GetMockRegion(true).RegionId;

            return entity;
        }

        public virtual ScreenElement GetMockScreenElement(bool isSaved)
        {
            ScreenElement entity = new ScreenElement();
            entity.Name = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual ScreenTemplate GetMockScreenTemplate(bool isSaved)
        {
            ScreenTemplate entity = new ScreenTemplate();
            entity.Name = TestUtility.Instance.RandomString(124, false); ;
            entity.ImageThumb = TestUtility.Instance.RandomString(249, false); ;
            entity.AssetId = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual SubGenre GetMockSubGenre(bool isSaved)
        {
            SubGenre entity = new SubGenre();
            entity.SubGenre = TestUtility.Instance.RandomString(10, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.GenreId = GetMockOgGenre(true).GenreId;

            return entity;
        }

        public virtual TransmissionType GetMockTransmissionType(bool isSaved)
        {
            TransmissionType entity = new TransmissionType();
            entity.Type = TestUtility.Instance.RandomString(249, false); ;
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();

            return entity;
        }

        public virtual WeekdayPlanning GetMockWeekdayPlanning(bool isSaved)
        {
            WeekdayPlanning entity = new WeekdayPlanning();
            entity.Weekday = TestUtility.Instance.RandomString(3, false); ;
            entity.Date = TestUtility.Instance.RandomDateTime();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ChannelRegionId = GetMockChannelRegion(true).ChannelRegionId;
            entity.SpecialDayId = GetMockOgSpecialDay(true).SpecialDayId;

            return entity;
        }

        public virtual WeekdayProgramFpc GetMockWeekdayProgramFpc(bool isSaved)
        {
            WeekdayProgramFpc entity = new WeekdayProgramFpc();
            entity.StartTime = TestUtility.Instance.RandomDateTime().TimeOfDay;
            entity.DurationInSec = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ProgramEpisodeId = GetMockProgramEpisode(true).ProgramEpisodeId;
            entity.WeekdayShowtimeId = GetMockWeekdayShowtime(true).WeekdayShowtimeId;

            return entity;
        }

        public virtual WeekdayShowtime GetMockWeekdayShowtime(bool isSaved)
        {
            WeekdayShowtime entity = new WeekdayShowtime();
            entity.StartTime = TestUtility.Instance.RandomDateTime().TimeOfDay;
            entity.DurationInSec = TestUtility.Instance.RandomNumber();
            entity.CreationDate = TestUtility.Instance.RandomDateTime();
            entity.LastUpdateDate = TestUtility.Instance.RandomDateTime();
            entity.IsActive = TestUtility.Instance.RandomBoolean();
            entity.ShowTimeId = GetMockOgShowTime(true).ShowTimeId;
            entity.WeekdayPlanningId = GetMockWeekdayPlanning(true).WeekdayPlanningId;

            return entity;
        }

    }


}
