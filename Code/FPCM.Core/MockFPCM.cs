﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPCM.Core.Entities;
using FPCM.Core.Enums;

namespace FPCM.Core
{
    public class MockFPCM
    {
        private static Channel _Channel;
        public static Channel Channel
        {
            get
            {
                if (_Channel == null)
                {
                    MockEntityDataGenerator mock = new MockEntityDataGenerator();
                    _Channel = mock.GetMockChannel();
                }
                return _Channel;
            }
        }

        private static ChannelRegion _ChannelRegion;
        public static ChannelRegion ChannelRegion
        {
            get
            {
                if (_ChannelRegion == null)
                {
                    MockEntityDataGenerator mock = new MockEntityDataGenerator();
                    _ChannelRegion = mock.GetMockChannelRegion(false);
                    _ChannelRegion.ChannelId = Channel.ChannelId;
                }
                return _ChannelRegion;
            }
        }

        private static WeekdayPlanning _WeekdayPlanning;
        public static WeekdayPlanning WeekdayPlanning
        {
            get
            {
                if (_WeekdayPlanning == null)
                {
                    _WeekdayPlanning = new Entities.WeekdayPlanning();
                    MockEntityDataGenerator mock = new MockEntityDataGenerator();
                    WeekdayPlanning wdp = mock.GetMockWeekdayPlanning(false);
                    wdp.WeekdayPlanningId = 1;
                    wdp.Date = DateTime.Now;
                    wdp.ChannelRegionId = ChannelRegion.ChannelRegionId;
                    wdp.Weekday = DateTime.Now.DayOfWeek.ToString().Substring(0, 3);
                    wdp.SpecialDayId = (int?)null;
                    _WeekdayPlanning = wdp;
                }
                return _WeekdayPlanning;
            }
        }

        private static List<WeekdayShowtime> _WeekdayShowtimes;
        public static List<WeekdayShowtime> WeekdayShowtimes
        {
            get
            {
                if (_WeekdayShowtimes == null || _WeekdayShowtimes.Count == 0)
                {
                    _WeekdayShowtimes = new List<WeekdayShowtime>();
                    MockEntityDataGenerator mock = new MockEntityDataGenerator();
                    for (int i = 0; i < 2; i++)
                    {
                        WeekdayShowtime wst = mock.GetMockWeekdayShowtime(false);
                        if (i == 0)
                        {
                            wst.WeekdayShowtimeId = 1;
                            wst.DurationInSec = 18000;
                            wst.StartTime = DateTime.Parse("00:00:00").TimeOfDay;
                            wst.WeekdayPlanningId = WeekdayPlanning.WeekdayPlanningId;
                            wst.ShowTimeId = (int)ShowtimeEs.DayTime;
                        }
                        if (i == 1)
                        {
                            wst.WeekdayShowtimeId = 2;
                            wst.DurationInSec = 7200;
                            wst.StartTime = DateTime.Parse("05:00:00").TimeOfDay;
                            wst.WeekdayPlanningId = WeekdayPlanning.WeekdayPlanningId;
                            wst.ShowTimeId = (int)ShowtimeEs.PrimeTime;
                        }
                        _WeekdayShowtimes.Add(wst);
                    }
                }
                return _WeekdayShowtimes;
            }
        }

        #region Program Level FPC

        private static List<WeekdayProgramFpc> _WeekdayProgramFpcList;
        public static List<WeekdayProgramFpc> WeekdayProgramFpcList
        {
            get
            {
                if (_WeekdayProgramFpcList == null || _WeekdayProgramFpcList.Count == 0)
                {
                    _WeekdayProgramFpcList = new List<WeekdayProgramFpc>();
                    MockEntityDataGenerator mock = new MockEntityDataGenerator();
                    for (int i = 0; i < 2; i++)
                    {
                        WeekdayProgramFpc prg = mock.GetMockWeekdayProgramFpc(false);
                        if (i == 0)
                        {
                            prg.WeekdayProgramFpcId = 1;
                            prg.DurationInSec = 3300;
                            prg.WeekdayShowtimeId = 1;
                            prg.StartTime = DateTime.Parse("00:05:00").TimeOfDay;
                            prg.ProgramEpisodeId = ProgramEpisodeList.First().ProgramEpisodeId ;
                            prg.WeekdayShowtimeId = WeekdayShowtimes.First().ShowTimeId;
                        }
                        if (i == 1)
                        {
                            prg.WeekdayProgramFpcId = 2;
                            prg.DurationInSec = 3300;
                            prg.WeekdayShowtimeId = 1;
                            prg.StartTime = DateTime.Parse("05:05:00").TimeOfDay;
                            prg.ProgramEpisodeId = ProgramEpisodeList[1].ProgramEpisodeId;
                            prg.WeekdayShowtimeId = WeekdayShowtimes[1].ShowTimeId;
                        }
                        _WeekdayProgramFpcList.Add(prg);
                    }
                }
                return _WeekdayProgramFpcList;
            }
        }

        private static List<ProgramSlot> _ProgramSlots;
        public static List<ProgramSlot> ProgramSlots
        {
            get
            {
                if (_ProgramSlots == null || _ProgramSlots.Count == 0)
                {
                    _ProgramSlots = new List<ProgramSlot>();
                    MockEntityDataGenerator mock = new MockEntityDataGenerator();
                    for (int i = 0; i < 2; i++)
                    {
                        ProgramSlot prgSlot = new ProgramSlot();
                        if (i == 0)
                        {
                            prgSlot.CreationDate = DateTime.Now;
                            prgSlot.ProgramSlotId = 1;
                            prgSlot.HourlySlotId = 1;
                            prgSlot.HalfHourlySlotId = 1;
                            prgSlot.WeekdayProgramFpcId = WeekdayProgramFpcList[0].WeekdayProgramFpcId;
                        }
                        if (i == 1)
                        {
                            prgSlot.CreationDate = DateTime.Now;
                            prgSlot.ProgramSlotId = 2;
                            prgSlot.HourlySlotId = 6;
                            prgSlot.HalfHourlySlotId = 11;
                            prgSlot.WeekdayProgramFpcId = WeekdayProgramFpcList[1].WeekdayProgramFpcId;
                        }
                        _ProgramSlots.Add(prgSlot);
                    }
                }
                return _ProgramSlots;
            }
        }

        private static List<Program> _Programs;
        public static List<Program> Programs
        {
            get
            {
                if (_Programs == null || _Programs.Count == 0)
                {
                    _Programs = new List<Program>();
                    MockEntityDataGenerator mock = new MockEntityDataGenerator();
                    for (int i = 0; i < 2; i++)
                    {
                        Program program = mock.GetMockProgram(false);
                        if (i == 0)
                        {
                            program.ProgramId = 1;
                            program.Name = "11th Hour with Waseem Badami";
                            program.GenreId = (int)GenreEs.News;
                            program.SubGenreId = (int)GenreEs.TalkShow;
                            program.SegmentsCount = 4;
                            program.BreakCount = 3;
                            program.ProgramTimeFrom = DateTime.Parse("00:05:00").TimeOfDay;
                            program.ProgramTimeTo = DateTime.Parse("00:55:00").TimeOfDay;
                            program.TransmissionTypeId = (int)TransmissionTypeEs.Live;                            
                        }
                        if (i == 1)
                        {
                            program.ProgramId = 2;
                            program.Name = "Off The Record";
                            program.GenreId = (int)GenreEs.News;
                            program.SubGenreId = (int)GenreEs.TalkShow;
                            program.SegmentsCount = 4;
                            program.BreakCount = 3;
                            program.ProgramTimeFrom = DateTime.Parse("05:05:00").TimeOfDay;
                            program.ProgramTimeTo = DateTime.Parse("05:55:00").TimeOfDay;
                            program.TransmissionTypeId = (int)TransmissionTypeEs.Live;
                        }
                        _Programs.Add(program);
                    }
                }
                return _Programs;
            }
        }

        private static List<ProgramEpisode> _ProgramEpisodeList;
        public static List<ProgramEpisode> ProgramEpisodeList
        {
            get
            {
                if (_ProgramEpisodeList == null)
                {
                    _ProgramEpisodeList = new List<ProgramEpisode>();

                    ProgramEpisode episode1 = new ProgramEpisode();
                    episode1.CopyFrom((object)Programs.FirstOrDefault());
                    episode1.ProgramId = Programs.FirstOrDefault().ProgramId;
                    episode1.EpisodeTitle = "EHWB-01";
                    episode1.CreationDate = DateTime.Now;
                    episode1.ProgramEpisodeId = 1;
                    episode1.TopicsCountFrom = 6;
                    episode1.TopicsCountTo = 10;

                    _ProgramEpisodeList.Add(episode1);

                    ProgramEpisode episode2 = new ProgramEpisode();
                    episode2.CopyFrom((object)Programs.FirstOrDefault());
                    episode2.ProgramId = Programs[1].ProgramId;
                    episode2.EpisodeTitle = "OTR-01";
                    episode2.CreationDate = DateTime.Now;
                    episode2.ProgramEpisodeId = 2;
                    episode2.TopicsCountFrom = 6;
                    episode2.TopicsCountTo = 10;

                    _ProgramEpisodeList.Add(episode2);

                }
                return _ProgramEpisodeList;
            }
        }

        private static List<ScreenElement> _ScreenElements;
        public static List<ScreenElement> ScreenElements
        {
            get
            {
                if (ScreenElements == null || _ScreenElements.Count == 0)
                {
                    _ScreenElements = new List<ScreenElement>();

                    ScreenElement scrElem = new ScreenElement();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Anchor";
                    scrElem.ScreenElementId = 1;

                    _ScreenElements.Add(scrElem);

                    scrElem = new ScreenElement();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Graphics";
                    scrElem.ScreenElementId = 2;

                    _ScreenElements.Add(scrElem);

                    scrElem = new ScreenElement();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Video";
                    scrElem.ScreenElementId = 3;

                    _ScreenElements.Add(scrElem);

                    scrElem = new ScreenElement();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Analyst";
                    scrElem.ScreenElementId = 4;

                    _ScreenElements.Add(scrElem);

                    scrElem = new ScreenElement();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Correspondent";
                    scrElem.ScreenElementId = 5;

                    _ScreenElements.Add(scrElem);

                    scrElem = new ScreenElement();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Guest";
                    scrElem.ScreenElementId = 6;

                    _ScreenElements.Add(scrElem);

                }

                return _ScreenElements;
            }

        }

        private static List<ScreenPosition> _ScreenPositions;
        public static List<ScreenPosition> ScreenPositions
        {
            get
            {
                if (_ScreenPositions == null || _ScreenPositions.Count == 0)
                {
                    _ScreenPositions = new List<ScreenPosition>();

                    ScreenPosition scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "On Camera Left";
                    scrPos.ScreenPositionId = 1;
                    scrPos.ShortCode = "OCL";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "On Camera Right";
                    scrPos.ScreenPositionId = 2;
                    scrPos.ShortCode = "OCR";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "On Camera Middle";
                    scrPos.ScreenPositionId = 3;
                    scrPos.ShortCode = "OCM";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "Zoom In Left";
                    scrPos.ScreenPositionId = 4;
                    scrPos.ShortCode = "ZIL";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "Zoom In Right";
                    scrPos.ScreenPositionId = 5;
                    scrPos.ShortCode = "ZIR";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "Zoom In Middle";
                    scrPos.ScreenPositionId = 6;
                    scrPos.ShortCode = "ZIM";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "2-Split";
                    scrPos.ScreenPositionId = 1;
                    scrPos.ShortCode = "2S";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "3-Split";
                    scrPos.ScreenPositionId = 1;
                    scrPos.ShortCode = "3S";

                    _ScreenPositions.Add(scrPos);

                    scrPos = new ScreenPosition();
                    scrPos.CreationDate = DateTime.Now;
                    scrPos.IsActive = true;
                    scrPos.Position = "4-Split";
                    scrPos.ScreenPositionId = 1;
                    scrPos.ShortCode = "4S";

                    _ScreenPositions.Add(scrPos);

                }

                return _ScreenPositions;
            }
        }

        private static List<ScreenElementCategory> _ScreenElementCategoryList;
        public static List<ScreenElementCategory> ScreenElementCategoryList
        {
            get
            {
                if (_ScreenElementCategoryList == null || _ScreenElementCategoryList.Count == 0)
                {
                    _ScreenElementCategoryList = new List<ScreenElementCategory>();

                    ScreenElementCategory scrElem = new ScreenElementCategory();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Only Anchor";
                    scrElem.ScreenElementCategoryId = 1;
                    scrElem.ShortCode = "A";

                    _ScreenElementCategoryList.Add(scrElem);

                    scrElem = new ScreenElementCategory();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Only Graphics";
                    scrElem.ScreenElementCategoryId = 2;
                    scrElem.ShortCode = "G";

                    _ScreenElementCategoryList.Add(scrElem);

                    scrElem = new ScreenElementCategory();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Only Video";
                    scrElem.ScreenElementCategoryId = 3;
                    scrElem.ShortCode = "V";

                    _ScreenElementCategoryList.Add(scrElem);

                    scrElem = new ScreenElementCategory();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Only Analyst";
                    scrElem.ScreenElementCategoryId = 4;
                    scrElem.ShortCode = "N";

                    _ScreenElementCategoryList.Add(scrElem);

                    scrElem = new ScreenElementCategory();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Only Correspondent";
                    scrElem.ScreenElementCategoryId = 5;
                    scrElem.ShortCode = "C";

                    _ScreenElementCategoryList.Add(scrElem);

                    scrElem = new ScreenElementCategory();
                    scrElem.CreationDate = DateTime.Now;
                    scrElem.IsActive = true;
                    scrElem.Name = "Only High Profile";
                    scrElem.ScreenElementCategoryId = 6;
                    scrElem.ShortCode = "H";

                    _ScreenElementCategoryList.Add(scrElem);

                }

                return _ScreenElementCategoryList;
            }

        }

        private static List<ScreenTemplate> _ScreenTemplates;
        public static List<ScreenTemplate> ScreenTemplates
        {
            get
            {
                if (_ScreenTemplates == null || _ScreenTemplates.Count == 0)
                {
                    _ScreenTemplates = new List<ScreenTemplate>();

                    ScreenTemplate scrTemp = new ScreenTemplate();
                    scrTemp.ScreenTemplateId = 1;
                    scrTemp.ScreenElementCategoryId = ScreenElementCategoryList[0].ScreenElementCategoryId;
                    scrTemp.CreationDate = DateTime.Now;
                    scrTemp.ImageThumb = "";
                    scrTemp.Name = "Anchor On Camera Left";

                    _ScreenTemplates.Add(scrTemp);

                    scrTemp = new ScreenTemplate();
                    scrTemp.ScreenTemplateId = 2;
                    scrTemp.ScreenElementCategoryId = ScreenElementCategoryList[0].ScreenElementCategoryId;
                    scrTemp.CreationDate = DateTime.Now;
                    scrTemp.ImageThumb = "";
                    scrTemp.Name = "Anchor On Camera Right";

                    _ScreenTemplates.Add(scrTemp);

                    scrTemp = new ScreenTemplate();
                    scrTemp.ScreenTemplateId = 3;
                    scrTemp.ScreenElementCategoryId = ScreenElementCategoryList[0].ScreenElementCategoryId;
                    scrTemp.CreationDate = DateTime.Now;
                    scrTemp.ImageThumb = "";
                    scrTemp.Name = "Anchor On Camera Middle";

                    _ScreenTemplates.Add(scrTemp);

                    scrTemp = new ScreenTemplate();
                    scrTemp.ScreenTemplateId = 4;
                    scrTemp.ScreenElementCategoryId = ScreenElementCategoryList[1].ScreenElementCategoryId;
                    scrTemp.CreationDate = DateTime.Now;
                    scrTemp.ImageThumb = "";
                    scrTemp.Name = "Graphics on Middle";

                    _ScreenTemplates.Add(scrTemp);
                }

                return _ScreenTemplates;
            }
        }

        private static List<ScreenTemplatePosition> _ScreenTemplatePositions;
        public static List<ScreenTemplatePosition> ScreenTemplatePositions
        {
            get
            {
                if (_ScreenTemplatePositions == null || _ScreenTemplatePositions.Count == 0)
                {
                    _ScreenTemplatePositions = new List<ScreenTemplatePosition>();

                    ScreenTemplatePosition scrTempPos = new ScreenTemplatePosition();
                    scrTempPos.ScreenTemplatePositionId = 1;
                    scrTempPos.ScreenTemplateId = ScreenTemplates[0].ScreenTemplateId;
                    scrTempPos.ScreenPositionId = ScreenPositions[0].ScreenPositionId;
                    scrTempPos.ScreenElementId = ScreenElements[0].ScreenElementId;
                    scrTempPos.CreationDate = DateTime.Now;
                    scrTempPos.IsActive = true;

                    _ScreenTemplatePositions.Add(scrTempPos);

                    scrTempPos = new ScreenTemplatePosition();
                    scrTempPos.ScreenTemplatePositionId = 2;
                    scrTempPos.ScreenTemplateId = ScreenTemplates[1].ScreenTemplateId;
                    scrTempPos.ScreenPositionId = ScreenPositions[1].ScreenPositionId;
                    scrTempPos.ScreenElementId = ScreenElements[0].ScreenElementId;
                    scrTempPos.CreationDate = DateTime.Now;
                    scrTempPos.IsActive = true;

                    _ScreenTemplatePositions.Add(scrTempPos);

                    scrTempPos = new ScreenTemplatePosition();
                    scrTempPos.ScreenTemplatePositionId = 3;
                    scrTempPos.ScreenTemplateId = ScreenTemplates[2].ScreenTemplateId;
                    scrTempPos.ScreenPositionId = ScreenPositions[2].ScreenPositionId;
                    scrTempPos.ScreenElementId = ScreenElements[0].ScreenElementId;
                    scrTempPos.CreationDate = DateTime.Now;
                    scrTempPos.IsActive = true;

                    _ScreenTemplatePositions.Add(scrTempPos);

                    scrTempPos = new ScreenTemplatePosition();
                    scrTempPos.ScreenTemplatePositionId = 4;
                    scrTempPos.ScreenTemplateId = ScreenTemplates[3].ScreenTemplateId;
                    scrTempPos.ScreenPositionId = ScreenPositions[2].ScreenPositionId;
                    scrTempPos.ScreenElementId = ScreenElements[1].ScreenElementId;
                    scrTempPos.CreationDate = DateTime.Now;
                    scrTempPos.IsActive = true;

                    _ScreenTemplatePositions.Add(scrTempPos); 
                }

                return _ScreenTemplatePositions;
            }
        }

        private static List<ProgramElementTemplate> _ProgramElementTemplates;
        public static List<ProgramElementTemplate> ProgramElementTemplates
        {
            get
            {
                if (_ProgramElementTemplates == null || _ProgramElementTemplates.Count == 0)
                {
                    _ProgramElementTemplates = new List<ProgramElementTemplate>();

                    ProgramElementTemplate prgElemTemp = new ProgramElementTemplate();
                    prgElemTemp.ProgramElementTemplateId = 1;
                    prgElemTemp.CreationDate = DateTime.Now;
                    prgElemTemp.IsActive = true;
                    prgElemTemp.ProgramEpisodeId = ProgramEpisodeList.First().ProgramEpisodeId ;
                    prgElemTemp.ScreenTemplateId = ScreenTemplates[0].ScreenTemplateId;
                    _ProgramElementTemplates.Add(prgElemTemp);

                    prgElemTemp = new ProgramElementTemplate();
                    prgElemTemp.ProgramElementTemplateId = 2;
                    prgElemTemp.CreationDate = DateTime.Now;
                    prgElemTemp.IsActive = true;
                    prgElemTemp.ProgramEpisodeId = ProgramEpisodeList.First().ProgramEpisodeId;
                    prgElemTemp.ScreenTemplateId = ScreenTemplates[1].ScreenTemplateId;
                    _ProgramElementTemplates.Add(prgElemTemp);

                    prgElemTemp = new ProgramElementTemplate();
                    prgElemTemp.ProgramElementTemplateId = 3;
                    prgElemTemp.CreationDate = DateTime.Now;
                    prgElemTemp.IsActive = true;
                    prgElemTemp.ProgramEpisodeId = ProgramEpisodeList.First().ProgramEpisodeId;
                    prgElemTemp.ScreenTemplateId = ScreenTemplates[2].ScreenTemplateId;
                    _ProgramElementTemplates.Add(prgElemTemp);

                    prgElemTemp = new ProgramElementTemplate();
                    prgElemTemp.ProgramElementTemplateId = 4;
                    prgElemTemp.CreationDate = DateTime.Now;
                    prgElemTemp.IsActive = true;
                    prgElemTemp.ProgramEpisodeId = ProgramEpisodeList.First().ProgramEpisodeId;
                    prgElemTemp.ScreenTemplateId = ScreenTemplates[3].ScreenTemplateId;
                    _ProgramElementTemplates.Add(prgElemTemp);
                    
                }
                return _ProgramElementTemplates;
            }
        }

        #endregion
    }
}
 