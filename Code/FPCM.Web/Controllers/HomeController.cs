﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCM.Core;
using FPCM.Core.Entities;

namespace FPCM.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<ProgramElementTemplate> tmpltList = MockFPCM.ProgramElementTemplates;
            return View();
        }
    }
}
