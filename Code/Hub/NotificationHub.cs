﻿using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Threading;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core;

namespace Hub
{
    public partial class Form1 : Form
    {
        public IDeviceService iDeviceService = IoC.Resolve<IDeviceService>("DeviceService");
        private string ncsId = string.Empty;
        public MOSServer server;
        public List<MOSClient> clients = new List<MOSClient>();
        public static List<Device> list = new List<Device>();

        public void Start(int port,string id)
        {
            list=  iDeviceService.GetDeviceByGroupId(86);
            ncsId = id;
            StartListening(port);
            ConnectClients();
        }

        public void StartListening(int port)
        {
            server = new MOSServer(ncsId);
            server.NotificationReceived += NotificationReceived;
            server.StartListening(port);
        }

        public void ConnectClients()
        {
            string message = string.Empty;
            for (int i = 0; i < list.Count; i++)
            {
                MOSClient client = new MOSClient(list[i].DeviceKey, list[i].Ip, list[i].Port, ncsId);
                client.GroupId = list[i].GroupId.ToString();
                clients.Add(client);
                client.Connect(out message);               
            }
        }

        public MOS NotificationReceived(MOS request)
        {
            MOS response = new MOS();
            ROAck roAck = new ROAck();

            try
            {
                Notification notification = (Notification)request.command;
                response.messageID = request.messageID;
                response.ncsID = request.ncsID;
                response.mosID = request.mosID;

                Thread oThread = new Thread(() => { NotifyClients(request); });
                oThread.Start();

                roAck.roID = notification.roID;
                roAck.roStatus = "OK";
            }
            catch
            {
                roAck.roStatus = "NACK";
            }
            response.command = roAck;


            return response;
        }

        public void NotifyClients(MOS request)
        {
            string message = string.Empty;
            Notification notification = (Notification)request.command;

                foreach (MOSClient client in clients)
                {
                    if (!client.IsConnected)
                    {
                        client.Connect(out message);
                    }
                    if (client.IsConnected)
                    {
                        request.ncsID = ncsId;
                        MOS result = client.SendMOSCommand(request, out message);
                    }
                }
        }
    }
}
