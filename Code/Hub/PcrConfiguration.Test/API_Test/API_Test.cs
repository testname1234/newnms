﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Test.Helper;
using PcrConfiguration.Core;
using System.Net;
using Newtonsoft.Json;
using PcrConfiguration.Core.DataTransfer.Device;
using PcrConfiguration.Core.DataTransfer.DeviceStatus;
using PcrConfiguration.Core.DataTransfer.DeviceStatusHistory;
using PcrConfiguration.Core.DataTransfer.DeviceType;
using PcrConfiguration.Core.DataTransfer.Group;

namespace PcrConfiguration.Test
{
    [TestClass]
    public class API_Test
    {
        #region Private Field

        const string initialUrl = "http://localhost:61113/api";
        const string dir = "D:\\GeneratedCode\\PcrConfiguration.WebAPI\\Views\\Home\\";
        const string contentDir = "D:\\GeneratedCode\\PcrConfiguration.WebAPI\\Content\\";
        static string htmlTemplate = @"<header><div class='content-wrapper'><div class='float-left'><p class='site-title'><a href='~/'>PcrConfiguration Web API</a></p></div></div></header><div id='body'><section class='content-wrapper main-content clear-fix'><h3>Web Calls</h3><ol class='round'>HTMLTEMPLATE</ol></section></div>";
        static string htmlContent = string.Empty;
        static string indexFile = dir + "index.cshtml";

        #endregion

        #region Tests
        
		[TestMethod]
		public void GetDevice_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Device device = mock.GetMockDevice(true);
            AssertDevice_Get(device, Get<PcrConfiguration.Core.DataTransfer.Device.GetOutput>(string.Format("/device/GetDeviceById/{0}", device.DeviceId), "GET_Device", null).Data);
		}

		[TestMethod]
		public void GetDeviceStatus_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatus devicestatus = mock.GetMockDeviceStatus(true);
			AssertDeviceStatus_Get(devicestatus,Get<PcrConfiguration.Core.DataTransfer.DeviceStatus.GetOutput>(string.Format("/devicestatus/{0}",devicestatus.DeviceStatusId),"GET_DeviceStatus",null).Data);
		}

		[TestMethod]
		public void GetDeviceStatusHistory_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatusHistory devicestatushistory = mock.GetMockDeviceStatusHistory(true);
			AssertDeviceStatusHistory_Get(devicestatushistory,Get<PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.GetOutput>(string.Format("/devicestatushistory/{0}",devicestatushistory.DeviceStatusHistoryId),"GET_DeviceStatusHistory",null).Data);
		}

		[TestMethod]
		public void GetDeviceType_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceType devicetype = mock.GetMockDeviceType(true);
			AssertDeviceType_Get(devicetype,Get<PcrConfiguration.Core.DataTransfer.DeviceType.GetOutput>(string.Format("/devicetype/{0}",devicetype.DeviceTypeId),"GET_DeviceType",null).Data);
		}

		[TestMethod]
		public void GetGroup_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Group group = mock.GetMockGroup(true);
            AssertGroup_Get(group, Get<PcrConfiguration.Core.DataTransfer.Group.GetOutput>(string.Format("/group/GetGroupById/{0}", group.GroupId), "GET_Group", null).Data);
		}

		[TestMethod]
		public void GetAllDevice_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Device device = mock.GetMockDevice(true);
            Assert.AreEqual(true, Get<List<PcrConfiguration.Core.DataTransfer.Device.GetOutput>>("/device/GetAllDevices", "GETALL_Device", null).IsSuccess);
		}

		[TestMethod]
		public void GetAllDeviceStatus_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatus devicestatus = mock.GetMockDeviceStatus(true);
			Assert.AreEqual(true,Get<List<PcrConfiguration.Core.DataTransfer.DeviceStatus.GetOutput>>("/devicestatus","GETALL_DeviceStatus",null).IsSuccess);
		}

		[TestMethod]
		public void GetAllDeviceStatusHistory_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatusHistory devicestatushistory = mock.GetMockDeviceStatusHistory(true);
			Assert.AreEqual(true,Get<List<PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.GetOutput>>("/devicestatushistory","GETALL_DeviceStatusHistory",null).IsSuccess);
		}

		[TestMethod]
		public void GetAllDeviceType_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceType devicetype = mock.GetMockDeviceType(true);
			Assert.AreEqual(true,Get<List<PcrConfiguration.Core.DataTransfer.DeviceType.GetOutput>>("/devicetype","GETALL_DeviceType",null).IsSuccess);
		}

		[TestMethod]
		public void GetAllGroup_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Group group = mock.GetMockGroup(true);
            Assert.AreEqual(true, Get<List<PcrConfiguration.Core.DataTransfer.Group.GetOutput>>("/group/GetAllGroups", "GETALL_Group", null).IsSuccess);
		}

		[TestMethod]
		public void PostDevice_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Device device = mock.GetMockDevice(false);
		PcrConfiguration.Core.DataTransfer.Device.PostInput input = new PcrConfiguration.Core.DataTransfer.Device.PostInput();
		input.CopyFrom(device);
        AssertDevice_Post(device, Post<PcrConfiguration.Core.DataTransfer.Device.PostOutput, PcrConfiguration.Core.DataTransfer.Device.PostInput>("/device/InsertDevice", input, "POST_Device", null).Data);
		}

		[TestMethod]
		public void PostDeviceStatus_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatus devicestatus = mock.GetMockDeviceStatus(false);
		PcrConfiguration.Core.DataTransfer.DeviceStatus.PostInput input = new PcrConfiguration.Core.DataTransfer.DeviceStatus.PostInput();
		input.CopyFrom(devicestatus);
			AssertDeviceStatus_Post(devicestatus,Post<PcrConfiguration.Core.DataTransfer.DeviceStatus.PostOutput,PcrConfiguration.Core.DataTransfer.DeviceStatus.PostInput>("/devicestatus",input,"POST_DeviceStatus",null).Data);
		}

		[TestMethod]
		public void PostDeviceStatusHistory_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatusHistory devicestatushistory = mock.GetMockDeviceStatusHistory(false);
		PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PostInput input = new PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PostInput();
		input.CopyFrom(devicestatushistory);
			AssertDeviceStatusHistory_Post(devicestatushistory,Post<PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PostOutput,PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PostInput>("/devicestatushistory",input,"POST_DeviceStatusHistory",null).Data);
		}

		[TestMethod]
		public void PostDeviceType_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceType devicetype = mock.GetMockDeviceType(false);
		PcrConfiguration.Core.DataTransfer.DeviceType.PostInput input = new PcrConfiguration.Core.DataTransfer.DeviceType.PostInput();
		input.CopyFrom(devicetype);
			AssertDeviceType_Post(devicetype,Post<PcrConfiguration.Core.DataTransfer.DeviceType.PostOutput,PcrConfiguration.Core.DataTransfer.DeviceType.PostInput>("/devicetype",input,"POST_DeviceType",null).Data);
		}

		[TestMethod]
		public void PostGroup_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Group group = mock.GetMockGroup(false);
		PcrConfiguration.Core.DataTransfer.Group.PostInput input = new PcrConfiguration.Core.DataTransfer.Group.PostInput();
		input.CopyFrom(group);
        AssertGroup_Post(group, Post<PcrConfiguration.Core.DataTransfer.Group.PostOutput, PcrConfiguration.Core.DataTransfer.Group.PostInput>("/group/InserGroup", input, "POST_Group", null).Data);
		}

		[TestMethod]
		public void PutDevice_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Device device_toupdate = mock.GetMockDevice(true);
		Device device_update = mock.GetMockDevice(false);
		device_update.DeviceId = device_toupdate.DeviceId;
		PcrConfiguration.Core.DataTransfer.Device.PutInput input = new PcrConfiguration.Core.DataTransfer.Device.PutInput();
		input.CopyFrom(device_update);
			AssertDevice_Put(device_update,Put<PcrConfiguration.Core.DataTransfer.Device.PutOutput,PcrConfiguration.Core.DataTransfer.Device.PutInput>("/device",input,"PUT_Device",null).Data);
		}

		[TestMethod]
		public void PutDeviceStatus_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatus devicestatus_toupdate = mock.GetMockDeviceStatus(true);
		DeviceStatus devicestatus_update = mock.GetMockDeviceStatus(false);
		devicestatus_update.DeviceStatusId = devicestatus_toupdate.DeviceStatusId;
		PcrConfiguration.Core.DataTransfer.DeviceStatus.PutInput input = new PcrConfiguration.Core.DataTransfer.DeviceStatus.PutInput();
		input.CopyFrom(devicestatus_update);
			AssertDeviceStatus_Put(devicestatus_update,Put<PcrConfiguration.Core.DataTransfer.DeviceStatus.PutOutput,PcrConfiguration.Core.DataTransfer.DeviceStatus.PutInput>("/devicestatus",input,"PUT_DeviceStatus",null).Data);
		}

		[TestMethod]
		public void PutDeviceStatusHistory_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatusHistory devicestatushistory_toupdate = mock.GetMockDeviceStatusHistory(true);
		DeviceStatusHistory devicestatushistory_update = mock.GetMockDeviceStatusHistory(false);
		devicestatushistory_update.DeviceStatusHistoryId = devicestatushistory_toupdate.DeviceStatusHistoryId;
		PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PutInput input = new PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PutInput();
		input.CopyFrom(devicestatushistory_update);
			AssertDeviceStatusHistory_Put(devicestatushistory_update,Put<PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PutOutput,PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PutInput>("/devicestatushistory",input,"PUT_DeviceStatusHistory",null).Data);
		}

		[TestMethod]
		public void PutDeviceType_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceType devicetype_toupdate = mock.GetMockDeviceType(true);
		DeviceType devicetype_update = mock.GetMockDeviceType(false);
		devicetype_update.DeviceTypeId = devicetype_toupdate.DeviceTypeId;
		PcrConfiguration.Core.DataTransfer.DeviceType.PutInput input = new PcrConfiguration.Core.DataTransfer.DeviceType.PutInput();
		input.CopyFrom(devicetype_update);
			AssertDeviceType_Put(devicetype_update,Put<PcrConfiguration.Core.DataTransfer.DeviceType.PutOutput,PcrConfiguration.Core.DataTransfer.DeviceType.PutInput>("/devicetype",input,"PUT_DeviceType",null).Data);
		}

		[TestMethod]
		public void PutGroup_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Group group_toupdate = mock.GetMockGroup(true);
		Group group_update = mock.GetMockGroup(false);
		group_update.GroupId = group_toupdate.GroupId;
		PcrConfiguration.Core.DataTransfer.Group.PutInput input = new PcrConfiguration.Core.DataTransfer.Group.PutInput();
		input.CopyFrom(group_update);
			AssertGroup_Put(group_update,Put<PcrConfiguration.Core.DataTransfer.Group.PutOutput,PcrConfiguration.Core.DataTransfer.Group.PutInput>("/group",input,"PUT_Group",null).Data);
		}

		[TestMethod]
		public void DeleteDevice_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Device device = mock.GetMockDevice(true);
			Assert.AreEqual("true",Delete<String>(string.Format("/device/{0}",device.DeviceId),"DELETE_Device",null).Data.ToString());
		}

		[TestMethod]
		public void DeleteDeviceStatus_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatus devicestatus = mock.GetMockDeviceStatus(true);
			Assert.AreEqual("true",Delete<String>(string.Format("/devicestatus/{0}",devicestatus.DeviceStatusId),"DELETE_DeviceStatus",null).Data.ToString());
		}

		[TestMethod]
		public void DeleteDeviceStatusHistory_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceStatusHistory devicestatushistory = mock.GetMockDeviceStatusHistory(true);
			Assert.AreEqual("true",Delete<String>(string.Format("/devicestatushistory/{0}",devicestatushistory.DeviceStatusHistoryId),"DELETE_DeviceStatusHistory",null).Data.ToString());
		}

		[TestMethod]
		public void DeleteDeviceType_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			DeviceType devicetype = mock.GetMockDeviceType(true);
			Assert.AreEqual("true",Delete<String>(string.Format("/devicetype/{0}",devicetype.DeviceTypeId),"DELETE_DeviceType",null).Data.ToString());
		}

		[TestMethod]
		public void DeleteGroup_Test()
		{
			MockEntityDataGeneratorBase mock = new MockEntityDataGeneratorBase();
			Group group = mock.GetMockGroup(true);
			Assert.AreEqual("true",Delete<String>(string.Format("/group/{0}",group.GroupId),"DELETE_Group",null).Data.ToString());
		}

        [ClassCleanup]
        public static void Write_Main_File()
        {
            WriteMainFile();
        }        

        #endregion

        #region Helpers

        public DataTransfer<TResponse> Get<TResponse>(string requestUrl)
        {
            WebProxyUtil proxy = new WebProxyUtil();
            HttpWebResponse response = proxy.GetRequest(initialUrl + requestUrl, null, true, null);
            try
            {
                if (response != null)
                {
                    DataTransfer<TResponse> dataTransfer = JSONHelper.GetObject<DataTransfer<TResponse>>(response.GetResponseStream());
                    if (response != null)
                        response.Close();
                    return dataTransfer;
                }
                else return null;
            }
            catch (Exception)
            {
                if (response != null)
                    response.Close();
                throw;
            }
        }
        public DataTransfer<TResponse> Get<TResponse>(string requestUrl, string requestName, Dictionary<string, string> headerParameter = null)
        {
            WebProxyUtil proxy = new WebProxyUtil();
            string fileName = string.Format("{0}.txt", requestName);
            htmlContent += string.Format("<li><a href='{0}'>{1}</a></li>", "~/Content/Services/" + fileName, requestName);
            PrintObj<DataTransfer<TResponse>, string> printObj = new PrintObj<DataTransfer<TResponse>, string>();
            printObj.Method = "GET";
            if (headerParameter != null && headerParameter.Count > 0)
            {
                printObj.Headers = new string[headerParameter.Keys.Count];
                int i = 0;
                foreach (string key in headerParameter.Keys)
                {
                    printObj.Headers[i] = string.Format("{0}={1}", key, headerParameter[key]);
                    i++;
                }
            }
            printObj.RequestUrl = initialUrl + requestUrl;
            HttpWebResponse response = proxy.GetRequest(initialUrl + requestUrl, null, true, headerParameter);
            try
            {
                if (response != null)
                {
                    DataTransfer<TResponse> dataTransfer = JSONHelper.GetObject<DataTransfer<TResponse>>(response.GetResponseStream());
                    printObj.Response = dataTransfer;
                    WriteFile(printObj, fileName);
                    if (response != null)
                        response.Close();
                    return dataTransfer;
                }
                else return null;
            }
            catch (Exception)
            {
                if (response != null)
                    response.Close();
                throw;
            }
        }
        public DataTransfer<TResponse> Put<TResponse, TRequest>(string requestUrl, TRequest request, string requestName, Dictionary<string, string> headerParameter = null)
        {
            WebProxyUtil proxy = new WebProxyUtil();
            string fileName = string.Format("{0}.txt", requestName);
            htmlContent += string.Format("<li><a href='{0}'>{1}</a></li>", "~/Content/Services/" + fileName, requestName);
            PrintObj<DataTransfer<TResponse>, TRequest> printObj = new PrintObj<DataTransfer<TResponse>, TRequest>();
            printObj.Method = "PUT";
            printObj.PostBody = request;
            if (headerParameter != null && headerParameter.Count > 0)
            {
                printObj.Headers = new string[headerParameter.Keys.Count];
                int i = 0;
                foreach (string key in headerParameter.Keys)
                {
                    printObj.Headers[i] = string.Format("{0}={1}", key, headerParameter[key]);
                    i++;
                }
            }
            printObj.RequestUrl = initialUrl + requestUrl;
            HttpWebResponse response = proxy.PutRequest(initialUrl + requestUrl, request, null, true, headerParameter);
            try
            {
                if (response != null)
                {
                    DataTransfer<TResponse> dataTransfer = JSONHelper.GetObject<DataTransfer<TResponse>>(response.GetResponseStream());
                    printObj.Response = dataTransfer;
                    WriteFile(printObj, fileName);
                    if (response != null)
                        response.Close();
                    return dataTransfer;
                }
                else return null;
            }
            catch (Exception)
            {
                if (response != null)
                    response.Close();
                throw;
            }
        }
        public DataTransfer<TResponse> Delete<TResponse>(string requestUrl, string requestName, Dictionary<string, string> headerParameter = null)
        {
            WebProxyUtil proxy = new WebProxyUtil();
            string fileName = string.Format("{0}.txt", requestName);
            htmlContent += string.Format("<li><a href='{0}'>{1}</a></li>", "~/Content/Services/" + fileName, requestName);
            PrintObj<DataTransfer<TResponse>,String> printObj = new PrintObj<DataTransfer<TResponse>,String>();
            printObj.Method = "DELETE";
            if (headerParameter != null && headerParameter.Count > 0)
            {
                printObj.Headers = new string[headerParameter.Keys.Count];
                int i = 0;
                foreach (string key in headerParameter.Keys)
                {
                    printObj.Headers[i] = string.Format("{0}={1}", key, headerParameter[key]);
                    i++;
                }
            }
            printObj.RequestUrl = initialUrl + requestUrl;
            HttpWebResponse response = proxy.DeleteRequest(initialUrl + requestUrl, null, true, headerParameter);
            try
            {
                if (response != null)
                {
                    DataTransfer<TResponse> dataTransfer = JSONHelper.GetObject<DataTransfer<TResponse>>(response.GetResponseStream());
                    printObj.Response = dataTransfer;
                    WriteFile(printObj, fileName);
                    if (response != null)
                        response.Close();
                    return dataTransfer;
                }
                else return null;
            }
            catch (Exception)
            {
                if (response != null)
                    response.Close();
                throw;
            }
        }
        public DataTransfer<TResponse> Post<TResponse, TRequest>(string requestUrl, TRequest request, string requestName, Dictionary<string, string> headerParameter = null)
        {
            WebProxyUtil proxy = new WebProxyUtil();
            string fileName = string.Format("{0}.txt", requestName);
            htmlContent += string.Format("<li><a href='{0}'>{1}</a></li>", "~/Content/Services/" + fileName, requestName);
            PrintObj<DataTransfer<TResponse>, TRequest> printObj = new PrintObj<DataTransfer<TResponse>, TRequest>();
            printObj.Method = "POST";
            printObj.PostBody = request;
            if (headerParameter != null && headerParameter.Count > 0)
            {
                printObj.Headers = new string[headerParameter.Keys.Count];
                int i = 0;
                foreach (string key in headerParameter.Keys)
                {
                    printObj.Headers[i] = string.Format("{0}={1}", key, headerParameter[key]);
                    i++;
                }
            }
            printObj.RequestUrl = initialUrl + requestUrl;
            HttpWebResponse response = proxy.PostRequest(initialUrl + requestUrl, request, null, true, headerParameter);
            try
            {
                if (response != null)
                {
                    DataTransfer<TResponse> dataTransfer = JSONHelper.GetObject<DataTransfer<TResponse>>(response.GetResponseStream());
                    printObj.Response = dataTransfer;
                    WriteFile(printObj, fileName);
                    if (response != null)
                        response.Close();
                    return dataTransfer;
                }
                else return null;
            }
            catch (Exception)
            {
                if (response != null)
                    response.Close();
                throw;
            }
        }
        public void WriteFile<TResponse, TRequest>(PrintObj<TResponse, TRequest> printObj, string fileName)
        {
            string[] content = new string[1];
            fileName = contentDir + @"Services\" + fileName;
            content[0] = JsonConvert.SerializeObject(printObj, Formatting.Indented);
            System.IO.File.WriteAllLines(fileName, content);
        }
        public static void WriteMainFile()
        {
            string[] content = new string[1];
            content[0] = htmlTemplate.Replace("HTMLTEMPLATE", htmlContent);
            System.IO.File.WriteAllLines(indexFile, content);
        }

		public void AssertDevice_Get(Device expected,PcrConfiguration.Core.DataTransfer.Device.GetOutput actual)
		{
			Assert.AreEqual(expected.DeviceId.ToString(), actual.DeviceId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
			Assert.AreEqual(expected.DeviceKey.ToString(), actual.DeviceKey.ToString());
			Assert.AreEqual(expected.IsMosDevice.ToString(), actual.IsMosDevice.ToString());
			Assert.AreEqual(expected.Ip.ToString(), actual.Ip.ToString());
			Assert.AreEqual(expected.Port.ToString(), actual.Port.ToString());
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.DeviceTypeId.ToString(), actual.DeviceTypeId.ToString());
		}
		public void AssertDeviceStatus_Get(DeviceStatus expected,PcrConfiguration.Core.DataTransfer.DeviceStatus.GetOutput actual)
		{
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
		}
		public void AssertDeviceStatusHistory_Get(DeviceStatusHistory expected,PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.GetOutput actual)
		{
			Assert.AreEqual(expected.DeviceStatusHistoryId.ToString(), actual.DeviceStatusHistoryId.ToString());
			Assert.AreEqual(expected.DeviceId.ToString(), actual.DeviceId.ToString());
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
		}
		public void AssertDeviceType_Get(DeviceType expected,PcrConfiguration.Core.DataTransfer.DeviceType.GetOutput actual)
		{
			Assert.AreEqual(expected.DeviceTypeId.ToString(), actual.DeviceTypeId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
		}
		public void AssertGroup_Get(Group expected,PcrConfiguration.Core.DataTransfer.Group.GetOutput actual)
		{
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
			Assert.AreEqual(expected.GroupKey.ToString(), actual.GroupKey.ToString());
			Assert.AreEqual(expected.ParentId.ToString(), actual.ParentId.ToString());
		}
		public void AssertDevice_Post(Device expected,PcrConfiguration.Core.DataTransfer.Device.PostOutput actual)
		{
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
			Assert.AreEqual(expected.DeviceKey.ToString(), actual.DeviceKey.ToString());
			Assert.AreEqual(expected.IsMosDevice.ToString(), actual.IsMosDevice.ToString());
			Assert.AreEqual(expected.Ip.ToString(), actual.Ip.ToString());
			Assert.AreEqual(expected.Port.ToString(), actual.Port.ToString());
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.DeviceTypeId.ToString(), actual.DeviceTypeId.ToString());
		}
		public void AssertDeviceStatus_Post(DeviceStatus expected,PcrConfiguration.Core.DataTransfer.DeviceStatus.PostOutput actual)
		{
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
		}
		public void AssertDeviceStatusHistory_Post(DeviceStatusHistory expected,PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PostOutput actual)
		{
			Assert.AreEqual(expected.DeviceId.ToString(), actual.DeviceId.ToString());
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
		}
		public void AssertDeviceType_Post(DeviceType expected,PcrConfiguration.Core.DataTransfer.DeviceType.PostOutput actual)
		{
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
		}
		public void AssertGroup_Post(Group expected,PcrConfiguration.Core.DataTransfer.Group.PostOutput actual)
		{
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
			Assert.AreEqual(expected.GroupKey.ToString(), actual.GroupKey.ToString());
			Assert.AreEqual(expected.ParentId.ToString(), actual.ParentId.ToString());
		}
		public void AssertDevice_Put(Device expected,PcrConfiguration.Core.DataTransfer.Device.PutOutput actual)
		{
			Assert.AreEqual(expected.DeviceId.ToString(), actual.DeviceId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
			Assert.AreEqual(expected.DeviceKey.ToString(), actual.DeviceKey.ToString());
			Assert.AreEqual(expected.IsMosDevice.ToString(), actual.IsMosDevice.ToString());
			Assert.AreEqual(expected.Ip.ToString(), actual.Ip.ToString());
			Assert.AreEqual(expected.Port.ToString(), actual.Port.ToString());
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.DeviceTypeId.ToString(), actual.DeviceTypeId.ToString());
		}
		public void AssertDeviceStatus_Put(DeviceStatus expected,PcrConfiguration.Core.DataTransfer.DeviceStatus.PutOutput actual)
		{
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
		}
		public void AssertDeviceStatusHistory_Put(DeviceStatusHistory expected,PcrConfiguration.Core.DataTransfer.DeviceStatusHistory.PutOutput actual)
		{
			Assert.AreEqual(expected.DeviceStatusHistoryId.ToString(), actual.DeviceStatusHistoryId.ToString());
			Assert.AreEqual(expected.DeviceId.ToString(), actual.DeviceId.ToString());
			Assert.AreEqual(expected.DeviceStatusId.ToString(), actual.DeviceStatusId.ToString());
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
		}
		public void AssertDeviceType_Put(DeviceType expected,PcrConfiguration.Core.DataTransfer.DeviceType.PutOutput actual)
		{
			Assert.AreEqual(expected.DeviceTypeId.ToString(), actual.DeviceTypeId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
		}
		public void AssertGroup_Put(Group expected,PcrConfiguration.Core.DataTransfer.Group.PutOutput actual)
		{
			Assert.AreEqual(expected.GroupId.ToString(), actual.GroupId.ToString());
			Assert.AreEqual(expected.Name.ToString(), actual.Name.ToString());
			Assert.AreEqual(expected.GroupKey.ToString(), actual.GroupKey.ToString());
			Assert.AreEqual(expected.ParentId.ToString(), actual.ParentId.ToString());
		}        
        
        #endregion
    }
}
