﻿using System;
using System.Runtime.Serialization.Json;
using System.IO;

namespace PcrConfiguration.Test.Helper
{
    public class JSONHelper
    {
        public static string GetString(object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                var ser = new DataContractJsonSerializer(obj.GetType());
                ser.WriteObject(ms, obj);
                ms.Seek(0, SeekOrigin.Begin);
                var sr = new StreamReader(ms);
                return sr.ReadToEnd();
            }
        }

        public static object GetObject(Stream stream, object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                var ser = new DataContractJsonSerializer(obj.GetType());
                obj = ser.ReadObject(stream);
                return obj;
            }
        }

        public static R GetObject<R>(Stream stream)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                var ser = new DataContractJsonSerializer(typeof(R));
                object obj = ser.ReadObject(stream);
                if (obj != null)
                {
                    stream.Close();
                    return (R)obj;
                }
                else
                {
                    stream.Close();
                    return default(R);
                }
            }
        }

        public static object GetObject(string json, object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                StringReader sr = new StringReader(json);
                byte[] byteArray = System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(json);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(byteArray);
                ms.Position = 0;
                var ser = new DataContractJsonSerializer(obj.GetType());
                obj = ser.ReadObject(memoryStream);
                return obj;
            }
        }

        public static string GetStringMessage(Stream stm)
        {
            StreamReader reader = new StreamReader(stm);
            return reader.ReadToEnd();
        }

    }
}
