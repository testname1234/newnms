﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core;

namespace PcrConfiguration.Test
{
		
	public partial class MockEntityDataGeneratorBase
	{
		public virtual Device GetMockDevice(bool isSaved)
		{
			Device entity=new Device();
			entity.Name=TestUtility.Instance.RandomString(24, false);;
			entity.DeviceKey=TestUtility.Instance.RandomString(11, false);;
			entity.IsMosDevice=TestUtility.Instance.RandomBoolean();
			entity.Ip=TestUtility.Instance.RandomString(14, false);;
			entity.Port=TestUtility.Instance.RandomNumber();
			entity.LastHeartBeatTime=TestUtility.Instance.RandomDateTime();
			entity.LastUpdatedDate=TestUtility.Instance.RandomDateTime();
			entity.CreationDate=TestUtility.Instance.RandomDateTime();
			entity.DeviceStatusId=GetMockDeviceStatus(true).DeviceStatusId;
			entity.DeviceTypeId=GetMockDeviceType(true).DeviceTypeId;
			entity.GroupId=GetMockGroup(true).GroupId;
			if(isSaved)
			{
				IDeviceRepository rep=IoC.Resolve<IDeviceRepository>("DeviceRepository");
				entity=rep.InsertDevice(entity);
			}
			return entity;
		}

		public virtual DeviceStatus GetMockDeviceStatus(bool isSaved)
		{
			DeviceStatus entity=new DeviceStatus();
			entity.Name=TestUtility.Instance.RandomString(14, false);;
			if(isSaved)
			{
				IDeviceStatusRepository rep=IoC.Resolve<IDeviceStatusRepository>("DeviceStatusRepository");
				entity=rep.InsertDeviceStatus(entity);
			}
			return entity;
		}

		public virtual DeviceStatusHistory GetMockDeviceStatusHistory(bool isSaved)
		{
			DeviceStatusHistory entity=new DeviceStatusHistory();
			entity.CreationDate=TestUtility.Instance.RandomDateTime();
			entity.DeviceId=GetMockDevice(true).DeviceId;
			entity.DeviceStatusId=GetMockDeviceStatus(true).DeviceStatusId;
			entity.GroupId=GetMockGroup(true).GroupId;
			if(isSaved)
			{
				IDeviceStatusHistoryRepository rep=IoC.Resolve<IDeviceStatusHistoryRepository>("DeviceStatusHistoryRepository");
				entity=rep.InsertDeviceStatusHistory(entity);
			}
			return entity;
		}

		public virtual DeviceType GetMockDeviceType(bool isSaved)
		{
			DeviceType entity=new DeviceType();
			entity.Name=TestUtility.Instance.RandomString(14, false);;
			if(isSaved)
			{
				IDeviceTypeRepository rep=IoC.Resolve<IDeviceTypeRepository>("DeviceTypeRepository");
				entity=rep.InsertDeviceType(entity);
			}
			return entity;
		}

		public virtual Group GetMockGroup(bool isSaved)
		{
			Group entity=new Group();
			entity.Name=TestUtility.Instance.RandomString(24, false);;
			entity.GroupKey=TestUtility.Instance.RandomString(11, false);;
			entity.ParentId=TestUtility.Instance.RandomNumber();
			entity.CreationDate=TestUtility.Instance.RandomDateTime();
			if(isSaved)
			{
				IGroupRepository rep=IoC.Resolve<IGroupRepository>("GroupRepository");
				entity=rep.InsertGroup(entity);
			}
			return entity;
		}

	}
	
	
}
