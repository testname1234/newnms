﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PcrConfiguration.Test.Helper
{
    
    public class PrintObj<TResponse, TRequest>
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string[] Headers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Method { get; set; }

       [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string RequestUrl { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TRequest PostBody { get; set; }

        [JsonProperty]
        public TResponse Response { get; set; }
    }
}
