﻿
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core;

namespace PcrConfiguration.Test.RepositoryBase
{
	[TestClass]
	public class DeviceRepositoryBaseTest
	{
		
		private IDeviceRepository _DeviceRepository;		

		[TestMethod]
		public void GetDevice()
		{
			 _DeviceRepository = IoC.Resolve<IDeviceRepository>("DeviceRepository");
			using(TransactionScope scope=new TransactionScope())
			{
				MockEntityDataGeneratorBase mockEntityGeneratorBase=new MockEntityDataGeneratorBase();
				Device expectedDevice=mockEntityGeneratorBase.GetMockDevice(true);
				Device actualDevice = _DeviceRepository.GetDevice(expectedDevice.DeviceId);
				Assert.IsNotNull(actualDevice);
			}
		}	
	}
	
	
}
