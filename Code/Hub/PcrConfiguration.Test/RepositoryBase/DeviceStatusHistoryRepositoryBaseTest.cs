﻿
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core;

namespace PcrConfiguration.Test.RepositoryBase
{
	[TestClass]
	public class DeviceStatusHistoryRepositoryBaseTest
	{
		
		private IDeviceStatusHistoryRepository _DeviceStatusHistoryRepository;		

		[TestMethod]
		public void GetDeviceStatusHistory()
		{
			 _DeviceStatusHistoryRepository = IoC.Resolve<IDeviceStatusHistoryRepository>("DeviceStatusHistoryRepository");
			using(TransactionScope scope=new TransactionScope())
			{
				MockEntityDataGeneratorBase mockEntityGeneratorBase=new MockEntityDataGeneratorBase();
				DeviceStatusHistory expectedDeviceStatusHistory=mockEntityGeneratorBase.GetMockDeviceStatusHistory(true);
				DeviceStatusHistory actualDeviceStatusHistory = _DeviceStatusHistoryRepository.GetDeviceStatusHistory(expectedDeviceStatusHistory.DeviceStatusHistoryId);
				Assert.IsNotNull(actualDeviceStatusHistory);
			}
		}	
	}
	
	
}
