﻿
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core;

namespace PcrConfiguration.Test.RepositoryBase
{
	[TestClass]
	public class DeviceStatusRepositoryBaseTest
	{
		
		private IDeviceStatusRepository _DeviceStatusRepository;		

		[TestMethod]
		public void GetDeviceStatus()
		{
			 _DeviceStatusRepository = IoC.Resolve<IDeviceStatusRepository>("DeviceStatusRepository");
			using(TransactionScope scope=new TransactionScope())
			{
				MockEntityDataGeneratorBase mockEntityGeneratorBase=new MockEntityDataGeneratorBase();
				DeviceStatus expectedDeviceStatus=mockEntityGeneratorBase.GetMockDeviceStatus(true);
				DeviceStatus actualDeviceStatus = _DeviceStatusRepository.GetDeviceStatus(expectedDeviceStatus.DeviceStatusId);
				Assert.IsNotNull(actualDeviceStatus);
			}
		}	
	}
	
	
}
