﻿
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core;

namespace PcrConfiguration.Test.RepositoryBase
{
	[TestClass]
	public class DeviceTypeRepositoryBaseTest
	{
		
		private IDeviceTypeRepository _DeviceTypeRepository;		

		[TestMethod]
		public void GetDeviceType()
		{
			 _DeviceTypeRepository = IoC.Resolve<IDeviceTypeRepository>("DeviceTypeRepository");
			using(TransactionScope scope=new TransactionScope())
			{
				MockEntityDataGeneratorBase mockEntityGeneratorBase=new MockEntityDataGeneratorBase();
				DeviceType expectedDeviceType=mockEntityGeneratorBase.GetMockDeviceType(true);
				DeviceType actualDeviceType = _DeviceTypeRepository.GetDeviceType(expectedDeviceType.DeviceTypeId);
				Assert.IsNotNull(actualDeviceType);
			}
		}	
	}
	
	
}
