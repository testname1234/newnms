﻿
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core;

namespace PcrConfiguration.Test.RepositoryBase
{
	[TestClass]
	public class GroupRepositoryBaseTest
	{
		
		private IGroupRepository _GroupRepository;		

		[TestMethod]
		public void GetGroup()
		{
			 _GroupRepository = IoC.Resolve<IGroupRepository>("GroupRepository");
			using(TransactionScope scope=new TransactionScope())
			{
				MockEntityDataGeneratorBase mockEntityGeneratorBase=new MockEntityDataGeneratorBase();
				Group expectedGroup=mockEntityGeneratorBase.GetMockGroup(true);
				Group actualGroup = _GroupRepository.GetGroup(expectedGroup.GroupId);
				Assert.IsNotNull(actualGroup);
			}
		}	
	}
	
	
}
