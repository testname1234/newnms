﻿using System;
using System.Net;
using System.Collections.Generic;
using System.IO;

namespace PcrConfiguration.Test.Helper
{
    public class WebProxyUtil
    {
        public HttpWebResponse PostRequest(string url, object entity, Dictionary<string, string> parameters, bool isJson = true, Dictionary<string, string> headerParameters = null)
        {
            return MakeRequest(url, "POST", entity, parameters, isJson,headerParameters);
        }

        public HttpWebResponse GetRequest(string url, Dictionary<string, string> parameters, bool isJson = true, Dictionary<string, string> headerParameters = null)
        {
            return MakeRequest(url, "GET", null, parameters, isJson,headerParameters);
        }
        public HttpWebResponse PutRequest(string url, object entity, Dictionary<string, string> parameters, bool isJson = true, Dictionary<string, string> headerParameters = null)
        {
            return MakeRequest(url, "PUT", entity, parameters, isJson, headerParameters);
        }
        public HttpWebResponse DeleteRequest(string url, Dictionary<string, string> parameters, bool isJson = true, Dictionary<string, string> headerParameters = null)
        {
            return MakeRequest(url, "DELETE", null, parameters, isJson, headerParameters);
        }


        public object MakeRequest<R>(string url, string method, object request, Dictionary<string, string> parameters, bool isJson = true, Dictionary<string, string> headerParameters = null)
        {
            HttpWebResponse resp = MakeRequest(url, method, request, parameters, isJson,headerParameters);
            if (resp != null)
            {
                using (Stream respStream = resp.GetResponseStream())
                {
                    if (respStream != null)
                    {
                        if (isJson)
                        {
                            R obj = JSONHelper.GetObject<R>(respStream);
                            return obj;
                        }
                        else
                        {
                            using (TextReader tr = new StreamReader(respStream))
                            {
                                return tr.ReadToEnd();
                            }
                        }
                    }
                }
            }
            return default(R);
        }

        private static HttpWebResponse MakeRequest(string url, string method, object request, Dictionary<string, string> parameters, bool isJson = true, Dictionary<string, string> headerParameters = null)
        {
            if (parameters != null && parameters.Count > 0)
            {
                int i = 0;
                foreach (string key in parameters.Keys)
                {
                    if (i == 0)
                    {
                        url += string.Format("?{0}={1}", key, parameters[key]);
                    }
                    else
                    {
                        url += string.Format("&{0}={1}", key, parameters[key]);
                    }
                    i++;
                }
            }

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
            if (isJson)
            {
                req.Accept = "text/json";
                if (method == "POST" || method=="PUT")
                    req.ContentType = "application/json; charset=UTF-8";
                else req.ContentType = "text/json; charset=UTF-8";
            }
            req.Method = method;
            if (headerParameters != null && headerParameters.Count > 0)
            {
                foreach (string key in headerParameters.Keys)
                {
                    req.Headers.Add(key, headerParameters[key]);
                }
            }


            try
            {
                if (request != null)
                {
                    using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                    {
                        streamWriter.Write(JSONHelper.GetString(request));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }
            }
            catch (WebException e) { return (HttpWebResponse)e.Response; }
            HttpWebResponse resp;
            try
            {
                resp = (HttpWebResponse)req.GetResponse();
            }
            catch (WebException e)
            {
                resp = (HttpWebResponse)e.Response;
            }
            return resp;
        }
    }
}
