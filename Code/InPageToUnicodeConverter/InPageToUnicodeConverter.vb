﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text.RegularExpressions
Imports System.Drawing

Public Class InPageToUnicodeConverter
    Dim my_targetFName_Sp As String

    Public Sub InitializeData()
        Try
            If ip2uc.Count = 0 Then
                initIp2ucCharacter()
            End If

            If cpi2u.Count = 0 Then
                init_cpinpage2unicode()
            End If

            If cpu2i.Count = 0 Then
                init_cpunicode2inpage()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ConvertToUnicode(sourceBinaryData As Byte()) As String

        Dim my_OutPut As String = " "
        Dim my_OutPut_Sp As String = " "

        InitializeData()

        Dim startP As Integer = 0
        Dim endP As Integer = sourceBinaryData.Length - 10

        For i = startP To endP
            If (sourceBinaryData(i) = 4) Then
                ' find hamza position
                If sourceBinaryData(i + 1) = 163 And FindHamzaPosition(sourceBinaryData, i) Then

                    my_OutPut += Convert.ToChar(1574).ToString   ' 1574 ئ ya   shift + 4
                    i += 1
                ElseIf sourceBinaryData(i + 1) = 165 And ((168 > sourceBinaryData(i + 3) And sourceBinaryData(i + 3) > 128) Or sourceBinaryData(i + 3) = 170 Or _
                     sourceBinaryData(i + 3) = 182 Or sourceBinaryData(i + 3) = 184 Or sourceBinaryData(i + 3) = 185 Or sourceBinaryData(i + 3) = 200 Or _
                     sourceBinaryData(i + 3) = 201) Then

                    my_OutPut += Convert.ToChar(1740).ToString  'bari-ya ے convert to  ٰ ی  
                    i += 1

                ElseIf sourceBinaryData(i + 1) = 161 And Not (sourceBinaryData(i + 3) = 32 Or sourceBinaryData(i + 2) = 13 _
                    Or (255 > sourceBinaryData(i + 3) And sourceBinaryData(i + 3) > 202) Or sourceBinaryData(i + 2) = 9 _
                      Or FindHamzaPosition(sourceBinaryData, i + 3) = False Or (198 > sourceBinaryData(i + 3) And sourceBinaryData(i + 3) > 167)) Then
                    my_OutPut += Convert.ToChar(1722).ToString  ' add space after ں  noon guna
                    my_OutPut += Convert.ToChar(32).ToString
                    i += 1
                ElseIf ((sourceBinaryData(i + 1) = 177) And (sourceBinaryData(i + 3) = 177)) Then
                    '  remove 1 jazam 
                    my_OutPut += Convert.ToChar(ip2uc.Item(Convert.ToInt32(sourceBinaryData(i + 1)))).ToString
                    i += 3
                ElseIf sourceBinaryData(i + 1) = 169 And Not sourceBinaryData(i + 3) = 169 Then
                    ' //  extra character  " tatbeeq "  pass this character 
                    my_OutPut += Convert.ToChar(ip2uc.Item(Convert.ToInt32(sourceBinaryData(i + 1)))).ToString
                    i += 1
                ElseIf sourceBinaryData(i + 1) = 166 And my_OutPut.Last = Convert.ToChar(1574).ToString Then
                    'remove ya-hamza  before hamza
                    my_OutPut = my_OutPut.Remove(my_OutPut.Length - 1, 1)
                    my_OutPut += Convert.ToChar(ip2uc.Item(166)).ToString
                    my_OutPut += Convert.ToChar(ip2uc.Item(191)).ToString
                    i += 1
                ElseIf sourceBinaryData(i + 1) = 253 Or sourceBinaryData(i + 1) = 254 Then
                    '  Change " 
                    If sourceBinaryData(i + 1) = 253 Then
                        my_OutPut += Convert.ToChar(ip2uc.Item(254)).ToString
                        i += 1
                    Else
                        my_OutPut += Convert.ToChar(ip2uc.Item(253)).ToString
                        i += 1
                    End If
                ElseIf sourceBinaryData(i + 1) = 224 Then
                    ' ... double  
                    my_OutPut += Convert.ToChar(ip2uc.Item(224)).ToString
                    my_OutPut += Convert.ToChar(ip2uc.Item(224)).ToString
                    i += 1
                ElseIf sourceBinaryData(i + 1) = 184 And Not (sourceBinaryData(i + 3) = 32 Or sourceBinaryData(i + 2) = 13 Or sourceBinaryData(i + 2) = 9 _
                       Or (255 > sourceBinaryData(i + 3) And sourceBinaryData(i + 3) > 202) Or (198 > sourceBinaryData(i + 3) And sourceBinaryData(i + 3) > 167)) Then
                    ' add space after ي
                    my_OutPut += Convert.ToChar(ip2uc.Item(184)).ToString
                    my_OutPut += Convert.ToChar(ip2uc.Item(32)).ToString
                    i += 1
                ElseIf ((sourceBinaryData(i + 1) = 162 Or sourceBinaryData(i + 1) = 182) And my_OutPut.Last = Convert.ToChar(1574).ToString) Then
                    'remove ya-hamza befor wao-haza or wao
                    my_OutPut = my_OutPut.Remove(my_OutPut.Length - 1, 1)
                    my_OutPut += Convert.ToChar(ip2uc.Item(182)).ToString
                    i += 1
                ElseIf (218 > sourceBinaryData(i + 1) And sourceBinaryData(i + 1) > 207) And ((218 > sourceBinaryData(i + 3) And sourceBinaryData(i + 3) > 207) _
                                                                                   Or (sourceBinaryData(i + 3) = 223 Or sourceBinaryData(i + 2) = 47)) Then
                    'ginti ;)                                                          

                    Dim temValue As String = ""
                    Do Until Not ((218 > sourceBinaryData(i + 1) And sourceBinaryData(i + 1) > 207) Or (sourceBinaryData(i + 1) = 223 Or sourceBinaryData(i) = 47))
                        If sourceBinaryData(i) = 47 Then
                            temValue += Convert.ToChar(47).ToString
                        Else
                            temValue += Convert.ToChar(ip2uc.Item(Convert.ToInt32(sourceBinaryData(i + 1)))).ToString
                        End If

                        If sourceBinaryData(i + 1) = 47 Or sourceBinaryData(i) = 47 Then
                            i += 1
                        Else
                            i += 2
                        End If

                    Loop
                    my_OutPut += ChangePositon(temValue)
                    i -= 1
                Else
                    my_OutPut += Convert.ToChar(ip2uc.Item(Convert.ToInt32(sourceBinaryData(i + 1)))).ToString
                    i += 1
                End If


            ElseIf sourceBinaryData(i) = 32 Then
                my_OutPut += Convert.ToChar(32)
            ElseIf sourceBinaryData(i) = 13 Then
                my_OutPut += Convert.ToChar(13)
                my_OutPut += Convert.ToChar(10)
                i += 3
            ElseIf sourceBinaryData(i) = 9 Then
                my_OutPut += Convert.ToChar(9)
            ElseIf 64 > sourceBinaryData(i) And sourceBinaryData(i) > 32 Then

                If (58 > sourceBinaryData(i) And sourceBinaryData(i) > 47) And sourceBinaryData(i + 1) = 32 Then

                    Dim boolChkEnter As Boolean = False
                    Dim my_tempVar As String = ""
                    Do Until Not ((58 > sourceBinaryData(i) And sourceBinaryData(i) > 47) Or sourceBinaryData(i) = 32) ' Or sourceBinaryData(i) = 47)
                        boolChkEnter = True
                        my_tempVar += Convert.ToChar(sourceBinaryData(i)).ToString
                        i += 1
                    Loop
                    my_OutPut += ChangePositon(my_tempVar)

                    If boolChkEnter Then
                        i -= 1
                    End If

                Else
                    my_OutPut += Convert.ToChar(sourceBinaryData(i)).ToString
                End If

            ElseIf 256 > sourceBinaryData(i) And sourceBinaryData(i) > 32 Then
                my_OutPut += Convert.ToChar(sourceBinaryData(i)).ToString
            End If
        Next i

        Dim removeSpaces() As String = {"ا", "د", "ڈ", "ذ", "ر", "ڑ", "ز", "ژ", "و", "آ", "أ", "ؤ"}
        my_OutPut_Sp = my_OutPut

        For i = 0 To removeSpaces.Length - 1
            my_OutPut_Sp = Replace(my_OutPut_Sp, removeSpaces(i) & " ", removeSpaces(i))
        Next i
        my_OutPut = Regex.Replace(my_OutPut, "[ ]+[ ]", " ")

        Return my_OutPut

    End Function

    Public Function Inpage2Unicode(ipString As String) As String

        InitializeData()

        Dim ipChar() As Char = ipString
        Dim ipLength As Integer = ipString.Length - 1
        Dim ipByte(ipLength) As Int32
        Dim ucOutput As String = " "

        For i = 0 To ipLength Step 1
            ipByte(i) = Convert.ToInt32(ipChar(i))
        Next
        Try


            For i = 0 To ipByte.Length - 1
                If (ipByte(i) = 4) Then
                    ' find hamza position
                    If ipByte(i + 1) = 163 And FindHamzaPositionI2U(ipByte, i) Then

                        ucOutput += Convert.ToChar(1574).ToString   ' 1574 ئ ya   shift + 4
                        i += 1

                    ElseIf (i + 3 < ipLength) And (ipByte(i + 1) = 165 And ((167 > ipByte(i + 3) And ipByte(i + 3) > 128) Or _
                                                    (750 > ipByte(i + 3) And ipByte(i + 3) > 338) Or _
                                                    (8485 > ipByte(i + 3) And ipByte(i + 3) > 8210) Or ipByte(i + 3) = 170 _
                          Or ipByte(i + 3) = 182 Or ipByte(i + 3) = 184 Or ipByte(i + 3) = 185 Or ipByte(i + 3) = 200 Or ipByte(i + 3) = 201)) Then

                        ucOutput += Convert.ToChar(1740).ToString  'bari-ya ے convert to  ٰ ی  
                        i += 1

                    ElseIf (i + 3 < ipLength) And (ipByte(i + 1) = 161 And Not (ipByte(i + 3) = 32 Or ipByte(i + 2) = 13 _
                      Or (255 > ipByte(i + 3) And ipByte(i + 3) > 202) Or ipByte(i + 2) = 9 _
                      Or (198 > ipByte(i + 3) And ipByte(i + 3) > 167))) Then
                        ucOutput += Convert.ToChar(1722).ToString  ' add space after ں  noon guna
                        ucOutput += Convert.ToChar(32).ToString
                        i += 1
                    ElseIf (i + 3 < ipLength) And ((ipByte(i + 1) = 177) And (ipByte(i + 3) = 177)) Then
                        '  remove 1 jazam 
                        ucOutput += Convert.ToChar(cpi2u.Item(Convert.ToInt32(ipByte(i + 1)))).ToString
                        i += 3
                    ElseIf ipByte(i + 1) = 166 And ucOutput.Last = Convert.ToChar(1574).ToString Then
                        'remove ya-hamza  before hamza
                        ucOutput = ucOutput.Remove(ucOutput.Length - 1, 1)
                        ucOutput += Convert.ToChar(cpi2u.Item(166)).ToString
                        ucOutput += Convert.ToChar(cpi2u.Item(191)).ToString
                        i += 1
                    ElseIf ipByte(i + 1) = 253 Or ipByte(i + 1) = 254 Then
                        '  Change " 
                        If ipByte(i + 1) = 253 Then
                            ucOutput += Convert.ToChar(cpi2u.Item(254)).ToString
                            i += 1
                        Else
                            ucOutput += Convert.ToChar(cpi2u.Item(253)).ToString
                            i += 1
                        End If
                    ElseIf ipByte(i + 1) = 224 Then
                        ' ... double  
                        ucOutput += Convert.ToChar(cpi2u.Item(224)).ToString
                        ucOutput += Convert.ToChar(cpi2u.Item(224)).ToString
                        i += 1
                    ElseIf (i + 3 < ipLength) And (ipByte(i + 1) = 184 And Not (ipByte(i + 3) = 32 Or ipByte(i + 2) = 13 Or ipByte(i + 2) = 9 _
                                Or (255 > ipByte(i + 3) And ipByte(i + 3) > 202) Or (198 > ipByte(i + 3) And ipByte(i + 3) > 167))) Then

                        ' add space after ي
                        ucOutput += Convert.ToChar(cpi2u.Item(184)).ToString
                        ucOutput += Convert.ToChar(cpi2u.Item(32)).ToString
                        i += 1
                    ElseIf ((ipByte(i + 1) = 162 Or ipByte(i + 1) = 182) And ucOutput.Last = Convert.ToChar(1574).ToString) Then
                        'remove ya-hamza befor wao-haza or wao
                        ucOutput = ucOutput.Remove(ucOutput.Length - 1, 1)
                        ucOutput += Convert.ToChar(cpi2u.Item(182)).ToString
                        i += 1
                    ElseIf (i + 3 < ipLength) And ((218 > ipByte(i + 1) And ipByte(i + 1) > 207) And ((218 > ipByte(i + 3) And ipByte(i + 3) > 207) _
                                                                                   Or (ipByte(i + 3) = 223 Or ipByte(i + 2) = 47))) Then
                        'ginti ;)                                                          

                        Dim temValue As String = ""
                        Do Until Not ((218 > ipByte(i + 1) And ipByte(i + 1) > 207) Or (ipByte(i + 1) = 223 Or ipByte(i) = 47))
                            If ipByte(i) = 47 Then
                                temValue += Convert.ToChar(47).ToString
                            Else
                                temValue += Convert.ToChar(cpi2u.Item(Convert.ToInt32(ipByte(i + 1)))).ToString
                            End If

                            If ipByte(i + 1) = 47 Or ipByte(i) = 47 Then
                                i += 1
                            Else
                                i += 2
                            End If

                        Loop

                        ucOutput += ChangePositon(temValue)
                        i -= 1
                    Else
                        ucOutput += Convert.ToChar(cpi2u.Item(Convert.ToInt32(ipByte(i + 1)))).ToString
                        i += 1
                    End If


                ElseIf ipByte(i) = 32 Then
                    ucOutput += Convert.ToChar(32)
                ElseIf ipByte(i) = 13 Then
                    ucOutput += Convert.ToChar(13)
                    ucOutput += Convert.ToChar(10)
                    i += 1
                ElseIf ipByte(i) = 9 Then
                    ucOutput += Convert.ToChar(9)
                ElseIf 64 > ipByte(i) And ipByte(i) > 32 Then

                    If (58 > ipByte(i) And ipByte(i) > 47) And ipByte(i + 1) = 32 Then

                        Dim boolChkEnter As Boolean = False
                        Dim my_tempVar As String = ""
                        Do Until Not ((58 > ipByte(i) And ipByte(i) > 47) Or ipByte(i) = 32) ' Or ipByte(i) = 47)
                            boolChkEnter = True
                            my_tempVar += Convert.ToChar(ipByte(i)).ToString
                            i += 1
                        Loop

                        ucOutput += ChangePositon(my_tempVar)

                        If boolChkEnter Then
                            i -= 1
                        End If

                    Else
                        ucOutput += Convert.ToChar(ipByte(i)).ToString
                    End If

                ElseIf 127 > ipByte(i) And ipByte(i) > 32 Then
                    ucOutput += Convert.ToChar(ipByte(i)).ToString
                End If
            Next
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

        ucOutput = Regex.Replace(ucOutput, "[ ]+[ ]", " ")

        Return ucOutput
    End Function

    Private Function FindHamzaPositionI2U(ByVal ipByte As Int32(), ByVal i As Int32) As Boolean
        Try
            If ipByte(i + 3) <> 32 And Not ipByte(i + 2) = 13 Then

                If (ipByte(i + 3) = 170 Or ipByte(i + 3) = 171 Or ipByte(i + 3) = 172 _
                 Or ipByte(i + 3) = 176 Or ipByte(i + 3) = 177 Or ipByte(i + 3) = 168 Or ipByte(i + 3) = 181 _
                  Or ipByte(i + 3) = 173 Or ipByte(i + 3) = 180 Or ipByte(i + 3) = 179 Or ipByte(i + 3) = 169) Then

                    If Not (ipByte(i + 4) = 13 Or ipByte(i + 5) = 32) Then

                        Return True
                        Exit Function

                    Else
                        Return False
                        Exit Function
                    End If
                ElseIf (186 < ipByte(i + 3) And ipByte(i + 3) < 255) Then
                    Return False
                    Exit Function
                Else
                    Return True
                    Exit Function
                End If
            Else
                Return False
                Exit Function
            End If
        Catch ex As Exception

        End Try

    End Function

    Private Function FindHamzaPosition(sourceBinaryData As Byte(), ByVal i As Int32) As Boolean
        If sourceBinaryData(i + 3) <> 32 And Not sourceBinaryData(i + 2) = 13 Then

            If (sourceBinaryData(i + 3) = 170 Or sourceBinaryData(i + 3) = 171 Or sourceBinaryData(i + 3) = 172 _
             Or sourceBinaryData(i + 3) = 176 Or sourceBinaryData(i + 3) = 177 Or sourceBinaryData(i + 3) = 168 Or sourceBinaryData(i + 3) = 181 _
              Or sourceBinaryData(i + 3) = 173 Or sourceBinaryData(i + 3) = 180 Or sourceBinaryData(i + 3) = 179 Or sourceBinaryData(i + 3) = 169) Then

                If Not (sourceBinaryData(i + 4) = 13 Or sourceBinaryData(i + 5) = 32) Then

                    Return True
                    Exit Function

                Else
                    Return False
                    Exit Function
                End If
            ElseIf (186 < sourceBinaryData(i + 3) And sourceBinaryData(i + 3) < 255) Then
                Return False
                Exit Function
            Else
                Return True
                Exit Function
            End If
        Else
            Return False
            Exit Function
        End If
    End Function

    Private Sub Remove_Spaces(ByRef my_OutPut As String, ByRef my_OutPut_Sp As String)
        Dim removeSpaces() As String = {"ا", "د", "ڈ", "ذ", "ر", "ڑ", "ز", "ژ", "و", "آ", "أ", "ؤ"}
        my_OutPut_Sp = my_OutPut

        For i = 0 To removeSpaces.Length - 1
            my_OutPut_Sp = Replace(my_OutPut_Sp, removeSpaces(i) & " ", removeSpaces(i))
        Next i
        my_OutPut = Regex.Replace(my_OutPut, "[ ]+[ ]", " ")
        Try
            System.IO.File.WriteAllText(my_targetFName_Sp, my_OutPut_Sp, System.Text.Encoding.UTF8)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Function Handle() As IntPtr
        Throw New NotImplementedException
    End Function

End Class
