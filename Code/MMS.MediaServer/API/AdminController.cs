﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using MS.Core;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Bucket;
using MS.Core.Entites;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using MS.Service;
using MS.MediaStorage;
using MS.Web.Models;
using NMS.Core.Helper;
using NMS.Core.Models;


namespace MS.Web.API
{
    public class AdminController : ApiController
    {
        IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
        IBucketCredentialsService bucketcredentialsService = IoC.Resolve<IBucketCredentialsService>("BucketCredentialsService");
        IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");


        [HttpPost]
        [ActionName("Login")]
        public DataTransfer<BucketCredentials> Login(MS.Core.DataTransfer.BucketCredentials.PostInput input)
        {

            DataTransfer<BucketCredentials> transfer = new DataTransfer<BucketCredentials>();
            try
            {
                transfer.IsSuccess = true;
                transfer.Data =  bucketcredentialsService.GetBucketByLoginIdAndApiKey(input.LoginId, input.ApiKey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("UserBySessionKey")]
        public MMS.Integration.DataTransfer<MMS.Integration.UserManagement.DataTransfer.UserOutput> UserBySessionKey(string id)
        {
            MMS.Integration.DataTransfer<MMS.Integration.UserManagement.DataTransfer.UserOutput> result = new MMS.Integration.DataTransfer<MMS.Integration.UserManagement.DataTransfer.UserOutput>();
            if (!string.IsNullOrEmpty(id))
            {
                int dec = Convert.ToInt32(EncryptionHelper.Decrypt(id));
                string url = ConfigurationManager.AppSettings["MMSUsermanagementUrl"] + "api/admin/UserByUserId?id=" + dec;
                HttpWebRequestHelper helper = new HttpWebRequestHelper();
                result = helper.GetRequest<MMS.Integration.DataTransfer<MMS.Integration.UserManagement.DataTransfer.UserOutput>>(url, null);
                //var res = helper.GetRequest(url, null, true);
            }
            else
            {
                result.IsSuccess = false;
            }
            return result;
                
        }
        [HttpPost]
        [ActionName("LoginMMS")]
        public DataTransfer<MMSLoadInitialDataLoginOutput> LoginMMS(NMS.Core.DataTransfer.User.PostInput input)
        {
          
            DataTransfer<MMSLoadInitialDataLoginOutput> transfer = new DataTransfer<MMSLoadInitialDataLoginOutput>();

            try
            {
                if (input != null)
                {

                    HttpWebRequestHelper helper = new HttpWebRequestHelper();

                    MMSLoadInitialDataLoginInput mmsInput = new MMSLoadInitialDataLoginInput();
                    mmsInput.LoginId = input.Login;
                    mmsInput.Password = input.Password;
                    DataTransfer<MMSLoadInitialDataLoginOutput> output = helper.PostRequest<DataTransfer<MMSLoadInitialDataLoginOutput>>( ConfigurationManager.AppSettings["MMSUsermanagementUrl"] + "api/admin/Authenticate", mmsInput, null);
         
                    if (output != null && output.IsSuccess == true)
                    {
                        transfer.Data = output.Data;

                        //Session["User"] = "";
                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { output.Errors[0].ToString() + "" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

    }
}
