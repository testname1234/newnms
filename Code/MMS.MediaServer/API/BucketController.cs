﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using MS.Core;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Bucket;
using MS.Core.Entites;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using MS.MediaStorage;
using MS.Web.Models;
using NMS.Core.Helper;
using MS.Core.Models;

namespace MS.Web.API
{
    public class BucketController : ApiController
    {
        IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
        IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");

        #region Bucket Configuration
        [HttpGet]
        [ActionName("CreateSubBucket")]
        public DataTransfer<bool> CreateSubBucket(int bucketId, string apiKey, string name)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            Storage storage = new Storage(bucketId, apiKey);

            try
            {
                transfer.IsSuccess = true;
                transfer.Data = storage.CreateSubBucket(name);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                transfer.IsSuccess = false;
                transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("DeleteSubBucket")]
        public DataTransfer<bool> DeleteSubBucket(int bucketId, string apiKey, string name)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            Storage storage = new Storage(bucketId, apiKey);

            try
            {
                transfer.IsSuccess = true;
                transfer.Data = storage.DeleteSubBucket(name);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetAllSubBucket")]
        public DataTransfer<List<Bucket>> GetAllSubBucket(int bucketId, string apiKey)
        {
            DataTransfer<List<Bucket>> transfer = new DataTransfer<List<Bucket>>();
            Storage storage = new Storage(bucketId, apiKey);

            try
            {
                transfer.IsSuccess = true;
                transfer.Data = storage.GetAllSubBucket();
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetAllBucket")]
        public DataTransfer<List<FolderModel>> GetAllBucket(int id)
        {
            DataTransfer<List<FolderModel>> transfer = new DataTransfer<List<FolderModel>>();
            
            try
            {
                transfer.IsSuccess = true;
                transfer.Data = bucketService.GetAllBucketWithParentName(id);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetSubBucketResources")]
        public DataTransfer<List<ResourceModel>> GetSubBucketResources(int id)
        {
            DataTransfer<List<ResourceModel>> transfer = new DataTransfer<List<ResourceModel>>();

            try
            {
                transfer.IsSuccess = true;
                transfer.Data = bucketService.GetSubBucketResources(id);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetSubBucket")]
        public DataTransfer<bool> GetSubBucket(int bucketId, string apiKey, string name)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            Storage storage = new Storage(bucketId, apiKey);

            try
            {
                transfer.Data = true;
                transfer.IsSuccess = true;
                if (storage.GetSubBucket(name) == null)
                    transfer.Data = false;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
            }

            return transfer;
        }



        #endregion

        [HttpGet]
        [ActionName("GetFile")]
        public HttpResponseMessage GetFile(string Bucket, string FilePath)
        {

            try
            {
                Bucket bucket = bucketService.GetBucketByName(Bucket);
                if (bucket != null)
                {
                    Resource resource = resourceService.GetResourceByFilePath(FilePath, bucket.BucketId);
                    if (resource != null && (resource.ResourceStatus == ResourceStatuses.Completed || (!string.IsNullOrEmpty(resource.Source) && resource.ResourceStatus == ResourceStatuses.Uploading)))
                    {

                        string resourcePath = string.Empty;
                        Format format = new Format();
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            Bucket ParentBucket = bucketService.GetBucket(Convert.ToInt32(resource.BucketId));

                            resourcePath = "\\\\" + server.ServerIp + "\\" + ParentBucket.Name + "\\" + resource.FilePath;
                            format = LocalCache.Cache.Formats.Where(x => x.Format == Path.GetExtension(resourcePath)).FirstOrDefault();
                            if (!string.IsNullOrEmpty(resourcePath))
                            {
                                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                                byte[] resourceByteArray;
                                bool isPartial = false;
                                RangeItemHeaderValue range = null;
                                long totalLength = 0;
                                if (Request.Headers.Date.HasValue && Request.Headers.Date.Value == resource.LastUpdateDate)
                                    return new HttpResponseMessage(HttpStatusCode.NotModified);
                                if (Request.Headers.Range != null && Request.Headers.Range.Ranges != null && Request.Headers.Range.Ranges.Count() > 0 && Request.Headers.Range.Ranges.First().From > 0)
                                {
                                    response = new HttpResponseMessage(HttpStatusCode.PartialContent);
                                    range = Request.Headers.Range.Ranges.First();
                                    if (range.From.HasValue && range.To.HasValue)
                                    {
                                        resourceByteArray = FileHelper.GetPartialFile(resourcePath, (int)range.From.Value, (int)range.To.Value, out totalLength);
                                    }
                                    else
                                    {
                                        resourceByteArray = FileHelper.GetImageFromCache(resourcePath);
                                        totalLength = resourceByteArray.Length;
                                    }
                                    isPartial = true;
                                }
                                else
                                    resourceByteArray = FileHelper.GetImageFromCache(resourcePath);
                                MemoryStream dataStream = new MemoryStream(resourceByteArray);
                                response.Content = new StreamContent(dataStream);
                                response.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddDays(30));
                                response.Headers.CacheControl = new CacheControlHeaderValue();
                                response.Headers.CacheControl.NoCache = false;
                                response.Headers.CacheControl.MaxAge = TimeSpan.FromDays(30);
                                response.Headers.CacheControl.Public = true;
                                response.Headers.Date = resource.LastUpdateDate;
                                if (range != null)
                                {
                                    response.Content.Headers.ContentRange = new ContentRangeHeaderValue(range.From.Value, range.To.HasValue ? range.To.Value : resourceByteArray.Length - 1, totalLength);
                                    //response.Headers.ETag = new EntityTagHeaderValue("\"a\"");
                                }
                                response.Headers.AcceptRanges.Add("bytes");

                                // if (!string.IsNullOrEmpty(resource.FileName))
                                //     response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = resource.FileName };


                                if (format != null)
                                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(format.MimeType);

                                // Console.WriteLine("Resource Request Time: {0}ms", (DateTime.UtcNow - dt).TotalMilliseconds);
                                return response;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                throw;
            }
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }


        [HttpGet]
        [ActionName("GetAllFiles")]
        public DataTransfer<List<MediaFile>> GetAllFiles(int BucketId, string FilePath)
        {
            DataTransfer<List<MediaFile>> transfer = new DataTransfer<List<MediaFile>>();
            try
            {
                List<MediaFile> files = new List<MediaFile>();
                List<Resource> resources = resourceService.GetAllResourcesByFilePath(FilePath, BucketId);
                foreach (Resource item in resources)
                {
                    MediaFile file = new MediaFile();
                    file.FilePath = item.FilePath;
                    file.FileName = item.FileName;
                    file.BucketId = Convert.ToInt32(item.BucketId);
                    files.Add(file);
                }
                transfer.IsSuccess = true;
                transfer.Data = files;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex)); 
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetResourcesByResourceTypeId")]
        public DataTransfer<List<Resource>> GetResourcesByResourceTypeId(int ResourceTypeId)
        {
            DataTransfer<List<Resource>> transfer = new DataTransfer<List<Resource>>();
            try
            {
                List<Resource> resources = resourceService.GetResourceByResourceTypeId(ResourceTypeId);

                transfer.IsSuccess = true;
                transfer.Data = resources;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex)); 
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("PostResource")]
        public Core.DataTransfer.Resource.PostOutput PostResource(Core.DataTransfer.Resource.PostInput input)
        {
            Core.DataTransfer.Resource.PostOutput output = new Core.DataTransfer.Resource.PostOutput();
            try
            {
                Storage storage = new Storage(input.BucketId, input.ApiKey);
                Resource res = storage.AddFile(input);
               
                output.CopyFrom(res);
            }
            catch (Exception ex)
            { 
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                throw;
            }
            return output;
        }


        [HttpPost]
        [ActionName("PostAllAtOnce")]
        public Core.DataTransfer.Resource.PostOutput PostAllAtOnce(HttpContext context)
        {
            Core.DataTransfer.Resource.PostOutput output = new Core.DataTransfer.Resource.PostOutput();
            try
            {
                if (context != null)
                {
                    Core.DataTransfer.Resource.PostInput input = new Core.DataTransfer.Resource.PostInput();
                    input.BucketId = Convert.ToInt32(context.Request.Headers["BucketId"]);
                    input.ApiKey = context.Request.Headers["ApiKey"];
                    input.Source = context.Request.Headers["Source"];
                    input.FilePath = context.Request.Headers["FilePath"];
                    input.FileName = context.Request.Headers["FileName"];
                    input.Caption = context.Request.Headers["Caption"]; ;
                    input.SourceId = context.Request.Headers["SourceId"];
                    input.SourceName = context.Request.Headers["SourceName"];
                    input.SourceTypeId = context.Request.Headers["SourceTypeId"];
                    var file = context.Request.Files[0];

                    Storage storage = new Storage(input.BucketId, input.ApiKey);
                    Resource res = storage.AddFile(input);
                    if (res != null)
                    {
                        resourceService.CopyFilesOnServerAndTempFolder(res, file);
                    }
                }
                
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                throw;
            }
            return output;
        }


        [HttpPost]
        [ActionName("PostMedia")]
        public Task<HttpResponseMessage> PostMedia(string ApiKey, int BucketId, string FilePath, string Source, string Id = null, bool isHd = true)
        {
            MS.Core.DataTransfer.Resource.PostInput input = new Core.DataTransfer.Resource.PostInput();
            try
            {
                input.BucketId = BucketId;
                input.ApiKey = ApiKey;
                input.FilePath = FilePath.Replace('/', '\\');
                input.Source = Source;
                input.FileName = input.FilePath.Replace('/', '\\');
                input.FileName = input.FilePath.Substring(input.FileName.LastIndexOf('\\') + 1, (input.FileName.Length) - (input.FileName.LastIndexOf('\\') + 1));

                Storage storage = new Storage(input.BucketId, input.ApiKey);
                Bucket bucket = bucketService.GetBucket(input.BucketId);

                Resource resource = null;
                Guid guid = new Guid();
                if (Guid.TryParse(Id, out guid))
                {
                    resource = resourceService.GetResourceByGuidId(guid);
                    resource.FileName = input.FileName;
                    resource.FilePath = input.FilePath;
                    resourceService.UpdateResource(resource);
                }
                else
                {
                    resource = resourceService.GetResourceByFilePath(input.FilePath, input.BucketId);
                }

                if (resource == null)
                {
                    resource = storage.AddFile(input);
                }

                HttpRequestMessage request = this.Request;
                Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                if (server != null)
                {
                    string root = "\\" + bucket.Path + "\\" + input.FilePath.Replace('/', '\\').Substring(0, input.FilePath.LastIndexOf('\\') + 1);
                    // input.FileName = input.FilePath.Substring(input.FilePath.LastIndexOf('\\') + 1, (input.FilePath.Length) - (input.FilePath.LastIndexOf('\\') + 1));
                    if (!request.Content.IsMimeMultipartContent())
                    {
                        if (HttpContext.Current.Request.Files.Count > 0)
                        {
                            var file = HttpContext.Current.Request.Files[0];
                            string ext = System.IO.Path.GetExtension(file.FileName);
                            string mimeType = file.ContentType;
                            Format format = null;
                            format = LocalCache.Cache.Formats.Where(x => x.MimeType == mimeType).FirstOrDefault();
                            file.SaveAs("\\\\" + server.ServerIp + (isHd ? ConfigurationManager.AppSettings["MediaLocation"] : ConfigurationManager.AppSettings["MediaLocationLowRes"]) + root);
                            storage.SaveResource(resource, format, mimeType, ext, null, null, root, input.IsHd, server.ServerIp);
                        }
                    }
                    else
                    {
                        string serverSavePath = "\\\\" + server.ServerIp + (isHd ? ConfigurationManager.AppSettings["MediaLocation"] : ConfigurationManager.AppSettings["MediaLocationLowRes"]) + root;
                        var provider = new CustomMultipartFormDataStreamProvider(serverSavePath);

                        var task = request.Content.ReadAsMultipartAsync(provider).
                            ContinueWith<HttpResponseMessage>(o =>
                            {
                                var fileData = provider.FileData.First();
                                string file1 = fileData.LocalFileName;
                                string ext = System.IO.Path.GetExtension(fileData.Headers.ContentDisposition.FileName.Trim('"'));
                                string mimeType = fileData.Headers.ContentType.MediaType;
                                Format format = LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                storage.SaveResource(resource, format, mimeType, ext, file1, serverSavePath + input.FileName.Replace('/', '\\'), root, isHd, server.ServerIp);
                                return new HttpResponseMessage(HttpStatusCode.OK)
                                {
                                    Content = new StringContent("File uploaded.")
                                };
                            }
                        );
                        return task;
                    }
                }

            }
            catch(Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                throw;
            }

            return Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            );
        }

       

    }
}
