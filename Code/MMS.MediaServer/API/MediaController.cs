﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using MS.Core;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using NMS.Core.Helper;

namespace MS.Web.API
{
    public class MediaController : ApiController
    {
        IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
        IFormatService formatService = IoC.Resolve<IFormatService>("FormatService");
        public HttpResponseMessage Get(string Id, bool IsHD)
        {
            Guid guid = new Guid();
            if (Guid.TryParse(Id, out guid))
            {
                DateTime dt = DateTime.UtcNow;
                Resource resource = resourceService.GetResourceByGuidId(guid);
                if (resource != null && resource.ResourceStatus == ResourceStatuses.Completed)
                {
                    string resourcePath = string.Empty;
                    Format format = new Format();
                    if (IsHD)
                    {
                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.HighResolutionFormatId).First();
                        resourcePath = resource.HighResolutionFile + "." + format.Format;
                    }
                    else
                    {
                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.LowResolutionFormatId).First();
                        resourcePath = resource.LowResolutionFile + "." + format.Format;
                    }
                    byte[] resourceByteArray = ImageHelper.GetImageFromCache(resourcePath);

                    MemoryStream dataStream = new MemoryStream(resourceByteArray);
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new StreamContent(dataStream);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(format.MimeType);
                    Console.WriteLine("Resource Request Time: {0}ms", (DateTime.UtcNow - dt).TotalMilliseconds);
                    return response;
                }
            }
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [ActionName("PostResource")]
        public MS.Core.DataTransfer.Resource.PostOutput PostResource(MS.Core.DataTransfer.Resource.PostInput input)
        {
            Resource res = new Resource();
            res.CopyFrom((object)input);
            res.Guid = Guid.NewGuid();
            res.CreationDate = DateTime.UtcNow;
            res.LastUpdateDate = res.CreationDate;
            res.IsActive = true;
            res.ResourceStatus = ResourceStatuses.Uploading;
            res = resourceService.InsertResource(res);
            MS.Core.DataTransfer.Resource.PostOutput output = new Core.DataTransfer.Resource.PostOutput();
            output.CopyFrom(res);
            return output;
        }

        [HttpPost]
        [ActionName("PostMedia")]
        public Task<HttpResponseMessage> PostMedia(string Id,bool IsHD)
        {
            Guid guid = new Guid();
            if (Guid.TryParse(Id, out guid))
            {
                Resource resource = resourceService.GetResourceByGuidId(guid);
                if (resource != null)
                {
                    HttpRequestMessage request = this.Request;
                    if (!request.Content.IsMimeMultipartContent())
                    {
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }

                    string root = ConfigurationManager.AppSettings["ImageLocation"];
                    var provider = new CustomMultipartFormDataStreamProvider(root);

                    var task = request.Content.ReadAsMultipartAsync(provider).
                        ContinueWith<HttpResponseMessage>(o =>
                        {

                            string file1 = provider.FileData.First().LocalFileName;
                            string ext = System.IO.Path.GetExtension(file1);
                            string mimeType = request.Content.Headers.ContentType.MediaType.ToLower();
                            Format format = LocalCache.Cache.Formats.Where(x => x.MimeType == mimeType).FirstOrDefault();
                            if (format == null)
                            {
                                format = new Format();
                                format.Format = ext;
                                format.MimeType = mimeType;
                                format = formatService.InsertFormat(format);
                                LocalCache.Cache.UpdateFormats();
                            }
                            if (IsHD)
                            {
                                resource.HighResolutionFile = root + "/" + resource.Guid.ToString() + "." + ext;
                                resource.HighResolutionFormatId = format.FormatId;
                            }
                            else
                            {
                                resource.LowResolutionFile = root + "/" + resource.Guid.ToString() + "." + ext;
                                resource.LowResolutionFormatId = format.FormatId;
                            }

                            File.Move(file1, root + "/" + resource.Guid.ToString() + "." + ext);
                            resource.LastUpdateDate = DateTime.UtcNow;
                            resource.ResourceStatus = ResourceStatuses.Completed;
                            resourceService.UpdateResource(resource);
                            return new HttpResponseMessage(HttpStatusCode.OK)
                            {
                                Content = new StringContent("File uploaded.")
                            };
                        }
                    );
                    return task;
                }
            }
            return Task<HttpResponseMessage>.Factory.StartNew(() =>
                        {
                            return new HttpResponseMessage(HttpStatusCode.NoContent);
                        }
                    );
        }
    }
}

