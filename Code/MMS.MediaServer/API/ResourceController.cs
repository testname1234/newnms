﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using FFMPEGLib;
using MS.Core;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.Helper;
using MS.Core.IService;
using MS.Core.Models;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using System.Web.Http.Cors;
using Elmah.Mvc;
using System.Threading;
using System.Drawing;
using System.Web.Script.Serialization;
using MS.Web.Models;

namespace MS.Web.API
{
    [EnableCors("*", "*", "*")]
    public class ResourceController : ApiController
    {
        IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
        IResourceDetailService resourcedetailService = IoC.Resolve<IResourceDetailService>("ResourceDetailService");
        IFormatService formatService = IoC.Resolve<IFormatService>("FormatService");
        IResourceMetaService resourceMetaService = IoC.Resolve<IResourceMetaService>("ResourceMetaService");
        IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
        IUserFavouritesService userfavouriteservice = IoC.Resolve<IUserFavouritesService>("UserFavouritesService");
        public string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
        IResourceLogService resourceLogService = IoC.Resolve<IResourceLogService>("ResourceLogService");
        public string fullRootPath = "";


        [HttpGet]
        [ActionName("GetResource")]
        public HttpResponseMessage GetResource(string id, bool IsHD = false, bool Thumb = false)
        {

            Guid guid = new Guid();
              Stream dataStream = null;
            try
            {
                if (Guid.TryParse(id, out guid))
                {
                    DateTime dt = DateTime.UtcNow;
                    Resource resource = resourceService.GetResourceByGuidId(guid);
                    if (Thumb && resource.ResourceTypeId != (int)ResourceTypes.Image)
                    {
                        return GetThumb(id);
                    }

                    if (resource != null && (resource.ResourceStatus == ResourceStatuses.Completed || (!string.IsNullOrEmpty(resource.Source) && (resource.ResourceStatus == ResourceStatuses.Uploading || resource.ResourceStatus == ResourceStatuses.TempFile))))
                    {
                        string resourcePath = string.Empty;
                        Format format = new Format();
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            if (resource.BucketId != null && resource.BucketId > 0 && !string.IsNullOrEmpty(resource.FilePath))
                            {
                                Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);

                                if (IsHD)
                                {
                                    if (!string.IsNullOrEmpty(resource.HighResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.HighResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocation"] + "\\" + bucket.Path + "\\" + resource.HighResolutionFile;
                                    }
                                    else if (!string.IsNullOrEmpty(resource.Source))
                                    {
                                        resourcePath = string.Empty;
                                    }
                                    else if (!string.IsNullOrEmpty(resource.LowResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.LowResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationLowRes"] + "\\" + bucket.Path + "\\" + resource.LowResolutionFile;
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(resource.LowResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.LowResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationLowRes"] + "\\" + bucket.Path + "\\" + resource.LowResolutionFile;
                                    }
                                    else if (!string.IsNullOrEmpty(resource.HighResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.HighResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocation"] + "\\" + bucket.Path + "\\" + resource.HighResolutionFile;
                                    }
                                }
                            }
                            else
                            {
                                if (IsHD)
                                {
                                    if (!string.IsNullOrEmpty(resource.HighResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.HighResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + resource.HighResolutionFile;
                                    }
                                    else if (!string.IsNullOrEmpty(resource.Source))
                                    {
                                        resourcePath = string.Empty;
                                    }
                                    else if (!string.IsNullOrEmpty(resource.LowResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.LowResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + resource.LowResolutionFile;
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(resource.LowResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.LowResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + resource.LowResolutionFile;
                                    }
                                    else if (!string.IsNullOrEmpty(resource.HighResolutionFile))
                                    {
                                        format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.HighResolutionFormatId).First();
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + resource.HighResolutionFile;
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(resourcePath))
                            {
                                resourcePath = resource.Source;
                                format = LocalCache.Cache.Formats.Where(x => x.Format == Path.GetExtension(resourcePath)).FirstOrDefault();
                            }
                            if (!string.IsNullOrEmpty(resourcePath))
                            {
                                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                                byte[] resourceByteArray;
                                bool isPartial = false;
                                RangeItemHeaderValue range = null;
                                long totalLength = 0;
                                if (Request.Headers.Date.HasValue && Request.Headers.Date.Value == resource.LastUpdateDate)
                                    return new HttpResponseMessage(HttpStatusCode.NotModified);
                                if (Request.Headers.Range != null && Request.Headers.Range.Ranges != null && Request.Headers.Range.Ranges.Count() > 0)
                                {
                                    response = new HttpResponseMessage(HttpStatusCode.PartialContent);
                                    range = Request.Headers.Range.Ranges.First();
                                    if (range.From.HasValue && range.From.Value > 0)
                                    {
                                        resourceByteArray = FileHelper.GetPartialFile(resourcePath, (int)range.From.Value, range.To.HasValue ? (int)range.To.Value + 1 : -1, out totalLength);
                                        dataStream = new MemoryStream(resourceByteArray);
                                    }
                                    else
                                    {
                                        dataStream = new FileStream(resourcePath, FileMode.Open, FileAccess.Read);
                                        totalLength = dataStream.Length;
                                    }
                                    isPartial = true;
                                }
                                else
                                    dataStream = new FileStream(resourcePath, FileMode.Open, FileAccess.Read);
                                response.Content = new StreamContent(dataStream);
                                //response.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddDays(30));
                                //response.Headers.CacheControl = new CacheControlHeaderValue();
                                //response.Headers.CacheControl.NoCache = false;
                                //response.Headers.CacheControl.MaxAge = TimeSpan.FromDays(30);
                                //response.Headers.CacheControl.Public = true;
                                response.Headers.Date = resource.LastUpdateDate;
                                if (range != null)
                                {
                                    response.Content.Headers.ContentRange = new ContentRangeHeaderValue(range.From.Value, range.To.HasValue ? range.To.Value : (dataStream.Length + range.From.Value) - 1, totalLength);
                                    response.Headers.ETag = new EntityTagHeaderValue("\"9313e5c83e4cd01:0\"");
                                }
                                response.Headers.AcceptRanges.Add("bytes");

                                if (!string.IsNullOrEmpty(resource.FileName) && resource.ResourceTypeId != (int)ResourceTypes.Video && resource.ResourceTypeId != (int)ResourceTypes.Image)
                                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = resource.FileName };


                                if (format != null)
                                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(format.MimeType);
                                Console.WriteLine("Resource Request Time: {0}ms", (DateTime.UtcNow - dt).TotalMilliseconds);

                                if (HttpContext.Current.Request.QueryString["UserId"] != null)
                                {
                                    LogResource log = new LogResource();
                                    log.AccessTypeId = (int)AccessType.ViewResource;
                                    log.UserId = Convert.ToInt32(HttpContext.Current.Request.QueryString["UserId"].ToString());
                                    log.Guid = resource.Guid.ToString();
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(logresource), (object)log);
                                }
                                else
                                {
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateLastAccessDate), (object)resource);
                                }
                                return response;
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                throw;
            }
            finally
            {
                //if (dataStream != null)
                //    dataStream.Dispose();
            }
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        [HttpGet]
        [ActionName("GetResourceByCaption")]
        public MS.Core.DataTransfer.Resource.ResourceOutput GetResourceByCaption(string caption)
        {
            MS.Core.DataTransfer.Resource.ResourceOutput output = new Core.DataTransfer.Resource.ResourceOutput();
            try
            {
                if (!String.IsNullOrEmpty(caption))
                {
                    List<Resource> resources = resourceService.GetResourceByCaption(caption);
                    output.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                    output.Resources.CopyFrom(resources);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                throw;
            }

            return output;
        }


        [HttpGet]
        [ActionName("GetResourceMetaByGuid")]
        public MS.Core.DataTransfer.Resource.GET.ResourceMetaOutput GetResourceMetaByGuid(string Guid)
        {
            MS.Core.DataTransfer.Resource.GET.ResourceMetaOutput output = new MS.Core.DataTransfer.Resource.GET.ResourceMetaOutput();
            try
            {
                if (!String.IsNullOrEmpty(Guid))
                {
                    List<ResourceMeta> lstmeta = new List<ResourceMeta>();
                    List<ResourceMeta> resources = resourceMetaService.GetResourceMetaByGuid(Guid);
                    ResourceDetail Details = resourcedetailService.GetResourceDetailByGuid(Guid);

                    if (Details != null)
                    {
                        output.Duration = Details.ResourceDuration;
                        output.Description = Details.Description;
                        output.Filesize = Details.FileSize.ToString();
                    }

                    if (resources != null && resources.Count > 0)
                    {
                        var Cateogry = resources.Where(x => x.Name.ToLower() == "category").ToList();
                        var Location = resources.Where(x => x.Name.ToLower() == "location").ToList();
                        var Caption = resources.Where(x => x.Name.ToLower() == "caption").ToList();

                        if (Cateogry != null && Cateogry.Count > 0)
                            output.Cateogry = Cateogry.FirstOrDefault().Value;

                        if (Location != null && Location.Count > 0)
                            output.Location = Location.FirstOrDefault().Value;

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.Slug))
                            {
                                output.Caption = Details.Slug;
                            }
                            else
                            {
                                if (Caption != null && Caption.Count > 0)
                                    output.Caption = Caption.FirstOrDefault().Value;
                            }
                        }
                        else
                        {
                            if (Caption != null && Caption.Count > 0)
                                output.Caption = Caption.FirstOrDefault().Value;
                        }

                        lstmeta = resources.Where(x => x.Name.ToLower() == "tag").ToList();
                    }
                    output.ResourceMetas = new List<Core.DataTransfer.Resource.GET.MetaOutPut>();

                    if(lstmeta != null && lstmeta.Count>0)
                    output.ResourceMetas.CopyFrom(lstmeta);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                throw new Exception();
            }

            return output;
        }


        [HttpPost]
        [ActionName("GetResourceInfoByGuidIds")]
        public DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>> GetResourceInfoByGuidIds(MS.Core.DataTransfer.Resource.GET.ResourceOutputGuids OutputGuids)
        {
            DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>> transfer = new DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>>();
            try
            {
                List<Resource> resource = resourceService.GetResourceByGuidIds(OutputGuids.Guids);
                transfer.Data = new List<Core.DataTransfer.Resource.GetOutput>();
                transfer.Data.CopyFrom(resource);
                return transfer;
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                return new DataTransfer<List<Core.DataTransfer.Resource.GetOutput>>() { Errors = new string[] { exp.Message }, IsSuccess = false };
            }
        }

        [HttpGet]
        [ActionName("GetResourceInfoByGuid")]
        public DataTransfer<MS.Core.DataTransfer.Resource.GetOutput> GetResourceInfoByGuid(string guid)
        {
            DataTransfer<MS.Core.DataTransfer.Resource.GetOutput> transfer = new DataTransfer<MS.Core.DataTransfer.Resource.GetOutput>();
            try
            {
                Resource resource = resourceService.GetResourceByGuidId(new Guid(guid));
                transfer.Data = new Core.DataTransfer.Resource.GetOutput();
                transfer.Data.CopyFrom(resource);
                return transfer;
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                return new DataTransfer<MS.Core.DataTransfer.Resource.GetOutput>() { Errors = new string[] { exp.Message }, IsSuccess = false };
            }
        }

        //[HttpGet]
        //[ActionName("Search")]
        //public MS.Core.DataTransfer.Resource.ResourceOutput Search(string term, int pageSize, int pageNumber)
        //{
        //    MS.Core.DataTransfer.Resource.ResourceOutput transfer = new MS.Core.DataTransfer.Resource.ResourceOutput();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(term) && pageSize > 0)
        //        {
        //            List<Resource> resources = resourceService.SearchResource(term, string.Empty, pageSize, pageNumber);
        //            transfer.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
        //            transfer.Resources.CopyFrom(resources);
        //        }
        //    }
        //    catch(Exception exp)
        //    {
        //        Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
        //    }

        //    return transfer;
        //}

        [HttpGet]
        [ActionName("SearchResource")]
        public MS.Core.DataTransfer.Resource.ResourceOutput SearchResource(string term, string fromDate, string toDate, int pageSize,bool IsHDSelected, int pageNumber, int? resourceTypeId = null, int? bucketid = null, Nullable<bool> alldata = true)
        {
            MS.Core.DataTransfer.Resource.ResourceOutput transfer = new MS.Core.DataTransfer.Resource.ResourceOutput();
           
            try
            {
                if (pageSize > 0)
                {
                    int returnCount = 0;
                    if (bucketid.HasValue)
                    {
                        if (bucketid.Value < 0)
                            bucketid = null;
                    }

                    List<Resource> resources = resourceService.SearchResources(term, fromDate, toDate, string.Empty, pageSize, IsHDSelected, pageNumber, resourceTypeId, bucketid, alldata.HasValue ? alldata.Value : true, out returnCount);
                    transfer.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                    
                    transfer.Count = returnCount;
                    transfer.Resources.CopyFrom(resources);
                    //transfer.Resources = transfer.Resources.Where(c => c.isHD == IsHDSelected).ToList();
                    //transfer.Count = transfer.Resources.Count;
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetResourceByDate")]
        public MS.Core.DataTransfer.Resource.ResourceOutput GetResourceByDate(string ResourceDate)
        {
            MS.Core.DataTransfer.Resource.ResourceOutput transfer = new MS.Core.DataTransfer.Resource.ResourceOutput();
            try
            {
                DateTime CreationDate = new DateTime();
                DateTime date = new DateTime();
                if (DateTime.TryParse(ResourceDate, out date))
                {
                    CreationDate = date.ToUniversalTime();
                }

                List<Resource> resources = resourceService.GetResourceByDate(CreationDate);
                transfer.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                if (resources != null && resources.Count > 0)
                {
                    transfer.Count = resources.Count;
                    transfer.Resources.CopyFrom(resources);
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("SearchResources")]
        public DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput> SearchResources(string term, string fromDate, string toDate, int pageSize,bool isHDSelected, int pageNumber, int? resourceTypeId = null, int? bucketid = null, Nullable<bool> alldata = true)
        {
            DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput> transfer = new DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput>();
            try
            {
                transfer.Data = new Core.DataTransfer.Resource.ResourceOutput();
                if (!string.IsNullOrEmpty(term) && pageSize > 0)
                {
                    int returnCount;
                    List<Resource> resources = resourceService.SearchResources(term, fromDate, toDate, string.Empty, pageSize, isHDSelected, pageNumber, resourceTypeId, bucketid, alldata.HasValue ? alldata.Value : true, out returnCount);
                    transfer.Data.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                    transfer.Data.Count = returnCount;
                    transfer.Data.Resources.CopyFrom(resources);
                }
            }
            catch (Exception exp)
            {
                transfer.Errors = new string[] { exp.Message };
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("SearchMetas")]
        public DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput> SearchMetas(string term, string Metas, int pageSize, int pageNumber, int? resourceTypeId = null, int? bucketid = null, Nullable<bool> alldata = true)
        {
            DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput> transfer = new DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput>();
            try
            {
                transfer.Data = new Core.DataTransfer.Resource.ResourceOutput();
                if (pageSize > 0)
                {
                    if (bucketid.HasValue)
                    {
                        if (bucketid.Value < 0)
                            bucketid = null;
                    }

                    int returnCount = 0;
                    List<Resource> resources = resourceService.AdvanceSearch(term, Metas, pageSize, pageNumber, resourceTypeId, bucketid, alldata.HasValue ? alldata.Value : true, out returnCount);
                    transfer.Data.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                    transfer.Data.Count = returnCount;
                    transfer.Data.Resources.CopyFrom(resources);
                }
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            }

            return transfer;
        }


        [HttpGet]
        [ActionName("SearchTag")]
        public DataTransfer<List<string>> SearchTag(string term, int pageSize, int pageNumber)
        {
            DataTransfer<List<string>> transfer = new DataTransfer<List<string>>();

            try
            {
                transfer.IsSuccess = true;
                transfer.Data = resourceService.SearchTag(term, pageSize, pageNumber);
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;

        }


        [HttpGet]
        [ActionName("SearchCelebrity")]
        public DataTransfer<List<string>> SearchCelebrity(string term, int pageSize, int pageNumber)
        {
            DataTransfer<List<string>> transfer = new DataTransfer<List<string>>();

            try
            {
                transfer.IsSuccess = true;
                transfer.Data = resourceService.SearchCelebrityName(term, pageSize, pageNumber);
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetResourcePathByGuid")]
        public string GetResourcePathByGuid(string id)
        {
            string path = "";
            try
            {
                if (!String.IsNullOrEmpty(id))
                {
                    Guid guid = new Guid();
                    if (Guid.TryParse(id, out guid))
                    {
                        path = resourceService.GetResourcePathByGuid(guid);
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }

            return path;
        }

        [HttpGet]
        [ActionName("GetResourceforDuration")]
        public MS.Core.DataTransfer.Resource.ResourceOutput GetResourceforDuration(DateTime ResourceDate)
        {
            MS.Core.DataTransfer.Resource.ResourceOutput output = new Core.DataTransfer.Resource.ResourceOutput();
            try
            {
                if (ResourceDate != null)
                {
                    List<Resource> resources = resourceService.GetResourceforDuration(ResourceDate);
                    output.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                    output.Resources.CopyFrom(resources);
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;

            }
            return output;
        }

        [HttpGet]
        [ActionName("GetResourceByMeta")]
        public MS.Core.DataTransfer.Resource.ResourceOutput GetResourceByMeta(string MetaString)
        {

            MS.Core.DataTransfer.Resource.ResourceOutput output = new Core.DataTransfer.Resource.ResourceOutput();
            try
            {
                if (!String.IsNullOrEmpty(MetaString))
                {
                    List<Resource> resources = resourceService.GetResourceByMeta(MetaString);
                    if (resources != null && resources.Count > 0)
                    {
                        output.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                        output.Resources.CopyFrom(resources);
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }
            return output;
        }

        [HttpGet]
        [ActionName("GetThumb")]
        public HttpResponseMessage GetThumb(string id)
        {
            Guid guid = new Guid();
            try
            {
                if (Guid.TryParse(id, out guid))
                {
                    DateTime dt = DateTime.UtcNow;
                    Resource resource = resourceService.GetResourceByGuidId(guid);

                    if (resource != null)
                    {
                        if (Request.Headers.Date.HasValue && Request.Headers.Date.Value == resource.LastUpdateDate)
                            return new HttpResponseMessage(HttpStatusCode.NotModified);



                        string resourcePath = string.Empty;
                        Format format = new Format();
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            if (resource.ResourceTypeId == (int)ResourceTypes.Audio)
                                resourcePath = HttpContext.Current.Server.MapPath("~/Images") + "/audiothumb.jpg";
                            else if (resource.ResourceTypeId == (int)ResourceTypes.Document)
                            {
                                if (!string.IsNullOrEmpty(resource.ThumbUrl))
                                {
                                    if (resource.BucketId != null && resource.BucketId > 0 && !string.IsNullOrEmpty(resource.FilePath))
                                    {
                                        Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationThumb"] + "\\" + bucket.Path + "\\" + resource.ThumbUrl;
                                    }
                                    else
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + resource.ThumbUrl;
                                }
                                else
                                    resourcePath = HttpContext.Current.Server.MapPath("~/Images") + "/docthumb.jpg";
                            }
                            else
                            {
                                if (resource.BucketId != null && resource.BucketId > 0 && !string.IsNullOrEmpty(resource.FilePath))
                                {
                                    Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                                    //resourcePath = "\\\\" + server.ServerIp + "\\" + resource.ThumbUrl;
                                    if (!string.IsNullOrEmpty(resource.ThumbUrl))
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationThumb"] + "\\" + bucket.Path + "\\" + resource.ThumbUrl;
                                    else
                                        if (resource.ResourceStatusId == (int)ResourceStatuses.Completed)
                                            resourcePath = HttpContext.Current.Server.MapPath("~/Images") + "/nothumb.png";
                                        else
                                            resourcePath = HttpContext.Current.Server.MapPath("~/Images") + "/medianotfound.jpg";

                                }
                                else
                                    if (!string.IsNullOrEmpty(resource.ThumbUrl))
                                        resourcePath = "\\\\" + server.ServerIp + "\\" + resource.ThumbUrl;
                                    else
                                        if (resource.ResourceStatusId == (int)ResourceStatuses.Completed)
                                            resourcePath = HttpContext.Current.Server.MapPath("~/Images") + "/nothumb.png";
                                        else
                                            resourcePath = HttpContext.Current.Server.MapPath("~/Images") + "/medianotfound.jpg";
                            }

                            byte[] resourceByteArray = FileHelper.GetImageFromCache(resourcePath);

                            MemoryStream dataStream = new MemoryStream(resourceByteArray);
                            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                            response.Content = new StreamContent(dataStream);
                            response.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddDays(30));
                            response.Headers.CacheControl = new CacheControlHeaderValue();
                            response.Headers.CacheControl.NoCache = false;
                            response.Headers.CacheControl.MaxAge = TimeSpan.FromDays(30);
                            response.Headers.CacheControl.Public = true;
                            response.Headers.Date = resource.LastUpdateDate;
                            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                            Console.WriteLine("Resource Request Time: {0}ms", (DateTime.UtcNow - dt).TotalMilliseconds);
                            return response;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        [HttpGet]
        [ActionName("DeleteResource")]
        public HttpResponseMessage DeleteResource(string id)
        {
            Guid guid = new Guid();
            string resourcePath = string.Empty;
            string lowrespath = string.Empty;
            string thumbpath = string.Empty;
            try
            {
                if (Guid.TryParse(id, out guid))
                {
                    Resource resource = resourceService.GetResourceByGuidId(guid);
                    if (resource != null)
                    {
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            if (resource.BucketId != null && resource.BucketId > 0 && !string.IsNullOrEmpty(resource.FilePath))
                            {
                                Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);

                                lowrespath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationLowRes"] + "\\" + bucket.Path + "\\";
                                resourcePath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocation"] + "\\" + bucket.Path + "\\";
                                thumbpath = "\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationThumb"] + "\\" + bucket.Path + "\\";

                                try
                                {
                                    if (!string.IsNullOrEmpty(resource.HighResolutionFile) && File.Exists(resourcePath + resource.HighResolutionFile))
                                        File.Delete(resourcePath + resource.HighResolutionFile);
                                }
                                catch { }
                                try
                                {
                                    if (!string.IsNullOrEmpty(resource.LowResolutionFile) && File.Exists(lowrespath + resource.LowResolutionFile))
                                        File.Delete(lowrespath + resource.LowResolutionFile);
                                }
                                catch { }
                                try
                                {
                                    if (!string.IsNullOrEmpty(resource.ThumbUrl) && File.Exists(thumbpath + resource.ThumbUrl))
                                        File.Delete(thumbpath + resource.ThumbUrl);
                                }
                                catch { }
                            }
                            else
                            {
                                resourcePath = "\\\\" + server.ServerIp + "\\";

                                try
                                {
                                    if (!string.IsNullOrEmpty(resource.HighResolutionFile) && File.Exists(resourcePath + resource.HighResolutionFile))
                                        File.Delete(resourcePath + resource.HighResolutionFile);
                                }
                                catch { }
                                try
                                {
                                    if (!string.IsNullOrEmpty(resource.LowResolutionFile) && File.Exists(resourcePath + resource.LowResolutionFile))
                                        File.Delete(resourcePath + resource.LowResolutionFile);
                                }
                                catch { }
                                try
                                {
                                    if (!string.IsNullOrEmpty(resource.ThumbUrl) && File.Exists(resourcePath + resource.ThumbUrl))
                                        File.Delete(resourcePath + resource.ThumbUrl);
                                }
                                catch { }
                            }
                        }

                        resource.ResourceStatusId = (int)ResourceStatuses.Deleted;
                        resourceService.UpdateResource(resource);
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        public static bool ThumbnailCallback()
        {
            return true;
        }


        [HttpPost]
        [ActionName("PostResource")]
        public MS.Core.DataTransfer.Resource.PostOutput PostResource(MS.Core.DataTransfer.Resource.PostInput input)
        {
            // if (string.IsNullOrEmpty(input.FilePath))postresou
            //     throw new Exception("FilePath not provided");
            MS.Core.DataTransfer.Resource.PostOutput output = new Core.DataTransfer.Resource.PostOutput();
            try
            {
                string filepath = string.Empty;
                Resource res = new Resource();
                res.CopyFrom((object)input);
                res.Guid = Guid.NewGuid();
                res.CreationDate = DateTime.UtcNow;
                res.LastUpdateDate = res.CreationDate;
                res.IsActive = true;
                try { filepath = res.FilePath.Substring(0, res.FilePath.LastIndexOf('\\')); }
                catch { }

                res.FilePath = res.FilePath.Replace('/', '\\');
                res.Source = res.Source.Replace("{guid}", res.Guid.ToString());

                if (res.FilePath.Contains("{guid}"))
                {
                    res.FilePath = res.FilePath.Replace("{guid}", res.Guid.ToString());
                }
                res.ServerId = LocalCache.Cache.Servers.Where(x => x.IsDefault).First().ServerId;




                if (!string.IsNullOrEmpty(res.ThumbUrl))
                {
                    if (res.BucketId.HasValue)
                    {
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == res.ServerId).FirstOrDefault();
                        Bucket bucket = bucketService.GetBucket(res.BucketId.Value);

                        if (server != null && bucket != null)
                        {
                            string saveFolderPath = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + bucket.Path + "\\" + res.FilePath.Substring(0, res.FilePath.LastIndexOf('\\') + 1);
                            string ext = System.IO.Path.GetExtension(res.FileName);
                            string mediaLocation = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
                            string root = bucket.Path + "\\";
                            string destinationFileName = res.FileName;

                            byte[] data = null;
                            using (WebClient client = new WebClient())
                            {
                                data = client.DownloadData(ConfigurationManager.AppSettings["ScrapingServerIP"].ToString() + "/api/amazon/download?url=" + res.ThumbUrl);
                            }


                            if (!string.IsNullOrEmpty(ext))
                                destinationFileName = destinationFileName.Replace(ext, ".jpg");
                            else
                                destinationFileName += ".jpg";

                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(data))
                            {
                                using (System.Drawing.Image image = System.Drawing.Image.FromStream(ms))
                                {
                                    System.Drawing.Image thumbnailImage = image.GetThumbnailImage(135, 75, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
                                    using (FileStream imageStream = new FileStream(saveFolderPath + destinationFileName, FileMode.Create))
                                    {
                                        thumbnailImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(ext))
                                res.ThumbUrl = res.FilePath.Replace(ext, ".jpg");
                            else
                                res.ThumbUrl = res.FilePath + ".jpg";


                            res.LastUpdateDate = DateTime.UtcNow;

                        }

                    }
                }

                if (!string.IsNullOrEmpty(res.FilePath) && res.BucketId != null && res.BucketId > 0)
                {
                    var res1 = resourceService.GetResourceByFilePathAndBucketId(res.FilePath, res.BucketId.Value);
                    if (res1 != null)
                    {
                        output.CopyFrom(res1);
                        return output;
                    }
                }


                if (res.ResourceStatusId == 0)
                    res.ResourceStatus = ResourceStatuses.Uploading;
                if (res.ResourceTypeId == 0)
                    res.ResourceTypeId = (int)ExtensionHelper.GetResourceType(System.IO.Path.GetExtension(res.FileName).Trim('.'));

                try
                {
                    Bucket buck = bucketService.GetBucketByPath(filepath);
                    if (buck != null)
                    {
                        res.SubBucketId = buck.BucketId;
                    }
                }
                catch (Exception exp)
                {
                    Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                    throw;
                }


                res.Caption = input.Caption;
                res.Category = input.Category;
                res.Location = input.Location;
                res = resourceService.InsertResource(res);

                input.Guid = res.Guid.ToString();
                AddResourceMetas(input);

                output.CopyFrom(res);

            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }
            return output;
        }

        [HttpPost]
        [ActionName("AddUserFavourite")]
        public MS.Core.DataTransfer.UserFavourites.PostOutput AddUserFavourite(MS.Core.DataTransfer.UserFavourites.PostInput input)
        {
            MS.Core.DataTransfer.UserFavourites.PostOutput output = new Core.DataTransfer.UserFavourites.PostOutput();
            try
            {
                UserFavourites userfavourite = new UserFavourites();
                userfavourite.ResourceGuid = Guid.Parse(input.ResourceGuid);
                userfavourite.UserId = Convert.ToInt32(input.UserId);
                userfavourite.CreationDate = DateTime.UtcNow;
                userfavourite.LastUpdateDate = DateTime.UtcNow;
                userfavourite = userfavouriteservice.InsertUserFavourites(userfavourite);

                output.CopyFrom(userfavourite);
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }

            return output;
        }

        [HttpGet]
        [ActionName("GetUserFavourite")]
        public List<MS.Core.DataTransfer.UserFavourites.PostOutput> GetUserFavourite(int UserId)
        {
            List<MS.Core.DataTransfer.UserFavourites.PostOutput> items = new List<Core.DataTransfer.UserFavourites.PostOutput>();
            List<UserFavourites> userfav = userfavouriteservice.GetUserFavouritesByUserId(UserId);
            // ResourceTypeId
            if (userfav != null)
            {
                foreach (UserFavourites item in userfav)
                {
                    if (item.ResourceGuid.HasValue)
                    {
                        Resource resource = resourceService.GetResourceByGuidIdforUserFavourite(item.ResourceGuid.Value);
                        if (resource != null)
                        {
                            item.Slug = resource.Slug;
                            item.ResourceTypeId = resource.ResourceTypeId;
                        }
                    }
                }
            }

            if (userfav != null)
            {
                items.CopyFrom(userfav);
            }

            return items;
        }


        [HttpGet]
        [ActionName("DeleteUserFavourite")]
        public DataTransfer<bool> DeleteUserFavourite(int UserId, string guid)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.IsSuccess = true;
                transfer.Data = userfavouriteservice.DeleteUserFavouritesByGuid(UserId, guid);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
            }

            return transfer;

        }

        [HttpPost]
        [ActionName("AddResourceMeta")]
        public MS.Core.DataTransfer.Resource.PostInput AddResourceMeta(MS.Core.DataTransfer.Resource.PostInput input)
        {
            try
            {
                AddResourceMetas(input);
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }

            return input;
        }

        private void AddResourceMetas(MS.Core.DataTransfer.Resource.PostInput input)
        {

            DateTime dateNow = DateTime.UtcNow;
            Resource resource = resourceService.GetResourceByGuidId(new Guid(input.Guid));
            try
            {
                if (resource != null)
                {
                    if (!String.IsNullOrEmpty(input.Category))
                    {
                        resource.Category = input.Category;
                    }
                    if (!String.IsNullOrEmpty(input.Location))
                    {
                        resource.Location = input.Location;
                    }
                    if (!String.IsNullOrEmpty(input.Caption))
                    {
                        resource.Caption = input.Caption;
                    }
                    resourceService.UpdateResourceInfo(resource);

                    if (!String.IsNullOrEmpty(input.Caption))
                    {
                        ResourceMeta resourcemeta = new ResourceMeta();
                        resourcemeta.ResourceGuid = new Guid(input.Guid);
                        resourcemeta.Name = "Caption";
                        resourcemeta.Value = input.Caption;
                        resourcemeta.CreationDate = dateNow;
                        resourceMetaService.CheckAndInsertResourceMeta(resourcemeta);
                    }

                    if (!String.IsNullOrEmpty(input.Category))
                    {
                        ResourceMeta resourcemeta = new ResourceMeta();
                        resourcemeta.ResourceGuid = new Guid(input.Guid);
                        resourcemeta.Name = "Category";
                        resourcemeta.Value = input.Category;
                        resourcemeta.CreationDate = dateNow;
                        resourceMetaService.CheckAndInsertResourceMeta(resourcemeta);
                    }

                    if (!String.IsNullOrEmpty(input.Location))
                    {
                        ResourceMeta resourcemeta = new ResourceMeta();
                        resourcemeta.ResourceGuid = new Guid(input.Guid);
                        resourcemeta.Name = "Location";
                        resourcemeta.Value = input.Location;
                        resourcemeta.CreationDate = dateNow;
                        resourceMetaService.CheckAndInsertResourceMeta(resourcemeta);
                    }

                    if (input.ResourceMeta != null)
                    {
                        foreach (KeyValuePair<string, string> keyvalue in input.ResourceMeta)
                        {

                            ResourceMeta resourcemeta = new ResourceMeta();
                            resourcemeta.ResourceGuid = new Guid(input.Guid);
                            resourcemeta.Name = keyvalue.Key;
                            resourcemeta.Value = keyvalue.Value;
                            resourcemeta.CreationDate = dateNow;
                            resourceMetaService.InsertResourceMeta(resourcemeta);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;

            }
        }

        [HttpGet]
        [ActionName("UpdateResourceStatus")]
        public HttpResponseMessage UpdateResourceStatus(string guid, int statusId)
        {
            Guid _guid = new Guid();
            try
            {
                if (Guid.TryParse(guid, out _guid))
                {
                    Resource res = resourceService.GetResourceByGuidId(_guid);
                    res.ResourceStatusId = statusId;
                    res.LastUpdateDate = DateTime.UtcNow;
                    resourceService.UpdateResource(res);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [ActionName("GenerateSnapshot")]
        public DataTransfer<string> GenerateSnapshot(string id)
        {
            try
            {
                Guid guid = new Guid();
                if (Guid.TryParse(id, out guid))
                {
                    DateTime dt = DateTime.UtcNow;
                    Resource resource = resourceService.GetResourceByGuidId(guid);
                    if (resource != null && string.IsNullOrEmpty(resource.ThumbUrl))
                    {
                        string resourcePath = string.Empty;
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                            resource.FilePath = resource.FilePath.Replace("/", "\\");
                            string ext = System.IO.Path.GetExtension(resource.FilePath);
                            string folderPath = resource.FilePath.Substring(0, resource.FilePath.LastIndexOf('\\') + 1);
                            string root = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + bucket.Path + "\\" + folderPath;
                            string filePath = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocation"] + bucket.Path + "\\" + resource.FilePath;
                            if (string.IsNullOrEmpty(resource.HighResolutionFile) && string.IsNullOrEmpty(resource.LowResolutionFile))
                                filePath = resource.Source;

                            string saveFileName = resource.FilePath.Replace(ext, ".jpg");
                            saveFileName = saveFileName.Substring(saveFileName.LastIndexOf('\\') + 1, (saveFileName.Length - (saveFileName.LastIndexOf('\\') + 1)));

                            if (ResourceHelper.GenerateSnapshot(HttpContext.Current.Server.MapPath("~/ffmpeg/"), filePath, root, saveFileName, ((ResourceTypes)resource.ResourceTypeId)))
                            {
                                resource.ThumbUrl = resource.FilePath.Replace(ext, ".jpg");
                                resourceService.UpdateResource(resource);
                                return new DataTransfer<string>() { Data = "Snapshot generated successfully" };
                            }
                            else return new DataTransfer<string>() { Errors = new string[] { "Snapshot not supported for this " + ((ResourceTypes)resource.ResourceTypeId).ToString() + " type" }, IsSuccess = false };
                        }
                    }
                }
                return new DataTransfer<string>() { Errors = new string[] { "No Resource Found" }, IsSuccess = false };
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                return new DataTransfer<string>() { Errors = new string[] { exp.Message }, IsSuccess = false };
            }
        }

        [HttpPost]
        [ActionName("GenerateCustomSnapshot")]
        public DataTransfer<string> GenerateCustomSnapshot(CustomSnapshotInput input)
        {
            try
            {

                Guid guid = new Guid();
                if (Guid.TryParse(input.Id, out guid))
                {
                    DateTime dt = DateTime.UtcNow;
                    Resource resource = resourceService.GetResourceByGuidId(guid);
                    if (resource != null)
                    {
                        string resourcePath = string.Empty;
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            if (!string.IsNullOrEmpty(resource.FilePath))
                            {
                                Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                                resource.FilePath = resource.FilePath.Replace("/", "\\");
                                string ext = System.IO.Path.GetExtension(resource.FilePath);
                                string folderPath = resource.FilePath.Substring(0, resource.FilePath.LastIndexOf('\\') + 1);
                                string root = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + bucket.Path + "\\" + folderPath;
                                string filePath = resource.Source;
                                string saveFileName = resource.FilePath.Replace(ext, ".jpg");
                                saveFileName = saveFileName.Substring(saveFileName.LastIndexOf('\\') + 1, (saveFileName.Length - (saveFileName.LastIndexOf('\\') + 1)));


                                if (ResourceHelper.GenerateSnapshot(HttpContext.Current.Server.MapPath("~/ffmpeg/"), input.FilePath, root, saveFileName, ((ResourceTypes)resource.ResourceTypeId)))
                                {
                                    resource.ThumbUrl = resource.FilePath.Replace(ext, ".jpg");
                                    resourceService.UpdateResource(resource);
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(GenerateFileInformation), (object)resource);
                                    return new DataTransfer<string>() { Data = "Snapshot generated successfully" };
                                }
                                else return new DataTransfer<string>() { Errors = new string[] { "Snapshot not supported for this " + ((ResourceTypes)resource.ResourceTypeId).ToString() + " type" }, IsSuccess = false };
                            }
                            else
                            {
                                string root = "\\" + ConfigurationManager.AppSettings["MediaLocation"] + "\\Thumbs";
                                if (!Directory.Exists("\\\\" + server.ServerIp + root))
                                {
                                    Directory.CreateDirectory("\\\\" + server.ServerIp + root);
                                }
                                if (ResourceHelper.GenerateSnapshot(HttpContext.Current.Server.MapPath("~/ffmpeg/"), input.FilePath, "\\\\" + server.ServerIp + root + "\\", resource.Guid.ToString() + ".jpg", (ResourceTypes)resource.ResourceTypeId, TimeSpan.FromSeconds(input.TimeSpan)))
                                {
                                    resource.ThumbUrl = root + "\\" + resource.Guid.ToString() + ".jpg";
                                    resourceService.UpdateResource(resource);
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(GenerateFileInformation), (object)resource);
                                    return new DataTransfer<string>() { Data = "Snapshot generated successfully" };
                                }
                                else return new DataTransfer<string>() { Errors = new string[] { "Snapshot not supported for this " + ((ResourceTypes)resource.ResourceTypeId).ToString() + " type" }, IsSuccess = false };
                            }
                        }
                    }
                }
                return new DataTransfer<string>() { Errors = new string[] { "No Resource Found" }, IsSuccess = false };
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                return new DataTransfer<string>() { Errors = new string[] { exp.Message }, IsSuccess = false };
            }
        }

        [HttpPost]
        [ActionName("PostThumb")]
        public Task<HttpResponseMessage> PostThumb(string Id)
        {
            Guid guid = new Guid();
            try
            {
                if (Guid.TryParse(Id, out guid))
                {
                    Resource resource = resourceService.GetResourceByGuidId(guid);
                    if (resource != null)
                    {
                        HttpRequestMessage request = this.Request;
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                        if (server != null && bucket != null)
                        {
                            string root = bucket.Path + "\\";
                            string mediaLocation = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
                            resource.FilePath = resource.FilePath.Replace('/', '\\');

                            if (!request.Content.IsMimeMultipartContent())
                            {
                                if (HttpContext.Current.Request.Files.Count > 0)
                                {
                                    var file = HttpContext.Current.Request.Files[0];
                                    string ext = System.IO.Path.GetExtension(file.FileName);
                                    string mimeType = file.ContentType;
                                    Format format = null;
                                    format = LocalCache.Cache.Formats.Where(x => x.MimeType == mimeType).FirstOrDefault();
                                    file.SaveAs("\\\\" + server.ServerIp + mediaLocation + root + "\\" + resource.FilePath);

                                    resource.LastUpdateDate = DateTime.UtcNow;
                                    resource.ThumbUrl = resource.FilePath.Replace(ext, ".jpg");
                                    resourceService.UpdateResource(resource);
                                }
                            }
                            else
                            {
                                var provider = new CustomMultipartFormDataStreamProvider("\\\\" + server.ServerIp + mediaLocation + root);

                                var task = request.Content.ReadAsMultipartAsync(provider).
                                    ContinueWith<HttpResponseMessage>(o =>
                                    {
                                        var fileData = provider.FileData.First();
                                        string file1 = fileData.LocalFileName;
                                        //string ext = System.IO.Path.GetExtension(fileData.Headers.ContentDisposition.FileName.Trim('"'));
                                        string ext = System.IO.Path.GetExtension(resource.FileName);
                                        string mimeType = fileData.Headers.ContentType.MediaType;
                                        string fileNameExt = System.IO.Path.GetExtension(resource.FilePath);
                                        string savePath = "\\\\" + server.ServerIp + mediaLocation + root + "\\" + resource.FilePath.Replace(fileNameExt, ".jpg");
                                        Format format = LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                        File.Move(file1, savePath);

                                        resource.LastUpdateDate = DateTime.UtcNow;
                                        resource.ThumbUrl = resource.FilePath.Replace(fileNameExt, ".jpg");
                                        resourceService.UpdateResource(resource);
                                        //SaveBucketResource(resource, format, mimeType, ext, file1, "\\\\" + server.ServerIp + mediaLocation + root + "\\" + resource.FilePath, root, IsHD, server.ServerIp);
                                        return new HttpResponseMessage(HttpStatusCode.OK)
                                        {
                                            Content = new StringContent("File uploaded.")
                                        };
                                    }
                                );
                                return task;
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }

            return Task<HttpResponseMessage>.Factory.StartNew(() =>
                {
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                }
            );
        }

        [HttpPost]
        [ActionName("PostMedia")]
        public Task<HttpResponseMessage> PostMedia(string Id, bool IsHD)
        {
            Guid guid = new Guid();
            try
            {
                if (Guid.TryParse(Id, out guid))
                {
                    Resource resource = resourceService.GetResourceByGuidId(guid);
                    if (resource != null)
                    {
                        HttpRequestMessage request = this.Request;
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            if (String.IsNullOrEmpty(resource.FilePath))
                            {
                                string root = "\\" + ConfigurationManager.AppSettings["MediaLocation"] + DateTime.UtcNow.ToString("dd-MM-yyyy") + "\\";
                                if (!Directory.Exists("\\\\" + server.ServerIp + root))
                                {
                                    Directory.CreateDirectory("\\\\" + server.ServerIp + root);
                                }
                                if (!request.Content.IsMimeMultipartContent())
                                {
                                    if (HttpContext.Current.Request.Files.Count > 0)
                                    {
                                        var file = HttpContext.Current.Request.Files[0];
                                        string ext = System.IO.Path.GetExtension(file.FileName);
                                        string mimeType = file.ContentType;
                                        Format format = null;
                                        format = LocalCache.Cache.Formats.Where(x => x.MimeType == mimeType).FirstOrDefault();
                                        file.SaveAs("\\\\" + server.ServerIp + root + "/" + resource.Guid.ToString() + ext);
                                        SaveResource(resource, format, mimeType, ext, null, null, root, IsHD, server.ServerIp);
                                    }
                                }
                                else
                                {
                                    var provider = new CustomMultipartFormDataStreamProvider("\\\\" + server.ServerIp + root);

                                    var task = request.Content.ReadAsMultipartAsync(provider).
                                        ContinueWith<HttpResponseMessage>(o =>
                                        {
                                            var fileData = provider.FileData.First();
                                            string file1 = fileData.LocalFileName;
                                            string ext = System.IO.Path.GetExtension(fileData.Headers.ContentDisposition.FileName.Trim('"'));
                                            string mimeType = fileData.Headers.ContentType.MediaType;
                                            Format format = LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                            SaveResource(resource, format, mimeType, ext, file1, "\\\\" + server.ServerIp + root + "/" + resource.Guid.ToString() + ext, root, IsHD, server.ServerIp);
                                            return new HttpResponseMessage(HttpStatusCode.OK)
                                            {
                                                Content = new StringContent("File uploaded.")
                                            };
                                        }
                                    );
                                    return task;
                                }
                            }
                            else
                            {
                                Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                                if (bucket != null)
                                {
                                    string root = bucket.Path + "\\";
                                    string mediaLocation = ConfigurationManager.AppSettings["MediaLocation"].ToString();
                                    if (!IsHD)
                                        mediaLocation = ConfigurationManager.AppSettings["MediaLocationLowRes"].ToString();
                                    resource.FilePath = resource.FilePath.Replace('/', '\\');

                                    if (!request.Content.IsMimeMultipartContent())
                                    {
                                        if (HttpContext.Current.Request.Files.Count > 0)
                                        {
                                            var file = HttpContext.Current.Request.Files[0];
                                            string ext = System.IO.Path.GetExtension(file.FileName);
                                            string mimeType = file.ContentType;
                                            Format format = null;
                                            format = LocalCache.Cache.Formats.Where(x => x.MimeType == mimeType).FirstOrDefault();
                                            file.SaveAs("\\\\" + server.ServerIp + mediaLocation + root + "\\" + resource.FilePath);
                                            SaveBucketResource(resource, format, mimeType, ext, null, null, root, IsHD, server.ServerIp);
                                        }
                                    }
                                    else
                                    {
                                        var provider = new CustomMultipartFormDataStreamProvider("\\\\" + server.ServerIp + mediaLocation + root);

                                        var task = request.Content.ReadAsMultipartAsync(provider).
                                            ContinueWith<HttpResponseMessage>(o =>
                                            {
                                                var fileData = provider.FileData.First();
                                                string file1 = fileData.LocalFileName;
                                                string ext = System.IO.Path.GetExtension(fileData.Headers.ContentDisposition.FileName.Trim('"'));
                                                string mimeType = fileData.Headers.ContentType.MediaType;
                                                Format format = LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                                SaveBucketResource(resource, format, mimeType, ext, file1, "\\\\" + server.ServerIp + mediaLocation + root + "\\" + resource.FilePath, root, IsHD, server.ServerIp);
                                                return new HttpResponseMessage(HttpStatusCode.OK)
                                                {
                                                    Content = new StringContent("File uploaded.")
                                                };
                                            }
                                        );
                                        return task;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }
            return Task<HttpResponseMessage>.Factory.StartNew(() =>
                        {
                            return new HttpResponseMessage(HttpStatusCode.NoContent);
                        }
                    );
        }

        [HttpPost]
        [ActionName("CropImage")]
        public DataTransfer<string> CropImage(CropImageInput input)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();

            try
            {
                MediaStorage.Storage storage = new MediaStorage.Storage(input.BucketId, input.ApiKey);
                transfer.IsSuccess = true;
                transfer.Data = storage.CropAndInsertImage(input);
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("ClipAndMergeVideos")]
        public DataTransfer<string> ClipAndMergeVideos(VideoClipInput input)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();

            try
            {
                MediaStorage.Storage storage = new MediaStorage.Storage(input.BucketId, input.ApiKey);
                transfer.IsSuccess = true;
                //transfer.Data = storage.ClipAndMergeVideos(input);
                transfer.Data = storage.AddVideoClipTask(input);
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("PostMediaForProcessing")]
        public Task<HttpResponseMessage> PostMediaForProcessing(string Id)
        {
            Guid guid = new Guid();
            try
            {
                if (Guid.TryParse(Id, out guid))
                {
                    Resource resource = resourceService.GetResourceByGuidId(guid);
                    if (resource != null)
                    {
                        HttpRequestMessage request = this.Request;
                        Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                        if (server != null)
                        {
                            if (!String.IsNullOrEmpty(resource.FilePath))
                            {
                                Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                                if (bucket != null)
                                {
                                    string root = bucket.Path + "\\";
                                    string mediaLocation = ConfigurationManager.AppSettings["MediaLocationTemp"].ToString();
                                    resource.FilePath = resource.FilePath.Replace('/', '\\');
                                    string fileName = resource.Guid + System.IO.Path.GetExtension(resource.FileName);

                                    if (!request.Content.IsMimeMultipartContent())
                                    {
                                        if (HttpContext.Current.Request.Files.Count > 0)
                                        {
                                            var file = HttpContext.Current.Request.Files[0];
                                            string ext = System.IO.Path.GetExtension(file.FileName);
                                            string mimeType = file.ContentType;
                                            Format format = null;
                                            format = LocalCache.Cache.Formats.Where(x => x.MimeType == mimeType).FirstOrDefault();
                                            file.SaveAs("\\\\" + server.ServerIp + mediaLocation + fileName);
                                            SaveBucketResource(resource, format, mimeType, ext, null, null, root, server.ServerIp);
                                        }
                                    }
                                    else
                                    {
                                        var provider = new CustomMultipartFormDataStreamProvider("\\\\" + server.ServerIp + mediaLocation);

                                        var task = request.Content.ReadAsMultipartAsync(provider).
                                            ContinueWith<HttpResponseMessage>(o =>
                                            {
                                                var fileData = provider.FileData.First();
                                                string file1 = fileData.LocalFileName;
                                                string ext = System.IO.Path.GetExtension(resource.FileName);
                                                string mimeType = fileData.Headers.ContentType.MediaType;
                                                Format format = LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                                SaveBucketResource(resource, format, mimeType, ext, file1, "\\\\" + server.ServerIp + mediaLocation + fileName, root, server.ServerIp);
                                                return new HttpResponseMessage(HttpStatusCode.OK)
                                                {
                                                    Content = new StringContent("File uploaded.")
                                                };
                                            }
                                        );
                                        return task;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
                throw;
            }

            return Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
                    );
        }

        //private static void CatchException(Exception ex)
        //{
        //    string url = HttpContext.Current.Request.PhysicalApplicationPath;
        //    using (FileStream fs = new FileStream("c:\\UploadErrorLog.txt", FileMode.Append, FileAccess.Write))
        //    using (StreamWriter sw = new StreamWriter(fs))
        //    {
        //        var st = new StackTrace(ex, true);
        //        var frame = st.GetFrame(0);
        //        var Errorline = frame.GetFileLineNumber();
        //        sw.WriteLine("Exception: on Line - " + Errorline + "=(" + ex.ToString() + ")\n");
        //        sw.WriteLine("Stack Trace: " + ex.StackTrace + ")");
        //        sw.Close();
        //    }
        //}

        private static void CatchException(Exception ex)
        {
            try
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                string path = "c:\\UploadErrorLog.txt";
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                }
                using (StreamWriter w = File.AppendText(path))
                {
                    w.WriteLine("\r\nLog Entry : ");
                    w.WriteLine("{0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    string err = "Error in: " +
                                  ". Error Message:" + ex.ToString();
                    w.WriteLine(err);
                    w.WriteLine("__________________________");
                    w.Flush();
                    w.Close();
                }
            }
            catch (Exception ex1)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex1));
            }
        }


        private Resource SaveBucketResource(Resource resource, MS.Core.Entities.Format format, string mimeType, string ext, string from, string to, string root, string serverIP)
        {
            string tempSaveLocation = ConfigurationManager.AppSettings["MediaLocationTemp"].ToString();
            string thumbLocation = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
            Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
            if (string.IsNullOrEmpty(resource.FileName))
            {
                if (resource.FilePath.LastIndexOf('\\') >= 0)
                    resource.FileName = resource.FilePath.Substring(resource.FilePath.LastIndexOf('\\') + 1, (resource.FilePath.Length - (resource.FilePath.LastIndexOf('\\') + 1)));
                else
                    resource.FileName = resource.FilePath;
            }

            if (format == null)
            {
                CatchException(new Exception("Save Resource 2"));
                format = new Format();
                format.Format = ext;
                format.MimeType = mimeType;
                format = formatService.InsertFormat(format);
                LocalCache.Cache.UpdateFormats();
            }
            if (from != null && to != null)
            {
                if (File.Exists(to))
                {
                    File.Delete(to);
                }
                File.Move(from, to);
            }
            if (string.IsNullOrEmpty(resource.ThumbUrl))
            {
                try
                {
                    string ffmpegPath = AppDomain.CurrentDomain.BaseDirectory + "\\ffmpeg\\";
                    string saveFolderPath = @"\\" + serverIP + ConfigurationSettings.AppSettings["MediaLocationThumb"] + bucket.Path + "\\" + resource.FilePath.Substring(0, resource.FilePath.LastIndexOf('\\') + 1);
                    MS.Core.Enums.ResourceTypes resType = MS.Core.Enums.ResourceTypes.Video;
                    System.IO.FileInfo fInfo = new System.IO.FileInfo(to);


                    FFMPEGLib.Helper.FfProbeInformation info = FFMPEGLib.FFMPEG.GetMediaInformation(to);
                    if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
                    {
                        resType = MS.Core.Enums.ResourceTypes.Image;
                        if (info != null && info.streams != null && info.streams.Count > 0)
                        {
                            resource.Width = info.streams[0].width;
                            resource.Height = info.streams[0].height;
                        }
                    }
                    else if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video)
                    {
                        resType = MS.Core.Enums.ResourceTypes.Video;
                        if (info != null && info.streams != null && info.streams.Count > 0)
                        {
                            foreach (FFMPEGLib.Helper.Stream s in info.streams)
                            {
                                if (s.codec_type == "audio")
                                {
                                    resource.Duration = Convert.ToDouble(s.duration);
                                    resource.AudioCodec = s.codec_name;
                                }
                                if (s.codec_type == "video")
                                {
                                    resource.VideoCodec = s.codec_name;
                                    resource.Duration = Convert.ToDouble(s.duration);
                                    resource.Bitrate = s.bit_rate;
                                    resource.Framerate = s.avg_frame_rate;
                                    resource.Width = s.width;
                                    resource.Height = s.height;
                                }
                            }
                        }
                    }
                    else if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Audio)
                    {
                        resType = MS.Core.Enums.ResourceTypes.Audio;
                        if (info != null && info.streams != null && info.streams.Count > 0)
                        {
                            foreach (FFMPEGLib.Helper.Stream s in info.streams)
                            {
                                if (s.codec_type == "audio")
                                {
                                    resource.Duration = Convert.ToDouble(s.duration);
                                    resource.AudioCodec = s.codec_name;
                                    resource.Bitrate = s.bit_rate;
                                }
                            }
                        }
                    }
                    string str = HttpRuntime.AppDomainAppPath;
                    string destinationFileName = resource.FileName;

                    if (!string.IsNullOrEmpty(ext))
                        destinationFileName = destinationFileName.Replace(ext, ".jpg");
                    else
                        destinationFileName += ".jpg";


                    if (ResourceHelper.GenerateSnapshot(HttpRuntime.AppDomainAppPath + "\\ffmpeg\\", to, saveFolderPath, destinationFileName, resType))
                    {
                        if (!string.IsNullOrEmpty(ext))
                            resource.ThumbUrl = resource.FilePath.Replace(ext, ".jpg");
                        else
                            resource.ThumbUrl = resource.FilePath + ".jpg";
                    }
                }
                catch (Exception ex)
                {
                    CatchException(ex);

                }
            }
            resource.LastUpdateDate = DateTime.UtcNow;
            resource.ResourceStatus = ResourceStatuses.Transcoding;
            return resourceService.UpdateResource(resource);
        }

        private Resource SaveBucketResource(Resource resource, Format format, string mimeType, string ext, string from, string to, string root, bool isHd, string serverIp)
        {
            string mediaLocation = ConfigurationManager.AppSettings["MediaLocation"].ToString();
            string mediaLocationThumb = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
            if (format == null)
            {
                CatchException(new Exception("Save Resource 2"));
                format = new Format();
                format.Format = ext;
                format.MimeType = mimeType;
                format = formatService.InsertFormat(format);
                LocalCache.Cache.UpdateFormats();
            }
            if (isHd)
            {
                CatchException(new Exception("Save Resource 3"));
                resource.HighResolutionFile = resource.FilePath;
                resource.HighResolutionFormatId = format.FormatId;
            }
            else
            {
                mediaLocation = ConfigurationManager.AppSettings["MediaLocationLowRes"].ToString();
                resource.LowResolutionFile = resource.FilePath;
                resource.LowResolutionFormatId = format.FormatId;
            }
            if (from != null && to != null)
            {
                if (File.Exists(to))
                {
                    File.Delete(to);
                }
                File.Move(from, to);
            }
            resource.LastUpdateDate = DateTime.UtcNow;
            //resource.FilePath = to;
            resource.ResourceStatus = ResourceStatuses.Completed;
            if (string.IsNullOrEmpty(resource.ThumbUrl))
            {
                try
                {
                    string str = HttpRuntime.AppDomainAppPath;
                    if (ResourceHelper.GenerateSnapshot(HttpRuntime.AppDomainAppPath + "\\ffmpeg\\", "\\\\" + serverIp + mediaLocation + root + "\\" + resource.FilePath, "\\\\" + serverIp + mediaLocationThumb + root, resource.FilePath.Replace(ext, ".jpg"), (ResourceTypes)resource.ResourceTypeId))
                    {
                        resource.ThumbUrl = resource.FilePath.Replace(ext, ".jpg");
                    }
                    if ((ResourceTypes)resource.ResourceTypeId == ResourceTypes.Video || (ResourceTypes)resource.ResourceTypeId == ResourceTypes.Audio)
                    {
                        var duration = FFMPEG.GetDurationInMilliSeconds("\\\\" + serverIp + mediaLocation + root + resource.HighResolutionFile) / 1000;
                        if (duration > 0)
                            resource.Duration = duration;
                    }
                }
                catch (Exception ex)
                {
                    CatchException(ex);

                }
            }
            return resourceService.UpdateResource(resource);
        }

        private Resource SaveResource(Resource resource, Format format, string mimeType, string ext, string from, string to, string root, bool IsHD, string serverIp)
        {
            if (format == null)
            {
                CatchException(new Exception("Save Resource 2"));
                format = new Format();
                format.Format = ext;
                format.MimeType = mimeType;
                format = formatService.InsertFormat(format);
                LocalCache.Cache.UpdateFormats();
            }
            if (IsHD)
            {
                CatchException(new Exception("Save Resource 3"));
                resource.HighResolutionFile = root + "\\" + resource.Guid.ToString() + ext;
                resource.HighResolutionFormatId = format.FormatId;
            }
            else
            {
                resource.LowResolutionFile = root + "\\" + resource.Guid.ToString() + ext;
                resource.LowResolutionFormatId = format.FormatId;
            }
            if (from != null && to != null)
            {
                if (File.Exists(to))
                {
                    File.Delete(to);
                }
                File.Move(from, to);
            }
            resource.LastUpdateDate = DateTime.UtcNow;
            resource.ResourceStatus = ResourceStatuses.Completed;
            if (string.IsNullOrEmpty(resource.ThumbUrl))
            {
                try
                {
                    string str = HttpRuntime.AppDomainAppPath;
                    if (ResourceHelper.GenerateSnapshot(HttpRuntime.AppDomainAppPath + "\\ffmpeg\\", "\\\\" + serverIp + root + "\\" + resource.Guid.ToString() + ext, "\\\\" + serverIp + ConfigurationManager.AppSettings["MediaLocation"] + "\\Thumbs\\", resource.Guid.ToString() + ".jpg", (ResourceTypes)resource.ResourceTypeId))
                    {
                        resource.ThumbUrl = ConfigurationManager.AppSettings["MediaLocation"] + "\\Thumbs\\" + resource.Guid.ToString() + ".jpg";
                    }
                    if ((ResourceTypes)resource.ResourceTypeId == ResourceTypes.Video || (ResourceTypes)resource.ResourceTypeId == ResourceTypes.Audio)
                    {
                        var duration = FFMPEG.GetDurationInMilliSeconds("\\\\" + serverIp + resource.HighResolutionFile) / 1000;
                        if (duration > 0)
                            resource.Duration = duration;
                    }
                }
                catch (Exception ex)
                {
                    CatchException(ex);
                }
            }
            return resourceService.UpdateResource(resource);
        }
        [HttpPost]
        [ActionName("PostHeader")]
        public HttpResponseMessage PostHeader(string inputStream)
        {
            BucketController bController = new BucketController();

            Core.DataTransfer.Resource.PostInput resource = new JavaScriptSerializer().Deserialize<Core.DataTransfer.Resource.PostInput>(inputStream);

            resource.ResourceStatusId = ((int)ResourceStatuses.TempFile).ToString();
            Core.DataTransfer.Resource.PostOutput result = bController.PostResource(resource);

            return UploadFile(HttpContext.Current, result);
        }
        private HttpResponseMessage WriteJsonIframeSafe(HttpContext context, List<FilesStatus> statuses)
        {
            context.Response.AddHeader("Vary", "Accept");
            var response = new HttpResponseMessage()
            {
                // Content = new StringContent(_js.Serialize(statuses.ToArray()))
            };
            if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
            {
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }
            return response;
        }

        private HttpResponseMessage UploadFile(HttpContext context, Core.DataTransfer.Resource.PostOutput resource)
        {
            var statuses = new List<FilesStatus>();
            var headers = context.Request.Headers;

            if (string.IsNullOrEmpty(headers["X-File-Name"]))
            {
                UploadWholeFile(context, statuses, resource);
            }

            return WriteJsonIframeSafe(context, statuses);
        }

        private void UploadWholeFile(HttpContext context, List<FilesStatus> statuses, Core.DataTransfer.Resource.PostOutput resource)
        {
            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                Guid guid = new Guid();
                if (Guid.TryParse(Convert.ToString(resource.Guid), out guid))
                {
                    fullRootPath = context.Request.Headers["fullPath"];
                    fullRootPath = fullRootPath.Replace("\\", "\\\\");
                    string _thumbnail_url = string.Empty;
                    var file = context.Request.Files[i];
                    _storageRoot = fullRootPath;
                    string fullPath = _storageRoot + Path.GetFileName(guid.ToString()) + Path.GetExtension(file.FileName);
                    Directory.CreateDirectory(_storageRoot);
                    file.SaveAs(fullPath);

                    string fullName = Path.GetFileName(guid.ToString()) + Path.GetExtension(file.FileName);
                    statuses.Add(new FilesStatus(fullName, file.ContentLength, fullPath) { thumbnail_url = _thumbnail_url });
                    CustomSnapshotInput input = new CustomSnapshotInput();
                    input.FilePath = fullPath;
                    input.Id = guid.ToString();
                    input.ResourceTypeId = resource.ResourceTypeId;
                    GenerateCustomSnapshot(input);
                }
            }
        }

        private void logresource(object param)
        {
            LogResource outerresource = param as LogResource;
            try
            {
                LogResource(outerresource.UserId, outerresource.Guid, outerresource.AccessTypeId);
            }
            catch
            {

            }
        }

        private void GenerateFileInformation(object param)
        {
            Resource outerresource = param as Resource;


            try
            {
                resourceService.GenerateFileInformation(outerresource, outerresource.Source);
            }
            catch (Exception exp)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            }
        }

        private void UpdateLastAccessDate(object param)
        {
            try
            {
                Resource outerresource = param as Resource;
                Resource resource = resourceService.GetResourceByGuidId(outerresource.Guid);
                if (resource != null)
                {

                    resource.LastAccesDate = DateTime.UtcNow;
                    if (resource.AccessCount == null)
                        resource.AccessCount = 0;
                    resource.AccessCount += 1;
                    resourceService.UpdateResource(resource);
                }
            }
            catch (Exception exp) { }
        }

        [HttpPost]
        [ActionName("ResourceProgess")]
        public List<MS.Core.DataTransfer.UserFavourites.PostOutput> ResourceProgess()
        {
            List<MS.Core.DataTransfer.UserFavourites.PostOutput> items = new List<Core.DataTransfer.UserFavourites.PostOutput>();
            return items;
        }
        [HttpGet]
        [ActionName("LogResource")]
        public DataTransfer<bool> LogResource(int UserId, string ResourceGuid, int AccessTypeId)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                ResourceLog log = new ResourceLog();
                log.AccessTypeId = AccessTypeId;
                Guid result;
                if (Guid.TryParse(ResourceGuid, out result))
                {
                    log.ResourceGuid = result;
                }

                log.UserId = UserId;
                log.CreationDate = DateTime.UtcNow;
                resourceLogService.InsertResourceLog(log);
                Resource resource = resourceService.GetResourceByGuidId(result);
                if (resource != null)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateLastAccessDate), (object)resource);
                }

                transfer.Data = true;
                transfer.IsSuccess = true;
            }
            catch (Exception ex)
            {
                return new DataTransfer<bool>() { Errors = new string[] { ex.Message }, IsSuccess = false };
            }

            return transfer;
        }


    }

    #region FileStatus
    public class FilesStatus
    {
        public const string HandlerPath = "/";

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }

        public FilesStatus()
        {
        }

        public FilesStatus(FileInfo fileInfo)
        {
            SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName);
        }

        public FilesStatus(string fileName, int fileLength, string fullPath)
        {
            SetValues(fileName, fileLength, fullPath);
        }

        private void SetValues(string fileName, int fileLength, string fullPath)
        {
            name = fileName;
            type = "image/png";
            size = fileLength;
            progress = "1.0";
            url = HandlerPath + "api/ResourceUploader?f=" + fileName;
            delete_url = HandlerPath + "api/ResourceUploader?f=" + fileName;
            delete_type = "DELETE";
            var ext = Path.GetExtension(fullPath);
            var fileSize = ConvertBytesToMegabytes(new FileInfo(fullPath).Length);
            //if (fileSize > 3 || !IsImage(ext))
            //{
            //    thumbnail_url = "/Content/img/generalFile.png";
            //}
            //else
            // {
            //thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
            ///////////////thumbnail_url = fullPath;
            //}
        }

        private bool IsImage(string ext)
        {
            return ext == ".gif" || ext == ".jpg" || ext == ".png";
        }

        private string EncodeFile(string fileName)
        {
            byte[] bytes;
            using (Image image = Image.FromFile(fileName))
            {
                var ratioX = (double)80 / image.Width;
                var ratioY = (double)80 / image.Height;
                var ratio = Math.Min(ratioX, ratioY);
                var newWidth = (int)(image.Width * ratio);
                var newHeight = (int)(image.Height * ratio);
                var newImage = new Bitmap(newWidth, newHeight);
                Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
                ImageConverter converter = new ImageConverter();
                bytes = (byte[])converter.ConvertTo(newImage, typeof(byte[]));
                newImage.Dispose();
            }
            return Convert.ToBase64String(bytes);
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }
    #endregion
}

