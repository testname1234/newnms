﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdvanceSearch.aspx.cs" Inherits="MS.Web.AdvanceSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="ui-widget">
        <br />
        <br />
    Search Media  <asp:TextBox ID="txtSearch" runat="server" CssClass="tb" ClientIDMode="Static" autofocus="" ></asp:TextBox> 
         <select  id="ddlResourceType" >
         <option value="2">Video</option>
         <option value="1">Images</option>
         </select>
          <input  type="checkbox" id="isHd" value="HD"  onclick="OnHdcheckChanged();" title="HD" style="margin-top:5px; display:none;"/><label style="margin-left:5px; margin-right:5px; margin-top:5px; display:none; ">HD</label>
        <input id="btnAdvanceSearch" type="button" value="Search"  />
        <br />
        <ul id="myTags">
        </ul>

    <label id="lblCounts"></label>
    <ul id="SearchMediacontent" class="newsColume">
    </ul>
        <div style="margin-left:500px;">
        <input id="btnprev" type="button" value="Prev"  style= "display:none;"   />
        <input id="btnMore" type="button" value="Next"  style=" margin-left:5px; display:none;"  />
        </div>
         <input id="hfpagenumber" type="hidden" value="1" />

    </div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".tb").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "api/Resource/SearchCelebrity?term=" + request.term + "&pageSize=10&pageNumber=1",
                    dataType: "json",
                    type: "Get",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                       
                        console.log(data.Data);
                        response($.map(data.Data, function (item) {

                            console.log(item);
                            return {
                                value: item
                            }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 2,
            allowSpaces:true

        });
        var availableTags = [];
        $("#myTags").tagit({
            allowSpaces: true,
            autocomplete: {
                delay: 0,
                minLength: 3,
               
                select: function (event, ui) {
                   // debugger;
                    //console.log(ui.item);
                    // select(ui.item);
                    // addTag(ui.item);
                },
                tagSource:availableTags,
                source: function (request, response) {
                    $.ajax({
                        url: "api/Resource/SearchTag?term=" + request.term + "&pageSize=10&pageNumber=1",
                        dataType: "json",
                        type: "Get",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            
                            console.log(data.Data);
                            response($.map(data.Data, function (item) {

                              //  console.log(item);
                                return {
                                    value: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                  });
                },
            }
        });
    });

</script>
</asp:Content>
