﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace MS.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();


            config.Routes.MapHttpRoute(
                    name: "ActionApi",
                    routeTemplate: "api/{controller}/{action}/{id}",
                    defaults: new { id = RouteParameter.Optional }
                );

            config.Routes.MapHttpRoute(
                    name: "ResourceGet",
                    routeTemplate: "api/{controller}/{action}/{id}/{IsHD}",
                    defaults: new { id = RouteParameter.Optional, IsHD = RouteParameter.Optional }
                );

            config.Routes.MapHttpRoute(
                name: "ControllerAndAction",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }

            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                  name: "anything",
                  routeTemplate: "{Bucket}/{*FilePath}",
                  defaults: new { FilePath = RouteParameter.Optional, controller = "Bucket", action = "GetFile" }
              );


            var formatter = new JsonMediaTypeFormatter();
            config.Formatters.Insert(0, formatter);
            config.Formatters.Remove(config.Formatters.XmlFormatter);

        }
    }
}
