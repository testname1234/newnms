﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BucketAuth.aspx.cs" Inherits="MS.Web.BucketAuth" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Home - Media Server</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="content/style.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="mainwrap">
            <header>
                <section class="clear">
                    <div class="userinfo">
                        <span id="userName">Media Server</span>
                    </div>
                </section>
            </header>
            <div class="innnerwrap">
              
                 <table>
                    <tr>
                        <td colspan="2" align="center" class="padding">
                            <h1>Add Bucket Authoriazation</h1>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Buckets</td>
                        <td class="padding">
                            <asp:DropDownList ID="ddlBuckets" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Users</td>
                        <td class="padding">
                           <asp:DropDownList ID="ddlUsers" runat="server">
                           </asp:DropDownList>
                        </td>
                    </tr>
                     <tr>
                        <td class="padding">IsRead</td>
                        <td class="padding">
                          <asp:CheckBox ID="chkIsRead" runat="server" />
                        </td>
                    </tr>
                      <tr>
                        <td class="padding">IsWrite</td>
                        <td class="padding">
                          <asp:CheckBox ID="chkIsWrite" runat="server" />
                        </td>
                    </tr>
                      <tr>
                        <td colspan="2" class="padding">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" />
                        </td>
                    </tr>
                 </table>

                <asp:GridView ID="gvBucketAuthorization" runat="server"> 
                    <Columns>
                    <asp:TemplateField ItemStyle-Width = "30px"  HeaderText = "CustomerID">
                        <ItemTemplate>
                            <asp:Label ID="lblBucketId" runat="server"
                            Text='<%# Eval("BucketId")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
        </div>
    </form>
</body>
</html>