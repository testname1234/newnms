﻿using MS.Core;
using MS.Core.DataTransfer;
using MS.Core.Entities;
using MS.Core.IService;
using MS.Web.Models;
using NMS.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web
{
    public partial class BucketAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                IBucketAuthorizationService service = IoC.Resolve<IBucketAuthorizationService>("BucketAuthorizationService");
                GetAllBucketAuth(service);


                IBucketService bucketservice = IoC.Resolve<IBucketService>("BucketService");

                List<Bucket> buckets = bucketservice.GetAllBucket();

                ddlBuckets.DataSource = buckets;
                ddlBuckets.DataTextField = "Name";
                ddlBuckets.DataValueField = "BucketId";
                ddlBuckets.DataBind();

                ddlBuckets.Items.Insert(0, new ListItem("-- Select --", ""));
                HttpWebRequestHelper helper = new HttpWebRequestHelper();

               // HttpWebResponse output = helper.GetRequest("http://10.3.12.120/api/admin/GetUsersByWorkRoleId?workRolesId=1", null, false);


                using (var w = new WebClient())
                {
                   DataTransfer<List<NMSUser>> users = new DataTransfer<List<NMSUser>>();
                    var json_data = string.Empty;
                    // attempt to download JSON data as a string
                    try
                    {
                        json_data = w.DownloadString("http://10.3.12.120/api/admin/ModuleUsers?id=1");
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        users = serializer.Deserialize<DataTransfer<List<NMSUser>>>(json_data);
                        if (users.Data.Count > 1)
                        {
                            ddlUsers.DataSource = users.Data;
                            ddlUsers.DataTextField = "LoginId";
                            ddlUsers.DataValueField = "UserId";
                            ddlUsers.DataBind();
                        }
                    }
                    catch (Exception) { }
                   
                }
            }
        }

        private void GetAllBucketAuth(IBucketAuthorizationService service)
        {
            List<BucketAuthorization> lstba = service.GetAllBucketAuthorization();
            gvBucketAuthorization.DataSource = lstba;
            gvBucketAuthorization.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            IBucketAuthorizationService service = IoC.Resolve<IBucketAuthorizationService>("BucketAuthorizationService");
            BucketAuthorization auth = new BucketAuthorization();
            auth.BucketId = Convert.ToInt32(ddlBuckets.SelectedValue);
            auth.UserId = Convert.ToInt32(ddlUsers.SelectedValue);
            auth.IsRead = chkIsRead.Checked;
            auth.IsWrite = chkIsWrite.Checked;
            service.InsertBucketAuthorization(auth);
            GetAllBucketAuth(service);
            

        }
    }
}