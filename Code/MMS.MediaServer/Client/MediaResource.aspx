﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MediaResource.aspx.cs" Inherits="MS.Web.Client.MediaResource" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        
.imageSection {
margin-left: 0;
background: #000;
position: relative;
text-align: center;
height: 458px;
}

.slideImg.floatleft {
float: left;
display: block;
width: 30%;
background: #fff;
height: 100%;
}
.slideImg .leftContent {
float: left;
width: 100%;
min-height: 110px;
height: 100%;
}
.leftContent .leftNews{width:auto;margin-right:12px;float:left}
.leftContent .newsImage{display:block;float:left;margin-right:10px;width:135px}
.leftContent .newsImage img{max-width:100%;margin-bottom:5px}
.leftContent .leftNews h3{font-size:13px;float:left;text-align:left}
.leftContent .leftNews h3 a{color:#3079ed}
.leftContent .contentScare{float:left;width:100%;min-height:110px;max-height:325px}
.leftContent .contentScare p{margin-bottom:10px;line-height:18px;font-size:11px;color:#666;text-align:left}
.slideImg .leftContent p.fontsiz{margin-bottom:10px;line-height:18px;font-size:12px!important;color:#666}



    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="imageSection">
            <div class="slideImg floatleft" data-bind="with: currentContent">
                <div class="leftContent">
                    <div class="leftNews">
                        <h3><a><asp:Label ID="lblslug" runat="server"></asp:Label></a></h3>
                    </div>

                    <div class="contentScare" style="overflow: hidden;">
                        <p><b>BarCode: </b>   <asp:Label ID="lblBarcode" runat="server"></asp:Label>  </p> 
                         <p><asp:Label ID="lblDescription" runat="server"></asp:Label>  </p>
                    </div>
                </div>
            </div>
            <div class="slideImg half" >
               
        <%--  <iframe id="Videosource"  width="595px"   height ="390px" visible="false"  runat="server" ></iframe>--%>
            <%--<video width="595px"  height ="390px"   controls="" autoplay="" id="fg" name="media"><source id="sad" runat="server" type="video/mp4" /> </video>--%>
            <video width="595px"  height ="390px"   controls="" autoplay="" name="media" id="vidsource2" runat="server" ><source id="Videosource3"  visible="false" runat="server" type="video/mp4"/></video>
            <asp:Image ID="source" runat="server" Visible="false" width="595px"  height ="390px" />
            </div>


      
            <div class="slideImg contentstats">
              <span>Media Files </span>
              <span data-bind="text:$root.currentIndex() + 1">1</span>
              <span>of</span>
              <span data-bind="text:$root.contentList().length">50</span>
            </div>

        </div>
    </form>
</body>
</html>

