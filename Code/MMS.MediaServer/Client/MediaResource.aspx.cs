﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MS.Core.Enums;

namespace MS.Web.Client
{
    public partial class MediaResource : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Guid"] != null)
                {

                    //IResourceDetailService service = IoC.Resolve<IResourceDetailService>("ResourceDetailService");
                    //ResourceDetail detail = null;

                    //if (Request.QueryString["Barcode"] != "undefined" && Request.QueryString["ResourceTypeId"] != null)
                    //{
                    //    detail = service.GetResourceDetailByBarCodebyResourceType(Convert.ToInt32(Request.QueryString["Barcode"].ToString()), Convert.ToInt32(Request.QueryString["ResourceTypeId"].ToString()));
                    //}

                    //if (detail != null)
                    //{
                    //    lblslug.Text = detail.Slug;
                    //    lblBarcode.Text = detail.Barcode.ToString();
                    //    lblDescription.Text = detail.Description;

                    //}

                    if (Convert.ToInt32(Request.QueryString["ResourceTypeId"].ToString()) == (int)ResourceTypes.Image)
                    {
                       //  vidsource.Visible = false;
                        vidsource2.Visible = false;
                        source.Visible = true;

                        if (Request.QueryString["IsHd"] != null)
                        {
                            source.ImageUrl = ConfigurationManager.AppSettings["MsApi"].ToString() + "/Api/Resource/GetResource/" + Request.QueryString["Guid"].ToString() + "?IsHd=True";
                        }
                        else
                        {
                            source.ImageUrl = ConfigurationManager.AppSettings["MsApi"].ToString() + "/Api/Resource/GetResource/" + Request.QueryString["Guid"].ToString();
                        }
                    }
                    else if (Convert.ToInt32(Request.QueryString["ResourceTypeId"].ToString()) == (int)ResourceTypes.Video)
                    {
                      //  vidsource.Visible = true;
                        Videosource3.Visible = true;
                        source.Visible = false;

                        if (Request.QueryString["IsHd"] != null)
                        {
                            vidsource2.Attributes["src"] =  ConfigurationManager.AppSettings["MsApi"].ToString() + "/Api/Resource/GetResource/" + Request.QueryString["Guid"].ToString() + "?IsHd=True";
                        }
                        else
                        {
                            Videosource3.Attributes["src"] =  ConfigurationManager.AppSettings["MsApi"].ToString() + "/Api/Resource/GetResource/"  + Request.QueryString["Guid"].ToString();
                        }
                    }
                }
            }
        }
    }
}