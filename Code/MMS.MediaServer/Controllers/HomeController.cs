﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MS.Core.Entities;
using MS.Core.IService;
using MS.Web.Models;
using NMS.Core;

namespace MS.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: 
        IBucketCredentialsService bucketcredentialService = IoC.Resolve<IBucketCredentialsService>("BucketCredentialsService");

        [AllowAnonymous]
        public ActionResult Home()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Home(BucketLogin model)
        {
            
            if (ModelState.IsValid)
            {
                try
                {
                    BucketCredentials bucketcreds = bucketcredentialService.GetBucketByLoginIdAndApiKey(model.LoginId, model.ApiKey);
                    //Bucket bucket = bucketService.GetBucketByLoginIdAndApiKey(model.LoginId,model.ApiKey);

                    if (bucketcreds != null)
                    {
                        // ViewData["Errors"] = @"<font color = ""Red"" > Name already exist </font>";
                        return  RedirectToAction("Admin");
                    }
                   
                    ModelState.AddModelError("LoginId", "Credentials invalid");
                   // ViewData["Errors"] = "";
                   
                }
                catch (Exception ex)
                {

                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);

        }

        #region Index
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            BucketModel model = new BucketModel();
            model.Servers = GetServers();
            model.Buckets = GetBuckets();
            return View(model);
        }
       
        private static List<Core.Entities.Server> GetServers()
        {
            IServerService serverService = IoC.Resolve<IServerService>("ServerService");
            List<MS.Core.Entities.Server> lstServer = serverService.GetAllServer();
            return lstServer;
        }
        private static List<Core.Entities.Bucket> GetBuckets()
        {
            IBucketService serverService = IoC.Resolve<IBucketService>("BucketService");
            IBucketCredentialsService buckcredService = IoC.Resolve<IBucketCredentialsService>("BucketCredentialsService");
            List<MS.Core.Entities.Bucket> lstServer = serverService.GetAllBucket().Where(a=>a.ParentId==null).ToList();
            foreach (MS.Core.Entities.Bucket item in lstServer)
            {

                BucketCredentials credential = buckcredService.GetBucketCredentialsByBucketId(item.BucketId).First();
                item.ApiKey = credential.ApiKey;
                item.LoginId = credential.LoginId;
            }
            return lstServer;
        }

        [HttpGet]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ShowBuckets()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(BucketModel model)
        {
            model.Servers = GetServers();
            if (ModelState.IsValid)
            {
                try
                {
                    IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
                    Bucket bucket = bucketService.GetBucketByName(model.Name);

                    if (bucket != null)
                    {
                        // ViewData["Errors"] = @"<font color = ""Red"" > Name already exist </font>";
                        ModelState.AddModelError("Name", "Name already exist");
                        return View(model);
                    }

                    Bucket bucket2 = bucketService.GetBucketByPath(model.Path);
                    if (bucket2 != null)
                    {
                        ModelState.AddModelError("Path", "Path already exist");
                        return View(model);
                    }


                    ViewData["Errors"] = "";
                    AddBucket(model);
                    CreateFolder(model);
                }
                catch (Exception ex)
                {

                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private static void CreateFolder(BucketModel model)
        {
            string basepath = string.Empty, basePathLowRes = string.Empty, basePathThumb = string.Empty, basePathTemp = string.Empty;

            IServerService serverService = IoC.Resolve<IServerService>("ServerService");
            //Server server = serverService.GetServer(Convert.ToInt32(model.ServerId));
            //basepath = server.DefaultBucketPath;
            Server server = serverService.GetDefaultServer();
            basepath = ConfigurationManager.AppSettings["MediaLocation"].ToString();
            basePathLowRes = ConfigurationManager.AppSettings["MediaLocationLowRes"].ToString();
            basePathThumb = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
            basePathTemp = ConfigurationManager.AppSettings["MediaLocationTemp"].ToString();

            if (!Directory.Exists("\\\\" + server.ServerIp + basepath + model.Path))
            {
                Directory.CreateDirectory("\\\\" + server.ServerIp + basepath + model.Path);
            }
            if (!Directory.Exists("\\\\" + server.ServerIp + basePathLowRes + model.Path))
            {
                Directory.CreateDirectory("\\\\" + server.ServerIp + basePathLowRes + model.Path);
            }
            if (!Directory.Exists("\\\\" + server.ServerIp + basePathThumb + model.Path))
            {
                Directory.CreateDirectory("\\\\" + server.ServerIp + basePathThumb + model.Path);
            }
            if (!Directory.Exists("\\\\" + server.ServerIp + basePathTemp + model.Path))
            {
                Directory.CreateDirectory("\\\\" + server.ServerIp + basePathTemp + model.Path);
            }
            
        }
        private static void AddBucket(BucketModel model)
        {
            IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
            IBucketCredentialsService credentialService = IoC.Resolve<IBucketCredentialsService>("BucketCredentialsService");
            IServerService serverService = IoC.Resolve<IServerService>("ServerService");

            Server defaultServer = serverService.GetDefaultServer();
            Bucket thisBucket = new Bucket();
            thisBucket.Name = model.Name;
            thisBucket.Path = model.Path;
            thisBucket.AutoSync = model.isAutoSynchronized;
            thisBucket.Size = model.Size;
            thisBucket.CreationDate = DateTime.UtcNow;
            thisBucket.LastUpdateDate = thisBucket.CreationDate;
            thisBucket.IsParent = true;

            if (model.ServerId == 0)
                thisBucket.ServerId = null;
            else
                thisBucket.ServerId = defaultServer.ServerId;

            thisBucket = bucketService.InsertBucket(thisBucket);

            BucketCredentials credentials = new BucketCredentials();
            credentials.BucketId = thisBucket.BucketId;
            credentials.LoginId = thisBucket.Name;
            credentials.ApiKey = System.Web.Security.Membership.GeneratePassword(10, 2);
            credentials = credentialService.InsertBucketCredentials(credentials);
        }

        #endregion
    }
}
