﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SolrManager;
using SolrManager.InputEntities;
using MS.Repository;
using MS.Core.Entities;
using MS.Core;

namespace MS.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            string solrCollection = ConfigurationManager.AppSettings["solrUrl2"];
            SolrService<SolrResource> sServer = new SolrService<SolrResource>();
            sServer.Connect(solrCollection);

            string solrCollection3 = ConfigurationManager.AppSettings["solrUrl3"];
            SolrService<SolrManager.InputEntities.ResourceMeta> sServer1 = new SolrService<SolrManager.InputEntities.ResourceMeta>();
            sServer1.Connect(solrCollection3);
        }
    }
}