﻿using MS.Core.DataTransfer;
using MS.Web.Models;
using NMS.Core.Helper;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            MMSLoadInitialDataLoginInput mmsInput = new MMSLoadInitialDataLoginInput();
            mmsInput.LoginId = txtUserId.Text;
            mmsInput.Password = txtPassword.Text;
            DataTransfer<MMSLoadInitialDataLoginOutput> output = helper.PostRequest<DataTransfer<MMSLoadInitialDataLoginOutput>>("http://10.3.12.120/api/admin/Authenticate", mmsInput, null);

            if (output != null && output.IsSuccess == true)
            {
                if (output.Data.UserId > 0)
                {
                    Response.Redirect("~/MsAdmin/Home.aspx");
                }
                else
                {
                    //FailureText.Visible = true;
                    //FailureText.Text = "Invalid Credentials";
                }
            }
            else
            {
                //FailureText.Visible = true;
                //FailureText.Text = "Invalid Credentials";

            }
        }
    }
}