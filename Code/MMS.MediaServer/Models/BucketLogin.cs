﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MS.Web.Models
{
    public class BucketLogin
    {
        [Required]
        [Display(Name = "LoginId")]
        public string LoginId { get; set; }

        [Required]
        [Display(Name = "ApiKey")]
        public string ApiKey { get; set; }
    }
}