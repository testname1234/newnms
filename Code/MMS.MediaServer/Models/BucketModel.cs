﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MS.Core.Entities;

namespace MS.Web.Models
{
    public class BucketModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Path")]
        public string Path { get; set; }

      
        [Display(Name = "AutoSynchronization")]
        public bool isAutoSynchronized { get; set; }

        [Required]
       // [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Size")]
        public double Size { get; set; }

        
        public int? ServerId { get; set; }
       
        public List<Server> Servers { get; set; }
        public List<Bucket> Buckets { get; set; }

    }
}