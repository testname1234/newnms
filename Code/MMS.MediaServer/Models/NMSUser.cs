﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MS.Web.Models
{
    [Serializable]
    public class NMSUser
    {
        public string Name { get { return Fullname + "-" + Designation; } }

       
        public int? WorkRoleId { get; set; }

       
        public virtual System.Int32 UserId { get; set; }

       
        public virtual System.String LoginId { get; set; }

        
        public virtual System.String Password { get; set; }

       
        public virtual System.String Fullname { get; set; }

        
        public virtual System.String Department { get; set; }

       
        public virtual System.String Designation { get; set; }

        
        public virtual System.Boolean? IsActive { get; set; }

        public virtual System.DateTime? CreationDate { get; set; }
       
        public virtual System.DateTime? LastUpdateDate { get; set; }
       
        public virtual System.String UpdationIp { get; set; }
        
        public virtual System.String EmailAddress { get; set; }

    }
}