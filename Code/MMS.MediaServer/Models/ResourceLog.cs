﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MS.Web.Models
{
    public class LogResource
    {
        public int UserId { get; set; }
        public string Guid { get; set; }
        public int AccessTypeId { get; set; }
    }
}