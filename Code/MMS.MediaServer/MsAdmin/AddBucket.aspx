﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MsAdmin/Admin.Master" AutoEventWireup="true" CodeBehind="AddBucket.aspx.cs" Inherits="MS.Web.MsAdmin.AddBucket" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <table>
                    <tr>
                        <td colspan="2" align="center" class="padding">
                            <h1>Add Bucket</h1>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Name</td>
                        <td class="padding">
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="padding">Path</td>
                        <td class="padding">
                            <asp:TextBox ID="txtPath" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="padding">Enable AutoSynchronization</td>
                        <td class="padding">
                            <asp:CheckBox ID="chkEnable" runat="server" Text="Yes" />
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Size</td>
                        <td class="padding">
                            <asp:TextBox ID="txtSize" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="padding">Requested Server</td>
                        <td class="padding">
                            <asp:DropDownList ID="ddlServer" runat="server"></asp:DropDownList></td>
                    </tr>
                <tr>
                <td class="padding">IsPrivate</td>
                <td class="padding"><asp:CheckBox ID="chkisprivate" runat="server" /></td>
               </tr>
             <tr>
                <td class="padding">Allow Transcode</td>
                <td class="padding"><asp:CheckBox ID="chktranscode" runat="server" /></td>
            </tr>
                    <tr>
                        <td class="padding">
                          <asp:HyperLink ID="linkBack" runat="server" Text="Back" NavigateUrl="~/MsAdmin/Buckets.aspx"></asp:HyperLink>
                        </td>
                        <td>
                             <asp:Button ID="btnSubmit" runat="server" Text="Add" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>
</asp:Content>
