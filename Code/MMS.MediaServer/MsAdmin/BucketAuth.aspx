﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MsAdmin/Admin.Master" AutoEventWireup="true" CodeBehind="BucketAuth.aspx.cs" Inherits="MS.Web.MsAdmin.BucketAuth" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <table>
                    <tr>
                        <td colspan="2" align="center" class="padding">
                            <h1>Add Bucket Authorization</h1>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Buckets</td>
                        <td class="padding">
                            <asp:DropDownList ID="ddlBuckets" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Users</td>
                        <td class="padding">
                           <asp:DropDownList ID="ddlUsers" runat="server">
                           </asp:DropDownList>
                        </td>
                    </tr>
                     <tr>
                        <td class="padding">IsRead</td>
                        <td class="padding">
                          <asp:CheckBox ID="chkIsRead" runat="server" />
                        </td>
                    </tr>
                      <tr>
                        <td class="padding">IsWrite</td>
                        <td class="padding">
                          <asp:CheckBox ID="chkIsWrite" runat="server" />
                        </td>
                    </tr>
                      <tr>
                        <td colspan="2" class="padding">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" />
                        </td>
                    </tr>
                 </table>

                <asp:GridView ID="gvBucketAuthorization" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gvBucketAuthorization_RowCommand"> 
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                         <asp:BoundField DataField="BucketName"   HeaderText="Bucket"/>   
                         <asp:BoundField DataField="UserName" HeaderText="User" />
                         <asp:BoundField DataField="IsRead" HeaderText="IsRead" />
                         <asp:BoundField DataField="IsWrite" HeaderText="IsWrite" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDelete"  runat="server" CommandName="Deleteauth" CommandArgument='<%# Eval("BucketAuthorizationId")%>' >Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#374760" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>

<script type="text/javascript">
    $('#ContentPlaceHolder1_ddlBuckets').select2();
    $('#ContentPlaceHolder1_ddlUsers').select2();
    
</script>
</asp:Content>
