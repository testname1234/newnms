﻿using MS.Core;
using MS.Core.DataTransfer;
using MS.Core.Entities;
using MS.Core.IService;
using MS.Web.Models;
using NMS.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web.MsAdmin
{
    public partial class BucketAuth : System.Web.UI.Page
    {
        List<Bucket> buckets;
        List<NMSUser> nmsusers;
        IBucketAuthorizationService service = IoC.Resolve<IBucketAuthorizationService>("BucketAuthorizationService");
        IBucketService bucketservice = IoC.Resolve<IBucketService>("BucketService");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

                buckets = bucketservice.GetAllBucket();

                ddlBuckets.DataSource = buckets;
                ddlBuckets.DataTextField = "Name";
                ddlBuckets.DataValueField = "BucketId";
                ddlBuckets.DataBind();

                ddlBuckets.Items.Insert(0, new ListItem("-- Select --", ""));
                HttpWebRequestHelper helper = new HttpWebRequestHelper();

                // HttpWebResponse output = helper.GetRequest("http://10.3.12.120/api/admin/GetUsersByWorkRoleId?workRolesId=1", null, false);


                using (var w = new WebClient())
                {
                    DataTransfer<List<NMSUser>> users = new DataTransfer<List<NMSUser>>();
                    var json_data = string.Empty;
                    // attempt to download JSON data as a string
                    try
                    {
                        json_data = w.DownloadString("http://10.3.12.120/api/admin/ModuleUsers?id=1");
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        users = serializer.Deserialize<DataTransfer<List<NMSUser>>>(json_data);
                        if (users.Data.Count > 1)
                        {
                            nmsusers = users.Data.ToList<NMSUser>();
                            ViewState["NMSUser"] = nmsusers; 
                            ddlUsers.DataSource = users.Data;
                            ddlUsers.DataTextField = "LoginId";
                            ddlUsers.DataValueField = "UserId";
                            ddlUsers.DataBind();
                        }
                    }
                    catch (Exception) { }

                }
                GetAllBucketAuth(service);
            }
        }

        private void GetAllBucketAuth(IBucketAuthorizationService service)
        {
            List<BucketAuthorization> lstba = service.GetAllBucketAuthorization();
            nmsusers = (List<NMSUser>)ViewState["NMSUser"];
            buckets = bucketservice.GetAllBucket();
            if (lstba != null)
            {
                foreach (BucketAuthorization item in lstba)
                {
                    try
                    {
                        Bucket bauth = buckets.Where(a => a.BucketId == item.BucketId).First();
                        if (bauth != null)
                        {
                            item.UserName = nmsusers.Where(a => a.UserId == item.UserId).First().LoginId;
                            item.BucketName = buckets.Where(a => a.BucketId == item.BucketId).First().Name;
                        }
                        
                    }
                    catch
                    {
                    
                    }
                }
                gvBucketAuthorization.DataSource = lstba;
                gvBucketAuthorization.DataBind();
            
            }
           
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
           
            BucketAuthorization auth = new BucketAuthorization();
            auth.BucketId = Convert.ToInt32(ddlBuckets.SelectedValue);
            auth.UserId = Convert.ToInt32(ddlUsers.SelectedValue);
            auth.IsRead = chkIsRead.Checked;
            auth.IsWrite = chkIsWrite.Checked;
            service.InsertBucketAuthorization(auth);
            GetAllBucketAuth(service);
        }

        protected void gvBucketAuthorization_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Deleteauth")
            { 
              service.DeleteBucketAuthorization(Convert.ToInt32(e.CommandArgument.ToString()));
            }
        }
    }
}