﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MsAdmin/Admin.Master" AutoEventWireup="true" CodeBehind="Buckets.aspx.cs" Inherits="MS.Web.MsAdmin.Buckets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
  <br />

    <table style="margin-left:200px;">
         <tr>
            <td></td>
             <td style="padding-left:722px;"><asp:HyperLink ID="HyperLink1" NavigateUrl="~/MsAdmin/BucketAuth.aspx" runat="server">Bucket Authorization</asp:HyperLink>  |  <asp:HyperLink ID="hl" NavigateUrl="~/MsAdmin/AddBucket.aspx" runat="server">Add Bucket</asp:HyperLink> </td>
        </tr>
          <tr>
            <td ></td>
            <td>
 <asp:GridView ID="gvBuckets" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Height="421px" Width="915px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="BucketId"   HeaderText="BucketId"/>   
            <asp:BoundField DataField="Name" HeaderText="Name" />
            <asp:BoundField DataField="Path" HeaderText="Path" />
            <asp:BoundField DataField="LoginId" HeaderText="LoginId" />
            <asp:BoundField DataField="ApiKey" HeaderText="ApiKey" />
            <asp:BoundField DataField="IsPrivate" HeaderText="IsPrivate" />
            <asp:BoundField DataField="AllowTranscode" HeaderText="AllowTranscode" />
            <asp:TemplateField HeaderText="">
               <ItemTemplate>
                   <asp:HyperLink ID="lnkSubbucket" runat="server" CommandName="showsubBucket" CommandArgument='<%# Eval("BucketId")%>' NavigateUrl= '<%# "~/MsAdmin/subBuckets.aspx?BucketId="+Eval("BucketId") %>'>Sub Buckets</asp:HyperLink>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="">
               <ItemTemplate>
                   <asp:HyperLink ID="lnkEdit" runat="server" CommandName="EditBucket" CommandArgument='<%# Eval("BucketId")%>' NavigateUrl= '<%# "~/MsAdmin/EditBucket.aspx?BucketId="+Eval("BucketId") %>'>Edit</asp:HyperLink>
               </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#374760" Font-Bold="True" ForeColor="White" HorizontalAlign="Justify" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>


            </td>
        </tr>

    </table>
    
     

</asp:Content>
