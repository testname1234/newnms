﻿using MS.Core;
using MS.Core.Entities;
using MS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web.MsAdmin
{
    public partial class Buckets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                IBucketService serverService = IoC.Resolve<IBucketService>("BucketService");
                IBucketCredentialsService buckcredService = IoC.Resolve<IBucketCredentialsService>("BucketCredentialsService");
                List<MS.Core.Entities.Bucket> lstServer = serverService.GetAllBucket().Where(a => a.ParentId == null).ToList();
                foreach (MS.Core.Entities.Bucket item in lstServer)
                {

                    BucketCredentials credential = buckcredService.GetBucketCredentialsByBucketId(item.BucketId).First();
                    item.ApiKey = credential.ApiKey;
                    item.LoginId = credential.LoginId;
                }

                gvBuckets.DataSource = lstServer;
                gvBuckets.DataBind();

            }
        }
    }
}