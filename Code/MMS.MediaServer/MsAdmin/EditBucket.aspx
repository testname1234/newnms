﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MsAdmin/Admin.Master" AutoEventWireup="true" CodeBehind="EditBucket.aspx.cs" Inherits="MS.Web.MsAdmin.EditBucket" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      
        <br />
        <br />
         
    <table style="margin-left:50px;">
            <tr>
              <td></td>
              <td></td>
            </tr>
             <tr>
                <td>Bucket</td>
                <td><asp:label ID="lblbucket" runat="server" /></td>
            </tr>
             <tr>
                <td>Path</td>
                <td><asp:label ID="lblpath" runat="server" /></td>
            </tr>

            <tr>
                <td>IsPrivate</td>
                <td><asp:CheckBox ID="chkisprivate" runat="server" /></td>
            </tr>
             <tr>
                <td>Allow Transcode</td>
                <td><asp:CheckBox ID="chktranscode" runat="server" /></td>
            </tr>
            <tr>
                <td><asp:HyperLink ID="linkBack" runat="server" Text="Back" NavigateUrl="~/MsAdmin/Buckets.aspx" ></asp:HyperLink></td>
                <td><asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" Height="38px" Width="75px" /></td>
            </tr>
        </table>

</asp:Content>
