﻿using MS.Core;
using MS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web.MsAdmin
{
    public partial class EditSubBucket : System.Web.UI.Page
    {
        IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
        MS.Core.Entities.Bucket buck;
        public int BucketId
        {
            get
            {
                return (Request.QueryString["BucketId"] != null) ? Convert.ToInt32(Request.QueryString["BucketID"].ToString()) : 0;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (BucketId != 0)
                {

                    buck = bucketService.GetBucket(BucketId);
                    lblbucket.Text = buck.Name;
                    lblpath.Text = buck.Path;
                    chkisprivate.Checked = Convert.ToBoolean(buck.IsPrivate);
                    chktranscode.Checked = Convert.ToBoolean(buck.AllowTranscode);
                }


            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            buck = bucketService.GetBucket(BucketId);
            if (buck != null)
            {
                buck.IsPrivate = chkisprivate.Checked;
                buck.AllowTranscode = chktranscode.Checked;
                bucketService.UpdateBucket(buck);
            }
        }
    }
}