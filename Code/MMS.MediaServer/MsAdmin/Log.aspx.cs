﻿using MS.Core;
using MS.Core.DataTransfer;
using MS.Core.IService;
using MS.Core.Models;
using MS.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web.MsAdmin
{
    public partial class Log : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IResourceLogService  log = IoC.Resolve<IResourceLogService>("ResourceLogService");
                List<UserLog> logs = log.getAllUSerLog();
                List<NMSUser> nmsusers = new List<NMSUser>();

                using (var w = new WebClient())
                {
                    DataTransfer<List<NMSUser>> users = new DataTransfer<List<NMSUser>>();
                    var json_data = string.Empty;
                    // attempt to download JSON data as a string
                    try
                    {
                        json_data = w.DownloadString("http://10.3.12.120/api/admin/ModuleUsers?id=1");
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        users = serializer.Deserialize<DataTransfer<List<NMSUser>>>(json_data);
                        if (users.Data.Count > 1)
                        {
                            nmsusers = users.Data.ToList<NMSUser>();
                        }
                    }
                    catch (Exception) { }

                }

                foreach (UserLog item in logs)
                {
                    try
                    {
                        item.UserName = nmsusers.Where(a => a.UserId == Convert.ToInt32(item.UserId)).First().LoginId;
                    }
                    catch
                    {
                        
                        
                    }
                    
                }
                
                
                gvLogResource.DataSource = logs;
                gvLogResource.DataBind();



            }

        }

       
    }
}