﻿using MS.Core;
using MS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web.MsAdmin
{
    public partial class SubBuckets : System.Web.UI.Page
    {
        public int BucketId
        {
            get
            {
                return (Request.QueryString["BucketId"] != null) ? Convert.ToInt32(Request.QueryString["BucketID"].ToString()) : 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                IBucketService serverService = IoC.Resolve<IBucketService>("BucketService");
                IBucketCredentialsService buckcredService = IoC.Resolve<IBucketCredentialsService>("BucketCredentialsService");
                List<MS.Core.Entities.Bucket> lstServer = serverService.GetAllBucket().Where(a => a.ParentId == BucketId).ToList();
                gvBuckets.DataSource = lstServer;
                gvBuckets.DataBind();
            }
        }
    }
}