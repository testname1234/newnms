﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MsAdmin/Admin.Master" AutoEventWireup="true" CodeBehind="ViewLog.aspx.cs" Inherits="MS.Web.MsAdmin.ViewLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="gvViewLogs" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="664px" AllowPaging="true"  OnPageIndexChanging="gvViewLogs_PageIndexChanging" PageSize = "6">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
       <Columns>
           <asp:BoundField DataField="UserId" HeaderText="User" />
           <asp:TemplateField HeaderText="Resource">
               <ItemTemplate>
                   <div style="height:4px;"></div>
                 <asp:HyperLink ID="hl" Target="_blank" runat="server" NavigateUrl='<%# "http://10.3.12.119/api/resource/getresource/" + Eval("ResourceGuid")%>'>    <asp:Image ID="img"  ImageUrl='<%# "http://10.3.12.119/api/resource/getthumb/" + Eval("ResourceGuid")%>' Width="135px" Height="75px"  runat="server" /></asp:HyperLink>
                   <%--<asp:HyperLink ID="hl" runat="server" Text='<%# Eval("ResourceGuid") %>' Target="_blank" NavigateUrl='<%# "http://10.3.12.119/api/resource/getresource/" + Eval("ResourceGuid") %>' ></asp:HyperLink>--%>
               </ItemTemplate>

           </asp:TemplateField>
           <asp:TemplateField HeaderText="AccessType">
               <ItemTemplate>
                   <asp:Label ID="lbl" runat="server" Text='<%# this.GetAccessType(Eval("AccessTypeId").ToString()) %>'></asp:Label>
               </ItemTemplate>

           </asp:TemplateField>
           <asp:BoundField DataField="CreationDate" HeaderText="Date" />
       </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#374760" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
</asp:Content>
