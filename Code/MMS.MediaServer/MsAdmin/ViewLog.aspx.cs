﻿using MS.Core;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MS.Web.MsAdmin
{
    public partial class ViewLog : System.Web.UI.Page
    {
        IResourceLogService log = IoC.Resolve<IResourceLogService>("ResourceLogService");
        public int UserId {
            get
            {
                return (Request.QueryString["UserId"]!=null) ? Convert.ToInt32(Request.QueryString["UserId"].ToString()) : 0;

            }
        
        }

        public string GetAccessType(string accessTypeID)
        {
          return  Enum.Parse(typeof(AccessType), accessTypeID).ToString();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IResourceLogService log = IoC.Resolve<IResourceLogService>("ResourceLogService");
                if (UserId != 0)
                {
                    FillData(log);
                    gvViewLogs.DataBind();
                }
            }
        }

        private void FillData(IResourceLogService log)
        {
            List<ResourceLog> rl = log.GetResourceLogbyUserId(UserId);
            gvViewLogs.DataSource = rl;
        }

        protected void gvViewLogs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FillData(log);
            gvViewLogs.PageIndex = e.NewPageIndex;
            gvViewLogs.DataBind();
        }
    }
}