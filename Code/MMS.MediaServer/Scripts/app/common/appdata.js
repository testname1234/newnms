﻿define('appdata',
    ['ko', 'utils', 'moment', 'helper', 'enum'],
    function (ko, utils, moment, helper, e) {
        // Properties
        // ------------------------------

        var
            sessionKey = helper.getCookie('SessionKey'),
            currentTime = ko.observable(),
            channelLastUpdateDate = utils.getDefaultUTCDate(),
            tickerCategoryLastUpdateDate = utils.getDefaultUTCDate(),
            videoLastUpdateDate = utils.getDefaultUTCDate(),

            // Computed Properties
            // ------------------------------

            // Methods
            // ------------------------------

            extractUserInformation = function () {
                var tempUser = new User();

                tempUser.id = parseInt($.trim(helper.getCookie('user-id')));
                tempUser.name = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.displayName = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.userType = parseInt($.trim(helper.getCookie('role-id')));

                currentUser(tempUser);
            },

            init = function () {
                setInterval(function () {
                    currentTime(new Date());
                }, 1000);
            };

        init();

        return {
            extractUserInformation: extractUserInformation,
            sessionKey: sessionKey,
            channelLastUpdateDate: channelLastUpdateDate,
            tickerCategoryLastUpdateDate: tickerCategoryLastUpdateDate,
            videoLastUpdateDate: videoLastUpdateDate
        };
    });