﻿define('datacontext',
	['jquery', 'underscore', 'ko', 'model.mapper', 'model', 'config', 'utils'],
	function ($, _, ko, modelmapper, model, config, utils) {
	    var logger = config.logger,

			getCurrentUserId = function () {
			    return config.currentUser().id();
			},

			itemsToArray = function (items, observableArray, filter, sortFunction) {
			    // Maps the memo to an observableArray,
			    // then returns the observableArray
			    if (!observableArray) return;

			    // Create an array from the memo object
			    var underlyingArray = utils.mapMemoToArray(items);

			    if (filter) {
			        underlyingArray = _.filter(underlyingArray, function (o) {
			            var match = filter.predicate(filter, o);
			            return match;
			        });
			    }
			    if (sortFunction) {
			        underlyingArray.sort(sortFunction);
			    }
			    observableArray(underlyingArray);
			},

			mapToContext = function (dtoList, items, results, mapper, filter, sortFunction) {
			    // Loop through the raw dto list and populate a dictionary of the items
			    items = _.reduce(dtoList, function (memo, dto) {
			        var id = mapper.getDtoId(dto);
			        var existingItem = items[id];
			        memo[id] = mapper.fromDto(dto, existingItem);
			        return memo;
			    }, {});
			    itemsToArray(items, results, filter, sortFunction);
			    return items; // must return these
			},
			EntitySet = function (getFunction, mapper, nullo, updateFunction) {
			    var
					items = {},
					observableList = ko.observableArray([]),
					bunchDictionary = {},
					newsDictionary = {},
					filterDictionary = {},
					channelsDictionary = {},
					programsDictionary = {},
					episodeDictionary = {},
                    tickerDictionary = {},
					programElementDictionary = {},
					templateDictionary = {},
					hourlySlotDictionary = {},
					newspaperDictionary = {},
					dailyNewsPaperDictionary = {},
					newspaperDictionaryByDate = {},
					screenTemplateCamerasDictionary = {},
					screenTemplateMicsDictionary = {},
					resourceDictionary = {},
					UserRoleDictionary = {},
                    roStoryDictionary = {},
                    roStoryItemDictionary = {},
                    casperItemsDictionary = {},

					mapDtoToContext = function (dto) {
					    var id = mapper.getDtoId(dto);
					    var existingItem = items[id];
					    items[id] = mapper.fromDto(dto, existingItem);
					    return items[id];
					},

					add = function (dto, options) {
					    var newItem = mapDtoToContext(dto);

					    if (options) {
					        if (options.addToObservable) {
					            observableList.push(newItem);
					        }
					        if (options.groupByEpisodeId) {
					            if (episodeDictionary[newItem.episodeId]) {
					                if (episodeDictionary[newItem.episodeId]().length === 0) {
					                    episodeDictionary[newItem.episodeId]([newItem]);
					                }
					                else {
					                    var simpleArray = episodeDictionary[newItem.episodeId]();
					                    simpleArray.push(newItem);
					                    episodeDictionary[newItem.episodeId](simpleArray);
					                }
					            }
					            else {
					                episodeDictionary[newItem.episodeId] = ko.observableArray([newItem]);
					            }
					        }
					        if (options.groupByProgramElementId) {
					            if (programElementDictionary[newItem.programElementId]) {
					                if (programElementDictionary[newItem.programElementId]().length === 0) {
					                    programElementDictionary[newItem.programElementId]([newItem]);
					                }
					                else {
					                    var simpleArray = programElementDictionary[newItem.programElementId]();
					                    simpleArray.push(newItem);
					                    programElementDictionary[newItem.programElementId](simpleArray);
					                }
					            }
					            else {
					                programElementDictionary[newItem.programElementId] = ko.observableArray([newItem]);
					            }
					        }
					    }

					    return newItem;
					},

					addToObservableList = function (itemsArray) {
					    if (observableList().length > 0) {
					        var simpleArray = observableList();
					        for (var j = 0; j < itemsArray.length; j++) {
					            simpleArray.unshift(itemsArray[j])
					            items[itemsArray[j].id] = itemsArray[j];
					        }
					        observableList(simpleArray);
					    }
					    else {
					        observableList(itemsArray);
					    }
					},

                    addToDictionary = function (obj, refId, dictionaryId) {
                        var dictionary = {};

                        switch (dictionaryId) {
                            case 'episode':
                                dictionary = episodeDictionary;
                                break;
                            case 'program-element':
                                dictionary = programElementDictionary;
                                break;
                            default:
                                return;
                                break;
                        }

                        if (obj && refId) {
                            var tempArray = dictionary[refId]()
                            if (tempArray) {
                                tempArray.push(obj);
                                dictionary[refId](tempArray);
                            }
                        }
                    },

					removeById = function (id, removeFromObservableList) {
					    delete items[id];
					    if (removeFromObservableList) {
					        for (var i = 0; i < observableList().length; i++) {
					            if (observableList()[i].id === id) {
					                return observableList.splice(i, 1);
					            }
					        };
					    }
					},

					removeFromDictionary = function (id, refId, dictionaryId) {
					    var dictionary = {};

					    switch (dictionaryId) {
					        case 'episode':
					            dictionary = episodeDictionary;
					            break;
					        case 'program-element':
					            dictionary = programElementDictionary;
					            break;
					        default:
					            return;
					            break;
					    }

					    if (id && refId) {
					        var tempArray = dictionary[refId]()
					        if (tempArray) {
					            for (var i = 0; i < tempArray.length; i++) {
					                if (tempArray[i].id === id) {
					                    return tempArray.splice(i, 1);
					                }
					            }
					        }
					    }
					},

					getLocalById = function (id) {
					    // This is the only place we set to NULLO
					    return !!id && !!items[id] ? items[id] : nullo;
					},

					getByHourlySlotId = function (id) {
					    var array = !!id && !!hourlySlotDictionary[id] ? hourlySlotDictionary[id] : ko.observableArray([]);
					    if (!!hourlySlotDictionary[id] === false)
					        hourlySlotDictionary[id] = array;
					    return array;
					},

					getByProgramId = function (id) {
					    var array = !!id && !!programsDictionary[id] ? programsDictionary[id] : ko.observableArray([]);
					    if (!!programsDictionary[id] === false)
					        programsDictionary[id] = array;
					    return array;
					},

					getByEpisodeId = function (id) {
					    var array = !!id && !!episodeDictionary[id] ? episodeDictionary[id] : ko.observableArray([]);
					    if (!!episodeDictionary[id] === false)
					        episodeDictionary[id] = array;
					    return array;
					},

                    getByTickerId = function (id) {
                        var array = !!id && !!tickerDictionary[id] ? tickerDictionary[id] : ko.observableArray([]);
                        if (!!tickerDictionary[id] === false)
                            tickerDictionary[id] = array;
                        return array;
                    },
                    getByTickerTypeId = function (id) {
                        var array = !!id && !!tickerDictionary[id] ? tickerDictionary[id] : ko.observableArray([]);
                        if (!!tickerDictionary[id] === false)
                            tickerDictionary[id] = array;
                        return array;
                    },


					getByProgramElementId = function (id) {
					    var array = !!id && !!programElementDictionary[id] ? programElementDictionary[id] : ko.observableArray([]);
					    if (!!programElementDictionary[id] === false)
					        programElementDictionary[id] = array;
					    return array;
					},

					getByChannelId = function (id) {
					    var array = !!id && !!channelsDictionary[id] ? channelsDictionary[id] : ko.observableArray([]);
					    if (!!channelsDictionary[id] === false)
					        channelsDictionary[id] = array;
					    return array;
					},

					getByNewspaperId = function (id) {
					    var array = !!id && !!newspaperDictionary[id] ? newspaperDictionary[id] : ko.observableArray([]);
					    if (!!newspaperDictionary[id] === false)
					        newspaperDictionary[id] = array;
					    return array;
					},

					getByNewsPaperId = function (id) {
					    var array = !!id && !!newspaperDictionary[id] ? newspaperDictionary[id] : ko.observableArray([]);
					    if (!!newspaperDictionary[id] === false)
					        newspaperDictionary[id] = array;
					    return array;
					},

					getByDailyNewsPaperId = function (id) {
					    var array = !!id && !!dailyNewsPaperDictionary[id] ? dailyNewsPaperDictionary[id] : ko.observableArray([]);
					    if (!!dailyNewsPaperDictionary[id] === false)
					        dailyNewsPaperDictionary[id] = array;
					    return array;
					},

					getByDailyNewsPaperDate = function (id) {
					    var array = !!id && !!newspaperDictionaryByDate[id] ? newspaperDictionaryByDate[id] : ko.observableArray([]);
					    if (!!newspaperDictionaryByDate[id] === false)
					        newspaperDictionaryByDate[id] = array;
					    return array;
					},

					getByBunchId = function (id) {
					    var array = !!id && !!bunchDictionary[id] ? bunchDictionary[id] : ko.observableArray([]);
					    if (!!bunchDictionary[id] === false)
					        bunchDictionary[id] = array;
					    return array;
					},

					getByTemplateId = function (id) {
					    var array = !!id && !!templateDictionary[id] ? templateDictionary[id] : ko.observableArray([]);
					    if (!!templateDictionary[id] === false)
					        templateDictionary[id] = array;
					    return array;
					},

					getByParentNewsId = function (id) {
					    var array = !!id && !!newsDictionary[id] ? newsDictionary[id] : ko.observableArray([]);
					    if (!!newsDictionary[id] === false)
					        newsDictionary[id] = array;
					    return array;
					},

					getByNewsId = function (id) {
					    var array = !!id && !!newsDictionary[id] ? newsDictionary[id] : ko.observableArray([]);
					    if (!!newsDictionary[id] === false)
					        newsDictionary[id] = array;
					    return array;
					},

					getByFilterId = function (id) {
					    var array = !!id && !!filterDictionary[id] ? filterDictionary[id] : ko.observableArray([]);
					    if (!!filterDictionary[id] === false)
					        filterDictionary[id] = array;
					    return array;
					},

					getCamerasByScreenTemplateId = function (id) {
					    var array = !!id && !!screenTemplateCamerasDictionary[id] ? screenTemplateCamerasDictionary[id] : ko.observableArray([]);
					    if (!!screenTemplateCamerasDictionary[id] === false)
					        screenTemplateCamerasDictionary[id] = array;
					    return array;
					},

					getMicsByScreenTemplateId = function (id) {
					    var array = !!id && !!screenTemplateMicsDictionary[id] ? screenTemplateMicsDictionary[id] : ko.observableArray([]);
					    if (!!screenTemplateMicsDictionary[id] === false)
					        screenTemplateMicsDictionary[id] = array;
					    return array;
					},

                    getROStoriesByRunOrderId = function (id) {
                        var array = !!id && !!roStoryDictionary[id] ? roStoryDictionary[id] : ko.observableArray([]);
                        if (!!roStoryDictionary[id] === false)
                            roStoryDictionary[id] = array;
                        return array;
                    },
                    getROStoryItemsByStoryId = function (id) {
                        var array = !!id && !!roStoryItemDictionary[id] ? roStoryItemDictionary[id] : ko.observableArray([]);
                        if (!!roStoryItemDictionary[id] === false)
                            roStoryItemDictionary[id] = array;
                        return array;
                    },
                    getCasperItemsByRunOrderId = function (id) {
                        var array = !!id && !!casperItemsDictionary[id] ? casperItemsDictionary[id] : ko.observableArray([]);
                        if (!!casperItemsDictionary[id] === false)
                            casperItemsDictionary[id] = array;
                        return array;
                    },

					getResourceByScreenTemplateId = function (id) {
					    var array = !!id && !!resourceDictionary[id] ? resourceDictionary[id] : ko.observableArray([]);
					    if (!!resourceDictionary[id] === false)
					        resourceDictionary[id] = array;
					    return array;
					},

					getModuleUrlBymoduleId = function (id) {
					    var array = !!id && !!UserRoleDictionary[id] ? UserRoleDictionary[id] : ko.observableArray([]);
					    if (!!UserRoleDictionary[id] === false)
					        UserRoleDictionary[id] = array;
					    return array;
					},

					getAllLocal = function () {
					    return utils.mapMemoToArray(items);
					},

					getAllLocalByIds = function (listIds) {
					    var itemsArray = [];
					    for (var i = 0; i < listIds.length; i++) {
					        var tempObj = getLocalById(listIds[i]);
					        if (tempObj) {
					            itemsArray.push(tempObj);
					        }
					    }
					    return itemsArray;
					},

					getObservableList = function (id) {
					    if (id) {
					        var array = !!id && !!observableList()[id] ? observableList()[id] : ko.observableArray([]);
					        if (!!observableList()[id] === false)
					            observableList()[id] = array;
					        return array;
					    }
					    else {
					        return observableList();
					    }
					},

					getData = function (options) {
					    return $.Deferred(function (def) {
					        var results = options && options.results,
								sortFunction = options && options.sortFunction,
								filter = options && options.filter,
								forceRefresh = options && options.forceRefresh,
								param = options && options.param,
								getFunctionOverride = options && options.getFunctionOverride;

					        getFunction = getFunctionOverride || getFunction;

					        // If the internal items object doesnt exist,
					        // or it exists but has no properties,
					        // or we force a refresh
					        if (forceRefresh || !items || !utils.hasProperties(items)) {
					            getFunction({
					                success: function (dtoList) {
					                    items = mapToContext(dtoList, items, results, mapper, filter, sortFunction);
					                    def.resolve(results);
					                },
					                error: function (response) {
					                    logger.error(config.toasts.errorGettingData);
					                    def.reject();
					                }
					            }, param);
					        } else {
					            itemsToArray(items, results, filter, sortFunction);
					            def.resolve(results);
					        }
					    }).promise();
					},

					//fillData = function (list, options) 
                    //{
					//    if (list && list.length > 0) {

					//        if (options && options.sort) {
					//            list.sort(function (entity1, entity2) {
					//                return mapper.getSortedValue(entity1) > mapper.getSortedValue(entity2) ? 1 : -1;
					//            });
					//        }

					//        var temp = [];

					//        temp = mapToContext(list, items, null, mapper, null, null);

					//        var itemsArray = getItemsArray(temp);
					//        if (options) {
					//            var newsIds = [];
					//            var newsArray = [];
					//            var bunchIds = [];
					//            var bunchArray = [];
					//            var filterIds = [];
					//            var filterArray = [];
					//            var channelIds = [];
					//            var channelArray = [];
					//            var newspaperArray = [];
					//            var dailyNewsPaperArray = [];
					//            var newspaperArrayByDate = [];
					//            var programIds = [];
					//            var programArray = [];
					//            var episodeIds = [];
					//            var episodeArray = [];
					//            var tickerIds = [];
					//            var tickerArray = [];
					//            var programElementIds = [];
					//            var programElementArray = [];
					//            var hourlySlotIds = [];
					//            var hourlySlotArray = [];
					//            var selectedDateIds = [];
					//            var selectedDateArray = [];
					//            var newspaperIds = [];
					//            var dailyNewsPaperIds = [];
					//            var newspaperDates = [];
					//            var templateIds = [];
					//            var templateArray = [];
					//            var camerasScreenTemplateIds = [];
					//            var micsScreenTemplateIds = [];
					//            var micsScreenTemplateIds = [];
					//            var resourceScreenTemplateIds = [];
					//            var micsArray = [];
					//            var camerasArray = [];
					//            var resourceArray = [];
					//            var modulesId = [];
					//            var moduleurlarray = [];
					//            var roStoryIds = [];
					//            var roStoryArray = [];
					//            var roStoryItemsIds = [];
					//            var roStoryItemsArray = [];
					//            var casperItemIds = [];
					//            var casperItemsArray = [];
					//            var videoStatusArray = [];

					//            if (options.bunchId) {
					//                var id = options.bunchId;
					//                bunchIds.push(id);

					//                if (bunchArray[id] == null) {
					//                    bunchArray[id] = [];
					//                }

					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    bunchArray[id].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.newsId) {
					//                var id = options.newsId;
					//                newsIds.push(id);

					//                if (newsArray[id] == null) {
					//                    newsArray[id] = [];
					//                }

					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    newsArray[id].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByNewsId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (newsArray[item.newsId] == null) {
					//                        newsArray[item.newsId] = [];
					//                        newsIds.push(item.newsId);
					//                    }

					//                    newsArray[item.newsId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.filterId) {
					//                var id = options.filterId;
					//                filterIds.push(id);

					//                if (filterArray[id] == null) {
					//                    filterArray[id] = [];
					//                }

					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    filterArray[id].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByBunchId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (bunchArray[item.bunchId] == null) {
					//                        bunchArray[item.bunchId] = [];
					//                        bunchIds.push(item.bunchId);
					//                    }

					//                    bunchArray[item.bunchId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByParentNewsId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (newsArray[item.parentNewsId] == null) {
					//                        newsArray[item.parentNewsId] = [];
					//                        newsIds.push(item.parentNewsId);
					//                    }

					//                    newsArray[item.parentNewsId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByFilterId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (filterArray[item.filterId] == null) {
					//                        filterArray[item.filterId] = [];
					//                        filterIds.push(item.filterId);
					//                    }

					//                    filterArray[item.filterId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByChannelId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (channelArray[item.channelId()] == null) {
					//                        channelArray[item.channelId()] = [];
					//                        channelIds.push(item.channelId());
					//                    }

					//                    channelArray[item.channelId()].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByVideoStatusId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (videoStatusArray[item.videoStatusId()] == null) {
					//                        videoStatusArray[item.videoStatusId()] = [];
					//                        videoStatusArray.push(item.videoStatusId());
					//                    }

					//                    videoStatusArray[item.videoStatusId()].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByNewsPaperId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (newspaperArray[item.newsPaperId] == null) {
					//                        newspaperArray[item.newsPaperId] = [];
					//                        newspaperIds.push(item.newsPaperId);
					//                    }

					//                    newspaperArray[item.newsPaperId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByDailyNewsPaperId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (dailyNewsPaperArray[item.dailyNewsPaperId] == null) {
					//                        dailyNewsPaperArray[item.dailyNewsPaperId] = [];
					//                        dailyNewsPaperIds.push(item.dailyNewsPaperId);
					//                    }

					//                    dailyNewsPaperArray[item.dailyNewsPaperId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByDailyNewsPaperDate) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (newspaperArrayByDate[item.name] == null) {
					//                        newspaperArrayByDate[item.name] = [];
					//                        newspaperDates.push(item.name);
					//                    }

					//                    newspaperArrayByDate[item.name].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByProgramIdObservable) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (programArray[item.programId()] == null) {
					//                        programArray[item.programId()] = [];
					//                        programIds.push(item.programId());
					//                    }

					//                    programArray[item.programId()].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByHourlySlotId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (hourlySlotArray[item.hourlySlotId] == null) {
					//                        hourlySlotArray[item.hourlySlotId] = [];
					//                        hourlySlotIds.push(item.hourlySlotId);
					//                    }

					//                    hourlySlotArray[item.hourlySlotId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupBySelectedDateId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (selectedDateArray[item.selectedDateId] == null) {
					//                        selectedDateArray[item.selectedDateId] = [];
					//                        selectedDateIds.push(item.selectedDateId);
					//                    }

					//                    selectedDateArray[item.selectedDateId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByTemplateId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (templateArray[item.screenTemplateId] == null) {
					//                        templateArray[item.screenTemplateId] = [];
					//                        templateIds.push(item.screenTemplateId);
					//                    }

					//                    templateArray[item.screenTemplateId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByProgramId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (programArray[item.programId] == null) {
					//                        programArray[item.programId] = [];
					//                        programIds.push(item.programId);
					//                    }

					//                    programArray[item.programId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByEpisodeId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (episodeArray[item.episodeId] == null) {
					//                        episodeArray[item.episodeId] = [];
					//                        episodeIds.push(item.episodeId);
					//                    }

					//                    episodeArray[item.episodeId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupByTickerId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (tickerArray[item.tickerId] == null) {
					//                        tickerArray[item.tickerId] = [];
					//                        tickerIds.push(item.tickerId);
					//                    }

					//                    tickerArray[item.tickerId].push(itemsArray[i]);
					//                }
					//            }

					//            if (options.groupByProgramElementId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (programElementArray[item.programElementId] == null) {
					//                        programElementArray[item.programElementId] = [];
					//                        programElementIds.push(item.programElementId);
					//                    }

					//                    programElementArray[item.programElementId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupCamerasByScreenTemplateId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (camerasArray[item.storyScreenTemplateId] == null) {
					//                        camerasArray[item.storyScreenTemplateId] = [];
					//                        camerasScreenTemplateIds.push(item.storyScreenTemplateId);
					//                    }

					//                    camerasArray[item.storyScreenTemplateId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupMicsByScreenTemplateId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (micsArray[item.storyScreenTemplateId] == null) {
					//                        micsArray[item.storyScreenTemplateId] = [];
					//                        micsScreenTemplateIds.push(item.storyScreenTemplateId);
					//                    }

					//                    micsArray[item.storyScreenTemplateId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupROStoryByRunOrderId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (roStoryArray[item.roId] == null) {
					//                        roStoryArray[item.roId] = [];
					//                        roStoryIds.push(item.roId);
					//                    }

					//                    roStoryArray[item.roId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupROStoryItemsByStoryId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (roStoryItemsArray[item.storyId] == null) {
					//                        roStoryItemsArray[item.storyId] = [];
					//                        roStoryItemsIds.push(item.storyId);
					//                    }

					//                    roStoryItemsArray[item.storyId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupCasperItemsByCasperRunOrderId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (casperItemsArray[item.roId] == null) {
					//                        casperItemsArray[item.roId] = [];
					//                        casperItemIds.push(item.roId);
					//                    }

					//                    casperItemsArray[item.roId].push(itemsArray[i]);
					//                }
					//            }
					//            if (options.groupResourcesByScreenTemplateId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];
					//                    if (resourceArray[item.storyScreenTemplateId] == null) {
					//                        resourceArray[item.storyScreenTemplateId] = [];
					//                        resourceScreenTemplateIds.push(item.storyScreenTemplateId);
					//                    }

					//                    resourceArray[item.storyScreenTemplateId].push(itemsArray[i]);
					//                }
					//            }

					//            if (options.groupModuleUrlByModuleId) {
					//                for (var i = 0; i < itemsArray.length; i++) {
					//                    var item = itemsArray[i];

					//                    if (moduleurlarray[item.userId] == null) {
					//                        moduleurlarray[item.userId] = [];
					//                        modulesId.push(item.userId);
					//                    }

					//                    moduleurlarray[item.userId].push(itemsArray[i]);
					//                }
					//            }

					//            for (var i = 0; i < templateIds.length; i++) {
					//                var id = templateIds[i];
					//                if (templateDictionary[id]) {
					//                    if (templateDictionary[id]().length === 0) {
					//                        templateDictionary[id](templateArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = templateDictionary[id]();

					//                        for (var j = 0; j < templateArray[id].length; j++) {
					//                            simpleArray.push(templateArray[id][j])
					//                        }
					//                        templateDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    templateDictionary[bunchIds[i]] = ko.observableArray(templateArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < bunchIds.length; i++) {
					//                var id = bunchIds[i];
					//                if (bunchDictionary[id]) {
					//                    if (bunchDictionary[id]().length === 0) {
					//                        bunchDictionary[id](bunchArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = bunchDictionary[id]();

					//                        for (var j = 0; j < bunchArray[id].length; j++) {
					//                            simpleArray.push(bunchArray[id][j])
					//                        }
					//                        bunchDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    bunchDictionary[bunchIds[i]] = ko.observableArray(bunchArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < newsIds.length; i++) {
					//                var id = newsIds[i];
					//                if (newsDictionary[id]) {
					//                    if (newsDictionary[id]().length === 0) {
					//                        newsDictionary[id](newsArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = newsDictionary[id]();

					//                        for (var j = 0; j < newsArray[id].length; j++) {
					//                            simpleArray.push(newsArray[id][j])
					//                        }
					//                        newsDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    newsDictionary[newsIds[i]] = ko.observableArray(newsArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < filterIds.length; i++) {
					//                var id = filterIds[i];
					//                if (filterDictionary[id]) {
					//                    if (filterDictionary[id]().length === 0) {
					//                        filterDictionary[id](filterArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = filterDictionary[id]();

					//                        for (var j = 0; j < filterArray[id].length; j++) {
					//                            simpleArray.push(filterArray[id][j])
					//                        }
					//                        filterDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    filterDictionary[filterIds[i]] = ko.observableArray(filterArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < channelIds.length; i++) {
					//                var id = channelIds[i];
					//                if (channelsDictionary[id]) {
					//                    if (channelsDictionary[id]().length === 0) {
					//                        channelsDictionary[id](channelArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = channelsDictionary[id]();

					//                        for (var j = 0; j < channelArray[id].length; j++) {
					//                            simpleArray.push(channelArray[id][j])
					//                        }
					//                        channelsDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    channelsDictionary[channelIds[i]] = ko.observableArray(channelArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < newspaperIds.length; i++) {
					//                var id = newspaperIds[i];
					//                if (newspaperDictionary[id]) {
					//                    if (newspaperDictionary[id]().length === 0) {
					//                        newspaperDictionary[id](newspaperArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = newspaperDictionary[id]();

					//                        for (var j = 0; j < newspaperArray[id].length; j++) {
					//                            simpleArray.push(newspaperArray[id][j])
					//                        }
					//                        newspaperDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    newspaperDictionary[newspaperIds[i]] = ko.observableArray(newspaperArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < dailyNewsPaperIds.length; i++) {
					//                var id = dailyNewsPaperIds[i];
					//                if (dailyNewsPaperDictionary[id]) {
					//                    if (dailyNewsPaperDictionary[id]().length === 0) {
					//                        dailyNewsPaperDictionary[id](dailyNewsPaperArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = dailyNewsPaperDictionary[id]();

					//                        for (var j = 0; j < dailyNewsPaperArray[id].length; j++) {
					//                            simpleArray.push(dailyNewsPaperArray[id][j])
					//                        }
					//                        dailyNewsPaperDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    dailyNewsPaperDictionary[dailyNewsPaperIds[i]] = ko.observableArray(dailyNewsPaperArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < newspaperDates.length; i++) {
					//                var id = newspaperDates[i];
					//                if (newspaperDictionaryByDate[id]) {
					//                    if (newspaperDictionaryByDate[id]().length === 0) {
					//                        newspaperDictionaryByDate[id](newspaperArrayByDate[id]);
					//                    }
					//                    else {
					//                        var simpleArray = newspaperDictionaryByDate[id]();

					//                        for (var j = 0; j < newspaperArrayByDate[id].length; j++) {
					//                            simpleArray.push(newspaperArrayByDate[id][j])
					//                        }
					//                        newspaperDictionaryByDate[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    newspaperDictionaryByDate[newspaperDates[i]] = ko.observableArray(newspaperArrayByDate[id]);
					//                }
					//            }

					//            for (var i = 0; i < programIds.length; i++) {
					//                var id = programIds[i];
					//                if (programsDictionary[id]) {
					//                    if (programsDictionary[id]().length === 0) {
					//                        programsDictionary[id](programArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = programsDictionary[id]();

					//                        for (var j = 0; j < programArray[id].length; j++) {
					//                            simpleArray.push(programArray[id][j])
					//                        }
					//                        programsDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    programsDictionary[programIds[i]] = ko.observableArray(programArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < episodeIds.length; i++) {
					//                var id = episodeIds[i];
					//                if (episodeDictionary[id]) {
					//                    if (episodeDictionary[id]().length === 0) {
					//                        episodeDictionary[id](programArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = episodeDictionary[id]();

					//                        for (var j = 0; j < episodeArray[id].length; j++) {
					//                            simpleArray.push(episodeArray[id][j])
					//                        }
					//                        episodeDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    episodeDictionary[episodeIds[i]] = ko.observableArray(episodeArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < tickerIds.length; i++) {
					//                var id = tickerIds[i];
					//                if (tickerDictionary[id]) {
					//                    if (tickerDictionary[id]().length === 0) {
					//                        tickerDictionary[id](tickerArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = tickerDictionary[id]();

					//                        for (var j = 0; j < tickerArray[id].length; j++) {
					//                            simpleArray.push(tickerArray[id][j])
					//                        }
					//                        tickerDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    tickerDictionary[tickerIds[i]] = ko.observableArray(tickerArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < programElementIds.length; i++) {
					//                var id = programElementIds[i];
					//                if (programElementDictionary[id]) {
					//                    if (programElementDictionary[id]().length === 0) {
					//                        programElementDictionary[id](programElementArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = programElementDictionary[id]();

					//                        for (var j = 0; j < programElementArray[id].length; j++) {
					//                            simpleArray.push(programElementArray[id][j])
					//                        }
					//                        programElementDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    programElementDictionary[programElementIds[i]] = ko.observableArray(programElementArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < hourlySlotIds.length; i++) {
					//                var id = hourlySlotIds[i];
					//                if (hourlySlotDictionary[id]) {
					//                    if (hourlySlotDictionary[id]().length === 0) {
					//                        hourlySlotDictionary[id](hourlySlotArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = hourlySlotDictionary[id]();

					//                        for (var j = 0; j < hourlySlotArray[id].length; j++) {
					//                            simpleArray.push(hourlySlotArray[id][j])
					//                        }
					//                        hourlySlotDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    hourlySlotDictionary[hourlySlotIds[i]] = ko.observableArray(hourlySlotArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < camerasScreenTemplateIds.length; i++) {
					//                var id = camerasScreenTemplateIds[i];
					//                if (screenTemplateCamerasDictionary[id]) {
					//                    if (screenTemplateCamerasDictionary[id]().length === 0) {
					//                        screenTemplateCamerasDictionary[id](camerasArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = screenTemplateCamerasDictionary[id]();

					//                        for (var j = 0; j < camerasArray[id].length; j++) {
					//                            simpleArray.push(camerasArray[id][j])
					//                        }
					//                        screenTemplateCamerasDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    screenTemplateCamerasDictionary[camerasScreenTemplateIds[i]] = ko.observableArray(camerasArray[id]);
					//                }
					//            }

					//            for (var i = 0; i < micsScreenTemplateIds.length; i++) {
					//                var id = micsScreenTemplateIds[i];
					//                if (screenTemplateMicsDictionary[id]) {
					//                    if (screenTemplateMicsDictionary[id]().length === 0) {
					//                        screenTemplateMicsDictionary[id](micsArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = screenTemplateMicsDictionary[id]();

					//                        for (var j = 0; j < micsArray[id].length; j++) {
					//                            simpleArray.push(micsArray[id][j])
					//                        }
					//                        screenTemplateMicsDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    screenTemplateMicsDictionary[micsScreenTemplateIds[i]] = ko.observableArray(micsArray[id]);
					//                }
					//            }
					//            for (var i = 0; i < roStoryIds.length; i++) {
					//                var id = roStoryIds[i];
					//                if (roStoryDictionary[id]) {
					//                    if (roStoryDictionary[id]().length === 0) {
					//                        roStoryDictionary[id](roStoryArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = roStoryDictionary[id]();

					//                        for (var j = 0; j < roStoryArray[id].length; j++) {
					//                            simpleArray.push(roStoryArray[id][j])
					//                        }
					//                        roStoryDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    roStoryDictionary[roStoryIds[i]] = ko.observableArray(roStoryArray[id]);
					//                }
					//            }
					//            for (var i = 0; i < roStoryItemsIds.length; i++) {
					//                var id = roStoryItemsIds[i];
					//                if (roStoryItemDictionary[id]) {
					//                    if (roStoryItemDictionary[id]().length === 0) {
					//                        roStoryItemDictionary[id](roStoryItemsArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = roStoryItemDictionary[id]();

					//                        for (var j = 0; j < roStoryItemsArray[id].length; j++) {
					//                            simpleArray.push(roStoryItemsArray[id][j])
					//                        }
					//                        roStoryItemDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    roStoryItemDictionary[roStoryItemsIds[i]] = ko.observableArray(roStoryItemsArray[id]);
					//                }
					//            }
					//            for (var i = 0; i < casperItemIds.length; i++) {
					//                var id = casperItemIds[i];
					//                if (casperItemsDictionary[id]) {
					//                    if (casperItemsDictionary[id]().length === 0) {
					//                        casperItemsDictionary[id](casperItemsArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = casperItemsDictionary[id]();

					//                        for (var j = 0; j < casperItemsArray[id].length; j++) {
					//                            simpleArray.push(casperItemsArray[id][j])
					//                        }
					//                        casperItemsDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    casperItemsDictionary[casperItemIds[i]] = ko.observableArray(casperItemsArray[id]);
					//                }
					//            }
					//            for (var i = 0; i < resourceScreenTemplateIds.length; i++) {
					//                var id = resourceScreenTemplateIds[i];
					//                if (resourceDictionary[id]) {
					//                    if (resourceDictionary[id]().length === 0) {
					//                        resourceDictionary[id](resourceArray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = resourceDictionary[id]();

					//                        for (var j = 0; j < resourceArray[id].length; j++) {
					//                            simpleArray.push(resourceArray[id][j])
					//                        }
					//                        resourceDictionary[id](simpleArray);
					//                    }
					//                }
					//                else {
					//                    resourceDictionary[resourceScreenTemplateIds[i]] = ko.observableArray(resourceArray[id]);
					//                }
					//            }
					//            for (var i = 0; i < modulesId.length; i++) {
					//                var id = modulesId[i];
					//                if (UserRoleDictionary[id]) {
					//                    if (UserRoleDictionary[id]().length === 0) {
					//                        UserRoleDictionary[id](moduleurlarray[id]);
					//                    }
					//                    else {
					//                        var simpleArray = UserRoleDictionary[id]();
					//                    }
					//                    for (var j = 0; j < moduleurlarray[id].length; j++) {
					//                        simpleArray.push(moduleurlarray[id][j])
					//                    }
					//                    UserRoleDictionary[id](simpleArray);
					//                }

					//                else {
					//                    UserRoleDictionary[modulesId[i]] = ko.observableArray(moduleurlarray[id]);
					//                }
					//            }
					//        }

					//        if (observableList().length > 0) {
					//            var simpleArray = observableList();
					//            for (var j = 0; j < itemsArray.length; j++) {
					//                simpleArray.push(itemsArray[j])
					//                items[itemsArray[j].id] = itemsArray[j];
					//            }
					//            observableList(simpleArray);
					//        } else {
					//            observableList(itemsArray);
					//            items = temp;
					//        }

					//        if (options && options.returnList)
					//            return itemsArray;
					//        if (options && options.sort)
					//            return mapper.getSortedValue(list[list.length - 1]);
					//    }
					//},

                          fillData = function (list, options) {
                              if (list && list.length > 0) {
                                  if (options && options.sort) {
                                      list.sort(function (entity1, entity2) {
                                          return mapper.getSortedValue(entity1) > mapper.getSortedValue(entity2) ? 1 : -1;
                                      });
                                  }

                                  var temp = [];

                                  temp = mapToContext(list, items, null, mapper, null, null);

                                  var itemsArray = getItemsArray(temp);
                                  observableList([]);
                                  if (observableList().length > 0) {
                                      var simpleArray = observableList();
                                      for (var j = 0; j < itemsArray.length; j++) {
                                          if (_.indexOf(simpleArray, itemsArray[j]) < 0)
                                              simpleArray.push(itemsArray[j])
                                          //items[mapper.getDtoId(itemsArray[j])] = itemsArray[j];
                                          //add(itemsArray[j]);
                                      }
                                      observableList([]);
                                      observableList(simpleArray);
                                      items = {};
                                      for (var k = 0 ; k < simpleArray.length; k++) {
                                          items[mapper.getDtoId(simpleArray[k])] = simpleArray[k];
                                      }
                                  } else {
                                      observableList([]);
                                      observableList(itemsArray);
                                      items = temp;
                                  }

                                  if (options && options.returnList)
                                      return itemsArray;
                                  if (options && options.sort)
                                      return mapper.getSortedValue(list[list.length - 1]);
                              }
                          },


					getItemsArray = function (items) {
					    if (!items) return;

					    var underlyingArray = [];
					    for (var prop in items) {
					        if (items.hasOwnProperty(prop)) {
					            if (!getLocalById(items[prop].id))
					                underlyingArray.push(items[prop]);
					        }
					    }
					    return underlyingArray;
					},

					updateData = function (entity, updateObservableList) {
					    items[entity.id] = entity;

					    if (updateObservableList) {
					        for (var i = 0; i < observableList().length; i++) {
					            if (observableList()[i].id === entity.id) {
					                observableList()[i] = entity;
					                break;
					            }
					        };
					    }

					    return items[entity.id];
					};

			    return {
			        observableList: observableList,
			        mapDtoToContext: mapDtoToContext,
			        add: add,
			        addToDictionary: addToDictionary,
			        getAllLocal: getAllLocal,
			        getLocalById: getLocalById,
			        getAllLocalByIds: getAllLocalByIds,
			        getData: getData,
			        getObservableList: getObservableList,
			        removeById: removeById,
			        removeFromDictionary: removeFromDictionary,
			        fillData: fillData,
			        updateData: updateData,
			        addToObservableList: addToObservableList,
			        getByBunchId: getByBunchId,
			        getByNewsId: getByNewsId,
			        getByFilterId: getByFilterId,
			        getByParentNewsId: getByParentNewsId,
			        getByProgramId: getByProgramId,
			        getByEpisodeId: getByEpisodeId,
			        getByTickerId: getByTickerId,
			        getByProgramElementId: getByProgramElementId,
			        getByChannelId: getByChannelId,
			        getByHourlySlotId: getByHourlySlotId,
			        getByNewsPaperId: getByNewsPaperId,
			        getByDailyNewsPaperId: getByDailyNewsPaperId,
			        getByDailyNewsPaperDate: getByDailyNewsPaperDate,
			        getByTemplateId: getByTemplateId,
			        getCamerasByScreenTemplateId: getCamerasByScreenTemplateId,
			        getMicsByScreenTemplateId: getMicsByScreenTemplateId,
			        getResourceByScreenTemplateId: getResourceByScreenTemplateId,
			        getModuleUrlBymoduleId: getModuleUrlBymoduleId,
			        getROStoriesByRunOrderId: getROStoriesByRunOrderId,
			        getROStoryItemsByStoryId: getROStoryItemsByStoryId,
			        getCasperItemsByRunOrderId: getCasperItemsByRunOrderId,
			    };
			},

			//----------------------------------
			// Repositories
			//----------------------------------

			//users = new EntitySet(null, modelmapper.user, null),
            news = new EntitySet(null, modelmapper.news, null);
		    //UserRole = new EntitySet(null, modelmapper.UserRole, null),
            //tickers = new EntitySet(null, modelmapper.ticker, null),
            //tickerLines = new EntitySet(null, modelmapper.tickerLine, null),
            //tickerCategory = new EntitySet(null, modelmapper.tickerCategory, null),
            //tickerVideo = new EntitySet(null, modelmapper.tickerVideo, null),
	        //channels = new EntitySet(null, modelmapper.channel, null);

        
	    var datacontext = {
	        //users: users,
	        //UserRole: UserRole,
	        //tickers: tickers,
	        //channels: channels,
	        //tickerCategory: tickerCategory,
	        //tickerVideo: tickerVideo,
            news: news
	    };

	    model.setDataContext(datacontext);

	    return datacontext;
	});