﻿define('dataservice',
    [
        'config',
        'appdata'
    ],
    function (config, appdata) {
        var
            init = function () {


                //amplify.request.define('login', 'ajax', {
                //    url: '/api/Login',
                //    dataType: 'json',
                //    type: 'Get'
                //});

                amplify.request.define('login', 'ajax', {
                    url: '/api/Admin/Login',
                    dataType: 'json',
                    type: 'Post'
                });

                amplify.request.define('deleteNews', 'ajax', {
                    url: '/api/news/RemoveNews/?id={id}',
                    dataType: 'json',
                    type: 'Get'
                });
            },

            

        Login = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'login',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        }


           deleteNews = function (data) {
               return $.Deferred(function (deferred) {
                   amplify.request({
                       resourceId: 'deleteNews',
                       data: data,
                       success: deferred.resolve,
                       error: deferred.reject
                   });
               }).promise();
           }

        init();

        return {
            deleteNews: deleteNews,
            Login: Login
        }
    });