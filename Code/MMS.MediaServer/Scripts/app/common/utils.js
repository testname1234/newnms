﻿define('utils',
    ['underscore', 'moment', 'string'],
    function (_, moment, S) {
        var
            endOfDay = function (day) {
                return moment(new Date(day))
                    .add('days', 1)
                    .add('seconds', -1)
                    .toDate();
            },
            getFirstTimeslot = function (timeslots) {
                return moment(timeslots()[0].start()).format('MM-DD-YYYY');
            },
            hasProperties = function (obj) {
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        return true;
                    }
                }
                return false;
            },
            invokeFunctionIfExists = function (callback) {
                if (_.isFunction(callback)) {
                    callback();
                }
            },
            mapMemoToArray = function (items) {
                var underlyingArray = [];
                for (var prop in items) {
                    if (items.hasOwnProperty(prop)) {
                        underlyingArray.push(items[prop]);
                    }
                }
                return underlyingArray;
            },
            regExEscape = function (text) {
                // Removes regEx characters from search filter boxes in our app
                return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            },
            restoreFilter = function (filterData) {
                var stored = filterData.stored,
                    filter = filterData.filter,
                    dc = filterData.datacontext;

                // Create a list of the 5 filters to process
                var filterList = [
                    { raw: stored.favoriteOnly, filter: filter.favoriteOnly },
                    { raw: stored.searchText, filter: filter.searchText },
                    { raw: stored.speaker, filter: filter.speaker, fetch: dc.persons.getLocalById },
                    { raw: stored.timeslot, filter: filter.timeslot, fetch: dc.timeslots.getLocalById },
                    { raw: stored.track, filter: filter.track, fetch: dc.tracks.getLocalById }
                ];

                // For each filter, set the filter to the stored value, or get it from the DC
                _.each(filterList, function (map) {
                    var rawProperty = map.raw, // POJO
                        filterProperty = map.filter, // observable
                        fetchMethod = map.fetch;
                    if (rawProperty && filterProperty() !== rawProperty) {
                        if (fetchMethod) {
                            var obj = fetchMethod(rawProperty.id);
                            if (obj) {
                                filterProperty(obj);
                            }
                        } else {
                            filterProperty(rawProperty);
                        }
                    }
                });
            },
            simulateHumanClick = function (element) {
                if (element) {
                    var event1 = document.createEvent("HTMLEvents");
                    event1.initEvent("click", true, true);
                    element.dispatchEvent(event1);

                    var event2 = document.createEvent("MouseEvents");
                    event2.initMouseEvent("click", true, true, window,
                    0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    element.dispatchEvent(event2);
                }
            },

            getDefaultUTCDate = function () {
                return moment().subtract('days', 700).utc().toISOString();
            },

            getCurrentDate = function () {
                return moment().utc().toISOString();
            },

            fillArray = function (dtoArray, memArray) {
                if (dtoArray && dtoArray.length > 0) {
                    for (var j = 0; j < dtoArray.length; j++) {
                        memArray.push(dtoArray[j]);
                    }
                }
            },

            extendObservable = function (target, source) {
                var prop, srcVal, tgtProp, srcProp,
                    isObservable = false;

                for (prop in source) {
                    if (!source.hasOwnProperty(prop)) {
                        continue;
                    }

                    if (ko.isWriteableObservable(source[prop])) {
                        isObservable = true;
                        srcVal = source[prop]();
                    } else if (typeof (source[prop]) !== 'function') {
                        srcVal = source[prop];
                    }

                    if (ko.isWriteableObservable(target[prop])) {
                        target[prop](srcVal);
                    } else if (target[prop] === null || target[prop] === undefined) {
                        target[prop] = isObservable ? ko.observable(srcVal) : srcVal;
                    } else if (typeof (target[prop]) !== 'function') {
                        target[prop] = srcVal;
                    }

                    isObservable = false;
                }
            },

            getDuration = function (time, unit) {
                var tempDuration = moment.duration(time, unit);
                return S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
            },

            getFormattedTime = function (timeObj) {
                if (timeObj)
                    return S(timeObj.Hours).padLeft(2, '0') + ':' + S(timeObj.Minutes).padLeft(2, '0') + ':' + S(timeObj.Seconds).padLeft(2, '0');
                else
                    return '';
            },

            clone = function (obj, emptyObj) {
                var json = ko.toJSON(obj);
                var js = JSON.parse(json);
                return extendObservable(emptyObj, js);
            },

            sortNumeric = function (a, b, sortOrder) {
                if (sortOrder == 'asc')
                    return a - b;
                else
                    return b - a;
            },

            sortAlphaNumeric = function (a, b, sortOrder) {
                if (sortOrder == 'asc')
                    return a < b ? -1 : 1;
                else
                    return a > b ? -1 : 1;
            },

            sortDateTime = function (a, b, sortOrder) {
                if (sortOrder == 'asc')
                    return new Date(a) - new Date(b);
                else
                    return new Date(b) - new Date(a);
            },

            getTimeSlots = function (noOfDays) {
                var timeslots = [];
                var add = 0;
                var hourDiff = 24 / noOfDays;

                for (var i = 0; i < 24; i++) {
                    var time;
                    if (i < 10)
                        time = "0" + i + ":00"
                    else
                        time = add + i + ":00"

                    timeslots.push(time);
                }

                return timeslots;
            },
            getDifferenceBetweenDates = function (date1, date2) {
                //var a = new Date(date1);
                //var b = new Date(date2);
                //return Math.abs(Math.round((a - b) / (1000 * 60 * 60 * 24)));

                return moment(date1).diff(moment(moment(date2).format('l')).toISOString(), 'days', true);
            },
            getTimeDifference = function (a, b) {

                var timeDiff = Math.abs(new Date(a) - new Date(b)) / 1000;

                var days = Math.floor(timeDiff / 86400);
                timeDiff -= days * 86400;

                var hours = Math.floor(timeDiff / 3600) % 24;
                timeDiff -= hours * 3600;

                var minutes = Math.floor(timeDiff / 60) % 60;
                timeDiff -= minutes * 60;

                var seconds = Math.floor(timeDiff % 60);

                var totalSeconds = seconds + (minutes * 60) + (hours * 60 * 60) + (days * 60 * 60 * 24);
                var totalMilliseconds = totalSeconds * 1000;

                return {
                    Days: days,
                    Hours: hours,
                    Minutes: minutes,
                    Seconds: seconds,
                    TotalSeconds: totalSeconds,
                    TotalMilliseconds: totalMilliseconds
                };
            },
            arrayIsContainObject = function (array, objectToCheck, againstTheProperty) {
                var detailObject = { flag: false, objectIndex: -1 };
                if (array && objectToCheck && array instanceof Array && objectToCheck instanceof Object) {
                    for (var i = 0; i < array.length; i++) {
                        var tempObject = array[i];
                        if (tempObject.hasOwnProperty(againstTheProperty) && objectToCheck.hasOwnProperty(againstTheProperty)) {
                            if (tempObject[againstTheProperty] === objectToCheck[againstTheProperty]) {
                                detailObject.flag = true;
                                detailObject.objectIndex = i;
                                break;
                            }
                        }
                    }
                }
                return detailObject;
            },

            generateUid = function (separator) {
                var delim = separator || "-";

                function S4() {
                    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
                }

                return (S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + S4() + S4() + S4());
            },
            WhileScrolling1 = function () {

                var htTop = $('.header').outerHeight();
                var sngmenu = $('.singlebar-menu').outerHeight();
                var getval = Math.abs(mcs.top);


                $('#newsAlert').css({
                    'top': 460
                });

                $('#newsAlert1').css({
                    'top': 475
                });

                if (mcs.top < -49) {
                    var tp = mcs.top * -1;
                    $('html').addClass('scrolstart');
                    $('.singlebar-menu').css('top', 0);
                } else {
                    $('html').removeClass('scrolstart');
                    $('.singlebar-menu').css('top', 0);
                }

                if (mcs.top < -67) {
                    var tp = mcs.top * -1;
                    $('html').addClass('scrolstart1');
                    $('.newsNav').css('top', $('.singlebar-menu').height());
                } else {
                    $('html').removeClass('scrolstart1');
                }

                if (getval > 67) {
                    $('.leftnav, .rightSec').css({
                        'top': 0
                    });
                } else {
                    $('.leftnav, .rightSec').css({
                        'top': 0
                    });
                }
            },

            arrayOrderBy = function (arr, byAttr, orderBy) {
                if (arr && arr.length > 1) {
                    arr = arr.sort(function (entity1, entity2) {
                        var isObservable = ko.isObservable(entity1[byAttr]);
                        if (orderBy === "asc") {
                            return isObservable ? entity1[byAttr]() - entity2[byAttr]() : entity1[byAttr] - entity2[byAttr];
                        }
                        else if (orderBy === "desc") {
                            return isObservable ? entity2[byAttr]() - entity1[byAttr]() : entity2[byAttr] - entity1[byAttr];
                        }
                    });
                }
                return arr;
            },
            getDatesByRange = function (fromString, toString, formatString) {
                var d1 = new Date(fromString);
                var d2 = new Date(toString);
                arr = new Array();
                if (((d2 - d1) / (1000 * 3600 * 24)) < 0)
                    return arr;
                if (((d2 - d1) / (1000 * 3600 * 24)) == 0)
                    arr.push(d1);
                if (((d2 - d1) / (1000 * 3600 * 24)) > 0) {
                    while (d1 <= d2) {
                        arr.push(new Date(d1));
                        d1.setDate(d1.getDate() + 1);
                    }
                }
                return arr;
            };

        return {
            endOfDay: endOfDay,
            getFirstTimeslot: getFirstTimeslot,
            hasProperties: hasProperties,
            invokeFunctionIfExists: invokeFunctionIfExists,
            mapMemoToArray: mapMemoToArray,
            regExEscape: regExEscape,
            restoreFilter: restoreFilter,
            getDefaultUTCDate: getDefaultUTCDate,
            getCurrentDate: getCurrentDate,
            fillArray: fillArray,
            getDuration: getDuration,
            clone: clone,
            getDatesByRange: getDatesByRange,
            getTimeSlots: getTimeSlots,
            getDifferenceBetweenDates: getDifferenceBetweenDates,
            arrayIsContainObject: arrayIsContainObject,
            sortNumeric: sortNumeric,
            sortAlphaNumeric: sortAlphaNumeric,
            sortDateTime: sortDateTime,
            getFormattedTime: getFormattedTime,
            getTimeDifference: getTimeDifference,
            simulateHumanClick: simulateHumanClick,
            generateUid: generateUid,
            WhileScrolling1: WhileScrolling1,
            arrayOrderBy: arrayOrderBy
        };
    });