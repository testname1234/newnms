﻿define('manager.home',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config',
        'model',
        'model.mapper',
        'presenter',
        'router',
        'moment',
        'store'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, config, model, mapper, presenter, router, moment, localStorage) {
    	var
		Login = function (data) {
			debugger;
			return $.Deferred(function (d) {
				$.when(dataservice.Login(data))
				.done(function (responseData) {
				    console.log(responseData);
				    if (responseData.IsSuccess==true)
				    {
				        amplify.publish(config.eventIds.onLogIn, responseData);
				        d.resolve(responseData);
				        window.location.href = "#/homeview";
				    }
				})
				.fail(function (responseData) {
					d.reject(responseData);
				});
			}).promise();
		}

    	Loggoff = function () {


    	},

    	loadInitialData = function () {
    	    debugger;

    	}


    	return {
    	
    		Login: Login,
    		Loggoff: Loggoff,
    		loadInitialData: loadInitialData
    	}
    });