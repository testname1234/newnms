﻿define('manager',
    [
        'manager.usermanagement',
        'manager.student',
        'manager.news',
        'manager.home'
    ],
    function (usermanagement,student, news,home) {

        return {
            usermanagement: usermanagement,
            student: student,
            news: news,
            home: home
        };
});