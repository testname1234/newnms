﻿define('manager.news',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config',
        'model',
        'model.mapper',
        'presenter',
        'router',
        'moment',
        'store'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, config, model, mapper, presenter, router, moment, localStorage) {
        var


        getNewsData = function () {
            return $.Deferred(function (d) {
                $.when(dataservice.getNewsData())
                .done(function (responseData) {
                    console.log(responseData);
                    d.resolve(responseData);
                })
                .fail(function (responseData) {

                    d.reject(responseData);
                });
            }).promise();
        };


        postNews = function (data) {
            return $.Deferred(function (d) {
                $.when(dataservice.postNews(data))
                .done(function (responseData) {
                    console.log(responseData);
                    d.resolve(responseData);
                })
                .fail(function (responseData) {

                    d.reject(responseData);
                });
            }).promise();
        },

      putNews = function (data) {
            debugger;
            return $.Deferred(function (d) {
                $.when(dataservice.putNews(data))
                .done(function (responseData) {
                    console.log(responseData);
                    d.resolve(responseData);
                })
                .fail(function (responseData) {

                    d.reject(responseData);
                });
            }).promise();
        },


      deleteNews = function (data) {
            debugger;
            return $.Deferred(function (d) {
                $.when(dataservice.deleteNews(data))
                .done(function (responseData) {
                    console.log(responseData);
                    d.resolve(responseData);
                })
                .fail(function (responseData) {

                    d.reject(responseData);
                });
            }).promise();
        };



        return {
            //addStudent: addStudent,
            //getStudentById: getStudentById,
            getNewsData: getNewsData,
            postNews: postNews,
            putNews: putNews,
            deleteNews: deleteNews
        }
    });