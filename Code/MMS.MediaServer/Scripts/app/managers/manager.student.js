﻿define('manager.student',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config',
        'model',
        'model.mapper',
        'presenter',
        'router',
        'moment',
        'store'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, config, model, mapper, presenter, router, moment, localStorage) {
        var
            getAllStudent = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getAllStudent())
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                debugger
                                d.resolve();
                            } 
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },
            getStudentById = function (data) {
                
            },
            addStudent = function (data) {

            };

        //#endregion


        return {
            addStudent: addStudent,
            getStudentById: getStudentById,
            getAllStudent: getAllStudent
        };
    });