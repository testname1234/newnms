﻿define('manager.usermanagement',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config'
    ],
    function ($, e, dataservice, dc, _, utils, appdata,  config) {
        var logger = config.logger;


        userlogin = function (data) {
            return $.Deferred(function (d) {
                $.when(dataservice.usermanagementlogin(data))
                    .done(function (responseData) {
                        if (responseData && responseData.IsSuccess && responseData.Data) {
                            

                            var object = responseData.Data;
                            var userid = responseData.Data.UserId;
                            var sessionkey = responseData.Data.SessionKey;
                            
                            for (var j = 0; j < object.Modules.length; j++) {

                                object.Modules[j].ModuleAbbriviation = object.Modules[j].ModuleAbbriviation.replace(/ +/g, "");
                                object.Modules[j].WorkRoleName = object.Modules[j].WorkRoleName.replace(/ +/g, "");
                                object.Modules[j]["UserId"] = userid;
                                object.Modules[j]["SessionKey"] = sessionkey;
                                object.Modules[j]["UniqueId"] = j;
                                object.Modules[j]["UrlName"] = object.Modules[j].ModuleAbbriviation + ' ' + object.Modules[j].WorkRoleName;
                                object.Modules[j]["FullName"] = object.FullName;
                               

                            }
                            
                           
                            dc.UserRole.fillData(object.Modules, { groupModuleUrlByModuleId: true });


                            d.resolve(responseData);
                        }

                        else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    });
            }).promise();
        };



        return {
          
            userlogin: userlogin,
            
        }
    });