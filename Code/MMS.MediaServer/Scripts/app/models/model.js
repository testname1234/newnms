﻿define('model',
    [
        'model.news'
    ],
    function (
        News
        ) {

        var
            model = {
                News: News
            };

        model.setDataContext = function (dc) {
            model.News.datacontext(dc);
        };

        return model;
    });