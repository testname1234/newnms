﻿define('model.mapper',
    ['jquery', 'model', 'config', 'underscore', 'moment'],
    function ($, model, config, _, moment) {
        var
            user = {
                getDtoId: function (dto) {
                    return dto.UserId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.User();

                    item.id = dto.UserId,
                    item.name = dto.Name,
                    item.displayName = dto.Name;

                    if (dto.UserRoles)
                        item.userRoles(dto.UserRoles);

                    return item;
                }
            },
              
             UserRole = {
                 getDtoId: function (dto) {
                     return dto.UniqueId;
                 },
                 fromDto: function (dto, item) {
                     var userrole = dto;

                     item = item || new model.UserRole();
                     item.moduleId = userrole.ModuleId,
                     item.workRoleId = userrole.WorkRoleId,
                     item.moduleUrl = userrole.ModuleUrl;
                     item.userId = userrole.UserId;
                     item.sessionkey = userrole.SessionKey;
                     item.uniqueId = userrole.UniqueId;
                     item.moduleAbbriviation = userrole.ModuleAbbriviation;
                     item.workRoleName = userrole.WorkRoleName;
                     item.urlName = userrole.UrlName;
                     item.fullName = userrole.FullName;

                     return item;
                 }
             },
               news = {
                   getDtoId: function (dto) { return dto.NewsId; },
                   fromDto: function (dto, item) {
                       //var usernews = dto;
                       item = item || new model.News();
                       item.newsCategory = dto.NewsCategory;
                       item.newsDescription = dto.NewsDescription;
                       item.newsTitle = dto.NewsTitle;
                       item.newsId = dto.NewsId;
                       return item;
                   }
               },

            ticker = {
                getClone: function (entity) {

                    var newObj = new model.Ticker();

                    newObj.id = entity.id,
                    newObj.onAiredTime(entity.onAiredTime),
                    newObj.severity(entity.severity()),
                    newObj.sequenceId = entity.sequenceId,
                    newObj.userId(entity.userId()),
                    newObj.lastUpdateDate(entity.lastUpdateDate()),
                    newObj.creationDate(entity.creationDate()),
                    newObj.breakingStatusId(entity.breakingStatusId()),
                    newObj.latestStatusId(entity.latestStatusId()),
                    newObj.categoryStatusId(entity.categoryStatusId()),
                    newObj.frequency(entity.frequency()),
                    newObj.breakingSequenceId = entity.breakingSequenceId,
                    newObj.latestSequenceId = entity.latestSequenceId,
                    newObj.categorySequenceId = entity.categorySequenceId,
                    newObj.categoryId = entity.categoryId;
                    newObj.setDispalyId(entity.id);

                    if (entity.news) {
                        newObj.news(entity.news());
                    }
                    return newObj;
                },
                getDtoId: function (dto) {
                    return dto.TickerId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdatedDateStr;
                },
                fromDto: function (dto, item) {

                    item = item || new model.Ticker();

                    item.id = dto.TickerId,
                    item.onAiredTime(dto.OnAiredTimeStr),
                    item.severity(parseInt(dto.Severity) || 1),
                    item.sequenceId = dto.SequenceId,
                    item.userId(dto.UserId),

                    item.breakingStatusId(dto.BreakingStatusId),
                    item.latestStatusId(dto.LatestStatusId),
                    item.categoryStatusId(dto.CategoryStatusId),

                    item.frequency(dto.Frequency || 1),

                    item.breakingSequenceId = dto.BreakingSequenceId,
                    item.latestSequenceId = dto.LatestSequenceId,
                    item.categorySequenceId = dto.CategorySequenceId,

                    item.lastUpdateDate(dto.LastUpdatedDateStr),
                    item.creationDate(dto.CreationDateStr),

                    item.categoryId = dto.CategoryId;
                    item.setDispalyId(item.id);

                    if (dto.news) {
                        item.news(dto.news());
                    }

                    return item;
                }
            },

            channel = {
                getDtoId: function (dto) {
                    return dto.ChannelId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Channel();

                    item.id = dto.ChannelId,
                    item.name = dto.Name,
                    item.creationDate(dto.CreationDateStr),
                    item.lastUpdateDate(dto.LastUpdateDateStr);
                    return item;
                }
            },
            tickerLine = {
                getDtoId: function (dto) {
                    return dto.TickerLineId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdatedDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.TickerLine();

                    item.id = dto.TickerLineId;
                    item.tickerId = dto.TickerId;
                    item.title(dto.Text || '');
                    item.languageCode(dto.LanguageCode);
                    item.sequenceId = dto.SequenceId;
                    item.creationDate = dto.CreationDateStr;

                    return item;
                },
            },
            tickerCategory = {
                getDtoId: function (dto) {
                    return dto.TickerCategoryId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.TickerCategory();

                    item.id = dto.TickerCategoryId;
                    item.categoryId = dto.CategoryId;
                    item.name = dto.Name;
                    item.creationDate(dto.CreationDateStr),
                    item.lastUpdateDate(dto.LastUpdateDateStr);

                    return item;
                }
            },
            tickerVideo = {
                getDtoId: function (dto) {
                    return dto.VideoId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.TickerVideo();

                    item.id = dto.VideoId,
                    item.url = dto.Url || '',
                    item.startTime = dto.StartTimeStr || '',
                    item.endTime = dto.EndTimeStr || '',
                    item.channelId(dto.ChannelId),
                    item.videoStatusId(dto.VideoStatusId),
                    item.creationDate(dto.CreationDateStr),
                    item.lastUpdateDate(dto.LastUpdateDateStr);

                    return item;
                }
            };




        return {
            user: user,
            ticker: ticker,
            tickerLine: tickerLine,
            UserRole: UserRole,
            channel: channel,
            tickerCategory: tickerCategory,
            tickerVideo: tickerVideo,
            news: news
        };
    });