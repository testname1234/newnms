﻿//define('model.news',
//    ['ko'],
//    function (ko) {
//       var _dc = this,
//        News = function () {
//            var self = this;
//            self.newsCategory;
//            self.newsDescription;
//            self.newsId;
//            self.newsTitle;
//            self.isNullo = false;

//            return self;
//        };

//        News.Nullo = new News();
//        News.Nullo.isNullo = true;
//        News.datacontext = function (dc) {
//            if (dc) { _dc = dc; }
//            return _dc;
//        };

//        return News;
//    });
define('model.news', [],
    function () {

        var
            _dc = this,

            News = function () {
                var self = this;

                            self.newsCategory,
                            self.newsDescription,
                            self.newsId,
                            self.newsTitle,
                            self.isNullo = false;

                            return self;
            }

        News.Nullo = new News();

        News.Nullo.isNullo = true;

        // static member
        News.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return News;
    });