﻿define('binder',
    [
        'jquery',
        'ko',
        'config',
        'enum',
        'presenter',
        'vm',
        'appdata',
        'manager',
        'datacontext',
        'model',
        'dataservice'
    ],
    function ($, ko, config, e, presenter, vm, appdata, manager, dc, model, dataservice) {
        var logger = config.logger;

        var
            ids = config.viewIds.Admin,

            bindPreLoginViews = function () {
                debugger;

                ko.applyBindings(vm.shell, getView(ids.shellTopNavView));
                ko.applyBindings(vm.login, getView(ids.login));
                ko.applyBindings(vm.home, getView(ids.home));
                ko.applyBindings(vm.bucket, getView(ids.bucket));

               
                //ko.applyBindings(vm.student, getView(ids.studentView));
                //ko.applyBindings(vm.news, getView(ids.newsView));
                //ko.applyBindings(vm.news, getView(ids.anchorsView));
            },

            bindPostLoginViews = function () {
            },

            deleteExtraViews = function () {
            },

            bindStartUpEvents = function () {
 
            },

            getView = function (viewName) {
                return $(viewName).get(0);
            };

        return {
            bindPreLoginViews: bindPreLoginViews,
            bindPostLoginViews: bindPostLoginViews,
            bindStartUpEvents: bindStartUpEvents,
            deleteExtraViews: deleteExtraViews
        }
    });