﻿define('bootstrapper',
    [
        'enum',
        'config',
        'binder',
        'route-config',
        'manager',
        'vm',
        'presenter',
        'appdata',
        'helper',
        'ko',
        'amplify'
    ],
    function (e, config, binder, routeConfig, manager, vm, presenter, appdata, helper, ko, amplify) {
        var
            run = function () {
                
                //config.dataserviceInit();
                binder.bindPreLoginViews();
                binder.bindStartUpEvents();
                routeConfig.register();

                amplify.subscribe(config.eventIds.onLogIn, function (data) {
                    // logger.showLoading("loading initial data..");
                    debugger;
                    $.when(manager.home.loadInitialData())
                        .done(function () {
                            debugger;
                        })
                        .done(function () {

                       
                        });
                });



                //presenter.toggleActivity(true);
                
                //$.when(manager.student.getAllStudent())
                //   .done(function (responseData) {
                //       binder.bindPostLoginViews();
                //   })
                //   .fail(function (responseData) {
                //   });


            //    $.when(manager.news.getNewsData())
            //      .done(function (responseData) {
            //          binder.bindPostLoginViews();
            //      })
            //      .fail(function (responseData) {
            //      });
            };

        return {
            run: run
        }
    });