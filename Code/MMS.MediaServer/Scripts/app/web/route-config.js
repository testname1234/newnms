﻿define('route-config',
    ['config', 'router', 'vm', 'appdata', 'enum'],
    function (config, router, vm, appdata, e) {
        var
            logger = config.logger,

            register = function () {
                debugger;
                var routeData;
                routeData = [
                   // Pending Stories routes
                  
                     {
                         view: config.viewIds.Admin.login,
                         routes: [
                             {
                                 isDefault: true,
                                 route: config.hashes.Admin.login,
                                 title: 'Media Server',
                                 callback: vm.login.activate,
                                 group: '.route-top'
                             }
                         ]
                     },
                     {
                         view: config.viewIds.Admin.home,
                         routes: [
                             {
                                 isDefault: false,
                                 route: config.hashes.Admin.home,
                                 title: 'Media Server Home',
                                 callback: vm.home.activate,
                                 group: '.route-top'
                             }
                         ]
                     },
                     {
                         view: config.viewIds.Admin.bucket,
                         routes: [
                             {
                                 isDefault: false,
                                 route: config.hashes.Admin.bucket,
                                 title: 'Media Server Bucket',
                                 callback: vm.bucket.activate,
                                 group: '.route-top'
                             }
                         ]
                     }
                ];

                // Invalid routes
                routeData.push({
                    view: '',
                    route: /.*/,
                    title: '',
                    callback: function () {
                        logger.error(config.toasts.invalidRoute);
                    }
                });
                
                for (var i = 0; i < routeData.length; i++) {
                    router.register(routeData[i]);
                }

                // Crank up the router
                router.run();
            };

        return {
            register: register
        };
    });