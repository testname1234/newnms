﻿define('vm.home',
     ['config', 'messenger', 'model', 'datacontext', 'model.mapper', 'moment', 'appdata', 'enum', 'presenter', 'manager', 'underscore', 'utils', 'router'],
     function (config, messenger, model, dc, mapper, moment, appdata, e, presenter, manager, _, utils, router) {
         var
             data = "",
         
         activate = function (routeData, callback) {
             console.log("activated")
         },
         init = function () {
         }

         return {
             activate: activate,
             init: init
           
         }
     });