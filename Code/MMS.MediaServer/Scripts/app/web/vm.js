﻿define('vm',
    [
        'vm.shell',
        'vm.news',
        'vm.login',
        'vm.home',
        'vm.bucket'
    ],

    function (shell, news, login, home,bucket) {
        return {
            shell: shell,
            news: news,
            login: login,
            home: home,
            bucket: bucket
        };
    });