﻿define('vm.news',
     ['config', 'messenger', 'model', 'datacontext', 'model.mapper', 'moment', 'appdata', 'enum', 'presenter', 'manager', 'underscore', 'utils', 'router'],
     function (config, messenger, model, dc, mapper, moment, appdata, e, presenter, manager, _, utils, router) {
         var
             lstOfHeadlines = ko.observableArray([]),
         selectedNews = ko.observable(),
         newsCategory = ko.observable(),
         newsTitle = ko.observable(),
         newsDescription = ko.observable(),
         isPopVisible = ko.observable(false),
         enterNews = ko.observable(false),
         //currentNews = ko.observable(),
         newsID = ko.observable();
         currentNews = function () {
             enterNews(true);

         },



         cancelNews = function () {
             //isPopVisible(false);
             //enterNews(false);
             router.navigateTo('#/news-view');

         },

         showNews = function (data) {
             debugger;
             
             //isPopVisible(true);
             router.navigateTo('#/anchors-view');
             //selectedNews(data);
             isPopVisible(true);
             newsID(data.newsId)
             newsCategory(data.newsCategory);
             newsTitle(data.newsTitle);
             newsDescription(data.newsDescription);
         },


         fillNews = function () {
             debugger;
             $.when(manager.news.getNewsData())

             .done(function (responseData) {
                 dc.news.fillData(responseData.Data);
                 // lstOfHeadlines([]);
                 //for (var i = 0; i < dc.news.getObservableList().length; i++) {
                 //    lstOfHeadlines.push(dc.news.getObservableList()[i]);
                 //}

                 lstOfHeadlines([]);
                 lstOfHeadlines(dc.news.getObservableList())


             })
             .fail(function (responseData) {
                 alert();
             });
         },


         addNews = function () {
             debugger;
             var saveData = { NewsCategory: selectedNews().newsCategory, NewsTitle: selectedNews().newsTitle, NewsDescription: selectedNews().newsDescription, };
             console.log(saveData);
             $.when(manager.news.postNews(saveData))

             .done(function (responseData) {
                 debugger;
                 fillNews();
             })
             .fail(function (responseData) {
                 alert();
             });
             newsCategory('');
             newsTitle('');
             newsDescription('');
             enterNews(false);
         },

         submitNews = function () {
             debugger;
             var newsArr = { NewsId: selectedNews().newsId, NewsTitle: selectedNews().newsTitle, NewsDescription: selectedNews().newsDescription, };
             //selectedNews(data);
             //var newsArr = {};
             //newsArr.NewsId = data.newsId;
            
             //newsArr.NewsCategory = data.newsCategory;
             //newsArr.NewsTitle = data.newsTitle;
             //newsArr.NewsDescription = data.newsDescription;
             //console.log(newsArr);
             $.when(manager.news.putNews(newsArr))
                 .done(function (responseData) {
                     debugger;
                     fillNews();
                 })
                        .fail(function (responseData) {
                            alert();
                        });
             //isPopVisible(false);
             newsCategory('');
             newsTitle('');
             newsDescription('');
         },

            deleteNews = function (data, event) {
                debugger;
                var del = { id: data.newsId };
                $.when(manager.news.deleteNews(del))
                .done(function (responseData) {
                    debugger;
                    fillNews();
                })
                       .fail(function (responseData) {
                           alert();
                       });
            },



         activate = function (routeData, callback) {
             //lstOfHeadlines([]);
             debugger;
           //  fillNews();
            
         },
         init = function () {
         }

         return {
             activate: activate,
             init: init,
             lstOfHeadlines: lstOfHeadlines,
             newsCategory: newsCategory,
             newsTitle: newsTitle,
             newsDescription: newsDescription,
             isPopVisible: isPopVisible,
             showNews: showNews,
             addNews: addNews,
             selectedNews: selectedNews,
             submitNews: submitNews,
             cancelNews: cancelNews,
             enterNews: enterNews,
             currentNews: currentNews,
             deleteNews: deleteNews
          
             //currentNews: currentNews
         }
     });