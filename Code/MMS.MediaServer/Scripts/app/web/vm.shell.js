﻿define('vm.shell',
     [
         'ko',
         'config',
         'router',
         'enum',
         'datacontext',
         'model',
         'manager',
         'appdata',
         'presenter',
         'utils',
         'helper',
     ],
    function (ko, config, router, e, dc, model, manager, appdata, presenter, utils, helper) {
        var logger = config.logger;

        var
            // Properties
            //-------------------

            menuHashes = config.hashes.Admin,
           
           
            // Computed
            //-------------------

            
            currentHash = ko.computed({
                read: function () {
                    return router.currentHash();
                },
                deferEvaluation: true
            }),
            activeHash = ko.computed({
                read: function () {
                    if (currentHash().indexOf('student-view') !== -1)
                        return menuHashes.studentView;
                    if (currentHash().indexOf('news-view') !== -1)
                        return menuHashes.newsView;
                    if (currentHash().indexOf('anchors-view') !== -1)
                        return menuHashes.anchorsView;
                },
                deferEvaluation: true
            }),

            // Methods
            //-------------------

          
            activate = function (routeData) {
            },
            init = function () {
               
            };

        return {
            currentHash: currentHash,
            activeHash: activeHash,
            activate: activate,
            menuHashes: menuHashes,
            init: init
          
            
        };
    });