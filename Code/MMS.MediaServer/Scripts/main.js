﻿(function () {
    var root = this;

    define3rdPartyModules();
    loadPluginsAndBoot();

    function define3rdPartyModules() {
        define('jquery', [], function () { return root.jQuery; });
        define('ko', [], function () { return root.ko; });
        define('amplify', [], function () { return root.amplify; });
        define('infuser', [], function () { return root.infuser; });
        define('moment', [], function () { return root.moment; });
        define('sammy', [], function () { return root.Sammy; });
        define('toastr', [], function () { return root.toastr; });
        define('underscore', [], function () { return root._; });
        define('string', [], function () { return root.S; });
    }

    function loadPluginsAndBoot() {
        requirejs([
                'ko.bindingHandlers',
                'ko.debug.helpers'
        ], boot);
    }
    
    function boot() {
        require(['bootstrapper'], function (bs) { debugger; bs.run(); });
    }
})();