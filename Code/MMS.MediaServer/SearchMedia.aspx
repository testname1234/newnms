﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchMedia.aspx.cs" Inherits="MS.Web.SearchMedia" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" type="text/css" href="content/style.css" />
     <link rel="stylesheet" type="text/css" href="Content/colorbox.css" />
 <%--   <link rel="stylesheet" type="text/css" href="Content/jquery-ui.css" />--%>
     <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <%--  <script type="text/javascript"  src="Scripts/jquery.js"  ></script>--%>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.8.0.js"></script> 
  
</head>
<body>
    <form id="form1" runat="server">
    <%--   <ajaxToolkit:ToolkitScriptManager ID="ajaxtoolkit" runat="server"></ajaxToolkit:ToolkitScriptManager>--%>

    <div class="ui-widget">
        <br />
        <br />
    Search Media  <asp:TextBox ID="txtSearch" runat="server" CssClass="tb" ClientIDMode="Static" autofocus="" ></asp:TextBox> 
       
        
       <%--  <ajaxToolkit:AutoCompleteExtender  
            runat="server"   
            ID="autoComplete1" 
            TargetControlID="txtSearch" 
            ServiceMethod="SearchCelebrities"
            MinimumPrefixLength="4" 
            CompletionInterval="100"
            EnableCaching="true"
            CompletionSetCount="10"
            FirstRowSelected = "true"
            OnClientItemSelected = "ClientItemSelected"
            DelimiterCharacters=";,:">
            </ajaxToolkit:AutoCompleteExtender>--%>

         <select  id="ddlResourceType" >
         <option value="2">Video</option>
         <option value="1">Images</option>
         </select>
          <input  type="checkbox" id="isHd" value="HD"  onclick="OnHdcheckChanged();" title="HD" style="margin-top:5px; display:none;"/><label style="margin-left:5px; margin-right:5px; margin-top:5px; display:none; ">HD</label>
    <input id="btnSearch" type="button" value="Search"  />
     <label id="lblCounts"></label>
    <ul id="SearchMediacontent" class="newsColume">
    </ul>
         <div style="margin-left:500px;">
        <input id="btnprev" type="button" value="Prev"  style= "display:none;"   />
        <input id="btnMore" type="button" value="Next"  style=" margin-left:5px; display:none;"  />
    </div>
         <input id="hfpagenumber" type="hidden" value="1" />

    </div>
         <script type="text/javascript">

             function ClientItemSelected() {

                 $("#btnSearch").click();
             }


             $(document).ready(function () {
                 $(".tb").autocomplete({
                     source: function (request, response) {
                         $.ajax({
                             url: "api/Resource/SearchCelebrity?term=" + request.term + "&pageSize=10&pageNumber=1",
                             dataType: "json",
                             type: "Get",
                             contentType: "application/json; charset=utf-8",
                             dataFilter: function (data) { return data; },
                             success: function (data) {
                                 debugger;
                                 console.log(data.Data);
                                 response($.map(data.Data, function (item) {
                                  
                                     console.log(item);
                                     return {
                                         value: item
                                     }
                                 }))
                             },
                             error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                             }
                         });
                     },
                     minLength: 2
                 });

                 
                 $("#txt").autocomplete({
                     source: function (request, response) {
                         $.ajax({
                             url: "api/Resource/SearchCelebrity?term=" + request.term + "&pageSize=10&pageNumber=1",
                             dataType: "json",
                             type: "Get",
                             contentType: "application/json; charset=utf-8",
                             dataFilter: function (data) { return data; },
                             success: function (data) {
                                 debugger;
                                 console.log(data.Data);
                                 response($.map(data.Data, function (item) {

                                     console.log(item);
                                     return {
                                         value: item
                                     }
                                 }))
                             },
                             error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                             }
                         });
                     },
                     minLength: 2
                 });

             });
            
            


        </script>
       
         
         <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
     <%--  <script type="text/javascript"  src="Scripts/jquery-ui-1.8.20.min.js"  ></script>--%>

        <script type="text/javascript" src="Scripts/app/SearchMedia.js"></script>
       <%-- <script type="text/javascript" src="Scripts/jquery-migrate-1.2.1.js"></script>--%>
        <script type="text/javascript" src="Scripts/jquery.colorbox-min.js"></script>
    </form>
</body>
</html>
