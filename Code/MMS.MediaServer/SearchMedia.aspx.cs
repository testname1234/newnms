﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MS.MediaIntegration.API;


namespace MS.Web
{
    public partial class SearchMedia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchCelebrities(string prefixText)
        {
            MSApi api = new MSApi();
            MS.MediaIntegration.DataTransfer<List<string>> resource = api.SearchCelebrity(prefixText, 100, 1);

            List<string> celeb = new List<string>();

            if (resource != null)
            {
                foreach (string item in resource.Data)
                {
                    if (!string.IsNullOrEmpty(item))
                    { 
                       // string val = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(item, item);
                        celeb.Add(item);
                    }
                }
            }

            return celeb;
        }
    }
}