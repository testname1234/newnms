﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addbucket.aspx.cs" Inherits="MS.Web.addbucket" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home - Media Server</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="content/style.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="mainwrap">
            <header>
                <section class="clear">
                    <div class="userinfo">
                        <span id="userName">Media Server</span>
                    </div>
                </section>
            </header>
            <div class="innnerwrap">


                <table>
                    <tr>
                        <td colspan="2" align="center" class="padding">
                            <h1>Add Bucket</h1>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Name</td>
                        <td class="padding">
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="padding">Path</td>
                        <td class="padding">
                            <asp:TextBox ID="txtPath" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="padding">Enable AutoSynchronization</td>
                        <td class="padding">
                            <asp:CheckBox ID="chkEnable" runat="server" Text="Yes" />
                        </td>
                    </tr>
                    <tr>
                        <td class="padding">Size</td>
                        <td class="padding">
                            <asp:TextBox ID="txtSize" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="padding">Requested Server</td>
                        <td class="padding">
                            <asp:DropDownList ID="ddlServer" runat="server"></asp:DropDownList></td>
                    </tr>
                     
                    <tr>
                        <td colspan="2" class="padding">
                            <asp:Button ID="btnSubmit" runat="server" Text="Add" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btn2" runat="server" Text="test" OnClick="btn2_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            Create Bucket
            <asp:TextBox ID="txtBucketName" runat="server"></asp:TextBox>
            <asp:Button ID="Create" runat="server" Text="Create Bucket" OnClick="Create_Click" />
            <asp:Button ID="Delete" runat="server" Text="Delete Bucket" OnClick="Delete_Click" />
            <br />
            <asp:FileUpload ID="fuTest" runat="server" />
            <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" />


            Delete Bucket
             DeleteSubBucket
                Name : <asp:TextBox id="txtBucketNameforDelete" runat="server"></asp:TextBox> <asp:button  ID="btnDeleteBucket" runat="server" Text="Delete SubBucket" OnClick="btnDeleteBucket_Click"/>
       
            <div>
                Show Buckets
                <asp:Button ID="btnShowBuckets" runat="server" OnClick="btnShowBuckets_Click" />
            </div>
        </div>
    </form>
</body>
</html>
