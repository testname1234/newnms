﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MS.Core;
using MS.Core.IService;
using MS.Core.Entities;
using MS.MediaStorage;
using MS.Web.API;

namespace MS.Web
{
    public partial class addbucket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IServerService serverService = IoC.Resolve<IServerService>("ServerService");

                List<MS.Core.Entities.Server> lstServer = serverService.GetAllServer();

                ddlServer.DataSource = lstServer;
                ddlServer.DataTextField = "ServerIP";
                ddlServer.DataValueField = "ServerId";
                ddlServer.DataBind();

                ddlServer.Items.Insert(0, new ListItem("-- Select --", ""));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
            IBucketCredentialsService credentialService = IoC.Resolve<IBucketCredentialsService>("BucketCredentialsService");

            Bucket thisBucket = new Bucket();
            thisBucket.Name = txtName.Text;
            thisBucket.Path = txtPath.Text;
            thisBucket.AutoSync = chkEnable.Checked;
            thisBucket.Size = Convert.ToDouble(txtSize.Text);
            thisBucket.CreationDate = DateTime.UtcNow;
            thisBucket.LastUpdateDate = thisBucket.CreationDate;
            thisBucket.IsParent = true;
            if (ddlServer.SelectedValue == "")
                thisBucket.ServerId = null;
            else
                thisBucket.ServerId = Convert.ToInt32(ddlServer.SelectedValue);

            thisBucket = bucketService.InsertBucket(thisBucket);

            BucketCredentials credentials = new BucketCredentials();
            credentials.BucketId = thisBucket.BucketId;
            credentials.LoginId = thisBucket.Name;
            credentials.ApiKey = System.Web.Security.Membership.GeneratePassword(10, 2);
            credentials = credentialService.InsertBucketCredentials(credentials);
        }

        protected void btn2_Click(object sender, EventArgs e)
        {
            Storage test = new Storage(4, "kG:*:aJcbv");
            
            //test.CreateSubBucket("test1");
            //test.CreateSubBucket("test2");

            List<Bucket> allBuckets = test.GetAllSubBucket();
            Bucket oneBucket = test.GetSubBucket("test");
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            Storage test = new Storage(4, "kG:*:aJcbv");
            test.CreateSubBucket(txtBucketName.Text);
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            Storage test = new Storage(4, "kG:*:aJcbv");
            test.DeleteSubBucket(txtBucketName.Text);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Storage test = new Storage(4, "kG:*:aJcbv");
           // test.getallfile(txtBucketName.Text);

            BucketController controller = new BucketController();
            controller.GetAllFiles(4, "Resource2\\");

        }

        protected void btnDeleteBucket_Click(object sender, EventArgs e)
        {
            BucketController controller = new BucketController();
            controller.DeleteSubBucket(4,"kG:*:aJcbv",txtBucketNameforDelete.Text);
        }

        protected void btnShowBuckets_Click(object sender, EventArgs e)
        {

        }

    }
}