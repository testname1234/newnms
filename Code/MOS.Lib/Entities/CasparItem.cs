﻿using MOSProtocol.Lib.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [DataContract]
    [XmlRoot(ElementName = "items")]
    [XmlType(TypeName = "item")]
    public partial class CasparItem
    {

        public CasparItem()
        {
           // Id = Guid.NewGuid();
        }
        [FieldNameAttribute("id", true, false, 500)]
        [XmlElement("id")]
        [DataMember(EmitDefaultValue = false)]
        public int Id { get; set; }

        [FieldNameAttribute("type", true, false, 500)]
        [XmlElement("type")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Type { get; set; }

        [FieldNameAttribute("devicename", true, false, 1000)]
        [XmlElement("devicename")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Devicename { get; set; }

        [FieldNameAttribute("label", true, false, 1000)]
        [XmlElement("label")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Label { get; set; }

        [FieldNameAttribute("name", true, false, 1000)]
        [XmlElement("name")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Name { get; set; }

        [FieldNameAttribute("channel", true, false, 1000)]
        [XmlElement("channel")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Channel { get; set; }

        [FieldNameAttribute("videolayer", true, false, 4)]
        [XmlElement("videolayer")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Videolayer { get; set; }

        [FieldNameAttribute("delay", true, false, 4)]
        [XmlElement("delay")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string Delay { get; set; }

        [FieldNameAttribute("duration", true, false, 4)]
        [XmlElement("duration")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Duration { get; set; }

        [FieldNameAttribute("allowgpi", true, false, 1)]
        [XmlElement("allowgpi")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Allowgpi { get; set; }

        [FieldNameAttribute("allowremotetriggering", true, false, 1)]
        [XmlElement("allowremotetriggering")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Allowremotetriggering { get; set; }

        [FieldNameAttribute("remotetriggerid", true, false, 1)]
        [XmlElement("remotetriggerid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Remotetriggerid { get; set; }

        [FieldNameAttribute("flashlayer", true, false, 4)]
        [XmlElement("flashlayer")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Flashlayer { get; set; }

        [FieldNameAttribute("invoke", true, false, 4)]
        [XmlElement("invoke")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Invoke { get; set; }

        [FieldNameAttribute("usestoreddata", true, false, 1)]
        [XmlElement("usestoreddata")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Usestoreddata { get; set; }

        [FieldNameAttribute("useuppercasedata", true, false, 1)]
        [XmlElement("useuppercasedata")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Useuppercasedata { get; set; }

        [FieldNameAttribute("color", true, false, 500)]
        [XmlElement("color")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Color { get; set; }

        [FieldNameAttribute("transition", true, false, 500)]
        [XmlElement("transition")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Transition { get; set; }

        [FieldNameAttribute("transitionduration", true, false, 4)]
        [XmlElement("transitionduration")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Transitionduration { get; set; }

        [FieldNameAttribute("tween", true, false, 500)]
        [XmlElement("tween")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Tween { get; set; }

        [FieldNameAttribute("direction", true, false, 500)]
        [XmlElement("direction")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Direction { get; set; }

        [FieldNameAttribute("seek", true, false, 4)]
        [XmlElement("seek")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Seek { get; set; }

        [FieldNameAttribute("length", true, false, 4)]
        [XmlElement("length")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Length { get; set; }

        [FieldNameAttribute("loop", true, false, 1)]
        [XmlElement("loop")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Loop { get; set; }

        [FieldNameAttribute("freezeonload", true, false, 1)]
        [XmlElement("freezeonload")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Freezeonload { get; set; }

        [FieldNameAttribute("triggeronnext", true, false, 1)]
        [XmlElement("triggeronnext")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Triggeronnext { get; set; }

        [FieldNameAttribute("autoplay", true, false, 1)]
        [XmlElement("autoplay")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Autoplay { get; set; }

        [FieldNameAttribute("timecode", true, false, 500)]
        [XmlElement("timecode")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Timecode { get; set; }

        
        [IgnoreDataMember]
        [XmlIgnore]
        public virtual System.DateTime CreationDate { get; set; }

        
        [IgnoreDataMember]
        [XmlIgnore]
        public virtual System.DateTime LastUpdateDate { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        [IgnoreDataMember]
        [XmlIgnore]
        public virtual System.String IsActive { get; set; }

        [FieldNameAttribute("positionx", true, false, 8)]
        [XmlElement("positionx")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Positionx { get; set; }

        [FieldNameAttribute("positiony", true, false, 8)]
        [XmlElement("positiony")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Positiony { get; set; }

        [FieldNameAttribute("scalex", true, false, 8)]
        [XmlElement("scalex")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Scalex { get; set; }

        [FieldNameAttribute("scaley", true, false, 8)]
        [XmlElement("scaley")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Scaley { get; set; }

      
        [FieldNameAttribute("defer", true, false, 1)]
        [XmlElement("defer")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Defer { get; set; }

        [FieldNameAttribute("device", true, false, 4)]
        [XmlElement("device")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Device { get; set; }

        [FieldNameAttribute("format", true, false, 500)]
        [XmlElement("format")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Format { get; set; }

        [FieldNameAttribute("showmask", true, false, 1)]
        [XmlElement("showmask")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Showmask { get; set; }

        [FieldNameAttribute("blur", true, false, 4)]
        [XmlElement("blur")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Blur { get; set; }

        [FieldNameAttribute("key", true, false, 500)]
        [XmlElement("key")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Key { get; set; }

        [FieldNameAttribute("spread", true, false, 8)]
        [XmlElement("spread")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Spread { get; set; }

        [FieldNameAttribute("spill", true, false, 4)]
        [XmlElement("spill")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Spill { get; set; }

        [FieldNameAttribute("threshold", true, false, 8)]
        [XmlElement("threshold")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Threshold { get; set; }

        [FieldNameAttribute("cropleft", true, false, 8)]
        [XmlElement("cropleft")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String CropLeft { get; set; }

        [FieldNameAttribute("cropright", true, false, 8)]
        [XmlElement("cropright")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String CropRight { get; set; }

        [FieldNameAttribute("croptop", true, false, 8)]
        [XmlElement("croptop")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String CropTop { get; set; }

        [FieldNameAttribute("cropbottom", true, false, 8)]
        [XmlElement("cropbottom")]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String CropBottom { get; set; }

        [FieldName("flashtemplateid", true, false, 4)]
        [XmlElement("flashtemplateid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string Flashtemplateid { get; set; }

        [FieldName("flashtemplatetypeid", true, false, 4)]
        [XmlElement("flashtemplatetypeid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string FlashTemplateTypeId { get; set; }
        
        [FieldName("templateid", true, false, 4)]
        [XmlElement("templateid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string Templateid { get; set; }
		
		[FieldName("flashtemplatewindowid", true, false, 4)]
        [XmlElement("flashtemplatewindowid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string Flashtemplatewindowid { get; set; }

        [FieldName("videoid", true, false, 4)]
        [XmlElement("videoid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string VideoId { get; set; }

        [FieldName("videowallid", true, false, 4)]
        [XmlElement("videowallid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string VideoWallId { get; set; }


        [FieldName("url", true, false, 4)]
        [XmlElement("url")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string Url { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [XmlElement(ElementName = "templatedata")]
        public TemplatesData TemplateData { get; set; }

        [FieldName("sequenceorder", true, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        [XmlElement(ElementName = "sequenceorder")]
        public virtual string SequenceOrder { get; set; }


        [FieldName("storyid", true, false, 4)]
        [XmlElement("storyid")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string Storyid { get; set; }

        [FieldName("resourcetag", true, false, 4)]
        [XmlElement("resourcetag")]
        [DataMember(EmitDefaultValue = false)]
        public virtual string ResourceTag { get; set; }


        [XmlIgnore]
        public CasparItemType ItemType
        {
            get
            {
                return (CasparItemType)Enum.Parse(typeof(CasparItemType), Type);
            }
            set
            {
                Type = value.ToString();
            }
        }

        #region IEquatable<MosActiveItemBase> Members


        public virtual void CopyFrom(CasparItem other)
        {
            if (other != null)
            {
                this.Type = other.Type;
                this.Devicename = other.Devicename;
                this.Label = other.Label;
                this.Name = other.Name;
                this.Channel = other.Channel;
                this.Videolayer = other.Videolayer;
                this.Delay = other.Delay;
                this.Duration = other.Duration;
                this.Allowgpi = other.Allowgpi;
                this.Allowremotetriggering = other.Allowremotetriggering;
                this.Remotetriggerid = other.Remotetriggerid;
                this.Flashlayer = other.Flashlayer;
                this.Invoke = other.Invoke;
                this.Usestoreddata = other.Usestoreddata;
                this.Useuppercasedata = other.Useuppercasedata;
                this.Color = other.Color;
                this.Transition = other.Transition;
                this.Transitionduration = other.Transitionduration;
                this.Tween = other.Tween;
                this.Direction = other.Direction;
                this.Seek = other.Seek;
                this.Length = other.Length;
                this.Loop = other.Loop;
                this.Freezeonload = other.Freezeonload;
                this.Triggeronnext = other.Triggeronnext;
                this.Autoplay = other.Autoplay;
                this.Timecode = other.Timecode;
                this.CreationDate = other.CreationDate;
                this.LastUpdateDate = other.LastUpdateDate;
                this.IsActive = other.IsActive;
                this.Positionx = other.Positionx;
                this.Positiony = other.Positiony;
                this.Scalex = other.Scalex;
                this.Scaley = other.Scaley;                
                this.Defer = other.Defer;
                this.Device = other.Device;
                this.Format = other.Format;
                this.Showmask = other.Showmask;                
                this.Blur = other.Blur;
                this.Key = other.Key;
                this.Spread = other.Spread;
                this.Spill = other.Spill;
                this.Threshold = other.Threshold;
                this.Flashtemplateid = other.Flashtemplateid;
                this.Id = other.Id;
                this.Templateid = other.Templateid;
                this.FlashTemplateTypeId = other.FlashTemplateTypeId;
                this.VideoId = other.VideoId;
                this.SequenceOrder = other.SequenceOrder;
                this.Storyid = other.Storyid;
            }
        }

        #endregion

    }

    public class TemplatesData
    {
         [XmlElement(ElementName = "componentdata")]
        public List<ComponentData> ComponentDataList { get; set; }
    }

    public class ComponentData
    {
        [FieldName("id", true, false, 4)]
        [XmlElement("id")]
        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [FieldName("value", true, false, 4)]
        [XmlElement("value")]
        [DataMember(EmitDefaultValue = false)]
        public string Value { get; set; }
    }
    
    public class FieldNameAttribute : Attribute
    {
        public string FieldName { get; private set; }
        public bool AllowNull { get; private set; }
        public bool ForeignKeyField { get; private set; }
        public int Length { get; private set; }
        public FieldNameAttribute(string fieldName, bool isAllowNull, bool isForeignField, int length)
        {
            this.FieldName = fieldName;
            this.ForeignKeyField = isForeignField;
            this.AllowNull = isAllowNull;
            this.Length = length;
        }
    }
}
