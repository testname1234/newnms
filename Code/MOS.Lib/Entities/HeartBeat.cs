﻿using System;
using System.Xml.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [Serializable,XmlRoot("heartbeat")]
    public class HeartBeat : MOSCommand
    {
        /// <summary>
        /// Format is YYYY-MM-DD'T'hh:mm:ss[,ddd]['Z'], e.g. 2009-04-11T14:22:07,125Z or 2009-04-11T14:22:07,125-05:00.
        /// </summary>

        [XmlElement("time")]
        public DateTime time { get; set; }
    }
}
