﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("mos")]
    public class MOS
    {
        /// <summary>
        /// Character name for the MOS unique within a particular installation.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("mosID")]
        public string mosID { get; set; }

        /// <summary>
        /// Character name for the NCS unique within a particular installation. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("ncsID")]
        public string ncsID { get; set; }

        [XmlElement("groupID")]
        public string groupID { get; set; }

        [XmlElement("setid")]
        public List<int> SetId { get; set; }


        /// <summary>
        /// Unique identifier sent with requests.
        /// Format: signed integer 32-bit, value above or equal to 1, decimal or hexadecimal.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [XmlElement("messageID")]
        public Int32 messageID { get; set; }

        /// <summary>
        /// MOS Command. For Example 'heartbeat', 'roCreate', 'roStoryInsert' etc
        /// </summary>
        [XmlElement(Type = typeof(HeartBeat), ElementName = "heartbeat")]
        [XmlElement(Type = typeof(ROCreate), ElementName = "roCreate")]
        [XmlElement(Type = typeof(ROStoryInsert), ElementName = "roStoryInsert")]
        [XmlElement(Type = typeof(ROAck), ElementName = "roAck")]
        [XmlElement(Type = typeof(ROStorySend), ElementName = "roStorySend")]
        [XmlElement(Type = typeof(ROStoryDelete), ElementName = "roStoryDelete")]
        [XmlElement(Type = typeof(ROStorySwap), ElementName = "roStorySwap")]
        [XmlElement(Type = typeof(ROStoryMove), ElementName = "roStoryMove")]
        [XmlElement(Type = typeof(Notification), ElementName = "notification")]
        [XmlElement(Type = typeof(MOSRunOrder), ElementName = "mosRunorder")]



        public MOSCommand command { get; set; }
    }
}
