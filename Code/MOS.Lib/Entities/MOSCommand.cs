﻿using System;
using System.Xml.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [Serializable()]
    [XmlInclude(typeof(HeartBeat))]
    [XmlInclude(typeof(ROCreate))]
    [XmlInclude(typeof(ROStoryInsert))]
    [XmlInclude(typeof(ROStorySend))]
    [XmlInclude(typeof(ROStoryDelete))]
    [XmlInclude(typeof(ROStorySwap))]
    [XmlInclude(typeof(ROStoryMove))]
    public class MOSCommand
    {

    }
}
