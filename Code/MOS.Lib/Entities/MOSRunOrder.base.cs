﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [DataContract]
    public abstract partial class MOSRunOrderBase : MOSCommand
	{
			
		[FieldNameAttribute("Slug",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("StartTime",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string StartTimeStr
		{
			 get { return StartTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Duration",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime Duration{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string DurationStr
		{
			 get { return Duration.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Duration = date.ToUniversalTime();  }  } 
		}

		
		[FieldNameAttribute("RoId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RoId{ get; set; }

	
		
		
		
		
	}
	
	
}
