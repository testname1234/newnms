﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace MOSProtocol.Lib.Entities
{
    [DataContract]
    public partial class MOSRunOrder : MOSRunOrderBase 
	{

        public List<MOSStory> Stories {get; set; }
        public List<MOSUserAction> UserActionsPerformed {get; set;}
    }
}
