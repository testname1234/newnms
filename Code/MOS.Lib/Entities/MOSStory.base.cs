﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [DataContract]
    public abstract partial class MOSStoryBase : MOSCommand
    {

        [FieldNameAttribute("RoId", false, true, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 RoId { get; set; }

        [FieldNameAttribute("SequenceId", false, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 SequenceId { get; set; }

        [FieldNameAttribute("IsPlayed", true, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean? IsPlayed { get; set; }

        [FieldNameAttribute("IsSkipped", false, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean IsSkipped { get; set; }

        [FieldNameAttribute("Slug", false, false, 500)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Slug { get; set; }


        [FieldNameAttribute("StoryId", false, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 StoryId { get; set; }

    }
	
}
