﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace MOSProtocol.Lib.Entities
{
    [DataContract]
    public partial class MOSStory : MOSStoryBase 
	{

        public List<MOSStoryItem> StoryItems { get; set; }
		
	}
}
