﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [DataContract]
    public abstract partial class MOSStoryItemBase:MOSCommand
	{
			
		[FieldNameAttribute("StoryItemId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StoryItemId{ get; set; }

		[FieldNameAttribute("StoryId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StoryId{ get; set; }

		[FieldNameAttribute("SequenceId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceId{ get; set; }

		[FieldNameAttribute("Slug",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("Detail",false,false,4000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Detail{ get; set; }

		[FieldNameAttribute("Instructions",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Instructions{ get; set; }

		
		
		
		
		
		
	}
	
	
}
