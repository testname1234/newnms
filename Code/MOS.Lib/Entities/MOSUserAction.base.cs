﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [DataContract]
    public abstract partial class MOSUserActionBase:MOSCommand
	{
			
		[FieldNameAttribute("ActionId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ActionId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,0)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("RoId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RoId{ get; set; }

		[FieldNameAttribute("StoryId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StoryId{ get; set; }

		[FieldNameAttribute("UserId",false,true,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		
		[FieldNameAttribute("UserActionId",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserActionId{ get; set; }

	}
	
	
}
