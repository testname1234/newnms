﻿using MOSProtocol.Lib.Enums;
using System;
using System.Xml.Serialization;

namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("notification")]
    public class Notification :MOSCommand
    {
        [XmlElement("groupID")]
        public string groupID { get; set; }

        [XmlElement("roID")]
        public string roID { get; set; }

        [XmlElement("storyID")]
        public string storyID { get; set; }

        [XmlElement("storyStatus")]
        public StoryStatus storyStatus { get; set; }
    }
}
