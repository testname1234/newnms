﻿using System;
using System.Xml.Serialization;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [XmlRoot("objPaths")]
    public class ObjPaths
    {
        /// <summary>
        /// This is an unambiguous path to a media file - essence. The field length is 255 chars max, 
        /// but it is suggested that the length be kept to a minimum number of characters. 
        /// These path formats are acceptable:
        /// UNC (eg: \\machine\directory\file.extension)
        /// URL (eg: http://machine/directory/file.extension)
        /// FTP (eg: ftp://machine/directory/file.extension)
        /// </summary>
        [FieldLength(MaxLength = 255)]
        public string objPath { get; set; }

        /// <summary>
        /// This is an unambiguous path to a media file – proxy. The field length is 255 chars max, 
        /// but it is suggested that the length be kept to a minimum number of characters. 
        /// These path formats are acceptable:
        /// UNC (eg: \\machine\directory\file.extension)
        /// URL (eg: http://machine/directory/file.extension)
        /// (note: FTP is *NOT* allowed for objProxyPath)
        /// </summary>
        [FieldLength(MaxLength = 255)]
        public string objProxyPath { get; set; }

        /// <summary>
        /// This is an unambiguous path to the xml file – MOS Object. This field length is 255 chars max, 
        /// but is is suggested that the length be kept to a minimum number of characters. 
        /// These path formats are acceptable:
        /// UNC (eg: \\machine\directory\file.extension)
        /// URL (eg: http://machine/directory/file.extension)
        /// FTP (eg: ftp://machine/directory/file.extension)
        /// </summary>
        [FieldLength(MaxLength = 255)]
        public string objMetadataPath { get; set; }
    }
}
