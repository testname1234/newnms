﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Linq;
using MOSProtocol.Lib.Enums;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("roAck")]
    public class ROAck : MOSCommand, IXmlSerializable
    {
        /// <summary>
        /// Running Order UID: Unique Identifier defined by NCS. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [FieldNullable(IsNullable = false)]
        [XmlElement("roID")]
        public string roID { get; set; }

        /// <summary>
        /// Running Order Status: Options are: "OK" or error description. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("roStatus")]
        public string roStatus { get; set; }

        public string roAckMosId { get; set; }

        public List<ROAckStory> roAckStories { get; set; }

        public List<ROAck> roAck { get; set; }

        #region Methods

        public void WriteXml(XmlWriter writer)
        {
            StringBuilder xml = new StringBuilder();
            if (!string.IsNullOrEmpty(roID))
            {
                xml.Append(string.Format("<roID>{0}</roID>", roID));
            }
            if (!string.IsNullOrEmpty(roStatus))
            {
                xml.Append(string.Format("<roStatus>{0}</roStatus>", roStatus));
            }
            if (roAckStories != null)
            {
                foreach (ROAckStory item in roAckStories)
                {
                    if (!string.IsNullOrEmpty(item.storyID))
                    {
                        xml.Append(string.Format("<storyID>{0}</storyID>", item.storyID));
                    }
                    if (!string.IsNullOrEmpty(item.status))
                    {
                        xml.Append(string.Format("<status>{0}</status>", item.status));
                    }
                    if (!string.IsNullOrEmpty(item.itemID))
                    {
                        xml.Append(string.Format("<itemID>{0}</itemID>", item.itemID));
                    }
                    if (!string.IsNullOrEmpty(item.objID))
                    {
                        xml.Append(string.Format("<objID>{0}</objID>", item.objID));
                    }
                    if (!string.IsNullOrEmpty(item.itemChannel))
                    {
                        xml.Append(string.Format("<itemChannel>{0}</itemChannel>", item.itemChannel));
                    }
                }
            }

            writer.WriteRaw(xml.ToString());
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.Read())
            {
                if (!string.IsNullOrEmpty(reader.Name))
                {
                    if (reader.Name == "roID")
                    {
                        roID = reader.ReadString();
                    }
                    else if (reader.Name == "roStatus")
                    {
                        roStatus = reader.ReadString();
                    }
                    else if (reader.Name == "storyID")
                    {
                        string storyId = reader.ReadString();
                        if (roAckStories == null)
                        {
                            roAckStories = new List<ROAckStory>();
                        }
                        ROAckStory story = new ROAckStory() { storyID = storyId };
                        roAckStories.Add(story);
                    }
                    else if (reader.Name == "status")
                    {
                        if (roAckStories != null)
                        {
                            ROAckStory story = roAckStories.Last();
                            story.status = reader.ReadString();
                        }
                    }
                    else if (reader.Name == "itemID")
                    {
                        if (roAckStories != null)
                        {
                            ROAckStory story = roAckStories.Last();
                            story.itemID = reader.ReadString();
                        }
                    }
                    else if (reader.Name == "objID")
                    {
                        if (roAckStories != null)
                        {
                            ROAckStory story = roAckStories.Last();
                            story.objID = reader.ReadString();
                        }
                    }
                    else if (reader.Name == "itemChannel")
                    {
                        if (roAckStories != null)
                        {
                            ROAckStory story = roAckStories.Last();
                            story.itemChannel = reader.ReadString();
                        }
                    }

                }
            }
        }

        public XmlSchema GetSchema()
        {
            return (null);
        }

        #endregion

    }

    public class ROAckStory
    {
        /// <summary>
        /// Story UID: Defined by the NCS. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("storyID")]
        public string storyID { get; set; }

        /// <summary>
        /// Status: Options are “NEW” “UPDATED” “MOVED” “BUSY “ “DELETED”, "NCS CTRL", "MANUAL CTRL", "READY", "NOT READY", "PLAY," "STOP".
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("status")]
        public string status { get; set; }

        /// <summary>
        /// Item ID: Defined by NCS, UID not required. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("itemID")]
        public string itemID { get; set; }

        /// <summary>
        /// Object UID: Unique ID generated by the MOS and assigned to this object. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("objID")]
        public string objID { get; set; }

        /// <summary>
        /// Item Channel: Channel requested by the NCS for MOS to playback a running order item. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("itemChannel")]
        public string itemChannel { get; set; }

        [XmlIgnore]
        public StoryStatus storyStatus
        {
            get
            {
                string st = status.Replace(" ", "_");
                return (StoryStatus)Enum.Parse(typeof(StoryStatus), st, true);
            }
            set
            {
                status = value.ToString();
                status = status.Replace("_", " ");
            }
        }
    }
}
