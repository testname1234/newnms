﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("roCreate")]
    public class ROCreate : MOSCommand
    {
        /// <summary>
        /// Running Order UID: Unique Identifier defined by NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("roID")]
        public string roID { get; set; }

        /// <summary>
        /// Running Order Slug: Textual Running Order description. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("roSlug")]
        public string roSlug { get; set; }

        /// <summary>
        /// Running Order Channel: default channel requested by the NCS for MOS 
        /// to playback a running order. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        public string roChannel { get; set; }

        /// <summary>
        /// Running Order Editorial Start: date and time requested by NCS for MOS 
        /// to start playback of a running order. Format is YYYY-MM-DD'T'hh:mm:ss[,ddd]['Z'],
        /// e.g. 2009-04-11T14:22:07,125Z or 2009-04-11T14:22:07,125-05:00. Parameters displayed
        /// within brackets are optional. [,ddd] represents fractional time in which all three 
        /// digits must be present. [‘Z’] indicates time zone which can be expressed as an offset
        /// from UTC in hours and minutes. Optionally, the time zone may be replaced by the 
        /// character ‘Z’ to indicate UTC.
        /// </summary>
        [XmlElement("roEdStart")]
        public string roEdStart { get; set; }

        /// <summary>
        /// Running Order Editorial Duration: duration of entire running order. Format in hh:mm:ss, e.g. 00:58:25.
        /// </summary>
        [XmlElement("roEdDur")]
        public string roEdDur { get; set; }

        /// <summary>
        /// Running Order Air Trigger: “MANUAL”, “TIMED” or “CHAINED”.
        /// CHAINED (sign +/-) (value in # of samples)
        /// CHAINED -10 would start the specified clip 10 samples before the proceeding clip ended. 
        /// CHAINED 10 would start the specified clip 10 samples after the preceding clip ended, 
        /// thus making a pause of 10 samples between the clips. There is a space character between the word CHAINED and the value.
        /// slug Textual Object ID: This is the text slug of the object and is stored in the native language. 
        /// This can be stored in a language other than English. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("roTrigger")]
        public string roTrigger { get; set; }

        /// <summary>
        /// Macro Transition In: Defined by MOS. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        public string macroIn { get; set; }

        /// <summary>
        /// Macro Transition Out: Defined by MOS. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        public string macroOut { get; set; }

        /// <summary>
        /// The mosExternalMetadata block can appear in several messages as a mechanism for transporting additional metadata, 
        /// independent of schema or DTD.
        /// </summary>
        public string mosExternalMetadata { get; set; }

        /// <summary>
        /// Story: Container for story information in a Running Order message.
        /// </summary>
        [XmlElement("story")]
        public List<Story> story { get; set; }

        public List<CasparItem> casparItems { get; set; }

    }
}
