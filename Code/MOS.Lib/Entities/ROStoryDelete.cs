﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Validation;


namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("roStoryDelete")]
    public class ROStoryDelete : MOSCommand
    {
        /// <summary>
        /// Running Order UID: Unique Identifier defined by NCS. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [FieldNullable(IsNullable = false)]
        public string roID { get; set; }
        
        /// <summary>
        /// Story UID: Defined by the NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("storyID")]
        public string storyID { get; set; }

              

    }
}
