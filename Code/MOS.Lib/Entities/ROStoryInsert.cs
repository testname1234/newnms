﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Validation;


namespace MOSProtocol.Lib.Entities
{
    [XmlRoot("roStoryInsert")]
    public class ROStoryInsert : MOSCommand
    {
        /// <summary>
        /// Running Order UID: Unique Identifier defined by NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        public string roID { get; set; }

        /// <summary>
        /// Story UID: Defined by the NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        public string storyID { get; set; }

        public List<Story> story { get; set; }
    }
}
