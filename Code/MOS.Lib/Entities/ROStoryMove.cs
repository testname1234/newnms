﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Linq;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("roStoryMove")]
    public class ROStoryMove : MOSCommand, IXmlSerializable
    {
        /// <summary>
        /// Running Order UID: Unique Identifier defined by NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("roID")]
        public string roID { get; set; }
                
        public List<ROStoryMoveStories> roStoryMoveStories { get; set; }

        #region Methods

        public void WriteXml(XmlWriter writer)
        {
            StringBuilder xml = new StringBuilder();
            if (!string.IsNullOrEmpty(roID))
            {
                xml.Append(string.Format("<roID>{0}</roID>", roID));
            }
            foreach (ROStoryMoveStories item in roStoryMoveStories)
            {
                if (!string.IsNullOrEmpty(item.storyID))
                {
                    xml.Append(string.Format("<storyID>{0}</storyID>", item.storyID));
                }
              
            }

            writer.WriteRaw(xml.ToString());
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.Read())
            {
                if (!string.IsNullOrEmpty(reader.Name))
                {
                    if (reader.Name == "roID")
                    {
                        roID = reader.ReadString();
                    }
                    else if (reader.Name == "storyID")
                    {
                        string storyId = reader.ReadString();
                        if (roStoryMoveStories == null)
                        {
                            roStoryMoveStories = new List<ROStoryMoveStories>();
                        }
                        ROStoryMoveStories story = new ROStoryMoveStories() { storyID = storyId };
                        roStoryMoveStories.Add(story);
                    }
                   
                }
            }
        }

        public XmlSchema GetSchema()
        {
            return (null);
        }

        #endregion

    }


    public class ROStoryMoveStories
    {
        /// <summary>
        /// Story UID: Defined by the NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("storyID")]
        public string storyID { get; set; }
        
    }
}
