﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Linq;
using Validation;
using System.Text.RegularExpressions;

namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("roStorySend")]
    public class ROStorySend : MOSCommand, IXmlSerializable
    {
        /// <summary>
        /// Running Order UID: Unique Identifier defined by NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("roID")]
        public string roID { get; set; }

        /// <summary>
        /// Story UID: Defined by the NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("storyID")]
        public string storyID { get; set; }

        /// <summary>
        /// Story Slug: Textual Story description. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("storySlug")]
        public string storySlug { get; set; }

        /// <summary>
        /// Story Number: The name or number of the Story as used in the NCS. 
        /// This is an optional field originally intended for use by prompters. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("storyNum")]
        public string storyNum { get; set; }

        [FieldName("languagecode", true, false, 4)]
        [XmlElement("languagecode")]
        public virtual string LanguageCode { get; set; }

        /// <summary>
        /// Story Body: The actual text of the story within a running order.
        /// </summary>
        [XmlElement("storyBody")]
        public List<StoryBody> storyBodies { get; set; }


        #region Methods

        public void WriteXml(XmlWriter writer)
        {
            StringBuilder xml = new StringBuilder();
            if (!string.IsNullOrEmpty(roID))
            {
                xml.Append(string.Format("<roID>{0}</roID>", roID));
            }
            if (!string.IsNullOrEmpty(storyID))
            {
                xml.Append(string.Format("<storyID>{0}</storyID>", storyID));
            }
            if (!string.IsNullOrEmpty(storySlug))
            {
                xml.Append(string.Format("<storySlug>{0}</storySlug>", storySlug));
            }
            if (!string.IsNullOrEmpty(storyNum))
            {
                xml.Append(string.Format("<storyNum>{0}</storyNum>", storyNum));
            }

            xml.Append("<storyBody>");

            if (storyBodies != null)
            {
                foreach (StoryBody item in storyBodies)
                {
                    if (!string.IsNullOrEmpty(item.ParagraphWithInstructions))
                    {
                        xml.Append(string.Format("<p><pi>{0}</pi></p>", item.ParagraphWithInstructions));
                    }
                    if (!string.IsNullOrEmpty(item.Paragraph))
                    {
                        xml.Append(string.Format("<p>{0}</p>", item.Paragraph));
                    }
                }
            }
            xml.Append("</storyBody>");
           // xml = xml.ToString();
             string a = ReplaceHexadecimalSymbols(xml.ToString());
            writer.WriteRaw(a);
        }


        static string ReplaceHexadecimalSymbols(string txt)
        {
            string r = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]";
            return Regex.Replace(txt, r, "", RegexOptions.Compiled);
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.Read())
            {
                if (!string.IsNullOrEmpty(reader.Name))
                {
                    if (reader.Name == "roID")
                    {
                        roID = reader.ReadString();
                    }
                    if (reader.Name == "storyID")
                    {
                        storyID = reader.ReadString();
                    }
                    if (reader.Name == "storySlug")
                    {
                        storySlug = reader.ReadString();
                    }
                    if (reader.Name == "storyNum")
                    {
                        storyNum = reader.ReadString();
                    }
                    else if (reader.Name == "p")
                    {
                        reader.Read();

                        if (reader.Name == "pi")
                        {
                            if (storyBodies == null)
                            {
                                storyBodies = new List<StoryBody>();
                            }

                            string pi = reader.ReadString();
                            StoryBody storyBody = new StoryBody() { ParagraphWithInstructions = pi };
                            storyBodies.Add(storyBody);
                        }

                        else if (reader.Name.Equals("p") && storyBodies != null)
                        {
                            string p = reader.ReadString();
                            StoryBody storyBody = storyBodies.LastOrDefault();
                            storyBody.Paragraph = p;

                        }
                    }
                }
            }
        }

        public XmlSchema GetSchema()
        {
            return (null);
        }

        #endregion


    }

}
