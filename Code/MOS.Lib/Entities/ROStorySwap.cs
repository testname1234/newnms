﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Linq;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [Serializable, XmlRoot("roStorySwap")]
    public class ROStorySwap : MOSCommand, IXmlSerializable
    {
        /// <summary>
        /// Running Order UID: Unique Identifier defined by NCS. 128 chars max.
        /// </summary>
        [FieldNullable(IsNullable = false)]
        [FieldLength(MaxLength = 128)]
        [XmlElement("roID")]
        public string roID { get; set; }
                
        public List<ROStorySwapStories> roStorySwapStories { get; set; }

        #region Methods

        public void WriteXml(XmlWriter writer)
        {
            StringBuilder xml = new StringBuilder();
            if (!string.IsNullOrEmpty(roID))
            {
                xml.Append(string.Format("<roID>{0}</roID>", roID));
            }
            foreach (ROStorySwapStories item in roStorySwapStories)
            {
                if (!string.IsNullOrEmpty(item.storyID))
                {
                    xml.Append(string.Format("<storyID>{0}</storyID>", item.storyID));
                }
              
            }

            writer.WriteRaw(xml.ToString());
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.Read())
            {
                if (!string.IsNullOrEmpty(reader.Name))
                {
                    if (reader.Name == "roID")
                    {
                        roID = reader.ReadString();
                    }
                    else if (reader.Name == "storyID")
                    {
                        string storyId = reader.ReadString();
                        if (roStorySwapStories == null)
                        {
                            roStorySwapStories = new List<ROStorySwapStories>();
                        }
                        ROStorySwapStories story = new ROStorySwapStories() { storyID = storyId };
                        roStorySwapStories.Add(story);
                    }
                   
                }
            }
        }

        public XmlSchema GetSchema()
        {
            return (null);
        }

        #endregion

    }

        public class ROStorySwapStories
        {
            /// <summary>
            /// Story UID: Defined by the NCS. 128 chars max.
            /// </summary>
            [FieldNullable(IsNullable = false)]
            [FieldLength(MaxLength = 128)]
            [XmlElement("storyID")]
            public string storyID { get; set; }
        
        }
}
