﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [Serializable]
    public class Story
    {
        /// <summary>
        /// Story UID: Defined by the NCS. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("storyID")]
        public string storyID { get; set; }

        /// <summary>
        /// Story Slug: Textual Story description. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("storySlug")]
        public string storySlug { get; set; }

        /// <summary>
        /// Story Number: The name or number of the Story as used in the NCS. 
        /// This is an optional field originally intended for use by prompters. 128 chars max.
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("storyNum")]
        public string storyNum { get; set; }

        [FieldName("languagecode", true, false, 4)]
        [XmlElement("languagecode")]
        public string LanguageCode { get; set; }
            
        /// <summary>
        /// The mosExternalMetadata block can appear in several messages as a mechanism for transporting additional metadata, independent of schema or DTD.
        /// </summary>
        public string mosExternalMetadata { get; set; }

        /// <summary>
        /// Item: Container for item information within a Running Order message.
        /// </summary>
        public List<Item> item { get; set; }
    }
}
