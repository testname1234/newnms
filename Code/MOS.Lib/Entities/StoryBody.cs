﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Validation;

namespace MOSProtocol.Lib.Entities
{
    [Serializable]
    public class StoryBody
    {
        /// <summary>
        /// Presenter instructions: Instructions to the anchor or presenter that are not to be read such as "Turn to 2-shot."
        /// </summary>
        [FieldLength(MaxLength = 128)]
        [XmlElement("pi")]
        public string ParagraphWithInstructions { get; set; }

        /// <summary>
        /// Paragraph: Standard html delimitation for a new paragraph.
        /// </summary>
        [XmlElement("p")]
        public string Paragraph { get; set; }

        [FieldName("languagecode", true, false, 4)]
        [XmlElement("languagecode")]
        public virtual string LanguageCode { get; set; }
    }
}
