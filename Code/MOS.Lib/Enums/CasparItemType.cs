﻿using System;

namespace MOSProtocol.Lib.Enums
{
    public enum CasparItemType
    {
        TEMPLATE=1,
        MOVIE=2,
        TRANSFORMATION=3,
        STILL = 4,
        AUDIO=5,
        CROP=6,
        DECKLINKINPUT=7
    }
}
