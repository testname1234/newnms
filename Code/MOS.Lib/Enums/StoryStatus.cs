﻿using System;

namespace MOSProtocol.Lib.Enums
{
    public enum StoryStatus
    {
        NEW,
        UPDATED,
        MOVED,
        BUSY,
        DELETED,
        NCS_CTRL,
        MANUAL_CTRL,
        READY,
        NOT_READY,
        PLAY,
        STOP,
        NACK
    }
}
