﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MOSProtocol.Lib
{
    public static class Helper
    {
        public static string GetStringForMOSCommand<T>(T command)
        {
            StringBuilder stringBuilder = new StringBuilder();

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.OmitXmlDeclaration = true;

            XmlSerializerNamespaces xmlEmptyNameSpace = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });

            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlWriterSettings);

            XmlSerializer xml = new XmlSerializer(typeof(T));
            xml.Serialize(xmlWriter, command, xmlEmptyNameSpace);

            return stringBuilder.ToString();
        }
    }
}
