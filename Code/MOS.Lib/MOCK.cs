﻿using MOSProtocol.Lib.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MOSProtocol.Lib.Entities
{
    public class MOCK
    {

        public string _mosId { get; set; }
        public string _NCSId { get; set; }

        public MOS GetMockROCreate(string RoID, int StoryFrom, int StoryTo,string time)
        {
            MOS command = new MOS();
            command.mosID = _mosId;
            command.ncsID = _NCSId;

            string roid = RoID; //(new Random()).Next().ToString();

            ROCreate ro = new ROCreate();
            ro.roID = roid;
            ro.roSlug = "Bol & News Rundown 10PM";
            ro.roEdStart = time; //DateTime.UtcNow.ToString();
            ro.roEdDur = DateTime.UtcNow.ToString();//DateTime.Now.ToUniversalTime().ToString();
            ro.roTrigger = "TIMED";
            ro.story = new System.Collections.Generic.List<Story>();

            for (int i = StoryFrom; i <= StoryTo; i++)
            {
                Story story = new Story();
                story.storyID = Convert.ToString(i);
                story.storySlug = "STORY SLUG  " + Convert.ToString(i);
                story.storyNum = Convert.ToString(i);
                ro.story.Add(story);
            }

            command.command = ro;

            return command;
        }

        public MOS GetMockStroySend(string RoID, int storyId)
        {
            MOS command = new MOS();
            command.mosID = _mosId;
            command.ncsID = _NCSId;

            ROStorySend roStorySend = new ROStorySend();
            roStorySend.roID = RoID;
            roStorySend.storyID = storyId.ToString();
            roStorySend.storySlug = "A & B";
            roStorySend.storyNum = storyId.ToString();
            roStorySend.storyBodies = new List<StoryBody>();

            for (int i = 0; i < 3; i++)
            {
                StoryBody storyBody = new StoryBody();
                storyBody.ParagraphWithInstructions = "OC" + i;
                storyBody.Paragraph = "This is OC DetailThis is OC  ENDDDDDD" + i; 
                
                roStorySend.storyBodies.Add(storyBody);
            }


            command.command = roStorySend;

            return command;
        }

        public MOS GetMockStroyDelete()
        {
            MOS command = new MOS();
            command.mosID = _mosId;
            command.ncsID = _NCSId;

            string roid = "1"; //(new Random()).Next().ToString();

            ROStoryDelete roStoryDelete = new ROStoryDelete();
            roStoryDelete.roID = roid;
            roStoryDelete.storyID = "4";
            command.command = roStoryDelete;

            return command;
        }

        public MOS GetMockStroySwap()
        {
            MOS command = new MOS();
            command.mosID = _mosId;
            command.ncsID = _NCSId;

            string roid = "1"; //(new Random()).Next().ToString();

            ROStorySwap roStorySwap = new ROStorySwap();
            roStorySwap.roID = roid;
            roStorySwap.roStorySwapStories = new System.Collections.Generic.List<ROStorySwapStories>();

            ROStorySwapStories story = new ROStorySwapStories();
            story.storyID = "1";
            roStorySwap.roStorySwapStories.Add(story);

            story = new ROStorySwapStories();
            story.storyID = "2";
            roStorySwap.roStorySwapStories.Add(story);

            command.command = roStorySwap;

            return command;
        }

        public MOS GetMockStroyMove()
        {
            MOS command = new MOS();
            command.mosID = _mosId;
            command.ncsID = _NCSId;

            string roid = "1"; //(new Random()).Next().ToString();

            ROStoryMove roStoryMove = new ROStoryMove();
            roStoryMove.roID = roid;
            roStoryMove.roStoryMoveStories = new System.Collections.Generic.List<ROStoryMoveStories>();

            ROStoryMoveStories story = new ROStoryMoveStories();
            story.storyID = "4";
            roStoryMove.roStoryMoveStories.Add(story);

            // Move above Story Id = 1
            story = new ROStoryMoveStories();
            story.storyID = "1";
            roStoryMove.roStoryMoveStories.Add(story);

            command.command = roStoryMove;

            return command;
        }

        public MOS GetMockStroyUnSkip()
        {
            MOS command = new MOS();
            command.mosID = _mosId;
            command.ncsID = _NCSId;

            string roid = "1"; //(new Random()).Next().ToString();

            ROStoryInsert roStoryInsert = new ROStoryInsert();
            roStoryInsert.roID = roid;
            roStoryInsert.storyID = "5";
            roStoryInsert.story = new System.Collections.Generic.List<Story>();

            Story story = new Story();
            story.storyID = "4";
            story.storySlug = "Story & Slug 4";
            story.storyNum = "4";
            roStoryInsert.story.Add(story);

            command.command = roStoryInsert;

            return command;
        }

        public MOS GetMockCasparROCreate(string RoID, string xmlPath, string slug)
        {
            MOS command = new MOS();
            command.mosID = _mosId;
            command.ncsID = _NCSId;

            string roid = RoID;
            if (string.IsNullOrEmpty(RoID))
            {
                roid = new Random().Next(1, 1000).ToString();
            }

            ROCreate ro = new ROCreate();
            ro.roID = roid;
            ro.roSlug = slug + "-" + roid;
            ro.roEdStart = DateTime.UtcNow.ToString();
            ro.roEdDur = DateTime.Now.AddMinutes(5).ToString();
            ro.roTrigger = "TIMED";
            ro.casparItems = new List<CasparItem>();


            using (StreamReader reader = new StreamReader(xmlPath))
            {
                string xml = reader.ReadToEnd();

                // ro.casparItems = Common.Helper.DeserializeFromXml<List<CasparItem>>(xml,"items");
                MOS mos = Common.Helper.DeserializeFromXml<MOS>(xml);
                ro.casparItems = ((ROCreate)mos.command).casparItems;
                ro.story = ((ROCreate)mos.command).story;
                ro.roID = ((ROCreate)mos.command).roID;
                command.SetId = mos.SetId;
                command.mosID = mos.mosID;
                command.ncsID = mos.ncsID;
                command.messageID = mos.messageID;
                // command = mos;
            }


            command.command = ro;

            return command;
        }

        public MOS GetMockNotification(string roId,string storyId )
        {
            MOS mos = new MOS();
            mos.ncsID = _NCSId;
            mos.mosID = _mosId;
            mos.messageID = 1;
            Notification notification = new Notification();
            notification.groupID = "pcr";
            notification.roID = roId;
            notification.storyID = storyId;
            notification.storyStatus = StoryStatus.READY;
            mos.command = notification;
            return mos;

        }
    }
}
