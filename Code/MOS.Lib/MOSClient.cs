﻿using MOSProtocol.Lib.Entities;
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using System.Threading;

namespace MOSProtocol.Lib
{
    public class MOSClient :  MOSProtocol,IDisposable
    {
        #region Private Properties

        private TcpClient client;
        private NetworkStream networkStream;
        private string _mosId = string.Empty;
        private string _server = string.Empty;
        private int _port = 10541;
        private string _ncsId = string.Empty;
        private const int ReceiveTimeout = 20000;
        private const int SendTimeout = 20000;
        private int messageId = 1;

        #endregion

        public string GroupId { get; set; }

        public bool IsConnected
        {
            get
            {
                if (client != null)
                {
                    return client.Connected;
                }
                return false;
            }
        }

        public void Dispose()
        {
            if (client != null)
            {
                client.Client.Dispose();
                client = null;
            }
        }

        public MOSClient(string mosId, string server,int port, string ncsId)
        {
            _mosId = mosId;
            _server = server;
            _port = port;
            _ncsId = ncsId;
        }

        public bool Connect(out string message)
        {
            bool result = false;
            message = string.Empty;
            try
            {
                if (client == null || !client.Connected)
                {
                    client = new TcpClient(_server, _port);
                    //client.Client.BeginDisconnect(true, new AsyncCallback(Reconnect), client);
                    client.ReceiveTimeout = ReceiveTimeout;
                    client.SendTimeout = SendTimeout;
                    networkStream = client.GetStream();
                    result = true;
                }
            }
            catch (ArgumentNullException e)
            {
                message = string.Format("\nArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                message = string.Format("\nSocketException: {0}", e);
            }
            return result;
        }

        public void Reconnect(IAsyncResult ar)
        {
            string message = string.Empty;
            Connect(out message);
        }

        public bool Disconnect(out string message)
        {
            bool result = false;
            message = string.Empty;
            try
            {
                if (client != null && client.Connected)
                {
                    client.Close();
                    result = true;
                }
            }
            catch (ArgumentNullException e)
            {
                message = string.Format("\nArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                message = string.Format("\nSocketException: {0}", e);
            }
            return result;
        }

        public MOS SendHeartBeat(out string message)
        {
            message = string.Empty;
            MOS mosResponse = new MOS();

            try
            {
                if (client != null && client.Connected)
                {
                    MOS mos = new MOS();
                    mos.mosID = _mosId;
                    mos.ncsID = _ncsId;

                    HeartBeat heartbeat = new HeartBeat();
                    heartbeat.time = DateTime.UtcNow.AddDays(-1);

                    mos.command = heartbeat;

                    mosResponse = SendMOSCommand(mos, out message);

                }
                else
                {
                    message = "Error: Not Connected.";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return mosResponse;
        }

        public MOS SendMOSCommand(MOS command, out string message)
        {
            command.mosID = _mosId;
            command.ncsID = _ncsId;
            command.messageID = messageId++;
            message = string.Empty;
            MOS mosResponse = new MOS();

            try
            {
                if (client != null
                    && client.Connected)
                {
                    SendMOS(networkStream, command, out message);
                    mosResponse = ReceiveMOS(networkStream);
                }
                else
                {
                    message = "Error: Not Connected.";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return mosResponse;
        }

        public void SendDataToClient(string data)
        {
            Common.Helper.SendData(networkStream, data,System.Text.Encoding.BigEndianUnicode);
        }
    }
}
