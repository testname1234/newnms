﻿using MOSProtocol.Lib.Entities;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Validation;

namespace MOSProtocol.Lib
{
    public class MOSProtocol
    {
        public MOS ReceiveMOS(NetworkStream networkStream)
        {
            if (networkStream.CanRead)
            {
                byte[] myReadBuffer = new byte[1024];
                StringBuilder myCompleteMessage = new StringBuilder();
                int numberOfBytesRead = 0;

                do
                {
                    try
                    {
                        numberOfBytesRead = networkStream.Read(myReadBuffer, 0, myReadBuffer.Length);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    
                    myCompleteMessage.AppendFormat("{0}", Encoding.BigEndianUnicode.GetString(myReadBuffer, 0, numberOfBytesRead));

                    Thread.Sleep(100);
                }
                while (networkStream.DataAvailable);

                if (!string.IsNullOrEmpty(myCompleteMessage.ToString()))
                {
                    return Common.Helper.DeserializeFromXml<MOS>(myCompleteMessage.ToString().Replace("&", "&amp;"));
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public MOS ReceiveMOS(IAsyncResult ar,byte[] buffer)
        {
            NetworkStream networkStream = (NetworkStream)ar.AsyncState;

            StringBuilder myCompleteMessage = new StringBuilder();
            int numberOfBytesRead = 0;
            try
            {
                numberOfBytesRead=networkStream.EndRead(ar);
            }
            catch { }
            if (numberOfBytesRead > 0)
            {
                do
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(myCompleteMessage.ToString()))
                        {
                            numberOfBytesRead = networkStream.Read(buffer, 0, buffer.Length);
                        }
                    }
                    catch (Exception)
                    {
                        return null;
                    }

                    myCompleteMessage.AppendFormat("{0}", Encoding.BigEndianUnicode.GetString(buffer, 0, numberOfBytesRead));

                    Thread.Sleep(100);
                }
                while (networkStream.DataAvailable);

                if (!string.IsNullOrEmpty(myCompleteMessage.ToString()))
                {
                    return Common.Helper.DeserializeFromXml<MOS>(myCompleteMessage.ToString().Replace("&", "&amp;"));
                }
                else
                {
                    return null;
                }
            }
            return null;

        }

        public void SendMOS(NetworkStream networkStream, MOS command, out string message)
        {
            message = string.Empty;
            IList<string> errors= Validator.Validate(command);

            if (errors.Count.Equals(0))
            {
                try
                {
                    if (command != null
                        && command.messageID > 0)
                    {
                        string str = Helper.GetStringForMOSCommand(command);
                        Common.Helper.SendData(networkStream, str,System.Text.Encoding.BigEndianUnicode);
                    }
                    else
                    {
                        message = "Error: Invalid Command.";
                    }
                }
                catch (Exception e)
                {
                    message = e.Message;
                }
            }
            else
            {
                message += string.Join("\n", errors);
            }

        }
    }
}
