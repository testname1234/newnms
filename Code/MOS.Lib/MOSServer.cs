﻿using MOSProtocol.Lib.Entities;
using System;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MOSProtocol.Lib
{
    public delegate MOS OnCommandReceived(MOS MOSCommand);

    public class MOSServer : MOSProtocol
    {
        private string _id = string.Empty;

        public MOSServer(string MOSId)
        {
            _id = MOSId;
        }

        #region Event Signatures

        public event OnCommandReceived HeartBeatReceived;
        public event OnCommandReceived RunOrderReceived;
        public event OnCommandReceived StoryReceived;
        public event OnCommandReceived StoryInfoReceived;
        public event OnCommandReceived StorySwapReceived;
        public event OnCommandReceived StorySkipReceived;
        public event OnCommandReceived StoryUnSkipReceived;
        public event OnCommandReceived StoryMoveReceived;
        public event OnCommandReceived NotificationReceived;

        #endregion

        private TcpListener server = null;
        private TcpClient client = null;
        private bool listening = false;

        public bool IsListening
        {
            get
            {
                return listening;
            }
        }

        public void StartListening(int port)
        {
            try
            {
                IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                server = new TcpListener(ipAddress, port);
                server.Start();

                SetupAcceptClients(server);
            }
            catch 
            {

            }
        }

        public void SetupAcceptClients(TcpListener _server)
        {
            try
            {
                _server.BeginAcceptTcpClient(new AsyncCallback(AcceptClient), _server);
            }
            catch
            {
            }
        }

        public void SetupReadData(NetworkStream _stream)
        {
            try
            {
                if (_stream.CanRead)
                {
                    _stream.BeginRead(buffer, 0, buffer.Length, new AsyncCallback(Receive_Callback), _stream);
                }
            }
            catch
            {
            }
        }

        public void StopListening()
        {
            server.Stop();
            listening = false;
        }

        private byte[] buffer = new byte[1024];

        private void AcceptClient(IAsyncResult ar)
        {
            TcpListener _server = (TcpListener)ar.AsyncState;
            TcpClient client = _server.EndAcceptTcpClient(ar);
            NetworkStream _stream = client.GetStream();

            SetupReadData(_stream);

            SetupAcceptClients(_server);            
        }

        public void Receive_Callback(IAsyncResult ar)
        {
            NetworkStream stream = (NetworkStream)ar.AsyncState;
            
            MOS command = ReceiveMOS(ar,buffer);

            if (command != null)
            {

                MOS response = new MOS();
                response.mosID = command.mosID;
                response.ncsID = command.ncsID;
                response.messageID = command.messageID;

                if (command != null
                    && (command.mosID.Equals(_id)
                    || command.ncsID.Equals(_id)
                    ))
                {
                    response = MOSCommandReceived(command);
                }
                else
                {
                    ROAck roAck = new ROAck();
                    roAck.roStatus = "NACK";
                    response.command = roAck;
                }
                string message = string.Empty;
                SendMOS(stream, response, out message);

                SetupReadData(stream);
            };
        }

        public void Send_Callback(IAsyncResult ar)
        {
            NetworkStream networkStream = (NetworkStream)ar.AsyncState;
        }

        private void WaitForConnection(object _server)
        {
            TcpListener server = (TcpListener)_server;
            string data = string.Empty;


            server.BeginAcceptSocket(new AsyncCallback(AcceptClient), data);

            // Perform a blocking call to accept requests. 
            // You could also user server.AcceptSocket() here.
            client = server.AcceptTcpClient();

            while (listening)
            {
                try
                {
                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    //OnDataReceived( stream);
                }
                catch
                {
                    //StopListening();
                }
            }
        }

        private void OnDataReceived(NetworkStream stream,IAsyncResult ar)
        {

        }

        public void SendMOS(MOS request,out string message)
        {
            message = string.Empty;
            SendMOS(client.GetStream(), request, out message);
        }

        private MOS MOSCommandReceived(MOS mosCommand)
        {
            MOS mosResponse = new MOS();

            try
            {

                //HeartBeatReceived - HeartBeat
                if (mosCommand.command.GetType().Equals(typeof(HeartBeat)))
                {
                    if (HeartBeatReceived != null)
                    {
                        mosResponse = HeartBeatReceived.Invoke(mosCommand);
                    }
                    else
                    {
                        mosResponse = GetHeartBeat(mosCommand);
                    }
                }
                else if (mosCommand.command.GetType().Equals(typeof(ROCreate)))
                {
                    if (RunOrderReceived != null)
                    {
                        mosResponse = RunOrderReceived.Invoke(mosCommand);
                    }
                }
                else if (mosCommand.command.GetType().Equals(typeof(ROStoryInsert)))
                {
                    if (StoryReceived != null)
                    {
                        mosResponse = StoryUnSkipReceived.Invoke(mosCommand);
                    }
                }
                else if (mosCommand.command.GetType().Equals(typeof(ROStorySend)))
                {
                    if (StoryInfoReceived != null)
                    {
                        mosResponse = StoryInfoReceived.Invoke(mosCommand);
                    }
                }
                else if (mosCommand.command.GetType().Equals(typeof(ROStorySwap)))
                {
                    if (StorySwapReceived != null)
                    {
                        mosResponse = StorySwapReceived.Invoke(mosCommand);
                    }
                }
                else if (mosCommand.command.GetType().Equals(typeof(ROStoryDelete)))
                {
                    if (StorySkipReceived != null)
                    {
                        mosResponse = StorySkipReceived.Invoke(mosCommand);
                    }
                }
                else if (mosCommand.command.GetType().Equals(typeof(ROStoryMove)))
                {
                    if (StoryMoveReceived != null)
                    {
                        mosResponse = StoryMoveReceived.Invoke(mosCommand);
                    }
                }
                else if (mosCommand.command.GetType().Equals(typeof(Notification)))
                {
                    if (NotificationReceived != null)
                    {
                        mosResponse = NotificationReceived.Invoke(mosCommand);
                    }
                }

            }
            catch (Exception ex)
            {
               // throw ex;
            }

            return mosResponse;
        }

        private MOS GetHeartBeat(MOS NCSHeartBeat)
        {
            HeartBeat heartbeat = new HeartBeat();
            heartbeat.time = DateTime.UtcNow;
            NCSHeartBeat.command = heartbeat;
            return NCSHeartBeat;

        }

    }
}
