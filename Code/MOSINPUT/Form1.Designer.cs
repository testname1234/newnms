﻿namespace MOSINPUT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNCID = new System.Windows.Forms.TextBox();
            this.txtMosDevicePort = new System.Windows.Forms.TextBox();
            this.txtMosDeviceIP = new System.Windows.Forms.TextBox();
            this.txtMosDeviceId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIsTelepro = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCasperDevicePort = new System.Windows.Forms.TextBox();
            this.txtCasperDeviceIp = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lbleRoResuld = new System.Windows.Forms.Label();
            this.lblStoriesUpdated = new System.Windows.Forms.Label();
            this.lbleStoriesNew = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtNCID
            // 
            this.txtNCID.Location = new System.Drawing.Point(89, 124);
            this.txtNCID.Name = "txtNCID";
            this.txtNCID.Size = new System.Drawing.Size(169, 20);
            this.txtNCID.TabIndex = 17;
            this.txtNCID.Text = "windowsservice";
            // 
            // txtMosDevicePort
            // 
            this.txtMosDevicePort.Location = new System.Drawing.Point(90, 96);
            this.txtMosDevicePort.Name = "txtMosDevicePort";
            this.txtMosDevicePort.Size = new System.Drawing.Size(169, 20);
            this.txtMosDevicePort.TabIndex = 16;
            this.txtMosDevicePort.Text = "7250";
            // 
            // txtMosDeviceIP
            // 
            this.txtMosDeviceIP.Location = new System.Drawing.Point(89, 39);
            this.txtMosDeviceIP.Name = "txtMosDeviceIP";
            this.txtMosDeviceIP.Size = new System.Drawing.Size(169, 20);
            this.txtMosDeviceIP.TabIndex = 15;
            this.txtMosDeviceIP.Text = "10.1.20.26";
            // 
            // txtMosDeviceId
            // 
            this.txtMosDeviceId.Location = new System.Drawing.Point(91, 13);
            this.txtMosDeviceId.Name = "txtMosDeviceId";
            this.txtMosDeviceId.Size = new System.Drawing.Size(167, 20);
            this.txtMosDeviceId.TabIndex = 14;
            this.txtMosDeviceId.Text = "pcr";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "NCID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "MosDevicePort";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "MosDeviceIP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "MosDeviceId";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(183, 239);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "RunDown";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(90, 158);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(167, 21);
            this.comboBox1.TabIndex = 18;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "EpisodeId";
            // 
            // txtIsTelepro
            // 
            this.txtIsTelepro.AutoSize = true;
            this.txtIsTelepro.Checked = true;
            this.txtIsTelepro.CheckState = System.Windows.Forms.CheckState.Checked;
            this.txtIsTelepro.Location = new System.Drawing.Point(63, 216);
            this.txtIsTelepro.Name = "txtIsTelepro";
            this.txtIsTelepro.Size = new System.Drawing.Size(88, 17);
            this.txtIsTelepro.TabIndex = 20;
            this.txtIsTelepro.Text = "Teleprompter";
            this.txtIsTelepro.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(194, 216);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(71, 17);
            this.checkBox2.TabIndex = 21;
            this.checkBox2.Text = "SendMos";
            this.checkBox2.UseVisualStyleBackColor = false;
            this.checkBox2.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 189);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "CasperDevicePort";
            // 
            // txtCasperDevicePort
            // 
            this.txtCasperDevicePort.Location = new System.Drawing.Point(149, 186);
            this.txtCasperDevicePort.Name = "txtCasperDevicePort";
            this.txtCasperDevicePort.Size = new System.Drawing.Size(108, 20);
            this.txtCasperDevicePort.TabIndex = 24;
            this.txtCasperDevicePort.Text = "10541";
            // 
            // txtCasperDeviceIp
            // 
            this.txtCasperDeviceIp.Location = new System.Drawing.Point(92, 68);
            this.txtCasperDeviceIp.Name = "txtCasperDeviceIp";
            this.txtCasperDeviceIp.Size = new System.Drawing.Size(169, 20);
            this.txtCasperDeviceIp.TabIndex = 26;
            this.txtCasperDeviceIp.Text = "10.1.20.26";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "CasperDeviceIp";
            // 
            // lbleRoResuld
            // 
            this.lbleRoResuld.AutoSize = true;
            this.lbleRoResuld.Location = new System.Drawing.Point(8, 266);
            this.lbleRoResuld.Name = "lbleRoResuld";
            this.lbleRoResuld.Size = new System.Drawing.Size(0, 13);
            this.lbleRoResuld.TabIndex = 27;
            // 
            // lblStoriesUpdated
            // 
            this.lblStoriesUpdated.AutoSize = true;
            this.lblStoriesUpdated.Location = new System.Drawing.Point(8, 307);
            this.lblStoriesUpdated.Name = "lblStoriesUpdated";
            this.lblStoriesUpdated.Size = new System.Drawing.Size(0, 13);
            this.lblStoriesUpdated.TabIndex = 28;
            // 
            // lbleStoriesNew
            // 
            this.lbleStoriesNew.AutoSize = true;
            this.lbleStoriesNew.Location = new System.Drawing.Point(8, 350);
            this.lbleStoriesNew.Name = "lbleStoriesNew";
            this.lbleStoriesNew.Size = new System.Drawing.Size(0, 13);
            this.lbleStoriesNew.TabIndex = 29;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(318, 356);
            this.Controls.Add(this.lbleStoriesNew);
            this.Controls.Add(this.lblStoriesUpdated);
            this.Controls.Add(this.lbleRoResuld);
            this.Controls.Add(this.txtCasperDeviceIp);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCasperDevicePort);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.txtIsTelepro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.txtNCID);
            this.Controls.Add(this.txtMosDevicePort);
            this.Controls.Add(this.txtMosDeviceIP);
            this.Controls.Add(this.txtMosDeviceId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNCID;
        private System.Windows.Forms.TextBox txtMosDevicePort;
        private System.Windows.Forms.TextBox txtMosDeviceIP;
        private System.Windows.Forms.TextBox txtMosDeviceId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox txtIsTelepro;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCasperDevicePort;
        private System.Windows.Forms.TextBox txtCasperDeviceIp;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label lbleRoResuld;
        private System.Windows.Forms.Label lblStoriesUpdated;
        private System.Windows.Forms.Label lbleStoriesNew;
    }
}

