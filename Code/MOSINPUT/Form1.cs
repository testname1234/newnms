﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using CasparCGLib.Entities;
using HtmlAgilityPack;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Service;
using System.Web;
using System.Windows.Forms;
using NMS.Core;
using NMS.ProcessThreads;


namespace MOSINPUT
{
    public partial class Form1 : Form
    {
        public Form1()
         {
            InitializeComponent();
            try
            {
                IMosActiveEpisodeService mosActiveEpisodeService = ControlPanel.Core.IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
                List<MosActiveEpisode> MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByStatus(null);
                for (int i = 0; i < MosActiveEpisodes.Count; i++)
                {
                    comboBox1.Items.Add(MosActiveEpisodes[i].EpisodeId + " " + "(" + " " + MosActiveEpisodes[i].CreationDate + ")");
                }
            }
            catch (Exception exp)
            {
                logException(exp);
            }

            //
            //videowallid
            //
        }

        public void logException(Exception exp)
        {
            //File.AppendAllLines("c:\\log.txt", new string[] { exp.Message, exp.StackTrace });
            if (exp.InnerException != null) logException(exp.InnerException);
        }

        public int episodeId;
        public int statusCode;

        private void button1_Click(object sender, EventArgs e)
        {
            MOS objform = null;

            MosGenerationOptimized rG = new MosGenerationOptimized();
            //if (checkBox1.Checked && checkBox2.Checked)
            //{  
            //    objform= rG.SendCasperRundown(episodeId, statusCode);
            //    rG.SendMos(episodeId, statusCode);
            //    if (objform != null)
            //    {
            //        ShowResult(objform);
            //    }

            //}

            //else 
            // if (checkBox1.Checked)
            // {
            try
            {
                rG.ProcessMosByEpisode(episodeId, txtMosDeviceId.Text, txtMosDeviceIP.Text, txtCasperDevicePort.Text, txtCasperDevicePort.Text, txtNCID.Text, txtIsTelepro.Checked);
                lbleRoResuld.Text = "Method Executed";
            }
            catch (Exception ex)
            {
                lbleRoResuld.Text = ex.Message + " Stack Trace =" + ex.StackTrace;
                logException(ex);
            }

            //if (objform != null)
            //{
            //    ShowResult(objform);
            //}

            //else
            //{
            //    lbleRoResuld.Text = "Nothing Received From MosClient";
            //}

            //}
            ////else if (checkBox2.Checked)
            ////{
            ////    rG.SendMos(episodeId, statusCode);
            ////}
            //else
            //{
            //    // Do Nothing
            //}

        }

        public void ShowResult(MOS objform)
        {
            ROAck ro = new ROAck();
            ro.CopyFrom(objform.command);
            lbleRoResuld.Text = Convert.ToString("MosID =" + objform.mosID + ", MessageId =" + objform.messageID + ", roStatus =" + ro.roStatus + ", roId =" + ro.roID);

            if (ro.roAckStories != null && ro.roAckStories.Count > 0)
            {
                int SupdateCount = 0;
                int SNewCount = 0;

                foreach (var r in ro.roAckStories)
                {
                    if (r.storyStatus == MOSProtocol.Lib.Enums.StoryStatus.UPDATED)
                    {
                        SupdateCount++;
                    }
                    if (r.storyStatus == MOSProtocol.Lib.Enums.StoryStatus.NEW)
                    {
                        SNewCount = +1;
                    }
                }

                if (SupdateCount > 0)
                {
                    lblStoriesUpdated.Text = "Count of Updated Stories are " + SupdateCount;
                }

                if (SNewCount > 0)
                {
                    lbleStoriesNew.Text = "Count of New Stories are " + SNewCount;
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String epId = comboBox1.Text;
            epId = epId.Substring(0, epId.IndexOf("("));
            episodeId = Convert.ToInt32(epId);
            statusCode = 1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }


}
