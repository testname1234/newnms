﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MS.Core.DataInterfaces;
using MS.Core.Entities;

namespace MS.Core
{
    public class CacheStorage
    {
        private List<Server> _servers;
        public List<Server> Servers
        {
            get
            {
                if (_servers == null)
                {
                    UpdateServers();
                }
                return _servers;
            }
            set
            {
                _servers = value;
            }
        }
        private List<Format> _formats;
        public List<Format> Formats
        {
            get
            {
                if (_formats == null)
                {
                    UpdateFormats();
                }
                return _formats;
            }
            set
            {
                _formats = value;
            }
        }
        private List<Bucket> _buckets;
        public List<Bucket> Buckets
        {
            get
            {
                if (_buckets == null)
                {
                    UpdateBuckets();
                }
                return _buckets;
            }
            set
            {
                _buckets = value;
            }
        }

        public void UpdateServers()
        {
            IServerRepository serverRepository = IoC.Resolve<IServerRepository>("ServerRepository");
            Servers = serverRepository.GetAllServer();
        }

        public void UpdateFormats()
        {
            IFormatRepository formatRepository = IoC.Resolve<IFormatRepository>("FormatRepository");
            Formats = formatRepository.GetAllFormat();
        }

        public void UpdateBuckets()
        {
            IBucketRepository bucketRepository = IoC.Resolve<IBucketRepository>("BucketRepository");
            Buckets = bucketRepository.GetAllBucket();
        }
    }
}
