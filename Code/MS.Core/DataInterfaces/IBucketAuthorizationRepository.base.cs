﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IBucketAuthorizationRepositoryBase
	{
        
        Dictionary<string, string> GetBucketAuthorizationBasicSearchColumns();
        List<SearchColumn> GetBucketAuthorizationSearchColumns();
        List<SearchColumn> GetBucketAuthorizationAdvanceSearchColumns();
        

		BucketAuthorization GetBucketAuthorization(System.Int32 BucketAuthorizationId,string SelectClause=null);
		BucketAuthorization UpdateBucketAuthorization(BucketAuthorization entity);
		bool DeleteBucketAuthorization(System.Int32 BucketAuthorizationId);
		BucketAuthorization DeleteBucketAuthorization(BucketAuthorization entity);
		List<BucketAuthorization> GetPagedBucketAuthorization(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<BucketAuthorization> GetAllBucketAuthorization(string SelectClause=null);
		BucketAuthorization InsertBucketAuthorization(BucketAuthorization entity);
		List<BucketAuthorization> GetBucketAuthorizationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
