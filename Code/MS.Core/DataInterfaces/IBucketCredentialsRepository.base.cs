﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IBucketCredentialsRepositoryBase
	{
        
        Dictionary<string, string> GetBucketCredentialsBasicSearchColumns();
        List<SearchColumn> GetBucketCredentialsSearchColumns();
        List<SearchColumn> GetBucketCredentialsAdvanceSearchColumns();
        

		List<BucketCredentials> GetBucketCredentialsByBucketId(System.Int32 BucketId,string SelectClause=null);
		BucketCredentials GetBucketCredentials(System.Int32 BucketCredentialId,string SelectClause=null);
		BucketCredentials UpdateBucketCredentials(BucketCredentials entity);
		bool DeleteBucketCredentials(System.Int32 BucketCredentialId);
		BucketCredentials DeleteBucketCredentials(BucketCredentials entity);
		List<BucketCredentials> GetPagedBucketCredentials(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<BucketCredentials> GetAllBucketCredentials(string SelectClause=null);
		BucketCredentials InsertBucketCredentials(BucketCredentials entity);
		List<BucketCredentials> GetBucketCredentialsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
