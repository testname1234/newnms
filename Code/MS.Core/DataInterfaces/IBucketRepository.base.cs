﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IBucketRepositoryBase
	{
        
        Dictionary<string, string> GetBucketBasicSearchColumns();
        List<SearchColumn> GetBucketSearchColumns();
        List<SearchColumn> GetBucketAdvanceSearchColumns();
        

		Bucket GetBucket(System.Int32 BucketId,string SelectClause=null);
		Bucket UpdateBucket(Bucket entity);
		bool DeleteBucket(System.Int32 BucketId);
		Bucket DeleteBucket(Bucket entity);
		List<Bucket> GetPagedBucket(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Bucket> GetAllBucket(string SelectClause=null);
		Bucket InsertBucket(Bucket entity);
		List<Bucket> GetBucketByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);
    }	
	
}
