﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IBucketRepository: IBucketRepositoryBase
	{
        Bucket GetBucketByApiKey(int bucketId, string apiKey);

        List<Bucket> GetBucketByParentId(int parentId);

        Bucket GetBucketByName(string p);

        Bucket GetBucketByPath(string p);

        Bucket GetSubBucket(string name, int parentId);

        Bucket GetBucketAndCredentials(int bucketId);

        void MarkResourceIfBucketPrivate(int? nullable, int p);

        List<Bucket> GetAllBucketWithParentName(int userId);

        List<Models.ResourceModel> GetSubBucketResources(int bucketId);

        List<Bucket> GetBucketsToTranscode();

        List<Bucket> GetBucketsByParentId(int parentId);

    }
	
	
}
