﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface ICodecConfigRepositoryBase
	{
        
        Dictionary<string, string> GetCodecConfigBasicSearchColumns();
        List<SearchColumn> GetCodecConfigSearchColumns();
        List<SearchColumn> GetCodecConfigAdvanceSearchColumns();
        

		CodecConfig GetCodecConfig(System.Int32 CodecConfigId,string SelectClause=null);
		CodecConfig UpdateCodecConfig(CodecConfig entity);
		bool DeleteCodecConfig(System.Int32 CodecConfigId);
		CodecConfig DeleteCodecConfig(CodecConfig entity);
		List<CodecConfig> GetPagedCodecConfig(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CodecConfig> GetAllCodecConfig(string SelectClause=null);
		CodecConfig InsertCodecConfig(CodecConfig entity);
		List<CodecConfig> GetCodecConfigByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
