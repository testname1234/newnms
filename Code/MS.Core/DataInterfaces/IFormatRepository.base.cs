﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IFormatRepositoryBase
	{
        
        Dictionary<string, string> GetFormatBasicSearchColumns();
        List<SearchColumn> GetFormatSearchColumns();
        List<SearchColumn> GetFormatAdvanceSearchColumns();
        

		Format GetFormat(System.Int32 FormatId,string SelectClause=null);
		Format UpdateFormat(Format entity);
		bool DeleteFormat(System.Int32 FormatId);
		Format DeleteFormat(Format entity);
		List<Format> GetPagedFormat(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Format> GetAllFormat(string SelectClause=null);
		Format InsertFormat(Format entity);
		List<Format> GetFormatByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
