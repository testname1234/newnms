﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceDetailRepositoryBase
	{
        
        Dictionary<string, string> GetResourceDetailBasicSearchColumns();
        List<SearchColumn> GetResourceDetailSearchColumns();
        List<SearchColumn> GetResourceDetailAdvanceSearchColumns();
        

		ResourceDetail GetResourceDetail(System.Int32 ResourceDetailId,string SelectClause=null);
		ResourceDetail UpdateResourceDetail(ResourceDetail entity);
		bool DeleteResourceDetail(System.Int32 ResourceDetailId);
		ResourceDetail DeleteResourceDetail(ResourceDetail entity);
		List<ResourceDetail> GetPagedResourceDetail(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ResourceDetail> GetAllResourceDetail(string SelectClause=null);
		ResourceDetail InsertResourceDetail(ResourceDetail entity);
		List<ResourceDetail> GetResourceDetailByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
