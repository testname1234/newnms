﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceDetailRepository: IResourceDetailRepositoryBase
	{

        ResourceDetail GetResourceDetailbyResourceIdCustom(int p);

        ResourceDetail GetResourceDetailByGuid(string Guid, string SelectClause = null);
    }
	
	
}
