﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceLogRepositoryBase
	{
        
        Dictionary<string, string> GetResourceLogBasicSearchColumns();
        List<SearchColumn> GetResourceLogSearchColumns();
        List<SearchColumn> GetResourceLogAdvanceSearchColumns();
        

		ResourceLog GetResourceLog(System.Int32 ResourceLogId,string SelectClause=null);
		ResourceLog UpdateResourceLog(ResourceLog entity);
		bool DeleteResourceLog(System.Int32 ResourceLogId);
		ResourceLog DeleteResourceLog(ResourceLog entity);
		List<ResourceLog> GetPagedResourceLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ResourceLog> GetAllResourceLog(string SelectClause=null);
		ResourceLog InsertResourceLog(ResourceLog entity);
		List<ResourceLog> GetResourceLogByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
