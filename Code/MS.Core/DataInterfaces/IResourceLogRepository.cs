﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceLogRepository: IResourceLogRepositoryBase
	{

        List<Models.UserLog> getAllUSerLog();

        List<ResourceLog> GetResourceLogbyUserId(int p);
    }
	
	
}
