﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceMetaRepositoryBase
	{
        
        Dictionary<string, string> GetResourceMetaBasicSearchColumns();
        List<SearchColumn> GetResourceMetaSearchColumns();
        List<SearchColumn> GetResourceMetaAdvanceSearchColumns();
        

		ResourceMeta GetResourceMeta(System.Int32 ResourceMetaId,string SelectClause=null);
		ResourceMeta UpdateResourceMeta(ResourceMeta entity);
		bool DeleteResourceMeta(System.Int32 ResourceMetaId);
		ResourceMeta DeleteResourceMeta(ResourceMeta entity);
		List<ResourceMeta> GetPagedResourceMeta(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ResourceMeta> GetAllResourceMeta(string SelectClause=null);
		ResourceMeta InsertResourceMeta(ResourceMeta entity);
		List<ResourceMeta> GetResourceMetaByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
