﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceMetaRepository: IResourceMetaRepositoryBase
	{
        ResourceMeta GetResourceMetaByResource(ResourceMeta resourceMeta, string SelectClause = null);

        ResourceMeta GetMetaByGuidKeyAndValue(ResourceMeta resourceMeta, string SelectClause = null);

        List<ResourceMeta> GetResourceMetaByGuid(string Guid);

        List<ResourceMeta> GetResourceMetaforThread();

        List<ResourceMeta> GetResourceMetaforThreadAfterDate(DateTime lastUpdateDate);
    }
	
	
}
