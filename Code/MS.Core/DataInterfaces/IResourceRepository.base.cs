﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceRepositoryBase
	{
        
        Dictionary<string, string> GetResourceBasicSearchColumns();
        List<SearchColumn> GetResourceSearchColumns();
        List<SearchColumn> GetResourceAdvanceSearchColumns();
        

		List<Resource> GetResourceByResourceTypeId(System.Int32 ResourceTypeId,string SelectClause=null);
		List<Resource> GetResourceByHighResolutionFormatId(System.Int32? HighResolutionFormatId,string SelectClause=null);
		List<Resource> GetResourceByLowResolutionFormatId(System.Int32? LowResolutionFormatId,string SelectClause=null);
		List<Resource> GetResourceByResourceStatusId(System.Int32 ResourceStatusId,string SelectClause=null);
		List<Resource> GetResourceByServerId(System.Int32? ServerId,string SelectClause=null);
		Resource GetResource(System.Int32 ResourceId,string SelectClause=null);        
		Resource UpdateResource(Resource entity);
		bool DeleteResource(System.Int32 ResourceId);
		Resource DeleteResource(Resource entity);
		List<Resource> GetPagedResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Resource> GetAllResource(string SelectClause=null);
		Resource InsertResource(Resource entity);
		List<Resource> GetResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
