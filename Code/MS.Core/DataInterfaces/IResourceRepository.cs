﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceRepository: IResourceRepositoryBase
	{
        Resource GetResourceByGuidId(Guid guid);

        bool DeleteResourceByGuid(Guid guid);

        Resource GetResourceBySource(string source);

        Resource GetResourceBySourceAndBucketId(string source, int bucketId);

        Resource GetResourceByFilePathAndBucketId(string filePath, int bucketId);

        Resource UpdateResourceInfo(Resource entity);

        List<Resource> GetResourceByMeta(string Metastring);

        List<Resource> GetResourceByCaption(string Caption);

        Resource UpdateResourceDuration(Resource entity);

        List<Resource> GetResourceforDuration(DateTime resourceDate);

        Resource GetResourceByFilePath(string FilePath, int bucketId);

        List<Resource> GetResourceByBucketId(int bucketId);

        List<Resource> GetAllResourcesByFilePath(string path);

        List<Resource> GetAllResourcesByFilePath(string FilePath, int BucketId);

        string GetResourcePathByGuid(Guid guid);

        List<Resource> GetResourceByGuids(List<string> resourceGuids);

        List<Resource> GetAllResourceWithDetailAndMetas();

        List<Resource> GetResourcesAfterThisDate(long dateStamp);

        Resource GetResourceByBarcode(string barcode);

        List<Resource> GetResourceByStatusId(int StatusId);

        List<Resource> GetResourceByStatusId(int StatusId, int count);

        List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId);

        List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId, int Count);

        List<Resource> GetTop100Resources();

        Resource GetResourceByBarcodeAndResourceType(string barcode, int resourceTypeId);

        List<string> GetAllCelebrityNames();
        List<string> GetAllCelebrityNames(DateTime lastExecutionDate);
        List<Resource> GetResourceByGuidIds(List<string> list);
        List<Resource> GetResourceByBarcodeRange(int start, int end);
        
        Resource GetResourceByGuidIdforUserFavourite(Guid guid);

        List<Resource> GetResourcesForLowResTranscoding(int count);

        Resource UpdateResourceWithTimeStamp(Resource entity);

        bool UpdateResourceServerName(string ServerName);

        List<Resource> GetResourceIdFilePathandSubBucketIdByParent(int bucketId);

        List<Resource> GetByDate(DateTime CreationDate);

        bool UpdateResourceStatus(int resourceId, int statusId);

        List<Resource> GetResourceByStatusIdAfterDate(long dateStamp, int statusid);

        List<Resource> GetdeletedResources();
    }
	
	
}
