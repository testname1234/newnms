﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceStatusRepositoryBase
	{
        
        Dictionary<string, string> GetResourceStatusBasicSearchColumns();
        List<SearchColumn> GetResourceStatusSearchColumns();
        List<SearchColumn> GetResourceStatusAdvanceSearchColumns();
        

		ResourceStatus GetResourceStatus(System.Int32 ResourceStatusId,string SelectClause=null);
		ResourceStatus UpdateResourceStatus(ResourceStatus entity);
		bool DeleteResourceStatus(System.Int32 ResourceStatusId);
		ResourceStatus DeleteResourceStatus(ResourceStatus entity);
		List<ResourceStatus> GetPagedResourceStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ResourceStatus> GetAllResourceStatus(string SelectClause=null);
		ResourceStatus InsertResourceStatus(ResourceStatus entity);
		List<ResourceStatus> GetResourceStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
