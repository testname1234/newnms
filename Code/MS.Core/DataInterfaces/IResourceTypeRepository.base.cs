﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IResourceTypeRepositoryBase
	{
        
        Dictionary<string, string> GetResourceTypeBasicSearchColumns();
        List<SearchColumn> GetResourceTypeSearchColumns();
        List<SearchColumn> GetResourceTypeAdvanceSearchColumns();
        

		ResourceType GetResourceType(System.Int32 ResourceTypeId,string SelectClause=null);
		ResourceType UpdateResourceType(ResourceType entity);
		bool DeleteResourceType(System.Int32 ResourceTypeId);
		ResourceType DeleteResourceType(ResourceType entity);
		List<ResourceType> GetPagedResourceType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ResourceType> GetAllResourceType(string SelectClause=null);
		ResourceType InsertResourceType(ResourceType entity);
		List<ResourceType> GetResourceTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
