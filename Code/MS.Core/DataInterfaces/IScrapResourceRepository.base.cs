﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IScrapResourceRepositoryBase
	{
        
        Dictionary<string, string> GetScrapResourceBasicSearchColumns();
        List<SearchColumn> GetScrapResourceSearchColumns();
        List<SearchColumn> GetScrapResourceAdvanceSearchColumns();
        

		ScrapResource GetScrapResource(System.Int32 ScrapResourceId,string SelectClause=null);
		ScrapResource UpdateScrapResource(ScrapResource entity);
		bool DeleteScrapResource(System.Int32 ScrapResourceId);
		ScrapResource DeleteScrapResource(ScrapResource entity);
		List<ScrapResource> GetPagedScrapResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ScrapResource> GetAllScrapResource(string SelectClause=null);
		ScrapResource InsertScrapResource(ScrapResource entity);
		List<ScrapResource> GetScrapResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
