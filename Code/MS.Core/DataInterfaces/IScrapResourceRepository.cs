﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.Enums;

namespace MS.Core.DataInterfaces
{
		
	public interface IScrapResourceRepository: IScrapResourceRepositoryBase
	{
        List<ScrapResource> GetScrapResourceByStatusId(ScrapResourceStatuses? status);
        List<ScrapResource> GetScrapResourceByStatusIdAndSystemTypeId(ScrapResourceStatuses? status, int? SystemTypeId);
        void UpdateScrapResourceNoReturn(ScrapResource entity);
        List<ScrapResource> GetDownloadedResources();
        List<ScrapResource> GetScrapResourceByDate(DateTime CreationDate);
	}
	
	
}
