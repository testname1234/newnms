﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface ISearchHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetSearchHistoryBasicSearchColumns();
        List<SearchColumn> GetSearchHistorySearchColumns();
        List<SearchColumn> GetSearchHistoryAdvanceSearchColumns();
        

		SearchHistory GetSearchHistory(System.Int32 SearchHistoryId,string SelectClause=null);
		SearchHistory UpdateSearchHistory(SearchHistory entity);
		bool DeleteSearchHistory(System.Int32 SearchHistoryId);
		SearchHistory DeleteSearchHistory(SearchHistory entity);
		List<SearchHistory> GetPagedSearchHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SearchHistory> GetAllSearchHistory(string SelectClause=null);
		SearchHistory InsertSearchHistory(SearchHistory entity);
		List<SearchHistory> GetSearchHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
