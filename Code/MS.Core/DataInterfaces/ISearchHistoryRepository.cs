﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface ISearchHistoryRepository: ISearchHistoryRepositoryBase
	{

        bool keywordExists(string term);

        List<string> GetAllSearchHistoryKeywords();

        List<string> GetAllSearchHistoryKeywords(DateTime lastUpdateDate);
    }
	
	
}
