﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IServerRepositoryBase
	{
        
        Dictionary<string, string> GetServerBasicSearchColumns();
        List<SearchColumn> GetServerSearchColumns();
        List<SearchColumn> GetServerAdvanceSearchColumns();
        

		Server GetServer(System.Int32 ServerId,string SelectClause=null);
		Server UpdateServer(Server entity);
		bool DeleteServer(System.Int32 ServerId);
		Server DeleteServer(Server entity);
		List<Server> GetPagedServer(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Server> GetAllServer(string SelectClause=null);
		Server InsertServer(Server entity);
		List<Server> GetServerByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
