﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface ISlugBucketRepositoryBase
	{
        
        Dictionary<string, string> GetSlugBucketBasicSearchColumns();
        List<SearchColumn> GetSlugBucketSearchColumns();
        List<SearchColumn> GetSlugBucketAdvanceSearchColumns();
        

		SlugBucket GetSlugBucket(System.Int32 SlugBucketId,string SelectClause=null);
		SlugBucket UpdateSlugBucket(SlugBucket entity);
		bool DeleteSlugBucket(System.Int32 SlugBucketId);
		SlugBucket DeleteSlugBucket(SlugBucket entity);
		List<SlugBucket> GetPagedSlugBucket(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlugBucket> GetAllSlugBucket(string SelectClause=null);
		SlugBucket InsertSlugBucket(SlugBucket entity);
		List<SlugBucket> GetSlugBucketByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
