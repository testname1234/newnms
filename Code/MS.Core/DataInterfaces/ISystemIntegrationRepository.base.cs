﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface ISystemIntegrationRepositoryBase
	{
        
        Dictionary<string, string> GetSystemIntegrationBasicSearchColumns();
        List<SearchColumn> GetSystemIntegrationSearchColumns();
        List<SearchColumn> GetSystemIntegrationAdvanceSearchColumns();
        

		SystemIntegration GetSystemIntegration(System.Int32 Id,string SelectClause=null);
		SystemIntegration UpdateSystemIntegration(SystemIntegration entity);
		bool DeleteSystemIntegration(System.Int32 Id);
		SystemIntegration DeleteSystemIntegration(SystemIntegration entity);
		List<SystemIntegration> GetPagedSystemIntegration(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SystemIntegration> GetAllSystemIntegration(string SelectClause=null);
		SystemIntegration InsertSystemIntegration(SystemIntegration entity);
		List<SystemIntegration> GetSystemIntegrationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
