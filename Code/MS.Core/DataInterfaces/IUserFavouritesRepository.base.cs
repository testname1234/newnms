﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IUserFavouritesRepositoryBase
	{
        
        Dictionary<string, string> GetUserFavouritesBasicSearchColumns();
        List<SearchColumn> GetUserFavouritesSearchColumns();
        List<SearchColumn> GetUserFavouritesAdvanceSearchColumns();
        

		UserFavourites GetUserFavourites(System.Int32 UserFavouritesId,string SelectClause=null);
		UserFavourites UpdateUserFavourites(UserFavourites entity);
		bool DeleteUserFavourites(System.Int32 UserFavouritesId);
		UserFavourites DeleteUserFavourites(UserFavourites entity);
		List<UserFavourites> GetPagedUserFavourites(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<UserFavourites> GetAllUserFavourites(string SelectClause=null);
		UserFavourites InsertUserFavourites(UserFavourites entity);
		List<UserFavourites> GetUserFavouritesByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
