﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IUserFavouritesRepository: IUserFavouritesRepositoryBase
	{

        List<UserFavourites> GetUserFavouritesByUserId(int UserId);
        bool DeleteUserFavouritesbYUserIdandGuid(int UserId, string guid);
    }
	
	
}
