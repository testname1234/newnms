﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;

namespace MS.Core.DataInterfaces
{
		
	public interface IVideoClipTaskRepositoryBase
	{
        
        Dictionary<string, string> GetVideoClipTaskBasicSearchColumns();
        List<SearchColumn> GetVideoClipTaskSearchColumns();
        List<SearchColumn> GetVideoClipTaskAdvanceSearchColumns();
        

		VideoClipTask GetVideoClipTask(System.Int32 VideoClipTaskId,string SelectClause=null);
		VideoClipTask UpdateVideoClipTask(VideoClipTask entity);
		bool DeleteVideoClipTask(System.Int32 VideoClipTaskId);
		VideoClipTask DeleteVideoClipTask(VideoClipTask entity);
		List<VideoClipTask> GetPagedVideoClipTask(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<VideoClipTask> GetAllVideoClipTask(string SelectClause=null);
		VideoClipTask InsertVideoClipTask(VideoClipTask entity);
		List<VideoClipTask> GetVideoClipTaskByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
