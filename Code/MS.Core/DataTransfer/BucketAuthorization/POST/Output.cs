﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.BucketAuthorization
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 BucketAuthorizationId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? UserId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? BucketId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsRead{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsWrite{ get; set; }

	}	
}
