﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.BucketCredentials
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 BucketCredentialId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LoginId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ApiKey{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 BucketId{ get; set; }

	}	
}
