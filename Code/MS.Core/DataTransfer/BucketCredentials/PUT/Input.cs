﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.BucketCredentials
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string BucketCredentialId{ get; set; }

		[FieldLength(MaxLength = 50)]
		[DataMember (EmitDefaultValue=false)]
		public string LoginId{ get; set; }

		[FieldLength(MaxLength = 10)]
		[DataMember (EmitDefaultValue=false)]
		public string ApiKey{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string BucketId{ get; set; }

	}	
}
