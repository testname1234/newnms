﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.CodecConfig
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CodecConfigId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ImageLowResWidth{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ImageLowResHeight{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String VideoLowResCodec{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String VideoHighResCodec{ get; set; }

	}	
}
