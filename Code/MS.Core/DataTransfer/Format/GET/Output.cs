﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.Format
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FormatId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Format{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String MimeType{ get; set; }

	}	
}
