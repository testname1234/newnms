﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.Format
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string FormatId{ get; set; }

		[FieldLength(MaxLength = 10)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string Format{ get; set; }

		[FieldLength(MaxLength = 100)]
		[FieldNullable(IsNullable = true)]
		[DataMember (EmitDefaultValue=false)]
		public string MimeType{ get; set; }

	}	
}
