﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.Format
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string FormatId{ get; set; }

		[FieldLength(MaxLength = 10)]
		[DataMember (EmitDefaultValue=false)]
		public string Format{ get; set; }

		[FieldLength(MaxLength = 100)]
		[DataMember (EmitDefaultValue=false)]
		public string MimeType{ get; set; }

	}	
}
