﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MS.Core.DataTransfer.Resource.GET
{
   public class MetaOutPut
    {
        [DataMember(EmitDefaultValue = false)]
        public System.String Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Value { get; set; }
    }
}
