﻿using System;
using System.Configuration;
using System.Runtime.Serialization;
using System.Web;
using Validation;

namespace MS.Core.DataTransfer.Resource
{
    [DataContract]
	public class GetOutput
	{
        [DataMember(EmitDefaultValue = false)]
		public System.Guid Guid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceTypeId{ get; set; }

		[DataMember (EmitDefaultValue=true)]
		public System.String FileName{ get; set; }

       
        [DataMember(EmitDefaultValue = false)]
        public string ResourceStatusId { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public System.String FileTitle 
        { 
            get
            {
                if (FileName != null)
                    return FileName.Contains(".") ? FileName.Substring(0, FileName.IndexOf('.')) : FileName;
                else
                    return string.Empty;
            }
            set
            { 
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.String FileExt
        {
            get
            {
                if (FileName != null && FileName.Contains("."))
                    return "." + FileName.Split('.')[1];
                else
                    return string.Empty;
            }
            set
            {
            }
        }

		[DataMember (EmitDefaultValue=false)]
        public System.String HighResolutionSource
        {
            get
            {
                return ConfigurationManager.AppSettings["MsApi"] + "/api/resource/getresource/?id=" + this.Guid + "&ishd=true";
            }
            set
            {
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.String Source
        {
            get
            {
                return ConfigurationManager.AppSettings["MsApi"] + "/api/resource/getresource/" + this.Guid;
            }
            set
            {
            }
        }

		[DataMember (EmitDefaultValue=false)]
        public System.String ThumbUrl 
        {
            get
            {
                return ConfigurationManager.AppSettings["MsApi"] + "/api/resource/GetThumb/" + this.Guid;
            }
            set
            {
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.String Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Category { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? isHD { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public System.String Location { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Double? Duration { get; set; }

        private string _slug;
        [DataMember(EmitDefaultValue = false)]
        public System.String Slug { get { return string.IsNullOrEmpty(_slug) ? this.Caption : _slug; } set { _slug = value; } }

        [DataMember(EmitDefaultValue = false)]
        public System.String Description { get; set; }

        [IgnoreDataMember]
        public System.DateTime CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [FieldNameAttribute("LastUpdateDate", false, false, 8)]
        [IgnoreDataMember]
        public virtual System.DateTime LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public virtual string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public int Barcode { get; set; }

        [FieldNameAttribute("ResourceDate", false, false, 8)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.DateTime ResourceDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SystemType { get; set; }

	}	
}
