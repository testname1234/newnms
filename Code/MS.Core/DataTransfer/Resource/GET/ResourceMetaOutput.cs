﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MS.Core.DataTransfer.Resource.GET
{
    [DataContract]
    public class ResourceMetaOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<MetaOutPut> ResourceMetas { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Count { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Cateogry { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Location { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Duration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Filesize { get; set; }
    }
}
