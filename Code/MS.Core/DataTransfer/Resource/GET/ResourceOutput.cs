﻿using System;
using System.Runtime.Serialization;
using Validation;
using MS.Core;
using System.Collections.Generic;

namespace MS.Core.DataTransfer.Resource
{
    [DataContract]
    public class ResourceOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<MS.Core.DataTransfer.Resource.GetOutput> Resources { get; set; }

        [DataMember(EmitDefaultValue = true)]
        public int Count { get; set; }
	}	
}
