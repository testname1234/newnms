﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MS.Core.DataTransfer.Resource.GET
{
   public class ResourceOutputGuids
    {
        [DataMember(EmitDefaultValue = false)]
        public List<string> Guids { get; set; }
    }
}
