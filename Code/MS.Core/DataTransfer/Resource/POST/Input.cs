﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.Resource
{
    [DataContract]
    public class PostInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int BucketId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ApiKey { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ResourceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsHd { get; set; }

        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string Guid { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string ResourceTypeId { get; set; }

        [FieldLength(MaxLength = 500)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string HighResolutionFile { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string HighResolutionFormatId { get; set; }

        [FieldLength(MaxLength = 500)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string LowResolutionFile { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string LowResolutionFormatId { get; set; }

        [FieldLength(MaxLength = 255)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string FileName { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string ResourceStatusId { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string ServerId { get; set; }

        [FieldLength(MaxLength = 500)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string Source { get; set; }

        [FieldLength(MaxLength = 500)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string ThumbUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDate { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Boolean)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string IsActive { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Boolean)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsFromExternalSource { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Category { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Location { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Double? Duration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<KeyValuePair<string, string>> ResourceMeta { get; set; }

        [FieldLength(MaxLength = 500)]
        [DataMember(EmitDefaultValue = false)]
        public string FilePath { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SystemType { get; set; }


        [FieldTypeValidation(DataType = DataTypes.Boolean)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string IsPrivate { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string SourceTypeId { get; set; }

        [FieldLength(MaxLength = 100)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string SourceName { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string SourceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastAccesDate { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string AccessCount { get; set; }


        bool _allowDuplicate = false;
        public bool AllowDuplicate
        {
            get
            {
                return _allowDuplicate;
            }
            set
            {
                _allowDuplicate = value;
            }
        }
    }
}
