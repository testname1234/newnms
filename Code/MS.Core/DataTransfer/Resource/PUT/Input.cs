﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.Resource
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string ResourceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string Guid{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string ResourceTypeId{ get; set; }

		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string HighResolutionFile{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string HighResolutionFormatId{ get; set; }

		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string LowResolutionFile{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string LowResolutionFormatId{ get; set; }

		[FieldLength(MaxLength = 255)]
		[DataMember (EmitDefaultValue=false)]
		public string FileName{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string ResourceStatusId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string ServerId{ get; set; }

		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string Source{ get; set; }

		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string ThumbUrl{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[DataMember (EmitDefaultValue=false)]
		public string IsActive{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Category { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Location { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Double? Duration { get; set; }

	}	
}
