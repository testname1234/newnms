﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.Resource
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid Guid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String HighResolutionFile{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? HighResolutionFormatId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LowResolutionFile{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LowResolutionFormatId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String FileName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ServerId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Source{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ThumbUrl{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Category { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Location { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Double? Duration { get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsActive{ get; set; }

	}	
}
