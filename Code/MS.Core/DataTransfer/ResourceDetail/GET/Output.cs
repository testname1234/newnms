﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.ResourceDetail
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceDetailId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Description{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Decimal? FileSize{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? ResourceDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string ResourceDateStr
		{
			 get {if(ResourceDate.HasValue) return ResourceDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ResourceDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LtoStatus{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Barcode{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreatedDateStr
		{
			 get {if(CreatedDate.HasValue) return CreatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatedDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastupdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastupdatedDateStr
		{
			 get {if(LastupdatedDate.HasValue) return LastupdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastupdatedDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid Guid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ResourceDuration{ get; set; }

	}	
}
