﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.ResourceLog
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceLogId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid? ResourceGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? UserId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? AccessTypeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

	}	
}
