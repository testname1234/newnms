﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.ResourceStatus
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string ResourceStatusId{ get; set; }

		[FieldLength(MaxLength = 50)]
		[DataMember (EmitDefaultValue=false)]
		public string Status{ get; set; }

	}	
}
