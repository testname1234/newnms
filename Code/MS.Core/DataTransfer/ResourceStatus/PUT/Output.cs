﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.ResourceStatus
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Status{ get; set; }

	}	
}
