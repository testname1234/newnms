﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.SearchHistory
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string SearchHistoryId{ get; set; }

		[FieldLength(MaxLength = 300)]
		[DataMember (EmitDefaultValue=false)]
		public string SearchKeyword{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string Frequency{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

	}	
}
