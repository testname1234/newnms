﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.SystemIntegration
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string ArchiveRetrievalDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string Id{ get; set; }

	}	
}
