﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.SystemIntegration
{
    [DataContract]
	public class PutInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string ArchiveRetrievalDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string Id{ get; set; }

	}	
}
