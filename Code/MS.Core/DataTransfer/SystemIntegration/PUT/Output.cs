﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace MS.Core.DataTransfer.SystemIntegration
{
    [DataContract]
	public class PutOutput
	{
			
		[IgnoreDataMember]
		public System.DateTime? ArchiveRetrievalDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string ArchiveRetrievalDateStr
		{
			 get {if(ArchiveRetrievalDate.HasValue) return ArchiveRetrievalDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ArchiveRetrievalDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 Id{ get; set; }

	}	
}
