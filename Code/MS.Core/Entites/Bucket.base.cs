﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class BucketBase:EntityBase, IEquatable<BucketBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("BucketId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BucketId{ get; set; }

		[FieldNameAttribute("Name",false,false,150)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Path",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Path{ get; set; }

		[FieldNameAttribute("AutoSync",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean AutoSync{ get; set; }

		[FieldNameAttribute("Size",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Size{ get; set; }

		[FieldNameAttribute("ServerId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ServerId{ get; set; }

		[FieldNameAttribute("LastSyncDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastSyncDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastSyncDateStr
		{
			 get {if(LastSyncDate.HasValue) return LastSyncDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastSyncDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("FolderUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? FolderUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string FolderUpdateDateStr
		{
			 get {if(FolderUpdateDate.HasValue) return FolderUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FolderUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsParent",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsParent{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("IsPrivate",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsPrivate{ get; set; }

		[FieldNameAttribute("AllowTranscode",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? AllowTranscode{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<BucketBase> Members

        public virtual bool Equals(BucketBase other)
        {
			if(this.BucketId==other.BucketId  && this.Name==other.Name  && this.Path==other.Path  && this.AutoSync==other.AutoSync  && this.Size==other.Size  && this.ServerId==other.ServerId  && this.LastSyncDate==other.LastSyncDate  && this.FolderUpdateDate==other.FolderUpdateDate  && this.IsParent==other.IsParent  && this.ParentId==other.ParentId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsPrivate==other.IsPrivate  && this.AllowTranscode==other.AllowTranscode )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Bucket other)
        {
			if(other!=null)
			{
				this.BucketId=other.BucketId;
				this.Name=other.Name;
				this.Path=other.Path;
				this.AutoSync=other.AutoSync;
				this.Size=other.Size;
				this.ServerId=other.ServerId;
				this.LastSyncDate=other.LastSyncDate;
				this.FolderUpdateDate=other.FolderUpdateDate;
				this.IsParent=other.IsParent;
				this.ParentId=other.ParentId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsPrivate=other.IsPrivate;
				this.AllowTranscode=other.AllowTranscode;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
