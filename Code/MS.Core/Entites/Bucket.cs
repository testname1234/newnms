﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace MS.Core.Entities
{
    [DataContract]
	public partial class Bucket : BucketBase 
	{
        public string LoginId { get; set; }
        public string ApiKey { get; set; }

        public bool? IsRead { get; set; }
        public bool? IsWrite { get; set; }   
	}
}
