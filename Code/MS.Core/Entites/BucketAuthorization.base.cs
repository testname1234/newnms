﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class BucketAuthorizationBase:EntityBase, IEquatable<BucketAuthorizationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("BucketAuthorizationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BucketAuthorizationId{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		[FieldNameAttribute("BucketId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? BucketId{ get; set; }

		[FieldNameAttribute("IsRead",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsRead{ get; set; }

		[FieldNameAttribute("IsWrite",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsWrite{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<BucketAuthorizationBase> Members

        public virtual bool Equals(BucketAuthorizationBase other)
        {
			if(this.BucketAuthorizationId==other.BucketAuthorizationId  && this.UserId==other.UserId  && this.BucketId==other.BucketId  && this.IsRead==other.IsRead  && this.IsWrite==other.IsWrite )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(BucketAuthorization other)
        {
			if(other!=null)
			{
				this.BucketAuthorizationId=other.BucketAuthorizationId;
				this.UserId=other.UserId;
				this.BucketId=other.BucketId;
				this.IsRead=other.IsRead;
				this.IsWrite=other.IsWrite;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
