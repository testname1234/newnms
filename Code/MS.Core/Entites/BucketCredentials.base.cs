﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class BucketCredentialsBase:EntityBase, IEquatable<BucketCredentialsBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("BucketCredentialId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BucketCredentialId{ get; set; }

		[FieldNameAttribute("LoginId",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LoginId{ get; set; }

		[FieldNameAttribute("ApiKey",false,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ApiKey{ get; set; }

		[FieldNameAttribute("BucketId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BucketId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<BucketCredentialsBase> Members

        public virtual bool Equals(BucketCredentialsBase other)
        {
			if(this.BucketCredentialId==other.BucketCredentialId  && this.LoginId==other.LoginId  && this.ApiKey==other.ApiKey  && this.BucketId==other.BucketId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(BucketCredentials other)
        {
			if(other!=null)
			{
				this.BucketCredentialId=other.BucketCredentialId;
				this.LoginId=other.LoginId;
				this.ApiKey=other.ApiKey;
				this.BucketId=other.BucketId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
