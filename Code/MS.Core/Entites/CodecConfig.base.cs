﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class CodecConfigBase:EntityBase, IEquatable<CodecConfigBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CodecConfigId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CodecConfigId{ get; set; }

		[FieldNameAttribute("ImageLowResWidth",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ImageLowResWidth{ get; set; }

		[FieldNameAttribute("ImageLowResHeight",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ImageLowResHeight{ get; set; }

		[FieldNameAttribute("VideoLowResCodec",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String VideoLowResCodec{ get; set; }

		[FieldNameAttribute("VideoHighResCodec",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String VideoHighResCodec{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CodecConfigBase> Members

        public virtual bool Equals(CodecConfigBase other)
        {
			if(this.CodecConfigId==other.CodecConfigId  && this.ImageLowResWidth==other.ImageLowResWidth  && this.ImageLowResHeight==other.ImageLowResHeight  && this.VideoLowResCodec==other.VideoLowResCodec  && this.VideoHighResCodec==other.VideoHighResCodec )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CodecConfig other)
        {
			if(other!=null)
			{
				this.CodecConfigId=other.CodecConfigId;
				this.ImageLowResWidth=other.ImageLowResWidth;
				this.ImageLowResHeight=other.ImageLowResHeight;
				this.VideoLowResCodec=other.VideoLowResCodec;
				this.VideoHighResCodec=other.VideoHighResCodec;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
