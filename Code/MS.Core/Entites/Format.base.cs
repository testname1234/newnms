﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class FormatBase:EntityBase, IEquatable<FormatBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FormatId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FormatId{ get; set; }

		[FieldNameAttribute("Format",false,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Format{ get; set; }

		[FieldNameAttribute("MimeType",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String MimeType{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FormatBase> Members

        public virtual bool Equals(FormatBase other)
        {
			if(this.FormatId==other.FormatId  && this.Format==other.Format  && this.MimeType==other.MimeType )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Format other)
        {
			if(other!=null)
			{
				this.FormatId=other.FormatId;
				this.Format=other.Format;
				this.MimeType=other.MimeType;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
