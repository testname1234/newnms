﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MS.Core.Entites
{
    public class MediaFile
    {
        public int BucketId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        // public long Size { get; set; }
    }
}
