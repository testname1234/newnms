﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceBase:EntityBase, IEquatable<ResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceId{ get; set; }

		[FieldNameAttribute("Guid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid Guid{ get; set; }

		[FieldNameAttribute("ResourceTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceTypeId{ get; set; }

		[FieldNameAttribute("HighResolutionFile",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String HighResolutionFile{ get; set; }

		[FieldNameAttribute("HighResolutionFormatId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? HighResolutionFormatId{ get; set; }

		[FieldNameAttribute("LowResolutionFile",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LowResolutionFile{ get; set; }

		[FieldNameAttribute("LowResolutionFormatId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LowResolutionFormatId{ get; set; }

		[FieldNameAttribute("FileName",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FileName{ get; set; }

		[FieldNameAttribute("ResourceStatusId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceStatusId{ get; set; }

		[FieldNameAttribute("ServerId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ServerId{ get; set; }

		[FieldNameAttribute("Source",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Source{ get; set; }

		[FieldNameAttribute("ThumbUrl",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ThumbUrl{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

        [FieldNameAttribute("Caption",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Caption{ get; set; }

		[FieldNameAttribute("Category",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Category{ get; set; }

		[FieldNameAttribute("Location",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Location{ get; set; }

		[FieldNameAttribute("FilePath",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FilePath{ get; set; }

		[FieldNameAttribute("Duration",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Duration{ get; set; }

		[FieldNameAttribute("Bitrate",true,false,15)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Bitrate{ get; set; }

		[FieldNameAttribute("Framerate",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Framerate{ get; set; }

		[FieldNameAttribute("AudioCodec",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String AudioCodec{ get; set; }

		[FieldNameAttribute("VideoCodec",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String VideoCodec{ get; set; }

		[FieldNameAttribute("Width",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Width{ get; set; }

		[FieldNameAttribute("Height",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Height{ get; set; }

		[FieldNameAttribute("BucketId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? BucketId{ get; set; }

		[FieldNameAttribute("SystemType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SystemType{ get; set; }

		[FieldNameAttribute("IsPrivate",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsPrivate{ get; set; }

		[FieldNameAttribute("SourceTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceTypeId{ get; set; }

		[FieldNameAttribute("SourceName",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SourceName{ get; set; }

		[FieldNameAttribute("SourceId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceId{ get; set; }

		[FieldNameAttribute("LastAccesDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastAccesDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastAccesDateStr
		{
			 get {if(LastAccesDate.HasValue) return LastAccesDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastAccesDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("AccessCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AccessCount{ get; set; }

		[FieldNameAttribute("SubBucketId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SubBucketId{ get; set; }

		[FieldNameAttribute("FootageTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FootageTypeId{ get; set; }

		[FieldNameAttribute("DateStamp",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Byte[] DateStamp{ get; set; }

		[FieldNameAttribute("ServerName",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ServerName{ get; set; }

		[FieldNameAttribute("AllowLowResTrancoding",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? AllowLowResTrancoding{ get; set; }

        [FieldNameAttribute("isHD", true, false, 1)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean? isHD { get; set; }


        public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceBase> Members

        public virtual bool Equals(ResourceBase other)
        {
			if(this.ResourceId==other.ResourceId  && this.Guid==other.Guid  && this.ResourceTypeId==other.ResourceTypeId  && this.HighResolutionFile==other.HighResolutionFile  && this.HighResolutionFormatId==other.HighResolutionFormatId  && this.LowResolutionFile==other.LowResolutionFile  && this.LowResolutionFormatId==other.LowResolutionFormatId  && this.FileName==other.FileName  && this.ResourceStatusId==other.ResourceStatusId  && this.ServerId==other.ServerId  && this.Source==other.Source  && this.ThumbUrl==other.ThumbUrl  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Caption==other.Caption  && this.Category==other.Category  && this.Location==other.Location  && this.FilePath==other.FilePath  && this.Duration==other.Duration  && this.Bitrate==other.Bitrate  && this.Framerate==other.Framerate  && this.AudioCodec==other.AudioCodec  && this.VideoCodec==other.VideoCodec  && this.Width==other.Width  && this.Height==other.Height  && this.BucketId==other.BucketId  && this.SystemType==other.SystemType  && this.IsPrivate==other.IsPrivate  && this.SourceTypeId==other.SourceTypeId  && this.SourceName==other.SourceName  && this.SourceId==other.SourceId  && this.LastAccesDate==other.LastAccesDate  && this.AccessCount==other.AccessCount  && this.SubBucketId==other.SubBucketId  && this.FootageTypeId==other.FootageTypeId  && this.DateStamp==other.DateStamp  && this.ServerName==other.ServerName  && this.AllowLowResTrancoding==other.AllowLowResTrancoding )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Resource other)
        {
			if(other!=null)
			{
				this.ResourceId=other.ResourceId;
				this.Guid=other.Guid;
				this.ResourceTypeId=other.ResourceTypeId;
				this.HighResolutionFile=other.HighResolutionFile;
				this.HighResolutionFormatId=other.HighResolutionFormatId;
				this.LowResolutionFile=other.LowResolutionFile;
				this.LowResolutionFormatId=other.LowResolutionFormatId;
				this.FileName=other.FileName;
				this.ResourceStatusId=other.ResourceStatusId;
				this.ServerId=other.ServerId;
				this.Source=other.Source;
				this.ThumbUrl=other.ThumbUrl;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Caption=other.Caption;
				this.Category=other.Category;
				this.Location=other.Location;
				this.FilePath=other.FilePath;
				this.Duration=other.Duration;
				this.Bitrate=other.Bitrate;
				this.Framerate=other.Framerate;
				this.AudioCodec=other.AudioCodec;
				this.VideoCodec=other.VideoCodec;
				this.Width=other.Width;
				this.Height=other.Height;
				this.BucketId=other.BucketId;
				this.SystemType=other.SystemType;
				this.IsPrivate=other.IsPrivate;
				this.SourceTypeId=other.SourceTypeId;
				this.SourceName=other.SourceName;
				this.SourceId=other.SourceId;
				this.LastAccesDate=other.LastAccesDate;
				this.AccessCount=other.AccessCount;
				this.SubBucketId=other.SubBucketId;
				this.FootageTypeId=other.FootageTypeId;
				this.DateStamp=other.DateStamp;
				this.ServerName=other.ServerName;
				this.AllowLowResTrancoding=other.AllowLowResTrancoding;
                this.isHD = other.isHD;
            }
			
		
		}

        #endregion
		
		
		
	}
	
	
}
