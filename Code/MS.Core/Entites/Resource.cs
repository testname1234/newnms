﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using MS.Core.Enums;


namespace MS.Core.Entities
{
    [DataContract]
    public partial class Resource : ResourceBase
    {
        public ResourceStatuses ResourceStatus { get { return (ResourceStatuses)base.ResourceStatusId; } set { base.ResourceStatusId = (int)value; } }

        public bool IsFromExternalSource { get; set; }

        public string Slug { get; set; }

        public string Keyword { get; set; }

        public string Description { get; set; }

        public string Metas { get; set; }

        public int? Barcode { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime? ResourceDate { get; set; }

        public long? TimeStamp { get; set; }
    }
}
