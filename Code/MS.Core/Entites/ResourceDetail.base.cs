﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceDetailBase:EntityBase, IEquatable<ResourceDetailBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceDetailId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceDetailId{ get; set; }

		[FieldNameAttribute("slug",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("Description",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Description{ get; set; }

		[FieldNameAttribute("FileSize",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? FileSize{ get; set; }

		[FieldNameAttribute("ResourceDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? ResourceDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string ResourceDateStr
		{
			 get {if(ResourceDate.HasValue) return ResourceDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ResourceDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LtoStatus",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LtoStatus{ get; set; }

		[FieldNameAttribute("Barcode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Barcode{ get; set; }

		[FieldNameAttribute("CreatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreatedDateStr
		{
			 get {if(CreatedDate.HasValue) return CreatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastupdatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastupdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastupdatedDateStr
		{
			 get {if(LastupdatedDate.HasValue) return LastupdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastupdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("ResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceId{ get; set; }

		[FieldNameAttribute("Guid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid Guid{ get; set; }

		[FieldNameAttribute("ResourceDuration",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResourceDuration{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceDetailBase> Members

        public virtual bool Equals(ResourceDetailBase other)
        {
			if(this.ResourceDetailId==other.ResourceDetailId  && this.Slug==other.Slug  && this.Description==other.Description  && this.FileSize==other.FileSize  && this.ResourceDate==other.ResourceDate  && this.LtoStatus==other.LtoStatus  && this.Barcode==other.Barcode  && this.LastupdatedDate==other.LastupdatedDate  && this.IsActive==other.IsActive  && this.ResourceId==other.ResourceId  && this.Guid==other.Guid  && this.ResourceDuration==other.ResourceDuration )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ResourceDetail other)
        {
			if(other!=null)
			{
				this.ResourceDetailId=other.ResourceDetailId;
				this.Slug=other.Slug;
				this.Description=other.Description;
				this.FileSize=other.FileSize;
				this.ResourceDate=other.ResourceDate;
				this.LtoStatus=other.LtoStatus;
				this.Barcode=other.Barcode;
				this.CreatedDate=other.CreatedDate;
				this.LastupdatedDate=other.LastupdatedDate;
				this.IsActive=other.IsActive;
				this.ResourceId=other.ResourceId;
				this.Guid=other.Guid;
				this.ResourceDuration=other.ResourceDuration;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
