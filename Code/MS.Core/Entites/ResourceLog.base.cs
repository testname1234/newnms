﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceLogBase:EntityBase, IEquatable<ResourceLogBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceLogId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceLogId{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? ResourceGuid{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		[FieldNameAttribute("AccessTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AccessTypeId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceLogBase> Members

        public virtual bool Equals(ResourceLogBase other)
        {
			if(this.ResourceLogId==other.ResourceLogId  && this.ResourceGuid==other.ResourceGuid  && this.UserId==other.UserId  && this.AccessTypeId==other.AccessTypeId  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ResourceLog other)
        {
			if(other!=null)
			{
				this.ResourceLogId=other.ResourceLogId;
				this.ResourceGuid=other.ResourceGuid;
				this.UserId=other.UserId;
				this.AccessTypeId=other.AccessTypeId;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
