﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceMetaBase:EntityBase, IEquatable<ResourceMetaBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceMetaId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceMetaId{ get; set; }

		[FieldNameAttribute("ResourceGuid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid ResourceGuid{ get; set; }

		[FieldNameAttribute("Name",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Value",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Value{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceMetaBase> Members

        public virtual bool Equals(ResourceMetaBase other)
        {
			if(this.ResourceMetaId==other.ResourceMetaId  && this.ResourceGuid==other.ResourceGuid  && this.Name==other.Name  && this.Value==other.Value  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ResourceMeta other)
        {
			if(other!=null)
			{
				this.ResourceMetaId=other.ResourceMetaId;
				this.ResourceGuid=other.ResourceGuid;
				this.Name=other.Name;
				this.Value=other.Value;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
