﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceStatusBase:EntityBase, IEquatable<ResourceStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceStatusId{ get; set; }

		[FieldNameAttribute("Status",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Status{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceStatusBase> Members

        public virtual bool Equals(ResourceStatusBase other)
        {
			if(this.ResourceStatusId==other.ResourceStatusId  && this.Status==other.Status )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ResourceStatus other)
        {
			if(other!=null)
			{
				this.ResourceStatusId=other.ResourceStatusId;
				this.Status=other.Status;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
