﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceTypeBase:EntityBase, IEquatable<ResourceTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceTypeId{ get; set; }

		[FieldNameAttribute("Name",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceTypeBase> Members

        public virtual bool Equals(ResourceTypeBase other)
        {
			if(this.ResourceTypeId==other.ResourceTypeId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ResourceType other)
        {
			if(other!=null)
			{
				this.ResourceTypeId=other.ResourceTypeId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
