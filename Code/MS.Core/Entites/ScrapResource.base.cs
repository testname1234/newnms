﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ScrapResourceBase:EntityBase, IEquatable<ScrapResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ScrapResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScrapResourceId{ get; set; }

		[FieldNameAttribute("ResourceGuid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid ResourceGuid{ get; set; }

		[FieldNameAttribute("Source",false,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Source{ get; set; }

		[FieldNameAttribute("DownloadedFilePath",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DownloadedFilePath{ get; set; }

		[FieldNameAttribute("StatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("SystemType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SystemType{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ScrapResourceBase> Members

        public virtual bool Equals(ScrapResourceBase other)
        {
			if(this.ScrapResourceId==other.ScrapResourceId  && this.ResourceGuid==other.ResourceGuid  && this.Source==other.Source  && this.DownloadedFilePath==other.DownloadedFilePath  && this.StatusId==other.StatusId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.SystemType==other.SystemType )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ScrapResource other)
        {
			if(other!=null)
			{
				this.ScrapResourceId=other.ScrapResourceId;
				this.ResourceGuid=other.ResourceGuid;
				this.Source=other.Source;
				this.DownloadedFilePath=other.DownloadedFilePath;
				this.StatusId=other.StatusId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.SystemType=other.SystemType;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
