﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using MS.Core.Enums;


namespace MS.Core.Entities
{
    [DataContract]
	public partial class ScrapResource : ScrapResourceBase 
	{
        public ScrapResourceStatuses Status { get { return (ScrapResourceStatuses)base.StatusId; } set { base.StatusId = (int)value; } }                
	}
}
