﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class SearchHistoryBase:EntityBase, IEquatable<SearchHistoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SearchHistoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SearchHistoryId{ get; set; }

		[FieldNameAttribute("SearchKeyword",true,false,300)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SearchKeyword{ get; set; }

		[FieldNameAttribute("Frequency",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Frequency{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SearchHistoryBase> Members

        public virtual bool Equals(SearchHistoryBase other)
        {
			if(this.SearchHistoryId==other.SearchHistoryId  && this.SearchKeyword==other.SearchKeyword  && this.Frequency==other.Frequency  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SearchHistory other)
        {
			if(other!=null)
			{
				this.SearchHistoryId=other.SearchHistoryId;
				this.SearchKeyword=other.SearchKeyword;
				this.Frequency=other.Frequency;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
