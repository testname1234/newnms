﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class ServerBase:EntityBase, IEquatable<ServerBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ServerId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ServerId{ get; set; }

		[FieldNameAttribute("ServerIP",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ServerIp{ get; set; }

		[FieldNameAttribute("IsDefault",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsDefault{ get; set; }

		[FieldNameAttribute("DefaultBucketPath",true,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DefaultBucketPath{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ServerBase> Members

        public virtual bool Equals(ServerBase other)
        {
			if(this.ServerId==other.ServerId  && this.ServerIp==other.ServerIp  && this.IsDefault==other.IsDefault  && this.DefaultBucketPath==other.DefaultBucketPath  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Server other)
        {
			if(other!=null)
			{
				this.ServerId=other.ServerId;
				this.ServerIp=other.ServerIp;
				this.IsDefault=other.IsDefault;
				this.DefaultBucketPath=other.DefaultBucketPath;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
