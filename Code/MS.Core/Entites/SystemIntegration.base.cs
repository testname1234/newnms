﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class SystemIntegrationBase:EntityBase, IEquatable<SystemIntegrationBase>
	{
			
        [PrimaryKey]
        [FieldNameAttribute("Id", false, false, 4)]
		[DataMember (EmitDefaultValue=false)]
        public virtual System.Int32 Id { get; set; }

		[FieldNameAttribute("ArchiveRetrievalDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? ArchiveRetrievalDate{ get; set;}

        


		[DataMember (EmitDefaultValue=false)]
		public virtual string ArchiveRetrievalDateStr
		{
			 get {if(ArchiveRetrievalDate.HasValue) return ArchiveRetrievalDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ArchiveRetrievalDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SystemIntegrationBase> Members

        public virtual bool Equals(SystemIntegrationBase other)
        {
			if(this.ArchiveRetrievalDate==other.ArchiveRetrievalDate  && this.Id==other.Id )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SystemIntegration other)
        {
			if(other!=null)
			{
				this.ArchiveRetrievalDate=other.ArchiveRetrievalDate;
				this.Id=other.Id;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
