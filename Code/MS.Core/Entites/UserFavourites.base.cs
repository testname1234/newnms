﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class UserFavouritesBase:EntityBase, IEquatable<UserFavouritesBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("UserFavouritesId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserFavouritesId{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? ResourceGuid{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<UserFavouritesBase> Members

        public virtual bool Equals(UserFavouritesBase other)
        {
			if(this.UserFavouritesId==other.UserFavouritesId  && this.ResourceGuid==other.ResourceGuid  && this.UserId==other.UserId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(UserFavourites other)
        {
			if(other!=null)
			{
				this.UserFavouritesId=other.UserFavouritesId;
				this.ResourceGuid=other.ResourceGuid;
				this.UserId=other.UserId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
