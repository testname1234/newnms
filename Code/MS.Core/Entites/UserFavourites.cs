﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace MS.Core.Entities
{
    [DataContract]
	public partial class UserFavourites : UserFavouritesBase 
	{
        [DataMember(EmitDefaultValue = false)]
        public  System.Int32 ResourceTypeId { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string Slug { get; set; }
	}
}
