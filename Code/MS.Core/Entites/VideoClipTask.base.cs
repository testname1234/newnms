﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace MS.Core.Entities
{
    [DataContract]
	public abstract partial class VideoClipTaskBase:EntityBase, IEquatable<VideoClipTaskBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("VideoClipTaskId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 VideoClipTaskId{ get; set; }

		[FieldNameAttribute("ResourceGuid",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResourceGuid{ get; set; }

		[FieldNameAttribute("InputObjectJson",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String InputObjectJson{ get; set; }

		[FieldNameAttribute("StatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<VideoClipTaskBase> Members

        public virtual bool Equals(VideoClipTaskBase other)
        {
			if(this.VideoClipTaskId==other.VideoClipTaskId  && this.ResourceGuid==other.ResourceGuid  && this.InputObjectJson==other.InputObjectJson  && this.StatusId==other.StatusId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(VideoClipTask other)
        {
			if(other!=null)
			{
				this.VideoClipTaskId=other.VideoClipTaskId;
				this.ResourceGuid=other.ResourceGuid;
				this.InputObjectJson=other.InputObjectJson;
				this.StatusId=other.StatusId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
