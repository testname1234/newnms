﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MS.Core.Enums
{
    public enum AccessType
    {
        ViewResource=1,
        DownloadLowREs=2,
        DownloadHighREs = 3,
    }
}
