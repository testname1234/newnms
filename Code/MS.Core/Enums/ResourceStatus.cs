﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MS.Core.Enums
{
    public enum ResourceStatuses
    {
        Uploading=1,
        DownloadPending=2,
        Transcoding=3,
        Completed=4,
        FileInUse = 5,
        FileInfoPending=6,
        TranscodingRequired=7,
        Discarded = 8,
	    Deleted = 9,
        TempFile = 11
    }
}
