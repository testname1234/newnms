﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MS.Core.Enums
{
    public enum ResourceTypes
    {
        Image=1,
        Video=2,
        Audio=3,
        Document=4,
        None=5,
        ProgramBarCode = 6
    }
}
