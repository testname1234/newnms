﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MS.Core.Enums
{
    public enum ScrapResourceStatuses
    {
        DownloadPending=1,
        Downloaded=2,
        Completed=3,
        DownloadPendingtry1 = 4,
        DownloadPendingtry2 = 5,
        DownloadPendingtry3 = 6,
        Error = 7,
        MSPendingtry1 = 8,
        MSPendingtry2 = 9,
        MSError= 10,
    }
}
