﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MS.Core.Enums
{
    public enum VideoClipTaskStatus
    {
        Created=1,
        Completed=2,
        Error=3
    }
}
