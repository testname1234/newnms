﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MS.Core.Enums;

namespace MS.Core.Helper
{
    public class ResourceHelper
    {
        public static bool GenerateSnapshot(string ffmpegpath, string source, string folderPath, string destinationFile, ResourceTypes type, TimeSpan? ts = null)
        {
            if (type == ResourceTypes.Video)
            {
                FFMPEGLib.FFMPEG.InitialPath = ffmpegpath;
                FFMPEGLib.FFMPEG.CreateThumb(source, destinationFile, folderPath, ts);
                return true;
            }
            else if (type == ResourceTypes.Image)
            {
                using (System.Drawing.Image image = System.Drawing.Image.FromFile(source))
                {
                    System.Drawing.Image thumbnailImage = image.GetThumbnailImage(135, 75, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
                    using (FileStream imageStream = new FileStream(folderPath + destinationFile, FileMode.Create))
                    {
                        thumbnailImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
                return true;
            }
            return false;
        }

        public static bool ChangeImageResolution(string source, string folderPath, string destinationFile, int width, int height)
        {
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(source))
            {
                using (FileStream imageStream = new FileStream(folderPath + destinationFile, FileMode.Create))
                {
                    image.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
            
            return true;
        }

        public static bool ThumbnailCallback()
        {
            return true;
        }
    }


}
