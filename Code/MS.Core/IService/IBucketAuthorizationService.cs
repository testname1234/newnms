﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.BucketAuthorization;

namespace MS.Core.IService
{
		
	public interface IBucketAuthorizationService
	{
        Dictionary<string, string> GetBucketAuthorizationBasicSearchColumns();
        
        List<SearchColumn> GetBucketAuthorizationAdvanceSearchColumns();

		BucketAuthorization GetBucketAuthorization(System.Int32 BucketAuthorizationId);
		DataTransfer<List<GetOutput>> GetAll();
		BucketAuthorization UpdateBucketAuthorization(BucketAuthorization entity);
		bool DeleteBucketAuthorization(System.Int32 BucketAuthorizationId);
		List<BucketAuthorization> GetAllBucketAuthorization();
		BucketAuthorization InsertBucketAuthorization(BucketAuthorization entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
