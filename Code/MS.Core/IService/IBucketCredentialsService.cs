﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.BucketCredentials;

namespace MS.Core.IService
{
		
	public interface IBucketCredentialsService
	{
        Dictionary<string, string> GetBucketCredentialsBasicSearchColumns();
        
        List<SearchColumn> GetBucketCredentialsAdvanceSearchColumns();

		List<BucketCredentials> GetBucketCredentialsByBucketId(System.Int32 BucketId);
		BucketCredentials GetBucketCredentials(System.Int32 BucketCredentialId);
		DataTransfer<List<GetOutput>> GetAll();
		BucketCredentials UpdateBucketCredentials(BucketCredentials entity);
		bool DeleteBucketCredentials(System.Int32 BucketCredentialId);
		List<BucketCredentials> GetAllBucketCredentials();
		BucketCredentials InsertBucketCredentials(BucketCredentials entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);


        BucketCredentials GetBucketCredentialsByApiKey(int bucketId, string apiKey);

        BucketCredentials GetBucketByLoginIdAndApiKey(string p1, string p2);
    }
	
	
}
