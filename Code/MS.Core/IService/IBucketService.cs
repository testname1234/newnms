﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Bucket;
using MS.Core.Models;

namespace MS.Core.IService
{
		
	public interface IBucketService
	{
        Dictionary<string, string> GetBucketBasicSearchColumns();
        
        List<SearchColumn> GetBucketAdvanceSearchColumns();

		Bucket GetBucket(System.Int32 BucketId);
		DataTransfer<List<GetOutput>> GetAll();
		Bucket UpdateBucket(Bucket entity);
		bool DeleteBucket(System.Int32 BucketId);
		List<Bucket> GetAllBucket();
		Bucket InsertBucket(Bucket entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Bucket> GetBucketByParentId(int parentId);

        Bucket GetBucketByName(string p);

        Bucket GetBucketByPath(string p);

		Bucket GetBucketByApiKey(int bucketId, string apiKey);

        Bucket GetSubBucket(string name, int parentId);

        Bucket GetBucketAndCredentials(int bucketId);

        List<FolderModel> GetAllBucketWithParentName(int userId);

        List<ResourceModel> GetSubBucketResources(int bucketId);

        List<Bucket> GetBucketsToTranscode();

        List<Bucket> GetBucketsByParentId(int parentId);
    }
	
	
}
