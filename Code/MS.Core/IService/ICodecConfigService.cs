﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.CodecConfig;

namespace MS.Core.IService
{
		
	public interface ICodecConfigService
	{
        Dictionary<string, string> GetCodecConfigBasicSearchColumns();
        
        List<SearchColumn> GetCodecConfigAdvanceSearchColumns();

		CodecConfig GetCodecConfig(System.Int32 CodecConfigId);
		DataTransfer<List<GetOutput>> GetAll();
		CodecConfig UpdateCodecConfig(CodecConfig entity);
		bool DeleteCodecConfig(System.Int32 CodecConfigId);
		List<CodecConfig> GetAllCodecConfig();
		CodecConfig InsertCodecConfig(CodecConfig entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
