﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Format;

namespace MS.Core.IService
{
		
	public interface IFormatService
	{
        Dictionary<string, string> GetFormatBasicSearchColumns();
        
        List<SearchColumn> GetFormatAdvanceSearchColumns();

		Format GetFormat(System.Int32 FormatId);
		DataTransfer<List<GetOutput>> GetAll();
		Format UpdateFormat(Format entity);
		bool DeleteFormat(System.Int32 FormatId);
		List<Format> GetAllFormat();
		Format InsertFormat(Format entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
