﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceDetail;

namespace MS.Core.IService
{
		
	public interface IResourceDetailService
	{
        Dictionary<string, string> GetResourceDetailBasicSearchColumns();
        
        List<SearchColumn> GetResourceDetailAdvanceSearchColumns();

		ResourceDetail GetResourceDetail(System.Int32 ResourceDetailId);
		DataTransfer<List<GetOutput>> GetAll();
		ResourceDetail UpdateResourceDetail(ResourceDetail entity);
		bool DeleteResourceDetail(System.Int32 ResourceDetailId);
		List<ResourceDetail> GetAllResourceDetail();
		ResourceDetail InsertResourceDetail(ResourceDetail entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        ResourceDetail GetResourceDetailByGuid(string Guid, string SelectClause = null);
    }
	
	
}
