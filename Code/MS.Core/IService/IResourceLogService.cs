﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceLog;
using MS.Core.Models;
namespace MS.Core.IService
{
		
	public interface IResourceLogService
	{
        Dictionary<string, string> GetResourceLogBasicSearchColumns();
        
        List<SearchColumn> GetResourceLogAdvanceSearchColumns();

		ResourceLog GetResourceLog(System.Int32 ResourceLogId);
		DataTransfer<List<GetOutput>> GetAll();
		ResourceLog UpdateResourceLog(ResourceLog entity);
		bool DeleteResourceLog(System.Int32 ResourceLogId);
		List<ResourceLog> GetAllResourceLog();
		ResourceLog InsertResourceLog(ResourceLog entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<UserLog> getAllUSerLog();

        List<ResourceLog> GetResourceLogbyUserId(int p);
    }
	
	
}
