﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceMeta;

namespace MS.Core.IService
{
		
	public interface IResourceMetaService
	{
        Dictionary<string, string> GetResourceMetaBasicSearchColumns();
        
        List<SearchColumn> GetResourceMetaAdvanceSearchColumns();

		ResourceMeta GetResourceMeta(System.Int32 ResourceMetaId);
		DataTransfer<List<GetOutput>> GetAll();
		ResourceMeta UpdateResourceMeta(ResourceMeta entity);
		bool DeleteResourceMeta(System.Int32 ResourceMetaId);
		List<ResourceMeta> GetAllResourceMeta();
        ResourceMeta CheckAndInsertResourceMeta(ResourceMeta entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        ResourceMeta InsertResourceMeta(ResourceMeta entity);

        List<ResourceMeta> GetResourceMetaByGuid(string Guid);

        List<ResourceMeta> GetResourceMetaforThread();

        List<ResourceMeta> GetResourceMetaforThreadAfterDate(DateTime lastUpdateDate);

        ResourceMeta InsertResourceMetaWithoutChecking(ResourceMeta entity);
    }
	
	
}
