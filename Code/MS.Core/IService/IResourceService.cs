﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Resource;
using MS.Core.Enums;
using SolrManager;
using SolrManager.InputEntities;

namespace MS.Core.IService
{
		
	public interface IResourceService
	{
        Dictionary<string, string> GetResourceBasicSearchColumns();
        
        List<SearchColumn> GetResourceAdvanceSearchColumns();

		List<Resource> GetResourceByResourceTypeId(System.Int32 ResourceTypeId);
		List<Resource> GetResourceByResourceStatusId(System.Int32 ResourceStatusId);
		List<Resource> GetResourceByServerId(System.Int32? ServerId);
        List<Resource> GetResourceByStatusId(int StatusId);
            
        List<Resource> GetResourceByStatusId(int StatusId, int count);
        List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId);
        List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId, int Count);
		Resource GetResource(System.Int32 ResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		Resource UpdateResource(Resource entity);
		bool DeleteResource(System.Int32 ResourceId);
		List<Resource> GetAllResource();
		Resource InsertResource(Resource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        Resource GetResourceByGuidId(Guid guid);
        Resource GetResourceByFilePath(string FilePath, int BucketId);
        List<Resource> GetResourceByCaption(string caption);
        List<Resource> GetResourceByMeta(string MetaString);

        bool DeleteResourceByGuid(Guid guid);

        Resource GetResourceBySource(string source);

        Resource GetResourceBySourceAndBucketId(string source, int bucketId);

        Resource GetResourceByBarcode(string barcode);

        List<ScrapResource> GetScrapResourceByStatusId(ScrapResourceStatuses? status);

        List<ScrapResource> GetScrapResourceByStatusIdAndSystemTypeId(ScrapResourceStatuses? status, int? SystemTypeId);  

        void UpdateScrapResource(ScrapResource scrapRes);

        Resource UpdateResourceInfo(Resource entity);

       // Resource UpdateResourceDuration(Resource entity);

        List<Resource> GetResourceforDuration(DateTime resourceDate);

        void UpdateScrapResourceNoReturn(ScrapResource scrapRes);

        List<Resource> GetResourceByBucketId(int bucketId);

        List<Resource> GetAllResourcesByFilePath(string path);

        List<Resource> GetAllResourcesByFilePath(string FilePath, int BucketId);

        string GetResourcePathByGuid(Guid guid);

        //List<Resource> SearchResource(string term, string collectionURL, int pageSize, int pageNumber);

        //List<Resource> SearchResource(string term, string collectionURL, int pageSize, int pageNumber, int? resourceTypeId,int? bucketid ,bool aldata,  out int resourceCount);

        //int FillSolrResourceCache(string collectionUrl);

        int FillSolrResourceCache(SolrService<SolrResource> solrService);

        void GenerateFileInformation(Resource res, string tempFilePath);

        Resource GenerateResourceThumbAndFindInformation(Resource res, string tempFilePath, Bucket bucket, Server server);

        Resource GetResourceByFilePathAndBucketId(string filePath, int bucketId);

        List<ScrapResource> GetDownloadedResources();
      
        List<ScrapResource> GetScrapResourceByDate(DateTime CreationDate);

        void DeleteScrapResource(int scrapResourceid);
        Resource GetResourceByBarcodeAndResourceType(string barcode, int resourceTypeId);

        List<Resource> GetTop100Resources();

        List<string> GetAllCelebrityNames();

        List<string> GetAllCelebrityNames(DateTime lastExecutionDate);

        List<string> SearchCelebrityName(string term, int pageSize, int pageNumber);

        List<Resource> GetResourceByGuidIds(List<string> list);

        List<Resource> GetResourceByBarcodeRange(int start1, int start2);


        Resource GetResourceByGuidIdforUserFavourite(Guid guid);

        List<string> SearchTag(string term, int pageSize, int pageNumber);

        List<Resource> SearchMeta(string term, string Metas, int pageSize, int pageNumber, int? resourceTypeId, int? bucketid, bool p, out int returnCount);

        List<Resource> AdvanceSearch(string term, string Metas, int pageSize, int pageNumber, int? resourceTypeId, int? bucketid, bool p, out int returnCount);


        List<Resource> SearchResources(string term , string fromDate, string toDate, string p1, int pageSize, bool isHD,int pageNumber, int? resourceTypeId, int? bucketid, bool p2, out int returnCount);
        List<Resource> SearchResourceByDates(DateTime fromDate, DateTime toDate, string p1, int pageSize, bool isHD,int pageNumber, int? resourceTypeId, int? bucketid, bool p2, out int returnCount);

        List<Resource> SearchResourceByTermnDates(string term,DateTime fromDate, DateTime toDate, string p1, int pageSize, bool isHD,int pageNumber, int? resourceTypeId, int? bucketid, bool p2, out int returnCount);


        List<Resource> GetAllResourceWithDetailAndMetas();

        List<Resource> GetResourcesAfterThisDate(long dateStamp);

        bool CopyFilesOnServerAndTempFolder(Resource res, System.Web.HttpPostedFile file);

        List<Resource> GetResourcesForLowResTranscoding(int count);

        Resource UpdateResourceWithTimeStamp(Resource entity);

        bool UpdateResourceServerName(string ServerName);

        List<Resource> GetResourceByDate(DateTime ResourceDate);
        List<Resource> GetResourceIdFilePathandSubBucketIdByParent(int bucketId);

        bool UpdateResourceStatus(int resourceId, int statusId);


        List<Resource> GetResourceByStatusIdAfterDate(long dateStamp, int p);

        List<Resource> GetdeletedResources();
    }
	
	
}
