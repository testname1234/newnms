﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceStatus;

namespace MS.Core.IService
{
		
	public interface IResourceStatusService
	{
        Dictionary<string, string> GetResourceStatusBasicSearchColumns();
        
        List<SearchColumn> GetResourceStatusAdvanceSearchColumns();

		ResourceStatus GetResourceStatus(System.Int32 ResourceStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		ResourceStatus UpdateResourceStatus(ResourceStatus entity);
		bool DeleteResourceStatus(System.Int32 ResourceStatusId);
		List<ResourceStatus> GetAllResourceStatus();
		ResourceStatus InsertResourceStatus(ResourceStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
