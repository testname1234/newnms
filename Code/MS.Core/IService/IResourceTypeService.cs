﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceType;

namespace MS.Core.IService
{
		
	public interface IResourceTypeService
	{
        Dictionary<string, string> GetResourceTypeBasicSearchColumns();
        
        List<SearchColumn> GetResourceTypeAdvanceSearchColumns();

		ResourceType GetResourceType(System.Int32 ResourceTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		ResourceType UpdateResourceType(ResourceType entity);
		bool DeleteResourceType(System.Int32 ResourceTypeId);
		List<ResourceType> GetAllResourceType();
		ResourceType InsertResourceType(ResourceType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
