﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.SearchHistory;

namespace MS.Core.IService
{
		
	public interface ISearchHistoryService
	{
        Dictionary<string, string> GetSearchHistoryBasicSearchColumns();
        
        List<SearchColumn> GetSearchHistoryAdvanceSearchColumns();

		SearchHistory GetSearchHistory(System.Int32 SearchHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		SearchHistory UpdateSearchHistory(SearchHistory entity);
		bool DeleteSearchHistory(System.Int32 SearchHistoryId);
		List<SearchHistory> GetAllSearchHistory();
		SearchHistory InsertSearchHistory(SearchHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<string> GetAllSearchHistoryKeywords();

        List<string> GetAllSearchHistoryKeywords(DateTime lastUpdateDate);
    }
	
	
}
