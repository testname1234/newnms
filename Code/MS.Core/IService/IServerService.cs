﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Server;

namespace MS.Core.IService
{
		
	public interface IServerService
	{
        Dictionary<string, string> GetServerBasicSearchColumns();
        
        List<SearchColumn> GetServerAdvanceSearchColumns();

		Server GetServer(System.Int32 ServerId);
        Server GetDefaultServer();
		DataTransfer<List<GetOutput>> GetAll();
		Server UpdateServer(Server entity);
		bool DeleteServer(System.Int32 ServerId);
		List<Server> GetAllServer();
		Server InsertServer(Server entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
