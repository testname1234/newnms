﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.SlugBucket;

namespace MS.Core.IService
{
		
	public interface ISlugBucketService
	{
        Dictionary<string, string> GetSlugBucketBasicSearchColumns();
        
        List<SearchColumn> GetSlugBucketAdvanceSearchColumns();

		SlugBucket GetSlugBucket(System.Int32 SlugBucketId);
		DataTransfer<List<GetOutput>> GetAll();
		SlugBucket UpdateSlugBucket(SlugBucket entity);
		bool DeleteSlugBucket(System.Int32 SlugBucketId);
		List<SlugBucket> GetAllSlugBucket();
		SlugBucket InsertSlugBucket(SlugBucket entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
