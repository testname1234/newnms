﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.SystemIntegration;

namespace MS.Core.IService
{
		
	public interface ISystemIntegrationService
	{
        Dictionary<string, string> GetSystemIntegrationBasicSearchColumns();
        
        List<SearchColumn> GetSystemIntegrationAdvanceSearchColumns();

		SystemIntegration GetSystemIntegration(System.Int32 Id);
		DataTransfer<List<GetOutput>> GetAll();
		SystemIntegration UpdateSystemIntegration(SystemIntegration entity);
		bool DeleteSystemIntegration(System.Int32 Id);
		List<SystemIntegration> GetAllSystemIntegration();
		SystemIntegration InsertSystemIntegration(SystemIntegration entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
