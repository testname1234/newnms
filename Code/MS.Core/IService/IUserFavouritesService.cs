﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.UserFavourites;

namespace MS.Core.IService
{
		
	public interface IUserFavouritesService
	{
        Dictionary<string, string> GetUserFavouritesBasicSearchColumns();
        
        List<SearchColumn> GetUserFavouritesAdvanceSearchColumns();

		UserFavourites GetUserFavourites(System.Int32 UserFavouritesId);
		DataTransfer<List<GetOutput>> GetAll();
		UserFavourites UpdateUserFavourites(UserFavourites entity);
		bool DeleteUserFavourites(System.Int32 UserFavouritesId);
		List<UserFavourites> GetAllUserFavourites();
		UserFavourites InsertUserFavourites(UserFavourites entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<UserFavourites> GetUserFavouritesByUserId(int UserId);

        bool DeleteUserFavouritesByGuid(int UserId, string guid);
    }
	
	
}
