﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataTransfer;

namespace MS.Core.IService
{
		
	public interface IVideoClipTaskService
	{
        Dictionary<string, string> GetVideoClipTaskBasicSearchColumns();
        
        List<SearchColumn> GetVideoClipTaskAdvanceSearchColumns();

		VideoClipTask GetVideoClipTask(System.Int32 VideoClipTaskId);
		VideoClipTask UpdateVideoClipTask(VideoClipTask entity);
		bool DeleteVideoClipTask(System.Int32 VideoClipTaskId);
		List<VideoClipTask> GetAllVideoClipTask();
		VideoClipTask InsertVideoClipTask(VideoClipTask entity);

        DataTransfer<string> Delete(string id);

        List<VideoClipTask> GetVideoClipByStatusId(int VideoClipStatusId);
	}
	
	
}
