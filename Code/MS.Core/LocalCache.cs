﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MS.Core.Entities;

namespace MS.Core
{
    public class LocalCache
    {
        private static CacheStorage _cache;
        public static CacheStorage Cache
        {
            get
            {
                if (_cache == null)
                    _cache = new CacheStorage();
                return _cache;
            }
        }
    }
}
