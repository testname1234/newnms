﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MS.Core.Models
{
    public class CropImageInput
    {
        public string ImageUrl { get; set; }
        public List<PointInput> Points { get; set; }
        public int BucketId { get; set; }
        public string ApiKey { get; set; }
    }

    public class PointInput
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
