﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MS.Core.Models
{
    public class CustomSnapshotInput
    {
        public string Id { get; set; }

        public double TimeSpan { get; set; }

        public int ResourceTypeId { get; set; }

        public string FilePath { get; set; }
    }
}