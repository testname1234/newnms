﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MS.Core.Models
{
    public class FolderModel
    {
        public int BucketID { get; set; }

        public string ApiKey { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public List<FolderModel> Folders { get; set; }

        public string DisplayName { get; set; }

        public bool IsRead { get; set; }

        public bool IsWrite { get; set; }

        public List<FolderModel> UserBuckets { get; set; }
    }
}
