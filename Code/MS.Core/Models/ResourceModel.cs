﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MS.Core.Models
{
    public class ResourceModel
    {
        public string Guid { get; set; }
        public string Source { get; set; }
        public string DownloadHighRes { get; set; }
        public string DownloadLowRes { get; set; }
        public string Title { get; set; }
        public string SourceName { get; set; }
        public string ThumbUrl { get; set; }
        public int? ResourceType { get; set; }
        public string DurationStr { get { return Duration == null ? null : TimeSpan.FromSeconds(Duration.Value).ToString(); } }

        public string DurationStrCropped { get { return Duration == null ? null : this.DurationStr.Substring(0,8); } }

        public string ClippedTitle { get; set; }
        public string ResourceTypeName { get {
            if (this.ResourceType == 1)
                return "Image";
            else if (this.ResourceType == 2)
                return "Video";
            else if (this.ResourceType == 3)
                return "Audio";
            else
                return "Document";
        } }

        public double? Duration { get; set; }

        public virtual System.DateTime? CreationDate { get; set; }


        public virtual string CreationDateStr
        {
            get { if (CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        public virtual string CreationDateString
        {
            get { if (CreationDate.HasValue) return CreationDate.Value.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToLocalTime(); } }
        }

        public int? ServerId { get; set; }

        public int? BucketId { get; set; }
    }
}
