﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MS.Core.Models
{
    public class UserLog
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int Hits { get; set; }
    }
}
