﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MS.Core.Models
{
    public class VideoClipInput
    {
        public int BucketId { get; set; }
        public string ApiKey { get; set; }
        public string Guid { get; set; }
        public List<VideoEdits> VidEdits { get; set; }
    }

    public class VideoEdits
    {
        public string VideoGuid { get; set; }
        public int SequenceNumber { get; set; }
        public double From { get; set; }
        public double To { get; set; }
        public string HighResUrl 
        { 
            get 
            {
                return "/api/resource/getresource?id=" + VideoGuid + "&isHd=true";
            } 
        }
        public string LowResUrl
        {
            get
            {
                return "/api/resource/getresource/" + VideoGuid;
            }
        }
    }
}
