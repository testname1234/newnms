﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Http;
using System.Collections.Specialized;
using MS.Core.Entities;
using System.Net;
using System.IO;
using MS.Core.Entites;
using MS.Core.IService;
using MS.Core;

namespace MS.MediaIntegration.API
{
    public class MSApi
    {
        //IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");

        public static string postMedia = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/PostMedia";
        public static string getResource = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/GetResource";

        public static string _CreateSubBucket = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/CreateSubBucket";
        public static string _DeleteSubBucket = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/DeleteSubBucket";

        public static string _GetAllSubBucket = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/GetAllSubBucket";
        public static string _GetSubBucket = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/GetSubBucket";


        public static string _GetResourcesByResourceTypeId = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/GetResourcesByResourceTypeId";
        public static string _GetAllFiles = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/GetAllFiles";

        public static string postResource = ConfigurationManager.AppSettings["MsApi"] + "/api/Bucket/PostResource";
        public static string AddResourcemeta = ConfigurationManager.AppSettings["MsApi"] + "/api/Resource/AddResourceMeta";

        public static string searchResource = ConfigurationManager.AppSettings["MsApi"] + "/api/Resource/SearchResource";
        public static string getResourceByDate = ConfigurationManager.AppSettings["MsApi"] + "/api/Resource/GetResourceByDate";
        public static string searchCelebrity = ConfigurationManager.AppSettings["MsApi"] + "/api/Resource/SearchCelebrity";
        public static string downloadMedia = ConfigurationManager.AppSettings["MsApi"] + "/api/Resource/PostMedia";
        public static string clipAndMergeVideos = ConfigurationManager.AppSettings["MsApi"] + "/api/Resource/ClipAndMergeVideos";
        public static string postResourceAndTranscode = ConfigurationManager.AppSettings["MsApi"] + "/api/Resource/PostMediaForProcessing";

        #region Bucket Configuration
        public DataTransfer<bool> CreateSubBucket(int bucketId, string apiKey, string name)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<DataTransfer<bool>>(_CreateSubBucket + string.Format("?bucketId={0}&apiKey={1}&name={2}", bucketId, Uri.EscapeDataString(apiKey), name), null, false);
        }


        public DataTransfer<bool> DeleteSubBucket(int bucketId, string apiKey, string name)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<DataTransfer<bool>>(_DeleteSubBucket + string.Format("?bucketId={0}&apiKey={1}&name={2}", bucketId, Uri.EscapeDataString(apiKey), name), null, false);
        }


        public DataTransfer<List<Bucket>> GetAllSubBucket(int bucketId, string apiKey)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<DataTransfer<List<Bucket>>>(_GetAllSubBucket + string.Format("?bucketId={0}&apiKey={1}", bucketId, Uri.EscapeDataString(apiKey)), null, false);

        }

        public DataTransfer<bool> GetSubBucket(int bucketId, string apiKey, string name)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<DataTransfer<bool>>(_GetSubBucket + string.Format("?bucketId={0}&apiKey={1}&name={2}", bucketId, Uri.EscapeDataString(apiKey), name), null, false);

        }

        public DataTransfer<List<MediaFile>> GetAllFiles(int BucketId, string FilePath)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<DataTransfer<List<MediaFile>>>(_GetAllFiles + string.Format("?BucketId={0}&FilePath={1}", BucketId, FilePath), null, false);

        }

        public DataTransfer<List<Resource>> GetResourcesByResourceTypeId(int ResourceTypeId)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<DataTransfer<List<Resource>>>(_GetResourcesByResourceTypeId + string.Format("?ResourceTypeId={0}", ResourceTypeId), null, false);
        }

        public MS.Core.DataTransfer.Resource.ResourceOutput SearchResource(string term, int pageSize, int pageNumber)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(searchResource + string.Format("?term={0}&pageSize={1}&pageNumber={2}", term, pageSize, pageNumber), null, false);
        }

        public MS.Core.DataTransfer.Resource.ResourceOutput GetResourceByDate(DateTime ResourceDate, int pageSize, int pageNumber)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(getResourceByDate + string.Format("?term={0}&pageSize={1}&pageNumber={2}", ResourceDate, pageSize, pageNumber), null, false);
        }

        public DataTransfer<List<string>> SearchCelebrity(string term, int pageSize, int pageNumber)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<DataTransfer<List<string>>>(searchCelebrity + string.Format("?term={0}&pageSize={1}&pageNumber={2}", term, pageSize, pageNumber), null, false);
        }




        #endregion

        //public HttpResponseMessage GetResource(string FilePath)
        //{
        //    HttpWebRequestHelper helper = new HttpWebRequestHelper();
        //    return helper.GetRequest<HttpResponseMessage>(getResource + string.Format("FilePath={2}", FilePath), null, false);
        //}


        public MemoryStream GetResource(string ApiKey, int BucketId, string FilePath)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.GetRequest<MemoryStream>(@"" + getResource + string.Format("?ApiKey={0}&BucketId={1}&FilePath={2}",Uri.EscapeDataString(ApiKey), BucketId, FilePath), null, false);
        }

        public HttpStatusCode PostMedia(string ApiKey, int BucketId, string FilePath, string Source, byte[] FileBytes, string ContentType, string Id = null)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            string FileName = FilePath.Substring(FilePath.LastIndexOf('\\') + 1, (FilePath.Length) - (FilePath.LastIndexOf('\\') + 1));
            string parameters = "";
            if (!string.IsNullOrEmpty(Id))
            {
                parameters = string.Format("?ApiKey={0}&BucketId={1}&FilePath={2}&Source={3}&Id={4}", Uri.EscapeDataString(ApiKey), BucketId, FilePath, Source, Id);
            }
            else
            {
                parameters = string.Format("?ApiKey={0}&BucketId={1}&FilePath={2}&Source={3}", Uri.EscapeDataString(ApiKey), BucketId, FilePath, Source);
            }

            return helper.HttpUploadFile(@"" + postMedia + parameters, FileName, FileBytes, "file", ContentType, new NameValueCollection());
        }

        public Core.DataTransfer.Resource.PostOutput PostResource(Core.DataTransfer.Resource.PostInput input)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.PostRequest<Core.DataTransfer.Resource.PostOutput>(postResource, input, null);
        }

        public MS.Core.DataTransfer.Resource.PostInput AddResourceMeta(MS.Core.DataTransfer.Resource.PostInput input)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.PostRequest<MS.Core.DataTransfer.Resource.PostInput>(AddResourcemeta, input, null);
        }


        public HttpStatusCode PostMedia(string id, bool IsHD, string fileName, byte[] data, string paramName, string ContentType, NameValueCollection nvc)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.HttpUploadFile(downloadMedia + "?Id=" + id + "&IsHD=" + IsHD, fileName, data, paramName, ContentType, nvc);
        }

        public HttpStatusCode PostMediaAndTranscode(string id, string fileName, byte[] data, string paramName, string ContentType, NameValueCollection nvc)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.HttpUploadFile(postResourceAndTranscode + "?Id=" + id, fileName, data, paramName, ContentType, nvc);
        }

        public DataTransfer<string> ClipAndMergeVideos(MS.Core.Models.VideoClipInput input)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            //return helper.HttpUploadFile(clipAndMergeVideos, fileName, data, paramName, ContentType, nvc);
            return helper.PostRequest<DataTransfer<string>>(clipAndMergeVideos, input, null);
        }
    }
}
