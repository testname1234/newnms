﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MS.MediaIntegration
{
    public class DataTransfer<T>
    {
        public bool IsSuccess { get; set; }

        public string[] Errors { get; set; }

        public T Data { get; set; }
    }
}
