﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using MS.Core;
using MS.Core.Entities;
using MS.Core.IService;
using MS.Core.Helper;
using System.Net;
using System.Net.Http.Headers;
using MS.Core.Enums;
using NMS.Core.Helper;
using System.Reflection;
using NMS.Core.Models;
using System.Globalization;
using FFMPEGLib;
using System.Drawing;
using MS.MediaIntegration.API;
using System.Threading;
using MS.Repository;

namespace MS.MediaStorage
{
    public class Storage
    {
        #region Variables
        IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
        IResourceMetaService resourceMetaService = IoC.Resolve<IResourceMetaService>("ResourceMetaService");
        IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
        IFormatService formatService = IoC.Resolve<IFormatService>("FormatService");
        int bucketId;
        string loginId, apiKey;
        Bucket bucket = new Bucket();
        #endregion

        #region Constructors
        public Storage(int bId, string lId, string key)
        {
            bucketId = bId;
            loginId = lId;
            apiKey = key;
        }

        public Storage(int bId, string key)
        {
            Bucket requestedBucket = LocalCache.Cache.Buckets.Where(x => x.BucketId == bId).First();

            if (requestedBucket.IsParent)
            {
                bucketId = requestedBucket.BucketId;
                apiKey = key;
                bucket = bucketService.GetBucketByApiKey(bucketId, apiKey);
            }
            else
            {
                bucketId = requestedBucket.ParentId.Value;
                apiKey = key;
                bucket = bucketService.GetBucketByApiKey(bucketId, apiKey);
            }
        }

        private Storage()
        {

        }

        public bool Validate()
        {
            bucket = bucketService.GetBucketByApiKey(bucketId, apiKey);

            if (bucket != null)
                return true;
            else
                return false;
        }
        #endregion

        #region Bucket
        public bool CreateSubBucket(string bucketName,DateTime? creationDate=null,DateTime? lastUpdateDate=null)
        {
            var bucket= CreateBucket(bucketName, creationDate, lastUpdateDate);
            if (bucket != null)
                return true;

            return false;
        }

        public Bucket CreateBucket(string bucketName, DateTime? creationDate, DateTime? lastUpdateDate)
        {
            if (Validate())
            {
                string basepath = string.Empty, basePathLowRes = string.Empty, basePathThumb = string.Empty, basePathTemp = string.Empty;
                IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
                IServerService serverService = IoC.Resolve<IServerService>("ServerService");
                Server server = serverService.GetDefaultServer();
                string mediaLocation = ConfigurationManager.AppSettings["MediaLocation"].ToString();
                string mediaLocationLowRes = ConfigurationManager.AppSettings["MediaLocationLowRes"].ToString();
                string mediaLocationThumb = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
                string mediaLocationTemp = ConfigurationManager.AppSettings["MediaLocationTemp"].ToString();

                basepath = @"\\" + server.ServerIp + mediaLocation + bucket.Path + @"\";
                basePathLowRes = @"\\" + server.ServerIp + mediaLocationLowRes + bucket.Path + @"\";
                basePathThumb = @"\\" + server.ServerIp + mediaLocationThumb + bucket.Path + @"\";
                basePathTemp = @"\\" + server.ServerIp + mediaLocationTemp + bucket.Path + @"\";
                if (bucketName.Contains('\\'))
                {
                    string parentBucket = bucketName.Substring(0, bucketName.LastIndexOf("\\"));
                    if (!Directory.Exists(basepath + parentBucket))
                    {
                        var parBucket = bucketService.GetSubBucket(parentBucket, bucket.BucketId);
                        if (parBucket == null)
                        {
                            throw new Exception("Failed: Parent SubBucket Not Found, please create that first");
                        }
                    }
                }

                var childBucket = bucketService.GetSubBucket(bucketName,bucket.BucketId);

                if (childBucket == null)
                {
                    //System.Security.AccessControl.DirectorySecurity dirSec = new System.Security.AccessControl.DirectorySecurity();

                    Directory.CreateDirectory(basepath + bucketName);
                    Directory.CreateDirectory(basePathLowRes + bucketName);
                    Directory.CreateDirectory(basePathThumb + bucketName);
                    Directory.CreateDirectory(basePathTemp + bucketName);

                    Bucket subBucket = new Bucket();
                    subBucket.Name = bucketName;
                    subBucket.Path = bucketName;
                    subBucket.ParentId = bucket.BucketId;
                    subBucket.IsParent = false;
                    subBucket.CreationDate = creationDate.HasValue ? creationDate.Value : DateTime.UtcNow;
                    subBucket.LastUpdateDate = lastUpdateDate.HasValue ? lastUpdateDate.Value : subBucket.CreationDate;
                    subBucket.IsActive = true;
                    subBucket = bucketService.InsertBucket(subBucket);
                    return subBucket;
                }
                else
                {
                    return childBucket;   
                    //throw new Exception("Failed: SubBucket Already Exists");
                }
                return null;
            }
            else
            {
                throw new Exception("Failed: SubBucket creation failed, Unable to authencate the request");
            }
        }

        public bool DeleteSubBucket(string bucketName)
        {
            if (Validate())
            {

                string basepath = string.Empty, basePathLowRes = string.Empty, basePathThumb = string.Empty;
                string mediaLocation = ConfigurationManager.AppSettings["MediaLocation"].ToString();
                string mediaLocationLowRes = ConfigurationManager.AppSettings["MediaLocationLowRes"].ToString();
                string mediaLocationThumb = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
                IServerService serverService = IoC.Resolve<IServerService>("ServerService");
                Server server = serverService.GetServer(Convert.ToInt32(bucket.ServerId));
                basepath = "\\\\" + server.ServerIp + mediaLocation + bucket.Path + "\\";
                basePathLowRes = "\\\\" + server.ServerIp + mediaLocationLowRes + bucket.Path + "\\";
                basePathThumb = "\\\\" + server.ServerIp + mediaLocationThumb + bucket.Path + "\\";

                if (Directory.Exists(basepath + bucketName))
                {
                    if (!IsDirectoryEmpty(basepath + bucketName))
                    {
                        throw new Exception("Failed: Directory is not empty");
                    }
                    Bucket subBucket = bucketService.GetSubBucket(bucketName,bucket.BucketId);
                    bucketService.DeleteBucket(subBucket.BucketId);
                    Directory.Delete(basepath + bucketName);

                    if (Directory.Exists(basePathLowRes + bucketName))
                        Directory.Delete(basePathLowRes + bucketName);

                    if (Directory.Exists(basePathThumb + bucketName))
                        Directory.Delete(basePathThumb + bucketName);
                }
                else
                {
                    throw new Exception("Failed: SubBucket not found");

                }

                return true;
            }
            else
            {
                throw new Exception("Failed: SubBucket creation failed, Unable to authencate the request");
            }
        }

        public List<Bucket> GetAllSubBucket()
        {
            if (Validate())
            {
                return bucketService.GetBucketByParentId(bucket.BucketId);
            }
            else
            {
                throw new Exception("Failed: Unable to authencate the request");
            }
        }

        public Bucket GetSubBucket(string name)
        {
            if (Validate())
            {
                return bucketService.GetSubBucket(name, bucket.BucketId);
            }
            else
            {
                throw new Exception("Failed: SubBucket creation failed, Unable to authencate the request");
            }
        }

        public bool IsDirectoryEmpty(string path)
        {
            IEnumerable<string> items = Directory.EnumerateFileSystemEntries(path);
            using (IEnumerator<string> en = items.GetEnumerator())
            {
                return !en.MoveNext();
            }
        }
        #endregion

        #region Resource
        public Resource AddFile(MS.Core.DataTransfer.Resource.PostInput input)
        {
            Server server = LocalCache.Cache.Servers.Where(x => x.IsDefault).First();
            Bucket bucket = LocalCache.Cache.Buckets.Where(x => x.BucketId == input.BucketId).First();
            Bucket parentBucket = new Bucket();

            string filepath = string.Empty;
            Resource res = new Resource();
            res.CopyFrom((object)input);
            res.Guid = Guid.NewGuid();
            res.CreationDate = DateTime.UtcNow;
            res.LastUpdateDate = res.CreationDate;
            res.IsActive = true;            
            res.ServerId = server.ServerId;
            res.isHD = input.IsHd ? true : false;

            if (!bucket.IsParent)
            {
                res.BucketId = bucket.ParentId;
                res.SubBucketId = bucket.BucketId;
                parentBucket = LocalCache.Cache.Buckets.Where(x => x.BucketId == bucket.ParentId).First();
            }

            

            if (!string.IsNullOrEmpty(res.ThumbUrl))
            {
                if (res.BucketId.HasValue)
                {
                    if (server != null && bucket != null && !string.IsNullOrEmpty(res.FilePath))
                    {
                        res.FilePath = res.FilePath.Replace('/', '\\');
                        string saveFolderPath = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + bucket.Path + "\\" + res.FilePath.Substring(0, res.FilePath.LastIndexOf('\\') + 1);
                        string ext = System.IO.Path.GetExtension(res.FileName);
                        string mediaLocation = ConfigurationManager.AppSettings["MediaLocationThumb"].ToString();
                        string root = bucket.Path + "\\";
                        string destinationFileName = res.FileName;

                        byte[] data = null;
                        using (WebClient client = new WebClient())
                        {
                            data = client.DownloadData(ConfigurationManager.AppSettings["ScrapingServerIP"].ToString() + "/api/amazon/download?url=" + res.ThumbUrl);
                        }

                        if (!string.IsNullOrEmpty(ext))
                            destinationFileName = destinationFileName.Replace(ext, ".jpg");
                        else
                            destinationFileName += ".jpg";

                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream(data))
                        {
                            using (System.Drawing.Image image = System.Drawing.Image.FromStream(ms))
                            {
                                System.Drawing.Image thumbnailImage = image.GetThumbnailImage(135, 75, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
                                using (FileStream imageStream = new FileStream(saveFolderPath + destinationFileName, FileMode.Create))
                                {
                                    thumbnailImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(ext))
                            res.ThumbUrl = res.FilePath.Replace(ext, ".jpg");
                        else
                            res.ThumbUrl = res.FilePath + ".jpg";

                    }
                }
            }

            if (!string.IsNullOrEmpty(res.Source))
              res.Source = res.Source.Replace("{guid}", res.Guid.ToString());


            if (!string.IsNullOrEmpty(input.FilePath))
            {
                res.FilePath = res.FilePath.Replace("{guid}", res.Guid.ToString()).Replace('/', '\\');
                if (string.IsNullOrEmpty(res.FileName))
                {
                    if (res.FilePath.LastIndexOf('\\') >= 0)
                        res.FileName = res.FilePath.Substring(res.FilePath.LastIndexOf('\\') + 1, (res.FilePath.Length - (res.FilePath.LastIndexOf('\\') + 1)));
                    else
                        res.FileName = res.FilePath;
                }

                try
                {
                    try
                    {
                        filepath = res.FilePath.Substring(0, res.FilePath.LastIndexOf('\\'));
                        filepath = filepath.Replace('\\','/'); 
                    }
                    catch { }

                    Bucket buck = bucketService.GetBucketByPath(filepath);
                    if (buck != null)
                    {
                        res.SubBucketId = buck.BucketId;
                    }
                }
                catch (Exception exp)
                {

                }
            }
            else
            {
                string resourceFilePath = bucket.Path.Replace("\\", "/") + "/" + DateTime.Now.Year;
                CreateSubBucket(resourceFilePath);
                resourceFilePath = resourceFilePath + "/" + DateTime.Now.ToString("MMM", CultureInfo.InvariantCulture);
                CreateSubBucket(resourceFilePath);
                resourceFilePath = resourceFilePath + "/" + DateTime.Now.Day;
                CreateSubBucket(resourceFilePath);
                resourceFilePath = resourceFilePath + "/" + input.FileName;
                res.FilePath = resourceFilePath;
            }

            //if (!string.IsNullOrEmpty(res.FilePath) && res.BucketId != null && res.BucketId > 0 && !input.AllowDuplicate)
            //{
            //    var res1 = resourceService.GetResourceByFilePathAndBucketId(res.FilePath, res.BucketId.Value);
            //    if (res1 != null)
            //    {
            //        return res1;
            //    }
            //}

            res.ServerId = LocalCache.Cache.Servers.Where(x => x.IsDefault).First().ServerId;
            if (res.ResourceStatusId == 0)
                res.ResourceStatus = ResourceStatuses.Uploading;
            if (res.ResourceTypeId == 0)
                res.ResourceTypeId = (int)ExtensionHelper.GetResourceType(System.IO.Path.GetExtension(res.FileName).Trim('.'));

            
            res = resourceService.InsertResource(res);

            input.Guid = res.Guid.ToString();
            AddFileMetas(input);
            ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateLastAccessDate), (object)res);  
            return res;
        }

        private void UpdateLastAccessDate(object param)
        {
            try
            {
                Resource outerresource = param as Resource;
                Resource resource = resourceService.GetResourceByGuidId(outerresource.Guid);
                if (resource != null)
                {

                    resource.LastAccesDate = DateTime.UtcNow;
                    resource.AccessCount += 1;
                    resourceService.UpdateResource(resource);
                }
            }
            catch (Exception exp)
            {
 
            }
        }

        public static bool ThumbnailCallback()
        {
            return true;
        }

        public MS.Core.DataTransfer.Resource.PostInput AddFileMeta(MS.Core.DataTransfer.Resource.PostInput input)
        {
            AddFileMetas(input);
            return input;
        }

        public void AddFileMetasWithOutChecking(MS.Core.DataTransfer.Resource.PostInput input)
        {
            DateTime dateNow = DateTime.UtcNow;
            Resource resource = resourceService.GetResourceByGuidId(new Guid(input.Guid));
            if (resource != null)
            {
                if (!String.IsNullOrEmpty(input.Category))
                {
                    resource.Category = input.Category;
                }
                if (!String.IsNullOrEmpty(input.Location))
                {
                    resource.Location = input.Location;
                }
                if (!String.IsNullOrEmpty(input.Caption))
                {
                    resource.Caption = input.Caption;
                }
                resourceService.UpdateResourceInfo(resource);

                if (!String.IsNullOrEmpty(input.Caption))
                {
                    ResourceMeta resourcemeta = new ResourceMeta();
                    resourcemeta.ResourceGuid = new Guid(input.Guid);
                    resourcemeta.Name = "Caption";
                    resourcemeta.Value = input.Caption;
                    resourcemeta.CreationDate = dateNow;
                    resourceMetaService.InsertResourceMetaWithoutChecking(resourcemeta);
                }

                if (!String.IsNullOrEmpty(input.Category))
                {
                    ResourceMeta resourcemeta = new ResourceMeta();
                    resourcemeta.ResourceGuid = new Guid(input.Guid);
                    resourcemeta.Name = "Category";
                    resourcemeta.Value = input.Category;
                    resourcemeta.CreationDate = dateNow;
                    resourceMetaService.InsertResourceMetaWithoutChecking(resourcemeta);
                }

                if (!String.IsNullOrEmpty(input.Location))
                {
                    ResourceMeta resourcemeta = new ResourceMeta();
                    resourcemeta.ResourceGuid = new Guid(input.Guid);
                    resourcemeta.Name = "Location";
                    resourcemeta.Value = input.Location;
                    resourcemeta.CreationDate = dateNow;
                    resourceMetaService.InsertResourceMetaWithoutChecking(resourcemeta);
                }

                if (input.ResourceMeta != null)
                {
                    foreach (KeyValuePair<string, string> keyvalue in input.ResourceMeta)
                    {

                        ResourceMeta resourcemeta = new ResourceMeta();
                        resourcemeta.ResourceGuid = new Guid(input.Guid);
                        resourcemeta.Name = keyvalue.Key;
                        resourcemeta.Value = keyvalue.Value;
                        resourcemeta.CreationDate = dateNow;
                        resourceMetaService.InsertResourceMetaWithoutChecking(resourcemeta);
                    }
                }
            }
        }

        private void AddFileMetas(MS.Core.DataTransfer.Resource.PostInput input)
        {
            DateTime dateNow = DateTime.UtcNow;
            Resource resource = resourceService.GetResourceByGuidId(new Guid(input.Guid));
            if (resource != null)
            {
                if (!String.IsNullOrEmpty(input.Category))
                {
                    resource.Category = input.Category;
                }
                if (!String.IsNullOrEmpty(input.Location))
                {
                    resource.Location = input.Location;
                }
                if (!String.IsNullOrEmpty(input.Caption))
                {
                    resource.Caption = input.Caption;
                }
                resourceService.UpdateResourceInfo(resource);

                if (!String.IsNullOrEmpty(input.Caption))
                {
                    ResourceMeta resourcemeta = new ResourceMeta();
                    resourcemeta.ResourceGuid = new Guid(input.Guid);
                    resourcemeta.Name = "Caption";
                    resourcemeta.Value = input.Caption;
                    resourcemeta.CreationDate = dateNow;
                    resourceMetaService.CheckAndInsertResourceMeta(resourcemeta);
                }

                if (!String.IsNullOrEmpty(input.Category))
                {
                    ResourceMeta resourcemeta = new ResourceMeta();
                    resourcemeta.ResourceGuid = new Guid(input.Guid);
                    resourcemeta.Name = "Category";
                    resourcemeta.Value = input.Category;
                    resourcemeta.CreationDate = dateNow;
                    resourceMetaService.CheckAndInsertResourceMeta(resourcemeta);
                }

                if (!String.IsNullOrEmpty(input.Location))
                {
                    ResourceMeta resourcemeta = new ResourceMeta();
                    resourcemeta.ResourceGuid = new Guid(input.Guid);
                    resourcemeta.Name = "Location";
                    resourcemeta.Value = input.Location;
                    resourcemeta.CreationDate = dateNow;
                    resourceMetaService.CheckAndInsertResourceMeta(resourcemeta);
                }

                if (input.ResourceMeta != null)
                {
                    foreach (KeyValuePair<string, string> keyvalue in input.ResourceMeta)
                    {

                        ResourceMeta resourcemeta = new ResourceMeta();
                        resourcemeta.ResourceGuid = new Guid(input.Guid);
                        resourcemeta.Name = keyvalue.Key;
                        resourcemeta.Value = keyvalue.Value;
                        resourcemeta.CreationDate = dateNow;
                        resourceMetaService.InsertResourceMeta(resourcemeta);
                    }
                }
            }
        }

        public HttpResponseMessage GetFileById(string id, bool isHd = false, DateTimeOffset? requestHeaderDate = null, RangeHeaderValue headerRange = null)
        {
            Guid guid = new Guid();
            if (Guid.TryParse(id, out guid))
            {
                DateTime dt = DateTime.UtcNow;
                Resource resource = resourceService.GetResourceByGuidId(guid);
                if (resource != null && (resource.ResourceStatus == ResourceStatuses.Completed || (!string.IsNullOrEmpty(resource.Source) && resource.ResourceStatus == ResourceStatuses.Uploading)))
                {
                    string resourcePath = string.Empty;
                    Format format = new Format();
                    Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                    if (server != null)
                    {
                        if (isHd)
                        {
                            if (!string.IsNullOrEmpty(resource.HighResolutionFile))
                            {
                                format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.HighResolutionFormatId).First();
                                resourcePath = "\\\\" + server.ServerIp + "\\" + resource.HighResolutionFile;
                            }
                            else if (!string.IsNullOrEmpty(resource.LowResolutionFile))
                            {
                                format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.LowResolutionFormatId).First();
                                resourcePath = "\\\\" + server.ServerIp + "\\" + resource.LowResolutionFile;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(resource.LowResolutionFile))
                            {
                                format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.LowResolutionFormatId).First();
                                resourcePath = "\\\\" + server.ServerIp + "\\" + resource.LowResolutionFile;
                            }
                            else if (!string.IsNullOrEmpty(resource.HighResolutionFile))
                            {
                                format = LocalCache.Cache.Formats.Where(x => x.FormatId == resource.HighResolutionFormatId).First();
                                resourcePath = "\\\\" + server.ServerIp + "\\" + resource.HighResolutionFile;
                            }
                        }

                        if (string.IsNullOrEmpty(resourcePath))
                        {
                            resourcePath = resource.Source;
                            format = LocalCache.Cache.Formats.Where(x => x.Format == Path.GetExtension(resourcePath)).FirstOrDefault();
                        }
                        if (!string.IsNullOrEmpty(resourcePath))
                        {
                            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                            byte[] resourceByteArray;
                            bool isPartial = false;
                            RangeItemHeaderValue range = null;
                            long totalLength = 0;
                            if (requestHeaderDate.HasValue && requestHeaderDate.Value == resource.LastUpdateDate)
                                return new HttpResponseMessage(HttpStatusCode.NotModified);
                            if (headerRange != null && headerRange.Ranges != null && headerRange.Ranges.Count() > 0 && headerRange.Ranges.First().From > 0)
                            {
                                response = new HttpResponseMessage(HttpStatusCode.PartialContent);
                                range = headerRange.Ranges.First();
                                if (range.From.HasValue && range.To.HasValue)
                                {
                                    resourceByteArray = FileHelper.GetPartialFile(resourcePath, (int)range.From.Value, (int)range.To.Value + 1, out totalLength);
                                }
                                else
                                {
                                    resourceByteArray = FileHelper.GetImageFromCache(resourcePath);
                                    totalLength = resourceByteArray.Length;
                                }
                                isPartial = true;
                            }
                            else
                                resourceByteArray = FileHelper.GetImageFromCache(resourcePath);
                            MemoryStream dataStream = new MemoryStream(resourceByteArray);
                            response.Content = new StreamContent(dataStream);
                            response.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddDays(30));
                            response.Headers.CacheControl = new CacheControlHeaderValue();
                            response.Headers.CacheControl.NoCache = false;
                            response.Headers.CacheControl.MaxAge = TimeSpan.FromDays(30);
                            response.Headers.CacheControl.Public = true;
                            response.Headers.Date = resource.LastUpdateDate;
                            if (range != null)
                            {
                                response.Content.Headers.ContentRange = new ContentRangeHeaderValue(range.From.Value, range.To.HasValue ? range.To.Value : resourceByteArray.Length - 1, totalLength);
                                //response.Headers.ETag = new EntityTagHeaderValue("\"a\"");
                            }
                            response.Headers.AcceptRanges.Add("bytes");

                            if (!string.IsNullOrEmpty(resource.FileName))
                                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = resource.FileName };

                            if (format != null)
                                response.Content.Headers.ContentType = new MediaTypeHeaderValue(format.MimeType);
                            Console.WriteLine("Resource Request Time: {0}ms", (DateTime.UtcNow - dt).TotalMilliseconds);
                            return response;
                        }
                    }
                }
            }
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        public List<Resource> GetFileByCaption(string caption)
        {
            List<Resource> resources = new List<Resource>();
            if (!String.IsNullOrEmpty(caption))
            {
                resources = resourceService.GetResourceByCaption(caption);
            }
            return resources;
        }

        public List<Resource> GetFileforDuration(DateTime ResourceDate)
        {
            List<Resource> resources = new List<Resource>();
            if (ResourceDate != null)
            {
                resources = resourceService.GetResourceforDuration(ResourceDate);
            }
            return resources;
        }

        public List<Resource> GetFileByMeta(string MetaString)
        {
            List<Resource> resources = new List<Resource>();
            if (!String.IsNullOrEmpty(MetaString))
            {
                resources = resourceService.GetResourceByMeta(MetaString);
            }
            return resources;
        }

        public HttpResponseMessage DeleteFile(string id)
        {
            Guid guid = new Guid();
            if (Guid.TryParse(id, out guid))
            {
                Resource resource = resourceService.GetResourceByGuidId(guid);
                try
                {
                    if (!string.IsNullOrEmpty(resource.HighResolutionFile) && File.Exists(resource.HighResolutionFile))
                        File.Delete(resource.HighResolutionFile);
                }
                catch { }
                try
                {
                    if (!string.IsNullOrEmpty(resource.LowResolutionFile) && File.Exists(resource.LowResolutionFile))
                        File.Delete(resource.LowResolutionFile);
                }
                catch { }
                try
                {
                    if (!string.IsNullOrEmpty(resource.Source) && File.Exists(resource.Source))
                        File.Delete(resource.Source);
                }
                catch { }
                try
                {
                    if (!string.IsNullOrEmpty(resource.ThumbUrl) && File.Exists(resource.ThumbUrl))
                        File.Delete(resource.ThumbUrl);
                }
                catch { }
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        public HttpResponseMessage UpdateResourceStatus(string guid, int statusId)
        {
            Guid _guid = new Guid();
            if (Guid.TryParse(guid, out _guid))
            {
                Resource res = resourceService.GetResourceByGuidId(_guid);
                res.ResourceStatusId = statusId;
                res.LastUpdateDate = DateTime.UtcNow;
                resourceService.UpdateResource(res);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        public string GenerateSnapshot(string id)
        {
            Guid guid = new Guid();
            string assemblyFolderLocation = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            if (Guid.TryParse(id, out guid))
            {
                DateTime dt = DateTime.UtcNow;
                Resource resource = resourceService.GetResourceByGuidId(guid);
                if (resource != null && string.IsNullOrEmpty(resource.ThumbUrl))
                {
                    string resourcePath = string.Empty;
                    Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                    if (server != null)
                    {
                        string root = "\\" + ConfigurationManager.AppSettings["MediaLocation"] + "\\Thumbs";
                        if (!Directory.Exists("\\\\" + server.ServerIp + root))
                        {
                            Directory.CreateDirectory("\\\\" + server.ServerIp + root);
                        }
                        if (ResourceHelper.GenerateSnapshot(assemblyFolderLocation + "\\ffmpeg\\", resource.Source, "\\\\" + server.ServerIp + root + "\\", resource.Guid.ToString() + ".jpg", (ResourceTypes)resource.ResourceTypeId))
                        {
                            resource.ThumbUrl = root + "\\" + resource.Guid.ToString() + ".jpg";
                            resourceService.UpdateResource(resource);
                            return "Snapshot generated successfully";
                        }
                        else return "Snapshot not supported for this " + ((ResourceTypes)resource.ResourceTypeId).ToString() + " type";
                    }
                }
            }
            return "No Resource Found";
        }

        public string GenerateCustomSnapshot(CustomSnapshotInput input)
        {

            Guid guid = new Guid();
            string assemblyFolderLocation = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            if (Guid.TryParse(input.Id, out guid))
            {
                DateTime dt = DateTime.UtcNow;
                Resource resource = resourceService.GetResourceByGuidId(guid);
                if (resource != null && string.IsNullOrEmpty(resource.ThumbUrl))
                {
                    string resourcePath = string.Empty;
                    Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                    if (server != null)
                    {
                        string root = "\\" + ConfigurationManager.AppSettings["MediaLocation"] + "\\Thumbs";
                        if (!Directory.Exists("\\\\" + server.ServerIp + root))
                        {
                            Directory.CreateDirectory("\\\\" + server.ServerIp + root);
                        }
                        if (ResourceHelper.GenerateSnapshot(assemblyFolderLocation + "\\ffmpeg\\", input.FilePath, "\\\\" + server.ServerIp + root + "\\", resource.Guid.ToString() + ".jpg", (ResourceTypes)resource.ResourceTypeId, TimeSpan.FromSeconds(input.TimeSpan)))
                        {
                            resource.ThumbUrl = root + "\\" + resource.Guid.ToString() + ".jpg";
                            resourceService.UpdateResource(resource);
                            return "Snapshot generated successfully";
                        }
                        else return "Snapshot not supported for this " + ((ResourceTypes)resource.ResourceTypeId).ToString() + " type";
                    }
                }
            }
            return "No Resource Found";

        }

        public Resource SaveResource(Resource resource, Format format, string mimeType, string ext, string from, string to, string root, bool IsHD, string serverIp)
        {
            if (format == null)
            {
                CatchException(new Exception("Save Resource 2"));
                format = new Format();
                format.Format = ext;
                format.MimeType = mimeType;
                format = formatService.InsertFormat(format);
                LocalCache.Cache.UpdateFormats();
            }
            if (IsHD)
            {
                CatchException(new Exception("Save Resource 3"));
                resource.HighResolutionFile = resource.FilePath;
                resource.HighResolutionFormatId = format.FormatId;
            }
            else
            {
                resource.LowResolutionFile = resource.FilePath;
                resource.LowResolutionFormatId = format.FormatId;
            }
            if (from != null && to != null)
            {
                if (File.Exists(to))
                {
                    File.Delete(to);
                }

                File.Move(from, to);
            }
            resource.LastUpdateDate = DateTime.UtcNow;
            resource.ResourceStatus = ResourceStatuses.Completed;
            if (string.IsNullOrEmpty(resource.ThumbUrl))
            {
                try
                {
                    string assemblyFolderLocation = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
                    if (ResourceHelper.GenerateSnapshot(assemblyFolderLocation + "\\ffmpeg\\", "\\\\" + serverIp + ConfigurationManager.AppSettings["MediaLocation"] + root + "\\" + resource.FileName, "\\\\" + serverIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + root, resource.FileName, (ResourceTypes)resource.ResourceTypeId))
                    {
                        resource.ThumbUrl = resource.FilePath;
                    }
                    if ((ResourceTypes)resource.ResourceTypeId == ResourceTypes.Video || (ResourceTypes)resource.ResourceTypeId == ResourceTypes.Audio)
                    {
                        resource.Duration = FFMPEG.GetDurationInMilliSeconds(to + "\\" + resource.HighResolutionFile);
                    }
                }
                catch (Exception ex)
                {
                    CatchException(ex);

                }
            }
            return resourceService.UpdateResource(resource);
        }

        private static void CatchException(Exception ex)
        {
            try
            {
                string path = "c:\\UploadErrorLog.txt";
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                }
                using (StreamWriter w = File.AppendText(path))
                {
                    w.WriteLine("\r\nLog Entry : ");
                    w.WriteLine("{0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    string err = "Error in: " +
                                  ". Error Message:" + ex.ToString();
                    w.WriteLine(err);
                    w.WriteLine("__________________________");
                    w.Flush();
                    w.Close();
                }
            }
            catch (Exception ex1)
            {
                // WriteLogError(ex.Message);
            }
        }

        public string CropAndInsertImage(MS.Core.Models.CropImageInput input)
        {
            List<Point> points = new List<Point>();
            Resource cropedResource = new Resource();
            foreach (var p in input.Points)
                points.Add(new Point(p.X, p.Y));
            string fileName = "CroppedImage" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".jpg";
            string downloadFileName = ConfigurationManager.AppSettings["TempSavePath"] + fileName;

            using (WebClient wb = new WebClient())
            {
                wb.DownloadFile(input.ImageUrl, downloadFileName);
            }

            using (Bitmap cropImage = CropImage(downloadFileName, points))
            {
                MS.Core.DataTransfer.Resource.PostInput resourceInput = new Core.DataTransfer.Resource.PostInput();
                resourceInput.BucketId = input.BucketId;
                resourceInput.ApiKey = input.ApiKey;
                resourceInput.FileName = fileName;
                resourceInput.FilePath = "/CropImages/" + fileName;
                resourceInput.Source = input.ImageUrl;
                resourceInput.AllowDuplicate = true;
                cropedResource = AddFile(resourceInput);

                Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == cropedResource.ServerId.Value).First();
                Bucket bucket = bucketService.GetBucket(cropedResource.BucketId.Value);
                Format format = LocalCache.Cache.Formats.Where(x => x.FormatId == 1).First();
                string ext = System.IO.Path.GetExtension(cropedResource.FileName);
                cropedResource.FilePath = cropedResource.FilePath.Replace('/', '\\');
                string sourcePath = "\\\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocation"] + bucket.Path + cropedResource.FilePath;
                string sourceThumbPath = "\\\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + bucket.Path + cropedResource.FilePath;
                string folderPath = sourceThumbPath.Substring(0, sourceThumbPath.LastIndexOf('\\') + 1);
                fileName = sourceThumbPath.Replace(folderPath, string.Empty);


                cropImage.Save(sourcePath);
                cropedResource.HighResolutionFile = cropedResource.FilePath;
                cropedResource.HighResolutionFormatId = format.FormatId;

                string assemblyFolderLocation = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
                if (ResourceHelper.GenerateSnapshot(assemblyFolderLocation + "\\ffmpeg\\", sourcePath, folderPath, fileName, ResourceTypes.Image))
                {
                    cropedResource.ThumbUrl = cropedResource.FilePath;
                    resourceService.UpdateResource(cropedResource);
                }
            }

            if (System.IO.File.Exists(downloadFileName))
                System.IO.File.Delete(downloadFileName);
            //MSApi api = new MSApi();

            //byte[] fileData = ImageToByte(cropImage);
            //api.PostMedia(cropedResource.Guid.ToString(), true, downloadFileName, fileData, "file", "image/jpeg", new System.Collections.Specialized.NameValueCollection());

            return cropedResource.Guid.ToString();

        }

        public string AddVideoClipTask(MS.Core.Models.VideoClipInput input)
        {
            IVideoClipTaskService vcService = IoC.Resolve<IVideoClipTaskService>("VideoClipTaskService");

            VideoClipTask task = new VideoClipTask();
            task.ResourceGuid = input.Guid;
            task.StatusId = (int)MS.Core.Enums.VideoClipTaskStatus.Created;
            task.InputObjectJson = JSONHelper.GetString(input);
            task.CreationDate = DateTime.UtcNow;
            task.LastUpdateDate = task.CreationDate;
            vcService.InsertVideoClipTask(task);
            return "Success";
        }

        public string ClipAndMergeVideos(MS.Core.Models.VideoClipInput input)
        {
            WebClient wb = new WebClient();
            string mediaServerUrl = ConfigurationManager.AppSettings["MsApi"];
            string tempSavePath = ConfigurationManager.AppSettings["TempSavePath"];
            List<string> clipUrls = new List<string>();

            int totalCount = input.VidEdits.Count();

            for (int i = 0; i < totalCount; i++)
            {
                MS.Core.Models.VideoEdits vEdit = input.VidEdits.Where(x => x.SequenceNumber == (i + 1)).ToList().First();
                string fileName = "video" + vEdit.VideoGuid + (i + 1) + ".mp4";
                string savePath = tempSavePath + fileName;
                string downloadPath = tempSavePath + vEdit.VideoGuid;
                wb.DownloadFile(mediaServerUrl + vEdit.HighResUrl, downloadPath);
                TimeSpan from = TimeSpan.FromSeconds(vEdit.From);
                TimeSpan to = TimeSpan.FromSeconds(vEdit.To);

                FFMPEG.ClipVideo(downloadPath, from, to, savePath);
                System.IO.File.Delete(downloadPath);
                clipUrls.Add(savePath);
            }

            Guid finalFileName = Guid.NewGuid();
            string finalVideoPath = tempSavePath + @"\" + finalFileName.ToString() + ".mp4";
            FFMPEG.ConcatenateVideos(clipUrls, finalVideoPath);

            Resource resource = resourceService.GetResourceByGuidId(new Guid(input.Guid));
            Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
            Server server = LocalCache.Cache.Servers.Where(x=>x.ServerId == resource.ServerId.Value).First();

            string resourceHighResPath = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocation"] + bucket.Path + @"\" + resource.FilePath;
            string resourceLowResPath = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationLowRes"] + bucket.Path + @"\" + resource.FilePath;
            System.IO.File.Move(resourceHighResPath, resourceLowResPath);
            System.IO.File.Copy(finalVideoPath, resourceHighResPath, true);

            resource.HighResolutionFile = resource.FilePath;
            resource.LowResolutionFile = resource.FilePath;
            resource.LowResolutionFormatId = resource.HighResolutionFormatId;
            resourceService.UpdateResource(resource);

            System.IO.File.Delete(finalVideoPath);
            foreach (string url in clipUrls)
                System.IO.File.Delete(url);
 
            return "Success";
        }
        #endregion

        public Bitmap CropImage(string imageUrl, List<Point> polygonPoints)
        {
            using (Bitmap Source = new Bitmap(imageUrl))
            {
                using (Graphics Ga = Graphics.FromImage(Source))
                {
                    SolidBrush Brush = new SolidBrush(Color.FromArgb(1, 1, 1));

                    var graphicPath = new System.Drawing.Drawing2D.GraphicsPath();
                    graphicPath.AddPolygon(polygonPoints.ToArray());
                    Region region = new Region();
                    region.Exclude(graphicPath);
                    Ga.FillRegion(Brush, region);
                    int minX = polygonPoints.Select(x => x.X).Min();
                    int maxX = polygonPoints.Select(x => x.X).Max();
                    int minY = polygonPoints.Select(x => x.Y).Min();
                    int maxY = polygonPoints.Select(x => x.Y).Max();
                    Rectangle rectcrop = new Rectangle(minX, minY, maxX - minX, maxY - minY);

                    return Source.Clone(rectcrop, Source.PixelFormat);
                }
            }
        }

        public byte[] ImageToByte(Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }


        public string GenerateSnapShot(int subBucketId,string filePath,string fileName,int resourceTypeId)
        {
               string url = "";
             
            try
            {
                IServerService serverService = IoC.Resolve<IServerService>("ServerService");
                BucketRepository bucketRepository = new BucketRepository();
                var subBucket = bucketRepository.GetBucket(subBucketId);

                DateTime dt = DateTime.UtcNow;
                string resourcePath = string.Empty;
                Server server = serverService.GetDefaultServer();
                if (server != null)
                {
                    string ext = System.IO.Path.GetExtension(filePath);

                    string folderPath =subBucket.BucketId==bucket.BucketId?string.Empty: subBucket.Path;
                    string root = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + bucket.Path + folderPath + "\\";
                    string saveFileName = fileName.Replace(ext, ".jpg");


                    if (ResourceHelper.GenerateSnapshot(AppDomain.CurrentDomain.BaseDirectory + "\\ffmpeg\\", filePath, root, saveFileName, ((ResourceTypes)resourceTypeId)))
                    {
                        if (File.Exists(root + saveFileName))
                        {
                            url = folderPath + "/" + saveFileName;
                        }
                    }

                }
            }
            catch
            { 
            
            }

            return url;
                
        }
        
    }
}
