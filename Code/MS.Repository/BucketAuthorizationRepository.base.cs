﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class BucketAuthorizationRepositoryBase : Repository, IBucketAuthorizationRepositoryBase 
	{
        
        public BucketAuthorizationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("BucketAuthorizationId",new SearchColumn(){Name="BucketAuthorizationId",Title="BucketAuthorizationId",SelectClause="BucketAuthorizationId",WhereClause="AllRecords.BucketAuthorizationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BucketAuthorizationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BucketId",new SearchColumn(){Name="BucketId",Title="BucketId",SelectClause="BucketId",WhereClause="AllRecords.BucketId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="BucketId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsRead",new SearchColumn(){Name="IsRead",Title="IsRead",SelectClause="IsRead",WhereClause="AllRecords.IsRead",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsRead",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsWrite",new SearchColumn(){Name="IsWrite",Title="IsWrite",SelectClause="IsWrite",WhereClause="AllRecords.IsWrite",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsWrite",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetBucketAuthorizationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetBucketAuthorizationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetBucketAuthorizationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetBucketAuthorizationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[BucketAuthorization].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[BucketAuthorization].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual BucketAuthorization GetBucketAuthorization(System.Int32 BucketAuthorizationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBucketAuthorizationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BucketAuthorization] with (nolock)  where BucketAuthorizationId=@BucketAuthorizationId ";
			SqlParameter parameter=new SqlParameter("@BucketAuthorizationId",BucketAuthorizationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return BucketAuthorizationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<BucketAuthorization> GetBucketAuthorizationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBucketAuthorizationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [BucketAuthorization] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BucketAuthorization>(ds,BucketAuthorizationFromDataRow);
		}

		public virtual List<BucketAuthorization> GetAllBucketAuthorization(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBucketAuthorizationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BucketAuthorization] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BucketAuthorization>(ds, BucketAuthorizationFromDataRow);
		}

		public virtual List<BucketAuthorization> GetPagedBucketAuthorization(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetBucketAuthorizationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [BucketAuthorizationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([BucketAuthorizationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [BucketAuthorizationId] ";
            tempsql += " FROM [BucketAuthorization] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("BucketAuthorizationId"))
					tempsql += " , (AllRecords.[BucketAuthorizationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[BucketAuthorizationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetBucketAuthorizationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [BucketAuthorization] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [BucketAuthorization].[BucketAuthorizationId] = PageIndex.[BucketAuthorizationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BucketAuthorization>(ds, BucketAuthorizationFromDataRow);
			}else{ return null;}
		}

		private int GetBucketAuthorizationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM BucketAuthorization as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM BucketAuthorization as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(BucketAuthorization))]
		public virtual BucketAuthorization InsertBucketAuthorization(BucketAuthorization entity)
		{

			BucketAuthorization other=new BucketAuthorization();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into BucketAuthorization ( [UserId]
				,[BucketId]
				,[IsRead]
				,[IsWrite] )
				Values
				( @UserId
				, @BucketId
				, @IsRead
				, @IsWrite );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@BucketId",entity.BucketId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRead",entity.IsRead ?? (object)DBNull.Value)
					, new SqlParameter("@IsWrite",entity.IsWrite ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetBucketAuthorization(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(BucketAuthorization))]
		public virtual BucketAuthorization UpdateBucketAuthorization(BucketAuthorization entity)
		{

			if (entity.IsTransient()) return entity;
			BucketAuthorization other = GetBucketAuthorization(entity.BucketAuthorizationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update BucketAuthorization set  [UserId]=@UserId
							, [BucketId]=@BucketId
							, [IsRead]=@IsRead
							, [IsWrite]=@IsWrite 
							 where BucketAuthorizationId=@BucketAuthorizationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@BucketId",entity.BucketId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRead",entity.IsRead ?? (object)DBNull.Value)
					, new SqlParameter("@IsWrite",entity.IsWrite ?? (object)DBNull.Value)
					, new SqlParameter("@BucketAuthorizationId",entity.BucketAuthorizationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetBucketAuthorization(entity.BucketAuthorizationId);
		}

		public virtual bool DeleteBucketAuthorization(System.Int32 BucketAuthorizationId)
		{

			string sql="delete from BucketAuthorization where BucketAuthorizationId=@BucketAuthorizationId";
			SqlParameter parameter=new SqlParameter("@BucketAuthorizationId",BucketAuthorizationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(BucketAuthorization))]
		public virtual BucketAuthorization DeleteBucketAuthorization(BucketAuthorization entity)
		{
			this.DeleteBucketAuthorization(entity.BucketAuthorizationId);
			return entity;
		}


		public virtual BucketAuthorization BucketAuthorizationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			BucketAuthorization entity=new BucketAuthorization();
			if (dr.Table.Columns.Contains("BucketAuthorizationId"))
			{
			entity.BucketAuthorizationId = (System.Int32)dr["BucketAuthorizationId"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = dr["UserId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("BucketId"))
			{
			entity.BucketId = dr["BucketId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["BucketId"];
			}
			if (dr.Table.Columns.Contains("IsRead"))
			{
			entity.IsRead = dr["IsRead"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsRead"];
			}
			if (dr.Table.Columns.Contains("IsWrite"))
			{
			entity.IsWrite = dr["IsWrite"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsWrite"];
			}
			return entity;
		}

	}
	
	
}
