﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class BucketCredentialsRepositoryBase : Repository, IBucketCredentialsRepositoryBase 
	{
        
        public BucketCredentialsRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("BucketCredentialId",new SearchColumn(){Name="BucketCredentialId",Title="BucketCredentialId",SelectClause="BucketCredentialId",WhereClause="AllRecords.BucketCredentialId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BucketCredentialId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LoginId",new SearchColumn(){Name="LoginId",Title="LoginId",SelectClause="LoginId",WhereClause="AllRecords.LoginId",DataType="System.String",IsForeignColumn=false,PropertyName="LoginId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ApiKey",new SearchColumn(){Name="ApiKey",Title="ApiKey",SelectClause="ApiKey",WhereClause="AllRecords.ApiKey",DataType="System.String",IsForeignColumn=false,PropertyName="ApiKey",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BucketId",new SearchColumn(){Name="BucketId",Title="BucketId",SelectClause="BucketId",WhereClause="AllRecords.BucketId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BucketId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetBucketCredentialsSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetBucketCredentialsBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetBucketCredentialsAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetBucketCredentialsSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[BucketCredentials].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[BucketCredentials].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<BucketCredentials> GetBucketCredentialsByBucketId(System.Int32 BucketId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBucketCredentialsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BucketCredentials] with (nolock)  where BucketId=@BucketId  ";
			SqlParameter parameter=new SqlParameter("@BucketId",BucketId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BucketCredentials>(ds,BucketCredentialsFromDataRow);
		}

		public virtual BucketCredentials GetBucketCredentials(System.Int32 BucketCredentialId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBucketCredentialsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BucketCredentials] with (nolock)  where BucketCredentialId=@BucketCredentialId ";
			SqlParameter parameter=new SqlParameter("@BucketCredentialId",BucketCredentialId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return BucketCredentialsFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<BucketCredentials> GetBucketCredentialsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBucketCredentialsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [BucketCredentials] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BucketCredentials>(ds,BucketCredentialsFromDataRow);
		}

		public virtual List<BucketCredentials> GetAllBucketCredentials(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBucketCredentialsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BucketCredentials] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BucketCredentials>(ds, BucketCredentialsFromDataRow);
		}

		public virtual List<BucketCredentials> GetPagedBucketCredentials(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetBucketCredentialsCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [BucketCredentialId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([BucketCredentialId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [BucketCredentialId] ";
            tempsql += " FROM [BucketCredentials] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("BucketCredentialId"))
					tempsql += " , (AllRecords.[BucketCredentialId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[BucketCredentialId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetBucketCredentialsSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [BucketCredentials] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [BucketCredentials].[BucketCredentialId] = PageIndex.[BucketCredentialId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BucketCredentials>(ds, BucketCredentialsFromDataRow);
			}else{ return null;}
		}

		private int GetBucketCredentialsCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM BucketCredentials as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM BucketCredentials as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(BucketCredentials))]
		public virtual BucketCredentials InsertBucketCredentials(BucketCredentials entity)
		{

			BucketCredentials other=new BucketCredentials();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into BucketCredentials ( [LoginId]
				,[ApiKey]
				,[BucketId] )
				Values
				( @LoginId
				, @ApiKey
				, @BucketId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@LoginId",entity.LoginId)
					, new SqlParameter("@ApiKey",entity.ApiKey)
					, new SqlParameter("@BucketId",entity.BucketId)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetBucketCredentials(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(BucketCredentials))]
		public virtual BucketCredentials UpdateBucketCredentials(BucketCredentials entity)
		{

			if (entity.IsTransient()) return entity;
			BucketCredentials other = GetBucketCredentials(entity.BucketCredentialId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update BucketCredentials set  [LoginId]=@LoginId
							, [ApiKey]=@ApiKey
							, [BucketId]=@BucketId 
							 where BucketCredentialId=@BucketCredentialId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@LoginId",entity.LoginId)
					, new SqlParameter("@ApiKey",entity.ApiKey)
					, new SqlParameter("@BucketId",entity.BucketId)
					, new SqlParameter("@BucketCredentialId",entity.BucketCredentialId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetBucketCredentials(entity.BucketCredentialId);
		}

		public virtual bool DeleteBucketCredentials(System.Int32 BucketCredentialId)
		{

			string sql="delete from BucketCredentials where BucketCredentialId=@BucketCredentialId";
			SqlParameter parameter=new SqlParameter("@BucketCredentialId",BucketCredentialId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(BucketCredentials))]
		public virtual BucketCredentials DeleteBucketCredentials(BucketCredentials entity)
		{
			this.DeleteBucketCredentials(entity.BucketCredentialId);
			return entity;
		}


		public virtual BucketCredentials BucketCredentialsFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			BucketCredentials entity=new BucketCredentials();
			if (dr.Table.Columns.Contains("BucketCredentialId"))
			{
			entity.BucketCredentialId = (System.Int32)dr["BucketCredentialId"];
			}
			if (dr.Table.Columns.Contains("LoginId"))
			{
			entity.LoginId = dr["LoginId"].ToString();
			}
			if (dr.Table.Columns.Contains("ApiKey"))
			{
			entity.ApiKey = dr["ApiKey"].ToString();
			}
			if (dr.Table.Columns.Contains("BucketId"))
			{
			entity.BucketId = (System.Int32)dr["BucketId"];
			}
			return entity;
		}

	}
	
	
}
