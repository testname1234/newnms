﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace MS.Repository
{
		
	public partial class BucketCredentialsRepository: BucketCredentialsRepositoryBase, IBucketCredentialsRepository
	{
        public BucketCredentials GetBucketCredentialsByApiKey(int bucketId, string apiKey)
        {

            string sql = GetBucketCredentialsSelectClause();
            sql += "from [BucketCredentials] with (nolock)  where BucketId=@BucketId and ApiKey = @ApiKey";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BucketId", bucketId)
					, new SqlParameter("@ApiKey", apiKey)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return BucketCredentialsFromDataRow(ds.Tables[0].Rows[0]);

        }


        public BucketCredentials GetBucketByLoginIdAndApiKey(string loginid, string apikey)
        {
            string sql = GetBucketCredentialsSelectClause();
            sql += "from [BucketCredentials] with (nolock)  where LoginId=@LoginId and ApiKey = @ApiKey";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@LoginId", loginid)
					, new SqlParameter("@ApiKey", apikey)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return BucketCredentialsFromDataRow(ds.Tables[0].Rows[0]);
        }
    }
	
	
}
