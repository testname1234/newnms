﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace MS.Repository
{
		
	public partial class BucketRepository: BucketRepositoryBase, IBucketRepository
	{
        public Bucket GetBucketByApiKey(int bucketId, string apiKey)
        {

            string sql = "select b.BucketId,b.Name,b.Path,b.AutoSync,b.Size,b.ServerId,b.LastSyncDate,b.FolderUpdateDate,b.IsParent,b.ParentId,b.CreationDate,b.LastUpdateDate,b.IsActive ";
            sql += "from [Bucket] b with (nolock) inner join [BucketCredentials] bc with (nolock) on b.BucketId = bc.BucketId where b.BucketId=@BucketId and bc.ApiKey = @ApiKey";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BucketId", bucketId)
					, new SqlParameter("@ApiKey", apiKey)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return BucketFromDataRow(ds.Tables[0].Rows[0]);

        }

        public List<Bucket> GetAllBucketWithParentName(int userId)
        {
            string sql = @"select parentid,bucketid, name, isparent, 
(select b1.path + b.Name from bucket b1 where b.ParentId = b1.BucketId) as path ,
(select bc.apikey from bucketcredentials bc where bc.bucketid=b.bucketid) as apikey,
(select ba.isread from BucketAuthorization ba where ba.bucketid=b.bucketid and ba.userid=@UserId) as IsRead,
(select ba.iswrite from BucketAuthorization ba where ba.bucketid=b.bucketid and ba.userid=@UserId) as IsWrite
from Bucket b 
";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { new SqlParameter("@UserId", userId) });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bucket>(ds, BucketFromDataRow);
        }

        public List<Bucket> GetBucketByParentId(int parentId)
        {
            string sql = GetBucketSelectClause();
            sql += "from [Bucket] with (nolock)  where ParentId=@ParentId ";
            SqlParameter parameter = new SqlParameter("@ParentId", parentId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bucket>(ds, BucketFromDataRow);
        }

        public Bucket GetSubBucket(string name, int parentId)
        {
            string sql = GetBucketSelectClause();
            sql += "from [Bucket] with (nolock)  where Name=@Name and ParentId=@ParentId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Name", name)
					, new SqlParameter("@ParentId", parentId)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return BucketFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Bucket GetBucketByName(string name)
        {
            string sql =  GetBucketSelectClause();
            sql += "from [Bucket] with (nolock)  where Name=@Name ";
            SqlParameter parameter = new SqlParameter("@Name", name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return BucketFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Bucket GetBucketByPath(string path)
        {
            string sql = GetBucketSelectClause();
            sql += "from [Bucket] with (nolock)  where Path=@Path ";
            SqlParameter parameter = new SqlParameter("@Path", path);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return BucketFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Bucket GetBucketAndCredentials(int bucketId)
        {
            string sql = "select b.BucketId,b.Name,b.Path,b.AutoSync,b.Size,b.ServerId,b.LastSyncDate,b.FolderUpdateDate,b.IsParent,b.ParentId,b.CreationDate,b.LastUpdateDate,b.IsActive,bc.ApiKey ";
            sql += "from Bucket b with (nolock) Inner Join BucketCredentials bc with (nolock) on b.BucketId = bc.BucketId where b.BucketId=@bucketId ";
            SqlParameter parameter = new SqlParameter("@bucketId", bucketId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return BucketFromDataRow(ds.Tables[0].Rows[0]);
        }

        public override Bucket BucketFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            Bucket entity = base.BucketFromDataRow(dr);            

            if (dr.Table.Columns.Contains("ApiKey"))
            {
                entity.ApiKey = dr["ApiKey"].ToString();
            }
            if (dr.Table.Columns.Contains("IsRead"))
            {
                entity.IsRead = dr["IsRead"] != DBNull.Value ? (bool?)dr["IsRead"] : null;
            }
            if (dr.Table.Columns.Contains("IsWrite"))
            {
                entity.IsWrite = dr["IsWrite"] != DBNull.Value ? (bool?)dr["IsWrite"] : null;
            }
            
            return entity;
        }


        public void MarkResourceIfBucketPrivate(int? nullable, int p)
        {
           
        }


        public List<Core.Models.ResourceModel> GetSubBucketResources(int bucketId)
        {
            string sql = @"select sourcename,source,serverid,bucketid,guid,lowresolutionfile,highresolutionfile,thumburl,caption,resourcetypeid,duration,creationdate from resource where subBucketId=@bucketId and (resourcestatusid = 4 or resourcestatusid = 11)
order by creationdate desc ";
            SqlParameter parameter = new SqlParameter("@bucketId", bucketId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            List<Core.Models.ResourceModel> resources = new List<Core.Models.ResourceModel>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var resource = new Core.Models.ResourceModel();
                resource.ServerId = row["serverid"] != DBNull.Value ? (int?)row["serverid"] : null;
                resource.BucketId = row["bucketid"] != DBNull.Value ? (int?)row["bucketid"] : null;
                resource.DownloadHighRes = row["highresolutionfile"] != DBNull.Value ? row["highresolutionfile"].ToString() : null;
                resource.DownloadLowRes = row["lowresolutionfile"] != DBNull.Value ? row["lowresolutionfile"].ToString() : null;
                resource.ThumbUrl = row["thumburl"] != DBNull.Value ? row["thumburl"].ToString() : null;
                resource.Title = row["caption"] != DBNull.Value ? row["caption"].ToString() : null;
                resource.Guid = row["guid"] != DBNull.Value ? row["guid"].ToString() : null;
                resource.Duration = row["duration"] != DBNull.Value ? (double?)row["duration"] : null;
                resource.Source = row["source"] != DBNull.Value ? row["source"].ToString() : null;
                resource.SourceName = row["sourcename"] != DBNull.Value ? row["sourcename"].ToString() : null;
                resource.ResourceType = row["resourcetypeid"] != DBNull.Value ? (int?)(row["resourcetypeid"]) : null;
                resource.CreationDate = row["creationdate"] != DBNull.Value ? (DateTime?)(row["creationdate"]) : null;
                resources.Add(resource);
            }
            return resources;

        }

        public List<Bucket> GetBucketsToTranscode()
        {
            string sql = GetBucketSelectClause();
            sql += "from [Bucket] with (nolock)  where AllowTranscode=1";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bucket>(ds, BucketFromDataRow);
        }


        public List<Bucket> GetBucketsByParentId(int parentId)
        {
            string sql = GetBucketSelectClause();
            sql += "from [Bucket] with (nolock)  where ParentId=@ParentId";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { new SqlParameter("@ParentId",parentId) });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bucket>(ds, BucketFromDataRow);
        }
    }
	
	
}
