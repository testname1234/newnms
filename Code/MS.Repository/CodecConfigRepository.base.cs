﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class CodecConfigRepositoryBase : Repository, ICodecConfigRepositoryBase 
	{
        
        public CodecConfigRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CodecConfigId",new SearchColumn(){Name="CodecConfigId",Title="CodecConfigId",SelectClause="CodecConfigId",WhereClause="AllRecords.CodecConfigId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CodecConfigId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImageLowResWidth",new SearchColumn(){Name="ImageLowResWidth",Title="ImageLowResWidth",SelectClause="ImageLowResWidth",WhereClause="AllRecords.ImageLowResWidth",DataType="System.Int32",IsForeignColumn=false,PropertyName="ImageLowResWidth",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImageLowResHeight",new SearchColumn(){Name="ImageLowResHeight",Title="ImageLowResHeight",SelectClause="ImageLowResHeight",WhereClause="AllRecords.ImageLowResHeight",DataType="System.Int32",IsForeignColumn=false,PropertyName="ImageLowResHeight",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoLowResCodec",new SearchColumn(){Name="VideoLowResCodec",Title="VideoLowResCodec",SelectClause="VideoLowResCodec",WhereClause="AllRecords.VideoLowResCodec",DataType="System.String",IsForeignColumn=false,PropertyName="VideoLowResCodec",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoHighResCodec",new SearchColumn(){Name="VideoHighResCodec",Title="VideoHighResCodec",SelectClause="VideoHighResCodec",WhereClause="AllRecords.VideoHighResCodec",DataType="System.String",IsForeignColumn=false,PropertyName="VideoHighResCodec",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCodecConfigSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCodecConfigBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCodecConfigAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCodecConfigSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "CodecConfig."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",CodecConfig."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual CodecConfig GetCodecConfig(System.Int32 CodecConfigId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCodecConfigSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from CodecConfig with (nolock)  where CodecConfigId=@CodecConfigId ";
			SqlParameter parameter=new SqlParameter("@CodecConfigId",CodecConfigId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CodecConfigFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CodecConfig> GetCodecConfigByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCodecConfigSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from CodecConfig with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CodecConfig>(ds,CodecConfigFromDataRow);
		}

		public virtual List<CodecConfig> GetAllCodecConfig(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCodecConfigSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from CodecConfig with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CodecConfig>(ds, CodecConfigFromDataRow);
		}

		public virtual List<CodecConfig> GetPagedCodecConfig(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCodecConfigCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CodecConfigId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CodecConfigId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CodecConfigId] ";
            tempsql += " FROM [CodecConfig] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CodecConfigId"))
					tempsql += " , (AllRecords.[CodecConfigId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CodecConfigId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCodecConfigSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CodecConfig] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CodecConfig].[CodecConfigId] = PageIndex.[CodecConfigId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CodecConfig>(ds, CodecConfigFromDataRow);
			}else{ return null;}
		}

		private int GetCodecConfigCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CodecConfig as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CodecConfig as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CodecConfig))]
		public virtual CodecConfig InsertCodecConfig(CodecConfig entity)
		{

			CodecConfig other=new CodecConfig();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CodecConfig ( [ImageLowResWidth]
				,[ImageLowResHeight]
				,[VideoLowResCodec]
				,[VideoHighResCodec] )
				Values
				( @ImageLowResWidth
				, @ImageLowResHeight
				, @VideoLowResCodec
				, @VideoHighResCodec );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ImageLowResWidth",entity.ImageLowResWidth)
					, new SqlParameter("@ImageLowResHeight",entity.ImageLowResHeight)
					, new SqlParameter("@VideoLowResCodec",entity.VideoLowResCodec)
					, new SqlParameter("@VideoHighResCodec",entity.VideoHighResCodec)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCodecConfig(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CodecConfig))]
		public virtual CodecConfig UpdateCodecConfig(CodecConfig entity)
		{

			if (entity.IsTransient()) return entity;
			CodecConfig other = GetCodecConfig(entity.CodecConfigId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CodecConfig set  [ImageLowResWidth]=@ImageLowResWidth
							, [ImageLowResHeight]=@ImageLowResHeight
							, [VideoLowResCodec]=@VideoLowResCodec
							, [VideoHighResCodec]=@VideoHighResCodec 
							 where CodecConfigId=@CodecConfigId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ImageLowResWidth",entity.ImageLowResWidth)
					, new SqlParameter("@ImageLowResHeight",entity.ImageLowResHeight)
					, new SqlParameter("@VideoLowResCodec",entity.VideoLowResCodec)
					, new SqlParameter("@VideoHighResCodec",entity.VideoHighResCodec)
					, new SqlParameter("@CodecConfigId",entity.CodecConfigId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCodecConfig(entity.CodecConfigId);
		}

		public virtual bool DeleteCodecConfig(System.Int32 CodecConfigId)
		{

			string sql="delete from CodecConfig with (nolock) where CodecConfigId=@CodecConfigId";
			SqlParameter parameter=new SqlParameter("@CodecConfigId",CodecConfigId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CodecConfig))]
		public virtual CodecConfig DeleteCodecConfig(CodecConfig entity)
		{
			this.DeleteCodecConfig(entity.CodecConfigId);
			return entity;
		}


		public virtual CodecConfig CodecConfigFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CodecConfig entity=new CodecConfig();
			if (dr.Table.Columns.Contains("CodecConfigId"))
			{
			entity.CodecConfigId = (System.Int32)dr["CodecConfigId"];
			}
			if (dr.Table.Columns.Contains("ImageLowResWidth"))
			{
			entity.ImageLowResWidth = (System.Int32)dr["ImageLowResWidth"];
			}
			if (dr.Table.Columns.Contains("ImageLowResHeight"))
			{
			entity.ImageLowResHeight = (System.Int32)dr["ImageLowResHeight"];
			}
			if (dr.Table.Columns.Contains("VideoLowResCodec"))
			{
			entity.VideoLowResCodec = dr["VideoLowResCodec"].ToString();
			}
			if (dr.Table.Columns.Contains("VideoHighResCodec"))
			{
			entity.VideoHighResCodec = dr["VideoHighResCodec"].ToString();
			}
			return entity;
		}

	}
	
	
}
