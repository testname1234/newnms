﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class FormatRepositoryBase : Repository, IFormatRepositoryBase 
	{
        
        public FormatRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FormatId",new SearchColumn(){Name="FormatId",Title="FormatId",SelectClause="FormatId",WhereClause="AllRecords.FormatId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FormatId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Format",new SearchColumn(){Name="Format",Title="Format",SelectClause="Format",WhereClause="AllRecords.Format",DataType="System.String",IsForeignColumn=false,PropertyName="Format",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MimeType",new SearchColumn(){Name="MimeType",Title="MimeType",SelectClause="MimeType",WhereClause="AllRecords.MimeType",DataType="System.String",IsForeignColumn=false,PropertyName="MimeType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFormatSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFormatBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFormatAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFormatSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "Format."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",Format."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Format GetFormat(System.Int32 FormatId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFormatSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from Format with (nolock)  where FormatId=@FormatId ";
			SqlParameter parameter=new SqlParameter("@FormatId",FormatId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FormatFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Format> GetFormatByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFormatSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from Format with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Format>(ds,FormatFromDataRow);
		}

		public virtual List<Format> GetAllFormat(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFormatSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from Format with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Format>(ds, FormatFromDataRow);
		}

		public virtual List<Format> GetPagedFormat(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFormatCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FormatId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FormatId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FormatId] ";
            tempsql += " FROM [Format] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FormatId"))
					tempsql += " , (AllRecords.[FormatId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FormatId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFormatSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Format] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Format].[FormatId] = PageIndex.[FormatId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Format>(ds, FormatFromDataRow);
			}else{ return null;}
		}

		private int GetFormatCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Format as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Format as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Format))]
		public virtual Format InsertFormat(Format entity)
		{

			Format other=new Format();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Format ( [Format]
				,[MimeType] )
				Values
				( @Format
				, @MimeType );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Format",entity.Format)
					, new SqlParameter("@MimeType",entity.MimeType ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFormat(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Format))]
		public virtual Format UpdateFormat(Format entity)
		{

			if (entity.IsTransient()) return entity;
			Format other = GetFormat(entity.FormatId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Format set  [Format]=@Format
							, [MimeType]=@MimeType 
							 where FormatId=@FormatId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Format",entity.Format)
					, new SqlParameter("@MimeType",entity.MimeType ?? (object)DBNull.Value)
					, new SqlParameter("@FormatId",entity.FormatId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFormat(entity.FormatId);
		}

		public virtual bool DeleteFormat(System.Int32 FormatId)
		{

			string sql="delete from Format with (nolock) where FormatId=@FormatId";
			SqlParameter parameter=new SqlParameter("@FormatId",FormatId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Format))]
		public virtual Format DeleteFormat(Format entity)
		{
			this.DeleteFormat(entity.FormatId);
			return entity;
		}


		public virtual Format FormatFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Format entity=new Format();
			if (dr.Table.Columns.Contains("FormatId"))
			{
			entity.FormatId = (System.Int32)dr["FormatId"];
			}
			if (dr.Table.Columns.Contains("Format"))
			{
			entity.Format = dr["Format"].ToString();
			}
			if (dr.Table.Columns.Contains("MimeType"))
			{
			entity.MimeType = dr["MimeType"].ToString();
			}
			return entity;
		}

	}
	
	
}
