﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class ResourceDetailRepositoryBase : Repository, IResourceDetailRepositoryBase 
	{
        
        public ResourceDetailRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ResourceDetailId",new SearchColumn(){Name="ResourceDetailId",Title="ResourceDetailId",SelectClause="ResourceDetailId",WhereClause="AllRecords.ResourceDetailId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ResourceDetailId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("slug",new SearchColumn(){Name="slug",Title="slug",SelectClause="slug",WhereClause="AllRecords.slug",DataType="System.String",IsForeignColumn=false,PropertyName="Slug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Description",new SearchColumn(){Name="Description",Title="Description",SelectClause="Description",WhereClause="AllRecords.Description",DataType="System.String",IsForeignColumn=false,PropertyName="Description",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FileSize",new SearchColumn(){Name="FileSize",Title="FileSize",SelectClause="FileSize",WhereClause="AllRecords.FileSize",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="FileSize",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceDate",new SearchColumn(){Name="ResourceDate",Title="ResourceDate",SelectClause="ResourceDate",WhereClause="AllRecords.ResourceDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="ResourceDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LtoStatus",new SearchColumn(){Name="LtoStatus",Title="LtoStatus",SelectClause="LtoStatus",WhereClause="AllRecords.LtoStatus",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LtoStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Barcode",new SearchColumn(){Name="Barcode",Title="Barcode",SelectClause="Barcode",WhereClause="AllRecords.Barcode",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Barcode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedDate",new SearchColumn(){Name="CreatedDate",Title="CreatedDate",SelectClause="CreatedDate",WhereClause="AllRecords.CreatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastupdatedDate",new SearchColumn(){Name="LastupdatedDate",Title="LastupdatedDate",SelectClause="LastupdatedDate",WhereClause="AllRecords.LastupdatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastupdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceId",new SearchColumn(){Name="ResourceId",Title="ResourceId",SelectClause="ResourceId",WhereClause="AllRecords.ResourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Guid",new SearchColumn(){Name="Guid",Title="Guid",SelectClause="Guid",WhereClause="AllRecords.Guid",DataType="System.Guid",IsForeignColumn=false,PropertyName="Guid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceDuration",new SearchColumn(){Name="ResourceDuration",Title="ResourceDuration",SelectClause="ResourceDuration",WhereClause="AllRecords.ResourceDuration",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceDuration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetResourceDetailSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetResourceDetailBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetResourceDetailAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetResourceDetailSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ResourceDetail].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ResourceDetail].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ResourceDetail GetResourceDetail(System.Int32 ResourceDetailId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ResourceDetail] with (nolock)  where ResourceDetailId=@ResourceDetailId ";
			SqlParameter parameter=new SqlParameter("@ResourceDetailId",ResourceDetailId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ResourceDetailFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ResourceDetail> GetResourceDetailByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ResourceDetail] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceDetail>(ds,ResourceDetailFromDataRow);
		}

		public virtual List<ResourceDetail> GetAllResourceDetail(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ResourceDetail] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceDetail>(ds, ResourceDetailFromDataRow);
		}

		public virtual List<ResourceDetail> GetPagedResourceDetail(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetResourceDetailCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ResourceDetailId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ResourceDetailId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ResourceDetailId] ";
            tempsql += " FROM [ResourceDetail] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ResourceDetailId"))
					tempsql += " , (AllRecords.[ResourceDetailId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ResourceDetailId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetResourceDetailSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ResourceDetail] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ResourceDetail].[ResourceDetailId] = PageIndex.[ResourceDetailId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceDetail>(ds, ResourceDetailFromDataRow);
			}else{ return null;}
		}

		private int GetResourceDetailCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ResourceDetail as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ResourceDetail as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ResourceDetail))]
		public virtual ResourceDetail InsertResourceDetail(ResourceDetail entity)
		{

			ResourceDetail other=new ResourceDetail();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ResourceDetail ( [slug]
				,[Description]
				,[FileSize]
				,[ResourceDate]
				,[LtoStatus]
				,[Barcode]
				,[CreatedDate]
				,[LastupdatedDate]
				,[IsActive]
				,[ResourceId]
				,[Guid]
				,[ResourceDuration] )
				Values
				( @slug
				, @Description
				, @FileSize
				, @ResourceDate
				, @LtoStatus
				, @Barcode
				, @CreatedDate
				, @LastupdatedDate
				, @IsActive
				, @ResourceId
				, @Guid
				, @ResourceDuration );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@FileSize",entity.FileSize ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceDate",entity.ResourceDate ?? (object)DBNull.Value)
					, new SqlParameter("@LtoStatus",entity.LtoStatus ?? (object)DBNull.Value)
					, new SqlParameter("@Barcode",entity.Barcode ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedDate",entity.CreatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastupdatedDate",entity.LastupdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceId",entity.ResourceId)
					, new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@ResourceDuration",entity.ResourceDuration ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetResourceDetail(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ResourceDetail))]
		public virtual ResourceDetail UpdateResourceDetail(ResourceDetail entity)
		{

			if (entity.IsTransient()) return entity;
			ResourceDetail other = GetResourceDetail(entity.ResourceDetailId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ResourceDetail set  [slug]=@slug
							, [Description]=@Description
							, [FileSize]=@FileSize
							, [ResourceDate]=@ResourceDate
							, [LtoStatus]=@LtoStatus
							, [Barcode]=@Barcode
							, [LastupdatedDate]=@LastupdatedDate
							, [IsActive]=@IsActive
							, [ResourceId]=@ResourceId
							, [Guid]=@Guid
							, [ResourceDuration]=@ResourceDuration 
							 where ResourceDetailId=@ResourceDetailId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@FileSize",entity.FileSize ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceDate",entity.ResourceDate ?? (object)DBNull.Value)
					, new SqlParameter("@LtoStatus",entity.LtoStatus ?? (object)DBNull.Value)
					, new SqlParameter("@Barcode",entity.Barcode ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedDate",entity.CreatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastupdatedDate",entity.LastupdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceId",entity.ResourceId)
					, new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@ResourceDuration",entity.ResourceDuration ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceDetailId",entity.ResourceDetailId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetResourceDetail(entity.ResourceDetailId);
		}

		public virtual bool DeleteResourceDetail(System.Int32 ResourceDetailId)
		{

			string sql="delete from ResourceDetail where ResourceDetailId=@ResourceDetailId";
			SqlParameter parameter=new SqlParameter("@ResourceDetailId",ResourceDetailId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ResourceDetail))]
		public virtual ResourceDetail DeleteResourceDetail(ResourceDetail entity)
		{
			this.DeleteResourceDetail(entity.ResourceDetailId);
			return entity;
		}


		public virtual ResourceDetail ResourceDetailFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ResourceDetail entity=new ResourceDetail();
			if (dr.Table.Columns.Contains("ResourceDetailId"))
			{
			entity.ResourceDetailId = (System.Int32)dr["ResourceDetailId"];
			}
			if (dr.Table.Columns.Contains("slug"))
			{
			entity.Slug = dr["slug"].ToString();
			}
			if (dr.Table.Columns.Contains("Description"))
			{
			entity.Description = dr["Description"].ToString();
			}
			if (dr.Table.Columns.Contains("FileSize"))
			{
			entity.FileSize = dr["FileSize"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["FileSize"];
			}
			if (dr.Table.Columns.Contains("ResourceDate"))
			{
			entity.ResourceDate = dr["ResourceDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["ResourceDate"];
			}
			if (dr.Table.Columns.Contains("LtoStatus"))
			{
			entity.LtoStatus = dr["LtoStatus"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LtoStatus"];
			}
			if (dr.Table.Columns.Contains("Barcode"))
			{
			entity.Barcode = dr["Barcode"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Barcode"];
			}
			if (dr.Table.Columns.Contains("CreatedDate"))
			{
			entity.CreatedDate = dr["CreatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreatedDate"];
			}
			if (dr.Table.Columns.Contains("LastupdatedDate"))
			{
			entity.LastupdatedDate = dr["LastupdatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastupdatedDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("ResourceId"))
			{
			entity.ResourceId = (System.Int32)dr["ResourceId"];
			}
			if (dr.Table.Columns.Contains("Guid"))
			{
			entity.Guid = (System.Guid)dr["Guid"];
			}
			if (dr.Table.Columns.Contains("ResourceDuration"))
			{
			entity.ResourceDuration = dr["ResourceDuration"].ToString();
			}
			return entity;
		}

	}
	
	
}
