﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace MS.Repository
{
		
	public partial class ResourceDetailRepository: ResourceDetailRepositoryBase, IResourceDetailRepository
	{
        
        public ResourceDetail GetResourceDetailbyResourceIdCustom(int ResourceId)
        {
            string sql = "select ResourceDate,Slug,Description,ResourceId ";
            sql += "from ResourceDetail  where ResourceId =@ResourceId";
            SqlParameter parameter = new SqlParameter("@ResourceId", ResourceId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceDetailFromDataRow(ds.Tables[0].Rows[0]);
        }

        public ResourceDetail GetResourceDetailByGuid(string Guid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceDetailSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [ResourceDetail] with (nolock)  where [Guid]=@Guid ";
            SqlParameter parameter = new SqlParameter("@Guid", Guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceDetailFromDataRow(ds.Tables[0].Rows[0]);
        }
    }
	
	
}
