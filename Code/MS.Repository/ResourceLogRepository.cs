﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data;
using MS.Core.Models;
using System.Data.SqlClient;

namespace MS.Repository
{
		
	public partial class ResourceLogRepository: ResourceLogRepositoryBase, IResourceLogRepository
	{

        public List<Core.Models.UserLog> getAllUSerLog()
        {
            string sql = @"select UserId, count(*) as Hits  from resourcelog group by UserId order by Hits desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<UserLog>(ds, ResourceLogReport);
        }

        public virtual UserLog ResourceLogReport(DataRow dr)
        {
            if (dr == null) return null;
            UserLog entity = new UserLog();

            if (dr.Table.Columns.Contains("UserId"))
            {
                entity.UserId = dr["UserId"].ToString();
            }

            if (dr.Table.Columns.Contains("Hits"))
            {
                entity.Hits = Convert.ToInt32(dr["Hits"].ToString());
            }

            return entity;

        }


        public List<ResourceLog> GetResourceLogbyUserId(int p)
        {
            string sql = "select * from resourcelog where userid  = @UserID";
            SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserID",p)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ResourceLog>(ds, ResourceLogFromDataRow);
        }
    }
	
	
}
