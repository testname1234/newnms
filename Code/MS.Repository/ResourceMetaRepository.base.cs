﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class ResourceMetaRepositoryBase : Repository, IResourceMetaRepositoryBase 
	{
        
        public ResourceMetaRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ResourceMetaId",new SearchColumn(){Name="ResourceMetaId",Title="ResourceMetaId",SelectClause="ResourceMetaId",WhereClause="AllRecords.ResourceMetaId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ResourceMetaId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.Guid",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Value",new SearchColumn(){Name="Value",Title="Value",SelectClause="Value",WhereClause="AllRecords.Value",DataType="System.String",IsForeignColumn=false,PropertyName="Value",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetResourceMetaSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetResourceMetaBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetResourceMetaAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetResourceMetaSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ResourceMeta].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ResourceMeta].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ResourceMeta GetResourceMeta(System.Int32 ResourceMetaId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceMetaSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ResourceMeta with (nolock)  where ResourceMetaId=@ResourceMetaId ";
			SqlParameter parameter=new SqlParameter("@ResourceMetaId",ResourceMetaId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ResourceMetaFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ResourceMeta> GetResourceMetaByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceMetaSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ResourceMeta with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceMeta>(ds,ResourceMetaFromDataRow);
		}

		public virtual List<ResourceMeta> GetAllResourceMeta(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceMetaSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ResourceMeta with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceMeta>(ds, ResourceMetaFromDataRow);
		}

		public virtual List<ResourceMeta> GetPagedResourceMeta(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetResourceMetaCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ResourceMetaId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ResourceMetaId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ResourceMetaId] ";
            tempsql += " FROM [ResourceMeta] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ResourceMetaId"))
					tempsql += " , (AllRecords.[ResourceMetaId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ResourceMetaId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetResourceMetaSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ResourceMeta] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ResourceMeta].[ResourceMetaId] = PageIndex.[ResourceMetaId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceMeta>(ds, ResourceMetaFromDataRow);
			}else{ return null;}
		}

		private int GetResourceMetaCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ResourceMeta as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ResourceMeta as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ResourceMeta))]
		public virtual ResourceMeta InsertResourceMeta(ResourceMeta entity)
		{

			ResourceMeta other=new ResourceMeta();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ResourceMeta ( [ResourceGuid]
				,[Name]
				,[Value]
				,[CreationDate] )
				Values
				( @ResourceGuid
				, @Name
				, @Value
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Value",entity.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetResourceMeta(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ResourceMeta))]
		public virtual ResourceMeta UpdateResourceMeta(ResourceMeta entity)
		{

			if (entity.IsTransient()) return entity;
			ResourceMeta other = GetResourceMeta(entity.ResourceMetaId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ResourceMeta set  [ResourceGuid]=@ResourceGuid
							, [Name]=@Name
							, [Value]=@Value
							, [CreationDate]=@CreationDate 
							 where ResourceMetaId=@ResourceMetaId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Value",entity.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@ResourceMetaId",entity.ResourceMetaId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetResourceMeta(entity.ResourceMetaId);
		}

		public virtual bool DeleteResourceMeta(System.Int32 ResourceMetaId)
		{

			string sql="delete from ResourceMeta with (nolock) where ResourceMetaId=@ResourceMetaId";
			SqlParameter parameter=new SqlParameter("@ResourceMetaId",ResourceMetaId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ResourceMeta))]
		public virtual ResourceMeta DeleteResourceMeta(ResourceMeta entity)
		{
			this.DeleteResourceMeta(entity.ResourceMetaId);
			return entity;
		}


		public virtual ResourceMeta ResourceMetaFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ResourceMeta entity=new ResourceMeta();
			if (dr.Table.Columns.Contains("ResourceMetaId"))
			{
			entity.ResourceMetaId = (System.Int32)dr["ResourceMetaId"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = (System.Guid)dr["ResourceGuid"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("Value"))
			{
			entity.Value = dr["Value"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
