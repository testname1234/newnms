﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace MS.Repository
{
		
	public partial class ResourceMetaRepository: ResourceMetaRepositoryBase, IResourceMetaRepository
	{
        public virtual ResourceMeta GetResourceMetaByResource(ResourceMeta resourceMeta, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceMetaSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from ResourceMeta with (nolock)  where ResourceGuid=@ResourceGuid and Name = @Name";
            SqlParameter parameter1 = new SqlParameter("@ResourceGuid", resourceMeta.ResourceGuid);
            SqlParameter parameter2 = new SqlParameter("@Name", resourceMeta.Name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1,parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceMetaFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual ResourceMeta GetMetaByGuidKeyAndValue(ResourceMeta resourceMeta, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceMetaSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from ResourceMeta with (nolock)  where ResourceGuid=@ResourceGuid and Name = @Name and Value=@value";
            SqlParameter parameter1 = new SqlParameter("@ResourceGuid", resourceMeta.ResourceGuid);
            SqlParameter parameter2 = new SqlParameter("@Name", resourceMeta.Name);
            SqlParameter parameter3 = new SqlParameter("@value", resourceMeta.Value);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2, parameter3 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceMetaFromDataRow(ds.Tables[0].Rows[0]);
        }


        public List<ResourceMeta> GetResourceMetaByGuid(string Guid)
        {
            string sql = GetResourceMetaSelectClause();
            sql += "from ResourceMeta with (nolock)  where ResourceGuid=@ResourceGuid";
            SqlParameter parameter1 = new SqlParameter("@ResourceGuid", Guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ResourceMeta>(ds, ResourceMetaFromDataRow);
        }


        public List<ResourceMeta> GetResourceMetaforThread()
        {
            string sql = "SELECT * FROM (SELECT distinct LTrim(value) as value,Name,ROW_NUMBER() OVER (PARTITION BY value ORDER BY value) AS RowNumber FROM ResourceMeta with (nolock) where name <> 'Tag' and name <> 'Caption' and name <> 'category' and  value <> ''  ) AS a WHERE   a.RowNumber = 1";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ResourceMeta>(ds, ResourceMetaFromDataRow);
        }


        public List<ResourceMeta> GetResourceMetaforThreadAfterDate(DateTime lastUpdateDate)
        {
            string sql = @"SELECT * FROM (SELECT distinct LTrim(value) as value,Name,ROW_NUMBER() OVER (PARTITION BY value ORDER BY value) AS RowNumber FROM ResourceMeta with (nolock) where name <> 'Tag' and name <> 'Caption' and name <> 'category' and  value <> ''  AND creationDate > @LastUpdateDate) AS a WHERE   a.RowNumber = 1";
            SqlHelper.CommandTimeout = 1000000;
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@LastUpdateDate", lastUpdateDate)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ResourceMeta>(ds, ResourceMetaFromDataRow);
        }
    }
	
	
}
