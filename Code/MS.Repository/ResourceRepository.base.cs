﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{

    public abstract partial class ResourceRepositoryBase : Repository, IResourceRepositoryBase
    {

        public ResourceRepositoryBase()
        {
            this.SearchColumns = new Dictionary<string, SearchColumn>();

            this.SearchColumns.Add("ResourceId", new SearchColumn() { Name = "ResourceId", Title = "ResourceId", SelectClause = "ResourceId", WhereClause = "AllRecords.ResourceId", DataType = "System.Int32", IsForeignColumn = false, PropertyName = "ResourceId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Guid", new SearchColumn() { Name = "Guid", Title = "Guid", SelectClause = "Guid", WhereClause = "AllRecords.Guid", DataType = "System.Guid", IsForeignColumn = false, PropertyName = "Guid", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("ResourceTypeId", new SearchColumn() { Name = "ResourceTypeId", Title = "ResourceTypeId", SelectClause = "ResourceTypeId", WhereClause = "AllRecords.ResourceTypeId", DataType = "System.Int32", IsForeignColumn = false, PropertyName = "ResourceTypeId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("HighResolutionFile", new SearchColumn() { Name = "HighResolutionFile", Title = "HighResolutionFile", SelectClause = "HighResolutionFile", WhereClause = "AllRecords.HighResolutionFile", DataType = "System.String", IsForeignColumn = false, PropertyName = "HighResolutionFile", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("HighResolutionFormatId", new SearchColumn() { Name = "HighResolutionFormatId", Title = "HighResolutionFormatId", SelectClause = "HighResolutionFormatId", WhereClause = "AllRecords.HighResolutionFormatId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "HighResolutionFormatId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("LowResolutionFile", new SearchColumn() { Name = "LowResolutionFile", Title = "LowResolutionFile", SelectClause = "LowResolutionFile", WhereClause = "AllRecords.LowResolutionFile", DataType = "System.String", IsForeignColumn = false, PropertyName = "LowResolutionFile", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("LowResolutionFormatId", new SearchColumn() { Name = "LowResolutionFormatId", Title = "LowResolutionFormatId", SelectClause = "LowResolutionFormatId", WhereClause = "AllRecords.LowResolutionFormatId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "LowResolutionFormatId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("FileName", new SearchColumn() { Name = "FileName", Title = "FileName", SelectClause = "FileName", WhereClause = "AllRecords.FileName", DataType = "System.String", IsForeignColumn = false, PropertyName = "FileName", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("ResourceStatusId", new SearchColumn() { Name = "ResourceStatusId", Title = "ResourceStatusId", SelectClause = "ResourceStatusId", WhereClause = "AllRecords.ResourceStatusId", DataType = "System.Int32", IsForeignColumn = false, PropertyName = "ResourceStatusId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("ServerId", new SearchColumn() { Name = "ServerId", Title = "ServerId", SelectClause = "ServerId", WhereClause = "AllRecords.ServerId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "ServerId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Source", new SearchColumn() { Name = "Source", Title = "Source", SelectClause = "Source", WhereClause = "AllRecords.Source", DataType = "System.String", IsForeignColumn = false, PropertyName = "Source", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("ThumbUrl", new SearchColumn() { Name = "ThumbUrl", Title = "ThumbUrl", SelectClause = "ThumbUrl", WhereClause = "AllRecords.ThumbUrl", DataType = "System.String", IsForeignColumn = false, PropertyName = "ThumbUrl", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("CreationDate", new SearchColumn() { Name = "CreationDate", Title = "CreationDate", SelectClause = "CreationDate", WhereClause = "AllRecords.CreationDate", DataType = "System.DateTime", IsForeignColumn = false, PropertyName = "CreationDate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("LastUpdateDate", new SearchColumn() { Name = "LastUpdateDate", Title = "LastUpdateDate", SelectClause = "LastUpdateDate", WhereClause = "AllRecords.LastUpdateDate", DataType = "System.DateTime", IsForeignColumn = false, PropertyName = "LastUpdateDate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("IsActive", new SearchColumn() { Name = "IsActive", Title = "IsActive", SelectClause = "IsActive", WhereClause = "AllRecords.IsActive", DataType = "System.Boolean", IsForeignColumn = false, PropertyName = "IsActive", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Caption", new SearchColumn() { Name = "Caption", Title = "Caption", SelectClause = "Caption", WhereClause = "AllRecords.Caption", DataType = "System.String", IsForeignColumn = false, PropertyName = "Caption", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Category", new SearchColumn() { Name = "Category", Title = "Category", SelectClause = "Category", WhereClause = "AllRecords.Category", DataType = "System.String", IsForeignColumn = false, PropertyName = "Category", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Location", new SearchColumn() { Name = "Location", Title = "Location", SelectClause = "Location", WhereClause = "AllRecords.Location", DataType = "System.String", IsForeignColumn = false, PropertyName = "Location", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("FilePath", new SearchColumn() { Name = "FilePath", Title = "FilePath", SelectClause = "FilePath", WhereClause = "AllRecords.FilePath", DataType = "System.String", IsForeignColumn = false, PropertyName = "FilePath", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Duration", new SearchColumn() { Name = "Duration", Title = "Duration", SelectClause = "Duration", WhereClause = "AllRecords.Duration", DataType = "System.Double?", IsForeignColumn = false, PropertyName = "Duration", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Bitrate", new SearchColumn() { Name = "Bitrate", Title = "Bitrate", SelectClause = "Bitrate", WhereClause = "AllRecords.Bitrate", DataType = "System.String", IsForeignColumn = false, PropertyName = "Bitrate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Framerate", new SearchColumn() { Name = "Framerate", Title = "Framerate", SelectClause = "Framerate", WhereClause = "AllRecords.Framerate", DataType = "System.String", IsForeignColumn = false, PropertyName = "Framerate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("AudioCodec", new SearchColumn() { Name = "AudioCodec", Title = "AudioCodec", SelectClause = "AudioCodec", WhereClause = "AllRecords.AudioCodec", DataType = "System.String", IsForeignColumn = false, PropertyName = "AudioCodec", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("VideoCodec", new SearchColumn() { Name = "VideoCodec", Title = "VideoCodec", SelectClause = "VideoCodec", WhereClause = "AllRecords.VideoCodec", DataType = "System.String", IsForeignColumn = false, PropertyName = "VideoCodec", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Width", new SearchColumn() { Name = "Width", Title = "Width", SelectClause = "Width", WhereClause = "AllRecords.Width", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "Width", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Height", new SearchColumn() { Name = "Height", Title = "Height", SelectClause = "Height", WhereClause = "AllRecords.Height", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "Height", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("BucketId", new SearchColumn() { Name = "BucketId", Title = "BucketId", SelectClause = "BucketId", WhereClause = "AllRecords.BucketId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "BucketId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("SystemType", new SearchColumn() { Name = "SystemType", Title = "SystemType", SelectClause = "SystemType", WhereClause = "AllRecords.SystemType", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "SystemType", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("IsPrivate", new SearchColumn() { Name = "IsPrivate", Title = "IsPrivate", SelectClause = "IsPrivate", WhereClause = "AllRecords.IsPrivate", DataType = "System.Boolean?", IsForeignColumn = false, PropertyName = "IsPrivate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("SourceTypeId", new SearchColumn() { Name = "SourceTypeId", Title = "SourceTypeId", SelectClause = "SourceTypeId", WhereClause = "AllRecords.SourceTypeId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "SourceTypeId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("SourceName", new SearchColumn() { Name = "SourceName", Title = "SourceName", SelectClause = "SourceName", WhereClause = "AllRecords.SourceName", DataType = "System.String", IsForeignColumn = false, PropertyName = "SourceName", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("SourceId", new SearchColumn() { Name = "SourceId", Title = "SourceId", SelectClause = "SourceId", WhereClause = "AllRecords.SourceId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "SourceId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("LastAccesDate", new SearchColumn() { Name = "LastAccesDate", Title = "LastAccesDate", SelectClause = "LastAccesDate", WhereClause = "AllRecords.LastAccesDate", DataType = "System.DateTime?", IsForeignColumn = false, PropertyName = "LastAccesDate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("AccessCount", new SearchColumn() { Name = "AccessCount", Title = "AccessCount", SelectClause = "AccessCount", WhereClause = "AllRecords.AccessCount", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "AccessCount", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("SubBucketId", new SearchColumn() { Name = "SubBucketId", Title = "SubBucketId", SelectClause = "SubBucketId", WhereClause = "AllRecords.SubBucketId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "SubBucketId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("FootageTypeId", new SearchColumn() { Name = "FootageTypeId", Title = "FootageTypeId", SelectClause = "FootageTypeId", WhereClause = "AllRecords.FootageTypeId", DataType = "System.Int32?", IsForeignColumn = false, PropertyName = "FootageTypeId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("ServerName", new SearchColumn() { Name = "ServerName", Title = "ServerName", SelectClause = "ServerName", WhereClause = "AllRecords.ServerName", DataType = "System.String", IsForeignColumn = false, PropertyName = "ServerName", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("AllowLowResTrancoding", new SearchColumn() { Name = "AllowLowResTrancoding", Title = "AllowLowResTrancoding", SelectClause = "AllowLowResTrancoding", WhereClause = "AllRecords.AllowLowResTrancoding", DataType = "System.Boolean?", IsForeignColumn = false, PropertyName = "AllowLowResTrancoding", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("isHD", new SearchColumn() { Name = "isHD", Title = "isHD", SelectClause = "isHD", WhereClause = "AllRecords.isHD", DataType = "System.Boolean?", IsForeignColumn = false, PropertyName = "isHD", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });

        }

        public virtual List<SearchColumn> GetResourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }



        public virtual Dictionary<string, string> GetResourceBasicSearchColumns()
        {
            Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetResourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    keyValuePair.Value.Value = string.Empty;
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }


        public virtual string GetResourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery = string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
                    if (keyValuePair.Value.IsForeignColumn)
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                        else
                        {
                            selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "[Resource].[" + keyValuePair.Key + "]";
                        }
                        else
                        {
                            selectQuery += ",[Resource].[" + keyValuePair.Key + "]";
                        }
                    }
                }
            }
            return "Select " + selectQuery + " ";
        }


        public virtual List<Resource> GetResourceByResourceTypeId(System.Int32 ResourceTypeId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Resource] with (nolock)  where ResourceTypeId=@ResourceTypeId  ";
            SqlParameter parameter = new SqlParameter("@ResourceTypeId", ResourceTypeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual List<Resource> GetResourceByHighResolutionFormatId(System.Int32? HighResolutionFormatId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Resource] with (nolock)  where HighResolutionFormatId=@HighResolutionFormatId  ";
            SqlParameter parameter = new SqlParameter("@HighResolutionFormatId", HighResolutionFormatId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual List<Resource> GetResourceByLowResolutionFormatId(System.Int32? LowResolutionFormatId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Resource] with (nolock)  where LowResolutionFormatId=@LowResolutionFormatId  ";
            SqlParameter parameter = new SqlParameter("@LowResolutionFormatId", LowResolutionFormatId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual List<Resource> GetResourceByResourceStatusId(System.Int32 ResourceStatusId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Resource] with (nolock)  where ResourceStatusId=@ResourceStatusId  ";
            SqlParameter parameter = new SqlParameter("@ResourceStatusId", ResourceStatusId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual List<Resource> GetResourceByServerId(System.Int32? ServerId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Resource] with (nolock)  where ServerId=@ServerId  ";
            SqlParameter parameter = new SqlParameter("@ServerId", ServerId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual Resource GetResource(System.Int32 ResourceId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Resource] with (nolock)  where ResourceId=@ResourceId ";
            SqlParameter parameter = new SqlParameter("@ResourceId", ResourceId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Resource> GetResourceByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += string.Format("from [Resource] with (nolock)  where {0} {1} '{2}' ", Key, operand.ToOperandString(), Value);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual List<Resource> GetAllResource(string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Resource] with (nolock)  ";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual List<Resource> GetPagedResource(string orderByClause, int pageSize, int startIndex, out int count, List<SearchColumn> searchColumns, string SelectClause = null)
        {

            string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
            if (!String.IsNullOrEmpty(orderByClause))
            {
                KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
            }

            count = GetResourceCount(whereClause, searchColumns);
            if (count > 0)
            {
                if (count < startIndex) startIndex = (count / pageSize) * pageSize;

                int PageLowerBound = startIndex;
                int PageUpperBound = PageLowerBound + pageSize;
                string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ResourceId] int				   
				            );";

                //Insert into the temp table
                string tempsql = "INSERT INTO #PageIndex ([ResourceId])";
                tempsql += " SELECT ";
                if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
                tempsql += " [ResourceId] ";
                tempsql += " FROM [Resource] AllRecords with (NOLOCK)";
                if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
                if (orderByClause.Length > 0)
                {
                    tempsql += " ORDER BY " + orderByClause;
                    if (!orderByClause.Contains("ResourceId"))
                        tempsql += " , (AllRecords.[ResourceId])";
                }
                else
                {
                    tempsql += " ORDER BY (AllRecords.[ResourceId])";
                }

                // Return paged results
                string pagedResultsSql =
                    (string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause))) + @" FROM [Resource] , #PageIndex PageIndex WHERE ";
                pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString();
                pagedResultsSql += @" AND [Resource].[ResourceId] = PageIndex.[ResourceId] 
				                  ORDER BY PageIndex.IndexId;";
                pagedResultsSql += " drop table #PageIndex";
                sql = sql + tempsql + " " + pagedResultsSql;
                sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
            }
            else { return null; }
        }

        private int GetResourceCount(string whereClause, List<SearchColumn> searchColumns)
        {

            string sql = string.Empty;
            if (string.IsNullOrEmpty(whereClause))
                sql = "SELECT Count(*) FROM Resource as AllRecords  ";
            else
                sql = "SELECT Count(*) FROM Resource as AllRecords  where  " + whereClause;
            var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
            return rowCount == DBNull.Value ? 0 : (int)rowCount;
        }

        [MOLog(AuditOperations.Create, typeof(Resource))]
        public virtual Resource InsertResource(Resource entity)
        {

            Resource other = new Resource();
            other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into Resource ( [Guid]
				,[ResourceTypeId]
				,[HighResolutionFile]
				,[HighResolutionFormatId]
				,[LowResolutionFile]
				,[LowResolutionFormatId]
				,[FileName]
				,[ResourceStatusId]
				,[ServerId]
				,[Source]
				,[ThumbUrl]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[Caption]
				,[Category]
				,[Location]
				,[FilePath]
				,[Duration]
				,[Bitrate]
				,[Framerate]
				,[AudioCodec]
				,[VideoCodec]
				,[Width]
				,[Height]
				,[BucketId]
				,[SystemType]
				,[IsPrivate]
				,[SourceTypeId]
				,[SourceName]
				,[SourceId]
				,[LastAccesDate]
				,[AccessCount]
				,[SubBucketId]
				,[FootageTypeId]
				,[ServerName]
				,[AllowLowResTrancoding], [isHD] )
				Values
				( @Guid
				, @ResourceTypeId
				, @HighResolutionFile
				, @HighResolutionFormatId
				, @LowResolutionFile
				, @LowResolutionFormatId
				, @FileName
				, @ResourceStatusId
				, @ServerId
				, @Source
				, @ThumbUrl
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @Caption
				, @Category
				, @Location
				, @FilePath
				, @Duration
				, @Bitrate
				, @Framerate
				, @AudioCodec
				, @VideoCodec
				, @Width
				, @Height
				, @BucketId
				, @SystemType
				, @IsPrivate
				, @SourceTypeId
				, @SourceName
				, @SourceId
				, @LastAccesDate
				, @AccessCount
				, @SubBucketId
				, @FootageTypeId
				, @ServerName
				, @AllowLowResTrancoding, @isHD );
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Guid",entity.Guid)
                    , new SqlParameter("@ResourceTypeId",entity.ResourceTypeId)
                    , new SqlParameter("@HighResolutionFile",entity.HighResolutionFile ?? (object)DBNull.Value)
                    , new SqlParameter("@HighResolutionFormatId",entity.HighResolutionFormatId ?? (object)DBNull.Value)
                    , new SqlParameter("@LowResolutionFile",entity.LowResolutionFile ?? (object)DBNull.Value)
                    , new SqlParameter("@LowResolutionFormatId",entity.LowResolutionFormatId ?? (object)DBNull.Value)
                    , new SqlParameter("@FileName",entity.FileName ?? (object)DBNull.Value)
                    , new SqlParameter("@ResourceStatusId",entity.ResourceStatusId)
                    , new SqlParameter("@ServerId",entity.ServerId ?? (object)DBNull.Value)
                    , new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
                    , new SqlParameter("@ThumbUrl",entity.ThumbUrl ?? (object)DBNull.Value)
                    , new SqlParameter("@CreationDate",entity.CreationDate)
                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
                    , new SqlParameter("@IsActive",entity.IsActive)
                    , new SqlParameter("@Caption",entity.Caption ?? (object)DBNull.Value)
                    , new SqlParameter("@Category",entity.Category ?? (object)DBNull.Value)
                    , new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
                    , new SqlParameter("@FilePath",entity.FilePath ?? (object)DBNull.Value)
                    , new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
                    , new SqlParameter("@Bitrate",entity.Bitrate ?? (object)DBNull.Value)
                    , new SqlParameter("@Framerate",entity.Framerate ?? (object)DBNull.Value)
                    , new SqlParameter("@AudioCodec",entity.AudioCodec ?? (object)DBNull.Value)
                    , new SqlParameter("@VideoCodec",entity.VideoCodec ?? (object)DBNull.Value)
                    , new SqlParameter("@Width",entity.Width ?? (object)DBNull.Value)
                    , new SqlParameter("@Height",entity.Height ?? (object)DBNull.Value)
                    , new SqlParameter("@BucketId",entity.BucketId ?? (object)DBNull.Value)
                    , new SqlParameter("@SystemType",entity.SystemType ?? (object)DBNull.Value)
                    , new SqlParameter("@IsPrivate",entity.IsPrivate ?? (object)DBNull.Value)
                    , new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
                    , new SqlParameter("@SourceName",entity.SourceName ?? (object)DBNull.Value)
                    , new SqlParameter("@SourceId",entity.SourceId ?? (object)DBNull.Value)
                    , new SqlParameter("@LastAccesDate",entity.LastAccesDate ?? (object)DBNull.Value)
                    , new SqlParameter("@AccessCount",entity.AccessCount ?? (object)DBNull.Value)
                    , new SqlParameter("@SubBucketId",entity.SubBucketId ?? (object)DBNull.Value)
                    , new SqlParameter("@FootageTypeId",entity.FootageTypeId ?? (object)DBNull.Value)
                    , new SqlParameter("@ServerName",entity.ServerName ?? (object)DBNull.Value)
                    , new SqlParameter("@AllowLowResTrancoding",entity.AllowLowResTrancoding ?? (object)DBNull.Value)
                    , new SqlParameter("@isHD",entity.isHD)
                };
                var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
                return GetResource(Convert.ToInt32(identity));
            }
            return entity;
        }

        [MOLog(AuditOperations.Update, typeof(Resource))]
        public virtual Resource UpdateResource(Resource entity)
        {

            if (entity.IsTransient()) return entity;
            Resource other = GetResource(entity.ResourceId);
            if (entity.Equals(other)) return entity;
            string sql = @"Update Resource set  [Guid]=@Guid
							, [ResourceTypeId]=@ResourceTypeId
							, [HighResolutionFile]=@HighResolutionFile
							, [HighResolutionFormatId]=@HighResolutionFormatId
							, [LowResolutionFile]=@LowResolutionFile
							, [LowResolutionFormatId]=@LowResolutionFormatId
							, [FileName]=@FileName
							, [ResourceStatusId]=@ResourceStatusId
							, [ServerId]=@ServerId
							, [Source]=@Source
							, [ThumbUrl]=@ThumbUrl
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [Caption]=@Caption
							, [Category]=@Category
							, [Location]=@Location
							, [FilePath]=@FilePath
							, [Duration]=@Duration
							, [Bitrate]=@Bitrate
							, [Framerate]=@Framerate
							, [AudioCodec]=@AudioCodec
							, [VideoCodec]=@VideoCodec
							, [Width]=@Width
							, [Height]=@Height
							, [BucketId]=@BucketId
							, [SystemType]=@SystemType
							, [IsPrivate]=@IsPrivate
							, [SourceTypeId]=@SourceTypeId
							, [SourceName]=@SourceName
							, [SourceId]=@SourceId
							, [LastAccesDate]=@LastAccesDate
							, [AccessCount]=@AccessCount
							, [SubBucketId]=@SubBucketId
							, [FootageTypeId]=@FootageTypeId
							, [ServerName]=@ServerName
							, [AllowLowResTrancoding]=@AllowLowResTrancoding
                            , [isHD]=@isHD
							 where ResourceId=@ResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Guid",entity.Guid)
                    , new SqlParameter("@ResourceTypeId",entity.ResourceTypeId)
                    , new SqlParameter("@HighResolutionFile",entity.HighResolutionFile ?? (object)DBNull.Value)
                    , new SqlParameter("@HighResolutionFormatId",entity.HighResolutionFormatId ?? (object)DBNull.Value)
                    , new SqlParameter("@LowResolutionFile",entity.LowResolutionFile ?? (object)DBNull.Value)
                    , new SqlParameter("@LowResolutionFormatId",entity.LowResolutionFormatId ?? (object)DBNull.Value)
                    , new SqlParameter("@FileName",entity.FileName ?? (object)DBNull.Value)
                    , new SqlParameter("@ResourceStatusId",entity.ResourceStatusId)
                    , new SqlParameter("@ServerId",entity.ServerId ?? (object)DBNull.Value)
                    , new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
                    , new SqlParameter("@ThumbUrl",entity.ThumbUrl ?? (object)DBNull.Value)
                    , new SqlParameter("@CreationDate",entity.CreationDate)
                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
                    , new SqlParameter("@IsActive",entity.IsActive)
                    , new SqlParameter("@Caption",entity.Caption ?? (object)DBNull.Value)
                    , new SqlParameter("@Category",entity.Category ?? (object)DBNull.Value)
                    , new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
                    , new SqlParameter("@FilePath",entity.FilePath ?? (object)DBNull.Value)
                    , new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
                    , new SqlParameter("@Bitrate",entity.Bitrate ?? (object)DBNull.Value)
                    , new SqlParameter("@Framerate",entity.Framerate ?? (object)DBNull.Value)
                    , new SqlParameter("@AudioCodec",entity.AudioCodec ?? (object)DBNull.Value)
                    , new SqlParameter("@VideoCodec",entity.VideoCodec ?? (object)DBNull.Value)
                    , new SqlParameter("@Width",entity.Width ?? (object)DBNull.Value)
                    , new SqlParameter("@Height",entity.Height ?? (object)DBNull.Value)
                    , new SqlParameter("@BucketId",entity.BucketId ?? (object)DBNull.Value)
                    , new SqlParameter("@SystemType",entity.SystemType ?? (object)DBNull.Value)
                    , new SqlParameter("@IsPrivate",entity.IsPrivate ?? (object)DBNull.Value)
                    , new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
                    , new SqlParameter("@SourceName",entity.SourceName ?? (object)DBNull.Value)
                    , new SqlParameter("@SourceId",entity.SourceId ?? (object)DBNull.Value)
                    , new SqlParameter("@LastAccesDate",entity.LastAccesDate ?? (object)DBNull.Value)
                    , new SqlParameter("@AccessCount",entity.AccessCount ?? (object)DBNull.Value)
                    , new SqlParameter("@SubBucketId",entity.SubBucketId ?? (object)DBNull.Value)
                    , new SqlParameter("@FootageTypeId",entity.FootageTypeId ?? (object)DBNull.Value)
                    , new SqlParameter("@ServerName",entity.ServerName ?? (object)DBNull.Value)
                    , new SqlParameter("@AllowLowResTrancoding",entity.AllowLowResTrancoding ?? (object)DBNull.Value)
                    , new SqlParameter("@isHD",entity.isHD ?? (object)DBNull.Value)
                    , new SqlParameter("@ResourceId",entity.ResourceId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetResource(entity.ResourceId);
        }

        public virtual bool DeleteResource(System.Int32 ResourceId)
        {

            string sql = "delete from Resource where ResourceId=@ResourceId";
            SqlParameter parameter = new SqlParameter("@ResourceId", ResourceId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        [MOLog(AuditOperations.Delete, typeof(Resource))]
        public virtual Resource DeleteResource(Resource entity)
        {
            this.DeleteResource(entity.ResourceId);
            return entity;
        }


        public virtual Resource ResourceFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            Resource entity = new Resource();
            if (dr.Table.Columns.Contains("ResourceId"))
            {
                entity.ResourceId = (System.Int32)dr["ResourceId"];
            }
            if (dr.Table.Columns.Contains("Guid"))
            {
                entity.Guid = (System.Guid)dr["Guid"];
            }
            if (dr.Table.Columns.Contains("ResourceTypeId"))
            {
                entity.ResourceTypeId = (System.Int32)dr["ResourceTypeId"];
            }
            if (dr.Table.Columns.Contains("HighResolutionFile"))
            {
                entity.HighResolutionFile = dr["HighResolutionFile"].ToString();
            }
            if (dr.Table.Columns.Contains("HighResolutionFormatId"))
            {
                entity.HighResolutionFormatId = dr["HighResolutionFormatId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["HighResolutionFormatId"];
            }
            if (dr.Table.Columns.Contains("LowResolutionFile"))
            {
                entity.LowResolutionFile = dr["LowResolutionFile"].ToString();
            }
            if (dr.Table.Columns.Contains("LowResolutionFormatId"))
            {
                entity.LowResolutionFormatId = dr["LowResolutionFormatId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["LowResolutionFormatId"];
            }
            if (dr.Table.Columns.Contains("FileName"))
            {
                entity.FileName = dr["FileName"].ToString();
            }
            if (dr.Table.Columns.Contains("ResourceStatusId"))
            {
                entity.ResourceStatusId = (System.Int32)dr["ResourceStatusId"];
            }
            if (dr.Table.Columns.Contains("ServerId"))
            {
                entity.ServerId = dr["ServerId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["ServerId"];
            }
            if (dr.Table.Columns.Contains("Source"))
            {
                entity.Source = dr["Source"].ToString();
            }
            if (dr.Table.Columns.Contains("ThumbUrl"))
            {
                entity.ThumbUrl = dr["ThumbUrl"].ToString();
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("IsActive"))
            {
                entity.IsActive = (System.Boolean)dr["IsActive"];
            }
            if (dr.Table.Columns.Contains("Caption"))
            {
                entity.Caption = dr["Caption"].ToString();
            }
            if (dr.Table.Columns.Contains("Category"))
            {
                entity.Category = dr["Category"].ToString();
            }
            if (dr.Table.Columns.Contains("Location"))
            {
                entity.Location = dr["Location"].ToString();
            }
            if (dr.Table.Columns.Contains("FilePath"))
            {
                entity.FilePath = dr["FilePath"].ToString();
            }
            if (dr.Table.Columns.Contains("Duration"))
            {
                entity.Duration = dr["Duration"] == DBNull.Value ? (System.Double?)null : (System.Double?)dr["Duration"];
            }
            if (dr.Table.Columns.Contains("Bitrate"))
            {
                entity.Bitrate = dr["Bitrate"].ToString();
            }
            if (dr.Table.Columns.Contains("Framerate"))
            {
                entity.Framerate = dr["Framerate"].ToString();
            }
            if (dr.Table.Columns.Contains("AudioCodec"))
            {
                entity.AudioCodec = dr["AudioCodec"].ToString();
            }
            if (dr.Table.Columns.Contains("VideoCodec"))
            {
                entity.VideoCodec = dr["VideoCodec"].ToString();
            }
            if (dr.Table.Columns.Contains("Width"))
            {
                entity.Width = dr["Width"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["Width"];
            }
            if (dr.Table.Columns.Contains("Height"))
            {
                entity.Height = dr["Height"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["Height"];
            }
            if (dr.Table.Columns.Contains("BucketId"))
            {
                entity.BucketId = dr["BucketId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["BucketId"];
            }
            if (dr.Table.Columns.Contains("SystemType"))
            {
                entity.SystemType = dr["SystemType"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SystemType"];
            }
            if (dr.Table.Columns.Contains("IsPrivate"))
            {
                entity.IsPrivate = dr["IsPrivate"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsPrivate"];
            }
            if (dr.Table.Columns.Contains("SourceTypeId"))
            {
                entity.SourceTypeId = dr["SourceTypeId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SourceTypeId"];
            }
            if (dr.Table.Columns.Contains("SourceName"))
            {
                entity.SourceName = dr["SourceName"].ToString();
            }
            if (dr.Table.Columns.Contains("SourceId"))
            {
                entity.SourceId = dr["SourceId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SourceId"];
            }
            if (dr.Table.Columns.Contains("LastAccesDate"))
            {
                entity.LastAccesDate = dr["LastAccesDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["LastAccesDate"];
            }
            if (dr.Table.Columns.Contains("AccessCount"))
            {
                entity.AccessCount = dr["AccessCount"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["AccessCount"];
            }
            if (dr.Table.Columns.Contains("SubBucketId"))
            {
                entity.SubBucketId = dr["SubBucketId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SubBucketId"];
            }
            if (dr.Table.Columns.Contains("FootageTypeId"))
            {
                entity.FootageTypeId = dr["FootageTypeId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["FootageTypeId"];
            }
            if (dr.Table.Columns.Contains("ServerName"))
            {
                entity.ServerName = dr["ServerName"].ToString();
            }
            if (dr.Table.Columns.Contains("AllowLowResTrancoding"))
            {
                entity.AllowLowResTrancoding = dr["AllowLowResTrancoding"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["AllowLowResTrancoding"];
            }
            if (dr.Table.Columns.Contains("isHD"))
            {
                entity.isHD = dr["isHD"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["isHD"];
            }
            return entity;
        }

    }


}
