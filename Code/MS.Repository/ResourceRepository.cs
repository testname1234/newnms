﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;
using MS.Core.Enums;

namespace MS.Repository
{

    public partial class ResourceRepository : ResourceRepositoryBase, IResourceRepository
    {
        public Resource GetResourceByGuidId(Guid guid)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where Guid=@Guid ";
            SqlParameter parameter = new SqlParameter("@Guid", guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public string GetResourcePathByGuid(Guid guid)
        {
            string sql = "select Source from Resource with (nolock)  where Guid=@Guid ";
            SqlParameter parameter = new SqlParameter("@Guid", guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ds.Tables[0].Rows[0][0].ToString();
        }


        public bool DeleteResourceByGuid(Guid guid)
        {
            string sql = "delete from Resource with (nolock) where Guid=@Guid";
            SqlParameter parameter = new SqlParameter("@Guid", guid);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }


        public Resource GetResourceBySource(string source)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where Source=@Source ";
            SqlParameter parameter = new SqlParameter("@Source", source);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Resource GetResourceBySourceAndBucketId(string source, int bucketId)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where Source=@Source and BucketId=@BucketId";
            SqlParameter parameter1 = new SqlParameter("@Source", source);
            SqlParameter parameter2 = new SqlParameter("@BucketId", bucketId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Resource GetResourceByFilePathAndBucketId(string filePath, int bucketId)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where FilePath=@FilePath and BucketId=@BucketId";
            SqlParameter parameter1 = new SqlParameter("@FilePath", filePath);
            SqlParameter parameter2 = new SqlParameter("@BucketId", bucketId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Resource> GetResourceIdFilePathandSubBucketIdByParent(int bucketId)
        {
            string sql = "select ResourceId,filepath,subbucketid,ResourceStatusId from Resource (nolock) where BucketId=@BucketId";
            SqlParameter parameter2 = new SqlParameter("@BucketId", bucketId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            List<Resource> resources = new List<Resource>();
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetResourceByCaption(string Caption)
        {
            string sql = GetResourceSelectClause();
            sql += " from Resource with (nolock) where Caption like '%@Caption%'";
            SqlParameter parameter = new SqlParameter("@Caption", Caption);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetNoThumbResources()
        {
            string sql = "select * from resource (nolock) where ResourceStatusId=4 and ThumbUrl='' and resourcetypeid=1 and SystemType=5";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetResourceByMeta(string Metastring)
        {
            string trimmedString = Metastring.Trim(new char[] { ',' });
            string[] arr = trimmedString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            DataSet ds1 = null;
            string query = "";
            //query = "select distinct top 5 r.* from Resource r with (nolock) inner join ResourceMeta rm (nolock) on r.Guid = rm.ResourceGuid where Value like '%'+@Value1+'%' or Value like '%'+@Value2+'%' or Value like '%'+@Value3+'%'";
            query = "select distinct top 2 r.* from Resource r with (nolock) inner join ResourceMeta rm (nolock) on r.Guid = rm.ResourceGuid where Value = @Value1 or Value = @Value2 or Value = @Value3 order by 1 desc";
            SqlParameter parameter1 = new SqlParameter("@Value1", arr[0]);
            SqlParameter parameter2 = new SqlParameter("@Value2", arr.Length <= 1 ? arr[0] : arr[1]);
            SqlParameter parameter3 = new SqlParameter("@Value3", arr.Length <= 2 ? arr[0] : arr[2]);
            ds1 = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, query, new SqlParameter[] { parameter1, parameter2, parameter3 });
            if (ds1 == null || ds1.Tables.Count != 1 || ds1.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds1, ResourceFromDataRow);
        }


        public Resource UpdateResourceDuration(Resource entity)
        {
            if (entity.IsTransient()) return entity;
            Resource other = GetResource(entity.ResourceId);
            if (entity.Equals(other)) return entity;
            string sql = @"Update Resource set 
							 Duration=@Duration 
							 where ResourceId=@ResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{					
					  new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceId",entity.ResourceId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetResource(entity.ResourceId);
        }

        public Resource UpdateResourceInfo(Resource entity)
        {
            if (entity.IsTransient()) return entity;
            Resource other = GetResource(entity.ResourceId);
            if (entity.Equals(other)) return entity;
            string sql = @"Update Resource set 
							[LastUpdateDate]=@LastUpdateDate							
							, [Caption]=@Caption
							, [Category]=@Category
							, [Location]=@Location 
							 where ResourceId=@ResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{					
					 new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)					
					, new SqlParameter("@Caption",entity.Caption ?? (object)DBNull.Value)
					, new SqlParameter("@Category",entity.Category ?? (object)DBNull.Value)
					, new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceId",entity.ResourceId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetResource(entity.ResourceId);
        }

        public List<Resource> GetResourceforDuration(DateTime resourceDate)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where (ResourceTypeId = @ResourceTypeId1 or ResourceTypeId = @ResourceTypeId2) and CreationDate >= @resourceDate";
            SqlParameter parameter1 = new SqlParameter("@resourceDate", resourceDate);
            SqlParameter parameter2 = new SqlParameter("@ResourceTypeId1", ResourceTypes.Audio);
            SqlParameter parameter3 = new SqlParameter("@ResourceTypeId2", ResourceTypes.Video);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2, parameter3 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public Resource GetResourceByFilePath(string FilePath, int bucketId)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where FilePath=@FilePath and BucketId=@BucketId";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@FilePath", FilePath), 
                new SqlParameter("@BucketId", bucketId) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Resource> GetResourceByBucketId(int bucketId)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where BucketId=@BucketId";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BucketId", bucketId) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }



        public List<Resource> GetAllResourcesByFilePath(string path)
        {
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where FilePath like %@FilePath%";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@FilePath", path) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }


        public List<Resource> GetAllResourcesByFilePath(string FilePath, int BucketId)
        {
            string sql = GetResourceSelectClause();
            FilePath = '%' + FilePath;

            // sql += "from Resource with (nolock)  where FilePath like @FilePath and BucketId=@BucketId";
            sql += "from Resource with (nolock)    where SUBSTRING(FilePath, 0,Len(FilePath)+1 - charIndex('\\',Reverse(FilePath))+1)   like  @FilePath and  BucketId = @BucketId";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BucketId", BucketId), new SqlParameter("@FilePath", FilePath) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetAllResourceWithDetailAndMetas()
        {
            //string sql = "SELECT top 10000 r.ResourceId, r.Guid, r.FilePath, r.FileName, r.highresolutionfile, r.LastUpdateDate, r.resourceTypeId, isnull(rd.Slug, id.description) as Slug , rd.Description, stuff((Select ' ' + value from ResourceMeta rm (nolock) where r.Guid = rm.ResourceGuid for xml path('')), 1, 1, '') as Metas FROM Resource r (nolock) LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid LEFT JOIN ImageDetail id (nolock) on r.resourceid = id.resourceid Where systemtype = 5 and r.resourcestatusid = 4 and r.IsPrivate = 0 Order by r.LastUpdateDate";
            string sql = @"SELECT top 30000 r.datestamp,r.ResourceId, r.Guid, r.FilePath, r.FileName, r.highresolutionfile, r.LastUpdateDate, r.resourceTypeId, r.bucketid, r.isprivate ,
                        isnull(rd.Slug, r.caption) as Slug , rd.Description,
                        isnull(rd.ResourceDate, r.CreationDate) as ResourceDate,
                        CASE    
                        WHEN  rd.ResourceId is null  THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT)
                        END AS IsCompleted,
                        stuff((Select ' ' + value from ResourceMeta rm (nolock) where r.Guid = rm.ResourceGuid for xml path('')), 1, 1, '') as Metas,
                        isHD = CASE WHEN isnull(r.isHD, 0) = 0 THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END,
                        r.CreationDate,rd.Barcode,k.Name as Keyword
                        FROM Resource r (nolock) 
                        LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid 
                        left Join RDKeyword rdkey on rd.ResourceDetailId=rdkey.ResourceDetailId
						left join Keyword  k on rdkey.KeywordId=k.KeywordId
                        Where r.resourcestatusid in (4,11)
                        order by r.datestamp ";
            SqlHelper.CommandTimeout = 1000000;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public void TestDateStamp()
        {
            string sql = @" SELECT top(5) r.datestamp from resource r  (nolock) order by r.datestamp desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            long val = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                val = BitConverter.ToInt64(dr["datestamp"] as byte[], 0);
            }

            sql = @" SELECT * from resource r (nolock) where r.datestamp>@datestamp";
            ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { new SqlParameter("@datestamp", BitConverter.GetBytes(val)) });
            var count = ds.Tables[0].Rows.Count;
            Console.WriteLine(count);
        }

        public List<Resource> GetResourceByGuids(List<string> resourceGuids)
        {
            if (resourceGuids.Count == 0)
                return null;
            string sql = "select r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType,rd.slug as Slug, rd.description as description, isnull(rd.ResourceDate, r.CreationDate) as ResourceDate,isnull(r.SystemType, 0) as SystemType , rd.barcode, isnull(r.isHD, 0) as isHD ";
            sql += "from Resource r (nolock) LEFT JOIN ResourceDetail rd (nolock) on r.Resourceid = rd.Resourceid Where r.Guid in ('" + string.Join("','", resourceGuids.ToArray()) + "')";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetResourcesAfterThisDate(long datestamp)
        {
            //string sql = "SELECT top 10000 r.ResourceId, r.Guid, r.FilePath, r.FileName, r.highresolutionfile, r.LastUpdateDate, r.resourceTypeId, isnull(rd.Slug, id.description) as Slug , rd.Description, stuff((Select ' ' + value from ResourceMeta rm (nolock) where r.Guid = rm.ResourceGuid for xml path('')), 1, 1, '') as Metas FROM Resource r (nolock) LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid LEFT JOIN ImageDetail id (nolock) on r.resourceid = id.resourceid Where systemtype = 5 and r.resourcestatusid = 4 and r.IsPrivate = 0 and r.LastUpdateDate > @LastUpdateDate Order by r.LastUpdateDate";
            string sql = @"SELECT top 10000 r.datestamp,r.ResourceId, r.Guid, r.FilePath, r.FileName, r.highresolutionfile, r.LastUpdateDate, r.resourceTypeId, r.bucketid, r.isprivate ,
                        isnull(rd.Slug, r.caption) as Slug , rd.Description,
                        isnull(rd.ResourceDate, r.CreationDate) as ResourceDate,
                        CASE WHEN  rd.ResourceId is null  THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT)
                        END AS IsCompleted,
                        stuff((Select ' ' + value from ResourceMeta rm (nolock) where r.Guid = rm.ResourceGuid for xml path('')), 1, 1, '') as Metas,
                        isHD = CASE WHEN isnull(r.isHD, 0) = 0 THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END,
                        r.CreationDate,rd.Barcode,k.Name as Keyword
                        FROM Resource r (nolock) 
                        LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid 
                        left  Join RDKeyword rdkey on rd.ResourceDetailId=rdkey.ResourceDetailId
						left join Keyword  k on rdkey.KeywordId=k.KeywordId
                        Where r.resourcestatusid  in (4,11) and r.datestamp > @datestamp
                        Order by r.datestamp";


            SqlHelper.CommandTimeout = 1000000;
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@datestamp", BitConverter.GetBytes(datestamp)) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetTop100Resources()
        {
            string sql = "select top 100 * from resource (nolock) where resourcestatusid = 4 order by 1 desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetByDate(DateTime CreationDate)
        {
            string sql = "select top 1000 * from resource (nolock) where resourcestatusid =11 and CreationDate >= @CreationDate order by CreationDate desc";
            SqlParameter parameter = new SqlParameter("@CreationDate", CreationDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public override Resource ResourceFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            Resource entity = base.ResourceFromDataRow(dr);

            if (dr.Table.Columns.Contains("DateStamp"))
            {
                entity.DateStamp = (dr.Table.Columns.Contains("DateStamp") && dr["DateStamp"] != DBNull.Value) ? (byte[])dr["DateStamp"] : new byte[0];
            }

            if (dr.Table.Columns.Contains("Slug"))
            {
                entity.Slug = dr["Slug"].ToString();
            }

            if (dr.Table.Columns.Contains("ResourceDate"))
            {
                entity.ResourceDate = dr["ResourceDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime)dr["ResourceDate"];
            }

            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("SystemType"))
            {
                entity.SystemType = dr["SystemType"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SystemType"];
            }

            if (dr.Table.Columns.Contains("Description"))
            {
                entity.Description = dr["Description"].ToString();
            }
            if (dr.Table.Columns.Contains("metas"))
            {
                entity.Metas = dr["metas"].ToString();
            }
            if (dr.Table.Columns.Contains("Keyword"))
            {
                entity.Keyword = dr["Keyword"].ToString();
            }

            if (dr.Table.Columns.Contains("Barcode"))
            {
                entity.Barcode = dr["Barcode"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["Barcode"];
            }

            if (dr.Table.Columns.Contains("IsCompleted"))
            {
                entity.IsCompleted = dr["IsCompleted"] == DBNull.Value ? (System.Boolean)false : (System.Boolean)dr["IsCompleted"];
            }
            if (dr.Table.Columns.Contains("isHD"))
            {
                entity.isHD = dr["isHD"] == DBNull.Value ? (System.Boolean)false : (System.Boolean)dr["isHD"];
            }

            if (dr.Table.Columns.Contains("datestamp"))
            {
                entity.TimeStamp = dr["datestamp"] == DBNull.Value ? null : (long?)BitConverter.ToInt64(dr["datestamp"] as byte[], 0);
            }

            return entity;
        }

        public Resource GetResourceByBarcode(string barcode)
        {
            string sql = "Select top 1 r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType,r.isHD";
            sql += " From Resource r (nolock) Left Join ResourceDetail rd (nolock) on r.ResourceId = rd.ResourceId where rd.Barcode = @Barcode";
            SqlParameter parameter = new SqlParameter("@Barcode", barcode);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Resource GetResourceByBarcodeAndResourceType(string barcode, int resourceTypeId)
        {
            string sql = "Select top 1 r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType,r.isHD";
            sql += " From Resource r (nolock) Left Join ResourceDetail rd (nolock) on r.ResourceId = rd.ResourceId where rd.Barcode = @Barcode and r.ResourceTypeId=@ResourceTypeId";
            SqlParameter parameter1 = new SqlParameter("@Barcode", barcode);
            SqlParameter parameter2 = new SqlParameter("@ResourceTypeId", resourceTypeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Resource> GetResourceByStatusId(int StatusId)
        {
            string sql = "Select distinct r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType, rd.Barcode";
            sql += " ,ISNULL(r.isHD , 0) as isHD from Resource r with (nolock) Inner Join ResourceDetail rd with (Nolock) on r.ResourceId = rd.ResourceId Where ResourceStatusId=@ResourceStatusId ";
            SqlParameter parameter = new SqlParameter("@ResourceStatusId", StatusId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetResourceByStatusId(int StatusId, int count)
        {
            string sql = "Select top " + count.ToString();
            sql += " r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType, rd.Barcode, r.isHD";
            sql += " from Resource r with (nolock) Left Join ResourceDetail rd with (Nolock) on r.ResourceId = rd.ResourceId Where ResourceStatusId=@ResourceStatusId ";
            SqlParameter parameter = new SqlParameter("@ResourceStatusId", StatusId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId)
        {
            string sql = "Select distinct r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType, rd.Barcode";
            sql += " from Resource r with (nolock) Left Join ResourceDetail rd with (Nolock) on r.ResourceId = rd.ResourceId Where ResourceStatusId=@ResourceStatusId and r.ResourceTypeId=@ResourceTypeId";
            SqlParameter parameter1 = new SqlParameter("@ResourceStatusId", StatusId);
            SqlParameter parameter2 = new SqlParameter("@ResourceTypeId", ResourceTypeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<string> GetAllCelebrityNames()
        {
            List<string> result = new List<string>();
            string sql = "select * from celebrity (nolock) order by createddate desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            else
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    result.Add(row["CelebrityName"].ToString());
                }
            }
            return result;
        }

        public List<string> GetAllCelebrityNames(DateTime lastExecutionDate)
        {
            List<string> result = new List<string>();
            string sql = "select * from celebrity (nolock) where  createddate > @createddate";
            SqlParameter parameter1 = new SqlParameter("@createddate", lastExecutionDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            else
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    result.Add(row["CelebrityName"].ToString());
                }
            }
            return result;
        }

        public List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId, int Count)
        {
            string sql = "Select distinct top " + Count.ToString();
            sql += " r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType, rd.Barcode";
            sql += " from Resource r with (nolock) Left Join ResourceDetail rd with (Nolock) on r.ResourceId = rd.ResourceId Where ResourceStatusId=@ResourceStatusId and r.ResourceTypeId=@ResourceTypeId";
            SqlParameter parameter1 = new SqlParameter("@ResourceStatusId", StatusId);
            SqlParameter parameter2 = new SqlParameter("@ResourceTypeId", ResourceTypeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetTop100Records()
        {
            string sql = "SELECT top 100 r.ResourceId, r.Guid, r.FilePath, r.FileName, r.highresolutionfile, r.LastUpdateDate, r.resourceTypeId, rd.Slug as Slug , rd.Description, stuff((Select ' ' + value from ResourceMeta rm (nolock) where r.Guid = rm.ResourceGuid for xml path('')), 1, 1, '') as Metas FROM Resource r (nolock) LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid Where systemtype = 5 and r.resourcestatusid = 4 and r.IsPrivate = 0 Order by r.LastUpdateDate";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        //public List<Resource> GetResourceByGuidIds(string guids)
        //{
        //    string listguid =   string.Join(",", guids);
        //    string sql = GetResourceSelectClause();
        //    sql += "from Resource with (nolock)  where Guid in (@Guid)";
        //    SqlParameter parameter = new SqlParameter("@Guid", listguid);
        //    DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
        //    if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
        //    return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        //}


        //public List<Resource> GetResourceByGuidIds(string guids)
        //{

        //    string sql = GetResourceSelectClause();
        //    sql += "from Resource with (nolock)  where Guid in (" + guids + ")";
        //   // SqlParameter parameter = new SqlParameter("@Guid", guids);
        //    DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
        //    if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
        //    return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);

        //}

        public List<Resource> GetResourceByGuidIds(List<string> list)
        {
            string listguids = string.Join("','", list);
            string sql = GetResourceSelectClause();
            sql += "from Resource with (nolock)  where Guid in ( '" + listguids + "' )";
            // SqlParameter parameter = new SqlParameter("@Guid", list);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetResourceByBarcodeRange(int start, int end)
        {

            string sql = @"SELECT  r.ResourceId, r.Guid, r.FilePath, r.FileName, r.highresolutionfile, r.LastUpdateDate, r.resourceTypeId, r.bucketid, r.isprivate ,
                        isnull(rd.Slug, r.caption) as Slug , rd.Description, 
                        isnull(rd.ResourceDate, r.CreationDate) as ResourceDate,
                        stuff((Select ' ' + value from ResourceMeta rm (nolock) where r.Guid = rm.ResourceGuid for xml path('')), 1, 1, '') as Metas 
                        FROM Resource r (nolock) 
                        LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid 
                        Where r.resourcestatusid = 4  and  rd.Barcode between @start and @end
                        order by r.LastUpdateDate";
            SqlHelper.CommandTimeout = 1000000;

            //string sql = "Select  r.ResourceId,r.Guid,r.ResourceTypeId,r.HighResolutionFile,r.HighResolutionFormatId,r.LowResolutionFile,r.LowResolutionFormatId,r.FileName,r.ResourceStatusId,r.ServerId,r.Source,r.ThumbUrl,r.CreationDate,r.LastUpdateDate,r.IsActive,r.Caption,r.Category,r.Location,r.FilePath,r.Duration,r.Bitrate,r.Framerate,r.AudioCodec,r.VideoCodec,r.Width,r.Height,r.BucketId,r.SystemType,r.isprivate";
            // sql += " From Resource r Left Join ResourceDetail rd on r.ResourceId = rd.ResourceId where rd.Barcode between @start and @end";
            SqlParameter parameter = new SqlParameter("@start", start);
            SqlParameter parameter2 = new SqlParameter("@end", end);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public Resource GetResourceByGuidIdforUserFavourite(Guid guid)
        {
            string sql = @"SELECT  r.ResourceId, r.Guid, r.FilePath, r.FileName, r.highresolutionfile, r.LastUpdateDate, r.resourceTypeId, r.bucketid, r.isprivate ,
                        isnull(rd.Slug, r.caption) as Slug  
                        FROM Resource r (nolock) 
                        LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid 
                        where r.guid  = @Guid";
            SqlParameter parameter = new SqlParameter("@Guid", guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Resource> GetResourcesa()
        {

            string sql = @"SELECT r.ResourceId FROM Resource r (nolock) LEFT JOIN ResourceDetail rd (nolock) on r.resourceid = rd.resourceid left join RDSource rs (nolock) on rd.ResourceDetailId = rs.ResourceDetailId Where rs.SourceTypeId = 6 Order by r.LastUpdateDate";
            SqlHelper.CommandTimeout = 1000000;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetFilepaths()
        {

            string sql = @"select resourceid, bucketid, REPLACE(REPLACE(filepath, REVERSE(SUBSTRING(REVERSE(filepath),1,CHARINDEX('\',REVERSE(filepath)))), ''), REVERSE(SUBSTRING(REVERSE(filepath),1,CHARINDEX('/',REVERSE(filepath)))), '') as FilePath
                            from resource (nolock) 
                            where isnull(filepath, '') <> '' and isnull(bucketid, 0) <> 0 and subbucketid is null
                            order by 2";
            SqlHelper.CommandTimeout = 1000000;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public void UpdateSubBucketId(int subBucketId, int resourceId)
        {
            string sql = "Update Resource set SubBucketId=@SubBucketId Where ResourceID=@ResourceID";
            SqlParameter parameter1 = new SqlParameter("@SubBucketId", subBucketId);
            SqlParameter parameter2 = new SqlParameter("@ResourceID", resourceId);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
        }

        public List<Resource> GetResourcesToFindInformation()
        {
            string sql = @"select * from resource (nolock) where Width is null and Height is null and isnull(filepath, '') <> '' and (resourceStatusid = 4 or creationdate > '2015-05-31') order by 1 desc";
            SqlHelper.CommandTimeout = 1000000;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetResourcesForLowResTranscoding(int count)
        {
			string sql = @"select top " + count.ToString() + " resourceid, DateStamp from resource (nolock) where LowResolutionFile = '' and AllowLowResTrancoding = 1 and servername is null Order by CreationDate desc";            SqlHelper.CommandTimeout = 1000000;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public List<Resource> GetAllLowResTranscodingResources()
        {
            string sql = @"select * from resource (nolock) where AllowLowResTrancoding = 1";
            SqlHelper.CommandTimeout = 1000000;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public Resource UpdateResourceWithTimeStamp(Resource entity)
        {
            string sql = @"Update Resource set [ServerName]=@ServerName
							 where ResourceId=@ResourceId and DateStamp=@DateStamp";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@ServerName",entity.ServerName)
					, new SqlParameter("@DateStamp",entity.DateStamp)
					, new SqlParameter("@ResourceId",entity.ResourceId)};
            SqlHelper.CommandTimeout = 1000000;
            var i = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (i == 1)
                return GetResource(entity.ResourceId);
            else
                return null;
        }

        public bool UpdateResourceServerName(string ServerName)
        {
            string sql = @"Update Resource set ServerName = null Where ServerName=@ServerName and AllowLowResTrancoding = 1";
            SqlParameter[] parameterArray = new SqlParameter[]{ new SqlParameter("@ServerName",ServerName) };
            var i = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (i > 1)
                return true;
            else
                return false;
        }


        public bool UpdateResourceStatus(int resourceId, int statusId)
        {
            string sql = @"Update Resource set ResourceStatusId=@StatusId,LastUpdateDate=getutcdate()
							 where ResourceId=@ResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@ResourceId",resourceId)
					, new SqlParameter("@StatusId",statusId)};
            var i = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (i == 1)
                return true;
            else
                return false;
        }



        public List<Resource> GetResourceByStatusIdAfterDate(long dateStamp, int statusid)
        {
            string sql = @"select * from [resource] with (nolock) where resourcestatusid = @StatusId  and datestamp > @datestamp order by datestamp";


            SqlHelper.CommandTimeout = 1000000;
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@datestamp", BitConverter.GetBytes(dateStamp)), new SqlParameter("@StatusId", statusid) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }


        public List<Resource> GetdeletedResources()
        {
            string sql = @"select  * from [resource] with (nolock) where resourcestatusid = 9";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }
    }


}
