﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class ResourceStatusRepositoryBase : Repository, IResourceStatusRepositoryBase 
	{
        
        public ResourceStatusRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ResourceStatusId",new SearchColumn(){Name="ResourceStatusId",Title="ResourceStatusId",SelectClause="ResourceStatusId",WhereClause="AllRecords.ResourceStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ResourceStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Status",new SearchColumn(){Name="Status",Title="Status",SelectClause="Status",WhereClause="AllRecords.Status",DataType="System.String",IsForeignColumn=false,PropertyName="Status",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetResourceStatusSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetResourceStatusBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetResourceStatusAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetResourceStatusSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ResourceStatus."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",ResourceStatus."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ResourceStatus GetResourceStatus(System.Int32 ResourceStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ResourceStatus with (nolock)  where ResourceStatusId=@ResourceStatusId ";
			SqlParameter parameter=new SqlParameter("@ResourceStatusId",ResourceStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ResourceStatusFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ResourceStatus> GetResourceStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ResourceStatus with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceStatus>(ds,ResourceStatusFromDataRow);
		}

		public virtual List<ResourceStatus> GetAllResourceStatus(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ResourceStatus with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceStatus>(ds, ResourceStatusFromDataRow);
		}

		public virtual List<ResourceStatus> GetPagedResourceStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetResourceStatusCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ResourceStatusId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ResourceStatusId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ResourceStatusId] ";
            tempsql += " FROM [ResourceStatus] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ResourceStatusId"))
					tempsql += " , (AllRecords.[ResourceStatusId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ResourceStatusId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetResourceStatusSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ResourceStatus] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ResourceStatus].[ResourceStatusId] = PageIndex.[ResourceStatusId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceStatus>(ds, ResourceStatusFromDataRow);
			}else{ return null;}
		}

		private int GetResourceStatusCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ResourceStatus as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ResourceStatus as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ResourceStatus))]
		public virtual ResourceStatus InsertResourceStatus(ResourceStatus entity)
		{

			ResourceStatus other=new ResourceStatus();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ResourceStatus ( [Status] )
				Values
				( @Status );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Status",entity.Status)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetResourceStatus(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ResourceStatus))]
		public virtual ResourceStatus UpdateResourceStatus(ResourceStatus entity)
		{

			if (entity.IsTransient()) return entity;
			ResourceStatus other = GetResourceStatus(entity.ResourceStatusId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ResourceStatus set  [Status]=@Status 
							 where ResourceStatusId=@ResourceStatusId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Status",entity.Status)
					, new SqlParameter("@ResourceStatusId",entity.ResourceStatusId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetResourceStatus(entity.ResourceStatusId);
		}

		public virtual bool DeleteResourceStatus(System.Int32 ResourceStatusId)
		{

			string sql="delete from ResourceStatus with (nolock) where ResourceStatusId=@ResourceStatusId";
			SqlParameter parameter=new SqlParameter("@ResourceStatusId",ResourceStatusId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ResourceStatus))]
		public virtual ResourceStatus DeleteResourceStatus(ResourceStatus entity)
		{
			this.DeleteResourceStatus(entity.ResourceStatusId);
			return entity;
		}


		public virtual ResourceStatus ResourceStatusFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ResourceStatus entity=new ResourceStatus();
			if (dr.Table.Columns.Contains("ResourceStatusId"))
			{
			entity.ResourceStatusId = (System.Int32)dr["ResourceStatusId"];
			}
			if (dr.Table.Columns.Contains("Status"))
			{
			entity.Status = dr["Status"].ToString();
			}
			return entity;
		}

	}
	
	
}
