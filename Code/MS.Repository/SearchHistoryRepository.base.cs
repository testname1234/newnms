﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class SearchHistoryRepositoryBase : Repository, ISearchHistoryRepositoryBase 
	{
        
        public SearchHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SearchHistoryId",new SearchColumn(){Name="SearchHistoryId",Title="SearchHistoryId",SelectClause="SearchHistoryId",WhereClause="AllRecords.SearchHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SearchHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SearchKeyword",new SearchColumn(){Name="SearchKeyword",Title="SearchKeyword",SelectClause="SearchKeyword",WhereClause="AllRecords.SearchKeyword",DataType="System.String",IsForeignColumn=false,PropertyName="SearchKeyword",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Frequency",new SearchColumn(){Name="Frequency",Title="Frequency",SelectClause="Frequency",WhereClause="AllRecords.Frequency",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Frequency",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSearchHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSearchHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSearchHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSearchHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SearchHistory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SearchHistory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual SearchHistory GetSearchHistory(System.Int32 SearchHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSearchHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SearchHistory] with (nolock)  where SearchHistoryId=@SearchHistoryId ";
			SqlParameter parameter=new SqlParameter("@SearchHistoryId",SearchHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SearchHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SearchHistory> GetSearchHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSearchHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SearchHistory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SearchHistory>(ds,SearchHistoryFromDataRow);
		}

		public virtual List<SearchHistory> GetAllSearchHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSearchHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SearchHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SearchHistory>(ds, SearchHistoryFromDataRow);
		}

		public virtual List<SearchHistory> GetPagedSearchHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSearchHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SearchHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SearchHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SearchHistoryId] ";
            tempsql += " FROM [SearchHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SearchHistoryId"))
					tempsql += " , (AllRecords.[SearchHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SearchHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSearchHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SearchHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SearchHistory].[SearchHistoryId] = PageIndex.[SearchHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SearchHistory>(ds, SearchHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetSearchHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SearchHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SearchHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SearchHistory))]
		public virtual SearchHistory InsertSearchHistory(SearchHistory entity)
		{

			SearchHistory other=new SearchHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SearchHistory ( [SearchKeyword]
				,[Frequency]
				,[CreationDate] )
				Values
				( @SearchKeyword
				, @Frequency
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SearchKeyword",entity.SearchKeyword ?? (object)DBNull.Value)
					, new SqlParameter("@Frequency",entity.Frequency ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSearchHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SearchHistory))]
		public virtual SearchHistory UpdateSearchHistory(SearchHistory entity)
		{

			if (entity.IsTransient()) return entity;
			SearchHistory other = GetSearchHistory(entity.SearchHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SearchHistory set  [SearchKeyword]=@SearchKeyword
							, [Frequency]=@Frequency
							, [CreationDate]=@CreationDate 
							 where SearchHistoryId=@SearchHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SearchKeyword",entity.SearchKeyword ?? (object)DBNull.Value)
					, new SqlParameter("@Frequency",entity.Frequency ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@SearchHistoryId",entity.SearchHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSearchHistory(entity.SearchHistoryId);
		}

		public virtual bool DeleteSearchHistory(System.Int32 SearchHistoryId)
		{

			string sql="delete from SearchHistory where SearchHistoryId=@SearchHistoryId";
			SqlParameter parameter=new SqlParameter("@SearchHistoryId",SearchHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SearchHistory))]
		public virtual SearchHistory DeleteSearchHistory(SearchHistory entity)
		{
			this.DeleteSearchHistory(entity.SearchHistoryId);
			return entity;
		}


		public virtual SearchHistory SearchHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SearchHistory entity=new SearchHistory();
			if (dr.Table.Columns.Contains("SearchHistoryId"))
			{
			entity.SearchHistoryId = (System.Int32)dr["SearchHistoryId"];
			}
			if (dr.Table.Columns.Contains("SearchKeyword"))
			{
			entity.SearchKeyword = dr["SearchKeyword"].ToString();
			}
			if (dr.Table.Columns.Contains("Frequency"))
			{
			entity.Frequency = dr["Frequency"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Frequency"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
