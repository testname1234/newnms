﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace MS.Repository
{
		
	public partial class SearchHistoryRepository: SearchHistoryRepositoryBase, ISearchHistoryRepository
	{

        public bool keywordExists(string term)
        {
            string sql = @"if exists (select * from [SearchHistory] with (nolock) where SearchKeyword  = @term)
                                      update  [SearchHistory] set Frequency+=1 where  SearchKeyword = @term;
	                                  select * from [SearchHistory] with (nolock) where SearchKeyword = @term";
            SqlParameter param  = new SqlParameter("@term",term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql,new SqlParameter[]{param});
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return false;
            return true;
        }


        public List<string> GetAllSearchHistoryKeywords()
        {
            List<string> result = new List<string>();
            string sql = "select SearchKeyword  from SearchHistory";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            else
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    result.Add(row["SearchKeyword"].ToString());
                }
            }
            return result;
        }


        public List<string> GetAllSearchHistoryKeywords(DateTime lastUpdateDate)
        {
            List<string> result = new List<string>();
            string sql = "select  SearchKeyword  from SearchHistory where creationDate > @creationDate";
            SqlParameter parameter1 = new SqlParameter("@creationDate", lastUpdateDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            else
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    result.Add(row["SearchKeyword"].ToString());
                }
            }
            return result;
        }
    }
	
	
}
