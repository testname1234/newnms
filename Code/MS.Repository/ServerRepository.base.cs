﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class ServerRepositoryBase : Repository, IServerRepositoryBase 
	{
        
        public ServerRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ServerId",new SearchColumn(){Name="ServerId",Title="ServerId",SelectClause="ServerId",WhereClause="AllRecords.ServerId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ServerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ServerIP",new SearchColumn(){Name="ServerIP",Title="ServerIP",SelectClause="ServerIP",WhereClause="AllRecords.ServerIP",DataType="System.String",IsForeignColumn=false,PropertyName="ServerIp",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsDefault",new SearchColumn(){Name="IsDefault",Title="IsDefault",SelectClause="IsDefault",WhereClause="AllRecords.IsDefault",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsDefault",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DefaultBucketPath",new SearchColumn(){Name="DefaultBucketPath",Title="DefaultBucketPath",SelectClause="DefaultBucketPath",WhereClause="AllRecords.DefaultBucketPath",DataType="System.String",IsForeignColumn=false,PropertyName="DefaultBucketPath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetServerSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetServerBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetServerAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetServerSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Server].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Server].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Server GetServer(System.Int32 ServerId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetServerSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Server] with (nolock)  where ServerId=@ServerId ";
			SqlParameter parameter=new SqlParameter("@ServerId",ServerId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ServerFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Server> GetServerByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetServerSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Server] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Server>(ds,ServerFromDataRow);
		}

		public virtual List<Server> GetAllServer(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetServerSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Server] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Server>(ds, ServerFromDataRow);
		}

		public virtual List<Server> GetPagedServer(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetServerCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ServerId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ServerId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ServerId] ";
            tempsql += " FROM [Server] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ServerId"))
					tempsql += " , (AllRecords.[ServerId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ServerId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetServerSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Server] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Server].[ServerId] = PageIndex.[ServerId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Server>(ds, ServerFromDataRow);
			}else{ return null;}
		}

		private int GetServerCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Server as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Server as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Server))]
		public virtual Server InsertServer(Server entity)
		{

			Server other=new Server();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Server ( [ServerIP]
				,[IsDefault]
				,[DefaultBucketPath]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @ServerIP
				, @IsDefault
				, @DefaultBucketPath
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ServerIP",entity.ServerIp)
					, new SqlParameter("@IsDefault",entity.IsDefault)
					, new SqlParameter("@DefaultBucketPath",entity.DefaultBucketPath ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetServer(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Server))]
		public virtual Server UpdateServer(Server entity)
		{

			if (entity.IsTransient()) return entity;
			Server other = GetServer(entity.ServerId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Server set  [ServerIP]=@ServerIP
							, [IsDefault]=@IsDefault
							, [DefaultBucketPath]=@DefaultBucketPath
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ServerId=@ServerId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ServerIP",entity.ServerIp)
					, new SqlParameter("@IsDefault",entity.IsDefault)
					, new SqlParameter("@DefaultBucketPath",entity.DefaultBucketPath ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ServerId",entity.ServerId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetServer(entity.ServerId);
		}

		public virtual bool DeleteServer(System.Int32 ServerId)
		{

			string sql="delete from Server where ServerId=@ServerId";
			SqlParameter parameter=new SqlParameter("@ServerId",ServerId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Server))]
		public virtual Server DeleteServer(Server entity)
		{
			this.DeleteServer(entity.ServerId);
			return entity;
		}


		public virtual Server ServerFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Server entity=new Server();
			if (dr.Table.Columns.Contains("ServerId"))
			{
			entity.ServerId = (System.Int32)dr["ServerId"];
			}
			if (dr.Table.Columns.Contains("ServerIP"))
			{
			entity.ServerIp = dr["ServerIP"].ToString();
			}
			if (dr.Table.Columns.Contains("IsDefault"))
			{
			entity.IsDefault = (System.Boolean)dr["IsDefault"];
			}
			if (dr.Table.Columns.Contains("DefaultBucketPath"))
			{
			entity.DefaultBucketPath = dr["DefaultBucketPath"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
