﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data;

namespace MS.Repository
{
		
	public partial class ServerRepository: ServerRepositoryBase, IServerRepository
	{
        public Server GetDefaultServer()
        {
            string sql = GetServerSelectClause();
            sql += "from server with (nolock) where IsDefault=1";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ServerFromDataRow(ds.Tables[0].Rows[0]);

        }        
	}
	
}
