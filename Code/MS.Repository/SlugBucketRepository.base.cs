﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class SlugBucketRepositoryBase : Repository, ISlugBucketRepositoryBase 
	{
        
        public SlugBucketRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlugBucketId",new SearchColumn(){Name="SlugBucketId",Title="SlugBucketId",SelectClause="SlugBucketId",WhereClause="AllRecords.SlugBucketId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlugBucketId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Slug",new SearchColumn(){Name="Slug",Title="Slug",SelectClause="Slug",WhereClause="AllRecords.Slug",DataType="System.String",IsForeignColumn=false,PropertyName="Slug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BucketId",new SearchColumn(){Name="BucketId",Title="BucketId",SelectClause="BucketId",WhereClause="AllRecords.BucketId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BucketId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlugBucketSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlugBucketBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlugBucketAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlugBucketSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlugBucket].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlugBucket].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual SlugBucket GetSlugBucket(System.Int32 SlugBucketId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlugBucketSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlugBucket] with (nolock)  where SlugBucketId=@SlugBucketId ";
			SqlParameter parameter=new SqlParameter("@SlugBucketId",SlugBucketId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlugBucketFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlugBucket> GetSlugBucketByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlugBucketSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlugBucket] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlugBucket>(ds,SlugBucketFromDataRow);
		}

		public virtual List<SlugBucket> GetAllSlugBucket(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlugBucketSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlugBucket] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlugBucket>(ds, SlugBucketFromDataRow);
		}

		public virtual List<SlugBucket> GetPagedSlugBucket(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlugBucketCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlugBucketId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlugBucketId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlugBucketId] ";
            tempsql += " FROM [SlugBucket] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlugBucketId"))
					tempsql += " , (AllRecords.[SlugBucketId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlugBucketId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlugBucketSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlugBucket] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlugBucket].[SlugBucketId] = PageIndex.[SlugBucketId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlugBucket>(ds, SlugBucketFromDataRow);
			}else{ return null;}
		}

		private int GetSlugBucketCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlugBucket as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlugBucket as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlugBucket))]
		public virtual SlugBucket InsertSlugBucket(SlugBucket entity)
		{

			SlugBucket other=new SlugBucket();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlugBucket ( [Slug]
				,[BucketId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Slug
				, @BucketId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@BucketId",entity.BucketId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlugBucket(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlugBucket))]
		public virtual SlugBucket UpdateSlugBucket(SlugBucket entity)
		{

			if (entity.IsTransient()) return entity;
			SlugBucket other = GetSlugBucket(entity.SlugBucketId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlugBucket set  [Slug]=@Slug
							, [BucketId]=@BucketId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where SlugBucketId=@SlugBucketId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@BucketId",entity.BucketId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@SlugBucketId",entity.SlugBucketId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlugBucket(entity.SlugBucketId);
		}

		public virtual bool DeleteSlugBucket(System.Int32 SlugBucketId)
		{

			string sql="delete from SlugBucket where SlugBucketId=@SlugBucketId";
			SqlParameter parameter=new SqlParameter("@SlugBucketId",SlugBucketId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlugBucket))]
		public virtual SlugBucket DeleteSlugBucket(SlugBucket entity)
		{
			this.DeleteSlugBucket(entity.SlugBucketId);
			return entity;
		}


		public virtual SlugBucket SlugBucketFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlugBucket entity=new SlugBucket();
			if (dr.Table.Columns.Contains("SlugBucketId"))
			{
			entity.SlugBucketId = (System.Int32)dr["SlugBucketId"];
			}
			if (dr.Table.Columns.Contains("Slug"))
			{
			entity.Slug = dr["Slug"].ToString();
			}
			if (dr.Table.Columns.Contains("BucketId"))
			{
			entity.BucketId = (System.Int32)dr["BucketId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
