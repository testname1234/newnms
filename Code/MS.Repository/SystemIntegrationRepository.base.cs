﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class SystemIntegrationRepositoryBase : Repository, ISystemIntegrationRepositoryBase 
	{
        
        public SystemIntegrationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ArchiveRetrievalDate",new SearchColumn(){Name="ArchiveRetrievalDate",Title="ArchiveRetrievalDate",SelectClause="ArchiveRetrievalDate",WhereClause="AllRecords.ArchiveRetrievalDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="ArchiveRetrievalDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Id",new SearchColumn(){Name="Id",Title="Id",SelectClause="Id",WhereClause="AllRecords.Id",DataType="System.Int32",IsForeignColumn=false,PropertyName="Id",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSystemIntegrationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSystemIntegrationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSystemIntegrationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSystemIntegrationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SystemIntegration].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SystemIntegration].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual SystemIntegration GetSystemIntegration(System.Int32 Id,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSystemIntegrationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SystemIntegration] with (nolock)  where Id=@Id ";
			SqlParameter parameter=new SqlParameter("@Id",Id);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SystemIntegrationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SystemIntegration> GetSystemIntegrationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSystemIntegrationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SystemIntegration] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SystemIntegration>(ds,SystemIntegrationFromDataRow);
		}

		public virtual List<SystemIntegration> GetAllSystemIntegration(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSystemIntegrationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SystemIntegration] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SystemIntegration>(ds, SystemIntegrationFromDataRow);
		}

		public virtual List<SystemIntegration> GetPagedSystemIntegration(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSystemIntegrationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [Id] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([Id])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [Id] ";
            tempsql += " FROM [SystemIntegration] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("Id"))
					tempsql += " , (AllRecords.[Id])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[Id])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSystemIntegrationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SystemIntegration] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SystemIntegration].[Id] = PageIndex.[Id] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SystemIntegration>(ds, SystemIntegrationFromDataRow);
			}else{ return null;}
		}

		private int GetSystemIntegrationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SystemIntegration as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SystemIntegration as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SystemIntegration))]
		public virtual SystemIntegration InsertSystemIntegration(SystemIntegration entity)
		{

			SystemIntegration other=new SystemIntegration();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SystemIntegration ( [ArchiveRetrievalDate] )
				Values
				( @ArchiveRetrievalDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ArchiveRetrievalDate",entity.ArchiveRetrievalDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSystemIntegration(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SystemIntegration))]
		public virtual SystemIntegration UpdateSystemIntegration(SystemIntegration entity)
		{

			if (entity.IsTransient()) return entity;
			SystemIntegration other = GetSystemIntegration(entity.Id);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SystemIntegration set  [ArchiveRetrievalDate]=@ArchiveRetrievalDate 
							 where Id=@Id";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ArchiveRetrievalDate",entity.ArchiveRetrievalDate ?? (object)DBNull.Value)
					, new SqlParameter("@Id",entity.Id)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSystemIntegration(entity.Id);
		}

		public virtual bool DeleteSystemIntegration(System.Int32 Id)
		{

			string sql="delete from SystemIntegration where Id=@Id";
			SqlParameter parameter=new SqlParameter("@Id",Id);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SystemIntegration))]
		public virtual SystemIntegration DeleteSystemIntegration(SystemIntegration entity)
		{
			this.DeleteSystemIntegration(entity.Id);
			return entity;
		}


		public virtual SystemIntegration SystemIntegrationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SystemIntegration entity=new SystemIntegration();
			if (dr.Table.Columns.Contains("ArchiveRetrievalDate"))
			{
			entity.ArchiveRetrievalDate = dr["ArchiveRetrievalDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["ArchiveRetrievalDate"];
			}
			if (dr.Table.Columns.Contains("Id"))
			{
			entity.Id = (System.Int32)dr["Id"];
			}
			return entity;
		}

	}
	
	
}
