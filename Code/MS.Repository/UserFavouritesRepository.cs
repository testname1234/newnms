﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace MS.Repository
{
		
	public partial class UserFavouritesRepository: UserFavouritesRepositoryBase, IUserFavouritesRepository
	{

        public List<UserFavourites> GetUserFavouritesByUserId(int UserId)
        {
            string sql = GetUserFavouritesSelectClause();
            sql += "from [UserFavourites] with (nolock)  where UserId = @UserId";
            SqlParameter param = new SqlParameter("@UserId", UserId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter [] {param});
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<UserFavourites>(ds, UserFavouritesFromDataRow);
        }

        public bool DeleteUserFavouritesbYUserIdandGuid(int UserId, string guid)
        {
            string sql = "delete from [UserFavourites]  where UserId = @UserId and ResourceGuid = @guid";
            SqlParameter param = new SqlParameter("@UserId", UserId);
            SqlParameter param2 = new SqlParameter("@guid", guid);
            bool isupdated = false;
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { param, param2 });
            isupdated = (Convert.ToInt32(identity)) == 1 ? true : false;
            return isupdated;
        }


        //public List<UserFavourites> DeleteUserFavouritesbYUserIdandGuid(int UserId, string guid)
        //{
        //    string sql = "delete from [UserFavourites]  where UserId = @UserId and ResourceGuid = @guid";
        //    SqlParameter param = new SqlParameter("@UserId", UserId);
        //    SqlParameter param2= new SqlParameter("@guid", guid);
        //    bool isupdated = false;
        //    var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { param, param2 });
        //    isupdated = (Convert.ToInt32(identity)) == 1 ? true : false;


        //    sql = GetUserFavouritesSelectClause();
        //    sql += "from [UserFavourites] with (nolock)  where ResourceGuid = @guid";
        //    DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { param, param2 });
        //    if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
        //    return CollectionFromDataSet<UserFavourites>(ds, UserFavouritesFromDataRow);
        //}
    }
}
