﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.Repository
{
		
	public abstract partial class VideoClipTaskRepositoryBase : Repository, IVideoClipTaskRepositoryBase 
	{
        
        public VideoClipTaskRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("VideoClipTaskId",new SearchColumn(){Name="VideoClipTaskId",Title="VideoClipTaskId",SelectClause="VideoClipTaskId",WhereClause="AllRecords.VideoClipTaskId",DataType="System.Int32",IsForeignColumn=false,PropertyName="VideoClipTaskId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("InputObjectJson",new SearchColumn(){Name="InputObjectJson",Title="InputObjectJson",SelectClause="InputObjectJson",WhereClause="AllRecords.InputObjectJson",DataType="System.String",IsForeignColumn=false,PropertyName="InputObjectJson",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetVideoClipTaskSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetVideoClipTaskBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetVideoClipTaskAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetVideoClipTaskSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[VideoClipTask].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[VideoClipTask].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual VideoClipTask GetVideoClipTask(System.Int32 VideoClipTaskId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoClipTaskSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [VideoClipTask] with (nolock)  where VideoClipTaskId=@VideoClipTaskId ";
			SqlParameter parameter=new SqlParameter("@VideoClipTaskId",VideoClipTaskId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return VideoClipTaskFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<VideoClipTask> GetVideoClipTaskByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoClipTaskSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [VideoClipTask] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoClipTask>(ds,VideoClipTaskFromDataRow);
		}

		public virtual List<VideoClipTask> GetAllVideoClipTask(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoClipTaskSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [VideoClipTask] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoClipTask>(ds, VideoClipTaskFromDataRow);
		}

		public virtual List<VideoClipTask> GetPagedVideoClipTask(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetVideoClipTaskCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [VideoClipTaskId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([VideoClipTaskId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [VideoClipTaskId] ";
            tempsql += " FROM [VideoClipTask] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("VideoClipTaskId"))
					tempsql += " , (AllRecords.[VideoClipTaskId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[VideoClipTaskId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetVideoClipTaskSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [VideoClipTask] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [VideoClipTask].[VideoClipTaskId] = PageIndex.[VideoClipTaskId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoClipTask>(ds, VideoClipTaskFromDataRow);
			}else{ return null;}
		}

		private int GetVideoClipTaskCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM VideoClipTask as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM VideoClipTask as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(VideoClipTask))]
		public virtual VideoClipTask InsertVideoClipTask(VideoClipTask entity)
		{

			VideoClipTask other=new VideoClipTask();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into VideoClipTask ( [ResourceGuid]
				,[InputObjectJson]
				,[StatusId]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @ResourceGuid
				, @InputObjectJson
				, @StatusId
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@InputObjectJson",entity.InputObjectJson)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetVideoClipTask(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(VideoClipTask))]
		public virtual VideoClipTask UpdateVideoClipTask(VideoClipTask entity)
		{

			if (entity.IsTransient()) return entity;
			VideoClipTask other = GetVideoClipTask(entity.VideoClipTaskId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update VideoClipTask set  [ResourceGuid]=@ResourceGuid
							, [InputObjectJson]=@InputObjectJson
							, [StatusId]=@StatusId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where VideoClipTaskId=@VideoClipTaskId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@InputObjectJson",entity.InputObjectJson)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@VideoClipTaskId",entity.VideoClipTaskId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetVideoClipTask(entity.VideoClipTaskId);
		}

		public virtual bool DeleteVideoClipTask(System.Int32 VideoClipTaskId)
		{

			string sql="delete from VideoClipTask where VideoClipTaskId=@VideoClipTaskId";
			SqlParameter parameter=new SqlParameter("@VideoClipTaskId",VideoClipTaskId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(VideoClipTask))]
		public virtual VideoClipTask DeleteVideoClipTask(VideoClipTask entity)
		{
			this.DeleteVideoClipTask(entity.VideoClipTaskId);
			return entity;
		}


		public virtual VideoClipTask VideoClipTaskFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			VideoClipTask entity=new VideoClipTask();
			if (dr.Table.Columns.Contains("VideoClipTaskId"))
			{
			entity.VideoClipTaskId = (System.Int32)dr["VideoClipTaskId"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = dr["ResourceGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("InputObjectJson"))
			{
			entity.InputObjectJson = dr["InputObjectJson"].ToString();
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
