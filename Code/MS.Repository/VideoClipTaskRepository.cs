﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace MS.Repository
{
		
	public partial class VideoClipTaskRepository: VideoClipTaskRepositoryBase, IVideoClipTaskRepository
	{
        public List<VideoClipTask> GetVideoClipByStatusId(int VideoClipStatusId)
        {

            string sql = GetVideoClipTaskSelectClause();
            sql += "from VideoClipTask with (nolock)  where StatusId=@StatusId";
            SqlParameter parameter = new SqlParameter("@StatusId", VideoClipStatusId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<VideoClipTask>(ds, VideoClipTaskFromDataRow);
        }
	}
	
	
}
