﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.Extensions;

namespace MS.ScrapRepository
{
		
	public abstract partial class ScrapResourceRepositoryBase : Repository, IScrapResourceRepositoryBase 
	{
        
        public ScrapResourceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ScrapResourceId",new SearchColumn(){Name="ScrapResourceId",Title="ScrapResourceId",SelectClause="ScrapResourceId",WhereClause="AllRecords.ScrapResourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScrapResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.Guid",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Source",new SearchColumn(){Name="Source",Title="Source",SelectClause="Source",WhereClause="AllRecords.Source",DataType="System.String",IsForeignColumn=false,PropertyName="Source",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DownloadedFilePath",new SearchColumn(){Name="DownloadedFilePath",Title="DownloadedFilePath",SelectClause="DownloadedFilePath",WhereClause="AllRecords.DownloadedFilePath",DataType="System.String",IsForeignColumn=false,PropertyName="DownloadedFilePath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SystemType",new SearchColumn(){Name="SystemType",Title="SystemType",SelectClause="SystemType",WhereClause="AllRecords.SystemType",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SystemType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetScrapResourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetScrapResourceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetScrapResourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetScrapResourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ScrapResource].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ScrapResource].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ScrapResource GetScrapResource(System.Int32 ScrapResourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScrapResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScrapResource] with (nolock)  where ScrapResourceId=@ScrapResourceId ";
			SqlParameter parameter=new SqlParameter("@ScrapResourceId",ScrapResourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ScrapResourceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ScrapResource> GetScrapResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScrapResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ScrapResource] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScrapResource>(ds,ScrapResourceFromDataRow);
		}

		public virtual List<ScrapResource> GetAllScrapResource(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScrapResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScrapResource] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
		}

		public virtual List<ScrapResource> GetPagedScrapResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetScrapResourceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ScrapResourceId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ScrapResourceId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ScrapResourceId] ";
            tempsql += " FROM [ScrapResource] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ScrapResourceId"))
					tempsql += " , (AllRecords.[ScrapResourceId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ScrapResourceId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetScrapResourceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ScrapResource] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ScrapResource].[ScrapResourceId] = PageIndex.[ScrapResourceId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
			}else{ return null;}
		}

		private int GetScrapResourceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ScrapResource as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ScrapResource as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ScrapResource))]
		public virtual ScrapResource InsertScrapResource(ScrapResource entity)
		{

			ScrapResource other=new ScrapResource();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ScrapResource ( [ResourceGuid]
				,[Source]
				,[DownloadedFilePath]
				,[StatusId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[SystemType] )
				Values
				( @ResourceGuid
				, @Source
				, @DownloadedFilePath
				, @StatusId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @SystemType );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@Source",entity.Source)
					, new SqlParameter("@DownloadedFilePath",entity.DownloadedFilePath ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@SystemType",entity.SystemType ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetScrapResource(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ScrapResource))]
		public virtual ScrapResource UpdateScrapResource(ScrapResource entity)
		{

			if (entity.IsTransient()) return entity;
			ScrapResource other = GetScrapResource(entity.ScrapResourceId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ScrapResource set  [ResourceGuid]=@ResourceGuid
							, [Source]=@Source
							, [DownloadedFilePath]=@DownloadedFilePath
							, [StatusId]=@StatusId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [SystemType]=@SystemType 
							 where ScrapResourceId=@ScrapResourceId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@Source",entity.Source)
					, new SqlParameter("@DownloadedFilePath",entity.DownloadedFilePath ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@SystemType",entity.SystemType ?? (object)DBNull.Value)
					, new SqlParameter("@ScrapResourceId",entity.ScrapResourceId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetScrapResource(entity.ScrapResourceId);
		}

		public virtual bool DeleteScrapResource(System.Int32 ScrapResourceId)
		{

			string sql="delete from ScrapResource where ScrapResourceId=@ScrapResourceId";
			SqlParameter parameter=new SqlParameter("@ScrapResourceId",ScrapResourceId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ScrapResource))]
		public virtual ScrapResource DeleteScrapResource(ScrapResource entity)
		{
			this.DeleteScrapResource(entity.ScrapResourceId);
			return entity;
		}


		public virtual ScrapResource ScrapResourceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ScrapResource entity=new ScrapResource();
			if (dr.Table.Columns.Contains("ScrapResourceId"))
			{
			entity.ScrapResourceId = (System.Int32)dr["ScrapResourceId"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = (System.Guid)dr["ResourceGuid"];
			}
			if (dr.Table.Columns.Contains("Source"))
			{
			entity.Source = dr["Source"].ToString();
			}
			if (dr.Table.Columns.Contains("DownloadedFilePath"))
			{
			entity.DownloadedFilePath = dr["DownloadedFilePath"].ToString();
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("SystemType"))
			{
			entity.SystemType = dr["SystemType"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SystemType"];
			}
			return entity;
		}

	}
	
	
}
