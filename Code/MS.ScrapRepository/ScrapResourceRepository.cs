﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace MS.ScrapRepository
{

    public partial class ScrapResourceRepository : ScrapResourceRepositoryBase, IScrapResourceRepository
    {

        public List<ScrapResource> GetScrapResourceByStatusId(Core.Enums.ScrapResourceStatuses? status)
        {
            if (status == null)
            {
                string sql = GetScrapResourceSelectClause();
                sql += "from ScrapResource with (nolock)  where StatusId in (4,5,6)";
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
            }
            else
            {
                string sql = GetScrapResourceSelectClause();
                sql += "from ScrapResource with (nolock)  where StatusId=@StatusId  ";
                SqlParameter parameter = new SqlParameter("@StatusId", (int)status);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
            }
        }

        public List<ScrapResource> GetScrapResourceByStatusIdAndSystemTypeId(Core.Enums.ScrapResourceStatuses? status, int? SystemTypeId)
        {
            if (status == null)
            {
                string sql = GetScrapResourceSelectClause();
                sql += "from ScrapResource with (nolock)  where StatusId in (4,5,6) and isnull(SystemType,0)=" + SystemTypeId;
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
            }
            else
            {
                string sql = GetScrapResourceSelectClause();
                sql += "from ScrapResource with (nolock)  where StatusId=@StatusId and isnull(SystemType,0)=" + SystemTypeId;
                SqlParameter parameter = new SqlParameter("@StatusId", (int)status);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
            }
        }

        public List<ScrapResource> GetDownloadedResources()
        {
            string sql = GetScrapResourceSelectClause();
            sql += "from ScrapResource with (nolock)  where StatusId in (2,8,9)";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
        }

        public List<ScrapResource> GetScrapResourceByDate(DateTime CreationDate)
        {
            string sql = GetScrapResourceSelectClause();
            sql += "from ScrapResource with (nolock)  where CreationDate <= @date";
            SqlParameter parameter2 = new SqlParameter("@date", CreationDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql,new SqlParameter[]{parameter2});
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
        }

        public List<ScrapResource> GetScrapResourceByStatusIdAndDate(Core.Enums.ScrapResourceStatuses status, DateTime date)
        {
            string sql = GetScrapResourceSelectClause();
            sql += "from ScrapResource with (nolock)  where StatusId=@StatusId and CreationDate >= @date";
            SqlParameter parameter1 = new SqlParameter("@StatusId", (int)status);
            SqlParameter parameter2 = new SqlParameter("@date", date);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ScrapResource>(ds, ScrapResourceFromDataRow);
        }

        public void UpdateScrapResourceNoReturn(ScrapResource entity)
        {
            string sql = @"Update ScrapResource set  [ResourceGuid]=@ResourceGuid
							, [Source]=@Source
							, [DownloadedFilePath]=@DownloadedFilePath
							, [StatusId]=@StatusId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ScrapResourceId=@ScrapResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@Source",entity.Source)
					, new SqlParameter("@DownloadedFilePath",entity.DownloadedFilePath)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ScrapResourceId",entity.ScrapResourceId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }
    }


}
