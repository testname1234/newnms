﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.BucketAuthorization;
using Validation;
using System.Linq;

namespace MS.Service
{
		
	public class BucketAuthorizationService : IBucketAuthorizationService 
	{
		private IBucketAuthorizationRepository _iBucketAuthorizationRepository;
        
		public BucketAuthorizationService(IBucketAuthorizationRepository iBucketAuthorizationRepository)
		{
			this._iBucketAuthorizationRepository = iBucketAuthorizationRepository;
		}
        
        public Dictionary<string, string> GetBucketAuthorizationBasicSearchColumns()
        {
            
            return this._iBucketAuthorizationRepository.GetBucketAuthorizationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetBucketAuthorizationAdvanceSearchColumns()
        {
            
            return this._iBucketAuthorizationRepository.GetBucketAuthorizationAdvanceSearchColumns();
           
        }
        

		public BucketAuthorization GetBucketAuthorization(System.Int32 BucketAuthorizationId)
		{
			return _iBucketAuthorizationRepository.GetBucketAuthorization(BucketAuthorizationId);
		}

		public BucketAuthorization UpdateBucketAuthorization(BucketAuthorization entity)
		{
			return _iBucketAuthorizationRepository.UpdateBucketAuthorization(entity);
		}

		public bool DeleteBucketAuthorization(System.Int32 BucketAuthorizationId)
		{
			return _iBucketAuthorizationRepository.DeleteBucketAuthorization(BucketAuthorizationId);
		}

		public List<BucketAuthorization> GetAllBucketAuthorization()
		{
			return _iBucketAuthorizationRepository.GetAllBucketAuthorization();
		}

		public BucketAuthorization InsertBucketAuthorization(BucketAuthorization entity)
		{
			 return _iBucketAuthorizationRepository.InsertBucketAuthorization(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bucketauthorizationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bucketauthorizationid))
            {
				BucketAuthorization bucketauthorization = _iBucketAuthorizationRepository.GetBucketAuthorization(bucketauthorizationid);
                if(bucketauthorization!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bucketauthorization);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<BucketAuthorization> bucketauthorizationlist = _iBucketAuthorizationRepository.GetAllBucketAuthorization();
            if (bucketauthorizationlist != null && bucketauthorizationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bucketauthorizationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                BucketAuthorization bucketauthorization = new BucketAuthorization();
                PostOutput output = new PostOutput();
                bucketauthorization.CopyFrom(Input);
                bucketauthorization = _iBucketAuthorizationRepository.InsertBucketAuthorization(bucketauthorization);
                output.CopyFrom(bucketauthorization);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BucketAuthorization bucketauthorizationinput = new BucketAuthorization();
                BucketAuthorization bucketauthorizationoutput = new BucketAuthorization();
                PutOutput output = new PutOutput();
                bucketauthorizationinput.CopyFrom(Input);
                BucketAuthorization bucketauthorization = _iBucketAuthorizationRepository.GetBucketAuthorization(bucketauthorizationinput.BucketAuthorizationId);
                if (bucketauthorization!=null)
                {
                    bucketauthorizationoutput = _iBucketAuthorizationRepository.UpdateBucketAuthorization(bucketauthorizationinput);
                    if(bucketauthorizationoutput!=null)
                    {
                        output.CopyFrom(bucketauthorizationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bucketauthorizationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bucketauthorizationid))
            {
				 bool IsDeleted = _iBucketAuthorizationRepository.DeleteBucketAuthorization(bucketauthorizationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
