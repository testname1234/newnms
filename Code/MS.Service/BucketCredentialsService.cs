﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.BucketCredentials;
using Validation;
using System.Linq;
using MS.Core;

namespace MS.Service
{
		
	public class BucketCredentialsService : IBucketCredentialsService 
	{
		private IBucketCredentialsRepository _iBucketCredentialsRepository;
        
		public BucketCredentialsService(IBucketCredentialsRepository iBucketCredentialsRepository)
		{
			this._iBucketCredentialsRepository = iBucketCredentialsRepository;
		}
        
        public Dictionary<string, string> GetBucketCredentialsBasicSearchColumns()
        {
            
            return this._iBucketCredentialsRepository.GetBucketCredentialsBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetBucketCredentialsAdvanceSearchColumns()
        {
            
            return this._iBucketCredentialsRepository.GetBucketCredentialsAdvanceSearchColumns();
           
        }
        

		public virtual List<BucketCredentials> GetBucketCredentialsByBucketId(System.Int32 BucketId)
		{
			return _iBucketCredentialsRepository.GetBucketCredentialsByBucketId(BucketId);
		}

		public BucketCredentials GetBucketCredentials(System.Int32 BucketCredentialId)
		{
			return _iBucketCredentialsRepository.GetBucketCredentials(BucketCredentialId);
		}

		public BucketCredentials UpdateBucketCredentials(BucketCredentials entity)
		{
			return _iBucketCredentialsRepository.UpdateBucketCredentials(entity);
		}

		public bool DeleteBucketCredentials(System.Int32 BucketCredentialId)
		{
			return _iBucketCredentialsRepository.DeleteBucketCredentials(BucketCredentialId);
		}

		public List<BucketCredentials> GetAllBucketCredentials()
		{
			return _iBucketCredentialsRepository.GetAllBucketCredentials();
		}

		public BucketCredentials InsertBucketCredentials(BucketCredentials entity)
		{
			 return _iBucketCredentialsRepository.InsertBucketCredentials(entity);
		}

        public BucketCredentials GetBucketCredentialsByApiKey(int bucketId, string apiKey)
        {
            return _iBucketCredentialsRepository.GetBucketCredentialsByApiKey(bucketId, apiKey);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bucketcredentialid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bucketcredentialid))
            {
				BucketCredentials bucketcredentials = _iBucketCredentialsRepository.GetBucketCredentials(bucketcredentialid);
                if(bucketcredentials!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bucketcredentials);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<BucketCredentials> bucketcredentialslist = _iBucketCredentialsRepository.GetAllBucketCredentials();
            if (bucketcredentialslist != null && bucketcredentialslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bucketcredentialslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                BucketCredentials bucketcredentials = new BucketCredentials();
                PostOutput output = new PostOutput();
                bucketcredentials.CopyFrom(Input);
                bucketcredentials = _iBucketCredentialsRepository.InsertBucketCredentials(bucketcredentials);
                output.CopyFrom(bucketcredentials);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BucketCredentials bucketcredentialsinput = new BucketCredentials();
                BucketCredentials bucketcredentialsoutput = new BucketCredentials();
                PutOutput output = new PutOutput();
                bucketcredentialsinput.CopyFrom(Input);
                BucketCredentials bucketcredentials = _iBucketCredentialsRepository.GetBucketCredentials(bucketcredentialsinput.BucketCredentialId);
                if (bucketcredentials!=null)
                {
                    bucketcredentialsoutput = _iBucketCredentialsRepository.UpdateBucketCredentials(bucketcredentialsinput);
                    if(bucketcredentialsoutput!=null)
                    {
                        output.CopyFrom(bucketcredentialsoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bucketcredentialid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bucketcredentialid))
            {
				 bool IsDeleted = _iBucketCredentialsRepository.DeleteBucketCredentials(bucketcredentialid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public BucketCredentials GetBucketByLoginIdAndApiKey(string loginid, string apikey)
         {
             return _iBucketCredentialsRepository.GetBucketByLoginIdAndApiKey(loginid, apikey);
         }
    }
	
	
}
