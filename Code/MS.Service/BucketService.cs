﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Bucket;
using Validation;
using System.Linq;
using MS.Core;
using MS.Core.Models;
using System.Configuration;

namespace MS.Service
{
		
	public class BucketService : IBucketService 
	{
		private IBucketRepository _iBucketRepository;
        
		public BucketService(IBucketRepository iBucketRepository)
		{
			this._iBucketRepository = iBucketRepository;
		}
        
        public Dictionary<string, string> GetBucketBasicSearchColumns()
        {
            
            return this._iBucketRepository.GetBucketBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetBucketAdvanceSearchColumns()
        {
            
            return this._iBucketRepository.GetBucketAdvanceSearchColumns();
           
        }
        

		public Bucket GetBucket(System.Int32 BucketId)
		{
			return _iBucketRepository.GetBucket(BucketId);
		}

		public Bucket UpdateBucket(Bucket entity)
		{
			return _iBucketRepository.UpdateBucket(entity);
		}        

		public bool DeleteBucket(System.Int32 BucketId)
		{
			return _iBucketRepository.DeleteBucket(BucketId);
		}

		public List<Bucket> GetAllBucket()
		{
			return _iBucketRepository.GetAllBucket();
		}

		public Bucket InsertBucket(Bucket entity)
		{
			 return _iBucketRepository.InsertBucket(entity);
		}

        //public Bucket InsertBucket(PostInput input)
        //{
        //    Bucket bucket = new Bucket();
        //    bucket.Name = input.Name;
        //    bucket.Path = input.Path;

        //}

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bucketid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bucketid))
            {
				Bucket bucket = _iBucketRepository.GetBucket(bucketid);
                if(bucket!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bucket);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Bucket> bucketlist = _iBucketRepository.GetAllBucket();
            if (bucketlist != null && bucketlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bucketlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Bucket bucket = new Bucket();
                PostOutput output = new PostOutput();
                bucket.CopyFrom(Input);
                bucket = _iBucketRepository.InsertBucket(bucket);
                output.CopyFrom(bucket);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Bucket bucketinput = new Bucket();
                Bucket bucketoutput = new Bucket();
                PutOutput output = new PutOutput();
                bucketinput.CopyFrom(Input);
                Bucket bucket = _iBucketRepository.GetBucket(bucketinput.BucketId);
                if (bucket!=null)
                {
                    bucketoutput = _iBucketRepository.UpdateBucket(bucketinput);
                    if(bucketoutput!=null)
                    {
                        output.CopyFrom(bucketoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bucketid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bucketid))
            {
				 bool IsDeleted = _iBucketRepository.DeleteBucket(bucketid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public List<Bucket> GetBucketByParentId(int parentId)
         {
             return _iBucketRepository.GetBucketByParentId(parentId);
         }

        public List<FolderModel> GetAllBucketWithParentName(int userId)
        {
            List<FolderModel> folders = new List<FolderModel>();
            var buckets = _iBucketRepository.GetAllBucketWithParentName(userId);

            DateTime dt = DateTime.UtcNow;
            foreach (var bucket in buckets)
            {
                var f = GetSubFolders(bucket, buckets, bucket.IsRead.HasValue? bucket.IsRead.Value:false, bucket.IsWrite.HasValue ? bucket.IsWrite.Value : false, null);
                if (f != null)
                    folders.Add(f);
            }
            System.Diagnostics.Debug.WriteLine(" Time=" + DateTime.UtcNow.Subtract(dt).TotalMilliseconds);
            if (folders.Count > 0)
            {
                folders.First().UserBuckets = new List<FolderModel>();
                foreach (var folder in folders)
                {
                    if (folder.IsWrite)
                    {
                        folders.First().UserBuckets.Add(new FolderModel() { Name = folder.Name, DisplayName = folder.DisplayName, ApiKey = folder.ApiKey, BucketID = folder.BucketID });
                    }
                }
            }
            return folders;
        }

         private FolderModel GetSubFolders(Bucket bucket, List<Bucket> buckets,bool isParentRead,bool isParentWrite,string apiKey)
         {
             FolderModel folder = new FolderModel();
             folder.ApiKey = !string.IsNullOrEmpty(bucket.ApiKey) ? bucket.ApiKey : apiKey;
             folder.BucketID = bucket.BucketId;
             folder.Name = string.IsNullOrEmpty(bucket.Path) ? bucket.Name : bucket.Path;
             folder.DisplayName = folder.Name.Split(new string[] { "\\", "/" }, StringSplitOptions.RemoveEmptyEntries).Last();
             folder.Folders = new List<FolderModel>();
             folder.IsRead = bucket.IsRead ?? isParentRead;
             folder.IsWrite = bucket.IsWrite ?? isParentWrite;
             if (folder.IsRead || folder.IsWrite)
             {
                 var bucks = buckets.Where(x => x.Path.StartsWith(folder.Name) && x.Path.Count(y => y == '/' || y == '\\') == folder.Name.Count(y => y == '/' || y == '\\') + 1).ToList();
                 for (int i = 0; i < bucks.Count(); i++)
                 {
                     var f = GetSubFolders(bucks[i], buckets, folder.IsRead, folder.IsWrite, folder.ApiKey);
                     if (f != null)
                         folder.Folders.Add(f);
                 }                
                 return folder;
             }
             else return null;
         }

         public Bucket GetBucketByName(string p)
         {
             return _iBucketRepository.GetBucketByName(p);
         }

         public Bucket GetBucketByPath(string p)
         {
             return _iBucketRepository.GetBucketByPath(p);
         }
    

         public Bucket GetBucketByApiKey(int bucketId, string apiKey)
         {
             return _iBucketRepository.GetBucketByApiKey(bucketId, apiKey);
         }

         public Bucket GetSubBucket(string name, int parentId)
         {
             return _iBucketRepository.GetSubBucket(name, parentId);
         }

         public Bucket GetBucketAndCredentials(int bucketId)
         {
             return _iBucketRepository.GetBucketAndCredentials(bucketId);
         }


         public List<ResourceModel> GetSubBucketResources(int bucketId)
         {
             List<Bucket> buckets = GetAllBucket();

             List<ResourceModel> resources = _iBucketRepository.GetSubBucketResources(bucketId);
             if (resources != null)
             {
                 foreach (var res in resources)
                 {
                     var server = LocalCache.Cache.Servers.Where(x => x.ServerId == res.ServerId).FirstOrDefault();
                     var bucket = buckets.Where(x => x.BucketId == res.BucketId).FirstOrDefault();
                     if (bucket != null && server != null)
                     {
                         if (!string.IsNullOrEmpty(res.DownloadLowRes) || !string.IsNullOrEmpty(res.DownloadHighRes))
                         {
                             if (!string.IsNullOrEmpty(res.DownloadLowRes))
                                 res.DownloadLowRes = "http:\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationLowRes"] + "\\" + bucket.Path + "\\" + res.DownloadLowRes;
                             else res.DownloadLowRes = "file:\\" + res.Source;
                            if (!string.IsNullOrEmpty(res.DownloadHighRes))
                                 res.DownloadHighRes = "http:\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocation"] + "\\" + bucket.Path + "\\" + res.DownloadHighRes;
                            else res.DownloadHighRes = "file:\\" + res.Source;
                        }
                         else
                         {
                             res.DownloadLowRes = "file:" + res.Source;
                             res.DownloadHighRes = "file:" + res.Source;
                         }
                         res.ThumbUrl = "http:\\\\" + server.ServerIp + "\\" + ConfigurationManager.AppSettings["MediaLocationThumb"] + "\\" + bucket.Path + "\\" + res.ThumbUrl;
                     }
                     if (res.Title != null && res.Title.Length > 25)
                     {
                         res.ClippedTitle = res.Title.Substring(0, 25);
                     }
                     else
                     {
                         res.ClippedTitle = res.Title;
                     }
                 }
             }
             return resources;
         }

         public List<Bucket> GetBucketsToTranscode()
         {
             return _iBucketRepository.GetBucketsToTranscode();
         }

         public List<Bucket> GetBucketsByParentId(int parentId)
         {
             return _iBucketRepository.GetBucketsByParentId(parentId);
         }
    }
	
	
}
