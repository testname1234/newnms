﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.CodecConfig;
using Validation;
using System.Linq;
using MS.Core;

namespace MS.Service
{
		
	public class CodecConfigService : ICodecConfigService 
	{
		private ICodecConfigRepository _iCodecConfigRepository;
        
		public CodecConfigService(ICodecConfigRepository iCodecConfigRepository)
		{
			this._iCodecConfigRepository = iCodecConfigRepository;
		}
        
        public Dictionary<string, string> GetCodecConfigBasicSearchColumns()
        {
            
            return this._iCodecConfigRepository.GetCodecConfigBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCodecConfigAdvanceSearchColumns()
        {
            
            return this._iCodecConfigRepository.GetCodecConfigAdvanceSearchColumns();
           
        }
        

		public CodecConfig GetCodecConfig(System.Int32 CodecConfigId)
		{
			return _iCodecConfigRepository.GetCodecConfig(CodecConfigId);
		}

		public CodecConfig UpdateCodecConfig(CodecConfig entity)
		{
			return _iCodecConfigRepository.UpdateCodecConfig(entity);
		}

		public bool DeleteCodecConfig(System.Int32 CodecConfigId)
		{
			return _iCodecConfigRepository.DeleteCodecConfig(CodecConfigId);
		}

		public List<CodecConfig> GetAllCodecConfig()
		{
			return _iCodecConfigRepository.GetAllCodecConfig();
		}

		public CodecConfig InsertCodecConfig(CodecConfig entity)
		{
			 return _iCodecConfigRepository.InsertCodecConfig(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 codecconfigid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out codecconfigid))
            {
				CodecConfig codecconfig = _iCodecConfigRepository.GetCodecConfig(codecconfigid);
                if(codecconfig!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(codecconfig);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CodecConfig> codecconfiglist = _iCodecConfigRepository.GetAllCodecConfig();
            if (codecconfiglist != null && codecconfiglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(codecconfiglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CodecConfig codecconfig = new CodecConfig();
                PostOutput output = new PostOutput();
                codecconfig.CopyFrom(Input);
                codecconfig = _iCodecConfigRepository.InsertCodecConfig(codecconfig);
                output.CopyFrom(codecconfig);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CodecConfig codecconfiginput = new CodecConfig();
                CodecConfig codecconfigoutput = new CodecConfig();
                PutOutput output = new PutOutput();
                codecconfiginput.CopyFrom(Input);
                CodecConfig codecconfig = _iCodecConfigRepository.GetCodecConfig(codecconfiginput.CodecConfigId);
                if (codecconfig!=null)
                {
                    codecconfigoutput = _iCodecConfigRepository.UpdateCodecConfig(codecconfiginput);
                    if(codecconfigoutput!=null)
                    {
                        output.CopyFrom(codecconfigoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 codecconfigid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out codecconfigid))
            {
				 bool IsDeleted = _iCodecConfigRepository.DeleteCodecConfig(codecconfigid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
