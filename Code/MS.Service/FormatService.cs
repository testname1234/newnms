﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Format;
using Validation;
using System.Linq;
using MS.Core;

namespace MS.Service
{
		
	public class FormatService : IFormatService 
	{
		private IFormatRepository _iFormatRepository;
        
		public FormatService(IFormatRepository iFormatRepository)
		{
			this._iFormatRepository = iFormatRepository;
		}
        
        public Dictionary<string, string> GetFormatBasicSearchColumns()
        {
            
            return this._iFormatRepository.GetFormatBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFormatAdvanceSearchColumns()
        {
            
            return this._iFormatRepository.GetFormatAdvanceSearchColumns();
           
        }
        

		public Format GetFormat(System.Int32 FormatId)
		{
			return _iFormatRepository.GetFormat(FormatId);
		}

		public Format UpdateFormat(Format entity)
		{
			return _iFormatRepository.UpdateFormat(entity);
		}

		public bool DeleteFormat(System.Int32 FormatId)
		{
			return _iFormatRepository.DeleteFormat(FormatId);
		}

		public List<Format> GetAllFormat()
		{
			return _iFormatRepository.GetAllFormat();
		}

		public Format InsertFormat(Format entity)
		{
			 return _iFormatRepository.InsertFormat(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 formatid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out formatid))
            {
				Format format = _iFormatRepository.GetFormat(formatid);
                if(format!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(format);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Format> formatlist = _iFormatRepository.GetAllFormat();
            if (formatlist != null && formatlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(formatlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Format format = new Format();
                PostOutput output = new PostOutput();
                format.CopyFrom(Input);
                format = _iFormatRepository.InsertFormat(format);
                output.CopyFrom(format);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Format formatinput = new Format();
                Format formatoutput = new Format();
                PutOutput output = new PutOutput();
                formatinput.CopyFrom(Input);
                Format format = _iFormatRepository.GetFormat(formatinput.FormatId);
                if (format!=null)
                {
                    formatoutput = _iFormatRepository.UpdateFormat(formatinput);
                    if(formatoutput!=null)
                    {
                        output.CopyFrom(formatoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 formatid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out formatid))
            {
				 bool IsDeleted = _iFormatRepository.DeleteFormat(formatid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
