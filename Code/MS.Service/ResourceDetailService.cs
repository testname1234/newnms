﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceDetail;
using Validation;
using MS.Core;
using System.Linq;
using MS.Core;

namespace MS.Service
{
		
	public class ResourceDetailService : IResourceDetailService 
	{
		private IResourceDetailRepository _iResourceDetailRepository;
        
		public ResourceDetailService(IResourceDetailRepository iResourceDetailRepository)
		{
			this._iResourceDetailRepository = iResourceDetailRepository;
		}
        
        public Dictionary<string, string> GetResourceDetailBasicSearchColumns()
        {
            
            return this._iResourceDetailRepository.GetResourceDetailBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetResourceDetailAdvanceSearchColumns()
        {
            
            return this._iResourceDetailRepository.GetResourceDetailAdvanceSearchColumns();
           
        }
        

		public ResourceDetail GetResourceDetail(System.Int32 ResourceDetailId)
		{
			return _iResourceDetailRepository.GetResourceDetail(ResourceDetailId);
		}

		public ResourceDetail UpdateResourceDetail(ResourceDetail entity)
		{
			return _iResourceDetailRepository.UpdateResourceDetail(entity);
		}

		public bool DeleteResourceDetail(System.Int32 ResourceDetailId)
		{
			return _iResourceDetailRepository.DeleteResourceDetail(ResourceDetailId);
		}

		public List<ResourceDetail> GetAllResourceDetail()
		{
			return _iResourceDetailRepository.GetAllResourceDetail();
		}

		public ResourceDetail InsertResourceDetail(ResourceDetail entity)
		{
			 return _iResourceDetailRepository.InsertResourceDetail(entity);
		}

        public ResourceDetail GetResourceDetailByGuid(string Guid, string SelectClause = null)
        {
            return _iResourceDetailRepository.GetResourceDetailByGuid(Guid, SelectClause);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourcedetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcedetailid))
            {
				ResourceDetail resourcedetail = _iResourceDetailRepository.GetResourceDetail(resourcedetailid);
                if(resourcedetail!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resourcedetail);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ResourceDetail> resourcedetaillist = _iResourceDetailRepository.GetAllResourceDetail();
            if (resourcedetaillist != null && resourcedetaillist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourcedetaillist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ResourceDetail resourcedetail = new ResourceDetail();
                PostOutput output = new PostOutput();
                resourcedetail.CopyFrom(Input);
                resourcedetail = _iResourceDetailRepository.InsertResourceDetail(resourcedetail);
                output.CopyFrom(resourcedetail);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ResourceDetail resourcedetailinput = new ResourceDetail();
                ResourceDetail resourcedetailoutput = new ResourceDetail();
                PutOutput output = new PutOutput();
                resourcedetailinput.CopyFrom(Input);
                ResourceDetail resourcedetail = _iResourceDetailRepository.GetResourceDetail(resourcedetailinput.ResourceDetailId);
                if (resourcedetail!=null)
                {
                    resourcedetailoutput = _iResourceDetailRepository.UpdateResourceDetail(resourcedetailinput);
                    if(resourcedetailoutput!=null)
                    {
                        output.CopyFrom(resourcedetailoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourcedetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcedetailid))
            {
				 bool IsDeleted = _iResourceDetailRepository.DeleteResourceDetail(resourcedetailid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
