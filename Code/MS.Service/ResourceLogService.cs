﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceLog;
using Validation;
using System.Linq;

namespace MS.Service
{
		
	public class ResourceLogService : IResourceLogService 
	{
		private IResourceLogRepository _iResourceLogRepository;
        
		public ResourceLogService(IResourceLogRepository iResourceLogRepository)
		{
			this._iResourceLogRepository = iResourceLogRepository;
		}
        
        public Dictionary<string, string> GetResourceLogBasicSearchColumns()
        {
            
            return this._iResourceLogRepository.GetResourceLogBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetResourceLogAdvanceSearchColumns()
        {
            
            return this._iResourceLogRepository.GetResourceLogAdvanceSearchColumns();
           
        }
        

		public ResourceLog GetResourceLog(System.Int32 ResourceLogId)
		{
			return _iResourceLogRepository.GetResourceLog(ResourceLogId);
		}

		public ResourceLog UpdateResourceLog(ResourceLog entity)
		{
			return _iResourceLogRepository.UpdateResourceLog(entity);
		}

		public bool DeleteResourceLog(System.Int32 ResourceLogId)
		{
			return _iResourceLogRepository.DeleteResourceLog(ResourceLogId);
		}

		public List<ResourceLog> GetAllResourceLog()
		{
			return _iResourceLogRepository.GetAllResourceLog();
		}

		public ResourceLog InsertResourceLog(ResourceLog entity)
		{
			 return _iResourceLogRepository.InsertResourceLog(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourcelogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcelogid))
            {
				ResourceLog resourcelog = _iResourceLogRepository.GetResourceLog(resourcelogid);
                if(resourcelog!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resourcelog);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ResourceLog> resourceloglist = _iResourceLogRepository.GetAllResourceLog();
            if (resourceloglist != null && resourceloglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourceloglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ResourceLog resourcelog = new ResourceLog();
                PostOutput output = new PostOutput();
                resourcelog.CopyFrom(Input);
                resourcelog = _iResourceLogRepository.InsertResourceLog(resourcelog);
                output.CopyFrom(resourcelog);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ResourceLog resourceloginput = new ResourceLog();
                ResourceLog resourcelogoutput = new ResourceLog();
                PutOutput output = new PutOutput();
                resourceloginput.CopyFrom(Input);
                ResourceLog resourcelog = _iResourceLogRepository.GetResourceLog(resourceloginput.ResourceLogId);
                if (resourcelog!=null)
                {
                    resourcelogoutput = _iResourceLogRepository.UpdateResourceLog(resourceloginput);
                    if(resourcelogoutput!=null)
                    {
                        output.CopyFrom(resourcelogoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourcelogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcelogid))
            {
				 bool IsDeleted = _iResourceLogRepository.DeleteResourceLog(resourcelogid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }





         public List<Core.Models.UserLog> getAllUSerLog()
         {
             return _iResourceLogRepository.getAllUSerLog();
         }


         public List<ResourceLog> GetResourceLogbyUserId(int p)
         {
             return _iResourceLogRepository.GetResourceLogbyUserId(p);
         }
    }
	
	
}
