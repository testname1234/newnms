﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceMeta;
using Validation;
using System.Linq;
using MS.Core;

namespace MS.Service
{

    public class ResourceMetaService : IResourceMetaService
    {
        private IResourceMetaRepository _iResourceMetaRepository;

        public ResourceMetaService(IResourceMetaRepository iResourceMetaRepository)
        {
            this._iResourceMetaRepository = iResourceMetaRepository;
        }

        public Dictionary<string, string> GetResourceMetaBasicSearchColumns()
        {

            return this._iResourceMetaRepository.GetResourceMetaBasicSearchColumns();

        }

        public List<SearchColumn> GetResourceMetaAdvanceSearchColumns()
        {

            return this._iResourceMetaRepository.GetResourceMetaAdvanceSearchColumns();

        }


        public ResourceMeta GetResourceMeta(System.Int32 ResourceMetaId)
        {
            return _iResourceMetaRepository.GetResourceMeta(ResourceMetaId);
        }

        public ResourceMeta UpdateResourceMeta(ResourceMeta entity)
        {
            return _iResourceMetaRepository.UpdateResourceMeta(entity);
        }

        public bool DeleteResourceMeta(System.Int32 ResourceMetaId)
        {
            return _iResourceMetaRepository.DeleteResourceMeta(ResourceMetaId);
        }

        public List<ResourceMeta> GetAllResourceMeta()
        {
            return _iResourceMetaRepository.GetAllResourceMeta();
        }

        public ResourceMeta CheckAndInsertResourceMeta(ResourceMeta entity)
        {
            ResourceMeta resourceMeta = _iResourceMetaRepository.GetResourceMetaByResource(entity);
            if (resourceMeta == null)
            {
                return _iResourceMetaRepository.InsertResourceMeta(entity);
            }
            else
            {
                return resourceMeta;
            }
        }

        public ResourceMeta InsertResourceMetaWithoutChecking(ResourceMeta entity)
        {
            return _iResourceMetaRepository.InsertResourceMeta(entity);
        }

        public ResourceMeta InsertResourceMeta(ResourceMeta entity)
        {
            ResourceMeta resourceMeta = _iResourceMetaRepository.GetMetaByGuidKeyAndValue(entity);
            if (resourceMeta == null)
            {
                return _iResourceMetaRepository.InsertResourceMeta(entity);
            }
            else
            {
                return resourceMeta;
            }
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourcemetaid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out resourcemetaid))
            {
                ResourceMeta resourcemeta = _iResourceMetaRepository.GetResourceMeta(resourcemetaid);
                if (resourcemeta != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resourcemeta);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ResourceMeta> resourcemetalist = _iResourceMetaRepository.GetAllResourceMeta();
            if (resourcemetalist != null && resourcemetalist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourcemetalist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ResourceMeta resourcemeta = new ResourceMeta();
                PostOutput output = new PostOutput();
                resourcemeta.CopyFrom(Input);
                resourcemeta = _iResourceMetaRepository.InsertResourceMeta(resourcemeta);
                output.CopyFrom(resourcemeta);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ResourceMeta resourcemetainput = new ResourceMeta();
                ResourceMeta resourcemetaoutput = new ResourceMeta();
                PutOutput output = new PutOutput();
                resourcemetainput.CopyFrom(Input);
                ResourceMeta resourcemeta = _iResourceMetaRepository.GetResourceMeta(resourcemetainput.ResourceMetaId);
                if (resourcemeta != null)
                {
                    resourcemetaoutput = _iResourceMetaRepository.UpdateResourceMeta(resourcemetainput);
                    if (resourcemetaoutput != null)
                    {
                        output.CopyFrom(resourcemetaoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourcemetaid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out resourcemetaid))
            {
                bool IsDeleted = _iResourceMetaRepository.DeleteResourceMeta(resourcemetaid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public List<ResourceMeta> GetResourceMetaByGuid(string Guid)
        {
            return _iResourceMetaRepository.GetResourceMetaByGuid(Guid);
        }


        public List<ResourceMeta> GetResourceMetaforThread()
        {
            return _iResourceMetaRepository.GetResourceMetaforThread();
        }


        public List<ResourceMeta> GetResourceMetaforThreadAfterDate(DateTime lastUpdateDate)
        {
            return _iResourceMetaRepository.GetResourceMetaforThreadAfterDate(lastUpdateDate);
        }
    }


}
