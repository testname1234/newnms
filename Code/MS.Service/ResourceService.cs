﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Resource;
using Validation;
using System.Linq;
using MS.Core;
using MS.Core.Enums;
using SolrManager;
using SolrManager.InputEntities;
using SolrNet;
using MS.MediaIntegration.API;
using FFMPEGLib.Helper;
using MS.Core.Helper;
using System.Configuration;
using ControlPanel.Core.IService;

namespace MS.Service
{

    public class ResourceService : IResourceService
    {
        private IResourceRepository _iResourceRepository;
        private IScrapResourceRepository _iScrapResourceRepository;
        private ISearchHistoryRepository _iSearchHistoryRepository;
        private IBucketRepository _iBucketRepository;
        public ResourceService(IResourceRepository iResourceRepository, IScrapResourceRepository iScrapResourceRepository, ISearchHistoryRepository iSearchHistoryRepository, IBucketRepository iBucketRepository)
        {
            this._iResourceRepository = iResourceRepository;
            this._iScrapResourceRepository = iScrapResourceRepository;
            this._iSearchHistoryRepository = iSearchHistoryRepository;
            this._iBucketRepository = iBucketRepository;
        }

        public Dictionary<string, string> GetResourceBasicSearchColumns()
        {
            return this._iResourceRepository.GetResourceBasicSearchColumns();
        }

        public List<SearchColumn> GetResourceAdvanceSearchColumns()
        {
            return this._iResourceRepository.GetResourceAdvanceSearchColumns();
        }


        public virtual List<Resource> GetResourceByResourceTypeId(System.Int32 ResourceTypeId)
        {
            return _iResourceRepository.GetResourceByResourceTypeId(ResourceTypeId);
        }

        public virtual List<Resource> GetResourceByHighResolutionFormatId(System.Int32? HighResolutionFormatId)
        {
            return _iResourceRepository.GetResourceByHighResolutionFormatId(HighResolutionFormatId);
        }

        public virtual List<Resource> GetResourceByLowResolutionFormatId(System.Int32? LowResolutionFormatId)
        {
            return _iResourceRepository.GetResourceByLowResolutionFormatId(LowResolutionFormatId);
        }

        public virtual List<Resource> GetResourceByResourceStatusId(System.Int32 ResourceStatusId)
        {
            return _iResourceRepository.GetResourceByResourceStatusId(ResourceStatusId);
        }

        public virtual List<Resource> GetResourceByServerId(System.Int32? ServerId)
        {
            return _iResourceRepository.GetResourceByServerId(ServerId);
        }

        public virtual List<Resource> GetResourceByStatusId(int StatusId)
        {
            return _iResourceRepository.GetResourceByStatusId(StatusId);
        }

        public virtual List<Resource> GetResourceByStatusId(int StatusId, int count)
        {
            return _iResourceRepository.GetResourceByStatusId(StatusId, count);
        }

        public virtual List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId)
        {
            return _iResourceRepository.GetResourceByStatusIdAndResourceTypeId(StatusId, ResourceTypeId);
        }

        public virtual List<Resource> GetResourceByStatusIdAndResourceTypeId(int StatusId, int ResourceTypeId, int Count)
        {
            return _iResourceRepository.GetResourceByStatusIdAndResourceTypeId(StatusId, ResourceTypeId, Count);
        }

        public Resource GetResource(System.Int32 ResourceId)
        {
            return _iResourceRepository.GetResource(ResourceId);
        }

        public string GetResourcePathByGuid(Guid guid)
        {
            return _iResourceRepository.GetResourcePathByGuid(guid);
        }

        public List<Resource> GetResourceByCaption(string Caption)
        {
            return _iResourceRepository.GetResourceByCaption(Caption);
        }

        public List<Resource> GetResourceforDuration(DateTime ResourceDate)
        {
            return _iResourceRepository.GetResourceforDuration(ResourceDate);
        }

        public List<Resource> GetResourceByMeta(string MetaString)
        {
            return _iResourceRepository.GetResourceByMeta(MetaString);
        }

        public Resource UpdateResource(Resource entity)
        {
            return _iResourceRepository.UpdateResource(entity);
        }

        //public Resource UpdateResourceDuration(Resource entity)
        //{
        //    return _iResourceRepository.UpdateResourceDuration(entity);
        //}

        public bool DeleteResource(System.Int32 ResourceId)
        {
            return _iResourceRepository.DeleteResource(ResourceId);
        }

        public List<Resource> GetAllResource()
        {
            return _iResourceRepository.GetAllResource();
        }

        public Resource InsertResource(Resource entity)
        {
            if (entity.IsFromExternalSource)
            {
                ScrapResource res = new ScrapResource();
                res.CopyFrom(entity);
                res.Status = ScrapResourceStatuses.DownloadPendingtry1;
                res.ResourceGuid = entity.Guid;
                res.DownloadedFilePath = string.Empty;
                res.SystemType = entity.SystemType;
                _iScrapResourceRepository.InsertScrapResource(res);
            }
            return _iResourceRepository.InsertResource(entity);
        }

        public Resource UpdateResourceInfo(Resource entity)
        {
            return _iResourceRepository.UpdateResourceInfo(entity);
        }

        public bool CopyFilesOnServerAndTempFolder(Resource res, System.Web.HttpPostedFile file)
        {
            string fullRootPath = res.Source.Substring(0, res.Source.IndexOf("{guid}"));
            var guid = res.Guid;
            //fullRootPath = fullRootPath.Replace("\\", "\\\\");
            string _thumbnail_url = string.Empty;
            //_storageRoot = fullRootPath;
            string fullPath = fullRootPath + System.IO.Path.GetFileName(guid.ToString()) + System.IO.Path.GetExtension(file.FileName);
            System.IO.Directory.CreateDirectory(fullRootPath);
            file.SaveAs(fullPath);
            //_thumbnail_url = resourceService.GenerateSnapshotOnMediaServer(guid);
            string[] _imageExtensions = { "jpg", "bmp", "gif", "png" };
            string[] _videoExtensions = { ".mp3", ".OGG", ".RMA", ".avi", ".mp4", ".divx", ".wmv", };

            string fullName = System.IO.Path.GetFileName(guid.ToString()) + System.IO.Path.GetExtension(file.FileName);
            //statuses.Add(new FilesStatus(fullName, file.ContentLength, fullPath) { thumbnail_url = _thumbnail_url });


            MS.Core.Models.CustomSnapshotInput input = new MS.Core.Models.CustomSnapshotInput();
            input.FilePath = fullPath;
            input.Id = guid.ToString();
            string[] ext = fullName.Split('.');
            if (_imageExtensions.Contains(ext[ext.Length - 1]))
            {
                input.ResourceTypeId = 1;
            }
            if (_videoExtensions.Contains(ext[ext.Length - 1]))
            {
                input.ResourceTypeId = 2;
            }
            {
                DateTime dt = DateTime.UtcNow;
                Resource resource = res;
                if (resource != null)
                {
                    string resourcePath = string.Empty;
                    Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId).FirstOrDefault();
                    if (server != null)
                    {
                        if (!string.IsNullOrEmpty(resource.FilePath))
                        {
                            IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
                            Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                            resource.FilePath = resource.FilePath.Replace("/", "\\");
                            string exten = System.IO.Path.GetExtension(resource.FilePath);
                            string folderPath = resource.FilePath.Substring(0, resource.FilePath.LastIndexOf('\\') + 1);
                            string root = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocationThumb"] + bucket.Path + "\\" + folderPath;
                            string filePath = resource.Source;
                            string saveFileName = resource.FilePath.Replace(exten, ".jpg");
                            saveFileName = saveFileName.Substring(saveFileName.LastIndexOf('\\') + 1, (saveFileName.Length - (saveFileName.LastIndexOf('\\') + 1)));


                            if (ResourceHelper.GenerateSnapshot(System.Web.HttpContext.Current.Server.MapPath("~/ffmpeg/"), input.FilePath, root, saveFileName, ((ResourceTypes)resource.ResourceTypeId)))
                            {
                                resource.ThumbUrl = resource.FilePath.Replace(exten, ".jpg");
                                UpdateResource(resource);
                                return true;
                            }
                        }
                    }
                }

            }
            return false;
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourceid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out resourceid))
            {
                Resource resource = _iResourceRepository.GetResource(resourceid);
                if (resource != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resource);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Resource> resourcelist = _iResourceRepository.GetAllResource();
            if (resourcelist != null && resourcelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Resource resource = new Resource();
                PostOutput output = new PostOutput();
                resource.CopyFrom(Input);
                resource = _iResourceRepository.InsertResource(resource);
                output.CopyFrom(resource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Resource resourceinput = new Resource();
                Resource resourceoutput = new Resource();
                PutOutput output = new PutOutput();
                resourceinput.CopyFrom(Input);
                Resource resource = _iResourceRepository.GetResource(resourceinput.ResourceId);
                if (resource != null)
                {
                    resourceoutput = _iResourceRepository.UpdateResource(resourceinput);
                    if (resourceoutput != null)
                    {
                        output.CopyFrom(resourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourceid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out resourceid))
            {
                bool IsDeleted = _iResourceRepository.DeleteResource(resourceid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public Resource GetResourceByGuidId(Guid guid)
        {
            return _iResourceRepository.GetResourceByGuidId(guid);
        }

        public Resource GetResourceByFilePath(string FilePath, int BucketId)
        {
            return _iResourceRepository.GetResourceByFilePath(FilePath, BucketId);
        }


        public bool DeleteResourceByGuid(Guid guid)
        {
            return _iResourceRepository.DeleteResourceByGuid(guid);
        }


        public Resource GetResourceBySource(string source)
        {
            return _iResourceRepository.GetResourceBySource(source);
        }

        public Resource GetResourceBySourceAndBucketId(string source, int bucketId)
        {
            return _iResourceRepository.GetResourceBySourceAndBucketId(source, bucketId);
        }

        public Resource GetResourceByFilePathAndBucketId(string filePath, int bucketId)
        {
            return _iResourceRepository.GetResourceByFilePathAndBucketId(filePath, bucketId);
        }

        public Resource GetResourceByBarcode(string barcode)
        {
            return _iResourceRepository.GetResourceByBarcode(barcode);
        }

        public Resource GetResourceByBarcodeAndResourceType(string barcode, int resourceTypeId)
        {
            return _iResourceRepository.GetResourceByBarcodeAndResourceType(barcode, resourceTypeId);
        }

        public List<ScrapResource> GetScrapResourceByStatusId(ScrapResourceStatuses? status)
        {
            return _iScrapResourceRepository.GetScrapResourceByStatusId(status);
        }

        public List<ScrapResource> GetScrapResourceByStatusIdAndSystemTypeId(ScrapResourceStatuses? status, int? SystemTypeId)
        {

            return _iScrapResourceRepository.GetScrapResourceByStatusIdAndSystemTypeId(status, SystemTypeId);
        }

        public List<ScrapResource> GetScrapResourceByDate(DateTime CreationDate)
        {
            return _iScrapResourceRepository.GetScrapResourceByDate(CreationDate);
        }

        public List<ScrapResource> GetDownloadedResources()
        {
            return _iScrapResourceRepository.GetDownloadedResources();
        }



        public void UpdateScrapResource(ScrapResource scrapRes)
        {
            _iScrapResourceRepository.UpdateScrapResource(scrapRes);
        }

        public void UpdateScrapResourceNoReturn(ScrapResource scrapRes)
        {
            _iScrapResourceRepository.UpdateScrapResourceNoReturn(scrapRes);
        }

        public void DeleteScrapResource(int scrapResourceid)
        {
            _iScrapResourceRepository.DeleteScrapResource(scrapResourceid);
        }

        public List<Resource> GetResourceByBucketId(int bucketId)
        {
            return _iResourceRepository.GetResourceByBucketId(bucketId);
        }


        public List<Resource> GetAllResourcesByFilePath(string path)
        {
            return _iResourceRepository.GetAllResourcesByFilePath(path);
        }


        public List<Resource> GetAllResourcesByFilePath(string FilePath, int BucketId)
        {
            return _iResourceRepository.GetAllResourcesByFilePath(FilePath, BucketId);
        }

        //public virtual List<Resource> SearchResource(string term, string collectionURL, int pageSize, int pageNumber)
        //{
        //    List<Resource> searchResources = new List<Resource>();
        //    SolrService<SolrResource> sServer = new SolrService<SolrResource>();
        //    string solrQuery = term;
        //    var searchResult = sServer.Search(solrQuery, pageSize, pageNumber, term);
        //    List<Resource> orderedList = new List<Resource>();

        //    if (searchResult != null && searchResult.Results.Count > 0)
        //    {
        //        List<string> resourceList = searchResult.Results.Select(x => x.Guid).ToList();

        //        searchResources = _iResourceRepository.GetResourceByGuids(resourceList);


        //        foreach (string guid in resourceList)
        //        {
        //            orderedList.Add(searchResources.Where(x => x.Guid == new Guid(guid)).First());
        //        }
        //    }

        //    if (orderedList != null)
        //        orderedList = orderedList.OrderByDescending(a => a.ResourceDate).ToList();


        //    //OrderByDescending(a=>a.ResourceDate).ToList()
        //    return orderedList;
        //}

        public virtual List<Resource> SearchResource(string term, string collectionURL, int pageSize, int pageNumber, int? resourceTypeId, int? bucketid, bool alldata, out int resourceCount)
        {

            List<Resource> searchResources = new List<Resource>();
            SolrService<SolrResource> sServer = new SolrService<SolrResource>();

            //  string solrQuery = "((Slug:(*" + term + "*)^2  description:(*" + term + "*))  AND  (Metas:(*" + collectionURL + "*)^10))";

            string solrQuery = "*" + term + "*";
            string mm = "100%"; //minimum should match

            if (bucketid != null)
            {

                if (alldata)
                {
                    solrQuery += " BucketId:" + bucketid.Value + " IsPrivate:false";
                    mm = "99%";
                }
                else
                    solrQuery += " BucketId:" + bucketid.Value;

            }
            else
                solrQuery += " IsPrivate:false";


            if (resourceTypeId != null)
                solrQuery += " ResourceTypeId:" + resourceTypeId.Value;

            string searchColumn = "Slug^2 Metas^10 description";

            var searchResult = sServer.SearchEDismax(solrQuery, pageSize, pageNumber, term, searchColumn, true, null, mm);
            resourceCount = searchResult.TotalCount;
            List<Resource> orderedList = new List<Resource>();

            if (searchResult != null && searchResult.Results.Count > 0)
            {
                List<string> resourceList = searchResult.Results.Select(x => x.Guid).ToList();
                searchResources = _iResourceRepository.GetResourceByGuids(resourceList);

                foreach (string guid in resourceList)
                {
                    orderedList.Add(searchResources.Where(x => x.Guid == new Guid(guid)).First());
                }

                InsertsearchHistory(term, pageNumber);
            }

            if (orderedList != null)
                orderedList = orderedList.OrderByDescending(a => a.ResourceDate).ToList();

            return orderedList;
        }

        public virtual List<Resource> SearchResources(string term, string fromDate, string toDate, string collectionURL, int pageSize, bool isHD, int pageNumber, int? resourceTypeId, int? bucketid, bool alldata, out int resourceCount)
        {
            string searchQuery3 = string.Empty;
            string searchQuery4 = string.Empty;
            string searchQuery5 = string.Empty;

            //string[] allMetas = term.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            #region bucket and private
            if (bucketid != null)
                searchQuery3 = "BucketId:" + bucketid;

            if (alldata)
            {
                if (searchQuery3 != string.Empty)
                    searchQuery3 += " OR IsPrivate:false";
                else
                    searchQuery3 = "IsPrivate:false";
            }
            else
            {
                if (searchQuery3 == string.Empty)
                    searchQuery3 = "IsPrivate:true";
            }
            searchQuery3 = "(" + searchQuery3 + ")";
            #endregion

            #region resourcetype
            if (resourceTypeId != null)
            {
                searchQuery4 = "(ResourceTypeId:" + resourceTypeId.Value + ")";
            }

            searchQuery5 = "(isHD:" + isHD + ")";
            #endregion

            SolrService<SolrResource> sServer = new SolrService<SolrResource>();
            string solrQuery = string.Empty;

            if (!string.IsNullOrEmpty(term) && string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                solrQuery = sServer.GenerateQuery(term, new List<string>() { "Slug", "description", "Metas" }) + " AND " + searchQuery3 + " AND " + searchQuery5;
            }

            if (string.IsNullOrEmpty(term) && !string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                string term1 = Convert.ToDateTime(fromDate).ToString("yyyy-MM-ddT00:00:00Z");
                string term2 = Convert.ToDateTime(toDate).ToString("yyyy-MM-ddT23:59:59Z");
                solrQuery = sServer.GenerateQueryRange(term1, term2, new List<string>() { "ResourceDate" }) + " AND " + searchQuery3 + " AND " + searchQuery5;
            }

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                string fromdate = Convert.ToDateTime(fromDate).ToString("yyyy-MM-ddT00:00:00Z");
                string todate = Convert.ToDateTime(toDate).ToString("yyyy-MM-ddT23:59:59Z");
                solrQuery = sServer.GenerateQueryRangewWithText(term, fromdate, todate, new List<string>() { "Slug","description","Metas" }) + " AND " + searchQuery3 + " AND " + searchQuery5;
            }

            if (searchQuery4 != string.Empty)
                solrQuery += " AND " + searchQuery4;

            // ((Slug:*imran AND Slug:khan*) OR (description:*imran AND description:khan*) OR (Metas:*imran AND Metas:khan*)) AND (BucketId:186 OR IsPrivate:false) AND ResourceTypeId:1
            List<Resource> searchResources = new List<Resource>();


            List<SolrNet.SortOrder> sortparams = new List<SortOrder>();
            sortparams.Add(new SortOrder("IsCompleted", Order.DESC));
            sortparams.Add(new SortOrder("score", Order.DESC));
            sortparams.Add(new SortOrder("ResourceDate", Order.DESC));


            var searchResult = sServer.QuerySolr(solrQuery, pageSize, pageNumber, sortparams);
            List<Resource> orderedList = new List<Resource>();
            resourceCount = searchResult.TotalCount;

            if (searchResult != null && searchResult.Results.Count > 0)
            {
                resourceCount = searchResult.TotalCount;
                List<string> resourceList = searchResult.Results.Select(x => x.Guid).ToList();
                searchResources = _iResourceRepository.GetResourceByGuids(resourceList);

                foreach (string guid in resourceList)
                {
                    orderedList.Add(searchResources.Where(x => x.Guid == new Guid(guid)).First());
                }

                InsertsearchHistory(term, pageNumber);
            }

            //if (orderedList != null)
            //    orderedList = orderedList.OrderByDescending(a => a.ResourceDate).ToList();

            return orderedList;

        }

        public virtual List<Resource> SearchResourceByDates(DateTime fromDate, DateTime toDate, string collectionURL, int pageSize, bool isHD, int pageNumber, int? resourceTypeId, int? bucketid, bool alldata, out int resourceCount)
        {
            string searchQuery3 = string.Empty;
            string searchQuery4 = string.Empty;
            string searchQuery5 = string.Empty;
            //string[] allMetas = fromDate.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            #region bucket and private
            if (bucketid != null)
                searchQuery3 = "BucketId:" + bucketid;

            if (alldata)
            {
                if (searchQuery3 != string.Empty)
                    searchQuery3 += " OR IsPrivate:false";
                else
                    searchQuery3 = "IsPrivate:false";
            }
            else
            {
                if (searchQuery3 == string.Empty)
                    searchQuery3 = "IsPrivate:true";
            }
            searchQuery3 = "(" + searchQuery3 + ")";
            #endregion

            #region resourcetype
            if (resourceTypeId != null)
            {
                searchQuery4 = "(ResourceTypeId:" + resourceTypeId.Value + ")";
            }


            searchQuery5 = "(isHD:" + isHD + ")";

            #endregion

            SolrService<SolrResource> sServer = new SolrService<SolrResource>();
            string solrQuery = string.Empty;
            //DateTime enteredDate = DateTime.Parse(fromDate);
            string term = Convert.ToDateTime(fromDate).ToString("yyyy-MM-ddTHH:mm:ssZ");
            string term2 = Convert.ToDateTime(toDate).ToString("yyyy-MM-ddTHH:mm:ssZ");
            solrQuery = sServer.GenerateQueryRange(term, term2, new List<string>() { "ResourceDate" }) + " AND " + searchQuery3 + " AND " + searchQuery5;

            if (searchQuery4 != string.Empty)
                solrQuery += " AND " + searchQuery4;

            // ((Slug:*imran AND Slug:khan*) OR (description:*imran AND description:khan*) OR (Metas:*imran AND Metas:khan*)) AND (BucketId:186 OR IsPrivate:false) AND ResourceTypeId:1
            List<Resource> searchResources = new List<Resource>();


            List<SolrNet.SortOrder> sortparams = new List<SortOrder>();
            sortparams.Add(new SortOrder("IsCompleted", Order.DESC));
            sortparams.Add(new SortOrder("score", Order.DESC));
            sortparams.Add(new SortOrder("ResourceDate", Order.DESC));


            var searchResult = sServer.QuerySolr(solrQuery, pageSize, pageNumber, sortparams);
            List<Resource> orderedList = new List<Resource>();
            resourceCount = searchResult.TotalCount;

            if (searchResult != null && searchResult.Results.Count > 0)
            {
                resourceCount = searchResult.TotalCount;
                List<string> resourceList = searchResult.Results.Select(x => x.Guid).ToList();
                searchResources = _iResourceRepository.GetResourceByGuids(resourceList);

                foreach (string guid in resourceList)
                {
                    orderedList.Add(searchResources.Where(x => x.Guid == new Guid(guid)).First());
                }

                InsertsearchHistory(Convert.ToString(fromDate), pageNumber);
            }

            //if (orderedList != null)
            //    orderedList = orderedList.OrderByDescending(a => a.ResourceDate).ToList();

            return orderedList;

        }

        public virtual List<Resource> SearchResourceByTermnDates(string term, DateTime fromDate, DateTime toDate, string collectionURL, int pageSize, bool isHD, int pageNumber, int? resourceTypeId, int? bucketid, bool alldata, out int resourceCount)
        {
            string searchQuery3 = string.Empty;
            string searchQuery4 = string.Empty;
            string searchQuery5 = string.Empty;


            string[] allMetas = term.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            #region bucket and private
            if (bucketid != null)
                searchQuery3 = "BucketId:" + bucketid;

            if (alldata)
            {
                if (searchQuery3 != string.Empty)
                    searchQuery3 += " OR IsPrivate:false";
                else
                    searchQuery3 = "IsPrivate:false";
            }
            else
            {
                if (searchQuery3 == string.Empty)
                    searchQuery3 = "IsPrivate:true";
            }
            searchQuery3 = "(" + searchQuery3 + ")";
            #endregion

            #region resourcetype
            if (resourceTypeId != null)
            {
                searchQuery4 = "(ResourceTypeId:" + resourceTypeId.Value + ")";
            }

            searchQuery5 = "(isHD:" + isHD + ")";
            #endregion

            SolrService<SolrResource> sServer = new SolrService<SolrResource>();
            string solrQuery = string.Empty;
            //DateTime enteredDate = DateTime.Parse(fromDate);
            string fromdate = Convert.ToDateTime(fromDate).ToString("yyyy-MM-ddTHH:mm:ssZ");
            string todate = Convert.ToDateTime(toDate).ToString("yyyy-MM-ddTHH:mm:ssZ");
            solrQuery = sServer.GenerateQueryRangewWithText(term, fromdate, todate, new List<string>() { "Slug" }) + " AND " + searchQuery3 + " AND " + searchQuery5;

            if (searchQuery4 != string.Empty)
                solrQuery += " AND " + searchQuery4;

            // ((Slug:*imran AND Slug:khan*) OR (description:*imran AND description:khan*) OR (Metas:*imran AND Metas:khan*)) AND (BucketId:186 OR IsPrivate:false) AND ResourceTypeId:1
            List<Resource> searchResources = new List<Resource>();


            List<SolrNet.SortOrder> sortparams = new List<SortOrder>();
            //sortparams.Add(new SortOrder("IsCompleted", Order.DESC));
            //sortparams.Add(new SortOrder("score", Order.DESC));
            sortparams.Add(new SortOrder("ResourceDate", Order.DESC));


            var searchResult = sServer.QuerySolr(solrQuery, pageSize, pageNumber, sortparams);
            List<Resource> orderedList = new List<Resource>();
            resourceCount = searchResult.TotalCount;

            if (searchResult != null && searchResult.Results.Count > 0)
            {
                resourceCount = searchResult.TotalCount;
                List<string> resourceList = searchResult.Results.Select(x => x.Guid).ToList();
                searchResources = _iResourceRepository.GetResourceByGuids(resourceList);

                foreach (string guid in resourceList)
                {
                    orderedList.Add(searchResources.Where(x => x.Guid == new Guid(guid)).First());
                }

                InsertsearchHistory(Convert.ToString(fromDate), pageNumber);
            }

            //if (orderedList != null)
            //    orderedList = orderedList.OrderByDescending(a => a.ResourceDate).ToList();

            return orderedList;

        }


        public virtual List<Resource> GetResourceByDate(DateTime ResourceDate)
        {
            List<Resource> resources = _iResourceRepository.GetByDate(ResourceDate);
            return resources;
        }

        public void InsertsearchHistory(string term, int pageNumber)
        {
            bool IfExist = false;
            try
            {

                if (pageNumber > 1)
                {
                    IfExist = _iSearchHistoryRepository.keywordExists(term);

                    if (!IfExist)
                    {
                        SearchHistory history = new SearchHistory();
                        history.SearchKeyword = term;
                        history.CreationDate = DateTime.UtcNow;
                        history.Frequency = 1;
                        _iSearchHistoryRepository.InsertSearchHistory(history);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public int FillSolrResourceCache(SolrService<SolrResource> solrService)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            int resourceUpdatedCount = 0;
            List<SolrResource> solrResouces = new List<SolrResource>();

            string query = "id:\"Max_Date\"";
            var searchResult = solrService.Sort(query, "DateStamp", Order.DESC, 1);

            if (searchResult != null && searchResult.Results.Count > 0)
            {

              
                long dateStamp = searchResult.Results.First().TimeStamp;

                string message = string.Format("GetResourcesAfterThisDate IsHD: {0}", dateStamp.ToString());
                logService.InsertSystemEventLog(message, null, ControlPanel.Core.Enums.EventCodes.Log);

                List<Resource> moreResources = _iResourceRepository.GetResourcesAfterThisDate(dateStamp);


                if (moreResources != null && moreResources.Count > 0)
                {
                    long maxTimeStamp = moreResources.Last().TimeStamp.Value;
                    moreResources = moreResources.Where(x => !string.IsNullOrEmpty(x.Slug) || !string.IsNullOrEmpty(x.Metas)).ToList();

                    foreach (Resource item in moreResources)
                    {
                        if (item.BucketId.HasValue)
                        {
                            Bucket bucket = _iBucketRepository.GetBucket(item.BucketId.Value);
                            if (bucket != null)
                            {
                                if (bucket.IsPrivate == true)
                                {
                                    item.IsPrivate = true;
                                }
                            }
                        }
                    }
                    solrResouces.Add(new SolrResource() { id = "Max_Date", TimeStamp = maxTimeStamp });
                    solrResouces.CopyFrom(moreResources);
                    solrService.AddRecords(solrResouces);
                    resourceUpdatedCount = moreResources.Count;
                }



            }
            else
            {
                List<MS.Core.Entities.Resource> resources = _iResourceRepository.GetAllResourceWithDetailAndMetas();
                long maxTimeStamp = resources.Last().TimeStamp.Value;

                string message = string.Format("GetAllResourceWithDetailAndMetas IsHD: {0}", maxTimeStamp.ToString());
                logService.InsertSystemEventLog(message, null, ControlPanel.Core.Enums.EventCodes.Log);

                resources = resources.Where(x => !string.IsNullOrEmpty(x.Slug) || !string.IsNullOrEmpty(x.Metas)).ToList();

                foreach (Resource item in resources)
                {

                    if (item.BucketId.HasValue)
                    {
                        Bucket bucket = _iBucketRepository.GetBucket(item.BucketId.Value);
                        if (bucket != null)
                        {
                            if (bucket.IsPrivate == true)
                            {
                                item.IsPrivate = true;
                            }
                        }
                    }
                }

                if (resources != null && resources.Count > 0)
                {
                    solrResouces.CopyFrom(resources);
                    solrService.Clear();
                    solrResouces.Add(new SolrResource() { id = "Max_Date", TimeStamp = maxTimeStamp });
                    solrService.AddRecords(solrResouces);
                    resourceUpdatedCount = resources.Count;
                }
            }


            return resourceUpdatedCount;
        }

        protected bool IsFileLocked(System.IO.FileInfo file)
        {
            System.IO.FileStream stream = null;

            try
            {
                stream = file.Open(System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
            }
            catch (System.IO.IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public Resource GenerateResourceThumbAndFindInformation(Resource res, string tempFilePath, Bucket bucket, Server server)
        {

            MSApi MediaApi = new MSApi();
            string dir1 = "\\" + DateTime.UtcNow.Year.ToString();
            string dir2 = "\\" + DateTime.UtcNow.Year.ToString() + "\\" + DateTime.UtcNow.Month.ToString("00");
            List<string> lstDir = new List<string>();
            lstDir.Add(dir1);
            lstDir.Add(dir2);

            foreach (string d in lstDir)
            {
                var Result = MediaApi.CreateSubBucket(bucket.BucketId, bucket.ApiKey, d);
                if (!Result.IsSuccess)
                    throw new Exception(Result.Errors[0]);
            }

            string ffmpegPath = AppDomain.CurrentDomain.BaseDirectory + "\\ffmpeg\\";
            res.FilePath = dir2 + "\\" + res.FileName;
            string ext = System.IO.Path.GetExtension(res.FileName);
            string saveFolderPath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocationThumb"] + bucket.Path + "\\" + dir2 + "\\";
            MS.Core.Enums.ResourceTypes resType = MS.Core.Enums.ResourceTypes.Video;

            System.IO.FileInfo fInfo = new System.IO.FileInfo(tempFilePath);
            //if (!IsFileLocked(fInfo))
            //{
            FfProbeInformation info = FFMPEGLib.FFMPEG.GetMediaInformation(tempFilePath);

            if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
            {
                resType = MS.Core.Enums.ResourceTypes.Image;
                if (info != null && info.streams != null && info.streams.Count > 0)
                {
                    res.Width = info.streams[0].width;
                    res.Height = info.streams[0].height;
                }
            }
            else if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video)
            {
                resType = MS.Core.Enums.ResourceTypes.Video;
                if (info != null && info.streams != null && info.streams.Count > 0)
                {
                    foreach (Stream s in info.streams)
                    {
                        if (s.codec_type == "audio")
                        {
                            res.AudioCodec = s.codec_name;
                        }
                        if (s.codec_type == "video")
                        {
                            res.VideoCodec = s.codec_name;
                            res.Duration = Convert.ToDouble(s.duration);
                            res.Bitrate = s.bit_rate;
                            res.Framerate = s.avg_frame_rate;
                            res.Width = s.width;
                            res.Height = s.height;
                        }
                    }
                }
            }

            if (ResourceHelper.GenerateSnapshot(ffmpegPath, tempFilePath, saveFolderPath, res.FileName.Replace(ext, ".jpg"), resType))
                res.ThumbUrl = res.FilePath.Replace(ext, ".jpg");
            res.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.TranscodingRequired;
            _iResourceRepository.UpdateResource(res);

            //}
            return res;
        }

        public List<Resource> GetTop100Resources()
        {
            return _iResourceRepository.GetTop100Resources();
        }

        public List<string> GetAllCelebrityNames()
        {
            return _iResourceRepository.GetAllCelebrityNames();
        }

        public List<string> GetAllCelebrityNames(DateTime lastExecutionDate)
        {
            return _iResourceRepository.GetAllCelebrityNames(lastExecutionDate);
        }

        public List<string> SearchCelebrityName(string term, int pageSize, int pageNumber)
        {
            List<string> result = new List<string>();
            SolrService<SolrManager.InputEntities.ResourceMeta> sServer1 = new SolrService<SolrManager.InputEntities.ResourceMeta>();

            string solrQuery = "(Key:(Celebrity) AND Value:(*" + term + "*))";
            string searchColumn = "Key Value";
            var celebResult = sServer1.SearchEDismax(solrQuery, pageSize, pageNumber, term, searchColumn, true);
            if (celebResult != null && celebResult.Results.Count > 0)
            {
                result = celebResult.Results.Select(x => x.Value).ToList();
            }
            return result;
        }


        public List<Resource> GetResourceByGuidIds(List<string> list)
        {
            return _iResourceRepository.GetResourceByGuidIds(list);
        }





        public List<Resource> GetResourceByBarcodeRange(int start, int end)
        {
            return _iResourceRepository.GetResourceByBarcodeRange(start, end);
        }





        public Resource GetResourceByGuidIdforUserFavourite(Guid guid)
        {
            return _iResourceRepository.GetResourceByGuidIdforUserFavourite(guid);
        }


        public List<string> SearchTag(string term, int pageSize, int pageNumber)
        {
            List<string> result = new List<string>();
            SolrService<SolrManager.InputEntities.ResourceMeta> sServer1 = new SolrService<SolrManager.InputEntities.ResourceMeta>();
            string[] terms = term.Split(new char[] { ' ' });
            string solrquery2 = string.Empty;
            foreach (var item in terms)
            {
                if (solrquery2 == string.Empty)
                {
                    solrquery2 += "Value:" + item;
                }
                else
                {
                    solrquery2 += " AND Value:" + item;
                }
            }

            string solrQuery = solrquery2 + "*";
            string searchColumn = "Value";
            var celebResult = sServer1.QuerySolr(solrQuery, pageSize, pageNumber);
            if (celebResult != null && celebResult.Results.Count > 0)
            {
                result = celebResult.Results.Select(x => x.Value).ToList();
            }
            return result;
        }


        public List<Resource> AdvanceSearch(string term, string metas, int pageSize, int pageNumber, int? resourceTypeId, int? bucketid, bool alldata, out int resourceCount)
        {
            string searchQuery1 = string.Empty; //"(Slug:*" + term + "* OR description:*" + term + "*)";
            string searchQuery2 = string.Empty;
            string searchQuery3 = string.Empty;
            string searchQuery4 = string.Empty;
            string[] allMetas = null;

            if (metas != null)
                allMetas = metas.Replace(" ", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            #region slug and description
            if (term != null)
            {
                string searchSlug = string.Empty;
                string searchDescription = string.Empty;
                string[] slugs = term.Replace(" ", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                string[] descriptions = term.Replace(" ", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                string searchQueryslug = "Slug:" + term + "* OR  ";
                foreach (string slug in slugs)
                {
                    if (searchSlug == string.Empty)
                        searchSlug = "Slug:*" + slug;
                    else
                        searchSlug += " AND Slug:" + slug;
                }

                searchSlug += "*";

                if (slugs.Count() > 1)
                    searchSlug = "(" + searchQueryslug + searchSlug + ")";



                string searchQuerydescription = "description:" + term + "* OR  ";
                foreach (string description in descriptions)
                {
                    if (searchDescription == string.Empty)
                        searchDescription = "description:*" + description;
                    else
                        searchDescription += " AND description:" + description;
                }

                searchDescription += "*";

                if (descriptions.Count() > 1)
                    searchDescription = "(" + searchQuerydescription + searchDescription + ")";


                searchQuery1 = "(" + searchSlug + " OR " + searchDescription + ")";
            }
            #endregion

            #region metas
            if (allMetas != null)
            {
                foreach (string meta in allMetas)
                {
                    if (searchQuery2 == string.Empty)
                        searchQuery2 = "Metas:*" + meta;
                    else
                        searchQuery2 += " AND Metas:" + meta;
                }
            }

            searchQuery2 += "*";
            string searchQueryMeta = "Metas:" + metas + "* OR  ";

            if (metas != null && allMetas.Count() > 1)
                searchQuery2 = "(" + searchQueryMeta + searchQuery2 + ")";

            #endregion

            #region bucket and private
            if (bucketid != null)
                searchQuery3 = "BucketId:" + bucketid;

            if (alldata)
            {
                if (searchQuery3 != string.Empty)
                    searchQuery3 += " OR IsPrivate:false";
                else
                    searchQuery3 = "IsPrivate:false";
            }
            else
            {
                if (searchQuery3 == string.Empty)
                    searchQuery3 = "IsPrivate:true";
            }
            searchQuery3 = "(" + searchQuery3 + ")";
            #endregion

            #region resourcetype
            if (resourceTypeId != null)
            {
                searchQuery4 = "(ResourceTypeId:" + resourceTypeId.Value + ")";
            }
            #endregion

            #region solrQuery
            string solrQuery = string.Empty;

            if (searchQuery1 != string.Empty)
            {
                if (!string.IsNullOrEmpty(searchQuery2))
                    solrQuery = searchQuery1 + " AND " + searchQuery2 + " AND " + searchQuery3;
                else
                    solrQuery = searchQuery1 + " AND " + searchQuery3;
            }
            else
            {
                solrQuery = searchQuery2 + " AND " + searchQuery3;
            }

            if (searchQuery4 != string.Empty)
                solrQuery += solrQuery + " AND " + searchQuery4;

            //set priority
            // solrQuery+=  " OR IsCompleted:true^1000";

            #endregion
            List<Resource> searchResources = new List<Resource>();
            SolrService<SolrResource> sServer = new SolrService<SolrResource>();

            List<SolrNet.SortOrder> sortparams = new List<SortOrder>();
            sortparams.Add(new SortOrder("IsCompleted", Order.DESC));
            sortparams.Add(new SortOrder("score", Order.DESC));
            sortparams.Add(new SortOrder("ResourceDate", Order.DESC));

            var searchResult = sServer.QuerySolr(solrQuery, pageSize, pageNumber, sortparams);
            List<Resource> orderedList = new List<Resource>();
            resourceCount = searchResult.TotalCount;

            if (searchResult != null && searchResult.Results.Count > 0)
            {
                resourceCount = searchResult.TotalCount;
                List<string> resourceList = searchResult.Results.Select(x => x.Guid).ToList();
                searchResources = _iResourceRepository.GetResourceByGuids(resourceList);

                foreach (string guid in resourceList)
                {
                    orderedList.Add(searchResources.Where(x => x.Guid == new Guid(guid)).First());
                }

                InsertsearchHistory(term, pageNumber);
            }

            //if (orderedList != null)
            //    orderedList = orderedList.OrderByDescending(a => a.ResourceDate).ToList();

            return orderedList;
        }


        public List<Resource> SearchMeta(string term, string collectionURL, int pageSize, int pageNumber, int? resourceTypeId, int? bucketid, bool alldata, out int resourceCount)
        {
            List<Resource> searchResources = new List<Resource>();
            SolrService<SolrResource> sServer = new SolrService<SolrResource>();
            string solrQuery = "";

            //(((Slug:*sania mirza* OR description:*sania mirza*) AND Metas:Tennis AND Metas:islam) AND (BucketId:186 OR IsPrivate:false))

            if (!string.IsNullOrEmpty(term))
                solrQuery = "(Slug:" + term + "  description:" + term + ") Metas:" + collectionURL;
            else
                solrQuery = "Metas:" + collectionURL;

            // string solrQuery = "*" + term + "*";
            string mm = "99%"; //minimum should match

            if (bucketid != null)
            {

                if (alldata)
                {
                    solrQuery += " BucketId:" + bucketid.Value + " IsPrivate:false";
                    mm = "99%";
                }
                else
                    solrQuery += " BucketId:" + bucketid.Value;

            }
            else
                solrQuery += " IsPrivate:false";


            if (resourceTypeId != null)
                solrQuery += " ResourceTypeId:" + resourceTypeId.Value;

            string searchColumn = "Slug^2 Metas^10 description";

            var searchResult = sServer.SearchEDismax(solrQuery, pageSize, pageNumber, term, searchColumn, true, null, mm);
            resourceCount = searchResult.TotalCount;
            List<Resource> orderedList = new List<Resource>();

            if (searchResult != null && searchResult.Results.Count > 0)
            {
                List<string> resourceList = searchResult.Results.Select(x => x.Guid).ToList();
                searchResources = _iResourceRepository.GetResourceByGuids(resourceList);

                foreach (string guid in resourceList)
                {
                    orderedList.Add(searchResources.Where(x => x.Guid == new Guid(guid)).First());
                }

                InsertsearchHistory(term, pageNumber);
            }

            //if (orderedList != null)
            //    orderedList = orderedList.OrderByDescending(a => a.ResourceDate).ToList();

            return orderedList;
        }


        public List<Resource> GetAllResourceWithDetailAndMetas()
        {
            throw new NotImplementedException();
        }

        public List<Resource> GetResourcesAfterThisDate(long dateStamp)
        {
            return _iResourceRepository.GetResourcesAfterThisDate(dateStamp);
        }

        public void GenerateFileInformation(Resource res, string tempFilePath)
        {
            string ffmpegPath = AppDomain.CurrentDomain.BaseDirectory + "\\ffmpeg\\";
            MS.Core.Enums.ResourceTypes resType = MS.Core.Enums.ResourceTypes.Video;

            FfProbeInformation info = FFMPEGLib.FFMPEG.GetMediaInformation(tempFilePath);

            if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
            {
                resType = MS.Core.Enums.ResourceTypes.Image;
                if (info != null && info.streams != null && info.streams.Count > 0)
                {
                    res.Width = info.streams[0].width;
                    res.Height = info.streams[0].height;
                }
            }
            else if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video)
            {
                resType = MS.Core.Enums.ResourceTypes.Video;
                if (info != null && info.streams != null && info.streams.Count > 0)
                {
                    foreach (Stream s in info.streams)
                    {
                        if (s.codec_type == "audio")
                        {
                            res.AudioCodec = s.codec_name;
                        }
                        if (s.codec_type == "video")
                        {
                            res.VideoCodec = s.codec_name;
                            res.Duration = Convert.ToDouble(s.duration);
                            res.Bitrate = s.bit_rate;
                            res.Framerate = s.avg_frame_rate;
                            res.Width = s.width;
                            res.Height = s.height;
                        }
                    }
                }
            }

            _iResourceRepository.UpdateResource(res);
        }

        public List<Resource> GetResourcesForLowResTranscoding(int count)
        {
            return _iResourceRepository.GetResourcesForLowResTranscoding(count);
        }

        public Resource UpdateResourceWithTimeStamp(Resource entity)
        {
            return _iResourceRepository.UpdateResourceWithTimeStamp(entity);
        }

        public List<Resource> GetResourceIdFilePathandSubBucketIdByParent(int bucketId)
        {

            return _iResourceRepository.GetResourceIdFilePathandSubBucketIdByParent(bucketId);
        }

        public bool UpdateResourceStatus(int resourceId, int statusId)
        {
            return _iResourceRepository.UpdateResourceStatus(resourceId, statusId);
        }

        public bool UpdateResourceServerName(string ServerName)
        {
            return _iResourceRepository.UpdateResourceServerName(ServerName);
        }


        public List<Resource> GetResourceByStatusIdAfterDate(long dateStamp, int statusid)
        {
            return _iResourceRepository.GetResourceByStatusIdAfterDate(dateStamp, statusid);
        }


        public List<Resource> GetdeletedResources()
        {
            return _iResourceRepository.GetdeletedResources();
        }
    }


}
