﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceStatus;
using Validation;
using System.Linq;
using MS.Core;

namespace MS.Service
{
		
	public class ResourceStatusService : IResourceStatusService 
	{
		private IResourceStatusRepository _iResourceStatusRepository;
        
		public ResourceStatusService(IResourceStatusRepository iResourceStatusRepository)
		{
			this._iResourceStatusRepository = iResourceStatusRepository;
		}
        
        public Dictionary<string, string> GetResourceStatusBasicSearchColumns()
        {
            
            return this._iResourceStatusRepository.GetResourceStatusBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetResourceStatusAdvanceSearchColumns()
        {
            
            return this._iResourceStatusRepository.GetResourceStatusAdvanceSearchColumns();
           
        }
        

		public ResourceStatus GetResourceStatus(System.Int32 ResourceStatusId)
		{
			return _iResourceStatusRepository.GetResourceStatus(ResourceStatusId);
		}

		public ResourceStatus UpdateResourceStatus(ResourceStatus entity)
		{
			return _iResourceStatusRepository.UpdateResourceStatus(entity);
		}

		public bool DeleteResourceStatus(System.Int32 ResourceStatusId)
		{
			return _iResourceStatusRepository.DeleteResourceStatus(ResourceStatusId);
		}

		public List<ResourceStatus> GetAllResourceStatus()
		{
			return _iResourceStatusRepository.GetAllResourceStatus();
		}

		public ResourceStatus InsertResourceStatus(ResourceStatus entity)
		{
			 return _iResourceStatusRepository.InsertResourceStatus(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourcestatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcestatusid))
            {
				ResourceStatus resourcestatus = _iResourceStatusRepository.GetResourceStatus(resourcestatusid);
                if(resourcestatus!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resourcestatus);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ResourceStatus> resourcestatuslist = _iResourceStatusRepository.GetAllResourceStatus();
            if (resourcestatuslist != null && resourcestatuslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourcestatuslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ResourceStatus resourcestatus = new ResourceStatus();
                PostOutput output = new PostOutput();
                resourcestatus.CopyFrom(Input);
                resourcestatus = _iResourceStatusRepository.InsertResourceStatus(resourcestatus);
                output.CopyFrom(resourcestatus);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ResourceStatus resourcestatusinput = new ResourceStatus();
                ResourceStatus resourcestatusoutput = new ResourceStatus();
                PutOutput output = new PutOutput();
                resourcestatusinput.CopyFrom(Input);
                ResourceStatus resourcestatus = _iResourceStatusRepository.GetResourceStatus(resourcestatusinput.ResourceStatusId);
                if (resourcestatus!=null)
                {
                    resourcestatusoutput = _iResourceStatusRepository.UpdateResourceStatus(resourcestatusinput);
                    if(resourcestatusoutput!=null)
                    {
                        output.CopyFrom(resourcestatusoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourcestatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcestatusid))
            {
				 bool IsDeleted = _iResourceStatusRepository.DeleteResourceStatus(resourcestatusid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
