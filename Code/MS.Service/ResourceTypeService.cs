﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.ResourceType;
using Validation;
using System.Linq;
using MS.Core;

namespace MS.Service
{
		
	public class ResourceTypeService : IResourceTypeService 
	{
		private IResourceTypeRepository _iResourceTypeRepository;
        
		public ResourceTypeService(IResourceTypeRepository iResourceTypeRepository)
		{
			this._iResourceTypeRepository = iResourceTypeRepository;
		}
        
        public Dictionary<string, string> GetResourceTypeBasicSearchColumns()
        {
            
            return this._iResourceTypeRepository.GetResourceTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetResourceTypeAdvanceSearchColumns()
        {
            
            return this._iResourceTypeRepository.GetResourceTypeAdvanceSearchColumns();
           
        }
        

		public ResourceType GetResourceType(System.Int32 ResourceTypeId)
		{
			return _iResourceTypeRepository.GetResourceType(ResourceTypeId);
		}

		public ResourceType UpdateResourceType(ResourceType entity)
		{
			return _iResourceTypeRepository.UpdateResourceType(entity);
		}

		public bool DeleteResourceType(System.Int32 ResourceTypeId)
		{
			return _iResourceTypeRepository.DeleteResourceType(ResourceTypeId);
		}

		public List<ResourceType> GetAllResourceType()
		{
			return _iResourceTypeRepository.GetAllResourceType();
		}

		public ResourceType InsertResourceType(ResourceType entity)
		{
			 return _iResourceTypeRepository.InsertResourceType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourcetypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcetypeid))
            {
				ResourceType resourcetype = _iResourceTypeRepository.GetResourceType(resourcetypeid);
                if(resourcetype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resourcetype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ResourceType> resourcetypelist = _iResourceTypeRepository.GetAllResourceType();
            if (resourcetypelist != null && resourcetypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourcetypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ResourceType resourcetype = new ResourceType();
                PostOutput output = new PostOutput();
                resourcetype.CopyFrom(Input);
                resourcetype = _iResourceTypeRepository.InsertResourceType(resourcetype);
                output.CopyFrom(resourcetype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ResourceType resourcetypeinput = new ResourceType();
                ResourceType resourcetypeoutput = new ResourceType();
                PutOutput output = new PutOutput();
                resourcetypeinput.CopyFrom(Input);
                ResourceType resourcetype = _iResourceTypeRepository.GetResourceType(resourcetypeinput.ResourceTypeId);
                if (resourcetype!=null)
                {
                    resourcetypeoutput = _iResourceTypeRepository.UpdateResourceType(resourcetypeinput);
                    if(resourcetypeoutput!=null)
                    {
                        output.CopyFrom(resourcetypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourcetypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourcetypeid))
            {
				 bool IsDeleted = _iResourceTypeRepository.DeleteResourceType(resourcetypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
