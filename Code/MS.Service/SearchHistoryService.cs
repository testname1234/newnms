﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.SearchHistory;
using Validation;
using System.Linq;

namespace MS.Service
{
		
	public class SearchHistoryService : ISearchHistoryService 
	{
		private ISearchHistoryRepository _iSearchHistoryRepository;
        
		public SearchHistoryService(ISearchHistoryRepository iSearchHistoryRepository)
		{
			this._iSearchHistoryRepository = iSearchHistoryRepository;
		}
        
        public Dictionary<string, string> GetSearchHistoryBasicSearchColumns()
        {
            
            return this._iSearchHistoryRepository.GetSearchHistoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSearchHistoryAdvanceSearchColumns()
        {
            
            return this._iSearchHistoryRepository.GetSearchHistoryAdvanceSearchColumns();
           
        }
        

		public SearchHistory GetSearchHistory(System.Int32 SearchHistoryId)
		{
			return _iSearchHistoryRepository.GetSearchHistory(SearchHistoryId);
		}

		public SearchHistory UpdateSearchHistory(SearchHistory entity)
		{
			return _iSearchHistoryRepository.UpdateSearchHistory(entity);
		}

		public bool DeleteSearchHistory(System.Int32 SearchHistoryId)
		{
			return _iSearchHistoryRepository.DeleteSearchHistory(SearchHistoryId);
		}

		public List<SearchHistory> GetAllSearchHistory()
		{
			return _iSearchHistoryRepository.GetAllSearchHistory();
		}

		public SearchHistory InsertSearchHistory(SearchHistory entity)
		{
			 return _iSearchHistoryRepository.InsertSearchHistory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 searchhistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out searchhistoryid))
            {
				SearchHistory searchhistory = _iSearchHistoryRepository.GetSearchHistory(searchhistoryid);
                if(searchhistory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(searchhistory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SearchHistory> searchhistorylist = _iSearchHistoryRepository.GetAllSearchHistory();
            if (searchhistorylist != null && searchhistorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(searchhistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SearchHistory searchhistory = new SearchHistory();
                PostOutput output = new PostOutput();
                searchhistory.CopyFrom(Input);
                searchhistory = _iSearchHistoryRepository.InsertSearchHistory(searchhistory);
                output.CopyFrom(searchhistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SearchHistory searchhistoryinput = new SearchHistory();
                SearchHistory searchhistoryoutput = new SearchHistory();
                PutOutput output = new PutOutput();
                searchhistoryinput.CopyFrom(Input);
                SearchHistory searchhistory = _iSearchHistoryRepository.GetSearchHistory(searchhistoryinput.SearchHistoryId);
                if (searchhistory!=null)
                {
                    searchhistoryoutput = _iSearchHistoryRepository.UpdateSearchHistory(searchhistoryinput);
                    if(searchhistoryoutput!=null)
                    {
                        output.CopyFrom(searchhistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 searchhistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out searchhistoryid))
            {
				 bool IsDeleted = _iSearchHistoryRepository.DeleteSearchHistory(searchhistoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<string> GetAllSearchHistoryKeywords()
         {
             return _iSearchHistoryRepository.GetAllSearchHistoryKeywords();
         }

         List<string> ISearchHistoryService.GetAllSearchHistoryKeywords(DateTime lastUpdateDate)
         {
             return _iSearchHistoryRepository.GetAllSearchHistoryKeywords(lastUpdateDate);
         }
    }
	
	
}
