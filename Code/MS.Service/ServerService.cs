﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.Server;
using Validation;
using System.Linq;
using MS.Core;

namespace MS.Service
{
		
	public class ServerService : IServerService 
	{
		private IServerRepository _iServerRepository;
        
		public ServerService(IServerRepository iServerRepository)
		{
			this._iServerRepository = iServerRepository;
		}
        
        public Dictionary<string, string> GetServerBasicSearchColumns()
        {
            
            return this._iServerRepository.GetServerBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetServerAdvanceSearchColumns()
        {
            
            return this._iServerRepository.GetServerAdvanceSearchColumns();
           
        }
        

		public Server GetServer(System.Int32 ServerId)
		{
			return _iServerRepository.GetServer(ServerId);
		}

        public Server GetDefaultServer()
        {
            return _iServerRepository.GetDefaultServer();
        }

		public Server UpdateServer(Server entity)
		{
			return _iServerRepository.UpdateServer(entity);
		}

		public bool DeleteServer(System.Int32 ServerId)
		{
			return _iServerRepository.DeleteServer(ServerId);
		}

		public List<Server> GetAllServer()
		{
			return _iServerRepository.GetAllServer();
		}

		public Server InsertServer(Server entity)
		{
			 return _iServerRepository.InsertServer(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 serverid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out serverid))
            {
				Server server = _iServerRepository.GetServer(serverid);
                if(server!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(server);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Server> serverlist = _iServerRepository.GetAllServer();
            if (serverlist != null && serverlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(serverlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Server server = new Server();
                PostOutput output = new PostOutput();
                server.CopyFrom(Input);
                server = _iServerRepository.InsertServer(server);
                output.CopyFrom(server);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Server serverinput = new Server();
                Server serveroutput = new Server();
                PutOutput output = new PutOutput();
                serverinput.CopyFrom(Input);
                Server server = _iServerRepository.GetServer(serverinput.ServerId);
                if (server!=null)
                {
                    serveroutput = _iServerRepository.UpdateServer(serverinput);
                    if(serveroutput!=null)
                    {
                        output.CopyFrom(serveroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 serverid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out serverid))
            {
				 bool IsDeleted = _iServerRepository.DeleteServer(serverid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
