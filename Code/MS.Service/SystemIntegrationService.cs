﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.SystemIntegration;
using Validation;
using System.Linq;

namespace MS.Core.Service
{
		
	public class SystemIntegrationService : ISystemIntegrationService 
	{
		private ISystemIntegrationRepository _iSystemIntegrationRepository;
        
		public SystemIntegrationService(ISystemIntegrationRepository iSystemIntegrationRepository)
		{
			this._iSystemIntegrationRepository = iSystemIntegrationRepository;
		}
        
        public Dictionary<string, string> GetSystemIntegrationBasicSearchColumns()
        {
            
            return this._iSystemIntegrationRepository.GetSystemIntegrationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSystemIntegrationAdvanceSearchColumns()
        {
            
            return this._iSystemIntegrationRepository.GetSystemIntegrationAdvanceSearchColumns();
           
        }
        

		public SystemIntegration GetSystemIntegration(System.Int32 Id)
		{
			return _iSystemIntegrationRepository.GetSystemIntegration(Id);
		}

		public SystemIntegration UpdateSystemIntegration(SystemIntegration entity)
		{
			return _iSystemIntegrationRepository.UpdateSystemIntegration(entity);
		}

		public bool DeleteSystemIntegration(System.Int32 Id)
		{
			return _iSystemIntegrationRepository.DeleteSystemIntegration(Id);
		}

		public List<SystemIntegration> GetAllSystemIntegration()
		{
			return _iSystemIntegrationRepository.GetAllSystemIntegration();
		}

		public SystemIntegration InsertSystemIntegration(SystemIntegration entity)
		{
			 return _iSystemIntegrationRepository.InsertSystemIntegration(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 id=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out id))
            {
				SystemIntegration systemintegration = _iSystemIntegrationRepository.GetSystemIntegration(id);
                if(systemintegration!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(systemintegration);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SystemIntegration> systemintegrationlist = _iSystemIntegrationRepository.GetAllSystemIntegration();
            if (systemintegrationlist != null && systemintegrationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(systemintegrationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SystemIntegration systemintegration = new SystemIntegration();
                PostOutput output = new PostOutput();
                systemintegration.CopyFrom(Input);
                systemintegration = _iSystemIntegrationRepository.InsertSystemIntegration(systemintegration);
                output.CopyFrom(systemintegration);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SystemIntegration systemintegrationinput = new SystemIntegration();
                SystemIntegration systemintegrationoutput = new SystemIntegration();
                PutOutput output = new PutOutput();
                systemintegrationinput.CopyFrom(Input);
                SystemIntegration systemintegration = _iSystemIntegrationRepository.GetSystemIntegration(systemintegrationinput.Id);
                if (systemintegration!=null)
                {
                    systemintegrationoutput = _iSystemIntegrationRepository.UpdateSystemIntegration(systemintegrationinput);
                    if(systemintegrationoutput!=null)
                    {
                        output.CopyFrom(systemintegrationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 id=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out id))
            {
				 bool IsDeleted = _iSystemIntegrationRepository.DeleteSystemIntegration(id);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
