﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using MS.Core.DataTransfer.UserFavourites;
using Validation;
using System.Linq;
using SolrManager;
using SolrManager.InputEntities;
using System.Configuration;


namespace MS.Service
{
		
	public class UserFavouritesService : IUserFavouritesService 
	{
		private IUserFavouritesRepository _iUserFavouritesRepository;
        private IResourceRepository _iResourceRepository;
        public UserFavouritesService(IUserFavouritesRepository iUserFavouritesRepository, IResourceRepository iResourceRepository)
		{
			this._iUserFavouritesRepository = iUserFavouritesRepository;
            this._iResourceRepository = iResourceRepository;
		}
        
        public Dictionary<string, string> GetUserFavouritesBasicSearchColumns()
        {
            
            return this._iUserFavouritesRepository.GetUserFavouritesBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetUserFavouritesAdvanceSearchColumns()
        {
            
            return this._iUserFavouritesRepository.GetUserFavouritesAdvanceSearchColumns();
           
        }
        

		public UserFavourites GetUserFavourites(System.Int32 UserFavouritesId)
		{
			return _iUserFavouritesRepository.GetUserFavourites(UserFavouritesId);
		}

		public UserFavourites UpdateUserFavourites(UserFavourites entity)
		{
			return _iUserFavouritesRepository.UpdateUserFavourites(entity);
		}

		public bool DeleteUserFavourites(System.Int32 UserFavouritesId)
		{
			return _iUserFavouritesRepository.DeleteUserFavourites(UserFavouritesId);
		}

		public List<UserFavourites> GetAllUserFavourites()
		{
			return _iUserFavouritesRepository.GetAllUserFavourites();
		}

		public UserFavourites InsertUserFavourites(UserFavourites entity)
		{
			 return _iUserFavouritesRepository.InsertUserFavourites(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 userfavouritesid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out userfavouritesid))
            {
				UserFavourites userfavourites = _iUserFavouritesRepository.GetUserFavourites(userfavouritesid);
                if(userfavourites!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(userfavourites);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<UserFavourites> userfavouriteslist = _iUserFavouritesRepository.GetAllUserFavourites();
            if (userfavouriteslist != null && userfavouriteslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(userfavouriteslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                UserFavourites userfavourites = new UserFavourites();
                PostOutput output = new PostOutput();
                userfavourites.CopyFrom(Input);
                userfavourites = _iUserFavouritesRepository.InsertUserFavourites(userfavourites);
                output.CopyFrom(userfavourites);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                UserFavourites userfavouritesinput = new UserFavourites();
                UserFavourites userfavouritesoutput = new UserFavourites();
                PutOutput output = new PutOutput();
                userfavouritesinput.CopyFrom(Input);
                UserFavourites userfavourites = _iUserFavouritesRepository.GetUserFavourites(userfavouritesinput.UserFavouritesId);
                if (userfavourites!=null)
                {
                    userfavouritesoutput = _iUserFavouritesRepository.UpdateUserFavourites(userfavouritesinput);
                    if(userfavouritesoutput!=null)
                    {
                        output.CopyFrom(userfavouritesoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 userfavouritesid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out userfavouritesid))
            {
				 bool IsDeleted = _iUserFavouritesRepository.DeleteUserFavourites(userfavouritesid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<UserFavourites> GetUserFavouritesByUserId(int UserId)
         {
           return   _iUserFavouritesRepository.GetUserFavouritesByUserId(UserId);
         }


         public bool DeleteUserFavouritesByGuid(int UserId, string guid)
         {
             //List<UserFavourites> lstuserfav  = _iUserFavouritesRepository.DeleteUserFavouritesbYUserIdandGuid(UserId, guid);
             bool isupdated = false;
             isupdated = _iUserFavouritesRepository.DeleteUserFavouritesbYUserIdandGuid(UserId, guid);
             
             //List<Resource> listResource = new List<Resource>();
             //List<SolrResource> solrResouces = new List<SolrResource>();
             //SolrResource solrResource = new SolrResource();
             //if (lstuserfav != null)
             //{
             //    List<int> UserIds = new List<int>();

             //    foreach (UserFavourites item in lstuserfav)
             //    {
             //        if(item.UserId!=null)
             //        {
             //           UserIds.Add(Convert.ToInt32(item.UserId));
             //        }
             //    }

             //    SolrService<SolrResource> solrService = new SolrService<SolrResource>();
             //    string solrCollectionUrl = ConfigurationManager.AppSettings["solrUrl2"];

             //    try
             //    {
             //        solrService.Connect(solrCollectionUrl);
             //    }
             //    catch (Exception ex)
             //    {

             //    }

             //    Resource resource = _iResourceRepository.GetResourceByGuidId(new Guid(guid));
             //    if (resource != null)
             //    {
             //        solrResource.CopyFrom(resource);
             //        solrResource.UserIds = UserIds;
             //        solrResouces.Add(solrResource);
             //        solrService.AddRecords(solrResouces);
             //        return true;
             //    }
             //}

             return isupdated;

         }
    }
	
	
}
