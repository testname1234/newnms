﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using MS.Core.Entities;
using MS.Core.DataInterfaces;
using MS.Core.IService;
using MS.Core.DataTransfer;
using Validation;
using System.Linq;

namespace MS.Service
{
		
	public class VideoClipTaskService : IVideoClipTaskService 
	{
		private IVideoClipTaskRepository _iVideoClipTaskRepository;
        
		public VideoClipTaskService(IVideoClipTaskRepository iVideoClipTaskRepository)
		{
			this._iVideoClipTaskRepository = iVideoClipTaskRepository;
		}
        
        public Dictionary<string, string> GetVideoClipTaskBasicSearchColumns()
        {
            
            return this._iVideoClipTaskRepository.GetVideoClipTaskBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetVideoClipTaskAdvanceSearchColumns()
        {
            
            return this._iVideoClipTaskRepository.GetVideoClipTaskAdvanceSearchColumns();
           
        }
        

		public VideoClipTask GetVideoClipTask(System.Int32 VideoClipTaskId)
		{
			return _iVideoClipTaskRepository.GetVideoClipTask(VideoClipTaskId);
		}

		public VideoClipTask UpdateVideoClipTask(VideoClipTask entity)
		{
			return _iVideoClipTaskRepository.UpdateVideoClipTask(entity);
		}

		public bool DeleteVideoClipTask(System.Int32 VideoClipTaskId)
		{
			return _iVideoClipTaskRepository.DeleteVideoClipTask(VideoClipTaskId);
		}

		public List<VideoClipTask> GetAllVideoClipTask()
		{
			return _iVideoClipTaskRepository.GetAllVideoClipTask();
		}

		public VideoClipTask InsertVideoClipTask(VideoClipTask entity)
		{
			 return _iVideoClipTaskRepository.InsertVideoClipTask(entity);
		}


         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 videocliptaskid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out videocliptaskid))
            {
				 bool IsDeleted = _iVideoClipTaskRepository.DeleteVideoClipTask(videocliptaskid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public List<VideoClipTask> GetVideoClipByStatusId(int VideoClipStatusId)
         {
             return _iVideoClipTaskRepository.GetVideoClipByStatusId(VideoClipStatusId);
         }
	}
	
	
}
