﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Repository;
using NMS.Service;


namespace CameraApplication
{
    public partial class MediaUploader : Form
    {
        IAssignmentService assignmentService = IoC.Resolve<IAssignmentService>("AssignmentService");
        IAssignmentResourceService assignmentResourceService = IoC.Resolve<IAssignmentResourceService>("AssignmentResourceService");

        public MediaUploader()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //AssignmentRepository assgRepo = new AssignmentRepository();
            //Assignment assign = assgRepo.GetByBarcode(Convert.ToInt32(txtBarcode.Text));
            try
            {
                Convert.ToInt32(txtBarcode.Text);
            }
            catch
            {
                hdnAssign.Text = "";
                txtSlug.Text = "";
                MessageBox.Show("Invalid Barcode.");
                return;
            }
            Assignment assignment = assignmentService.GetAssignmentByKeyValue("Barcode", txtBarcode.Text);
            if (assignment != null && assignment.AssignmentId != 0)
            {
                hdnAssign.Text = assignment.AssignmentId.ToString();
                txtSlug.Text = assignment.Slug;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (hdnAssign.Text == "")
            {
                MessageBox.Show("Enter Barcode.");
                return;
            }
            DialogResult result = folderBrowserDialog1.ShowDialog();
            txtPath.Text = folderBrowserDialog1.SelectedPath;
            txtPath.Refresh();
            if (result == DialogResult.OK)
            {
                this.Refresh();
                float Max = 100;
                float ratio = 0;
                float TotalFiles = 0;
                //string fileName = "test";
                string sourcePath = folderBrowserDialog1.SelectedPath;
                string targetPath = ConfigurationManager.AppSettings["TargetPath"];

                // Use Path class to manipulate file and directory paths.
                //string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                //string destFile = System.IO.Path.Combine(targetPath, fileName);

                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }
                //System.IO.File.Copy(sourceFile, destFile, true);

                if (System.IO.Directory.Exists(sourcePath))
                {
                    MS.MediaIntegration.API.MSApi MediaApi = new MS.MediaIntegration.API.MSApi();

                    string[] files = System.IO.Directory.GetFiles(sourcePath);
                    TotalFiles = files.Length;
                    ratio = Max / TotalFiles;
                    int fileMoved = 0;
                    checkedListBox1.Items.Clear();
                    checkedListBox1.SelectionMode = SelectionMode.None;
                    foreach (string file in files)
                    {
                        checkedListBox1.Items.Add(file, CheckState.Unchecked);
                    }
                    checkedListBox1.Refresh();
                    foreach (string file in files)
                    {
                        checkedListBox1.SetItemCheckState(fileMoved, CheckState.Indeterminate);
                        checkedListBox1.Refresh();
                        MS.Core.DataTransfer.Resource.PostInput input = new MS.Core.DataTransfer.Resource.PostInput();
                        input.BucketId = ((int)NMS.Core.Enums.NMSBucket.NMSBucket);
                        input.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"];
                        string ext = System.IO.Path.GetExtension(file);
                        input.ResourceTypeId = ((int)MS.Core.Helper.ExtensionHelper.GetResourceType(ext.Replace(".", "").ToLower())).ToString();
                        input.FileName = System.IO.Path.GetFileName(file);
                        input.Source = file;
                        input.CreationDate = DateTime.UtcNow.ToString();
                        input.LastUpdateDate = input.CreationDate;
                        input.Caption = txtSlug.Text;
                        string folder = ConfigurationManager.AppSettings["FolderName"];
                        string dir1 = "/" + folder;
                        string dir2 = "/" + folder + "/" + DateTime.UtcNow.Year.ToString();
                        string dir3 = "/" + folder + "/" + DateTime.UtcNow.Year.ToString() + "/" + DateTime.UtcNow.Month.ToString("00");
                        string dir4 = "/" + folder + "/" + DateTime.UtcNow.Year.ToString() + "/" + DateTime.UtcNow.Month.ToString("00") + "/" + DateTime.UtcNow.Day.ToString("00");
                        List<string> lstDir = new List<string>();
                        lstDir.Add(dir1);
                        lstDir.Add(dir2);
                        lstDir.Add(dir3);
                        lstDir.Add(dir4);
                        foreach (string d in lstDir)
                        {
                            var Result = MediaApi.CreateSubBucket(input.BucketId, input.ApiKey, d);
                        }
                        input.FilePath = dir4 + "/" + input.FileName;

                        MS.Core.DataTransfer.Resource.PostOutput output = MediaApi.PostResource(input);

                        AssignmentResource assignmentResource = new AssignmentResource();
                        assignmentResource.AssignmentId = Convert.ToInt32(hdnAssign.Text);
                        assignmentResource.Guid = output.Guid.ToString();
                        assignmentResource.CreationDate = DateTime.UtcNow;
                        assignmentResource.LastUpdateDate = assignmentResource.CreationDate;
                        assignmentResource.IsActive = true;
                        assignmentResourceService.InsertAssignmentResource(assignmentResource);

                        fileMoved = fileMoved + 1;
                        string fileName = System.IO.Path.GetFileName(file);
                        string destFile = System.IO.Path.Combine(targetPath, output.Guid.ToString() + ext);
                        System.IO.File.Copy(file, destFile, true);
                        lbMovedCount.Text = Convert.ToString(fileMoved) + "/" + TotalFiles;
                        lbMovedCount.Refresh();
                        progressBar1.Minimum = 0;
                        progressBar1.Maximum = 100;
                        progressBar1.Value = Convert.ToInt32(fileMoved * ratio);
                        checkedListBox1.SetItemCheckState(fileMoved - 1, CheckState.Checked);
                        checkedListBox1.Refresh();
                    }
                }
                else
                {
                    Console.WriteLine("Source path does not exist!");
                }

            }
        }

    }
}
