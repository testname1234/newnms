﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.Helper;
using MS.Core.IService;
using MS.MediaStorage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Ms.ProcessThreads
{

    [Serializable]
    public class SanFileDirectory
    {

        public string DirectoryPath { get; set; }
        public List<SanFile> Files { get; set; }
    }
    [Serializable]
    public class SanFile
    {
        public string FilePath { get; set; }

        public int BucketId { get; set; }

        public int ResourceId { get; set; }

        public string FullPath { get; set; }


    }
    public class BucketWatcherArguement
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public string APIKey { get; set; }

        public string TempPath { get; set; }

        public string Location { get; set; }

        public int ServerId { get; set; }


    }


    public class BucketWatcherThread: ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            DateTime startTime = DateTime.UtcNow;
           
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");

            try
            {
                //FileSystemWatcher

                if (Process.GetProcessesByName("FileChangeNotifier").Length == 0)
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"C:\FileWatcherSingleLocationUtility\FileChangeNotifier.exe";
                    Process.Start(startInfo);
                }
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in BucketWatcherThreadFileUtityMap-{1}: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
            }

            return string.Format("Took {0} seconds to Oen Utility Process", (DateTime.UtcNow - startTime).TotalSeconds.ToString("0.0"));

            IBucketService bucketService = IoC.Resolve<IBucketService>("BucketService");
            IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");

            BucketWatcherArguement bucketWatcherArguement = JSONhelper.GetObject<BucketWatcherArguement>(argument);

            int resourceCount = 0;
            int bucketCount = 0;

            string apiKey = bucketWatcherArguement.APIKey;

            int mainBucketId = bucketWatcherArguement.Id;
            string location = bucketWatcherArguement.Location;
            Storage storage = new Storage(mainBucketId, apiKey);

            int serverId = bucketWatcherArguement.ServerId;
            string bucketName = bucketWatcherArguement.Name.ToLower();
            bucketName = bucketName.ToLower();
            string parentBucketfolderPath = bucketWatcherArguement.TempPath.ToLower();
            if (parentBucketfolderPath.Substring(parentBucketfolderPath.Length - 1) != "\\")
                parentBucketfolderPath += "\\";
            string[] files = Directory.GetFiles(parentBucketfolderPath, "*.*", SearchOption.AllDirectories);

            List<Resource> resources = resourceService.GetResourceIdFilePathandSubBucketIdByParent(mainBucketId);
            if (resources == null) resources = new List<Resource>();
            resources = resources.Where(x => x.ResourceStatusId == (int)ResourceStatuses.TempFile).ToList();
            if (resources == null) resources = new List<Resource>();

            List<Bucket> buckets = bucketService.GetBucketsByParentId(mainBucketId);
            if (buckets == null) buckets = new List<Bucket>();

            Dictionary<string, Resource> resourceDictionary = new Dictionary<string, Resource>();
            foreach (var res in resources)
            {
                resourceDictionary[res.FilePath.ToLower()] = res;
            }




            for (var i = 0; i < files.Length; i++)
            {
                try
                {
                    if (files[i].Length >= 1000 || files[i].ToLower().Contains("thumbs.db"))
                        i++;
                    if (i == files.Length)
                        break;
                    string fileName = files[i].ToLower();
                    string fPath = fileName.Replace(parentBucketfolderPath, "");
                    string filePath = "\\" + fPath;
                    if (!resourceDictionary.ContainsKey(filePath))
                    {

                        FileInfo fileInfo = new FileInfo(fileName);
                        if (fileInfo.Exists)
                        {

                            string[] array = fPath.Split('\\');
                            int parentBucketId = mainBucketId;

                            string path = "";

                            if (array.Length > 1)
                            {
                                Bucket currentBucket = null;
                                for (var j = 0; j < array.Length - 1; j++)
                                {
                                    path = path + @"\" + array[j];

                                    currentBucket = buckets.Where(x => x.Path.ToLower() == path && x.ParentId == mainBucketId).FirstOrDefault();
                                    if (currentBucket == null)
                                    {

                                        string directoryPath = parentBucketfolderPath + @"\" + path;
                                        DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);

                                        DirectoryInfo info = new DirectoryInfo(directoryPath);
                                        if (info.Exists)
                                        {

                                            currentBucket = storage.CreateBucket(path, info.CreationTime, info.LastWriteTime);
                                            bucketCount++;



                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    parentBucketId = currentBucket.BucketId;


                                }


                            }

                            MS.Core.Entities.Resource resource = new MS.Core.Entities.Resource();
                            var flPath = path + @"\" + array[array.Length - 1];

                            resource = new MS.Core.Entities.Resource();
                            resource.FilePath = flPath;
                            resource.BucketId = mainBucketId;
                            resource.SubBucketId = parentBucketId;
                            resource.Source = fileInfo.FullName;
                            resource.FileName = fileInfo.Name;
                            resource.Category = "";
                            resource.Location = location;
                            resource.HighResolutionFile = string.Empty;
                            resource.LowResolutionFile = string.Empty;
                            resource.ThumbUrl = string.Empty;
                            resource.Caption = fileInfo.Name;
                            resource.ResourceStatusId = 11;
                            resource.Bitrate = string.Empty;
                            resource.Framerate = string.Empty;
                            resource.AudioCodec = string.Empty;
                            resource.VideoCodec = string.Empty;
                            resource.ServerId = serverId;
                            resource.SourceName = "SAN";
                            DateTime creationDate = fileInfo.CreationTimeUtc;
                            if (fileInfo.CreationTimeUtc > DateTime.UtcNow.AddHours(1))
                                creationDate = DateTime.UtcNow;
                            resource.CreationDate = creationDate;

                            resource.LastUpdateDate = DateTime.UtcNow;
                            resource.ResourceTypeId = (int)MS.Core.Enums.ResourceTypes.None;

                            resource.Guid = Guid.NewGuid();
                            string ext = System.IO.Path.GetExtension(fileInfo.Name);
                            resource.ResourceTypeId = (int)ExtensionHelper.GetResourceType(ext);

                            if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video || resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image || resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.ProgramBarCode)
                            {
                                resource.AllowLowResTrancoding = true;
                                resource.ThumbUrl = storage.GenerateSnapShot(parentBucketId, fileInfo.FullName, fileInfo.Name, resource.ResourceTypeId);
                            }
                            MS.Core.DataTransfer.Resource.PostInput resourceMetas = new MS.Core.DataTransfer.Resource.PostInput();

                            resource.isHD = false;
                            resource = resourceService.InsertResource(resource);
                            resourceCount++;
                            resourceMetas.CopyFrom(resource);



                            resourceMetas.ResourceMeta = new List<KeyValuePair<string, string>>();
                            for (var j = 0; j < array.Length - 1; j++)
                            {
                                resourceMetas.ResourceMeta.Add(new KeyValuePair<string, string>("Tag", array[j]));
                            }
                            storage.AddFileMetasWithOutChecking(resourceMetas);
                            Console.WriteLine(resource.Source);


                        }
                    }
                    else
                    {
                        resourceDictionary.Remove(filePath);
                    }





                }
                catch (Exception exp)
                {
                    logService.InsertSystemEventLog(string.Format("Error in BucketWatcherThread-{1}: {0}", exp.Message, bucketName), exp.StackTrace, EventCodes.Error);
                }


            }

            var deletedResources = resourceDictionary.Values;
            if (deletedResources != null && files.Length > 0)
            {
                foreach (var resource in deletedResources)
                {
                    resourceService.UpdateResourceStatus(resource.ResourceId, (int)ResourceStatuses.Deleted);
                }
            }

            Console.WriteLine(string.Format("Took {0} seconds to Add {1} buckets and {2} resources in bucket {3}", (DateTime.UtcNow - startTime).TotalSeconds.ToString("0.0"), bucketCount, resourceCount, bucketName));

            return string.Format("Took {0} seconds to Oen Utility Process", (DateTime.UtcNow - startTime).TotalSeconds.ToString("0.0"));

        }
    }
}
