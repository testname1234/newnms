﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using FFMPEGLib;
using MS.Core.Entities;
using MS.Core.IService;
using MS.Core.Models;
using Newtonsoft.Json;

namespace Ms.ProcessThreads
{
    public class ClipAndMergeVideosThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IVideoClipTaskService vcService = MS.Core.IoC.Resolve<IVideoClipTaskService>("VideoClipTaskService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            IBucketService bucketService = MS.Core.IoC.Resolve<IBucketService>("BucketService");
            IServerService serverService = MS.Core.IoC.Resolve<IServerService>("ServerService");

            int successCount = 0, totalCount = 0;

            try
            {
                List<VideoClipTask> tasks = vcService.GetVideoClipByStatusId((int)MS.Core.Enums.VideoClipTaskStatus.Created);
                if(tasks != null)
                    totalCount = tasks.Count();

                if(tasks != null && tasks.Count > 0)
                {
                    foreach (VideoClipTask task in tasks)
                    {
                        try
                        {
                            VideoClipInput input = JsonConvert.DeserializeObject<VideoClipInput>(task.InputObjectJson);

                            WebClient wb = new WebClient();
                            string mediaServerUrl = ConfigurationManager.AppSettings["MsApi"];
                            string tempSavePath = ConfigurationManager.AppSettings["TempSavePath"];
                            List<string> clipUrls = new List<string>();

                            int VideoEditsCount = input.VidEdits.Count();

                            for (int i = 0; i < VideoEditsCount; i++)
                            {
                                MS.Core.Models.VideoEdits vEdit = input.VidEdits.Where(x => x.SequenceNumber == (i + 1)).ToList().First();
                                string fileName = "video" + vEdit.VideoGuid + (i + 1) + ".ts";
                                string savePath = tempSavePath + fileName;
                                string downloadPath = tempSavePath + vEdit.VideoGuid;
                                wb.DownloadFile(mediaServerUrl + vEdit.HighResUrl, downloadPath);
                                TimeSpan from = TimeSpan.FromSeconds(vEdit.From);
                                TimeSpan to = TimeSpan.FromSeconds(vEdit.To);

                                FFMPEG.ClipVideoWith420pix(downloadPath, from, to, savePath);
                                System.IO.File.Delete(downloadPath);
                                clipUrls.Add(savePath);
                            }

                            Guid finalFileName = Guid.NewGuid();
                            string finalVideoPath = tempSavePath + @"\" + finalFileName.ToString() + ".mp4";
                            FFMPEG.ConcatenateVideosForPreviewWithEncoding(clipUrls, finalVideoPath);

                            Resource resource = resourceService.GetResourceByGuidId(new Guid(input.Guid));
                            Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                            //Server server = LocalCache.Cache.Servers.Where(x => x.ServerId == resource.ServerId.Value).First();
                            Server server = serverService.GetServer(resource.ServerId.Value);

                            string resourceHighResPath = @"\\" + server.ServerIp + ConfigurationManager.AppSettings["MediaLocation"] + bucket.Path + @"\" + resource.FilePath;
                            //System.IO.File.Move(resourceHighResPath, resourceLowResPath);
                            System.IO.File.Copy(finalVideoPath, resourceHighResPath, true);

                            resource.HighResolutionFile = resource.FilePath;
                            resource.HighResolutionFormatId = 3;
                            resourceService.UpdateResource(resource);

                            System.IO.File.Delete(finalVideoPath);
                            foreach (string url in clipUrls)
                                System.IO.File.Delete(url);

                            task.StatusId = (int)MS.Core.Enums.VideoClipTaskStatus.Completed;
                            task.LastUpdateDate = DateTime.UtcNow;
                            vcService.UpdateVideoClipTask(task);

                            successCount++;
                        }
                        catch (Exception exp)
                        {
                            task.StatusId = (int)MS.Core.Enums.VideoClipTaskStatus.Error;
                            vcService.UpdateVideoClipTask(task);
                        }
                    }
                }

                string message = string.Format("ClipAndMergeVideosThread: {0}/{1} have been Clipped and created Successfully", successCount, totalCount);
                if (totalCount > 0)
                    logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ClipAndMergeVideosThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
