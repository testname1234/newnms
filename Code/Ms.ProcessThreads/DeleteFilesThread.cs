﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.Helper;
using MS.Core.IService;
using MS.MediaStorage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Ms.ProcessThreads
{



    public class DeleteFilesArguement
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public string APIKey { get; set; }

        public string TempPath { get; set; }

        public string Location { get; set; }

        public int ServerId { get; set; }


    }


    public class DeleteFilesThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");

            try
            {
                DateTime startTime = DateTime.UtcNow;
                DeleteFilesArguement deleteFilesArguement = JSONhelper.GetObject<DeleteFilesArguement>(argument);


                string apiKey = deleteFilesArguement.APIKey;

                int mainBucketId = deleteFilesArguement.Id;
                string location = deleteFilesArguement.Location;
                int serverId = deleteFilesArguement.ServerId;
                string bucketName = deleteFilesArguement.Name.ToLower();
                bucketName = bucketName.ToLower();
                string parentBucketfolderPath = deleteFilesArguement.TempPath.ToLower();
                if (parentBucketfolderPath.Substring(parentBucketfolderPath.Length - 1) != "\\")
                    parentBucketfolderPath += "\\";

                string[] files = Directory.GetFiles(parentBucketfolderPath, "*.*", SearchOption.AllDirectories);
                string ResponseMsg = string.Empty;

                for (var i = 0; i < files.Length; i++)
                {
                    string fileName = files[i].ToLower();
                    string fPath = fileName.Replace(parentBucketfolderPath, "");
                    string filePath = "\\" + fPath;
                    FileInfo fileInfo = new FileInfo(fileName);
                    if (fileInfo.Exists)
                    {
                        if ((DateTime.UtcNow - fileInfo.CreationTimeUtc).Days > 0)
                        {
                            ResponseMsg += fileInfo.FullName + " /n";
                            File.Delete(fileInfo.FullName);
                        }
                    }
                }

                logService.InsertSystemEventLog("Files Delete Thread Successfully", ResponseMsg, EventCodes.Log);
                return "true";
            }
            catch (Exception ex)
            {
                logService.InsertSystemEventLog(string.Format("Error in Files Delete Thread Successfully: {0}", ex.Message), ex.StackTrace, EventCodes.Error);
                return "error";
            }
        }
    }
}
