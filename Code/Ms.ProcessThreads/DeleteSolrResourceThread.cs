﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using SolrManager;
using SolrManager.InputEntities;
using SolrManager.OutputEntities;
using SolrNet;

namespace Ms.ProcessThreads
{
   
    public class DeleteSolrResourceThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }
        public static SolrService<SolrResource> sService = new SolrService<SolrResource>();
        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            int numberOfResourcesUpdated = 0;
            List<string> lstCelebrityNames = new List<string>();
            try
            {
                DateTime currentDate = DateTime.UtcNow;
                string solrCollectionUrl = ConfigurationManager.AppSettings["solrUrl2"];
                try
                {
                    try
                    {
                        sService.Connect(solrCollectionUrl);
                    }
                    catch
                    {

                    }
                  
                    string query = "id:\"Max_DeletedDate\"";
                    var searchResult = sService.Sort(query, "DeletedDateStamp", Order.DESC, 1);
                    if (searchResult != null && searchResult.Results.Count > 0)
                    {
                        long dateStamp = searchResult.Results.First().DeletedTimeStamp;
                        List<Resource> moreResources = resourceService.GetResourceByStatusIdAfterDate(dateStamp,9);
                        
                        if (moreResources != null && moreResources.Count > 0)
                        {
                            long maxTimeStamp = moreResources.Last().TimeStamp.Value;
                            foreach (Resource item in moreResources)
                            {
                                sService.DeleteById(item.Guid.ToString());
                            }
                          
                            List<SolrResource> lstresources  = new List<SolrResource>();
                            lstresources.Add(new SolrResource() { id = "Max_DeletedDate", TimeStamp = maxTimeStamp });
                            sService.AddRecords(lstresources);
                            numberOfResourcesUpdated = moreResources.Count;
                        }

                    }
                    else
                    {
                        long maxTimeStamp=0;
                        string maxdatequery = "id:\"Max_Date\"";
                        var maxdatesearchResult = sService.Sort(maxdatequery, "DateStamp", Order.DESC, 1);
                        if (maxdatesearchResult != null && maxdatesearchResult.Results.Count > 0)
                        {
                            maxTimeStamp = maxdatesearchResult.Results.First().TimeStamp;
                        }


                        List<Resource> resources = resourceService.GetdeletedResources();
                        foreach (Resource item in resources)
                        {
                            sService.DeleteById(item.Guid.ToString());
                        }

                        List<SolrResource> lstresources = new List<SolrResource>();
                        lstresources.Add(new SolrResource() { id = "Max_DeletedDate", TimeStamp = maxTimeStamp });
                        sService.AddRecords(lstresources);
                        numberOfResourcesUpdated = resources.Count;
                    }
                }
                catch (Exception ex)
                {

                }

                string message = string.Format("{0} Resources Deleted successfully", numberOfResourcesUpdated);
                // logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in DeleteSolrResourceThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

    }
}
