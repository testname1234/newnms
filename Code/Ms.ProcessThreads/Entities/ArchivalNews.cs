﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ms.ProcessThreads.Entities
{
    [Serializable]
    public class ArchivalNews
    {
        public int ResourceId { get; set; }
        public int FilterTypeId { get; set; }
        public string Barcode { get; set; }
        public int LanguageCode { get; set; }
        public string Guid { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<string> Categories { get; set; }
        public List<string> Tags { get; set; }
        public string Caption { get; set; }
        public string Location { get; set; }
        public string NewsDate { get; set; }
        public double? duration { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
