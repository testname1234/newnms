﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core;
using MS.Core.Helper;
using MS.Core.Entities;
using MS.Core.IService;
using FFMPEGLib;
using FFMPEGLib.Helper;

namespace Ms.ProcessThreads
{
    public class ExtractFileInfoThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            IServerService serverService = MS.Core.IoC.Resolve<IServerService>("ServerService");
            IBucketService bucketService = MS.Core.IoC.Resolve<IBucketService>("BucketService");
            List<Resource> lstResources = new List<Resource>();
            int successCount = 0, totalCount = 0;

            try
            {
                int takeCount = Convert.ToInt32(argument);
                string tempArchivalFolderPath = ConfigurationSettings.AppSettings["tempArchivalFolderPath"];
                //lstResources = resourceService.GetResourceByServerId(1);
                lstResources = resourceService.GetResourceByStatusId((int)MS.Core.Enums.ResourceStatuses.FileInfoPending, takeCount);
                if (lstResources != null)
                    totalCount = lstResources.Count;

                if (lstResources != null && lstResources.Count > 0)
                {
                    foreach (Resource res in lstResources)
                    {
                        MS.Core.Enums.ResourceTypes rType = (MS.Core.Enums.ResourceTypes)res.ResourceTypeId;
                        string extensions = ExtensionHelper.GetResourceTypeExtensions(rType);

                        string[] files = System.IO.Directory.GetFiles(tempArchivalFolderPath + "\\", "*" + res.Barcode.ToString().PadLeft(6, '0') + ".*")
                                            .Where(x => extensions.Contains(System.IO.Path.GetExtension(x).ToLower())).ToArray();

                        if (files.Count() > 0)
                        {
                            Server server = serverService.GetServer(res.ServerId.Value);
                            if (res.BucketId != null)
                            {
                                Bucket bucket = bucketService.GetBucketAndCredentials(res.BucketId.Value);
                                string filePath = files[0];
                                res.FileName = filePath.Substring(filePath.LastIndexOf('\\') + 1, (filePath.Length) - (filePath.LastIndexOf('\\') + 1));

                                Resource res1 = resourceService.GenerateResourceThumbAndFindInformation(res, filePath, bucket, server);
                                successCount++;
                            }
                            else
                            {
                                //logService.InsertSystemEventLog(string.Format("ExtractFileInfoThread: No bucket assigned to this barcode: {0}", res.Barcode), null, EventCodes.Warning);
                            }
                        }
                    }
                }
                string message = string.Format("ExtractFileInfoThread: {0}/{1} resources prcessed successfully", successCount, totalCount);
                logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ExtractFileInfoThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
