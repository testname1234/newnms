﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.IService;
using SolrManager;
using SolrManager.InputEntities;
using SolrNet;

namespace Ms.ProcessThreads
{
    public class FillSolrCelebrityNamesThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }
        public static SolrService<ResourceMeta> sService = new SolrService<ResourceMeta>();
        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            IResourceMetaService resourceMetaService = MS.Core.IoC.Resolve<IResourceMetaService>("ResourceMetaService");

            List<string> lstCelebrityNames = new List<string>();
            int successCount = 0, totalCount = 0;
            int resourceUpdatedCount = 0;

            try
            {
                DateTime currentDate = DateTime.UtcNow;
                string solrCollectionUrl = ConfigurationManager.AppSettings["solrUrl3"];
                try
                {
                    List<ResourceMeta> ResourceMetas = new List<ResourceMeta>();
                   
                        try
                        {
                            sService.Connect(solrCollectionUrl);
                        }
                        catch
                        {

                        }
                        string query = "Key:(*:*)";
                        var lastResult = sService.Sort(query, "LastUpdateDate", Order.DESC, 1);

                        if (lastResult != null && lastResult.Results.Count > 0)
                        {
                            DateTime lastUpdateDate = lastResult.Results.First().LastUpdateDate;

                            List<MS.Core.Entities.ResourceMeta> metas = resourceMetaService.GetResourceMetaforThreadAfterDate(lastUpdateDate);
                            if (metas != null)
                            {
                                foreach (MS.Core.Entities.ResourceMeta rmeta in metas)
                                {
                                    ResourceMeta meta = new ResourceMeta();
                                    meta.id = rmeta.Name + "_" + rmeta.Value;
                                    meta.Key = rmeta.Name;
                                    meta.Value = rmeta.Value;
                                    meta.LastUpdateDate = DateTime.UtcNow;
                                    ResourceMetas.Add(meta);
                                }

                                // ResourceMetas.CopyFrom(metas);
                                sService.AddRecords(ResourceMetas);
                                resourceUpdatedCount = ResourceMetas.Count;
                            }
                        }
                        else
                        {
                            List<MS.Core.Entities.ResourceMeta> metas = resourceMetaService.GetResourceMetaforThread();
                            if (metas != null)
                            {
                                foreach (MS.Core.Entities.ResourceMeta rmeta in metas)
                                {
                                    ResourceMeta meta = new ResourceMeta();
                                    meta.id = rmeta.Name + "_" + rmeta.Value;
                                    meta.Key = rmeta.Name;
                                    meta.Value = rmeta.Value;
                                    meta.LastUpdateDate = DateTime.UtcNow;
                                    ResourceMetas.Add(meta);
                                }


                                //ResourceMetas.CopyFrom(metas);
                                sService.AddRecords(ResourceMetas);
                                resourceUpdatedCount = ResourceMetas.Count;
                            }
                        }
                }
                catch (Exception ex)
                {

                }

                DateTime lastDateFromSolrCeleb;

                string message = string.Format("FilleResourceMetaThread: {0}/{1} resources prcessed successfully", successCount, totalCount);
                // logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in FilleResourceMetaThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

    }
}
