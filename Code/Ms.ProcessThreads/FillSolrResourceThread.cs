﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.IService;
using SolrManager;
using SolrManager.InputEntities;

namespace Ms.ProcessThreads
{
    public class FillSolrResourceThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }
        public static SolrService<SolrResource> sService = new SolrService<SolrResource>();
        public string Initialize()
        {

            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {


            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in FillCacheThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }


            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");

            try
            {
                string solrCollectionUrl = ConfigurationManager.AppSettings["solrUrl2"];
                try
                {
                    sService.Connect(solrCollectionUrl);
                }
                catch (Exception ex)
                {

                }

                int numberOfResourcesUpdated = resourceService.FillSolrResourceCache(sService);

                string message = string.Format("FillSolrResourceThread: Number of resouces isHD updated: {0}", numberOfResourcesUpdated);
                logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in isHD FillSolrResourceThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
