﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ms.ProcessThreads
{
    public class GenerateSnapshotThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            int successCount = 0, totalCount = 0;

            try
            {
                

                string message = string.Format("GenerateSnapshotThread: {0}/{1} resources prcessed successfully", successCount, totalCount);
                logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in GenerateSnapshotThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
