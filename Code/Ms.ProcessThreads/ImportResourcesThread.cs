﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.IService;

namespace Ms.ProcessThreads
{
    public class ImportResourcesThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        { 
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            int successCount = 0, totalCount = 0;

            try
            {
                int takeCount = Convert.ToInt32(argument);
                string archivalFolderPath = ConfigurationSettings.AppSettings["archivalFolderPath"];
                string tempArchivalFolderPath = ConfigurationSettings.AppSettings["tempArchivalFolderPath"];
                string[] files = Directory.GetFiles(archivalFolderPath, "*.*").Take(takeCount).ToArray();
                
                totalCount = files.Count();

                foreach(string file in files)
                {
                    string ext = System.IO.Path.GetExtension(file);
                    if (string.IsNullOrEmpty(ext))
                        continue;
                    string barcode = file.Substring(file.LastIndexOf('\\') + 1, (file.Length) - (file.LastIndexOf('\\') + 1)).Replace(ext, "");
                    string destinationPath = file.Replace(archivalFolderPath, tempArchivalFolderPath);
                    var resourceType = MS.Core.Helper.ExtensionHelper.GetResourceType(ext.Replace(".", "").ToLower());
                    int intValue = 0;

                    if (Int32.TryParse(barcode, out intValue))
                    {
                        Resource resource = resourceService.GetResourceByBarcodeAndResourceType(barcode, (int)resourceType);
                        if (resource != null)
                        {
                            resource.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.FileInfoPending;

                            if (!File.Exists(destinationPath))
                            {
                                System.IO.File.Move(file, destinationPath);
                                resourceService.UpdateResource(resource);
                                successCount++;
                            }
                        }
                        else
                        {
                            string message1 = string.Format("ImportResourcesThread: Cannot find the barcode : {0}", barcode);
                            //logService.InsertSystemEventLog(message1, null, EventCodes.Warning);
                        }
                    }
                }
                string message = string.Format("ImportResourcesThread: {0}/{1} resources moved to temp folder successfully", successCount, totalCount);
                if (totalCount > 0)
                    logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ImportResourcesThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
