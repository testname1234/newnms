﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Helper;
using MS.Core.IService;
using NMS.Core.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ms.ProcessThreads
{
    public class LowResTranscodingThread : ISystemProcessThread
    {


        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            List<Resource> lstResource = new List<Resource>();
            int successCount = 0;

            try
            {
                ThreadArgument arg = JSONHelper.GetObject<ThreadArgument>(argument);

                lstResource = resourceService.GetResourcesForLowResTranscoding(arg.ThreadCount);

                if (lstResource != null)
                {
                    Parallel.ForEach(lstResource, new ParallelOptions() { MaxDegreeOfParallelism = arg.ThreadCount }, res =>
                    {

                        res.ServerName = Environment.MachineName;
                        Resource resourceToTranscode = resourceService.UpdateResourceWithTimeStamp(res);
                        try
                        {

                            if (resourceToTranscode != null)
                            {
                                TranscodeFile(resourceToTranscode);
                                successCount++;
                            }
                        }
                        catch (Exception exp)
                        {
                            resourceToTranscode.ServerName = string.Empty;
                            resourceService.UpdateResource(resourceToTranscode);
                            //logService.InsertSystemEventLog(string.Format("Error Transcoding Resource in LowResTranscodingThread: Resource ID : {1) : {0}", exp.Message, res.ResourceId), exp.StackTrace, EventCodes.Error);
                        }

                    });
                }

                string message = string.Format("LowResTranscodingThread: {0} Resources prcessed successfully", successCount);
                logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in LowResTranscodingThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        void TranscodeFile(Resource resource)
        {
            IServerService serverService = MS.Core.IoC.Resolve<IServerService>("ServerService");
            IBucketService bucketService = MS.Core.IoC.Resolve<IBucketService>("BucketService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            string ext = System.IO.Path.GetExtension(resource.Source);
            Server server = serverService.GetServer(resource.ServerId.Value);
            Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
            ICodecConfigService codecService = MS.Core.IoC.Resolve<ICodecConfigService>("CodecConfigService");
            string lowResSavePath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocationLowRes"] + bucket.Path + resource.FilePath;
            CodecConfig cConfig = codecService.GetCodecConfig(1);

            if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
            {
                int lowResWidth = cConfig.ImageLowResWidth;
                int lowResHeight = cConfig.ImageLowResHeight;
                Format lowResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ".jpg").FirstOrDefault();

                string saveFolder = lowResSavePath.Substring(0, lowResSavePath.LastIndexOf("\\") + 1);

                if (!string.IsNullOrEmpty(ext))
                    ResourceHelper.ChangeImageResolution(resource.Source, saveFolder, resource.FileName.Replace(ext, ".jpg"), lowResWidth, lowResHeight);
                else
                    ResourceHelper.ChangeImageResolution(resource.Source, saveFolder, resource.FileName + ".jpg", lowResWidth, lowResHeight);
                resource.LowResolutionFormatId = lowResFormat.FormatId;
                resource.LowResolutionFile = resource.FilePath.Replace(ext, ".jpg");
                resource.LastUpdateDate = DateTime.UtcNow;
                resource.ServerName = string.Empty;
                resourceService.UpdateResource(resource);
            }
            else if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video || resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.ProgramBarCode)
            {
                string lowResCodec = cConfig.VideoLowResCodec;
                Format lowResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ".mp4").FirstOrDefault();

                FFMPEGLib.FFMPEG.ConvertToCustomCodec(resource.Source, lowResSavePath.Replace(ext, ".mp4"), lowResCodec);
                resource.LowResolutionFile = resource.FilePath.Replace(ext, ".mp4");
                resource.LowResolutionFormatId = lowResFormat.FormatId;
                resource.LastUpdateDate = DateTime.UtcNow;
                resource.ServerName = string.Empty;
                resourceService.UpdateResource(resource);
            }
            else
            {
                resource.ServerName = null;
                resourceService.UpdateResource(resource);
            }
        }
    }
}
