﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ms.ProcessThreads.Entities;
using MsMedia.Core;
using MsMedia.Core.Entities;
using MsMedia.Core.IService;
using MsMedia.Repository;
using MsMedia.Service;
using NMS.Core.IService;

namespace Ms.ProcessThreads
{
    public class PostArchivalNews
    {

        public string Execute(string argument)
        {

            # region InterfaceObjects
            IResourceDetailService resourceDetailService = IoC.Resolve<IResourceDetailService>("ResourceDetailService");
            IRdCelebrityService rdcelebserv = IoC.Resolve<IRdCelebrityService>("RdCelebrityService");
            IRdEnvironmentService rdenvService = IoC.Resolve<IRdEnvironmentService>("RdEnvironmentService");
            IRdEventService rdeventService = IoC.Resolve<IRdEventService>("RdEventService");
            IRdKeywordService rdkeywordvService = IoC.Resolve<IRdKeywordService>("RdKeywordService");
            IRdLocationService rdlocationService = IoC.Resolve<IRdLocationService>("RdLocationService");
            IRdMiscellaneousService rdmiscService = IoC.Resolve<IRdMiscellaneousService>("RdMiscellaneousService");
            IRdNatureService rdnatureService = IoC.Resolve<IRdNatureService>("RdNatureService");
            IRdSourceService rdsourceService = IoC.Resolve<IRdSourceService>("RdSourceService");
            IRdTechnologyService rdtechService = IoC.Resolve<IRdTechnologyService>("RdTechnologyService");
            MsMedia.Core.IService.ICelebrityService celebserv = IoC.Resolve<MsMedia.Core.IService.ICelebrityService>("CelebrityService");
            ICelebrityAgeGroupService celebagserv = IoC.Resolve<ICelebrityAgeGroupService>("CelebrityAgeGroupService");
            ICelebrityBodyLanguageService celebblserv = IoC.Resolve<ICelebrityBodyLanguageService>("CelebrityBodyLanguageService");
            ICelebrityDepartmentService celebdeptserv = IoC.Resolve<ICelebrityDepartmentService>("CelebrityDepartmentService");
            ICelebrityDesignationService celebdesgserv = IoC.Resolve<ICelebrityDesignationService>("CelebrityDesignationService");
            ICelebrityDressService celebdressserv = IoC.Resolve<ICelebrityDressService>("CelebrityDressService");
            ICelebrityExpressionService celebesserv = IoC.Resolve<ICelebrityExpressionService>("CelebrityExpressionService");
            ICelebrityFaceLookService celebflserv = IoC.Resolve<ICelebrityFaceLookService>("CelebrityFaceLookService");
            ICelebrityHeadLookService celebhlserv = IoC.Resolve<ICelebrityHeadLookService>("CelebrityHeadLookService");
            ICelebrityNativeService celebnsserv = IoC.Resolve<ICelebrityNativeService>("CelebrityNativeService");
            ICelebrityPoliticalPartyService celebppserv = IoC.Resolve<ICelebrityPoliticalPartyService>("CelebrityPoliticalPartyService");
            ICelebrityProfessionService celebprofserv = IoC.Resolve<ICelebrityProfessionService>("CelebrityProfessionService");
            ICelebrityReligionService celebreligserv = IoC.Resolve<ICelebrityReligionService>("CelebrityReligionService");
            ICelebritySectService celebsectserv = IoC.Resolve<ICelebritySectService>("CelebritySectService");
            ICelebrityStatusService celebstatusserv = IoC.Resolve<ICelebrityStatusService>("CelebrityStatusService");
            IKeywordService celebkeyserv = IoC.Resolve<IKeywordService>("KeywordService");
            ILocationAreaService celebareaserv = IoC.Resolve<ILocationAreaService>("LocationAreaService");
            ILocationBuildingService celebbuildserv = IoC.Resolve<ILocationBuildingService>("LocationBuildingService");
            ILocationBuildingTypeService celebbuildtypeserv = IoC.Resolve<ILocationBuildingTypeService>("LocationBuildingTypeService");
            ILocationCountryService celebcountserv = IoC.Resolve<ILocationCountryService>("LocationCountryService");
            ILocationCityService celebcityserv = IoC.Resolve<ILocationCityService>("LocationCityService");
            ILocationProvinceService celebprovserv = IoC.Resolve<ILocationProvinceService>("LocationProvinceService");
            IMiscellaneousService celebmiscserv = IoC.Resolve<IMiscellaneousService>("MiscellaneousService");
            MsMedia.Core.IService.IResourceService resourceService = IoC.Resolve<MsMedia.Core.IService.IResourceService>("ResourceService");
            
            IEnvironmentDayPartService envdayService  =  IoC.Resolve<IEnvironmentDayPartService>("EnvironmentDayPartService");
            IEnvironmentSeasonService  envseasonService  =  IoC.Resolve<IEnvironmentSeasonService>("EnvironmentSeasonService");
            IEnvironmentWeatherService envweatherService   = IoC.Resolve<IEnvironmentWeatherService>("EnvironmentSeasonService");
            IEventDetailService  eventdetailService  =  IoC.Resolve<IEventDetailService>("EventDetailService");
            MsMedia.Core.IService.IEventTypeService eventtypeservice  =   IoC.Resolve<MsMedia.Core.IService.IEventTypeService>("EventTypeService");
            INatureService natureService  =  IoC.Resolve<INatureService>("NatureService");
            INatureTypeService naturetypeService =  IoC.Resolve<INatureTypeService>("NatureTypeService");
            ISourceTypeService  sourcetypeService  =   IoC.Resolve<ISourceTypeService>("SourceTypeService");
            ISourceVersionService sourceVersionService  =  IoC.Resolve<ISourceVersionService>("SourceVersionService");
            ITechnologyService techservice = IoC.Resolve<ITechnologyService>("TechnologyService");
            ITechnologyTypeService techtypeservice = IoC.Resolve<ITechnologyTypeService>("TechnologyTypeService");

# endregion 

            INewsService newsService = IoC.Resolve<INewsService>("NewsService");
            List<ArchivalNews> lstNews = new List<ArchivalNews>();
            DateTime date  = DateTime.Now;
            List<Resource> resources  = resourceService.GetResourcesAfterDate(date);
            foreach (Resource resourceitem in resources)
            {
                List<ResourceDetail> resourcedetail = resourceDetailService.GetAllResourceDetailbyResourceId(resourceitem.ResourceId, "");
                if (resourcedetail != null)
                {
                    foreach (ResourceDetail detail in resourcedetail)
                    {
                        ArchivalNews news = new ArchivalNews();
                       

                        RdCelebrity _rdCelebrity = rdcelebserv.GetRdCelebrityEntityByResourceDetailId(detail.ResourceDetailId);
                        RdEnvironment _rdEnvironment = rdenvService.GetRdEnvironmentEntityByResourceDetailId(detail.ResourceDetailId);
                        RdEvent _rdRdEvent = rdeventService.GetRdEventEntityByResourceDetailId(detail.ResourceDetailId);
                        RdKeyword _rdKeyword = rdkeywordvService.GetRdKeywordEntityByResourceDetailId(detail.ResourceDetailId);
                        List<RdLocation> _listrdLocation = rdlocationService.GetRdLocationByResourceDetailId(detail.ResourceDetailId);
                        RdLocation _rdLocation = null;
                        if (_listrdLocation != null)
                        {
                            _rdLocation = rdlocationService.GetRdLocationByResourceDetailId(detail.ResourceDetailId).LastOrDefault();
                        }

                        RdMiscellaneous _rdMiscellaneous = rdmiscService.GetRdMiscellaneousEntityByResourceDetailId(detail.ResourceDetailId);
                        RdNature _rdNature = rdnatureService.GetRdNatureEntityByResourceDetailId(detail.ResourceDetailId);
                        RdSource _rdSource = rdsourceService.GetRdSourceEntityByResourceDetailId(detail.ResourceDetailId);
                        RdTechnology _rdTechnology = rdtechService.GetRdTechnologyEntityByResourceDetailId(detail.ResourceDetailId);
                        
                        Celebrity celeb = null;
                        CelebrityAgeGroup _CelebrityAgeGroup = null;
                        CelebrityBodyLanguage _CelebrityBodyLanguage = null;
                        CelebrityDepartment _CelebrityDepartment = null;
                        CelebrityDesignation _CelebrityDesignation = null;
                        CelebrityDress _CelebrityDress = null;
                        CelebrityExpression _CelebrityExpression = null;
                        CelebrityFaceLook _CelebrityFaceLook = null;
                        CelebrityHeadLook _CelebrityHeadLook = null;
                        CelebrityNative _CelebrityNative = null;
                        CelebrityPoliticalParty _CelebrityPoliticalParty = null;
                        CelebrityProfession _CelebrityProfession = null;
                        CelebrityReligion _CelebrityReligion = null;
                        CelebritySect _CelebritySect = null;
                        CelebrityStatus _CelebrityStatus = null;

                        if (_rdCelebrity != null)
                        {
                            celeb = celebserv.GetCelebrity(Convert.ToInt32(_rdCelebrity.CelebrityId));

                            _CelebrityAgeGroup = celebagserv.GetCelebrityAgeGroup(Convert.ToInt32(_rdCelebrity.CelebrityAgeGroupId));
                            _CelebrityBodyLanguage = celebblserv.GetCelebrityBodyLanguage(Convert.ToInt32(_rdCelebrity.CelebrityBodyLanguageId));
                            _CelebrityDepartment = celebdeptserv.GetCelebrityDepartment(Convert.ToInt32(_rdCelebrity.CelebrityDepartmentId));
                            _CelebrityDesignation = celebdesgserv.GetCelebrityDesignation(Convert.ToInt32(_rdCelebrity.CelebrityDesignationId));
                            _CelebrityDress = celebdressserv.GetCelebrityDress(Convert.ToInt32(_rdCelebrity.CelebrityDressId));
                            _CelebrityExpression = celebesserv.GetCelebrityExpression(Convert.ToInt32(_rdCelebrity.CelebrityExpressionId));
                            _CelebrityFaceLook = celebflserv.GetCelebrityFaceLook(Convert.ToInt32(_rdCelebrity.CelebrityFaceLookId));
                            _CelebrityHeadLook = celebhlserv.GetCelebrityHeadLook(Convert.ToInt32(_rdCelebrity.CelebrityHeadLookId));
                            _CelebrityNative = celebnsserv.GetCelebrityNative(Convert.ToInt32(_rdCelebrity.CelebrityNativeId));
                            _CelebrityPoliticalParty = celebppserv.GetCelebrityPoliticalParty(Convert.ToInt32(_rdCelebrity.CelebrityPoliticalPartyId));
                            _CelebrityProfession = celebprofserv.GetCelebrityProfession(Convert.ToInt32(_rdCelebrity.CelebrityProfessionId));
                            _CelebrityReligion = celebreligserv.GetCelebrityReligion(Convert.ToInt32(_rdCelebrity.CelebrityReligionId));
                            _CelebritySect = celebsectserv.GetCelebritySect(Convert.ToInt32(_rdCelebrity.CelebritySectId));
                            _CelebrityStatus = celebstatusserv.GetCelebrityStatus(Convert.ToInt32(_rdCelebrity.CelebrityStatusId));

                            if (celeb != null)
                            {
                                news.Tags.Add(celeb.Name);
                            }

                            if (_CelebrityBodyLanguage != null)
                            {
                                news.Tags.Add(_CelebrityBodyLanguage.Name);
                            }

                            if (_CelebrityDress != null)
                            {
                                news.Tags.Add(_CelebrityDress.Name);
                            }
                            if (_CelebrityExpression != null)
                            {

                                news.Tags.Add(_CelebrityExpression.Name);
                            }
                            if (_CelebrityFaceLook != null)
                            {
                                news.Tags.Add(_CelebrityFaceLook.Name);
                            }
                            if (_CelebrityHeadLook != null)
                            {
                                news.Tags.Add(_CelebrityHeadLook.Name);
                            }
                            if (_CelebrityAgeGroup != null)
                            {
                                news.Tags.Add(_CelebrityAgeGroup.Name);
                            }
                            if (_CelebrityDepartment != null)
                            {
                                news.Tags.Add(_CelebrityDepartment.Name);
                            }
                            if (_CelebrityDesignation != null)
                            {
                                news.Tags.Add(_CelebrityDesignation.Name);
                            }
                            if (_CelebrityNative != null)
                            {
                                news.Tags.Add(_CelebrityNative.Name);
                            }
                            if (_CelebrityPoliticalParty != null)
                            {
                                news.Tags.Add(_CelebrityPoliticalParty.Name);
                            }
                            if (_CelebrityProfession != null)
                            {
                                news.Tags.Add(_CelebrityProfession.Name);
                            }
                            if (_CelebrityReligion != null)
                            {
                                news.Tags.Add(_CelebrityReligion.Name);
                            }
                            if (_CelebritySect != null)
                            {
                                news.Tags.Add(_CelebritySect.Name);
                            }
                            if (_CelebrityStatus != null)
                            {
                                news.Tags.Add(_CelebrityStatus.Name);
                            }


                        }


                        Keyword _Keyword = null;
                        if (_rdKeyword != null)
                        {
                            _Keyword = celebkeyserv.GetKeyword(Convert.ToInt32(_rdKeyword.KeywordId));
                            if (_Keyword != null)
                            {
                                news.Tags.Add(_Keyword.Name);
                                news.Categories.Add(_Keyword.Name);
                            }

                        }

                        LocationArea _LocationArea = null;
                        LocationBuilding _LocationBuilding = null;
                        LocationBuildingType _LocationBuildingType = null;
                        LocationCity _LocationCity = null;
                        LocationCountry _LocationCountry = null;
                        LocationProvince _LocationProvince = null;
                        if (_rdLocation != null)
                        {
                            _LocationArea = celebareaserv.GetLocationArea(Convert.ToInt32(_rdLocation.LocationAreaId));
                            _LocationBuilding = celebbuildserv.GetLocationBuilding(Convert.ToInt32(_rdLocation.LocationBuildingId));
                            _LocationBuildingType = celebbuildtypeserv.GetLocationBuildingType(Convert.ToInt32(_rdLocation.LocationBuildingTypeId));
                            _LocationCountry = celebcountserv.GetLocationCountry(Convert.ToInt32(_rdLocation.LocationCountryId));
                            _LocationProvince = celebprovserv.GetLocationProvince(Convert.ToInt32(_rdLocation.LocationProvinceId));
                            _LocationCity = celebcityserv.GetLocationCity(Convert.ToInt32(_rdLocation.LocationCityId));

                            if (_LocationBuilding != null)
                            {
                                news.Location = _LocationBuilding.Name;
                            }
                            else if (_LocationArea != null)
                            {
                                news.Location = _LocationArea.Name;
                            }
                            else if (_LocationCity != null)
                            {
                                news.Location = _LocationCity.Name;
                            }
                            else if (_LocationProvince != null)
                            {
                                news.Location = _LocationProvince.Name;
                            }
                            else if (_LocationCountry != null)
                            {
                                news.Location = _LocationCountry.Name;
                            }

                        }

                        Miscellaneous _Miscellaneous = null;
                       

                        if (_rdMiscellaneous != null)
                        {
                            _Miscellaneous = celebmiscserv.GetMiscellaneous(Convert.ToInt32(_rdMiscellaneous.MiscellaneousId));
                            if (_Miscellaneous != null)
                            {
                                news.Tags.Add(_Miscellaneous.Name);
                            }
                        
                        }

                        EnvironmentDayPart _Environment = null;
                      ////  EnvironmentSeason _EnvironmentSeason = null;
                     //   EnvironmentWeather  _EnvironmentWeather = null;

                        if (_rdEnvironment != null)
                        {
                            _Environment =   envdayService.GetEnvironmentDayPart(Convert.ToInt32(_rdEnvironment.EnvironmentDayPartId));
                            if (_Environment != null)
                            {
                                news.Tags.Add(_Environment.Name);
                            }
                        }

                        EventDetail _EventDetail = null;
                        EventType _EventType = null;
                        if (_rdRdEvent != null)
                        {
                            _EventDetail  = eventdetailService.GetEventDetail(Convert.ToInt32(_rdRdEvent.EventDetailId));
                            if(_EventDetail!=null)
                            {
                                news.Tags.Add(_EventDetail.Name);
                            }
                            _EventType = eventtypeservice.GetEventType(Convert.ToInt32(_rdRdEvent.EventTypeId));
                            if (_EventType != null)
                            {
                                news.Tags.Add(_EventType.Name);
                            }

                        }


                        Nature _Nature = null;
                        NatureType _NatureType = null;
                        if (_rdNature != null)
                        {
                            _Nature = natureService.GetNature(Convert.ToInt32(_rdNature.NatureId));
                            if (_Nature != null)
                            {
                                news.Tags.Add(_Nature.Name);
                            }

                            _NatureType = naturetypeService.GetNatureType(Convert.ToInt32(_rdNature.NatureTypeId));

                            if (_NatureType != null)
                            {
                                news.Tags.Add(_NatureType.Name);
                            }
                        }

                        SourceVersion _SourceVersion = null;
                        SourceType _SourceType = null;
                        if (_rdSource != null)
                        {
                            _SourceVersion = sourceVersionService.GetSourceVersion(Convert.ToInt32(_rdSource.SourceVersionId));

                            if (_SourceVersion != null)
                            {
                                news.Tags.Add(_SourceVersion.Name);
                            }

                            _SourceType = sourcetypeService.GetSourceType(Convert.ToInt32(_rdSource.SourceTypeId));

                            if (_SourceType != null)
                            {
                                news.Tags.Add(_SourceType.Name);
                            }
                        }

                        Technology _Technology = null;
                        TechnologyType _TechnologyType = null;
                        if (_rdTechnology != null)
                        {
                            _Technology =  techservice.GetTechnology(Convert.ToInt32(_rdTechnology.TechnologyId));

                            if (_Technology != null)
                            {
                                news.Tags.Add(_Technology.Name);
                            }

                            _TechnologyType = techtypeservice.GetTechnologyType(Convert.ToInt32(_rdTechnology.TechnologyTypeId));

                            if (_TechnologyType != null)
                            {
                                news.Tags.Add(_TechnologyType.Name);
                            }
                        
                        }

                        news.ResourceId = detail.ResourceId;
                        news.Guid = detail.Guid.ToString();
                        news.Title = detail.Slug;
                        news.Description = detail.Description;
                        news.Barcode = detail.Barcode.ToString();
                        news.FilterTypeId = 0;
                        news.LanguageCode = 1;
                        news.CreatedDate = detail.ResourceDate;
                        news.UpdatedDate = DateTime.UtcNow;
                        lstNews.Add(news);
                    }
                }
              
            }

            return "";
        }
    }
}
