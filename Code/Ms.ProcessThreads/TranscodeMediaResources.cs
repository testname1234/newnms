﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Helper;
using MS.Core.IService;

namespace Ms.ProcessThreads
{
    public class TranscodeMediaResources : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            IServerService serverService = MS.Core.IoC.Resolve<IServerService>("ServerService");
            IBucketService bucketService = MS.Core.IoC.Resolve<IBucketService>("BucketService");
            ICodecConfigService codecService = MS.Core.IoC.Resolve<ICodecConfigService>("CodecConfigService");
            List<Resource> lstResources = new List<Resource>();

            int successCount = 0, totalCount = 0;

            try
            {
                string folderToProcessFiles = argument;
                string[] files = Directory.GetFiles(folderToProcessFiles, "*.*").ToArray();
                CodecConfig cConfig = codecService.GetCodecConfig(1);
                //lstResources = resourceService.GetResourceByStatusId((int)MS.Core.Enums.ResourceStatuses.Transcoding, 100);
                //if (lstResources != null)
                //    totalCount = lstResources.Count;

                if (files.Count() > 0)
                {
                    totalCount = files.Count();
                    foreach (string file in files)
                    {

                        string ext = System.IO.Path.GetExtension(file);
                        if (string.IsNullOrEmpty(ext) || file.Contains("Thumbs.db") || file.Contains(".ini"))
                            continue;

                        string guid = file.Substring(file.LastIndexOf('\\') + 1, (file.Length) - (file.LastIndexOf('\\') + 1)).Replace(ext, "");
                        var resourceType = MS.Core.Helper.ExtensionHelper.GetResourceType(ext.Replace(".", "").ToLower());
                        
                        var resourceGuid = new Guid();

                        if (Guid.TryParse(guid, out resourceGuid))
                        {
                            Resource resource = resourceService.GetResourceByGuidId(new Guid(guid));

                            try
                            {
                                if (resource != null && resource.BucketId != null && resource.BucketId.Value > 0 && resource.ServerId != null && resource.FilePath != null)
                                {

                                    Server server = serverService.GetServer(resource.ServerId.Value);
                                    Bucket bucket = bucketService.GetBucket(resource.BucketId.Value);
                                    Resource res = new Resource();
                                    string highResSavePath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocation"] + bucket.Path + resource.FilePath;
                                    string lowResSavePath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocationLowRes"] + bucket.Path + resource.FilePath;

                                    if (resource.FilePath.LastIndexOf('\\') >= 0)
                                        resource.FileName = resource.FilePath.Substring(resource.FilePath.LastIndexOf('\\') + 1, (resource.FilePath.Length - (resource.FilePath.LastIndexOf('\\') + 1)));
                                    else
                                        resource.FileName = resource.FilePath;


                                    string temporaryFilePath = file;

                                    if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
                                    {
                                        int lowResWidth = cConfig.ImageLowResWidth;
                                        int lowResHeight = cConfig.ImageLowResHeight;
                                        Format highResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                        Format lowResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ".jpg").FirstOrDefault();

                                        string saveFolder = lowResSavePath.Substring(0, lowResSavePath.LastIndexOf("\\") + 1);

                                        try
                                        {
                                            if (!string.IsNullOrEmpty(ext))
                                                ResourceHelper.ChangeImageResolution(temporaryFilePath, saveFolder, resource.FileName.Replace(ext, ".jpg"), lowResWidth, lowResHeight);
                                            else
                                                ResourceHelper.ChangeImageResolution(temporaryFilePath, saveFolder, resource.FileName + ".jpg", lowResWidth, lowResHeight);
                                            resource.LowResolutionFormatId = lowResFormat.FormatId;
                                            resource.LowResolutionFile = resource.FilePath.Replace(ext, ".jpg");


                                            if (System.IO.File.Exists(highResSavePath))
                                            {
                                                System.IO.File.Delete(highResSavePath);

                                                int counter = 0;

                                                while (System.IO.File.Exists(highResSavePath))
                                                {
                                                    System.Threading.Thread.Sleep(1000);
                                                    counter++;
                                                    if (counter >= 10)
                                                        break;
                                                }
                                            }

                                            System.IO.File.Move(temporaryFilePath, highResSavePath);

                                            resource.HighResolutionFormatId = highResFormat.FormatId;
                                            resource.HighResolutionFile = resource.FilePath;
                                            resource.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.Completed;
                                            resource.LastUpdateDate = DateTime.UtcNow;
                                            resourceService.UpdateResource(resource);
                                        }
                                        catch (Exception exp)
                                        {
                                            string message1 = string.Format("Warning TranscodeMediaResources: ResourceID : {0} is not a valid image format", resource.Guid);
                                            logService.InsertSystemEventLog(message1, null, EventCodes.Warning);
                                        }
                                    }
                                    else if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video || resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.ProgramBarCode)
                                    {
                                        string highResCodec = cConfig.VideoHighResCodec;
                                        string lowResCodec = cConfig.VideoLowResCodec;

                                        FFMPEGLib.FFMPEG.ConvertToCustomCodec(temporaryFilePath, highResSavePath.Replace(ext, ".mp4"), highResCodec);
                                        FFMPEGLib.FFMPEG.ConvertToCustomCodec(temporaryFilePath, lowResSavePath.Replace(ext, ".mp4"), lowResCodec);

                                        resource.HighResolutionFile = resource.FilePath.Replace(ext, ".mp4");
                                        resource.HighResolutionFormatId = 3;
                                        resource.LowResolutionFile = resource.FilePath.Replace(ext, ".mp4");
                                        resource.LowResolutionFormatId = 3;
                                        resource.FileName = resource.FileName.Replace(ext, ".mp4");
                                        resource.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.Completed;
                                        resource.LastUpdateDate = DateTime.UtcNow;
                                        resourceService.UpdateResource(resource);

                                        System.IO.File.Delete(temporaryFilePath);
                                    }
                                    successCount++;
                                }
                                else
                                {
                                    string message1 = string.Format("Warning TranscodeMediaResources: ResourceID : {0} is not found on the temp location", guid);
                                    //logService.InsertSystemEventLog(message1, null, EventCodes.Warning);
                                }
                            }
                            catch (Exception exp)
                            {
                                logService.InsertSystemEventLog(string.Format("Error in TranscodeMediaResources: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                            }
                        }
                    }
                }

                string message = string.Format("TranscodeMediaResources: {0}/{1} have been transocded successfully", successCount, totalCount);
                if (totalCount > 0)
                    logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in TranscodeMediaResources: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        void GenerateSnapShotAndExtractInfo(Resource resource)
        {

        }
    }
}
