﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Helper;
using MS.Core.IService;

namespace Ms.ProcessThreads
{
    public class TranscodeTempFilesThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            IServerService serverService = MS.Core.IoC.Resolve<IServerService>("ServerService");
            IBucketService bucketService = MS.Core.IoC.Resolve<IBucketService>("BucketService");
            ICodecConfigService codecService = MS.Core.IoC.Resolve<ICodecConfigService>("CodecConfigService");
            List<Resource> lstResources = new List<Resource>();

            int successCount = 0, totalCount = 0;

            try
            {
                string tempArchivalFolderPath = ConfigurationSettings.AppSettings["tempArchivalFolderPath"];
                CodecConfig cConfig = codecService.GetCodecConfig(1);
                lstResources = resourceService.GetResourceByStatusId((int)MS.Core.Enums.ResourceStatuses.Transcoding, 100);
                if (lstResources != null)
                    totalCount = lstResources.Count;

                if (lstResources != null && lstResources.Count > 0)
                {
                    foreach (Resource res in lstResources)
                    {
                        string[] files = System.IO.Directory.GetFiles(tempArchivalFolderPath + "\\", res.Guid.ToString() + ".*");
                        if (files.Count() > 0)
                        {
                            Server server = serverService.GetServer(res.ServerId.Value);
                            Bucket bucket = bucketService.GetBucket(res.BucketId.Value);
                            string highResSavePath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocation"] + bucket.Path + res.FilePath;
                            string lowResSavePath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocationLowRes"] + bucket.Path + res.FilePath;
                            if (string.IsNullOrEmpty(res.FileName))
                            {
                                if (res.FilePath.LastIndexOf('\\') >= 0)
                                    res.FileName = res.FilePath.Substring(res.FilePath.LastIndexOf('\\') + 1, (res.FilePath.Length - (res.FilePath.LastIndexOf('\\') + 1)));
                                else
                                    res.FileName = res.FilePath;
                            }
                            string ext = System.IO.Path.GetExtension(res.FileName);

                            string temporaryFilePath = files[0];

                            if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
                            {
                                int lowResWidth = cConfig.ImageLowResWidth;
                                int lowResHeight = cConfig.ImageLowResHeight;
                                Format highResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                Format lowResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ".jpg").FirstOrDefault();

                                string saveFolder = lowResSavePath.Substring(0, lowResSavePath.LastIndexOf("\\") + 1);
                                //string saveFileName = res.FilePath.Substring(res.FilePath.LastIndexOf('\\') + 1, (res.FilePath.Length - (res.FilePath.LastIndexOf('\\') + 1)));

                                try
                                {
                                    if (!string.IsNullOrEmpty(ext))
                                        ResourceHelper.ChangeImageResolution(temporaryFilePath, saveFolder, res.FileName.Replace(ext, ".jpg"), lowResWidth, lowResHeight);
                                    else
                                        ResourceHelper.ChangeImageResolution(temporaryFilePath, saveFolder, res.FileName + ".jpg", lowResWidth, lowResHeight);
                                    res.LowResolutionFormatId = lowResFormat.FormatId;
                                    res.LowResolutionFile = res.FilePath.Replace(ext, ".jpg");


                                    if (System.IO.File.Exists(highResSavePath))
                                    {
                                        System.IO.File.Delete(highResSavePath);

                                        int counter = 0;

                                        while (System.IO.File.Exists(highResSavePath))
                                        {
                                            System.Threading.Thread.Sleep(1000);
                                            counter++;
                                            if (counter >= 10)
                                                break;
                                        }
                                    }

                                    System.IO.File.Move(temporaryFilePath, highResSavePath);

                                    res.HighResolutionFormatId = highResFormat.FormatId;
                                    res.HighResolutionFile = res.FilePath;
                                }
                                catch (Exception exp)
                                {
                                    string message1 = string.Format("Warning TranscodeTempFilesThread: ResourceID : {0} is not a valid image format", res.Guid);
                                    logService.InsertSystemEventLog(message1, null, EventCodes.Warning);
                                }
                            }
                            else if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video || res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.ProgramBarCode)
                            {
                                string highResCodec = cConfig.VideoHighResCodec;
                                string lowResCodec = cConfig.VideoLowResCodec;

                                if (ext == string.Empty)
                                {
                                    highResSavePath += ".mp4";
                                    lowResSavePath += ".mp4";
                                    res.FilePath += ".mp4";
                                }
                                else
                                {
                                    highResSavePath = highResSavePath.Replace(ext, ".mp4");
                                    lowResSavePath = lowResSavePath.Replace(ext, ".mp4");
                                    res.FilePath = res.FilePath.Replace(ext, ".mp4");
                                }

                                FFMPEGLib.FFMPEG.ConvertToCustomCodec(temporaryFilePath, highResSavePath, highResCodec);
                                FFMPEGLib.FFMPEG.ConvertToCustomCodec(temporaryFilePath, lowResSavePath, lowResCodec);


                                res.HighResolutionFile = res.FilePath;
                                //Format format = LocalCache.Cache.Formats.Where(x => x.Format == ".mp4").FirstOrDefault();
                                res.HighResolutionFormatId = 3;
                                res.LowResolutionFile = res.FilePath;
                                res.LowResolutionFormatId = 3;
                                res.FileName = res.FilePath;
                            }
                            res.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.Completed;
                            res.LastUpdateDate = DateTime.UtcNow;
                            resourceService.UpdateResource(res);
                            successCount++;
                        }
                        else
                        {
                            string message1 = string.Format("Warning TranscodeTempFilesThread: ResourceID : {0} is not found on the temp location", res.Guid);
                            logService.InsertSystemEventLog(message1, null, EventCodes.Warning);
                        }
                    }
                }

                string message = string.Format("TranscodeTempFilesThread: {0}/{1} have been transocded successfully", successCount, totalCount);
                if (totalCount > 0)
                    logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in TranscodeTempFilesThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
