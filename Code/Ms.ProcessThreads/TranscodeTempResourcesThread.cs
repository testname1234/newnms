﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core;
using MS.Core.Entities;
using MS.Core.Helper;
using MS.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ms.ProcessThreads
{
    public class TranscodeTempResourcesThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            IBucketService bucketService = MS.Core.IoC.Resolve<IBucketService>("BucketService");
            List<Resource> lstResources = new List<Resource>();
            ICodecConfigService codecService = MS.Core.IoC.Resolve<ICodecConfigService>("CodecConfigService");
            CodecConfig cConfig = codecService.GetCodecConfig(1);
            string tempArchivalFolderPath = ConfigurationSettings.AppSettings["MediaLocationTemp"];
            Server server = new Server();
            string folderToWatch = string.Empty;
            int successCount = 0;

            try
            {
                List<Bucket> bucketsToTranscode = bucketService.GetBucketsToTranscode();

                if (bucketsToTranscode != null)
                {
                    foreach (Bucket b in bucketsToTranscode)
                    {
                        if (b.ParentId != null)
                        {
                            Bucket parentBucket = LocalCache.Cache.Buckets.Where(x => x.BucketId == b.ParentId).First();
                            server = LocalCache.Cache.Servers.Where(x => x.ServerId == parentBucket.ServerId).First();
                            folderToWatch = "\\\\" + server.ServerIp + tempArchivalFolderPath + parentBucket.Path + "\\" + b.Path.Replace("/", "\\");
                        }
                        else
                        {
                            server = LocalCache.Cache.Servers.Where(x => x.ServerId == b.ServerId).First();
                            folderToWatch = "\\\\" + server.ServerIp + tempArchivalFolderPath + b.Path.Replace("/", "\\");
                        }

                        
                        string[] files = System.IO.Directory.GetFiles(folderToWatch);
                        files = files.Where(x => !x.Contains("Thumbs.db")).Where(x => !x.Contains("*.ini")).ToArray();

                        if (files.Count() > 0)
                        {
                            foreach (string file in files)
                            {
                                string ext = System.IO.Path.GetExtension(file);
                                string guid = file.Substring(file.LastIndexOf('\\') + 1, (file.Length) - (file.LastIndexOf('\\') + 1)).Replace(ext, "");
                                Resource resource = resourceService.GetResourceByGuidId(new Guid(guid));

                                if (resource != null)
                                {
                                    string highResSavePath = file.Replace(tempArchivalFolderPath, ConfigurationSettings.AppSettings["MediaLocation"]);
                                    string lowResSavePath = file.Replace(tempArchivalFolderPath, ConfigurationSettings.AppSettings["MediaLocationLowRes"]);

                                    if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
                                    {
                                        int lowResWidth = cConfig.ImageLowResWidth;
                                        int lowResHeight = cConfig.ImageLowResHeight;
                                        Format highResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                        Format lowResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ".jpg").FirstOrDefault();

                                        string saveFolder = lowResSavePath.Substring(0, lowResSavePath.LastIndexOf("\\") + 1);

                                        try
                                        {
                                            if (!string.IsNullOrEmpty(ext))
                                                ResourceHelper.ChangeImageResolution(file, saveFolder, resource.FileName.Replace(ext, ".jpg"), lowResWidth, lowResHeight);
                                            else
                                                ResourceHelper.ChangeImageResolution(file, saveFolder, resource.FileName + ".jpg", lowResWidth, lowResHeight);
                                            resource.LowResolutionFormatId = lowResFormat.FormatId;
                                            resource.LowResolutionFile = resource.FilePath.Replace(ext, ".jpg");


                                            if (System.IO.File.Exists(highResSavePath))
                                            {
                                                System.IO.File.Delete(highResSavePath);

                                                int counter = 0;

                                                while (System.IO.File.Exists(highResSavePath))
                                                {
                                                    System.Threading.Thread.Sleep(1000);
                                                    counter++;
                                                    if (counter >= 10)
                                                        break;
                                                }
                                            }

                                            System.IO.File.Move(file, highResSavePath);

                                            resource.HighResolutionFormatId = highResFormat.FormatId;
                                            resource.HighResolutionFile = resource.FilePath;
                                            resource.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.Completed;
                                            resource.LastUpdateDate = DateTime.UtcNow;
                                            resourceService.UpdateResource(resource);
                                        }
                                        catch (Exception exp)
                                        {
                                            string message1 = string.Format("Warning TranscodeTempResourcesThread: ResourceID : {0} is not a valid image format", resource.Guid);
                                            logService.InsertSystemEventLog(message1, null, EventCodes.Warning);
                                        }
                                    }
                                    else if (resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video || resource.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.ProgramBarCode)
                                    {
                                        string highResCodec = cConfig.VideoHighResCodec;
                                        string lowResCodec = cConfig.VideoLowResCodec;

                                        FFMPEGLib.FFMPEG.ConvertToCustomCodec(file, highResSavePath.Replace(ext, ".mp4"), highResCodec);
                                        FFMPEGLib.FFMPEG.ConvertToCustomCodec(file, lowResSavePath.Replace(ext, ".mp4"), lowResCodec);

                                        resource.HighResolutionFile = resource.FilePath.Replace(ext, ".mp4");
                                        resource.HighResolutionFormatId = 3;
                                        resource.LowResolutionFile = resource.FilePath.Replace(ext, ".mp4");
                                        resource.LowResolutionFormatId = 3;
                                        resource.FileName = resource.FileName.Replace(ext, ".mp4");
                                        resource.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.Completed;
                                        resource.LastUpdateDate = DateTime.UtcNow;
                                        resourceService.UpdateResource(resource);

                                    }
                                    else
                                    {
                                        resource.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.Completed;
                                        resource.LastUpdateDate = DateTime.UtcNow;
                                        resourceService.UpdateResource(resource);
                                    }
                                    successCount++;

                                    if (System.IO.File.Exists(file))
                                        System.IO.File.Delete(file);
                                }
                            }
                        }
                    }
                }

                string message = string.Format("TranscodeTempResourcesThread: {0} resources prcessed successfully", successCount);
                logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in TranscodeTempResourcesThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
