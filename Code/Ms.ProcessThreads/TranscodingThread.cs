﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core;
using MS.Core.Entities;
using MS.Core.Helper;
using MS.Core.IService;
using Newtonsoft.Json;

namespace Ms.ProcessThreads
{
    public class TranscodingThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = MS.Core.IoC.Resolve<IResourceService>("MSResourceService");
            IServerService serverService = MS.Core.IoC.Resolve<IServerService>("ServerService");
            IBucketService bucketService = MS.Core.IoC.Resolve<IBucketService>("BucketService");
            ICodecConfigService codecService = MS.Core.IoC.Resolve<ICodecConfigService>("CodecConfigService");
            List<Resource> lstResources = new List<Resource>();
            
            int successCount = 0, totalCount = 0;

            try
            {
                int takeCount = Convert.ToInt32(argument);
                string tempArchivalFolderPath = ConfigurationSettings.AppSettings["tempArchivalFolderPath"];
                CodecConfig cConfig = codecService.GetCodecConfig(1);
                lstResources = resourceService.GetResourceByStatusId((int)MS.Core.Enums.ResourceStatuses.TranscodingRequired, takeCount);

                //lstResources = lstResources.Where(x => x.Barcode == 0143814).ToList();


                if (lstResources != null && lstResources.Count > 0)
                {
                    totalCount = lstResources.Count;
                    int pageSize = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(lstResources.Count) / 4.0));

                    Parallel.For(0, 4, i =>
                    {
                        List<Resource> thisLoopResources = lstResources.Skip(i * pageSize).Take(pageSize).ToList();
                        //List<Resource> thisLoopResources = lstResources;

                    foreach (Resource res in thisLoopResources)
                        {
                            MS.Core.Enums.ResourceTypes rType = (MS.Core.Enums.ResourceTypes)res.ResourceTypeId;
                            string extensions = ExtensionHelper.GetResourceTypeExtensions(rType);

                            string[] files = System.IO.Directory.GetFiles(tempArchivalFolderPath + "\\", "*" + res.Barcode.ToString().PadLeft(6, '0') + ".*")
                                .Where(x => extensions.Contains(System.IO.Path.GetExtension(x).ToLower())).ToArray();
                            if (files.Count() > 0)
                            {
                                Server server = serverService.GetServer(res.ServerId.Value);
                                Bucket bucket = bucketService.GetBucket(res.BucketId.Value);
                                string highResSavePath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocation"] + bucket.Path + res.FilePath;
                                string lowResSavePath = @"\\" + server.ServerIp + ConfigurationSettings.AppSettings["MediaLocationLowRes"] + bucket.Path + res.FilePath;
                                string ext = System.IO.Path.GetExtension(res.FileName);

                                string temporaryFilePath = files[0];

                                if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Image)
                                {
                                    Format highResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ext).FirstOrDefault();
                                    Format lowResFormat = MS.Core.LocalCache.Cache.Formats.Where(x => x.Format == ".jpg").FirstOrDefault();
                                    int lowResWidth = cConfig.ImageLowResWidth;
                                    int lowResHeight = cConfig.ImageLowResHeight;

                                    string saveFolder = lowResSavePath.Substring(0, lowResSavePath.LastIndexOf("\\") + 1);
                                    System.IO.File.Copy(temporaryFilePath, highResSavePath, true);

                                    ResourceHelper.ChangeImageResolution(temporaryFilePath, saveFolder, res.FileName.Replace(ext, ".jpg"), lowResWidth, lowResHeight);

                                    res.HighResolutionFormatId = highResFormat.FormatId;
                                    res.HighResolutionFile = res.FilePath;
                                    res.LowResolutionFormatId = lowResFormat.FormatId;
                                    res.LowResolutionFile = res.FilePath;
                                }
                                else if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Video || res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.ProgramBarCode)
                                {
                                    string highResCodec = cConfig.VideoHighResCodec;
                                    string lowResCodec = cConfig.VideoLowResCodec;

                                    logService.InsertSystemEventLog(res.Barcode.ToString() + " - Video isHD = " + res.isHD.ToString(), null, EventCodes.Log);

                                    try
                                    {
                                        if (System.IO.File.Exists(highResSavePath.Replace(ext, ".mp4")))
                                            System.IO.File.Delete(highResSavePath.Replace(ext, ".mp4"));

                                        if (System.IO.File.Exists(highResSavePath))
                                            System.IO.File.Delete(highResSavePath);
                                    }
                                    catch (Exception ex) { logService.InsertSystemEventLog("Exception deleting file from target locaiton[HIGH]: " + highResSavePath.Replace(ext, ".mp4") + "\nException: " + ex.Message, null, EventCodes.Log); }

                                    try
                                    {
                                        if (System.IO.File.Exists(lowResSavePath.Replace(ext, ".mp4")))
                                            System.IO.File.Delete(lowResSavePath.Replace(ext, ".mp4"));
                                    }
                                    catch (Exception ex) { logService.InsertSystemEventLog("Exception deleting file from target locaiton[LOW]: " + lowResSavePath.Replace(ext, ".mp4") + "\nException: " + ex.Message, null, EventCodes.Log); }

                                    if (res.isHD == false)
                                    {
                                        FFMPEGLib.FFMPEG.ConvertToCustomCodec(temporaryFilePath, highResSavePath.Replace(ext, ".mp4"), highResCodec);
                                        FFMPEGLib.FFMPEG.ConvertToCustomCodec(temporaryFilePath, lowResSavePath.Replace(ext, ".mp4"), lowResCodec);

                                        res.FileName = res.FileName.Replace(ext, ".mp4");
                                        res.HighResolutionFile = res.FilePath.Replace(ext, ".mp4");
                                    }
                                    else
                                    {
                                        System.IO.File.Copy(temporaryFilePath, highResSavePath);
                                        res.HighResolutionFile = res.FilePath;
                                        FFMPEGLib.FFMPEG.ConvertToCustomCodec(temporaryFilePath, lowResSavePath.Replace(ext, ".mp4"), lowResCodec);
                                    }

                                    //Format format = LocalCache.Cache.Formats.Where(x => x.Format == ".mp4").FirstOrDefault();
                                    res.HighResolutionFormatId = 3;
                                    res.LowResolutionFile = res.FilePath.Replace(ext, ".mp4");
                                    res.LowResolutionFormatId = 3;
                                }
                                else if (res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.Audio || res.ResourceTypeId == (int)MS.Core.Enums.ResourceTypes.ProgramBarCode)
                                {
                                    string highResCodec = cConfig.VideoHighResCodec;
                                    string lowResCodec = cConfig.VideoLowResCodec;

                                    logService.InsertSystemEventLog(res.Barcode.ToString() + " - audio isHD = " + res.isHD.ToString(), null, EventCodes.Log);

                                    try
                                    {
                                        if (System.IO.File.Exists(highResSavePath.Replace(ext, ".mp3")))
                                            System.IO.File.Delete(highResSavePath.Replace(ext, ".mp3"));

                                        if (System.IO.File.Exists(highResSavePath))
                                            System.IO.File.Delete(highResSavePath);
                                    }
                                    catch (Exception ex) { logService.InsertSystemEventLog("Exception deleting file from target locaiton[HIGH]: " + highResSavePath.Replace(ext, ".mp3") + "\nException: " + ex.Message, null, EventCodes.Log); }

                                    try
                                    {
                                        if (System.IO.File.Exists(lowResSavePath.Replace(ext, ".mp3")))
                                            System.IO.File.Delete(lowResSavePath.Replace(ext, ".mp3"));
                                    }
                                    catch (Exception ex) { logService.InsertSystemEventLog("Exception deleting file from target locaiton[LOW]: " + lowResSavePath.Replace(ext, ".mp3") + "\nException: " + ex.Message, null, EventCodes.Log); }

                                    if (res.isHD == false)
                                    {
                                        FFMPEGLib.FFMPEG.ConvertToAudioHighResCodec(temporaryFilePath, highResSavePath.Replace(ext, ".mp3"), highResCodec);
                                        FFMPEGLib.FFMPEG.ConvertToAudioLowResCodec(temporaryFilePath, lowResSavePath.Replace(ext, ".mp3"), lowResCodec);

                                        res.FileName = res.FileName.Replace(ext, ".mp3");
                                        res.HighResolutionFile = res.FilePath.Replace(ext, ".mp3");
                                    }
                                    else
                                    {
                                        System.IO.File.Copy(temporaryFilePath, highResSavePath);
                                        res.HighResolutionFile = res.FilePath;
                                        FFMPEGLib.FFMPEG.ConvertToAudioLowResCodec(temporaryFilePath, lowResSavePath.Replace(ext, ".mp3"), lowResCodec);
                                    }
                                    //Format format = LocalCache.Cache.Formats.Where(x => x.Format == ".mp4").FirstOrDefault();
                                    res.HighResolutionFormatId = 3;
                                    res.LowResolutionFile = res.FilePath.Replace(ext, ".mp3");
                                    res.LowResolutionFormatId = 3;
                                }
                                res.ResourceStatusId = (int)MS.Core.Enums.ResourceStatuses.Completed;
                                res.LastUpdateDate = DateTime.UtcNow;
                                resourceService.UpdateResource(res);
                                successCount++;
                            }
                        }
                });


            }

                string message = string.Format("TranscodingThread: {0}/{1} have been transocded successfully", successCount, totalCount);
                if (totalCount > 0)
                    logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return message;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in TranscodingThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
