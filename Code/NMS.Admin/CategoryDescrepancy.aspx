﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CategoryDescrepancy.aspx.cs" Inherits="NMS.Admin.CategoryDescrepancy" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="jPaginate - jQuery Pagination Plugin" />
        <meta name="keywords" content="jquery, plugin, pagination, fancy"  />       
        
 
</head>
<body>

    <form id="form1" runat="server">        
    <div style='margin: 5px 5px 5px 5px; border: 2px solid black;'>
        <table width="100%" border="0">
            <tr>
                <td align="right">
                    Page Size&nbsp;:&nbsp;<asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="10" Value="10" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                        <asp:ListItem Text="40" Value="40"></asp:ListItem>
                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                    </asp:DropDownList> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%--</td>--%>
            <%--</tr>
            <tr>--%>
                <%--<td align="right">--%>
                    <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%--</td>--%>
            <%--</tr>
            <tr>--%>
                <%--<td align="right">--%>
                    <asp:LinkButton ID="lbtnFirst" runat="server" Text=" first " OnClick="lbtnFirst_Click"></asp:LinkButton>
                    &nbsp; &nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Text=" previous " OnClick="lbtnPrev_Click"></asp:LinkButton>&nbsp;
                    &nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Text=" next " OnClick="lbtnNext_Click"></asp:LinkButton>
                    &nbsp; &nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Text=" last " OnClick="lbtnLast_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr><td width="50%">
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="dlActionNew" runat="server" onchange="ShowOptions(this);">
                                        <asp:ListItem Value="-1" Text="--select"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="New Category"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Existing Category"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trNewCategoryNew" style="display: none;" runat="server">
                                <td>New Category Name:
                                <asp:TextBox ID="txtNewNameNew" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtNewNameNew" ID="rvtxtNewNameNew" runat="server" ErrorMessage="*"
                                        ValidationGroup="NewGroup1" ForeColor="Red"></asp:RequiredFieldValidator>

                                    <asp:Button ID="btnSubmitNewNew" OnClick="btnSubmitNewNew_Click"  runat="server" Text="Create" />



                                </td>
                            </tr>
                            <tr id="trSelectCategoryNew" style="display: none;" runat="server">
                                <td>
                                    <table style="width: 400px" border="1">
                                        <tr>
                                            <td width="50%">Select Category Name:
                                            </td>
                                            <td width="50%">
                                                <asp:DropDownList ID="ddlCategoryNew" Width="100px" runat="server">
                                                    <asp:ListItem Value="-1" Text="--select--"></asp:ListItem>
                                                </asp:DropDownList>

                                                <br />
                                                <asp:RadioButton ID="rdoAliasNew" runat="server" Text="Alias" GroupName="rdoAction"
                                                    onClick="ShowOptionsForRadio(this,-2);" />
                                                <asp:RadioButton ID="rdoNewChildNew" runat="server" Text="Category" GroupName="rdoAction"
                                                    onClick="ShowOptionsForRadio(this,-3);" />
                                            </td>
                                        </tr>
                                        <tr id="trAliasOrChildNew" style="display: none;" runat="server">
                                            <td width="50%">
                                                <span id="spnTitleNew" runat="server"></span>
                                            </td>
                                            <td width="50%">
                                                <asp:TextBox ID="txtAliasOrChildNameNew" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAliasOrChildNameNew" ID="rvtxtAliasOrChildNameNew" runat="server" ErrorMessage="*"
                                                    ValidationGroup="ExsistingGroupNew" ForeColor="Red"></asp:RequiredFieldValidator>

                                                <asp:Button ID="btnSubmitExistingNew" OnClick="btnSubmitExistingNew_Click" runat="server" Text="Update" ValidationGroup="ExsistingGroupNew" />

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        </div>



        <%--<div style="overflow: hidden; font: x-large">
            <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="rptPaging_ItemCommandss">                
                <ItemTemplate>
                    <asp:LinkButton ID="btnPage"
                        Style="padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 15pt tahoma;"
                        CommandName="Page" CommandArgument="<%# Container.DataItem %>" 
                        runat="server" ForeColor="White" Font-Bold="True"><%# Container.DataItem %>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:Repeater>
        </div>--%>
        
        <asp:Repeater ID="rptSummary" runat="server" OnItemDataBound="rptDiscrepancy_ItemDataBound" OnItemCommand="rptDiscrepancy_ItemCommand">
            <HeaderTemplate>
                <table width="100%" border="1">
                    <tr>
                        <td colspan="5" align="center">Descrepancy Summary</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center">S.No
                        </td>
                        <td width="20%" align="center">Decrepancy Title
                        </td>
                        <td width="20%" align="center">Head Count
                        </td>
                        <td width="25%" align="center">Action
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr id="trRecord">
                    <td width="5%" align="center">
                        <asp:Label ID="lblSNO" runat="server"></asp:Label>
                        <asp:Label ID="descrepencyNewsFileID" runat="server" style="Display:none"></asp:Label>
                    </td>
                    <td width="10%">
                        <a id="hlnkDescrepencyValue" runat="server" target="_blank" ><%# Eval("DescrepencyValue") %></a>
                        <%--<asp:Label ID="DescrepencyValue" runat="server" style="display:none" Text='<%# Eval("DescrepencyValue") %>'></asp:Label>--%>
                    </td>
                    <td width="10%" align="center">
                        <%#Eval("TotalCount") %>
                    </td>
                    <td width="50%">
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="dlAction" runat="server" onchange="ShowOptions(this);">
                                        <asp:ListItem Value="-1" Text="--select"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="New Category"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Existing Category"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trNewCategory" style="display: none;" runat="server">
                                <td>New Category Name:
                                <asp:TextBox ID="txtNewName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtNewName" ID="rvtxtNewName" runat="server" ErrorMessage="*"
                                        ValidationGroup="NewGroup1" ForeColor="Red"></asp:RequiredFieldValidator>

                                    <asp:Button ID="btnSubmitNew" CommandName="new"   runat="server" Text="Create" />



                                </td>
                            </tr>
                            <tr id="trSelectCategory" style="display: none;" runat="server">
                                <td>
                                    <table style="width: 400px" border="1">
                                        <tr>
                                            <td width="50%">Select Category Name:
                                            </td>
                                            <td width="50%">
                                                <asp:DropDownList ID="ddlCategory" Width="100px" runat="server">
                                                    <asp:ListItem Value="-1" Text="--select--"></asp:ListItem>
                                                </asp:DropDownList>

                                                <br />
                                                <asp:RadioButton ID="rdoAlias" runat="server" Text="Alias" GroupName="rdoAction"
                                                    onClick="ShowOptionsForRadio(this,-2);" />
                                                <asp:RadioButton ID="rdoNewChild" runat="server" Text="Category" GroupName="rdoAction"
                                                    onClick="ShowOptionsForRadio(this,-3);" />
                                            </td>
                                        </tr>
                                        <tr id="trAliasOrChild" style="display: none;" runat="server">
                                            <td width="50%">
                                                <span id="spnTitle" runat="server"></span>
                                            </td>
                                            <td width="50%">
                                                <asp:TextBox ID="txtAliasOrChildName" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAliasOrChildName" ID="rvtxtAliasOrChildName" runat="server" ErrorMessage="*"
                                                    ValidationGroup="ExsistingGroup" ForeColor="Red"></asp:RequiredFieldValidator>

                                                <asp:Button ID="btnSubmitExisting" CommandName="existing" runat="server" Text="Update" ValidationGroup="ExsistingGroup" />

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <br />
  
                <div id="demo2">                   
                </div>
        <br />
        <br />


        <%--2nd repeater paging--%>
        <%--<div style="overflow: hidden; font: x-large">
            <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                <ItemTemplate>
                    <asp:LinkButton ID="btnPage"
                        Style="padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 15pt tahoma;"
                        CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                        runat="server" ForeColor="White" Font-Bold="True"><%# Container.DataItem %>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:Repeater>
        </div>--%>










        <%--<div style='margin: 5px 5px 5px 5px; border: 2px solid black;'>
        <table width="100%" border="0">
            <tr>
                <td align="right">
                    Page Size&nbsp;:&nbsp;<asp:DropDownList ID="ddlPageSizee" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="7" Selected="True" Value="7"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                        <asp:ListItem Text="99" Value="99"></asp:ListItem>
                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                        <asp:ListItem Text="200" Value="200"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblCurrentPagee" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:LinkButton ID="LinkButton01" runat="server" Text=" first " OnClick="lbtnFirstt_Click"></asp:LinkButton>
                    &nbsp; &nbsp;
                    <asp:LinkButton ID="LinkButton02" runat="server" Text=" previous " OnClick="lbtnPrevv_Click"></asp:LinkButton>&nbsp;
                    &nbsp;
                    <asp:LinkButton ID="LinkButton03" runat="server" Text=" next " OnClick="lbtnNextt_Click"></asp:LinkButton>
                    &nbsp; &nbsp;
                    <asp:LinkButton ID="LinkButton04" runat="server" Text=" last " OnClick="lbtnLastt_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
        </div>--%>











        <asp:Repeater ID="rptDiscrepancy" runat="server" OnItemDataBound="rptDiscrepancy_ItemDataBound" OnItemCommand="rptDiscrepancy_ItemCommand">
            <HeaderTemplate>
                <table width="100%" border="1" style="display:none">
                    <tr>
                        <td colspan="5" align="center">Descrepancy Details</td>
                    </tr>
                    <tr>
                        <td width="5%">S.No
                        </td>
                        <td width="10%">Descrepancy Title
                        </td>
                        <td width="30%">Source
                        </td>
                        <td width="20%">Source URL
                        </td>
                        <td width="25%" style="display: none">Action
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr id="trRecord">
                    <td width="5%" align="center">
                        <asp:Label ID="lblSNO" runat="server"></asp:Label>
                    </td>
                    <td width="8%">
                        <a id="hlnkDescrepencyValue" runat="server" target="_blank" ><%# Eval("DescrepencyValue") %></a>
                    </td>
                    <td width="20%">
                        <%#Eval("RawNewsSource") %><br />
                        <%#Eval("RawNewsTitle") %>

                    </td>
                    <td width="10%">
                        <%#Eval("RawNewsUrl") %>
                    </td>
                    <td width="50%" style="display: none">
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="dlAction" runat="server" onchange="ShowOptions(this);">
                                        <asp:ListItem Value="-1" Text="--select"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="New Category"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Existing Category"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trNewCategory" style="display: none;" runat="server">
                                <td>New Category Name:
                                <asp:TextBox ID="txtNewName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtNewName" ID="rvtxtNewName" runat="server" ErrorMessage="*"
                                        ValidationGroup="NewGroup1" ForeColor="Red"></asp:RequiredFieldValidator>

                                    <asp:Button ID="btnSubmitNew" CommandName="new" runat="server" Text="Update" />



                                </td>
                            </tr>
                            <tr id="trSelectCategory" style="display: none;" runat="server">
                                <td>
                                    <table style="width: 400px" border="1">
                                        <tr>
                                            <td width="50%">Select Category Name:
                                            </td>
                                            <td width="50%">
                                                <asp:DropDownList ID="ddlCategory" Width="100px" runat="server">
                                                    <asp:ListItem Value="-1" Text="--select--"></asp:ListItem>
                                                </asp:DropDownList>

                                                <br />
                                                <asp:RadioButton ID="rdoAlias" runat="server" Text="Alias" GroupName="rdoAction"
                                                    onClick="ShowOptionsForRadio(this,-2);" />
                                                <asp:RadioButton ID="rdoNewChild" runat="server" Text="Category" GroupName="rdoAction"
                                                    onClick="ShowOptionsForRadio(this,-3);" />
                                            </td>
                                        </tr>
                                        <tr id="trAliasOrChild" style="display: none;" runat="server">
                                            <td width="50%">
                                                <span id="spnTitle" runat="server"></span>
                                            </td>
                                            <td width="50%">
                                                <asp:TextBox ID="txtAliasOrChildName" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAliasOrChildName" ID="rvtxtAliasOrChildName" runat="server" ErrorMessage="*"
                                                    ValidationGroup="ExsistingGroup" ForeColor="Red"></asp:RequiredFieldValidator>

                                                <asp:Button ID="btnSubmitExisting" CommandName="existing" runat="server" Text="Update" ValidationGroup="ExsistingGroup" />

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </form>            

</body>
<script type="text/javascript">
    function newsFunc(a,b)
    {
        console.log(a);
        console.log(b);
    }
    function ShowOptions(obj) {
        var IDTemplate = obj.id.replace('_dlAction_', '|');

        objNewCategoryID = obj.id.replace('dlAction', 'trNewCategory');
        objSelectCategoryID = obj.id.replace('dlAction', 'trSelectCategory');
        objAliasOrChildID = obj.id.replace('dlAction', 'trAliasOrChild');
        spnTitleID = obj.id.replace('dlAction', 'spnTitle');

        objID = obj.options[obj.selectedIndex].value;
        objNewCategory = document.getElementById(objNewCategoryID);
        objSelectCategory = document.getElementById(objSelectCategoryID);
        objAliasOrChild = document.getElementById(objAliasOrChildID);
        spnTitle = document.getElementById(spnTitleID);

        if (objID == "1")    //means new category
        {
            objNewCategory.style.display = 'block';
            objSelectCategory.style.display = 'none';
        }
        else if (objID == "2") {
            objNewCategory.style.display = 'none';
            objSelectCategory.style.display = 'block';

        }
        else if (objID == "-2" || objID == "-3") {
            //alert(objID);
            objAliasOrChild.style.display = 'block';

            if (objID == "-2")
                spnTitle.innerHTML = "Alias Name";
            else
                spnTitle.innerHTML = "Child Name";
        } else if (objID == "-1") {
            objSelectCategory.style.display = 'none';
            objNewCategory.style.display = 'none';
        }
    }
    function ShowOptionsForRadio(obj, objID) {

        var IDTemplate = obj.id.replace('rdoAlias', '');
        IDTemplate = IDTemplate.replace('rdoNewChild', '');

        var idToReplace = '';
        if (objID == "-2")    // means rdoAlias
            idToReplace = 'rdoAlias';
        else if (objID == "-3")
            idToReplace = 'rdoNewChild';

        objNewCategoryID = obj.id.replace(idToReplace, 'trNewCategory');
        objSelectCategoryID = obj.id.replace(idToReplace, 'trSelectCategory');
        objAliasOrChildID = obj.id.replace(idToReplace, 'trAliasOrChild');
        spnTitleID = obj.id.replace(idToReplace, 'spnTitle');

        objNewCategory = document.getElementById(objNewCategoryID);
        objSelectCategory = document.getElementById(objSelectCategoryID);
        objAliasOrChild = document.getElementById(objAliasOrChildID);
        spnTitle = document.getElementById(spnTitleID);

        //alert(spnTitle);
        if (objID == "-2" || objID == "-3") {
            //alert(objID);
            objAliasOrChild.style.display = 'block';

            if (objID == "-2")
                spnTitle.innerHTML = "Alias Name";
            else
                spnTitle.innerHTML = "Child Name";
        }
    }
</script>
</html>
