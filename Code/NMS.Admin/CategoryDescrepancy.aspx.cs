﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NMS.MongoRepository;
using NMS.Service;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.IService;
using NMS.Core;
using NMS.Core.Enums;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Drawing;


namespace NMS.Admin
{
    public partial class CategoryDescrepancy : System.Web.UI.Page
    {
        IDescrepencyNewsFileService objDescrepencyNewsFileService = IoC.Resolve<IDescrepencyNewsFileService>("DescrepencyNewsFileService");
        //paging 4 rpt1-->
        public int NowViewing
        {
            get
            {
                object obj = ViewState["_NowViewing"];
                if (obj == null)
                    return 0;
                else
                    return (int)obj;
            }
            set
            {
                this.ViewState["_NowViewing"] = value;
            }
        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);
            ddlPageSize.SelectedIndexChanged += new EventHandler(ddlPageSize_SelectedIndexChanged);
        }

        void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fill repeater for Pager event
           // FillRepeater(Navigation.Pager);
            FillRepeater(Navigation.First);
        }

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!Page.IsPostBack)
        //        FillRepeater(Navigation.None);
        //}

        protected void lbtnPrev_Click(object sender, EventArgs e)
        {
            //Fill repeater for Previous event
            FillRepeater(Navigation.Previous);
        }

        /// <summary>
        /// Assign PagedDataSource to Repeater
        /// </summary>
        /// <param name="navigation">A Navigation enum to identify current page</param>
        private void FillRepeater(Navigation navigation)
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<NMS.Core.Entities.Mongo.MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyByStatus((int)DescrepencyType.Category);
            var group = from obj in lstDescrepancy
                        group obj by obj.DescrepencyValue
                            into summary
                            select new DescrepancySummary { DescrepencyValue = summary.Key, TotalCount = summary.Count() };


            List<DescrepancySummary> lstDescrepancySummary = group.OrderByDescending(x => x.TotalCount).ToList<DescrepancySummary>();

            PagedDataSource pgitemss = new PagedDataSource();
            pgitemss.DataSource = lstDescrepancySummary;

            //Create the object of PagedDataSource
            PagedDataSource objPds = new PagedDataSource();

            //Assign our data source to PagedDataSource object
            objPds.DataSource = pgitemss;

            //Set the allow paging to true
            objPds.AllowPaging = true;

            //Set the number of items you want to show
            objPds.PageSize = int.Parse(ddlPageSize.SelectedValue);

            //Based on navigation manage the NowViewing
            switch (navigation)
            {
                case Navigation.Next:       //Increment NowViewing by 1
                    NowViewing++;
                    break;
                case Navigation.Previous:   //Decrement NowViewing by 1
                    NowViewing--;
                    break;
                case Navigation.Last:       //Make NowViewing to last page for PagedDataSource
                    NowViewing = objPds.PageCount - 1;
                    break;
                case Navigation.Pager:      //Change NowViewing based on pager size and page count
                    if (int.Parse(ddlPageSize.SelectedValue) >= objPds.PageCount)
                        NowViewing = objPds.PageCount - 1;
                    break;
                case Navigation.Sorting:
                    break;
                default:                    //Default NowViewing set to 0
                    NowViewing = 0;
                    break;
            }

            //Set the current page index
            objPds.CurrentPageIndex = NowViewing;

            //Change the text Now viewing text
            lblCurrentPage.Text = "Now viewing : " + (NowViewing + 1).ToString() + " of " + objPds.PageCount.ToString();

            // Disable Prev, Next, First, Last buttons if necessary
            lbtnPrev.Enabled = !objPds.IsFirstPage;
            lbtnNext.Enabled = !objPds.IsLastPage;
            lbtnFirst.Enabled = !objPds.IsFirstPage;
            lbtnLast.Enabled = !objPds.IsLastPage;

            //Assign PagedDataSource to repeater
            rptSummary.DataSource = objPds;
            rptSummary.DataBind();

        }

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            //Fill repeater for Next event
            FillRepeater(Navigation.Next);
        }

        protected void lbtnFirst_Click(object sender, EventArgs e)
        {
            //Fill repeater for First event
            FillRepeater(Navigation.First);
        }

        protected void lbtnLast_Click(object sender, EventArgs e)
        {
            //Fill repeater for Last event
            FillRepeater(Navigation.Last);
        }
        protected void rptUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ViewState["SortExpression"] = e.CommandName;
            FillRepeater(Navigation.Sorting);
        }
        //paging 4 rpt1--x

        List<Category> lstCategories;
        protected void Page_Load(object sender, EventArgs e)
        {
            BindCategories();
            if (!IsPostBack)
            {
                //BindCategoryDescrepancy();
                // BindDescrepancySummary();     
                BindHeaderCategories();
            }
            if (!Page.IsPostBack)
                FillRepeater(Navigation.None);
        }

        private void BindHeaderCategories()
        {
            ddlCategoryNew.DataSource = lstCategories;
            ddlCategoryNew.DataTextField = "Category";
            ddlCategoryNew.DataValueField = "CategoryId";
            ddlCategoryNew.DataBind();
        }
        private void BindCategoryDescrepancy()
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<NMS.Core.Entities.Mongo.MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyByStatus((int)DescrepencyType.Category);
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = lstDescrepancy;
            pgitems.AllowPaging = true;            
            pgitems.PageSize = 10;        
            BindCategories();
            rptDiscrepancy.DataSource = pgitems;
            rptDiscrepancy.DataBind();
            // BindDescrepancySummary(lstDescrepancy);
        }

        private void BindDescrepancySummary()
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<NMS.Core.Entities.Mongo.MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyByStatus((int)DescrepencyType.Category);
            var group = from obj in lstDescrepancy
                        group obj by obj.DescrepencyValue
                            into summary
                            select new DescrepancySummary { DescrepencyValue = summary.Key, TotalCount = summary.Count()};
            List<DescrepancySummary> lstDescrepancySummary = group.OrderByDescending(x => x.TotalCount).ToList<DescrepancySummary>();
            PagedDataSource pgitemss = new PagedDataSource();
            pgitemss.DataSource = lstDescrepancySummary;      
        }

        protected void rptDiscrepancy_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblSNO = (Label)e.Item.FindControl("lblSNO");
                lblSNO.Text = Convert.ToString(e.Item.ItemIndex + 1);

                Label descrepencyNewsFileID = (Label)e.Item.FindControl("descrepencyNewsFileID");
                descrepencyNewsFileID.Text = DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();
                

                HtmlAnchor hlnkDescrepencyValue = (HtmlAnchor)e.Item.FindControl("hlnkDescrepencyValue");
                hlnkDescrepencyValue.HRef = "DescripencyDetail.aspx?Type=2&Discrepancyvalue=" + DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();

                DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
                ddlCategory.DataSource = lstCategories;
                ddlCategory.DataTextField = "Category";
                ddlCategory.DataValueField= "CategoryId";
                ddlCategory.DataBind();

                Button btnSubmitNew = (Button)e.Item.FindControl("btnSubmitNew");
                Button btnSubmitExisting = (Button)e.Item.FindControl("btnSubmitExisting");

                TextBox txtNewName  = (TextBox)e.Item.FindControl("txtNewName");
                TextBox txtAliasOrChildName = (TextBox)e.Item.FindControl("txtAliasOrChildName");
                txtAliasOrChildName.Text = DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();
                btnSubmitNew.CommandArgument = DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();

                btnSubmitExisting.CommandArgument = DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();


                RequiredFieldValidator rvtxtNewName = (RequiredFieldValidator)e.Item.FindControl("rvtxtNewName");
                RequiredFieldValidator rvtxtAliasOrChildName = (RequiredFieldValidator)e.Item.FindControl("rvtxtAliasOrChildName");

                //rvtxtNewName.ControlToValidate = txtNewName.ClientID;
                //rvtxtAliasOrChildName.ControlToValidate = txtAliasOrChildName.ClientID;
            }
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["PageNumber"] = value; }
        }

        //This method will fire when clicking on the page no link from the pager repeater
        protected void rptPaging_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            string a = "btnPage";
            //LinkButton linkbtTitle = e.Item.;
            //LinkButton linkbtTitle = (LinkButton)e.Item.FindControl("btnPage");
            //linkbtTitle.ForeColor = System.Drawing.Color.HotPink;

            Repeater curObj = (Repeater)source;
           // curObj.CssClass = "abc";


            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindCategoryDescrepancy();            
        }

        protected void rptPaging_ItemCommandss(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            LinkButton txtNewName = (LinkButton)e.Item.FindControl(UniqueID);
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindDescrepancySummary();
            BindCategoryDescrepancy();
            BindCategories();            
        }

        private void BindCategories()
        {

            ICategoryService objCateoryService = IoC.Resolve<ICategoryService>("CategoryService");
            lstCategories = objCateoryService.GetAllCategory().OrderBy(x => x.Category).ToList();
            lstCategories.Insert(0, new Category { Category = "--select--", CategoryId = -1 });
        }

        protected void rptDiscrepancy_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label descrepencyNewsFileID = (Label)e.Item.FindControl("descrepencyNewsFileID");
            string Id = descrepencyNewsFileID.Text;
            if (e.CommandName.ToString() == "new")
            {
                //Label descrepencyValue = (Label)e.Item.FindControl("DescrepencyValue");
                TextBox txtNewName = (TextBox)e.Item.FindControl("txtNewName");
                AddNewCategory(txtNewName.Text, e.CommandArgument.ToString(), 0, Id);
                GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
                //Response.Write("add new name as -->"+ txtNewName.Text);
                //BindCategoryDescrepancy();

                BindCategories();
                FillRepeater(Navigation.None);
            }

            else if (e.CommandName.ToString() == "existing")
            {
                //Label descrepencyValue = (Label)e.Item.FindControl("DescrepencyValue");
                TextBox txtAliasOrChildName = (TextBox)e.Item.FindControl("txtAliasOrChildName");
                RadioButton rdoAlias = (RadioButton)e.Item.FindControl("rdoAlias");
                RadioButton rdoNewChild = (RadioButton)e.Item.FindControl("rdoNewChild");
                DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");

                if (ddlCategory.SelectedValue != "-1")
                {
                    if (rdoAlias.Checked && ddlCategory.SelectedValue != "-1")
                    {
                        //Response.Write("Alias name as -->" + txtAliasOrChildName.Text);
                        //AddNewLocationAlias(1, txtAliasOrChildName.Text.ToString());
                        AddNewCategoryAlias(Convert.ToInt32(ddlCategory.SelectedValue.ToString()), txtAliasOrChildName.Text);
                        UpdateDescrepencyCategory(Id, Convert.ToInt32(ddlCategory.SelectedValue.ToString()));
                        GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
                        //  BindCategoryDescrepancy();
                    }
                    else if (rdoNewChild.Checked && ddlCategory.SelectedValue != "-1")
                    {
                        AddNewCategory(txtAliasOrChildName.Text, e.CommandArgument.ToString(), Convert.ToInt32(ddlCategory.SelectedValue), Id);
                        GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
                        //  BindCategoryDescrepancy();
                    }
                    BindCategories();
                    FillRepeater(Navigation.None);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert('please select category from the list first');", true);
                }
            }
        }

        private void AddNewCategory(string categoryName, string categoryAliasName, int parentId,string DNewsFileId)
        {
            ICategoryService objCategorService = IoC.Resolve<ICategoryService>("CategoryService");
            
            Category objCategory = new Category { Category = categoryName, CreationDate = DateTime.UtcNow, IsApproved = true, IsActive = true };     //for time being its false
            objCategory.LastUpdateDate = objCategory.CreationDate;

            if (parentId != 0)
                objCategory.ParentId = parentId;

            Category objNewlyAddedCategory = objCategorService.InsertCategoryIfNotExists(objCategory);

            AddNewCategoryAlias(objNewlyAddedCategory.CategoryId, objNewlyAddedCategory.Category);

            if (categoryName.ToLower() != categoryAliasName.ToLower() && categoryAliasName != string.Empty)
                AddNewCategoryAlias(objNewlyAddedCategory.CategoryId, categoryAliasName);

            UpdateDescrepencyCategory(DNewsFileId, objNewlyAddedCategory.CategoryId);
        }
        private void UpdateDescrepencyCategory(string DescrepencyValue,int categoryId)
        {
            List<int> DescrepencyIds = objDescrepencyNewsFileService.GetDescrepencyNewsFileIdByDescrepencyValue(DescrepencyValue);
            for (int i = 0; i < DescrepencyIds.Count(); i++)
            {
                objDescrepencyNewsFileService.UpdateDescrepencyCategory(DescrepencyIds[i].ToString(), categoryId);
            }
        }
        private void AddNewCategoryAlias(int categoryId, string AliasName)
        {
            ICategoryAliasService objCategoryAliasService = IoC.Resolve<ICategoryAliasService>("CategoryAliasService");
            CategoryAlias objCategoryAlias = new CategoryAlias { Alias = AliasName, CategoryId = categoryId, CreationDate = DateTime.UtcNow, IsActive=true  };
            objCategoryAlias.LastUpdateDate = objCategoryAlias.CreationDate;
            objCategoryAliasService.InsertCategoryAliasIfNotExists(objCategoryAlias);
        }

        private void GetAllDescrepancyNewsByDescrepancyValue(string descrepancyValue)
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
           // List<MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyNews(DescrepencyType.Category, descrepancyValue);
           objNewsService.UpdateDescrepancyNewsByValue((int)DescrepencyType.Category, descrepancyValue);         
        }

        private void BindCategoryAlias()
        {
            ICategoryAliasService objCategoryAliasService = IoC.Resolve<ICategoryAliasService>("CategoryAliasService");
            List<CategoryAlias> objCategoryAlias = objCategoryAliasService.GetAllCategoryAlias();
            if (objCategoryAlias != null)
            {
            
                foreach (CategoryAlias oMDNews in objCategoryAlias)
                {
                    Response.Write(oMDNews.Alias + "<BR>");
                }
            }   
        }
        public enum Navigation
        {
            None,
            First,
            Next,
            Previous,
            Last,
            Pager,
            Sorting
        }

       
        protected void btnSubmitNewNew_Click(object sender, EventArgs e)
        {
            string Val = txtNewNameNew.Text;
            if (!string.IsNullOrEmpty(Val))
            {
                AddNewCategoryIndependent(Val, 0);
                dlActionNew.SelectedValue = "-1";
                FillRepeater(Navigation.None);
            }
        }

        protected void btnSubmitExistingNew_Click(object sender, EventArgs e)
        {
            string Val = txtAliasOrChildNameNew.Text;
            if (ddlCategoryNew.SelectedValue != "-1")
            {
                if (rdoAliasNew.Checked)
                {
                    AddNewCategoryAlias(Convert.ToInt32(ddlCategoryNew.SelectedValue), Val);
                }
                else { AddNewCategoryIndependent(Val, Convert.ToInt32(ddlCategoryNew.SelectedValue)); }
                dlActionNew.SelectedValue = "-1";
                FillRepeater(Navigation.None);
            }

        }
        
        private void AddNewCategoryIndependent(string categoryName, int parentId)
        {
            ICategoryService objCategorService = IoC.Resolve<ICategoryService>("CategoryService");

            Category objCategory = new Category { Category = categoryName, CreationDate = DateTime.UtcNow, IsApproved = true, IsActive = true };     //for time being its false
            objCategory.LastUpdateDate = objCategory.CreationDate;

            if (parentId != 0)
                objCategory.ParentId = parentId;

            Category objNewlyAddedCategory = objCategorService.InsertCategoryIfNotExists(objCategory);

            AddNewCategoryAlias(objNewlyAddedCategory.CategoryId, objNewlyAddedCategory.Category);


        }
    }
}