﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CleanUp.aspx.cs" Inherits="NMS.Admin.CleanUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ForeColor="Red" ID="ExceptionMsg" runat="server"></asp:Label>
        <asp:Label ForeColor="Red" ID="RollbackExceptionMsg" runat="server"></asp:Label>
        <asp:Label ForeColor="Green" ID="SuccessMsg" runat="server"></asp:Label>
     <div style='margin: 5px 5px 5px 5px; border: 2px solid black;'>
        <table width="100%" border="0">
            <tr>Category</tr>
            <tr>
                <td align="left">
                    From Category :&nbsp;:&nbsp;<asp:DropDownList ID="fromCategory" runat="server">
                     
                    </asp:DropDownList> 
                </td>
            </tr>
             <tr>
                <td align="left">
                     To Category :&nbsp;:&nbsp;<asp:DropDownList ID="toCategory" runat="server">
                     
                    </asp:DropDownList> 
                </td>
            </tr>
            
           </table>
         <asp:Button ID ="submitCategory" runat="server" OnClick="submitCategory_Click"  Text="Update"/>
          <%--<asp:Repeater ID="rptCategories" runat="server">
            <HeaderTemplate>
                <table width="100%" border="1">
                    <tr>
                        <td colspan="5" align="center">UnUsedCategories</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center">S.No
                        </td>
                        <td width="20%" align="center">Decrepancy Title
                        </td>
                        <td width="20%" align="center">Head Count
                        </td>
                        <td width="25%" align="center">Action
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr id="trRecord">
                    <td width="5%" align="center">
                        <asp:Label ID="lblSNO" runat="server"></asp:Label>
                    </td>
                    <td width="10%">
                        <a id="hlnkDescrepencyValue" runat="server" target="_blank" ></a>
                    </td>
                    <td width="10%" align="center">
                        
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>--%>
        </div>

        <div style='margin: 5px 5px 5px 5px; border: 2px solid black;'>
      <table width="100%" border="0">
            <tr>Location</tr>
            <tr>
                <td align="left">
                    From Location :&nbsp;:&nbsp;<asp:DropDownList ID="fromLocation" runat="server">
                     
                    </asp:DropDownList> 
                </td>
            </tr>
             <tr>
                <td align="left">
                     To Location :&nbsp;:&nbsp;<asp:DropDownList ID="toLocation" runat="server">
                     
                    </asp:DropDownList> 
                </td>
            </tr>
           
           </table>
            <asp:Button ID ="submitLocation" runat="server" OnClick="submitLocation_Click"  Text="Update"/>
            <%--<asp:Repeater ID="rptLocations" runat="server" >
            <HeaderTemplate>
                <table width="100%" border="1">
                    <tr>
                        <td colspan="5" align="center">UnUsedLocations</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center">S.No
                        </td>
                        <td width="20%" align="center">Decrepancy Title
                        </td>
                        <td width="20%" align="center">Head Count
                        </td>
                        <td width="25%" align="center">Action
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr id="trRecord">
                    <td width="5%" align="center">
                        <asp:Label ID="lblSNO" runat="server"></asp:Label>
                    </td>
                    <td width="10%">
                        <a id="hlnkDescrepencyValue" runat="server" target="_blank" ></a>
                    </td>
                    <td width="10%" align="center">
                        
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>--%>
        </div>

        <div style='margin: 5px 5px 5px 5px; border: 2px solid black;' >
            <asp:Repeater ID="rptFilters" runat="server" OnItemDataBound="rptFilters_ItemDataBound" OnItemCommand="rptFilters_ItemCommand">
            <HeaderTemplate>
                <table width="100%" border="1">
                    <tr>
                        <td colspan="5" align="center">Filters</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center">S.No
                        </td>
                        <td width="20%" align="center">[Value]
                        </td>
                        <td width="20%" align="center">Head Count
                        </td>
                        <td width="25%" align="center">Action
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr id="trRecord">
                    <td width="5%" align="center">
                        <asp:Label ID="lblSNO" runat="server"></asp:Label>
                    </td>
                    <td width="10%">
                       <a id="hlnkCategoryId" runat="server" target="_blank" > <%# Eval("CategoryId") %></a>
                    </td>
                    <td width="10%" align="center">
                         <%# Eval("TotalCount") %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
    </form>
</body>
</html>
