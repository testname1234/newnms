﻿using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NMS.Admin
{
    public partial class CleanUp : System.Web.UI.Page
    {
        List<Category> lstCategories;
        List<Location> lstLocation;
        static string connectionString = string.Empty;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            FillLists();
            connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["conn"]);
           
            if (!Page.IsPostBack) {
                FillDropdown();
                LoadFilters();
            }
        }

        private void LoadFilters()
        {
            IFilterService filterService = IoC.Resolve<IFilterService>("FilterService");
            //INewsFileFilterService newsFileFilterService = IoC.Resolve<INewsFileFilterService>("NewsFileFilterService");
            List<Filter> Allfilter =  filterService.GetAllFilter();
            Allfilter = Allfilter.Where(x => x.Value != null && x.FilterTypeId == 13).ToList();
            var group = from obj in Allfilter
                        group obj by obj.Value
                            into summary
                        select new FilterSummary { CategoryId = summary.Key.Value, TotalCount = summary.Count() };

            group = group.Where(x => x.TotalCount > 1);
            List<FilterSummary> lstDescrepancySummary = group.OrderByDescending(x => x.TotalCount).ToList<FilterSummary>();
            rptFilters.DataSource = lstDescrepancySummary;
            rptFilters.DataBind();
        }

        private void FillDropdown()
        {

            var cat = lstCategories.Select(p => new { CategoryId = p.CategoryId, Category =  p.Category + ":---" + p.CategoryId.ToString() });
            var loc = lstLocation.Select(p => new { LocationId = p.LocationId, Location =  p.Location + ":---" + p.LocationId.ToString() });

            genericMethod(fromCategory, cat, "Category","CategoryId");
            genericMethod(toCategory, cat, "Category", "CategoryId");
            genericMethod(fromLocation, loc, "Location", "LocationId");
            genericMethod(toLocation, loc, "Location", "LocationId");
        }

        private void FillLists()
        {
            BindCategories();
            BindLocations();
        }

        private void BindCategories()
        {
            ICategoryService objCateoryService = IoC.Resolve<ICategoryService>("CategoryService");
            lstCategories = objCateoryService.GetAllCategory().OrderBy(x => x.Category).ToList();
            lstCategories.Insert(0, new Category { Category = "--select--", CategoryId = -1 });
        }

        private void BindLocations()
        {
            ILocationService objLocationService = IoC.Resolve<ILocationService>("LocationService");
            lstLocation = objLocationService.GetAllLocation().OrderBy(x => x.Location).ToList();
            lstLocation.Insert(0, new Location { Location = "--select--", LocationId = -1 });
        }

        private void genericMethod(DropDownList control,object list,string textField, string valueField)
        {
            control.DataSource = list;
            control.DataTextField = textField;
            control.DataValueField = valueField;
            control.DataBind();
        }
        
        private bool categoryExecuteSqlTransaction(string fromCategory, string toCategory)
        {
            bool flag = true;
            ExceptionMsg.Text = "";
            RollbackExceptionMsg.Text = "";
            SuccessMsg.Text = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("SampleTransaction");

                // Must assign both transaction object and connection
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    string sqlCMD = string.Format(@"
                                      declare @fromCategory int;
                                      declare @toCategory int;
                                      declare @fromFilterId int;
                                      declare @toFilterId int;  
                                      set @fromCategory= {0}
                                      set @toCategory= {1}
                                      set @fromFilterId = (select filterid from filter where[value] = @fromCategory and FilterTypeId = 13)
                                      set @toFilterId = (select filterid from filter where[value] = @toCategory and FilterTypeId = 13)
                                      update newsfile set CategoryId=@toCategory where CategoryId=@fromCategory
                                      update FileCategory set CategoryId=@toCategory where CategoryId=@fromCategory
                                      update CategoryAlias set CategoryId=@toCategory where CategoryId=@fromCategory
                                      update Slot set CategoryId=@toCategory where CategoryId=@fromCategory
                                      update NewsFileFilter set filterid=@toFilterId where filterid=@fromFilterId
                                      delete from filter where filterid=@fromFilterId
                                      delete from Category where CategoryId=@fromCategory", fromCategory, toCategory);
                    command.CommandText = sqlCMD;
                        
                    command.ExecuteNonQuery();
        
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    flag = false;
                    ExceptionMsg.Text = ex.Message;
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        RollbackExceptionMsg.Text = ex2.Message;
                    }
                    return flag;
                }
                connection.Close();
            }
            return flag;
        }

        private bool locationExecuteSqlTransaction(string fromLocation,string toLocation)
        {
            bool flag = true;
            ExceptionMsg.Text = "";
            RollbackExceptionMsg.Text = "";
            SuccessMsg.Text = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("SampleTransaction");
                
                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    string sqlCMD = string.Format(@"
                                      declare @fromLocation int;
                                      declare @toLocation int;
                                      set @fromLocation= {0}
                                      set @toLocation= {1}
                                      update newsfile set locationid=@toLocation where Locationid=@fromLocation
                                      update filelocation set locationid=@toLocation where Locationid=@fromLocation
                                      update locationalias set locationid=@toLocation where Locationid=@fromLocation
                                      delete from location where locationid=@fromLocation", fromLocation, toLocation);

                    command.CommandText = sqlCMD;
                    command.ExecuteNonQuery();

                    // Attempt to commit the transaction.
                    transaction.Commit();
                   // Console.WriteLine("Both records are written to database.");
                }
                catch (Exception ex)
                {
                    flag = false;
                    ExceptionMsg.Text = ex.Message;
                    // Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    // Console.WriteLine("  Message: {0}", ex.Message);

                    // Attempt to roll back the transaction.
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        RollbackExceptionMsg.Text = ex2.Message;
                        // This catch block will handle any errors that may have occurred
                        // on the server that would cause the rollback to fail, such as
                        // a closed connection.
                        //Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        //Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                    return flag;
                }
                connection.Close();
            }
            return flag;
        }

        protected void submitCategory_Click(object sender, EventArgs e)
        {
            if (fromCategory.SelectedValue != "-1" && toCategory.SelectedValue != "-1" && fromCategory.SelectedValue != toCategory.SelectedValue)
            {
                if (categoryExecuteSqlTransaction(fromCategory.SelectedValue, toCategory.SelectedValue))
                {
                    SuccessMsg.Text = "Category converted from " + fromCategory.SelectedValue + " To " + toCategory.SelectedValue;
                    FillDropdown();
                }
              
                
            }
        }

        protected void submitLocation_Click(object sender, EventArgs e)
        {
            if (fromLocation.SelectedValue != "-1" && toLocation.SelectedValue != "-1" && fromLocation.SelectedValue != toLocation.SelectedValue)
            {
                if (locationExecuteSqlTransaction(fromLocation.SelectedValue, toLocation.SelectedValue))
                {
                    SuccessMsg.Text = "Location converted from " + fromLocation.SelectedValue + " To :" + toLocation.SelectedValue;
                    FillDropdown();
                }
              
                
            }
        }

        protected void rptFilters_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblSNO = (Label)e.Item.FindControl("lblSNO");
                lblSNO.Text = Convert.ToString(e.Item.ItemIndex + 1);

                HtmlAnchor hlnkCategoryId = (HtmlAnchor)e.Item.FindControl("hlnkCategoryId");
                hlnkCategoryId.HRef = "filterDetail.aspx?CategoryId=" + DataBinder.Eval(e.Item.DataItem, "CategoryId").ToString();
            }
        }

        protected void rptFilters_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //Label descrepencyNewsFileID = (Label)e.Item.FindControl("descrepencyNewsFileID");
            //string Id = descrepencyNewsFileID.Text;
            //if (e.CommandName.ToString() == "new")
            //{
            //    //Label descrepencyValue = (Label)e.Item.FindControl("DescrepencyValue");
            //    TextBox txtNewName = (TextBox)e.Item.FindControl("txtNewName");
            //    AddNewCategory(txtNewName.Text, e.CommandArgument.ToString(), 0, Id);
            //    GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
            //    //Response.Write("add new name as -->"+ txtNewName.Text);
            //    //BindCategoryDescrepancy();

            //    BindCategories();
            //    FillRepeater(Navigation.None);
            //}

            //else if (e.CommandName.ToString() == "existing")
            //{
            //    //Label descrepencyValue = (Label)e.Item.FindControl("DescrepencyValue");
            //    TextBox txtAliasOrChildName = (TextBox)e.Item.FindControl("txtAliasOrChildName");
            //    RadioButton rdoAlias = (RadioButton)e.Item.FindControl("rdoAlias");
            //    RadioButton rdoNewChild = (RadioButton)e.Item.FindControl("rdoNewChild");
            //    DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");

            //    if (ddlCategory.SelectedValue != "-1")
            //    {
            //        if (rdoAlias.Checked && ddlCategory.SelectedValue != "-1")
            //        {
            //            //Response.Write("Alias name as -->" + txtAliasOrChildName.Text);
            //            //AddNewLocationAlias(1, txtAliasOrChildName.Text.ToString());
            //            AddNewCategoryAlias(Convert.ToInt32(ddlCategory.SelectedValue.ToString()), txtAliasOrChildName.Text);
            //            UpdateDescrepencyCategory(Id, Convert.ToInt32(ddlCategory.SelectedValue.ToString()));
            //            GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
            //            //  BindCategoryDescrepancy();
            //        }
            //        else if (rdoNewChild.Checked && ddlCategory.SelectedValue != "-1")
            //        {
            //            AddNewCategory(txtAliasOrChildName.Text, e.CommandArgument.ToString(), Convert.ToInt32(ddlCategory.SelectedValue), Id);
            //            GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
            //            //  BindCategoryDescrepancy();
            //        }
            //        BindCategories();
            //        FillRepeater(Navigation.None);

            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert('please select category from the list first');", true);
            //    }
            //}
        }
    }


    public class FilterSummary
    {
        public int CategoryId { get; set; }
        public int TotalCount { get; set; }
    }
}