﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ControlPanel.aspx.cs" Inherits="NMS.Admin.ControlPanell" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Control Panel
    </title>
    
    <style type="text/css">
        .Table {
            border-width: 1px;
            border-style: solid;
        }

            .Table th {
                text-align: center;
                font-weight: bold;
                width: 120px;
                border-width: 1px;
                border-style: solid;
            }

            .Table td {
            }

        .CollapsibleListHeader {
            background-color: #EDEADD;
            height: 20px;
            color: Black;
            font-weight: bold;
        }

        .consoleWindow {
            font-family: monospace;
            overflow: auto;
            height: 300px;
            width: 95%;
            margin: 0 auto;
            background-color: black;
            color: white;
            padding: 5px;
        }

            .consoleWindow .log {
                color:white;
                display:block;
            }

            .consoleWindow .error {
                color:red;
                display:block;
            }

            .consoleWindow .warning {
                color:yellow;
                display:block;
            }
    </style>
</head>
<body>
    <div>
        <table id="tblMain" cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="border: solid 1px Black;">
            <tr class="CollapsibleListHeader">
                <td>
                    <%--<div onclick="toggle('<%= tblChildItems.ClientID %>')" style="cursor: pointer;">
                        <table cellpadding="0" width="100%">
                            <tr>
                                <td style="padding-left: 5px;">LivePerson Control Panel
                                </td>
                                <td align="right" id="tdCommand" style="padding-right: 5px;">
                                    <table cellpadding="0">
                                        <tr>
                                            <td>
                                                <div id="lblCommand" style="cursor: pointer; font-size: 11px; text-align: right;">Show</div>
                                            </td>
                                            <td>
                                                <div id="lblSign" style='width: 20px; font-size: large; cursor: pointer;'>+</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>--%>
                </td>
            </tr>
        </table>

        <form runat="server">
        <asp:HiddenField ID="hdn1"  runat="server" value="false" />
        <asp:HiddenField ID="hdn2"  runat="server" value="false" />
        </form>


        <table cellpadding="0" cellspacing="1" border="0" class="Table" width="100%">
            <tr class="gridView_header">
                <th width="180px">Service</th>
                <th>Message</th>
                <th width="80px">Status</th>
                <th width="50px">Operation</th>
                <th width="50px">Continuous</th>
                <th width="90px">Scheduled</th>
            </tr>

            <!-- ko foreach: vm.systemProcesses -->
            <tr>
                <th colspan="6" style="background-color: #edeadd; text-align: left; white-space: nowrap;">&nbsp;&nbsp;<label data-bind="text:name"></label></th>
            </tr>
            <!-- ko foreach: $data.systemProcessThreadList -->
            <tr>
                <td>
                    <label data-bind="text:name"></label>
                </td>
                <td>
                    <label data-bind="text:message"></label>
                </td>
                <td>&nbsp;&nbsp; 
                    <label data-bind="text:$data.status"></label>
                </td>

                <td align="center">
                    <a  data-bind="attr: { href: 'ControlPanel.aspx?id=' + systemProcessThreadId() + '&opt=enabled', title: enabled },text:enableButtonText"></a>
               
                </td>
                <td align="center">
                   <a  data-bind="attr: { href: 'ControlPanel.aspx?id=' + systemProcessThreadId() + '&opt=cont', title: continuous },text:continuousButtonText"></a>
                  
                </td>
                <td style="width: 90px;">
                    <label data-bind="text:continuousScheduleTime"></label>
                </td>
            </tr>
            <!-- /ko -->
            <!-- /ko -->

        </table>



        <table cellpadding="0" cellspacing="3" align="center" width="100%" style="border: solid 1px Black;">
            <tr>
                <td>

                    <br />
                    <div id='divActivity' class="consoleWindow">
                        <!-- ko foreach: vm.eventLogs -->
                        <span data-bind="attr:{class:cssClass()},text:message()">
                        </span>
                        <!-- /ko -->
                    </div>
                    <input type="hidden" id='txtLastActivity' />
                    <br />

                </td>
            </tr>
        </table>
    </div>
    <script src="/Scripts/jquery-1.8.2.min.js"></script>
    <script src="/Scripts/lib/knockout-2.2.1.js"></script>
    <script src="/Scripts/common.js"></script>
    <script src="/Scripts/host/controlpanel.js"></script>
    
    <script type="text/javascript" language="javascript">
        var interval = 5000;
        var threadLastUpdateDate = $('[id$=hdn2]').val();
                var eventLoglastUpdateDate = $('[id$=hdn2]').val();
                var vm = new viewModel(JSON.parse($('[id$=hdn1]').val()));

                $(document).ready(function () {
                    ko.applyBindings(vm);
                    startUpdate();
                });


        function startUpdate() {
            setTimeout(function () {
                Common.postCall('ControlPanel.aspx/GetUpdatedSystemProcessThreads', { ThreadLastUpdateDateStr: threadLastUpdateDate, EventLogLastUpdateDateStr: eventLoglastUpdateDate }, function (data) {
                    data = JSON.parse(data.d);
                    if (data.Threads) {
                        vm.updateProcessThreads(data.Threads);
                        if (data.EventLogs)
                            for (var i = 0; i < data.EventLogs.length; i++) {
                                vm.addLog(new EventLog(data.EventLogs[i].Title + " " + data.EventLogs[i].Description, data.EventLogs[i].EventCode));
                            }
                    }
                    threadLastUpdateDate = data.ThreadLastUpdateDateStr;
                    eventLoglastUpdateDate = data.EventLogLastUpdateDateStr;
                    startUpdate();
                }, function (err) {
                    vm.addLog(new EventLog("Error Occurred", EventLogTypes.Error));
                    startUpdate();
                })
            }, interval);
        }
            </script>
</body>
</html>




