﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NMS.Core;
using NMS.Core.Helper;
using ControlPanel.Core;
using System.Runtime.Serialization;
using ControlPanel.Core.IService;
using ControlPanel.Core.DataInterfaces;
using System.Web.Script.Serialization;



namespace NMS.Admin
{
    public partial class ControlPanell : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                GetAllSystemProcess();
           
                if (Request.QueryString["opt"] == "enabled")
                {
                    ToggleEnabled(Request["id"]);
                }
                else if (Request.QueryString["opt"] == "cont")
                {
                    ToggleContinuous(Request["id"]);
                }
            
        }


        public void GetAllSystemProcess()
        {
            ControlPanel.Core.CPServiceClient webClient = new CPServiceClient();
            List<ControlPanel.Core.Entities.SystemProcess> systemProcesses = webClient.GetAllSystemProcess();
            DateTime? lastUpdateDate = null;
            if (systemProcesses != null && systemProcesses.Count > 0)
            {
                systemProcesses = systemProcesses.OrderBy(x => x.DisplayOrder).ToList();
                foreach (ControlPanel.Core.Entities.SystemProcess systemProcess in systemProcesses)
                {
                    systemProcess.SystemProcessThreadList = webClient.GetSystemProcessThreadsByProcessID(systemProcess.SystemProcessId).OrderBy(x => x.DisplayOrder.Length).ThenBy(x => x.DisplayOrder).ToList();
                    if (systemProcess.SystemProcessThreadList != null)
                    {
                        var currentMax = systemProcess.SystemProcessThreadList.Select(x => x.LastUpdateDate).Max();
                        if (!lastUpdateDate.HasValue || currentMax > lastUpdateDate)
                            lastUpdateDate = currentMax;
                    }
                }
            }

            hdn1.Value = new JavaScriptSerializer().Serialize(systemProcesses);

            hdn2.Value = lastUpdateDate.ToString();

        }


        public void ToggleContinuous(string id)
        {
            int systemProcessThreadID = 0;
            int.TryParse(id, out systemProcessThreadID);
            if (systemProcessThreadID > 0)
            {
                ControlPanel.Core.CPServiceClient webClient = new CPServiceClient();
                webClient.ToggleSystemProcessThreadContinuous(systemProcessThreadID);
            }
            GetAllSystemProcess();
        }

        public void ToggleEnabled(string id)
        {
            int systemProcessThreadID = 0;
            int.TryParse(id, out systemProcessThreadID);
            if (systemProcessThreadID > 0)
            {
                ControlPanel.Core.CPServiceClient webClient = new CPServiceClient();
                webClient.ToggleSystemProcessThreadEnabled(systemProcessThreadID);
            }
            GetAllSystemProcess();
        }



        [DataContract]
        public class UpdateDate
        {

            [IgnoreDataMember]
            public virtual System.DateTime ThreadLastUpdateDate { get; set; }


            [DataMember(EmitDefaultValue = false)]
            public virtual string ThreadLastUpdateDateStr
            {
                get { return ThreadLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
                set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ThreadLastUpdateDate = date.ToUniversalTime(); } }
            }

            [IgnoreDataMember]
            public virtual System.DateTime EventLogLastUpdateDate { get; set; }


            [DataMember(EmitDefaultValue = false)]
            public virtual string EventLogLastUpdateDateStr
            {
                get { return EventLogLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
                set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EventLogLastUpdateDate = date.ToUniversalTime(); } }
            }
        }

        [DataContract]
        public class ControlPanelUpdateModel
        {
            [DataMember(EmitDefaultValue = false)]
            public List<ControlPanel.Core.Entities.SystemProcessThread> Threads { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public List<ControlPanel.Core.Entities.SystemEventLog> EventLogs { get; set; }

            [IgnoreDataMember]
            public virtual System.DateTime? ThreadLastUpdateDate { get; set; }


            [DataMember(EmitDefaultValue = false)]
            public virtual string ThreadLastUpdateDateStr
            {
                get { return (ThreadLastUpdateDate.HasValue) ? ThreadLastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") : string.Empty; }
                set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ThreadLastUpdateDate = date.ToUniversalTime(); } }
            }

            [IgnoreDataMember]
            public virtual System.DateTime? EventLogLastUpdateDate { get; set; }


            [DataMember(EmitDefaultValue = false)]
            public virtual string EventLogLastUpdateDateStr
            {
                get { return (EventLogLastUpdateDate.HasValue) ? EventLogLastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") : String.Empty; }
                set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EventLogLastUpdateDate = date.ToUniversalTime(); } }
            }
        }


        [WebMethod]
        public static string GetUpdatedSystemProcessThreads(string ThreadLastUpdateDateStr, string EventLogLastUpdateDateStr)
        {
            try
            {
                DateTime ThreadLastUpdateDate = Convert.ToDateTime(ThreadLastUpdateDateStr);
                DateTime EventLogLastUpdateDate = Convert.ToDateTime(EventLogLastUpdateDateStr);
                ControlPanel.Core.CPServiceClient webClient = new CPServiceClient();
                var threads = webClient.GetSystemProcessThreadsByLastUpdateDate(ThreadLastUpdateDate);
                var eventLogs = webClient.GetEventLogByLastUpdateDate(EventLogLastUpdateDate);
                DateTime maxThreadDate = ThreadLastUpdateDate;
                DateTime maxEventLogDate = EventLogLastUpdateDate;

                if (threads.Data == null)
                    threads.Data = new List<ControlPanel.Core.Entities.SystemProcessThread>();
                else if (threads.Data.Count > 0)
                    maxThreadDate = threads.Data.Select(x => x.LastUpdateDate).Max().Value;

                if (eventLogs == null) eventLogs = new List<ControlPanel.Core.Entities.SystemEventLog>();
                else if (eventLogs.Count > 0)
                    maxEventLogDate = eventLogs.Select(x => x.DateOccured).Max();

                if (threads.Data != null && threads.Data.Count > 0)

                    return JSONHelper.GetString(new ControlPanelUpdateModel() { Threads = threads.Data, ThreadLastUpdateDate = maxThreadDate, EventLogs = eventLogs, EventLogLastUpdateDate = maxEventLogDate });
                else return JSONHelper.GetString(new ControlPanelUpdateModel() { ThreadLastUpdateDate = ThreadLastUpdateDate, EventLogLastUpdateDate = EventLogLastUpdateDate });
            }
            catch (Exception exp) { return exp.Message; }
        }

    }
}