﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescripencyDetail.aspx.cs" Inherits="NMS.Admin.DescripencyDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Repeater ID="rptDiscrepancy" runat="server" OnItemDataBound ="rptResult_ItemDataBound" OnItemCommand="rptDiscrepancy_ItemCommand">
                <HeaderTemplate>
                    <table width="100%" border="1">
                        <tr>
                            <td colspan="5" align="center">Descrepancy Details</td>
                        </tr>
                        <tr>                            
                            <td width="5%">S.No
                            </td>
                            <td width="10%">Descrepancy Title
                            </td>
                            <td width="30%">Source
                            </td>
                            <td width="20%">Source URL
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="trRecord">
                        <td width="5%" align="center">
                            <asp:Label ID="lblSNO" runat="server"></asp:Label>
                        </td>
                        <td width="10%">
                           <span id='DescrepencyValue' runat="server"> <%#Eval("DescrepencyValue") %></span>

                            <div style="display:none" id='EditableArea' runat="server">
                                <asp:TextBox ID="txtNewName" runat="server" Text='<%#Eval("DescrepencyValue") %>'></asp:TextBox>
                                <asp:Button ID="btnSubmitUpdate" CommandName="Update" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DescrepencyNewsFileId") %>'   runat="server" Text="Update" />
                                <asp:Button runat="server" id="btnCancelupdate" OnClientClick="return HideEditableArea(this)" Text="Cancel" />
                            </div>
                            <asp:Button Id="btnEdit" runat="server" OnClientClick="return ShowEditableArea(this)" Text="Edit"  />
                        </td>
                        <td width="10%">
                            <%#Eval("RawNewsSource") %><br />
                            <%#Eval("RawNewsTitle") %>
                        </td>
                        <td width="10%">
                            <%#Eval("RawNewsUrl") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>
<script src="scripts/jquery-1.8.2.min.js"></script>
<script type="text/javascript">

    //var EnableEditableArea= function(){
    //    //var ele = document.getElementById('= rptDiscrepancy.FindControl("btnSubmitUpdate").ClientID');
    //    $("#EditableArea").show();
    //}
    
    var ShowEditableArea = function (obj) {
        var editableArea = "#"+obj.id.replace('btnEdit', 'EditableArea');
        var editButtonHide = "#" + obj.id;
        var hideSpan = "#" + obj.id.replace('btnEdit', 'DescrepencyValue');
        
        $(hideSpan).hide();
        $(editButtonHide).hide();
        $(editableArea).show();
        return false;
    }

    var HideEditableArea = function (obj) {
        var editableArea = "#" + obj.id.replace('btnCancelupdate', 'EditableArea');
        var editButtonShow = "#" + obj.id.replace('btnCancelupdate', 'btnEdit');
        var showSpan = "#" + obj.id.replace('btnCancelupdate', 'DescrepencyValue');
        $(editableArea).hide();
        $(showSpan).show();
        $(editButtonShow).show();
        return false;
    }
    


</script>
