﻿using Newtonsoft.Json.Linq;
using NMS.Core;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NMS.Admin
{
    public partial class DescripencyDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadPage();
            }
        }
        
        protected void rptResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblSNO = (Label)e.Item.FindControl("lblSNO");
                lblSNO.Text = Convert.ToString(e.Item.ItemIndex + 1);
            }
        }

        protected void rptDiscrepancy_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.ToString() == "Update")
            {
                string NewsFileId = Convert.ToString(e.CommandArgument);
                string NewDescrepencyValue = ((TextBox)e.Item.FindControl("txtNewName")).Text;
                IDescrepencyNewsFileService objIDescrepencyNewsFileService = IoC.Resolve<IDescrepencyNewsFileService>("DescrepencyNewsFileService");
                if (!string.IsNullOrEmpty(NewDescrepencyValue)) {
                    objIDescrepencyNewsFileService.UpdateDescrepencyValueByDescrepencyNewsFileId(NewsFileId, NewDescrepencyValue);
                    LoadPage();
                }
            }
       }

        private void LoadPage()
        {
            List<MDescrepencyNews> lstDescrepancy = null;
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            //get parameters from query string type and value
            int DiscrepencyType = 1;
            string DiscrepencyValue = "";
            if (Request.QueryString["Type"] != null && Request.QueryString["DiscrepancyValue"] != null)
            {
                DiscrepencyType = Convert.ToInt32(Request.QueryString["Type"]);
                DiscrepencyValue = Request.QueryString["DiscrepancyValue"].ToString();
            }
            if (DiscrepencyType == 2)
                lstDescrepancy = objNewsService.GetAllDescrepancyNews(DescrepencyType.Category, DiscrepencyValue);
            if (DiscrepencyType == 1)
                lstDescrepancy = objNewsService.GetAllDescrepancyNews(DescrepencyType.Location, DiscrepencyValue);

            if (lstDescrepancy != null && lstDescrepancy.Count > 0)
            {
                rptDiscrepancy.DataSource = lstDescrepancy;
                rptDiscrepancy.DataBind();
            }
        }
    }
}