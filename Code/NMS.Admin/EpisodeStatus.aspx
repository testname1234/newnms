﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EpisodeStatus.aspx.cs" Inherits="NMS.Admin.EpisodeStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 319px;
        }

        .auto-style2 {
            height: 59px;
        }

        .auto-style3 {
            font-size: xx-large;
        }
    </style>
    <style type="text/css">
        body {
            font-family:Calibri;
            font-weight:600;
            background:#eee;
            text-align:center;            
        }

        .ssscustom {
            /*background: #666b66;*/
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;           
        }

        .sssme {
            /*background: #b6b8b6;*/
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
            color:darkgreen;
            text-decoration:underline;
            font-weight:800;
        }

        .sasasss {
            text-decoration: none !important;
            color: blue;
            font-family: sans-serif;
        }
            .sasasss:hover {
                color: red;
            }

        .trcntr {
            text-align: center;                        
        }
        .trcnttr {
            background:rgb(170, 213, 170);             
        }
        .captn {            
            /*font-family: sans-serif;
            font-weight: bolder;*/
            color: #393c39;
        }
        .whiteclr {
            font-size:14px;
            font-family:Calibri;
            background:white;
            color: rgb(79, 88, 92);
        }
        .tdteleprompterjson .tableteleprom {
            text-align:center;
        }
        .tdlayoutjson .fxd {
            text-align:center;
        }
        .thclr {
            color: darkgreen;
        }
        .axxnnn {
              border-radius: 5px 5px 5px 5px;
              border-color: aliceblue;             
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1 ssscustom">
                <caption class="auto-style3 sssme">
                    <strong>EPISODE STATUS</strong></caption>
                <tr>
                    <td class="auto-style2">
                        <%--<asp:HiddenField ID="mae_hdn" runat="server" />--%>
                        <asp:Repeater ID="mactepisodeGridView" runat="server" OnItemCommand="mactepisodeGridView_ItemCommand" OnItemDataBound="mactepisodeGridView_ItemDataBound">
                            <HeaderTemplate>
                                <table border="1" cellpadding="2" align="center" class="axxnnn">
                                    <tr class="trcntr trcnttr">
                                        <%--<th>MosActiveEpisodeId</th>--%>
                                        <th class="thclr">Episode<br />Id</th>
                                        <th class="thclr">Program</th>
                                        <th class="thclr">Episode</th>
                                        <th class="thclr">Status<br />Code</th>
                                        <th class="thclr">Creation<br />Date</th>
                                        <th class="thclr">LastUpdate<br />Date</th>
                                        <%--<th>Tele<br />Prompter</th>
                                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>--%>
                                        <%--<th>FourWindow</th>--%>
                                        <th class="thclr">TelePrompter</th>
                                        <th class="thclr">Graphics</th>
                                        <%--<th>FourWindow<br />Json</th>--%>
                                        <th class="thclr">StatusDescription</th>
                                        <th class="thclr">Update Row</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="trcntr whiteclr" runat="server">
                                    <%--<td>
                                        <asp:Label ID="mosaepisodeLabel1" runat="server" Text='<%# Eval("MosActiveEpisodeId") %>' /></td>--%>
                                    <td>
                                        <asp:Label ID="mosaepisodeLabel2" runat="server" Text='<%# Eval("EpisodeId") %>' /></td>
                                    <td>
                                        <asp:Label ID="programlabel" runat="server" Text='<%# Eval("ProgramName") %>' /></td>
                                    <td>
                                        <asp:Label ID="episodelabel" runat="server" Text='<%# Eval("EpisodeStartTime") %>' /></td>
                                    <td id="statuscodtd" runat="server">
                                        <asp:Label ID="StatusCode" runat="server" /></td>                                    
                                    <td>
                                        <asp:Label ID="mosaepisodeLabel4" runat="server" Text='<%# Eval("CreationDate") %>' /></td>
                                    <td>
                                        <asp:Label ID="mosaepisodeLabel5" runat="server" Text='<%# Eval("LastUpdateDate") %>' /></td>

                                    <%--<td id="tdteleprompter" runat="server">
                                        <asp:Label ID="mosaepisodeLabel6" Visible="false" runat="server" Text='<%# Eval("PCRTelePropterStatus") %>' /></td>
                                    <td id="tdlayout" runat="server">
                                        <asp:Label ID="mosaepisodeLabel7" Visible="false" runat="server" Text='<%# Eval("PCRPlayoutStatus") %>' /></td>--%>
                                    <%--<td id="tdwindowstatus" runat="server">
                                        <asp:Label ID="mosaepisodeLabel8" Visible="false" runat="server" Text='<%# Eval("PCRFourWindowStatus") %>' /></td>--%>


                                    <td id="tdteleprompterjson" class="tdteleprompterjson" runat="server">                                                                        
                                        <table class="tableteleprom"><tr>
                                        <asp:repeater ID="TelepropJsonRepeater" runat="server" OnItemDataBound="TelepropJsonRepeater_ItemDataBound">
                                        <headertemplate><td></headertemplate>
                                        <ItemTemplate>
                                            <td id="innertd_telejson" runat="server">
                                                <asp:Label ID="innerlbl_roAckMosId" runat="server" Text='<%# Eval("roAckMosId") %>'/><br />
                                                <%--Status:<asp:Label ID="innerlbl_roStatus" runat="server" Text='<%# Eval("roStatus") %>'/>--%>
                                            </td>
                                        </ItemTemplate>
                                        <footertemplate>
                                        
                                        </footertemplate>
                                        </asp:repeater>
                                        </tr></table>
                                        </td>



                                    <td id="tdlayoutjson" class="tdlayoutjson" runat="server">
                                            <table class="fxd"><tr>
                                        <asp:repeater ID="PCRPlayoutJsonRepeater" runat="server" OnItemDataBound="PCRPlayoutJsonRepeater_ItemDataBound">
                                        <headertemplate><td></headertemplate>
                                        <ItemTemplate>
                                            <td id="innnertd_playoutjson" runat="server">
                                                <asp:Label ID="innnerlbl_roAckMosId" runat="server" Text='<%# Eval("roAckMosId") %>'/><br />
                                                <%--Status:<asp:Label ID="innnerlbl_roStatus" runat="server" Text='<%# Eval("roStatus") %>'/>--%>
                                            </td>
                                        </ItemTemplate>
                                        <footertemplate>
                                        
                                        </footertemplate>
                                        </asp:repeater>
                                        </tr></table>                                        
                                    </td>

                                    <%--<td id="tdwindowstatusjson" runat="server">
                                        <asp:Label ID="mosaepisodefourwindowLabel" Visible="false" runat="server" Text='<%# Eval("PCRFourWindowJson") %>' /></td>--%>
                                    <td>
                                        <asp:TextBox TextMode="MultiLine" ID="mosaepisodeLabel9" Enabled="false" runat="server" Text='<%# Eval("StatusDescription") %>' /></td>
                                    <td>
                                        <asp:LinkButton class="sasasss" Font-Bold="true"   runat="server" ID="mosaepisodeBtn" Text="Resend" CommandArgument='<%# Eval("MosActiveEpisodeId") %>' CommandName="updaterow" /></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                    <td class="auto-style2"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
