﻿using NMS.Core;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using NMS.Repository;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NMS.Core.Entities;
using System.Drawing;
using NMS.Core.Enums;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.IO;
using System.Web.Script.Serialization;
using MOSProtocol.Lib.Entities;
using Newtonsoft.Json;
using System.Data;

namespace NMS.Admin
{
    public partial class EpisodeStatus : System.Web.UI.Page
    {
        IMosActiveEpisodeService MostAct = IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
        //IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
        MosActiveEpisodeRepository repo = new MosActiveEpisodeRepository();
        protected void Page_Load(object sender, EventArgs e)
        {
            MosActiveEpisode mosepisode = new MosActiveEpisode();
            if (!IsPostBack)
                BindEpisode();
        }

        private void BindEpisode()
        {
            List<MosActiveEpisode> ep = repo.GetAllMosEpisodeWithProgram().OrderByDescending(x => x.LastUpdateDate).ToList();
            mactepisodeGridView.DataSource = ep;
            mactepisodeGridView.DataBind();
        }

        protected void mactepisodeGridView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            MosActiveEpisode mosobj = new MosActiveEpisode();
            LinkButton mosaepisodeBtn = (LinkButton)e.Item.FindControl("mosaepisodeBtn");
            TextBox StatusDescription = (TextBox)e.Item.FindControl("StatusDescription");
            string empDescr = string.Empty;
            empDescr = Convert.ToString(StatusDescription);
            string StatDescrip = Convert.ToString(empDescr);
            if (e.CommandName == "updaterow")
            {
                int MosActiveEpisodeId = Convert.ToInt32(e.CommandArgument);
                repo.UpdateStatus(2, MosActiveEpisodeId, "", "");
                BindEpisode();
                Response.Write(@"<script language='javascript'>alert('Successfully Updated!');</script>");                
            }
        }
        List<ROAck> listRoack = new List<ROAck>();
        List<ROAck> MPlay = new List<ROAck>();
        ROAck roackk = new ROAck();
        ROAck MosPlayout = new ROAck();
        protected void mactepisodeGridView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {

                int StatusCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "StatusCode").ToString());
                Boolean Teleprompterstatus = DataBinder.Eval(e.Item.DataItem, "PCRTelePropterStatus") == null ? false : Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "PCRTelePropterStatus").ToString());
                Boolean PCRPlayoutStatus = DataBinder.Eval(e.Item.DataItem, "PCRPlayoutStatus") == null ? false : Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "PCRPlayoutStatus").ToString());
                Boolean PCRFourWindowStatus = DataBinder.Eval(e.Item.DataItem, "PCRFourWindowStatus") == null ? false : Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "PCRFourWindowStatus").ToString());

                HtmlTableCell tdteleprompter = (HtmlTableCell)e.Item.FindControl("tdteleprompter");
                HtmlTableCell tdlayout = (HtmlTableCell)e.Item.FindControl("tdlayout");
                HtmlTableCell tdwindowstatus = (HtmlTableCell)e.Item.FindControl("tdwindowstatus");

                HtmlTableCell statuscodtd = (HtmlTableCell)e.Item.FindControl("statuscodtd");
                HtmlTableCell tdteleprompterjson = (HtmlTableCell)e.Item.FindControl("tdteleprompterjson");
                HtmlTableCell tdlayoutjson = (HtmlTableCell)e.Item.FindControl("tdlayoutjson");
                HtmlTableCell tdwindowstatusjson = (HtmlTableCell)e.Item.FindControl("tdwindowstatusjson");

                string MosStatus = Enum.GetName(typeof(MosEpisodeStatus), StatusCode);
                Label LblStatuscode = (Label)e.Item.FindControl("StatusCode");
                LblStatuscode.Text = MosStatus;
                if (MosStatus == MosEpisodeStatus.Updated.ToString())
                {
                    statuscodtd.Style.Add("Background", "#ffff7f");
                    tdteleprompterjson.Style.Add("Background", "#fff");
                    tdlayoutjson.Style.Add("Background", "#fff");
                }
                if (MosStatus == MosEpisodeStatus.SentToPcr.ToString())
                {
                    statuscodtd.Style.Add("Background", "rgb(16, 255, 16)");
                }
                if (MosStatus == MosEpisodeStatus.Error.ToString())
                {
                    statuscodtd.Style.Add("Background", "#ff4c4c");
                }

                var TeleprompterJson = DataBinder.Eval(e.Item.DataItem, "PCRTelePropterJson") == null ? "" : Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PCRTelePropterJson").ToString());
                var PCRPlayoutJson = DataBinder.Eval(e.Item.DataItem, "PCRPlayoutJson") == null ? "" : Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PCRPlayoutJson").ToString());

                Repeater TelepropJsonRepeater = (Repeater)e.Item.FindControl("TelepropJsonRepeater");
                Repeater PCRPlayoutJsonRepeater = (Repeater)e.Item.FindControl("PCRPlayoutJsonRepeater");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                
                roackk = serializer.Deserialize<ROAck>(TeleprompterJson);
                MosPlayout = serializer.Deserialize<ROAck>(PCRPlayoutJson);

                if (roackk != null)
                {
                    TelepropJsonRepeater.DataSource = roackk.roAck;
                    TelepropJsonRepeater.DataBind();                    
                }
              
                if (MosPlayout != null)
                {
                    PCRPlayoutJsonRepeater.DataSource = MosPlayout.roAck;
                    PCRPlayoutJsonRepeater.DataBind();
                }
              
            }
        }

        protected void TelepropJsonRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HtmlTableCell tdteleprompterjson = (HtmlTableCell)e.Item.FindControl("tdteleprompterjson");
                HtmlTableCell innertd_telejson = (HtmlTableCell)e.Item.FindControl("innertd_telejson");
                Label innerlbl_roAckMosId = (Label)e.Item.FindControl("innerlbl_roAckMosId");
                Label innerlbl_roStatus = (Label)e.Item.FindControl("innerlbl_roStatus");


                string MosId = DataBinder.Eval(e.Item.DataItem, "roAckMosId").ToString();
                bool RoStatus = DataBinder.Eval(e.Item.DataItem, "roStatus") == null ? false : true;
                innerlbl_roAckMosId.Text = MosId;

                if (RoStatus == true)
                {
                    innertd_telejson.Style.Add("Background", "rgb(16, 255, 16)");
                }

                if (RoStatus == false)
                {
                    innertd_telejson.Style.Add("Background", "#ff4c4c");
                }                
            }
        }

        protected void PCRPlayoutJsonRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HtmlTableCell tdlayoutjson = (HtmlTableCell)e.Item.FindControl("tdlayoutjson");
                HtmlTableCell innnertd_playoutjson = (HtmlTableCell)e.Item.FindControl("innnertd_playoutjson");
                Label innnerlbl_roAckMosId = (Label)e.Item.FindControl("innnerlbl_roAckMosId");
                Label innnerlbl_roStatus = (Label)e.Item.FindControl("innnerlbl_roStatus");

                string MosId = DataBinder.Eval(e.Item.DataItem, "roAckMosId").ToString();
                bool RoStatus = DataBinder.Eval(e.Item.DataItem, "roStatus") == null ? false : true;
                innnerlbl_roAckMosId.Text = MosId;
                if (RoStatus == true)
                {
                    innnertd_playoutjson.Style.Add("Background", "rgb(16, 255, 16)");
                }

                if (RoStatus == false)
                {
                    innnertd_playoutjson.Style.Add("Background", "#ff4c4c");
                }    
            }
        }
    }
}