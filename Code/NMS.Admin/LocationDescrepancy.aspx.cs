﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NMS.MongoRepository;
using NMS.Service;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.IService;
using NMS.Core;
using NMS.Core.Enums;
using System.Collections;
using System.Web.UI.HtmlControls;

namespace NMS.Admin
{
    public partial class LocationDescrepancy : System.Web.UI.Page
    {

        IDescrepencyNewsFileService objDescrepencyNewsFileService = IoC.Resolve<IDescrepencyNewsFileService>("DescrepencyNewsFileService");
        //paging 4 rpt1-->
        public int NowViewing
        {
            get
            {
                object obj = ViewState["_NowViewing"];
                if (obj == null)
                    return 0;
                else
                    return (int)obj;
            }
            set
            {
                this.ViewState["_NowViewing"] = value;
            }
        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);
            ddlPageSize.SelectedIndexChanged += new EventHandler(ddlPageSize_SelectedIndexChanged);
        }

        void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fill repeater for Pager event
            //FillRepeater(Navigation.Pager);
            FillRepeater(Navigation.First);
        }

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!Page.IsPostBack)
        //        FillRepeater(Navigation.None);
        //}

        protected void lbtnPrev_Click(object sender, EventArgs e)
        {
            //Fill repeater for Previous event
            FillRepeater(Navigation.Previous);
        }

        /// <summary>
        /// Assign PagedDataSource to Repeater
        /// </summary>
        /// <param name="navigation">A Navigation enum to identify current page</param>
        private void FillRepeater(Navigation navigation)
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyByStatus((int)DescrepencyType.Location);

            var group = from obj in lstDescrepancy
                        group obj by obj.DescrepencyValue
                            into summary
                            select new DescrepancySummary { DescrepencyValue = summary.Key, TotalCount = summary.Count() };

            List<DescrepancySummary> lstDescrepancySummary = group.OrderByDescending(x => x.TotalCount).ToList<DescrepancySummary>();

            PagedDataSource pgitemss = new PagedDataSource();
            pgitemss.DataSource = lstDescrepancySummary;

            //Create the object of PagedDataSource
            PagedDataSource objPds = new PagedDataSource();

            //Assign our data source to PagedDataSource object
            objPds.DataSource = pgitemss;

            //Set the allow paging to true
            objPds.AllowPaging = true;

            //Set the number of items you want to show
            objPds.PageSize = int.Parse(ddlPageSize.SelectedValue);

            //Based on navigation manage the NowViewing
            switch (navigation)
            {
                case Navigation.Next:       //Increment NowViewing by 1
                    NowViewing++;
                    break;
                case Navigation.Previous:   //Decrement NowViewing by 1
                    NowViewing--;
                    break;
                case Navigation.Last:       //Make NowViewing to last page for PagedDataSource
                    NowViewing = objPds.PageCount - 1;
                    break;
                case Navigation.Pager:      //Change NowViewing based on pager size and page count
                    if (int.Parse(ddlPageSize.SelectedValue) >= objPds.PageCount)
                        NowViewing = objPds.PageCount - 1;
                    break;
                case Navigation.Sorting:
                    break;
                default:                    //Default NowViewing set to 0
                    NowViewing = 0;
                    break;
            }

            //Set the current page index
            objPds.CurrentPageIndex = NowViewing;

            //Change the text Now viewing text
            lblCurrentPage.Text = "Now viewing : " + (NowViewing + 1).ToString() + " of " + objPds.PageCount.ToString();

            // Disable Prev, Next, First, Last buttons if necessary
            lbtnPrev.Enabled = !objPds.IsFirstPage;
            lbtnNext.Enabled = !objPds.IsLastPage;
            lbtnFirst.Enabled = !objPds.IsFirstPage;
            lbtnLast.Enabled = !objPds.IsLastPage;

            //Assign PagedDataSource to repeater
            rptSummary.DataSource = objPds;
            rptSummary.DataBind();

        }

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            //Fill repeater for Next event
            FillRepeater(Navigation.Next);
        }

        protected void lbtnFirst_Click(object sender, EventArgs e)
        {
            //Fill repeater for First event
            FillRepeater(Navigation.First);
        }

        protected void lbtnLast_Click(object sender, EventArgs e)
        {
            //Fill repeater for Last event
            FillRepeater(Navigation.Last);
        }
        protected void rptUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ViewState["SortExpression"] = e.CommandName;
            FillRepeater(Navigation.Sorting);
        }
        //paging 4 rpt1--x




        List<Location> lstLocation;
        protected void Page_Load(object sender, EventArgs e)
        {
            BindLocations();
            if (!IsPostBack)
            {

                //BindCategoryDescrepancy();
                //BindDescrepancySummary();
                //BindLocationAlias();
                BindHeaderLocation();
                
            }
            if (!Page.IsPostBack)
                FillRepeater(Navigation.None);
        }

        private void BindHeaderLocation()
        {
            ddlCategoryNew.DataSource = lstLocation;
            ddlCategoryNew.DataTextField = "Location";
            ddlCategoryNew.DataValueField = "LocationId";
            ddlCategoryNew.DataBind();

            ListItem selectedListItem = ddlCategoryNew.Items.FindByValue("-1");
            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }
            ddlCategoryNew.SelectedValue = "-1";
        }

        private void BindCategoryDescrepancy()
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            //List<NMS.Core.Entities.Mongo.MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyNews(DescrepencyType.Category);
            //List<MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyNews(DescrepencyType.Location);
            List<MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyByStatus((int)DescrepencyType.Location);

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = lstDescrepancy;
            pgitems.AllowPaging = true;

            //Control page size from here 
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            //if (pgitems.PageCount > 1)
            //{
            //    rptPaging.Visible = true;
            //    ArrayList pages = new ArrayList();
            //    for (int i = 0; i <= pgitems.PageCount - 1; i++)
            //    {
            //        pages.Add((i + 1).ToString());
            //    }
            //    rptPaging.DataSource = pages;
            //    rptPaging.DataBind();
            //}
            //else
            //{
            //    rptPaging.Visible = false;
            //}

            BindLocations();            
            rptDiscrepancy.DataSource = pgitems;
            rptDiscrepancy.DataBind();

           // BindDescrepancySummary(lstDescrepancy);

        }

        public void BindDescrepancySummary()
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyByStatus((int)DescrepencyType.Location);

            var group = from obj in lstDescrepancy
                        group obj by obj.DescrepencyValue
                            into summary
                            select new DescrepancySummary { DescrepencyValue = summary.Key, TotalCount = summary.Count() };
            
            List<DescrepancySummary> lstDescrepancySummary = group.OrderByDescending(x=>x.TotalCount).ToList<DescrepancySummary>();            
            PagedDataSource pgitemss = new PagedDataSource();
            pgitemss.DataSource = lstDescrepancySummary;            
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["PageNumber"] = value; }
        }
        protected void rptDiscrepancy_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                //HiddenField hfDiscValue = (HiddenField)e.Item.FindControl("hfDiscValue");
                //HtmlAnchor hlnkDescrepencyValue = (HtmlAnchor)e.Item.FindControl("hlnkDescrepencyValue");
                //hlnkDescrepencyValue.HRef += "?id=" + 1;
                //Label descrepencyNewsFileID = (Label)e.Item.FindControl("descrepencyNewsFileID");
                //descrepencyNewsFileID.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DescrepencyValue"));

                ((Label)e.Item.FindControl("descrepencyNewsFileID")).Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DescrepencyValue"));

                ((Label)e.Item.FindControl("lblSno")).Text = Convert.ToString(e.Item.ItemIndex + 1);
                HtmlAnchor hlnkDescrepencyValue = (HtmlAnchor)e.Item.FindControl("hlnkDescrepencyValue");
                hlnkDescrepencyValue.HRef = "DescripencyDetail.aspx?Type=1&Discrepancyvalue=" + DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();

                Label lblSNO = (Label)e.Item.FindControl("lblSNO");
                lblSNO.Text = Convert.ToString(e.Item.ItemIndex + 1);

                DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
                ddlCategory.DataSource = lstLocation;
                ddlCategory.DataTextField = "Location";
                ddlCategory.DataValueField = "LocationId";
                ddlCategory.DataBind();

                Button btnSubmitNew = (Button)e.Item.FindControl("btnSubmitNew");
                Button btnSubmitExisting = (Button)e.Item.FindControl("btnSubmitExisting");

                TextBox txtNewName = (TextBox)e.Item.FindControl("txtNewName");
                TextBox txtAliasOrChildName = (TextBox)e.Item.FindControl("txtAliasOrChildName");
                txtAliasOrChildName.Text = DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();
                btnSubmitNew.CommandArgument = DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();

                btnSubmitExisting.CommandArgument = DataBinder.Eval(e.Item.DataItem, "DescrepencyValue").ToString();

                RequiredFieldValidator rvtxtNewName = (RequiredFieldValidator)e.Item.FindControl("rvtxtNewName");
                RequiredFieldValidator rvtxtAliasOrChildName = (RequiredFieldValidator)e.Item.FindControl("rvtxtAliasOrChildName");

                //rvtxtNewName.ControlToValidate = txtNewName.ClientID;
                //rvtxtAliasOrChildName.ControlToValidate = txtAliasOrChildName.ClientID;
            }
        }

        protected void rptPaging_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindCategoryDescrepancy();
        }
        protected void rptPaging_ItemCommandss(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindDescrepancySummary();
        }

        private void BindLocations()
        {
            ILocationService objLocationService = IoC.Resolve<ILocationService>("LocationService");
            lstLocation = objLocationService.GetAllLocation().OrderBy(x => x.Location).ToList();
            lstLocation.Insert(0, new Location { Location = "--select--", LocationId = -1 });
        }

        protected void rptDiscrepancy_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string Id = ((Label)e.Item.FindControl("descrepencyNewsFileID")).Text;
            if (e.CommandName.ToString() == "new")
            {
                TextBox txtNewName = (TextBox)e.Item.FindControl("txtNewName");
                AddNewLocation(txtNewName.Text, e.CommandArgument.ToString(), 0, Id);
                GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());


                //Response.Write("add new name as -->"+ txtNewName.Text);
                // BindCategoryDescrepancy();
                BindLocations();
                FillRepeater(Navigation.None);

            }

            else if (e.CommandName.ToString() == "existing")
            {
                TextBox txtAliasOrChildName = (TextBox)e.Item.FindControl("txtAliasOrChildName");
                RadioButton rdoAlias = (RadioButton)e.Item.FindControl("rdoAlias");
                RadioButton rdoNewChild = (RadioButton)e.Item.FindControl("rdoNewChild");
                DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");

                if (ddlCategory.SelectedValue != "-1")
                {
                    if (rdoAlias.Checked && ddlCategory.SelectedValue != "-1")
                    {
                        //Response.Write("Alias name as -->" + txtAliasOrChildName.Text);
                        //AddNewLocationAlias(1, txtAliasOrChildName.Text.ToString());
                        AddNewLocationAlias(Convert.ToInt32(ddlCategory.SelectedValue.ToString()), txtAliasOrChildName.Text);
                        UpdateDescrepencyLocation(Id, Convert.ToInt32(ddlCategory.SelectedValue.ToString()));
                        GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
                        //BindCategoryDescrepancy();
                    }
                    else if (rdoNewChild.Checked && ddlCategory.SelectedValue != "-1")
                    {
                        AddNewLocation(txtAliasOrChildName.Text, e.CommandArgument.ToString(), Convert.ToInt32(ddlCategory.SelectedValue), Id);
                        GetAllDescrepancyNewsByDescrepancyValue(e.CommandArgument.ToString());
                        //BindCategoryDescrepancy();
                    }
                    BindLocations();
                    FillRepeater(Navigation.None);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert('please select location from the list first');", true);
                }
            }
        }


        private void AddNewLocation(string locationName, string locationAliasName, int parentId, string Id)
        {
            ILocationService objLocationService = IoC.Resolve<ILocationService>("LocationService");
            Location objLocation = new Location { Location = locationName, CreationDate = DateTime.UtcNow, IsApproved = true };
            objLocation.LastUpdateDate = objLocation.CreationDate;

            if (parentId != 0)
                objLocation.ParentId = parentId;

            Location objNewlyAddedLocation = objLocationService.InsertLocationIfNotExists(objLocation);

            AddNewLocationAlias(objNewlyAddedLocation.LocationId, objNewlyAddedLocation.Location);

            if (locationName.ToLower() != locationAliasName.ToLower() && locationAliasName != string.Empty)
                AddNewLocationAlias(objNewlyAddedLocation.LocationId, locationAliasName);

            UpdateDescrepencyLocation(Id, objNewlyAddedLocation.LocationId);
        }
        private void UpdateDescrepencyLocation(string DescrepencyValue, int locationId)
        {
            List<int> DescrepencyIds = objDescrepencyNewsFileService.GetDescrepencyNewsFileIdByDescrepencyValue(DescrepencyValue);

            for (int i = 0; i < DescrepencyIds.Count(); i++)
            {
                objDescrepencyNewsFileService.UpdateDescrepencyCategory(DescrepencyIds[i].ToString(), locationId);
            }
        }
        private void AddNewLocationAlias(int locationId, string AliasName)
        {

            ILocationAliasService objLocationAliasService = IoC.Resolve<ILocationAliasService>("LocationAliasService");
            LocationAlias objLocationAlias = new LocationAlias { Alias = AliasName, LocationId = locationId, CreationDate = DateTime.UtcNow, IsActive = true };
            objLocationAlias.LastUpdateDate = objLocationAlias.CreationDate;
            objLocationAliasService.InsertLocationAliasIfNotExists(objLocationAlias);
        }

        private void GetAllDescrepancyNewsByDescrepancyValue(string descrepancyValue)
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            //List<MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyNews(DescrepencyType.Location, descrepancyValue);
            objNewsService.UpdateDescrepancyNewsByValue((int)DescrepencyType.Location, descrepancyValue);
        
        }

        private void BindLocationAlias()
        { /*
            ILocationAliasService objLocationAliasService = IoC.Resolve<ILocationAliasService>("LocationAliasService");
            List<LocationAlias> lstLocationAlias = objLocationAliasService.GetAllLocationAlias();
           
                      foreach (LocationAlias alias in lstLocationAlias)
                      {
                          Response.Write(alias.Alias + "<Br>");
                      }
            
          
                      var group = from obj in lstLocationAlias
                                  group obj by obj.Alias
                                      into summary
                                      select new DescrepancySummary { DescrepencyValue = summary.Key, TotalCount = summary.Count() };


                      List<DescrepancySummary> lstDescrepancySummary = group.ToList<DescrepancySummary>();
                      rptSummary.DataSource = lstDescrepancySummary;
                      rptSummary.DataBind();
                       */ 
            
        }

        protected void btnSubmitNewNew_Click(object sender, EventArgs e)
        {
            string Val = txtNewNameNew.Text;
            if (!string.IsNullOrEmpty(Val))
            {
                AddNewLocationIndependent(Val, 0);
                dlActionNew.SelectedValue = "-1";
                FillRepeater(Navigation.None);
            }
        }

        protected void btnSubmitExistingNew_Click(object sender, EventArgs e)
        {
            string Val=   txtAliasOrChildNameNew.Text;
            if (ddlCategoryNew.SelectedValue != "-1")
            {
                if (rdoAliasNew.Checked)
                {
                    AddNewLocationAlias(Convert.ToInt32(ddlCategoryNew.SelectedValue), Val);
                }
                else { AddNewLocationIndependent(Val, Convert.ToInt32(ddlCategoryNew.SelectedValue)); }
                dlActionNew.SelectedValue = "-1";
                FillRepeater(Navigation.None);
            }

        }

        private void AddNewLocationIndependent(string locationName, int parentId)
        {
            ILocationService objLocationService = IoC.Resolve<ILocationService>("LocationService");
            Location objLocation = new Location { Location = locationName, CreationDate = DateTime.UtcNow, IsApproved = true , IsActive = true };
            objLocation.LastUpdateDate = objLocation.CreationDate;

            if (parentId != 0)
                objLocation.ParentId = parentId;

            Location objNewlyAddedLocation = objLocationService.InsertLocationIfNotExists(objLocation);
            AddNewLocationAlias(objNewlyAddedLocation.LocationId, objNewlyAddedLocation.Location);


        }

    }
    public enum Navigation
    {
        None,
        First,
        Next,
        Previous,
        Last,
        Pager,
        Sorting
    }

    public class DescrepancySummary
    {
        public string DescrepencyValue { get; set; }
        public int TotalCount { get; set; }       
    }


}
