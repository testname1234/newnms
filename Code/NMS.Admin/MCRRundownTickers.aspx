﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MCRRundownTickers.aspx.cs" Inherits="NMS.Admin.MCRRundownTickers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: auto;
        }

        .auto-style2 {
            height: 59px;
        }

        .auto-style3 {
            font-size: xx-large;
        }
    </style>
    <style type="text/css">
        body {
            font-family:Calibri;
            font-weight:600;
            background:#EAE6E6;
        }

        .ssscustom {
            /*background: #666b66;*/
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
        }

        .sssme {
            /*background: #b6b8b6;*/
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
        }

        .sasasss {
            text-decoration: none !important;
            color: blue;
            font-family: Calibri;
        }

        .trcntr {
            text-align: center;            
        }
        .trcnttr {
            background:#B8B3B3;
        }
        .captn {            
            font-family: calibri;
            font-weight: bolder;
            color: #393c39;
        }
        .whiteclr {
            background:#ffffff;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1 ssscustom">
                <caption class="auto-style3 sssme">
                    <strong>Breaking Rundown</strong></caption>
                <tr>
                    <td class="auto-style2">
                        <asp:HiddenField ID="mae_hdn" runat="server" />
                        <asp:Repeater ID="rpt_Breaking" runat="server" >
                            <HeaderTemplate>
                                <table border="1" cellpadding="2" align="center">
                                    <tr class="trcntr trcnttr">
                                        <th>Ticker Id</th>
                                        <th>SequenceNumber</th>
                                        <th>LanguageCode</th>
                                        <th>CreationDate</th>
                                        <th>TickerLineId</th>
                                        <th>Text</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="trcntr whiteclr" runat="server">
                                    <td><asp:Label ID="lblBTickerId" runat="server" Text='<%# Eval("TickerId") %>' /></td>
                                    <td><asp:Label ID="lblBSequenceNumber" runat="server" Text='<%# Eval("SequenceNumber") %>' /></td>
                                    <td><asp:Label ID="lblBLanguageCode" runat="server" Text='<%# Eval("LanguageCode") %>' /></td>
                                    <td><asp:Label ID="lblBCreationDate" runat="server" Text='<%# Eval("CreationDate") %>' /></td>
                                    <td><asp:Label ID="lblBTickerLineId" runat="server" Text='<%# Eval("TickerLineId") %>' /></td>
                                    <td style="text-align: right;direction: rtl;" title='<%# Eval("Text") %>'><asp:Label ID="lblBText" runat="server" Text='<%# Eval("Text") %>' /></td>
                                 </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                
            </table>
        </div>
        <div>
            <table class="auto-style1 ssscustom">
                <caption class="auto-style3 sssme">
                    <strong>Alert Rundown</strong></caption>
                <tr>
                    <td class="auto-style2">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:Repeater ID="rpt_Alert" runat="server" >
                            <HeaderTemplate>
                                <table border="1" cellpadding="2" align="center">
                                    <tr class="trcntr trcnttr">
                                        <th>Ticker Id</th>
                                        <th>SequenceNumber</th>
                                        <th>LanguageCode</th>
                                        <th>CreationDate</th>
                                        <th>TickerLineId</th>
                                        <th>Text</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="Tr1" class="trcntr whiteclr" runat="server">
                                    <td><asp:Label ID="lblATickerId" runat="server" Text='<%# Eval("TickerId") %>' /></td>
                                    <td><asp:Label ID="lblASequenceNumber" runat="server" Text='<%# Eval("SequenceNumber") %>' /></td>
                                    <td><asp:Label ID="lblALanguageCode" runat="server" Text='<%# Eval("LanguageCode") %>' /></td>
                                    <td><asp:Label ID="lblACreationDate" runat="server" Text='<%# Eval("CreationDate") %>' /></td>
                                    <td><asp:Label ID="lblATickerLineId" runat="server" Text='<%# Eval("TickerLineId") %>' /></td>
                                    <td style="text-align: right;direction: rtl;" title='<%# Eval("Text") %>'><asp:Label ID="lblAText" runat="server" Text='<%# Eval("Text") %>' /></td>
                                 </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                    
                </tr>
            </table>
        </div>
        <div>
            <table class="auto-style1 ssscustom">
                <caption class="auto-style3 sssme">
                    <strong>Category Rundown</strong></caption>
                <tr>
                    <td class="auto-style2">
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                        <asp:Repeater ID="rpt_Category" runat="server" >
                            <HeaderTemplate>
                                <table border="1" cellpadding="2" align="center">
                                    <tr class="trcntr trcnttr">
                                        <th>Ticker Id</th>
                                        <th>Category Name</th>
                                        <th>Category Id</th>
                                        <th>SequenceNumber</th>
                                        <th>LanguageCode</th>
                                        <th>CreationDate</th>
                                        <th>TickerLineId</th>
                                        <th>Text</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="Tr2" class="trcntr whiteclr" runat="server">
                                    <td><asp:Label ID="lblCTickerId" runat="server" Text='<%# Eval("TickerId") %>' /></td>
                                    <td><asp:Label ID="lblCCategoryName" runat="server" Text='<%# Eval("CategoryName") %>' /></td>
                                    <td><asp:Label ID="lblCCategoryId" runat="server" Text='<%# Eval("CategoryId") %>' /></td>
                                    <td><asp:Label ID="lblCSequenceNumber" runat="server" Text='<%# Eval("SequenceNumber") %>' /></td>
                                    <td><asp:Label ID="lblCLanguageCode" runat="server" Text='<%# Eval("LanguageCode") %>' /></td>
                                    <td><asp:Label ID="lblCCreationDate" runat="server" Text='<%# Eval("CreationDate") %>' /></td>
                                    <td><asp:Label ID="lblCTickerLineId" runat="server" Text='<%# Eval("TickerLineId") %>' /></td>
                                    <td style="text-align: right;direction: rtl;" title='<%# Eval("Text") %>'><asp:Label ID="lblCText" runat="server" Text='<%# Eval("Text") %>' /></td>
                                 </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                 
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
