﻿using NMS.Core;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using NMS.Repository;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NMS.Core.Entities;
using System.Drawing;
using NMS.Core.Enums;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.IO;
using System.Web.Script.Serialization;
using MOSProtocol.Lib.Entities;
using Newtonsoft.Json;
using System.Data;

namespace NMS.Admin
{
    public partial class MCRRundownTickers : System.Web.UI.Page
    {
        IMcrBreakingTickerRundownService mcrBreakingService = IoC.Resolve<IMcrBreakingTickerRundownService>("McrBreakingTickerRundownService");
        IMcrLatestTickerRundownService mcrLatestService = IoC.Resolve<IMcrLatestTickerRundownService>("McrLatestTickerRundownService");
        IMcrCategoryTickerRundownService mcrCategoryService = IoC.Resolve<IMcrCategoryTickerRundownService>("McrCategoryTickerRundownService");


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindBreakingTickers();
                BindLatestTickers();
                BindCategoryTickers();
            }
        }

        private void BindBreakingTickers()
        {
            List<McrBreakingTickerRundown>  list = mcrBreakingService.GetAllMcrBreakingTickerRundown().ToList();
            rpt_Breaking.DataSource = list;
            rpt_Breaking.DataBind();
        }
        private void BindLatestTickers()
        {
            List<McrLatestTickerRundown> list = mcrLatestService.GetAllMcrLatestTickerRundown().ToList();
            rpt_Alert.DataSource = list;
            rpt_Alert.DataBind();
        }
        private void BindCategoryTickers()
        {
            List<McrCategoryTickerRundown> list = mcrCategoryService.GetAllMcrCategoryTickerRundown().ToList();
            rpt_Category.DataSource = list;
            rpt_Category.DataBind();
        }
    }
}