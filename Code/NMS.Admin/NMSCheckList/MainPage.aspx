﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="NMS.Admin.NMSCheckList.MainPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            /*background:rgb(237, 237, 237);*/
        }

        .trheadercolor {
            background: rgb(170, 213, 170);
        }

        .tblealign {
            margin: 20px 0 0 250px;
            float: left;
            -webkit-border-radius: 5px 5px 5px 5px;
            -moz-border-radius: 5px 5px 5px 5px;
            border-radius: 5px 5px 5px 5px;
            border-color: aliceblue;
        }

        .heading {
            /*margin:0 0 0 520px;
            float:left;*/
            margin: 0 0 0 550px;
            float: left;
            font-size: 36px;
            font-family: sans-serif;
            color: darkgreen;
            /*color: rgb(93, 98, 93);*/
            text-decoration: underline;
        }

        .bdstyle {
            background: white;
            color: rgb(79, 88, 92);
        }

        .thclr {
            color: darkgreen;
        }

        .sasasss {
            text-decoration: none !important;
            color: blue;
            font-family: Calibri;
        }

            .sasasss:hover {
                color: red;
            }
        /*#loader {
            display:none;
        }*/
    </style>


    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="../scripts/jquery-1.8.2.min.js"></script>
    <%--<script type="text/javascript">
        function showimage() {
            $('#loader').show()
        }
        
    </script>--%>
</head>
<body> 
        <%--<img id="loader" src="ajax-loader.gif" />--%>
    <form id="form1" runat="server">


        <div>
            <h1 class="heading">CHECKLISTS</h1>
            <asp:Repeater ID="MainPageGdView1" runat="server" OnItemCommand="mactepisodeGridView_ItemCommand" OnItemDataBound="mactepisodeGridView_ItemDataBound">
                <HeaderTemplate>
                    <table id="tbl1" class="tblealign tablesorter" border="1" cellpadding="2">
                        <thead>
                            <tr class="trheadercolor">
                                <th class="thclr">ChecklistID</th>
                                <th class="thclr">Name</th>
                                <th class="thclr">Status</th>
                                <th class="thclr">Error</th>
                                <th class="thclr">CreationDate</th>
                                <th class="thclr">Marked Bit</th>
                                <th class="thclr">Check Source</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="trstatus" class="bdstyle" runat="server">
                        <td><%--<asp:Label ID="mpageLabel1" runat="server" Text='<%# Eval("ChecklistID") %>' />--%>
                            <asp:Label ID="lblSNO" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="programlabel" runat="server" Text='<%# Eval("ChecklistName") %>' /></td>
                        <td id="tdstatus" runat="server">
                            <asp:Label ID="statuslabel" runat="server" Text='' /><%--<%# Eval("Status") %>--%></td>
                        <td>
                            <asp:TextBox TextMode="MultiLine" Enabled="false" ID="errortbox" runat="server" Text='<%# Eval("ErrorMessage") %>' /></td>
                        <td>
                            <asp:Label ID="mosaepisodeLabel4" runat="server" Text='<%# Eval("CreationDate") %>' /></td>
                        <td>
                            <asp:Label ID="priorityLabel" runat="server" Text='<%# Eval("Priority") %>' /></td>
                        <td>
                            <asp:LinkButton class="sasasss" runat="server" ID="updaterow" Text="Update Row" CommandArgument='<%# Eval("ChecklistID") %>' CommandName="updaterow" /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                                </table>
                </FooterTemplate>
            </asp:Repeater>

        </div>








<%--        <div id="Progress_Update_Animation" >
	<asp:Panel runat="server" ID="Panel_ProgressUpdate" >
		<table width="250px" style="padding: 8px;" class="panel_gray_white round_corners_all shadow_white_gray">
			<tr>
				<td>
					<table cellpadding="2px">
						<tr>
							<td align="left" valign="middle" style="width: 41px;" >
								<img alt="" src="<%= Page.ResolveUrl("~")%>ajax-loader.gif" class="center" />
							</td>
							<td align="left" valign="middle">
								<asp:Label ID="Label1" runat="server" Text="Processing..." ForeColor="White" Font-Names="Calibri" Font-Size="X-Large" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td class="line_gradient"></td>
						</tr>
					</table>
				</td>
				
			</tr>
			<tr>
				<td>
					<table cellpadding="4px">
						<tr>
							<td align="left" valign="middle" style="width: 41px;" >
								<img alt="" src="<%= Page.ResolveUrl("~")%>etc/img/loader/spinner.gif" class="center" />
							</td>
							<td align="left" valign="middle">
								<asp:Label ID="Label2" runat="server" Text="Please wait" ForeColor="#B9B9B9" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</asp:Panel>

	<asp:ModalPopupExtender ID="ModalPopupExtender_ProgressUpdate" runat="server"
		DynamicServicePath="" Enabled="True"
		PopupControlID="Panel_ProgressUpdate" 
		TargetControlID="Panel_ProgressUpdate" 
		BackgroundCssClass="modalPopUpBackground">
	</asp:ModalPopupExtender>

	<script language="javascript" type="text/javascript">
	    Sys.Net.WebRequestManager.add_invokingRequest(Loadpopup);
	    Sys.Net.WebRequestManager.add_completedRequest(unLoadpopup);
	    function Loadpopup(sender, args) {
	        $find('<%=ModalPopupExtender_ProgressUpdate.ClientID %>').show();
		}
		function unLoadpopup(sender, args) {
		    $find('<%=ModalPopupExtender_ProgressUpdate.ClientID %>').hide();
		}
		function pageUnload() {
		    Sys.Net.WebRequestManager.remove_invokingRequest(Loadpopup);
		    Sys.Net.WebRequestManager.remove_completedRequest(unLoadpopup);
		}
	</script>
</div>--%>
    </form>
</body>
</html>
