﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using HtmlAgilityPack;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Configuration;
using NMS.Repository;
using NMS.Core.Entities;

namespace NMS.Admin.NMSCheckList
{
    public partial class MainPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                CheckListRepository repo = new CheckListRepository();
                List<CheckList> CheckListPriority = repo.GetPriority();
                if (CheckListPriority != null)
                {
                    MainPageGdView1.DataSource = CheckListPriority.OrderBy(x => x.Status).ToList();
                    MainPageGdView1.DataBind();
                }
            }
        }


        protected void mactepisodeGridView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label programlabel = (Label)e.Item.FindControl("programlabel");
            Label statuslabel = (Label)e.Item.FindControl("statuslabel");
            HtmlTableCell tdstatus = (HtmlTableCell)e.Item.FindControl("tdstatus");
            Label priorityLabel = (Label)e.Item.FindControl("priorityLabel");
            CheckListRepository repo = new CheckListRepository();

            if (e.CommandName == "updaterow")
            {
                if (priorityLabel.Text == Convert.ToString(true))
                {
                    repo.UpdatePriority(programlabel.Text, false);
                }
                else
                {
                    repo.UpdatePriority(programlabel.Text, true);
                }

                List<CheckList> checklistpriority = repo.GetPriority();                
                MainPageGdView1.DataSource = checklistpriority.OrderBy(x => x.Status).ToList();
                MainPageGdView1.DataBind();
            }
        }
        public void mactepisodeGridView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblSNO = (Label)e.Item.FindControl("lblSNO");
                lblSNO.Text = Convert.ToString(e.Item.ItemIndex + 1);
                TextBox errortbox = (TextBox)e.Item.FindControl("errortbox");
                Boolean Status = DataBinder.Eval(e.Item.DataItem, "Status") == null ? false : Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Status").ToString());
                Boolean Priority = DataBinder.Eval(e.Item.DataItem, "Priority") == null ? false : Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "Priority").ToString());
                Label statuslabel = (Label)e.Item.FindControl("statuslabel");
                HtmlTableCell tdstatus = (HtmlTableCell)e.Item.FindControl("tdstatus");

                if (Priority == true)
                {
                    tdstatus.Style.Add("Background", "rgb(16, 255, 16)");
                    Status = true;
                }
                else
                {
                    if (Status == true)
                    {
                        tdstatus.Style.Add("Background", "rgb(16, 255, 16)");
                    }
                    else
                    {
                        tdstatus.Style.Add("Background", "#E71313");

                    }
                }
            }
        }
    }
}