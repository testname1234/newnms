﻿//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Threading;
//using System.Web;

//namespace NMS.Admin.NMSCheckList
//{
//    public class ScrapMaxDateConnection
//    {
//        public static string connectionstring
//        {
//            get
//            {
//                return ConfigurationManager.ConnectionStrings["scrapmaxdatetble"].ToString();
//            }
//        }
//        public static DataTable GetResult()
//        {
//            DataTable table = new DataTable();

//            using (SqlConnection con = new SqlConnection(connectionstring))
//            {
//                try
//                {
//                    SqlCommand cmd = new SqlCommand();
//                    cmd.Connection = con;
//                    cmd.CommandType = CommandType.Text;
//                    cmd.CommandText = @"select Source,sourcedate, CASE when datediff(hh,sourcedate,GETUTCDATE()) >= 4  
//                    THEN 'false' ELSE 'true'  END as Status from
//                    (select Source, max(LastUpdateDate) sourcedate from news (nolock) where isnull(ReporterId,0) = 0
//                    group by Source)t where Source not in ('karachi','Twitter') order by status";
//                    con.Open();
//                    SqlDataAdapter da = new SqlDataAdapter(cmd);
//                    da.Fill(table);
//                    return table;                    
//                }
//                catch (Exception ex)
//                {
//                    throw ex;
//                }
//                finally
//                {
//                    if (con.State == ConnectionState.Open)
//                        con.Close();
//                }
//            }
//        }

//        public int Selection( string Name)
//        {
//            using (SqlConnection con = new SqlConnection(connectionstring))
//            {
//                int result;
//                    try
//                    {
//                        SqlCommand cmd1 = new SqlCommand();
//                        cmd1.Connection = con;
//                        cmd1.CommandType = CommandType.Text;
//                        cmd1.CommandText = @"IF EXISTS (SELECT TOP 1 1 FROM CheckList WHERE ChecklistName = @Name)
//BEGIN
//SELECT 1
//END
//ELSE
//BEGIN
//SELECT 0
//end";
//                        cmd1.Parameters.AddWithValue("@Name", Name);                                                
//                        con.Open();
//                        result =  Convert.ToInt32(cmd1.ExecuteScalar());
//                        con.Close();
//                        return result;
//                    }
//                    catch (Exception ex)
//                    {
//                        throw ex;
//                    }
//                    finally
//                    {
//                    }
//            }
//        }

//        public int InsertResult(string Name, bool Status, string ErrorMessage, DateTime CreationDate, bool Priority, int resulttest)
//        {
//            using (SqlConnection con = new SqlConnection(connectionstring))
//            {
//                try
//                {
//                    SqlCommand cmd1 = new SqlCommand();
//                    cmd1.Connection = con;
//                    cmd1.CommandType = CommandType.Text;                    
//                    cmd1.CommandText = @"Insert into CheckList (ChecklistName,Status,ErrorMessage,CreationDate,Priority) 
//                    Values (@Name,@Status,@ErrorMessage,@CreationDate,@Priority)";                    
                    
//                    SqlParameter NameParam = cmd1.Parameters.AddWithValue("@Name", Name);
//                    if (Name == null)
//                    {
//                        NameParam.Value = DBNull.Value;
//                    }
//                    SqlParameter StatusParam = cmd1.Parameters.AddWithValue("@Status", Status);
//                    if (Status == null)
//                    {
//                        StatusParam.Value = DBNull.Value;
//                    }
//                    SqlParameter ErrorMessageParam = cmd1.Parameters.AddWithValue("@ErrorMessage", ErrorMessage);
//                    if (ErrorMessage == null)
//                    {
//                        ErrorMessageParam.Value = DBNull.Value;
//                    }
//                    SqlParameter CreationDateParam = cmd1.Parameters.AddWithValue("@CreationDate", CreationDate);
//                    if (CreationDate == null)
//                    {
//                        CreationDateParam.Value = DBNull.Value;
//                    }
//                    SqlParameter PriorityParam = cmd1.Parameters.AddWithValue("@Priority", Priority);
//                    if (Priority == null)
//                    {
//                        PriorityParam.Value = DBNull.Value;
//                    }
//                    con.Open();
//                    cmd1.ExecuteNonQuery();
//                    con.Close();
//                    return resulttest;
//                }
//                catch (Exception ex)
//                {
//                    throw ex;
//                }
//                finally
//                {                    
//                }
//            }
//        }

//        public void UpdateResult(string Name, bool Status, string ErrorMessage, DateTime CreationDate)
//        {
//            using (SqlConnection con = new SqlConnection(connectionstring))
//            {
//                {
//                    try
//                    {
//                        SqlCommand cmd1 = new SqlCommand();
//                        cmd1.Connection = con;
//                        cmd1.CommandType = CommandType.Text;
//                        cmd1.CommandText = "update CheckList set ChecklistName=@Name,Status=@Status,ErrorMessage=@ErrorMessage,CreationDate=@CreationDate where ChecklistName=@Name";
//                        cmd1.Parameters.AddWithValue("@Name", Name);
//                        cmd1.Parameters.AddWithValue("@Status", Status);
//                        SqlParameter ErrorMessageParam = cmd1.Parameters.AddWithValue("@ErrorMessage", ErrorMessage);
//                        if (ErrorMessage == null)
//                        {
//                            ErrorMessageParam.Value = DBNull.Value;
//                        }
//                        cmd1.Parameters.AddWithValue("@CreationDate", CreationDate);
//                        //cmd1.Parameters.AddWithValue("@Priority", Priority);
//                        con.Open();
//                        cmd1.ExecuteNonQuery();
//                        con.Close();
                        
//                    }
//                    catch (Exception ex)
//                    {
//                        throw ex;
//                    }
//                    finally
//                    {
//                    }
//                }
//            }
//        }
//    }
//}