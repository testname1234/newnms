﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsMonitoring.aspx.cs" Inherits="NMS.Admin.NewsMonitoring" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%";>
            <table align="center" width="50%">
                <tr>
                    <td width="60%" align="center" >

                        <asp:Repeater ID="rptSummary" runat="server" OnItemDataBound="rptSummary_ItemDatabound">
                            <HeaderTemplate>
                                <table width="70%" border="1" style="text-align: center">
                                    <tr>
                                        <td colspan="5" align="center"><strong style="font-size: large">News Monitoring Summary</strong> </td>
                                    </tr>
                                    <tr>
                                        <td width="10%" align="center"><strong>SNo.</strong>
                                        </td>
                                        <td width="20%" align="center"><strong>News Source</strong>
                                        </td>
                                        <td width="20%" align="center"><strong>Last News date</strong>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="trRecord" runat="server">
                                    <td width="5%">
                                        <%#Eval("Container.ItemIndex" + 1) %>
                                    </td>
                                    <td width="10%">
                                        <%#Eval("creationdate") %>
                                    </td>
                                    <td width="10%" align="center">
                                        <asp:Label ID="lblLastNewsDate" runat="server" Text='<%#Eval("lastupdateddate") %>'></asp:Label>
                                     </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>


                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
