﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.IService;
using System.Xml.Linq;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NMS.Core.Enums;

namespace NMS.Admin
{

    public partial class NewsMonitoring : System.Web.UI.Page
    {
        public string connectionstring
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["myconn"].ToString();
            }
        }
        public DataTable GetResult()
        {
            DataTable table = new DataTable();

            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "select_newscategory";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(table);
                    rptSummary.DataSource = table;
                    rptSummary.DataBind();
                    con.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                }
                return table;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if()
            //{
                
            //}
            //else
            //{
            //}
            GetResult();
        }

        private void BindNewsUpdate()
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<NMS.Core.Entities.Mongo.MDescrepencyNews> lstDescrepancy = objNewsService.GetAllDescrepancyNews(DescrepencyType.Category);
            List<News> News = objNewsService.GetDisitnctNewsSource();
            rptSummary.DataSource = News;
            rptSummary.DataBind();
        }
        protected void rptSummary_ItemDatabound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item)
            {
                Label lblLastNewsDate = (Label)e.Item.FindControl("lblLastNewsDate");
                HtmlTableRow trRecord = (HtmlTableRow)e.Item.FindControl("trRecord");
                //string lastnewdate = DataBinder.Eval(e.Item.DataItem, "lastupdateddate").ToString();

                string cusrrentDate = DateTime.Now.ToString();

                int days = Convert.ToInt32((Convert.ToDateTime(lblLastNewsDate.Text) - DateTime.Now).TotalDays);
                if (days >= 1)
                {
                    trRecord.BgColor = System.Drawing.Color.Red.ToString();
                }
                else
                {
                    trRecord.BgColor = System.Drawing.Color.Green.ToString();
                }
            }

        }

        private void LatestUpdate()
        {

        }

    }
}