﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VideoCutterStatus.aspx.cs" Inherits="NMS.Admin.VideoCutterStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 319px;
        }

        .auto-style2 {
            height: 59px;
        }

        .auto-style3 {
            font-size: xx-large;
        }
    </style>
    <style type="text/css">
        body {
            font-family:Calibri;
            font-weight:600;
            background:#EAE6E6;
        }

        .ssscustom {
            /*background: #666b66;*/
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
        }

        .sssme {
            /*background: #b6b8b6;*/
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
        }

        .sasasss {
            text-decoration: none !important;
            color: blue;
            font-family: Calibri;
        }

        .trcntr {
            text-align: center;            
        }
        .trcnttr {
            background:#B8B3B3;
        }
        .captn {            
            font-family: calibri;
            font-weight: bolder;
            color: #393c39;
        }
        .whiteclr {
            background:#ffffff;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1 ssscustom">
                <caption class="auto-style3 sssme">
                    <strong>VIDEO CUTTER  STATUS</strong></caption>
                <tr>
                    <td class="auto-style2">
                        <asp:HiddenField ID="mae_hdn" runat="server" />
                        <asp:Repeater ID="rpt_VideoCutter" runat="server" >
                            <HeaderTemplate>
                                <table border="1" cellpadding="2" align="center">
                                    <tr class="trcntr trcnttr">
                                        <th>S.No</th>
                                        <th>User IP</th>
                                        <th>Host</th>
                                        <th>Video Cutter Version</th>
                                        <th>Creation Date</th>
                                        <th>Last Update Date</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="trcntr whiteclr" runat="server">
                                    <td><asp:Label ID="lblVideoCutterIpId" runat="server" Text='<%# Eval("VideoCutterIpId") %>' /></td>
                                    <td><asp:Label ID="lblUserIP" runat="server" Text='<%# Eval("UserIP") %>' /></td>
                                    <td><asp:Label ID="lblHost" runat="server" Text='<%# Eval("Host") %>' /></td>
                                    <td><asp:Label ID="lblVideoCutterVersion" runat="server" Text='<%# Eval("VideoCutterVersion") %>' /></td>
                                    <td><asp:Label ID="lblCreationDate" runat="server" Text='<%# Eval("CreationDate") %>' /></td>
                                    <td><asp:Label ID="lblLastUpdateDate" runat="server" Text='<%# Eval("LastUpdateDate") %>' /></td>
                                 </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                    <td class="auto-style2"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
