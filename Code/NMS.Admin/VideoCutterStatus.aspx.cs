﻿using NMS.Core;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using NMS.Repository;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NMS.Core.Entities;
using System.Drawing;
using NMS.Core.Enums;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.IO;
using System.Web.Script.Serialization;
using MOSProtocol.Lib.Entities;
using Newtonsoft.Json;
using System.Data;

namespace NMS.Admin
{
    public partial class VideoCutterStatus : System.Web.UI.Page
    {
        IVideoCutterIpService videoCutterIp = IoC.Resolve<IVideoCutterIpService>("VideoCutterIpService");


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
                BindCutterStatus();
        }

        private void BindCutterStatus()
        {
            List<VideoCutterIp> list = videoCutterIp.GetAllVideoCutterIp().ToList();
            rpt_VideoCutter.DataSource = list;
            rpt_VideoCutter.DataBind();
        }
    }
}