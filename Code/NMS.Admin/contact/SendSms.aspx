﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SendSms.aspx.cs" Inherits="NMS.Admin.contact.SendSms" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView runat="server" ID="gv" AutoGenerateColumns="true">
            </asp:GridView>

            <br /><br />
            <asp:Button runat="server" ID="btnSubmit" Text="Send Sms to All" OnClick="btnSubmit_Click" />
        </div>
    </form>
</body>
</html>
