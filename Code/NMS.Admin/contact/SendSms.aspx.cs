﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Office.Interop;
using System.ComponentModel;
using System.Net;
using NMS.Core.Helper;
using System.Configuration;

namespace NMS.Admin.contact
{
    public partial class SendSms : System.Web.UI.Page
    {
        Microsoft.Office.Interop.Excel.Workbook MyBook = null;
        Microsoft.Office.Interop.Excel.Application MyApp = null;
        Microsoft.Office.Interop.Excel.Worksheet MySheet = null;

        public class Employee
        {
            public string Name { get; set; }
            public string Employee_ID { get; set; }
            public string Email_ID { get; set; }
            public string Number { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //MyApp = new Microsoft.Office.Interop.Excel.Application();
            //MyApp.Visible = false;
            //MyBook = MyApp.Workbooks.Open("c:/EmployeesData.xlsx");
            //MySheet = (Microsoft.Office.Interop.Excel.Worksheet)MyBook.Sheets[1]; // Explicit cast is not required here            
            //int lastRow = MySheet.Cells.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell).Row;

            //BindingList<Employee> EmpList = new BindingList<Employee>();
            //for (int index = 2; index <= lastRow; index++)
            //{
            //    System.Array MyValues = (System.Array)MySheet.get_Range("A" + index.ToString(), "D" + index.ToString()).Cells.Value;
            //    EmpList.Add(new Employee
            //    {
            //        Name = MyValues.GetValue(1, 1) != null ? MyValues.GetValue(1, 1).ToString() : "",
            //        Employee_ID = MyValues.GetValue(1, 2) != null ? MyValues.GetValue(1, 2).ToString() : "",
            //        Email_ID = MyValues.GetValue(1, 3) != null ? MyValues.GetValue(1, 3).ToString() : "",
            //        // Number = MyValues.GetValue(1, 4).ToString()
            //    });
            //}
            //gv.DataSource = EmpList;
            //gv.DataBind();

        }

        private static void SendSmsFunction(List<string> ListMessages)
        {
            foreach (string row in ListMessages)
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            MyApp = new Microsoft.Office.Interop.Excel.Application();
            MyApp.Visible = false;
            MyBook = MyApp.Workbooks.Open("c:/EmployeesData.xlsx");
            MySheet = (Microsoft.Office.Interop.Excel.Worksheet)MyBook.Sheets[1]; // Explicit cast is not required here            
            int lastRow = MySheet.Cells.SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell).Row;

            BindingList<Employee> EmpList = new BindingList<Employee>();
            List<string> PhoneNumbers = new List<string>();
            for (int index = 2; index <= lastRow; index++)
            {
                System.Array MyValues = (System.Array)MySheet.get_Range("A" + index.ToString(),"D" + index.ToString()).Cells.Value;
                if (MyValues.GetValue(1, 3) != null && MyValues.GetValue(1, 3).ToString().Length > 2)
                {
                    MyValues.GetValue(1, 3).ToString().Replace("-", "").Replace("_", "").Replace(" ", "");
                    PhoneNumbers.Add(MyValues.GetValue(1, 3).ToString());
                }
            }

            HttpWebRequestHelper webProxy = new HttpWebRequestHelper();
            string smsId = ConfigurationManager.AppSettings["SmsID"].ToString();
            string smsShortCode = ConfigurationManager.AppSettings["SmsShortCode"];
            string smsPassword = ConfigurationManager.AppSettings["SmsPassword"];
            List<string> phoneNumbers = new List<string>();
            phoneNumbers.Add(ConfigurationManager.AppSettings["phoneNumbers"]);

            //string message = "To all Bolwala and Axactions, This is a very important stage of our fight. We need maximum participation from everyone. Tomorrow i.e. 3rd, we have to gather at the art council. Gather as many people as you can at 3:30 pm. And on 4th,we have to show our solidarity with Shoaib Sb. and reach the High Court at 7:30 am.";
            string message = "test";

            for (int i = 0; i < phoneNumbers.Count; i++)
            {
                try
                {
                    //if (ConfigurationManager.AppSettings["test"] != Convert.ToString(1))
                    //{
                    string requestUrl = "http://bsms.ufone.com/bsms_app4/sendapi.jsp?id=" + smsId + "&message=" + message + "&shortcode=" + smsShortCode + "&lang=English&mobilenum=" + "92" + "3229996020" + "&password=" + smsPassword;
                    HttpWebResponse response = webProxy.PostRequest(requestUrl, null, null, false);
                    if (response != null)
                        response.Close();
                    //}
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                }
            }
        }


    }
}