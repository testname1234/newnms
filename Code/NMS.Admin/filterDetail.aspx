﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="filterDetail.aspx.cs" Inherits="NMS.Admin.filterDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ForeColor="Red"  ID="ExceptionMsg" runat="server"></asp:Label>
        <asp:Label ForeColor="Red" ID="RollbackExceptionMsg" runat="server"></asp:Label>
        <asp:Label ForeColor="Green" ID="SuccessMsg" runat="server"></asp:Label>
    <div>
        <asp:Label ID="CategoryName" runat="server"></asp:Label>
            <asp:Repeater ID="rptDiscrepancy" runat="server" OnItemDataBound ="rptResult_ItemDataBound" OnItemCommand="rptDiscrepancy_ItemCommand">
                <HeaderTemplate>
                    <table width="100%" border="1">
                        <tr>
                            <td colspan="5" align="center">Filter Details</td>
                        </tr>
                        <tr>                            
                            <td width="5%">S.No
                            </td>
                            <td width="10%">Filter Id
                            </td>
                            <td width="30%">Name
                            </td>
                            <td width="20%">[Value]
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="trRecord">
                        <td width="5%" align="center">
                            <asp:Label ID="lblSNO" runat="server"></asp:Label>
                        </td>
                        <td width="10%">
                           <span id='FilterId' runat="server"> <%#Eval("FilterId") %></span>
                        </td>
                        <td width="10%">
                            <%#Eval("Name") %><br />
                            
                        </td>
                        <td width="10%">
                            <%#Eval("Value") %>
                        </td>
                        <td><asp:Button ID="btnSubmitUpdate" CommandName="Update" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FilterId") %>'   runat="server" Text="Update" /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
    </form>
</body>
</html>
