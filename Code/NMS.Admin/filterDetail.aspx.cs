﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NMS.Core.DataInterfaces;
using NMS.Core.DataTransfer.Category;

namespace NMS.Admin
{
    public partial class filterDetail : System.Web.UI.Page
    {


        static string connectionString = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["conn"]);
            if (!IsPostBack)
            {
                LoadPage();
            }
        }

        private void LoadPage()
        {
            ICategoryService   categoryService = IoC.Resolve<ICategoryService>("CategoryService");
            List<Filter> lstFilter = null;
            IFilterService objfilterService = IoC.Resolve<IFilterService>("FilterService");
            //get parameters from query string type and value
            
            string Category = string.Empty;
            if (Request.QueryString["CategoryId"] != null)
            {
                Category = Request.QueryString["CategoryId"].ToString();
            }
            categoryService.Get(Category);
            DataTransfer<GetOutput> cat=  categoryService.Get(Category);
            CategoryName.Text = cat.Data.Category;
            lstFilter = objfilterService.GetAllFilter();
            lstFilter = lstFilter.Where(x => x.Value == Convert.ToInt32(Category) && x.FilterTypeId == 13).ToList();
            if (lstFilter != null && lstFilter.Count > 0)
            {
                rptDiscrepancy.DataSource = lstFilter;
                rptDiscrepancy.DataBind();
            }
        }

        protected void rptResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblSNO = (Label)e.Item.FindControl("lblSNO");
                lblSNO.Text = Convert.ToString(e.Item.ItemIndex + 1);
            }
        }

        protected void rptDiscrepancy_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.ToString() == "Update")
            {
                string filterId = Convert.ToString(e.CommandArgument);
                if(filterExecuteSqlTransaction(filterId))
                { 
                    SuccessMsg.Text = "Success";
                    Response.Redirect("/cleanup.aspx");
                }
                //LoadPage();

            }
        }
        private bool filterExecuteSqlTransaction(string filterId)
        {
            bool flag = true;
            ExceptionMsg.Text = "";
            RollbackExceptionMsg.Text = "";
            SuccessMsg.Text = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("SampleTransaction");

                // Must assign both transaction object and connection
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    IFilterService objfilterService = IoC.Resolve<IFilterService>("FilterService");
                    string Category = Request.QueryString["CategoryId"].ToString();
                    List<Filter> lstFilter2 = objfilterService.GetAllFilter();
                    lstFilter2 = lstFilter2.Where(x => x.Value == Convert.ToInt32(Category) && x.FilterTypeId == 13 && x.FilterId != Convert.ToInt32(filterId)).ToList();
                   
                    for (int i = 0; i < lstFilter2.Count; i++)
                    {
                        string sqlCMD = string.Format(@"
                                      declare @selectedFilterId int;
                                      declare @fromFilterId int;
                                      set @selectedFilterId= {0}
                                      set @fromFilterId= {1}
                                      update NewsFileFilter set filterid=@selectedFilterId where filterid=@fromFilterId
                                      delete from filter where filterid=@fromFilterId", filterId, lstFilter2[i].FilterId);
                        command.CommandText = sqlCMD;
                        command.ExecuteNonQuery();
                    }

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    // Console.WriteLine("Both records are written to database.");
                }
                catch (Exception ex)
                {
                    flag = false;
                    ExceptionMsg.Text = ex.Message;
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred
                        // on the server that would cause the rollback to fail, such as
                        // a closed connection.
                        //Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        //Console.WriteLine("  Message: {0}", ex2.Message);
                        RollbackExceptionMsg.Text = ex2.Message;
                    }
                    return flag;
                }
                connection.Close();
            }
            return flag;
        }
    }
}