﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FilterCount.aspx.cs" Inherits="NMS.Admin.monitor.FilterCount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table style="width: 100%">
                <tr>

                    <td style="width:20%">Cache Filter Keys</td>
                    <td>
                        <asp:DropDownList ID="ddlFilterCacheKeys" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFilterCacheKeys_SelectedIndexChanged"></asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td colspan="2">

                        <table style="width: 100%">

                            <tr>
                                <td style="width:20%">Alive Time </td>
                                <td>
                                    <asp:Label ID="lblAliveTime" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%">Activity Time </td>
                                <td>
                                    <asp:Label ID="lblActivityTime" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>

                                <td colspan="2">
                                    <asp:Repeater ID="rptData" runat="server" OnItemDataBound="rptData_ItemDataBound">

                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>S.No</td>
                                                     <td>FilterId</td>
                                                    <td>Filter TypeId</td>
                                                    <td>Filter Name</td>
                                                    <td>Value</td>
                                                </tr>
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <tr>
                                                <td style="width:5%"><asp:Label ID="lblSNO" runat="server" ></asp:Label> </td>
                                                <td style="width:5%"><%#Eval("FilterId") %></td>
                                                <td style="width:5%"><%#Eval("FilterTypeId") %></td>
                                                <td style="width:25%"><%#Eval("FilterName") %></td>
                                                <td style="width:15%"><%#Eval("Count") %></td>
                                            </tr>


                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
