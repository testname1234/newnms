﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CacheHelper.AppFabric;
using NMS.CacheRepository;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;

namespace NMS.Admin.monitor
{
    public partial class FilterCount : System.Web.UI.Page
    {
        ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
        IFilterService objFilterService = IoC.Resolve<IFilterService>("FilterService");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                BindCacheFilterKeys();
            }
        }


        private void BindCacheFilterKeys()
        {
            FilterCountRepository objRepository = new FilterCountRepository();
            List<string> lstCacheKeys = objRepository.GetAllProducersCacheKeys();

            ddlFilterCacheKeys.DataSource = lstCacheKeys;
            ddlFilterCacheKeys.DataBind();

            if (lstCacheKeys.Count > 0)
            {
                GetIndividualCacheKeyResult(lstCacheKeys[0]);
            }

        }


        private void GetIndividualCacheKeyResult(string key)
        {

            CacheInitialDataInput objCurrent = objCacheManager.GetFromCache<CacheInitialDataInput>(key);

            lblAliveTime.Text = objCurrent.AliveTime.ToString();
            lblActivityTime.Text = objCurrent.ActivityTime.ToString();


            List<Filter> lstAllFilters = objFilterService.GetAllFilter();
            
            var lstCollection = objCurrent.Data.Select(x => new
            {
                FilterId = x.FilterId,
                Count = x.Count,
                FilterTypeId = (lstAllFilters.Where(y => y.FilterId == x.FilterId).First().FilterTypeId),
                FilterName = (lstAllFilters.Where(y => y.FilterId == x.FilterId).First().Name)
            }).ToList().OrderByDescending(z => z.Count).OrderByDescending(z => z.FilterTypeId);

            rptData.DataSource = lstCollection.AsEnumerable();
            rptData.DataBind();

        }
        protected void ddlFilterCacheKeys_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetIndividualCacheKeyResult(ddlFilterCacheKeys.SelectedItem.Text.ToString());
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblSNO = (Label)e.Item.FindControl("lblSNO");
                lblSNO.Text = (e.Item.ItemIndex + 1).ToString();
            }
        }
    }
}