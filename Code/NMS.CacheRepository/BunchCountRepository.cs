﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CacheHelper.AppFabric;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.IService;
using NMS.MongoRepository;

namespace NMS.CacheRepository
{
    public class BunchCountRepository : IMBunchCount
    {
        public void PollBunchCount()
        { 

            //first get the maximum date of bunchNews found in cache
            DateTime LastUpdatedDate = GetBunchLastUpdatedDate();
            
            //get all bunch news group by date
            
            MBunchNewsRepository objRepository = new MBunchNewsRepository();

            List<MBunchCount>  lstBunchIDCount = objRepository.GetBunchNewsGroupByBunchId(LastUpdatedDate);

            UpdateCacheForBunchNews(lstBunchIDCount, LastUpdatedDate);


        }
        /// <summary>
        /// Will be using cache to get the last updated date
        /// </summary>
        /// <returns></returns>
        public DateTime GetLastUpdatedFetchDate()
        {
            DateTime myDateTime;

            ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            List<MBunchCount> cachelstBunchIDCount = objCacheManager.GetFromCache<List<MBunchCount>>(CacheKey.BunchIDCount.ToDescription());

            if (cachelstBunchIDCount != null && cachelstBunchIDCount.Count > 0)
            {

                myDateTime = cachelstBunchIDCount.OrderByDescending(x => x.LastUpdateDate).Take(1).ToList<MBunchCount>()[0].LastUpdateDate;
            }
            else
                return System.DateTime.UtcNow.AddDays(-360);


            return myDateTime;
        }

        /// <summary>
        /// will be using SQL db and fetch the date from the table
        /// </summary>
        /// <returns></returns>
        public DateTime GetBunchLastUpdatedDate()
        {
            DateTime myDateTime;

            IScrapMaxDatesService objScrapMaxDatesService = IoC.Resolve<IScrapMaxDatesService>("ScrapMaxDatesService");
            ScrapMaxDates objScrapDate = objScrapMaxDatesService.GetScrapMaxDatesBySource("");

            if (objScrapDate != null)
                myDateTime = objScrapDate.MaxUpdateDate;

            else
                myDateTime = DateTime.UtcNow.AddYears(-2);

            return myDateTime;
        }

        

        public bool UpdateCacheForBunchNews(List<MBunchCount> lstBunchIDCount, DateTime LastUpdatedDate)
        {

            bool isSaved = false;
            try
            {
                bool isNeedToUpdatCache = false;
                //get all the data from the cache
                List<MBunchCount> cachelstBunchIDCount;

                ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
                cachelstBunchIDCount = objCacheManager.GetFromCache<List<MBunchCount>>(CacheKey.BunchIDCount.ToDescription());

                //process all the entities
                if (cachelstBunchIDCount != null && cachelstBunchIDCount.Count > 0 && lstBunchIDCount.Count > 0)
                {
                    /// find if all the entities exists in cache or not if not add them first ////

                    foreach (MBunchCount recordItem in lstBunchIDCount)
                    {
                        var objExists = cachelstBunchIDCount.Exists(x => x.BunchId == recordItem.BunchId);
                        if (Convert.ToBoolean(objExists) == false)
                            cachelstBunchIDCount.Add(recordItem);
                    }

                    foreach (MBunchCount cacheItem in cachelstBunchIDCount)
                    {
                        // MFilterCount filterCount = lstfilterCount.Where(x => x.FilterId = cacheItem.FilterId).ToList <MFilterCount>();

                        List<MBunchCount> objTemp = lstBunchIDCount.Where(x => x.BunchId == cacheItem.BunchId).ToList<MBunchCount>();

                        if (objTemp.Count > 0)
                        {
                            cacheItem.LastUpdateDate = objTemp[0].LastUpdateDate;
                            cacheItem.Count = objTemp[0].Count;
                            cacheItem.LastUpdateDate = LastUpdatedDate;
                            isNeedToUpdatCache = true;
                        }
                    }
                }
                else
                {
                    cachelstBunchIDCount = lstBunchIDCount;
                    isNeedToUpdatCache = true;
                }

                //save them all again in cache
                if (isNeedToUpdatCache)
                    objCacheManager.UpdateCache<List<MBunchCount>>(CacheKey.BunchIDCount.ToDescription(), (object)cachelstBunchIDCount);

                isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }


        /// <summary>
        /// Update the bunch News count in mongo DB
        /// </summary>
        /// <param name="lstBunchIDCount"></param>
        /// <param name="LastUpdatedDate"></param>
        /// <returns></returns>
        public bool UpdateBunchNewsCountInDB(List<MBunchCount> lstBunchIDCount, DateTime LastUpdatedDate)
        {
            bool isSaved = false;
            try
            {

                /////////// update the lastUpdated Date in scraper Date //////////////



                foreach (MBunchCount cacheItem in lstBunchIDCount)
                {
                    // MFilterCount filterCount = lstfilterCount.Where(x => x.FilterId = cacheItem.FilterId).ToList <MFilterCount>();

                    List<MBunchCount> objTemp = lstBunchIDCount.Where(x => x.BunchId == cacheItem.BunchId).ToList<MBunchCount>();

                    if (objTemp.Count > 0)
                    {
                        cacheItem.LastUpdateDate = objTemp[0].LastUpdateDate;
                        cacheItem.Count = objTemp[0].Count;
                        cacheItem.LastUpdateDate = LastUpdatedDate;
                     
                    }
                }

                isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }
    }
}
