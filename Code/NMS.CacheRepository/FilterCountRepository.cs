﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMS.Core.DataInterfaces.Mongo;
using CacheHelper.AppFabric;
using NMS.Core;
using NMS.Core.Enums;
using NMS.Core.Entities.Mongo;
using NMS.Core.Models;
using NMS.MongoRepository;
using NMS.Core.Entities;
using NMS.Core.IService;


namespace NMS.CacheRepository
{
    
    public class FilterCountRepository : IMFilterCountRepository
    {
        public string CacheKeyName = "FILTERSID_FROM_TO_ProgramFilterID";

        public bool InsertOrUpdate(Core.Entities.Mongo.MFilterCount filterCount)
        {
            throw new NotImplementedException();
        }

        public bool Update(Core.Entities.Mongo.MFilterCount filterCount)
        {
            throw new NotImplementedException();
        }

        public bool CheckIfFilterCountExists(Core.Entities.Mongo.MFilterCount filterCount)
        {
            throw new NotImplementedException();
        }

        public DateTime GetMaxLastUpdateDate()
        {
            throw new NotImplementedException();
        }

        public Core.Entities.Mongo.MFilterCount GetById(string Id)
        {
            ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            List<MFilterCount> rec;
            List<MFilterCount> cachelstFilterCount = objCacheManager.GetFromCache<List<MFilterCount>>(CacheKey.FilterCount.ToDescription());

            if (cachelstFilterCount != null)
            {
                rec = cachelstFilterCount.Where(x => x._id == Id).Take(1).ToList<MFilterCount>();
                if (rec == null || rec.Count == 0)
                    return null;
                else return rec[0];
            }
            else
                return null;


        }

        public List<Core.Entities.Mongo.MFilterCount> GetFilterCountByLastUpdateDate(DateTime lastUpdateDate)
        {

            //return base.GetByQuery(Query<MFilterCount>.GT(x => x.LastUpdateDate, lastUpdateDate));
            ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            List<MFilterCount> rec;
            List<MFilterCount> cachelstFilterCount = objCacheManager.GetFromCache<List<MFilterCount>>(CacheKey.FilterCount.ToDescription());

            if (cachelstFilterCount != null)
            {
                rec = cachelstFilterCount.Where(x => x.LastUpdateDate > lastUpdateDate).ToList<MFilterCount>();
                return rec;
            }
            else
                return new List<MFilterCount>();

        }

        public DateTime GetMaxLastUpdateDate(int filterId)
        {
            ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");

            List<MFilterCount> cachelstFilterCount = objCacheManager.GetFromCache<List<MFilterCount>>(CacheKey.FilterCount.ToDescription());

            if (cachelstFilterCount != null)
            {
                List<MFilterCount> rec = cachelstFilterCount.Where(y=> y.FilterId == filterId).OrderByDescending(x => x.LastUpdateDate).Take(1).ToList<MFilterCount>();

                ///var rec = base.GetMinMaxValue(SortBy<MFilterCount>.Descending(x => x.LastUpdateDate), Fields<MFilterCount>.Include(x => x.LastUpdateDate));
                if (rec == null || rec.Count == 0)
                    return DateTime.UtcNow.AddYears(-100);
                else return rec[0].LastUpdateDate;
            }
            else
                return DateTime.UtcNow.AddYears(-100);
        }

        public List<int> GetDistinctFilterIds()
        {
            throw new NotImplementedException();
        }


        //new methods for cache integration
        public bool InsertOrUpdate(List<Core.Entities.Mongo.MFilterCount> lstFilterCount)
        {

            bool isSaved = false;
            try
            {
                bool isNeedToUpdatCache = false;
                //get all the data from the cache
                List<MFilterCount> cachelstFilterCount;

                    ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
                    cachelstFilterCount = objCacheManager.GetFromCache<List<MFilterCount>>(CacheKey.FilterCount.ToDescription());

                    //process all the entities
                    if (cachelstFilterCount != null && cachelstFilterCount.Count > 0 && lstFilterCount.Count > 0)
                    {
                        /// find if all the entities exists in cache or not if not add them first ////

                        foreach (MFilterCount recordItem in lstFilterCount)
                        {
                            var objExists = cachelstFilterCount.Exists(x => x.FilterId == recordItem.FilterId);
                            if (Convert.ToBoolean(objExists) == false)
                                cachelstFilterCount.Add(recordItem);
                        }

                        foreach (MFilterCount cacheItem in cachelstFilterCount)
                        {
                            // MFilterCount filterCount = lstfilterCount.Where(x => x.FilterId = cacheItem.FilterId).ToList <MFilterCount>();

                            List<MFilterCount> objTemp = lstFilterCount.Where(x => x.FilterId == cacheItem.FilterId).ToList<MFilterCount>();

                            if (objTemp.Count > 0)
                            {
                                cacheItem.LastUpdateDate = objTemp[0].LastUpdateDate;
                                cacheItem.Count = objTemp[0].Count;
                                isNeedToUpdatCache = true;
                            }
                        }
                    }
                    else
                    {
                        cachelstFilterCount = lstFilterCount;
                        isNeedToUpdatCache = true;
                    }

                //save them all again in cache
                if(isNeedToUpdatCache)
                    objCacheManager.UpdateCache<List<MFilterCount>>(CacheKey.FilterCount.ToDescription(), (object)cachelstFilterCount);

                isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }


        public List<MFilterCount> GetFilterCountByLastUpdateDate(List<Filter> filterIds, DateTime from, DateTime to,List<int> programFilterIds)
        {
            bool IsNeedToAddInCache = false;
            ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");


            //first make the CacheInitialDataInput object

            CacheInitialDataInput objCacheObject = SetInputParamaters(DateTime.UtcNow, DateTime.UtcNow.AddMinutes(-1), from, to, filterIds, programFilterIds);

            string myKeyName = GetCacheKeyNameFromCacheInputParamaters(objCacheObject.Input);


            //as now we have key, find, if the Cache has Key or not? 
            List<string> lstAllKeys = GetAllProducersCacheKeys();

            if (lstAllKeys != null && lstAllKeys.Count > 0)
            {
                //find our key in CacheKeyCollection

                if (lstAllKeys.Exists(x => x == myKeyName))
                {
                    CacheInitialDataInput objCurrent = objCacheManager.GetFromCache<CacheInitialDataInput>(myKeyName);

                    objCurrent.AliveTime = System.DateTime.UtcNow;

                    objCacheManager.UpdateCache<CacheInitialDataInput>(myKeyName, objCurrent);
                    return objCurrent.Data;

                }
                else
                    IsNeedToAddInCache = true;

            }
            else
            {
                //means there is no such keys name in cache CacheKeyListForDataPolling
                objCacheManager.UpdateCache<List<string>>(CacheKey.CacheKeyListForDataPolling.ToDescription(), new List<string>());
                IsNeedToAddInCache = true;
            }

            if (IsNeedToAddInCache)
            {
                // add the newly key in cache
                List<string> ActualKeysInCache = GetAllProducersCacheKeys();
                ActualKeysInCache.Add(myKeyName);

                //update the key collection
                objCacheManager.UpdateCache<List<string>>(CacheKey.CacheKeyListForDataPolling.ToDescription(), ActualKeysInCache);

                //add the key as well in cache
                objCacheManager.UpdateCache<CacheInitialDataInput>(myKeyName, objCacheObject);
            }


            return null;
        }

        public void ProcessProducerDataPolling()
        {
            
            ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            List<string> lstAllKeys = GetAllProducersCacheKeys();

            if (lstAllKeys != null && lstAllKeys.Count > 0)
            {
                foreach(string key in lstAllKeys)
                {
                    CacheInitialDataInput objCurrent = objCacheManager.GetFromCache<CacheInitialDataInput>(key);

                    //if for some reason cache key is found but data is null delete the cache key and cache list
                    if (objCurrent == null)
                    {
                        objCacheManager.RemoveFromCache(key);

                        //update the collection of keys again for next cycle
                        List<string> ActualKeysInCache = GetAllProducersCacheKeys();
                        ActualKeysInCache.Remove(key);
                        objCacheManager.UpdateCache<List<string>>(CacheKey.CacheKeyListForDataPolling.ToDescription(), ActualKeysInCache);
                        continue;
                    }


                    //check wheather key is expired or not
                    if (objCurrent.IsExpired)
                    {
                        //remove the cache key 
                        objCacheManager.RemoveFromCache(key);

                        //update the collection of keys again for next cycle
                        List<string> ActualKeysInCache = GetAllProducersCacheKeys();
                        ActualKeysInCache.Remove(key);
                        objCacheManager.UpdateCache<List<string>>(CacheKey.CacheKeyListForDataPolling.ToDescription(), ActualKeysInCache);
                        continue;
                    }
                    else
                    {
                        if (objCurrent.IsNewDataRequired)
                        {
                            //mono implementation goes here

                            List<MFilterCount> lstFilterCount = ParseDataFromMongoAsGroupByFilterId(objCurrent.Input);
                            if (lstFilterCount != null && lstFilterCount.Count > 0)
                            {
                                objCurrent.Data = lstFilterCount.OrderByDescending(x=>x.FilterId).ToList();
                                //update the object and set that in cahce again
                            }
                                objCurrent.ActivityTime = System.DateTime.UtcNow;
                                objCacheManager.UpdateCache<CacheInitialDataInput>(key, objCurrent);
                        }                       
                    }

                }
            }

        }


        private List<MFilterCount> ParseDataFromMongoAsGroupByFilterId(CacheInputRequest input)
        {
            IMNewsRepository repo = IoC.Resolve<IMNewsRepository>("MNewsRepository");
            MNewsFilterRepository objFilterCountRepository = new MNewsFilterRepository();

            //////// few modificatoin in to time ////////////
            DateTime toDate = input.To;
            if (input.To.Date == System.DateTime.UtcNow.Date)
                toDate = System.DateTime.UtcNow;
            List<Filter> filters = new List<Filter>();
            if (input.Filters != null)
                filters.AddRange(input.Filters);
            if (input.ProgramFilters != null)
            {
                foreach (var id in input.ProgramFilters)
                    filters.Add(new Filter() { FilterId = id, FilterTypeId = 18 });
            }
            return repo.GetNewsFilterCounts(filters, input.From, toDate);
        }


        #region Cache Input / Output Related functions 

        public CacheInitialDataInput SetInputParamaters(DateTime AliveTime, DateTime ActivityTime, DateTime From, DateTime To, List<Filter> filters,List<int> programFilters)
        {

            CacheInitialDataInput oCacheKeyObject = new CacheInitialDataInput();
            oCacheKeyObject.Data = null;
            oCacheKeyObject.ActivityTime = ActivityTime;
            oCacheKeyObject.AliveTime = AliveTime;
            oCacheKeyObject.Input = new CacheInputRequest { Filters = filters, From = From, To = To, ProgramFilters = programFilters };
            return oCacheKeyObject;
        }

        private string GetCacheKeyNameFromCacheInputParamaters(NMS.Core.Models.CacheInputRequest obj)
        {
            string myKey = CacheKeyName;
            //myKey = myKey.Replace("FILTERSID", string.Join(",", obj.Filters.OrderBy(x => x)));

            string filterIDs = string.Join(",", obj.Filters.Where((x => x.FilterTypeId == 13 || x.FilterTypeId == 15)).OrderBy(x => x.FilterId).Select(x => x.FilterId.ToString()).ToArray());
            //if (obj.ProgramFilters != null)
            //{
            //    string programFilterIDs = string.Join(",", obj.ProgramFilters);
            //    myKey = myKey.Replace("ProgramFilterID", programFilterIDs);
            //}
            myKey = myKey.Replace("FILTERSID", filterIDs);
            myKey = myKey.Replace("FROM", obj.From.ToShortDateString());
            myKey = myKey.Replace("TO", obj.To.ToShortDateString());

            return myKey;
        }

        public List<string> GetAllProducersCacheKeys()
        {
            ICacheManager objCacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            return objCacheManager.GetFromCache<List<string>>(CacheKey.CacheKeyListForDataPolling.ToDescription());
 
        }
        #endregion

    }
}



/*
List<MFilterCount> myLst = (from a in lstfilterCount
join b in cachelstFilterCount
on a.FilterId equals b.FilterId
select a
).ToList<MFilterCount>();
*/