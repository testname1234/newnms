﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CacheHelper.AppFabric;
using NMS.Core.Entities;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.Models;
using NMS.Core.Enums;
using NMS.Core.Helper;

namespace NMS.CacheRepository
{
    public class NewsCacheRepository : INewsCacheRepository
    {

        public LoadNews GetNewsAndBunch(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null)
        {
            ICacheManager cacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            string key = GetKeyName(filters, pageCount, startIndex, from, to, term, discaredFilters);
            string str = cacheManager.GetFromCache<string>(key);
            if (!string.IsNullOrEmpty(str))
                return JSONHelper.GetObject<LoadNews>(str);
            else return null;
        }

        public List<NewsBunchArgs> GetAllKeys()
        {
            ICacheManager cacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            return cacheManager.GetFromCache<List<NewsBunchArgs>>(CacheKey.GetNewsAndBunch.ToDescription());
        }


        private string GetKeyName(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null)
        {
            string filterIds = string.Join(",", filters.Select(x => x.FilterId).ToArray());
            string discaredFilterIds = string.Join(",", discaredFilters.Select(x => x.FilterId).ToArray());
            return string.Format("{0}_{1}_{2}_{3}_{4}_{5}", filterIds, "dt", startIndex.ToString(), pageCount.ToString(), term, discaredFilterIds);
        }


        public void UpdateCache(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters, LoadNews loadNews)
        {
            ICacheManager cacheManager = IoC.Resolve<ICacheManager>("CacheManager");
            string key = GetKeyName(filters, pageCount, startIndex, from, to, term, discaredFilters);
            var lst = cacheManager.GetFromCache<List<NewsBunchArgs>>(CacheKey.GetNewsAndBunch.ToDescription());
            if (lst == null)
            {
                lst = new List<NewsBunchArgs>();
            }
            if (!lst.Any(x => x.key == key))
            {
                lst.Add(new NewsBunchArgs()
                {
                    key = key,
                    filters = filters,
                    pageCount = pageCount,
                    startIndex = startIndex,
                    from = from,
                    to = to,
                    term = term,
                    discaredFilters = discaredFilters,
                    DateUpdated = DateTime.UtcNow
                });
            }
            else
            {
                lst.First(x => x.key == key).DateUpdated = DateTime.UtcNow;
            }
            cacheManager.UpdateCache<List<NewsBunchArgs>>(CacheKey.GetNewsAndBunch.ToDescription(), lst);
            var json = JSONHelper.GetString(loadNews);
            cacheManager.UpdateCache<string>(key, json);
        }
    }
}
