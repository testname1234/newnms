﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace NMS.Core
{
    public class AppSettings
    {
        public static string _mediaServerUrlOverride;

        public static string MediaServerUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_mediaServerUrlOverride))
                    return ConfigurationManager.AppSettings["MediaServerUrl"];
                else return _mediaServerUrlOverride;
            }
        }


        public static string WebAppUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["WebAppUrl"];
            }
        }

        public static string SleniumTemplateUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SeleniuminputFile"];
            }
        }

        public static string MediaServerApiKey
        {
            get
            {
                return ConfigurationManager.AppSettings["MediaServerAPIKey"];
            }
        }

        public static string PcrWebApi
        {
            get
            {
                return ConfigurationManager.AppSettings["PcrWebApi"];
            }
        }

        public static string UserConsoleApiUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["UserConsoleApiUrl"];
            }
        }

        public static string VideoFormat
        {
            get
            {
                return ConfigurationManager.AppSettings["VideoFormat"];
            }
        }

        public static string ImageFormat
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageFormat"];
            }
        }

        public static string MediaFolderPath
        {
            get
            {
                return ConfigurationManager.AppSettings["MediaFolderPath"];
            }
        }

        public static string ClipsFolderPath
        {
            get
            {
                return ConfigurationManager.AppSettings["ClipsFolderPath"];
            }
        }
        
        public static string MMSUsermanagementUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["MMSUsermanagementUrl"];
            }
        }

        public static string ModuleId
        {
            get
            {
                return ConfigurationManager.AppSettings["ModuleId"];
            }
        }

        public static string ReporterFolder
        {
            get
            {
                return "Reporter";
            }
        }
    }
}
