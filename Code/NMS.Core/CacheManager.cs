﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMS.Core.Entities;
using System.Configuration;
using NMS.Core.Helper;
using System.Threading;
using NMS.Core.Models;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;

namespace NMS.Core
{
    public static class CacheManager
    {
        private const int MaxResourcesExpiryTime = 60;
        private const int MaxTeamExpiryTime = 60;
        private const int MaxNewsFilePollingDataExpiryTime = 1;
        private const int MaxWorkRoleFolderExpiryTime = 60;
        private const int MaxFolderExpiryTime = 10;
        private const int MaxCategoryExpiryTime = 10;
        private const int MaxChannelVideoExpiryTime = 60;
        static Store<MS.Core.DataTransfer.Resource.ResourceOutput> resourcesStore;
        static Store<NewsFilePollingData> newsFilePollingDataStore;
        static Store<List<WorkRoleFolder>> workRoleFolderStore;
        static Store<List<Folder>> folderStore;
        static Store<List<Team>> teamStore;
        static Store<List<Category>> categoryStore;

        static CacheManager()
        {
            try
            {
                resourcesStore = new Store<MS.Core.DataTransfer.Resource.ResourceOutput>(MaxResourcesExpiryTime);
                resourcesStore.OnUpdateDate += resources_OnUpdateDate;
                resourcesStore.updateData(null);


                newsFilePollingDataStore = new Store<NewsFilePollingData>(MaxNewsFilePollingDataExpiryTime);
                newsFilePollingDataStore.OnUpdateDate += newsFilePollingDataStore_OnUpdateDate;

                workRoleFolderStore = new Store<List<WorkRoleFolder>>(MaxWorkRoleFolderExpiryTime);
                workRoleFolderStore.OnUpdateDate += workRoleFolderStore_OnUpdateDate;

                folderStore = new Store<List<Folder>>(MaxFolderExpiryTime);
                folderStore.OnUpdateDate += folderStore_OnUpdateDate;

                teamStore = new Store<List<Team>>(MaxTeamExpiryTime);
                teamStore.OnUpdateDate += teamStore_OnUpdateDate;

                categoryStore = new Store<List<Category>>(MaxCategoryExpiryTime);
                categoryStore.OnUpdateDate += CategoryStore_OnUpdateDate;
                
            }
            catch (Exception exp)
            {
                //  ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                //  logService.InsertSystemEventLog(string.Format("Error in CacheManager: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
            }
        }

        private static List<Category> CategoryStore_OnUpdateDate()
        {
            ICategoryRepository repository = IoC.Resolve<ICategoryRepository>("CategoryRepository");
            return repository.GetAllCategory();
        }

        public static List<ChannelVideo> ChannelVideosStoreByDateTime(DateTime dt)
        {
            dt = dt.Date;
            var dtNow = DateTime.Now.AddMinutes(-5);
            if (ChannelVideos == null)
                ChannelVideos = new Dictionary<DateTime, List<ChannelVideo>>();
            if (dt == dtNow.Date)
            {
                List<DateTime> toRemove = new List<DateTime>();
                foreach (var _dt in ChannelVideos.Keys.Where(x => x.Date == dtNow.Date))
                {
                    toRemove.Add(_dt);
                    //ChannelVideos.Remove(_dt);
                }
                foreach(var del in toRemove)
                {
                    ChannelVideos.Remove(del);
                }
                dt = dt.AddHours(dtNow.Hour);
            }
            if (ChannelVideos.ContainsKey(dt))
                return ChannelVideos[dt];
            List<ChannelVideo> ret = new List<ChannelVideo>();
            NMS.Core.Helper.HttpWebRequestHelper helper = new NMS.Core.Helper.HttpWebRequestHelper();
            string date = dt.ToString("yyyy-MM-dd");
            string VideoDataAPi = ConfigurationManager.AppSettings["videoMatchingHost"] + "ReelMonitoring/GetLiveTCS";
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.Models.ChannelVideoOutput>>>(VideoDataAPi + "?dt=" + date, null);
            if (output.IsSuccess && output.Data != null)
            {
                List<NMS.Core.Models.ChannelVideoOutput> lstChannelData = new List<NMS.Core.Models.ChannelVideoOutput>();
                lstChannelData = output.Data;
                IChannelVideoService cServ = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
                int i = 1;
                foreach (NMS.Core.Models.ChannelVideoOutput channelData in lstChannelData)
                {
                    ChannelVideo channelVideo = new ChannelVideo();
                    channelData.From = channelData.From.AddHours(5); // it will be converted back to -5 for utc conversion in datatransfer entity
                    channelVideo.CopyFrom(channelData);

                    channelVideo.ChannelVideoId = i;
                    channelVideo.To = channelVideo.From.Value.AddHours(1);
                    channelVideo.ChannelId = channelData.ChannelId;
                    channelVideo.ProgramId = -1; // fake program id for hourly slot
                    channelVideo.PhysicalPath = channelData.PhysicalPath;
                    DateTime dt1 = DateTime.Now;
                    //channelVideo.DurationSeconds = cServ.GetDuration(channelData.PhysicalPath);
                    System.Diagnostics.Debug.WriteLine(DateTime.Now.Subtract(dt1).TotalMilliseconds);
                    channelVideo.Url = "http:" + channelData.PhysicalPath.Replace(@"\", "/");

                    channelVideo.CreationDate = DateTime.UtcNow;
                    channelVideo.LastUpdateDate = channelVideo.CreationDate;
                    channelVideo.IsActive = true;
                    ret.Add(channelVideo);
                    i++;
                }
            }
            ChannelVideos[dt] = ret;
            return ChannelVideos[dt];
        }

        static List<Team> teamStore_OnUpdateDate()
        {
            ITeamRepository repository = IoC.Resolve<ITeamRepository>("TeamRepository");
            return repository.GetAllTeam();
        }

        static List<Folder> folderStore_OnUpdateDate()
        {
            IFolderRepository folderRepository = IoC.Resolve<IFolderRepository>("FolderRepository");
            DateTime dtMax = DateTime.UtcNow.AddYears(-5);
            if (folderStore.MaxLastUpdateDate != DateTime.MinValue)
                dtMax = folderStore.MaxLastUpdateDate;
            var result = folderRepository.GetFolderByCreationDate(dtMax);
            if (result != null && result.Count > 0)
                folderStore.MaxLastUpdateDate = result.Max(x => x.CreationDate);
            return result == null ? folderStore._data : result;
        }

        static List<WorkRoleFolder> workRoleFolderStore_OnUpdateDate()
        {
            IWorkRoleFolderRepository workRoleFolderRepository = IoC.Resolve<IWorkRoleFolderRepository>("WorkRoleFolderRepository");
            return workRoleFolderRepository.GetAllWorkRoleFolder();
        }

        static NewsFilePollingData newsFilePollingDataStore_OnUpdateDate()
        {
            
            INewsFileRepository newsFileRepository = IoC.Resolve<INewsFileRepository>("NewsFileRepository");
            IFileResourceRepository fileResourceRepository = IoC.Resolve<IFileResourceRepository>("FileResourceRepository");
            IFileStatusHistoryRepository fileStatusHistoryRepository = IoC.Resolve<IFileStatusHistoryRepository>("FileStatusHistoryRepository");
            IFileFolderHistoryRepository fileFolderHistoryRepository = IoC.Resolve<IFileFolderHistoryRepository>("FileFolderHistoryRepository");
            IFileDetailRepository fileDetailRepository = IoC.Resolve<IFileDetailRepository>("FileDetailRepository");
            DateTime dtMax = DateTime.UtcNow;
            if (newsFilePollingDataStore._data == null)
                newsFilePollingDataStore._data = new NewsFilePollingData();

            NewsFilePollingData result = newsFilePollingDataStore._data;
            List<NewsFile> files = new List<NewsFile>();
            if (newsFilePollingDataStore._data.Files == null)
            {
                files = newsFileRepository.GetAllNewsFile();
            }
            else
            {
                dtMax = newsFilePollingDataStore._data.Files.Max(x => x.CreationDate);
                files = newsFileRepository.GetNewsFilebyLastUpdateDate(dtMax);
            }
            if (files != null)
            {
                if (result.Files == null)
                {
                    result.Files = new List<NewsFile>();
                    result.Files.AddRange(files);
                }
                else
                {
                    foreach (var f in files)
                    {
                        result.Files.RemoveAll(x => x.NewsFileId == f.NewsFileId);
                        result.Files.Add(f);
                    }
                }
            }

            if (newsFilePollingDataStore._data.FileDetails == null)
                dtMax = DateTime.UtcNow.AddYears(-1);
            else dtMax = newsFilePollingDataStore._data.FileDetails.Max(x => x.LastUpdateDate);
            var fileDetails  = fileDetailRepository.GetFileDetailByLastUpdateDate(dtMax);
            if (fileDetails != null)
            {
                if (result.FileDetails == null)
                    result.FileDetails = new List<FileDetail>();
                result.FileDetails.AddRange(fileDetails);
            }

            if (newsFilePollingDataStore._data.FileFolderHistory == null)
                dtMax = DateTime.UtcNow.AddYears(-1);
            else dtMax = newsFilePollingDataStore._data.FileFolderHistory.Max(x => x.CreationDate);
            var fileFolders = fileFolderHistoryRepository.GetFileFolderHistoryByCreationDate(dtMax);
            if (fileFolders != null)
            {
                if (result.FileFolderHistory == null)
                    result.FileFolderHistory = new List<FileFolderHistory>();
                result.FileFolderHistory.AddRange(fileFolders);
            }

            if (newsFilePollingDataStore._data.FileResources == null)
                dtMax = DateTime.UtcNow.AddYears(-1);
            else dtMax = newsFilePollingDataStore._data.FileResources.Max(x => x.LastUpdateDate);
            var resources = fileResourceRepository.GetFileResourceByLastUpdateDate(dtMax);
            if (resources != null)
            {
                if (result.FileResources == null)
                    result.FileResources = new List<FileResource>();
                result.FileResources.AddRange(resources);
            }

            if (newsFilePollingDataStore._data.FileStatusHistory == null)
                dtMax = DateTime.UtcNow.AddYears(-1);
            else dtMax = newsFilePollingDataStore._data.FileStatusHistory.Max(x => x.CreationDate);
            var fileStatuses = fileStatusHistoryRepository.GetFileStatusHistoryByCreationDate(dtMax);
            if (fileStatuses != null)
            {
                if (result.FileStatusHistory == null)
                    result.FileStatusHistory = new List<FileStatusHistory>();
                result.FileStatusHistory.AddRange(fileStatuses);
            }
            return result;
        }

        static MS.Core.DataTransfer.Resource.ResourceOutput resources_OnUpdateDate()
        {
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            MS.Core.DataTransfer.Resource.ResourceOutput Resources = new MS.Core.DataTransfer.Resource.ResourceOutput();
            string ResourceDate = DateTime.UtcNow.AddHours(-24).ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            return requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/getresourcebydate?ResourceDate=" + ResourceDate, null);
        }

        public static MS.Core.DataTransfer.Resource.ResourceOutput Resources { get { return resourcesStore.Data; } }
        public static NewsFilePollingData NewsFilePollingData { get { return newsFilePollingDataStore.Data; } }
        public static List<WorkRoleFolder> WorkRoleFolder { get { return workRoleFolderStore.Data; } }
        public static List<Folder> Folders { get { return folderStore.Data; } }
        public static List<Team> Teams { get { return teamStore.Data; } }
        public static List<Category> Categories { get { return categoryStore.Data; } }
        private static Dictionary<DateTime, List<ChannelVideo>> ChannelVideos;
    }
}
