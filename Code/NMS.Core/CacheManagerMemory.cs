﻿using CacheHelper.AppFabric;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core
{
    public class CacheManagerMemory: ICacheManager
    {
        static Dictionary<string, object> data = new Dictionary<string, object>();
        public CacheManagerMemory()
        {

        }

        public T GetFromCache<T>(string key)
        {
            if (data.ContainsKey(key))
                return (T)data[key];
            return default(T);
        }

        public bool RemoveFromCache(string key)
        {
            return data.Remove(key);
        }

        public void UpdateCache<T>(string key, object _data)
        {
            data[key] = _data;
        }
    }
}
