﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IAlertRepositoryBase
	{
        
        Dictionary<string, string> GetAlertBasicSearchColumns();
        List<SearchColumn> GetAlertSearchColumns();
        List<SearchColumn> GetAlertAdvanceSearchColumns();
        

		Alert GetAlert(System.Int32 AlertId,string SelectClause=null);
		Alert UpdateAlert(Alert entity);
		bool DeleteAlert(System.Int32 AlertId);
		Alert DeleteAlert(Alert entity);
		List<Alert> GetPagedAlert(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Alert> GetAllAlert(string SelectClause=null);
		Alert InsertAlert(Alert entity);
		List<Alert> GetAlertByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
