﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IAssignmentRepositoryBase
	{
        
        Dictionary<string, string> GetAssignmentBasicSearchColumns();
        List<SearchColumn> GetAssignmentSearchColumns();
        List<SearchColumn> GetAssignmentAdvanceSearchColumns();
        

		Assignment GetAssignment(System.Int32 AssignmentId,string SelectClause=null);
		Assignment UpdateAssignment(Assignment entity);
		bool DeleteAssignment(System.Int32 AssignmentId);
		Assignment DeleteAssignment(Assignment entity);
		List<Assignment> GetPagedAssignment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Assignment> GetAllAssignment(string SelectClause=null);
		Assignment InsertAssignment(Assignment entity);
		List<Assignment> GetAssignmentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
