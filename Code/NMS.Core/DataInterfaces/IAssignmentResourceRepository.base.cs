﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IAssignmentResourceRepositoryBase
	{
        
        Dictionary<string, string> GetAssignmentResourceBasicSearchColumns();
        List<SearchColumn> GetAssignmentResourceSearchColumns();
        List<SearchColumn> GetAssignmentResourceAdvanceSearchColumns();
        

		AssignmentResource GetAssignmentResource(System.Int32 AssignmentResourceId,string SelectClause=null);
		AssignmentResource UpdateAssignmentResource(AssignmentResource entity);
		bool DeleteAssignmentResource(System.Int32 AssignmentResourceId);
		AssignmentResource DeleteAssignmentResource(AssignmentResource entity);
		List<AssignmentResource> GetPagedAssignmentResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<AssignmentResource> GetAllAssignmentResource(string SelectClause=null);
		AssignmentResource InsertAssignmentResource(AssignmentResource entity);
		List<AssignmentResource> GetAssignmentResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
