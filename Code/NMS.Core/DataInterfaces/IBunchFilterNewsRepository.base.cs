﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchFilterNewsRepositoryBase
	{
        
        Dictionary<string, string> GetBunchFilterNewsBasicSearchColumns();
        List<SearchColumn> GetBunchFilterNewsSearchColumns();
        List<SearchColumn> GetBunchFilterNewsAdvanceSearchColumns();
        

		
		
		BunchFilterNews GetBunchFilterNews(System.Int32 BunchFilterNewsId,string SelectClause=null);
		BunchFilterNews UpdateBunchFilterNews(BunchFilterNews entity);
		bool DeleteBunchFilterNews(System.Int32 BunchFilterNewsId);
		BunchFilterNews DeleteBunchFilterNews(BunchFilterNews entity);
		List<BunchFilterNews> GetPagedBunchFilterNews(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<BunchFilterNews> GetAllBunchFilterNews(string SelectClause=null);
		BunchFilterNews InsertBunchFilterNews(BunchFilterNews entity);
		List<BunchFilterNews> GetBunchFilterNewsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
