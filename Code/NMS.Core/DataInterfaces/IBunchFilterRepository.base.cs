﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchFilterRepositoryBase
	{
        
        Dictionary<string, string> GetBunchFilterBasicSearchColumns();
        List<SearchColumn> GetBunchFilterSearchColumns();
        List<SearchColumn> GetBunchFilterAdvanceSearchColumns();
        

		List<BunchFilter> GetBunchFilterByFilterId(System.Int32 FilterId,string SelectClause=null);
		List<BunchFilter> GetBunchFilterByBunchId(System.Int32 BunchId,string SelectClause=null);
		BunchFilter GetBunchFilter(System.Int32 BunchFilterId,string SelectClause=null);
		BunchFilter UpdateBunchFilter(BunchFilter entity);
		bool DeleteBunchFilter(System.Int32 BunchFilterId);
		BunchFilter DeleteBunchFilter(BunchFilter entity);
		List<BunchFilter> GetPagedBunchFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<BunchFilter> GetAllBunchFilter(string SelectClause=null);
		BunchFilter InsertBunchFilter(BunchFilter entity);
		List<BunchFilter> GetBunchFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
