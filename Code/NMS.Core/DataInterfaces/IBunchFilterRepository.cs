﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchFilterRepository: IBunchFilterRepositoryBase
	{
        BunchFilter GetByFilterIdAndBunchId(System.Int32 FilterId, System.Int32 BunchId, string SelectClause = null);
        bool DeleteByFilterIdAndBunchId(System.Int32 FilterId, System.Int32 BunchId);
        void InsertBunchFilterNoReturn(BunchFilter entity);
        void UpdateBunchFilterNoReturn(BunchFilter entity);
	}
	
	
}
