﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchNewsRepositoryBase
	{
        
        Dictionary<string, string> GetBunchNewsBasicSearchColumns();
        List<SearchColumn> GetBunchNewsSearchColumns();
        List<SearchColumn> GetBunchNewsAdvanceSearchColumns();
        

		List<BunchNews> GetBunchNewsByBunchId(System.Int32? BunchId,string SelectClause=null);
		List<BunchNews> GetBunchNewsByNewsId(System.Int32? NewsId,string SelectClause=null);
		BunchNews GetBunchNews(System.Int32 BunchNewsId,string SelectClause=null);
		BunchNews UpdateBunchNews(BunchNews entity);
		bool DeleteBunchNews(System.Int32 BunchNewsId);
		BunchNews DeleteBunchNews(BunchNews entity);
		List<BunchNews> GetPagedBunchNews(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<BunchNews> GetAllBunchNews(string SelectClause=null);
		BunchNews InsertBunchNews(BunchNews entity);
        List<BunchNews> GetBunchNewsByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);	

    }
	
	
}
