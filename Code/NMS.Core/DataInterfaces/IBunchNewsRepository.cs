﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchNewsRepository: IBunchNewsRepositoryBase
	{
        bool DeleteByBunchId(System.Int32 BunchId);
        List<BunchNews> GetByBunchIdAndNewsid(System.Int32 BunchId, System.Int32 NewsId, string SelectClause = null);
        bool DeleteByNewsId(System.Int32 NewsId);
	}
	
	
}
