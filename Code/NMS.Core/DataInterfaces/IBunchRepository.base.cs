﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{

    public interface IBunchRepositoryBase
    {

        Dictionary<string, string> GetBunchBasicSearchColumns();
        List<SearchColumn> GetBunchSearchColumns();
        List<SearchColumn> GetBunchAdvanceSearchColumns();


        Bunch GetBunch(System.Int32 BunchId, string SelectClause = null);
        Bunch UpdateBunch(Bunch entity);
        bool DeleteBunch(System.Int32 BunchId);
        Bunch DeleteBunch(Bunch entity);
        List<Bunch> GetPagedBunch(string orderByClause, int pageSize, int startIndex, out int count, List<SearchColumn> searchColumns, string SelectClause = null);
        List<Bunch> GetAllBunch(string SelectClause = null);
        Bunch InsertBunch(Bunch entity);
        List<Bunch> GetBunchByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);
        void InsertBunchNoReturn(Bunch entity);
    }



}
