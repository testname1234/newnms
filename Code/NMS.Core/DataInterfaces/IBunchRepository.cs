﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{

    public interface IBunchRepository : IBunchRepositoryBase
    {
        Bunch GetBunchByGuid(System.String BunchGuid, string SelectClause = null);
        bool DeleteBunchByGuid(System.String Guid);
        void InsertBunchWIthPK(Bunch entity);
        void UpdateBunchNoReturn(Bunch entity);
    }


}
