﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchResourceRepositoryBase
	{
        
        Dictionary<string, string> GetBunchResourceBasicSearchColumns();
        List<SearchColumn> GetBunchResourceSearchColumns();
        List<SearchColumn> GetBunchResourceAdvanceSearchColumns();
        

		List<BunchResource> GetBunchResourceByBunchId(System.Int32 BunchId,string SelectClause=null);
		List<BunchResource> GetBunchResourceByResourceId(System.Int32 ResourceId,string SelectClause=null);
		BunchResource GetBunchResource(System.Int32 BunchResourceId,string SelectClause=null);
		BunchResource UpdateBunchResource(BunchResource entity);
		bool DeleteBunchResource(System.Int32 BunchResourceId);
		BunchResource DeleteBunchResource(BunchResource entity);
		List<BunchResource> GetPagedBunchResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<BunchResource> GetAllBunchResource(string SelectClause=null);
		BunchResource InsertBunchResource(BunchResource entity);
		List<BunchResource> GetBunchResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
