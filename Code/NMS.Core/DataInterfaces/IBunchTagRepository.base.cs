﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchTagRepositoryBase
	{
        
        Dictionary<string, string> GetBunchTagBasicSearchColumns();
        List<SearchColumn> GetBunchTagSearchColumns();
        List<SearchColumn> GetBunchTagAdvanceSearchColumns();
        

		List<BunchTag> GetBunchTagByBunchId(System.Int32 BunchId,string SelectClause=null);
		List<BunchTag> GetBunchTagByTagId(System.Int32 TagId,string SelectClause=null);
		BunchTag GetBunchTag(System.Int32 BunchTagId,string SelectClause=null);
		BunchTag UpdateBunchTag(BunchTag entity);
		bool DeleteBunchTag(System.Int32 BunchTagId);
		BunchTag DeleteBunchTag(BunchTag entity);
		List<BunchTag> GetPagedBunchTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<BunchTag> GetAllBunchTag(string SelectClause=null);
		BunchTag InsertBunchTag(BunchTag entity);
		List<BunchTag> GetBunchTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
