﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IBunchTagRepository: IBunchTagRepositoryBase
	{
        BunchTag GetByBunchIdAndTagId(System.Int32 BunchId, System.Int32 TagId, string SelectClause = null);
        bool InsertBunchTagNoReturn(BunchTag entity);
	}
	
	
}
