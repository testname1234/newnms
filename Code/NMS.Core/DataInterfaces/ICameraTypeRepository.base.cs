﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICameraTypeRepositoryBase
	{
        
        Dictionary<string, string> GetCameraTypeBasicSearchColumns();
        List<SearchColumn> GetCameraTypeSearchColumns();
        List<SearchColumn> GetCameraTypeAdvanceSearchColumns();
        

		CameraType GetCameraType(System.Int32 CameraTypeId,string SelectClause=null);
		CameraType UpdateCameraType(CameraType entity);
		bool DeleteCameraType(System.Int32 CameraTypeId);
		CameraType DeleteCameraType(CameraType entity);
		List<CameraType> GetPagedCameraType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CameraType> GetAllCameraType(string SelectClause=null);
		CameraType InsertCameraType(CameraType entity);
		List<CameraType> GetCameraTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
