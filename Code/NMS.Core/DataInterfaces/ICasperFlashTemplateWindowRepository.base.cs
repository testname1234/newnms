﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperFlashTemplateWindowRepositoryBase
	{
        
        Dictionary<string, string> GetCasperFlashTemplateWindowBasicSearchColumns();
        List<SearchColumn> GetCasperFlashTemplateWindowSearchColumns();
        List<SearchColumn> GetCasperFlashTemplateWindowAdvanceSearchColumns();
        

		CasperFlashTemplateWindow GetCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId,string SelectClause=null);
		CasperFlashTemplateWindow UpdateCasperFlashTemplateWindow(CasperFlashTemplateWindow entity);
		bool DeleteCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId);
		CasperFlashTemplateWindow DeleteCasperFlashTemplateWindow(CasperFlashTemplateWindow entity);
		List<CasperFlashTemplateWindow> GetPagedCasperFlashTemplateWindow(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CasperFlashTemplateWindow> GetAllCasperFlashTemplateWindow(string SelectClause=null);
		CasperFlashTemplateWindow InsertCasperFlashTemplateWindow(CasperFlashTemplateWindow entity);
		List<CasperFlashTemplateWindow> GetCasperFlashTemplateWindowByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
