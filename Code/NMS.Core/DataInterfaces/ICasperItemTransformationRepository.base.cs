﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperItemTransformationRepositoryBase
	{
        
        Dictionary<string, string> GetCasperItemTransformationBasicSearchColumns();
        List<SearchColumn> GetCasperItemTransformationSearchColumns();
        List<SearchColumn> GetCasperItemTransformationAdvanceSearchColumns();
        

		CasperItemTransformation GetCasperItemTransformation(System.Int32 CasperItemTransformationId,string SelectClause=null);
		CasperItemTransformation UpdateCasperItemTransformation(CasperItemTransformation entity);
		bool DeleteCasperItemTransformation(System.Int32 CasperItemTransformationId);
		CasperItemTransformation DeleteCasperItemTransformation(CasperItemTransformation entity);
		List<CasperItemTransformation> GetPagedCasperItemTransformation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CasperItemTransformation> GetAllCasperItemTransformation(string SelectClause=null);
		CasperItemTransformation InsertCasperItemTransformation(CasperItemTransformation entity);
		List<CasperItemTransformation> GetCasperItemTransformationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
