﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{	
	public interface ICasperItemTransformationRepository: ICasperItemTransformationRepositoryBase
	{
        CasperItemTransformation GetCasperItemTransformationByCasperTemplateItemId(System.Int32 casperTemplateItemId, string SelectClause = null);
        CasperItemTransformation GetByFlashtemplatewindowid(System.Int32 Flashtemplatewindowid, string SelectClause = null);
	}

}
