﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateItemRepositoryBase
	{
        
        Dictionary<string, string> GetCasperTemplateItemBasicSearchColumns();
        List<SearchColumn> GetCasperTemplateItemSearchColumns();
        List<SearchColumn> GetCasperTemplateItemAdvanceSearchColumns();
        

		CasperTemplateItem GetCasperTemplateItem(System.Int32 CasperTemplatItemId,string SelectClause=null);
		CasperTemplateItem UpdateCasperTemplateItem(CasperTemplateItem entity);
		bool DeleteCasperTemplateItem(System.Int32 CasperTemplatItemId);
		CasperTemplateItem DeleteCasperTemplateItem(CasperTemplateItem entity);
		List<CasperTemplateItem> GetPagedCasperTemplateItem(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CasperTemplateItem> GetAllCasperTemplateItem(string SelectClause=null);
		CasperTemplateItem InsertCasperTemplateItem(CasperTemplateItem entity);
		List<CasperTemplateItem> GetCasperTemplateItemByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
