﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateItemRepository: ICasperTemplateItemRepositoryBase
	{
        List<CasperTemplateItem> GetCasperTemplateItemByCasperTemplateId(System.Int32 casperTemplateId, string SelectClause = null);

        CasperTemplateItem GetCasperTemplateItemByCasperTemplateIdAndType(System.Int32 casperTemplateId, string type, string SelectClause = null);

        CasperTemplateItem GetCasperTemplateItemByitemType(string type, string SelectClause = null);

        CasperTemplateItem GetByFlashTemplateWindowId(int FlashTemplateWindowId, string SelectClause = null);

        CasperTemplateItem GetByItemName(string name, string SelectClause = null);

        CasperTemplateItem GetByLabel(string name, string SelectClause = null);
    
    }
	
}
