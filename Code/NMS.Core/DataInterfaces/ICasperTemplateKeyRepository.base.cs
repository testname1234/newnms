﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateKeyRepositoryBase
	{
        
        Dictionary<string, string> GetCasperTemplateKeyBasicSearchColumns();
        List<SearchColumn> GetCasperTemplateKeySearchColumns();
        List<SearchColumn> GetCasperTemplateKeyAdvanceSearchColumns();
        

		CasperTemplateKey GetCasperTemplateKey(System.Int32 CasperTemplateKeyId,string SelectClause=null);
		CasperTemplateKey UpdateCasperTemplateKey(CasperTemplateKey entity);
		bool DeleteCasperTemplateKey(System.Int32 CasperTemplateKeyId);
		CasperTemplateKey DeleteCasperTemplateKey(CasperTemplateKey entity);
		List<CasperTemplateKey> GetPagedCasperTemplateKey(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CasperTemplateKey> GetAllCasperTemplateKey(string SelectClause=null);
		CasperTemplateKey InsertCasperTemplateKey(CasperTemplateKey entity);
		List<CasperTemplateKey> GetCasperTemplateKeyByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
