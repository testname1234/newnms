﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateKeyRepository: ICasperTemplateKeyRepositoryBase
	{
        CasperTemplateKey GetCasperTemplateKey(System.Int32 FlashTemplateKeyId, string SelectClause = null);
	}
	
	
}
