﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateRepositoryBase
	{
        
        Dictionary<string, string> GetCasperTemplateBasicSearchColumns();
        List<SearchColumn> GetCasperTemplateSearchColumns();
        List<SearchColumn> GetCasperTemplateAdvanceSearchColumns();
        

		CasperTemplate GetCasperTemplate(System.Int32 CasperTemlateId,string SelectClause=null);
		CasperTemplate UpdateCasperTemplate(CasperTemplate entity);
		bool DeleteCasperTemplate(System.Int32 CasperTemlateId);
		CasperTemplate DeleteCasperTemplate(CasperTemplate entity);
		List<CasperTemplate> GetPagedCasperTemplate(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CasperTemplate> GetAllCasperTemplate(string SelectClause=null);
		CasperTemplate InsertCasperTemplate(CasperTemplate entity);
		List<CasperTemplate> GetCasperTemplateByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
