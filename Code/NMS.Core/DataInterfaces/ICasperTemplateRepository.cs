﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateRepository: ICasperTemplateRepositoryBase
	{
        CasperTemplate GetCasperTemplateByTemplateName(string Template, string SelectClause = null);
        CasperTemplate GetBYFlashTemplateId(int FlashTemplateId, string SelectClause = null);
	}

}
