﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateTypeRepositoryBase
	{
        
        Dictionary<string, string> GetCasperTemplateTypeBasicSearchColumns();
        List<SearchColumn> GetCasperTemplateTypeSearchColumns();
        List<SearchColumn> GetCasperTemplateTypeAdvanceSearchColumns();
        

		CasperTemplateType GetCasperTemplateType(System.Int32 CasperTemplateTypeId,string SelectClause=null);
		CasperTemplateType UpdateCasperTemplateType(CasperTemplateType entity);
		bool DeleteCasperTemplateType(System.Int32 CasperTemplateTypeId);
		CasperTemplateType DeleteCasperTemplateType(CasperTemplateType entity);
		List<CasperTemplateType> GetPagedCasperTemplateType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CasperTemplateType> GetAllCasperTemplateType(string SelectClause=null);
		CasperTemplateType InsertCasperTemplateType(CasperTemplateType entity);
		List<CasperTemplateType> GetCasperTemplateTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
