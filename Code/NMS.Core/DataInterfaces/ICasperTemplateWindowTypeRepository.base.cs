﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICasperTemplateWindowTypeRepositoryBase
	{
        
        Dictionary<string, string> GetCasperTemplateWindowTypeBasicSearchColumns();
        List<SearchColumn> GetCasperTemplateWindowTypeSearchColumns();
        List<SearchColumn> GetCasperTemplateWindowTypeAdvanceSearchColumns();
        

		CasperTemplateWindowType GetCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId,string SelectClause=null);
		CasperTemplateWindowType UpdateCasperTemplateWindowType(CasperTemplateWindowType entity);
		bool DeleteCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId);
		CasperTemplateWindowType DeleteCasperTemplateWindowType(CasperTemplateWindowType entity);
		List<CasperTemplateWindowType> GetPagedCasperTemplateWindowType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CasperTemplateWindowType> GetAllCasperTemplateWindowType(string SelectClause=null);
		CasperTemplateWindowType InsertCasperTemplateWindowType(CasperTemplateWindowType entity);
		List<CasperTemplateWindowType> GetCasperTemplateWindowTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
