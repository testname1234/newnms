﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICategoryAliasRepositoryBase
	{
        
        Dictionary<string, string> GetCategoryAliasBasicSearchColumns();
        List<SearchColumn> GetCategoryAliasSearchColumns();
        List<SearchColumn> GetCategoryAliasAdvanceSearchColumns();
        

		List<CategoryAlias> GetCategoryAliasByCategoryId(System.Int32 CategoryId,string SelectClause=null);
		CategoryAlias GetCategoryAlias(System.Int32 CategoryAliasId,string SelectClause=null);
		CategoryAlias UpdateCategoryAlias(CategoryAlias entity);
		bool DeleteCategoryAlias(System.Int32 CategoryAliasId);
		CategoryAlias DeleteCategoryAlias(CategoryAlias entity);
		List<CategoryAlias> GetPagedCategoryAlias(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CategoryAlias> GetAllCategoryAlias(string SelectClause=null);
		CategoryAlias InsertCategoryAlias(CategoryAlias entity);
		List<CategoryAlias> GetCategoryAliasByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
