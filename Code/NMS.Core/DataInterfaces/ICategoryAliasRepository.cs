﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICategoryAliasRepository: ICategoryAliasRepositoryBase
	{
		CategoryAlias GetByAlias(string alias);
        bool Exist(CategoryAlias entity);
	}
	
	
}
