﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICategoryRepositoryBase
	{
        
        Dictionary<string, string> GetCategoryBasicSearchColumns();
        List<SearchColumn> GetCategorySearchColumns();
        List<SearchColumn> GetCategoryAdvanceSearchColumns();
        

		Category GetCategory(System.Int32 CategoryId,string SelectClause=null);
		Category UpdateCategory(Category entity);
		bool DeleteCategory(System.Int32 CategoryId);
		Category DeleteCategory(Category entity);
		List<Category> GetPagedCategory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Category> GetAllCategory(string SelectClause=null);
		Category InsertCategory(Category entity);
		List<Category> GetCategoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);

    
    
    }
	
	
}
