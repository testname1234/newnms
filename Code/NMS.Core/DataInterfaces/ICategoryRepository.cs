﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICategoryRepository: ICategoryRepositoryBase
	{
        Category GetCategoryCreateIfNotExist(Category category);
        bool Exist(Category category);
        Category GetCategoryByName(string name);

        List<Category> GetCategoryByTerm(string term);

        CategoryAlias GetByAlias(string alias);
        Category InsertCategoryIfNotExists(Category category);
        List<Category> GetByDate(DateTime LastUpdateDate);
        
    }
	
	
}
