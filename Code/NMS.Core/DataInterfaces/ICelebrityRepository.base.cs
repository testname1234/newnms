﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICelebrityRepositoryBase
	{
        
        Dictionary<string, string> GetCelebrityBasicSearchColumns();
        List<SearchColumn> GetCelebritySearchColumns();
        List<SearchColumn> GetCelebrityAdvanceSearchColumns();
        

		List<Celebrity> GetCelebrityByCategoryId(System.Int32 CategoryId,string SelectClause=null);
		Celebrity GetCelebrity(System.Int32 CelebrityId,string SelectClause=null);
		Celebrity UpdateCelebrity(Celebrity entity);
		bool DeleteCelebrity(System.Int32 CelebrityId);
		Celebrity DeleteCelebrity(Celebrity entity);
		List<Celebrity> GetPagedCelebrity(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Celebrity> GetAllCelebrity(string SelectClause=null);
		Celebrity InsertCelebrity(Celebrity entity);
		List<Celebrity> GetCelebrityByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
