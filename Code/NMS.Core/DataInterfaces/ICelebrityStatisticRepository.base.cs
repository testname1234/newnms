﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICelebrityStatisticRepositoryBase
	{
        
        Dictionary<string, string> GetCelebrityStatisticBasicSearchColumns();
        List<SearchColumn> GetCelebrityStatisticSearchColumns();
        List<SearchColumn> GetCelebrityStatisticAdvanceSearchColumns();
        

		List<CelebrityStatistic> GetCelebrityStatisticByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		CelebrityStatistic GetCelebrityStatistic(System.Int32 CelebrityStatisticId,string SelectClause=null);
		CelebrityStatistic UpdateCelebrityStatistic(CelebrityStatistic entity);
		bool DeleteCelebrityStatistic(System.Int32 CelebrityStatisticId);
		CelebrityStatistic DeleteCelebrityStatistic(CelebrityStatistic entity);
		List<CelebrityStatistic> GetPagedCelebrityStatistic(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CelebrityStatistic> GetAllCelebrityStatistic(string SelectClause=null);
		CelebrityStatistic InsertCelebrityStatistic(CelebrityStatistic entity);
		List<CelebrityStatistic> GetCelebrityStatisticByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
