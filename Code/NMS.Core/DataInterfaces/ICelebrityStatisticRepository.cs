﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICelebrityStatisticRepository: ICelebrityStatisticRepositoryBase
	{
        bool DeleteCelebrityStatisticByEpisodeId(System.Int32 episodeId);
        bool UpdateCelebrityStatistic(System.Int32 celebrityId);
	}
	
	
}
