﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICelebrityTypeRepositoryBase
	{
        
        Dictionary<string, string> GetCelebrityTypeBasicSearchColumns();
        List<SearchColumn> GetCelebrityTypeSearchColumns();
        List<SearchColumn> GetCelebrityTypeAdvanceSearchColumns();
        

		CelebrityType GetCelebrityType(System.Int32 CelebrityTypeId,string SelectClause=null);
		CelebrityType UpdateCelebrityType(CelebrityType entity);
		bool DeleteCelebrityType(System.Int32 CelebrityTypeId);
		CelebrityType DeleteCelebrityType(CelebrityType entity);
		List<CelebrityType> GetPagedCelebrityType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CelebrityType> GetAllCelebrityType(string SelectClause=null);
		CelebrityType InsertCelebrityType(CelebrityType entity);
		List<CelebrityType> GetCelebrityTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
