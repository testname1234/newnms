﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICelebrityVoiceArtistRepositoryBase
	{
        
        Dictionary<string, string> GetCelebrityVoiceArtistBasicSearchColumns();
        List<SearchColumn> GetCelebrityVoiceArtistSearchColumns();
        List<SearchColumn> GetCelebrityVoiceArtistAdvanceSearchColumns();
        

		List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByVoiceArtistId(System.Int32? VoiceArtistId,string SelectClause=null);
		CelebrityVoiceArtist GetCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId,string SelectClause=null);
		CelebrityVoiceArtist UpdateCelebrityVoiceArtist(CelebrityVoiceArtist entity);
		bool DeleteCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId);
		CelebrityVoiceArtist DeleteCelebrityVoiceArtist(CelebrityVoiceArtist entity);
		List<CelebrityVoiceArtist> GetPagedCelebrityVoiceArtist(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CelebrityVoiceArtist> GetAllCelebrityVoiceArtist(string SelectClause=null);
		CelebrityVoiceArtist InsertCelebrityVoiceArtist(CelebrityVoiceArtist entity);
		List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
