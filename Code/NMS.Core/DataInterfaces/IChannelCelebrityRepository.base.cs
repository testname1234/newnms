﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IChannelCelebrityRepositoryBase
	{
        
        Dictionary<string, string> GetChannelCelebrityBasicSearchColumns();
        List<SearchColumn> GetChannelCelebritySearchColumns();
        List<SearchColumn> GetChannelCelebrityAdvanceSearchColumns();
        

		List<ChannelCelebrity> GetChannelCelebrityByChannelId(System.Int32 ChannelId,string SelectClause=null);
		List<ChannelCelebrity> GetChannelCelebrityByCelebrityId(System.Int32 CelebrityId,string SelectClause=null);
		List<ChannelCelebrity> GetChannelCelebrityByCelebrityTypeId(System.Int32 CelebrityTypeId,string SelectClause=null);
		ChannelCelebrity GetChannelCelebrity(System.Int32 ChannelCelebrityId,string SelectClause=null);
		ChannelCelebrity UpdateChannelCelebrity(ChannelCelebrity entity);
		bool DeleteChannelCelebrity(System.Int32 ChannelCelebrityId);
		ChannelCelebrity DeleteChannelCelebrity(ChannelCelebrity entity);
		List<ChannelCelebrity> GetPagedChannelCelebrity(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ChannelCelebrity> GetAllChannelCelebrity(string SelectClause=null);
		ChannelCelebrity InsertChannelCelebrity(ChannelCelebrity entity);
		List<ChannelCelebrity> GetChannelCelebrityByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
