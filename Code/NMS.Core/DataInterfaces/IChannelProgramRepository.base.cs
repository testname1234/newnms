﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IChannelProgramRepositoryBase
	{
        
        Dictionary<string, string> GetChannelProgramBasicSearchColumns();
        List<SearchColumn> GetChannelProgramSearchColumns();
        List<SearchColumn> GetChannelProgramAdvanceSearchColumns();
        

		List<ChannelProgram> GetChannelProgramByChannelId(System.Int32 ChannelId,string SelectClause=null);
		List<ChannelProgram> GetChannelProgramByProgramId(System.Int32 ProgramId,string SelectClause=null);
		ChannelProgram GetChannelProgram(System.Int32 ChannelProgramId,string SelectClause=null);
		ChannelProgram UpdateChannelProgram(ChannelProgram entity);
		bool DeleteChannelProgram(System.Int32 ChannelProgramId);
		ChannelProgram DeleteChannelProgram(ChannelProgram entity);
		List<ChannelProgram> GetPagedChannelProgram(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ChannelProgram> GetAllChannelProgram(string SelectClause=null);
		ChannelProgram InsertChannelProgram(ChannelProgram entity);
		List<ChannelProgram> GetChannelProgramByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
