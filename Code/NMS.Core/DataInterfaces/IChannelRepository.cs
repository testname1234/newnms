﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IChannelRepository: IChannelRepositoryBase
	{
        List<Channel> GetChannelsByOtherChannelBit(bool flag);
        List<Channel> GetChannelsByUserId(int userId);
	}
	
	
}
