﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IChannelVideoRepositoryBase
	{
        
        Dictionary<string, string> GetChannelVideoBasicSearchColumns();
        List<SearchColumn> GetChannelVideoSearchColumns();
        List<SearchColumn> GetChannelVideoAdvanceSearchColumns();
        

		ChannelVideo GetChannelVideo(System.Int32 ChannelVideoId,string SelectClause=null);
		ChannelVideo UpdateChannelVideo(ChannelVideo entity);
		bool DeleteChannelVideo(System.Int32 ChannelVideoId);
		ChannelVideo DeleteChannelVideo(ChannelVideo entity);
		List<ChannelVideo> GetPagedChannelVideo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ChannelVideo> GetAllChannelVideo(string SelectClause=null);
		ChannelVideo InsertChannelVideo(ChannelVideo entity);
		List<ChannelVideo> GetChannelVideoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
