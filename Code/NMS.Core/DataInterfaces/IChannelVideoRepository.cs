﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using System.Data.SqlClient;

namespace NMS.Core.DataInterfaces
{
		
	public interface IChannelVideoRepository: IChannelVideoRepositoryBase
	{
        bool ChannelVideoExist(int channelId, DateTime dt);

        List<ChannelVideo> GetAllChannelVideosBetweenDates(DateTime dtFrom, DateTime dtTo);

        void MarkIsProcessed(int id);
        //void InsertChannelVideo(ChannelVideo entity, SqlConnection Consql);

        ChannelVideo InsertChannelVideo(ChannelVideo entity, string connectionString);

        List<ChannelVideo> GetDistinctPrograms();

        ChannelVideo GetLastChannelVideo(int ChannelId);

        ChannelVideo GetChannelVideoByStartEndTime(DateTime StartDate, DateTime EndTime, int ChannelId);
	}
	
	
}
