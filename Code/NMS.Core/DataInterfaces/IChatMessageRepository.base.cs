﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IChatMessageRepositoryBase
	{
        
        Dictionary<string, string> GetChatMessageBasicSearchColumns();
        List<SearchColumn> GetChatMessageSearchColumns();
        List<SearchColumn> GetChatMessageAdvanceSearchColumns();
        

		ChatMessage GetChatMessage(System.Int32 MessageId,string SelectClause=null);
		ChatMessage UpdateChatMessage(ChatMessage entity);
		bool DeleteChatMessage(System.Int32 MessageId);
		ChatMessage DeleteChatMessage(ChatMessage entity);
		List<ChatMessage> GetPagedChatMessage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ChatMessage> GetAllChatMessage(string SelectClause=null);
		ChatMessage InsertChatMessage(ChatMessage entity);
		List<ChatMessage> GetChatMessageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
