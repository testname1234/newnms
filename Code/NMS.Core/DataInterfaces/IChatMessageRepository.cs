﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IChatMessageRepository: IChatMessageRepositoryBase
	{

        List<ChatMessage> GetChatMessagesForUser(int userId, DateTime date);

        List<ChatMessage> GetAllGroupMessages(DateTime date);
    }
	
	
}
