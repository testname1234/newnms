﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICheckListRepositoryBase
	{
        
        Dictionary<string, string> GetCheckListBasicSearchColumns();
        List<SearchColumn> GetCheckListSearchColumns();
        List<SearchColumn> GetCheckListAdvanceSearchColumns();
        

		CheckList GetCheckList(System.Int32 ChecklistId,string SelectClause=null);
		CheckList UpdateCheckList(CheckList entity);
		bool DeleteCheckList(System.Int32 ChecklistId);
		CheckList DeleteCheckList(CheckList entity);
		List<CheckList> GetPagedCheckList(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<CheckList> GetAllCheckList(string SelectClause=null);
		CheckList InsertCheckList(CheckList entity);
		List<CheckList> GetCheckListByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
