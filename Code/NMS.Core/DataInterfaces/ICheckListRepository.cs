﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICheckListRepository: ICheckListRepositoryBase
	{
        List<CheckList> GetNameErrorMessageCheckList();
        void UpdatePriority(string Name, bool Priority);
        List<CheckList> GetPriority();
        int Selection(string Name);
        CheckList InsertResult(CheckList entity);
        CheckList UpdateResult(CheckList entity);
	}
	
	
}
