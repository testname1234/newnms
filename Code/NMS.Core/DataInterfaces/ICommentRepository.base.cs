﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICommentRepositoryBase
	{
        
        Dictionary<string, string> GetCommentBasicSearchColumns();
        List<SearchColumn> GetCommentSearchColumns();
        List<SearchColumn> GetCommentAdvanceSearchColumns();
        

		Comment GetComment(System.Int32 Commentid,string SelectClause=null);
		Comment UpdateComment(Comment entity);
		bool DeleteComment(System.Int32 Commentid);
		Comment DeleteComment(Comment entity);
		List<Comment> GetPagedComment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Comment> GetAllComment(string SelectClause=null);
		Comment InsertComment(Comment entity);
		List<Comment> GetCommentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
