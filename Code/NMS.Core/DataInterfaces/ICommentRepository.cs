﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ICommentRepository: ICommentRepositoryBase
	{
        Comment GetByGuid(string Guid, string SelectClause = null);

        Comment GetByNewsGuid(string NewsGuid, string SelectClause = null);
		
	}
	
	
}
