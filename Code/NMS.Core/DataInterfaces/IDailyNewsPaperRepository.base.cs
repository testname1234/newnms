﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IDailyNewsPaperRepositoryBase
	{
        
        Dictionary<string, string> GetDailyNewsPaperBasicSearchColumns();
        List<SearchColumn> GetDailyNewsPaperSearchColumns();
        List<SearchColumn> GetDailyNewsPaperAdvanceSearchColumns();
        

		List<DailyNewsPaper> GetDailyNewsPaperByNewsPaperId(System.Int32 NewsPaperId,string SelectClause=null);
		DailyNewsPaper GetDailyNewsPaper(System.Int32 DailyNewsPaperId,string SelectClause=null);
		DailyNewsPaper UpdateDailyNewsPaper(DailyNewsPaper entity);
		bool DeleteDailyNewsPaper(System.Int32 DailyNewsPaperId);
		DailyNewsPaper DeleteDailyNewsPaper(DailyNewsPaper entity);
		List<DailyNewsPaper> GetPagedDailyNewsPaper(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<DailyNewsPaper> GetAllDailyNewsPaper(string SelectClause=null);
		DailyNewsPaper InsertDailyNewsPaper(DailyNewsPaper entity);
		List<DailyNewsPaper> GetDailyNewsPaperByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
