﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IDailyNewsPaperRepository: IDailyNewsPaperRepositoryBase
	{
        List<DailyNewsPaper> GetDailyNewsPaperByDate(System.DateTime fromDate, System.DateTime toDate, string SelectClause = null);
        List<DailyNewsPaper> GeByDateAndNewsPaperId(System.DateTime fromDate, System.DateTime toDate, int NewsPaperId, string SelectClause = null);
	}
	
	
}
