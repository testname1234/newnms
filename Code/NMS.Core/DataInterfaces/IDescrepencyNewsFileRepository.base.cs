﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IDescrepencyNewsFileRepositoryBase
	{
        
        Dictionary<string, string> GetDescrepencyNewsFileBasicSearchColumns();
        List<SearchColumn> GetDescrepencyNewsFileSearchColumns();
        List<SearchColumn> GetDescrepencyNewsFileAdvanceSearchColumns();
        

		DescrepencyNewsFile GetDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId,string SelectClause=null);
		DescrepencyNewsFile UpdateDescrepencyNewsFile(DescrepencyNewsFile entity);
		bool DeleteDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId);
		DescrepencyNewsFile DeleteDescrepencyNewsFile(DescrepencyNewsFile entity);
		List<DescrepencyNewsFile> GetPagedDescrepencyNewsFile(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<DescrepencyNewsFile> GetAllDescrepencyNewsFile(string SelectClause=null);
		DescrepencyNewsFile InsertDescrepencyNewsFile(DescrepencyNewsFile entity);
		List<DescrepencyNewsFile> GetDescrepencyNewsFileByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
