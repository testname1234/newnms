﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IDescrepencyNewsFileRepository: IDescrepencyNewsFileRepositoryBase
	{
        List<DescrepencyNewsFile> GetAllDescrepencyByStatus(int DescreypencyType);
        bool UpdateDescrepencyNewsByValue(int DescrepencyType, string DescrepencyValue);
        List<DescrepencyNewsFile> GetAllByDescrepancyType(int DescreypencyType, string DescrepencyValue);
        bool UpdateNewsFileId(int id, int NewFileId);
        List<DescrepencyNewsFile> GetDiscrepencyBystatus(int descrepancyTypeId, int Status);
        bool UpdateDescrepencyCategory(string id, int CategoryId);
        List<int> GetDescrepencyNewsFileIdByDescrepencyValue(string DescrepencyValue);

        bool UpdateDescrepencyValueByDescrepencyNewsFileId(string DescrepencyNewsFileId, string DescrepencyValue);
    }


}
