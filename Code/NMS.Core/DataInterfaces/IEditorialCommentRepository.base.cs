﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEditorialCommentRepositoryBase
	{
        
        Dictionary<string, string> GetEditorialCommentBasicSearchColumns();
        List<SearchColumn> GetEditorialCommentSearchColumns();
        List<SearchColumn> GetEditorialCommentAdvanceSearchColumns();
        

		EditorialComment GetEditorialComment(System.Int32 EditorialCommentId,string SelectClause=null);
		EditorialComment UpdateEditorialComment(EditorialComment entity);
		bool DeleteEditorialComment(System.Int32 EditorialCommentId);
		EditorialComment DeleteEditorialComment(EditorialComment entity);
		List<EditorialComment> GetPagedEditorialComment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<EditorialComment> GetAllEditorialComment(string SelectClause=null);
		EditorialComment InsertEditorialComment(EditorialComment entity);
		List<EditorialComment> GetEditorialCommentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
