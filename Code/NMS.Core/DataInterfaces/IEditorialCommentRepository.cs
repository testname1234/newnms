﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEditorialCommentRepository: IEditorialCommentRepositoryBase
	{
        List<EditorialComment> GetEditorialCommentByNewsFileId(int NewsFileId);
        List<EditorialComment> GetEditorialCommentBySlotId(int slotId);
        List<EditorialComment> GetCommentsBySlotIdAndLastUpdateDate(DateTime lastUpdateDate, int slotId);
        List<EditorialComment> GetEditorialCommentByTickerId(int tickerId, int languageId);
    }
	
	
}
