﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEpisodePcrMosRepositoryBase
	{
        
        Dictionary<string, string> GetEpisodePcrMosBasicSearchColumns();
        List<SearchColumn> GetEpisodePcrMosSearchColumns();
        List<SearchColumn> GetEpisodePcrMosAdvanceSearchColumns();
        

		EpisodePcrMos GetEpisodePcrMos(System.Int32 EpisodePcrMosId,string SelectClause=null);
		EpisodePcrMos UpdateEpisodePcrMos(EpisodePcrMos entity);
		bool DeleteEpisodePcrMos(System.Int32 EpisodePcrMosId);
		EpisodePcrMos DeleteEpisodePcrMos(EpisodePcrMos entity);
		List<EpisodePcrMos> GetPagedEpisodePcrMos(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<EpisodePcrMos> GetAllEpisodePcrMos(string SelectClause=null);
		EpisodePcrMos InsertEpisodePcrMos(EpisodePcrMos entity);
		List<EpisodePcrMos> GetEpisodePcrMosByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
