﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEpisodePcrMosRepository: IEpisodePcrMosRepositoryBase
	{
        List<EpisodePcrMos> GetEpisodePcrMosBycasperTemplateId(int casperTemplateId, int SlotScreenTemplateId, int SlotId, string SelectClause = null);
        
	}
	
	
}
