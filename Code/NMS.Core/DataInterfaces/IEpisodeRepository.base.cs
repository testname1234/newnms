﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEpisodeRepositoryBase
	{
        
        Dictionary<string, string> GetEpisodeBasicSearchColumns();
        List<SearchColumn> GetEpisodeSearchColumns();
        List<SearchColumn> GetEpisodeAdvanceSearchColumns();
        

		List<Episode> GetEpisodeByProgramId(System.Int32 ProgramId,string SelectClause=null);
		Episode GetEpisode(System.Int32 EpisodeId,string SelectClause=null);
		Episode UpdateEpisode(Episode entity);
		bool DeleteEpisode(System.Int32 EpisodeId);
		Episode DeleteEpisode(Episode entity);
		List<Episode> GetPagedEpisode(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Episode> GetAllEpisode(string SelectClause=null);
		Episode InsertEpisode(Episode entity);
		List<Episode> GetEpisodeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
