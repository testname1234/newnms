﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Enums;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEpisodeRepository: IEpisodeRepositoryBase
	{
        List<Episode> GetEpisodeByProgramIdAndDate(int programId, DateTime date);

        List<Episode> GetProgramEpisodeByDate(int userId, Enums.TeamRoles teamRoles, DateTime dateTime, DateTime lastUpdateDate);

        Episode GetEpisodeBySlotId(System.Int32 SlotId);

        Episode GetEpisodeBySegmentId(System.Int32 SegmentId);
        List<Episode> GetEpisodesByProgramType(ProgramTypes programType, string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null);

        List<int> GetProgramSetMappingID(int EpisodeId);
        Episode GetEpisodeWithProgramName(System.Int32 EpisodeId);

        List<NMS.Core.DataTransfer.Episode.GetOutput> GetEpisodeDetailByProgram(List<int> lstprogramid, DateTime from, DateTime to);
    }
	
	
}
