﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEventInfoRepositoryBase
	{
        
        Dictionary<string, string> GetEventInfoBasicSearchColumns();
        List<SearchColumn> GetEventInfoSearchColumns();
        List<SearchColumn> GetEventInfoAdvanceSearchColumns();
        

		EventInfo GetEventInfo(System.Int32 EventInfoId,string SelectClause=null);
		EventInfo UpdateEventInfo(EventInfo entity);
		bool DeleteEventInfo(System.Int32 EventInfoId);
		EventInfo DeleteEventInfo(EventInfo entity);
		List<EventInfo> GetPagedEventInfo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<EventInfo> GetAllEventInfo(string SelectClause=null);
		EventInfo InsertEventInfo(EventInfo entity);
		List<EventInfo> GetEventInfoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
