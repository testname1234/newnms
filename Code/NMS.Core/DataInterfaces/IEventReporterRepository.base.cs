﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEventReporterRepositoryBase
	{
        
        Dictionary<string, string> GetEventReporterBasicSearchColumns();
        List<SearchColumn> GetEventReporterSearchColumns();
        List<SearchColumn> GetEventReporterAdvanceSearchColumns();
        

		List<EventReporter> GetEventReporterByNewsFileId(System.Int32? NewsFileId,string SelectClause=null);
		EventReporter GetEventReporter(System.Int32 EventReporterId,string SelectClause=null);
		EventReporter UpdateEventReporter(EventReporter entity);
		bool DeleteEventReporter(System.Int32 EventReporterId);
		EventReporter DeleteEventReporter(EventReporter entity);
		List<EventReporter> GetPagedEventReporter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<EventReporter> GetAllEventReporter(string SelectClause=null);
		EventReporter InsertEventReporter(EventReporter entity);
		List<EventReporter> GetEventReporterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
