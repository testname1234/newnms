﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEventResourceRepositoryBase
	{
        
        Dictionary<string, string> GetEventResourceBasicSearchColumns();
        List<SearchColumn> GetEventResourceSearchColumns();
        List<SearchColumn> GetEventResourceAdvanceSearchColumns();
        

		List<EventResource> GetEventResourceByNewsFileId(System.Int32? NewsFileId,string SelectClause=null);
		EventResource GetEventResource(System.Int32 EventResourceId,string SelectClause=null);
		EventResource UpdateEventResource(EventResource entity);
		bool DeleteEventResource(System.Int32 EventResourceId);
		EventResource DeleteEventResource(EventResource entity);
		List<EventResource> GetPagedEventResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<EventResource> GetAllEventResource(string SelectClause=null);
		EventResource InsertEventResource(EventResource entity);
		List<EventResource> GetEventResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
