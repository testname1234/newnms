﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEventResourceRepository: IEventResourceRepositoryBase
	{
        bool DeleteEventResourceByNewsFileId(System.Int32 Id);

    }
	
	
}
