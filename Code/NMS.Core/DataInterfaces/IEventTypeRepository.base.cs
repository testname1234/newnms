﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEventTypeRepositoryBase
	{
        
        Dictionary<string, string> GetEventTypeBasicSearchColumns();
        List<SearchColumn> GetEventTypeSearchColumns();
        List<SearchColumn> GetEventTypeAdvanceSearchColumns();
        

		EventType GetEventType(System.Int32 EventTypeId,string SelectClause=null);
		EventType UpdateEventType(EventType entity);
		bool DeleteEventType(System.Int32 EventTypeId);
		EventType DeleteEventType(EventType entity);
		List<EventType> GetPagedEventType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<EventType> GetAllEventType(string SelectClause=null);
		EventType InsertEventType(EventType entity);
		List<EventType> GetEventTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
