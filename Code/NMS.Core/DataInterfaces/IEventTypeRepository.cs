﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IEventTypeRepository: IEventTypeRepositoryBase
	{
        List<EventType> GetEventTypeByTerm(string term);

        EventType GetEventTypeByExactTerm(string Name);

    }
	
	
}
