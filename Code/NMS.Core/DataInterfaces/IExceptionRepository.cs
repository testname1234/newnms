﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.DataInterfaces
{
    public interface IExceptionRepository
    {
        void InsertException(Exception exp);
        bool JSErrorLogging(string msg, string url, string linenumber, string user, string type,string host);

        void MCRErrorLogging(string exp);
    }
}
