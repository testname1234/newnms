﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFacebookAccountRepositoryBase
	{
        
        Dictionary<string, string> GetFacebookAccountBasicSearchColumns();
        List<SearchColumn> GetFacebookAccountSearchColumns();
        List<SearchColumn> GetFacebookAccountAdvanceSearchColumns();
        

		List<FacebookAccount> GetFacebookAccountByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		FacebookAccount GetFacebookAccount(System.Int32 FacebookAccountId,string SelectClause=null);
		FacebookAccount UpdateFacebookAccount(FacebookAccount entity);
		bool DeleteFacebookAccount(System.Int32 FacebookAccountId);
		FacebookAccount DeleteFacebookAccount(FacebookAccount entity);
		List<FacebookAccount> GetPagedFacebookAccount(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FacebookAccount> GetAllFacebookAccount(string SelectClause=null);
		FacebookAccount InsertFacebookAccount(FacebookAccount entity);
		List<FacebookAccount> GetFacebookAccountByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
