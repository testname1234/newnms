﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFacebookCredentialRepositoryBase
	{
        
        Dictionary<string, string> GetFacebookCredentialBasicSearchColumns();
        List<SearchColumn> GetFacebookCredentialSearchColumns();
        List<SearchColumn> GetFacebookCredentialAdvanceSearchColumns();
        

		FacebookCredential GetFacebookCredential(System.Int32 FacebookCredentialId,string SelectClause=null);
		FacebookCredential UpdateFacebookCredential(FacebookCredential entity);
		bool DeleteFacebookCredential(System.Int32 FacebookCredentialId);
		FacebookCredential DeleteFacebookCredential(FacebookCredential entity);
		List<FacebookCredential> GetPagedFacebookCredential(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FacebookCredential> GetAllFacebookCredential(string SelectClause=null);
		FacebookCredential InsertFacebookCredential(FacebookCredential entity);
		List<FacebookCredential> GetFacebookCredentialByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
