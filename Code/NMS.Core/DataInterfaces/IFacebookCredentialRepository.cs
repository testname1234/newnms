﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFacebookCredentialRepository: IFacebookCredentialRepositoryBase
	{
        void UdpateAccessToken(string AccessToken, DateTime LastUpdateDate, int CredentialID);
        string GetAccessToken();
	}
	
	
}
