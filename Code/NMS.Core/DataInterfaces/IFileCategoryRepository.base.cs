﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileCategoryRepositoryBase
	{
        
        Dictionary<string, string> GetFileCategoryBasicSearchColumns();
        List<SearchColumn> GetFileCategorySearchColumns();
        List<SearchColumn> GetFileCategoryAdvanceSearchColumns();
        

		List<FileCategory> GetFileCategoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		List<FileCategory> GetFileCategoryByCategoryId(System.Int32 CategoryId,string SelectClause=null);
		FileCategory GetFileCategory(System.Int32 FileCategoryId,string SelectClause=null);
		FileCategory UpdateFileCategory(FileCategory entity);
		bool DeleteFileCategory(System.Int32 FileCategoryId);
		FileCategory DeleteFileCategory(FileCategory entity);
		List<FileCategory> GetPagedFileCategory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FileCategory> GetAllFileCategory(string SelectClause=null);
		FileCategory InsertFileCategory(FileCategory entity);
		List<FileCategory> GetFileCategoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
