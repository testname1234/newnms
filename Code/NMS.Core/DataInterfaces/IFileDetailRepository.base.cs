﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileDetailRepositoryBase
	{
        
        Dictionary<string, string> GetFileDetailBasicSearchColumns();
        List<SearchColumn> GetFileDetailSearchColumns();
        List<SearchColumn> GetFileDetailAdvanceSearchColumns();
        

		List<FileDetail> GetFileDetailByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		FileDetail GetFileDetail(System.Int32 FileDetailId,string SelectClause=null);
		FileDetail UpdateFileDetail(FileDetail entity);
		bool DeleteFileDetail(System.Int32 FileDetailId);
		FileDetail DeleteFileDetail(FileDetail entity);
		List<FileDetail> GetPagedFileDetail(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FileDetail> GetAllFileDetail(string SelectClause=null);
		FileDetail InsertFileDetail(FileDetail entity);
		List<FileDetail> GetFileDetailByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
