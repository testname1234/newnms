﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileDetailRepository: IFileDetailRepositoryBase
	{
        List<FileDetail> GetFileDetailByCreationDate(DateTime dt);
        List<FileDetail> GetFileDetailByLastUpdateDate(DateTime dt);
        List<FileDetail> GetFileDetailWithFilterFromToAndSearch(string from, string to, string searchText);
        List<FileDetail> getNewsFileDetailsByNewsFileIds(int FolderId, int StoryCount);

        List<FileDetail> GetApprovedNewsfile(int[] programIds);

    }
	
	
}
