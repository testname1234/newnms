﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileFolderHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetFileFolderHistoryBasicSearchColumns();
        List<SearchColumn> GetFileFolderHistorySearchColumns();
        List<SearchColumn> GetFileFolderHistoryAdvanceSearchColumns();
        

		List<FileFolderHistory> GetFileFolderHistoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		List<FileFolderHistory> GetFileFolderHistoryByFolderId(System.Int32 FolderId,string SelectClause=null);
		FileFolderHistory GetFileFolderHistory(System.Int32 FileFolderHistoryId,string SelectClause=null);
		FileFolderHistory UpdateFileFolderHistory(FileFolderHistory entity);
		bool DeleteFileFolderHistory(System.Int32 FileFolderHistoryId);
		FileFolderHistory DeleteFileFolderHistory(FileFolderHistory entity);
		List<FileFolderHistory> GetPagedFileFolderHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FileFolderHistory> GetAllFileFolderHistory(string SelectClause=null);
		FileFolderHistory InsertFileFolderHistory(FileFolderHistory entity);
		List<FileFolderHistory> GetFileFolderHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
