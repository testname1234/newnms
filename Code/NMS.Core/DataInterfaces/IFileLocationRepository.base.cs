﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileLocationRepositoryBase
	{
        
        Dictionary<string, string> GetFileLocationBasicSearchColumns();
        List<SearchColumn> GetFileLocationSearchColumns();
        List<SearchColumn> GetFileLocationAdvanceSearchColumns();
        

		List<FileLocation> GetFileLocationByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		List<FileLocation> GetFileLocationByLocationId(System.Int32 LocationId,string SelectClause=null);
		FileLocation GetFileLocation(System.Int32 FileLocationId,string SelectClause=null);
		FileLocation UpdateFileLocation(FileLocation entity);
		bool DeleteFileLocation(System.Int32 FileLocationId);
		FileLocation DeleteFileLocation(FileLocation entity);
		List<FileLocation> GetPagedFileLocation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FileLocation> GetAllFileLocation(string SelectClause=null);
		FileLocation InsertFileLocation(FileLocation entity);
		List<FileLocation> GetFileLocationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
