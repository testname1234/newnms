﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileResourceRepositoryBase
	{
        
        Dictionary<string, string> GetFileResourceBasicSearchColumns();
        List<SearchColumn> GetFileResourceSearchColumns();
        List<SearchColumn> GetFileResourceAdvanceSearchColumns();
        

		List<FileResource> GetFileResourceByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		FileResource GetFileResource(System.Int32 FileResourceId,string SelectClause=null);
		FileResource UpdateFileResource(FileResource entity);
		bool DeleteFileResource(System.Int32 FileResourceId);
		FileResource DeleteFileResource(FileResource entity);
		List<FileResource> GetPagedFileResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FileResource> GetAllFileResource(string SelectClause=null);
		FileResource InsertFileResource(FileResource entity);
		List<FileResource> GetFileResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
