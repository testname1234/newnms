﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileResourceRepository: IFileResourceRepositoryBase
	{
        List<FileResource> GetFileResourceByCreationDate(DateTime dt);

        List<FileResource> GetFileResourceByLastUpdateDate(DateTime dt);

        FileResource GetFileResourceByGuid(Guid guid);

        bool MarkFileResourceInActiveByGuid(string guid);

        bool DeleteFileResourceByNewsFileId(int newsFileId);
    }
	
	
}
