﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileStatusHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetFileStatusHistoryBasicSearchColumns();
        List<SearchColumn> GetFileStatusHistorySearchColumns();
        List<SearchColumn> GetFileStatusHistoryAdvanceSearchColumns();
        

		List<FileStatusHistory> GetFileStatusHistoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		FileStatusHistory GetFileStatusHistory(System.Int32 FileStatusHistoryId,string SelectClause=null);
		FileStatusHistory UpdateFileStatusHistory(FileStatusHistory entity);
		bool DeleteFileStatusHistory(System.Int32 FileStatusHistoryId);
		FileStatusHistory DeleteFileStatusHistory(FileStatusHistory entity);
		List<FileStatusHistory> GetPagedFileStatusHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FileStatusHistory> GetAllFileStatusHistory(string SelectClause=null);
		FileStatusHistory InsertFileStatusHistory(FileStatusHistory entity);
		List<FileStatusHistory> GetFileStatusHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
