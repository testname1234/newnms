﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFileTagRepositoryBase
	{
        
        Dictionary<string, string> GetFileTagBasicSearchColumns();
        List<SearchColumn> GetFileTagSearchColumns();
        List<SearchColumn> GetFileTagAdvanceSearchColumns();
        

		List<FileTag> GetFileTagByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		List<FileTag> GetFileTagByTagId(System.Int32 TagId,string SelectClause=null);
		FileTag GetFileTag(System.Int32 FileTagId,string SelectClause=null);
		FileTag UpdateFileTag(FileTag entity);
		bool DeleteFileTag(System.Int32 FileTagId);
		FileTag DeleteFileTag(FileTag entity);
		List<FileTag> GetPagedFileTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FileTag> GetAllFileTag(string SelectClause=null);
		FileTag InsertFileTag(FileTag entity);
		List<FileTag> GetFileTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
