﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFilterRepositoryBase
	{
        
        Dictionary<string, string> GetFilterBasicSearchColumns();
        List<SearchColumn> GetFilterSearchColumns();
        List<SearchColumn> GetFilterAdvanceSearchColumns();
        

		List<Filter> GetFilterByFilterTypeId(System.Int32 FilterTypeId,string SelectClause=null);
		Filter GetFilter(System.Int32 FilterId,string SelectClause=null);
		Filter UpdateFilter(Filter entity);
		bool DeleteFilter(System.Int32 FilterId);
		Filter DeleteFilter(Filter entity);
		List<Filter> GetPagedFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Filter> GetAllFilter(string SelectClause=null);
		Filter InsertFilter(Filter entity);
		List<Filter> GetFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
