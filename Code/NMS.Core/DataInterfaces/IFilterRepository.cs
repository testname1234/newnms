﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Enums;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFilterRepository: IFilterRepositoryBase
	{
        Filter GetFilterCreateIfNotExist(Filter filter);
        bool Exist(Filter filter);
        Filter GetFilterByNameAndFilterType(FilterTypes filterType,string name);

        Filter GetFilterByFilterTypeAndValue(int filterTypeId, int value);

        List<int> GetDistinctFilterIds();

        List<Filter> GetAllParentFilters();

        Filter GetFilterCreateIfNotExistByValue(Filter filter);

        List<Filter> GetFilterByLastUpdateDate(DateTime filterLastUpdateDate);

        
    }
	
	
}
