﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFilterTypeRepositoryBase
	{
        
        Dictionary<string, string> GetFilterTypeBasicSearchColumns();
        List<SearchColumn> GetFilterTypeSearchColumns();
        List<SearchColumn> GetFilterTypeAdvanceSearchColumns();
        

		FilterType GetFilterType(System.Int32 FilterTypeId,string SelectClause=null);
		FilterType UpdateFilterType(FilterType entity);
		bool DeleteFilterType(System.Int32 FilterTypeId);
		FilterType DeleteFilterType(FilterType entity);
		List<FilterType> GetPagedFilterType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<FilterType> GetAllFilterType(string SelectClause=null);
		FilterType InsertFilterType(FilterType entity);
		List<FilterType> GetFilterTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
