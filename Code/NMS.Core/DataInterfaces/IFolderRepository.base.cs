﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IFolderRepositoryBase
	{
        
        Dictionary<string, string> GetFolderBasicSearchColumns();
        List<SearchColumn> GetFolderSearchColumns();
        List<SearchColumn> GetFolderAdvanceSearchColumns();
        

		Folder GetFolder(System.Int32 FolderId,string SelectClause=null);
		Folder UpdateFolder(Folder entity);
		bool DeleteFolder(System.Int32 FolderId);
		Folder DeleteFolder(Folder entity);
		List<Folder> GetPagedFolder(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Folder> GetAllFolder(string SelectClause=null);
		Folder InsertFolder(Folder entity);
		List<Folder> GetFolderByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
