﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{

    public interface IFolderRepository : IFolderRepositoryBase
    {
        List<Folder> GetAllFolder();
        List<Folder> GetFolderByCreationDate(DateTime dt);
        bool EpisodeFolderExist(int episodeId);
        List<Folder> GetByEpisodeId(int episodeId);

        List<Folder> GetFolderByProgramId(int ProgramId);
    }
	
	
}
