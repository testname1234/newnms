﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IGroupUserRepositoryBase
	{
        
        Dictionary<string, string> GetGroupUserBasicSearchColumns();
        List<SearchColumn> GetGroupUserSearchColumns();
        List<SearchColumn> GetGroupUserAdvanceSearchColumns();
        

		GroupUser GetGroupUser(System.Int32 GroupUserId,string SelectClause=null);
		GroupUser UpdateGroupUser(GroupUser entity);
		bool DeleteGroupUser(System.Int32 GroupUserId);
		GroupUser DeleteGroupUser(GroupUser entity);
		List<GroupUser> GetPagedGroupUser(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<GroupUser> GetAllGroupUser(string SelectClause=null);
		GroupUser InsertGroupUser(GroupUser entity);
		List<GroupUser> GetGroupUserByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
