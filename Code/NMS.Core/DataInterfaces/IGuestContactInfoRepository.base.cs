﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IGuestContactInfoRepositoryBase
	{
        
        Dictionary<string, string> GetGuestContactInfoBasicSearchColumns();
        List<SearchColumn> GetGuestContactInfoSearchColumns();
        List<SearchColumn> GetGuestContactInfoAdvanceSearchColumns();
        

		List<GuestContactInfo> GetGuestContactInfoByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		GuestContactInfo GetGuestContactInfo(System.Int32 GuestContactInfoId,string SelectClause=null);
		GuestContactInfo UpdateGuestContactInfo(GuestContactInfo entity);
		bool DeleteGuestContactInfo(System.Int32 GuestContactInfoId);
		GuestContactInfo DeleteGuestContactInfo(GuestContactInfo entity);
		List<GuestContactInfo> GetPagedGuestContactInfo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<GuestContactInfo> GetAllGuestContactInfo(string SelectClause=null);
		GuestContactInfo InsertGuestContactInfo(GuestContactInfo entity);
		List<GuestContactInfo> GetGuestContactInfoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
