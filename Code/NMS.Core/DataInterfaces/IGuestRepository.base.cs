﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IGuestRepositoryBase
	{
        
        Dictionary<string, string> GetGuestBasicSearchColumns();
        List<SearchColumn> GetGuestSearchColumns();
        List<SearchColumn> GetGuestAdvanceSearchColumns();
        

		Guest GetGuest(System.Int32 GuestId,string SelectClause=null);
		Guest UpdateGuest(Guest entity);
		bool DeleteGuest(System.Int32 GuestId);
		Guest DeleteGuest(Guest entity);
		List<Guest> GetPagedGuest(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Guest> GetAllGuest(string SelectClause=null);
		Guest InsertGuest(Guest entity);
		List<Guest> GetGuestByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
