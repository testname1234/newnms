﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IGuestRepository: IGuestRepositoryBase
	{
        List<Guest> GetAllGuestBySlotId(int SlotId);

        bool DeleteAllGuestBySlotId(System.Int32 SlotId);

        List<Guest> GetGuestEpisodeReport();
    }
	
	
}
