﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IKeyValueRepositoryBase
	{
        
        Dictionary<string, string> GetKeyValueBasicSearchColumns();
        List<SearchColumn> GetKeyValueSearchColumns();
        List<SearchColumn> GetKeyValueAdvanceSearchColumns();
        

		KeyValue GetKeyValue(System.Int32 KeyValueId,string SelectClause=null);
		KeyValue UpdateKeyValue(KeyValue entity);
		bool DeleteKeyValue(System.Int32 KeyValueId);
		KeyValue DeleteKeyValue(KeyValue entity);
		List<KeyValue> GetPagedKeyValue(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<KeyValue> GetAllKeyValue(string SelectClause=null);
		KeyValue InsertKeyValue(KeyValue entity);
		List<KeyValue> GetKeyValueByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
