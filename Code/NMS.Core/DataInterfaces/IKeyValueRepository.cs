﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{

    public interface IKeyValueRepository : IKeyValueRepositoryBase
    {
        string GetValueByKey(string key);
        bool SetValueByKey(string key, string value);
    }


}
