﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ILocationAliasRepositoryBase
	{
        
        Dictionary<string, string> GetLocationAliasBasicSearchColumns();
        List<SearchColumn> GetLocationAliasSearchColumns();
        List<SearchColumn> GetLocationAliasAdvanceSearchColumns();
        

		List<LocationAlias> GetLocationAliasByLocationId(System.Int32 LocationId,string SelectClause=null);
		LocationAlias GetLocationAlias(System.Int32 LocationAliasId,string SelectClause=null);
		LocationAlias UpdateLocationAlias(LocationAlias entity);
		bool DeleteLocationAlias(System.Int32 LocationAliasId);
		LocationAlias DeleteLocationAlias(LocationAlias entity);
		List<LocationAlias> GetPagedLocationAlias(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<LocationAlias> GetAllLocationAlias(string SelectClause=null);
		LocationAlias InsertLocationAlias(LocationAlias entity);
		List<LocationAlias> GetLocationAliasByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);
        
    }
	    
	
}
