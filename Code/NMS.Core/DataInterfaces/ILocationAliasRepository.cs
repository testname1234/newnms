﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ILocationAliasRepository: ILocationAliasRepositoryBase
	{
        LocationAlias GetByAlias(string alias);
        bool Exists(LocationAlias entity);
	}
	
	
}
