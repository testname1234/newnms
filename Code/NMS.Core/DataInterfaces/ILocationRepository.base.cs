﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ILocationRepositoryBase
	{
        
        Dictionary<string, string> GetLocationBasicSearchColumns();
        List<SearchColumn> GetLocationSearchColumns();
        List<SearchColumn> GetLocationAdvanceSearchColumns();
        

		Location GetLocation(System.Int32 LocationId,string SelectClause=null);
		Location UpdateLocation(Location entity);
		bool DeleteLocation(System.Int32 LocationId);
		Location DeleteLocation(Location entity);
		List<Location> GetPagedLocation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Location> GetAllLocation(string SelectClause=null);
		Location InsertLocation(Location entity);
		List<Location> GetLocationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);
    }
	
	
}
