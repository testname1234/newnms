﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ILocationRepository: ILocationRepositoryBase
	{
        Location GetLocationCreateIfNotExist(Location location);
        bool Exist(Location location);
        Location GetLocationByName(string name);
        List<Location> GetLocationByTerm(string term);

        LocationAlias GetByAlias(string alias);
        Location InsertLocationIfNotExists(Location entity);
        List<Location> GetByDate(DateTime LastUpdateDate);
        List<int> GetAllChildLocations(int locationId);
    }
	
	
}
