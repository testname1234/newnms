﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ILogHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetLogHistoryBasicSearchColumns();
        List<SearchColumn> GetLogHistorySearchColumns();
        List<SearchColumn> GetLogHistoryAdvanceSearchColumns();
        

		LogHistory GetLogHistory(System.Int32 LogHistoryId,string SelectClause=null);
		LogHistory UpdateLogHistory(LogHistory entity);
		bool DeleteLogHistory(System.Int32 LogHistoryId);
		LogHistory DeleteLogHistory(LogHistory entity);
		List<LogHistory> GetPagedLogHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<LogHistory> GetAllLogHistory(string SelectClause=null);
		LogHistory InsertLogHistory(LogHistory entity);
		List<LogHistory> GetLogHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
