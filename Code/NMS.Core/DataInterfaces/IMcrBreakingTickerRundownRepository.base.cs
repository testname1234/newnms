﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrBreakingTickerRundownRepositoryBase
	{
        
        Dictionary<string, string> GetMcrBreakingTickerRundownBasicSearchColumns();
        List<SearchColumn> GetMcrBreakingTickerRundownSearchColumns();
        List<SearchColumn> GetMcrBreakingTickerRundownAdvanceSearchColumns();
        

		McrBreakingTickerRundown GetMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId,string SelectClause=null);
		McrBreakingTickerRundown UpdateMcrBreakingTickerRundown(McrBreakingTickerRundown entity);
		bool DeleteMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId);
		McrBreakingTickerRundown DeleteMcrBreakingTickerRundown(McrBreakingTickerRundown entity);
		List<McrBreakingTickerRundown> GetPagedMcrBreakingTickerRundown(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrBreakingTickerRundown> GetAllMcrBreakingTickerRundown(string SelectClause=null);
		McrBreakingTickerRundown InsertMcrBreakingTickerRundown(McrBreakingTickerRundown entity);
		List<McrBreakingTickerRundown> GetMcrBreakingTickerRundownByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
