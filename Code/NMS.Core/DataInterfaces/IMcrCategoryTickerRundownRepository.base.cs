﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrCategoryTickerRundownRepositoryBase
	{
        
        Dictionary<string, string> GetMcrCategoryTickerRundownBasicSearchColumns();
        List<SearchColumn> GetMcrCategoryTickerRundownSearchColumns();
        List<SearchColumn> GetMcrCategoryTickerRundownAdvanceSearchColumns();
        

		McrCategoryTickerRundown GetMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId,string SelectClause=null);
		McrCategoryTickerRundown UpdateMcrCategoryTickerRundown(McrCategoryTickerRundown entity);
		bool DeleteMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId);
		McrCategoryTickerRundown DeleteMcrCategoryTickerRundown(McrCategoryTickerRundown entity);
		List<McrCategoryTickerRundown> GetPagedMcrCategoryTickerRundown(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrCategoryTickerRundown> GetAllMcrCategoryTickerRundown(string SelectClause=null);
		McrCategoryTickerRundown InsertMcrCategoryTickerRundown(McrCategoryTickerRundown entity);
		List<McrCategoryTickerRundown> GetMcrCategoryTickerRundownByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
