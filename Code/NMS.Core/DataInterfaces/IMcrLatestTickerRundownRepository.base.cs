﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrLatestTickerRundownRepositoryBase
	{
        
        Dictionary<string, string> GetMcrLatestTickerRundownBasicSearchColumns();
        List<SearchColumn> GetMcrLatestTickerRundownSearchColumns();
        List<SearchColumn> GetMcrLatestTickerRundownAdvanceSearchColumns();
        

		McrLatestTickerRundown GetMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId,string SelectClause=null);
		McrLatestTickerRundown UpdateMcrLatestTickerRundown(McrLatestTickerRundown entity);
		bool DeleteMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId);
		McrLatestTickerRundown DeleteMcrLatestTickerRundown(McrLatestTickerRundown entity);
		List<McrLatestTickerRundown> GetPagedMcrLatestTickerRundown(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrLatestTickerRundown> GetAllMcrLatestTickerRundown(string SelectClause=null);
		McrLatestTickerRundown InsertMcrLatestTickerRundown(McrLatestTickerRundown entity);
		List<McrLatestTickerRundown> GetMcrLatestTickerRundownByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
