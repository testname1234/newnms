﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrTickerBroadcastedRepositoryBase
	{
        
        Dictionary<string, string> GetMcrTickerBroadcastedBasicSearchColumns();
        List<SearchColumn> GetMcrTickerBroadcastedSearchColumns();
        List<SearchColumn> GetMcrTickerBroadcastedAdvanceSearchColumns();
        

		List<McrTickerBroadcasted> GetMcrTickerBroadcastedByTickerCategoryId(System.Int32 TickerCategoryId,string SelectClause=null);
		McrTickerBroadcasted GetMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId,string SelectClause=null);
		McrTickerBroadcasted UpdateMcrTickerBroadcasted(McrTickerBroadcasted entity);
		bool DeleteMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId);
		McrTickerBroadcasted DeleteMcrTickerBroadcasted(McrTickerBroadcasted entity);
		List<McrTickerBroadcasted> GetPagedMcrTickerBroadcasted(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrTickerBroadcasted> GetAllMcrTickerBroadcasted(string SelectClause=null);
		McrTickerBroadcasted InsertMcrTickerBroadcasted(McrTickerBroadcasted entity);
		List<McrTickerBroadcasted> GetMcrTickerBroadcastedByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
