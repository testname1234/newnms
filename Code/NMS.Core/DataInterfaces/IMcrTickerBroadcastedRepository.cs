﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrTickerBroadcastedRepository: IMcrTickerBroadcastedRepositoryBase
	{
        List<McrTickerBroadcasted> GetMcrTickerBroadcastedByDate(DateTime From, DateTime To);

    }
	
	
}
