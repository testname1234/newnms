﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrTickerHistoryLogRepositoryBase
	{
        
        Dictionary<string, string> GetMcrTickerHistoryLogBasicSearchColumns();
        List<SearchColumn> GetMcrTickerHistoryLogSearchColumns();
        List<SearchColumn> GetMcrTickerHistoryLogAdvanceSearchColumns();
        

		McrTickerHistoryLog GetMcrTickerHistoryLog(System.Int32 McrTickerHistoryLogId,string SelectClause=null);
		McrTickerHistoryLog UpdateMcrTickerHistoryLog(McrTickerHistoryLog entity);
		bool DeleteMcrTickerHistoryLog(System.Int32 McrTickerHistoryLogId);
		McrTickerHistoryLog DeleteMcrTickerHistoryLog(McrTickerHistoryLog entity);
		List<McrTickerHistoryLog> GetPagedMcrTickerHistoryLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrTickerHistoryLog> GetAllMcrTickerHistoryLog(string SelectClause=null);
		McrTickerHistoryLog InsertMcrTickerHistoryLog(McrTickerHistoryLog entity);
		List<McrTickerHistoryLog> GetMcrTickerHistoryLogByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
