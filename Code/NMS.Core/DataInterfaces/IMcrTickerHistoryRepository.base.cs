﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrTickerHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetMcrTickerHistoryBasicSearchColumns();
        List<SearchColumn> GetMcrTickerHistorySearchColumns();
        List<SearchColumn> GetMcrTickerHistoryAdvanceSearchColumns();
        

		List<McrTickerHistory> GetMcrTickerHistoryByTickerId(System.Int32? TickerId,string SelectClause=null);
		List<McrTickerHistory> GetMcrTickerHistoryByTickerCategoryId(System.Int32? TickerCategoryId,string SelectClause=null);
		McrTickerHistory GetMcrTickerHistory(System.Int32 McrTickerHistoryId,string SelectClause=null);
		McrTickerHistory UpdateMcrTickerHistory(McrTickerHistory entity);
		bool DeleteMcrTickerHistory(System.Int32 McrTickerHistoryId);
		McrTickerHistory DeleteMcrTickerHistory(McrTickerHistory entity);
		List<McrTickerHistory> GetPagedMcrTickerHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrTickerHistory> GetAllMcrTickerHistory(string SelectClause=null);
		McrTickerHistory InsertMcrTickerHistory(McrTickerHistory entity);
		List<McrTickerHistory> GetMcrTickerHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
