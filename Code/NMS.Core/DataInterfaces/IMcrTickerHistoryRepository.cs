﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{

    public interface IMcrTickerHistoryRepository : IMcrTickerHistoryRepositoryBase
    {
        List<McrTickerHistory> GetLastBroadcastedTickers(int tickerCategoryId, int size, List<int> tickerIds);
        List<McrTickerHistory> GetMcrTickerHistoryByBroadcastedId(int Id);
    }


}
