﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrTickerRundownStatusRepositoryBase
	{
        
        Dictionary<string, string> GetMcrTickerRundownStatusBasicSearchColumns();
        List<SearchColumn> GetMcrTickerRundownStatusSearchColumns();
        List<SearchColumn> GetMcrTickerRundownStatusAdvanceSearchColumns();
        

		List<McrTickerRundownStatus> GetMcrTickerRundownStatusByStatusId(System.Int32 StatusId,string SelectClause=null);
		McrTickerRundownStatus GetMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId,string SelectClause=null);
		McrTickerRundownStatus UpdateMcrTickerRundownStatus(McrTickerRundownStatus entity);
		bool DeleteMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId);
		McrTickerRundownStatus DeleteMcrTickerRundownStatus(McrTickerRundownStatus entity);
		List<McrTickerRundownStatus> GetPagedMcrTickerRundownStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrTickerRundownStatus> GetAllMcrTickerRundownStatus(string SelectClause=null);
		McrTickerRundownStatus InsertMcrTickerRundownStatus(McrTickerRundownStatus entity);
		List<McrTickerRundownStatus> GetMcrTickerRundownStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
