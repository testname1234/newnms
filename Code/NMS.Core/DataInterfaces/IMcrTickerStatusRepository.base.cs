﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMcrTickerStatusRepositoryBase
	{
        
        Dictionary<string, string> GetMcrTickerStatusBasicSearchColumns();
        List<SearchColumn> GetMcrTickerStatusSearchColumns();
        List<SearchColumn> GetMcrTickerStatusAdvanceSearchColumns();
        

		McrTickerStatus GetMcrTickerStatus(System.Int32 McrTickerStatusId,string SelectClause=null);
		McrTickerStatus UpdateMcrTickerStatus(McrTickerStatus entity);
		bool DeleteMcrTickerStatus(System.Int32 McrTickerStatusId);
		McrTickerStatus DeleteMcrTickerStatus(McrTickerStatus entity);
		List<McrTickerStatus> GetPagedMcrTickerStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<McrTickerStatus> GetAllMcrTickerStatus(string SelectClause=null);
		McrTickerStatus InsertMcrTickerStatus(McrTickerStatus entity);
		List<McrTickerStatus> GetMcrTickerStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
