﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMessageRepositoryBase
	{
        
        Dictionary<string, string> GetMessageBasicSearchColumns();
        List<SearchColumn> GetMessageSearchColumns();
        List<SearchColumn> GetMessageAdvanceSearchColumns();
        

		Message GetMessage(System.Int32 MessageId,string SelectClause=null);
		Message UpdateMessage(Message entity);
		bool DeleteMessage(System.Int32 MessageId);
		Message DeleteMessage(Message entity);
		List<Message> GetPagedMessage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Message> GetAllMessage(string SelectClause=null);
		Message InsertMessage(Message entity);
		List<Message> GetMessageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
