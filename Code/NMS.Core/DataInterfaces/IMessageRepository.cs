﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{

    public interface IMessageRepository : IMessageRepositoryBase
    {
        List<Message> GetAllUserMessages(int UserId, DateTime LastUpdatedDate);
        void UpdateMessagesRecievedStatus(List<int> MessageIds);
        List<Message> GetAllUserPendingMessages(int UserId);
        List<Message> GetAllUserPendingMessagesBySlotScreenTemplateId(int SlotScreenTemplateId);
        List<Message> GetMessages(int SlotScreenTemplateId, DateTime lastUpdateDate);

    }
	
	
}
