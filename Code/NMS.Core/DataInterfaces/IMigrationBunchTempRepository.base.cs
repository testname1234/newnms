﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMigrationBunchTempRepositoryBase
	{
        
        Dictionary<string, string> GetMigrationBunchTempBasicSearchColumns();
        List<SearchColumn> GetMigrationBunchTempSearchColumns();
        List<SearchColumn> GetMigrationBunchTempAdvanceSearchColumns();
        

		MigrationBunchTemp GetMigrationBunchTemp(System.Int32 MigrationBunchTempId,string SelectClause=null);
		MigrationBunchTemp UpdateMigrationBunchTemp(MigrationBunchTemp entity);
		bool DeleteMigrationBunchTemp(System.Int32 MigrationBunchTempId);
		MigrationBunchTemp DeleteMigrationBunchTemp(MigrationBunchTemp entity);
		List<MigrationBunchTemp> GetPagedMigrationBunchTemp(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<MigrationBunchTemp> GetAllMigrationBunchTemp(string SelectClause=null);
		MigrationBunchTemp InsertMigrationBunchTemp(MigrationBunchTemp entity);
		List<MigrationBunchTemp> GetMigrationBunchTempByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
