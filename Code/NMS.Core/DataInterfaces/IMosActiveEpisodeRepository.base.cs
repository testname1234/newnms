﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMosActiveEpisodeRepositoryBase
	{
        
        Dictionary<string, string> GetMosActiveEpisodeBasicSearchColumns();
        List<SearchColumn> GetMosActiveEpisodeSearchColumns();
        List<SearchColumn> GetMosActiveEpisodeAdvanceSearchColumns();
        

		List<MosActiveEpisode> GetMosActiveEpisodeByEpisodeId(System.Int32? EpisodeId,string SelectClause=null);
		MosActiveEpisode GetMosActiveEpisode(System.Int32 MosActiveEpisodeId,string SelectClause=null);
		MosActiveEpisode UpdateMosActiveEpisode(MosActiveEpisode entity);
		bool DeleteMosActiveEpisode(System.Int32 MosActiveEpisodeId);
		MosActiveEpisode DeleteMosActiveEpisode(MosActiveEpisode entity);
		List<MosActiveEpisode> GetPagedMosActiveEpisode(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<MosActiveEpisode> GetAllMosActiveEpisode(string SelectClause=null);
		MosActiveEpisode InsertMosActiveEpisode(MosActiveEpisode entity);
		List<MosActiveEpisode> GetMosActiveEpisodeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
