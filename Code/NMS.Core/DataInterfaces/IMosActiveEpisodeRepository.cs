﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMosActiveEpisodeRepository: IMosActiveEpisodeRepositoryBase
	{
        List<MosActiveEpisode> GetMosActiveEpisodeByStatus(System.Int32? StatusCode, string SelectClause = null);

        //void UpdateStatus(int StatusCode, int MosActiveEpisodeId);
        void UpdateStatus(int StatusCode, int MosActiveEpisodeId, string PCRTelePropterJson, string PCRPlayoutJson);
        List<MosActiveEpisode> GetAllMosEpisodeStatusSmsThread(string SelectClause = null);
        void UpdateMosActiveEpisodeIsAcknowledged(MosActiveEpisode entity);
	}
	
	
}
