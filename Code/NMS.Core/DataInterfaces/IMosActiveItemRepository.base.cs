﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMosActiveItemRepositoryBase
	{
        
        Dictionary<string, string> GetMosActiveItemBasicSearchColumns();
        List<SearchColumn> GetMosActiveItemSearchColumns();
        List<SearchColumn> GetMosActiveItemAdvanceSearchColumns();
        

		List<MosActiveItem> GetMosActiveItemByEpisodeId(System.Int32? EpisodeId,string SelectClause=null);
		MosActiveItem GetMosActiveItem(System.Int32 MosActiveItemId,string SelectClause=null);
		MosActiveItem UpdateMosActiveItem(MosActiveItem entity);
		bool DeleteMosActiveItem(System.Int32 MosActiveItemId);
		MosActiveItem DeleteMosActiveItem(MosActiveItem entity);
		List<MosActiveItem> GetPagedMosActiveItem(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<MosActiveItem> GetAllMosActiveItem(string SelectClause=null);
		MosActiveItem InsertMosActiveItem(MosActiveItem entity);
		List<MosActiveItem> GetMosActiveItemByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
