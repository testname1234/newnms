﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMosActiveTransformationRepositoryBase
	{
        
        Dictionary<string, string> GetMosActiveTransformationBasicSearchColumns();
        List<SearchColumn> GetMosActiveTransformationSearchColumns();
        List<SearchColumn> GetMosActiveTransformationAdvanceSearchColumns();
        

		List<MosActiveTransformation> GetMosActiveTransformationByMosActiveItemId(System.Int32? MosActiveItemId,string SelectClause=null);
		MosActiveTransformation GetMosActiveTransformation(System.Int32 MosActiveDetailId,string SelectClause=null);
		MosActiveTransformation UpdateMosActiveTransformation(MosActiveTransformation entity);
		bool DeleteMosActiveTransformation(System.Int32 MosActiveDetailId);
		MosActiveTransformation DeleteMosActiveTransformation(MosActiveTransformation entity);
		List<MosActiveTransformation> GetPagedMosActiveTransformation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<MosActiveTransformation> GetAllMosActiveTransformation(string SelectClause=null);
		MosActiveTransformation InsertMosActiveTransformation(MosActiveTransformation entity);
		List<MosActiveTransformation> GetMosActiveTransformationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
