﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IMosItemRepositoryBase
	{
        
        Dictionary<string, string> GetMosItemBasicSearchColumns();
        List<SearchColumn> GetMosItemSearchColumns();
        List<SearchColumn> GetMosItemAdvanceSearchColumns();
        

		MosItem GetMosItem(System.Int32 CasperMosItemId,string SelectClause=null);
		MosItem UpdateMosItem(MosItem entity);
		bool DeleteMosItem(System.Int32 CasperMosItemId);
		MosItem DeleteMosItem(MosItem entity);
		List<MosItem> GetPagedMosItem(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<MosItem> GetAllMosItem(string SelectClause=null);
		MosItem InsertMosItem(MosItem entity);
		List<MosItem> GetMosItemByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
