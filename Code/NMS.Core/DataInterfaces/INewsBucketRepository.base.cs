﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsBucketRepositoryBase
	{
        
        Dictionary<string, string> GetNewsBucketBasicSearchColumns();
        List<SearchColumn> GetNewsBucketSearchColumns();
        List<SearchColumn> GetNewsBucketAdvanceSearchColumns();
        

		NewsBucket GetNewsBucket(System.Int32 NewsBucketId,string SelectClause=null);
		NewsBucket UpdateNewsBucket(NewsBucket entity);
		bool DeleteNewsBucket(System.Int32 NewsBucketId);
		NewsBucket DeleteNewsBucket(NewsBucket entity);
		List<NewsBucket> GetPagedNewsBucket(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsBucket> GetAllNewsBucket(string SelectClause=null);
		NewsBucket InsertNewsBucket(NewsBucket entity);
		List<NewsBucket> GetNewsBucketByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	
    }
	
	
}
