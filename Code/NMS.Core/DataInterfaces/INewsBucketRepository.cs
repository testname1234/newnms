﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsBucketRepository: INewsBucketRepositoryBase
	{
        List<NewsBucket> GetNewsBucketByProgramIds(string CommaSepratedIds);

        NewsBucket UpdateSequence(NewsBucket item);
    }
	
	
}
