﻿using NMS.Core.Entities;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.DataInterfaces
{
    public interface INewsCacheRepository
    {
        LoadNews GetNewsAndBunch(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null);

        void UpdateCache(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters, LoadNews loadNews);

        List<NewsBunchArgs> GetAllKeys();
    }
}
