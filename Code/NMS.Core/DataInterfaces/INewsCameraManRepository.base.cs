﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsCameraManRepositoryBase
	{
        
        Dictionary<string, string> GetNewsCameraManBasicSearchColumns();
        List<SearchColumn> GetNewsCameraManSearchColumns();
        List<SearchColumn> GetNewsCameraManAdvanceSearchColumns();
        

		NewsCameraMan GetNewsCameraMan(System.Int32 NewsCameraManId,string SelectClause=null);
		NewsCameraMan UpdateNewsCameraMan(NewsCameraMan entity);
		bool DeleteNewsCameraMan(System.Int32 NewsCameraManId);
		NewsCameraMan DeleteNewsCameraMan(NewsCameraMan entity);
		List<NewsCameraMan> GetPagedNewsCameraMan(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsCameraMan> GetAllNewsCameraMan(string SelectClause=null);
		NewsCameraMan InsertNewsCameraMan(NewsCameraMan entity);
		List<NewsCameraMan> GetNewsCameraManByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
