﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsCategoryRepositoryBase
	{
        
        Dictionary<string, string> GetNewsCategoryBasicSearchColumns();
        List<SearchColumn> GetNewsCategorySearchColumns();
        List<SearchColumn> GetNewsCategoryAdvanceSearchColumns();
        

		List<NewsCategory> GetNewsCategoryByNewsId(System.Int32 NewsId,string SelectClause=null);
		List<NewsCategory> GetNewsCategoryByCategoryId(System.Int32 CategoryId,string SelectClause=null);
		NewsCategory GetNewsCategory(System.Int32 NewsCategoryId,string SelectClause=null);
		NewsCategory UpdateNewsCategory(NewsCategory entity);
		bool DeleteNewsCategory(System.Int32 NewsCategoryId);
		NewsCategory DeleteNewsCategory(NewsCategory entity);
		List<NewsCategory> GetPagedNewsCategory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsCategory> GetAllNewsCategory(string SelectClause=null);
		NewsCategory InsertNewsCategory(NewsCategory entity);
		List<NewsCategory> GetNewsCategoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
