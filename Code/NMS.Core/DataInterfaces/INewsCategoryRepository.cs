﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsCategoryRepository: INewsCategoryRepositoryBase
	{
        bool DeleteNewsCategoryByNewsId(System.Int32 NewsId);
	}
	
	
}
