﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFileFilterRepositoryBase
	{
        
        Dictionary<string, string> GetNewsFileFilterBasicSearchColumns();
        List<SearchColumn> GetNewsFileFilterSearchColumns();
        List<SearchColumn> GetNewsFileFilterAdvanceSearchColumns();
        

		List<NewsFileFilter> GetNewsFileFilterByFilterId(System.Int32? FilterId,string SelectClause=null);
		List<NewsFileFilter> GetNewsFileFilterByNewsFileId(System.Int32? NewsFileId,string SelectClause=null);
		NewsFileFilter GetNewsFileFilter(System.Int32 NewsFileFilterId,string SelectClause=null);
		NewsFileFilter UpdateNewsFileFilter(NewsFileFilter entity);
		bool DeleteNewsFileFilter(System.Int32 NewsFileFilterId);
		NewsFileFilter DeleteNewsFileFilter(NewsFileFilter entity);
		List<NewsFileFilter> GetPagedNewsFileFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsFileFilter> GetAllNewsFileFilter(string SelectClause=null);
		NewsFileFilter InsertNewsFileFilter(NewsFileFilter entity);
		List<NewsFileFilter> GetNewsFileFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
