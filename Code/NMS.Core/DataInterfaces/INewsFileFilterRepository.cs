﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFileFilterRepository: INewsFileFilterRepositoryBase
	{
        NewsFileFilter GetNewsFileFilterOfNewsFileByFilterType(int NewsFileId, int FilterTypeId);
        bool DeleteNewsFileFilterByNewsFileId(System.Int32 NewsFileId);
    }
	
	
}
