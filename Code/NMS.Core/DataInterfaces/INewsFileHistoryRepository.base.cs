﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFileHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetNewsFileHistoryBasicSearchColumns();
        List<SearchColumn> GetNewsFileHistorySearchColumns();
        List<SearchColumn> GetNewsFileHistoryAdvanceSearchColumns();
        

		List<NewsFileHistory> GetNewsFileHistoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null);
		NewsFileHistory GetNewsFileHistory(System.Int32 NewsFileHistoryId,string SelectClause=null);
		NewsFileHistory UpdateNewsFileHistory(NewsFileHistory entity);
		bool DeleteNewsFileHistory(System.Int32 NewsFileHistoryId);
		NewsFileHistory DeleteNewsFileHistory(NewsFileHistory entity);
		List<NewsFileHistory> GetPagedNewsFileHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsFileHistory> GetAllNewsFileHistory(string SelectClause=null);
		NewsFileHistory InsertNewsFileHistory(NewsFileHistory entity);
		List<NewsFileHistory> GetNewsFileHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
