﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFileOrganizationRepositoryBase
	{
        
        Dictionary<string, string> GetNewsFileOrganizationBasicSearchColumns();
        List<SearchColumn> GetNewsFileOrganizationSearchColumns();
        List<SearchColumn> GetNewsFileOrganizationAdvanceSearchColumns();
        

		NewsFileOrganization GetNewsFileOrganization(System.Int32 NewsFileOrganizationId,string SelectClause=null);
		NewsFileOrganization UpdateNewsFileOrganization(NewsFileOrganization entity);
		bool DeleteNewsFileOrganization(System.Int32 NewsFileOrganizationId);
		NewsFileOrganization DeleteNewsFileOrganization(NewsFileOrganization entity);
		List<NewsFileOrganization> GetPagedNewsFileOrganization(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsFileOrganization> GetAllNewsFileOrganization(string SelectClause=null);
		NewsFileOrganization InsertNewsFileOrganization(NewsFileOrganization entity);
		List<NewsFileOrganization> GetNewsFileOrganizationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
