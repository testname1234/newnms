﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFileOrganizationRepository: INewsFileOrganizationRepositoryBase
	{
        List<NewsFileOrganization> GetNewsFileOrganizationByNewsFileId(System.Int32 NewsFileId, string SelectClause = null);
        bool DeleteNewsFileOrganizationByNewsFileId(System.Int32 NewsFileId);
    }
	
	
}
