﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFileRepositoryBase
	{
        
        Dictionary<string, string> GetNewsFileBasicSearchColumns();
        List<SearchColumn> GetNewsFileSearchColumns();
        List<SearchColumn> GetNewsFileAdvanceSearchColumns();
        

		List<NewsFile> GetNewsFileByFolderId(System.Int32 FolderId,string SelectClause=null);
		List<NewsFile> GetNewsFileBySlugId(System.Int32? SlugId,string SelectClause=null);
		List<NewsFile> GetNewsFileByProgramId(System.Int32? ProgramId,string SelectClause=null);
		NewsFile GetNewsFile(System.Int32 NewsFileId,string SelectClause=null);
		NewsFile UpdateNewsFile(NewsFile entity);
		bool DeleteNewsFile(System.Int32 NewsFileId);
		NewsFile DeleteNewsFile(NewsFile entity);
		List<NewsFile> GetPagedNewsFile(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsFile> GetAllNewsFile(string SelectClause=null);
		NewsFile InsertNewsFile(NewsFile entity);
		List<NewsFile> GetNewsFileByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
