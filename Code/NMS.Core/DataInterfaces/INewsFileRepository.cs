﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer.NewsFile;

namespace NMS.Core.DataInterfaces
{

    public interface INewsFileRepository : INewsFileRepositoryBase
    {
        List<NewsFile> GetNewsFilebyCreationDate(DateTime dt);

        List<NewsFile> GetNewsFilebyLastUpdateDate(DateTime dt);

        bool UpdateNewsFileStatusAndLastUpdate(int Id, int status, DateTime dateTime);

        bool UpdateNewsFileSequence(int Id, int sequenceId);

        bool UpdateNewsFileStatusById(int Id, int status, DateTime dateTime);

        bool MarkNewsVerificationStatus(int Id, bool status, DateTime LastUpdatedate);

        bool CheckIfNewsExistsSQL(string title, string source);


        List<NewsFile> GetReporterNewsFile(int rId, DateTime dt, int startIndex);

        List<NewsFile> GetNewsFileAfterDateTime(DateTime dt, int rId);
      //  List<NewsFile> GetNewsFileForProducer(List<Filter> filters, DateTime newsLastUpdateDate, bool includeProgramRelatedNews,string term, string PageOffset, string PageNumber);

        List<NewsFile> GetNewsFileWithFilterFromTo(List<Filter> filters, DateTime From, int[] folderids,string term, int PageOffset, int PageSize, DateTime? To);
        
        List<NewsFile> GetNewsFileWithFilterFromTo(string From, string To, int rId); 

        List<NewsFile> GetNewsFileWithFilterFromToAndSearch(string From, string To, string SearchText, int rId);

        List<NewsFile> GetNewsFileByProgramIdwithstatus(int ProgramId, string SelectClause = null);

        List<NewsFile> GetLastInsertNewsFile(string SelectClause = null);

        NewsFile UpdateNewsFileSequenceByNewsFileId(int Id, int sequenceNo);

        List<NewsFile> GetNewsFilesByFolderId(int FolderId, int StoryCount);

        bool UpdateProgramCountAndHistory(int prgramId);

        bool MarkTitleVoiceOverStatus(NMS.Core.DataTransfer.FileDetail.PostOutput res);

        int GetMaxSequenceNumByFolderId(System.Int32 FolderId);

        List<NewsFile> Getnewsfilebytitleandsource(string Title, string source, string SelectClause = null);

        List<NewsFile> GetSocialMediaNewsFileWithFilterFromTo(List<Filter> filters, DateTime from, bool includeProgramRelatedNews, string term, string PageOffset, string PageSize, DateTime? to, string SocialMediaFilterId);

        List<NewsFile> GetNewsFileForTaggReport();

        List<NewsFile> TaggReportPolling(DateTime lstUpdate);
        List<NewsFile> GetAllDescrepencyByStatus(int DescreypencyType);
        NewsFile UpdateNewsFileCategoryById(int NewsFileId, int type);
        NewsFile UpdateNewsFileLocationById(int NewsFileId, int type);

        List<NewsFile> GetBroadcastedNewsFiles(DateTime from, DateTime to);

        List<NewsFile> GetBroadcatedNewsFileDetail(int Id);
        List<NewsFile> GetNewsByLocationIds(List<int> locationIds, DateTime lastSyncTime);
        List<NewsFile> GetNewsByTagAndEntityIds(List<int> tagIds, List<int> entityIds, DateTime lastSyncTime);
        List<NewsFile> GetByProgramPriority(Dictionary<int, int> programsAndCount);
        List<NewsFile> GetNewsByIdsForTicker(List<int> newsIds);
        List<NewsFile> GetNewsForFreshTicker(DateTime lastSyncTime);

        bool UpdateIsLiveBitNewsFile(int programid);
    }


}
