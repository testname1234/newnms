﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFilterRepositoryBase
	{
        
        Dictionary<string, string> GetNewsFilterBasicSearchColumns();
        List<SearchColumn> GetNewsFilterSearchColumns();
        List<SearchColumn> GetNewsFilterAdvanceSearchColumns();
        

		List<NewsFilter> GetNewsFilterByFilterId(System.Int32 FilterId,string SelectClause=null);
        List<NewsFilter> GetNewsFilterByNewsId(System.Int32 NewsId, string SelectClause = null);
		NewsFilter GetNewsFilter(System.Int32 NewsFilterId,string SelectClause=null);
		NewsFilter UpdateNewsFilter(NewsFilter entity);
		bool DeleteNewsFilter(System.Int32 NewsFilterId);
		NewsFilter DeleteNewsFilter(NewsFilter entity);
		List<NewsFilter> GetPagedNewsFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsFilter> GetAllNewsFilter(string SelectClause=null);
		NewsFilter InsertNewsFilter(NewsFilter entity);
		List<NewsFilter> GetNewsFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);        
    
    }
	
	
}
