﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsFilterRepository: INewsFilterRepositoryBase
	{
        NewsFilter GetByFilterIdAndNewsId(System.Int32 FilterId, System.Int32 NewsId, string SelectClause = null);
        bool DeleteNewsFilterByNewsId(System.Int32 NewsId);
	}
	
	
}
