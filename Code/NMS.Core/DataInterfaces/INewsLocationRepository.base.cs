﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsLocationRepositoryBase
	{
        
        Dictionary<string, string> GetNewsLocationBasicSearchColumns();
        List<SearchColumn> GetNewsLocationSearchColumns();
        List<SearchColumn> GetNewsLocationAdvanceSearchColumns();
        

		List<NewsLocation> GetNewsLocationByNewsId(System.Int32? NewsId,string SelectClause=null);
		List<NewsLocation> GetNewsLocationByLocationId(System.Int32? LocationId,string SelectClause=null);
		NewsLocation GetNewsLocation(System.Int32 NewsLocationid,string SelectClause=null);
		NewsLocation UpdateNewsLocation(NewsLocation entity);
		bool DeleteNewsLocation(System.Int32 NewsLocationid);
		NewsLocation DeleteNewsLocation(NewsLocation entity);
		List<NewsLocation> GetPagedNewsLocation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsLocation> GetAllNewsLocation(string SelectClause=null);
		NewsLocation InsertNewsLocation(NewsLocation entity);
		List<NewsLocation> GetNewsLocationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
