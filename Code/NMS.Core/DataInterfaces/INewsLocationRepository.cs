﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsLocationRepository: INewsLocationRepositoryBase
	{
        bool DeleteNewsLocationByNewsId(System.Int32 Newsid);
	}
	
	
}
