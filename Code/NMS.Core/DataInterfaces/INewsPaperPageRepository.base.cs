﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsPaperPageRepositoryBase
	{
        
        Dictionary<string, string> GetNewsPaperPageBasicSearchColumns();
        List<SearchColumn> GetNewsPaperPageSearchColumns();
        List<SearchColumn> GetNewsPaperPageAdvanceSearchColumns();
        

		List<NewsPaperPage> GetNewsPaperPageByDailyNewsPaperId(System.Int32 DailyNewsPaperId,string SelectClause=null);
		NewsPaperPage GetNewsPaperPage(System.Int32 NewsPaperPageId,string SelectClause=null);
		NewsPaperPage UpdateNewsPaperPage(NewsPaperPage entity);
		bool DeleteNewsPaperPage(System.Int32 NewsPaperPageId);
		NewsPaperPage DeleteNewsPaperPage(NewsPaperPage entity);
		List<NewsPaperPage> GetPagedNewsPaperPage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsPaperPage> GetAllNewsPaperPage(string SelectClause=null);
		NewsPaperPage InsertNewsPaperPage(NewsPaperPage entity);
		List<NewsPaperPage> GetNewsPaperPageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
