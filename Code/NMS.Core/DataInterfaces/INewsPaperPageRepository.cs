﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsPaperPageRepository: INewsPaperPageRepositoryBase
	{
        List<NewsPaperPage> GetNewsPaperPagesByDailyNewsPaperDate(DateTime fromDate, DateTime toDate);
        void MarkIsProcessed(int id);
	}
	
	
}
