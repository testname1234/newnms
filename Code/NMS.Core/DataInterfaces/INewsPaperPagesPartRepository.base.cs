﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsPaperPagesPartRepositoryBase
	{
        
        Dictionary<string, string> GetNewsPaperPagesPartBasicSearchColumns();
        List<SearchColumn> GetNewsPaperPagesPartSearchColumns();
        List<SearchColumn> GetNewsPaperPagesPartAdvanceSearchColumns();
        

		List<NewsPaperPagesPart> GetNewsPaperPagesPartByNewsPaperPageId(System.Int32? NewsPaperPageId,string SelectClause=null);
		NewsPaperPagesPart GetNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId,string SelectClause=null);
		NewsPaperPagesPart UpdateNewsPaperPagesPart(NewsPaperPagesPart entity);
		bool DeleteNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId);
		NewsPaperPagesPart DeleteNewsPaperPagesPart(NewsPaperPagesPart entity);
		List<NewsPaperPagesPart> GetPagedNewsPaperPagesPart(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsPaperPagesPart> GetAllNewsPaperPagesPart(string SelectClause=null);
		NewsPaperPagesPart InsertNewsPaperPagesPart(NewsPaperPagesPart entity);
		List<NewsPaperPagesPart> GetNewsPaperPagesPartByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
