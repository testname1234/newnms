﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsPaperPagesPartRepository: INewsPaperPagesPartRepositoryBase
	{
         List<NewsPaperPagesPart> GetNewsPaperPagesPartsByDailyNewsPaperDate(DateTime fromDate, DateTime toDate);
	}
	
	
}
