﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsPaperPagesPartsDetailRepositoryBase
	{
        
        Dictionary<string, string> GetNewsPaperPagesPartsDetailBasicSearchColumns();
        List<SearchColumn> GetNewsPaperPagesPartsDetailSearchColumns();
        List<SearchColumn> GetNewsPaperPagesPartsDetailAdvanceSearchColumns();
        

		List<NewsPaperPagesPartsDetail> GetNewsPaperPagesPartsDetailByNewsPaperPagesPartId(System.Int32? NewsPaperPagesPartId,string SelectClause=null);
		NewsPaperPagesPartsDetail GetNewsPaperPagesPartsDetail(System.Int32 NewsPaperPagesPartsDetailId,string SelectClause=null);
		NewsPaperPagesPartsDetail UpdateNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetail entity);
		bool DeleteNewsPaperPagesPartsDetail(System.Int32 NewsPaperPagesPartsDetailId);
		NewsPaperPagesPartsDetail DeleteNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetail entity);
		List<NewsPaperPagesPartsDetail> GetPagedNewsPaperPagesPartsDetail(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsPaperPagesPartsDetail> GetAllNewsPaperPagesPartsDetail(string SelectClause=null);
		NewsPaperPagesPartsDetail InsertNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetail entity);
		List<NewsPaperPagesPartsDetail> GetNewsPaperPagesPartsDetailByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
