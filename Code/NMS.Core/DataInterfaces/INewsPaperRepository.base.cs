﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsPaperRepositoryBase
	{
        
        Dictionary<string, string> GetNewsPaperBasicSearchColumns();
        List<SearchColumn> GetNewsPaperSearchColumns();
        List<SearchColumn> GetNewsPaperAdvanceSearchColumns();
        

		NewsPaper GetNewsPaper(System.Int32 NewsPaperId,string SelectClause=null);
		NewsPaper UpdateNewsPaper(NewsPaper entity);
		bool DeleteNewsPaper(System.Int32 NewsPaperId);
		NewsPaper DeleteNewsPaper(NewsPaper entity);
		List<NewsPaper> GetPagedNewsPaper(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsPaper> GetAllNewsPaper(string SelectClause=null);
		NewsPaper InsertNewsPaper(NewsPaper entity);
		List<NewsPaper> GetNewsPaperByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
