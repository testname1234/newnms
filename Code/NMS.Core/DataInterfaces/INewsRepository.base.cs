﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsRepositoryBase
	{
        
        Dictionary<string, string> GetNewsBasicSearchColumns();
        List<SearchColumn> GetNewsSearchColumns();
        List<SearchColumn> GetNewsAdvanceSearchColumns();
        

		List<News> GetNewsByLocationId(System.Int32? LocationId,string SelectClause=null);
		List<News> GetNewsByNewsTypeId(System.Int32 NewsTypeId,string SelectClause=null);
		News GetNews(System.Int32 NewsId,string SelectClause=null);
		News UpdateNews(News entity);
		bool DeleteNews(System.Int32 NewsId);
		News DeleteNews(News entity);
		List<News> GetPagedNews(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<News> GetAllNews(string SelectClause=null);
		News InsertNews(News entity);
		List<News> GetNewsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
