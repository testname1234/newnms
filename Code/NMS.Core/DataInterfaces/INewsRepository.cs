﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using System.Data;
using NMS.Core.Models;

namespace NMS.Core.DataInterfaces
{

    public interface INewsRepository : INewsRepositoryBase
    {
        void UpdateCompleteNews(News entity);
        List<News> GetNonIndexedNews();
        string GetBunchByNewsId(System.Int32 NewsId);
        List<News> GetDisitnctNewsSource();
        DateTime GetLastNewsDate();
        News GetByNewsGuid(System.String NewsGuid, string SelectClause = null);
        News GetByNewsGuidAndIsComplete(System.String NewsGuid, bool isComplete, string SelectClause = null);
        News GetByNewsId(System.Int32 NewsId, string SelectClause = null);
        List<News> GetByBunchGuid(System.String BunchGuid, string SelectClause = null);
        void UpdateNewsIndex(News entity);
        void UpdateNewsBunch(News entity);
        bool DeletebyNewsId(System.Int32 NewsId);
        List<News> GetbyIsCompleted();
        List<string> GetRelatedNews(System.Int32 NewsId);
        void UpdateNewsBunch(string NewsGuid, string BunchGuid);
        bool CheckNewsMatchedStatus(System.Int32 SourceNewsId, int DestNewsId);
        void UpdateNewsByBunchId(int NewsId, string BunchGuid);
        List<string> GetRelatedNewsV2(System.Int32 NewsId, string BunchGuid);
        List<News> GetNonIndexedNewsOptimized();

        bool CheckIfNewsExistsSQL(System.String Title, string Source, string SelectClause = null);
        //bool CheckIfNewsExistsSQL(System.String Title, int SourceTypeId, int SourceFilterId, string SelectClause = null);
        void InsertNewsNoReturn(News entity);
        List<string> GetRelatedNewsV2Load(string NewsGuid, string BunchGuid);
        void UpdateNewsBunchbyNewsIds(string NewsGuidCsv, string BunchGuid);
        List<TickerImage> GetTickerImages(DateTime lastUpdateDate);
        List<TickerImage> GetTickerImagesByLastUpdateDate(DateTime lastUpdateDate);
    }


}
