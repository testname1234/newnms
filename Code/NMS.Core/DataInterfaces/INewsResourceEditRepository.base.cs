﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsResourceEditRepositoryBase
	{
        
        Dictionary<string, string> GetNewsResourceEditBasicSearchColumns();
        List<SearchColumn> GetNewsResourceEditSearchColumns();
        List<SearchColumn> GetNewsResourceEditAdvanceSearchColumns();
        

		List<NewsResourceEdit> GetNewsResourceEditByNewsId(System.Int32? NewsId,string SelectClause=null);
		NewsResourceEdit GetNewsResourceEdit(System.Int32 NewsResourceEditId,string SelectClause=null);
		NewsResourceEdit UpdateNewsResourceEdit(NewsResourceEdit entity);
		bool DeleteNewsResourceEdit(System.Int32 NewsResourceEditId);
		NewsResourceEdit DeleteNewsResourceEdit(NewsResourceEdit entity);
		List<NewsResourceEdit> GetPagedNewsResourceEdit(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsResourceEdit> GetAllNewsResourceEdit(string SelectClause=null);
		NewsResourceEdit InsertNewsResourceEdit(NewsResourceEdit entity);
		List<NewsResourceEdit> GetNewsResourceEditByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
