﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsResourceEditRepository: INewsResourceEditRepositoryBase
	{
        NewsResourceEdit InsertNewsResourceEdit(NewsResourceEdit entity);

        List<NewsResourceEdit> GetActiveNewsResourceEdit();

        bool SetInactive(int id);
    }
	
	
}
