﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsResourceRepositoryBase
	{
        
        Dictionary<string, string> GetNewsResourceBasicSearchColumns();
        List<SearchColumn> GetNewsResourceSearchColumns();
        List<SearchColumn> GetNewsResourceAdvanceSearchColumns();
        

		NewsResource GetNewsResource(System.Int32 NewsResourceId,string SelectClause=null);
		NewsResource UpdateNewsResource(NewsResource entity);
		bool DeleteNewsResource(System.Int32 NewsResourceId);
		NewsResource DeleteNewsResource(NewsResource entity);
		List<NewsResource> GetPagedNewsResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsResource> GetAllNewsResource(string SelectClause=null);
		NewsResource InsertNewsResource(NewsResource entity);
		List<NewsResource> GetNewsResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);
        List<NewsResource> GetNewsResourceByNewsId(System.Int32 NewsId, string SelectClause = null);
    
    }
	
	
}
