﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsResourceRepository: INewsResourceRepositoryBase
	{
        bool DeleteNewsResourceByNewsId(System.Int32 NewsId);
	}
	
	
}
