﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsSourceRepositoryBase
	{
        
        Dictionary<string, string> GetNewsSourceBasicSearchColumns();
        List<SearchColumn> GetNewsSourceSearchColumns();
        List<SearchColumn> GetNewsSourceAdvanceSearchColumns();
        

		NewsSource GetNewsSource(System.Int32 NewsSourceId,string SelectClause=null);
		NewsSource UpdateNewsSource(NewsSource entity);
		bool DeleteNewsSource(System.Int32 NewsSourceId);
		NewsSource DeleteNewsSource(NewsSource entity);
		List<NewsSource> GetPagedNewsSource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsSource> GetAllNewsSource(string SelectClause=null);
		NewsSource InsertNewsSource(NewsSource entity);
		List<NewsSource> GetNewsSourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
