﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsStatisticsRepositoryBase
	{
        
        Dictionary<string, string> GetNewsStatisticsBasicSearchColumns();
        List<SearchColumn> GetNewsStatisticsSearchColumns();
        List<SearchColumn> GetNewsStatisticsAdvanceSearchColumns();
        

		NewsStatistics GetNewsStatistics(System.Int32 NewsStatisticsId,string SelectClause=null);
		NewsStatistics UpdateNewsStatistics(NewsStatistics entity);
		bool DeleteNewsStatistics(System.Int32 NewsStatisticsId);
		NewsStatistics DeleteNewsStatistics(NewsStatistics entity);
		List<NewsStatistics> GetPagedNewsStatistics(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsStatistics> GetAllNewsStatistics(string SelectClause=null);
		NewsStatistics InsertNewsStatistics(NewsStatistics entity);
		List<NewsStatistics> GetNewsStatisticsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
