﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsTagRepositoryBase
	{
        
        Dictionary<string, string> GetNewsTagBasicSearchColumns();
        List<SearchColumn> GetNewsTagSearchColumns();
        List<SearchColumn> GetNewsTagAdvanceSearchColumns();
        

		List<NewsTag> GetNewsTagByTagId(System.Int32 TagId,string SelectClause=null);
		List<NewsTag> GetNewsTagByNewsId(System.Int32 NewsId,string SelectClause=null);
		NewsTag GetNewsTag(System.Int32 NewsTagId,string SelectClause=null);
		NewsTag UpdateNewsTag(NewsTag entity);
		bool DeleteNewsTag(System.Int32 NewsTagId);
		NewsTag DeleteNewsTag(NewsTag entity);
		List<NewsTag> GetPagedNewsTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsTag> GetAllNewsTag(string SelectClause=null);
		NewsTag InsertNewsTag(NewsTag entity);
		List<NewsTag> GetNewsTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
