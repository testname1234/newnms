﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsTagRepository: INewsTagRepositoryBase
	{
        bool DeleteNewsTagBYNewsId(System.Int32 NewsId);
	}
	
	
}
