﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsTypeRepositoryBase
	{
        
        Dictionary<string, string> GetNewsTypeBasicSearchColumns();
        List<SearchColumn> GetNewsTypeSearchColumns();
        List<SearchColumn> GetNewsTypeAdvanceSearchColumns();
        

		NewsType GetNewsType(System.Int32 NewsTypeId,string SelectClause=null);
		NewsType UpdateNewsType(NewsType entity);
		bool DeleteNewsType(System.Int32 NewsTypeId);
		NewsType DeleteNewsType(NewsType entity);
		List<NewsType> GetPagedNewsType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsType> GetAllNewsType(string SelectClause=null);
		NewsType InsertNewsType(NewsType entity);
		List<NewsType> GetNewsTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
