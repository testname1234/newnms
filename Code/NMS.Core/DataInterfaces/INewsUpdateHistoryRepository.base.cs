﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsUpdateHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetNewsUpdateHistoryBasicSearchColumns();
        List<SearchColumn> GetNewsUpdateHistorySearchColumns();
        List<SearchColumn> GetNewsUpdateHistoryAdvanceSearchColumns();
        

		NewsUpdateHistory GetNewsUpdateHistory(System.Int32 NewsUpdateHistoryId,string SelectClause=null);
		NewsUpdateHistory UpdateNewsUpdateHistory(NewsUpdateHistory entity);
		bool DeleteNewsUpdateHistory(System.Int32 NewsUpdateHistoryId);
		NewsUpdateHistory DeleteNewsUpdateHistory(NewsUpdateHistory entity);
		List<NewsUpdateHistory> GetPagedNewsUpdateHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NewsUpdateHistory> GetAllNewsUpdateHistory(string SelectClause=null);
		NewsUpdateHistory InsertNewsUpdateHistory(NewsUpdateHistory entity);
		List<NewsUpdateHistory> GetNewsUpdateHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
