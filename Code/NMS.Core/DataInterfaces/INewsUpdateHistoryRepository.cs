﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INewsUpdateHistoryRepository: INewsUpdateHistoryRepositoryBase
	{
        List<NewsUpdateHistory> GetByGuid(System.String NewsGuid, string SelectClause = null);
	}
	
	
}
