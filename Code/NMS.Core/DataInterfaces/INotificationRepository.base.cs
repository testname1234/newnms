﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INotificationRepositoryBase
	{
        
        Dictionary<string, string> GetNotificationBasicSearchColumns();
        List<SearchColumn> GetNotificationSearchColumns();
        List<SearchColumn> GetNotificationAdvanceSearchColumns();
        

		List<Notification> GetNotificationByNotificationTypeId(System.Int32 NotificationTypeId,string SelectClause=null);
		Notification GetNotification(System.Int32 NotificationId,string SelectClause=null);
		Notification UpdateNotification(Notification entity);
		bool DeleteNotification(System.Int32 NotificationId);
		Notification DeleteNotification(Notification entity);
		List<Notification> GetPagedNotification(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Notification> GetAllNotification(string SelectClause=null);
		Notification InsertNotification(Notification entity);
		List<Notification> GetNotificationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
