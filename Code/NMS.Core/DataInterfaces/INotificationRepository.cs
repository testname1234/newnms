﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{	
	public interface INotificationRepository: INotificationRepositoryBase
	{
        List<Notification> GetNotifications(int userId, DateTime? lastUpdateDate = null, int rowCount = 10);
        bool MarkAsRead(System.Int32 notificationId);
        bool MarkAllAsRead(int userId);
        bool DeleteByJsonSlotId(int slotId);
        bool DeleteBySlotId(int slotId);
        bool DeleteBySlotScreenTemplateId(int Slotscreentemplateid);
	}
}
