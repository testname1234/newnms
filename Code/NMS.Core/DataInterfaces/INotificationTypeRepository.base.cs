﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface INotificationTypeRepositoryBase
	{
        
        Dictionary<string, string> GetNotificationTypeBasicSearchColumns();
        List<SearchColumn> GetNotificationTypeSearchColumns();
        List<SearchColumn> GetNotificationTypeAdvanceSearchColumns();
        

		NotificationType GetNotificationType(System.Int32 NotificationTypeId,string SelectClause=null);
		NotificationType UpdateNotificationType(NotificationType entity);
		bool DeleteNotificationType(System.Int32 NotificationTypeId);
		NotificationType DeleteNotificationType(NotificationType entity);
		List<NotificationType> GetPagedNotificationType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<NotificationType> GetAllNotificationType(string SelectClause=null);
		NotificationType InsertNotificationType(NotificationType entity);
		List<NotificationType> GetNotificationTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
