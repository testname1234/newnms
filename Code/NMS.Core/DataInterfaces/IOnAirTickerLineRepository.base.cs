﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IOnAirTickerLineRepositoryBase
	{
        
        Dictionary<string, string> GetOnAirTickerLineBasicSearchColumns();
        List<SearchColumn> GetOnAirTickerLineSearchColumns();
        List<SearchColumn> GetOnAirTickerLineAdvanceSearchColumns();
        

		List<OnAirTickerLine> GetOnAirTickerLineByTickerId(System.Int32? TickerId,string SelectClause=null);
		OnAirTickerLine GetOnAirTickerLine(System.Int32 TickerLineId,string SelectClause=null);
		OnAirTickerLine UpdateOnAirTickerLine(OnAirTickerLine entity);
		bool DeleteOnAirTickerLine(System.Int32 TickerLineId);
		OnAirTickerLine DeleteOnAirTickerLine(OnAirTickerLine entity);
		List<OnAirTickerLine> GetPagedOnAirTickerLine(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<OnAirTickerLine> GetAllOnAirTickerLine(string SelectClause=null);
		OnAirTickerLine InsertOnAirTickerLine(OnAirTickerLine entity);
		List<OnAirTickerLine> GetOnAirTickerLineByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
