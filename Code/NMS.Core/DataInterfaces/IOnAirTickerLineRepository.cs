﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IOnAirTickerLineRepository: IOnAirTickerLineRepositoryBase
	{
        List<OnAirTickerLine> SubmitOnAirTickerLineToMcrBreaking(OnAirTickerLine OnAirTickerLine);
        List<OnAirTickerLine> SubmitOnAirTickerLineToMcrLatest(OnAirTickerLine OnAirTickerLine);
        List<OnAirTickerLine> SubmitOnAirTickerLineToMcrCategory(OnAirTickerLine temp);

        bool DeleteOnAirTickerLineFromMCRCategory(OnAirTickerLine tempObj);
        bool DeleteOnAirTickerLineFromMCRLatest(OnAirTickerLine tempObj);
        bool DeleteOnAirTickerLineFromMCRBreaking(OnAirTickerLine tempObj);

        bool MigrateDataToOnAiredTable(OnAirTickerLine tempObj,int type);
        bool MigrateGroupToOnAiredTable(OnAirTickerLine tempObj, int type);



        OnAirTickerLine GetTickerLineBySeqNumAndTypeAndOnAirTickerId(int seqId, int tickerId,int type);

        OnAirTickerLine UpdateOnAirTickerLineSequenceNumber(OnAirTickerLine item);

        List<OnAirTickerLine> GetLatestOnAirTickerLine(DateTime dateTime);
    }
	
	
}
