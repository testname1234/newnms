﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IOnAirTickerRepositoryBase
	{
        
        Dictionary<string, string> GetOnAirTickerBasicSearchColumns();
        List<SearchColumn> GetOnAirTickerSearchColumns();
        List<SearchColumn> GetOnAirTickerAdvanceSearchColumns();
        

		OnAirTicker GetOnAirTicker(System.Int32 TickerId,string SelectClause=null);
		OnAirTicker UpdateOnAirTicker(OnAirTicker entity);
		bool DeleteOnAirTicker(System.Int32 TickerId);
		OnAirTicker DeleteOnAirTicker(OnAirTicker entity);
		List<OnAirTicker> GetPagedOnAirTicker(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<OnAirTicker> GetAllOnAirTicker(string SelectClause=null);
		OnAirTicker InsertOnAirTicker(OnAirTicker entity);
		List<OnAirTicker> GetOnAirTickerByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
