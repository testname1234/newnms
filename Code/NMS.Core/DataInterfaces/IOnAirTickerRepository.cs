﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IOnAirTickerRepository: IOnAirTickerRepositoryBase
	{
        OnAirTicker UpdateOnAirTickerSequenceNumber(OnAirTicker item);
        Dictionary<string, int> GetSequenceNumbers();

        List<OnAirTicker> GetLatestOnAirTicker(DateTime dateTime);
    }
	
	
}
