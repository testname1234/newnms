﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IOrganizationRepositoryBase
	{
        
        Dictionary<string, string> GetOrganizationBasicSearchColumns();
        List<SearchColumn> GetOrganizationSearchColumns();
        List<SearchColumn> GetOrganizationAdvanceSearchColumns();
        

		Organization GetOrganization(System.Int32 OrganizationId,string SelectClause=null);
		Organization UpdateOrganization(Organization entity);
		bool DeleteOrganization(System.Int32 OrganizationId);
		Organization DeleteOrganization(Organization entity);
		List<Organization> GetPagedOrganization(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Organization> GetAllOrganization(string SelectClause=null);
		Organization InsertOrganization(Organization entity);

    }
}
