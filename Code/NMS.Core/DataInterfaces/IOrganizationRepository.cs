﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IOrganizationRepository: IOrganizationRepositoryBase
	{
        List<Organization> GetOrganizationByTerm(string term);
        Organization GetOrganizationByName(string Name);
        

    }
	
	
}
