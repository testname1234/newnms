﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramAnchorRepositoryBase
	{
        
        Dictionary<string, string> GetProgramAnchorBasicSearchColumns();
        List<SearchColumn> GetProgramAnchorSearchColumns();
        List<SearchColumn> GetProgramAnchorAdvanceSearchColumns();
        

		List<ProgramAnchor> GetProgramAnchorByProgramId(System.Int32 ProgramId,string SelectClause=null);
		List<ProgramAnchor> GetProgramAnchorByCelebrityId(System.Int32 CelebrityId,string SelectClause=null);
		ProgramAnchor GetProgramAnchor(System.Int32 ProgramAnchorId,string SelectClause=null);
		ProgramAnchor UpdateProgramAnchor(ProgramAnchor entity);
		bool DeleteProgramAnchor(System.Int32 ProgramAnchorId);
		ProgramAnchor DeleteProgramAnchor(ProgramAnchor entity);
		List<ProgramAnchor> GetPagedProgramAnchor(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramAnchor> GetAllProgramAnchor(string SelectClause=null);
		ProgramAnchor InsertProgramAnchor(ProgramAnchor entity);
		List<ProgramAnchor> GetProgramAnchorByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
