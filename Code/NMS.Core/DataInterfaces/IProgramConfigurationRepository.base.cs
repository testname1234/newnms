﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramConfigurationRepositoryBase
	{
        
        Dictionary<string, string> GetProgramConfigurationBasicSearchColumns();
        List<SearchColumn> GetProgramConfigurationSearchColumns();
        List<SearchColumn> GetProgramConfigurationAdvanceSearchColumns();
        

		List<ProgramConfiguration> GetProgramConfigurationByProgramId(System.Int32 ProgramId,string SelectClause=null);
		List<ProgramConfiguration> GetProgramConfigurationBySegmentTypeId(System.Int32 SegmentTypeId,string SelectClause=null);
		ProgramConfiguration GetProgramConfiguration(System.Int32 ProgramConfigurationId,string SelectClause=null);
		ProgramConfiguration UpdateProgramConfiguration(ProgramConfiguration entity);
		bool DeleteProgramConfiguration(System.Int32 ProgramConfigurationId);
		ProgramConfiguration DeleteProgramConfiguration(ProgramConfiguration entity);
		List<ProgramConfiguration> GetPagedProgramConfiguration(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramConfiguration> GetAllProgramConfiguration(string SelectClause=null);
		ProgramConfiguration InsertProgramConfiguration(ProgramConfiguration entity);
		List<ProgramConfiguration> GetProgramConfigurationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
