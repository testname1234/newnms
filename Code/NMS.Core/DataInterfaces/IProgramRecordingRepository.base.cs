﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramRecordingRepositoryBase
	{
        
        Dictionary<string, string> GetProgramRecordingBasicSearchColumns();
        List<SearchColumn> GetProgramRecordingSearchColumns();
        List<SearchColumn> GetProgramRecordingAdvanceSearchColumns();
        

		List<ProgramRecording> GetProgramRecordingByProgramId(System.Int32? ProgramId,string SelectClause=null);
		ProgramRecording GetProgramRecording(System.Int32 ProgramRecordingId,string SelectClause=null);
		ProgramRecording UpdateProgramRecording(ProgramRecording entity);
		bool DeleteProgramRecording(System.Int32 ProgramRecordingId);
		ProgramRecording DeleteProgramRecording(ProgramRecording entity);
		List<ProgramRecording> GetPagedProgramRecording(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramRecording> GetAllProgramRecording(string SelectClause=null);
		ProgramRecording InsertProgramRecording(ProgramRecording entity);
		List<ProgramRecording> GetProgramRecordingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
