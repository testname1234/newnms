﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramRepositoryBase
	{
        
        Dictionary<string, string> GetProgramBasicSearchColumns();
        List<SearchColumn> GetProgramSearchColumns();
        List<SearchColumn> GetProgramAdvanceSearchColumns();
        

		List<Program> GetProgramByChannelId(System.Int32? ChannelId,string SelectClause=null);
		Program GetProgram(System.Int32 ProgramId,string SelectClause=null);
		Program UpdateProgram(Program entity);
		bool DeleteProgram(System.Int32 ProgramId);
		Program DeleteProgram(Program entity);
		List<Program> GetPagedProgram(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Program> GetAllProgram(string SelectClause=null);
        
        
        Program InsertProgram(Program entity);
		List<Program> GetProgramByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
