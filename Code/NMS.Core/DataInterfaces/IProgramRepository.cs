﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramRepository: IProgramRepositoryBase
	{
        List<Program> GetProgramByUserTeam(int userid);
        List<Program> GetProgramByChannelIdAndUserId(int ChannelId, int UserId);
        List<Program> GetProgramByMultipleKeyValue(string Key1, string Value1, string Key2, string Value2, Operands operand, string SelectClause = null);
        List<Program> GetAllProgramsForReporter(string SelectClause = null);
        int GetProgramIdByBroadcastedTime(int second, int interval);
    }
	
	
}
