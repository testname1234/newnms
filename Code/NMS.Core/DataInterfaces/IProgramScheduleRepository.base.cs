﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramScheduleRepositoryBase
	{
        
        Dictionary<string, string> GetProgramScheduleBasicSearchColumns();
        List<SearchColumn> GetProgramScheduleSearchColumns();
        List<SearchColumn> GetProgramScheduleAdvanceSearchColumns();
        

		List<ProgramSchedule> GetProgramScheduleByProgramId(System.Int32 ProgramId,string SelectClause=null);
		List<ProgramSchedule> GetProgramScheduleByRecurringTypeId(System.Int32 RecurringTypeId,string SelectClause=null);
		ProgramSchedule GetProgramSchedule(System.Int32 ProgramScheduleId,string SelectClause=null);
		ProgramSchedule UpdateProgramSchedule(ProgramSchedule entity);
		bool DeleteProgramSchedule(System.Int32 ProgramScheduleId);
		ProgramSchedule DeleteProgramSchedule(ProgramSchedule entity);
		List<ProgramSchedule> GetPagedProgramSchedule(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramSchedule> GetAllProgramSchedule(string SelectClause=null);
		ProgramSchedule InsertProgramSchedule(ProgramSchedule entity);
		List<ProgramSchedule> GetProgramScheduleByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);

    }
	
}
