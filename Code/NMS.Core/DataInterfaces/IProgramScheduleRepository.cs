﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramScheduleRepository: IProgramScheduleRepositoryBase
	{
        bool ifExistProgramSchedule(ProgramSchedule entity, string SelectClause = null);
	}
	
	
}
