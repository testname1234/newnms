﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramScreenElementRepositoryBase
	{
        
        Dictionary<string, string> GetProgramScreenElementBasicSearchColumns();
        List<SearchColumn> GetProgramScreenElementSearchColumns();
        List<SearchColumn> GetProgramScreenElementAdvanceSearchColumns();
        

		List<ProgramScreenElement> GetProgramScreenElementByProgramId(System.Int32 ProgramId,string SelectClause=null);
		ProgramScreenElement GetProgramScreenElement(System.Int32 ProgramScreenElementId,string SelectClause=null);
		ProgramScreenElement UpdateProgramScreenElement(ProgramScreenElement entity);
		bool DeleteProgramScreenElement(System.Int32 ProgramScreenElementId);
		ProgramScreenElement DeleteProgramScreenElement(ProgramScreenElement entity);
		List<ProgramScreenElement> GetPagedProgramScreenElement(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramScreenElement> GetAllProgramScreenElement(string SelectClause=null);
		ProgramScreenElement InsertProgramScreenElement(ProgramScreenElement entity);
		List<ProgramScreenElement> GetProgramScreenElementByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
