﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramSegmentRepositoryBase
	{
        
        Dictionary<string, string> GetProgramSegmentBasicSearchColumns();
        List<SearchColumn> GetProgramSegmentSearchColumns();
        List<SearchColumn> GetProgramSegmentAdvanceSearchColumns();
        

		List<ProgramSegment> GetProgramSegmentByProgramId(System.Int32 ProgramId,string SelectClause=null);
		List<ProgramSegment> GetProgramSegmentBySegmentTypeId(System.Int32 SegmentTypeId,string SelectClause=null);
		ProgramSegment GetProgramSegment(System.Int32 ProgramSegmentId,string SelectClause=null);
		ProgramSegment UpdateProgramSegment(ProgramSegment entity);
		bool DeleteProgramSegment(System.Int32 ProgramSegmentId);
		ProgramSegment DeleteProgramSegment(ProgramSegment entity);
		List<ProgramSegment> GetPagedProgramSegment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramSegment> GetAllProgramSegment(string SelectClause=null);
		ProgramSegment InsertProgramSegment(ProgramSegment entity);
		List<ProgramSegment> GetProgramSegmentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
