﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramSegmentRepository: IProgramSegmentRepositoryBase
	{
        bool DeleteProgramSegmentByProgramId(System.Int32 ProgramId);
	}
	
	
}
