﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramSetMappingRepositoryBase
	{
        
        Dictionary<string, string> GetProgramSetMappingBasicSearchColumns();
        List<SearchColumn> GetProgramSetMappingSearchColumns();
        List<SearchColumn> GetProgramSetMappingAdvanceSearchColumns();
        

		ProgramSetMapping GetProgramSetMapping(System.Int32 ProgramSetMappingId,string SelectClause=null);
		ProgramSetMapping UpdateProgramSetMapping(ProgramSetMapping entity);
		bool DeleteProgramSetMapping(System.Int32 ProgramSetMappingId);
		ProgramSetMapping DeleteProgramSetMapping(ProgramSetMapping entity);
		List<ProgramSetMapping> GetPagedProgramSetMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramSetMapping> GetAllProgramSetMapping(string SelectClause=null);
		ProgramSetMapping InsertProgramSetMapping(ProgramSetMapping entity);
		List<ProgramSetMapping> GetProgramSetMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
