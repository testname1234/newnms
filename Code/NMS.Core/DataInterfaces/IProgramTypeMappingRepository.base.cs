﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramTypeMappingRepositoryBase
	{
        
        Dictionary<string, string> GetProgramTypeMappingBasicSearchColumns();
        List<SearchColumn> GetProgramTypeMappingSearchColumns();
        List<SearchColumn> GetProgramTypeMappingAdvanceSearchColumns();
        

		List<ProgramTypeMapping> GetProgramTypeMappingByProgramId(System.Int32? ProgramId,string SelectClause=null);
		List<ProgramTypeMapping> GetProgramTypeMappingByProgramTypeId(System.Int32? ProgramTypeId,string SelectClause=null);
		ProgramTypeMapping GetProgramTypeMapping(System.Int32 ProgramTypeMappingId,string SelectClause=null);
		ProgramTypeMapping UpdateProgramTypeMapping(ProgramTypeMapping entity);
		bool DeleteProgramTypeMapping(System.Int32 ProgramTypeMappingId);
		ProgramTypeMapping DeleteProgramTypeMapping(ProgramTypeMapping entity);
		List<ProgramTypeMapping> GetPagedProgramTypeMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramTypeMapping> GetAllProgramTypeMapping(string SelectClause=null);
		ProgramTypeMapping InsertProgramTypeMapping(ProgramTypeMapping entity);
		List<ProgramTypeMapping> GetProgramTypeMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
