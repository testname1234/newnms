﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IProgramTypeRepositoryBase
	{
        
        Dictionary<string, string> GetProgramTypeBasicSearchColumns();
        List<SearchColumn> GetProgramTypeSearchColumns();
        List<SearchColumn> GetProgramTypeAdvanceSearchColumns();
        

		ProgramType GetProgramType(System.Int32 ProgramTypeId,string SelectClause=null);
		ProgramType UpdateProgramType(ProgramType entity);
		bool DeleteProgramType(System.Int32 ProgramTypeId);
		ProgramType DeleteProgramType(ProgramType entity);
		List<ProgramType> GetPagedProgramType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ProgramType> GetAllProgramType(string SelectClause=null);
		ProgramType InsertProgramType(ProgramType entity);
		List<ProgramType> GetProgramTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
