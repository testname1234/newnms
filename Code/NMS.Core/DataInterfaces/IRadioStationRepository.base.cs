﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRadioStationRepositoryBase
	{
        
        Dictionary<string, string> GetRadioStationBasicSearchColumns();
        List<SearchColumn> GetRadioStationSearchColumns();
        List<SearchColumn> GetRadioStationAdvanceSearchColumns();
        

		RadioStation GetRadioStation(System.Int32 RadioStationId,string SelectClause=null);
		RadioStation UpdateRadioStation(RadioStation entity);
		bool DeleteRadioStation(System.Int32 RadioStationId);
		RadioStation DeleteRadioStation(RadioStation entity);
		List<RadioStation> GetPagedRadioStation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<RadioStation> GetAllRadioStation(string SelectClause=null);
		RadioStation InsertRadioStation(RadioStation entity);
		List<RadioStation> GetRadioStationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
