﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRadioStreamRepositoryBase
	{
        
        Dictionary<string, string> GetRadioStreamBasicSearchColumns();
        List<SearchColumn> GetRadioStreamSearchColumns();
        List<SearchColumn> GetRadioStreamAdvanceSearchColumns();
        

		List<RadioStream> GetRadioStreamByRadioStationId(System.Int32 RadioStationId,string SelectClause=null);
		RadioStream GetRadioStream(System.Int32 RadioStreamId,string SelectClause=null);
		RadioStream UpdateRadioStream(RadioStream entity);
		bool DeleteRadioStream(System.Int32 RadioStreamId);
		RadioStream DeleteRadioStream(RadioStream entity);
		List<RadioStream> GetPagedRadioStream(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<RadioStream> GetAllRadioStream(string SelectClause=null);
		RadioStream InsertRadioStream(RadioStream entity);
		List<RadioStream> GetRadioStreamByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
