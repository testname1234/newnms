﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRadioStreamRepository: IRadioStreamRepositoryBase
	{
        List<RadioStream> GetRadioStreamBetweenDates(System.DateTime fromDate, System.DateTime toDate, string SelectClause = null);
        void MarkIsProcessed(int id);
	}
	
	
}
