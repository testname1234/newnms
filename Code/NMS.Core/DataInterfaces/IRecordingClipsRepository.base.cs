﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRecordingClipsRepositoryBase
	{
        
        Dictionary<string, string> GetRecordingClipsBasicSearchColumns();
        List<SearchColumn> GetRecordingClipsSearchColumns();
        List<SearchColumn> GetRecordingClipsAdvanceSearchColumns();
        

		List<RecordingClips> GetRecordingClipsByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		List<RecordingClips> GetRecordingClipsByProgramRecordingId(System.Int32? ProgramRecordingId,string SelectClause=null);
		RecordingClips GetRecordingClips(System.Int32 RecordingClipId,string SelectClause=null);
		RecordingClips UpdateRecordingClips(RecordingClips entity);

        
        bool DeleteRecordingClips(System.Int32 RecordingClipId);
		RecordingClips DeleteRecordingClips(RecordingClips entity);
		List<RecordingClips> GetPagedRecordingClips(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<RecordingClips> GetAllRecordingClips(string SelectClause=null);
		RecordingClips InsertRecordingClips(RecordingClips entity);
		List<RecordingClips> GetRecordingClipsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
