﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRecordingClipsRepository: IRecordingClipsRepositoryBase
	{
        List<RecordingClips> GetAllSameRecordings(RecordingClips entity);

        List<RecordingClips> GetAllRecordingsByParentId(int id);

        RecordingClips UpdateRecordingClipsByParentId(RecordingClips entity, int parentID);
    }
	
	
}
