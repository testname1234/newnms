﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRecurringTypeRepositoryBase
	{
        
        Dictionary<string, string> GetRecurringTypeBasicSearchColumns();
        List<SearchColumn> GetRecurringTypeSearchColumns();
        List<SearchColumn> GetRecurringTypeAdvanceSearchColumns();
        

		RecurringType GetRecurringType(System.Int32 RecurringTypeId,string SelectClause=null);
		RecurringType UpdateRecurringType(RecurringType entity);
		bool DeleteRecurringType(System.Int32 RecurringTypeId);
		RecurringType DeleteRecurringType(RecurringType entity);
		List<RecurringType> GetPagedRecurringType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<RecurringType> GetAllRecurringType(string SelectClause=null);
		RecurringType InsertRecurringType(RecurringType entity);
		List<RecurringType> GetRecurringTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
