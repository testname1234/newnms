﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IReelRepositoryBase
	{
        
        Dictionary<string, string> GetReelBasicSearchColumns();
        List<SearchColumn> GetReelSearchColumns();
        List<SearchColumn> GetReelAdvanceSearchColumns();
        

		List<Reel> GetReelByStatusId(System.Int32 StatusId,string SelectClause=null);
		Reel GetReel(System.Int32 ReelId,string SelectClause=null);
		Reel UpdateReel(Reel entity);
		bool DeleteReel(System.Int32 ReelId);
		Reel DeleteReel(Reel entity);
		List<Reel> GetPagedReel(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Reel> GetAllReel(string SelectClause=null);
		Reel InsertReel(Reel entity);
		List<Reel> GetReelByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
