﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IReelStatusRepositoryBase
	{
        
        Dictionary<string, string> GetReelStatusBasicSearchColumns();
        List<SearchColumn> GetReelStatusSearchColumns();
        List<SearchColumn> GetReelStatusAdvanceSearchColumns();
        

		ReelStatus GetReelStatus(System.Int32 ReelStatusId,string SelectClause=null);
		ReelStatus UpdateReelStatus(ReelStatus entity);
		bool DeleteReelStatus(System.Int32 ReelStatusId);
		ReelStatus DeleteReelStatus(ReelStatus entity);
		List<ReelStatus> GetPagedReelStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ReelStatus> GetAllReelStatus(string SelectClause=null);
		ReelStatus InsertReelStatus(ReelStatus entity);
		List<ReelStatus> GetReelStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
