﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IResourceEditclipinfoRepositoryBase
	{
        
        Dictionary<string, string> GetResourceEditclipinfoBasicSearchColumns();
        List<SearchColumn> GetResourceEditclipinfoSearchColumns();
        List<SearchColumn> GetResourceEditclipinfoAdvanceSearchColumns();
        

		List<ResourceEditclipinfo> GetResourceEditclipinfoByNewsResourceEditId(System.Int32? NewsResourceEditId,string SelectClause=null);
		ResourceEditclipinfo GetResourceEditclipinfo(System.Int32 ResourceEditclipinfoId,string SelectClause=null);
		ResourceEditclipinfo UpdateResourceEditclipinfo(ResourceEditclipinfo entity);
		bool DeleteResourceEditclipinfo(System.Int32 ResourceEditclipinfoId);
		ResourceEditclipinfo DeleteResourceEditclipinfo(ResourceEditclipinfo entity);
		List<ResourceEditclipinfo> GetPagedResourceEditclipinfo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ResourceEditclipinfo> GetAllResourceEditclipinfo(string SelectClause=null);
		ResourceEditclipinfo InsertResourceEditclipinfo(ResourceEditclipinfo entity);
		List<ResourceEditclipinfo> GetResourceEditclipinfoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
