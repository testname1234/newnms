﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IResourceEditclipinfoRepository: IResourceEditclipinfoRepositoryBase
	{
        bool DeleteByNewsResourceEditId(System.Int32 NewsResourceEditId);
	}
	
	
}
