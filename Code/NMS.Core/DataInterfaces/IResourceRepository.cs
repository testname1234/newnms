﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using System.Data.SqlClient;

namespace NMS.Core.DataInterfaces
{
		
	public interface IResourceRepository: IResourceRepositoryBase
	{
        DateTime GetLastResourceDate();
        Resource GetResourceBuGuid(string Guid, string SelectClause = null);
        List<Resource> GetByUpdatedDate(System.DateTime UpdatedDate, string SelectClause = null);
        void InsertResourceNoReturn(Resource entity);
        void UpdateResourceNoReturn(Resource entity);

	}
	
	
}
