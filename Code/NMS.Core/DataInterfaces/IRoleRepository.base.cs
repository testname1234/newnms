﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRoleRepositoryBase
	{
        
        Dictionary<string, string> GetRoleBasicSearchColumns();
        List<SearchColumn> GetRoleSearchColumns();
        List<SearchColumn> GetRoleAdvanceSearchColumns();
        

		Role GetRole(System.Int32 RoleId,string SelectClause=null);
		Role UpdateRole(Role entity);
		bool DeleteRole(System.Int32 RoleId);
		Role DeleteRole(Role entity);
		List<Role> GetPagedRole(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Role> GetAllRole(string SelectClause=null);
		Role InsertRole(Role entity);
		List<Role> GetRoleByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
