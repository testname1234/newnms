﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRunOrderLogRepositoryBase
	{
        
        Dictionary<string, string> GetRunOrderLogBasicSearchColumns();
        List<SearchColumn> GetRunOrderLogSearchColumns();
        List<SearchColumn> GetRunOrderLogAdvanceSearchColumns();
        

		RunOrderLog GetRunOrderLog(System.Int32 RunOrderLogId,string SelectClause=null);
		RunOrderLog UpdateRunOrderLog(RunOrderLog entity);
		bool DeleteRunOrderLog(System.Int32 RunOrderLogId);
		RunOrderLog DeleteRunOrderLog(RunOrderLog entity);
		List<RunOrderLog> GetPagedRunOrderLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<RunOrderLog> GetAllRunOrderLog(string SelectClause=null);
		RunOrderLog InsertRunOrderLog(RunOrderLog entity);
		List<RunOrderLog> GetRunOrderLogByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
