﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IRunOrderStoriesRepositoryBase
	{
        
        Dictionary<string, string> GetRunOrderStoriesBasicSearchColumns();
        List<SearchColumn> GetRunOrderStoriesSearchColumns();
        List<SearchColumn> GetRunOrderStoriesAdvanceSearchColumns();
        

		List<RunOrderStories> GetRunOrderStoriesByRunOrderLogId(System.Int32 RunOrderLogId,string SelectClause=null);
		RunOrderStories GetRunOrderStories(System.Int32 RunOrderStoryId,string SelectClause=null);
		RunOrderStories UpdateRunOrderStories(RunOrderStories entity);
		bool DeleteRunOrderStories(System.Int32 RunOrderStoryId);
		RunOrderStories DeleteRunOrderStories(RunOrderStories entity);
		List<RunOrderStories> GetPagedRunOrderStories(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<RunOrderStories> GetAllRunOrderStories(string SelectClause=null);
		RunOrderStories InsertRunOrderStories(RunOrderStories entity);
		List<RunOrderStories> GetRunOrderStoriesByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
