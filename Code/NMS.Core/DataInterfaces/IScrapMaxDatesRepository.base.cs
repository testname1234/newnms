﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScrapMaxDatesRepositoryBase
	{
        
        Dictionary<string, string> GetScrapMaxDatesBasicSearchColumns();
        List<SearchColumn> GetScrapMaxDatesSearchColumns();
        List<SearchColumn> GetScrapMaxDatesAdvanceSearchColumns();
        

		ScrapMaxDates GetScrapMaxDates(System.Int32 ScrapMaxDatesId,string SelectClause=null);
		ScrapMaxDates UpdateScrapMaxDates(ScrapMaxDates entity);
		bool DeleteScrapMaxDates(System.Int32 ScrapMaxDatesId);
		ScrapMaxDates DeleteScrapMaxDates(ScrapMaxDates entity);
		List<ScrapMaxDates> GetPagedScrapMaxDates(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ScrapMaxDates> GetAllScrapMaxDates(string SelectClause=null);
		ScrapMaxDates InsertScrapMaxDates(ScrapMaxDates entity);
		List<ScrapMaxDates> GetScrapMaxDatesByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
