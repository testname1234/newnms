﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using System.Data;

namespace NMS.Core.DataInterfaces
{

    public interface IScrapMaxDatesRepository : IScrapMaxDatesRepositoryBase
    {                
        ScrapMaxDates GetScrapMaxDatesBySource(System.String Source, string SelectClause = null);
        ScrapMaxDates UpdateScrapMaxDates(ScrapMaxDates entity);
        DataTable GetResult();

    }


}
