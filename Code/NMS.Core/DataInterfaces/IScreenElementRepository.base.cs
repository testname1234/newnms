﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScreenElementRepositoryBase
	{
        
        Dictionary<string, string> GetScreenElementBasicSearchColumns();
        List<SearchColumn> GetScreenElementSearchColumns();
        List<SearchColumn> GetScreenElementAdvanceSearchColumns();
        

		ScreenElement GetScreenElement(System.Int32 ScreenElementId,string SelectClause=null);
		ScreenElement UpdateScreenElement(ScreenElement entity);
		bool DeleteScreenElement(System.Int32 ScreenElementId);
		ScreenElement DeleteScreenElement(ScreenElement entity);
		List<ScreenElement> GetPagedScreenElement(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ScreenElement> GetAllScreenElement(string SelectClause=null);
		ScreenElement InsertScreenElement(ScreenElement entity);
		List<ScreenElement> GetScreenElementByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
