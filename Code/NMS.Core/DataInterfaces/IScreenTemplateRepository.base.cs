﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScreenTemplateRepositoryBase
	{
        
        Dictionary<string, string> GetScreenTemplateBasicSearchColumns();
        List<SearchColumn> GetScreenTemplateSearchColumns();
        List<SearchColumn> GetScreenTemplateAdvanceSearchColumns();
        

		List<ScreenTemplate> GetScreenTemplateByProgramId(System.Int32 ProgramId,string SelectClause=null);
		ScreenTemplate GetScreenTemplate(System.Int32 ScreenTemplateId,string SelectClause=null);
		ScreenTemplate UpdateScreenTemplate(ScreenTemplate entity);
		bool DeleteScreenTemplate(System.Int32 ScreenTemplateId);
		ScreenTemplate DeleteScreenTemplate(ScreenTemplate entity);
		List<ScreenTemplate> GetPagedScreenTemplate(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ScreenTemplate> GetAllScreenTemplate(string SelectClause=null);
		ScreenTemplate InsertScreenTemplate(ScreenTemplate entity);
		List<ScreenTemplate> GetScreenTemplateByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
