﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScreenTemplateRepository: IScreenTemplateRepositoryBase
	{
        void UpdateScreenTemplateGroup(string GroupId, int flashtemplateid);
        List<ScreenTemplate> GetScreenTemplate(int ProgramId, string groupId);

        DateTime GetMaxLastUpdateDate();
        ScreenTemplate GetbyScreenTemplateId(int screenTemplateId);
    }
	
	
}
