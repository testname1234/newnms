﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScreenTemplateStatusRepositoryBase
	{
        
        Dictionary<string, string> GetScreenTemplateStatusBasicSearchColumns();
        List<SearchColumn> GetScreenTemplateStatusSearchColumns();
        List<SearchColumn> GetScreenTemplateStatusAdvanceSearchColumns();
        

		ScreenTemplateStatus GetScreenTemplateStatus(System.Int32 ScreenTemplateStatusId,string SelectClause=null);
		ScreenTemplateStatus UpdateScreenTemplateStatus(ScreenTemplateStatus entity);
		bool DeleteScreenTemplateStatus(System.Int32 ScreenTemplateStatusId);
		ScreenTemplateStatus DeleteScreenTemplateStatus(ScreenTemplateStatus entity);
		List<ScreenTemplateStatus> GetPagedScreenTemplateStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ScreenTemplateStatus> GetAllScreenTemplateStatus(string SelectClause=null);
		ScreenTemplateStatus InsertScreenTemplateStatus(ScreenTemplateStatus entity);
		List<ScreenTemplateStatus> GetScreenTemplateStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
