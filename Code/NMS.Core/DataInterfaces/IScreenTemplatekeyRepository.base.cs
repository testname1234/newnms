﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScreenTemplatekeyRepositoryBase
	{
        
        Dictionary<string, string> GetScreenTemplatekeyBasicSearchColumns();
        List<SearchColumn> GetScreenTemplatekeySearchColumns();
        List<SearchColumn> GetScreenTemplatekeyAdvanceSearchColumns();
        

		List<ScreenTemplatekey> GetScreenTemplatekeyByScreenTemplateId(System.Int32? ScreenTemplateId,string SelectClause=null);
		ScreenTemplatekey GetScreenTemplatekey(System.Int32 ScreenTemplatekeyId,string SelectClause=null);
		ScreenTemplatekey UpdateScreenTemplatekey(ScreenTemplatekey entity);
		bool DeleteScreenTemplatekey(System.Int32 ScreenTemplatekeyId);
		ScreenTemplatekey DeleteScreenTemplatekey(ScreenTemplatekey entity);
		List<ScreenTemplatekey> GetPagedScreenTemplatekey(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ScreenTemplatekey> GetAllScreenTemplatekey(string SelectClause=null);
		ScreenTemplatekey InsertScreenTemplatekey(ScreenTemplatekey entity);
		List<ScreenTemplatekey> GetScreenTemplatekeyByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
