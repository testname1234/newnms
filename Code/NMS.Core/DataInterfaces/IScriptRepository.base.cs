﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScriptRepositoryBase
	{
        
        Dictionary<string, string> GetScriptBasicSearchColumns();
        List<SearchColumn> GetScriptSearchColumns();
        List<SearchColumn> GetScriptAdvanceSearchColumns();
        

		Script GetScript(System.Int32 ScriptId,string SelectClause=null);
		Script UpdateScript(Script entity);
		bool DeleteScript(System.Int32 ScriptId);
		Script DeleteScript(Script entity);
		List<Script> GetPagedScript(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Script> GetAllScript(string SelectClause=null);
		Script InsertScript(Script entity);
		List<Script> GetScriptByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
