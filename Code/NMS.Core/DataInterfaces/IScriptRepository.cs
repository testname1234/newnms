﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IScriptRepository: IScriptRepositoryBase
	{

        bool MarkAsExecuted(int id);

        List<Script> GetAllUnExecutedScripts(string token);
    }
	
	
}
