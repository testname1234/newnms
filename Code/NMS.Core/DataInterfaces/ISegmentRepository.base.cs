﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISegmentRepositoryBase
	{
        
        Dictionary<string, string> GetSegmentBasicSearchColumns();
        List<SearchColumn> GetSegmentSearchColumns();
        List<SearchColumn> GetSegmentAdvanceSearchColumns();
        

		List<Segment> GetSegmentBySegmentTypeId(System.Int32 SegmentTypeId,string SelectClause=null);
		List<Segment> GetSegmentByEpisodeId(System.Int32 EpisodeId,string SelectClause=null);
		Segment GetSegment(System.Int32 SegmentId,string SelectClause=null);
		Segment UpdateSegment(Segment entity);
		bool DeleteSegment(System.Int32 SegmentId);
		Segment DeleteSegment(Segment entity);
		List<Segment> GetPagedSegment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Segment> GetAllSegment(string SelectClause=null);
		Segment InsertSegment(Segment entity);
		List<Segment> GetSegmentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
