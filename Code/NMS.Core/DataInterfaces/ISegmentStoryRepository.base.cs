﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISegmentStoryRepositoryBase
	{
        
        Dictionary<string, string> GetSegmentStoryBasicSearchColumns();
        List<SearchColumn> GetSegmentStorySearchColumns();
        List<SearchColumn> GetSegmentStoryAdvanceSearchColumns();
        

		List<SegmentStory> GetSegmentStoryBySegmentId(System.Int32 SegmentId,string SelectClause=null);
		List<SegmentStory> GetSegmentStoryByStoryId(System.Int32 StoryId,string SelectClause=null);
		SegmentStory GetSegmentStory(System.Int32 SegmentStoryId,string SelectClause=null);
		SegmentStory UpdateSegmentStory(SegmentStory entity);
		bool DeleteSegmentStory(System.Int32 SegmentStoryId);
		SegmentStory DeleteSegmentStory(SegmentStory entity);
		List<SegmentStory> GetPagedSegmentStory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SegmentStory> GetAllSegmentStory(string SelectClause=null);
		SegmentStory InsertSegmentStory(SegmentStory entity);
		List<SegmentStory> GetSegmentStoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
