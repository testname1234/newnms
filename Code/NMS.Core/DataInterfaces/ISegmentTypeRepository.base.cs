﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISegmentTypeRepositoryBase
	{
        
        Dictionary<string, string> GetSegmentTypeBasicSearchColumns();
        List<SearchColumn> GetSegmentTypeSearchColumns();
        List<SearchColumn> GetSegmentTypeAdvanceSearchColumns();
        

		SegmentType GetSegmentType(System.Int32 SegmentTypeId,string SelectClause=null);
		SegmentType UpdateSegmentType(SegmentType entity);
		bool DeleteSegmentType(System.Int32 SegmentTypeId);
		SegmentType DeleteSegmentType(SegmentType entity);
		List<SegmentType> GetPagedSegmentType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SegmentType> GetAllSegmentType(string SelectClause=null);
		SegmentType InsertSegmentType(SegmentType entity);
		List<SegmentType> GetSegmentTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
