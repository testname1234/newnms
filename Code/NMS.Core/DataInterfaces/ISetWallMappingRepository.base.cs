﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISetWallMappingRepositoryBase
	{
        
        Dictionary<string, string> GetSetWallMappingBasicSearchColumns();
        List<SearchColumn> GetSetWallMappingSearchColumns();
        List<SearchColumn> GetSetWallMappingAdvanceSearchColumns();
        

		SetWallMapping GetSetWallMapping(System.Int32 SetWallMappingId,string SelectClause=null);
		SetWallMapping UpdateSetWallMapping(SetWallMapping entity);
		bool DeleteSetWallMapping(System.Int32 SetWallMappingId);
		SetWallMapping DeleteSetWallMapping(SetWallMapping entity);
		List<SetWallMapping> GetPagedSetWallMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SetWallMapping> GetAllSetWallMapping(string SelectClause=null);
		SetWallMapping InsertSetWallMapping(SetWallMapping entity);
		List<SetWallMapping> GetSetWallMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
