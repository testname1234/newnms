﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotHeadlineRepositoryBase
	{
        
        Dictionary<string, string> GetSlotHeadlineBasicSearchColumns();
        List<SearchColumn> GetSlotHeadlineSearchColumns();
        List<SearchColumn> GetSlotHeadlineAdvanceSearchColumns();
        

		List<SlotHeadline> GetSlotHeadlineBySlotId(System.Int32 SlotId,string SelectClause=null);
		SlotHeadline GetSlotHeadline(System.Int32 SlotHeadlineId,string SelectClause=null);
		SlotHeadline UpdateSlotHeadline(SlotHeadline entity);
		bool DeleteSlotHeadline(System.Int32 SlotHeadlineId);
		SlotHeadline DeleteSlotHeadline(SlotHeadline entity);
		List<SlotHeadline> GetPagedSlotHeadline(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotHeadline> GetAllSlotHeadline(string SelectClause=null);
		SlotHeadline InsertSlotHeadline(SlotHeadline entity);
		List<SlotHeadline> GetSlotHeadlineByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
