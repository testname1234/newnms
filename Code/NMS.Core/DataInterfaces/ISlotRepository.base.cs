﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotRepositoryBase
	{
        
        Dictionary<string, string> GetSlotBasicSearchColumns();
        List<SearchColumn> GetSlotSearchColumns();
        List<SearchColumn> GetSlotAdvanceSearchColumns();
        

		List<Slot> GetSlotBySlotTypeId(System.Int32 SlotTypeId,string SelectClause=null);
		List<Slot> GetSlotBySegmentId(System.Int32 SegmentId,string SelectClause=null);
		List<Slot> GetSlotByCategoryId(System.Int32? CategoryId,string SelectClause=null);
		Slot GetSlot(System.Int32 SlotId,string SelectClause=null);
		Slot UpdateSlot(Slot entity);
		bool DeleteSlot(System.Int32 SlotId);
		Slot DeleteSlot(Slot entity);
		List<Slot> GetPagedSlot(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Slot> GetAllSlot(string SelectClause=null);
		Slot InsertSlot(Slot entity);
		List<Slot> GetSlotByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
