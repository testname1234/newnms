﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{

    public interface ISlotRepository : ISlotRepositoryBase
    {
        Slot UpdateSlotSequence(Slot entity);
        List<Slot> GetSlotBySegmentAndNews(System.Int32 SegmentId, System.String NewsGuid, int? TickerId);
        List<Slot> GetSlotByEpisode(System.Int32 EpisodeId, string SelectClause = null);
        Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, int TickerId);
        Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, System.String NewsGuid);
        Slot GetSlotByEpisodeAndNewsFileId(System.Int32 EpisodeId, System.Int32 NewsFileId);
        int GetEpisodeId(int slotId);
        List<Slot> GetSlotsByEpisodeId(int EpisodeId);
    }
	
	
}
