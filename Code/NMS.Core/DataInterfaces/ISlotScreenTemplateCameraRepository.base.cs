﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateCameraRepositoryBase
	{
        
        Dictionary<string, string> GetSlotScreenTemplateCameraBasicSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateCameraSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateCameraAdvanceSearchColumns();
        

		List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId,string SelectClause=null);
		List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraByCameraTypeId(System.Int32 CameraTypeId,string SelectClause=null);
		SlotScreenTemplateCamera GetSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId,string SelectClause=null);
		SlotScreenTemplateCamera UpdateSlotScreenTemplateCamera(SlotScreenTemplateCamera entity);
		bool DeleteSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId);
		SlotScreenTemplateCamera DeleteSlotScreenTemplateCamera(SlotScreenTemplateCamera entity);
		List<SlotScreenTemplateCamera> GetPagedSlotScreenTemplateCamera(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotScreenTemplateCamera> GetAllSlotScreenTemplateCamera(string SelectClause=null);
		SlotScreenTemplateCamera InsertSlotScreenTemplateCamera(SlotScreenTemplateCamera entity);
		List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
