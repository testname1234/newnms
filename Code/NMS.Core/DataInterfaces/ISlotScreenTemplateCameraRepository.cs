﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateCameraRepository: ISlotScreenTemplateCameraRepositoryBase
	{
        bool DeleteSlotScreenTemplateCameraBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
	}
	
	
}
