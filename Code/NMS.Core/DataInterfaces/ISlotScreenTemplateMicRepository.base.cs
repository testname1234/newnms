﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateMicRepositoryBase
	{
        
        Dictionary<string, string> GetSlotScreenTemplateMicBasicSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateMicSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateMicAdvanceSearchColumns();
        

		List<SlotScreenTemplateMic> GetSlotScreenTemplateMicBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId,string SelectClause=null);
		SlotScreenTemplateMic GetSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId,string SelectClause=null);
		SlotScreenTemplateMic UpdateSlotScreenTemplateMic(SlotScreenTemplateMic entity);
		bool DeleteSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId);
		SlotScreenTemplateMic DeleteSlotScreenTemplateMic(SlotScreenTemplateMic entity);
		List<SlotScreenTemplateMic> GetPagedSlotScreenTemplateMic(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotScreenTemplateMic> GetAllSlotScreenTemplateMic(string SelectClause=null);
		SlotScreenTemplateMic InsertSlotScreenTemplateMic(SlotScreenTemplateMic entity);
		List<SlotScreenTemplateMic> GetSlotScreenTemplateMicByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
