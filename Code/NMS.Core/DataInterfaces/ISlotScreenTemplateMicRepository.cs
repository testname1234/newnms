﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateMicRepository: ISlotScreenTemplateMicRepositoryBase
	{
        bool DeleteSlotScreenTemplateMicBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
	}
	
	
}
