﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateRepositoryBase
	{
        
        Dictionary<string, string> GetSlotScreenTemplateBasicSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateAdvanceSearchColumns();
        

		List<SlotScreenTemplate> GetSlotScreenTemplateBySlotId(System.Int32 SlotId,string SelectClause=null);
		List<SlotScreenTemplate> GetSlotScreenTemplateByScreenTemplateId(System.Int32 ScreenTemplateId,string SelectClause=null);
		SlotScreenTemplate GetSlotScreenTemplate(System.Int32 SlotScreenTemplateId,string SelectClause=null);
		SlotScreenTemplate UpdateSlotScreenTemplate(SlotScreenTemplate entity);
		bool DeleteSlotScreenTemplate(System.Int32 SlotScreenTemplateId);		
		List<SlotScreenTemplate> GetPagedSlotScreenTemplate(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotScreenTemplate> GetAllSlotScreenTemplate(string SelectClause=null);
		SlotScreenTemplate InsertSlotScreenTemplate(SlotScreenTemplate entity);
		List<SlotScreenTemplate> GetSlotScreenTemplateByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
