﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateRepository: ISlotScreenTemplateRepositoryBase
	{
        List<SlotScreenTemplate> GetSlotScreenTemplateBySlotAndScreenTemplateId(System.Int32 ScreenTemplateId, System.Int32 SlotId, string SelectClause = null);
        SlotScreenTemplate UpdateSlotScreenTemplateSequence(SlotScreenTemplate entity);
        SlotScreenTemplate UpdateSlotScreenTemplateSequenceAndScript(SlotScreenTemplate entity);
        bool DeleteSlotScreenTemplatebySlotId(System.Int32 SlotId);
        SlotScreenTemplate UpdateSlotScreenTemplateThumb(SlotScreenTemplate entity);
        void UpdateSlotScreenScript(int slotScreenTemplateId, string script);
        List<SlotScreenTemplate> GetByEpisodeId(System.Int32 episodeId, string SelectClause = null);
        int GetProgramIdBySlotScreenTemplateId(System.Int32 slotScreenTemplateId);
        List<SlotScreenTemplate> GetSlotScreenTemplateByParentId(int parentId);
        int GetVideoWallId(int SetId, int slotScreenTemplateId);
        bool UpdateSlotScreenTemplateStatus(int slotScreenTemplateId, int? statusId, int? nleStatusId, int? storyWriterStatusId);
	}
	
	
}
