﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateResourceRepositoryBase
	{
        
        Dictionary<string, string> GetSlotScreenTemplateResourceBasicSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateResourceSearchColumns();
        List<SearchColumn> GetSlotScreenTemplateResourceAdvanceSearchColumns();
        

		List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId,string SelectClause=null);
		SlotScreenTemplateResource GetSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId,string SelectClause=null);
		SlotScreenTemplateResource UpdateSlotScreenTemplateResource(SlotScreenTemplateResource entity);
		bool DeleteSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId);
		SlotScreenTemplateResource DeleteSlotScreenTemplateResource(SlotScreenTemplateResource entity);
		List<SlotScreenTemplateResource> GetPagedSlotScreenTemplateResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotScreenTemplateResource> GetAllSlotScreenTemplateResource(string SelectClause=null);
		SlotScreenTemplateResource InsertSlotScreenTemplateResource(SlotScreenTemplateResource entity);
		List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
