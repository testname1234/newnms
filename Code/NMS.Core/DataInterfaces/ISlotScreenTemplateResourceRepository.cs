﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplateResourceRepository: ISlotScreenTemplateResourceRepositoryBase
	{
		void DeleteAllBySlotScreenTemplateId(int slotScreenTemplateId);
        List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceByEpisode(System.Int32 EpisodeId, string SelectClause = null);
        SlotScreenTemplateResource GetSlotScreenTemplateResourceByResourceGuid(System.String Guid,string SelectClause = null);
    
    }
	
	
}
