﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotScreenTemplatekeyRepositoryBase
	{
        
        Dictionary<string, string> GetSlotScreenTemplatekeyBasicSearchColumns();
        List<SearchColumn> GetSlotScreenTemplatekeySearchColumns();
        List<SearchColumn> GetSlotScreenTemplatekeyAdvanceSearchColumns();
        

		List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyByScreenTemplatekeyId(System.Int32? ScreenTemplatekeyId,string SelectClause=null);
		List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyBySlotScreenTemplateId(System.Int32? SlotScreenTemplateId,string SelectClause=null);
		SlotScreenTemplatekey GetSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId,string SelectClause=null);
		SlotScreenTemplatekey UpdateSlotScreenTemplatekey(SlotScreenTemplatekey entity);
		bool DeleteSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId);
		SlotScreenTemplatekey DeleteSlotScreenTemplatekey(SlotScreenTemplatekey entity);
		List<SlotScreenTemplatekey> GetPagedSlotScreenTemplatekey(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotScreenTemplatekey> GetAllSlotScreenTemplatekey(string SelectClause=null);
		SlotScreenTemplatekey InsertSlotScreenTemplatekey(SlotScreenTemplatekey entity);
		List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
