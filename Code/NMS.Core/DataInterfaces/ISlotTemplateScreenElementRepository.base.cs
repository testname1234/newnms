﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotTemplateScreenElementRepositoryBase
	{
        
        Dictionary<string, string> GetSlotTemplateScreenElementBasicSearchColumns();
        List<SearchColumn> GetSlotTemplateScreenElementSearchColumns();
        List<SearchColumn> GetSlotTemplateScreenElementAdvanceSearchColumns();
        

		List<SlotTemplateScreenElement> GetSlotTemplateScreenElementBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId,string SelectClause=null);
		List<SlotTemplateScreenElement> GetSlotTemplateScreenElementByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		SlotTemplateScreenElement GetSlotTemplateScreenElement(System.Int32 SlotTemplateScreenElementId,string SelectClause=null);
		SlotTemplateScreenElement UpdateSlotTemplateScreenElement(SlotTemplateScreenElement entity);
		bool DeleteSlotTemplateScreenElement(System.Int32 SlotTemplateScreenElementId);
		SlotTemplateScreenElement DeleteSlotTemplateScreenElement(SlotTemplateScreenElement entity);
		List<SlotTemplateScreenElement> GetPagedSlotTemplateScreenElement(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotTemplateScreenElement> GetAllSlotTemplateScreenElement(string SelectClause=null);
		SlotTemplateScreenElement InsertSlotTemplateScreenElement(SlotTemplateScreenElement entity);
		List<SlotTemplateScreenElement> GetSlotTemplateScreenElementByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
