﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotTemplateScreenElementRepository: ISlotTemplateScreenElementRepositoryBase
	{
        bool DeleteSlotTemplateScreenElementBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);

        SlotTemplateScreenElement UpdateSlotTemplateScreenElementResourceGuid(int slotTemplateScreenElementId, Guid? resourceGuid);

        bool DeleteSlotTemplateScreenElementBySlotId(System.Int32 SlotId);

        List<SlotTemplateScreenElement> GetBySlotScreenTemplateIdWithFilter(System.Int32 SlotScreenTemplateId, string SelectClause = null);

        SlotTemplateScreenElement UpdateSlotScreenTemplateElementCelebrity(int slotTemplateScreenElementId, int celebrityId);

        List<SlotTemplateScreenElement> GetSlotTemplateScreenElementByEpisodeId(System.Int32 EpisodeId);
	}
	
	
}
