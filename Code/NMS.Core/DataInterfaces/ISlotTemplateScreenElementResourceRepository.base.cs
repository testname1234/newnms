﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotTemplateScreenElementResourceRepositoryBase
	{
        
        Dictionary<string, string> GetSlotTemplateScreenElementResourceBasicSearchColumns();
        List<SearchColumn> GetSlotTemplateScreenElementResourceSearchColumns();
        List<SearchColumn> GetSlotTemplateScreenElementResourceAdvanceSearchColumns();
        

		List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(System.Int32? SlotTemplateScreenElementId,string SelectClause=null);
		List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceBySlotScreenTemplateId(System.Int32? SlotScreenTemplateId,string SelectClause=null);
		SlotTemplateScreenElementResource GetSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId,string SelectClause=null);
		SlotTemplateScreenElementResource UpdateSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity);
		bool DeleteSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId);
		SlotTemplateScreenElementResource DeleteSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity);
		List<SlotTemplateScreenElementResource> GetPagedSlotTemplateScreenElementResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotTemplateScreenElementResource> GetAllSlotTemplateScreenElementResource(string SelectClause=null);
		SlotTemplateScreenElementResource InsertSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity);
		List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
