﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotTemplateScreenElementResourceRepository: ISlotTemplateScreenElementResourceRepositoryBase
	{
        bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
        List<SlotTemplateScreenElementResource> GetBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId, string SelectClause = null);
        bool DeleteBySlotTemplateScreenElementId(System.Int32 SlotTemplateScreenElementId);
	}
	
	
}
