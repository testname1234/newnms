﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlotTypeRepositoryBase
	{
        
        Dictionary<string, string> GetSlotTypeBasicSearchColumns();
        List<SearchColumn> GetSlotTypeSearchColumns();
        List<SearchColumn> GetSlotTypeAdvanceSearchColumns();
        

		SlotType GetSlotType(System.Int32 SlotTypeId,string SelectClause=null);
		SlotType UpdateSlotType(SlotType entity);
		bool DeleteSlotType(System.Int32 SlotTypeId);
		SlotType DeleteSlotType(SlotType entity);
		List<SlotType> GetPagedSlotType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SlotType> GetAllSlotType(string SelectClause=null);
		SlotType InsertSlotType(SlotType entity);
		List<SlotType> GetSlotTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
