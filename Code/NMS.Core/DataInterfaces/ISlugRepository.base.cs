﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlugRepositoryBase
	{
        
        Dictionary<string, string> GetSlugBasicSearchColumns();
        List<SearchColumn> GetSlugSearchColumns();
        List<SearchColumn> GetSlugAdvanceSearchColumns();
        

		Slug GetSlug(System.Int32 SlugId,string SelectClause=null);
		Slug UpdateSlug(Slug entity);
		bool DeleteSlug(System.Int32 SlugId);
		Slug DeleteSlug(Slug entity);
		List<Slug> GetPagedSlug(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Slug> GetAllSlug(string SelectClause=null);
		Slug InsertSlug(Slug entity);
		List<Slug> GetSlugByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
