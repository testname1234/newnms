﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISlugRepository: ISlugRepositoryBase
	{
        Slug GetBySlug(System.String Slug, string SelectClause = null);
	}
	
	
}
