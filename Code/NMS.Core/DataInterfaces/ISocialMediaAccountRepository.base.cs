﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISocialMediaAccountRepositoryBase
	{
        
        Dictionary<string, string> GetSocialMediaAccountBasicSearchColumns();
        List<SearchColumn> GetSocialMediaAccountSearchColumns();
        List<SearchColumn> GetSocialMediaAccountAdvanceSearchColumns();
        

		List<SocialMediaAccount> GetSocialMediaAccountByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		SocialMediaAccount GetSocialMediaAccount(System.Int32 SocialMediaId,string SelectClause=null);
		SocialMediaAccount UpdateSocialMediaAccount(SocialMediaAccount entity);
		bool DeleteSocialMediaAccount(System.Int32 SocialMediaId);
		SocialMediaAccount DeleteSocialMediaAccount(SocialMediaAccount entity);
		List<SocialMediaAccount> GetPagedSocialMediaAccount(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SocialMediaAccount> GetAllSocialMediaAccount(string SelectClause=null);
		SocialMediaAccount InsertSocialMediaAccount(SocialMediaAccount entity);
		List<SocialMediaAccount> GetSocialMediaAccountByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
