﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISocialMediaAccountRepository: ISocialMediaAccountRepositoryBase
	{
        List<SocialMediaAccount> GetBySocialMediaType(System.Int32 SocialMediaType, string SelectClause = null);
        List<SocialMediaAccount> GetByIsFollowed(System.Boolean IsFollowed, string SelectClause = null);
        void UpdateTwitterAccountIsFollowed(int SocialMediaAccountId, Boolean IsFollowed);
	}
	
	
}
