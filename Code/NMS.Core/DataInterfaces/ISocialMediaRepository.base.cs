﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISocialMediaRepositoryBase
	{
        
        Dictionary<string, string> GetSocialMediaBasicSearchColumns();
        List<SearchColumn> GetSocialMediaSearchColumns();
        List<SearchColumn> GetSocialMediaAdvanceSearchColumns();
        

		SocialMedia GetSocialMedia(System.Int32 SocialMediaId,string SelectClause=null);
		SocialMedia UpdateSocialMedia(SocialMedia entity);
		bool DeleteSocialMedia(System.Int32 SocialMediaId);
		SocialMedia DeleteSocialMedia(SocialMedia entity);
		List<SocialMedia> GetPagedSocialMedia(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SocialMedia> GetAllSocialMedia(string SelectClause=null);
		SocialMedia InsertSocialMedia(SocialMedia entity);
		List<SocialMedia> GetSocialMediaByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
