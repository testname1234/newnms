﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISuggestedFlowRepositoryBase
	{
        
        Dictionary<string, string> GetSuggestedFlowBasicSearchColumns();
        List<SearchColumn> GetSuggestedFlowSearchColumns();
        List<SearchColumn> GetSuggestedFlowAdvanceSearchColumns();
        

		SuggestedFlow GetSuggestedFlow(System.Int32 SuggestedFlowId,string SelectClause=null);
		SuggestedFlow UpdateSuggestedFlow(SuggestedFlow entity);
		bool DeleteSuggestedFlow(System.Int32 SuggestedFlowId);
		SuggestedFlow DeleteSuggestedFlow(SuggestedFlow entity);
		List<SuggestedFlow> GetPagedSuggestedFlow(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<SuggestedFlow> GetAllSuggestedFlow(string SelectClause=null);
		SuggestedFlow InsertSuggestedFlow(SuggestedFlow entity);
		List<SuggestedFlow> GetSuggestedFlowByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
