﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ISuggestedFlowRepository: ISuggestedFlowRepositoryBase
	{
        List<SuggestedFlow> GetSuggestedFlowByKey(string Key);
        List<SuggestedFlow> GetSuggestedFlowsLikeKey(string Key, int ProgramId);
	}
	
	
}
