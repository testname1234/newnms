﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITagRepositoryBase
	{
        
        Dictionary<string, string> GetTagBasicSearchColumns();
        List<SearchColumn> GetTagSearchColumns();
        List<SearchColumn> GetTagAdvanceSearchColumns();
        

		Tag GetTag(System.Int32 TagId,string SelectClause=null);
		Tag UpdateTag(Tag entity);
		bool DeleteTag(System.Int32 TagId);
		Tag DeleteTag(Tag entity);
		List<Tag> GetPagedTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Tag> GetAllTag(string SelectClause=null);
		Tag InsertTag(Tag entity);
		List<Tag> GetTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);
        Tag GetTagByGuid(string Guid, string SelectClause = null);
    }

        
}
