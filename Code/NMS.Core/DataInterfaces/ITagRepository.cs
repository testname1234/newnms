﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITagRepository: ITagRepositoryBase
	{
        List<Tag> GetTagByTerm(string term);
        Tag GetByGuid(string Guid, string SelectClause = null);
        bool InsertTagNoReturn(Tag entity);
        bool AlreadyExist(string tag);
        List<Tag> GetTagByTermExact(string term);
	}
	
	
}
