﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITagResourceRepositoryBase
	{
        
        Dictionary<string, string> GetTagResourceBasicSearchColumns();
        List<SearchColumn> GetTagResourceSearchColumns();
        List<SearchColumn> GetTagResourceAdvanceSearchColumns();
        

		List<TagResource> GetTagResourceByTagId(System.Int32? TagId,string SelectClause=null);
		List<TagResource> GetTagResourceByResourceId(System.Int32? ResourceId,string SelectClause=null);
		TagResource GetTagResource(System.Int32 TagResourceid,string SelectClause=null);
		TagResource UpdateTagResource(TagResource entity);
		bool DeleteTagResource(System.Int32 TagResourceid);
		TagResource DeleteTagResource(TagResource entity);
		List<TagResource> GetPagedTagResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TagResource> GetAllTagResource(string SelectClause=null);
		TagResource InsertTagResource(TagResource entity);
		List<TagResource> GetTagResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
