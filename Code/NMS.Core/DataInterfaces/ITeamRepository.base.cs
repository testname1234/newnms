﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITeamRepositoryBase
	{
        
        Dictionary<string, string> GetTeamBasicSearchColumns();
        List<SearchColumn> GetTeamSearchColumns();
        List<SearchColumn> GetTeamAdvanceSearchColumns();
        

		List<Team> GetTeamByProgramId(System.Int32? ProgramId,string SelectClause=null);
		List<Team> GetTeamByTeamRoleId(System.Int32 TeamRoleId,string SelectClause=null);
		Team GetTeam(System.Int32 TeamId,string SelectClause=null);
		Team UpdateTeam(Team entity);
		bool DeleteTeam(System.Int32 TeamId);
		Team DeleteTeam(Team entity);
		List<Team> GetPagedTeam(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Team> GetAllTeam(string SelectClause=null);
		Team InsertTeam(Team entity);
		List<Team> GetTeamByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
