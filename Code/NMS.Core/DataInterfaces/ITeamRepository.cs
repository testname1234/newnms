﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITeamRepository: ITeamRepositoryBase
	{
        bool GetTeamByUserWorkProgramId(int userId, int workRoleId, int? programId);
        List<Team> GetTeamByProgramId(int? programId);
        bool DeleteFromTeam(int userId, int workRoleId, int? programId);
        List<Team> GetTeamByUserId(int userId);
	}
	
	
}
