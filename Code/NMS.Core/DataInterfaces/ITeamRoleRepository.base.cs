﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITeamRoleRepositoryBase
	{
        
        Dictionary<string, string> GetTeamRoleBasicSearchColumns();
        List<SearchColumn> GetTeamRoleSearchColumns();
        List<SearchColumn> GetTeamRoleAdvanceSearchColumns();
        

		TeamRole GetTeamRole(System.Int32 TeamRoleId,string SelectClause=null);
		TeamRole UpdateTeamRole(TeamRole entity);
		bool DeleteTeamRole(System.Int32 TeamRoleId);
		TeamRole DeleteTeamRole(TeamRole entity);
		List<TeamRole> GetPagedTeamRole(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TeamRole> GetAllTeamRole(string SelectClause=null);
		TeamRole InsertTeamRole(TeamRole entity);
		List<TeamRole> GetTeamRoleByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
