﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITemplateScreenElementRepositoryBase
	{
        
        Dictionary<string, string> GetTemplateScreenElementBasicSearchColumns();
        List<SearchColumn> GetTemplateScreenElementSearchColumns();
        List<SearchColumn> GetTemplateScreenElementAdvanceSearchColumns();
        

		List<TemplateScreenElement> GetTemplateScreenElementByScreenElementId(System.Int32 ScreenElementId,string SelectClause=null);
		List<TemplateScreenElement> GetTemplateScreenElementByScreenTemplateId(System.Int32? ScreenTemplateId,string SelectClause=null);
		TemplateScreenElement GetTemplateScreenElement(System.Int32 TemplateScreenElementId,string SelectClause=null);
		TemplateScreenElement UpdateTemplateScreenElement(TemplateScreenElement entity);
		bool DeleteTemplateScreenElement(System.Int32 TemplateScreenElementId);
		TemplateScreenElement DeleteTemplateScreenElement(TemplateScreenElement entity);
		List<TemplateScreenElement> GetPagedTemplateScreenElement(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TemplateScreenElement> GetAllTemplateScreenElement(string SelectClause=null);
		TemplateScreenElement InsertTemplateScreenElement(TemplateScreenElement entity);
		List<TemplateScreenElement> GetTemplateScreenElementByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
