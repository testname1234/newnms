﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerCategoryRepository: ITickerCategoryRepositoryBase
	{
        bool InsertTickerCategoryIfNotExist(int categoryId);

        List<TickerCategory> GetTickerCategory(DateTime lastUpdateDate);
        bool UpdateTickerCategorySequence(TickerCategory category);
        List<TickerCategory> GetTickerCategoryWithSubCategories(int tickerCategoryId);
        List<TickerCategory> GetTickerCategoryWithSubCategories(int[] tickerCategoryIds);
        List<TickerCategory> GetAllCategoriesHavingTickers();

        List<TickerCategory> GetAllTickerCategories();
        bool InsertTickerCategory(Ticker input);
        int GetMaxSequenceId();

    }
	
	
}
