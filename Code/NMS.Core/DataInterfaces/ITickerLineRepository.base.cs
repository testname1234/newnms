﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerLineRepositoryBase
	{
        
        Dictionary<string, string> GetTickerLineBasicSearchColumns();
        List<SearchColumn> GetTickerLineSearchColumns();
        List<SearchColumn> GetTickerLineAdvanceSearchColumns();
        

		List<TickerLine> GetTickerLineByTickerId(System.Int32? TickerId,string SelectClause=null);
		TickerLine GetTickerLine(System.Int32 TickerLineId,string SelectClause=null);
		TickerLine UpdateTickerLine(TickerLine entity);
		bool DeleteTickerLine(System.Int32 TickerLineId);
		TickerLine DeleteTickerLine(TickerLine entity);
		List<TickerLine> GetPagedTickerLine(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerLine> GetAllTickerLine(string SelectClause=null);
		TickerLine InsertTickerLine(TickerLine entity);
		List<TickerLine> GetTickerLineByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
