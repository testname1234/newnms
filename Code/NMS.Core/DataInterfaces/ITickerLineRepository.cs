﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Enums;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerLineRepository: ITickerLineRepositoryBase
	{
        bool UpdateTickerLineStatus(int tickerLineId, TickerStatuses statusId);
        bool UpdateTickerLine(int tickerLineId, string text);
        TickerLine UpdateTickerLineStatus(int tickerLineId, TickerStatuses status, int? tickerTypeId);
        bool DeleteMcrTickerLineData(int tickerLineId, int tickerTypeId);


        bool SubmitTickersToMcrCategory(int tickerLineId, int categoryId, int count,int tickerId);
        bool SubmitTickersToMcrBreaking(int tickerLineId, int count,int tickerId);
        bool SubmitTickersToMcrLatest(int tickerLineId, int count, int tickerId);
        List<TickerLine> GetTickerLineByStatusAndLastUpdateDate(TickerStatuses tickerStatus, DateTime? lastUpdateDate);
        TickerLine UpdateTickerLineSequence(TickerLine item, int TickerTypeId);
        TickerLine UpdateTickerLineNameSeverityFrequency(TickerLine tickerLine);

        bool FlushMcrCategoryTickerData();
        bool FlushMcrBreakingTickerData();
        bool FlushMcrLatestTickerData();

        Dictionary<string, int> GetSequenceNumbers();
        
    }
	
	
}
