﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerOnAirLogRepositoryBase
	{
        
        Dictionary<string, string> GetTickerOnAirLogBasicSearchColumns();
        List<SearchColumn> GetTickerOnAirLogSearchColumns();
        List<SearchColumn> GetTickerOnAirLogAdvanceSearchColumns();
        

		TickerOnAirLog GetTickerOnAirLog(System.Int32 TickerOnAirLogId,string SelectClause=null);
		TickerOnAirLog UpdateTickerOnAirLog(TickerOnAirLog entity);
		bool DeleteTickerOnAirLog(System.Int32 TickerOnAirLogId);
		TickerOnAirLog DeleteTickerOnAirLog(TickerOnAirLog entity);
		List<TickerOnAirLog> GetPagedTickerOnAirLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerOnAirLog> GetAllTickerOnAirLog(string SelectClause=null);
		TickerOnAirLog InsertTickerOnAirLog(TickerOnAirLog entity);
		List<TickerOnAirLog> GetTickerOnAirLogByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
