﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerRepositoryBase
	{
        
        Dictionary<string, string> GetTickerBasicSearchColumns();
        List<SearchColumn> GetTickerSearchColumns();
        List<SearchColumn> GetTickerAdvanceSearchColumns();
        

		Ticker GetTicker(System.Int32 TickerId,string SelectClause=null);
		Ticker UpdateTicker(Ticker entity);
		bool DeleteTicker(System.Int32 TickerId);
		Ticker DeleteTicker(Ticker entity);
		List<Ticker> GetPagedTicker(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Ticker> GetAllTicker(string SelectClause=null);
		Ticker InsertTicker(Ticker entity);
		List<Ticker> GetTickerByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
