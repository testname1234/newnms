﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Enums;

namespace NMS.Core.DataInterfaces
{

    public interface ITickerRepository : ITickerRepositoryBase
    {
        List<Ticker> GetTickers(string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null);

        List<Ticker> GetTickers(int? userId, DateTime? lastUpdateDate);

        List<Ticker> GetTickers(TickerStatuses tickerStatus, DateTime? lastUpdateDate);

        List<Ticker> GetTickersByTickerOnAirLog(DateTime? lastUpdateDate = null);

        bool SubmitTickersToMcrCategory(int tickerId, int categoryId, int count);
        bool SubmitTickersToMcrBreaking(int tickerId, int count);
        bool SubmitTickersToMcrLatest(int tickerId, int count);

        bool FlushMcrCategoryTickerData();
        bool FlushMcrBreakingTickerData();
        bool FlushMcrLatestTickerData();

        Ticker UpdateTickerStatus(Ticker ticker, TickerStatuses status, int? tickerTypeId);



        bool UpdateTicker(int tickerId, int severity, int repeatCount, int frequency);

        bool UpdateTickerLastUpdateDate(DateTime? date, int? tickerId);


        List<Ticker> GetTickerByLastUpdateDate(DateTime dateTime);

        List<Ticker> GetFilteredTickers(List<TickerStatus> discardedStatus);

        //bool DeleteMcrTickerData(int tickerId);

        bool UpdateMcrTickerData(int TickerId, int TickerLineId, int TickerTypeId, bool isFromOnAir);

        Ticker GetTickerLastRecord();
        bool UpdateTickerSequence(Ticker ticker, int type);
        Ticker GetTickerByNewsGuid(string NewsGuid);

        List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds);
        List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds, DateTime lastUpdateDate);
        bool SortTicker(int tickerId, int tickerCategoryId, int sequenceId, SortDirection direction);

        int GetMaxSequenceId();
        bool DeleteMcrTickerData(int tickerId, int tickerTypeId);

        bool UpdateTickerbById(int tickerId, string tickerText, int statusId);
        bool MoveTickerByCategoryId(int tickerId, int tickerCategoryId);
        List<int> GetNotExistingNewsFileIds(List<int> newsFileIds, int tickerCategoryId);
        bool UpdateMCRTickerId(int tickerId, int mcrTickerId);
        bool MoveNewsTickerToTop(int newsFileId, int tickerCategoryId);
        List<Ticker> GetTickersForMCR(int categoryId, int size, bool sortBySequence, bool? isApproved);
        bool MarkTickersDeleteByHour(int hour, int tickerCategoryId);
        bool MarkTickerValid(int tickerId);
        bool MarkTickersUnapproved(int tickerCategoryId);
        bool ApproveTickers(List<int> idsToApprove);
        bool UpdateCategorySequenceIdByCreationDate(int categoryId, DateTime date, int sequenceId);
        bool UpdateTickerSequenceByNewsFileId(int newsFileId, int tickerCategoryId, int sequenceId);
        List<Ticker> GetByIds(List<int> tickerIds);
    }


}
