﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerStatusRepositoryBase
	{
        
        Dictionary<string, string> GetTickerStatusBasicSearchColumns();
        List<SearchColumn> GetTickerStatusSearchColumns();
        List<SearchColumn> GetTickerStatusAdvanceSearchColumns();
        

		TickerStatus GetTickerStatus(System.Int32 TickerStatusId,string SelectClause=null);
		TickerStatus UpdateTickerStatus(TickerStatus entity);
		bool DeleteTickerStatus(System.Int32 TickerStatusId);
		TickerStatus DeleteTickerStatus(TickerStatus entity);
		List<TickerStatus> GetPagedTickerStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerStatus> GetAllTickerStatus(string SelectClause=null);
		TickerStatus InsertTickerStatus(TickerStatus entity);
		List<TickerStatus> GetTickerStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
