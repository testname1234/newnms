﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerTranslationRepositoryBase
	{
        
        Dictionary<string, string> GetTickerTranslationBasicSearchColumns();
        List<SearchColumn> GetTickerTranslationSearchColumns();
        List<SearchColumn> GetTickerTranslationAdvanceSearchColumns();
        

		TickerTranslation GetTickerTranslation(System.Int32 TickerTranslationId,string SelectClause=null);
		TickerTranslation UpdateTickerTranslation(TickerTranslation entity);
		bool DeleteTickerTranslation(System.Int32 TickerTranslationId);
		TickerTranslation DeleteTickerTranslation(TickerTranslation entity);
		List<TickerTranslation> GetPagedTickerTranslation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerTranslation> GetAllTickerTranslation(string SelectClause=null);
		TickerTranslation InsertTickerTranslation(TickerTranslation entity);
		List<TickerTranslation> GetTickerTranslationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
