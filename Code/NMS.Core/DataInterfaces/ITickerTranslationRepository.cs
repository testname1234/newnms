﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Enums;

namespace NMS.Core.DataInterfaces
{

    public interface ITickerTranslationRepository : ITickerTranslationRepositoryBase
    {
        List<TickerTranslation> GetTickerTranslations(List<int> tickerIds, LanguageCode languageCode);
        bool UpdateTranslation(int tickerId, string text, int statusId, LanguageCode languageCode, int userId);
        TickerTranslation GetTranslation(int tickerId, LanguageCode languageCode);
    }


}
