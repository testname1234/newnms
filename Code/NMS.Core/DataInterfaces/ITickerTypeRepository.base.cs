﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITickerTypeRepositoryBase
	{
        
        Dictionary<string, string> GetTickerTypeBasicSearchColumns();
        List<SearchColumn> GetTickerTypeSearchColumns();
        List<SearchColumn> GetTickerTypeAdvanceSearchColumns();
        

		TickerType GetTickerType(System.Int32 TickerTypeId,string SelectClause=null);
		TickerType UpdateTickerType(TickerType entity);
		bool DeleteTickerType(System.Int32 TickerTypeId);
		TickerType DeleteTickerType(TickerType entity);
		List<TickerType> GetPagedTickerType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerType> GetAllTickerType(string SelectClause=null);
		TickerType InsertTickerType(TickerType entity);
		List<TickerType> GetTickerTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
