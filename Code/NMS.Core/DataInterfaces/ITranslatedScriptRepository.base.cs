﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITranslatedScriptRepositoryBase
	{
        
        Dictionary<string, string> GetTranslatedScriptBasicSearchColumns();
        List<SearchColumn> GetTranslatedScriptSearchColumns();
        List<SearchColumn> GetTranslatedScriptAdvanceSearchColumns();
        

		List<TranslatedScript> GetTranslatedScriptByNewsFileId(System.Int32? NewsFileId,string SelectClause=null);
		TranslatedScript GetTranslatedScript(System.Int32 TranslatedScriptId,string SelectClause=null);
		TranslatedScript UpdateTranslatedScript(TranslatedScript entity);
		bool DeleteTranslatedScript(System.Int32 TranslatedScriptId);
		TranslatedScript DeleteTranslatedScript(TranslatedScript entity);
		List<TranslatedScript> GetPagedTranslatedScript(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TranslatedScript> GetAllTranslatedScript(string SelectClause=null);
		TranslatedScript InsertTranslatedScript(TranslatedScript entity);
		List<TranslatedScript> GetTranslatedScriptByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
