﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITwitterAccountRepositoryBase
	{
        
        Dictionary<string, string> GetTwitterAccountBasicSearchColumns();
        List<SearchColumn> GetTwitterAccountSearchColumns();
        List<SearchColumn> GetTwitterAccountAdvanceSearchColumns();
        

		List<TwitterAccount> GetTwitterAccountByCelebrityId(System.Int32? CelebrityId,string SelectClause=null);
		TwitterAccount GetTwitterAccount(System.Int32 TwitterAccountId,string SelectClause=null);
		TwitterAccount UpdateTwitterAccount(TwitterAccount entity);
		bool DeleteTwitterAccount(System.Int32 TwitterAccountId);
		TwitterAccount DeleteTwitterAccount(TwitterAccount entity);
		List<TwitterAccount> GetPagedTwitterAccount(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TwitterAccount> GetAllTwitterAccount(string SelectClause=null);
		TwitterAccount InsertTwitterAccount(TwitterAccount entity);
		List<TwitterAccount> GetTwitterAccountByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
