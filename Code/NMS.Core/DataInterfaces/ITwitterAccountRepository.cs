﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface ITwitterAccountRepository: ITwitterAccountRepositoryBase
	{
        List<TwitterAccount> GetByIsFollowed(bool isFollowed, string SelectClause = null);
        void UpdateTwitterAccount(int TwitterAccoutnId, bool Isfollowed);
	}
	
	
}
