﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IUserRepositoryBase
	{
        
        Dictionary<string, string> GetUserBasicSearchColumns();
        List<SearchColumn> GetUserSearchColumns();
        List<SearchColumn> GetUserAdvanceSearchColumns();
        

		User GetUser(System.Int32 UserId,string SelectClause=null);
		User UpdateUser(User entity);
		bool DeleteUser(System.Int32 UserId);
		User DeleteUser(User entity);
		List<User> GetPagedUser(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<User> GetAllUser(string SelectClause=null);
		User InsertUser(User entity);
		List<User> GetUserByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
