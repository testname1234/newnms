﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Enums;

namespace NMS.Core.DataInterfaces
{
		
	public interface IUserRepository: IUserRepositoryBase
	{
        User Login(string login, string password);
        List<User> GetUsers(TeamRoles teamRole);
	}
	
	
}
