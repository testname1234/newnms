﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IUserRoleRepositoryBase
	{
        
        Dictionary<string, string> GetUserRoleBasicSearchColumns();
        List<SearchColumn> GetUserRoleSearchColumns();
        List<SearchColumn> GetUserRoleAdvanceSearchColumns();
        

		List<UserRole> GetUserRoleByUserId(System.Int32 UserId,string SelectClause=null);
		List<UserRole> GetUserRoleByRoleId(System.Int32 RoleId,string SelectClause=null);
		UserRole GetUserRole(System.Int32 UserRoleId,string SelectClause=null);
		UserRole UpdateUserRole(UserRole entity);
		bool DeleteUserRole(System.Int32 UserRoleId);
		UserRole DeleteUserRole(UserRole entity);
		List<UserRole> GetPagedUserRole(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<UserRole> GetAllUserRole(string SelectClause=null);
		UserRole InsertUserRole(UserRole entity);
		List<UserRole> GetUserRoleByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
