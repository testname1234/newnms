﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IVideoCutterIpRepositoryBase
	{
        
        Dictionary<string, string> GetVideoCutterIpBasicSearchColumns();
        List<SearchColumn> GetVideoCutterIpSearchColumns();
        List<SearchColumn> GetVideoCutterIpAdvanceSearchColumns();
        

		VideoCutterIp GetVideoCutterIp(System.Int32 VideoCutterIpId,string SelectClause=null);
		VideoCutterIp UpdateVideoCutterIp(VideoCutterIp entity);
		bool DeleteVideoCutterIp(System.Int32 VideoCutterIpId);
		VideoCutterIp DeleteVideoCutterIp(VideoCutterIp entity);
		List<VideoCutterIp> GetPagedVideoCutterIp(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<VideoCutterIp> GetAllVideoCutterIp(string SelectClause=null);
		VideoCutterIp InsertVideoCutterIp(VideoCutterIp entity);
		List<VideoCutterIp> GetVideoCutterIpByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
