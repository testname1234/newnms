﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.DataInterfaces
{
    public interface IVideoRepository
    {
        byte[] GetVideo(string videoId);
    }
}
