﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IVoiceArtistRepositoryBase
	{
        
        Dictionary<string, string> GetVoiceArtistBasicSearchColumns();
        List<SearchColumn> GetVoiceArtistSearchColumns();
        List<SearchColumn> GetVoiceArtistAdvanceSearchColumns();
        

		VoiceArtist GetVoiceArtist(System.Int32 VoiceArtistId,string SelectClause=null);
		VoiceArtist UpdateVoiceArtist(VoiceArtist entity);
		bool DeleteVoiceArtist(System.Int32 VoiceArtistId);
		VoiceArtist DeleteVoiceArtist(VoiceArtist entity);
		List<VoiceArtist> GetPagedVoiceArtist(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<VoiceArtist> GetAllVoiceArtist(string SelectClause=null);
		VoiceArtist InsertVoiceArtist(VoiceArtist entity);
		List<VoiceArtist> GetVoiceArtistByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
