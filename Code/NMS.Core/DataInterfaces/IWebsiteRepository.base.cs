﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IWebsiteRepositoryBase
	{
        
        Dictionary<string, string> GetWebsiteBasicSearchColumns();
        List<SearchColumn> GetWebsiteSearchColumns();
        List<SearchColumn> GetWebsiteAdvanceSearchColumns();
        

		Website GetWebsite(System.Int32 WebsiteId,string SelectClause=null);
		Website UpdateWebsite(Website entity);
		bool DeleteWebsite(System.Int32 WebsiteId);
		Website DeleteWebsite(Website entity);
		List<Website> GetPagedWebsite(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Website> GetAllWebsite(string SelectClause=null);
		Website InsertWebsite(Website entity);
		List<Website> GetWebsiteByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
