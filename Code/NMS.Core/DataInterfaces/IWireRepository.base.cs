﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IWireRepositoryBase
	{
        
        Dictionary<string, string> GetWireBasicSearchColumns();
        List<SearchColumn> GetWireSearchColumns();
        List<SearchColumn> GetWireAdvanceSearchColumns();
        

		Wire GetWire(System.Int32 WireId,string SelectClause=null);
		Wire UpdateWire(Wire entity);
		bool DeleteWire(System.Int32 WireId);
		Wire DeleteWire(Wire entity);
		List<Wire> GetPagedWire(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Wire> GetAllWire(string SelectClause=null);
		Wire InsertWire(Wire entity);
		List<Wire> GetWireByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
