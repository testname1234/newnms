﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces
{
		
	public interface IWorkRoleFolderRepositoryBase
	{
        
        Dictionary<string, string> GetWorkRoleFolderBasicSearchColumns();
        List<SearchColumn> GetWorkRoleFolderSearchColumns();
        List<SearchColumn> GetWorkRoleFolderAdvanceSearchColumns();
        

		List<WorkRoleFolder> GetWorkRoleFolderByFolderId(System.Int32 FolderId,string SelectClause=null);
		WorkRoleFolder GetWorkRoleFolder(System.Int32 WorkRoleFolderId,string SelectClause=null);
		WorkRoleFolder UpdateWorkRoleFolder(WorkRoleFolder entity);
		bool DeleteWorkRoleFolder(System.Int32 WorkRoleFolderId);
		WorkRoleFolder DeleteWorkRoleFolder(WorkRoleFolder entity);
		List<WorkRoleFolder> GetPagedWorkRoleFolder(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<WorkRoleFolder> GetAllWorkRoleFolder(string SelectClause=null);
		WorkRoleFolder InsertWorkRoleFolder(WorkRoleFolder entity);
		List<WorkRoleFolder> GetWorkRoleFolderByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
