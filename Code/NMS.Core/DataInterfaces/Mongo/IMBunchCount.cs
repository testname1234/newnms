﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMBunchCount
    {
        void PollBunchCount();
        DateTime GetLastUpdatedFetchDate();     //earlier used from cache
        DateTime GetBunchLastUpdatedDate();         //will be using sql db 
        bool UpdateCacheForBunchNews(List<MBunchCount> lstBunchIDCount, DateTime LastUpdatedDate);

        bool UpdateBunchNewsCountInDB(List<MBunchCount> lstBunchIDCount, DateTime LastUpdatedDate);
    }
}
