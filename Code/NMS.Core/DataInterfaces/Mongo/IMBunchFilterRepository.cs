﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMBunchFilterRepository
    {
        bool InsertBunchFilter(MBunchFilter bunchFilter);

        List<MBunchFilter> GetPagedBunchFilter(int pageCount, int startIndex);

        bool CheckIfBunchFilterExists(MBunchFilter bunchFilter);

        List<MBunchFilter> GetNewsByFilterIds(List<int> filterIds, int pageCount, int startIndex, DateTime from, DateTime to, string term);

        List<MBunchFilter> GetNewsByFilterIds(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discardedfilterIds = null);

        List<MBunchFilter> GetByFilterIdAndBunchId(List<int> filterIds, string bunchId);

        bool DeleteByBunchId(string bunchId);

        void RemoveNewsFromBunchFilter(int filterId, string bunchId, string newsId);

        bool DeleteByBunchAndFilterId(MBunchFilter mBunchFilter);

        List<MBunchFilter> GetByBunchId(string bunchId);

        List<MBunchFilter> GetByUpdatedDate(DateTime updatedDate);

        List<MBunchFilter> GetByBunchIDs(List<string> BunchIDs);

        List<MBunchFilter> GetByUpdatedDateTop1000(DateTime updatedDate);

        bool UpdateBunchFilterNewsList(MBunchFilter bunchFilter);

        bool UpdateBunchGuidByOldBunchGuid(string OldBunchGuid, string BunchGuid);

        bool InsertBunchFilterNoReturn(MBunchFilter bunchFilter);

        List<MBunchFilter> GetAllBunches();

        bool BulkInsert(List<MBunchFilter> mBunchFilters);

        
    }
}
