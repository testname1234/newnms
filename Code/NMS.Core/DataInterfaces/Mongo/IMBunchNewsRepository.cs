﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMBunchNewsRepository
    {
        bool InsertBunchNews(MBunchNews bunchNews);

        List<MBunchNews> GetPagedBunchNews(int pageCount, int startIndex);

        bool CheckIfBunchNewsExists(MBunchNews bunchNews);

        bool DeleteByBunchId(string bunchId);

        List<MBunchNews> GetBunchNewsByBunchId(string bunchId);

        List<MBunchCount> GetBunchNewsGroupByBunchId(DateTime LastUpdatedDate);

        bool DeleteByBunchIdAndNewsId(string newsId, string bunchId);

        List<MBunchNews> GetByBunchIdAndNewsId(string newsId, string bunchId);

        bool UpdateBunchGuidByOldBunchGuid(string OldBunchGuid, string BunchGuid);

        bool InsertBunchNewsNoReturn(MBunchNews bunchNews);
    }
}
