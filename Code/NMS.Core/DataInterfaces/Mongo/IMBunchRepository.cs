﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMBunchRepository
    {
        MBunch InsertBunch(MBunch bunch);
        bool DeleteByBunchId(string bunchId);
        void UpdateBunchNewsCount(string bunchId, int NewsCount);
        MBunch GetByGuid(string Guid);
        List<MBunch> GetByIDs(List<string> bunchIds);
        List<MBunch> GetByUpdatedDate(DateTime Updateddate);
        MBunch InsertBunchNoCheck(MBunch bunch);
        List<MBunch> GetAllBunches();
        bool BulkInsert(List<MBunch> bunch);
    }
}
