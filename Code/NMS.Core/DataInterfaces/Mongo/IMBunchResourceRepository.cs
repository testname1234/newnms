﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMBunchResourceRepository
    {
        bool InsertBunchResource(MBunchResource bunchResource);

        List<MBunchResource> GetPagedBunchResource(int pageCount, int startIndex);

        bool CheckIfBunchResourceExists(MBunchResource bunchResource);

        bool DeleteByBunchId(string bunchId);
    }
}
