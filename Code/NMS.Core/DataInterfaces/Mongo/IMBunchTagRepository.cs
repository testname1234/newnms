﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMBunchTagRepository
    {
        bool InsertBunchTag(MBunchTag bunchTag);

        List<MBunchTag> GetPagedBunchTag(int pageCount, int startIndex);

        bool CheckIfBunchTagExists(MBunchTag bunchTag);

        bool DeleteByBunchId(string bunchId);

        bool DeleteByTagId(string tagId);

        List<MBunchTag> GetByBunchId(string BunchId);

        bool InsertBunchTagNoReturn(MBunchTag bunchTag);
    }
}
