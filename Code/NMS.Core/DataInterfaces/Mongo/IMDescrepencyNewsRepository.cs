﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMDescrepencyNewsRepository
    {
        void Insert(MDescrepencyNews descrepencyNews);

        List<MDescrepencyNews> GetPagedDescrepencyNews(int pageCount, int startIndex);

        List<MDescrepencyNews> GetAllByDescrepancyType(int descrepencyType);

        List<MDescrepencyNews> GetAllByDescrepancyType(int descrepencyType, string descrepancyValue);

        bool DeleteDescrepancyNews(string DescrepancyNewsID);

        List<MDescrepencyNews> GetDiscrepencyBystatus(int descrepencyType, int Status);
        void UpdateDescrepancyNewsByValue(int descrepencyType, string DescrepencyValue);
        List<MDescrepencyNews> GetAllDescrepancyByStatus(int DescrepencyType);
        List<MDescrepencyNews> GetDescrepancyByTitleAndSource(string Title, string Source);

    }
}
