﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMFilterCountRepository
    {
        bool InsertOrUpdate(MFilterCount filterCount);

        bool Update(MFilterCount filterCount);

        bool CheckIfFilterCountExists(MFilterCount filterCount);

        DateTime GetMaxLastUpdateDate();

        MFilterCount GetById(string Id);

        List<MFilterCount> GetFilterCountByLastUpdateDate(DateTime lastUpdateDate);

        List<MFilterCount> GetFilterCountByLastUpdateDate(List<Filter> filterIds, DateTime from, DateTime to, List<int> programFilterIds);
        

        DateTime GetMaxLastUpdateDate(int filterId);

        List<int> GetDistinctFilterIds();

        bool InsertOrUpdate(List<MFilterCount> lstFilterCount);

        void ProcessProducerDataPolling();
    }
}
