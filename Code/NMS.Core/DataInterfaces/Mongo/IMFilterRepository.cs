﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMFilterRepository
    {
        bool InsertFilter(MFilter filter);

        bool CheckIfFilterExists(string name,int filterTypeId);

        MFilter GetFilterCreateIfNotExist(MFilter filter);

        MFilter GetFilterByFilterId(int filterId);
    }
}
