﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMLocationRepository
    {
        bool InsertLocation(MLocation location);
        MLocation GetLocationCreateIfNotExist(MLocation location);
    }
}
