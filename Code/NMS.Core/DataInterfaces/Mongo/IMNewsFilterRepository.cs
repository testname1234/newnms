﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMNewsFilterRepository
    {
        bool InsertNewsFilter(MNewsFilter newsFilter);

        MNewsFilter GetNewsFilterCreateIfNotExist(MNewsFilter newsFilter);

        List<MNewsFilter> GetUpdatedNewsFilters(DateTime lastUpdateDate, int? reporterId = null, List<string> newsIds = null, int? filterId = null);

        List<MFilterCount> GroupByFilterId(DateTime from, DateTime to, List<int> catFilterIds, List<int> newsFilterIds);

        List<MFilterCount> GroupByFilterId(DateTime from, DateTime to, List<Filter> newsFilterIds, List<int> programFilterIds);

        bool CheckIfNewsFilterExists(MNewsFilter newsfilter);

        bool DeleteNewsFilterByNewsAndFilterId(MNewsFilter mnewsFilter);

        bool BulkInsert(List<MNewsFilter> mNewsFilters);

        
    }
}
