﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMNewsRepository
    {
        bool InsertNews(MNews news);

        bool CheckIfNewsExists(MNews news);

        MNews GetNewsByID(string id);

        void UpdateNews(MNews news);

        void UpdateNewsStatistics(MNews news);

        List<MNews> GetReportedNews(int reportedId, int pageCount, int startIndex);

        List<MNews> GetNewsByIDs(List<string> newsIds, bool getFullNews = false);

        List<MNews> GetNewsByBunchId(string bunchId);

        List<MNews> GetNewsByBunchId(string bunchId,List<Filter> filters);

        void UpdateThumb(string id, string resGuid);

        List<MNewsFilter> MarkVerifyNews(string[] id, MNewsFilter filter, bool isVerified);

        void UpdateNewsBunch(string Newsid, string BunchGuid);

        bool UpdateComment(MComment comment);

        List<MNews> GetNewsWithUpdatesAndRelatedById(string newsId, string bunchId);

        List<MNews> GetNewsForSimilar(List<int> CategoryIDs, DateTime date);

        List<MNews> GetNewsByTitle(string term);

        void UpdateResourcesAndFilters(MNews news);

        bool DeleteNews(MNews news);

        List<MNews> GetNewsByFilterIds(List<Filter> filters, DateTime lastUpdatedDate, int PageSize);

        void UpdateFiltersWithDate(MNews news);

        List<MNews> GetByUpdatedDate(DateTime Updateddate);

        List<MNews> GetByIDs(List<string> NewsIds);

        List<MNews> GetByBunchIDs(List<string> BunchIds);

        void UpdateNewsResources(MNews news);

        List<MNews> GetNewsWithoutResources();

        void UpdateNewsThumb(MNews news);

        List<MNews> GetFullNewsbyDate(DateTime date);

        List<MNews> GetByUpdatedDate2000(DateTime Updateddate, int StartIndex = 0, int RecordsCount = 100);

        List<MNews> GetAllBunches();

        void UpdateNewsTickerCount(MNews news);

        bool BulkInsert(List<MNews> mNews);        

        List<MNews> GetAllBunchesByDateAndFilters(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, List<int> discaredFilters);
        List<MNews> GetByBunchIDsWithFilters(List<string> Bunches, List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, List<int> discaredFilters);
        List<MNews> GetByBunchGuidByBunchIDs(List<string> BunchIds);
        void UpdateNewsBunchByNewsIds(List<string> newsids, string bunchGuid);
        List<MFilterCount> GetNewsFilterCounts(List<Filter> filters, DateTime from, DateTime to);

        List<int[]> GetFilterGroups(List<Filter> filters);
        bool DeleteByNewsId(List<string> newsIds);
    }
}
