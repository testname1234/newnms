﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMResourceRepository
    {
        bool InsertResource(MResource resource);

        bool CheckIfResourceExists(string name);

        MResource GetResourceCreateIfNotExist(MResource resource);

        MResource GetById(string id);

        void UpdateResourceDuration(MResource resource);

        List<MResource> GetResourcesByDate(DateTime resourcedate);

        MResource GetByGuid(string Guid);
        bool DeleteByResourceGuid(string resourceGuid);
        
    }
}
