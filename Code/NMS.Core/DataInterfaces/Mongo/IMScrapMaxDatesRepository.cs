﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMScrapMaxDatesRepository
    {
        MScrapMaxDates GetBySourceName(string sourceName);
        MScrapMaxDates GetScrapDateCreateIfNotExist(MScrapMaxDates scrapMaxDates);
        bool CheckIfscrapMaxDateExists(MScrapMaxDates scrapMaxDates);
        void UpdateScrapDate(MScrapMaxDates scrapMaxDates);

    }
}
