﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMTagRepository
    {
        bool InsertTag(MTag tag);


        bool CheckIfTagExists(string name);

        MTag GetTagCreateIfNotExist(MTag tag);

        List<MTag> GetTagByTerm(string term);

        bool DeleteByTagGuid(string tagGuid);

        MTag GetTagCreateIfNotExistOptimized(MTag tag);
        
    }
}
