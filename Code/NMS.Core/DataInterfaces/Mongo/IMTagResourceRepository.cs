﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataInterfaces.Mongo
{
    public interface IMTagResourceRepository
    {
        void CreateIfNotExist(MTagResource tagresource);
        bool InsertTagResource(MTagResource entity);
        bool CheckIfTagResourceExists(string tagId);
        bool DeleteByTagGuid(string tagId);
    }
}
