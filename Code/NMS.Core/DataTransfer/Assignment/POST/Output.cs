﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Assignment
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 AssignmentId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LocationId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CategoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? AssignmentType{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Picture{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Video{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Audio{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Document{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Barcode{ get; set; }

	}	
}
