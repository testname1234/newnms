﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.CasperFlashTemplateWindow
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CasperFlashTemplateWindowId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? FlashTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Decimal? TopLeftx{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Decimal? BottomRighty{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Decimal? Width{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Decimal? Height{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? PortId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? FlashWindowTypeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

	}	
}
