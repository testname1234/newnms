﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.CasperFlashTemplateWindow
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string CasperFlashTemplateWindowId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string FlashTemplateId{ get; set; }

		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string Name{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Decimal)]
		[DataMember (EmitDefaultValue=false)]
		public string TopLeftx{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Decimal)]
		[DataMember (EmitDefaultValue=false)]
		public string BottomRighty{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Decimal)]
		[DataMember (EmitDefaultValue=false)]
		public string Width{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Decimal)]
		[DataMember (EmitDefaultValue=false)]
		public string Height{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string PortId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string FlashWindowTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

	}	
}
