﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.CasperItemTransformation
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CasperItemTransformationId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CasperTemplateItemId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Type{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Label{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Channel{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Videolayer{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Devicename{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Allowgpi{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Allowremotetriggering{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Remotetriggerid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Tween{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Triggeronnext{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Positionx{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Positiony{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Scalex{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Scaley{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Defer{ get; set; }

	}	
}
