﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.CasperTemplate
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CasperTemlateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Template{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ItemCount{ get; set; }

	}	
}
