﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.CasperTemplateItem
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CasperTemplatItemId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CasperTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ItemName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Type{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Label{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Channel{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Videolayer{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Devicename{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Delay{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Duration{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Allowgpi{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Allowremotetriggering{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Remotetriggerid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Flashlayer{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Invoke{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Usestoreddata{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Useuppercasedata{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Color{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Transition{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Transitionduration{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Tween{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Direction{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Seek{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Length{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Loop{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Freezeonload{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Triggeronnext{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Autoplay{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Timecode{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Defer{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Device{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Format{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Showmask{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Blur{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Key{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Spread{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Spill{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Threshold{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? UserType{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CasperTransformationId{ get; set; }

	}	
}
