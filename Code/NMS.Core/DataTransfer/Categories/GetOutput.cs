﻿using System;

namespace NMS.Core.DataTransfer.Categories
{
    public class GetOutput
    {
        public Guid _id { get; set; }

        public string Category { get; set; }
 }
}
