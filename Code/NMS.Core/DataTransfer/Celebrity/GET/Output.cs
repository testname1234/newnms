﻿using System;
using System.Runtime.Serialization;
using Validation;
using System.Collections.Generic;

namespace NMS.Core.DataTransfer.Celebrity
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CelebrityId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid? ImageGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Designation{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LocationId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CategoryId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? WordFrequency{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? AddedToRundown { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? OnAired { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ExecutedOnOtherChannels { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<CelebrityStatistic.GetOutput> CelebrityStatistics { get; set; }
	}	
}
