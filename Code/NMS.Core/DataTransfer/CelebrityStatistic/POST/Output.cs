﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.CelebrityStatistic
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CelebrityStatisticId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ProgramId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ProgramName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ChannelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ChannelName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? EpisodeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? EpisodeTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string EpisodeTimeStr
		{
			 get {if(EpisodeTime.HasValue) return EpisodeTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EpisodeTime = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? StatisticType{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CelebrityId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

	}	
}
