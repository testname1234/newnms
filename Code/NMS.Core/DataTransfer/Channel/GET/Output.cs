﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Channel
{
    [DataContract]
	public class GetOutput
	{
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ChannelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }


        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsOtherChannel { get; set; }
    }	
}
