﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ChannelProgram
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ChannelProgramId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ChannelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ProgramId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsActive{ get; set; }

	}	
}
