﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ChannelVideo
{
    [DataContract]
    public class GetChannelVideoInput
	{
        [IgnoreDataMember]
        public virtual System.DateTime From { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual string FromStr
        {
            get { return From.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public virtual System.DateTime To { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual string ToStr
        {
            get { return To.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime(); } }
        }
	}	
}
