﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ChannelVideo
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ChannelVideoId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ChannelId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? From{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string FromStr
		{
			 get {if(From.HasValue) return From.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? To{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string ToStr
		{
			 get {if(To.HasValue) return To.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ProgramId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String PhysicalPath{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Url{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsProcessed{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsActive{ get; set; }

	}	
}
