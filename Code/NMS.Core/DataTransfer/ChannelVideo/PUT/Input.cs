﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ChannelVideo
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string ChannelVideoId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string ChannelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string From{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string To{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string ProgramId{ get; set; }

		[FieldLength(MaxLength = 1000)]
		[DataMember (EmitDefaultValue=false)]
		public string PhysicalPath{ get; set; }

		[FieldLength(MaxLength = 1000)]
		[DataMember (EmitDefaultValue=false)]
		public string Url{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[DataMember (EmitDefaultValue=false)]
		public string IsProcessed{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[DataMember (EmitDefaultValue=false)]
		public string IsActive{ get; set; }

	}	
}
