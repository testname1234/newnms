﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ChatMessage
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MessageId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 From{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 To{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Message{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsRecieved{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsSentToGroup{ get; set; }

	}	
}
