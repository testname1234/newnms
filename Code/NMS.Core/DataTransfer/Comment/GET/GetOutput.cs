﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NMS.Core.DataTransfer.Comment
{
    [DataContract]
    public class GetOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public string _id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Comment { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CommentTypeId { get; set; }

        [IgnoreDataMember]
        public System.DateTime CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }
    }
}
