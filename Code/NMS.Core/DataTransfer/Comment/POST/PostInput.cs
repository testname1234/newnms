﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NMS.Core.DataTransfer.Comment
{
    [DataContract]
    public class PostInput
    {
        [DataMember(EmitDefaultValue = false)]
        public string _id { get; set; }

        [DataMember(EmitDefaultValue=false)]
        public string NewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Comment { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CommentTypeId { get; set; }
    }
}
