﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.DescrepencyNewsFile
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DescrepencyNewsFileId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Title{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Source{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String DescrepencyValue{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? DescrepencyType{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? DiscrepencyStatusCode{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsInserted{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String RawNews{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? NewsFileId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CategoryId{ get; set; }

	}	
}
