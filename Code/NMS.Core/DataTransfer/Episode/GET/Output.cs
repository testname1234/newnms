﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Episode
{
    [DataContract]
    public class GetOutput
    {

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 EpisodeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 ProgramId { get; set; }

        [IgnoreDataMember]
        public System.DateTime From { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string FromStr
        {
            get { return From.ToString("yyyy-MM-dd HH:mm:ss.fff"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime To { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string ToStr
        {
            get { return To.ToString("yyyy-MM-dd HH:mm:ss.fff"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsActive { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.Segment.GetOutput> Segments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PreviewGuid { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalSlot { get; set; }

        [DataMember(EmitDefaultValue = true)]
        public int Done { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int[] ProgramIds { get; set; }
        
    }	
}
