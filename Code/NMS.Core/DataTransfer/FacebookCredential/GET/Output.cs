﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.FacebookCredential
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FacebookCredentialId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ClientId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ClientSecret{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String AccessToken{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

	}	
}
