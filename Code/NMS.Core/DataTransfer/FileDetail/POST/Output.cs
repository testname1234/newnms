﻿using System;
using System.Runtime.Serialization;
using Validation;
using System.Collections.Generic;

namespace NMS.Core.DataTransfer.FileDetail
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FileDetailId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsFileId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Text{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CreatedBy{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 FolderId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 LocationId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LanguageCode { get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ReportedBy{ get; set; }

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String CgValues{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 StatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.Entities.FileResource> FileResource { get; set; }


	}	
}
