﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Filter
{
    [DataContract]
	public class GetOutput
	{
        [DataMember(EmitDefaultValue = false)]
        public System.Int32 FilterId { get; set; }
			
		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Value{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FilterTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ParentId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String CssClass { get; set; }

        [DataMember (EmitDefaultValue=false)]
		public List<GetOutput> Children{ get; set; }

        [FieldNameAttribute("CreationDate", false, false, 8)]
        [IgnoreDataMember]
        public virtual System.DateTime CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public virtual string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [FieldNameAttribute("LastUpdateDate", false, false, 8)]
        [IgnoreDataMember]
        public virtual System.DateTime LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public virtual string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public bool IsApproved { get; set; }

    }
}
