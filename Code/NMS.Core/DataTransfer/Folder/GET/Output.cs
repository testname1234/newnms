﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Folder
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FolderId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CategoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LocationId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ParentFolderId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsRundown{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ProgramId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsPrivateFolder{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? EpisodeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string StartTimeStr
		{
			 get {if(StartTime.HasValue) return StartTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? EndTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string EndTimeStr
		{
			 get {if(EndTime.HasValue) return EndTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EndTime = date.ToUniversalTime();  }  } 
		}

        [DataMember(EmitDefaultValue = false)]
        public string Location { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Category { get; set; }

	}	
}
