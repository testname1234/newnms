﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Group
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 GroupId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String GroupName{ get; set; }

	}	
}
