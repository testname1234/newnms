﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.GroupUser
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32? GroupId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? UserId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 GroupUserId{ get; set; }

	}	
}
