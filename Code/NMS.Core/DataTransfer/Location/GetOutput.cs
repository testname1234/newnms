﻿using System;


namespace NMS.Core.DataTransfer.Location
{
    public class GetOutput
    {
        public int LocationId { get; set; }

        public string Location { get; set; }

        public virtual System.Int32? ParentId { get; set; }

        public virtual System.DateTime CreationDate { get; set; }


        public virtual string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        public virtual System.DateTime LastUpdateDate { get; set; }

        public virtual string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        public virtual System.Boolean IsActive { get; set; }

        public virtual System.Boolean? IsApproved { get; set; }

    }
}
