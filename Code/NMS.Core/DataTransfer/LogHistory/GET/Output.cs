﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.LogHistory
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 LogHistoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TableId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RefrenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.DateTime CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Method{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 UserId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String UserIp{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String UserType{ get; set; }

	}	
}
