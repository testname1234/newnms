﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NMS.Core.DataTransfer
{
    [DataContract]
    public class MarkVerifyNewsInput
    {
        [DataMember]
        public List<string> NewsIds { get; set; }

        [DataMember]
        public string bunchId { get; set; }

        [DataMember]
        public bool IsVerified { get; set; }
    }
}
