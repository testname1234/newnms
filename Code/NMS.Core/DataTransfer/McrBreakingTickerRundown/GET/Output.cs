﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.McrBreakingTickerRundown
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 McrBreakingTickerRundownId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Text{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SequenceNumber{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LanguageCode{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerLineId{ get; set; }

	}	
}
