﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.McrCategoryTickerRundown
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 McrCategoryTickerRundownId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Text{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String CategoryName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CategoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SequenceNumber{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LanguageCode{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerLineId{ get; set; }

	}	
}
