﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.McrLatestTickerRundown
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 McrLatestTickerRundownId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Text{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SequenceNumber{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LanguageCode{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerLineId{ get; set; }

	}	
}
