﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.McrTickerBroadcasted
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 McrTickerBroadcastedId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 Hour{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerCategoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 Limit{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Broadcasted{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

	}	
}
