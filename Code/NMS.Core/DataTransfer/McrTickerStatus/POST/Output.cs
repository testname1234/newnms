﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.McrTickerStatus
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 McrTickerStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Status{ get; set; }

	}	
}
