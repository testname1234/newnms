﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Message
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MessageId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Message{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 To{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 From{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsRecieved{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 SlotScreenTemplateId { get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

	}	
}
