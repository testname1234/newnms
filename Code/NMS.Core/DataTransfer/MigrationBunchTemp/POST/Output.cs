﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.MigrationBunchTemp
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MigrationBunchTempId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String BunchGuid{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreatedDateStr
		{
			 get {if(CreatedDate.HasValue) return CreatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatedDate = date.ToUniversalTime();  }  } 
		}

	}	
}
