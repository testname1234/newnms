﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.MosActiveEpisode
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MosActiveEpisodeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? EpisodeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? StatusCode{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? PcrTelePropterStatus{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? PcrPlayoutStatus{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? PcrFourWindowStatus{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String StatusDescription{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String PcrTelePropterJson{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String PcrPlayoutJson{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String PcrFourWindowJson{ get; set; }

	}	
}
