﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.MosActiveTransformation
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MosActiveDetailId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? MosActiveItemId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Type{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String DeviceName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Label{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Channel{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? VideoLayer{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Delay{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Duration{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? AllowGpi{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? AllowRemoteTriggering{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? RemoteTriggerId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Positionx{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Positiony{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Scalex{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Scaley{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TranstitionDuration{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Defer{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Color{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

	}	
}
