﻿using Newtonsoft.Json;
using NMS.Core.DataTransfer.ShortNews;
using NMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NMS.Core.Models;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.DataTransfer.News
{
    public class GetNewsCustomOutput
    {
        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<MFilterCount> FilterCounts { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<GetOutput> News { get; set; }


    }
}
