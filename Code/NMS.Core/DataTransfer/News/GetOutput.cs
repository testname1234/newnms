﻿using Newtonsoft.Json;
using NMS.Core.DataTransfer.ShortNews;
using NMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NMS.Core.Models;
using System.Text.RegularExpressions;

namespace NMS.Core.DataTransfer.News
{
    public class GetOutput
    {
        public GetOutput()
        {
            //Categories = new List<NMS.Core.DataTransfer.Categories>();
            //Source = "The Nations";
            //UpdatedDate = DateTime.UtcNow;
            //IsVerified = true;
            //IsRecommended = true;
            //NewsTypeId = (int)NewsTypes.Package;
            //Programmes = new List<Program.GetOutput>() { new Program.GetOutput() { Name="bol News"} };
            //OtherChannelProgrammes = new List<Program.GetOutput>() { new Program.GetOutput() { Name = "other News" } };
            //InternationalProgrammes = new List<Program.GetOutput>() { new Program.GetOutput() { Name = "intnl News" } };
            //ThumbnailUrl = "/content/images/producer/defaultnews.gif";
            //NewsId = "100045";
            //LanguageCode = "en";
        }

        public string _id { get; set; }



        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? LocationId { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean? IsTagged { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 NewsFileId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string BunchGuid { get; set; }
        public string Highlight { get; set; }
        public string Title { get; set; }

        private string description;
        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description
        {
            get { return description; }
            set
            {
                if (!string.IsNullOrEmpty(Source) && Source != "Google News")
                {
                    if (!String.IsNullOrEmpty(value))
                        description = Regex.Replace(Regex.Replace(value, "(?!</br?.?.>)(?!<br?.?.>)(?!</p>)<.*?>", ""), "<.*?>", "<br/>");
                }
                else { description = value; }
                
            }
        }


        private string shortDescription;
        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ShortDescription
        {
            get { return shortDescription; }

            set
            {
                if (!string.IsNullOrEmpty(Source) && Source != "Google News")
                {
                    if (!String.IsNullOrEmpty(value))
                        shortDescription = Regex.Replace(Regex.Replace(value, "(?!</br?.?.>)(?!<br?.?.>)(?!</p>)<.*?>", ""), "<.*?>", "<br/>");
                }
                else { shortDescription = value; }
            }
        }

        public virtual System.String ResourceGuid { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TranslatedTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TranslatedDescription { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Category.GetOutput> Categories { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Location.GetOutput> Locations { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ResourceEdit ResourceEdit { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Resource.GetOutput> Resources { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Comment.GetOutput> Comments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Source { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int SourceTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Version { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ParentNewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LanguageCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ThumbnailUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsVerified { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsRecommended { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsAired { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int NewsTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string NewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.News.GetOutput> Updates { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.NewsFilter.GetOutput> Filters { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Program.GetOutput> Programmes { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Program.GetOutput> OtherChannelProgrammes { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Program.GetOutput> InternationalProgrammes { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int AddedToRundownCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int OnAirCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int OtherChannelExecutionCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NewsStatisticsOutput> NewsStatistics { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int NewsTickerCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Tag.GetOutput> Tags { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Organization.GetOutput> Organization { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ReporterId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double Score { get; set; }

        [IgnoreDataMember]
        public DateTime CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public DateTime PublishTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string PublishTimeStr
        {
            get { return PublishTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { PublishTime = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public DateTime LastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public List<int> NewsCameraMen { get; set; }

        [DataMember]
        public List<NMS.Core.DataTransfer.Ticker.PostInput> Tickers { get; set; }

        [DataMember]
        public string NewsPaperdescription { get; set; }

        [DataMember]
        public string Suggestions { get; set; }

        [DataMember]
        public string Slug { get; set; }

        [DataMember]
        public List<NMS.Core.DataTransfer.NewsUpdateHistory.PostInput> newsUpdateHistories { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int AssignedTo { get; set; }



        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string IsLatestNews
        {
            get
            {
                TimeSpan diff = DateTime.UtcNow - CreationDate;
                if (diff.TotalMinutes <= 15)
                {
                    return "HighlightNews";
                }
                else
                {
                    return "No";
                }
            }
            set
            {

            }
        }
        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string itemType { get { return "News"; } set { } }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.Entities.EditorialComment> EditorialComments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? NewsStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? EventType { get; set; }

        public List<NMS.Core.Entities.EventReporter> EventReporter { get; set; }

        public List<NMS.Core.Entities.EventResource> EventResource { get; set; }

        [DataMember(EmitDefaultValue = true)]
        public bool? Coverage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? ReporterBureauId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BureauLocation { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EventTypeName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FilterTypeId { get; set; }

       
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? TaggedBy { get; set; }

       
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? InsertedBy { get; set; }
    }
}
