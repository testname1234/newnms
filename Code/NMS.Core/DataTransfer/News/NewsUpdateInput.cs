﻿using Newtonsoft.Json;
using NMS.Core.DataTransfer.ShortNews;
using NMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NMS.Core.Models;

namespace NMS.Core.DataTransfer.News
{
    public class NewsUpdateInput
    {
        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string bunchId { get; set; }

        public List<NMS.Core.DataTransfer.Filter.GetOutput> Filters { get; set; }

        public List<NMS.Core.DataTransfer.Filter.GetOutput> FiltersToDiscard { get; set; }
    }
}
