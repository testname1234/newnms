﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using NMS.Core.Entities.Mongo;
using NMS.Core.Entities;


namespace NMS.Core.DataTransfer.News
{
    public class PostInput
    {

        public DateTime PublishTime { get; set; }

        public List<NMS.Core.Entities.Resource> ImageGuids { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Guid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BunchGuid { get; set; }

        public string Highlight { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LanguageCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ParentNewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> CategoryIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> LocationIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime NewsDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string NewsDateStr
        {
            get { return NewsDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NewsDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public int FilterTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReporterId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NewsTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsTagged { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ThumbnailUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.Tag.PostInput> Tags { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.Resource.PostInput> Resources { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.Comment.PostInput> Comments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public NMS.Core.Models.ResourceEdit ResourceEdit { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LinkedNewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReferenceNewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ChannelId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public MarkVerifyNewsInput MarkNewsVerified { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NewsPaperId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int RadioStationId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramId { get; set; }

        public string Slug { get; set; }

        public List<int> NewsCameraMen { get; set; }

        [DataMember]
        public List<NMS.Core.DataTransfer.Ticker.PostInput> Tickers { get; set; }

        [DataMember]
        public string NewsPaperdescription { get; set; }

        [DataMember]
        public string Suggestions { get; set; }

        [DataMember]
        public int AssignedTo { get; set; }

        [DataMember]
        public int NewsFileId { get; set; }


        public string Source { get; set; }
        public string Url { get; set; }

        public int? BureauId { get; set; }

        public string CelebrityId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> Organizations { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EventType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> EventReporterId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> EventResourceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? Coverage { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? TaggedBy { get; set; }
                
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? InsertedBy { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLive { get; set; }

    }
}
