﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace NMS.Core.DataTransfer.News
{
    public class PutInput
    {
        public Guid Id { get; set; }

       
        public string LanguageCode {get; set;}

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> CategoryIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> LocationIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public decimal Latitude { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public decimal Longitude { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime NewsDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> Tags { get; set; }
    }
}
