﻿using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsBucket
{
    [DataContract]
    public class InputNewsBucket
    {
        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.NewsBucket.PostInput> News { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserRole { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramId { get; set; }


    }
}
