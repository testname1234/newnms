﻿using System;
using System.Collections.Generic;

namespace NMS.Core.DataTransfer.NewsBunch
{
    public class GetOutput
    {
        public GetOutput()
        {
            BunchId = Guid.NewGuid();
            News = new DataTransfer.News.GetOutput();
            RelatedNews = new List<ShortNews.RelatedNews>();
        }

        public Guid BunchId { get; set; }

        public NMS.Core.DataTransfer.News.GetOutput News { get; set; }

        public List<NMS.Core.DataTransfer.ShortNews.RelatedNews> RelatedNews { get; set; }
    }
}
