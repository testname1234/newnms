﻿using System;
using System.Runtime.Serialization;
using Validation;
using System.Linq;
using System.Collections.Generic;

namespace NMS.Core.DataTransfer.NewsFile.GET
{
    public class NMSPollingInput
    {
        [DataMember(EmitDefaultValue = false)]
        public System.Int32 WorkRoleId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 UserId { get; set; }

        [IgnoreDataMember]
        public System.DateTime NewsFileLastUpdate{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NewsFileLastUpdateStr
        {
            get { return NewsFileLastUpdate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NewsFileLastUpdate = date.ToUniversalTime(); } }
        }

            [IgnoreDataMember]
        public System.DateTime FileResourceLastUpdate { get; set; }

        [DataMember(EmitDefaultValue = false)]
            public string FileResourceLastUpdateStr
            {
                get { return FileResourceLastUpdate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
                set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FileResourceLastUpdate = date.ToUniversalTime(); } }
            }  

        [IgnoreDataMember]
        public System.DateTime FileDetailLastUpdate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FileDetailLastUpdateStr
        {
            get { return FileDetailLastUpdate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FileDetailLastUpdate = date.ToUniversalTime(); } }
        }


        [IgnoreDataMember]
        public System.DateTime FileStatusHistoryLastUpdate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FileStatusHistoryLastUpdateStr
        {
            get { return FileStatusHistoryLastUpdate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FileStatusHistoryLastUpdate = date.ToUniversalTime(); } }
        }

            [IgnoreDataMember]
        public System.DateTime FileFolderHistoryLastUpdate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FileFolderHistoryLastUpdateStr
            {
                get { return FileFolderHistoryLastUpdate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
                set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FileFolderHistoryLastUpdate = date.ToUniversalTime(); } }
            }

         public int[] FolderIds { get; set; }
         public int[] SequenceList { get; set; }

        public string PageOffset { get; set; }
        public string PageSize { get; set; }

    }
}
