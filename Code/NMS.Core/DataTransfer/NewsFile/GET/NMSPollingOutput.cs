﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsFile.GET
{
    public class NMSPollingOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<NewsFile.GetOutput> NewsFile { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<FileResource.GetOutput> FileResource { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<FileDetail.GetOutput> FileDetail { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<FileStatusHistory.GetOutput> FileStatusHistory { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<FileFolderHistory.GetOutput> FileFolderHistory { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<WorkRoleFolder.GetOutput> WorkRoleFolder { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Folder.GetOutput> Folder { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<DataTransfer.User.GetOutput> Users { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<DataTransfer.Program.GetOutput> Programs { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsTagged { get; set; }

    }
}
