﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsFile
{
    [DataContract]
	public class GetOutput
	{
		
        
        public string username { get; set; }

        [DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsFileId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SequenceNo{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 CreatedBy{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 StatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FolderId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LocationId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CategoryId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Category { get; set; }

        [IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.String Title{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String NewsPaperDescription{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LanguageCode{ get; set; }

		[IgnoreDataMember]
		public System.DateTime PublishTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string PublishTimeStr
		{
			 get { return PublishTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { PublishTime = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.String Source{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SourceTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String SourceNewsUrl{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SourceFilterId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ParentId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? NewsStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String ResourceGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String DescriptionText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool VoiceOver { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsDeleted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? BroadcastedCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string IsLatestNews
        {
            get {  TimeSpan diff = DateTime.UtcNow - CreationDate;
                if (diff.TotalMinutes <= 15) {
                    return "HighlightNews";
                }
                else {
                    return "No";
                }

            }
            set
            {
            }

        }


        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string itemType { get { return "News"; } set { } }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsVerified { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public bool? IsTagged { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 EcCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Highlights { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BureauLocation { get; set; }
    }	
}
