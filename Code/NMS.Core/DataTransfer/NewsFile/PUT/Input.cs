﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsFile
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string NewsFileId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string SequenceNo{ get; set; }

		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string Slug{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string CreatedBy{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string StatusId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string FolderId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string LocationId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string CategoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldLength(MaxLength = 100)]
		[DataMember (EmitDefaultValue=false)]
		public string Title{ get; set; }

		[FieldLength(MaxLength = -1)]
		[DataMember (EmitDefaultValue=false)]
		public string NewsPaperDescription{ get; set; }

		[FieldLength(MaxLength = 10)]
		[DataMember (EmitDefaultValue=false)]
		public string LanguageCode{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string PublishTime{ get; set; }

		[FieldLength(MaxLength = 255)]
		[DataMember (EmitDefaultValue=false)]
		public string Source{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string SourceTypeId{ get; set; }

		[FieldLength(MaxLength = 500)]
		[DataMember (EmitDefaultValue=false)]
		public string SourceNewsUrl{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string SourceFilterId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string ParentId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NewsStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescriptionText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ResourceGuid { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Boolean)]
        [DataMember(EmitDefaultValue = false)]
        public string IsVerified { get; set; }

    }	
}
