﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Validation;

namespace NMS.Core.DataTransfer.NewsFilter
{
    [DataContract]
	public class GetOutput
	{
        [DataMember(EmitDefaultValue = false)]
        public string _id { get; set; }
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsFilterId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FilterId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String NewsId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}
	}	
}
