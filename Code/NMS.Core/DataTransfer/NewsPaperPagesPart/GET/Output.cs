﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsPaperPagesPart
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsPaperPagesPartId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? NewsPaperPageId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? PartNumber{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Bottom{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Top{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Left{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Right{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.NewsPaperPagesPartsDetail.GetOutput> NewsPaperPagesPartsDetail { get; set; }

	}	
}
