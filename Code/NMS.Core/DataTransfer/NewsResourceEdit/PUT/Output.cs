﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsResourceEdit
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsResourceEditId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid? ResourceGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? NewsId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ResourceTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String FileName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Top{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Left{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Bottom{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Right{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Url{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Isactive{ get; set; }

	}	
}
