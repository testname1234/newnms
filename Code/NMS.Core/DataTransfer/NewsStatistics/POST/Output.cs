﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsStatistics
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsStatisticsId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ChannelName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ProgramId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ProgramName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? EpisodeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? Time{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string TimeStr
		{
			 get {if(Time.HasValue) return Time.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Time = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Type{ get; set; }

	}	
}
