﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsTag
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsTagId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Rank{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TagId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

	}	
}
