﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsType
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String NewsType{ get; set; }

	}	
}
