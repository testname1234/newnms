﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NewsUpdateHistory
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NewsUpdateHistoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String NewsGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? NewsId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String TranslatedDescription{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String TranslatedTitle{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Title{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String BunchGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Description{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String DescriptionText{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ShortDescription{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LanguageCode{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Author{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ReporterId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? IsVerified{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? IsAired{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? AddedToRundownCount{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? OnAirCount{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? OtherChannelExecutionCount{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? NewsTickerCount{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Slug{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String NewsPaperdescription{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Suggestions{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Source{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SourceTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String SourceNewsUrl{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SourceFilterId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ThumbnailId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? NewsTypeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? PublishTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string PublishTimeStr
		{
			 get {if(PublishTime.HasValue) return PublishTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { PublishTime = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

	}	
}
