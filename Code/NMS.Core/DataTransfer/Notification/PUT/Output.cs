﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Notification
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NotificationId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 NotificationTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Title{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Argument{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? RecipientId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SenderId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsRead{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Slotid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Slotscreentemplateid{ get; set; }

	}	
}
