﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.NotificationType
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string NotificationTypeId{ get; set; }

		[FieldLength(MaxLength = 250)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string Name{ get; set; }

		[FieldLength(MaxLength = -1)]
		[FieldNullable(IsNullable = true)]
		[DataMember (EmitDefaultValue=false)]
		public string Template{ get; set; }

		[FieldLength(MaxLength = -1)]
		[FieldNullable(IsNullable = true)]
		[DataMember (EmitDefaultValue=false)]
		public string Argument{ get; set; }

	}	
}
