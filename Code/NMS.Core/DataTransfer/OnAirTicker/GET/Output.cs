﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.OnAirTicker
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String TickerGroupName{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LocationId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CategoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsShow{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String NewsGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? UserId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? OnAiredTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string OnAiredTimeStr
		{
			 get {if(OnAiredTime.HasValue) return OnAiredTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { OnAiredTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CreatedBy{ get; set; }


        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.OnAirTickerLine.GetOutput> TickerLines { get; set; }
	}	
}
