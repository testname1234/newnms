﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.OnAirTickerLine
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerLineId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Text{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LanguageCode{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsShow{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? RepeatCount{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Severity{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Frequency{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CreatedBy{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? OperatorNumber{ get; set; }

	}	
}
