﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Program
{
    [DataContract]
    public class GetOutput
    {

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ChannelId { get; set; }

        [IgnoreDataMember]
        public System.DateTime CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsActive { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsApproved { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? MinStoryCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? MaxStoryCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ProgramTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String StartupUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String CreditsUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? BucketId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? FilterId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String ApiKey { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 Interval { get; set; }
    }
}
