﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Program
{
    [DataContract]
    
    public class NewsSequenceInput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<Slot.PostInput> NewsSequence { get; set; }
    }
}
