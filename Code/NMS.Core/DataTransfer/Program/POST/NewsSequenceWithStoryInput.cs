﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Program
{
    [DataContract]
    public class NewsSequenceWithStory
	{
		
		[DataMember (EmitDefaultValue=false)]
		public int StroyId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SequenceId { get; set; }
	}

    [DataContract]
    public class NewsSequenceWithStoryInput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<NewsSequenceWithStory> NewsSequence { get; set; }
    }
}
