﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Program
{
    [DataContract]
    public class ProgramScreenOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public System.Collections.Generic.List<NMS.Core.DataTransfer.ScreenElement.GetOutput> ScreenElements { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public System.Collections.Generic.List<NMS.Core.DataTransfer.ScreenTemplate.GetOutput> ScreenTemplates { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public System.Collections.Generic.List<NMS.Core.DataTransfer.TemplateScreenElement.GetOutput> TemplateScreenElements { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Collections.Generic.List<NMS.Core.DataTransfer.ScreenTemplatekey.GetOutput> ScreenTemplatekeys { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> ScreenElementIds { get; set; }
    }
}
