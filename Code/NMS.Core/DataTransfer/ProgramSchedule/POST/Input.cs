﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ProgramSchedule
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string ProgramScheduleId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string WeekDayId{ get; set; }

		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string StartTime{ get; set; }

		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string EndTime{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string RecurringTypeId{ get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string ProgramId { get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string IsActive{ get; set; }

	}	
}
