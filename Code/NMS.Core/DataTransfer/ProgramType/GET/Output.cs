﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ProgramType
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ProgramTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ProgramTypeName{ get; set; }

	}	
}
