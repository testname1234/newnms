﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ProgramType
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string ProgramTypeId{ get; set; }

		[FieldLength(MaxLength = 250)]
		[DataMember (EmitDefaultValue=false)]
		public string ProgramTypeName{ get; set; }

	}	
}
