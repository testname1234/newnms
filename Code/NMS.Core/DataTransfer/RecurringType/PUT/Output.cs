﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.RecurringType
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RecurringTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Type{ get; set; }

	}	
}
