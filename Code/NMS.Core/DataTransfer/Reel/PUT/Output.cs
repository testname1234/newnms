﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Reel
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ReelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ChannelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Path{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 StatusId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string StartTimeStr
		{
			 get { return StartTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime EndTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string EndTimeStr
		{
			 get { return EndTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EndTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.String OriginalPath{ get; set; }

	}	
}
