﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ReelStatus
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ReelStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Status{ get; set; }

	}	
}
