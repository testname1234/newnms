﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.DataTransfer.Resource.GET
{
    public class MediaServerResources
    {
        public List<NMS.Core.Entities.Resource> Resources { get; set; }
    }
}
