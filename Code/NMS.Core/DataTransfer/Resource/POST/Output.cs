﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Resource
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Guid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceTypeId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FileName { get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
        [DataMember(EmitDefaultValue = false)]
        public System.String Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Category { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Location { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Double? Duration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Slug { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Description { get; set; }

	}	
}
