﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ResourceEditclipinfo
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceEditclipinfoId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? NewsResourceEditId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? From{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? To{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? Isactive{ get; set; }

	}	
}
