﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ResourceEditclipinfo
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string ResourceEditclipinfoId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string NewsResourceEditId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Double)]
		[DataMember (EmitDefaultValue=false)]
		public string From{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Double)]
		[DataMember (EmitDefaultValue=false)]
		public string To{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[DataMember (EmitDefaultValue=false)]
		public string Isactive{ get; set; }

	}	
}
