﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ResourceType
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ResourceTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ResourceType{ get; set; }

	}	
}
