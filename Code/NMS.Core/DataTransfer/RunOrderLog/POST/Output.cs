﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.RunOrderLog
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RunOrderLogId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? GroupId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? MessageId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String MosId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String NcsId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? RoStoriesCount{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String RoStatus{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? RoId{ get; set; }

	}	
}
