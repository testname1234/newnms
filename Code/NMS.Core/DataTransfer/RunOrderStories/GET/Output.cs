﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.RunOrderStories
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RunOrderStoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 RunOrderLogId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ItemChannel{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ItemId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ObjId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Status{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? StoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String StoryStatus{ get; set; }

	}	
}
