﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ScrapMaxDates
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string ScrapMaxDatesId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string MaxUpdateDate{ get; set; }

		[FieldLength(MaxLength = 500)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string SourceName{ get; set; }

	}	
}
