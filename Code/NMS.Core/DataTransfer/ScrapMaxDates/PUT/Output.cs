﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ScrapMaxDates
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ScrapMaxDatesId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime MaxUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string MaxUpdateDateStr
		{
			 get { return MaxUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { MaxUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.String SourceName{ get; set; }

	}	
}
