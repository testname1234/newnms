﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ScreenElement
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string ScreenElementId{ get; set; }

		[FieldLength(MaxLength = 50)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string Name{ get; set; }

		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string ThumbGuid{ get; set; }

		[FieldNullable(IsNullable = true)]
		[DataMember (EmitDefaultValue=false)]
		public string ImageGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string IsActive{ get; set; }

	}	
}
