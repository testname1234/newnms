﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;
using NMS.Core.Entities;

namespace NMS.Core.DataTransfer.ScreenTemplate
{
    [DataContract]
    public class GetOutput
    {

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 ScreenTemplateId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsDefault { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Html { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? Duration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Guid ThumbGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String GroupId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String AssetId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String BackgroundColor { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Guid? BackgroundImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String BackgroundRepeat { get; set; }

        [IgnoreDataMember]
        public System.DateTime CreatonDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string CreatonDateStr
        {
            get { return CreatonDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatonDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsActive { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<DataTransfer.ScreenTemplatekey.GetOutput> Keys { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? RatioTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsVideoWallTemplate { get; set; }

    }
}
