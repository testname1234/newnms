﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ScreenTemplateStatus
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ScreenTemplateStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

	}	
}
