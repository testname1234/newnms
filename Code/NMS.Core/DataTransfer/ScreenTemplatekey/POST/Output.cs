﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.ScreenTemplatekey
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ScreenTemplatekeyId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ScreenTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? FlashTemplateKeyId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String KeyName{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string creationDateStr
		{
            get { if (CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } } 
		}

		[IgnoreDataMember]
		public System.DateTime? Updateddate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string updateddateStr
		{
            get { if (Updateddate.HasValue) return Updateddate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Updateddate = date.ToUniversalTime(); } } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Top{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Bottom{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Left{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Right{ get; set; }

	}	
}
