﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Segment
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SegmentId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SegmentTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 EpisodeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SequnceNumber{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double? Duration{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? StoryCount{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsActive{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Name { get; set; }

	}	
}
