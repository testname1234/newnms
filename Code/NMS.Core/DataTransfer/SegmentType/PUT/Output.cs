﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SegmentType
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SegmentTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Segment{ get; set; }

	}	
}
