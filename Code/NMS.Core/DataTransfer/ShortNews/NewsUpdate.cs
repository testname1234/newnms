﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
namespace NMS.Core.DataTransfer.ShortNews
{
    public class NewsUpdate
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        [IgnoreDataMember]
        public System.DateTime UpdatedDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string UpdatedDateStr
        {
            get { return UpdatedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { UpdatedDate = date.ToUniversalTime(); } }
        }
    }
}
