﻿using Newtonsoft.Json;
using NMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NMS.Core.DataTransfer.ShortNews
{
    public class RelatedNews
    {
        public RelatedNews()
        {
            Source = "Dawn";
            UpdatedDate = DateTime.UtcNow.AddHours(-1);
            ViaId = (int)Via.FieldReporter;
            Updates = 2;
            ThumbnailUrl = "/content/images/producer/defaultnews-icon.png";
        }
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Source { get; set; }

        public int ViaId { get; set; }

        public string ThumbnailUrl { get; set; }

        public int Updates { get; set; }


        [IgnoreDataMember]
        public System.DateTime UpdatedDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string UpdatedDateStr
        {
            get { return UpdatedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { UpdatedDate = date.ToUniversalTime(); } }
        }
    }
}
