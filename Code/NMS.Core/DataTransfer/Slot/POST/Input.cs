﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Slot
{
    [DataContract]
	public class PostInput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public string SlotId{ get; set; }

		[FieldLength(MaxLength = 50)]
		[FieldNullable(IsNullable = true)]
		[DataMember (EmitDefaultValue=false)]
		public string NewsGuid{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string SequnceNumber{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string SlotTypeId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string SegmentId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string CategoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string IsActive{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PreviewGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? TickerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String ThumbGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IncludeInRundown { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? NewsFileId { get; set; }

        [FieldNameAttribute("Title", true, false, 500)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Title { get; set; }

        [FieldNameAttribute("Description", true, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String LanguageCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.String ResearchText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? AnchorId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual string[] Headline { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Collections.Generic.List<NMS.Core.Entities.SlotHeadline> SlotHeadlines { get; set; }

    }	
}
