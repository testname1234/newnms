﻿using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Slot
{
    [DataContract]
    public class InputSlots
    {
        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.Slot.PostInput> Slots { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserRole { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public NewsStatisticsInput NewsStatisticsParam { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? NewsFileId { get; set; }
        
    }
}
