﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NMS.Core.DataTransfer.SlotScreenTemplate
{
    [DataContract]
    public class GetOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public System.Int32 SlotScreenTemplateId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 SlotId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 SequenceNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsDefault { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Html { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? Duration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Guid ThumbGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String GroupId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String AssetId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String BackgroundColor { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Guid? BackgroundImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String BackgroundRepeat { get; set; }

        [IgnoreDataMember]
        public System.DateTime CreatonDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreatonDateStr
        {
            get { return CreatonDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatonDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime LastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean IsActive { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Script { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 ScreenTemplateId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.SlotTemplateScreenElement.GetOutput> SlotTemplateScreenElements { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.SlotScreenTemplateMic.GetOutput> SlotScreenTemplateMics { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.SlotScreenTemplateCamera.GetOutput> SlotScreenTemplateCameras { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.SlotTemplateScreenElementResource.GetOutput> SlotTemplateScreenElementResources { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String TemplateScreenElementImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String TemplateScreenElementImageGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsAssignedToNle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsAssignedToStoryWriter { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.SlotScreenTemplateResource.GetOutput> SlotScreenTemplateResources { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.SlotScreenTemplatekey.GetOutput> SlotScreenTemplatekeys { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 UserRoleId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? VideoDuration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ScriptDuration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Instructions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? ParentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? VideoWallId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsVideoWallTemplate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? StatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? NleStatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? StoryWriterStatusId { get; set; }
    }
}