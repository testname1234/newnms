﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.DataTransfer.SlotScreenTemplate.POST
{
    public class SlotScreenTemplateFlow
    {
        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.SlotScreenTemplate.PostInput> SlotScreenTemplates { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int EpisodeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserRoleId { get; set; }
    }
}
