﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SlotScreenTemplate
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string SlotScreenTemplateId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string SlotId{ get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [DataMember(EmitDefaultValue = false)]
        public System.Int32 SequenceNumber { get; set; }


		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string Name{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[DataMember (EmitDefaultValue=false)]
		public string IsDefault{ get; set; }

		[FieldLength(MaxLength = -1)]
		[DataMember (EmitDefaultValue=false)]
		public string Html{ get; set; }

		[FieldLength(MaxLength = 255)]
		[DataMember (EmitDefaultValue=false)]
		public string Description{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string Duration{ get; set; }

		
		[DataMember (EmitDefaultValue=false)]
		public string ThumbGuid{ get; set; }

		[FieldLength(MaxLength = 50)]
		[DataMember (EmitDefaultValue=false)]
		public string GroupId{ get; set; }

		[FieldLength(MaxLength = 50)]
		[DataMember (EmitDefaultValue=false)]
		public string AssetId{ get; set; }

		[FieldLength(MaxLength = 50)]
		[DataMember (EmitDefaultValue=false)]
		public string BackgroundColor{ get; set; }

		
		[DataMember (EmitDefaultValue=false)]
		public string BackgroundImageUrl{ get; set; }

		[FieldLength(MaxLength = 50)]
		[DataMember (EmitDefaultValue=false)]
		public string BackgroundRepeat{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreatonDate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDate{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[DataMember (EmitDefaultValue=false)]
		public string IsActive{ get; set; }

		[FieldLength(MaxLength = -1)]
		[DataMember (EmitDefaultValue=false)]
		public string Script{ get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [DataMember(EmitDefaultValue = false)]
        public Int32 ScreenTemplateId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsAssignedToNle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsAssignedToStoryWriter { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? VideoDuration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ScriptDuration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? ParentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? VideoWallId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsVideoWallTemplate { get; set; }

	}	
}
