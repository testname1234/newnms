﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SlotScreenTemplate
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SlotScreenTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SlotId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 SequenceNumber { get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsDefault{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Html{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Description{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Duration{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid ThumbGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String GroupId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String AssetId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String BackgroundColor{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid? BackgroundImageUrl{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String BackgroundRepeat{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreatonDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreatonDateStr
		{
			 get { return CreatonDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatonDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Script{ get; set; }


        [DataMember(EmitDefaultValue = false)]
        public System.Int32 ScreenTemplateId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsAssignedToNle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsAssignedToStoryWriter { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? VideoDuration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ScriptDuration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? ParentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? VideoWallId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsVideoWallTemplate { get; set; }
	}	
}
