﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SlotScreenTemplateResource
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SlotScreenTemplateResourceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SlotScreenTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid ResourceGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Comment{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsUploadedFromNle{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

        [FieldNameAttribute("Caption", true, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Caption { get; set; }

        [FieldNameAttribute("Category", true, false, 500)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Category { get; set; }

        [FieldNameAttribute("Location", true, false, 500)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Location { get; set; }

        [FieldNameAttribute("Duration", true, false, 9)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Decimal? Duration { get; set; }

        [FieldNameAttribute("ResourceTypeId", true, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? ResourceTypeId { get; set; }

	}	
}
