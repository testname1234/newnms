﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SlotScreenTemplateResource
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string SlotScreenTemplateResourceId{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[DataMember (EmitDefaultValue=false)]
		public string SlotScreenTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string ResourceGuid{ get; set; }

		[FieldLength(MaxLength = -1)]
		[DataMember (EmitDefaultValue=false)]
		public string Comment{ get; set; }

		[FieldTypeValidation(DataType=DataTypes.Boolean)]
		[DataMember (EmitDefaultValue=false)]
		public string IsUploadedFromNle{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public string CreationDate{ get; set; }

	}	
}
