﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SlotScreenTemplateResource
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SlotScreenTemplateResourceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SlotScreenTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Guid ResourceGuid{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Comment{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsUploadedFromNle{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

	}	
}
