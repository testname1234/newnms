﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SlotTemplateScreenElementResource
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SlotTemplateScreenElementResourceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SlotTemplateScreenElementId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SlotScreenTemplateId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String ResourceGuid{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Caption{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Category{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Location{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Decimal? Duration{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ResourceTypeId{ get; set; }

	}	
}
