﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SuggestedFlow
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SuggestedFlowId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Key{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> TemplateIds 
        {
            get 
            {
                string[] temp = Key.Split('_');
                List<int> ids = new List<int>();

                for (int i = 0; i < temp.Length; i++)
                {
                    ids.Add(Convert.ToInt32(temp[i]));
                }

                return ids;
            }
        }
	}	
}
