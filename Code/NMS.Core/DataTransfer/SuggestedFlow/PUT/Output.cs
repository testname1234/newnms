﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.SuggestedFlow
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 SuggestedFlowId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Key{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ProgramId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? EpisodeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

	}	
}
