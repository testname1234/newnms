﻿using Newtonsoft.Json;
using NMS.Core.DataTransfer.ShortNews;
using NMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NMS.Core.DataTransfer.Tag
{
    public class GetOutput
    {
        public string _id { get; set; }

        public string Tag { get; set; }

        public double Rank { get; set; }
        public int TagId { get; set; }

    }
}
