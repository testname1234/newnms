﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.TemplateScreenElement
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TemplateScreenElementId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ScreenElementId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double Top{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double Bottom{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double Left{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double Right{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Double Zindex{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ScreenTemplateId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String ImageGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? CasperMosItemId { get; set; }

	}	
}
