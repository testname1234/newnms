﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.Ticker
{
    [DataContract]
    public class PostInput
    {

        [DataMember(EmitDefaultValue = false)]
        public string TickerId { get; set; }


        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string CategoryId { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string LocationId { get; set; }

        [FieldLength(MaxLength = 50)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string NewsGuid { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string UserId { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string TickerStatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string OnAiredTime { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string SequenceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastUpdatedDate { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Boolean)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string IsActive { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string TickerTypeId { get; set; }


        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string  CreatedBy { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public  System.String TickerGroupName { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.TickerLine.PostInput> TickerLines { get; set; }
    }
}
