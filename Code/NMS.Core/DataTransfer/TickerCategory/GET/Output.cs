﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;
using NMS.Core.Entities;

namespace NMS.Core.DataTransfer.TickerCategory
{
    [DataContract]
    public class GetOutput
    {

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 TickerCategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? SequenceNumber { get; set; }

        [IgnoreDataMember]
        public System.DateTime? CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string CreationDateStr
        {
            get { if (CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime? LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDateStr
        {
            get { if (LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public System.Boolean? IsActive { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Ticker.GetOutput> Tickers { get { return _Tickers; } set { _Tickers = value; } }
        private List<Ticker.GetOutput> _Tickers = new List<Ticker.GetOutput>();

        [DataMember(EmitDefaultValue = false)]
        public List<GetOutput> Subcategories { get { return _Subcategories; } set { _Subcategories = value; } }
        private List<GetOutput> _Subcategories = new List<GetOutput>();


        [DataMember(EmitDefaultValue = false)]
        public int LanguageCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? ParentTickerCategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerSize { get; set; }


        public int[] CategoryIds { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string Language
        {
            get
            {
                if (LanguageCode == 1) return "UR";
                else return "EN";
            }
            set
            {
            }
        }


        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? TicketCategoryTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? McrTickerCategoryId { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String McrTickerCategoryName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? HourPolicy { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? NewTickerPolicy { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? Priority { get; set; }

    }
}
