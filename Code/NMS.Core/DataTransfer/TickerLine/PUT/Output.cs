﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.TickerLine
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerLineId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Text{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String LanguageCode{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? SequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? BreakingStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LatestStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CategoryStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? BreakingSequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? LatestSequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CategorySequenceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? RepeatCount{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Severity{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? Frequency{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? CreatedBy{ get; set; }

	}	
}
