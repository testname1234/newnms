﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.TickerStatus
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String TickerStatusName{ get; set; }

	}	
}
