﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.TickerStatus
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string TickerStatusId{ get; set; }

		[FieldLength(MaxLength = 250)]
		[DataMember (EmitDefaultValue=false)]
		public string TickerStatusName{ get; set; }

	}	
}
