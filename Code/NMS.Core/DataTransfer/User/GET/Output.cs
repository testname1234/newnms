﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.User
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 UserId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Login{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.Team.GetOutput> UserRoles { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Fullname { get; set; }
	}	
}
