﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.DataTransfer.WorkRoleFolder
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 WorkRoleFolderId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 WorkRoleId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 FolderId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean AllowRead{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean AllowModify{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean AllowCreate{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean AllowCopy{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean AllowMove{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean AllowDelete{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 MaxColor{ get; set; }

	}	
}
