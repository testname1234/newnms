﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class AssignmentBase:EntityBase, IEquatable<AssignmentBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("AssignmentId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 AssignmentId{ get; set; }

		[FieldNameAttribute("Slug",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("LocationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("CategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		[FieldNameAttribute("AssignmentType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AssignmentType{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Picture",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Picture{ get; set; }

		[FieldNameAttribute("Video",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Video{ get; set; }

		[FieldNameAttribute("Audio",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Audio{ get; set; }

		[FieldNameAttribute("Document",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Document{ get; set; }

		[FieldNameAttribute("Barcode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Barcode{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<AssignmentBase> Members

        public virtual bool Equals(AssignmentBase other)
        {
			if(this.AssignmentId==other.AssignmentId  && this.Slug==other.Slug  && this.LocationId==other.LocationId  && this.CategoryId==other.CategoryId  && this.AssignmentType==other.AssignmentType  && this.CreationDate==other.CreationDate  && this.Picture==other.Picture  && this.Video==other.Video  && this.Audio==other.Audio  && this.Document==other.Document  && this.Barcode==other.Barcode )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Assignment other)
        {
			if(other!=null)
			{
				this.AssignmentId=other.AssignmentId;
				this.Slug=other.Slug;
				this.LocationId=other.LocationId;
				this.CategoryId=other.CategoryId;
				this.AssignmentType=other.AssignmentType;
				this.CreationDate=other.CreationDate;
				this.Picture=other.Picture;
				this.Video=other.Video;
				this.Audio=other.Audio;
				this.Document=other.Document;
				this.Barcode=other.Barcode;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
