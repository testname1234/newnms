﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class BunchFilterNewsBase:EntityBase, IEquatable<BunchFilterNewsBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("BunchFilterNewsId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BunchFilterNewsId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isactive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Isactive{ get; set; }

		[FieldNameAttribute("BunchGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BunchGuid{ get; set; }

		[FieldNameAttribute("FilterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FilterId{ get; set; }

		[FieldNameAttribute("Newsguid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Newsguid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<BunchFilterNewsBase> Members

        public virtual bool Equals(BunchFilterNewsBase other)
        {
			if(this.BunchFilterNewsId==other.BunchFilterNewsId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Isactive==other.Isactive  && this.BunchGuid==other.BunchGuid  && this.FilterId==other.FilterId  && this.Newsguid==other.Newsguid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(BunchFilterNews other)
        {
			if(other!=null)
			{
				this.BunchFilterNewsId=other.BunchFilterNewsId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Isactive=other.Isactive;
				this.BunchGuid=other.BunchGuid;
				this.FilterId=other.FilterId;
				this.Newsguid=other.Newsguid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
