﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class BunchNewsBase:EntityBase, IEquatable<BunchNewsBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("BunchNewsId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BunchNewsId{ get; set; }

		[FieldNameAttribute("BunchId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? BunchId{ get; set; }

		[FieldNameAttribute("NewsId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("BunchGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BunchGuid{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<BunchNewsBase> Members

        public virtual bool Equals(BunchNewsBase other)
        {
			if(this.BunchNewsId==other.BunchNewsId  && this.BunchId==other.BunchId  && this.NewsId==other.NewsId  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive  && this.BunchGuid==other.BunchGuid  && this.NewsGuid==other.NewsGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(BunchNews other)
        {
			if(other!=null)
			{
				this.BunchNewsId=other.BunchNewsId;
				this.BunchId=other.BunchId;
				this.NewsId=other.NewsId;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
				this.BunchGuid=other.BunchGuid;
				this.NewsGuid=other.NewsGuid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
