﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class BunchResourceBase:EntityBase, IEquatable<BunchResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("BunchResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BunchResourceId{ get; set; }

		[FieldNameAttribute("BunchId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 BunchId{ get; set; }

		[FieldNameAttribute("ResourceId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<BunchResourceBase> Members

        public virtual bool Equals(BunchResourceBase other)
        {
			if(this.BunchResourceId==other.BunchResourceId  && this.BunchId==other.BunchId  && this.ResourceId==other.ResourceId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(BunchResource other)
        {
			if(other!=null)
			{
				this.BunchResourceId=other.BunchResourceId;
				this.BunchId=other.BunchId;
				this.ResourceId=other.ResourceId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
