﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CameraTypeBase:EntityBase, IEquatable<CameraTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CameraTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CameraTypeId{ get; set; }

		[FieldNameAttribute("Name",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CameraTypeBase> Members

        public virtual bool Equals(CameraTypeBase other)
        {
			if(this.CameraTypeId==other.CameraTypeId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CameraType other)
        {
			if(other!=null)
			{
				this.CameraTypeId=other.CameraTypeId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
