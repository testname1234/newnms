﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CasperFlashTemplateWindowBase:EntityBase, IEquatable<CasperFlashTemplateWindowBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CasperFlashTemplateWindowId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CasperFlashTemplateWindowId{ get; set; }

		[FieldNameAttribute("FlashTemplateId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashTemplateId{ get; set; }

		[FieldNameAttribute("Name",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("TopLeftx",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? TopLeftx{ get; set; }

		[FieldNameAttribute("BottomRighty",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? BottomRighty{ get; set; }

		[FieldNameAttribute("Width",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? Width{ get; set; }

		[FieldNameAttribute("Height",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? Height{ get; set; }

		[FieldNameAttribute("PortId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? PortId{ get; set; }

		[FieldNameAttribute("FlashWindowTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashWindowTypeId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CasperFlashTemplateWindowBase> Members

        public virtual bool Equals(CasperFlashTemplateWindowBase other)
        {
			if(this.CasperFlashTemplateWindowId==other.CasperFlashTemplateWindowId  && this.FlashTemplateId==other.FlashTemplateId  && this.Name==other.Name  && this.TopLeftx==other.TopLeftx  && this.BottomRighty==other.BottomRighty  && this.Width==other.Width  && this.Height==other.Height  && this.PortId==other.PortId  && this.FlashWindowTypeId==other.FlashWindowTypeId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CasperFlashTemplateWindow other)
        {
			if(other!=null)
			{
				this.CasperFlashTemplateWindowId=other.CasperFlashTemplateWindowId;
				this.FlashTemplateId=other.FlashTemplateId;
				this.Name=other.Name;
				this.TopLeftx=other.TopLeftx;
				this.BottomRighty=other.BottomRighty;
				this.Width=other.Width;
				this.Height=other.Height;
				this.PortId=other.PortId;
				this.FlashWindowTypeId=other.FlashWindowTypeId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
