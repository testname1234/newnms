﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CasperItemTransformationBase:EntityBase, IEquatable<CasperItemTransformationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CasperItemTransformationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CasperItemTransformationId{ get; set; }

		[FieldNameAttribute("CasperTemplateItemId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CasperTemplateItemId{ get; set; }

		[FieldNameAttribute("Type",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		[FieldNameAttribute("Label",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Label{ get; set; }

		[FieldNameAttribute("Name",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Channel",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Channel{ get; set; }

		[FieldNameAttribute("Videolayer",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Videolayer{ get; set; }

		[FieldNameAttribute("Devicename",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Devicename{ get; set; }

		[FieldNameAttribute("Allowgpi",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Allowgpi{ get; set; }

		[FieldNameAttribute("Allowremotetriggering",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Allowremotetriggering{ get; set; }

		[FieldNameAttribute("Remotetriggerid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Remotetriggerid{ get; set; }

		[FieldNameAttribute("Tween",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Tween{ get; set; }

		[FieldNameAttribute("triggeronnext",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Triggeronnext{ get; set; }

		[FieldNameAttribute("Positionx",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Positionx{ get; set; }

		[FieldNameAttribute("Positiony",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Positiony{ get; set; }

		[FieldNameAttribute("Scalex",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Scalex{ get; set; }

		[FieldNameAttribute("Scaley",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Scaley{ get; set; }

		[FieldNameAttribute("defer",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Defer{ get; set; }

		[FieldNameAttribute("FlashWindowTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashWindowTypeId{ get; set; }

		[FieldNameAttribute("Flashtemplatewindowid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Flashtemplatewindowid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CasperItemTransformationBase> Members

        public virtual bool Equals(CasperItemTransformationBase other)
        {
			if(this.CasperItemTransformationId==other.CasperItemTransformationId  && this.CasperTemplateItemId==other.CasperTemplateItemId  && this.Type==other.Type  && this.Label==other.Label  && this.Name==other.Name  && this.Channel==other.Channel  && this.Videolayer==other.Videolayer  && this.Devicename==other.Devicename  && this.Allowgpi==other.Allowgpi  && this.Allowremotetriggering==other.Allowremotetriggering  && this.Remotetriggerid==other.Remotetriggerid  && this.Tween==other.Tween  && this.Triggeronnext==other.Triggeronnext  && this.Positionx==other.Positionx  && this.Positiony==other.Positiony  && this.Scalex==other.Scalex  && this.Scaley==other.Scaley  && this.Defer==other.Defer  && this.FlashWindowTypeId==other.FlashWindowTypeId  && this.Flashtemplatewindowid==other.Flashtemplatewindowid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CasperItemTransformation other)
        {
			if(other!=null)
			{
				this.CasperItemTransformationId=other.CasperItemTransformationId;
				this.CasperTemplateItemId=other.CasperTemplateItemId;
				this.Type=other.Type;
				this.Label=other.Label;
				this.Name=other.Name;
				this.Channel=other.Channel;
				this.Videolayer=other.Videolayer;
				this.Devicename=other.Devicename;
				this.Allowgpi=other.Allowgpi;
				this.Allowremotetriggering=other.Allowremotetriggering;
				this.Remotetriggerid=other.Remotetriggerid;
				this.Tween=other.Tween;
				this.Triggeronnext=other.Triggeronnext;
				this.Positionx=other.Positionx;
				this.Positiony=other.Positiony;
				this.Scalex=other.Scalex;
				this.Scaley=other.Scaley;
				this.Defer=other.Defer;
				this.FlashWindowTypeId=other.FlashWindowTypeId;
				this.Flashtemplatewindowid=other.Flashtemplatewindowid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
