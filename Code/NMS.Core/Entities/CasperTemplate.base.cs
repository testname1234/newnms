﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CasperTemplateBase:EntityBase, IEquatable<CasperTemplateBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CasperTemlateId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CasperTemlateId{ get; set; }

		[FieldNameAttribute("Template",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Template{ get; set; }

		[FieldNameAttribute("ItemCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ItemCount{ get; set; }

		[FieldNameAttribute("FlashTemplateId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashTemplateId{ get; set; }

		[FieldNameAttribute("TemplateTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TemplateTypeId{ get; set; }

		[FieldNameAttribute("Width",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? Width{ get; set; }

		[FieldNameAttribute("Height",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? Height{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResourceGuid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CasperTemplateBase> Members

        public virtual bool Equals(CasperTemplateBase other)
        {
			if(this.CasperTemlateId==other.CasperTemlateId  && this.Template==other.Template  && this.ItemCount==other.ItemCount  && this.FlashTemplateId==other.FlashTemplateId  && this.TemplateTypeId==other.TemplateTypeId  && this.Width==other.Width  && this.Height==other.Height  && this.ResourceGuid==other.ResourceGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CasperTemplate other)
        {
			if(other!=null)
			{
				this.CasperTemlateId=other.CasperTemlateId;
				this.Template=other.Template;
				this.ItemCount=other.ItemCount;
				this.FlashTemplateId=other.FlashTemplateId;
				this.TemplateTypeId=other.TemplateTypeId;
				this.Width=other.Width;
				this.Height=other.Height;
				this.ResourceGuid=other.ResourceGuid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
