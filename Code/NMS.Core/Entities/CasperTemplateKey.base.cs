﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CasperTemplateKeyBase:EntityBase, IEquatable<CasperTemplateKeyBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CasperTemplateKeyId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CasperTemplateKeyId{ get; set; }

		[FieldNameAttribute("FlashTemplateId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashTemplateId{ get; set; }

		[FieldNameAttribute("FlashKey",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FlashKey{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("FlashTemplateKeyId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashTemplateKeyId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CasperTemplateKeyBase> Members

        public virtual bool Equals(CasperTemplateKeyBase other)
        {
			if(this.CasperTemplateKeyId==other.CasperTemplateKeyId  && this.FlashTemplateId==other.FlashTemplateId  && this.FlashKey==other.FlashKey  && this.CreationDate==other.CreationDate  && this.FlashTemplateKeyId==other.FlashTemplateKeyId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CasperTemplateKey other)
        {
			if(other!=null)
			{
				this.CasperTemplateKeyId=other.CasperTemplateKeyId;
				this.FlashTemplateId=other.FlashTemplateId;
				this.FlashKey=other.FlashKey;
				this.CreationDate=other.CreationDate;
				this.FlashTemplateKeyId=other.FlashTemplateKeyId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
