﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CasperTemplateTypeBase:EntityBase, IEquatable<CasperTemplateTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CasperTemplateTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CasperTemplateTypeId{ get; set; }

		[FieldNameAttribute("TemplateTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TemplateTypeId{ get; set; }

		[FieldNameAttribute("Name",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CasperTemplateTypeBase> Members

        public virtual bool Equals(CasperTemplateTypeBase other)
        {
			if(this.CasperTemplateTypeId==other.CasperTemplateTypeId  && this.TemplateTypeId==other.TemplateTypeId  && this.Name==other.Name  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CasperTemplateType other)
        {
			if(other!=null)
			{
				this.CasperTemplateTypeId=other.CasperTemplateTypeId;
				this.TemplateTypeId=other.TemplateTypeId;
				this.Name=other.Name;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
