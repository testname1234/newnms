﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CasperTemplateWindowTypeBase:EntityBase, IEquatable<CasperTemplateWindowTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CasperTemplateWindowTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CasperTemplateWindowTypeId{ get; set; }

		[FieldNameAttribute("FlashWindowTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashWindowTypeId{ get; set; }

		[FieldNameAttribute("FlashWindowType",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FlashWindowType{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CasperTemplateWindowTypeBase> Members

        public virtual bool Equals(CasperTemplateWindowTypeBase other)
        {
			if(this.CasperTemplateWindowTypeId==other.CasperTemplateWindowTypeId  && this.FlashWindowTypeId==other.FlashWindowTypeId  && this.FlashWindowType==other.FlashWindowType  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CasperTemplateWindowType other)
        {
			if(other!=null)
			{
				this.CasperTemplateWindowTypeId=other.CasperTemplateWindowTypeId;
				this.FlashWindowTypeId=other.FlashWindowTypeId;
				this.FlashWindowType=other.FlashWindowType;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
