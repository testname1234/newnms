﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CategoryBase:EntityBase, IEquatable<CategoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CategoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CategoryId{ get; set; }

		[FieldNameAttribute("Category",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Category{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("IsApproved",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsApproved{ get; set; }

		[FieldNameAttribute("CategoryInUrdu",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String CategoryInUrdu{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CategoryBase> Members

        public virtual bool Equals(CategoryBase other)
        {
			if(this.CategoryId==other.CategoryId  && this.Category==other.Category  && this.ParentId==other.ParentId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsApproved==other.IsApproved  && this.CategoryInUrdu==other.CategoryInUrdu )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Category other)
        {
			if(other!=null)
			{
				this.CategoryId=other.CategoryId;
				this.Category=other.Category;
				this.ParentId=other.ParentId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsApproved=other.IsApproved;
				this.CategoryInUrdu=other.CategoryInUrdu;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
