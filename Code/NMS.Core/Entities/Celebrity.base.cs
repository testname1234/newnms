﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CelebrityBase:EntityBase, IEquatable<CelebrityBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CelebrityId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CelebrityId{ get; set; }

		[FieldNameAttribute("Name",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("ImageGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? ImageGuid{ get; set; }

		[FieldNameAttribute("Designation",true,false,2000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Designation{ get; set; }

		[FieldNameAttribute("LocationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("CategoryId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CategoryId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("WordFrequency",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? WordFrequency{ get; set; }

		[FieldNameAttribute("AddedToRundown",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AddedToRundown{ get; set; }

		[FieldNameAttribute("OnAired",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OnAired{ get; set; }

		[FieldNameAttribute("ExecutedOnOtherChannels",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ExecutedOnOtherChannels{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CelebrityBase> Members

        public virtual bool Equals(CelebrityBase other)
        {
			if(this.CelebrityId==other.CelebrityId  && this.Name==other.Name  && this.ImageGuid==other.ImageGuid  && this.Designation==other.Designation  && this.LocationId==other.LocationId  && this.CategoryId==other.CategoryId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.WordFrequency==other.WordFrequency  && this.AddedToRundown==other.AddedToRundown  && this.OnAired==other.OnAired  && this.ExecutedOnOtherChannels==other.ExecutedOnOtherChannels )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Celebrity other)
        {
			if(other!=null)
			{
				this.CelebrityId=other.CelebrityId;
				this.Name=other.Name;
				this.ImageGuid=other.ImageGuid;
				this.Designation=other.Designation;
				this.LocationId=other.LocationId;
				this.CategoryId=other.CategoryId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.WordFrequency=other.WordFrequency;
				this.AddedToRundown=other.AddedToRundown;
				this.OnAired=other.OnAired;
				this.ExecutedOnOtherChannels=other.ExecutedOnOtherChannels;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
