﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class Celebrity : CelebrityBase 
	{
        public string Location { get; set; }
        public string Category { get; set; }

        public List<CelebrityStatistic> CelebrityStatistics { get; set; }
	}
}
