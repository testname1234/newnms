﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CelebrityStatisticBase:EntityBase, IEquatable<CelebrityStatisticBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CelebrityStatisticId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CelebrityStatisticId{ get; set; }

		[FieldNameAttribute("ProgramId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("ProgramName",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ProgramName{ get; set; }

		[FieldNameAttribute("ChannelId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelId{ get; set; }

		[FieldNameAttribute("ChannelName",true,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ChannelName{ get; set; }

		[FieldNameAttribute("EpisodeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? EpisodeId{ get; set; }

		[FieldNameAttribute("EpisodeTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? EpisodeTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string EpisodeTimeStr
		{
			 get {if(EpisodeTime.HasValue) return EpisodeTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EpisodeTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("StatisticType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StatisticType{ get; set; }

		[FieldNameAttribute("CelebrityId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CelebrityStatisticBase> Members

        public virtual bool Equals(CelebrityStatisticBase other)
        {
			if(this.CelebrityStatisticId==other.CelebrityStatisticId  && this.ProgramId==other.ProgramId  && this.ProgramName==other.ProgramName  && this.ChannelId==other.ChannelId  && this.ChannelName==other.ChannelName  && this.EpisodeId==other.EpisodeId  && this.EpisodeTime==other.EpisodeTime  && this.StatisticType==other.StatisticType  && this.CelebrityId==other.CelebrityId  && this.CreationDate==other.CreationDate  && this.LastUpdatedDate==other.LastUpdatedDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CelebrityStatistic other)
        {
			if(other!=null)
			{
				this.CelebrityStatisticId=other.CelebrityStatisticId;
				this.ProgramId=other.ProgramId;
				this.ProgramName=other.ProgramName;
				this.ChannelId=other.ChannelId;
				this.ChannelName=other.ChannelName;
				this.EpisodeId=other.EpisodeId;
				this.EpisodeTime=other.EpisodeTime;
				this.StatisticType=other.StatisticType;
				this.CelebrityId=other.CelebrityId;
				this.CreationDate=other.CreationDate;
				this.LastUpdatedDate=other.LastUpdatedDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
