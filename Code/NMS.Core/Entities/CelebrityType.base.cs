﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CelebrityTypeBase:EntityBase, IEquatable<CelebrityTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CelebrityTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CelebrityTypeId{ get; set; }

		[FieldNameAttribute("Name",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CelebrityTypeBase> Members

        public virtual bool Equals(CelebrityTypeBase other)
        {
			if(this.CelebrityTypeId==other.CelebrityTypeId  && this.Name==other.Name  && this.IsActive==other.IsActive  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CelebrityType other)
        {
			if(other!=null)
			{
				this.CelebrityTypeId=other.CelebrityTypeId;
				this.Name=other.Name;
				this.IsActive=other.IsActive;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
