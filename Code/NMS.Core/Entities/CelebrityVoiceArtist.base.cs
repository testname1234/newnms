﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CelebrityVoiceArtistBase:EntityBase, IEquatable<CelebrityVoiceArtistBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CelebrityVoiceArtistId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CelebrityVoiceArtistId{ get; set; }

		[FieldNameAttribute("CelebrityId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		[FieldNameAttribute("VoiceArtistId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? VoiceArtistId{ get; set; }

		[FieldNameAttribute("Language",true,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Language{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CelebrityVoiceArtistBase> Members

        public virtual bool Equals(CelebrityVoiceArtistBase other)
        {
			if(this.CelebrityVoiceArtistId==other.CelebrityVoiceArtistId  && this.CelebrityId==other.CelebrityId  && this.VoiceArtistId==other.VoiceArtistId  && this.Language==other.Language  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CelebrityVoiceArtist other)
        {
			if(other!=null)
			{
				this.CelebrityVoiceArtistId=other.CelebrityVoiceArtistId;
				this.CelebrityId=other.CelebrityId;
				this.VoiceArtistId=other.VoiceArtistId;
				this.Language=other.Language;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
