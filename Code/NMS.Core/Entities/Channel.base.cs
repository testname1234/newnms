﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelBase:EntityBase, IEquatable<ChannelBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("IsOtherChannel",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsOtherChannel{ get; set; }

		[FieldNameAttribute("VideosPath",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String VideosPath{ get; set; }

		[FieldNameAttribute("ImportVideosPath",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ImportVideosPath{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelBase> Members

        public virtual bool Equals(ChannelBase other)
        {
			if(this.ChannelId==other.ChannelId  && this.Name==other.Name  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsOtherChannel==other.IsOtherChannel )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Channel other)
        {
			if(other!=null)
			{
				this.ChannelId=other.ChannelId;
				this.Name=other.Name;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsOtherChannel=other.IsOtherChannel;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
