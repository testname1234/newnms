﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class Channel : ChannelBase 
	{
        public string ChannelConnectionString { get; set; }
	}
}
