﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelProgramBase:EntityBase, IEquatable<ChannelProgramBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelProgramId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelProgramId{ get; set; }

		[FieldNameAttribute("ChannelId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelId{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelProgramBase> Members

        public virtual bool Equals(ChannelProgramBase other)
        {
			if(this.ChannelProgramId==other.ChannelProgramId  && this.ChannelId==other.ChannelId  && this.ProgramId==other.ProgramId  && this.CreationDate==other.CreationDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ChannelProgram other)
        {
			if(other!=null)
			{
				this.ChannelProgramId=other.ChannelProgramId;
				this.ChannelId=other.ChannelId;
				this.ProgramId=other.ProgramId;
				this.CreationDate=other.CreationDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
