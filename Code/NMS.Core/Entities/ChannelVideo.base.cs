﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelVideoBase:EntityBase, IEquatable<ChannelVideoBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelVideoId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelVideoId{ get; set; }

		[FieldNameAttribute("ChannelId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelId{ get; set; }

		[FieldNameAttribute("From",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? From{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string FromStr
		{
			 get {if(From.HasValue) return From.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("To",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? To{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string ToStr
		{
			 get {if(To.HasValue) return To.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("ProgramId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("PhysicalPath",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String PhysicalPath{ get; set; }

		[FieldNameAttribute("Url",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		[FieldNameAttribute("IsProcessed",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsProcessed{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelVideoBase> Members

        public virtual bool Equals(ChannelVideoBase other)
        {
			if(this.ChannelVideoId==other.ChannelVideoId  && this.ChannelId==other.ChannelId  && this.From==other.From  && this.To==other.To  && this.ProgramId==other.ProgramId  && this.PhysicalPath==other.PhysicalPath  && this.Url==other.Url  && this.IsProcessed==other.IsProcessed  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ChannelVideo other)
        {
			if(other!=null)
			{
				this.ChannelVideoId=other.ChannelVideoId;
				this.ChannelId=other.ChannelId;
				this.From=other.From;
				this.To=other.To;
				this.ProgramId=other.ProgramId;
				this.PhysicalPath=other.PhysicalPath;
				this.Url=other.Url;
				this.IsProcessed=other.IsProcessed;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
