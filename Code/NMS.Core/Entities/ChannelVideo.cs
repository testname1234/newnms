﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class ChannelVideo : ChannelVideoBase 
	{
        public double DurationSeconds { get; set; }
		
	}
}
