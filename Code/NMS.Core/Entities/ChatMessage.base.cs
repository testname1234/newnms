﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ChatMessageBase:EntityBase, IEquatable<ChatMessageBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MessageId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MessageId{ get; set; }

		[FieldNameAttribute("From",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 From{ get; set; }

		[FieldNameAttribute("To",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 To{ get; set; }

		[FieldNameAttribute("Message",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Message{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsRecieved",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsRecieved{ get; set; }

		[FieldNameAttribute("IsSentToGroup",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsSentToGroup{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChatMessageBase> Members

        public virtual bool Equals(ChatMessageBase other)
        {
			if(this.MessageId==other.MessageId  && this.From==other.From  && this.To==other.To  && this.Message==other.Message  && this.CreationDate==other.CreationDate  && this.IsRecieved==other.IsRecieved  && this.IsSentToGroup==other.IsSentToGroup )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ChatMessage other)
        {
			if(other!=null)
			{
				this.MessageId=other.MessageId;
				this.From=other.From;
				this.To=other.To;
				this.Message=other.Message;
				this.CreationDate=other.CreationDate;
				this.IsRecieved=other.IsRecieved;
				this.IsSentToGroup=other.IsSentToGroup;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
