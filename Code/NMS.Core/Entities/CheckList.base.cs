﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CheckListBase:EntityBase, IEquatable<CheckListBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChecklistId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChecklistId{ get; set; }

		[FieldNameAttribute("ChecklistName",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ChecklistName{ get; set; }

		[FieldNameAttribute("Status",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Status{ get; set; }

		[FieldNameAttribute("ErrorMessage",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ErrorMessage{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Priority",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Priority{ get; set; }

		[FieldNameAttribute("LastUpdatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CheckListBase> Members

        public virtual bool Equals(CheckListBase other)
        {
			if(this.ChecklistId==other.ChecklistId  && this.ChecklistName==other.ChecklistName  && this.Status==other.Status  && this.ErrorMessage==other.ErrorMessage  && this.CreationDate==other.CreationDate  && this.Priority==other.Priority  && this.LastUpdatedDate==other.LastUpdatedDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(CheckList other)
        {
			if(other!=null)
			{
				this.ChecklistId=other.ChecklistId;
				this.ChecklistName=other.ChecklistName;
				this.Status=other.Status;
				this.ErrorMessage=other.ErrorMessage;
				this.CreationDate=other.CreationDate;
				this.Priority=other.Priority;
				this.LastUpdatedDate=other.LastUpdatedDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
