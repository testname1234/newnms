﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class CommentBase:EntityBase, IEquatable<CommentBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("Commentid",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 Commentid{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isactive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Isactive{ get; set; }

		[FieldNameAttribute("CommentTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CommentTypeId{ get; set; }

		[FieldNameAttribute("Guid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Guid{ get; set; }

		[FieldNameAttribute("ReporterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ReporterId{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResourceGuid{ get; set; }

		[FieldNameAttribute("Comment",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Comment{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<CommentBase> Members

        public virtual bool Equals(CommentBase other)
        {
			if(this.Commentid==other.Commentid  && this.NewsGuid==other.NewsGuid  && this.UserId==other.UserId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Isactive==other.Isactive  && this.CommentTypeId==other.CommentTypeId  && this.Guid==other.Guid  && this.ReporterId==other.ReporterId  && this.ResourceGuid==other.ResourceGuid  && this.Comment==other.Comment )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Comment other)
        {
			if(other!=null)
			{
				this.Commentid=other.Commentid;
				this.NewsGuid=other.NewsGuid;
				this.UserId=other.UserId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Isactive=other.Isactive;
				this.CommentTypeId=other.CommentTypeId;
				this.Guid=other.Guid;
				this.ReporterId=other.ReporterId;
				this.ResourceGuid=other.ResourceGuid;
				this.Comment=other.Comment;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
