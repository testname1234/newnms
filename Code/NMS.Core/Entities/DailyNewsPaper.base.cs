﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class DailyNewsPaperBase:EntityBase, IEquatable<DailyNewsPaperBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("DailyNewsPaperId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DailyNewsPaperId{ get; set; }

		[FieldNameAttribute("NewsPaperId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsPaperId{ get; set; }

		[FieldNameAttribute("Date",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime Date{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string DateStr
		{
			 get { return Date.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Date = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<DailyNewsPaperBase> Members

        public virtual bool Equals(DailyNewsPaperBase other)
        {
			if(this.DailyNewsPaperId==other.DailyNewsPaperId  && this.NewsPaperId==other.NewsPaperId  && this.Date==other.Date  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(DailyNewsPaper other)
        {
			if(other!=null)
			{
				this.DailyNewsPaperId=other.DailyNewsPaperId;
				this.NewsPaperId=other.NewsPaperId;
				this.Date=other.Date;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
