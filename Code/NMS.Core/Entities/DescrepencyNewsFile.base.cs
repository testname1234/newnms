﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class DescrepencyNewsFileBase:EntityBase, IEquatable<DescrepencyNewsFileBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("DescrepencyNewsFileId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DescrepencyNewsFileId{ get; set; }

		[FieldNameAttribute("Title",true,false,200)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("Source",true,false,200)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Source{ get; set; }

		[FieldNameAttribute("DescrepencyValue",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DescrepencyValue{ get; set; }

		[FieldNameAttribute("DescrepencyType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? DescrepencyType{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("DiscrepencyStatusCode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? DiscrepencyStatusCode{ get; set; }

		[FieldNameAttribute("IsInserted",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsInserted{ get; set; }

		[FieldNameAttribute("RawNews",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String RawNews{ get; set; }

		[FieldNameAttribute("NewsFileId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsFileId{ get; set; }

		[FieldNameAttribute("CategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<DescrepencyNewsFileBase> Members

        public virtual bool Equals(DescrepencyNewsFileBase other)
        {
			if(this.DescrepencyNewsFileId==other.DescrepencyNewsFileId  && this.Title==other.Title  && this.Source==other.Source  && this.DescrepencyValue==other.DescrepencyValue  && this.DescrepencyType==other.DescrepencyType  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.DiscrepencyStatusCode==other.DiscrepencyStatusCode  && this.IsInserted==other.IsInserted  && this.RawNews==other.RawNews  && this.NewsFileId==other.NewsFileId  && this.CategoryId==other.CategoryId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(DescrepencyNewsFile other)
        {
			if(other!=null)
			{
				this.DescrepencyNewsFileId=other.DescrepencyNewsFileId;
				this.Title=other.Title;
				this.Source=other.Source;
				this.DescrepencyValue=other.DescrepencyValue;
				this.DescrepencyType=other.DescrepencyType;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.DiscrepencyStatusCode=other.DiscrepencyStatusCode;
				this.IsInserted=other.IsInserted;
				this.RawNews=other.RawNews;
				this.NewsFileId=other.NewsFileId;
				this.CategoryId=other.CategoryId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
