﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class EditorialCommentBase:EntityBase, IEquatable<EditorialCommentBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("EditorialCommentId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EditorialCommentId{ get; set; }

		[FieldNameAttribute("Comment",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Comment{ get; set; }

		[FieldNameAttribute("EditorId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EditorId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("SlotId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlotId{ get; set; }

		[FieldNameAttribute("TickerId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerId{ get; set; }

		[FieldNameAttribute("LanguageCode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LanguageCode{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<EditorialCommentBase> Members

        public virtual bool Equals(EditorialCommentBase other)
        {
			if(this.EditorialCommentId==other.EditorialCommentId  && this.Comment==other.Comment  && this.EditorId==other.EditorId  && this.NewsFileId==other.NewsFileId  && this.IsActive==other.IsActive  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.SlotId==other.SlotId  && this.TickerId==other.TickerId  && this.LanguageCode==other.LanguageCode )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(EditorialComment other)
        {
			if(other!=null)
			{
				this.EditorialCommentId=other.EditorialCommentId;
				this.Comment=other.Comment;
				this.EditorId=other.EditorId;
				this.NewsFileId=other.NewsFileId;
				this.IsActive=other.IsActive;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.SlotId=other.SlotId;
				this.TickerId=other.TickerId;
				this.LanguageCode=other.LanguageCode;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
