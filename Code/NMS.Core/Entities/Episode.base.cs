﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class EpisodeBase:EntityBase, IEquatable<EpisodeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("EpisodeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EpisodeId{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("From",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime From{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string FromStr
		{
			 get { return From.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("To",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime To{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string ToStr
		{
			 get { return To.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("PreviewGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? PreviewGuid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<EpisodeBase> Members

        public virtual bool Equals(EpisodeBase other)
        {
			if(this.EpisodeId==other.EpisodeId  && this.ProgramId==other.ProgramId  && this.From==other.From  && this.To==other.To  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.PreviewGuid==other.PreviewGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Episode other)
        {
			if(other!=null)
			{
				this.EpisodeId=other.EpisodeId;
				this.ProgramId=other.ProgramId;
				this.From=other.From;
				this.To=other.To;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.PreviewGuid=other.PreviewGuid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
