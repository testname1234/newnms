﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class Episode : EpisodeBase 
	{
        public List<Segment> Segments { get; set; }


        public string ProgramName { get; set; }
    }
}
