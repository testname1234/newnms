﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class EpisodePcrMosBase:EntityBase, IEquatable<EpisodePcrMosBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("EpisodePcrMosId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EpisodePcrMosId{ get; set; }

		[FieldNameAttribute("ItemName",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ItemName{ get; set; }

		[FieldNameAttribute("Type",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		[FieldNameAttribute("Label",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Label{ get; set; }

		[FieldNameAttribute("Name",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Channel",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Channel{ get; set; }

		[FieldNameAttribute("Videolayer",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Videolayer{ get; set; }

		[FieldNameAttribute("Devicename",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Devicename{ get; set; }

		[FieldNameAttribute("Delay",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Delay{ get; set; }

		[FieldNameAttribute("Duration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Duration{ get; set; }

		[FieldNameAttribute("Allowgpi",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Allowgpi{ get; set; }

		[FieldNameAttribute("Allowremotetriggering",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Allowremotetriggering{ get; set; }

		[FieldNameAttribute("Remotetriggerid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Remotetriggerid{ get; set; }

		[FieldNameAttribute("Flashlayer",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Flashlayer{ get; set; }

		[FieldNameAttribute("Invoke",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Invoke{ get; set; }

		[FieldNameAttribute("Usestoreddata",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Usestoreddata{ get; set; }

		[FieldNameAttribute("Useuppercasedata",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Useuppercasedata{ get; set; }

		[FieldNameAttribute("Color",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Color{ get; set; }

		[FieldNameAttribute("Transition",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Transition{ get; set; }

		[FieldNameAttribute("Transitionduration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Transitionduration{ get; set; }

		[FieldNameAttribute("Tween",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Tween{ get; set; }

		[FieldNameAttribute("Direction",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Direction{ get; set; }

		[FieldNameAttribute("Seek",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Seek{ get; set; }

		[FieldNameAttribute("Length",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Length{ get; set; }

		[FieldNameAttribute("Loop",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Loop{ get; set; }

		[FieldNameAttribute("Freezeonload",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Freezeonload{ get; set; }

		[FieldNameAttribute("Triggeronnext",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Triggeronnext{ get; set; }

		[FieldNameAttribute("Autoplay",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Autoplay{ get; set; }

		[FieldNameAttribute("Timecode",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Timecode{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("Defer",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Defer{ get; set; }

		[FieldNameAttribute("Device",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Device{ get; set; }

		[FieldNameAttribute("Format",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Format{ get; set; }

		[FieldNameAttribute("Showmask",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Showmask{ get; set; }

		[FieldNameAttribute("Blur",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Blur{ get; set; }

		[FieldNameAttribute("Key",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Key{ get; set; }

		[FieldNameAttribute("Spread",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Spread{ get; set; }

		[FieldNameAttribute("Spill",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Spill{ get; set; }

		[FieldNameAttribute("Threshold",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Threshold{ get; set; }

		[FieldNameAttribute("userType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserType{ get; set; }

		[FieldNameAttribute("casperTransformationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CasperTransformationId{ get; set; }

		[FieldNameAttribute("flashtemplateid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Flashtemplateid{ get; set; }

		[FieldNameAttribute("templateid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Templateid{ get; set; }

		[FieldNameAttribute("flashtemplatewindowid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Flashtemplatewindowid{ get; set; }

		[FieldNameAttribute("FlashTemplateTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashTemplateTypeId{ get; set; }

		[FieldNameAttribute("videoid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Videoid{ get; set; }

		[FieldNameAttribute("sequenceorder",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Sequenceorder{ get; set; }

		[FieldNameAttribute("casperTemplateId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CasperTemplateId{ get; set; }

		[FieldNameAttribute("CasperTemplatItemId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CasperTemplatItemId{ get; set; }

		[FieldNameAttribute("parentid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Parentid{ get; set; }

		[FieldNameAttribute("url",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		[FieldNameAttribute("portnumber",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Portnumber{ get; set; }

		[FieldNameAttribute("flashTemplateGuid",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FlashTemplateGuid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<EpisodePcrMosBase> Members

        public virtual bool Equals(EpisodePcrMosBase other)
        {
			if(this.EpisodePcrMosId==other.EpisodePcrMosId  && this.ItemName==other.ItemName  && this.Type==other.Type  && this.Label==other.Label  && this.Name==other.Name  && this.Channel==other.Channel  && this.Videolayer==other.Videolayer  && this.Devicename==other.Devicename  && this.Delay==other.Delay  && this.Duration==other.Duration  && this.Allowgpi==other.Allowgpi  && this.Allowremotetriggering==other.Allowremotetriggering  && this.Remotetriggerid==other.Remotetriggerid  && this.Flashlayer==other.Flashlayer  && this.Invoke==other.Invoke  && this.Usestoreddata==other.Usestoreddata  && this.Useuppercasedata==other.Useuppercasedata  && this.Color==other.Color  && this.Transition==other.Transition  && this.Transitionduration==other.Transitionduration  && this.Tween==other.Tween  && this.Direction==other.Direction  && this.Seek==other.Seek  && this.Length==other.Length  && this.Loop==other.Loop  && this.Freezeonload==other.Freezeonload  && this.Triggeronnext==other.Triggeronnext  && this.Autoplay==other.Autoplay  && this.Timecode==other.Timecode  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Defer==other.Defer  && this.Device==other.Device  && this.Format==other.Format  && this.Showmask==other.Showmask  && this.Blur==other.Blur  && this.Key==other.Key  && this.Spread==other.Spread  && this.Spill==other.Spill  && this.Threshold==other.Threshold  && this.UserType==other.UserType  && this.CasperTransformationId==other.CasperTransformationId  && this.Flashtemplateid==other.Flashtemplateid  && this.Templateid==other.Templateid  && this.Flashtemplatewindowid==other.Flashtemplatewindowid  && this.FlashTemplateTypeId==other.FlashTemplateTypeId  && this.Videoid==other.Videoid  && this.Sequenceorder==other.Sequenceorder  && this.CasperTemplateId==other.CasperTemplateId  && this.CasperTemplatItemId==other.CasperTemplatItemId  && this.Parentid==other.Parentid  && this.Url==other.Url  && this.Portnumber==other.Portnumber  && this.FlashTemplateGuid==other.FlashTemplateGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(EpisodePcrMos other)
        {
			if(other!=null)
			{
				this.EpisodePcrMosId=other.EpisodePcrMosId;
				this.ItemName=other.ItemName;
				this.Type=other.Type;
				this.Label=other.Label;
				this.Name=other.Name;
				this.Channel=other.Channel;
				this.Videolayer=other.Videolayer;
				this.Devicename=other.Devicename;
				this.Delay=other.Delay;
				this.Duration=other.Duration;
				this.Allowgpi=other.Allowgpi;
				this.Allowremotetriggering=other.Allowremotetriggering;
				this.Remotetriggerid=other.Remotetriggerid;
				this.Flashlayer=other.Flashlayer;
				this.Invoke=other.Invoke;
				this.Usestoreddata=other.Usestoreddata;
				this.Useuppercasedata=other.Useuppercasedata;
				this.Color=other.Color;
				this.Transition=other.Transition;
				this.Transitionduration=other.Transitionduration;
				this.Tween=other.Tween;
				this.Direction=other.Direction;
				this.Seek=other.Seek;
				this.Length=other.Length;
				this.Loop=other.Loop;
				this.Freezeonload=other.Freezeonload;
				this.Triggeronnext=other.Triggeronnext;
				this.Autoplay=other.Autoplay;
				this.Timecode=other.Timecode;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Defer=other.Defer;
				this.Device=other.Device;
				this.Format=other.Format;
				this.Showmask=other.Showmask;
				this.Blur=other.Blur;
				this.Key=other.Key;
				this.Spread=other.Spread;
				this.Spill=other.Spill;
				this.Threshold=other.Threshold;
				this.UserType=other.UserType;
				this.CasperTransformationId=other.CasperTransformationId;
				this.Flashtemplateid=other.Flashtemplateid;
				this.Templateid=other.Templateid;
				this.Flashtemplatewindowid=other.Flashtemplatewindowid;
				this.FlashTemplateTypeId=other.FlashTemplateTypeId;
				this.Videoid=other.Videoid;
				this.Sequenceorder=other.Sequenceorder;
				this.CasperTemplateId=other.CasperTemplateId;
				this.CasperTemplatItemId=other.CasperTemplatItemId;
				this.Parentid=other.Parentid;
				this.Url=other.Url;
				this.Portnumber=other.Portnumber;
				this.FlashTemplateGuid=other.FlashTemplateGuid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
