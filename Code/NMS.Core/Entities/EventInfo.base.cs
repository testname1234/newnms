﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class EventInfoBase:EntityBase, IEquatable<EventInfoBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("EventInfoId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EventInfoId{ get; set; }

		[FieldNameAttribute("Name",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("StartTime",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string StartTimeStr
		{
			 get { return StartTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("EndTime",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime EndTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string EndTimeStr
		{
			 get { return EndTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EndTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("SearchTags",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SearchTags{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<EventInfoBase> Members

        public virtual bool Equals(EventInfoBase other)
        {
			if(this.EventInfoId==other.EventInfoId  && this.Name==other.Name  && this.StartTime==other.StartTime  && this.EndTime==other.EndTime  && this.SearchTags==other.SearchTags  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(EventInfo other)
        {
			if(other!=null)
			{
				this.EventInfoId=other.EventInfoId;
				this.Name=other.Name;
				this.StartTime=other.StartTime;
				this.EndTime=other.EndTime;
				this.SearchTags=other.SearchTags;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
