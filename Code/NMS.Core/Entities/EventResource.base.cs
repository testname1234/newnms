﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class EventResourceBase:EntityBase, IEquatable<EventResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("EventResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EventResourceId{ get; set; }

		[FieldNameAttribute("EventResourceType",false,false,4)]
		[DataMember (EmitDefaultValue=true)]
		public virtual System.Int32 EventResourceType{ get; set; }

		[FieldNameAttribute("NewsFileId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsFileId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<EventResourceBase> Members

        public virtual bool Equals(EventResourceBase other)
        {
			if(this.EventResourceId==other.EventResourceId  && this.EventResourceType==other.EventResourceType  && this.NewsFileId==other.NewsFileId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(EventResource other)
        {
			if(other!=null)
			{
				this.EventResourceId=other.EventResourceId;
				this.EventResourceType=other.EventResourceType;
				this.NewsFileId=other.NewsFileId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
