﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FacebookAccountBase:EntityBase, IEquatable<FacebookAccountBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FacebookAccountId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FacebookAccountId{ get; set; }

		[FieldNameAttribute("UserName",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String UserName{ get; set; }

		[FieldNameAttribute("CelebrityId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		[FieldNameAttribute("Url",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("IsFollowed",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsFollowed{ get; set; }

		[FieldNameAttribute("Type",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FacebookAccountBase> Members

        public virtual bool Equals(FacebookAccountBase other)
        {
			if(this.FacebookAccountId==other.FacebookAccountId  && this.UserName==other.UserName  && this.CelebrityId==other.CelebrityId  && this.Url==other.Url  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsFollowed==other.IsFollowed  && this.Type==other.Type )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FacebookAccount other)
        {
			if(other!=null)
			{
				this.FacebookAccountId=other.FacebookAccountId;
				this.UserName=other.UserName;
				this.CelebrityId=other.CelebrityId;
				this.Url=other.Url;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsFollowed=other.IsFollowed;
				this.Type=other.Type;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
