﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FacebookCredentialBase:EntityBase, IEquatable<FacebookCredentialBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FacebookCredentialID",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FacebookCredentialId{ get; set; }

		[FieldNameAttribute("ClientID",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ClientId{ get; set; }

		[FieldNameAttribute("ClientSecret",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ClientSecret{ get; set; }

		[FieldNameAttribute("AccessToken",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String AccessToken{ get; set; }

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FacebookCredentialBase> Members

        public virtual bool Equals(FacebookCredentialBase other)
        {
			if(this.FacebookCredentialId==other.FacebookCredentialId  && this.ClientId==other.ClientId  && this.ClientSecret==other.ClientSecret  && this.AccessToken==other.AccessToken  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FacebookCredential other)
        {
			if(other!=null)
			{
				this.FacebookCredentialId=other.FacebookCredentialId;
				this.ClientId=other.ClientId;
				this.ClientSecret=other.ClientSecret;
				this.AccessToken=other.AccessToken;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
