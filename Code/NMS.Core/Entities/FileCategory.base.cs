﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FileCategoryBase:EntityBase, IEquatable<FileCategoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FileCategoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FileCategoryId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("CategoryId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CategoryId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FileCategoryBase> Members

        public virtual bool Equals(FileCategoryBase other)
        {
			if(this.FileCategoryId==other.FileCategoryId  && this.NewsFileId==other.NewsFileId  && this.CategoryId==other.CategoryId  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FileCategory other)
        {
			if(other!=null)
			{
				this.FileCategoryId=other.FileCategoryId;
				this.NewsFileId=other.NewsFileId;
				this.CategoryId=other.CategoryId;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
