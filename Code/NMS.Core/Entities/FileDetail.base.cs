﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FileDetailBase:EntityBase, IEquatable<FileDetailBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FileDetailId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FileDetailId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("Text",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Text{ get; set; }

		[FieldNameAttribute("CreatedBy",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CreatedBy{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("ReportedBy",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ReportedBy{ get; set; }

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Slug",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("CGValues",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String CgValues{ get; set; }

		[FieldNameAttribute("SlugId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlugId{ get; set; }

		[FieldNameAttribute("Title",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("NewsDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? NewsDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string NewsDateStr
		{
			 get {if(NewsDate.HasValue) return NewsDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NewsDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("DescriptionText",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DescriptionText{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FileDetailBase> Members

        public virtual bool Equals(FileDetailBase other)
        {
			if(this.FileDetailId==other.FileDetailId  && this.NewsFileId==other.NewsFileId  && this.Text==other.Text  && this.CreationDate==other.CreationDate  && this.ReportedBy==other.ReportedBy  && this.LastUpdateDate==other.LastUpdateDate  && this.Slug==other.Slug  && this.CgValues==other.CgValues  && this.SlugId==other.SlugId  && this.Title==other.Title  && this.NewsDate==other.NewsDate  && this.DescriptionText==other.DescriptionText )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FileDetail other)
        {
			if(other!=null)
			{
				this.FileDetailId=other.FileDetailId;
				this.NewsFileId=other.NewsFileId;
				this.Text=other.Text;
				this.CreatedBy=other.CreatedBy;
				this.CreationDate=other.CreationDate;
				this.ReportedBy=other.ReportedBy;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Slug=other.Slug;
				this.CgValues=other.CgValues;
				this.SlugId=other.SlugId;
				this.Title=other.Title;
				this.NewsDate=other.NewsDate;
				this.DescriptionText=other.DescriptionText;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
