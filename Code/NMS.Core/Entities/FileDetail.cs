﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class FileDetail : FileDetailBase 
	{

        [FieldNameAttribute("ProgramId", false, true, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ProgramId { get; set; }

    }
}
