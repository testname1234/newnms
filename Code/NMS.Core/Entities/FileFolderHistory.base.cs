﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FileFolderHistoryBase:EntityBase, IEquatable<FileFolderHistoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FileFolderHistoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FileFolderHistoryId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("UserId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		[FieldNameAttribute("FolderId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FolderId{ get; set; }

		[FieldNameAttribute("StatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FileFolderHistoryBase> Members

        public virtual bool Equals(FileFolderHistoryBase other)
        {
			if(this.FileFolderHistoryId==other.FileFolderHistoryId  && this.NewsFileId==other.NewsFileId  && this.UserId==other.UserId  && this.FolderId==other.FolderId  && this.StatusId==other.StatusId  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FileFolderHistory other)
        {
			if(other!=null)
			{
				this.FileFolderHistoryId=other.FileFolderHistoryId;
				this.NewsFileId=other.NewsFileId;
				this.UserId=other.UserId;
				this.FolderId=other.FolderId;
				this.StatusId=other.StatusId;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
