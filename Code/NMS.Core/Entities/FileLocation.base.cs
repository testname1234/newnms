﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FileLocationBase:EntityBase, IEquatable<FileLocationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FileLocationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FileLocationId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("LocationId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 LocationId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FileLocationBase> Members

        public virtual bool Equals(FileLocationBase other)
        {
			if(this.FileLocationId==other.FileLocationId  && this.NewsFileId==other.NewsFileId  && this.LocationId==other.LocationId  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FileLocation other)
        {
			if(other!=null)
			{
				this.FileLocationId=other.FileLocationId;
				this.NewsFileId=other.NewsFileId;
				this.LocationId=other.LocationId;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
