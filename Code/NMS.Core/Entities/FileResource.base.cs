﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FileResourceBase:EntityBase, IEquatable<FileResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FileResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FileResourceId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("ResourceTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceTypeId{ get; set; }

		[FieldNameAttribute("Guid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid Guid{ get; set; }

		[FieldNameAttribute("Caption",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Caption{ get; set; }

		[FieldNameAttribute("Duration",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Duration{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("UserId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FileResourceBase> Members

        public virtual bool Equals(FileResourceBase other)
        {
			if(this.FileResourceId==other.FileResourceId  && this.NewsFileId==other.NewsFileId  && this.ResourceTypeId==other.ResourceTypeId  && this.Guid==other.Guid  && this.Caption==other.Caption  && this.Duration==other.Duration  && this.CreationDate==other.CreationDate  && this.UserId==other.UserId  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FileResource other)
        {
			if(other!=null)
			{
				this.FileResourceId=other.FileResourceId;
				this.NewsFileId=other.NewsFileId;
				this.ResourceTypeId=other.ResourceTypeId;
				this.Guid=other.Guid;
				this.Caption=other.Caption;
				this.Duration=other.Duration;
				this.CreationDate=other.CreationDate;
				this.UserId=other.UserId;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
