﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FileTagBase:EntityBase, IEquatable<FileTagBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FileTagId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FileTagId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("TagId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TagId{ get; set; }

		[FieldNameAttribute("Rank",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Rank{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FileTagBase> Members

        public virtual bool Equals(FileTagBase other)
        {
			if(this.FileTagId==other.FileTagId  && this.NewsFileId==other.NewsFileId  && this.TagId==other.TagId  && this.Rank==other.Rank  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(FileTag other)
        {
			if(other!=null)
			{
				this.FileTagId=other.FileTagId;
				this.NewsFileId=other.NewsFileId;
				this.TagId=other.TagId;
				this.Rank=other.Rank;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
