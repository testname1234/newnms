﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FilterBase:EntityBase, IEquatable<FilterBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FilterId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FilterId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Value",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Value{ get; set; }

		[FieldNameAttribute("FilterTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FilterTypeId{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		[FieldNameAttribute("CssClass",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String CssClass{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("IsApproved",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsApproved{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FilterBase> Members

        public virtual bool Equals(FilterBase other)
        {
			if(this.FilterId==other.FilterId  && this.Name==other.Name  && this.Value==other.Value  && this.FilterTypeId==other.FilterTypeId  && this.ParentId==other.ParentId  && this.CssClass==other.CssClass  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsApproved==other.IsApproved )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Filter other)
        {
			if(other!=null)
			{
				this.FilterId=other.FilterId;
				this.Name=other.Name;
				this.Value=other.Value;
				this.FilterTypeId=other.FilterTypeId;
				this.ParentId=other.ParentId;
				this.CssClass=other.CssClass;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsApproved=other.IsApproved;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
