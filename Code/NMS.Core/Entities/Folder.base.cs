﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class FolderBase:EntityBase, IEquatable<FolderBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("FolderId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FolderId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("CategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		[FieldNameAttribute("LocationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("ParentFolderId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentFolderId{ get; set; }

		[FieldNameAttribute("IsRundown",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsRundown{ get; set; }

		[FieldNameAttribute("ProgramId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("IsPrivateFolder",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsPrivateFolder{ get; set; }

		[FieldNameAttribute("EpisodeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? EpisodeId{ get; set; }

		[FieldNameAttribute("StartTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string StartTimeStr
		{
			 get {if(StartTime.HasValue) return StartTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("EndTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? EndTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string EndTimeStr
		{
			 get {if(EndTime.HasValue) return EndTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EndTime = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<FolderBase> Members

        public virtual bool Equals(FolderBase other)
        {
			if(this.FolderId==other.FolderId  && this.Name==other.Name  && this.CategoryId==other.CategoryId  && this.LocationId==other.LocationId  && this.CreationDate==other.CreationDate  && this.ParentFolderId==other.ParentFolderId  && this.IsRundown==other.IsRundown  && this.ProgramId==other.ProgramId  && this.IsPrivateFolder==other.IsPrivateFolder  && this.EpisodeId==other.EpisodeId  && this.StartTime==other.StartTime  && this.EndTime==other.EndTime )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Folder other)
        {
			if(other!=null)
			{
				this.FolderId=other.FolderId;
				this.Name=other.Name;
				this.CategoryId=other.CategoryId;
				this.LocationId=other.LocationId;
				this.CreationDate=other.CreationDate;
				this.ParentFolderId=other.ParentFolderId;
				this.IsRundown=other.IsRundown;
				this.ProgramId=other.ProgramId;
				this.IsPrivateFolder=other.IsPrivateFolder;
				this.EpisodeId=other.EpisodeId;
				this.StartTime=other.StartTime;
				this.EndTime=other.EndTime;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
