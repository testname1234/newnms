﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class Folder : FolderBase 
	{
        public string Location { get; set; }
        public string Category { get; set; }
		
	}
}
