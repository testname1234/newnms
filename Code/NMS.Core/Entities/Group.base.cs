﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class GroupBase:EntityBase, IEquatable<GroupBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("GroupId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GroupId{ get; set; }

		[FieldNameAttribute("GroupName",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String GroupName{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<GroupBase> Members

        public virtual bool Equals(GroupBase other)
        {
			if(this.GroupId==other.GroupId  && this.GroupName==other.GroupName )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Group other)
        {
			if(other!=null)
			{
				this.GroupId=other.GroupId;
				this.GroupName=other.GroupName;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
