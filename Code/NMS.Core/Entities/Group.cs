﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace NMS.Core.Entities
{
    [DataContract]
	public partial class Group : GroupBase 
	{
        
        public List<int> UserIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> Users { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ClientSideMessageEntity> Messages { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Boolean isSelected = false;

        [DataMember(EmitDefaultValue = false)]
        public int NewCount = 0;

        [DataMember(EmitDefaultValue = false)]
        public int Scroll = 0;

		
	}

    public class ClientSideMessageEntity /// For Chat
    {
        public string from { get; set; }
        public string msg { get; set; }
        public DateTime dt { get; set; }
    }
}
