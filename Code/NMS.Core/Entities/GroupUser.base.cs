﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class GroupUserBase:EntityBase, IEquatable<GroupUserBase>
	{
			
		[FieldNameAttribute("GroupId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? GroupId{ get; set; }

		[FieldNameAttribute("UserID",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		[PrimaryKey]
		[FieldNameAttribute("GroupUserId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GroupUserId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<GroupUserBase> Members

        public virtual bool Equals(GroupUserBase other)
        {
			if(this.GroupId==other.GroupId  && this.UserId==other.UserId  && this.GroupUserId==other.GroupUserId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(GroupUser other)
        {
			if(other!=null)
			{
				this.GroupId=other.GroupId;
				this.UserId=other.UserId;
				this.GroupUserId=other.GroupUserId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
