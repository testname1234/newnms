﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class GuestBase:EntityBase, IEquatable<GuestBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("GuestId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GuestId{ get; set; }

		[FieldNameAttribute("SlotId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotId{ get; set; }

		[FieldNameAttribute("LocationType",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 LocationType{ get; set; }

		[FieldNameAttribute("Question",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Question{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("CelebrityId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<GuestBase> Members

        public virtual bool Equals(GuestBase other)
        {
			if(this.GuestId==other.GuestId  && this.SlotId==other.SlotId  && this.LocationType==other.LocationType  && this.Question==other.Question  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.CelebrityId==other.CelebrityId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Guest other)
        {
			if(other!=null)
			{
				this.GuestId=other.GuestId;
				this.SlotId=other.SlotId;
				this.LocationType=other.LocationType;
				this.Question=other.Question;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.CelebrityId=other.CelebrityId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
