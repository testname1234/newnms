﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
    public partial class Guest : GuestBase
    {

        [FieldNameAttribute("CelebrityName", true, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String CelebrityName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string guestName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string programName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime programDatetime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime guestStartTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime guestEndTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string medium { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int guestduration { get; set; }

        [DataMember(EmitDefaultValue = true)]
        public string guestContactNumber { get; set; }


    }
}
