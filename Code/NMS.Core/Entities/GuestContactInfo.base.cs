﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class GuestContactInfoBase:EntityBase, IEquatable<GuestContactInfoBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("GuestContactInfoId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GuestContactInfoId{ get; set; }

		[FieldNameAttribute("ContactInfoType",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ContactInfoType{ get; set; }

		[FieldNameAttribute("Value",false,false,200)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Value{ get; set; }

		[FieldNameAttribute("CelebrityId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<GuestContactInfoBase> Members

        public virtual bool Equals(GuestContactInfoBase other)
        {
			if(this.GuestContactInfoId==other.GuestContactInfoId  && this.ContactInfoType==other.ContactInfoType  && this.Value==other.Value  && this.CelebrityId==other.CelebrityId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(GuestContactInfo other)
        {
			if(other!=null)
			{
				this.GuestContactInfoId=other.GuestContactInfoId;
				this.ContactInfoType=other.ContactInfoType;
				this.Value=other.Value;
				this.CelebrityId=other.CelebrityId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
