﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class KeyValueBase:EntityBase, IEquatable<KeyValueBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("KeyValueId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 KeyValueId{ get; set; }

		[FieldNameAttribute("Key",true,false,225)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Key{ get; set; }

		[FieldNameAttribute("Value",true,false,2000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Value{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<KeyValueBase> Members

        public virtual bool Equals(KeyValueBase other)
        {
			if(this.KeyValueId==other.KeyValueId  && this.Key==other.Key  && this.Value==other.Value )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(KeyValue other)
        {
			if(other!=null)
			{
				this.KeyValueId=other.KeyValueId;
				this.Key=other.Key;
				this.Value=other.Value;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
