﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class LocationBase:EntityBase, IEquatable<LocationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("LocationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 LocationId{ get; set; }

		[FieldNameAttribute("Location",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Location{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("IsApproved",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsApproved{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<LocationBase> Members

        public virtual bool Equals(LocationBase other)
        {
			if(this.LocationId==other.LocationId  && this.Location==other.Location  && this.ParentId==other.ParentId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsApproved==other.IsApproved )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Location other)
        {
			if(other!=null)
			{
				this.LocationId=other.LocationId;
				this.Location=other.Location;
				this.ParentId=other.ParentId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsApproved=other.IsApproved;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
