﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class LocationAliasBase:EntityBase, IEquatable<LocationAliasBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("LocationAliasId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 LocationAliasId{ get; set; }

		[FieldNameAttribute("LocationId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 LocationId{ get; set; }

		[FieldNameAttribute("Alias",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Alias{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<LocationAliasBase> Members

        public virtual bool Equals(LocationAliasBase other)
        {
			if(this.LocationAliasId==other.LocationAliasId  && this.LocationId==other.LocationId  && this.Alias==other.Alias  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(LocationAlias other)
        {
			if(other!=null)
			{
				this.LocationAliasId=other.LocationAliasId;
				this.LocationId=other.LocationId;
				this.Alias=other.Alias;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
