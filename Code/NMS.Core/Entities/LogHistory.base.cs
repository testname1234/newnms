﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class LogHistoryBase:EntityBase, IEquatable<LogHistoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("LogHistoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 LogHistoryId{ get; set; }

		[FieldNameAttribute("TableId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TableId{ get; set; }

		[FieldNameAttribute("RefrenceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RefrenceId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,3)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.DateTime CreationDate{ get; set; }

		[FieldNameAttribute("Method",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Method{ get; set; }

		[FieldNameAttribute("UserId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		[FieldNameAttribute("UserIp",false,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String UserIp{ get; set; }

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("UserType",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String UserType{ get; set; }

		[FieldNameAttribute("EntityJson",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String EntityJson{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<LogHistoryBase> Members

        public virtual bool Equals(LogHistoryBase other)
        {
			if(this.LogHistoryId==other.LogHistoryId  && this.TableId==other.TableId  && this.RefrenceId==other.RefrenceId  && this.CreationDate==other.CreationDate  && this.Method==other.Method  && this.UserId==other.UserId  && this.UserIp==other.UserIp  && this.IsActive==other.IsActive  && this.UserType==other.UserType  && this.EntityJson==other.EntityJson )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(LogHistory other)
        {
			if(other!=null)
			{
				this.LogHistoryId=other.LogHistoryId;
				this.TableId=other.TableId;
				this.RefrenceId=other.RefrenceId;
				this.CreationDate=other.CreationDate;
				this.Method=other.Method;
				this.UserId=other.UserId;
				this.UserIp=other.UserIp;
				this.IsActive=other.IsActive;
				this.UserType=other.UserType;
				this.EntityJson=other.EntityJson;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
