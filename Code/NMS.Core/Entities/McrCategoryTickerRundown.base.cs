﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class McrCategoryTickerRundownBase:EntityBase, IEquatable<McrCategoryTickerRundownBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MCRCategoryTickerRundownId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrCategoryTickerRundownId{ get; set; }

		[FieldNameAttribute("TickerId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerId{ get; set; }

		[FieldNameAttribute("Text",false,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Text{ get; set; }

		[FieldNameAttribute("CategoryName",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String CategoryName{ get; set; }

		[FieldNameAttribute("CategoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CategoryId{ get; set; }

		[FieldNameAttribute("SequenceNumber",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceNumber{ get; set; }

		[FieldNameAttribute("LanguageCode",false,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("TickerLineId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerLineId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<McrCategoryTickerRundownBase> Members

        public virtual bool Equals(McrCategoryTickerRundownBase other)
        {
			if(this.McrCategoryTickerRundownId==other.McrCategoryTickerRundownId  && this.TickerId==other.TickerId  && this.Text==other.Text  && this.CategoryName==other.CategoryName  && this.CategoryId==other.CategoryId  && this.SequenceNumber==other.SequenceNumber  && this.LanguageCode==other.LanguageCode  && this.CreationDate==other.CreationDate  && this.TickerLineId==other.TickerLineId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(McrCategoryTickerRundown other)
        {
			if(other!=null)
			{
				this.McrCategoryTickerRundownId=other.McrCategoryTickerRundownId;
				this.TickerId=other.TickerId;
				this.Text=other.Text;
				this.CategoryName=other.CategoryName;
				this.CategoryId=other.CategoryId;
				this.SequenceNumber=other.SequenceNumber;
				this.LanguageCode=other.LanguageCode;
				this.CreationDate=other.CreationDate;
				this.TickerLineId=other.TickerLineId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
