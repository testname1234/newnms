﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class McrLatestTickerRundownBase:EntityBase, IEquatable<McrLatestTickerRundownBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MCRLatestTickerRundownId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrLatestTickerRundownId{ get; set; }

		[FieldNameAttribute("TickerId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerId{ get; set; }

		[FieldNameAttribute("Text",false,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Text{ get; set; }

		[FieldNameAttribute("SequenceNumber",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceNumber{ get; set; }

		[FieldNameAttribute("LanguageCode",false,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("TickerLineId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerLineId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<McrLatestTickerRundownBase> Members

        public virtual bool Equals(McrLatestTickerRundownBase other)
        {
			if(this.McrLatestTickerRundownId==other.McrLatestTickerRundownId  && this.TickerId==other.TickerId  && this.Text==other.Text  && this.SequenceNumber==other.SequenceNumber  && this.LanguageCode==other.LanguageCode  && this.CreationDate==other.CreationDate  && this.TickerLineId==other.TickerLineId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(McrLatestTickerRundown other)
        {
			if(other!=null)
			{
				this.McrLatestTickerRundownId=other.McrLatestTickerRundownId;
				this.TickerId=other.TickerId;
				this.Text=other.Text;
				this.SequenceNumber=other.SequenceNumber;
				this.LanguageCode=other.LanguageCode;
				this.CreationDate=other.CreationDate;
				this.TickerLineId=other.TickerLineId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
