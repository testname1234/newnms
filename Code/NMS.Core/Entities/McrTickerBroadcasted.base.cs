﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class McrTickerBroadcastedBase:EntityBase, IEquatable<McrTickerBroadcastedBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("McrTickerBroadcastedId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrTickerBroadcastedId{ get; set; }

		[FieldNameAttribute("Hour",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 Hour{ get; set; }

		[FieldNameAttribute("TickerCategoryId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerCategoryId{ get; set; }

		[FieldNameAttribute("Limit",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 Limit{ get; set; }

		[FieldNameAttribute("Broadcasted",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Broadcasted{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<McrTickerBroadcastedBase> Members

        public virtual bool Equals(McrTickerBroadcastedBase other)
        {
			if(this.McrTickerBroadcastedId==other.McrTickerBroadcastedId  && this.Hour==other.Hour  && this.TickerCategoryId==other.TickerCategoryId  && this.Limit==other.Limit  && this.Broadcasted==other.Broadcasted  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(McrTickerBroadcasted other)
        {
			if(other!=null)
			{
				this.McrTickerBroadcastedId=other.McrTickerBroadcastedId;
				this.Hour=other.Hour;
				this.TickerCategoryId=other.TickerCategoryId;
				this.Limit=other.Limit;
				this.Broadcasted=other.Broadcasted;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
