﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class McrTickerBroadcasted : McrTickerBroadcastedBase 
	{

        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }
		
	}
}
