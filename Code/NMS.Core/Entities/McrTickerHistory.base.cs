﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class McrTickerHistoryBase:EntityBase, IEquatable<McrTickerHistoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("McrTickerHistoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrTickerHistoryId{ get; set; }

		[FieldNameAttribute("TickerId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerId{ get; set; }

		[FieldNameAttribute("Text",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Text{ get; set; }

		[FieldNameAttribute("TickerCategoryId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerCategoryId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("McrTickerBroadcastedId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrTickerBroadcastedId{ get; set; }

		[FieldNameAttribute("Type",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<McrTickerHistoryBase> Members

        public virtual bool Equals(McrTickerHistoryBase other)
        {
			if(this.McrTickerHistoryId==other.McrTickerHistoryId  && this.TickerId==other.TickerId  && this.Text==other.Text  && this.TickerCategoryId==other.TickerCategoryId  && this.CreationDate==other.CreationDate  && this.McrTickerBroadcastedId==other.McrTickerBroadcastedId  && this.Type==other.Type )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(McrTickerHistory other)
        {
			if(other!=null)
			{
				this.McrTickerHistoryId=other.McrTickerHistoryId;
				this.TickerId=other.TickerId;
				this.Text=other.Text;
				this.TickerCategoryId=other.TickerCategoryId;
				this.CreationDate=other.CreationDate;
				this.McrTickerBroadcastedId=other.McrTickerBroadcastedId;
				this.Type=other.Type;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
