﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class McrTickerHistory : McrTickerHistoryBase 
	{
        [DataMember(EmitDefaultValue = false)]
        public string McrTickerCategoryName { get; set; }
    }
}
