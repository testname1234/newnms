﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class McrTickerHistoryLogBase:EntityBase, IEquatable<McrTickerHistoryLogBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("McrTickerHistoryLogId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrTickerHistoryLogId{ get; set; }

		[FieldNameAttribute("JsonObject",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String JsonObject{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<McrTickerHistoryLogBase> Members

        public virtual bool Equals(McrTickerHistoryLogBase other)
        {
			if(this.McrTickerHistoryLogId==other.McrTickerHistoryLogId  && this.JsonObject==other.JsonObject  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(McrTickerHistoryLog other)
        {
			if(other!=null)
			{
				this.McrTickerHistoryLogId=other.McrTickerHistoryLogId;
				this.JsonObject=other.JsonObject;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
