﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class McrTickerRundownStatusBase:EntityBase, IEquatable<McrTickerRundownStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MCRTickerRundownStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrTickerRundownStatusId{ get; set; }

		[FieldNameAttribute("StatusId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<McrTickerRundownStatusBase> Members

        public virtual bool Equals(McrTickerRundownStatusBase other)
        {
			if(this.McrTickerRundownStatusId==other.McrTickerRundownStatusId  && this.StatusId==other.StatusId  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(McrTickerRundownStatus other)
        {
			if(other!=null)
			{
				this.McrTickerRundownStatusId=other.McrTickerRundownStatusId;
				this.StatusId=other.StatusId;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
