﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class McrTickerStatusBase:EntityBase, IEquatable<McrTickerStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MCRTickerStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 McrTickerStatusId{ get; set; }

		[FieldNameAttribute("Status",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Status{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<McrTickerStatusBase> Members

        public virtual bool Equals(McrTickerStatusBase other)
        {
			if(this.McrTickerStatusId==other.McrTickerStatusId  && this.Status==other.Status )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(McrTickerStatus other)
        {
			if(other!=null)
			{
				this.McrTickerStatusId=other.McrTickerStatusId;
				this.Status=other.Status;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
