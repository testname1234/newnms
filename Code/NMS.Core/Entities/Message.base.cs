﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class MessageBase:EntityBase, IEquatable<MessageBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MessageId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MessageId{ get; set; }

		[FieldNameAttribute("Message",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Message{ get; set; }

		[FieldNameAttribute("To",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 To{ get; set; }

		[FieldNameAttribute("From",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 From{ get; set; }

		[FieldNameAttribute("IsRecieved",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsRecieved{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("SlotScreenTemplateId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlotScreenTemplateId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<MessageBase> Members

        public virtual bool Equals(MessageBase other)
        {
			if(this.MessageId==other.MessageId  && this.Message==other.Message  && this.To==other.To  && this.From==other.From  && this.IsRecieved==other.IsRecieved  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.SlotScreenTemplateId==other.SlotScreenTemplateId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Message other)
        {
			if(other!=null)
			{
				this.MessageId=other.MessageId;
				this.Message=other.Message;
				this.To=other.To;
				this.From=other.From;
				this.IsRecieved=other.IsRecieved;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.SlotScreenTemplateId=other.SlotScreenTemplateId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
