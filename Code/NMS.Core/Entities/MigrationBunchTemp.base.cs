﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class MigrationBunchTempBase:EntityBase, IEquatable<MigrationBunchTempBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MigrationBunchTempId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MigrationBunchTempId{ get; set; }

		[FieldNameAttribute("BunchGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BunchGuid{ get; set; }

		[FieldNameAttribute("CreatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreatedDateStr
		{
			 get {if(CreatedDate.HasValue) return CreatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatedDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<MigrationBunchTempBase> Members

        public virtual bool Equals(MigrationBunchTempBase other)
        {
			if(this.MigrationBunchTempId==other.MigrationBunchTempId  && this.BunchGuid==other.BunchGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(MigrationBunchTemp other)
        {
			if(other!=null)
			{
				this.MigrationBunchTempId=other.MigrationBunchTempId;
				this.BunchGuid=other.BunchGuid;
				this.CreatedDate=other.CreatedDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
