﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    [BsonIgnoreExtraElements]
    public class BaseEntity
    {
        public BaseEntity()
        {
            this._id = ObjectId.GenerateNewId().ToString();
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
    }
}
