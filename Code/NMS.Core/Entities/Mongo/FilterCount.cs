﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities.Mongo
{
    public class MFilterCount : BaseEntity
    {
        public int Count { get; set; }
        public int FilterId { get; set; }
        public DateTime LastUpdateDate { get; set; }
    }
}
