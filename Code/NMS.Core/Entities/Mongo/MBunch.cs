﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    [Serializable]
    [BsonIgnoreExtraElements]
    public class MBunch : BaseEntity
    {
        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }

        [BsonIgnoreIfNull]
        public List<MResource> Resources { get; set; }

        public int NewsCount { get; set; }
    }
}
