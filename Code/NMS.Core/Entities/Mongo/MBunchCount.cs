﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    public class MBunchCount
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string BunchId { get; set; }

        public int Count { get; set; }
          
        public DateTime LastUpdateDate { get; set; }
    }
}
