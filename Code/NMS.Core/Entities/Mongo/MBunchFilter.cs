﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    public class MBunchFilter : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string BunchId { get; set; }

        public int FilterId { get; set; }

        [BsonIgnoreIfNull]
        public List<MNews> NewsList { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }
    }
}
