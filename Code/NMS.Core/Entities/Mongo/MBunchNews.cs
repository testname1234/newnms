﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.Entities
{
    public class MBunchNews:BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string BunchId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string NewsId { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }

    }
}
