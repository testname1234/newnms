﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    public class MBunchTag : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string BunchId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string TagId { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }
    }
}
