﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities.Mongo
{
    public class MCategory : BaseEntity
    {
        public virtual int CategoryId { get; set; }

        public virtual string Category { get; set; }

        public virtual int? ParentId { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public bool IsApproved { get; set; }

        public bool IsActive { get; set; }
    }
}
