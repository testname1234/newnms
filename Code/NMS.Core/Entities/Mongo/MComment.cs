﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    public class MComment : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string NewsId { get; set; }

        public string Comment { get; set; }

        public int UserId { get; set; }

        public int ReporterId { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public int CommentTypeId { get; set; }

        public string ResourceGuid { get; set; }
    }
}
