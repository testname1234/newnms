﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MContent
    {
        public MNews1 News { get; set; }

        public string ImageUrls { get; set; }

        public List<string> VideoUrls { get; set; }

        public List<string> AudioUrls { get; set; }

    }
}
