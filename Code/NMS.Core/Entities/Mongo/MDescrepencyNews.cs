﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities.Mongo
{
    public class MDescrepencyNews : BaseEntity
    {
        public RawNews RawNewsO { get; set; }

        public int DescrepencyNewsFileId { get; set; }
        public int NewsFileId { get; set; }

        public int CategoryId { get; set; }

        public string RawNews { get; set; }

        public string Title { get; set; }

        public string Source { get; set; }

        public string DescrepencyValue { get; set; }

        public int DescrepencyType { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public int DiscrepencyStatusCode { get; set; }

        public bool IsInserted { get; set; }

        public string RawNewsTitle { get {   
                            if (RawNews != null)
                            {
                                JObject rss = JObject.Parse(RawNews);
                                return (string)rss["Title"];
                            }
                            else { return ""; }
                    
                    }   set { } }
        public string RawNewsSource { get
            {
                if (RawNews != null)
                {
                    JObject rss = JObject.Parse(RawNews);
                    return (string)rss["Source"];
                }
                else { return ""; }
            } set { } }
        public string RawNewsUrl {
            get
            {
                if (RawNews != null)
                {
                    JObject rss = JObject.Parse(RawNews);
                    return (string)rss["Url"];
                }
                else { return ""; }
            }
            set { }
        }
    }
}
