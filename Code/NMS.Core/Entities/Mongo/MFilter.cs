﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.Entities
{
     [BsonIgnoreExtraElements]
    public class MFilter : BaseEntity
    {
        public System.Int32 FilterId { get; set; }

        public System.String Name { get; set; }

        public System.Int32? Value { get; set; }

        public System.Int32 FilterTypeId { get; set; }

        public System.Int32? ParentId { get; set; }

        public System.String CssClass { get; set; }

        public System.DateTime CreationDate { get; set; }

        public System.DateTime LastUpdateDate { get; set; }

        public System.Boolean IsActive { get; set; }

        public System.Boolean IsApproved { get; set; }
    }
}
