﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    public class MLocation : BaseEntity
    {
        public virtual System.String Location { get; set; }

        public virtual System.DateTime CreationDate { get; set; }

        public virtual System.DateTime LastUpdateDate { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.Double)]
        public virtual System.Double Latitude { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.Double)]
        public virtual System.Double Longitude { get; set; }

        public virtual System.Boolean IsActive { get; set; }
    }
}
