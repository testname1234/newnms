﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NMS.Core.Entities.Mongo;
using NMS.Core.Helper;

namespace NMS.Core.Entities
{
    [Serializable]
    [BsonIgnoreExtraElements]
    public class MNews : BaseEntity
    {
        public MNews()
            : base()
        {
            IsSearchable = true;
        }
        public string Title { get; set; }

        private string _description;
        public string Description { get { return _description; } set { _description = value; ShortDescription = string.IsNullOrEmpty(value) ? null : value; } }

        public string Highlight { get; set; }
        private string _descriptionText;
        [BsonIgnore]
        public string DescriptionText { get; set; }

        [BsonIgnoreIfNull]
        public string ShortDescription { get; set; }

        public string TranslatedTitle { get; set; }

        public string TranslatedDescription { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime PublishTime { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public int SourceTypeId { get; set; }

        public int SourceFilterId { get; set; }

        [BsonIgnoreIfNull]
        public List<MNewsFilter> Filters { get; set; }

        [BsonIgnoreIfNull]
        public List<MTag> Tags { get; set; }

        [BsonIgnoreIfNull]
        public List<Location> Locations { get; set; }

        public System.String ResourceGuid { get; set; }

        public int Version { get; set; }

        private string _linkNewsId;

        [BsonRepresentation(BsonType.ObjectId)]
        public string LinkNewsId { get { return _linkNewsId; } set { _linkNewsId = value; IsSearchable = string.IsNullOrEmpty(value); } }

        private string _parentNewsId;

        [BsonRepresentation(BsonType.ObjectId)]
        public string ParentNewsId { get { return _parentNewsId; } set { _parentNewsId = value; IsSearchable = string.IsNullOrEmpty(value); } }

        public string ThumbnailUrl { get; set; }


        [BsonIgnoreIfNull]
        public List<MNews> Updates { get; set; }

        public string Author { get; set; }

        public int ReporterId { get; set; }

        public string LanguageCode { get; set; }

        public int NewsTypeId { get; set; }

        public bool? IsVerified { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string BunchGuid { get; set; }

        [BsonIgnoreIfNull]
        public List<Category> Categories { get; set; }

        [BsonIgnoreIfNull]
        public List<Organization> Organization { get; set; }

        [BsonIgnoreIfNull]
        public List<MResource> Resources { get; set; }

        [BsonIgnoreIfNull]
        public List<MComment> Comments { get; set; }

        public int ParentCategoryId { get; set; }

        public string ParentCategoryName { get; set; }

        public bool IsAired { get; set; }

        public bool IsSearchable { get; set; }

        public string Source { get; set; }

        [XmlIgnore]
        public NMS.Core.Models.ResourceEdit ResourceEdit { get; set; }

        public bool HasResourceEdit { get; set; }

        //Add Attribute for hasnoresource here
        public bool HasNoResource { get; set; }

        public bool IsCompleted { get; set; }

        public double Score { get; set; }
        public int AddedToRundownCount { get; set; }

        public int OnAirCount { get; set; }
        public int OtherChannelExecutionCount { get; set; }

        public int NewsTickerCount { get; set; }


        public List<MNewsStatistics> NewsStatistics { get; set; }

        public string Slug { get; set; }

        public int SlugId { get; set; }

        public string TranslatedSlug { get; set; }

        public List<int> NewsCameraMen { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ReferenceNewsId { get; set; }

        public int? EventType { get; set; }


        public MNews GetShortNews()
        {
            MNews news = new MNews();
            news.Title = this.Title;
            if (!string.IsNullOrEmpty(this.Description))
                news.ShortDescription = this.Description.Substring(0, 500);
            news.Resources = this.Resources;
            news.IsVerified = this.IsVerified;
            news.NewsTypeId = this.NewsTypeId;
            news.ParentNewsId = this.ParentNewsId;
            news.LastUpdateDate = this.LastUpdateDate;
            news.LanguageCode = this.LanguageCode;
            news.BunchGuid = this.BunchGuid;
            news.ThumbnailUrl = this.ThumbnailUrl;
            news.IsAired = this.IsAired;
            news.Resources = this.Resources;
            news.Updates = this.Updates;
            news.Filters = this.Filters;
            news.Categories = this.Categories;
            news.Source = this.Source;
            news.Comments = this.Comments;

            if (this.Categories != null)
            {
                news.ParentCategoryName = string.Join(", ", news.Categories.Where(x => !x.ParentId.HasValue).Select(x => x.Category).ToList());
            }
            return news;
        }

        public MNews GetAsUpdateNews()
        {
            MNews news = new MNews();
            news._id = this._id;
            news.BunchGuid = this.BunchGuid;
            news.Title = this.Title;
            news.TranslatedTitle = this.TranslatedTitle;
            news.LastUpdateDate = this.LastUpdateDate;
            return news;
        }

        public int? AssignedTo { get; set; }

        public int? ProgramId { get; set; }

        public List<EditorialComment> EditorialComments { get; set; }

        public List<EventReporter> EventReporter { get; set; }

        public List<EventResource> EventResource { get; set; }
        public virtual System.Int32? NewsStatus { get; set; }

        public bool? Coverage { get; set; }
        public string BureauLocation { get; set; }

        public string EventTypeName { get; set; }

        public int FilterTypeId { get; set; }
        
        public virtual System.Int32? ReporterBureauId { get; set; }

       
        public virtual System.Int32? TaggedBy { get; set; }
        
        public virtual System.Int32? InsertedBy { get; set; }
    }
}
