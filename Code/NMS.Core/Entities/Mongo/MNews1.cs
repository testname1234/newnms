﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NMS.Core.Enums;

namespace NMS.Core.Entities
{
    public class MNews1
    {        
        [DataMember(EmitDefaultValue = false)]
        public Guid Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Version { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Guid ParentNewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public NewsTypes NewsType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SubCategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NewsSourceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LanguageId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> Tags { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CityCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string StateCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CountryCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsTopNews { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsVerified { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> VerifiedBy { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsRecommended { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> RecommendedBy { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Stats { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Guid> RelatedNews { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Resources { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CategoryName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CityName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string StateName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CountryName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [IgnoreDataMember]
        public DateTime CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffffffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        [IgnoreDataMember]
        public DateTime LastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffffffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }
    }
}
