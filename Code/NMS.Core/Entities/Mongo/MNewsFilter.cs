﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
     [BsonIgnoreExtraElements]
    public class MNewsFilter : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string NewsId { get; set; }

        public int FilterId { get; set; }

        public int FilterTypeId { get; set; }

        public int ReporterId { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }
    }
}
