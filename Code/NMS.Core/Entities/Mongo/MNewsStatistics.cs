﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Entities.Mongo
{
    public class MNewsStatistics : BaseEntity
    {
        public string ChannelName { get; set; }
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public int EpisodeId { get; set; }
        public DateTime Time { get; set; }
        public int Type { get; set; }
    }
}
