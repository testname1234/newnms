﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MParsedNews
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public List<string> Categories { get; set; }
        public DateTime PublishTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public string Author { get; set; }
        public string Location { get; set; }
        public List<string> Images { get; set; }
    }
}
