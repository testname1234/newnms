﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.Entities
{
    [BsonIgnoreExtraElements]
    public class MResource : BaseEntity
    {
        public int ResourceTypeId { get; set; }

        public string Guid { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public double? Duration { get; set; }

        public string Caption { get; set; }

        public string Category { get; set; }

        public string Location { get; set; }

        public int IgnoreMeta { get; set; }
    }
}
