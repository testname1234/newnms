﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MRole
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
