﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MRoleFilter
    {
        public int RoleId { get; set; }

        public int FilterId { get; set; }
    }
}
