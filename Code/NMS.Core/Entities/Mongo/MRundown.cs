﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MRundown
    {

        public int ProgramId { get; set; }

        public List<MContent> Contents { get; set; }

    }
}
