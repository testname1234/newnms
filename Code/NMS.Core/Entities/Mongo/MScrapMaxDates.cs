﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    public class MScrapMaxDates : BaseEntity
    {
        public DateTime MaxUpdateDate { get; set; }

        public string SourceName { get; set; }        
    }
}
