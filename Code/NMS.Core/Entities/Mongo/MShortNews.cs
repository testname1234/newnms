﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MShortNews
    {
        public string Title { get; set; }

        public DateTime CreationDate { get; set; }

        public List<MFilter> Filters { get; set; }

        public string ThumbnailUrl { get; set; }
    }
}
