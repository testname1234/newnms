﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NMS.Core.Entities.Mongo
{
    public class MTag : BaseEntity
    {
        public string Tag { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.Double)]
        public double Rank { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdateDate { get; set; }
    }
}
