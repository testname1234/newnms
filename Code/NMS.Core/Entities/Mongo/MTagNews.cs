﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MTagNews
    {
        public string NewsId { get; set; }

        public string TagId { get; set; }
    }
}
