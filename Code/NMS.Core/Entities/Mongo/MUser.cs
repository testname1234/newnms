﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MUser
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public List<MRole> Roles { get; set; }

        public int UserTypeId { get; set; }
    }
}
