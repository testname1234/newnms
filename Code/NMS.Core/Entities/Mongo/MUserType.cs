﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Entities
{
    public class MUserType
    {
        public int UserTypeId { get; set; }

        public string Type { get; set; }
    }
}
