﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class MosActiveEpisodeBase:EntityBase, IEquatable<MosActiveEpisodeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MosActiveEpisodeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MosActiveEpisodeId{ get; set; }

		[FieldNameAttribute("EpisodeId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? EpisodeId{ get; set; }

		[FieldNameAttribute("StatusCode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StatusCode{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("PCRTelePropterStatus",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? PcrTelePropterStatus{ get; set; }

		[FieldNameAttribute("PCRPlayoutStatus",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? PcrPlayoutStatus{ get; set; }

		[FieldNameAttribute("PCRFourWindowStatus",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? PcrFourWindowStatus{ get; set; }

		[FieldNameAttribute("StatusDescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String StatusDescription{ get; set; }

		[FieldNameAttribute("PCRTelepropterJson",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String PcrTelepropterJson{ get; set; }

		[FieldNameAttribute("PCRPlayoutJson",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String PcrPlayoutJson{ get; set; }

		[FieldNameAttribute("PCRFourWindowJson",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String PcrFourWindowJson{ get; set; }

		[FieldNameAttribute("IsAcknowledged",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? IsAcknowledged{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<MosActiveEpisodeBase> Members

        public virtual bool Equals(MosActiveEpisodeBase other)
        {
			if(this.MosActiveEpisodeId==other.MosActiveEpisodeId  && this.EpisodeId==other.EpisodeId  && this.StatusCode==other.StatusCode  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.PcrTelePropterStatus==other.PcrTelePropterStatus  && this.PcrPlayoutStatus==other.PcrPlayoutStatus  && this.PcrFourWindowStatus==other.PcrFourWindowStatus  && this.StatusDescription==other.StatusDescription  && this.PcrTelepropterJson==other.PcrTelepropterJson  && this.PcrPlayoutJson==other.PcrPlayoutJson  && this.PcrFourWindowJson==other.PcrFourWindowJson  && this.IsAcknowledged==other.IsAcknowledged )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(MosActiveEpisode other)
        {
			if(other!=null)
			{
				this.MosActiveEpisodeId=other.MosActiveEpisodeId;
				this.EpisodeId=other.EpisodeId;
				this.StatusCode=other.StatusCode;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.PcrTelePropterStatus=other.PcrTelePropterStatus;
				this.PcrPlayoutStatus=other.PcrPlayoutStatus;
				this.PcrFourWindowStatus=other.PcrFourWindowStatus;
				this.StatusDescription=other.StatusDescription;
				this.PcrTelepropterJson=other.PcrTelepropterJson;
				this.PcrPlayoutJson=other.PcrPlayoutJson;
				this.PcrFourWindowJson=other.PcrFourWindowJson;
				this.IsAcknowledged=other.IsAcknowledged;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
