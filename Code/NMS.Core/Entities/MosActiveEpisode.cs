﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class MosActiveEpisode : MosActiveEpisodeBase 
	{
        public DateTime? EpisodeStartTime { get; set; }
        public string ProgramName { get; set; }        			
	}
}
