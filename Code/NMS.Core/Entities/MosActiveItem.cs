﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
    [XmlRoot(ElementName = "items")]
    [XmlType(TypeName = "item")]
    public partial class MosActiveItem  :MosActiveItemBase
    {

    }
}
