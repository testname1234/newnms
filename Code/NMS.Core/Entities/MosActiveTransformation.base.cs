﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class MosActiveTransformationBase:EntityBase, IEquatable<MosActiveTransformationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("MosActiveDetailId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MosActiveDetailId{ get; set; }

		[FieldNameAttribute("MosActiveItemId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? MosActiveItemId{ get; set; }

		[FieldNameAttribute("Type",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		[FieldNameAttribute("DeviceName",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DeviceName{ get; set; }

		[FieldNameAttribute("Label",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Label{ get; set; }

		[FieldNameAttribute("Name",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Channel",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Channel{ get; set; }

		[FieldNameAttribute("VideoLayer",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? VideoLayer{ get; set; }

		[FieldNameAttribute("Delay",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Delay{ get; set; }

		[FieldNameAttribute("Duration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Duration{ get; set; }

		[FieldNameAttribute("AllowGpi",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? AllowGpi{ get; set; }

		[FieldNameAttribute("AllowRemoteTriggering",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? AllowRemoteTriggering{ get; set; }

		[FieldNameAttribute("RemoteTriggerId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? RemoteTriggerId{ get; set; }

		[FieldNameAttribute("positionx",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Positionx{ get; set; }

		[FieldNameAttribute("positiony",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Positiony{ get; set; }

		[FieldNameAttribute("scalex",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Scalex{ get; set; }

		[FieldNameAttribute("scaley",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Scaley{ get; set; }

		[FieldNameAttribute("transtitionDuration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TranstitionDuration{ get; set; }

		[FieldNameAttribute("defer",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Defer{ get; set; }

		[FieldNameAttribute("color",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Color{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<MosActiveTransformationBase> Members

        public virtual bool Equals(MosActiveTransformationBase other)
        {
			if(this.MosActiveDetailId==other.MosActiveDetailId  && this.MosActiveItemId==other.MosActiveItemId  && this.Type==other.Type  && this.DeviceName==other.DeviceName  && this.Label==other.Label  && this.Name==other.Name  && this.Channel==other.Channel  && this.VideoLayer==other.VideoLayer  && this.Delay==other.Delay  && this.Duration==other.Duration  && this.AllowGpi==other.AllowGpi  && this.AllowRemoteTriggering==other.AllowRemoteTriggering  && this.RemoteTriggerId==other.RemoteTriggerId  && this.Positionx==other.Positionx  && this.Positiony==other.Positiony  && this.Scalex==other.Scalex  && this.Scaley==other.Scaley  && this.TranstitionDuration==other.TranstitionDuration  && this.Defer==other.Defer  && this.Color==other.Color  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(MosActiveTransformation other)
        {
			if(other!=null)
			{
				this.MosActiveDetailId=other.MosActiveDetailId;
				this.MosActiveItemId=other.MosActiveItemId;
				this.Type=other.Type;
				this.DeviceName=other.DeviceName;
				this.Label=other.Label;
				this.Name=other.Name;
				this.Channel=other.Channel;
				this.VideoLayer=other.VideoLayer;
				this.Delay=other.Delay;
				this.Duration=other.Duration;
				this.AllowGpi=other.AllowGpi;
				this.AllowRemoteTriggering=other.AllowRemoteTriggering;
				this.RemoteTriggerId=other.RemoteTriggerId;
				this.Positionx=other.Positionx;
				this.Positiony=other.Positiony;
				this.Scalex=other.Scalex;
				this.Scaley=other.Scaley;
				this.TranstitionDuration=other.TranstitionDuration;
				this.Defer=other.Defer;
				this.Color=other.Color;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
