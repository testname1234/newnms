﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class MosItemBase:EntityBase, IEquatable<MosItemBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("CasperMosItemId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CasperMosItemId{ get; set; }

		[FieldNameAttribute("Type",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		[FieldNameAttribute("DeviceName",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DeviceName{ get; set; }

		[FieldNameAttribute("Label",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Label{ get; set; }

		[FieldNameAttribute("Name",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Channel",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Channel{ get; set; }

		[FieldNameAttribute("VideoLayer",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? VideoLayer{ get; set; }

		[FieldNameAttribute("Delay",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Delay{ get; set; }

		[FieldNameAttribute("Duration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Duration{ get; set; }

		[FieldNameAttribute("AllowGpi",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? AllowGpi{ get; set; }

		[FieldNameAttribute("AllowRemoteTriggering",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? AllowRemoteTriggering{ get; set; }

		[FieldNameAttribute("RemoteTriggerId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? RemoteTriggerId{ get; set; }

		[FieldNameAttribute("FlashLayer",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashLayer{ get; set; }

		[FieldNameAttribute("Invoke",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Invoke{ get; set; }

		[FieldNameAttribute("UseStoredData",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? UseStoredData{ get; set; }

		[FieldNameAttribute("useuppercasedata",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Useuppercasedata{ get; set; }

		[FieldNameAttribute("color",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Color{ get; set; }

		[FieldNameAttribute("transition",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Transition{ get; set; }

		[FieldNameAttribute("transitionDuration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TransitionDuration{ get; set; }

		[FieldNameAttribute("tween",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Tween{ get; set; }

		[FieldNameAttribute("direction",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Direction{ get; set; }

		[FieldNameAttribute("seek",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Seek{ get; set; }

		[FieldNameAttribute("length",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Length{ get; set; }

		[FieldNameAttribute("loop",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Loop{ get; set; }

		[FieldNameAttribute("freezeonload",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Freezeonload{ get; set; }

		[FieldNameAttribute("triggeronnext",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Triggeronnext{ get; set; }

		[FieldNameAttribute("autoplay",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Autoplay{ get; set; }

		[FieldNameAttribute("timecode",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Timecode{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("positionx",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Positionx{ get; set; }

		[FieldNameAttribute("positiony",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Positiony{ get; set; }

		[FieldNameAttribute("scalex",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Scalex{ get; set; }

		[FieldNameAttribute("scaley",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Scaley{ get; set; }

		[FieldNameAttribute("defer",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Defer{ get; set; }

		[FieldNameAttribute("device",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Device{ get; set; }

		[FieldNameAttribute("format",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Format{ get; set; }

		[FieldNameAttribute("showmask",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Showmask{ get; set; }

		[FieldNameAttribute("blur",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Blur{ get; set; }

		[FieldNameAttribute("key",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Key{ get; set; }

		[FieldNameAttribute("spread",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Spread{ get; set; }

		[FieldNameAttribute("spill",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Spill{ get; set; }

		[FieldNameAttribute("threshold",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Threshold{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<MosItemBase> Members

        public virtual bool Equals(MosItemBase other)
        {
			if(this.CasperMosItemId==other.CasperMosItemId  && this.Type==other.Type  && this.DeviceName==other.DeviceName  && this.Label==other.Label  && this.Name==other.Name  && this.Channel==other.Channel  && this.VideoLayer==other.VideoLayer  && this.Delay==other.Delay  && this.Duration==other.Duration  && this.AllowGpi==other.AllowGpi  && this.AllowRemoteTriggering==other.AllowRemoteTriggering  && this.RemoteTriggerId==other.RemoteTriggerId  && this.FlashLayer==other.FlashLayer  && this.Invoke==other.Invoke  && this.UseStoredData==other.UseStoredData  && this.Useuppercasedata==other.Useuppercasedata  && this.Color==other.Color  && this.Transition==other.Transition  && this.TransitionDuration==other.TransitionDuration  && this.Tween==other.Tween  && this.Direction==other.Direction  && this.Seek==other.Seek  && this.Length==other.Length  && this.Loop==other.Loop  && this.Freezeonload==other.Freezeonload  && this.Triggeronnext==other.Triggeronnext  && this.Autoplay==other.Autoplay  && this.Timecode==other.Timecode  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Positionx==other.Positionx  && this.Positiony==other.Positiony  && this.Scalex==other.Scalex  && this.Scaley==other.Scaley  && this.Defer==other.Defer  && this.Device==other.Device  && this.Format==other.Format  && this.Showmask==other.Showmask  && this.Blur==other.Blur  && this.Key==other.Key  && this.Spread==other.Spread  && this.Spill==other.Spill  && this.Threshold==other.Threshold )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(MosItem other)
        {
			if(other!=null)
			{
				this.CasperMosItemId=other.CasperMosItemId;
				this.Type=other.Type;
				this.DeviceName=other.DeviceName;
				this.Label=other.Label;
				this.Name=other.Name;
				this.Channel=other.Channel;
				this.VideoLayer=other.VideoLayer;
				this.Delay=other.Delay;
				this.Duration=other.Duration;
				this.AllowGpi=other.AllowGpi;
				this.AllowRemoteTriggering=other.AllowRemoteTriggering;
				this.RemoteTriggerId=other.RemoteTriggerId;
				this.FlashLayer=other.FlashLayer;
				this.Invoke=other.Invoke;
				this.UseStoredData=other.UseStoredData;
				this.Useuppercasedata=other.Useuppercasedata;
				this.Color=other.Color;
				this.Transition=other.Transition;
				this.TransitionDuration=other.TransitionDuration;
				this.Tween=other.Tween;
				this.Direction=other.Direction;
				this.Seek=other.Seek;
				this.Length=other.Length;
				this.Loop=other.Loop;
				this.Freezeonload=other.Freezeonload;
				this.Triggeronnext=other.Triggeronnext;
				this.Autoplay=other.Autoplay;
				this.Timecode=other.Timecode;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Positionx=other.Positionx;
				this.Positiony=other.Positiony;
				this.Scalex=other.Scalex;
				this.Scaley=other.Scaley;
				this.Defer=other.Defer;
				this.Device=other.Device;
				this.Format=other.Format;
				this.Showmask=other.Showmask;
				this.Blur=other.Blur;
				this.Key=other.Key;
				this.Spread=other.Spread;
				this.Spill=other.Spill;
				this.Threshold=other.Threshold;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
