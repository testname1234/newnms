﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsBase:EntityBase, IEquatable<NewsBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsId{ get; set; }

		[FieldNameAttribute("Guid",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Guid{ get; set; }

		[FieldNameAttribute("TranslatedDescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TranslatedDescription{ get; set; }

		[FieldNameAttribute("TranslatedTitle",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TranslatedTitle{ get; set; }

		[FieldNameAttribute("Title",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("BunchGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BunchGuid{ get; set; }

		[FieldNameAttribute("Description",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Description{ get; set; }

		[FieldNameAttribute("LocationId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("Author",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Author{ get; set; }

		[FieldNameAttribute("UpdateTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? UpdateTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string UpdateTimeStr
		{
			 get {if(UpdateTime.HasValue) return UpdateTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { UpdateTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LanguageCode",true,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("LinkNewsId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LinkNewsId{ get; set; }

		[FieldNameAttribute("ParentNewsId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentNewsId{ get; set; }

		[FieldNameAttribute("PreviousVersionId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? PreviousVersionId{ get; set; }

		[FieldNameAttribute("PublishTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? PublishTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string PublishTimeStr
		{
			 get {if(PublishTime.HasValue) return PublishTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { PublishTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("ThumbnailId",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ThumbnailId{ get; set; }

		[FieldNameAttribute("VersionNumber",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? VersionNumber{ get; set; }

		[FieldNameAttribute("IsIndexed",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsIndexed{ get; set; }

		[FieldNameAttribute("NewsTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsTypeId{ get; set; }

		[FieldNameAttribute("Source",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Source{ get; set; }

		[FieldNameAttribute("SourceTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceTypeId{ get; set; }

		[FieldNameAttribute("SourceNewsUrl",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SourceNewsUrl{ get; set; }

		[FieldNameAttribute("SourceFilterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceFilterId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("DescriptionText",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DescriptionText{ get; set; }

		[FieldNameAttribute("NewsDirection",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsDirection{ get; set; }

		[FieldNameAttribute("ShortDescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ShortDescription{ get; set; }

		[FieldNameAttribute("Version",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Version{ get; set; }

		[FieldNameAttribute("ReporterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ReporterId{ get; set; }

		[FieldNameAttribute("IsVerified",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsVerified{ get; set; }

		[FieldNameAttribute("IsAired",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsAired{ get; set; }

		[FieldNameAttribute("IsSearchable",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsSearchable{ get; set; }

		[FieldNameAttribute("HasResourceEdit",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? HasResourceEdit{ get; set; }

		[FieldNameAttribute("ReferenceNewsId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ReferenceNewsId{ get; set; }

		[FieldNameAttribute("IsCompleted",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsCompleted{ get; set; }

		[FieldNameAttribute("HasNoResource",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? HasNoResource{ get; set; }

		[FieldNameAttribute("AddedToRundownCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AddedToRundownCount{ get; set; }

		[FieldNameAttribute("OnAirCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OnAirCount{ get; set; }

		[FieldNameAttribute("OtherChannelExecutionCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OtherChannelExecutionCount{ get; set; }

		[FieldNameAttribute("NewsTickerCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsTickerCount{ get; set; }

		[FieldNameAttribute("ReferenceNewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ReferenceNewsGuid{ get; set; }

		[FieldNameAttribute("ParentNewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ParentNewsGuid{ get; set; }

		[FieldNameAttribute("IsArchival",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsArchival{ get; set; }

		[FieldNameAttribute("Slug",true,false,4000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("NewsPaperdescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsPaperdescription{ get; set; }

		[FieldNameAttribute("Suggestions",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Suggestions{ get; set; }

		[FieldNameAttribute("MigrationStatus",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? MigrationStatus{ get; set; }

		[FieldNameAttribute("TranslatedSlug",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TranslatedSlug{ get; set; }

		[FieldNameAttribute("SlugId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlugId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsBase> Members

        public virtual bool Equals(NewsBase other)
        {
			if(this.NewsId==other.NewsId  && this.Guid==other.Guid  && this.TranslatedDescription==other.TranslatedDescription  && this.TranslatedTitle==other.TranslatedTitle  && this.Title==other.Title  && this.BunchGuid==other.BunchGuid  && this.Description==other.Description  && this.LocationId==other.LocationId  && this.Author==other.Author  && this.UpdateTime==other.UpdateTime  && this.LanguageCode==other.LanguageCode  && this.LinkNewsId==other.LinkNewsId  && this.ParentNewsId==other.ParentNewsId  && this.PreviousVersionId==other.PreviousVersionId  && this.PublishTime==other.PublishTime  && this.ThumbnailId==other.ThumbnailId  && this.VersionNumber==other.VersionNumber  && this.IsIndexed==other.IsIndexed  && this.NewsTypeId==other.NewsTypeId  && this.Source==other.Source  && this.SourceTypeId==other.SourceTypeId  && this.SourceNewsUrl==other.SourceNewsUrl  && this.SourceFilterId==other.SourceFilterId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.DescriptionText==other.DescriptionText  && this.NewsDirection==other.NewsDirection  && this.ShortDescription==other.ShortDescription  && this.Version==other.Version  && this.ReporterId==other.ReporterId  && this.IsVerified==other.IsVerified  && this.IsAired==other.IsAired  && this.IsSearchable==other.IsSearchable  && this.HasResourceEdit==other.HasResourceEdit  && this.ReferenceNewsId==other.ReferenceNewsId  && this.IsCompleted==other.IsCompleted  && this.HasNoResource==other.HasNoResource  && this.AddedToRundownCount==other.AddedToRundownCount  && this.OnAirCount==other.OnAirCount  && this.OtherChannelExecutionCount==other.OtherChannelExecutionCount  && this.NewsTickerCount==other.NewsTickerCount  && this.ReferenceNewsGuid==other.ReferenceNewsGuid  && this.ParentNewsGuid==other.ParentNewsGuid  && this.IsArchival==other.IsArchival  && this.Slug==other.Slug  && this.NewsPaperdescription==other.NewsPaperdescription  && this.Suggestions==other.Suggestions  && this.MigrationStatus==other.MigrationStatus  && this.TranslatedSlug==other.TranslatedSlug  && this.SlugId==other.SlugId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(News other)
        {
			if(other!=null)
			{
				this.NewsId=other.NewsId;
				this.Guid=other.Guid;
				this.TranslatedDescription=other.TranslatedDescription;
				this.TranslatedTitle=other.TranslatedTitle;
				this.Title=other.Title;
				this.BunchGuid=other.BunchGuid;
				this.Description=other.Description;
				this.LocationId=other.LocationId;
				this.Author=other.Author;
				this.UpdateTime=other.UpdateTime;
				this.LanguageCode=other.LanguageCode;
				this.LinkNewsId=other.LinkNewsId;
				this.ParentNewsId=other.ParentNewsId;
				this.PreviousVersionId=other.PreviousVersionId;
				this.PublishTime=other.PublishTime;
				this.ThumbnailId=other.ThumbnailId;
				this.VersionNumber=other.VersionNumber;
				this.IsIndexed=other.IsIndexed;
				this.NewsTypeId=other.NewsTypeId;
				this.Source=other.Source;
				this.SourceTypeId=other.SourceTypeId;
				this.SourceNewsUrl=other.SourceNewsUrl;
				this.SourceFilterId=other.SourceFilterId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.DescriptionText=other.DescriptionText;
				this.NewsDirection=other.NewsDirection;
				this.ShortDescription=other.ShortDescription;
				this.Version=other.Version;
				this.ReporterId=other.ReporterId;
				this.IsVerified=other.IsVerified;
				this.IsAired=other.IsAired;
				this.IsSearchable=other.IsSearchable;
				this.HasResourceEdit=other.HasResourceEdit;
				this.ReferenceNewsId=other.ReferenceNewsId;
				this.IsCompleted=other.IsCompleted;
				this.HasNoResource=other.HasNoResource;
				this.AddedToRundownCount=other.AddedToRundownCount;
				this.OnAirCount=other.OnAirCount;
				this.OtherChannelExecutionCount=other.OtherChannelExecutionCount;
				this.NewsTickerCount=other.NewsTickerCount;
				this.ReferenceNewsGuid=other.ReferenceNewsGuid;
				this.ParentNewsGuid=other.ParentNewsGuid;
				this.IsArchival=other.IsArchival;
				this.Slug=other.Slug;
				this.NewsPaperdescription=other.NewsPaperdescription;
				this.Suggestions=other.Suggestions;
				this.MigrationStatus=other.MigrationStatus;
				this.TranslatedSlug=other.TranslatedSlug;
				this.SlugId=other.SlugId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
