﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class News : NewsBase 
	{
        string _guid ;
        string _thumburl;
        public virtual System.Int32 Score { get; set; }
        public virtual System.String _id { get { return Guid; } set{_guid=value;} }
        public virtual System.String ThumbnailUrl { get { return ThumbnailId; } set { _thumburl = value; } }
        public virtual System.String filters { get; set; }
        public virtual System.String tags { get; set; }
	}
}
