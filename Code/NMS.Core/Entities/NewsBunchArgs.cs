﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Entities
{
    public class NewsBunchArgs
    {
        public string key { get; set; }
        public List<Filter> filters { get; set; }
        public int pageCount { get; set; }
        public int startIndex { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public string term { get; set; }
        public List<Filter> discaredFilters { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
