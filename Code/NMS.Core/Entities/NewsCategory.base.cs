﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsCategoryBase:EntityBase, IEquatable<NewsCategoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsCategoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsCategoryId{ get; set; }

		[FieldNameAttribute("NewsId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsId{ get; set; }

		[FieldNameAttribute("CategoryId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CategoryId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsCategoryBase> Members

        public virtual bool Equals(NewsCategoryBase other)
        {
			if(this.NewsCategoryId==other.NewsCategoryId  && this.NewsId==other.NewsId  && this.CategoryId==other.CategoryId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.NewsGuid==other.NewsGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsCategory other)
        {
			if(other!=null)
			{
				this.NewsCategoryId=other.NewsCategoryId;
				this.NewsId=other.NewsId;
				this.CategoryId=other.CategoryId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.NewsGuid=other.NewsGuid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
