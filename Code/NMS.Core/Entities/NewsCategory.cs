﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class NewsCategory : NewsCategoryBase 
	{

        [FieldNameAttribute("Category", false, false, 255)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Category { get; set; }

        [FieldNameAttribute("ParentId", true, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? ParentId { get; set; }

        [FieldNameAttribute("IsApproved", true, false, 1)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean? IsApproved { get; set; }

        [FieldNameAttribute("CategoryInUrdu", true, false, 255)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String CategoryInUrdu { get; set; }
			
	}
}
