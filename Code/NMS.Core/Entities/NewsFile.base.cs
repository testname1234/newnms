﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsFileBase:EntityBase, IEquatable<NewsFileBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsFileId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("SequenceNo",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SequenceNo{ get; set; }

		[FieldNameAttribute("Slug",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("CreatedBy",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CreatedBy{ get; set; }

		[FieldNameAttribute("StatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		[FieldNameAttribute("FolderId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FolderId{ get; set; }

		[FieldNameAttribute("LocationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("CategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Title",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("NewsPaperDescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsPaperDescription{ get; set; }

		[FieldNameAttribute("LanguageCode",false,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("PublishTime",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime PublishTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string PublishTimeStr
		{
			 get { return PublishTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { PublishTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Source",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Source{ get; set; }

		[FieldNameAttribute("SourceTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceTypeId{ get; set; }

		[FieldNameAttribute("SourceNewsUrl",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SourceNewsUrl{ get; set; }

		[FieldNameAttribute("SourceFilterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceFilterId{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		[FieldNameAttribute("SlugId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlugId{ get; set; }

		[FieldNameAttribute("ProgramId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("AssignedTo",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AssignedTo{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResourceGuid{ get; set; }

		[FieldNameAttribute("NewsStatus",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsStatus{ get; set; }

		[FieldNameAttribute("Highlights",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Highlights{ get; set; }

		[FieldNameAttribute("SearchableText",true,false,4000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SearchableText{ get; set; }

		[FieldNameAttribute("BroadcastedCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? BroadcastedCount{ get; set; }

		[FieldNameAttribute("IsDeleted",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsDeleted{ get; set; }

		[FieldNameAttribute("IsVerified",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsVerified{ get; set; }

		[FieldNameAttribute("IsTagged",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsTagged{ get; set; }

		[FieldNameAttribute("EcCount",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EcCount{ get; set; }

		[FieldNameAttribute("EventType",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? EventType{ get; set; }

		[FieldNameAttribute("Coverage",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Coverage{ get; set; }

		[FieldNameAttribute("ReporterBureauId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ReporterBureauId{ get; set; }

		[FieldNameAttribute("TaggedBy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TaggedBy{ get; set; }

		[FieldNameAttribute("InsertedBy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? InsertedBy{ get; set; }

		[FieldNameAttribute("IsLive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsLive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsFileBase> Members

        public virtual bool Equals(NewsFileBase other)
        {
			if(this.NewsFileId==other.NewsFileId  && this.SequenceNo==other.SequenceNo  && this.Slug==other.Slug  && this.StatusId==other.StatusId  && this.FolderId==other.FolderId  && this.LocationId==other.LocationId  && this.CategoryId==other.CategoryId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Title==other.Title  && this.NewsPaperDescription==other.NewsPaperDescription  && this.LanguageCode==other.LanguageCode  && this.PublishTime==other.PublishTime  && this.Source==other.Source  && this.SourceTypeId==other.SourceTypeId  && this.SourceNewsUrl==other.SourceNewsUrl  && this.SourceFilterId==other.SourceFilterId  && this.ParentId==other.ParentId  && this.SlugId==other.SlugId  && this.ProgramId==other.ProgramId  && this.AssignedTo==other.AssignedTo  && this.ResourceGuid==other.ResourceGuid  && this.NewsStatus==other.NewsStatus  && this.Highlights==other.Highlights  && this.SearchableText==other.SearchableText  && this.BroadcastedCount==other.BroadcastedCount  && this.IsDeleted==other.IsDeleted  && this.IsVerified==other.IsVerified  && this.IsTagged==other.IsTagged  && this.EcCount==other.EcCount  && this.EventType==other.EventType  && this.Coverage==other.Coverage  && this.ReporterBureauId==other.ReporterBureauId  && this.TaggedBy==other.TaggedBy  && this.InsertedBy==other.InsertedBy  && this.IsLive==other.IsLive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsFile other)
        {
			if(other!=null)
			{
				this.NewsFileId=other.NewsFileId;
				this.SequenceNo=other.SequenceNo;
				this.Slug=other.Slug;
				this.CreatedBy=other.CreatedBy;
				this.StatusId=other.StatusId;
				this.FolderId=other.FolderId;
				this.LocationId=other.LocationId;
				this.CategoryId=other.CategoryId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Title=other.Title;
				this.NewsPaperDescription=other.NewsPaperDescription;
				this.LanguageCode=other.LanguageCode;
				this.PublishTime=other.PublishTime;
				this.Source=other.Source;
				this.SourceTypeId=other.SourceTypeId;
				this.SourceNewsUrl=other.SourceNewsUrl;
				this.SourceFilterId=other.SourceFilterId;
				this.ParentId=other.ParentId;
				this.SlugId=other.SlugId;
				this.ProgramId=other.ProgramId;
				this.AssignedTo=other.AssignedTo;
				this.ResourceGuid=other.ResourceGuid;
				this.NewsStatus=other.NewsStatus;
				this.Highlights=other.Highlights;
				this.SearchableText=other.SearchableText;
				this.BroadcastedCount=other.BroadcastedCount;
				this.IsDeleted=other.IsDeleted;
				this.IsVerified=other.IsVerified;
				this.IsTagged=other.IsTagged;
				this.EcCount=other.EcCount;
				this.EventType=other.EventType;
				this.Coverage=other.Coverage;
				this.ReporterBureauId=other.ReporterBureauId;
				this.TaggedBy=other.TaggedBy;
				this.InsertedBy=other.InsertedBy;
				this.IsLive=other.IsLive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
