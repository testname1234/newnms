﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace NMS.Core.Entities
{
    [DataContract]
    public partial class NewsFile : NewsFileBase
    {
        public string username { get; set; }

        public string Category { get; set; }
        public bool VoiceOver { get; set; }
        public bool IsTitle { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string IsLatestNews
        {
            get
            {
                TimeSpan diff = DateTime.UtcNow - CreationDate;
                if (diff.TotalMinutes <= 15)
                {
                    return "HighlightNews";
                }
                else
                {
                    return "No";
                }
            }
            set
            {
            }

        }

        [DataMember(EmitDefaultValue = false)]
        public List<FileDetail> FileDetails { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> Organization { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Organization> lstOrganization { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Category> lstCategory { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Location> lstLocation { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Tag> lstTag { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramInterval { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProgramName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime ProgramTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ChildProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ParentNewsId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool ChildNewsStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<FileResource> Resources { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime BroadcastDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BureauLocation { get; set; }

    }
}
