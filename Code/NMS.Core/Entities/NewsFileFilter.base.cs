﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsFileFilterBase:EntityBase, IEquatable<NewsFileFilterBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsFileFilterId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileFilterId{ get; set; }

		[FieldNameAttribute("FilterId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FilterId{ get; set; }

		[FieldNameAttribute("NewsFileId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsFileId{ get; set; }

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsFileFilterBase> Members

        public virtual bool Equals(NewsFileFilterBase other)
        {
			if(this.NewsFileFilterId==other.NewsFileFilterId  && this.FilterId==other.FilterId  && this.NewsFileId==other.NewsFileId  && this.IsActive==other.IsActive  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsFileFilter other)
        {
			if(other!=null)
			{
				this.NewsFileFilterId=other.NewsFileFilterId;
				this.FilterId=other.FilterId;
				this.NewsFileId=other.NewsFileId;
				this.IsActive=other.IsActive;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
