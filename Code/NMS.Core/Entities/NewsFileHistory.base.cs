﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsFileHistoryBase:EntityBase, IEquatable<NewsFileHistoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsFileHistoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileHistoryId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("NewsFileJson",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsFileJson{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsFileHistoryBase> Members

        public virtual bool Equals(NewsFileHistoryBase other)
        {
			if(this.NewsFileHistoryId==other.NewsFileHistoryId  && this.NewsFileId==other.NewsFileId  && this.NewsFileJson==other.NewsFileJson  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsFileHistory other)
        {
			if(other!=null)
			{
				this.NewsFileHistoryId=other.NewsFileHistoryId;
				this.NewsFileId=other.NewsFileId;
				this.NewsFileJson=other.NewsFileJson;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
