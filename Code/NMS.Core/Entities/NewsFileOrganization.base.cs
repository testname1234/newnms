﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsFileOrganizationBase:EntityBase, IEquatable<NewsFileOrganizationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsFileOrganizationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileOrganizationId{ get; set; }

		[FieldNameAttribute("OrganizationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 OrganizationId{ get; set; }

		[FieldNameAttribute("NewsFileId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFileId{ get; set; }

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsFileOrganizationBase> Members

        public virtual bool Equals(NewsFileOrganizationBase other)
        {
			if(this.NewsFileOrganizationId==other.NewsFileOrganizationId  && this.OrganizationId==other.OrganizationId  && this.NewsFileId==other.NewsFileId  && this.IsActive==other.IsActive  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsFileOrganization other)
        {
			if(other!=null)
			{
				this.NewsFileOrganizationId=other.NewsFileOrganizationId;
				this.OrganizationId=other.OrganizationId;
				this.NewsFileId=other.NewsFileId;
				this.IsActive=other.IsActive;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
