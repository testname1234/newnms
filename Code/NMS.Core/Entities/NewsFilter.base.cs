﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsFilterBase:EntityBase, IEquatable<NewsFilterBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsFilterId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsFilterId{ get; set; }

		[FieldNameAttribute("FilterId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FilterId{ get; set; }

		[FieldNameAttribute("NewsId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		[FieldNameAttribute("ReporterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ReporterId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsFilterBase> Members

        public virtual bool Equals(NewsFilterBase other)
        {
			if(this.NewsFilterId==other.NewsFilterId  && this.FilterId==other.FilterId  && this.NewsId==other.NewsId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.NewsGuid==other.NewsGuid  && this.ReporterId==other.ReporterId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsFilter other)
        {
			if(other!=null)
			{
				this.NewsFilterId=other.NewsFilterId;
				this.FilterId=other.FilterId;
				this.NewsId=other.NewsId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.NewsGuid=other.NewsGuid;
				this.ReporterId=other.ReporterId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
