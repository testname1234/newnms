﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsLocationBase:EntityBase, IEquatable<NewsLocationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsLocationid",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsLocationid{ get; set; }

		[FieldNameAttribute("NewsId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsId{ get; set; }

		[FieldNameAttribute("LocationId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isactive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Isactive{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

      

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsLocationBase> Members

        public virtual bool Equals(NewsLocationBase other)
        {
			if(this.NewsLocationid==other.NewsLocationid  && this.NewsId==other.NewsId  && this.LocationId==other.LocationId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Isactive==other.Isactive  && this.NewsGuid==other.NewsGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsLocation other)
        {
			if(other!=null)
			{
				this.NewsLocationid=other.NewsLocationid;
				this.NewsId=other.NewsId;
				this.LocationId=other.LocationId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Isactive=other.Isactive;
				this.NewsGuid=other.NewsGuid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
