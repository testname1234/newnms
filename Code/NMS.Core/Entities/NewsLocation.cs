﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class NewsLocation : NewsLocationBase 
	{
        [FieldNameAttribute("ParentId", true, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? ParentId { get; set; }

        [FieldNameAttribute("IsApproved", true, false, 1)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean? IsApproved { get; set; }

        [FieldNameAttribute("Location", true, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Location { get; set; }
		
	}
}
