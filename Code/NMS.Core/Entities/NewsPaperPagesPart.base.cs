﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsPaperPagesPartBase:EntityBase, IEquatable<NewsPaperPagesPartBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsPaperPagesPartId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsPaperPagesPartId{ get; set; }

		[FieldNameAttribute("NewsPaperPageId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsPaperPageId{ get; set; }

		[FieldNameAttribute("PartNumber",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? PartNumber{ get; set; }

		[FieldNameAttribute("Bottom",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Bottom{ get; set; }

		[FieldNameAttribute("Top",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Top{ get; set; }

		[FieldNameAttribute("Left",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Left{ get; set; }

		[FieldNameAttribute("Right",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Right{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsPaperPagesPartBase> Members

        public virtual bool Equals(NewsPaperPagesPartBase other)
        {
			if(this.NewsPaperPagesPartId==other.NewsPaperPagesPartId  && this.NewsPaperPageId==other.NewsPaperPageId  && this.PartNumber==other.PartNumber  && this.Bottom==other.Bottom  && this.Top==other.Top  && this.Left==other.Left  && this.Right==other.Right  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsPaperPagesPart other)
        {
			if(other!=null)
			{
				this.NewsPaperPagesPartId=other.NewsPaperPagesPartId;
				this.NewsPaperPageId=other.NewsPaperPageId;
				this.PartNumber=other.PartNumber;
				this.Bottom=other.Bottom;
				this.Top=other.Top;
				this.Left=other.Left;
				this.Right=other.Right;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
