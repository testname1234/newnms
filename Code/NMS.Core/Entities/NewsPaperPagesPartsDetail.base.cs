﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsPaperPagesPartsDetailBase:EntityBase, IEquatable<NewsPaperPagesPartsDetailBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsPaperPagesPartsDetailId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsPaperPagesPartsDetailId{ get; set; }

		[FieldNameAttribute("NewsPaperPagesPartId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsPaperPagesPartId{ get; set; }

		[FieldNameAttribute("ImageGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ImageGuid{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsPaperPagesPartsDetailBase> Members

        public virtual bool Equals(NewsPaperPagesPartsDetailBase other)
        {
			if(this.NewsPaperPagesPartsDetailId==other.NewsPaperPagesPartsDetailId  && this.NewsPaperPagesPartId==other.NewsPaperPagesPartId  && this.ImageGuid==other.ImageGuid  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsPaperPagesPartsDetail other)
        {
			if(other!=null)
			{
				this.NewsPaperPagesPartsDetailId=other.NewsPaperPagesPartsDetailId;
				this.NewsPaperPagesPartId=other.NewsPaperPagesPartId;
				this.ImageGuid=other.ImageGuid;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
