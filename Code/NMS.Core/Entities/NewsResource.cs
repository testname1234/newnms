﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class NewsResource : NewsResourceBase 
	{
        
        [FieldNameAttribute("Guid", true, false, 50)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Guid { get; set; }

        [FieldNameAttribute("ResourceTypeId", true, false, 50)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ResourceTypeId { get; set; }

        [FieldNameAttribute("Duration", true, false, 50)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? Duration { get; set; }

        [FieldNameAttribute("Caption", true, false, 50)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Caption { get; set; }

        [FieldNameAttribute("Category", true, false, 50)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Category { get; set; }

        [FieldNameAttribute("Location", true, false, 50)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Location { get; set; }


	
		
	}
}
