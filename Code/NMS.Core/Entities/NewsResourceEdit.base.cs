﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsResourceEditBase:EntityBase, IEquatable<NewsResourceEditBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsResourceEditId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsResourceEditId{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? ResourceGuid{ get; set; }

		[FieldNameAttribute("NewsId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsId{ get; set; }

		[FieldNameAttribute("ResourceTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ResourceTypeId{ get; set; }

		[FieldNameAttribute("FileName",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FileName{ get; set; }

		[FieldNameAttribute("Top",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Top{ get; set; }

		[FieldNameAttribute("Left",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Left{ get; set; }

		[FieldNameAttribute("Bottom",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Bottom{ get; set; }

		[FieldNameAttribute("Right",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Right{ get; set; }

		[FieldNameAttribute("Url",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isactive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Isactive{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		[FieldNameAttribute("ChannelVideoId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelVideoId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsResourceEditBase> Members

        public virtual bool Equals(NewsResourceEditBase other)
        {
			if(this.NewsResourceEditId==other.NewsResourceEditId  && this.ResourceGuid==other.ResourceGuid  && this.NewsId==other.NewsId  && this.ResourceTypeId==other.ResourceTypeId  && this.FileName==other.FileName  && this.Top==other.Top  && this.Left==other.Left  && this.Bottom==other.Bottom  && this.Right==other.Right  && this.Url==other.Url  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Isactive==other.Isactive  && this.NewsGuid==other.NewsGuid  && this.ChannelVideoId==other.ChannelVideoId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsResourceEdit other)
        {
			if(other!=null)
			{
				this.NewsResourceEditId=other.NewsResourceEditId;
				this.ResourceGuid=other.ResourceGuid;
				this.NewsId=other.NewsId;
				this.ResourceTypeId=other.ResourceTypeId;
				this.FileName=other.FileName;
				this.Top=other.Top;
				this.Left=other.Left;
				this.Bottom=other.Bottom;
				this.Right=other.Right;
				this.Url=other.Url;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Isactive=other.Isactive;
				this.NewsGuid=other.NewsGuid;
				this.ChannelVideoId=other.ChannelVideoId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
