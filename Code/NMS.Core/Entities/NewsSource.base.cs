﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsSourceBase:EntityBase, IEquatable<NewsSourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsSourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsSourceId{ get; set; }

		[FieldNameAttribute("Source",true,false,200)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Source{ get; set; }

		[FieldNameAttribute("FilterTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FilterTypeId{ get; set; }

		[FieldNameAttribute("FilterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FilterId{ get; set; }

		[FieldNameAttribute("CreatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreatedDateStr
		{
			 get {if(CreatedDate.HasValue) return CreatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateddate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateddate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateddateStr
		{
			 get {if(LastUpdateddate.HasValue) return LastUpdateddate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateddate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("languageCode",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("FolderPath",true,false,5000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String FolderPath{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsSourceBase> Members

        public virtual bool Equals(NewsSourceBase other)
        {
			if(this.NewsSourceId==other.NewsSourceId  && this.Source==other.Source  && this.FilterTypeId==other.FilterTypeId  && this.FilterId==other.FilterId  && this.LastUpdateddate==other.LastUpdateddate  && this.IsActive==other.IsActive  && this.LanguageCode==other.LanguageCode  && this.FolderPath==other.FolderPath )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsSource other)
        {
			if(other!=null)
			{
				this.NewsSourceId=other.NewsSourceId;
				this.Source=other.Source;
				this.FilterTypeId=other.FilterTypeId;
				this.FilterId=other.FilterId;
				this.CreatedDate=other.CreatedDate;
				this.LastUpdateddate=other.LastUpdateddate;
				this.IsActive=other.IsActive;
				this.LanguageCode=other.LanguageCode;
				this.FolderPath=other.FolderPath;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
