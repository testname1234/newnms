﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsStatisticsBase:EntityBase, IEquatable<NewsStatisticsBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsStatisticsId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsStatisticsId{ get; set; }

		[FieldNameAttribute("ChannelName",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ChannelName{ get; set; }

		[FieldNameAttribute("ProgramId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("ProgramName",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ProgramName{ get; set; }

		[FieldNameAttribute("EpisodeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? EpisodeId{ get; set; }

		[FieldNameAttribute("Time",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? Time{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string TimeStr
		{
			 get {if(Time.HasValue) return Time.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Time = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Type",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Type{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsStatisticsBase> Members

        public virtual bool Equals(NewsStatisticsBase other)
        {
			if(this.NewsStatisticsId==other.NewsStatisticsId  && this.ChannelName==other.ChannelName  && this.ProgramId==other.ProgramId  && this.ProgramName==other.ProgramName  && this.EpisodeId==other.EpisodeId  && this.Time==other.Time  && this.Type==other.Type  && this.NewsGuid==other.NewsGuid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsStatistics other)
        {
			if(other!=null)
			{
				this.NewsStatisticsId=other.NewsStatisticsId;
				this.ChannelName=other.ChannelName;
				this.ProgramId=other.ProgramId;
				this.ProgramName=other.ProgramName;
				this.EpisodeId=other.EpisodeId;
				this.Time=other.Time;
				this.Type=other.Type;
				this.NewsGuid=other.NewsGuid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
