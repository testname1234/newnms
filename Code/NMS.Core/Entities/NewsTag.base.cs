﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsTagBase:EntityBase, IEquatable<NewsTagBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsTagId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsTagId{ get; set; }

		[FieldNameAttribute("Rank",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Rank{ get; set; }

		[FieldNameAttribute("TagId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TagId{ get; set; }

		[FieldNameAttribute("NewsId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		[FieldNameAttribute("TagGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TagGuid{ get; set; }

		[FieldNameAttribute("Tag",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Tag{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsTagBase> Members

        public virtual bool Equals(NewsTagBase other)
        {
			if(this.NewsTagId==other.NewsTagId  && this.Rank==other.Rank  && this.TagId==other.TagId  && this.NewsId==other.NewsId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.NewsGuid==other.NewsGuid  && this.TagGuid==other.TagGuid  && this.Tag==other.Tag )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsTag other)
        {
			if(other!=null)
			{
				this.NewsTagId=other.NewsTagId;
				this.Rank=other.Rank;
				this.TagId=other.TagId;
				this.NewsId=other.NewsId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.NewsGuid=other.NewsGuid;
				this.TagGuid=other.TagGuid;
				this.Tag=other.Tag;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
