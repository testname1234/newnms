﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsTypeBase:EntityBase, IEquatable<NewsTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsTypeId{ get; set; }

		[FieldNameAttribute("NewsType",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsType{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsTypeBase> Members

        public virtual bool Equals(NewsTypeBase other)
        {
			if(this.NewsTypeId==other.NewsTypeId  && this.NewsType==other.NewsType )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsType other)
        {
			if(other!=null)
			{
				this.NewsTypeId=other.NewsTypeId;
				this.NewsType=other.NewsType;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
