﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NewsUpdateHistoryBase:EntityBase, IEquatable<NewsUpdateHistoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NewsUpdateHistoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NewsUpdateHistoryId{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		[FieldNameAttribute("NewsId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsId{ get; set; }

		[FieldNameAttribute("TranslatedDescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TranslatedDescription{ get; set; }

		[FieldNameAttribute("TranslatedTitle",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TranslatedTitle{ get; set; }

		[FieldNameAttribute("Title",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("BunchGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BunchGuid{ get; set; }

		[FieldNameAttribute("Description",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Description{ get; set; }

		[FieldNameAttribute("DescriptionText",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DescriptionText{ get; set; }

		[FieldNameAttribute("ShortDescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ShortDescription{ get; set; }

		[FieldNameAttribute("LanguageCode",true,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("Author",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Author{ get; set; }

		[FieldNameAttribute("ReporterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ReporterId{ get; set; }

		[FieldNameAttribute("IsVerified",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsVerified{ get; set; }

		[FieldNameAttribute("IsAired",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsAired{ get; set; }

		[FieldNameAttribute("AddedToRundownCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AddedToRundownCount{ get; set; }

		[FieldNameAttribute("OnAirCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OnAirCount{ get; set; }

		[FieldNameAttribute("OtherChannelExecutionCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OtherChannelExecutionCount{ get; set; }

		[FieldNameAttribute("NewsTickerCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsTickerCount{ get; set; }

		[FieldNameAttribute("Slug",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Slug{ get; set; }

		[FieldNameAttribute("NewsPaperdescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsPaperdescription{ get; set; }

		[FieldNameAttribute("Suggestions",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Suggestions{ get; set; }

		[FieldNameAttribute("Source",true,false,200)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Source{ get; set; }

		[FieldNameAttribute("SourceTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceTypeId{ get; set; }

		[FieldNameAttribute("SourceNewsUrl",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SourceNewsUrl{ get; set; }

		[FieldNameAttribute("SourceFilterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SourceFilterId{ get; set; }

		[FieldNameAttribute("ThumbnailId",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ThumbnailId{ get; set; }

		[FieldNameAttribute("NewsTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsTypeId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("PublishTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? PublishTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string PublishTimeStr
		{
			 get {if(PublishTime.HasValue) return PublishTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { PublishTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("SlugId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlugId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NewsUpdateHistoryBase> Members

        public virtual bool Equals(NewsUpdateHistoryBase other)
        {
			if(this.NewsUpdateHistoryId==other.NewsUpdateHistoryId  && this.NewsGuid==other.NewsGuid  && this.NewsId==other.NewsId  && this.TranslatedDescription==other.TranslatedDescription  && this.TranslatedTitle==other.TranslatedTitle  && this.Title==other.Title  && this.BunchGuid==other.BunchGuid  && this.Description==other.Description  && this.DescriptionText==other.DescriptionText  && this.ShortDescription==other.ShortDescription  && this.LanguageCode==other.LanguageCode  && this.Author==other.Author  && this.ReporterId==other.ReporterId  && this.IsVerified==other.IsVerified  && this.IsAired==other.IsAired  && this.AddedToRundownCount==other.AddedToRundownCount  && this.OnAirCount==other.OnAirCount  && this.OtherChannelExecutionCount==other.OtherChannelExecutionCount  && this.NewsTickerCount==other.NewsTickerCount  && this.Slug==other.Slug  && this.NewsPaperdescription==other.NewsPaperdescription  && this.Suggestions==other.Suggestions  && this.Source==other.Source  && this.SourceTypeId==other.SourceTypeId  && this.SourceNewsUrl==other.SourceNewsUrl  && this.SourceFilterId==other.SourceFilterId  && this.ThumbnailId==other.ThumbnailId  && this.NewsTypeId==other.NewsTypeId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.PublishTime==other.PublishTime  && this.IsActive==other.IsActive  && this.SlugId==other.SlugId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NewsUpdateHistory other)
        {
			if(other!=null)
			{
				this.NewsUpdateHistoryId=other.NewsUpdateHistoryId;
				this.NewsGuid=other.NewsGuid;
				this.NewsId=other.NewsId;
				this.TranslatedDescription=other.TranslatedDescription;
				this.TranslatedTitle=other.TranslatedTitle;
				this.Title=other.Title;
				this.BunchGuid=other.BunchGuid;
				this.Description=other.Description;
				this.DescriptionText=other.DescriptionText;
				this.ShortDescription=other.ShortDescription;
				this.LanguageCode=other.LanguageCode;
				this.Author=other.Author;
				this.ReporterId=other.ReporterId;
				this.IsVerified=other.IsVerified;
				this.IsAired=other.IsAired;
				this.AddedToRundownCount=other.AddedToRundownCount;
				this.OnAirCount=other.OnAirCount;
				this.OtherChannelExecutionCount=other.OtherChannelExecutionCount;
				this.NewsTickerCount=other.NewsTickerCount;
				this.Slug=other.Slug;
				this.NewsPaperdescription=other.NewsPaperdescription;
				this.Suggestions=other.Suggestions;
				this.Source=other.Source;
				this.SourceTypeId=other.SourceTypeId;
				this.SourceNewsUrl=other.SourceNewsUrl;
				this.SourceFilterId=other.SourceFilterId;
				this.ThumbnailId=other.ThumbnailId;
				this.NewsTypeId=other.NewsTypeId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.PublishTime=other.PublishTime;
				this.IsActive=other.IsActive;
				this.SlugId=other.SlugId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
