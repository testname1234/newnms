﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NotificationBase:EntityBase, IEquatable<NotificationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NotificationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NotificationId{ get; set; }

		[FieldNameAttribute("NotificationTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NotificationTypeId{ get; set; }

		[FieldNameAttribute("Title",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("Argument",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Argument{ get; set; }

		[FieldNameAttribute("RecipientId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? RecipientId{ get; set; }

		[FieldNameAttribute("SenderId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SenderId{ get; set; }

		[FieldNameAttribute("IsRead",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsRead{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("slotid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Slotid{ get; set; }

		[FieldNameAttribute("slotscreentemplateid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Slotscreentemplateid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NotificationBase> Members

        public virtual bool Equals(NotificationBase other)
        {
			if(this.NotificationId==other.NotificationId  && this.NotificationTypeId==other.NotificationTypeId  && this.Title==other.Title  && this.Argument==other.Argument  && this.RecipientId==other.RecipientId  && this.SenderId==other.SenderId  && this.IsRead==other.IsRead  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Slotid==other.Slotid  && this.Slotscreentemplateid==other.Slotscreentemplateid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Notification other)
        {
			if(other!=null)
			{
				this.NotificationId=other.NotificationId;
				this.NotificationTypeId=other.NotificationTypeId;
				this.Title=other.Title;
				this.Argument=other.Argument;
				this.RecipientId=other.RecipientId;
				this.SenderId=other.SenderId;
				this.IsRead=other.IsRead;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Slotid=other.Slotid;
				this.Slotscreentemplateid=other.Slotscreentemplateid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
