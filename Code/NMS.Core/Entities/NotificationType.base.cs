﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class NotificationTypeBase:EntityBase, IEquatable<NotificationTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("NotificationTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 NotificationTypeId{ get; set; }

		[FieldNameAttribute("Name",false,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("Template",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Template{ get; set; }

		[FieldNameAttribute("Argument",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Argument{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<NotificationTypeBase> Members

        public virtual bool Equals(NotificationTypeBase other)
        {
			if(this.NotificationTypeId==other.NotificationTypeId  && this.Name==other.Name  && this.Template==other.Template  && this.Argument==other.Argument )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(NotificationType other)
        {
			if(other!=null)
			{
				this.NotificationTypeId=other.NotificationTypeId;
				this.Name=other.Name;
				this.Template=other.Template;
				this.Argument=other.Argument;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
