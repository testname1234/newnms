﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class OnAirTicker : OnAirTickerBase 
	{

        [DataMember(EmitDefaultValue = false)]
        public List<OnAirTickerLine> TickerLines { get; set; }
		
	}
}
