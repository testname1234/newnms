﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class OnAirTickerLine : OnAirTickerLineBase 
	{

        [FieldNameAttribute("TickerGroupName", true, false, 0)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String TickerGroupName { get; set; }

        [FieldNameAttribute("TickerSequenceId", true, false, 4)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? TickerSequenceId { get; set; }
	}
}
