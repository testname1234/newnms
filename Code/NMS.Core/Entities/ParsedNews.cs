﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using NMS.Core.Enums;

namespace NMS.Core.Entities
{
    [Serializable]
    public class ParsedNews
    {
        public ParsedNews()
        {
            ImageGuids = new List<string>();
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<string> Categories { get; set; }
        public DateTime PublishTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public string Author { get; set; }
        public string Location { get; set; }
        public string LanguageCode { get; set; }
        public List<string> ImageGuids { get; set; }
        public FilterTypes SourceType { get; set; }
        public string FilterName { get; set; }
    }
}
