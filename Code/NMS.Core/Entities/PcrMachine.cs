﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Enums;

namespace NMS.Core.Entities
{
    public class PcrMachine
    {
        public string IP { get; set; }

        public string Id { get; set; }

        public int Port { get; set; }

        public int PcrId { get; set; }

        public bool IsMOSDevice { get; set; }

        public int DeviceTypeId { get; set; }

        public DeviceType DeviceType
        {
            get
            {
                return (DeviceType)DeviceTypeId;
            }
            set
            {
                DeviceTypeId = (int)value;
            }
        }
    }
}
