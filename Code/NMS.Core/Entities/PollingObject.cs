﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using Validation;

namespace NMS.Core.Entities
{
    public class PollingObject
    {

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = false)]
        [DataMember(EmitDefaultValue = false)]
        public string CreatedBy { get; set; }

        [IgnoreDataMember]
        public DateTime LastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LastUpdateDateStr 
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }
        
        public string From { get; set; }
        //{
        //    get { return From.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
        //      set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime(); } }
        //}

        public string To { get; set; }
        //{
        //    get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
        //    set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        //}

        public string SearchText { get; set; }

        public int AssignedTo { get; set; }

    }
}
