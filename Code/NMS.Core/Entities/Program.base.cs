﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramBase:EntityBase, IEquatable<ProgramBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("Name",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("ChannelId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("IsApproved",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsApproved{ get; set; }

		[FieldNameAttribute("MinStoryCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? MinStoryCount{ get; set; }

		[FieldNameAttribute("MaxStoryCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? MaxStoryCount{ get; set; }

		[FieldNameAttribute("ProgramTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramTypeId{ get; set; }

		[FieldNameAttribute("BucketID",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? BucketId{ get; set; }

		[FieldNameAttribute("FilterId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FilterId{ get; set; }

		[FieldNameAttribute("ApiKey",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ApiKey{ get; set; }

		[FieldNameAttribute("Interval",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 Interval{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramBase> Members

        public virtual bool Equals(ProgramBase other)
        {
			if(this.ProgramId==other.ProgramId  && this.Name==other.Name  && this.ChannelId==other.ChannelId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsApproved==other.IsApproved  && this.MinStoryCount==other.MinStoryCount  && this.MaxStoryCount==other.MaxStoryCount  && this.ProgramTypeId==other.ProgramTypeId  && this.BucketId==other.BucketId  && this.FilterId==other.FilterId  && this.ApiKey==other.ApiKey  && this.Interval==other.Interval )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Program other)
        {
			if(other!=null)
			{
				this.ProgramId=other.ProgramId;
				this.Name=other.Name;
				this.ChannelId=other.ChannelId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsApproved=other.IsApproved;
				this.MinStoryCount=other.MinStoryCount;
				this.MaxStoryCount=other.MaxStoryCount;
				this.ProgramTypeId=other.ProgramTypeId;
				this.BucketId=other.BucketId;
				this.FilterId=other.FilterId;
				this.ApiKey=other.ApiKey;
				this.Interval=other.Interval;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
