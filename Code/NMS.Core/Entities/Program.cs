﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class Program : ProgramBase 
	{
        public ProgramSchedule Schedule { get; set; }
        public string StartupUrl { get; set; }
        public string CreditsUrl { get; set; }
	}
}
