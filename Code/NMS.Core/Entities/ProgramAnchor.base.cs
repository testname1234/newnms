﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramAnchorBase:EntityBase, IEquatable<ProgramAnchorBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramAnchorId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramAnchorId{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("CelebrityId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CelebrityId{ get; set; }

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramAnchorBase> Members

        public virtual bool Equals(ProgramAnchorBase other)
        {
			if(this.ProgramAnchorId==other.ProgramAnchorId  && this.ProgramId==other.ProgramId  && this.CelebrityId==other.CelebrityId  && this.IsActive==other.IsActive  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramAnchor other)
        {
			if(other!=null)
			{
				this.ProgramAnchorId=other.ProgramAnchorId;
				this.ProgramId=other.ProgramId;
				this.CelebrityId=other.CelebrityId;
				this.IsActive=other.IsActive;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
