﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramConfigurationBase:EntityBase, IEquatable<ProgramConfigurationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramConfigurationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramConfigurationId{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("SegmentTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SegmentTypeId{ get; set; }

		[FieldNameAttribute("Url",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramConfigurationBase> Members

        public virtual bool Equals(ProgramConfigurationBase other)
        {
			if(this.ProgramConfigurationId==other.ProgramConfigurationId  && this.ProgramId==other.ProgramId  && this.SegmentTypeId==other.SegmentTypeId  && this.Url==other.Url )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramConfiguration other)
        {
			if(other!=null)
			{
				this.ProgramConfigurationId=other.ProgramConfigurationId;
				this.ProgramId=other.ProgramId;
				this.SegmentTypeId=other.SegmentTypeId;
				this.Url=other.Url;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
