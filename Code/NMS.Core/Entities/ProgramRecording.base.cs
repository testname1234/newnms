﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramRecordingBase:EntityBase, IEquatable<ProgramRecordingBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramRecordingId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramRecordingId{ get; set; }

		[FieldNameAttribute("ProgramId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("Path",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Path{ get; set; }

		[FieldNameAttribute("Status",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Status{ get; set; }

		[FieldNameAttribute("From",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? From{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string FromStr
		{
			 get {if(From.HasValue) return From.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("To",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? To{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string ToStr
		{
			 get {if(To.HasValue) return To.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramRecordingBase> Members

        public virtual bool Equals(ProgramRecordingBase other)
        {
			if(this.ProgramRecordingId==other.ProgramRecordingId  && this.ProgramId==other.ProgramId  && this.Path==other.Path  && this.Status==other.Status  && this.From==other.From  && this.To==other.To  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramRecording other)
        {
			if(other!=null)
			{
				this.ProgramRecordingId=other.ProgramRecordingId;
				this.ProgramId=other.ProgramId;
				this.Path=other.Path;
				this.Status=other.Status;
				this.From=other.From;
				this.To=other.To;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
