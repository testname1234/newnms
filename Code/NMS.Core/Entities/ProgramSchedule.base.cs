﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramScheduleBase:EntityBase, IEquatable<ProgramScheduleBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramScheduleId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramScheduleId{ get; set; }

		[FieldNameAttribute("WeekDayId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WeekDayId{ get; set; }

		[FieldNameAttribute("StartTime",false,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual TimeSpan StartTime{ get; set; }

		[FieldNameAttribute("EndTime",false,false,5)]
		[DataMember (EmitDefaultValue=false)]
		public virtual TimeSpan EndTime{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("RecurringTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RecurringTypeId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramScheduleBase> Members

        public virtual bool Equals(ProgramScheduleBase other)
        {
			if(this.ProgramScheduleId==other.ProgramScheduleId  && this.WeekDayId==other.WeekDayId  && this.StartTime==other.StartTime  && this.EndTime==other.EndTime  && this.ProgramId==other.ProgramId  && this.RecurringTypeId==other.RecurringTypeId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramSchedule other)
        {
			if(other!=null)
			{
				this.ProgramScheduleId=other.ProgramScheduleId;
				this.WeekDayId=other.WeekDayId;
				this.StartTime=other.StartTime;
				this.EndTime=other.EndTime;
				this.ProgramId=other.ProgramId;
				this.RecurringTypeId=other.RecurringTypeId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
