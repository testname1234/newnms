﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramScreenElementBase:EntityBase, IEquatable<ProgramScreenElementBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramScreenElementId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramScreenElementId{ get; set; }

		[FieldNameAttribute("ScreenElementId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenElementId{ get; set; }

		[FieldNameAttribute("ImageGuid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid ImageGuid{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramScreenElementBase> Members

        public virtual bool Equals(ProgramScreenElementBase other)
        {
			if(this.ProgramScreenElementId==other.ProgramScreenElementId  && this.ScreenElementId==other.ScreenElementId  && this.ImageGuid==other.ImageGuid  && this.ProgramId==other.ProgramId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramScreenElement other)
        {
			if(other!=null)
			{
				this.ProgramScreenElementId=other.ProgramScreenElementId;
				this.ScreenElementId=other.ScreenElementId;
				this.ImageGuid=other.ImageGuid;
				this.ProgramId=other.ProgramId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
