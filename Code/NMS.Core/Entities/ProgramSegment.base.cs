﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramSegmentBase:EntityBase, IEquatable<ProgramSegmentBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramSegmentId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramSegmentId{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("SegmentTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SegmentTypeId{ get; set; }

		[FieldNameAttribute("SequenceNumber",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequenceNumber{ get; set; }

		[FieldNameAttribute("Duration",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Duration{ get; set; }

		[FieldNameAttribute("StoryCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StoryCount{ get; set; }

		[FieldNameAttribute("Name",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdatedDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get { return LastUpdatedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramSegmentBase> Members

        public virtual bool Equals(ProgramSegmentBase other)
        {
			if(this.ProgramSegmentId==other.ProgramSegmentId  && this.ProgramId==other.ProgramId  && this.SegmentTypeId==other.SegmentTypeId  && this.SequenceNumber==other.SequenceNumber  && this.Duration==other.Duration  && this.StoryCount==other.StoryCount  && this.Name==other.Name  && this.CreationDate==other.CreationDate  && this.LastUpdatedDate==other.LastUpdatedDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramSegment other)
        {
			if(other!=null)
			{
				this.ProgramSegmentId=other.ProgramSegmentId;
				this.ProgramId=other.ProgramId;
				this.SegmentTypeId=other.SegmentTypeId;
				this.SequenceNumber=other.SequenceNumber;
				this.Duration=other.Duration;
				this.StoryCount=other.StoryCount;
				this.Name=other.Name;
				this.CreationDate=other.CreationDate;
				this.LastUpdatedDate=other.LastUpdatedDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
