﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ProgramTypeBase:EntityBase, IEquatable<ProgramTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ProgramTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramTypeId{ get; set; }

		[FieldNameAttribute("ProgramTypeName",true,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ProgramTypeName{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ProgramTypeBase> Members

        public virtual bool Equals(ProgramTypeBase other)
        {
			if(this.ProgramTypeId==other.ProgramTypeId  && this.ProgramTypeName==other.ProgramTypeName )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ProgramType other)
        {
			if(other!=null)
			{
				this.ProgramTypeId=other.ProgramTypeId;
				this.ProgramTypeName=other.ProgramTypeName;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
