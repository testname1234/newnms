﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class RadioStreamBase:EntityBase, IEquatable<RadioStreamBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("RadioStreamId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RadioStreamId{ get; set; }

		[FieldNameAttribute("RadioStationId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RadioStationId{ get; set; }

		[FieldNameAttribute("PhysicalPath",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String PhysicalPath{ get; set; }

		[FieldNameAttribute("ProgramId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("Url",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		[FieldNameAttribute("To",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime To{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string ToStr
		{
			 get { return To.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("From",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime From{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string FromStr
		{
			 get { return From.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsProcessed",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsProcessed{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<RadioStreamBase> Members

        public virtual bool Equals(RadioStreamBase other)
        {
			if(this.RadioStreamId==other.RadioStreamId  && this.RadioStationId==other.RadioStationId  && this.PhysicalPath==other.PhysicalPath  && this.ProgramId==other.ProgramId  && this.Url==other.Url  && this.To==other.To  && this.From==other.From  && this.IsProcessed==other.IsProcessed  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(RadioStream other)
        {
			if(other!=null)
			{
				this.RadioStreamId=other.RadioStreamId;
				this.RadioStationId=other.RadioStationId;
				this.PhysicalPath=other.PhysicalPath;
				this.ProgramId=other.ProgramId;
				this.Url=other.Url;
				this.To=other.To;
				this.From=other.From;
				this.IsProcessed=other.IsProcessed;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
