﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using NMS.Core.Enums;

namespace NMS.Core.Entities
{
    [Serializable]
    public class RawNews
    {
        public RawNews()
        {
            ImageGuids = new List<Resource>();
            Categories = new List<string>();
            PublishTime = DateTime.UtcNow;
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DescriptionText { get; set; }
        public List<string> Categories { get; set; }
        public DateTime PublishTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public string Author { get; set; }
        public string Location { get; set; }
        public string LanguageCode { get; set; }
        public List<Resource> ImageGuids { get; set; }
        public FilterTypes SourceType { get; set; }
        public string Source { get; set; }
        public NewsTypes NewsType { get; set; }
        public string Url { get; set; }
        public string TranslatedTitle { get; set; }
        public string TranslatedSlug { get; set; }
        public string Slug { get; set; }
        public string TranslatedDescription { get; set; }
    }
}
