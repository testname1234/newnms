﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class RecordingClipsBase:EntityBase, IEquatable<RecordingClipsBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("RecordingClipId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RecordingClipId{ get; set; }

		[FieldNameAttribute("Script",true,false,700)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Script{ get; set; }

		[FieldNameAttribute("CelebrityId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		[FieldNameAttribute("ProgramRecordingId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramRecordingId{ get; set; }

		[FieldNameAttribute("From",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String From{ get; set; }

		[FieldNameAttribute("To",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String To{ get; set; }

		[FieldNameAttribute("CreatedBy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CreatedBy{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Status",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Status{ get; set; }

		[FieldNameAttribute("LanguageId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LanguageId{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<RecordingClipsBase> Members

        public virtual bool Equals(RecordingClipsBase other)
        {
			if(this.RecordingClipId==other.RecordingClipId  && this.Script==other.Script  && this.CelebrityId==other.CelebrityId  && this.ProgramRecordingId==other.ProgramRecordingId  && this.From==other.From  && this.To==other.To  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Status==other.Status  && this.LanguageId==other.LanguageId  && this.ParentId==other.ParentId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(RecordingClips other)
        {
			if(other!=null)
			{
				this.RecordingClipId=other.RecordingClipId;
				this.Script=other.Script;
				this.CelebrityId=other.CelebrityId;
				this.ProgramRecordingId=other.ProgramRecordingId;
				this.From=other.From;
				this.To=other.To;
				this.CreatedBy=other.CreatedBy;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Status=other.Status;
				this.LanguageId=other.LanguageId;
				this.ParentId=other.ParentId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
