﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ReelBase:EntityBase, IEquatable<ReelBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ReelId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ReelId{ get; set; }

		[FieldNameAttribute("ChannelId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelId{ get; set; }

		[FieldNameAttribute("Path",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Path{ get; set; }

		[FieldNameAttribute("StatusId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		[FieldNameAttribute("StartTime",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string StartTimeStr
		{
			 get { return StartTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("EndTime",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime EndTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string EndTimeStr
		{
			 get { return EndTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EndTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("OriginalPath",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String OriginalPath{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ReelBase> Members

        public virtual bool Equals(ReelBase other)
        {
			if(this.ReelId==other.ReelId  && this.ChannelId==other.ChannelId  && this.Path==other.Path  && this.StatusId==other.StatusId  && this.StartTime==other.StartTime  && this.EndTime==other.EndTime  && this.CreationDate==other.CreationDate  && this.OriginalPath==other.OriginalPath )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Reel other)
        {
			if(other!=null)
			{
				this.ReelId=other.ReelId;
				this.ChannelId=other.ChannelId;
				this.Path=other.Path;
				this.StatusId=other.StatusId;
				this.StartTime=other.StartTime;
				this.EndTime=other.EndTime;
				this.CreationDate=other.CreationDate;
				this.OriginalPath=other.OriginalPath;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
