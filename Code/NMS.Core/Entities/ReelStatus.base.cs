﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ReelStatusBase:EntityBase, IEquatable<ReelStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ReelStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ReelStatusId{ get; set; }

		[FieldNameAttribute("Status",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Status{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ReelStatusBase> Members

        public virtual bool Equals(ReelStatusBase other)
        {
			if(this.ReelStatusId==other.ReelStatusId  && this.Status==other.Status )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ReelStatus other)
        {
			if(other!=null)
			{
				this.ReelStatusId=other.ReelStatusId;
				this.Status=other.Status;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
