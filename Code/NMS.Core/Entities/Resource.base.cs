﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceBase:EntityBase, IEquatable<ResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceId{ get; set; }

		[FieldNameAttribute("Guid",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Guid{ get; set; }

		[FieldNameAttribute("ResourceTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceTypeId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("Caption",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Caption{ get; set; }

		[FieldNameAttribute("Category",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Category{ get; set; }

		[FieldNameAttribute("Location",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Location{ get; set; }

		[FieldNameAttribute("Duration",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Duration{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceBase> Members

        public virtual bool Equals(ResourceBase other)
        {
			if(this.ResourceId==other.ResourceId  && this.Guid==other.Guid  && this.ResourceTypeId==other.ResourceTypeId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Caption==other.Caption  && this.Category==other.Category  && this.Location==other.Location  && this.Duration==other.Duration )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Resource other)
        {
			if(other!=null)
			{
				this.ResourceId=other.ResourceId;
				this.Guid=other.Guid;
				this.ResourceTypeId=other.ResourceTypeId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Caption=other.Caption;
				this.Category=other.Category;
				this.Location=other.Location;
				this.Duration=other.Duration;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
