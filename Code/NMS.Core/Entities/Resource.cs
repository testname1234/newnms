﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class Resource : ResourceBase 
	{
        public string FilePath { get; set; }
	}
}
