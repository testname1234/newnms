﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceEditclipinfoBase:EntityBase, IEquatable<ResourceEditclipinfoBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceEditclipinfoId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceEditclipinfoId{ get; set; }

		[FieldNameAttribute("NewsResourceEditId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsResourceEditId{ get; set; }

		[FieldNameAttribute("From",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? From{ get; set; }

		[FieldNameAttribute("To",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? To{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isactive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Isactive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceEditclipinfoBase> Members

        public virtual bool Equals(ResourceEditclipinfoBase other)
        {
			if(this.ResourceEditclipinfoId==other.ResourceEditclipinfoId  && this.NewsResourceEditId==other.NewsResourceEditId  && this.From==other.From  && this.To==other.To  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Isactive==other.Isactive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ResourceEditclipinfo other)
        {
			if(other!=null)
			{
				this.ResourceEditclipinfoId=other.ResourceEditclipinfoId;
				this.NewsResourceEditId=other.NewsResourceEditId;
				this.From=other.From;
				this.To=other.To;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Isactive=other.Isactive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
