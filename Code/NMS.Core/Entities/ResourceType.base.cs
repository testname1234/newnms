﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ResourceTypeBase:EntityBase, IEquatable<ResourceTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ResourceTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ResourceTypeId{ get; set; }

		[FieldNameAttribute("ResourceType",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResourceType{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ResourceTypeBase> Members

        public virtual bool Equals(ResourceTypeBase other)
        {
			if(this.ResourceTypeId==other.ResourceTypeId  && this.ResourceType==other.ResourceType )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ResourceType other)
        {
			if(other!=null)
			{
				this.ResourceTypeId=other.ResourceTypeId;
				this.ResourceType=other.ResourceType;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
