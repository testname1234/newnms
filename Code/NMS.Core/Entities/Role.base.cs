﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class RoleBase:EntityBase, IEquatable<RoleBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("RoleId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RoleId{ get; set; }

		[FieldNameAttribute("Role",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Role{ get; set; }

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<RoleBase> Members

        public virtual bool Equals(RoleBase other)
        {
			if(this.RoleId==other.RoleId  && this.Role==other.Role  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Role other)
        {
			if(other!=null)
			{
				this.RoleId=other.RoleId;
				this.Role=other.Role;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
