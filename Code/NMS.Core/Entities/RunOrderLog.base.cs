﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class RunOrderLogBase:EntityBase, IEquatable<RunOrderLogBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("RunOrderLogId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RunOrderLogId{ get; set; }

		[FieldNameAttribute("groupId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? GroupId{ get; set; }

		[FieldNameAttribute("messageId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? MessageId{ get; set; }

		[FieldNameAttribute("mosId",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String MosId{ get; set; }

		[FieldNameAttribute("ncsId",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NcsId{ get; set; }

		[FieldNameAttribute("roStoriesCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? RoStoriesCount{ get; set; }

		[FieldNameAttribute("roStatus",true,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String RoStatus{ get; set; }

		[FieldNameAttribute("roId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? RoId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<RunOrderLogBase> Members

        public virtual bool Equals(RunOrderLogBase other)
        {
			if(this.RunOrderLogId==other.RunOrderLogId  && this.GroupId==other.GroupId  && this.MessageId==other.MessageId  && this.MosId==other.MosId  && this.NcsId==other.NcsId  && this.RoStoriesCount==other.RoStoriesCount  && this.RoStatus==other.RoStatus  && this.RoId==other.RoId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(RunOrderLog other)
        {
			if(other!=null)
			{
				this.RunOrderLogId=other.RunOrderLogId;
				this.GroupId=other.GroupId;
				this.MessageId=other.MessageId;
				this.MosId=other.MosId;
				this.NcsId=other.NcsId;
				this.RoStoriesCount=other.RoStoriesCount;
				this.RoStatus=other.RoStatus;
				this.RoId=other.RoId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
