﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class RunOrderStoriesBase:EntityBase, IEquatable<RunOrderStoriesBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("RunOrderStoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RunOrderStoryId{ get; set; }

		[FieldNameAttribute("RunOrderLogId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RunOrderLogId{ get; set; }

		[FieldNameAttribute("ItemChannel",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ItemChannel{ get; set; }

		[FieldNameAttribute("ItemId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ItemId{ get; set; }

		[FieldNameAttribute("ObjId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ObjId{ get; set; }

		[FieldNameAttribute("Status",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Status{ get; set; }

		[FieldNameAttribute("StoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StoryId{ get; set; }

		[FieldNameAttribute("StoryStatus",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String StoryStatus{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<RunOrderStoriesBase> Members

        public virtual bool Equals(RunOrderStoriesBase other)
        {
			if(this.RunOrderStoryId==other.RunOrderStoryId  && this.RunOrderLogId==other.RunOrderLogId  && this.ItemChannel==other.ItemChannel  && this.ItemId==other.ItemId  && this.ObjId==other.ObjId  && this.Status==other.Status  && this.StoryId==other.StoryId  && this.StoryStatus==other.StoryStatus )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(RunOrderStories other)
        {
			if(other!=null)
			{
				this.RunOrderStoryId=other.RunOrderStoryId;
				this.RunOrderLogId=other.RunOrderLogId;
				this.ItemChannel=other.ItemChannel;
				this.ItemId=other.ItemId;
				this.ObjId=other.ObjId;
				this.Status=other.Status;
				this.StoryId=other.StoryId;
				this.StoryStatus=other.StoryStatus;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
