﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ScrapMaxDatesBase:EntityBase, IEquatable<ScrapMaxDatesBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ScrapMaxDatesId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScrapMaxDatesId{ get; set; }

		[FieldNameAttribute("MaxUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime MaxUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string MaxUpdateDateStr
		{
			 get { return MaxUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { MaxUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("SourceName",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String SourceName{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ScrapMaxDatesBase> Members

        public virtual bool Equals(ScrapMaxDatesBase other)
        {
			if(this.ScrapMaxDatesId==other.ScrapMaxDatesId  && this.MaxUpdateDate==other.MaxUpdateDate  && this.SourceName==other.SourceName )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ScrapMaxDates other)
        {
			if(other!=null)
			{
				this.ScrapMaxDatesId=other.ScrapMaxDatesId;
				this.MaxUpdateDate=other.MaxUpdateDate;
				this.SourceName=other.SourceName;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
