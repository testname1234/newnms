﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ScreenTemplateBase:EntityBase, IEquatable<ScreenTemplateBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ScreenTemplateId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenTemplateId{ get; set; }

		[FieldNameAttribute("ProgramId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ProgramId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("IsDefault",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsDefault{ get; set; }

		[FieldNameAttribute("Html",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Html{ get; set; }

		[FieldNameAttribute("Description",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Description{ get; set; }

		[FieldNameAttribute("Duration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Duration{ get; set; }

		[FieldNameAttribute("ThumbGuid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid ThumbGuid{ get; set; }

		[FieldNameAttribute("GroupId",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String GroupId{ get; set; }

		[FieldNameAttribute("AssetId",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String AssetId{ get; set; }

		[FieldNameAttribute("BackgroundColor",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BackgroundColor{ get; set; }

		[FieldNameAttribute("BackgroundImageUrl",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? BackgroundImageUrl{ get; set; }

		[FieldNameAttribute("BackgroundRepeat",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BackgroundRepeat{ get; set; }

		[FieldNameAttribute("CreatonDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreatonDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreatonDateStr
		{
			 get { return CreatonDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatonDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("flashtemplateid",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 Flashtemplateid{ get; set; }

		[FieldNameAttribute("RatioTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? RatioTypeId{ get; set; }

		[FieldNameAttribute("IsVideoWallTemplate",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsVideoWallTemplate{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ScreenTemplateBase> Members

        public virtual bool Equals(ScreenTemplateBase other)
        {
			if(this.ScreenTemplateId==other.ScreenTemplateId  && this.ProgramId==other.ProgramId  && this.Name==other.Name  && this.IsDefault==other.IsDefault  && this.Html==other.Html  && this.Description==other.Description  && this.Duration==other.Duration  && this.ThumbGuid==other.ThumbGuid  && this.GroupId==other.GroupId  && this.AssetId==other.AssetId  && this.BackgroundColor==other.BackgroundColor  && this.BackgroundImageUrl==other.BackgroundImageUrl  && this.BackgroundRepeat==other.BackgroundRepeat  && this.CreatonDate==other.CreatonDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Flashtemplateid==other.Flashtemplateid  && this.RatioTypeId==other.RatioTypeId  && this.IsVideoWallTemplate==other.IsVideoWallTemplate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ScreenTemplate other)
        {
			if(other!=null)
			{
				this.ScreenTemplateId=other.ScreenTemplateId;
				this.ProgramId=other.ProgramId;
				this.Name=other.Name;
				this.IsDefault=other.IsDefault;
				this.Html=other.Html;
				this.Description=other.Description;
				this.Duration=other.Duration;
				this.ThumbGuid=other.ThumbGuid;
				this.GroupId=other.GroupId;
				this.AssetId=other.AssetId;
				this.BackgroundColor=other.BackgroundColor;
				this.BackgroundImageUrl=other.BackgroundImageUrl;
				this.BackgroundRepeat=other.BackgroundRepeat;
				this.CreatonDate=other.CreatonDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Flashtemplateid=other.Flashtemplateid;
				this.RatioTypeId=other.RatioTypeId;
				this.IsVideoWallTemplate=other.IsVideoWallTemplate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
