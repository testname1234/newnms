﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class ScreenTemplate : ScreenTemplateBase 
	{
        public List<TemplateScreenElement> Elements { get; set; }
        public List<ScreenTemplatekey> Keys { get; set; }
        public int? ParentId { get; set; }
	}
}
