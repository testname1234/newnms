﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ScreenTemplateStatusBase:EntityBase, IEquatable<ScreenTemplateStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ScreenTemplateStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenTemplateStatusId{ get; set; }

		[FieldNameAttribute("Name",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ScreenTemplateStatusBase> Members

        public virtual bool Equals(ScreenTemplateStatusBase other)
        {
			if(this.ScreenTemplateStatusId==other.ScreenTemplateStatusId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ScreenTemplateStatus other)
        {
			if(other!=null)
			{
				this.ScreenTemplateStatusId=other.ScreenTemplateStatusId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
