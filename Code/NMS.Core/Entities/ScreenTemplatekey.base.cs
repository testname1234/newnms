﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ScreenTemplatekeyBase:EntityBase, IEquatable<ScreenTemplatekeyBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ScreenTemplatekeyId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenTemplatekeyId{ get; set; }

		[FieldNameAttribute("ScreenTemplateId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ScreenTemplateId{ get; set; }

		[FieldNameAttribute("FlashTemplateKeyId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? FlashTemplateKeyId{ get; set; }

		[FieldNameAttribute("KeyName",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String KeyName{ get; set; }

		[FieldNameAttribute("creationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string creationDateStr
		{
            get { if (CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } } 
		}

		[FieldNameAttribute("updateddate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? Updateddate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string updateddateStr
		{
            get { if (Updateddate.HasValue) return Updateddate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Updateddate = date.ToUniversalTime(); } } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("Top",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Top{ get; set; }

		[FieldNameAttribute("Bottom",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Bottom{ get; set; }

		[FieldNameAttribute("left",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Left{ get; set; }

		[FieldNameAttribute("Right",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Right{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ScreenTemplatekeyBase> Members

        public virtual bool Equals(ScreenTemplatekeyBase other)
        {
			if(this.ScreenTemplatekeyId==other.ScreenTemplatekeyId  && this.ScreenTemplateId==other.ScreenTemplateId  && this.FlashTemplateKeyId==other.FlashTemplateKeyId  && this.KeyName==other.KeyName  && this.CreationDate==other.CreationDate  && this.Updateddate==other.Updateddate  && this.IsActive==other.IsActive  && this.Top==other.Top  && this.Bottom==other.Bottom  && this.Left==other.Left  && this.Right==other.Right )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ScreenTemplatekey other)
        {
			if(other!=null)
			{
				this.ScreenTemplatekeyId=other.ScreenTemplatekeyId;
				this.ScreenTemplateId=other.ScreenTemplateId;
				this.FlashTemplateKeyId=other.FlashTemplateKeyId;
				this.KeyName=other.KeyName;
				this.CreationDate=other.CreationDate;
				this.Updateddate=other.Updateddate;
				this.IsActive=other.IsActive;
				this.Top=other.Top;
				this.Bottom=other.Bottom;
				this.Left=other.Left;
				this.Right=other.Right;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
