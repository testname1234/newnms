﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class ScriptBase:EntityBase, IEquatable<ScriptBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ScriptId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScriptId{ get; set; }

		[FieldNameAttribute("UserId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		[FieldNameAttribute("Script",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Script{ get; set; }

		[FieldNameAttribute("IsExecuted",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsExecuted{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Token",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Token{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ScriptBase> Members

        public virtual bool Equals(ScriptBase other)
        {
			if(this.ScriptId==other.ScriptId  && this.UserId==other.UserId  && this.Script==other.Script  && this.IsExecuted==other.IsExecuted  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Token==other.Token )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Script other)
        {
			if(other!=null)
			{
				this.ScriptId=other.ScriptId;
				this.UserId=other.UserId;
				this.Script=other.Script;
				this.IsExecuted=other.IsExecuted;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Token=other.Token;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
