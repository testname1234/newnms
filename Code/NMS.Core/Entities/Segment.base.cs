﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SegmentBase:EntityBase, IEquatable<SegmentBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SegmentId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SegmentId{ get; set; }

		[FieldNameAttribute("SegmentTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SegmentTypeId{ get; set; }

		[FieldNameAttribute("EpisodeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 EpisodeId{ get; set; }

		[FieldNameAttribute("SequnceNumber",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequnceNumber{ get; set; }

		[FieldNameAttribute("Duration",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Duration{ get; set; }

		[FieldNameAttribute("StoryCount",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StoryCount{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("Name",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SegmentBase> Members

        public virtual bool Equals(SegmentBase other)
        {
			if(this.SegmentId==other.SegmentId  && this.SegmentTypeId==other.SegmentTypeId  && this.EpisodeId==other.EpisodeId  && this.SequnceNumber==other.SequnceNumber  && this.Duration==other.Duration  && this.StoryCount==other.StoryCount  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Segment other)
        {
			if(other!=null)
			{
				this.SegmentId=other.SegmentId;
				this.SegmentTypeId=other.SegmentTypeId;
				this.EpisodeId=other.EpisodeId;
				this.SequnceNumber=other.SequnceNumber;
				this.Duration=other.Duration;
				this.StoryCount=other.StoryCount;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
