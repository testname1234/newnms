﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SegmentTypeBase:EntityBase, IEquatable<SegmentTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SegmentTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SegmentTypeId{ get; set; }

		[FieldNameAttribute("Segment",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Segment{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SegmentTypeBase> Members

        public virtual bool Equals(SegmentTypeBase other)
        {
			if(this.SegmentTypeId==other.SegmentTypeId  && this.Segment==other.Segment )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SegmentType other)
        {
			if(other!=null)
			{
				this.SegmentTypeId=other.SegmentTypeId;
				this.Segment=other.Segment;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
