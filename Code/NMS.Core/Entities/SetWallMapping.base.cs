﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SetWallMappingBase:EntityBase, IEquatable<SetWallMappingBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SetWallMappingId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SetWallMappingId{ get; set; }

		[FieldNameAttribute("SetId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SetId{ get; set; }

		[FieldNameAttribute("WallId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WallId{ get; set; }

		[FieldNameAttribute("RatioTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 RatioTypeId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SetWallMappingBase> Members

        public virtual bool Equals(SetWallMappingBase other)
        {
			if(this.SetWallMappingId==other.SetWallMappingId  && this.SetId==other.SetId  && this.WallId==other.WallId  && this.RatioTypeId==other.RatioTypeId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SetWallMapping other)
        {
			if(other!=null)
			{
				this.SetWallMappingId=other.SetWallMappingId;
				this.SetId=other.SetId;
				this.WallId=other.WallId;
				this.RatioTypeId=other.RatioTypeId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
