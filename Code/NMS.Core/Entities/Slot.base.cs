﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotBase:EntityBase, IEquatable<SlotBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotId{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		[FieldNameAttribute("Title",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("Description",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Description{ get; set; }

		[FieldNameAttribute("ThumbGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? ThumbGuid{ get; set; }

		[FieldNameAttribute("SequnceNumber",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SequnceNumber{ get; set; }

		[FieldNameAttribute("SlotTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotTypeId{ get; set; }

		[FieldNameAttribute("SegmentId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SegmentId{ get; set; }

		[FieldNameAttribute("CategoryId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("TranslatedDescription",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TranslatedDescription{ get; set; }

		[FieldNameAttribute("TranslatedTitle",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TranslatedTitle{ get; set; }

		[FieldNameAttribute("LanguageCode",true,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("PreviewGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? PreviewGuid{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		[FieldNameAttribute("TickerId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerId{ get; set; }

		[FieldNameAttribute("IncludeInRundown",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IncludeInRundown{ get; set; }

		[FieldNameAttribute("NewsFileId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsFileId{ get; set; }

		[FieldNameAttribute("ResearchText",true,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResearchText{ get; set; }

		[FieldNameAttribute("AnchorId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? AnchorId{ get; set; }

		[FieldNameAttribute("Script",true,false,4000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Script{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotBase> Members

        public virtual bool Equals(SlotBase other)
        {
			if(this.SlotId==other.SlotId  && this.NewsGuid==other.NewsGuid  && this.Title==other.Title  && this.Description==other.Description  && this.ThumbGuid==other.ThumbGuid  && this.SequnceNumber==other.SequnceNumber  && this.SlotTypeId==other.SlotTypeId  && this.SegmentId==other.SegmentId  && this.CategoryId==other.CategoryId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.TranslatedDescription==other.TranslatedDescription  && this.TranslatedTitle==other.TranslatedTitle  && this.LanguageCode==other.LanguageCode  && this.PreviewGuid==other.PreviewGuid  && this.UserId==other.UserId  && this.TickerId==other.TickerId  && this.IncludeInRundown==other.IncludeInRundown  && this.NewsFileId==other.NewsFileId  && this.ResearchText==other.ResearchText  && this.AnchorId==other.AnchorId  && this.Script==other.Script )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Slot other)
        {
			if(other!=null)
			{
				this.SlotId=other.SlotId;
				this.NewsGuid=other.NewsGuid;
				this.Title=other.Title;
				this.Description=other.Description;
				this.ThumbGuid=other.ThumbGuid;
				this.SequnceNumber=other.SequnceNumber;
				this.SlotTypeId=other.SlotTypeId;
				this.SegmentId=other.SegmentId;
				this.CategoryId=other.CategoryId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.TranslatedDescription=other.TranslatedDescription;
				this.TranslatedTitle=other.TranslatedTitle;
				this.LanguageCode=other.LanguageCode;
				this.PreviewGuid=other.PreviewGuid;
				this.UserId=other.UserId;
				this.TickerId=other.TickerId;
				this.IncludeInRundown=other.IncludeInRundown;
				this.NewsFileId=other.NewsFileId;
				this.ResearchText=other.ResearchText;
				this.AnchorId=other.AnchorId;
				this.Script=other.Script;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
