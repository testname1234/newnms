﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace NMS.Core.Entities
{
    [DataContract]
    public partial class Slot : SlotBase
    {
        public List<SlotScreenTemplate> SlotScreenTemplates { get; set; }

        public List<Guest> Guest { get; set; }

        public List<EditorialComment> EditorialComments { get; set; }

        public List<SlotHeadline> SlotHeadlines { get; set; }

        public int OldSlotId { get; set; }
        public string AnchorName { get; set; }
    }
}
