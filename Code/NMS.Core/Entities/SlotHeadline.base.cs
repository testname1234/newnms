﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotHeadlineBase:EntityBase, IEquatable<SlotHeadlineBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotHeadlineId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotHeadlineId{ get; set; }

		[FieldNameAttribute("SlotId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotId{ get; set; }

		[FieldNameAttribute("Headline",false,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Headline{ get; set; }

		[FieldNameAttribute("LanguageCode",true,false,10)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotHeadlineBase> Members

        public virtual bool Equals(SlotHeadlineBase other)
        {
			if(this.SlotHeadlineId==other.SlotHeadlineId  && this.SlotId==other.SlotId  && this.Headline==other.Headline  && this.LanguageCode==other.LanguageCode  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotHeadline other)
        {
			if(other!=null)
			{
				this.SlotHeadlineId=other.SlotHeadlineId;
				this.SlotId=other.SlotId;
				this.Headline=other.Headline;
				this.LanguageCode=other.LanguageCode;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
