﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotScreenTemplateBase:EntityBase, IEquatable<SlotScreenTemplateBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotScreenTemplateId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotScreenTemplateId{ get; set; }

		[FieldNameAttribute("SlotId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotId{ get; set; }

		[FieldNameAttribute("ScreenTemplateId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenTemplateId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("IsDefault",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsDefault{ get; set; }

		[FieldNameAttribute("Html",false,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Html{ get; set; }

		[FieldNameAttribute("Description",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Description{ get; set; }

		[FieldNameAttribute("Duration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Duration{ get; set; }

		[FieldNameAttribute("ThumbGuid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid ThumbGuid{ get; set; }

		[FieldNameAttribute("GroupId",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String GroupId{ get; set; }

		[FieldNameAttribute("AssetId",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String AssetId{ get; set; }

		[FieldNameAttribute("BackgroundColor",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BackgroundColor{ get; set; }

		[FieldNameAttribute("BackgroundImageUrl",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? BackgroundImageUrl{ get; set; }

		[FieldNameAttribute("BackgroundRepeat",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String BackgroundRepeat{ get; set; }

		[FieldNameAttribute("CreatonDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreatonDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreatonDateStr
		{
			 get { return CreatonDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreatonDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("Script",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Script{ get; set; }

		[FieldNameAttribute("SequenceNumber",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SequenceNumber{ get; set; }

		[FieldNameAttribute("IsAssignedToNLE",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsAssignedToNle{ get; set; }

		[FieldNameAttribute("IsAssignedToStoryWriter",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsAssignedToStoryWriter{ get; set; }

		[FieldNameAttribute("VideoDuration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? VideoDuration{ get; set; }

		[FieldNameAttribute("ScriptDuration",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ScriptDuration{ get; set; }

		[FieldNameAttribute("Instructions",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Instructions{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		[FieldNameAttribute("VideoWallId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? VideoWallId{ get; set; }

		[FieldNameAttribute("IsVideoWallTemplate",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsVideoWallTemplate{ get; set; }

		[FieldNameAttribute("StatusId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StatusId{ get; set; }

		[FieldNameAttribute("NLEStatusId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NleStatusId{ get; set; }

		[FieldNameAttribute("StoryWriterStatusId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? StoryWriterStatusId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotScreenTemplateBase> Members

        public virtual bool Equals(SlotScreenTemplateBase other)
        {
			if(this.SlotScreenTemplateId==other.SlotScreenTemplateId  && this.SlotId==other.SlotId  && this.ScreenTemplateId==other.ScreenTemplateId  && this.Name==other.Name  && this.IsDefault==other.IsDefault  && this.Html==other.Html  && this.Description==other.Description  && this.Duration==other.Duration  && this.ThumbGuid==other.ThumbGuid  && this.GroupId==other.GroupId  && this.AssetId==other.AssetId  && this.BackgroundColor==other.BackgroundColor  && this.BackgroundImageUrl==other.BackgroundImageUrl  && this.BackgroundRepeat==other.BackgroundRepeat  && this.CreatonDate==other.CreatonDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Script==other.Script  && this.SequenceNumber==other.SequenceNumber  && this.IsAssignedToNle==other.IsAssignedToNle  && this.IsAssignedToStoryWriter==other.IsAssignedToStoryWriter  && this.VideoDuration==other.VideoDuration  && this.ScriptDuration==other.ScriptDuration  && this.Instructions==other.Instructions  && this.ParentId==other.ParentId  && this.VideoWallId==other.VideoWallId  && this.IsVideoWallTemplate==other.IsVideoWallTemplate  && this.StatusId==other.StatusId  && this.NleStatusId==other.NleStatusId  && this.StoryWriterStatusId==other.StoryWriterStatusId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotScreenTemplate other)
        {
			if(other!=null)
			{
				this.SlotScreenTemplateId=other.SlotScreenTemplateId;
				this.SlotId=other.SlotId;
				this.ScreenTemplateId=other.ScreenTemplateId;
				this.Name=other.Name;
				this.IsDefault=other.IsDefault;
				this.Html=other.Html;
				this.Description=other.Description;
				this.Duration=other.Duration;
				this.ThumbGuid=other.ThumbGuid;
				this.GroupId=other.GroupId;
				this.AssetId=other.AssetId;
				this.BackgroundColor=other.BackgroundColor;
				this.BackgroundImageUrl=other.BackgroundImageUrl;
				this.BackgroundRepeat=other.BackgroundRepeat;
				this.CreatonDate=other.CreatonDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Script=other.Script;
				this.SequenceNumber=other.SequenceNumber;
				this.IsAssignedToNle=other.IsAssignedToNle;
				this.IsAssignedToStoryWriter=other.IsAssignedToStoryWriter;
				this.VideoDuration=other.VideoDuration;
				this.ScriptDuration=other.ScriptDuration;
				this.Instructions=other.Instructions;
				this.ParentId=other.ParentId;
				this.VideoWallId=other.VideoWallId;
				this.IsVideoWallTemplate=other.IsVideoWallTemplate;
				this.StatusId=other.StatusId;
				this.NleStatusId=other.NleStatusId;
				this.StoryWriterStatusId=other.StoryWriterStatusId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
