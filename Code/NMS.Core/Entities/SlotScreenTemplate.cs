﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class SlotScreenTemplate : SlotScreenTemplateBase 
	{
        [DataMember(EmitDefaultValue = false)]
        public List<SlotTemplateScreenElement> SlotTemplateScreenElements { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<SlotScreenTemplateMic> SlotScreenTemplateMics { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<SlotScreenTemplateCamera> SlotScreenTemplateCameras { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<SlotScreenTemplateResource> SlotScreenTemplateResources { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<SlotTemplateScreenElementResource> SlotTemplateScreenElementResources { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<SlotScreenTemplatekey> SlotScreenTemplatekeys { get; set; }
	}
}
