﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotScreenTemplateCameraBase:EntityBase, IEquatable<SlotScreenTemplateCameraBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotScreenTemplateCameraId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotScreenTemplateCameraId{ get; set; }

		[FieldNameAttribute("SlotScreenTemplateId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotScreenTemplateId{ get; set; }

		[FieldNameAttribute("CameraTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 CameraTypeId{ get; set; }

		[FieldNameAttribute("Name",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("IsOn",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsOn{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotScreenTemplateCameraBase> Members

        public virtual bool Equals(SlotScreenTemplateCameraBase other)
        {
			if(this.SlotScreenTemplateCameraId==other.SlotScreenTemplateCameraId  && this.SlotScreenTemplateId==other.SlotScreenTemplateId  && this.CameraTypeId==other.CameraTypeId  && this.Name==other.Name  && this.IsOn==other.IsOn  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotScreenTemplateCamera other)
        {
			if(other!=null)
			{
				this.SlotScreenTemplateCameraId=other.SlotScreenTemplateCameraId;
				this.SlotScreenTemplateId=other.SlotScreenTemplateId;
				this.CameraTypeId=other.CameraTypeId;
				this.Name=other.Name;
				this.IsOn=other.IsOn;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
