﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotScreenTemplateResourceBase:EntityBase, IEquatable<SlotScreenTemplateResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotScreenTemplateResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotScreenTemplateResourceId{ get; set; }

		[FieldNameAttribute("SlotScreenTemplateId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotScreenTemplateId{ get; set; }

		[FieldNameAttribute("ResourceGuid",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid ResourceGuid{ get; set; }

		[FieldNameAttribute("Comment",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Comment{ get; set; }

		[FieldNameAttribute("IsUploadedFromNLE",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsUploadedFromNle{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("Caption",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Caption{ get; set; }

		[FieldNameAttribute("Category",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Category{ get; set; }

		[FieldNameAttribute("Location",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Location{ get; set; }

		[FieldNameAttribute("Duration",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? Duration{ get; set; }

		[FieldNameAttribute("ResourceTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ResourceTypeId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotScreenTemplateResourceBase> Members

        public virtual bool Equals(SlotScreenTemplateResourceBase other)
        {
			if(this.SlotScreenTemplateResourceId==other.SlotScreenTemplateResourceId  && this.SlotScreenTemplateId==other.SlotScreenTemplateId  && this.ResourceGuid==other.ResourceGuid  && this.Comment==other.Comment  && this.IsUploadedFromNle==other.IsUploadedFromNle  && this.CreationDate==other.CreationDate  && this.Caption==other.Caption  && this.Category==other.Category  && this.Location==other.Location  && this.Duration==other.Duration  && this.ResourceTypeId==other.ResourceTypeId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotScreenTemplateResource other)
        {
			if(other!=null)
			{
				this.SlotScreenTemplateResourceId=other.SlotScreenTemplateResourceId;
				this.SlotScreenTemplateId=other.SlotScreenTemplateId;
				this.ResourceGuid=other.ResourceGuid;
				this.Comment=other.Comment;
				this.IsUploadedFromNle=other.IsUploadedFromNle;
				this.CreationDate=other.CreationDate;
				this.Caption=other.Caption;
				this.Category=other.Category;
				this.Location=other.Location;
				this.Duration=other.Duration;
				this.ResourceTypeId=other.ResourceTypeId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
