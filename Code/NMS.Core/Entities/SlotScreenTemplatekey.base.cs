﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotScreenTemplatekeyBase:EntityBase, IEquatable<SlotScreenTemplatekeyBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotScreenTemplatekeyId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotScreenTemplatekeyId{ get; set; }

		[FieldNameAttribute("ScreenTemplatekeyId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ScreenTemplatekeyId{ get; set; }

		[FieldNameAttribute("SlotScreenTemplateId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlotScreenTemplateId{ get; set; }

		[FieldNameAttribute("KeyName",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String KeyName{ get; set; }

		[FieldNameAttribute("KeyValue",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String KeyValue{ get; set; }

		[FieldNameAttribute("creationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string creationDateStr
		{
            get { if (CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } } 
		}

		[FieldNameAttribute("updateddate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? Updateddate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string updateddateStr
		{
            get { if (Updateddate.HasValue) return Updateddate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Updateddate = date.ToUniversalTime(); } } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("languagecode",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Languagecode{ get; set; }

		[FieldNameAttribute("Top",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Top{ get; set; }

		[FieldNameAttribute("Bottom",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Bottom{ get; set; }

		[FieldNameAttribute("left",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Left{ get; set; }

		[FieldNameAttribute("Right",true,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double? Right{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotScreenTemplatekeyBase> Members

        public virtual bool Equals(SlotScreenTemplatekeyBase other)
        {
			if(this.SlotScreenTemplatekeyId==other.SlotScreenTemplatekeyId  && this.ScreenTemplatekeyId==other.ScreenTemplatekeyId  && this.SlotScreenTemplateId==other.SlotScreenTemplateId  && this.KeyName==other.KeyName  && this.KeyValue==other.KeyValue  && this.CreationDate==other.CreationDate  && this.Updateddate==other.Updateddate  && this.IsActive==other.IsActive  && this.Languagecode==other.Languagecode  && this.Top==other.Top  && this.Bottom==other.Bottom  && this.Left==other.Left  && this.Right==other.Right )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotScreenTemplatekey other)
        {
			if(other!=null)
			{
				this.SlotScreenTemplatekeyId=other.SlotScreenTemplatekeyId;
				this.ScreenTemplatekeyId=other.ScreenTemplatekeyId;
				this.SlotScreenTemplateId=other.SlotScreenTemplateId;
				this.KeyName=other.KeyName;
				this.KeyValue=other.KeyValue;
				this.CreationDate=other.CreationDate;
				this.Updateddate=other.Updateddate;
				this.IsActive=other.IsActive;
				this.Languagecode=other.Languagecode;
				this.Top=other.Top;
				this.Bottom=other.Bottom;
				this.Left=other.Left;
				this.Right=other.Right;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
