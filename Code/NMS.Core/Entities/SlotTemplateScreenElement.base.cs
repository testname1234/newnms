﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotTemplateScreenElementBase:EntityBase, IEquatable<SlotTemplateScreenElementBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotTemplateScreenElementId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotTemplateScreenElementId{ get; set; }

		[FieldNameAttribute("ScreenElementId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenElementId{ get; set; }

		[FieldNameAttribute("SlotScreenTemplateId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotScreenTemplateId{ get; set; }

		[FieldNameAttribute("Top",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Top{ get; set; }

		[FieldNameAttribute("Bottom",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Bottom{ get; set; }

		[FieldNameAttribute("Left",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Left{ get; set; }

		[FieldNameAttribute("Right",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Right{ get; set; }

		[FieldNameAttribute("ZIndex",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Zindex{ get; set; }

		[FieldNameAttribute("AssetId",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String AssetId{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid? ResourceGuid{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("CelebrityId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		[FieldNameAttribute("flashtemplatewindowid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Flashtemplatewindowid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotTemplateScreenElementBase> Members

        public virtual bool Equals(SlotTemplateScreenElementBase other)
        {
			if(this.SlotTemplateScreenElementId==other.SlotTemplateScreenElementId  && this.ScreenElementId==other.ScreenElementId  && this.SlotScreenTemplateId==other.SlotScreenTemplateId  && this.Top==other.Top  && this.Bottom==other.Bottom  && this.Left==other.Left  && this.Right==other.Right  && this.Zindex==other.Zindex  && this.AssetId==other.AssetId  && this.ResourceGuid==other.ResourceGuid  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.CelebrityId==other.CelebrityId  && this.Flashtemplatewindowid==other.Flashtemplatewindowid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotTemplateScreenElement other)
        {
			if(other!=null)
			{
				this.SlotTemplateScreenElementId=other.SlotTemplateScreenElementId;
				this.ScreenElementId=other.ScreenElementId;
				this.SlotScreenTemplateId=other.SlotScreenTemplateId;
				this.Top=other.Top;
				this.Bottom=other.Bottom;
				this.Left=other.Left;
				this.Right=other.Right;
				this.Zindex=other.Zindex;
				this.AssetId=other.AssetId;
				this.ResourceGuid=other.ResourceGuid;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.CelebrityId=other.CelebrityId;
				this.Flashtemplatewindowid=other.Flashtemplatewindowid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
