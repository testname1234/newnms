﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotTemplateScreenElementResourceBase:EntityBase, IEquatable<SlotTemplateScreenElementResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotTemplateScreenElementResourceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotTemplateScreenElementResourceId{ get; set; }

		[FieldNameAttribute("SlotTemplateScreenElementId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlotTemplateScreenElementId{ get; set; }

		[FieldNameAttribute("SlotScreenTemplateId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SlotScreenTemplateId{ get; set; }

		[FieldNameAttribute("ResourceGuid",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ResourceGuid{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("Caption",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Caption{ get; set; }

		[FieldNameAttribute("Category",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Category{ get; set; }

		[FieldNameAttribute("Location",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Location{ get; set; }

		[FieldNameAttribute("Duration",true,false,9)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Decimal? Duration{ get; set; }

		[FieldNameAttribute("ResourceTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ResourceTypeId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotTemplateScreenElementResourceBase> Members

        public virtual bool Equals(SlotTemplateScreenElementResourceBase other)
        {
			if(this.SlotTemplateScreenElementResourceId==other.SlotTemplateScreenElementResourceId  && this.SlotTemplateScreenElementId==other.SlotTemplateScreenElementId  && this.SlotScreenTemplateId==other.SlotScreenTemplateId  && this.ResourceGuid==other.ResourceGuid  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.Caption==other.Caption  && this.Category==other.Category  && this.Location==other.Location  && this.Duration==other.Duration  && this.ResourceTypeId==other.ResourceTypeId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotTemplateScreenElementResource other)
        {
			if(other!=null)
			{
				this.SlotTemplateScreenElementResourceId=other.SlotTemplateScreenElementResourceId;
				this.SlotTemplateScreenElementId=other.SlotTemplateScreenElementId;
				this.SlotScreenTemplateId=other.SlotScreenTemplateId;
				this.ResourceGuid=other.ResourceGuid;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.Caption=other.Caption;
				this.Category=other.Category;
				this.Location=other.Location;
				this.Duration=other.Duration;
				this.ResourceTypeId=other.ResourceTypeId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
