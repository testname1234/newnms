﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SlotTypeBase:EntityBase, IEquatable<SlotTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SlotTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SlotTypeId{ get; set; }

		[FieldNameAttribute("Name",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SlotTypeBase> Members

        public virtual bool Equals(SlotTypeBase other)
        {
			if(this.SlotTypeId==other.SlotTypeId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SlotType other)
        {
			if(other!=null)
			{
				this.SlotTypeId=other.SlotTypeId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
