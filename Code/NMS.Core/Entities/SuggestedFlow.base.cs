﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class SuggestedFlowBase:EntityBase, IEquatable<SuggestedFlowBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("SuggestedFlowId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 SuggestedFlowId{ get; set; }

		[FieldNameAttribute("Key",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Key{ get; set; }

		[FieldNameAttribute("ProgramId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("EpisodeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? EpisodeId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<SuggestedFlowBase> Members

        public virtual bool Equals(SuggestedFlowBase other)
        {
			if(this.SuggestedFlowId==other.SuggestedFlowId  && this.Key==other.Key  && this.ProgramId==other.ProgramId  && this.EpisodeId==other.EpisodeId  && this.CreationDate==other.CreationDate  && this.LastUpdatedDate==other.LastUpdatedDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(SuggestedFlow other)
        {
			if(other!=null)
			{
				this.SuggestedFlowId=other.SuggestedFlowId;
				this.Key=other.Key;
				this.ProgramId=other.ProgramId;
				this.EpisodeId=other.EpisodeId;
				this.CreationDate=other.CreationDate;
				this.LastUpdatedDate=other.LastUpdatedDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
