﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TagBase:EntityBase, IEquatable<TagBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TagId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TagId{ get; set; }

		[FieldNameAttribute("Rank",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Rank{ get; set; }

		[FieldNameAttribute("Tag",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Tag{ get; set; }

		[FieldNameAttribute("Guid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Guid{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TagBase> Members

        public virtual bool Equals(TagBase other)
        {
			if(this.TagId==other.TagId  && this.Rank==other.Rank  && this.Tag==other.Tag  && this.Guid==other.Guid  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Tag other)
        {
			if(other!=null)
			{
				this.TagId=other.TagId;
				this.Rank=other.Rank;
				this.Tag=other.Tag;
				this.Guid=other.Guid;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
