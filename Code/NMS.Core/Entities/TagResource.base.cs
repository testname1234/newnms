﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TagResourceBase:EntityBase, IEquatable<TagResourceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TagResourceid",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TagResourceid{ get; set; }

		[FieldNameAttribute("TagId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TagId{ get; set; }

		[FieldNameAttribute("ResourceId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ResourceId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("isactive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? Isactive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TagResourceBase> Members

        public virtual bool Equals(TagResourceBase other)
        {
			if(this.TagResourceid==other.TagResourceid  && this.TagId==other.TagId  && this.ResourceId==other.ResourceId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Isactive==other.Isactive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TagResource other)
        {
			if(other!=null)
			{
				this.TagResourceid=other.TagResourceid;
				this.TagId=other.TagId;
				this.ResourceId=other.ResourceId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.Isactive=other.Isactive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
