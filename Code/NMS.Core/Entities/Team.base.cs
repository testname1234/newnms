﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TeamBase:EntityBase, IEquatable<TeamBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TeamId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TeamId{ get; set; }

		[FieldNameAttribute("UserId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 UserId{ get; set; }

		[FieldNameAttribute("WorkRoleId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WorkRoleId{ get; set; }

		[FieldNameAttribute("ProgramId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ProgramId{ get; set; }

		[FieldNameAttribute("TeamRoleId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TeamRoleId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TeamBase> Members

        public virtual bool Equals(TeamBase other)
        {
			if(this.TeamId==other.TeamId  && this.UserId==other.UserId  && this.WorkRoleId==other.WorkRoleId  && this.ProgramId==other.ProgramId  && this.TeamRoleId==other.TeamRoleId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Team other)
        {
			if(other!=null)
			{
				this.TeamId=other.TeamId;
				this.UserId=other.UserId;
				this.WorkRoleId=other.WorkRoleId;
				this.ProgramId=other.ProgramId;
				this.TeamRoleId=other.TeamRoleId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
