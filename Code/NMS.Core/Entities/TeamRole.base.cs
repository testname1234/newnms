﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TeamRoleBase:EntityBase, IEquatable<TeamRoleBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TeamRoleId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TeamRoleId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TeamRoleBase> Members

        public virtual bool Equals(TeamRoleBase other)
        {
			if(this.TeamRoleId==other.TeamRoleId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TeamRole other)
        {
			if(other!=null)
			{
				this.TeamRoleId=other.TeamRoleId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
