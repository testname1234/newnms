﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Teleprompter.Entities
{
    public class RunOrder
    {
        public string ID { get; set; }

        public string Slug { get; set; }

        public string StartTime { get; set; }
        
        public string Duration { get; set; }

        public List<NMS.Core.Teleprompter.Entities.Story> Stories { get; set; }
    }
}
