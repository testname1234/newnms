﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Teleprompter.Entities
{
    public class Story
    {
        public Story() 
        {
           IsSkipped = false;
        }

        public string ID { get; set; }

        public double SequenceId { get; set; }

        public bool IsPlayed { get; set; }

        public bool IsSkipped { get; set; }

        public string RoId { get; set; }
 
        public string Slug { get; set; }

        public List<NMS.Core.Teleprompter.Entities.StoryItem> StoryItems { get; set; }

    }
}
