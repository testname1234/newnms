﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Teleprompter.Entities
{
    public class StoryItem
    {

        public string ID { get; set; }

        public string StoryId { get; set; }

        public int SequenceId { get; set; }

        public string Slug { get; set; }

        public string Detail { get; set; }


        public string Instructions { get; set; }

    }
}
