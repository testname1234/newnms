﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TemplateScreenElementBase:EntityBase, IEquatable<TemplateScreenElementBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TemplateScreenElementId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TemplateScreenElementId{ get; set; }

		[FieldNameAttribute("ScreenElementId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ScreenElementId{ get; set; }

		[FieldNameAttribute("Top",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Top{ get; set; }

		[FieldNameAttribute("Bottom",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Bottom{ get; set; }

		[FieldNameAttribute("Left",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Left{ get; set; }

		[FieldNameAttribute("Right",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Right{ get; set; }

		[FieldNameAttribute("ZIndex",false,false,8)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Double Zindex{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("ScreenTemplateId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ScreenTemplateId{ get; set; }

		[FieldNameAttribute("flashtemplatewindowid",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Flashtemplatewindowid{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TemplateScreenElementBase> Members

        public virtual bool Equals(TemplateScreenElementBase other)
        {
			if(this.TemplateScreenElementId==other.TemplateScreenElementId  && this.ScreenElementId==other.ScreenElementId  && this.Top==other.Top  && this.Bottom==other.Bottom  && this.Left==other.Left  && this.Right==other.Right  && this.Zindex==other.Zindex  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.ScreenTemplateId==other.ScreenTemplateId  && this.Flashtemplatewindowid==other.Flashtemplatewindowid )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TemplateScreenElement other)
        {
			if(other!=null)
			{
				this.TemplateScreenElementId=other.TemplateScreenElementId;
				this.ScreenElementId=other.ScreenElementId;
				this.Top=other.Top;
				this.Bottom=other.Bottom;
				this.Left=other.Left;
				this.Right=other.Right;
				this.Zindex=other.Zindex;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.ScreenTemplateId=other.ScreenTemplateId;
				this.Flashtemplatewindowid=other.Flashtemplatewindowid;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
