﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class TemplateScreenElement : TemplateScreenElementBase 
	{
        public int ProgramScreenElementId { get; set; }

        public Guid ImageGuid { get; set; }
	
		
	}
}
