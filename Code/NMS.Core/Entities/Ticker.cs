﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NMS.Core.Entities
{
    [DataContract]
    public partial class Ticker : TickerBase
    {
        public List<TickerLine> TickerLines { get; set; }

        public int CreatedDuration { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string itemType { get { return "Ticker"; } set { } }

        [DataMember(EmitDefaultValue = false)]
        public int McrTickerCategoryId {get;set;}

        [DataMember(EmitDefaultValue = false)]
        public string McrTickerCategoryName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Translation { get; set; }
    }
}
