﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerCategoryBase:EntityBase, IEquatable<TickerCategoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerCategoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerCategoryId{ get; set; }

		[FieldNameAttribute("CategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		[FieldNameAttribute("Name",true,false,510)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("SequenceNumber",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SequenceNumber{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("ParentTickerCategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentTickerCategoryId{ get; set; }

		[FieldNameAttribute("LanguageCode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LanguageCode{ get; set; }

		[FieldNameAttribute("TickerSize",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerSize{ get; set; }

		[FieldNameAttribute("TicketCategoryTypeId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TicketCategoryTypeId{ get; set; }

		[FieldNameAttribute("EntityIds",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String EntityIds{ get; set; }

		[FieldNameAttribute("TagIds",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TagIds{ get; set; }

		[FieldNameAttribute("LocationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("TabName",true,false,200)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TabName{ get; set; }

		[FieldNameAttribute("HourPolicy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? HourPolicy{ get; set; }

		[FieldNameAttribute("NewTickerPolicy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewTickerPolicy{ get; set; }

		[FieldNameAttribute("MCRTickerCategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? McrTickerCategoryId{ get; set; }

		[FieldNameAttribute("IsRepeat",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsRepeat{ get; set; }

		[FieldNameAttribute("Priority",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Priority{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerCategoryBase> Members

        public virtual bool Equals(TickerCategoryBase other)
        {
			if(this.TickerCategoryId==other.TickerCategoryId  && this.CategoryId==other.CategoryId  && this.Name==other.Name  && this.SequenceNumber==other.SequenceNumber  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.ParentTickerCategoryId==other.ParentTickerCategoryId  && this.LanguageCode==other.LanguageCode  && this.TickerSize==other.TickerSize  && this.TicketCategoryTypeId==other.TicketCategoryTypeId  && this.EntityIds==other.EntityIds  && this.TagIds==other.TagIds  && this.LocationId==other.LocationId  && this.TabName==other.TabName  && this.HourPolicy==other.HourPolicy  && this.NewTickerPolicy==other.NewTickerPolicy  && this.McrTickerCategoryId==other.McrTickerCategoryId  && this.IsRepeat==other.IsRepeat  && this.Priority==other.Priority )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerCategory other)
        {
			if(other!=null)
			{
				this.TickerCategoryId=other.TickerCategoryId;
				this.CategoryId=other.CategoryId;
				this.Name=other.Name;
				this.SequenceNumber=other.SequenceNumber;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.ParentTickerCategoryId=other.ParentTickerCategoryId;
				this.LanguageCode=other.LanguageCode;
				this.TickerSize=other.TickerSize;
				this.TicketCategoryTypeId=other.TicketCategoryTypeId;
				this.EntityIds=other.EntityIds;
				this.TagIds=other.TagIds;
				this.LocationId=other.LocationId;
				this.TabName=other.TabName;
				this.HourPolicy=other.HourPolicy;
				this.NewTickerPolicy=other.NewTickerPolicy;
				this.McrTickerCategoryId=other.McrTickerCategoryId;
				this.IsRepeat=other.IsRepeat;
				this.Priority=other.Priority;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
