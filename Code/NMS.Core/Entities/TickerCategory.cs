﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;
using NMS.Core.Enums;

namespace NMS.Core.Entities
{
    [DataContract]
	public partial class TickerCategory : TickerCategoryBase 
	{
        public  List<TickerCategory> TickerCategories = new List<TickerCategory>();
        [DataMember(EmitDefaultValue = false)]
        public List<Tag> Tags = new List<Tag>();
        [DataMember(EmitDefaultValue = false)]
        public List<Organization> Organizations = new List<Organization>();
        [DataMember(EmitDefaultValue = false)]
        public List<Location> Locations = new List<Location>();
        [DataMember(EmitDefaultValue = false)]
        public string McrTickerCategoryName { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Language
        {
            get
            {
                if (LanguageCode == 1) return "UR";
                else return "EN";
            }
            set
            {
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public string TickerType
        {
            get
            {
                if (TicketCategoryTypeId == (int)TickerCategoryType.Custom) return "Custom";
                if (TicketCategoryTypeId == (int)TickerCategoryType.Face) return "Face";
                if (TicketCategoryTypeId == (int)TickerCategoryType.Manual) return "Manual";
                if (TicketCategoryTypeId == (int)TickerCategoryType.Location) return "Location";
                if (TicketCategoryTypeId == (int)TickerCategoryType.Topic) return "Topic";
                if (TicketCategoryTypeId == (int)TickerCategoryType.Entity) return "Entity";
                return "";
            }
            set
            {
            }
        }

    }
}
