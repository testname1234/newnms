﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerOnAirLogBase:EntityBase, IEquatable<TickerOnAirLogBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerOnAirLogId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerOnAirLogId{ get; set; }

		[FieldNameAttribute("TickerId",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TickerId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerOnAirLogBase> Members

        public virtual bool Equals(TickerOnAirLogBase other)
        {
			if(this.TickerOnAirLogId==other.TickerOnAirLogId  && this.TickerId==other.TickerId  && this.CreationDate==other.CreationDate  && this.UserId==other.UserId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerOnAirLog other)
        {
			if(other!=null)
			{
				this.TickerOnAirLogId=other.TickerOnAirLogId;
				this.TickerId=other.TickerId;
				this.CreationDate=other.CreationDate;
				this.UserId=other.UserId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
