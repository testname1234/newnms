﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerStatusBase:EntityBase, IEquatable<TickerStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerStatusId{ get; set; }

		[FieldNameAttribute("TickerStatusName",true,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TickerStatusName{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerStatusBase> Members

        public virtual bool Equals(TickerStatusBase other)
        {
			if(this.TickerStatusId==other.TickerStatusId  && this.TickerStatusName==other.TickerStatusName )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerStatus other)
        {
			if(other!=null)
			{
				this.TickerStatusId=other.TickerStatusId;
				this.TickerStatusName=other.TickerStatusName;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
