﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerTranslationBase:EntityBase, IEquatable<TickerTranslationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerTranslationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerTranslationId{ get; set; }

		[FieldNameAttribute("TickerId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerId{ get; set; }

		[FieldNameAttribute("Text",true,false,2000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Text{ get; set; }

		[FieldNameAttribute("LanguageId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 LanguageId{ get; set; }

		[FieldNameAttribute("StatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		[FieldNameAttribute("LastUpdatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("CreatedBy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CreatedBy{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerTranslationBase> Members

        public virtual bool Equals(TickerTranslationBase other)
        {
			if(this.TickerTranslationId==other.TickerTranslationId  && this.TickerId==other.TickerId  && this.Text==other.Text  && this.LanguageId==other.LanguageId  && this.StatusId==other.StatusId  && this.LastUpdatedDate==other.LastUpdatedDate  && this.IsActive==other.IsActive  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerTranslation other)
        {
			if(other!=null)
			{
				this.TickerTranslationId=other.TickerTranslationId;
				this.TickerId=other.TickerId;
				this.Text=other.Text;
				this.LanguageId=other.LanguageId;
				this.StatusId=other.StatusId;
				this.LastUpdatedDate=other.LastUpdatedDate;
				this.IsActive=other.IsActive;
				this.CreatedBy=other.CreatedBy;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
