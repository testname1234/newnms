﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerTypeBase:EntityBase, IEquatable<TickerTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerTypeId{ get; set; }

		[FieldNameAttribute("Type",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Type{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerTypeBase> Members

        public virtual bool Equals(TickerTypeBase other)
        {
			if(this.TickerTypeId==other.TickerTypeId  && this.Type==other.Type )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerType other)
        {
			if(other!=null)
			{
				this.TickerTypeId=other.TickerTypeId;
				this.Type=other.Type;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
