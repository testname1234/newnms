﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TranslatedScriptBase:EntityBase, IEquatable<TranslatedScriptBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TranslatedScriptId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TranslatedScriptId{ get; set; }

		[FieldNameAttribute("NewsFileId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsFileId{ get; set; }

		[FieldNameAttribute("Title",true,false,700)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Title{ get; set; }

		[FieldNameAttribute("VoiceOver",true,false,700)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String VoiceOver{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreatedBy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CreatedBy{ get; set; }

		[FieldNameAttribute("Status",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? Status{ get; set; }

		[FieldNameAttribute("LanguageCode",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LanguageCode{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TranslatedScriptBase> Members

        public virtual bool Equals(TranslatedScriptBase other)
        {
			if(this.TranslatedScriptId==other.TranslatedScriptId  && this.NewsFileId==other.NewsFileId  && this.Title==other.Title  && this.VoiceOver==other.VoiceOver  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.Status==other.Status  && this.LanguageCode==other.LanguageCode )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TranslatedScript other)
        {
			if(other!=null)
			{
				this.TranslatedScriptId=other.TranslatedScriptId;
				this.NewsFileId=other.NewsFileId;
				this.Title=other.Title;
				this.VoiceOver=other.VoiceOver;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.CreatedBy=other.CreatedBy;
				this.Status=other.Status;
				this.LanguageCode=other.LanguageCode;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
