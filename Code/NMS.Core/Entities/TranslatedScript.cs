﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;


namespace NMS.Core.Entities
{
    [DataContract]
	public partial class TranslatedScript : TranslatedScriptBase 
	{

        [FieldNameAttribute("Comment", true, false, 700)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Comment { get; set; }

    }
}
