﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TwitterAccountBase:EntityBase, IEquatable<TwitterAccountBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TwitterAccountId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TwitterAccountId{ get; set; }

		[FieldNameAttribute("UserName",false,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String UserName{ get; set; }

		[FieldNameAttribute("CelebrityId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CelebrityId{ get; set; }

		[FieldNameAttribute("Url",true,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("IsFollowed",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsFollowed{ get; set; }

		[FieldNameAttribute("CategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		[FieldNameAttribute("LocationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TwitterAccountBase> Members

        public virtual bool Equals(TwitterAccountBase other)
        {
			if(this.TwitterAccountId==other.TwitterAccountId  && this.UserName==other.UserName  && this.CelebrityId==other.CelebrityId  && this.Url==other.Url  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsFollowed==other.IsFollowed  && this.CategoryId==other.CategoryId  && this.LocationId==other.LocationId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TwitterAccount other)
        {
			if(other!=null)
			{
				this.TwitterAccountId=other.TwitterAccountId;
				this.UserName=other.UserName;
				this.CelebrityId=other.CelebrityId;
				this.Url=other.Url;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsFollowed=other.IsFollowed;
				this.CategoryId=other.CategoryId;
				this.LocationId=other.LocationId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
