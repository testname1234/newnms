﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class VideoCutterIpBase:EntityBase, IEquatable<VideoCutterIpBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("VideoCutterIpId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 VideoCutterIpId{ get; set; }

		[FieldNameAttribute("UserIP",false,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String UserIp{ get; set; }

		[FieldNameAttribute("Host",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Host{ get; set; }

		[FieldNameAttribute("VideoCutterVersion",true,false,20)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String VideoCutterVersion{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<VideoCutterIpBase> Members

        public virtual bool Equals(VideoCutterIpBase other)
        {
			if(this.VideoCutterIpId==other.VideoCutterIpId  && this.UserIp==other.UserIp  && this.Host==other.Host  && this.VideoCutterVersion==other.VideoCutterVersion  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(VideoCutterIp other)
        {
			if(other!=null)
			{
				this.VideoCutterIpId=other.VideoCutterIpId;
				this.UserIp=other.UserIp;
				this.Host=other.Host;
				this.VideoCutterVersion=other.VideoCutterVersion;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
