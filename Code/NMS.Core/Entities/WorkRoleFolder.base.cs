﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class WorkRoleFolderBase:EntityBase, IEquatable<WorkRoleFolderBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("WorkRoleFolderId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WorkRoleFolderId{ get; set; }

		[FieldNameAttribute("WorkRoleId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 WorkRoleId{ get; set; }

		[FieldNameAttribute("FolderId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 FolderId{ get; set; }

		[FieldNameAttribute("AllowRead",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean AllowRead{ get; set; }

		[FieldNameAttribute("AllowModify",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean AllowModify{ get; set; }

		[FieldNameAttribute("AllowCreate",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean AllowCreate{ get; set; }

		[FieldNameAttribute("AllowCopy",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean AllowCopy{ get; set; }

		[FieldNameAttribute("AllowMove",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean AllowMove{ get; set; }

		[FieldNameAttribute("AllowDelete",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean AllowDelete{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("MaxColor",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 MaxColor{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<WorkRoleFolderBase> Members

        public virtual bool Equals(WorkRoleFolderBase other)
        {
			if(this.WorkRoleFolderId==other.WorkRoleFolderId  && this.WorkRoleId==other.WorkRoleId  && this.FolderId==other.FolderId  && this.AllowRead==other.AllowRead  && this.AllowModify==other.AllowModify  && this.AllowCreate==other.AllowCreate  && this.AllowCopy==other.AllowCopy  && this.AllowMove==other.AllowMove  && this.AllowDelete==other.AllowDelete  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.MaxColor==other.MaxColor )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(WorkRoleFolder other)
        {
			if(other!=null)
			{
				this.WorkRoleFolderId=other.WorkRoleFolderId;
				this.WorkRoleId=other.WorkRoleId;
				this.FolderId=other.FolderId;
				this.AllowRead=other.AllowRead;
				this.AllowModify=other.AllowModify;
				this.AllowCreate=other.AllowCreate;
				this.AllowCopy=other.AllowCopy;
				this.AllowMove=other.AllowMove;
				this.AllowDelete=other.AllowDelete;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.MaxColor=other.MaxColor;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
