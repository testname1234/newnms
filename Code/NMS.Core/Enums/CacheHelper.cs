﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace NMS.Core.Enums
{
    public enum CacheKey
    {
        [DescriptionAttribute("FilterCount")]    
        FilterCount = 1,

        [DescriptionAttribute("CacheKeyListForDataPolling")]
        CacheKeyListForDataPolling = 2,

        [DescriptionAttribute("BunchIDCount")]    
        BunchIDCount = 3,

        [DescriptionAttribute("GetNewsAndBunch")]
        GetNewsAndBunch = 4

    }

    public static class ExtensionMethods
    {

        public static string ToDescription(this Enum en) //ext method
        {

            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {

                object[] attrs = memInfo[0].GetCustomAttributes(
                typeof(DescriptionAttribute),

                false);

                if (attrs != null && attrs.Length > 0)

                    return ((DescriptionAttribute)attrs[0]).Description;

            }

            return en.ToString();

        }
    }
}
