﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace NMS.Core.Enums
{
    public enum CasperImportScreenTemplate
    {
        [DescriptionAttribute("Local CasparCG")]
        DeviceName = 4,

        [DescriptionAttribute("Template")]
        Template = 2,

        [DescriptionAttribute("DECKLINK INPUT")]
        decklink = 3,

        [DescriptionAttribute("Anchor")]
        Anchor = 5,

        [DescriptionAttribute("Video")]
        Video = 6,

        [DescriptionAttribute("Guest")]
        Guest = 7,

        [DescriptionAttribute("Transformation")]
        Transformation = 8,


        channel = 1
    }



}