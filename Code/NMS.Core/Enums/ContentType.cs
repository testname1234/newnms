﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum ContentType
    {
        Image = 1,
        Audio,
        Video,
        Document
    }
}
