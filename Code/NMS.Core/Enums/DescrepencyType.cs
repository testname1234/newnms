﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum DescrepencyType
    {
        Category = 1,
        Location = 2
    }
}
