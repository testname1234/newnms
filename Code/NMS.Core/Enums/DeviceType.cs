﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum DeviceType
    {
        Teleprompter = 1,
        WindowGraphics = 2,
        NameBrandingGraphic = 3,
        Playout = 4
    }
}
