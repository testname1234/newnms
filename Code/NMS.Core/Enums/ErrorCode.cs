﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum ErrorCode
    {
        InvalidInputProvided = 1001,
        InvalidCredentialsProvided = 1002,
        AuthenticationFailed = 1003,
        SessionKeyExpired = 1004,
        InvalidSessionKey = 1005
    }
}
