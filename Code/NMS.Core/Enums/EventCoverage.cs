﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum EventCoverage
    {
        Live = 0,
        Recording = 1
    }
}
