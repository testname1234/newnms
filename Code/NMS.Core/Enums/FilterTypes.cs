﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum FilterTypes
    {
        Website = 1,
        Radio = 2,
        SocialMedia = 3,
        NewsPaper = 4,
        PublicReporter = 6,
        FieldReporter = 7,
        Package = 8,
        Wire = 9,
        Channel = 10,
        Records = 11,
        PressRelease = 12,
        Category = 13,
        NewsFilter = 15,
        AllNews = 16,
        ArchivalSystem = 17,
        Program = 18,
        NewsMedia = 19,
        ReportedNews = 20,
        Event = 21
    }

    public enum NewsFilters
    {
        AllNews = 78,
        Verified = 79,
        NotVerified = 80,
        Rejected = 81,
        Recommended = 449,
        TopRated = 450,
        TopExecuted = 451,
        MyNews=739
    }
}
