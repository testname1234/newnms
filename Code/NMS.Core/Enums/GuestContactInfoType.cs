﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum GuestContactInfoType
    {
        PhoneNumber =1,
        Email = 2,
        SkypeName =3
    }
}
