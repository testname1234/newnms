﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum InsertNewsStatus
    {
        Inserted = 1,        
        DuplicateNews = 2,
        DiscrepancyCategory = 3,
        DiscrepancyLocation = 4
    }
}
