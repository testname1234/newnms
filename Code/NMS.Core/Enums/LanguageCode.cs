﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum LanguageCode
    {
        Urdu = 1,
        English = 2
    }
}
