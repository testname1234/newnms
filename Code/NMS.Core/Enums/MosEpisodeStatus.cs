﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace NMS.Core.Enums
{
    public enum MosEpisodeStatus
    {
        Pending = 1,
        Updated = 2,
        SentToPcr = 3,
        Error = 4        
    }


}
