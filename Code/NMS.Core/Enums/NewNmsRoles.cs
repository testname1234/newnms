﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum NewNmsRoles
    {
        Reporter=1,
        CopyWriter=2,
        Production=3
    }
}
