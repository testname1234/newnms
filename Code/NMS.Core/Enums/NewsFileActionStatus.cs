﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum NewsFileActionStatus
    {
        Copy = 1,
        Move = 2
    }
}
