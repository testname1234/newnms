﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum NewsFileStatus
    {
        Red = 1,
        Blue = 2,
        Yellow = 3,
        Green = 4,
        CopyWriterAndProductionApproved = 5,
        Deleted=7,
        
    }
}


