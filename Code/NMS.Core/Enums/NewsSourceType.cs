﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum NewsSourceType
    {
        Reporter = 1,
        PublicReporter,
        Wire,
        NewsPaper,
        Website,
        TVChannel,
        SocialMedia,
        Radio,
        Magazine
    }
}
