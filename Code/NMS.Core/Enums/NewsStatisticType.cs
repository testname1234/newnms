﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum NewsStatisticType
    {
        AddedToRundown = 1,
        OnAired,
        ExecutedOnOtherChannels,
        NewsTickerCount
    }
}
