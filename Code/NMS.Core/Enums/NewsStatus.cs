﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum NewsStatus
    {
        BreakingNews =1,
        ImportantNews = 2,
        NormalNews =3
    }
}
