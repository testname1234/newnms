﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum NewsTypes
    {
        Story = 1,
        Package,
        Assignment
    }
}
