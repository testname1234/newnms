﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum NotificationArguments
    {
        SlotId = 1,
        SlotScreenTemplateId,
        StorySlug
    }
}
