﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum NotificationTypes
    {
        TaskAssigned = 1,
        TaskSubmitted,
        TaskRevision,
        TaskApproved
    }
}
