﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum ProgramRecordingStatus
    {
      New = 1,
      Clipped = 2,
      Scripted = 3,
      ClipReject = 4,
      ScriptReject = 5,
      Approved = 6,
      Dubbed = 7,
      Completed = 8
    }

    public enum TranslatedScriptStatus
    {
        Reject = 1,
        Approvepending = 2,
        Approved = 3
    }
}
