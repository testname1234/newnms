﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core
{
    public static class RabbitChannels
    {
        public static string Program = "program";
    }
    public class RabbitRoutingKey
    {
        public static class ProgramEditorial
        {
            public static class Routes
            {
                public static string Slot = "programeditorial.slot";
                public static string Episode = "programeditorial.episode";
                public static string Program = "programeditorial.program";
            }
            public static class Events
            {
                public static string Slot_FirstComment = "Slot_FirstCommentAdded";
                public static string Episode_InfoChanged = "Episode_InfoChanged";
                public static string Episode_FirstSlot = "Episode_FirstSlot";
            }
        }
       
    }

}
