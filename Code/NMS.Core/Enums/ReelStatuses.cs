﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum ReelStatuses
    {
        Created = 1,
        Processed = 2,
        Completed = 3,
        TranscodingRequired = 4
    }
}
