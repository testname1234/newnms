﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public static class RegExpressions
    {

        private static string listExp = "(.*)(   )(.*)";
        private static string urlExp = "(.*href=\")(.*)(\">.*)";
        private static string htmlTagsFilteration = "<.*?>";
        private static string mediaExtraction = "src=(\"|')((?!><).)*?(\\.jpg|\\.gif|\\.png|\\.mp4|\\.flv|\\.jpeg)";
        private static string videoExtraction = ".*(mp4|flv)";
        private static string scriptsRemoval = "(<script(.*?)</script>|<style(.*?)</style>)";        

        public static string ListExp
        {
            get
            {
                return listExp;
            }
            set
            {
                listExp = value;
            }
        }
        public static string UrlExp
        {
            get
            {
                return urlExp;
            }
            set
            {
                urlExp = value;
            }
        }
        public static string HtmlTagsFilteration
        {
            get
            {
                return htmlTagsFilteration;
            }
            set
            {
                htmlTagsFilteration = value;
            }
        }
        public static string MediaExtraction
        {
            get
            {
                return mediaExtraction;
            }
            set
            {
                mediaExtraction = value;
            }
        }
        public static string VideoExtraction
        {
            get
            {
                return videoExtraction;
            }
            set
            {
                videoExtraction = value;
            }
        }
        public static string ScriptsRemoval
        {
            get
            {
                return scriptsRemoval;
            }
            set
            {
                scriptsRemoval = value;
            }
        }



    }
}
