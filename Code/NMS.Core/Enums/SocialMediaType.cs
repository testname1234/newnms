﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum SocialMediaType
    {
        FaceBook=1,
        Twitter=2,
        WhatsApp=3
    }
}
