﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum SortDirection
    {
        Up = 1,
        Down = 2,
    }
}
