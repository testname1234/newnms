﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Enums
{
    public enum TeamRoles
    {
        Producer = 5,
        StoryWriter = 21,
        NLE = 20,
        TickerWriter = 49,
        TickerManager = 50,
        TickerProducer	= 51,
        HeadlineProducer =	52
    }
}
