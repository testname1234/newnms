﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum TickerCategoryType
    {
        Custom = 1,
        Manual = 2,
        Location = 3,
        Topic = 4,
        Face = 5,
        Entity = 6
    }
}
