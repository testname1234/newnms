﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum TickerStatuses
    {
        Approved = 1,
        OnHold,
        Rejected,
        Deleted,
        Pending,
        OnAired,
        OnAir,
        Locked
    }
}
