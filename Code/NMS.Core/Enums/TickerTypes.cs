﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{

    public enum TickerTypes
    {
        Breaking = 1,
        Latest = 2,
        Category = 3,
        BreakingA = 13
    }
}
