﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum TranslationStatus
    {
        New = 1,
        ApprovalPending = 2,
        Approved = 3,
        Rejected = 4
    }
}
