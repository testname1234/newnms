﻿using System;

namespace NMS.Core.Enums
{
    public enum Via
    {
        PublicReporter = 1,
        FieldReporter = 2
    }
}
