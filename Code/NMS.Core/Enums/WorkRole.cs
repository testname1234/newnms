﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Enums
{
    public enum WorkRole
    {
        ControllerOutput = 1073,
        ControllerInput = 1072,
        TickerCopyWriterOneA = 1082,
        TickerProducerOneA = 1078,
        TickerAdmin = 2081,
        EnglishTickerWriter = 3081,
        EnglishTickerEditorial = 3082,
        EnglishTickerProducer = 3087,
        SocialFilter = 2074,
        TaggedUser =2072,
        TaggerReport = 2073,
        CopyWriter = 2075,
        Editorial = 2076,
        TickerEditorial = 2080,
        ProgramManager = 1073
    }
}
