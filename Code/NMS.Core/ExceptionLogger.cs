﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using NMS.Core.DataInterfaces;

namespace NMS.Core
{
    public class ExceptionLogger
    {
        public static void Log(Exception exp)
        {
            Console.WriteLine(DateTime.UtcNow.ToString() + "->" + exp.Message);
            if (HttpContext.Current != null)
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            else
            {
                IExceptionRepository iExceptionRepo = IoC.Resolve<IExceptionRepository>("ExceptionRepository");
                iExceptionRepo.InsertException(exp);
            }
        }
        public static bool LogJsError(string msg, string url, string linenumber, string user, string type,string ip)
        {
            IExceptionRepository iExceptionRepo = IoC.Resolve<IExceptionRepository>("ExceptionRepository");
            return iExceptionRepo.JSErrorLogging(msg, url, linenumber, user, type,ip);
        }

        public static void LogErrorFromMCR(string exp)
        {
                IExceptionRepository iExceptionRepo = IoC.Resolve<IExceptionRepository>("ExceptionRepository");
                //exp.Source = "MCR-Room";
                iExceptionRepo.MCRErrorLogging(exp);
        }
    }
}
