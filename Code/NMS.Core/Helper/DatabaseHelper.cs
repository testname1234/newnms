﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace NMS.Core.Helper
{
    public class DatabaseHelper
    {
        public static bool CreateDatabaseFromDbMigrate(string clientCode)
        {
            using (Process process = new Process())
            {
                process.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "\\migrate.exe";
                process.StartInfo.Arguments = string.Format("nobackup {0}", clientCode);
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.EnableRaisingEvents = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.RedirectStandardInput = true;
                process.Start();
                process.BeginOutputReadLine();
                process.WaitForExit(200000);
            }
            return true;
        }

        public static bool CreateDatabase(string dbName, string filePath, string connectionString)
        {

            string sqlCreateDBQuery = @" CREATE DATABASE <DatabaseName> ON  PRIMARY 
( NAME = N'<DatabaseName>', FILENAME = N'<FilePath><DatabaseName>.mdf'  , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'<DatabaseName>_log', FILENAME = N'<FilePath><DatabaseName>_log.ldf'  , MAXSIZE = 2048GB , FILEGROWTH = 10%);
EXEC dbo.sp_dbcmptlevel @dbname=N'<DatabaseName>', @new_cmptlevel=90;
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC <DatabaseName>.[dbo].[sp_fulltext_database] @action = 'disable';
end
ALTER DATABASE <DatabaseName> SET ANSI_NULL_DEFAULT OFF;
ALTER DATABASE <DatabaseName> SET ANSI_NULLS OFF;
ALTER DATABASE <DatabaseName> SET ANSI_PADDING OFF;
ALTER DATABASE <DatabaseName> SET ANSI_WARNINGS OFF;
ALTER DATABASE <DatabaseName> SET ARITHABORT OFF;
ALTER DATABASE <DatabaseName> SET AUTO_CLOSE OFF;
ALTER DATABASE <DatabaseName> SET AUTO_CREATE_STATISTICS ON;
ALTER DATABASE <DatabaseName> SET AUTO_SHRINK OFF;
ALTER DATABASE <DatabaseName> SET AUTO_UPDATE_STATISTICS ON;
ALTER DATABASE <DatabaseName> SET CURSOR_CLOSE_ON_COMMIT OFF;
ALTER DATABASE <DatabaseName> SET CURSOR_DEFAULT  GLOBAL;
ALTER DATABASE <DatabaseName> SET CONCAT_NULL_YIELDS_NULL OFF;
ALTER DATABASE <DatabaseName> SET NUMERIC_ROUNDABORT OFF;
ALTER DATABASE <DatabaseName> SET QUOTED_IDENTIFIER OFF;
ALTER DATABASE <DatabaseName> SET RECURSIVE_TRIGGERS OFF;
ALTER DATABASE <DatabaseName> SET  DISABLE_BROKER;
ALTER DATABASE <DatabaseName> SET AUTO_UPDATE_STATISTICS_ASYNC OFF;
ALTER DATABASE <DatabaseName> SET DATE_CORRELATION_OPTIMIZATION OFF;
ALTER DATABASE <DatabaseName> SET TRUSTWORTHY OFF;
ALTER DATABASE <DatabaseName> SET ALLOW_SNAPSHOT_ISOLATION OFF;
ALTER DATABASE <DatabaseName> SET PARAMETERIZATION SIMPLE;
ALTER DATABASE <DatabaseName> SET READ_COMMITTED_SNAPSHOT OFF;
ALTER DATABASE <DatabaseName> SET  READ_WRITE;
ALTER DATABASE <DatabaseName> SET RECOVERY FULL;
ALTER DATABASE <DatabaseName> SET  MULTI_USER;
ALTER DATABASE <DatabaseName> SET PAGE_VERIFY CHECKSUM;
ALTER DATABASE <DatabaseName> SET DB_CHAINING OFF;
";

            using (SqlConnection tmpConn = new SqlConnection())
            {
                tmpConn.ConnectionString = connectionString;
                sqlCreateDBQuery = sqlCreateDBQuery.Replace("<DatabaseName>", dbName).Replace("<FilePath>", filePath);

                SqlCommand myCommand = new SqlCommand(sqlCreateDBQuery, tmpConn);
                try
                {
                    myCommand.CommandTimeout = 0;
                    tmpConn.Open();
                    myCommand.ExecuteNonQuery();
                    return true;
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }

            }

        }

        public static bool RestoreDatabase(string databaseName, string backupPath, string filePath, string connectionString)
        {
            using (SqlConnection tmpConn = new SqlConnection())
            {
                tmpConn.ConnectionString = connectionString;
                string sql = string.Format(@"
DECLARE @Table TABLE (LogicalName varchar(128),[PhysicalName] varchar(128), [Type] varchar, [FileGroupName] varchar(128), [Size] varchar(128), 
            [MaxSize] varchar(128), [FileId]varchar(128), [CreateLSN]varchar(128), [DropLSN]varchar(128), [UniqueId]varchar(128), [ReadOnlyLSN]varchar(128), [ReadWriteLSN]varchar(128), 
            [BackupSizeInBytes]varchar(128), [SourceBlockSize]varchar(128), [FileGroupId]varchar(128), [LogGroupGUID]varchar(128), [DifferentialBaseLSN]varchar(128), [DifferentialBaseGUID]varchar(128), [IsReadOnly]varchar(128), [IsPresent]varchar(128), [TDEThumbprint]varchar(128)
)

INSERT INTO @Table
EXEC ('restore filelistonly
 from disk= ''{1}''')

declare @DbName varchar(255);
declare @LogName varchar(255);
set @DbName =(select LogicalName from @Table where [type]='D')
set @LogName =(select  LogicalName from @Table where [type]='L')

ALTER DATABASE {0} SET Single_User WITH Rollback Immediate
 RESTORE DATABASE {0} FROM DISK='{1}' With MOVE @DbName TO '{2}{0}.mdf', MOVE @LogName TO '{2}{0}_log.ldf', REPLACE ALTER DATABASE {0} SET Multi_User
", databaseName, backupPath, filePath);

                SqlCommand myCommand = new SqlCommand(sql, tmpConn);
                myCommand.CommandType = CommandType.Text;
                myCommand.CommandTimeout = 0;
                try
                {
                    tmpConn.Open();
                    myCommand.ExecuteNonQuery();
                    return true;
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }

            }
        }

        public static bool BackupDatabase(string databaseName, string backupPath, string connectionString)
        {
            string message = BackupDB(databaseName, backupPath, connectionString);
            if (message.Equals("success"))
                return true;
            else
                return false;
        }

        public static string BackupDB(string databaseName, string backupPath, string connectionString)
        {
            using (SqlConnection tmpConn = new SqlConnection())
            {
                tmpConn.ConnectionString = connectionString;
                string sql = string.Format("USE MASTER BACKUP DATABASE {0} TO DISK='{1}'", databaseName, backupPath);

                SqlCommand myCommand = new SqlCommand(sql, tmpConn);
                try
                {
                    myCommand.CommandTimeout = 0;
                    tmpConn.Open();
                    myCommand.ExecuteNonQuery();
                    return "success";
                }
                catch (System.Exception ex)
                {
                    ex.Log();
                    return ex.Message;
                }
            }
        }

        public static bool ShrinkDBLog(string connectionString)
        {
            using (SqlConnection tmpConn = new SqlConnection())
            {
                tmpConn.ConnectionString = connectionString;
                string sql = string.Format(@"
DECLARE @SqlStatement2 as nvarchar(max)
SET @SqlStatement2 = 'SELECT [name], [recovery_model_desc] FROM ' + DB_NAME() + '.sys.databases WHERE [name] = ''' + DB_NAME() + '''';
EXEC ( @SqlStatement2 )
DECLARE @SqlStatement as nvarchar(max)
DECLARE @LogFileLogicalName as sysname
SET @SqlStatement = 'ALTER DATABASE ' + DB_NAME() + ' SET RECOVERY SIMPLE'
EXEC ( @SqlStatement )
SELECT [name], [recovery_model_desc] FROM sys.databases WHERE [name] = DB_NAME()
SELECT @LogFileLogicalName = [Name] FROM sys.database_files WHERE type = 1
DBCC Shrinkfile(@LogFileLogicalName, 1)
SET @SqlStatement = 'ALTER DATABASE ' + DB_NAME() + ' SET RECOVERY FULL'
EXEC ( @SqlStatement )
SET @SqlStatement = 'SELECT [name], [recovery_model_desc] FROM ' + DB_NAME() + '.sys.databases WHERE [name] = ''' + DB_NAME() + ''''
EXEC ( @SqlStatement )
");

                SqlCommand myCommand = new SqlCommand(sql, tmpConn);
                myCommand.CommandType = CommandType.Text;
                try
                {
                    tmpConn.Open();
                    myCommand.ExecuteNonQuery();
                    return true;
                }
                catch (System.Exception ex)
                {
                    ex.Log();
                    return false;
                }
            }
        }

        public static bool DatabaseExists(string databaseName, string masterConString)
        {
            string sqlCreateDBQuery;
            bool result = false;

            try
            {
                using (SqlConnection tmpConn = new SqlConnection())
                {
                    tmpConn.ConnectionString = masterConString;

                    sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = '{0}'", databaseName);

                    using (tmpConn)
                    {
                        using (SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn))
                        {
                            tmpConn.Open();
                            sqlCmd.CommandTimeout = 0;
                            var r = sqlCmd.ExecuteScalar();                            
                            tmpConn.Close();

                            result = (r != null ? true : false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public static bool BackUpMongoDatabase(string host, string port, string destination)
        {
            DateTime dt = DateTime.Now;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            try
            {
                process.StartInfo = new System.Diagnostics.ProcessStartInfo
                    ("mongodump.exe", "--host " + host + " --port " + port + " --out \"" + destination + "\"");
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                process.WaitForExit();
                return true;
            }
            catch (System.IO.IOException ex)
            {
                process.Kill();
            }
            Console.WriteLine(DateTime.Now.Subtract(dt).TotalMilliseconds);
            return false;
        }
    }
}
