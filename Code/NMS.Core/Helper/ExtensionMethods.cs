﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NMS.Core.Helper
{
    public static class ExtensionMethods
    {

        public static List<T> ToList<T>(this DataTable dataTable)
        {
            var dataList = new List<T>();

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            var objFieldNames = (from PropertyInfo aProp in typeof(T).GetProperties(flags)
                                 select new
                                 {
                                     Name = aProp.Name,
                                     Type = Nullable.GetUnderlyingType(aProp.PropertyType) ?? aProp.PropertyType
                                 }).ToList();
            var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
                                     select new { Name = aHeader.ColumnName, Type = aHeader.DataType }).ToList();
            var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

            foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
            {
                //var aT = typeof(T);
                //var aT = default(T);
                var aT = Activator.CreateInstance<T>();
                foreach (var aField in commonFields)
                {
                    PropertyInfo propertyInfo = aT.GetType().GetProperty(aField.Name);
                    if (propertyInfo.CanWrite)
                    {
                        if (dataRow[aField.Name] != DBNull.Value)
                        {
                            propertyInfo.SetValue(aT, dataRow[aField.Name], null);
                        }
                    }
                }
                dataList.Add(aT);
            }
            return dataList;
        }

        public static DataTable ToDataTable<T>(this List<T> entities, List<string> ignoreColumnList = null)
        {
            // Retrieve the entities property info of all the properties
            PropertyInfo[] pInfos = typeof(T).GetProperties();

            // Create the new DataTable
            var table = new DataTable();
            // Iterate on all the entities' properties            
            foreach (PropertyInfo pInfo in pInfos)
            {
                if (pInfo != pInfos[0] && ignoreColumnList.IndexOf(pInfo.Name) == -1)
                {
                    //if(pInfo.get.is)
                    // table.Columns.Add(pInfo.Name, pInfo.PropertyType);
                    table.Columns.Add(pInfo.Name, Nullable.GetUnderlyingType(pInfo.PropertyType) ?? pInfo.PropertyType);
                    table.Columns[table.Columns.Count - 1].AllowDBNull = true;

                }
            }
            string a;
            // Create a new row of values for this entity           
            foreach (object obj in entities)
            {
                DataRow row = table.NewRow();
                foreach (PropertyInfo pInfo in obj.GetType().GetProperties())
                {
                    if (pInfo != pInfos[0] && ignoreColumnList.IndexOf(pInfo.Name) == -1)
                    {
                        //if (pInfo.PropertyType.ToString().ToLower().Contains("int") || pInfo.PropertyType.ToString().ToLower().Contains("double"))
                        //    row[pInfo.Name] = pInfo.GetValue(obj) ?? 0;
                        //else
                        row[pInfo.Name] = pInfo.GetValue(obj) ?? DBNull.Value;

                    }
                }
                table.Rows.Add(row);
            }
            // Return the finished DataTable
            return table;
        }

    }

}
