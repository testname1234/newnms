﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Helper.Notification
{
    public interface INotification
    {
        void Subscribe(string channelname,string id);
        void Publish(string channelname, string id, object obj);
        void Clear(string channelname);
        void UnSubscribe(string channelname, string id);
        void OpenConnection(string HostName);
    }
}
