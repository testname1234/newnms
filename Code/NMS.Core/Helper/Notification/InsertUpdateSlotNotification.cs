﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Helper.Notification
{
    public class InsertUpdateSlotNotification
    {   public int SegmentId { get; set; }
        public int EpisodeId { get; set; }
        public int ProgramId { get; set; }
        public int SlotId { get; set; }
    }
}
