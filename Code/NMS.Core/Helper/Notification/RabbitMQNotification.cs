﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NMS.Core.Helper.Notification
{
    public class RabbitMQNotification
    {
        public static IConnection connection { get; set; }
        public static bool IsConnected { get; set; }

        public RabbitMQNotification(string HostName)
        {
            if (connection == null || !connection.IsOpen)
            {
                OpenConnection(HostName);
            }
        }
        private IConnection OpenConnection(string HostName)
        {
            var factory = new ConnectionFactory() { HostName = HostName };
            connection = factory.CreateConnection();
            connection.AutoClose = false;
            IsConnected = true;
            return connection;
        }

        public void PublishMessage(object data, string ChannelName, string routingKey)
        {
            var t = new Task(() =>
            {
                try
                {
                    using (var channel = connection.CreateModel())
                    {
                        var props = channel.CreateBasicProperties();
                        props.DeliveryMode = 2;
                        //channel.QueueDeclare(queue: ChannelName, durable: true);
                        channel.QueueDeclare(queue: ChannelName,
                                durable: true,
                                exclusive: false,
                                autoDelete: true,
                                arguments: null);
                        var body = Encoding.UTF8.GetBytes(JSONHelper.GetString(data));
                        channel.BasicPublish(exchange: ChannelName,
                                             routingKey: routingKey,
                                             basicProperties: props,
                                             body: body);
                    }
                }

                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                }
            });
            t.Start();
        }

    }
}
