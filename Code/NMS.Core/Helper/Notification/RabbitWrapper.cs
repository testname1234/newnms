﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Helper.Notification
{
    public class RabbitWrapper<T>
    {
        private T _value;

        public T Data
        {
            get
            {
                return _value;
            }
            set
            {

                _value = value;
            }
        }
        public string EventName { get; set; }
    }
}
