﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Helper.Notification
{
    public class SignalRNotification 
    {
        public static Dictionary<string, List<string>> Connections;

        public SignalRNotification()
        {
            if (Connections == null)
                Connections = new Dictionary<string, List<string>>();
        }

        public void Clear(string channelname)
        {
            Connections.Remove(channelname);
        }

        public void Publish(string channelname, string id, object obj)
        {
            lock (Connections)
            {
                var lst = Connections[channelname];
                if (lst != null)
                {
                    foreach (var item in lst)
                    {

                    }
                }
            }
        }

        public void Subscribe(string channelname, string id)
        {
            lock (Connections)
            {
                var lst = Connections[channelname];
                if (lst == null)
                    lst = new List<string>();
                if (!lst.Contains(id))
                    lst.Add(id);
            }
        }

        public void UnSubscribe(string channelname, string id)
        {
            lock (Connections)
            {
                var lst = Connections[channelname];
                if (lst != null)
                {
                    lst.Remove(id);
                }
            }
        }
    }
}
