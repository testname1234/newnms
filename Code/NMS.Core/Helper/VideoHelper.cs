﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Helper
{
    public class VideoHelper
    {
        public static byte[] GetVideoFromCache(string filePath)
        {
            return File.ReadAllBytes(filePath);
        }

        public static void SaveVideoFromCache(string filePath, byte[] bytes)
        {
            File.WriteAllBytes(filePath, bytes);
        }
    }
}
