﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Helper
{
    public class Item
    {
        public string title { get; set; }
        public string link { get; set; }
        public GoogleGuid guid { get; set; }
        public string pubDate { get; set; }
        public string description { get; set; }
    }

    public class GoogleChannel
    {
        public string generator { get; set; }
        public string title { get; set; }
        public string link { get; set; }
        public string language { get; set; }
        public string webMaster { get; set; }
        public string copyright { get; set; }
        public string pubDate { get; set; }
        public string lastBuildDate { get; set; }
        public GoogleImage image { get; set; }
        public List<Item> item { get; set; }
        public string description { get; set; }
    }

    public class GoogleImage
    {
        public string title { get; set; }
        public string url { get; set; }
        public string link { get; set; }
    }

    public class GoogleGuid
    {
        public bool isPermaLink { get; set; }
        public string text { get; set; }
    }

    public class GoogleRSS
    {
        public Rss rss { get; set; }
    }

    public class Rss
    {   
        public GoogleChannel GoogleChannel { get; set; }
    }
    
}
