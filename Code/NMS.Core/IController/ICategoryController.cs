﻿using NMS.Core.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IController
{
    public interface ICategoryController
    {
        DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>> GetCategoryByTerm(string id = null);
    }
}
