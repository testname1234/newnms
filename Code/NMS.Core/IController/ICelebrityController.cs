﻿using NMS.Core.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IController
{
    public interface ICelebrityController
    {
        DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>> GetCelebrityByTerm(string term);
    }
}
