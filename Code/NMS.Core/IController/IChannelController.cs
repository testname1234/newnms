﻿using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChannelVideo;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IController
{
    public interface IChannelController
    {
        DataTransfer<LoadChannelOutput> LoadChannelInitialData(string id);

        DataTransfer<LoadChannelOutput> RefreshData(string id, bool? isLive = false);

        DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>> GetChannelVideos(GetChannelVideoInput input);

        DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>> GetAll();

        DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> GetAllPrograms();

    }
}
