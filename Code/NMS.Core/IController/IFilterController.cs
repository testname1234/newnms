﻿using NMS.Core.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IController
{
    public interface IFilterController
    {
        DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>> GetAllFilters();

        DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>> GetAllFilterTypes();
    }
}
