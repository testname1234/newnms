﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using NMS.Core.DataTransfer.News;

namespace NMS.Core.IController
{
    public interface INewsController
    {
        DataTransfer<LoadInitialDataOutput> LoadInitialData(LoadInitialDataInput input);
        
        DataTransfer<NMS.Core.DataTransfer.News.GetOutput> AddNews(NMS.Core.DataTransfer.News.PostInput input);


        HttpResponseMessage UpdateThumbId(string id, string resguid);

        
        HttpResponseMessage MarkVerifyNews(MarkVerifyNewsInput input);

        DataTransfer<LoadInitialDataOutput> ProducerPolling(LoadInitialDataInput input);

        DataTransfer<LoadInitialDataOutput> GetMoreNews(LoadInitialDataInput input);

        DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetBunch(LoadInitialDataInput input);

        

        DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsByTerm(string id);

        DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetAllMyNews(int rId, int pageCount, int startIndex);

        DataTransfer<LoadInitialDataOutput> ReporterLoadInitialData(bool? withoutFolder);

        DataTransfer<LoadInitialDataOutput> ReporterPolling(LoadInitialDataInput input);

        DataTransfer<Core.DataTransfer.Comment.PostOutput> InsertComment(Core.DataTransfer.Comment.PostInput input);

        DataTransfer<LoadInitialDataOutput> VerificationLoadInitialData(LoadInitialDataInput input);

        DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsWithUpdatesAndRelated(string id, string bunchId);

        DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsByIDs(NewsIdsModel model);

        DataTransfer<Core.DataTransfer.News.GetOutput> GetNews(string id);

        DataTransfer<GetNewsCustomOutput> GetBunchWithCount(LoadInitialDataInput input);

        DataTransfer<LoadInitialDataOutput> GetAllUsers(LoadInitialDataInput input);

    }
}
