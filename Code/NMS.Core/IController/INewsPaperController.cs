﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IController
{
    public interface INewsPaperController
    {
        DataTransfer<LoadNewsPaperOutput> LoadInitialData(string fromDate, string toDate);

        DataTransfer<LoadNewsPaperOutput> RefreshData(string fromDate, string toDate);
    }
}
