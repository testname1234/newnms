﻿using NMS.Core.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IController
{
    public interface INotificationController
    {
        DataTransfer<string> MarkAsRead(int nid, int uid);
    }
}
