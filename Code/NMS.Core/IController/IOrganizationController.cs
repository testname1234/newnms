﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;

namespace NMS.Core.IController
{
    public interface IOrganizationController
    {
        
        DataTransfer<List<NMS.Core.DataTransfer.Organization.GetOutput>> GetTagByTerm(string id = null);

    }
}
