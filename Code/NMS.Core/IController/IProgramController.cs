﻿using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Program;
using NMS.Core.DataTransfer.SlotScreenTemplate.POST;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.DataTransfer.Slot;
using NMS.Core.Entities;

namespace NMS.Core.IController
{
    public interface IProgramController 
    {
        DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> GetProgramEpisode(GetProgramEpisodeInput input);
        DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput> GetProgramScreenFlow(string programId, string Producerid);
        DataTransfer<string> GenerateScreenTemplateImage(NMS.Core.DataTransfer.ScreenTemplate.PostInput input);
        DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>> InsertUpdateSlots(InputSlots input);
        DataTransfer<bool> DeleteSlot(int SlotId);
        DataTransfer<bool> DeleteGuestById(int guestId);
        DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> SaveSlotScreenTemplateFlow(SlotScreenTemplateFlow input);
        DataTransfer<bool> DeleteSlotScreenTemplate(int id, NewsStatisticsInput NewsStatisticsParam);
        DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> SaveSlotScreenTemplateFlow(NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput input);
        DataTransfer<bool> UpdateScreenElementGuid(string id, string guid);
        //DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> GetTemplateMicAndCamera(int id);
        DataTransfer<NLELoadInitialDataOutput> NLELoadInitialData(NLELoadInitialDataInput input);
        DataTransfer<NLELoadInitialDataOutput> NLEPolling(NLELoadInitialDataInput input);
        DataTransfer<List<SlotScreenTemplate>> NLESaveSlotScreenElements(SaveScreenTemplateInput input);
        DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> GetProgramEpisode(int id);
        DataTransfer<NMS.Core.DataTransfer.Program.GetOutput> GetProgram(string id);
        DataTransfer<NMS.Core.DataTransfer.Message.PostOutput> SendMessage(NMS.Core.DataTransfer.Message.PostInput input);
        DataTransfer<List<Core.DataTransfer.Message.GetOutput>> GetSlotScreenTemplateComments(int id);
    }
}
