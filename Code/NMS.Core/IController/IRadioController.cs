﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IController
{
    public interface IRadioController
    {
        DataTransfer<LoadRadioOutput> LoadRadioInitialData(string fromDate, string toDate);
        DataTransfer<LoadRadioOutput> RefreshData(string fromDate, string toDate);
    }
}
