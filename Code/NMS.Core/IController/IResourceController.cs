﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using NMS.Core.Models;

namespace NMS.Core.IController
{
    public interface IResourceController
    {
        DataTransfer<List<NMS.Core.DataTransfer.Resource.PostOutput>> GetResourceIds(string id, int? bucketId, string apiKey, string sourceTypeId, string sourceTypeName, string sourceId, string Source = "Reporter");
        HttpResponseMessage DeleteResource(string Id);
        HttpResponseMessage Delete(string Id);
        HttpResponseMessage MarkIsProcessed(int id, int typeId);
        MS.Core.DataTransfer.Resource.ResourceOutput SearchResource(string term, int pageSize, int pageNumber, int? resourceTypeId,int bucketId,string alldata);
        DataTransfer<string> CroppImage(CroppedResource input);
    }
}
