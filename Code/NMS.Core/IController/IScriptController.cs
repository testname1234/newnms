﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;

namespace NMS.Core.IController
{
    public interface IScriptController 
    {
        DataTransfer<NMS.Core.DataTransfer.Script.PostOutput> Insert(NMS.Core.DataTransfer.Script.PostInput input);
        DataTransfer<bool> MarkAsExecuted(int id);
    }
}
