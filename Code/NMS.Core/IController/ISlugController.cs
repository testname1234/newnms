﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;

namespace NMS.Core.IController
{
    public interface ISlugController
    {
        DataTransfer<List<NMS.Core.DataTransfer.Slug.GetOutput>> InsertSlug(string id = null);
    }
}
