﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;

namespace NMS.Core.IController
{
    public interface ITagController
    {
        
        DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>> GetTagByTerm(string id = null);

    }
}
