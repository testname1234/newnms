﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.IController
{
    public interface ITickerController
    {
        DataTransfer<TickerOutput> GetMoreTickers(TickerInput input);
    }
}
