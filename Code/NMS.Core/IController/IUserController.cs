﻿using NMS.Core.DataTransfer;
using NMS.Core.Models;
using System;
using System.Collections.Generic;

namespace NMS.Core.IController
{
    public interface IUserController 
    {
        DataTransfer<NMS.Core.DataTransfer.User.PostOutput> Login(NMS.Core.DataTransfer.User.PostInput input);
        DataTransfer<MMSLoadInitialDataLoginOutput> LoginMMS(NMS.Core.DataTransfer.User.PostInput input);

    }
}
