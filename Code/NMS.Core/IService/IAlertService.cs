﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Alert;

namespace NMS.Core.IService
{
		
	public interface IAlertService
	{
        Dictionary<string, string> GetAlertBasicSearchColumns();
        
        List<SearchColumn> GetAlertAdvanceSearchColumns();

		Alert GetAlert(System.Int32 AlertId);
		DataTransfer<List<GetOutput>> GetAll();
		Alert UpdateAlert(Alert entity);
		bool DeleteAlert(System.Int32 AlertId);
		List<Alert> GetAllAlert();
		Alert InsertAlert(Alert entity);
        List<Alert> GetLatestAlerts(DateTime? lastUpdateDate = null, int rowCount = 10);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
