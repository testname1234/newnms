﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.AssignmentResource;

namespace NMS.Core.IService
{
		
	public interface IAssignmentResourceService
	{
        Dictionary<string, string> GetAssignmentResourceBasicSearchColumns();
        
        List<SearchColumn> GetAssignmentResourceAdvanceSearchColumns();

		AssignmentResource GetAssignmentResource(System.Int32 AssignmentResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		AssignmentResource UpdateAssignmentResource(AssignmentResource entity);
		bool DeleteAssignmentResource(System.Int32 AssignmentResourceId);
		List<AssignmentResource> GetAllAssignmentResource();
		AssignmentResource InsertAssignmentResource(AssignmentResource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
