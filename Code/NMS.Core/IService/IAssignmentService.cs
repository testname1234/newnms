﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Assignment;

namespace NMS.Core.IService
{
		
	public interface IAssignmentService
	{
        Dictionary<string, string> GetAssignmentBasicSearchColumns();
        
        List<SearchColumn> GetAssignmentAdvanceSearchColumns();

		Assignment GetAssignment(System.Int32 AssignmentId);
		DataTransfer<List<GetOutput>> GetAll();
		Assignment UpdateAssignment(Assignment entity);
		bool DeleteAssignment(System.Int32 AssignmentId);
		List<Assignment> GetAllAssignment();
		Assignment InsertAssignment(Assignment entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        Assignment GetAssignmentByKeyValue(string Key, string Value);
	}
	
	
}
