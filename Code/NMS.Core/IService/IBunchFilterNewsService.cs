﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchFilterNews;

namespace NMS.Core.IService
{
		
	public interface IBunchFilterNewsService
	{
        Dictionary<string, string> GetBunchFilterNewsBasicSearchColumns();
        
        List<SearchColumn> GetBunchFilterNewsAdvanceSearchColumns();

		BunchFilterNews GetBunchFilterNews(System.Int32 BunchFilterNewsId);
		DataTransfer<List<GetOutput>> GetAll();
		BunchFilterNews UpdateBunchFilterNews(BunchFilterNews entity);
		bool DeleteBunchFilterNews(System.Int32 BunchFilterNewsId);
		List<BunchFilterNews> GetAllBunchFilterNews();
		BunchFilterNews InsertBunchFilterNews(BunchFilterNews entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        
	}
	
	
}
