﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchFilter;
using NMS.Core.Entities.Mongo;
using SolrManager;
using SolrManager.InputEntities;

namespace NMS.Core.IService
{
		
	public interface IBunchFilterService
	{
        Dictionary<string, string> GetBunchFilterBasicSearchColumns();
        
        List<SearchColumn> GetBunchFilterAdvanceSearchColumns();

		List<BunchFilter> GetBunchFilterByFilterId(System.Int32 FilterId);
		List<BunchFilter> GetBunchFilterByBunchId(System.Int32 BunchId);
		BunchFilter GetBunchFilter(System.Int32 BunchFilterId);
		DataTransfer<List<GetOutput>> GetAll();
		BunchFilter UpdateBunchFilter(BunchFilter entity);
		bool DeleteBunchFilter(System.Int32 BunchFilterId);
		List<BunchFilter> GetAllBunchFilter();
		BunchFilter InsertBunchFilter(BunchFilter entity);
        bool InsertBunchFilter(MBunchFilter entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        void InserUpdateIfNotExist(BunchFilter bunchFilter);
        BunchFilter GetByFilterIdAndBunchId(System.Int32 FilterId, System.Int32 BunchId);
        bool CheckIfBunchFilterExists(MBunchFilter entity);
        bool DeleteByBunchAndFilterId(MBunchFilter entity);        
	}
	
	
}
