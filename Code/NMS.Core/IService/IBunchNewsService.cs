﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchNews;

namespace NMS.Core.IService
{
		
	public interface IBunchNewsService
	{
        Dictionary<string, string> GetBunchNewsBasicSearchColumns();
        
        List<SearchColumn> GetBunchNewsAdvanceSearchColumns();

		List<BunchNews> GetBunchNewsByBunchId(System.Int32? BunchId);
		List<BunchNews> GetBunchNewsByNewsId(System.Int32? NewsId);
		BunchNews GetBunchNews(System.Int32 BunchNewsId);
		DataTransfer<List<GetOutput>> GetAll();
		BunchNews UpdateBunchNews(BunchNews entity);
		bool DeleteBunchNews(System.Int32 BunchNewsId);
		List<BunchNews> GetAllBunchNews();
		BunchNews InsertBunchNews(BunchNews entity);
        bool InsertBunchNews(MBunchNews entity);
        bool DeleteByBunchIdAndNewsId(string newsId, string bunchId);
        List<MBunchNews> GetByBunchIdAndNewsId(string newsId, string bunchId);
        
        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
