﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchResource;

namespace NMS.Core.IService
{
		
	public interface IBunchResourceService
	{
        Dictionary<string, string> GetBunchResourceBasicSearchColumns();
        
        List<SearchColumn> GetBunchResourceAdvanceSearchColumns();

		List<BunchResource> GetBunchResourceByBunchId(System.Int32 BunchId);
		List<BunchResource> GetBunchResourceByResourceId(System.Int32 ResourceId);
		BunchResource GetBunchResource(System.Int32 BunchResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		BunchResource UpdateBunchResource(BunchResource entity);
		bool DeleteBunchResource(System.Int32 BunchResourceId);
		List<BunchResource> GetAllBunchResource();
		BunchResource InsertBunchResource(BunchResource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
