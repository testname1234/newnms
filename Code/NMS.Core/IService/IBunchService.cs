﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Bunch;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.IService
{
		
	public interface IBunchService
	{
        Dictionary<string, string> GetBunchBasicSearchColumns();
        
        List<SearchColumn> GetBunchAdvanceSearchColumns();

		Bunch GetBunch(System.Int32 BunchId);
		DataTransfer<List<GetOutput>> GetAll();
		Bunch UpdateBunch(Bunch entity);
		bool DeleteBunch(System.Int32 BunchId);
		List<Bunch> GetAllBunch();
		Bunch InsertBunch(Bunch entity);
        MBunch InsertBunch(MBunch entity);
        MBunch GetByGuid(string Guid);
        bool DeleteByBunchId(string Guid);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        Bunch GetBunchByGuid(System.String BunchGuid);
        void InsertBunchWIthPK(Bunch entity);
        void UpdateBunchNoReturn(Bunch entity);
	}
	
	
}
