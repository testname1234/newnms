﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchTag;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.IService
{
		
	public interface IBunchTagService
	{
        Dictionary<string, string> GetBunchTagBasicSearchColumns();
        
        List<SearchColumn> GetBunchTagAdvanceSearchColumns();

		List<BunchTag> GetBunchTagByBunchId(System.Int32 BunchId);
		List<BunchTag> GetBunchTagByTagId(System.Int32 TagId);
		BunchTag GetBunchTag(System.Int32 BunchTagId);
		DataTransfer<List<GetOutput>> GetAll();
		BunchTag UpdateBunchTag(BunchTag entity);
		bool DeleteBunchTag(System.Int32 BunchTagId);
		List<BunchTag> GetAllBunchTag();
		BunchTag InsertBunchTag(BunchTag entity);
        bool InsertBunchTag(MBunchTag entity);
        bool CheckIfBunchTagExists(MBunchTag mbunchTag);
        bool DeleteByTagId(string tagId);


        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        BunchTag InsertIfNotExists(BunchTag bunchTag);
        BunchTag GetByBunchIdAndTagId(System.Int32 BunchId, System.Int32 TagId);
        bool InsertIfNotExistsNoReturn(BunchTag bunchTag);         
	}
	
	
}
