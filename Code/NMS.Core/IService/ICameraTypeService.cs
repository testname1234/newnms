﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CameraType;

namespace NMS.Core.IService
{
		
	public interface ICameraTypeService
	{
        Dictionary<string, string> GetCameraTypeBasicSearchColumns();
        
        List<SearchColumn> GetCameraTypeAdvanceSearchColumns();

		CameraType GetCameraType(System.Int32 CameraTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		CameraType UpdateCameraType(CameraType entity);
		bool DeleteCameraType(System.Int32 CameraTypeId);
		List<CameraType> GetAllCameraType();
		CameraType InsertCameraType(CameraType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
