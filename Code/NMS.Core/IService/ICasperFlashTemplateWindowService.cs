﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperFlashTemplateWindow;

namespace NMS.Core.IService
{
		
	public interface ICasperFlashTemplateWindowService
	{
        Dictionary<string, string> GetCasperFlashTemplateWindowBasicSearchColumns();
        
        List<SearchColumn> GetCasperFlashTemplateWindowAdvanceSearchColumns();

		CasperFlashTemplateWindow GetCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId);
		DataTransfer<List<GetOutput>> GetAll();
		CasperFlashTemplateWindow UpdateCasperFlashTemplateWindow(CasperFlashTemplateWindow entity);
		bool DeleteCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId);
		List<CasperFlashTemplateWindow> GetAllCasperFlashTemplateWindow();
		CasperFlashTemplateWindow InsertCasperFlashTemplateWindow(CasperFlashTemplateWindow entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
