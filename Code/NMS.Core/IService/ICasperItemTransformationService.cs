﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperItemTransformation;

namespace NMS.Core.IService
{
		
	public interface ICasperItemTransformationService
	{
        Dictionary<string, string> GetCasperItemTransformationBasicSearchColumns();
        
        List<SearchColumn> GetCasperItemTransformationAdvanceSearchColumns();

		CasperItemTransformation GetCasperItemTransformation(System.Int32 CasperItemTransformationId);
		DataTransfer<List<GetOutput>> GetAll();
		CasperItemTransformation UpdateCasperItemTransformation(CasperItemTransformation entity);
		bool DeleteCasperItemTransformation(System.Int32 CasperItemTransformationId);
		List<CasperItemTransformation> GetAllCasperItemTransformation();
		CasperItemTransformation InsertCasperItemTransformation(CasperItemTransformation entity);
        CasperItemTransformation GetCasperItemTransformationByCasperTemplateItemId(System.Int32 casperTemplateItemId);


        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
