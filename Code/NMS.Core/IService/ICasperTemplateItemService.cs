﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplateItem;

namespace NMS.Core.IService
{
		
	public interface ICasperTemplateItemService
	{
        Dictionary<string, string> GetCasperTemplateItemBasicSearchColumns();
        
        List<SearchColumn> GetCasperTemplateItemAdvanceSearchColumns();

		CasperTemplateItem GetCasperTemplateItem(System.Int32 CasperTemplatItemId);
		DataTransfer<List<GetOutput>> GetAll();
		CasperTemplateItem UpdateCasperTemplateItem(CasperTemplateItem entity);
		bool DeleteCasperTemplateItem(System.Int32 CasperTemplatItemId);
		List<CasperTemplateItem> GetAllCasperTemplateItem();
		CasperTemplateItem InsertCasperTemplateItem(CasperTemplateItem entity);
        List<CasperTemplateItem> GetCasperTemplateItemByCasperTemplateId(System.Int32 CasperTemplateId);
        CasperTemplateItem GetCasperTemplateItemByCasperTemplateIdAndType(System.Int32 CasperTemplateId, string type);
        CasperTemplateItem GetCasperTemplateItemByitemType(string type);

        
        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
