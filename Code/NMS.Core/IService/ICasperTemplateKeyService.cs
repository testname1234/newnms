﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplateKey;

namespace NMS.Core.IService
{
		
	public interface ICasperTemplateKeyService
	{
        Dictionary<string, string> GetCasperTemplateKeyBasicSearchColumns();
        
        List<SearchColumn> GetCasperTemplateKeyAdvanceSearchColumns();

		CasperTemplateKey GetCasperTemplateKey(System.Int32 CasperTemplateKeyId);
		DataTransfer<List<GetOutput>> GetAll();
		CasperTemplateKey UpdateCasperTemplateKey(CasperTemplateKey entity);
		bool DeleteCasperTemplateKey(System.Int32 CasperTemplateKeyId);
		List<CasperTemplateKey> GetAllCasperTemplateKey();
		CasperTemplateKey InsertCasperTemplateKey(CasperTemplateKey entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
