﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplate;

namespace NMS.Core.IService
{
		
	public interface ICasperTemplateService
	{
        Dictionary<string, string> GetCasperTemplateBasicSearchColumns();
        
        List<SearchColumn> GetCasperTemplateAdvanceSearchColumns();

		CasperTemplate GetCasperTemplate(System.Int32 CasperTemlateId);
		DataTransfer<List<GetOutput>> GetAll();
		CasperTemplate UpdateCasperTemplate(CasperTemplate entity);
		bool DeleteCasperTemplate(System.Int32 CasperTemlateId);
		List<CasperTemplate> GetAllCasperTemplate();
		CasperTemplate InsertCasperTemplate(CasperTemplate entity);
        CasperTemplate GetCasperTemplateByTemplateName(string Template);
        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
