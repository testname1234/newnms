﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplateType;

namespace NMS.Core.IService
{
		
	public interface ICasperTemplateTypeService
	{
        Dictionary<string, string> GetCasperTemplateTypeBasicSearchColumns();
        
        List<SearchColumn> GetCasperTemplateTypeAdvanceSearchColumns();

		CasperTemplateType GetCasperTemplateType(System.Int32 CasperTemplateTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		CasperTemplateType UpdateCasperTemplateType(CasperTemplateType entity);
		bool DeleteCasperTemplateType(System.Int32 CasperTemplateTypeId);
		List<CasperTemplateType> GetAllCasperTemplateType();
		CasperTemplateType InsertCasperTemplateType(CasperTemplateType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
