﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplateWindowType;

namespace NMS.Core.IService
{
		
	public interface ICasperTemplateWindowTypeService
	{
        Dictionary<string, string> GetCasperTemplateWindowTypeBasicSearchColumns();
        
        List<SearchColumn> GetCasperTemplateWindowTypeAdvanceSearchColumns();

		CasperTemplateWindowType GetCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		CasperTemplateWindowType UpdateCasperTemplateWindowType(CasperTemplateWindowType entity);
		bool DeleteCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId);
		List<CasperTemplateWindowType> GetAllCasperTemplateWindowType();
		CasperTemplateWindowType InsertCasperTemplateWindowType(CasperTemplateWindowType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
