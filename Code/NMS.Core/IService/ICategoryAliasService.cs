﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CategoryAlias;

namespace NMS.Core.IService
{
		
	public interface ICategoryAliasService
	{
        Dictionary<string, string> GetCategoryAliasBasicSearchColumns();
        
        List<SearchColumn> GetCategoryAliasAdvanceSearchColumns();

		List<CategoryAlias> GetCategoryAliasByCategoryId(System.Int32 CategoryId);
		CategoryAlias GetCategoryAlias(System.Int32 CategoryAliasId);
		DataTransfer<List<GetOutput>> GetAll();
		CategoryAlias UpdateCategoryAlias(CategoryAlias entity);
		bool DeleteCategoryAlias(System.Int32 CategoryAliasId);
		List<CategoryAlias> GetAllCategoryAlias();
		CategoryAlias InsertCategoryAlias(CategoryAlias entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        CategoryAlias InsertCategoryAliasIfNotExists(CategoryAlias entity);
	}
	
	
}
