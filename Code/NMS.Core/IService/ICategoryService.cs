﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Category;

namespace NMS.Core.IService
{

    public interface ICategoryService
    {
        Dictionary<string, string> GetCategoryBasicSearchColumns();

        List<SearchColumn> GetCategoryAdvanceSearchColumns();

        Category GetCategory(System.Int32 CategoryId);
        DataTransfer<List<GetOutput>> GetAll();
        Category UpdateCategory(Category entity);
        bool DeleteCategory(System.Int32 CategoryId);
        List<Category> GetAllCategory();
        Category InsertCategory(Category entity);
        Category InsertCategoryIfNotExists(Category entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Category> GetCategoryByTerm(string term);
        List<Category> GetByDate(DateTime LastUpdatedDate);

        Category GetCategoryByName(string term);

        CategoryAlias GetByAlias(string alias);
        Category GetCategoryCreateIfNotExist(Category category);
    }
	
	
}
