﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Celebrity;
using NMS.Core.Models;
using NMS.Core.Enums;

namespace NMS.Core.IService
{
		
	public interface ICelebrityService
	{
        Dictionary<string, string> GetCelebrityBasicSearchColumns();
        
        List<SearchColumn> GetCelebrityAdvanceSearchColumns();
		
		List<Celebrity> GetCelebrityByCategoryId(System.Int32 CategoryId);
		Celebrity GetCelebrity(System.Int32 CelebrityId);
		DataTransfer<List<GetOutput>> GetAll();
		Celebrity UpdateCelebrity(Celebrity entity);
		bool DeleteCelebrity(System.Int32 CelebrityId);
		List<Celebrity> GetAllCelebrity();
		Celebrity InsertCelebrity(Celebrity entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        
        Celebrity GetBySocialMediaAccountUserName(string userName, int socialMediaType);
        List<Celebrity> GetCelebrityByTerm(string term);
        bool UpdateCelebrityStatistics(NewsStatisticsInput newsStatisticsParam, int celebrityId, NewsStatisticType type);

        Celebrity GetBySocialMediaAccountUserNameNoCeleb(string userName);

    }
	
	
}
