﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CelebrityStatistic;
using NMS.Core.Models;

namespace NMS.Core.IService
{
		
	public interface ICelebrityStatisticService
	{
        Dictionary<string, string> GetCelebrityStatisticBasicSearchColumns();
        
        List<SearchColumn> GetCelebrityStatisticAdvanceSearchColumns();

		List<CelebrityStatistic> GetCelebrityStatisticByCelebrityId(System.Int32? CelebrityId);
		CelebrityStatistic GetCelebrityStatistic(System.Int32 CelebrityStatisticId);
		DataTransfer<List<GetOutput>> GetAll();
		CelebrityStatistic UpdateCelebrityStatistic(CelebrityStatistic entity);
		bool DeleteCelebrityStatistic(System.Int32 CelebrityStatisticId);
		List<CelebrityStatistic> GetAllCelebrityStatistic();
		CelebrityStatistic InsertCelebrityStatistic(CelebrityStatistic entity);
        bool DeleteCelebrityStatisticByEpisodeId(System.Int32 episodeId);
        bool UpdateCelebrityStatistic(System.Int32 celebrityId);
        void RefreshCelebrityStatistics(NewsStatisticsInput NewsStatisticsParam);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
