﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CelebrityVoiceArtist;

namespace NMS.Core.IService
{
		
	public interface ICelebrityVoiceArtistService
	{
        Dictionary<string, string> GetCelebrityVoiceArtistBasicSearchColumns();
        
        List<SearchColumn> GetCelebrityVoiceArtistAdvanceSearchColumns();

		List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByCelebrityId(System.Int32? CelebrityId);
		List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByVoiceArtistId(System.Int32? VoiceArtistId);
		CelebrityVoiceArtist GetCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId);
		DataTransfer<List<GetOutput>> GetAll();
		CelebrityVoiceArtist UpdateCelebrityVoiceArtist(CelebrityVoiceArtist entity);
		bool DeleteCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId);
		List<CelebrityVoiceArtist> GetAllCelebrityVoiceArtist();
		CelebrityVoiceArtist InsertCelebrityVoiceArtist(CelebrityVoiceArtist entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
