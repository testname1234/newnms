﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChannelProgram;

namespace NMS.Core.IService
{
		
	public interface IChannelProgramService
	{
        Dictionary<string, string> GetChannelProgramBasicSearchColumns();
        
        List<SearchColumn> GetChannelProgramAdvanceSearchColumns();

		List<ChannelProgram> GetChannelProgramByChannelId(System.Int32 ChannelId);
		List<ChannelProgram> GetChannelProgramByProgramId(System.Int32 ProgramId);
		ChannelProgram GetChannelProgram(System.Int32 ChannelProgramId);
		DataTransfer<List<GetOutput>> GetAll();
		ChannelProgram UpdateChannelProgram(ChannelProgram entity);
		bool DeleteChannelProgram(System.Int32 ChannelProgramId);
		List<ChannelProgram> GetAllChannelProgram();
		ChannelProgram InsertChannelProgram(ChannelProgram entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
