﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Channel;

namespace NMS.Core.IService
{
		
	public interface IChannelService
	{
        Dictionary<string, string> GetChannelBasicSearchColumns();
        
        List<SearchColumn> GetChannelAdvanceSearchColumns();

		Channel GetChannel(System.Int32 ChannelId);
		DataTransfer<List<GetOutput>> GetAll();
		Channel UpdateChannel(Channel entity);
		bool DeleteChannel(System.Int32 ChannelId);
		List<Channel> GetAllChannel();
		Channel InsertChannel(Channel entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Channel> GetChannelsByOtherChannelBit(bool flag);

        int InsertNewReels(int channelId, int programId);

        List<Channel> GetChannelsByUserId(int UserId);
	}
	
	
}
