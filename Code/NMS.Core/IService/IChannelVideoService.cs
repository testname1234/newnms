﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChannelVideo;

namespace NMS.Core.IService
{
		
	public interface IChannelVideoService
	{
        Dictionary<string, string> GetChannelVideoBasicSearchColumns();
        
        List<SearchColumn> GetChannelVideoAdvanceSearchColumns();

		ChannelVideo GetChannelVideo(System.Int32 ChannelVideoId);
		DataTransfer<List<GetOutput>> GetAll();
		ChannelVideo UpdateChannelVideo(ChannelVideo entity);
        ChannelVideo GetLastChannelVideo(int ChannelId);
		bool DeleteChannelVideo(System.Int32 ChannelVideoId);
		List<ChannelVideo> GetAllChannelVideo();

        List<ChannelVideo> GetDistinctPrograms();

        ChannelVideo InsertChannelVideo(ChannelVideo entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<ChannelVideo> GetAllChannelVideosBetweenDates(DateTime dtFrom, DateTime dtTo);

        List<ChannelVideo> GetAllChannelLiveTCS(DateTime dtFrom, DateTime dtTo);

        double GetDuration(string filename);

        void MarkIsProcessed(int id);
	}
	
	
}
