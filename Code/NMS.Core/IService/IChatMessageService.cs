﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChatMessage;

namespace NMS.Core.IService
{
		
	public interface IChatMessageService
	{
        Dictionary<string, string> GetChatMessageBasicSearchColumns();
        
        List<SearchColumn> GetChatMessageAdvanceSearchColumns();

		ChatMessage GetChatMessage(System.Int32 MessageId);
		DataTransfer<List<GetOutput>> GetAll();
		ChatMessage UpdateChatMessage(ChatMessage entity);
		bool DeleteChatMessage(System.Int32 MessageId);
		List<ChatMessage> GetAllChatMessage();
		ChatMessage InsertChatMessage(ChatMessage entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        void InsertChatMessage(int from, int to, string message, DateTime dateTime, bool isRecieved, bool? isSentToGroup = null);


        List<ChatMessage> GetChatMessagesForUser(int userId);

        List<ChatMessage> GetAllGroupMessages();
    }
	
	
}
