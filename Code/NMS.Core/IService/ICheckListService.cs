﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CheckList;

namespace NMS.Core.IService
{
		
	public interface ICheckListService
	{
        Dictionary<string, string> GetCheckListBasicSearchColumns();
        
        List<SearchColumn> GetCheckListAdvanceSearchColumns();

		CheckList GetCheckList(System.Int32 ChecklistId);
		DataTransfer<List<GetOutput>> GetAll();
		CheckList UpdateCheckList(CheckList entity);
		bool DeleteCheckList(System.Int32 ChecklistId);
		List<CheckList> GetAllCheckList();
		CheckList InsertCheckList(CheckList entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<CheckList> GetNameErrorMessageCheckList();
        void UpdatePriority(string Name, bool Priority);
        List<CheckList> GetPriority();
        int Selection(string Name);
        CheckList InsertResult(CheckList entity);
        CheckList UpdateResult(CheckList entity);
	}
	
	
}
