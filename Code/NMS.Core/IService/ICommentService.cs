﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Comment;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.IService
{
		
	public interface ICommentService
	{
        Dictionary<string, string> GetCommentBasicSearchColumns();
        
        List<SearchColumn> GetCommentAdvanceSearchColumns();

		Comment GetComment(System.Int32 Commentid);
		DataTransfer<List<GetOutput>> GetAll();
		Comment UpdateComment(Comment entity);
		bool DeleteComment(System.Int32 Commentid);
		List<Comment> GetAllComment();
		Comment InsertComment(Comment entity);
        bool InsertComment(MComment entity);


        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
       // DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        Comment InsertIfNotExists(Comment comment);
        Comment GetByGuid(System.String Guid);
        Comment GetByNewsGuid(System.String NewsGuid);

	}
	
	
}
