﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.DailyNewsPaper;

namespace NMS.Core.IService
{
		
	public interface IDailyNewsPaperService
	{
        Dictionary<string, string> GetDailyNewsPaperBasicSearchColumns();
        
        List<SearchColumn> GetDailyNewsPaperAdvanceSearchColumns();

		List<DailyNewsPaper> GetDailyNewsPaperByNewsPaperId(System.Int32 NewsPaperId);
		DailyNewsPaper GetDailyNewsPaper(System.Int32 DailyNewsPaperId);
		DataTransfer<List<GetOutput>> GetAll();
		DailyNewsPaper UpdateDailyNewsPaper(DailyNewsPaper entity);
		bool DeleteDailyNewsPaper(System.Int32 DailyNewsPaperId);
		List<DailyNewsPaper> GetAllDailyNewsPaper();
		DailyNewsPaper InsertDailyNewsPaper(DailyNewsPaper entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        //List<DailyNewsPaper> GetDailyNewsPaperByDate(DateTime date);

        List<DailyNewsPaper> GetDailyNewsPaperByDate(System.DateTime fromDate, System.DateTime toDate);
        List<DailyNewsPaper> GeByDateAndNewsPaperId(System.DateTime fromDate, System.DateTime toDate, System.Int32 DailyNewsPaperId);
	}
	
	
}
