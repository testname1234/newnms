﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.DescrepencyNewsFile;

namespace NMS.Core.IService
{
		
	public interface IDescrepencyNewsFileService
	{
        Dictionary<string, string> GetDescrepencyNewsFileBasicSearchColumns();
        
        List<SearchColumn> GetDescrepencyNewsFileAdvanceSearchColumns();

		DescrepencyNewsFile GetDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId);
		DataTransfer<List<GetOutput>> GetAll();
		DescrepencyNewsFile UpdateDescrepencyNewsFile(DescrepencyNewsFile entity);
		bool DeleteDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId);
		List<DescrepencyNewsFile> GetAllDescrepencyNewsFile();
		DescrepencyNewsFile InsertDescrepencyNewsFile(DescrepencyNewsFile entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool UpdateNewsFileId(int Id, int NewsFileId);

        bool UpdateDescrepencyCategory(string Id, int CategoryId);

        List<int> GetDescrepencyNewsFileIdByDescrepencyValue(string DescrepencyValue);
        bool UpdateDescrepencyValueByDescrepencyNewsFileId(string NewsFileId, string DescrepencyValue);

    }
	
	
}
