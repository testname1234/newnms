﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EditorialComment;

namespace NMS.Core.IService
{
		
	public interface IEditorialCommentService
	{
        Dictionary<string, string> GetEditorialCommentBasicSearchColumns();
        
        List<SearchColumn> GetEditorialCommentAdvanceSearchColumns();

		EditorialComment GetEditorialComment(System.Int32 EditorialCommentId);
		DataTransfer<List<GetOutput>> GetAll();
		EditorialComment UpdateEditorialComment(EditorialComment entity);
		bool DeleteEditorialComment(System.Int32 EditorialCommentId);
		List<EditorialComment> GetAllEditorialComment();
		EditorialComment InsertEditorialComment(EditorialComment entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        EditorialComment AddComment(NMS.Core.DataTransfer.EditorialComment.PostInput input);

        List<EditorialComment> GetComment(NMS.Core.DataTransfer.EditorialComment.PostInput input);

        List<EditorialComment> GetCommentsBySlotIdAndLastUpdateDate(DateTime dt, int slotId);
        List<EditorialComment> GetCommentByTickerId(int tickerId, int userRoleId);

        List<EditorialComment> GetCommentByNewsFileIdAndLanguageCode(int newsfileId);
    }
	
	
}
