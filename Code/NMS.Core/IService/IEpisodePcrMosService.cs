﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;

namespace NMS.Core.IService
{
		
	public interface IEpisodePcrMosService
	{
        Dictionary<string, string> GetEpisodePcrMosBasicSearchColumns();
        
        List<SearchColumn> GetEpisodePcrMosAdvanceSearchColumns();

		EpisodePcrMos GetEpisodePcrMos(System.Int32 EpisodePcrMosId);
		EpisodePcrMos UpdateEpisodePcrMos(EpisodePcrMos entity);
		bool DeleteEpisodePcrMos(System.Int32 EpisodePcrMosId);
		List<EpisodePcrMos> GetAllEpisodePcrMos();
		EpisodePcrMos InsertEpisodePcrMos(EpisodePcrMos entity);
	}
	
	
}
