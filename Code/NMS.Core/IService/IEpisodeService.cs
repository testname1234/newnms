﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Episode;
using NMS.Core.Enums;

namespace NMS.Core.IService
{
		
	public interface IEpisodeService
	{
        Dictionary<string, string> GetEpisodeBasicSearchColumns();
        
        List<SearchColumn> GetEpisodeAdvanceSearchColumns();

		List<Episode> GetEpisodeByProgramId(System.Int32 ProgramId);
		Episode GetEpisode(System.Int32 EpisodeId);
        Episode GetEpisodeBySlotId(System.Int32 SlotId);
		DataTransfer<List<GetOutput>> GetAll();
		Episode UpdateEpisode(Episode entity);
		bool DeleteEpisode(System.Int32 EpisodeId);
		List<Episode> GetAllEpisode();
		Episode InsertEpisode(Episode entity);
        Episode GetEpisodeBySegmentId(System.Int32 SegmentId);
        List<Episode> GetEpisodesByProgramType(ProgramTypes programType, string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null);
        List<int> GetProgramSetMappingID(int EpisodeId);

        List<NMS.Core.DataTransfer.Episode.GetOutput> GetEpisodeDetailByProgram(List<int> lstprogramid, DateTime from, DateTime to);
	}
	
	
}
