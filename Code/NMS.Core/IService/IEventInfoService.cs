﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventInfo;

namespace NMS.Core.IService
{
		
	public interface IEventInfoService
	{
        Dictionary<string, string> GetEventInfoBasicSearchColumns();
        
        List<SearchColumn> GetEventInfoAdvanceSearchColumns();

		EventInfo GetEventInfo(System.Int32 EventInfoId);
		DataTransfer<List<GetOutput>> GetAll();
		EventInfo UpdateEventInfo(EventInfo entity);
		bool DeleteEventInfo(System.Int32 EventInfoId);
		List<EventInfo> GetAllEventInfo();
        List<EventInfo> GetUpcommingEvents();
		EventInfo InsertEventInfo(EventInfo entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
