﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventReporter;

namespace NMS.Core.IService
{
		
	public interface IEventReporterService
	{
        Dictionary<string, string> GetEventReporterBasicSearchColumns();
        
        List<SearchColumn> GetEventReporterAdvanceSearchColumns();

		List<EventReporter> GetEventReporterByNewsFileId(System.Int32? NewsFileId);
		EventReporter GetEventReporter(System.Int32 EventReporterId);
		DataTransfer<List<GetOutput>> GetAll();
		EventReporter UpdateEventReporter(EventReporter entity);
		bool DeleteEventReporter(System.Int32 EventReporterId);
		List<EventReporter> GetAllEventReporter();
		EventReporter InsertEventReporter(EventReporter entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
