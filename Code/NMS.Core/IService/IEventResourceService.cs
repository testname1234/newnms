﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventResource;

namespace NMS.Core.IService
{
		
	public interface IEventResourceService
	{
        Dictionary<string, string> GetEventResourceBasicSearchColumns();
        
        List<SearchColumn> GetEventResourceAdvanceSearchColumns();

		List<EventResource> GetEventResourceByNewsFileId(System.Int32? NewsFileId);
		EventResource GetEventResource(System.Int32 EventResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		EventResource UpdateEventResource(EventResource entity);
		bool DeleteEventResource(System.Int32 EventResourceId);
		List<EventResource> GetAllEventResource();
		EventResource InsertEventResource(EventResource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
