﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventType;

namespace NMS.Core.IService
{
		
	public interface IEventTypeService
	{
        Dictionary<string, string> GetEventTypeBasicSearchColumns();
        
        List<SearchColumn> GetEventTypeAdvanceSearchColumns();

		EventType GetEventType(System.Int32 EventTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		EventType UpdateEventType(EventType entity);
		bool DeleteEventType(System.Int32 EventTypeId);
		List<EventType> GetAllEventType();
		EventType InsertEventType(EventType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<EventType> GetEventTypeByTerm(string term);

    }
	
	
}
