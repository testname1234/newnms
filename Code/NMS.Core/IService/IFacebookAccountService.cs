﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FacebookAccount;

namespace NMS.Core.IService
{
		
	public interface IFacebookAccountService
	{
        Dictionary<string, string> GetFacebookAccountBasicSearchColumns();
        
        List<SearchColumn> GetFacebookAccountAdvanceSearchColumns();

		List<FacebookAccount> GetFacebookAccountByCelebrityId(System.Int32? CelebrityId);
		FacebookAccount GetFacebookAccount(System.Int32 FacebookAccountId);
		DataTransfer<List<GetOutput>> GetAll();
		FacebookAccount UpdateFacebookAccount(FacebookAccount entity);
		bool DeleteFacebookAccount(System.Int32 FacebookAccountId);
		List<FacebookAccount> GetAllFacebookAccount();
		FacebookAccount InsertFacebookAccount(FacebookAccount entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
