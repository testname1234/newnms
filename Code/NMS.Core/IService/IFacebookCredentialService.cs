﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FacebookCredential;

namespace NMS.Core.IService
{
		
	public interface IFacebookCredentialService
	{
        Dictionary<string, string> GetFacebookCredentialBasicSearchColumns();
        
        List<SearchColumn> GetFacebookCredentialAdvanceSearchColumns();

		FacebookCredential GetFacebookCredential(System.Int32 FacebookCredentialId);
		DataTransfer<List<GetOutput>> GetAll();
		FacebookCredential UpdateFacebookCredential(FacebookCredential entity);
		bool DeleteFacebookCredential(System.Int32 FacebookCredentialId);
		List<FacebookCredential> GetAllFacebookCredential();
		FacebookCredential InsertFacebookCredential(FacebookCredential entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        void UdpateAccessToken(string AccessToken, DateTime LastUpdateDate, int CredentialID);
        string GetAccessToken();
	}
	
	
}
