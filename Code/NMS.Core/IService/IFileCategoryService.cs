﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileCategory;

namespace NMS.Core.IService
{
		
	public interface IFileCategoryService
	{
        Dictionary<string, string> GetFileCategoryBasicSearchColumns();
        
        List<SearchColumn> GetFileCategoryAdvanceSearchColumns();

		List<FileCategory> GetFileCategoryByNewsFileId(System.Int32 NewsFileId);
		List<FileCategory> GetFileCategoryByCategoryId(System.Int32 CategoryId);
		FileCategory GetFileCategory(System.Int32 FileCategoryId);
		DataTransfer<List<GetOutput>> GetAll();
		FileCategory UpdateFileCategory(FileCategory entity);
		bool DeleteFileCategory(System.Int32 FileCategoryId);
		List<FileCategory> GetAllFileCategory();
		FileCategory InsertFileCategory(FileCategory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteFileCategoryByNewsFileId(int newsFileId);

    }
	
	
}
