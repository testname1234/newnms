﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileDetail;

namespace NMS.Core.IService
{
		
	public interface IFileDetailService
	{
        Dictionary<string, string> GetFileDetailBasicSearchColumns();
        
        List<SearchColumn> GetFileDetailAdvanceSearchColumns();

		List<FileDetail> GetFileDetailByNewsFileId(System.Int32 NewsFileId);
		FileDetail GetFileDetail(System.Int32 FileDetailId);
		DataTransfer<List<GetOutput>> GetAll();
		FileDetail UpdateFileDetail(FileDetail entity);
		bool DeleteFileDetail(System.Int32 FileDetailId);
		List<FileDetail> GetAllFileDetail();
		FileDetail InsertFileDetail(FileDetail entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
