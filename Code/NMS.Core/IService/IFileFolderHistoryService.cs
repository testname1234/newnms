﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileFolderHistory;

namespace NMS.Core.IService
{
		
	public interface IFileFolderHistoryService
	{
        Dictionary<string, string> GetFileFolderHistoryBasicSearchColumns();
        
        List<SearchColumn> GetFileFolderHistoryAdvanceSearchColumns();

		List<FileFolderHistory> GetFileFolderHistoryByNewsFileId(System.Int32 NewsFileId);
		List<FileFolderHistory> GetFileFolderHistoryByFolderId(System.Int32 FolderId);
		FileFolderHistory GetFileFolderHistory(System.Int32 FileFolderHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		FileFolderHistory UpdateFileFolderHistory(FileFolderHistory entity);
		bool DeleteFileFolderHistory(System.Int32 FileFolderHistoryId);
		List<FileFolderHistory> GetAllFileFolderHistory();
		FileFolderHistory InsertFileFolderHistory(FileFolderHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
