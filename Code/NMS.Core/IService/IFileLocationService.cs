﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileLocation;

namespace NMS.Core.IService
{
		
	public interface IFileLocationService
	{
        Dictionary<string, string> GetFileLocationBasicSearchColumns();
        
        List<SearchColumn> GetFileLocationAdvanceSearchColumns();

		List<FileLocation> GetFileLocationByNewsFileId(System.Int32 NewsFileId);
		List<FileLocation> GetFileLocationByLocationId(System.Int32 LocationId);
		FileLocation GetFileLocation(System.Int32 FileLocationId);
		DataTransfer<List<GetOutput>> GetAll();
		FileLocation UpdateFileLocation(FileLocation entity);
		bool DeleteFileLocation(System.Int32 FileLocationId);
		List<FileLocation> GetAllFileLocation();
		FileLocation InsertFileLocation(FileLocation entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteFileLocationByNewsFileId(int newsFileId);

    }
	
	
}
