﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileResource;

namespace NMS.Core.IService
{

    public interface IFileResourceService
    {
        Dictionary<string, string> GetFileResourceBasicSearchColumns();

        List<SearchColumn> GetFileResourceAdvanceSearchColumns();

        List<FileResource> GetFileResourceByNewsFileId(System.Int32 NewsFileId);
        FileResource GetFileResource(System.Int32 FileResourceId);
        DataTransfer<List<GetOutput>> GetAll();
        FileResource UpdateFileResource(FileResource entity);
        bool DeleteFileResource(System.Int32 FileResourceId);
        List<FileResource> GetAllFileResource();
        FileResource InsertFileResource(FileResource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        
    }
	
	
}
