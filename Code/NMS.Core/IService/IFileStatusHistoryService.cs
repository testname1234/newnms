﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileStatusHistory;

namespace NMS.Core.IService
{
		
	public interface IFileStatusHistoryService
	{
        Dictionary<string, string> GetFileStatusHistoryBasicSearchColumns();
        
        List<SearchColumn> GetFileStatusHistoryAdvanceSearchColumns();

		List<FileStatusHistory> GetFileStatusHistoryByNewsFileId(System.Int32 NewsFileId);
		FileStatusHistory GetFileStatusHistory(System.Int32 FileStatusHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		FileStatusHistory UpdateFileStatusHistory(FileStatusHistory entity);
		bool DeleteFileStatusHistory(System.Int32 FileStatusHistoryId);
		List<FileStatusHistory> GetAllFileStatusHistory();
		FileStatusHistory InsertFileStatusHistory(FileStatusHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
