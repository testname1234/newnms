﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileTag;

namespace NMS.Core.IService
{
		
	public interface IFileTagService
	{
        Dictionary<string, string> GetFileTagBasicSearchColumns();
        
        List<SearchColumn> GetFileTagAdvanceSearchColumns();

		List<FileTag> GetFileTagByNewsFileId(System.Int32 NewsFileId);
		List<FileTag> GetFileTagByTagId(System.Int32 TagId);
		FileTag GetFileTag(System.Int32 FileTagId);
		DataTransfer<List<GetOutput>> GetAll();
		FileTag UpdateFileTag(FileTag entity);
		bool DeleteFileTag(System.Int32 FileTagId);
		List<FileTag> GetAllFileTag();
		FileTag InsertFileTag(FileTag entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
