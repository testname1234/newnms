﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Filter;
using NMS.Core.Enums;

namespace NMS.Core.IService
{
		
	public interface IFilterService
	{
        Dictionary<string, string> GetFilterBasicSearchColumns();
        
        List<SearchColumn> GetFilterAdvanceSearchColumns();

		List<Filter> GetFilterByFilterTypeId(System.Int32 FilterTypeId);
		Filter GetFilter(System.Int32 FilterId);
		DataTransfer<List<GetOutput>> GetAll();
		Filter UpdateFilter(Filter entity);
		bool DeleteFilter(System.Int32 FilterId);
		List<Filter> GetAllFilter();
		Filter InsertFilter(Filter entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<GetOutput> GetAllFilterInHierarchy();
        List<Filter> GetFilterByLastUpdateDate(DateTime filterLastUpdateDate);

        Filter GetFilterByNameAndFilterType(FilterTypes filterType, string name);
        Filter GetFilterCreateIfNotExist(Filter filter);

        List<Filter> GetAllParentFilters();
    }
	
	
}
