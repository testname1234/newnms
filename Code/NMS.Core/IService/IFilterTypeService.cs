﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FilterType;

namespace NMS.Core.IService
{
		
	public interface IFilterTypeService
	{
        Dictionary<string, string> GetFilterTypeBasicSearchColumns();
        
        List<SearchColumn> GetFilterTypeAdvanceSearchColumns();

		FilterType GetFilterType(System.Int32 FilterTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		FilterType UpdateFilterType(FilterType entity);
		bool DeleteFilterType(System.Int32 FilterTypeId);
		List<FilterType> GetAllFilterType();
		FilterType InsertFilterType(FilterType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
