﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Folder;

namespace NMS.Core.IService
{
		
	public interface IFolderService
	{
        Dictionary<string, string> GetFolderBasicSearchColumns();
        
        List<SearchColumn> GetFolderAdvanceSearchColumns();

		Folder GetFolder(System.Int32 FolderId);
		DataTransfer<List<GetOutput>> GetAll();
		Folder UpdateFolder(Folder entity);
		bool DeleteFolder(System.Int32 FolderId);
		List<Folder> GetAllFolder();
		Folder InsertFolder(Folder entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        Folder CreateRundownFolder(PostInput input);
    }
	
	
}
