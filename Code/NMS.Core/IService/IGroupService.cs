﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Group;

namespace NMS.Core.IService
{
		
	public interface IGroupService
	{
        Dictionary<string, string> GetGroupBasicSearchColumns();
        
        List<SearchColumn> GetGroupAdvanceSearchColumns();

		Group GetGroup(System.Int32 GroupId);
		DataTransfer<List<GetOutput>> GetAll();
		Group UpdateGroup(Group entity);
		bool DeleteGroup(System.Int32 GroupId);
		List<Group> GetAllGroup();
		Group InsertGroup(Group entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool AddNewGroupWithUserIds(string GroupName, List<int> Users);

        List<Group> GetAllGroupWithUserIds();

        bool EditGroupWithUserIds(int GroupId, List<int> Users, List<int> oldUsersIds);

        bool DeleteGroupWithGroupUsers(int GroupId);
    }
	
	
}
