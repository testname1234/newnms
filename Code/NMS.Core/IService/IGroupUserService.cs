﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.GroupUser;

namespace NMS.Core.IService
{
		
	public interface IGroupUserService
	{
        Dictionary<string, string> GetGroupUserBasicSearchColumns();
        
        List<SearchColumn> GetGroupUserAdvanceSearchColumns();

		GroupUser GetGroupUser(System.Int32 GroupUserId);
		DataTransfer<List<GetOutput>> GetAll();
		GroupUser UpdateGroupUser(GroupUser entity);
		bool DeleteGroupUser(System.Int32 GroupUserId);
		List<GroupUser> GetAllGroupUser();
		GroupUser InsertGroupUser(GroupUser entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
