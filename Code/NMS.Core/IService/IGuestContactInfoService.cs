﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.GuestContactInfo;

namespace NMS.Core.IService
{
		
	public interface IGuestContactInfoService
	{
        Dictionary<string, string> GetGuestContactInfoBasicSearchColumns();
        
        List<SearchColumn> GetGuestContactInfoAdvanceSearchColumns();

		List<GuestContactInfo> GetGuestContactInfoByCelebrityId(System.Int32? CelebrityId);
		GuestContactInfo GetGuestContactInfo(System.Int32 GuestContactInfoId);
		DataTransfer<List<GetOutput>> GetAll();
		GuestContactInfo UpdateGuestContactInfo(GuestContactInfo entity);
		bool DeleteGuestContactInfo(System.Int32 GuestContactInfoId);
		List<GuestContactInfo> GetAllGuestContactInfo();
		GuestContactInfo InsertGuestContactInfo(GuestContactInfo entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
