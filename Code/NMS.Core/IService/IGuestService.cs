﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Guest;

namespace NMS.Core.IService
{
		
	public interface IGuestService
	{
        Dictionary<string, string> GetGuestBasicSearchColumns();
        
        List<SearchColumn> GetGuestAdvanceSearchColumns();

		Guest GetGuest(System.Int32 GuestId);
		DataTransfer<List<GetOutput>> GetAll();
		Guest UpdateGuest(Guest entity);
		bool DeleteGuest(System.Int32 GuestId);
		List<Guest> GetAllGuest();
		Guest InsertGuest(Guest entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Guest> GetAllGuestBySlotId(int slotId);

        bool DeleteAllGuestBySlotId(System.Int32 SlotId);

        List<Guest> GetGuestEpisodeReport();
    }
	
	
}
