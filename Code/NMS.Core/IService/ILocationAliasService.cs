﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.LocationAlias;

namespace NMS.Core.IService
{
		
	public interface ILocationAliasService
	{
        Dictionary<string, string> GetLocationAliasBasicSearchColumns();
        
        List<SearchColumn> GetLocationAliasAdvanceSearchColumns();

		List<LocationAlias> GetLocationAliasByLocationId(System.Int32 LocationId);
		LocationAlias GetLocationAlias(System.Int32 LocationAliasId);
		DataTransfer<List<GetOutput>> GetAll();
		LocationAlias UpdateLocationAlias(LocationAlias entity);
		bool DeleteLocationAlias(System.Int32 LocationAliasId);
		List<LocationAlias> GetAllLocationAlias();
		LocationAlias InsertLocationAlias(LocationAlias entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        LocationAlias InsertLocationAliasIfNotExists(LocationAlias entity);
	}
	
	
}
