﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Location;

namespace NMS.Core.IService
{
		
	public interface ILocationService
	{
        Dictionary<string, string> GetLocationBasicSearchColumns();
        
        List<SearchColumn> GetLocationAdvanceSearchColumns();

		Location GetLocation(System.Int32 LocationId);
		DataTransfer<List<GetOutput>> GetAll();
		Location UpdateLocation(Location entity);
		bool DeleteLocation(System.Int32 LocationId);
		List<Location> GetAllLocation();
		Location InsertLocation(Location entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Location> GetLocationByTerm(string term);

        Location InsertLocationIfNotExists(Location entity);

        List<Location> GetByDate(DateTime LastUpdateDate);

        Location GetLocationByName(string name);

        LocationAlias GetByAlias(string alias);

        Location GetLocationCreateIfNotExist(Location location);

    }
	
	
}
