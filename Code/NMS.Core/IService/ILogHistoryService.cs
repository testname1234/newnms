﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.LogHistory;

namespace NMS.Core.IService
{
		
	public interface ILogHistoryService
	{
        Dictionary<string, string> GetLogHistoryBasicSearchColumns();
        
        List<SearchColumn> GetLogHistoryAdvanceSearchColumns();

		LogHistory GetLogHistory(System.Int32 LogHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		LogHistory UpdateLogHistory(LogHistory entity);
		bool DeleteLogHistory(System.Int32 LogHistoryId);
		List<LogHistory> GetAllLogHistory();
		LogHistory InsertLogHistory(LogHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
