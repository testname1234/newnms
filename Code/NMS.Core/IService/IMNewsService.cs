﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.News;

namespace NMS.Core.IService
{
		
	public interface IMNewsService
	{
        void InsertNews(MParsedNews parsedNews);
	}
	
	
}
