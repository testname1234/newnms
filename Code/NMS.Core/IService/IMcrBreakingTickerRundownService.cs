﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrBreakingTickerRundown;

namespace NMS.Core.IService
{
		
	public interface IMcrBreakingTickerRundownService
	{
        Dictionary<string, string> GetMcrBreakingTickerRundownBasicSearchColumns();
        
        List<SearchColumn> GetMcrBreakingTickerRundownAdvanceSearchColumns();

		McrBreakingTickerRundown GetMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId);
		DataTransfer<List<GetOutput>> GetAll();
		McrBreakingTickerRundown UpdateMcrBreakingTickerRundown(McrBreakingTickerRundown entity);
		bool DeleteMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId);
		List<McrBreakingTickerRundown> GetAllMcrBreakingTickerRundown();
		McrBreakingTickerRundown InsertMcrBreakingTickerRundown(McrBreakingTickerRundown entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
