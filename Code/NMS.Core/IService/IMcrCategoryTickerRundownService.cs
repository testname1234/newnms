﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrCategoryTickerRundown;

namespace NMS.Core.IService
{
		
	public interface IMcrCategoryTickerRundownService
	{
        Dictionary<string, string> GetMcrCategoryTickerRundownBasicSearchColumns();
        
        List<SearchColumn> GetMcrCategoryTickerRundownAdvanceSearchColumns();

		McrCategoryTickerRundown GetMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId);
		DataTransfer<List<GetOutput>> GetAll();
		McrCategoryTickerRundown UpdateMcrCategoryTickerRundown(McrCategoryTickerRundown entity);
		bool DeleteMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId);
		List<McrCategoryTickerRundown> GetAllMcrCategoryTickerRundown();
		McrCategoryTickerRundown InsertMcrCategoryTickerRundown(McrCategoryTickerRundown entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
