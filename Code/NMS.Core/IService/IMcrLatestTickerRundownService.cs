﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrLatestTickerRundown;

namespace NMS.Core.IService
{
		
	public interface IMcrLatestTickerRundownService
	{
        Dictionary<string, string> GetMcrLatestTickerRundownBasicSearchColumns();
        
        List<SearchColumn> GetMcrLatestTickerRundownAdvanceSearchColumns();

		McrLatestTickerRundown GetMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId);
		DataTransfer<List<GetOutput>> GetAll();
		McrLatestTickerRundown UpdateMcrLatestTickerRundown(McrLatestTickerRundown entity);
		bool DeleteMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId);
		List<McrLatestTickerRundown> GetAllMcrLatestTickerRundown();
		McrLatestTickerRundown InsertMcrLatestTickerRundown(McrLatestTickerRundown entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
