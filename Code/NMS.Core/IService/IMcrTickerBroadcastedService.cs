﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerBroadcasted;

namespace NMS.Core.IService
{
		
	public interface IMcrTickerBroadcastedService
	{
        Dictionary<string, string> GetMcrTickerBroadcastedBasicSearchColumns();
        
        List<SearchColumn> GetMcrTickerBroadcastedAdvanceSearchColumns();

		List<McrTickerBroadcasted> GetMcrTickerBroadcastedByTickerCategoryId(System.Int32 TickerCategoryId);
		McrTickerBroadcasted GetMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId);
		DataTransfer<List<GetOutput>> GetAll();
		McrTickerBroadcasted UpdateMcrTickerBroadcasted(McrTickerBroadcasted entity);
		bool DeleteMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId);
		List<McrTickerBroadcasted> GetAllMcrTickerBroadcasted();
		McrTickerBroadcasted InsertMcrTickerBroadcasted(McrTickerBroadcasted entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<McrTickerBroadcasted> GetMcrTickerBroadcastedByDate(DateTime From, DateTime To);

    }
	
	
}
