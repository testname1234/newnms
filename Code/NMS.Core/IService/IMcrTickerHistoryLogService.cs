﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerHistoryLog;

namespace NMS.Core.IService
{
		
	public interface IMcrTickerHistoryLogService
	{
        Dictionary<string, string> GetMcrTickerHistoryLogBasicSearchColumns();
        
        List<SearchColumn> GetMcrTickerHistoryLogAdvanceSearchColumns();

		McrTickerHistoryLog GetMcrTickerHistoryLog(System.Int32 McrTickerHistoryLogId);
		DataTransfer<List<GetOutput>> GetAll();
		McrTickerHistoryLog UpdateMcrTickerHistoryLog(McrTickerHistoryLog entity);
		bool DeleteMcrTickerHistoryLog(System.Int32 McrTickerHistoryLogId);
		List<McrTickerHistoryLog> GetAllMcrTickerHistoryLog();
		McrTickerHistoryLog InsertMcrTickerHistoryLog(McrTickerHistoryLog entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
