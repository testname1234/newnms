﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerHistory;

namespace NMS.Core.IService
{
		
	public interface IMcrTickerHistoryService
	{
        Dictionary<string, string> GetMcrTickerHistoryBasicSearchColumns();
        
        List<SearchColumn> GetMcrTickerHistoryAdvanceSearchColumns();

		List<McrTickerHistory> GetMcrTickerHistoryByTickerId(System.Int32? TickerId);
		List<McrTickerHistory> GetMcrTickerHistoryByTickerCategoryId(System.Int32? TickerCategoryId);
		McrTickerHistory GetMcrTickerHistory(System.Int32 McrTickerHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		McrTickerHistory UpdateMcrTickerHistory(McrTickerHistory entity);
		bool DeleteMcrTickerHistory(System.Int32 McrTickerHistoryId);
		List<McrTickerHistory> GetAllMcrTickerHistory();
		McrTickerHistory InsertMcrTickerHistory(McrTickerHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<McrTickerHistory> GetLastBroadcastedTickers(int tickerCategoryId, int size, List<int> tickerIds);
        List<McrTickerHistory> GetMcrTickerHistoryByBroadcastedId(int Id);
    }
	
	
}
