﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerRundownStatus;

namespace NMS.Core.IService
{
		
	public interface IMcrTickerRundownStatusService
	{
        Dictionary<string, string> GetMcrTickerRundownStatusBasicSearchColumns();
        
        List<SearchColumn> GetMcrTickerRundownStatusAdvanceSearchColumns();

		List<McrTickerRundownStatus> GetMcrTickerRundownStatusByStatusId(System.Int32 StatusId);
		McrTickerRundownStatus GetMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		McrTickerRundownStatus UpdateMcrTickerRundownStatus(McrTickerRundownStatus entity);
		bool DeleteMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId);
		List<McrTickerRundownStatus> GetAllMcrTickerRundownStatus();
		McrTickerRundownStatus InsertMcrTickerRundownStatus(McrTickerRundownStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
