﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerStatus;

namespace NMS.Core.IService
{
		
	public interface IMcrTickerStatusService
	{
        Dictionary<string, string> GetMcrTickerStatusBasicSearchColumns();
        
        List<SearchColumn> GetMcrTickerStatusAdvanceSearchColumns();

		McrTickerStatus GetMcrTickerStatus(System.Int32 McrTickerStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		McrTickerStatus UpdateMcrTickerStatus(McrTickerStatus entity);
		bool DeleteMcrTickerStatus(System.Int32 McrTickerStatusId);
		List<McrTickerStatus> GetAllMcrTickerStatus();
		McrTickerStatus InsertMcrTickerStatus(McrTickerStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
