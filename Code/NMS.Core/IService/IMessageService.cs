﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Message;

namespace NMS.Core.IService
{

    public interface IMessageService
    {
        Dictionary<string, string> GetMessageBasicSearchColumns();

        List<SearchColumn> GetMessageAdvanceSearchColumns();

        Message GetMessage(System.Int32 MessageId);
        DataTransfer<List<GetOutput>> GetAll();
        Message UpdateMessage(Message entity);
        bool DeleteMessage(System.Int32 MessageId);
        List<Message> GetAllMessage();
        Message InsertMessage(Message entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Message> GetAllUserMessages(int UserId);
        List<Message> GetAllUserPendingMessagesBySlotScreenTemplateId(int SlotScreenTemplateId);
        List<Message> GetAllUserPendingMessages(int UserId, List<int> MessageId);
        List<Message> GetMessages(int SlotScreenTemplateId, DateTime lastUpdateDate);
    }
	
	
}
