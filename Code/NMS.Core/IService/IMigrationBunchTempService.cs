﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MigrationBunchTemp;

namespace NMS.Core.IService
{
		
	public interface IMigrationBunchTempService
	{
        Dictionary<string, string> GetMigrationBunchTempBasicSearchColumns();
        
        List<SearchColumn> GetMigrationBunchTempAdvanceSearchColumns();

		MigrationBunchTemp GetMigrationBunchTemp(System.Int32 MigrationBunchTempId);
		DataTransfer<List<GetOutput>> GetAll();
		MigrationBunchTemp UpdateMigrationBunchTemp(MigrationBunchTemp entity);
		bool DeleteMigrationBunchTemp(System.Int32 MigrationBunchTempId);
		List<MigrationBunchTemp> GetAllMigrationBunchTemp();
		MigrationBunchTemp InsertMigrationBunchTemp(MigrationBunchTemp entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
