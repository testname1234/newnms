﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosActiveEpisode;

namespace NMS.Core.IService
{
		
	public interface IMosActiveEpisodeService
	{
        Dictionary<string, string> GetMosActiveEpisodeBasicSearchColumns();
        
        List<SearchColumn> GetMosActiveEpisodeAdvanceSearchColumns();

		List<MosActiveEpisode> GetMosActiveEpisodeByEpisodeId(System.Int32? EpisodeId);
		MosActiveEpisode GetMosActiveEpisode(System.Int32 MosActiveEpisodeId);
		DataTransfer<List<GetOutput>> GetAll();
		MosActiveEpisode UpdateMosActiveEpisode(MosActiveEpisode entity);
		bool DeleteMosActiveEpisode(System.Int32 MosActiveEpisodeId);
		List<MosActiveEpisode> GetAllMosActiveEpisode();
		MosActiveEpisode InsertMosActiveEpisode(MosActiveEpisode entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        MosActiveEpisode InsertUpdateIfExist(MosActiveEpisode mosActiveEpisode);
        List<MosActiveEpisode> GetMosActiveEpisodeByStatus(System.Int32? StatusCode);

        void UpdateStatus(int P1, int P2, string P3, string P4);
        MosActiveEpisode InsertIfNotExist(MosActiveEpisode mosActiveEpisode);
	}
	
	
}
