﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosActiveItem;

namespace NMS.Core.IService
{
		
	public interface IMosActiveItemService
	{
        Dictionary<string, string> GetMosActiveItemBasicSearchColumns();
        
        List<SearchColumn> GetMosActiveItemAdvanceSearchColumns();

		List<MosActiveItem> GetMosActiveItemByEpisodeId(System.Int32? EpisodeId);
		MosActiveItem GetMosActiveItem(System.Int32 MosActiveItemId);
		DataTransfer<List<GetOutput>> GetAll();
		MosActiveItem UpdateMosActiveItem(MosActiveItem entity);
		bool DeleteMosActiveItem(System.Int32 MosActiveItemId);
		List<MosActiveItem> GetAllMosActiveItem();
		MosActiveItem InsertMosActiveItem(MosActiveItem entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<MosActiveItem> GetMosActiveItemWithRunDownByEpisodeId(System.Int32? EpisodeId);
	}
	
	
}
