﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosActiveTransformation;

namespace NMS.Core.IService
{
		
	public interface IMosActiveTransformationService
	{
        Dictionary<string, string> GetMosActiveTransformationBasicSearchColumns();
        
        List<SearchColumn> GetMosActiveTransformationAdvanceSearchColumns();

		List<MosActiveTransformation> GetMosActiveTransformationByMosActiveItemId(System.Int32? MosActiveItemId);
		MosActiveTransformation GetMosActiveTransformation(System.Int32 MosActiveDetailId);
		DataTransfer<List<GetOutput>> GetAll();
		MosActiveTransformation UpdateMosActiveTransformation(MosActiveTransformation entity);
		bool DeleteMosActiveTransformation(System.Int32 MosActiveDetailId);
		List<MosActiveTransformation> GetAllMosActiveTransformation();
		MosActiveTransformation InsertMosActiveTransformation(MosActiveTransformation entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
