﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosItem;

namespace NMS.Core.IService
{
		
	public interface IMosItemService
	{
        Dictionary<string, string> GetMosItemBasicSearchColumns();
        
        List<SearchColumn> GetMosItemAdvanceSearchColumns();

		MosItem GetMosItem(System.Int32 CasperMosItemId);
		DataTransfer<List<GetOutput>> GetAll();
		MosItem UpdateMosItem(MosItem entity);
		bool DeleteMosItem(System.Int32 CasperMosItemId);
		List<MosItem> GetAllMosItem();
		MosItem InsertMosItem(MosItem entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
