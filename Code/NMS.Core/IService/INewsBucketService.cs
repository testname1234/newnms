﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsBucket;

namespace NMS.Core.IService
{
		
	public interface INewsBucketService
	{
        Dictionary<string, string> GetNewsBucketBasicSearchColumns();
        
        List<SearchColumn> GetNewsBucketAdvanceSearchColumns();

		NewsBucket GetNewsBucket(System.Int32 NewsBucketId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsBucket UpdateNewsBucket(NewsBucket entity);
		bool DeleteNewsBucket(System.Int32 NewsBucketId);
		List<NewsBucket> GetAllNewsBucket();
		NewsBucket InsertNewsBucket(NewsBucket entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<NewsBucket> GetNewsBucketByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);
        List<NewsBucket> GetNewsBucketByProgramIds(string CommaSepratedeIds);

        NewsBucket UpdateSequence(NewsBucket item);
    }
	
	
}
