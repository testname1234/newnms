﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsCameraMan;

namespace NMS.Core.IService
{
		
	public interface INewsCameraManService
	{
        Dictionary<string, string> GetNewsCameraManBasicSearchColumns();
        
        List<SearchColumn> GetNewsCameraManAdvanceSearchColumns();

		NewsCameraMan GetNewsCameraMan(System.Int32 NewsCameraManId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsCameraMan UpdateNewsCameraMan(NewsCameraMan entity);
		bool DeleteNewsCameraMan(System.Int32 NewsCameraManId);
		List<NewsCameraMan> GetAllNewsCameraMan();
		NewsCameraMan InsertNewsCameraMan(NewsCameraMan entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
