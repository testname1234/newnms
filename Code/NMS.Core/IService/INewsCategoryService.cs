﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsCategory;

namespace NMS.Core.IService
{
		
	public interface INewsCategoryService
	{
        Dictionary<string, string> GetNewsCategoryBasicSearchColumns();
        
        List<SearchColumn> GetNewsCategoryAdvanceSearchColumns();

		List<NewsCategory> GetNewsCategoryByNewsId(System.Int32 NewsId);
		List<NewsCategory> GetNewsCategoryByCategoryId(System.Int32 CategoryId);
		NewsCategory GetNewsCategory(System.Int32 NewsCategoryId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsCategory UpdateNewsCategory(NewsCategory entity);
		bool DeleteNewsCategory(System.Int32 NewsCategoryId);
		List<NewsCategory> GetAllNewsCategory();
		NewsCategory InsertNewsCategory(NewsCategory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteNewsCategoryByNewsId(System.Int32 NewsId);
	}
	
	
}
