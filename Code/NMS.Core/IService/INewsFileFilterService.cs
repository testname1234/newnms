﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFileFilter;

namespace NMS.Core.IService
{
		
	public interface INewsFileFilterService
	{
        Dictionary<string, string> GetNewsFileFilterBasicSearchColumns();
        
        List<SearchColumn> GetNewsFileFilterAdvanceSearchColumns();

		List<NewsFileFilter> GetNewsFileFilterByFilterId(System.Int32? FilterId);
		List<NewsFileFilter> GetNewsFileFilterByNewsFileId(System.Int32? NewsFileId);
		NewsFileFilter GetNewsFileFilter(System.Int32 NewsFileFilterId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsFileFilter UpdateNewsFileFilter(NewsFileFilter entity);
		bool DeleteNewsFileFilter(System.Int32 NewsFileFilterId);
		List<NewsFileFilter> GetAllNewsFileFilter();
		NewsFileFilter InsertNewsFileFilter(NewsFileFilter entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
