﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFileHistory;

namespace NMS.Core.IService
{
		
	public interface INewsFileHistoryService
	{
        Dictionary<string, string> GetNewsFileHistoryBasicSearchColumns();
        
        List<SearchColumn> GetNewsFileHistoryAdvanceSearchColumns();

		List<NewsFileHistory> GetNewsFileHistoryByNewsFileId(System.Int32 NewsFileId);
		NewsFileHistory GetNewsFileHistory(System.Int32 NewsFileHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsFileHistory UpdateNewsFileHistory(NewsFileHistory entity);
		bool DeleteNewsFileHistory(System.Int32 NewsFileHistoryId);
		List<NewsFileHistory> GetAllNewsFileHistory();
		NewsFileHistory InsertNewsFileHistory(NewsFileHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
