﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFileOrganization;

namespace NMS.Core.IService
{
		
	public interface INewsFileOrganizationService
	{
        Dictionary<string, string> GetNewsFileOrganizationBasicSearchColumns();
        
        List<SearchColumn> GetNewsFileOrganizationAdvanceSearchColumns();

		NewsFileOrganization GetNewsFileOrganization(System.Int32 NewsFileOrganizationId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsFileOrganization UpdateNewsFileOrganization(NewsFileOrganization entity);
		bool DeleteNewsFileOrganization(System.Int32 NewsFileOrganizationId);
		List<NewsFileOrganization> GetAllNewsFileOrganization();
		NewsFileOrganization InsertNewsFileOrganization(NewsFileOrganization entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
