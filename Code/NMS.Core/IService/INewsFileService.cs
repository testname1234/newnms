﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFile;

namespace NMS.Core.IService
{

    public interface INewsFileService
    {
        Dictionary<string, string> GetNewsFileBasicSearchColumns();

        List<SearchColumn> GetNewsFileAdvanceSearchColumns();

        List<NewsFile> GetNewsFileByFolderId(System.Int32 FolderId);
        NewsFile GetNewsFile(System.Int32 NewsFileId);
        DataTransfer<List<GetOutput>> GetAll();
        NewsFile UpdateNewsFile(NewsFile entity);
        bool DeleteNewsFile(System.Int32 NewsFileId);
        List<NewsFile> GetAllNewsFile();
        NewsFile InsertNewsFile(NewsFile entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);


        DataTransfer.NewsFile.GET.NMSPollingOutput GetNewsFiles(DataTransfer.NewsFile.GET.NMSPollingInput input);



        NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput UpdateNewsFileAction(PostInput input);

        //NewsFile UpdateNewsFile(PostInput input);

        NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput UpdateNewsFile(DataTransfer.FileDetail.PostOutput input);

        NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput UpdateNewsFileStatus(PostInput input);

        FileStatusHistory PublishNews(PostInput input);

        DataTransfer.NewsFile.GET.NMSPollingOutput InsertUpdateNewsFile(NMS.Core.DataTransfer.FileDetail.PostOutput input);

        DataTransfer.NewsFile.GET.NMSPollingOutput GetRundownByEpisodeId(int episodeId);
        DataTransfer.NewsFile.GET.NMSPollingOutput GetRundownByEpisodeIdFromDB(int episodeId);


        bool DeleteFileResouceByGuid(string guid);

        bool UpdateNewsFileSequence(int Id, int sequenceId);

        NewsFile InsertNews(NMS.Core.DataTransfer.News.PostInput postInput);

        List<MNews> GetNewsByReporterId(int rId, int pageCount, int startIndex);
        List<MNews> MyNewsPolling(PollingObject obj);

        MNews GetNews(string id);

        bool MarkNewsFilestatus(PostInput input);

        bool MarkNewsVerificationStatus(PostInput input);

        DataTransfer.NewsFile.GET.NMSPollingOutput GetAllNewsFilesData(DataTransfer.NewsFile.GET.NMSPollingInput input);
     //   List<NewsFile> GetNewsFileForProducer(List<Filter> filters, DateTime newsLastUpdateDate, bool includeProgramRelatedNews, string term, string PageOffset, string PageNumber);
        MNews ConvertNewsFileToMNews(NewsFile newsfile);
        List<NewsFile> GetNewsFileWithFilterFromTo(List<Filter> filters, DateTime From, int[] folderids,string term, int PageOffset,int PageSize, DateTime? To);

        bool CheckIfNewsExistsSQL(string title, string source);

        List<NewsFile> GetNewsFileByProgramIdwithstatus(int ProgramId, string SelectClause = null);

        NewsFile UpdateNewsFileSequenceByNewsFileId(int Id, int sequenceId);
        List<NewsFile> GetNewsFilesByFolderId(int FolderId, int StoryCount);
        bool UpdateProgramCountAndHistory(int programId);

        int GetMaxSequenceNumByFolderId(System.Int32 FolderId);

        List<NewsFile> GetSocialMediaNewsFileWithFilterFromTo(List<Filter> filters, DateTime from, bool includeProgramRelatedNews, string term, string PageOffset, string PageSize, DateTime? to, string SocialMediaFilterId);

        NewsFile OrganizationTagsUpdate(NMS.Core.DataTransfer.News.PostInput input);

        List<NewsFile> GetNewsFileForTaggReport(DateTime? lstUpdate = null);
        NewsFile UpdateNewsFileCategoryLocationById(int NewsFileId, int CategoryId, int type);
        List<NewsFile> GetBroadcastedNewsFiles(DateTime from, DateTime to);

        List<NewsFile> GetBroadcatedNewsFileDetail(int Id);

        NewsFileHistory InsertNewsHistory(NMS.Core.DataTransfer.News.PostInput input);

        NewsFile UpdateNewsFileWithHistory(NMS.Core.DataTransfer.News.PostInput postInput);

        bool UpdateIsLiveBitNewsFile(int programid);

        bool MarkTitleVoiceOverStatus(NMS.Core.DataTransfer.FileDetail.PostOutput res);

        List<FileDetail> GetApprovedNewsfile(int[] programIds);

    }
	
	
}
