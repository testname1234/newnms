﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFilter;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.IService
{
		
	public interface INewsFilterService
	{
        Dictionary<string, string> GetNewsFilterBasicSearchColumns();
        
        List<SearchColumn> GetNewsFilterAdvanceSearchColumns();

		List<NewsFilter> GetNewsFilterByFilterId(System.Int32 FilterId);
		List<NewsFilter> GetNewsFilterByNewsId(System.Int32 NewsId);
		NewsFilter GetNewsFilter(System.Int32 NewsFilterId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsFilter UpdateNewsFilter(NewsFilter entity);
		bool DeleteNewsFilter(System.Int32 NewsFilterId);
        bool DeleteNewsFilterByNewsId(System.Int32 NewsId);
		List<NewsFilter> GetAllNewsFilter();
		NewsFilter InsertNewsFilter(NewsFilter entity);
        bool InsertNewsFilter(MNewsFilter entity);
        bool CheckIfNewsFilterExists(MNewsFilter entity);
        bool DeleteNewsFilterByNewsAndFilterId(MNewsFilter entity);


        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        NewsFilter GetByFilterIdAndNewsId(System.Int32 FilterId, System.Int32 NewsId);
	}
	
	
}
