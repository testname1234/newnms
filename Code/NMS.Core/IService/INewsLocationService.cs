﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsLocation;

namespace NMS.Core.IService
{
		
	public interface INewsLocationService
	{
        Dictionary<string, string> GetNewsLocationBasicSearchColumns();
        
        List<SearchColumn> GetNewsLocationAdvanceSearchColumns();

		List<NewsLocation> GetNewsLocationByNewsId(System.Int32? NewsId);
		List<NewsLocation> GetNewsLocationByLocationId(System.Int32? LocationId);
		NewsLocation GetNewsLocation(System.Int32 NewsLocationid);
		DataTransfer<List<GetOutput>> GetAll();
		NewsLocation UpdateNewsLocation(NewsLocation entity);
		bool DeleteNewsLocation(System.Int32 NewsLocationid);
		List<NewsLocation> GetAllNewsLocation();
		NewsLocation InsertNewsLocation(NewsLocation entity);
        bool DeleteNewsLocationByNewsId(System.Int32 Newsid);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
