﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaperPage;

namespace NMS.Core.IService
{
		
	public interface INewsPaperPageService
	{
        Dictionary<string, string> GetNewsPaperPageBasicSearchColumns();
        
        List<SearchColumn> GetNewsPaperPageAdvanceSearchColumns();

		List<NewsPaperPage> GetNewsPaperPageByDailyNewsPaperId(System.Int32 DailyNewsPaperId);
		NewsPaperPage GetNewsPaperPage(System.Int32 NewsPaperPageId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsPaperPage UpdateNewsPaperPage(NewsPaperPage entity);
		bool DeleteNewsPaperPage(System.Int32 NewsPaperPageId);
		List<NewsPaperPage> GetAllNewsPaperPage();
		NewsPaperPage InsertNewsPaperPage(NewsPaperPage entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        
        List<NewsPaperPage> GetNewsPaperPagesByDailyNewsPaperDate(DateTime fromDate, DateTime toDate);
        void MarkIsProcessed(int id);
	}
	
	
}
