﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaperPagesPart;

namespace NMS.Core.IService
{
		
	public interface INewsPaperPagesPartService
	{
        Dictionary<string, string> GetNewsPaperPagesPartBasicSearchColumns();
        
        List<SearchColumn> GetNewsPaperPagesPartAdvanceSearchColumns();

		List<NewsPaperPagesPart> GetNewsPaperPagesPartByNewsPaperPageId(System.Int32? NewsPaperPageId);
		NewsPaperPagesPart GetNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsPaperPagesPart UpdateNewsPaperPagesPart(NewsPaperPagesPart entity);
		bool DeleteNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId);
		List<NewsPaperPagesPart> GetAllNewsPaperPagesPart();
		NewsPaperPagesPart InsertNewsPaperPagesPart(NewsPaperPagesPart entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<NewsPaperPagesPart> GetNewsPaperPagesPartsByDailyNewsPaperDate(DateTime fromDate, DateTime toDate);
	}
	
	
}
