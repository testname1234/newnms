﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaperPagesPartsDetail;

namespace NMS.Core.IService
{
		
	public interface INewsPaperPagesPartsDetailService
	{
        Dictionary<string, string> GetNewsPaperPagesPartsDetailBasicSearchColumns();
        
        List<SearchColumn> GetNewsPaperPagesPartsDetailAdvanceSearchColumns();

		List<NewsPaperPagesPartsDetail> GetNewsPaperPagesPartsDetailByNewsPaperPagesPartId(System.Int32? NewsPaperPagesPartId);
		NewsPaperPagesPartsDetail GetNewsPaperPagesPartsDetail(System.Int32 NewsPaperPagesPartsDetailId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsPaperPagesPartsDetail UpdateNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetail entity);
		bool DeleteNewsPaperPagesPartsDetail(System.Int32 NewsPaperPagesPartsDetailId);
		List<NewsPaperPagesPartsDetail> GetAllNewsPaperPagesPartsDetail();
		NewsPaperPagesPartsDetail InsertNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetail entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<NewsPaperPagesPartsDetail> GetNewsPaperPagesPartsDetailByDailyNewsPaperDate(DateTime fromDate, DateTime toDate);
	}
	
	
}
