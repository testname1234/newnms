﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaper;

namespace NMS.Core.IService
{
		
	public interface INewsPaperService
	{
        Dictionary<string, string> GetNewsPaperBasicSearchColumns();
        
        List<SearchColumn> GetNewsPaperAdvanceSearchColumns();

		NewsPaper GetNewsPaper(System.Int32 NewsPaperId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsPaper UpdateNewsPaper(NewsPaper entity);
		bool DeleteNewsPaper(System.Int32 NewsPaperId);
		List<NewsPaper> GetAllNewsPaper();
		NewsPaper InsertNewsPaper(NewsPaper entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
