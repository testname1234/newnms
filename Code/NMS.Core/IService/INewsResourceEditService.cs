﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsResourceEdit;

namespace NMS.Core.IService
{
		
	public interface INewsResourceEditService
	{
        Dictionary<string, string> GetNewsResourceEditBasicSearchColumns();
        
        List<SearchColumn> GetNewsResourceEditAdvanceSearchColumns();

        List<NewsResourceEdit> GetActiveNewsResourceEdit();

        void SetInactive(int id);
        List<NewsResourceEdit> GetNewsResourceEditByNewsId(System.Int32? NewsId);
		NewsResourceEdit GetNewsResourceEdit(System.Int32 NewsResourceEditId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsResourceEdit UpdateNewsResourceEdit(NewsResourceEdit entity);
		bool DeleteNewsResourceEdit(System.Int32 NewsResourceEditId);
		List<NewsResourceEdit> GetAllNewsResourceEdit();
		NewsResourceEdit InsertNewsResourceEdit(NewsResourceEdit entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
