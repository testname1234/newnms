﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsResource;

namespace NMS.Core.IService
{
		
	public interface INewsResourceService
	{
        Dictionary<string, string> GetNewsResourceBasicSearchColumns();
        
        List<SearchColumn> GetNewsResourceAdvanceSearchColumns();

		NewsResource GetNewsResource(System.Int32 NewsResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsResource UpdateNewsResource(NewsResource entity);
		bool DeleteNewsResource(System.Int32 NewsResourceId);
		List<NewsResource> GetAllNewsResource();
		NewsResource InsertNewsResource(NewsResource entity);
        bool DeleteNewsResourceByNewsId(System.Int32 NewsId);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<NewsResource> GetNewsResourceByNewsId(System.Int32 NewsId);
	}
	
	
}
