﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.News;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Models;
using SolrManager;
using SolrManager.InputEntities;

namespace NMS.Core.IService
{

    public interface INewsService
    {
        Dictionary<string, string> GetNewsBasicSearchColumns();

        List<SearchColumn> GetNewsAdvanceSearchColumns();

        List<News> GetNewsByLocationId(System.Int32? LocationId);
        List<News> GetNewsByNewsTypeId(System.Int32 NewsTypeId);
        News GetNews(System.Int32 NewsId);
        DataTransfer<List<GetOutput>> GetAll();
        News UpdateNews(News entity);


        bool DeleteNews(System.Int32 NewsId);
        List<News> GetAllNews();

        void EditResourceVideo(object obj);
        News InsertNews(News entity);
        bool InsertNews(MNews entity);

        NMS.Core.DataTransfer.Resource.PostInput GenerateResource(Core.Models.ResourceEdit resourceEdit);
        DataTransfer<GetOutput> Get(string id);
        DataTransfer<string> Delete(string id);

        InsertNewsStatus InsertRawNews(RawNews news);
        MNews InsertReportedNews(PostInput news);
        void UpdateScrapDate(ScrapMaxDates ScrapMaxDates);

        List<MNews> GetReportedNews(int reportedId, int pageCount, int startIndex);

        List<MNews> GetNewsByFilterIds(List<int> filterIds, int pageCount, int startIndex, DateTime from, DateTime to, string term);

        List<MNews> GetNewsByFilterIds(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discardedFilters = null);

        List<MNews> GetNewsWithBunch(string bunchId, List<Filter> filterIds, int pageCount, int startIndex, DateTime from, DateTime to);

        List<MNewsFilter> GetUpdatedNewsFilters(DateTime lastUpdateDate, int? reporterId = null, List<string> newsIds = null);

        List<MNews> GetNewsByBunchId(string bunchId);

        bool UpdateThumbId(string newsId, string resGuid);

        bool MarkVerifyNews(List<string> ids, string bunchId, bool isVerified);

        MNews GetNewsById(string id);

        void UpdateFilterCount();        

        List<MFilterCount> GetFilterCountByLastUpdateDate(List<Filter> filterIds, DateTime from, DateTime to,List<int> programFilterIds);

        
        void MarkNewsTopRated();

        DateTime GetMaxScrapDate(ScrapMaxDates ScrapMaxDates);


        MComment InsertComment(MComment mComment);

        List<MComment> GetCommentsByLastUpdateDate(DateTime commentsLastUpdate, int reporterId);


        List<MNews> GetNewsWithUpdatesAndRelatedById(string newsId, string bunchId);
        bool CheckIfNewsExists(RawNews parsedNews, out InsertNewsStatus newsStatus);

        List<MDescrepencyNews> GetAllDescrepancyNews(DescrepencyType descrepencyType);

        List<MNews> GetNewsByTitle(string term);

        List<MDescrepencyNews> GetAllDescrepancyNews(DescrepencyType descrepencyType, string descrepancyValue);

        bool DeleteDescrepancyNews(int DescrepancyNewsID);

        void ProcessProducerDataPolling();

        List<News> GetDisitnctNewsSource();

        DateTime GetLastNewsDate();

        News GetByNewsGuid(System.String NewsGuid);

        News GetByNewsGuidAndIsComplete(System.String NewsGuid, bool isComplete, string SelectClause = null);


        News InsertUpdateNewsIfExist(News entity);

        bool CheckIfNewsExists(MNews entity);

        bool DeleteNews(MNews entity);

        List<MNews> GetNewsByFilterIds(List<Filter> filters, DateTime lastUpdateddate, int PageSize);
        List<MBunch> GetBunchsByFilterIds(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null);

        List<MNews> GetByIDs(List<string> NewsIds);

        //LoadNews GetNewsAndBunch(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false);
        LoadNews GetNewsAndBunchCache(List<Filter> filters, int pageCount, int startIndex, string from, string to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false);
        LoadNews GetNewsAndBunchCached(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null);

        int SeNewsCache(SolrService<SolarNews> sService, int MigrationType = 0);

        bool UpdateNewsStatistics(NewsStatisticsInput newsStatisticsParam, string newsId, NewsStatisticType type);
        bool DeleteNewsStatistics(string newsId, int episodeId);
        void IndexNewsOptimized();

        void UpdateGetNewsAndBunchCache();

        News GetNewsByIdSql(string id);

        bool CheckIfNewsExistsSQL(RawNews parsedNews, out InsertNewsStatus newsStatus);

        LoadNews GetNewsAndBunchWithArchival(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false);

        LoadNews GetNewsAndBunchOptimized(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false);
        List<MDescrepencyNews> GetDiscrepencyBystatus(DescrepencyType descrepancyTypeId, int Status);
        void UpdateDescrepancyNewsByValue(int descrepencyType, string DescrepencyValue);
        List<MDescrepencyNews> GetAllDescrepancyByStatus(int DescrepencyType);
        List<MDescrepencyNews> GetDescrepancyByTitleAndSource(string Title, string Source);

        List<TickerImage> GetTickerImages(DateTime lastUpdateDate);
        List<TickerImage> GetTickerImagesByLastUpdateDate(DateTime lastUpdateDate);
        MNews EditNews(PostInput postInput);

    }
}
