﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsSource;

namespace NMS.Core.IService
{
		
	public interface INewsSourceService
	{
        Dictionary<string, string> GetNewsSourceBasicSearchColumns();
        
        List<SearchColumn> GetNewsSourceAdvanceSearchColumns();

		NewsSource GetNewsSource(System.Int32 NewsSourceId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsSource UpdateNewsSource(NewsSource entity);
		bool DeleteNewsSource(System.Int32 NewsSourceId);
		List<NewsSource> GetAllNewsSource();
		NewsSource InsertNewsSource(NewsSource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
