﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsStatistics;

namespace NMS.Core.IService
{
		
	public interface INewsStatisticsService
	{
        Dictionary<string, string> GetNewsStatisticsBasicSearchColumns();
        
        List<SearchColumn> GetNewsStatisticsAdvanceSearchColumns();

		NewsStatistics GetNewsStatistics(System.Int32 NewsStatisticsId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsStatistics UpdateNewsStatistics(NewsStatistics entity);
		bool DeleteNewsStatistics(System.Int32 NewsStatisticsId);
		List<NewsStatistics> GetAllNewsStatistics();
		NewsStatistics InsertNewsStatistics(NewsStatistics entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
