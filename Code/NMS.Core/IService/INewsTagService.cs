﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsTag;

namespace NMS.Core.IService
{
		
	public interface INewsTagService
	{
        Dictionary<string, string> GetNewsTagBasicSearchColumns();
        
        List<SearchColumn> GetNewsTagAdvanceSearchColumns();

		List<NewsTag> GetNewsTagByTagId(System.Int32 TagId);
		List<NewsTag> GetNewsTagByNewsId(System.Int32 NewsId);
		NewsTag GetNewsTag(System.Int32 NewsTagId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsTag UpdateNewsTag(NewsTag entity);
		bool DeleteNewsTag(System.Int32 NewsTagId);
		List<NewsTag> GetAllNewsTag();
		NewsTag InsertNewsTag(NewsTag entity);
        bool DeleteNewsTagBYNewsId(System.Int32 NewsId);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
