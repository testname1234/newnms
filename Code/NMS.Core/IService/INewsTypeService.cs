﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsType;

namespace NMS.Core.IService
{
		
	public interface INewsTypeService
	{
        Dictionary<string, string> GetNewsTypeBasicSearchColumns();
        
        List<SearchColumn> GetNewsTypeAdvanceSearchColumns();

		NewsType GetNewsType(System.Int32 NewsTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsType UpdateNewsType(NewsType entity);
		bool DeleteNewsType(System.Int32 NewsTypeId);
		List<NewsType> GetAllNewsType();
		NewsType InsertNewsType(NewsType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
