﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsUpdateHistory;

namespace NMS.Core.IService
{
		
	public interface INewsUpdateHistoryService
	{
        Dictionary<string, string> GetNewsUpdateHistoryBasicSearchColumns();
        
        List<SearchColumn> GetNewsUpdateHistoryAdvanceSearchColumns();

		NewsUpdateHistory GetNewsUpdateHistory(System.Int32 NewsUpdateHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		NewsUpdateHistory UpdateNewsUpdateHistory(NewsUpdateHistory entity);
		bool DeleteNewsUpdateHistory(System.Int32 NewsUpdateHistoryId);
		List<NewsUpdateHistory> GetAllNewsUpdateHistory();
		NewsUpdateHistory InsertNewsUpdateHistory(NewsUpdateHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<NewsUpdateHistory> GetByNewsGuid(System.String NewsGuid);
	}
	
	
}
