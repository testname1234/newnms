﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Notification;
using NMS.Core.Enums;

namespace NMS.Core.IService
{
		
	public interface INotificationService
	{
        Dictionary<string, string> GetNotificationBasicSearchColumns();
        
        List<SearchColumn> GetNotificationAdvanceSearchColumns();

		List<Notification> GetNotificationByNotificationTypeId(System.Int32 NotificationTypeId);
		Notification GetNotification(System.Int32 NotificationId);
		DataTransfer<List<GetOutput>> GetAll();
		Notification UpdateNotification(Notification entity);
		bool DeleteNotification(System.Int32 NotificationId);
		List<Notification> GetAllNotification();
        List<Notification> GetNotifications(int userId, DateTime? lastUpdateDate = null, int rowCount = 10);
		Notification InsertNotification(Notification entity);
        bool MarkAsRead(System.Int32 notificationId, int userId);
        bool Notify(int senderId, string senderName, int recipientId, NotificationTypes notificationType, Dictionary<string, string> arguments = null);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool DeleteByJsonSlotId(System.Int32 SlotId);

        bool Notify(int senderId, string senderName, int recipientId, NotificationTypes notificationType, int slotId, int SlotScreenTemplateId, Dictionary<string, string> arguments = null);

        bool DeleteBySlotId(System.Int32 SlotId);
        bool DeleteBySlotScreenTemplateId(System.Int32 Slotscrentemplateid);
	}
	
	
}
