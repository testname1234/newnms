﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NotificationType;

namespace NMS.Core.IService
{
		
	public interface INotificationTypeService
	{
        Dictionary<string, string> GetNotificationTypeBasicSearchColumns();
        
        List<SearchColumn> GetNotificationTypeAdvanceSearchColumns();

		NotificationType GetNotificationType(System.Int32 NotificationTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		NotificationType UpdateNotificationType(NotificationType entity);
		bool DeleteNotificationType(System.Int32 NotificationTypeId);
		List<NotificationType> GetAllNotificationType();
		NotificationType InsertNotificationType(NotificationType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
