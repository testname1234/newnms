﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.OnAirTickerLine;

namespace NMS.Core.IService
{
		
	public interface IOnAirTickerLineService
	{
        Dictionary<string, string> GetOnAirTickerLineBasicSearchColumns();
        
        List<SearchColumn> GetOnAirTickerLineAdvanceSearchColumns();

		List<OnAirTickerLine> GetOnAirTickerLineByOnAirTickerId(System.Int32? OnAirTickerId);
		OnAirTickerLine GetOnAirTickerLine(System.Int32 OnAirTickerLineId);
		DataTransfer<List<GetOutput>> GetAll();
		OnAirTickerLine UpdateOnAirTickerLine(OnAirTickerLine entity);
		bool DeleteOnAirTickerLine(System.Int32 OnAirTickerLineId);
		List<OnAirTickerLine> GetAllOnAirTickerLine();
        OnAirTickerLine InsertOnAirTickerLine(OnAirTickerLine entity, int? type);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<OnAirTickerLine> GetOnAirTickerLineByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);

        List<OnAirTickerLine> SubmitOnAirTickerLineToMcrBreaking(OnAirTickerLine OnAirTickerLine);
        List<OnAirTickerLine> SubmitOnAirTickerLineToMcrLatest(OnAirTickerLine OnAirTickerLine);
        List<OnAirTickerLine> SubmitOnAirTickerLineToMcrCategory(OnAirTickerLine temp);
        
        bool DeleteOnAirTickerLineFromMCRBreaking(OnAirTickerLine tempObj);
        bool DeleteOnAirTickerLineFromMCRLatest(OnAirTickerLine tempObj);
        bool DeleteOnAirTickerLineFromMCRCategory(OnAirTickerLine tempObj);
        
        bool MigrateGroupToOnAiredTable(OnAirTickerLine tempObj, int type);



        OnAirTickerLine UpdateOnAirTickerLineSequenceNumber(OnAirTickerLine item);

        List<OnAirTickerLine> GetLatestOnAirTickerLine(DateTime dateTime);
    }
	
	
}
