﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.OnAirTicker;

namespace NMS.Core.IService
{

    public interface IOnAirTickerService
    {
        Dictionary<string, string> GetOnAirTickerBasicSearchColumns();

        List<SearchColumn> GetOnAirTickerAdvanceSearchColumns();

        OnAirTicker GetOnAirTicker(System.Int32 OnAirTickerId);
        DataTransfer<List<GetOutput>> GetAll();
        OnAirTicker UpdateOnAirTicker(OnAirTicker entity);
        bool DeleteOnAirTicker(System.Int32 OnAirTickerId);
        List<OnAirTicker> GetAllOnAirTicker();
        OnAirTicker InsertOnAirTicker(OnAirTicker entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        OnAirTicker FillOnAirTicker(OnAirTicker item);
        List<OnAirTicker> FillOnAirTicker(List<OnAirTicker> onAirTickers);
        List<OnAirTicker> GetOnAirTickerByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);
        Dictionary<string, int> GetSequenceNumbers();
        OnAirTicker UpdateOnAirTickerSequenceNumber(OnAirTicker item);

        List<OnAirTicker> GetLatestOnAirTicker(DateTime dateTime);
    }


}
