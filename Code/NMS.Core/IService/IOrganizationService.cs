﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Organization;

namespace NMS.Core.IService
{
		
	public interface IOrganizationService
	{
        Dictionary<string, string> GetOrganizationBasicSearchColumns();
        
        List<SearchColumn> GetOrganizationAdvanceSearchColumns();

		Organization GetOrganization(System.Int32 OrganizationId);
		DataTransfer<List<GetOutput>> GetAll();
		Organization UpdateOrganization(Organization entity);
		bool DeleteOrganization(System.Int32 OrganizationId);
		List<Organization> GetAllOrganization();
		Organization InsertOrganization(Organization entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Organization> GetOrganizationByTerm(string term);

    }
	
	
}
