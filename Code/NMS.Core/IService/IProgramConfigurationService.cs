﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
//using NMS.Core.DataTransfer.ProgramConfiguration;

namespace NMS.Core.IService
{
		
	public interface IProgramConfigurationService
	{
        Dictionary<string, string> GetProgramConfigurationBasicSearchColumns();
        
        List<SearchColumn> GetProgramConfigurationAdvanceSearchColumns();

		List<ProgramConfiguration> GetProgramConfigurationByProgramId(System.Int32 ProgramId);
		List<ProgramConfiguration> GetProgramConfigurationBySegmentTypeId(System.Int32 SegmentTypeId);
		ProgramConfiguration GetProgramConfiguration(System.Int32 ProgramConfigurationId);
		//DataTransfer<List<GetOutput>> GetAll();
		ProgramConfiguration UpdateProgramConfiguration(ProgramConfiguration entity);
		bool DeleteProgramConfiguration(System.Int32 ProgramConfigurationId);
		List<ProgramConfiguration> GetAllProgramConfiguration();
		ProgramConfiguration InsertProgramConfiguration(ProgramConfiguration entity);

        //DataTransfer<GetOutput> Get(string id);
        //DataTransfer<PostOutput> Insert(PostInput Input);
        //DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
