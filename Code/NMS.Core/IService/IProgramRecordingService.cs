﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramRecording;

namespace NMS.Core.IService
{
		
	public interface IProgramRecordingService
	{
        Dictionary<string, string> GetProgramRecordingBasicSearchColumns();
        
        List<SearchColumn> GetProgramRecordingAdvanceSearchColumns();

		List<ProgramRecording> GetProgramRecordingByProgramId(System.Int32? ProgramId);
		ProgramRecording GetProgramRecording(System.Int32 ProgramRecordingId);
		DataTransfer<List<GetOutput>> GetAll();
		ProgramRecording UpdateProgramRecording(ProgramRecording entity);
		bool DeleteProgramRecording(System.Int32 ProgramRecordingId);
		List<ProgramRecording> GetAllProgramRecording();
		ProgramRecording InsertProgramRecording(ProgramRecording entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
