﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramSchedule;

namespace NMS.Core.IService
{
		
	public interface IProgramScheduleService
	{
        Dictionary<string, string> GetProgramScheduleBasicSearchColumns();
        
        List<SearchColumn> GetProgramScheduleAdvanceSearchColumns();

		List<ProgramSchedule> GetProgramScheduleByProgramId(System.Int32 ProgramId);
		List<ProgramSchedule> GetProgramScheduleByRecurringTypeId(System.Int32 RecurringTypeId);
		ProgramSchedule GetProgramSchedule(System.Int32 ProgramScheduleId);
		DataTransfer<List<GetOutput>> GetAll();
		ProgramSchedule UpdateProgramSchedule(ProgramSchedule entity);
		bool DeleteProgramSchedule(System.Int32 ProgramScheduleId);
		List<ProgramSchedule> GetAllProgramSchedule();
		ProgramSchedule InsertProgramSchedule(ProgramSchedule entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
