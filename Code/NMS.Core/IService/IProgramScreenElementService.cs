﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramScreenElement;

namespace NMS.Core.IService
{
		
	public interface IProgramScreenElementService
	{
        Dictionary<string, string> GetProgramScreenElementBasicSearchColumns();
        
        List<SearchColumn> GetProgramScreenElementAdvanceSearchColumns();

		ProgramScreenElement GetProgramScreenElement(System.Int32 ProgramScreenElementId);
		DataTransfer<List<GetOutput>> GetAll();
		ProgramScreenElement UpdateProgramScreenElement(ProgramScreenElement entity);
		bool DeleteProgramScreenElement(System.Int32 ProgramScreenElementId);
		List<ProgramScreenElement> GetAllProgramScreenElement();
		ProgramScreenElement InsertProgramScreenElement(ProgramScreenElement entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
