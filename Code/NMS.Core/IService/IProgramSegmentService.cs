﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramSegment;

namespace NMS.Core.IService
{
		
	public interface IProgramSegmentService
	{
        Dictionary<string, string> GetProgramSegmentBasicSearchColumns();
        
        List<SearchColumn> GetProgramSegmentAdvanceSearchColumns();

		List<ProgramSegment> GetProgramSegmentByProgramId(System.Int32 ProgramId);
		List<ProgramSegment> GetProgramSegmentBySegmentTypeId(System.Int32 SegmentTypeId);
		ProgramSegment GetProgramSegment(System.Int32 ProgramSegmentId);
		DataTransfer<List<GetOutput>> GetAll();
		ProgramSegment UpdateProgramSegment(ProgramSegment entity);
		bool DeleteProgramSegment(System.Int32 ProgramSegmentId);
		List<ProgramSegment> GetAllProgramSegment();
		ProgramSegment InsertProgramSegment(ProgramSegment entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool DeleteProgramSegmentByProgramId(System.Int32 ProgramId);
	}
	
	
}
