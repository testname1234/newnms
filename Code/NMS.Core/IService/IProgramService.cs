﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Program;
using NMS.Core.Models;

namespace NMS.Core.IService
{
		
	public interface IProgramService
	{
        Dictionary<string, string> GetProgramBasicSearchColumns();
        
        List<SearchColumn> GetProgramAdvanceSearchColumns();

		List<Program> GetProgramByChannelId(System.Int32? ChannelId);
		Program GetProgram(System.Int32 ProgramId);
		DataTransfer<List<GetOutput>> GetAll();
        DataTransfer<List<GetOutput>> GetAllProgramsForReporter(bool? withoutFolder);
        Program UpdateProgram(Program entity);
		bool DeleteProgram(System.Int32 ProgramId);
		List<Program> GetAllProgram();
		Program InsertProgram(Program entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Episode> GetProgramEpisode(int programId, DateTime date);

        Episode GetProgramEpisodeByRundownId(int runDownId);

        EpisodePreviewModel GetRunDownPreviewByEpisodeId(int EpisodeId);

        string GenerateScreenTemplateImage(NMS.Core.DataTransfer.ScreenTemplate.PostInput input);

        List<Episode> GetProgramEpisodeByDate(int userId, Enums.TeamRoles teamRoles, DateTime dateTime, DateTime lastUpdateDate);
        List<Program> GetProgramByUserTeam(int userid);
        List<Program> GetProgramByChannelIdAndUserId(int ChannelId, int UserId);
        void FillEpisode(Episode episode, DateTime? lastUpdateDate = null, bool isAssignedToNLE = false, bool isAssignedToStoryWriter = false);

        List<SlotScreenTemplate> CopySlotInfo(int fromSlotID, Slot toSlot);
        int GetProgramIdByBroadcastedTime(int second, int interval);
        List<Celebrity> GetProgramAnchor(int programId);
    }
	
	
}
