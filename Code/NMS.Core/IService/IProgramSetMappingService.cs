﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramSetMapping;

namespace NMS.Core.IService
{
		
	public interface IProgramSetMappingService
	{
        Dictionary<string, string> GetProgramSetMappingBasicSearchColumns();
        
        List<SearchColumn> GetProgramSetMappingAdvanceSearchColumns();

		ProgramSetMapping GetProgramSetMapping(System.Int32 ProgramSetMappingId);
		DataTransfer<List<GetOutput>> GetAll();
		ProgramSetMapping UpdateProgramSetMapping(ProgramSetMapping entity);
		bool DeleteProgramSetMapping(System.Int32 ProgramSetMappingId);
		List<ProgramSetMapping> GetAllProgramSetMapping();
		ProgramSetMapping InsertProgramSetMapping(ProgramSetMapping entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        void SyncProgramSetMapping();
    }
	
	
}
