﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramTypeMapping;

namespace NMS.Core.IService
{
		
	public interface IProgramTypeMappingService
	{
        Dictionary<string, string> GetProgramTypeMappingBasicSearchColumns();
        
        List<SearchColumn> GetProgramTypeMappingAdvanceSearchColumns();

		List<ProgramTypeMapping> GetProgramTypeMappingByProgramId(System.Int32? ProgramId);
		List<ProgramTypeMapping> GetProgramTypeMappingByProgramTypeId(System.Int32? ProgramTypeId);
		ProgramTypeMapping GetProgramTypeMapping(System.Int32 ProgramTypeMappingId);
		DataTransfer<List<GetOutput>> GetAll();
		ProgramTypeMapping UpdateProgramTypeMapping(ProgramTypeMapping entity);
		bool DeleteProgramTypeMapping(System.Int32 ProgramTypeMappingId);
		List<ProgramTypeMapping> GetAllProgramTypeMapping();
		ProgramTypeMapping InsertProgramTypeMapping(ProgramTypeMapping entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
