﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramType;

namespace NMS.Core.IService
{
		
	public interface IProgramTypeService
	{
        Dictionary<string, string> GetProgramTypeBasicSearchColumns();
        
        List<SearchColumn> GetProgramTypeAdvanceSearchColumns();

		ProgramType GetProgramType(System.Int32 ProgramTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		ProgramType UpdateProgramType(ProgramType entity);
		bool DeleteProgramType(System.Int32 ProgramTypeId);
		List<ProgramType> GetAllProgramType();
		ProgramType InsertProgramType(ProgramType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
