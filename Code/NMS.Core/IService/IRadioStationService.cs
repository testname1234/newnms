﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RadioStation;

namespace NMS.Core.IService
{
		
	public interface IRadioStationService
	{
        Dictionary<string, string> GetRadioStationBasicSearchColumns();
        
        List<SearchColumn> GetRadioStationAdvanceSearchColumns();

		RadioStation GetRadioStation(System.Int32 RadioStationId);
		DataTransfer<List<GetOutput>> GetAll();
		RadioStation UpdateRadioStation(RadioStation entity);
		bool DeleteRadioStation(System.Int32 RadioStationId);
		List<RadioStation> GetAllRadioStation();
		RadioStation InsertRadioStation(RadioStation entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
