﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RadioStream;

namespace NMS.Core.IService
{
		
	public interface IRadioStreamService
	{
        Dictionary<string, string> GetRadioStreamBasicSearchColumns();
        
        List<SearchColumn> GetRadioStreamAdvanceSearchColumns();

		List<RadioStream> GetRadioStreamByRadioStationId(System.Int32 RadioStationId);
		RadioStream GetRadioStream(System.Int32 RadioStreamId);
		DataTransfer<List<GetOutput>> GetAll();
		RadioStream UpdateRadioStream(RadioStream entity);
		bool DeleteRadioStream(System.Int32 RadioStreamId);
		List<RadioStream> GetAllRadioStream();
		RadioStream InsertRadioStream(RadioStream entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<RadioStream> GetRadioStreamBetweenDates(System.DateTime fromDate, System.DateTime toDate);

        void MarkIsProcessed(int id);
	}
	
	
}
