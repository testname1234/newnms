﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RecordingClips;

namespace NMS.Core.IService
{
		
	public interface IRecordingClipsService
	{
        Dictionary<string, string> GetRecordingClipsBasicSearchColumns();
        
        List<SearchColumn> GetRecordingClipsAdvanceSearchColumns();

		List<RecordingClips> GetRecordingClipsByCelebrityId(System.Int32? CelebrityId);
		List<RecordingClips> GetRecordingClipsByProgramRecordingId(System.Int32? ProgramRecordingId);

        List<RecordingClips> GetRecordingClipsByProgramRecordingIdList(List<System.Int32> ProgramRecordingIds);

        RecordingClips GetRecordingClips(System.Int32 RecordingClipId);
		DataTransfer<List<GetOutput>> GetAll();
		RecordingClips UpdateRecordingClips(RecordingClips entity);
        RecordingClips UpdateRecordingClipsRejectClip(RecordingClips entity);

        bool DeleteRecordingClips(System.Int32 RecordingClipId);
		List<RecordingClips> GetAllRecordingClips();
		RecordingClips InsertRecordingClips(RecordingClips entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
