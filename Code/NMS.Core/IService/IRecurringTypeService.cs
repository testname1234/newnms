﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RecurringType;

namespace NMS.Core.IService
{
		
	public interface IRecurringTypeService
	{
        Dictionary<string, string> GetRecurringTypeBasicSearchColumns();
        
        List<SearchColumn> GetRecurringTypeAdvanceSearchColumns();

		RecurringType GetRecurringType(System.Int32 RecurringTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		RecurringType UpdateRecurringType(RecurringType entity);
		bool DeleteRecurringType(System.Int32 RecurringTypeId);
		List<RecurringType> GetAllRecurringType();
		RecurringType InsertRecurringType(RecurringType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
