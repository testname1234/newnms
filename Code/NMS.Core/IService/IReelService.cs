﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Reel;

namespace NMS.Core.IService
{
		
	public interface IReelService
	{
        Dictionary<string, string> GetReelBasicSearchColumns();
        
        List<SearchColumn> GetReelAdvanceSearchColumns();

		List<Reel> GetReelByStatusId(System.Int32 StatusId);
		Reel GetReel(System.Int32 ReelId);
		DataTransfer<List<GetOutput>> GetAll();
		Reel UpdateReel(Reel entity);
		bool DeleteReel(System.Int32 ReelId);
		List<Reel> GetAllReel();
		Reel InsertReel(Reel entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
