﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ReelStatus;

namespace NMS.Core.IService
{
		
	public interface IReelStatusService
	{
        Dictionary<string, string> GetReelStatusBasicSearchColumns();
        
        List<SearchColumn> GetReelStatusAdvanceSearchColumns();

		ReelStatus GetReelStatus(System.Int32 ReelStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		ReelStatus UpdateReelStatus(ReelStatus entity);
		bool DeleteReelStatus(System.Int32 ReelStatusId);
		List<ReelStatus> GetAllReelStatus();
		ReelStatus InsertReelStatus(ReelStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
