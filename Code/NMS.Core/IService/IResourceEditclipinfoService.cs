﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ResourceEditclipinfo;

namespace NMS.Core.IService
{
		
	public interface IResourceEditclipinfoService
	{
        Dictionary<string, string> GetResourceEditclipinfoBasicSearchColumns();
        
        List<SearchColumn> GetResourceEditclipinfoAdvanceSearchColumns();

		List<ResourceEditclipinfo> GetResourceEditclipinfoByNewsResourceEditId(System.Int32? NewsResourceEditId);
		ResourceEditclipinfo GetResourceEditclipinfo(System.Int32 ResourceEditclipinfoId);
		DataTransfer<List<GetOutput>> GetAll();
		ResourceEditclipinfo UpdateResourceEditclipinfo(ResourceEditclipinfo entity);
		bool DeleteResourceEditclipinfo(System.Int32 ResourceEditclipinfoId);
		List<ResourceEditclipinfo> GetAllResourceEditclipinfo();
		ResourceEditclipinfo InsertResourceEditclipinfo(ResourceEditclipinfo entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteByNewsResourceEditId(System.Int32 NewsResourceEditId);
	}
	
	
}
