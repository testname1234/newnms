﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Resource;
using NMS.Core.Models;
using System.Drawing;
using System.Data.SqlClient;
using System.Net;

namespace NMS.Core.IService
{
		
	public interface IResourceService
	{
        Dictionary<string, string> GetResourceBasicSearchColumns();
        
        List<SearchColumn> GetResourceAdvanceSearchColumns();

		List<Resource> GetResourceByResourceTypeId(System.Int32 ResourceTypeId);
		Resource GetResource(System.Int32 ResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		Resource UpdateResource(Resource entity);
		bool DeleteResource(System.Int32 ResourceId);
		List<Resource> GetAllResource();
		Resource InsertResource(Resource entity);
        bool InsertResource(MResource entity);
        bool DeleteByResourceGuid(string resourceGuid);
        MResource GetByGuid(string Guid);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<PostOutput> GetResourceGuidsFromMediaServer(List<PostInput> input, string rootPath, int? bucketId, string apiKey,string sourceTypeId,string sourceTypeName,string sourceId, string FolderSource = "Reporter");
        List<PostOutput> GetResourceGuidsFromMediaServer(List<PostInput> input, string rootPath);

        MResource GetMResource(string id);
        bool DeleteFromMediaServer(Guid id);
        string GenerateSnapshotOnMediaServer(Guid guid);
        string GenerateCustomSnapshotOnMediaServer(CustomSnapshotInput input);

        void UpdateResourceStatus(Guid guid, int resourceStatusId);

        Guid UploadImageOnMS(Image img);
        Guid UploadImageOnMS(string imageUrl);
        Guid UploadFile(byte[] data, string file, int resourceType, string mimeType, bool isHd = true, int bucketId = 0, string apikey = null, string caption = null, double? duration = null);

        string AddResourceMeta(MNews mongoNews);
        void UpdateResourceDuration(MResource entity);

        DateTime GetLastResourceDate();

        Resource InsertResourceIfNotExists(Resource resource);
        Resource GetResourceByGuid(string ResourceGuid);
        List<MResource> GetResourceByMeta(string resourceMeta);
        MResource GetMResourceByGuid(string Guid);
        List<Resource> GetByUpdatedDate(DateTime UpdatedDate);
        void InsertIfNotExistsNoReturn(Resource resource);

        Resource MediaPostResource(MS.Core.DataTransfer.Resource.PostInput resource, string FolderSource);
        Resource MediaPostResourceForGoogleData(MS.Core.DataTransfer.Resource.PostInput resource, string FolderSource);
       // HttpStatusCode MediaPostMedia(byte[] data, string Guid, string FileName, string Param, string ContentType);

        PostOutput GetResourceFromMediaServerByGuid(string guid);
        void updateResourceMeta(ListResources input);
	}
	
	
}
