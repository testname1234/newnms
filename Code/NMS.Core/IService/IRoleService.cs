﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Role;

namespace NMS.Core.IService
{
		
	public interface IRoleService
	{
        Dictionary<string, string> GetRoleBasicSearchColumns();
        
        List<SearchColumn> GetRoleAdvanceSearchColumns();

		Role GetRole(System.Int32 RoleId);
		DataTransfer<List<GetOutput>> GetAll();
		Role UpdateRole(Role entity);
		bool DeleteRole(System.Int32 RoleId);
		List<Role> GetAllRole();
		Role InsertRole(Role entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
