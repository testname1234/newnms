﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RunOrderLog;

namespace NMS.Core.IService
{
		
	public interface IRunOrderLogService
	{
        Dictionary<string, string> GetRunOrderLogBasicSearchColumns();
        
        List<SearchColumn> GetRunOrderLogAdvanceSearchColumns();

		RunOrderLog GetRunOrderLog(System.Int32 RunOrderLogId);
		DataTransfer<List<GetOutput>> GetAll();
		RunOrderLog UpdateRunOrderLog(RunOrderLog entity);
		bool DeleteRunOrderLog(System.Int32 RunOrderLogId);
		List<RunOrderLog> GetAllRunOrderLog();
		RunOrderLog InsertRunOrderLog(RunOrderLog entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
