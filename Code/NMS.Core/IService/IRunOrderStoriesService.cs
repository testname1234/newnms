﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RunOrderStories;

namespace NMS.Core.IService
{
		
	public interface IRunOrderStoriesService
	{
        Dictionary<string, string> GetRunOrderStoriesBasicSearchColumns();
        
        List<SearchColumn> GetRunOrderStoriesAdvanceSearchColumns();

		List<RunOrderStories> GetRunOrderStoriesByRunOrderLogId(System.Int32 RunOrderLogId);
		RunOrderStories GetRunOrderStories(System.Int32 RunOrderStoryId);
		DataTransfer<List<GetOutput>> GetAll();
		RunOrderStories UpdateRunOrderStories(RunOrderStories entity);
		bool DeleteRunOrderStories(System.Int32 RunOrderStoryId);
		List<RunOrderStories> GetAllRunOrderStories();
		RunOrderStories InsertRunOrderStories(RunOrderStories entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
