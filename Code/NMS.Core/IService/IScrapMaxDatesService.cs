﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScrapMaxDates;
using System.Data;

namespace NMS.Core.IService
{
		
	public interface IScrapMaxDatesService
	{
        Dictionary<string, string> GetScrapMaxDatesBasicSearchColumns();
        
        List<SearchColumn> GetScrapMaxDatesAdvanceSearchColumns();

		ScrapMaxDates GetScrapMaxDates(System.Int32 ScrapMaxDatesId);
		DataTransfer<List<GetOutput>> GetAll();
		ScrapMaxDates UpdateScrapMaxDates(ScrapMaxDates entity);        
		bool DeleteScrapMaxDates(System.Int32 ScrapMaxDatesId);
		List<ScrapMaxDates> GetAllScrapMaxDates();
		ScrapMaxDates InsertScrapMaxDates(ScrapMaxDates entity);
        ScrapMaxDates GetScrapDateCreateIfNotExist(ScrapMaxDates ScrapMaxDates);
        ScrapMaxDates GetScrapMaxDatesBySource(System.String Source);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        DataTable GetResult();
	}
	
	
}
