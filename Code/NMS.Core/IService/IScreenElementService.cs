﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScreenElement;

namespace NMS.Core.IService
{
		
	public interface IScreenElementService
	{
        Dictionary<string, string> GetScreenElementBasicSearchColumns();
        
        List<SearchColumn> GetScreenElementAdvanceSearchColumns();

		ScreenElement GetScreenElement(System.Int32 ScreenElementId);
		DataTransfer<List<GetOutput>> GetAll();
		ScreenElement UpdateScreenElement(ScreenElement entity);
		bool DeleteScreenElement(System.Int32 ScreenElementId);
		List<ScreenElement> GetAllScreenElement();
		ScreenElement InsertScreenElement(ScreenElement entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
