﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScreenTemplate;

namespace NMS.Core.IService
{
		
	public interface IScreenTemplateService
	{
        Dictionary<string, string> GetScreenTemplateBasicSearchColumns();
        
        List<SearchColumn> GetScreenTemplateAdvanceSearchColumns();

		List<ScreenTemplate> GetScreenTemplateByProgramId(System.Int32 ProgramId);
		ScreenTemplate GetScreenTemplate(System.Int32 ScreenTemplateId);
		DataTransfer<List<GetOutput>> GetAll();
		ScreenTemplate UpdateScreenTemplate(ScreenTemplate entity);
		bool DeleteScreenTemplate(System.Int32 ScreenTemplateId);
		List<ScreenTemplate> GetAllScreenTemplate();
		ScreenTemplate InsertScreenTemplate(ScreenTemplate entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        void SyncScreenTemplate();
	}
	
	
}
