﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScreenTemplateStatus;

namespace NMS.Core.IService
{
		
	public interface IScreenTemplateStatusService
	{
        Dictionary<string, string> GetScreenTemplateStatusBasicSearchColumns();
        
        List<SearchColumn> GetScreenTemplateStatusAdvanceSearchColumns();

		ScreenTemplateStatus GetScreenTemplateStatus(System.Int32 ScreenTemplateStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		ScreenTemplateStatus UpdateScreenTemplateStatus(ScreenTemplateStatus entity);
		bool DeleteScreenTemplateStatus(System.Int32 ScreenTemplateStatusId);
		List<ScreenTemplateStatus> GetAllScreenTemplateStatus();
		ScreenTemplateStatus InsertScreenTemplateStatus(ScreenTemplateStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
