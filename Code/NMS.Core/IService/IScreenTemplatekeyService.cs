﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScreenTemplatekey;

namespace NMS.Core.IService
{
		
	public interface IScreenTemplatekeyService
	{
        Dictionary<string, string> GetScreenTemplatekeyBasicSearchColumns();
        
        List<SearchColumn> GetScreenTemplatekeyAdvanceSearchColumns();

		List<ScreenTemplatekey> GetScreenTemplatekeyByScreenTemplateId(System.Int32? ScreenTemplateId);
		ScreenTemplatekey GetScreenTemplatekey(System.Int32 ScreenTemplatekeyId);
		DataTransfer<List<GetOutput>> GetAll();
		ScreenTemplatekey UpdateScreenTemplatekey(ScreenTemplatekey entity);
		bool DeleteScreenTemplatekey(System.Int32 ScreenTemplatekeyId);
		List<ScreenTemplatekey> GetAllScreenTemplatekey();
		ScreenTemplatekey InsertScreenTemplatekey(ScreenTemplatekey entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<ScreenTemplatekey> GetByScreenTemplateList(List<ScreenTemplate> ScreenTemplates);
	}
	
	
}
