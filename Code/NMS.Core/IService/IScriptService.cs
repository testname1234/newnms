﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Script;

namespace NMS.Core.IService
{
		
	public interface IScriptService
	{
        Dictionary<string, string> GetScriptBasicSearchColumns();
        
        List<SearchColumn> GetScriptAdvanceSearchColumns();

		Script GetScript(System.Int32 ScriptId);
		DataTransfer<List<GetOutput>> GetAll();
		Script UpdateScript(Script entity);
		bool DeleteScript(System.Int32 ScriptId);
		List<Script> GetAllScript();
		Script InsertScript(Script entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool MarkAsExecuted(int id);

        List<GetOutput> GetAllUnExecutedScripts(string token);
    }
	
	
}
