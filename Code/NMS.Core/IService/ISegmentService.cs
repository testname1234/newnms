﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Segment;

namespace NMS.Core.IService
{
		
	public interface ISegmentService
	{
        Dictionary<string, string> GetSegmentBasicSearchColumns();
        
        List<SearchColumn> GetSegmentAdvanceSearchColumns();

		List<Segment> GetSegmentBySegmentTypeId(System.Int32 SegmentTypeId);
		List<Segment> GetSegmentByEpisodeId(System.Int32 EpisodeId);
		Segment GetSegment(System.Int32 SegmentId);
		DataTransfer<List<GetOutput>> GetAll();
		Segment UpdateSegment(Segment entity);
		bool DeleteSegment(System.Int32 SegmentId);
		List<Segment> GetAllSegment();
		Segment InsertSegment(Segment entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
