﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SegmentStory;

namespace NMS.Core.IService
{
		
	public interface ISegmentStoryService
	{
        Dictionary<string, string> GetSegmentStoryBasicSearchColumns();
        
        List<SearchColumn> GetSegmentStoryAdvanceSearchColumns();

		List<SegmentStory> GetSegmentStoryBySegmentId(System.Int32 SegmentId);
		List<SegmentStory> GetSegmentStoryByStoryId(System.Int32 StoryId);
		SegmentStory GetSegmentStory(System.Int32 SegmentStoryId);
		DataTransfer<List<GetOutput>> GetAll();
		SegmentStory UpdateSegmentStory(SegmentStory entity);
		bool DeleteSegmentStory(System.Int32 SegmentStoryId);
		List<SegmentStory> GetAllSegmentStory();
		SegmentStory InsertSegmentStory(SegmentStory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
