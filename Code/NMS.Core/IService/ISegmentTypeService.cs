﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SegmentType;

namespace NMS.Core.IService
{
		
	public interface ISegmentTypeService
	{
        Dictionary<string, string> GetSegmentTypeBasicSearchColumns();
        
        List<SearchColumn> GetSegmentTypeAdvanceSearchColumns();

		SegmentType GetSegmentType(System.Int32 SegmentTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		SegmentType UpdateSegmentType(SegmentType entity);
		bool DeleteSegmentType(System.Int32 SegmentTypeId);
		List<SegmentType> GetAllSegmentType();
		SegmentType InsertSegmentType(SegmentType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
