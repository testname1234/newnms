﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotHeadline;

namespace NMS.Core.IService
{
		
	public interface ISlotHeadlineService
	{
        Dictionary<string, string> GetSlotHeadlineBasicSearchColumns();
        
        List<SearchColumn> GetSlotHeadlineAdvanceSearchColumns();

		List<SlotHeadline> GetSlotHeadlineBySlotId(System.Int32 SlotId);
		SlotHeadline GetSlotHeadline(System.Int32 SlotHeadlineId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotHeadline UpdateSlotHeadline(SlotHeadline entity);
		bool DeleteSlotHeadline(System.Int32 SlotHeadlineId);
		List<SlotHeadline> GetAllSlotHeadline();
		SlotHeadline InsertSlotHeadline(SlotHeadline entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool DeleteSlotHeadlineBySlotId(System.Int32 slotid);
    }
	
	
}
