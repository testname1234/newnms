﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplateCamera;

namespace NMS.Core.IService
{
		
	public interface ISlotScreenTemplateCameraService
	{
        Dictionary<string, string> GetSlotScreenTemplateCameraBasicSearchColumns();
        
        List<SearchColumn> GetSlotScreenTemplateCameraAdvanceSearchColumns();

		List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
		List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraByCameraTypeId(System.Int32 CameraTypeId);
		SlotScreenTemplateCamera GetSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotScreenTemplateCamera UpdateSlotScreenTemplateCamera(SlotScreenTemplateCamera entity);
		bool DeleteSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId);
		List<SlotScreenTemplateCamera> GetAllSlotScreenTemplateCamera();
		SlotScreenTemplateCamera InsertSlotScreenTemplateCamera(SlotScreenTemplateCamera entity);
        List<SlotScreenTemplateCamera> InsertSlotScreenTemplateCameraByEpisodeId(int EpisodeId);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
	}
	
	
}
