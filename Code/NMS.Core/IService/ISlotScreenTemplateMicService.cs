﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplateMic;

namespace NMS.Core.IService
{
		
	public interface ISlotScreenTemplateMicService
	{
        Dictionary<string, string> GetSlotScreenTemplateMicBasicSearchColumns();
        
        List<SearchColumn> GetSlotScreenTemplateMicAdvanceSearchColumns();

		List<SlotScreenTemplateMic> GetSlotScreenTemplateMicBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
		SlotScreenTemplateMic GetSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotScreenTemplateMic UpdateSlotScreenTemplateMic(SlotScreenTemplateMic entity);
		bool DeleteSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId);
		List<SlotScreenTemplateMic> GetAllSlotScreenTemplateMic();
		SlotScreenTemplateMic InsertSlotScreenTemplateMic(SlotScreenTemplateMic entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<SlotScreenTemplateMic> InsertSlotScreenTemplateMicByEpisodeId(int episodeId);
        bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
	}
	
	
}
