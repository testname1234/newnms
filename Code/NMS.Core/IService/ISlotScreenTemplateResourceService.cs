﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplateResource;

namespace NMS.Core.IService
{
		
	public interface ISlotScreenTemplateResourceService
	{
        Dictionary<string, string> GetSlotScreenTemplateResourceBasicSearchColumns();
        
        List<SearchColumn> GetSlotScreenTemplateResourceAdvanceSearchColumns();

		List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
		SlotScreenTemplateResource GetSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotScreenTemplateResource UpdateSlotScreenTemplateResource(SlotScreenTemplateResource entity);
		bool DeleteSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId);
		List<SlotScreenTemplateResource> GetAllSlotScreenTemplateResource();
		SlotScreenTemplateResource InsertSlotScreenTemplateResource(SlotScreenTemplateResource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        void DeleteAllBySlotScreenTemplateId(int slotScreenTemplateId);
        List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceByEpisode(System.Int32 EpisodeId);
        SlotScreenTemplateResource GetSlotScreenTemplateResourceByResourceGuid(string guid);
	}
	
	
}
