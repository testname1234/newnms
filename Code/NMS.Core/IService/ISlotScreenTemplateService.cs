﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplate;
using NMS.Core.Enums;

namespace NMS.Core.IService
{
		
	public interface ISlotScreenTemplateService
	{
        Dictionary<string, string> GetSlotScreenTemplateBasicSearchColumns();
        
        List<SearchColumn> GetSlotScreenTemplateAdvanceSearchColumns();

		List<SlotScreenTemplate> GetSlotScreenTemplateBySlotId(System.Int32 SlotId);
		SlotScreenTemplate GetSlotScreenTemplate(System.Int32 SlotScreenTemplateId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotScreenTemplate UpdateSlotScreenTemplate(SlotScreenTemplate entity);
		bool DeleteSlotScreenTemplate(System.Int32 SlotScreenTemplateId);
		List<SlotScreenTemplate> GetAllSlotScreenTemplate();
		SlotScreenTemplate InsertSlotScreenTemplate(SlotScreenTemplate entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<SlotScreenTemplate> GetSlotScreenTemplateBySlotAndScreenTemplateId(System.Int32 ScreenTemplateId, System.Int32 SlotId);
        SlotScreenTemplate UpdateSlotScreenTemplateSequence(SlotScreenTemplate entity);
        SlotScreenTemplate UpdateSlotScreenTemplateSequenceAndScript(SlotScreenTemplate entity);
        void UpdateSlotScreenScript(int slotScreenTemplateId, string script);
        bool UpdateSlotScreenTemplateStatus(int slotScreenTemplateId, int? statusId, int? nleStatusId, int? storyWriterStatusId, int userId, int senderRoleId, int programId);

        bool DeleteSlotScreenTemplatebySlotId(System.Int32 SloId);

        void MarkNotifications(SlotScreenTemplate template, int userId, int senderRole, int programId, NotificationTypes notificationType);

        SlotScreenTemplate UpdateSlotScreenTemplateThumb(SlotScreenTemplate entity);
        List<SlotScreenTemplate> GetByEpisodeId(System.Int32 episodeId);
        int GetProgramIdBySlotScreenTemplateId(System.Int32 slotScreenTemplateId);
        List<SlotScreenTemplate> GetSlotScreenTemplateByParentId(int parentId);
	}
	
	
}
