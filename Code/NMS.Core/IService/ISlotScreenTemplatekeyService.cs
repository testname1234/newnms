﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplatekey;

namespace NMS.Core.IService
{
		
	public interface ISlotScreenTemplatekeyService
	{
        Dictionary<string, string> GetSlotScreenTemplatekeyBasicSearchColumns();
        
        List<SearchColumn> GetSlotScreenTemplatekeyAdvanceSearchColumns();

		List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyByScreenTemplatekeyId(System.Int32? ScreenTemplatekeyId);
		SlotScreenTemplatekey GetSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotScreenTemplatekey UpdateSlotScreenTemplatekey(SlotScreenTemplatekey entity);
		bool DeleteSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId);
		List<SlotScreenTemplatekey> GetAllSlotScreenTemplatekey();
		SlotScreenTemplatekey InsertSlotScreenTemplatekey(SlotScreenTemplatekey entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
        List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyBySlotScreenTemplateId(System.Int32? SlotScreenTemplateId);
	}
	
	
}
