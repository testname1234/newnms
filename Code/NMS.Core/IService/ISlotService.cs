﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Slot;

namespace NMS.Core.IService
{
		
	public interface ISlotService
	{
        Dictionary<string, string> GetSlotBasicSearchColumns();
        
        List<SearchColumn> GetSlotAdvanceSearchColumns();

		List<Slot> GetSlotBySlotTypeId(System.Int32 SlotTypeId);
		List<Slot> GetSlotBySegmentId(System.Int32 SegmentId);
		List<Slot> GetSlotByCategoryId(System.Int32 CategoryId);
		Slot GetSlot(System.Int32 SlotId);
		DataTransfer<List<GetOutput>> GetAll();
		Slot UpdateSlot(Slot entity);
		bool DeleteSlot(System.Int32 SlotId);
		List<Slot> GetAllSlot();
		Slot InsertSlot(Slot entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        Slot InsertSlotBySegment(Slot entity);
        List<Slot> GetSlotByEpisode(System.Int32 EpisodeId);
        Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, System.String NewsGuid);

        Slot GetSlotByEpisodeAndNewsFileId(System.Int32 EpisodeId, System.Int32 NewsGuid);

        Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, int TickerId);
        Slot UpdateSlotSequence(Slot entity);
        bool InsertDefaultTemplates(Slot slot, int programId);

        int GetEpisodeId(int slotId);
        List<Slot> GetSlotsByEpisodeId(int EpisodeId);
    }
	
	
}
