﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotTemplateScreenElementResource;

namespace NMS.Core.IService
{
		
	public interface ISlotTemplateScreenElementResourceService
	{
        Dictionary<string, string> GetSlotTemplateScreenElementResourceBasicSearchColumns();
        
        List<SearchColumn> GetSlotTemplateScreenElementResourceAdvanceSearchColumns();

		List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(System.Int32? SlotTemplateScreenElementId);
		SlotTemplateScreenElementResource GetSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotTemplateScreenElementResource UpdateSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity);
		bool DeleteSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId);
		List<SlotTemplateScreenElementResource> GetAllSlotTemplateScreenElementResource();
		SlotTemplateScreenElementResource InsertSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
        List<SlotTemplateScreenElementResource> GetBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
        bool DeleteBySlotTemplateScreenElementId(System.Int32 SlotTemplateScreenElementId);
	}
	
	
}
