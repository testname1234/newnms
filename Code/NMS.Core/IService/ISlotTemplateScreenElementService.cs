﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotTemplateScreenElement;

namespace NMS.Core.IService
{
		
	public interface ISlotTemplateScreenElementService
	{
        Dictionary<string, string> GetSlotTemplateScreenElementBasicSearchColumns();
        
        List<SearchColumn> GetSlotTemplateScreenElementAdvanceSearchColumns();

		List<SlotTemplateScreenElement> GetSlotTemplateScreenElementBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);
		SlotTemplateScreenElement GetSlotTemplateScreenElement(System.Int32 SlotTemplateScreenElementId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotTemplateScreenElement UpdateSlotTemplateScreenElement(SlotTemplateScreenElement entity);
		bool DeleteSlotTemplateScreenElement(System.Int32 SlotTemplateScreenElementId);
		List<SlotTemplateScreenElement> GetAllSlotTemplateScreenElement();
		SlotTemplateScreenElement InsertSlotTemplateScreenElement(SlotTemplateScreenElement entity);
        SlotTemplateScreenElement UpdateSlotScreenTemplateElementCelebrity(int slotTemplateScreenElementId, int celebrityId);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool DeleteSlotTemplateScreenElementBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId);

        SlotTemplateScreenElement UpdateSlotTemplateScreenElementResourceGuid(int slotTemplateScreenElementId, Guid? resourceGuid);
        bool DeleteSlotTemplateScreenElementBySlotId(System.Int32 SlotScreenTemplateId);
        List<SlotTemplateScreenElement> GetBySlotScreenTemplateIdWithFilter(System.Int32 SlotScreenTemplateId);
        List<SlotTemplateScreenElement> GetSlotTemplateScreenElementByEpisodeId(System.Int32 EpisodeId);
	}
	
	
}
