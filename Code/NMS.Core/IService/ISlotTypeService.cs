﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotType;

namespace NMS.Core.IService
{
		
	public interface ISlotTypeService
	{
        Dictionary<string, string> GetSlotTypeBasicSearchColumns();
        
        List<SearchColumn> GetSlotTypeAdvanceSearchColumns();

		SlotType GetSlotType(System.Int32 SlotTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		SlotType UpdateSlotType(SlotType entity);
		bool DeleteSlotType(System.Int32 SlotTypeId);
		List<SlotType> GetAllSlotType();
		SlotType InsertSlotType(SlotType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
