﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Slug;

namespace NMS.Core.IService
{
		
	public interface ISlugService
	{
        Dictionary<string, string> GetSlugBasicSearchColumns();
        
        List<SearchColumn> GetSlugAdvanceSearchColumns();

		Slug GetSlug(System.Int32 SlugId);
		DataTransfer<List<GetOutput>> GetAll();
		Slug UpdateSlug(Slug entity);
		bool DeleteSlug(System.Int32 SlugId);
		List<Slug> GetAllSlug();
		Slug InsertSlug(Slug entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        Slug GetBySlug(System.String Slug);
        Slug InsertIfNotExists(Slug entity);
	}
	
	
}
