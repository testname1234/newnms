﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SocialMediaAccount;

namespace NMS.Core.IService
{
		
	public interface ISocialMediaAccountService
	{
        Dictionary<string, string> GetSocialMediaAccountBasicSearchColumns();
        
        List<SearchColumn> GetSocialMediaAccountAdvanceSearchColumns();
        void UpdateTwitterAccountIsFollowed(int SocialMediaAccountId, Boolean IsFollowed);
		List<SocialMediaAccount> GetSocialMediaAccountByCelebrityId(System.Int32? CelebrityId);
		SocialMediaAccount GetSocialMediaAccount(System.Int32 SocialMediaId);
		DataTransfer<List<GetOutput>> GetAll();
		SocialMediaAccount UpdateSocialMediaAccount(SocialMediaAccount entity);
		bool DeleteSocialMediaAccount(System.Int32 SocialMediaId);
		List<SocialMediaAccount> GetAllSocialMediaAccount();
		SocialMediaAccount InsertSocialMediaAccount(SocialMediaAccount entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<SocialMediaAccount> GetBySocialMediaType(System.Int32 SocialMediaType);
        List<SocialMediaAccount> GetByIsFollowed(System.Boolean IsFollowed);
	}
	
	
}
