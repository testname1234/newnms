﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SocialMedia;

namespace NMS.Core.IService
{
		
	public interface ISocialMediaService
	{
        Dictionary<string, string> GetSocialMediaBasicSearchColumns();
        
        List<SearchColumn> GetSocialMediaAdvanceSearchColumns();

		SocialMedia GetSocialMedia(System.Int32 SocialMediaId);
		DataTransfer<List<GetOutput>> GetAll();
		SocialMedia UpdateSocialMedia(SocialMedia entity);
		bool DeleteSocialMedia(System.Int32 SocialMediaId);
		List<SocialMedia> GetAllSocialMedia();
		SocialMedia InsertSocialMedia(SocialMedia entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
