﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SuggestedFlow;

namespace NMS.Core.IService
{
		
	public interface ISuggestedFlowService
	{
        Dictionary<string, string> GetSuggestedFlowBasicSearchColumns();
        
        List<SearchColumn> GetSuggestedFlowAdvanceSearchColumns();

		SuggestedFlow GetSuggestedFlow(System.Int32 SuggestedFlowId);
		DataTransfer<List<GetOutput>> GetAll();
		SuggestedFlow UpdateSuggestedFlow(SuggestedFlow entity);
		bool DeleteSuggestedFlow(System.Int32 SuggestedFlowId);
		List<SuggestedFlow> GetAllSuggestedFlow();
		SuggestedFlow InsertSuggestedFlow(SuggestedFlow entity);
        List<SuggestedFlow> GetSuggestedFlowByKey(string Key);
        List<SuggestedFlow> GetSuggestedFlowsLikeKey(string Key, int ProgramId);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
