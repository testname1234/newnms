﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TagResource;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.IService
{
		
	public interface ITagResourceService
	{
        Dictionary<string, string> GetTagResourceBasicSearchColumns();
        
        List<SearchColumn> GetTagResourceAdvanceSearchColumns();

		List<TagResource> GetTagResourceByTagId(System.Int32? TagId);
		List<TagResource> GetTagResourceByResourceId(System.Int32? ResourceId);
		TagResource GetTagResource(System.Int32 TagResourceid);
		DataTransfer<List<GetOutput>> GetAll();
		TagResource UpdateTagResource(TagResource entity);
		bool DeleteTagResource(System.Int32 TagResourceid);
		List<TagResource> GetAllTagResource();
		TagResource InsertTagResource(TagResource entity);
        bool InsertTagResource(MTagResource entity);
        bool CheckIfTagResourceExists(string tagId);
        bool DeleteByTagGuid(string tagId);


        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool InsertTagResourceNoReturn(TagResource entity);
	}
	
	
}
