﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Tag;
using NMS.Core.Entities.Mongo;

namespace NMS.Core.IService
{
		
	public interface ITagService
	{
        Dictionary<string, string> GetTagBasicSearchColumns();
        
        List<SearchColumn> GetTagAdvanceSearchColumns();

		Tag GetTag(System.Int32 TagId);
		DataTransfer<List<GetOutput>> GetAll();
		Tag UpdateTag(Tag entity);
		bool DeleteTag(System.Int32 TagId);
		List<Tag> GetAllTag();
		Tag InsertTag(Tag entity);
        bool InsertTag(MTag entity);
        bool CheckIfTagExists(string tag);
        bool DeleteByTagGuid(string tagGuid);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Tag> GetTagByTerm(string term);
        Tag InsertIfNotExists(Tag tag);
        Tag GetByGuid(System.String Guid);
        bool InsertTagNoReturn(Tag entity);
        void InsertIfNotExistsNoReturn(Tag tag);

	}
	
	
}
