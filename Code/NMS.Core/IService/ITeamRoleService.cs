﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TeamRole;

namespace NMS.Core.IService
{
		
	public interface ITeamRoleService
	{
        Dictionary<string, string> GetTeamRoleBasicSearchColumns();
        
        List<SearchColumn> GetTeamRoleAdvanceSearchColumns();

		TeamRole GetTeamRole(System.Int32 TeamRoleId);
		DataTransfer<List<GetOutput>> GetAll();
		TeamRole UpdateTeamRole(TeamRole entity);
		bool DeleteTeamRole(System.Int32 TeamRoleId);
		List<TeamRole> GetAllTeamRole();
		TeamRole InsertTeamRole(TeamRole entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
