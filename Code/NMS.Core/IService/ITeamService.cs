﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Team;

namespace NMS.Core.IService
{
		
	public interface ITeamService
	{
        Dictionary<string, string> GetTeamBasicSearchColumns();
        
        List<SearchColumn> GetTeamAdvanceSearchColumns();

		List<Team> GetTeamByProgramId(System.Int32 ProgramId);
		List<Team> GetTeamByTeamRoleId(System.Int32 TeamRoleId);
		Team GetTeam(System.Int32 TeamId);
		DataTransfer<List<GetOutput>> GetAll();
		Team UpdateTeam(Team entity);
		bool DeleteTeam(System.Int32 TeamId);
		List<Team> GetAllTeam();
		Team InsertTeam(Team entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool GetTeamByUserWorkProgramId(int userId, int workRoleId, int programId);
        string InsertTeam(PostInput Input);
        bool DeleteFromTeam(int userId, int workRoleId, int programId);
        List<Team> GetTeamByUserId(int userId);
	}
	
	
}
