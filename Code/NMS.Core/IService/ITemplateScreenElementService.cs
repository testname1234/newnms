﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TemplateScreenElement;

namespace NMS.Core.IService
{
		
	public interface ITemplateScreenElementService
	{
        Dictionary<string, string> GetTemplateScreenElementBasicSearchColumns();
        
        List<SearchColumn> GetTemplateScreenElementAdvanceSearchColumns();

		List<TemplateScreenElement> GetTemplateScreenElementByScreenElementId(System.Int32 ScreenElementId);
		List<TemplateScreenElement> GetTemplateScreenElementByScreenTemplateId(System.Int32? ScreenTemplateId);
		TemplateScreenElement GetTemplateScreenElement(System.Int32 TemplateScreenElementId);
		DataTransfer<List<GetOutput>> GetAll();
		TemplateScreenElement UpdateTemplateScreenElement(TemplateScreenElement entity);
		bool DeleteTemplateScreenElement(System.Int32 TemplateScreenElementId);
		List<TemplateScreenElement> GetAllTemplateScreenElement();
		TemplateScreenElement InsertTemplateScreenElement(TemplateScreenElement entity);
        List<TemplateScreenElement> GetTemplateScreenElementsByProgramId(int programId);
        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
