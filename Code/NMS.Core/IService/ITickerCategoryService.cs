﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TickerCategory;

namespace NMS.Core.IService
{
		
	public interface ITickerCategoryService
	{
        Dictionary<string, string> GetTickerCategoryBasicSearchColumns();
        
        List<SearchColumn> GetTickerCategoryAdvanceSearchColumns();

        List<TickerCategory> GetTickerCategoryWithSubCategories(System.Int32 tickerCategoryId);
        List<TickerCategory> GetTickerCategoryWithSubCategories(System.Int32[] tickerCategoryIds);
        DataTransfer<List<GetOutput>> GetAll();
		TickerCategory UpdateTickerCategory(TickerCategory entity);
		bool DeleteTickerCategory(System.Int32 TickerCategoryId);
		List<TickerCategory> GetAllTickerCategory();
		TickerCategory InsertTickerCategory(TickerCategory entity);
        List<TickerCategory> GetTickerCategory(DateTime lastUpdateDate);
        bool InsertTickerCategoryIfNotExist(int categoryId);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool UpdateTickerCategorySequence(TickerCategory category);
	}
	
	
}
