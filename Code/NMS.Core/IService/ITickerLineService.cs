﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TickerLine;
using NMS.Core.Enums;

namespace NMS.Core.IService
{
		
	public interface ITickerLineService
	{
        Dictionary<string, string> GetTickerLineBasicSearchColumns();
        
        List<SearchColumn> GetTickerLineAdvanceSearchColumns();

		List<TickerLine> GetTickerLineByTickerId(System.Int32? TickerId);
		TickerLine GetTickerLine(System.Int32 TickerLineId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerLine UpdateTickerLine(TickerLine entity);
		bool DeleteTickerLine(System.Int32 TickerLineId);
		List<TickerLine> GetAllTickerLine();
		TickerLine InsertTickerLine(TickerLine entity);
        bool UpdateTickerLineStatus(int tickerLineId, TickerStatuses statusId);
        bool UpdateTickerLine(int tickerLineId, string text);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        TickerLine UpdateTickerLineStatus(int tickerLineId, TickerStatuses status, int? tickerTypeId);
        bool DeleteMcrTickerLineData(int tickerLineId, int tickerTypeId);



        bool SubmitTickersToMcrCategory(TickerLine ticker,int categoryId,int seqId,int tickerId);

        bool SubmitTickersToMcrBreaking(TickerLine ticker, int seqId);

        bool SubmitTickersToMcrLatest(TickerLine ticker, int seqId);

        List<TickerLine> GetTickerLineByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);

        List<TickerLine> GetTickerLineByStatusAndLastUpdateDate(TickerStatuses tickerStatus, DateTime? lastUpdateDate);

        TickerLine UpdateTickerLineSequence(TickerLine item, int TickerTypeId);

        TickerLine UpdateTickerLineNameSeverityFrequency(TickerLine tickerLine);

        bool FlushMcrTickerData(bool category = false, bool breaking = false, bool latest = false);
        Dictionary<string, int> GetSequenceNumbers();
    }
	
	
}
