﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TickerOnAirLog;

namespace NMS.Core.IService
{
		
	public interface ITickerOnAirLogService
	{
        Dictionary<string, string> GetTickerOnAirLogBasicSearchColumns();
        
        List<SearchColumn> GetTickerOnAirLogAdvanceSearchColumns();

		TickerOnAirLog GetTickerOnAirLog(System.Int32 TickerOnAirLogId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerOnAirLog UpdateTickerOnAirLog(TickerOnAirLog entity);
		bool DeleteTickerOnAirLog(System.Int32 TickerOnAirLogId);
		List<TickerOnAirLog> GetAllTickerOnAirLog();
		TickerOnAirLog InsertTickerOnAirLog(TickerOnAirLog entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
