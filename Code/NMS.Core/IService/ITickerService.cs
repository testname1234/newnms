﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Ticker;
using NMS.Core.Enums;
using NMS.Core.Models;

namespace NMS.Core.IService
{

    public interface ITickerService
    {
        Dictionary<string, string> GetTickerBasicSearchColumns();

        List<SearchColumn> GetTickerAdvanceSearchColumns();

        Ticker GetTicker(System.Int32 TickerId);
        DataTransfer<List<GetOutput>> GetAll();
        Ticker UpdateTicker(Ticker entity);
        bool DeleteTicker(System.Int32 TickerId);
        List<Ticker> GetAllTicker();
        Ticker InsertTicker(Ticker entity);
        List<Ticker> GetTickers(string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null);
        List<Ticker> GetTickers(int? userId, DateTime? lastUpdateDate);
        List<Ticker> GetTickers(TickerStatuses tickerStatus, DateTime? lastUpdateDate);
        List<Ticker> GetTickersByTickerOnAirLog(DateTime? lastUpdateDate = null);
        Ticker UpdateTickerStatus(int tickerId, TickerStatuses status, int? tickerTypeId);


        bool UpdateTicker(int tickerId, int severity, int repeatCount, int frequency);
        void pushTickersInCategories(bool isManual = false, bool isSync = true, bool isPush = true);

        //bool SubmitTickersToMcr(Ticker ticker, int count);
        bool FlushMcrTickerData(bool category = false, bool breaking = false, bool latest = false);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        bool UpdateTickerLastUpdateDate(DateTime? date, int? tickerId);

        List<Ticker> GetTickerByLastUpdateDate(DateTime dateTime);

        List<Ticker> GetFilteredTickers(List<TickerStatus> discardedStatus);

        bool DeleteMcrTickerData(int tickerId, int tickerTypeId);

        bool UpdateMcrTickerData(int TickerId, int TickerLineId, int TickerTypeId, bool isFromOnAir);

        bool SubmitTickersToMcrCategory(Ticker ticker, int seqId);

        bool SubmitTickersToMcrBreaking(Ticker ticker, int seqId);

        bool SubmitTickersToMcrLatest(Ticker ticker, int seqId);
        Ticker GetTickerLastRecord();

        bool UpdateTickerSequence(Ticker ticker, int Type);

        void FillTickers(List<Ticker> tickers);
        void FillTicker(Ticker ticker);
        Ticker GetFilledTicker(int tickerId);
        Ticker GetTickerByNewsGuid(string NewsGuid);


        List<Ticker> GetTickerByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);

        Ticker InsertTicker(PostInput entity, int userRoleId, DateTime dtNow);

        List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds);
        List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds, DateTime lastUpdateDate);
        Ticker CreateTicker(string text, int tickerCategoryId, int? newsFileId, int? UserId, int userRoleId, bool isManual = false);
        bool SortTicker(int tickerId, int tickerCategoryId, int sequenceId, SortDirection direction);
        bool UpdateTickerbById(int tickerId, string text, int statusId, int? userId, int userRoleId);
        List<NMS.Core.DataTransfer.TickerCategory.GetOutput> GetAllTickerCategoriesGroupByParentCategory(int[] ParentCategoryIds);
        bool MoveTickerByCategoryId(int TickerId, int TickerCategoryId);
        List<TickerCategory> GetAllCategoriesHavingTickers();
        List<TickerCategory> GetAllTickerCategories();
        TickerCategory InsertTickerCategory(TickerCategory input);

        TickerCategory updateTickerCategory(TickerCategory input);

        List<NMS.Core.DataTransfer.TickerCategory.GetOutput> GetAllTickerCategoriesGroupByTabName(string tabName);

        bool DeleteTickerCategory(TickerCategory input);
        void UpdateMCRTickerId(int tickerId, int mcrTickerId);
        bool MarkTickerValid(int tickerId);
        bool SubmitTickersByCateogryId(int categoryId);

        List<Ticker> GetTickersForMCR(int categoryId, int size, bool sortBySequence, bool? isApproved);
        List<TickerTranslation> GetTickerTranslations(List<int> tickerIds, LanguageCode languageCode);
        TickerTranslation GetTranslation(int tickerId, LanguageCode languageCode);
    }


}

