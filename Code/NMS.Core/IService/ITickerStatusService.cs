﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TickerStatus;

namespace NMS.Core.IService
{
		
	public interface ITickerStatusService
	{
        Dictionary<string, string> GetTickerStatusBasicSearchColumns();
        
        List<SearchColumn> GetTickerStatusAdvanceSearchColumns();

		TickerStatus GetTickerStatus(System.Int32 TickerStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerStatus UpdateTickerStatus(TickerStatus entity);
		bool DeleteTickerStatus(System.Int32 TickerStatusId);
		List<TickerStatus> GetAllTickerStatus();
		TickerStatus InsertTickerStatus(TickerStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
