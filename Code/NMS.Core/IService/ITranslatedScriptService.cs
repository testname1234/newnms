﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TranslatedScript;

namespace NMS.Core.IService
{
		
	public interface ITranslatedScriptService
	{
        Dictionary<string, string> GetTranslatedScriptBasicSearchColumns();
        
        List<SearchColumn> GetTranslatedScriptAdvanceSearchColumns();

		List<TranslatedScript> GetTranslatedScriptByNewsFileId(System.Int32? NewsFileId);
		TranslatedScript GetTranslatedScript(System.Int32 TranslatedScriptId);
		DataTransfer<List<GetOutput>> GetAll();
		TranslatedScript UpdateTranslatedScript(TranslatedScript entity);
		bool DeleteTranslatedScript(System.Int32 TranslatedScriptId);
		List<TranslatedScript> GetAllTranslatedScript();
		TranslatedScript InsertTranslatedScript(TranslatedScript entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<TranslatedScript> GetTranslatedScriptByLanguageId(int languageId);

    }
	
	
}
