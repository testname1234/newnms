﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Alert;

namespace NMS.Core.IService
{
    public interface ITwitterAPI
    {
         Boolean PostTweet(string tweet,string type);
    }
}



