﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TwitterAccount;

namespace NMS.Core.IService
{
		
	public interface ITwitterAccountService
	{
        Dictionary<string, string> GetTwitterAccountBasicSearchColumns();
        
        List<SearchColumn> GetTwitterAccountAdvanceSearchColumns();

		List<TwitterAccount> GetTwitterAccountByCelebrityId(System.Int32 CelebrityId);
		TwitterAccount GetTwitterAccount(System.Int32 TwitterAccountId);
		DataTransfer<List<GetOutput>> GetAll();
		TwitterAccount UpdateTwitterAccount(TwitterAccount entity);
		bool DeleteTwitterAccount(System.Int32 TwitterAccountId);
		List<TwitterAccount> GetAllTwitterAccount();
		TwitterAccount InsertTwitterAccount(TwitterAccount entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
        List<TwitterAccount> GetByIsFollowed(bool IsFollowed);
        void UpdateTwitterAccountIsFollowed(int TwitterAccoutnId, bool Isfollowed);
	}
	
	
}
