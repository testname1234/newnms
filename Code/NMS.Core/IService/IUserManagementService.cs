﻿using NMS.Core.Entities;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.IService
{
    public interface IUserManagementService
    {
        List<User> GetAllUsers();
        List<Team> GetAllUserWorkRoleMappings();
    }
}
