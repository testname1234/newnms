﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.UserRole;

namespace NMS.Core.IService
{
		
	public interface IUserRoleService
	{
        Dictionary<string, string> GetUserRoleBasicSearchColumns();
        
        List<SearchColumn> GetUserRoleAdvanceSearchColumns();

		List<UserRole> GetUserRoleByUserId(System.Int32 UserId);
		List<UserRole> GetUserRoleByRoleId(System.Int32 RoleId);
		UserRole GetUserRole(System.Int32 UserRoleId);
		DataTransfer<List<GetOutput>> GetAll();
		UserRole UpdateUserRole(UserRole entity);
		bool DeleteUserRole(System.Int32 UserRoleId);
		List<UserRole> GetAllUserRole();
		UserRole InsertUserRole(UserRole entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
