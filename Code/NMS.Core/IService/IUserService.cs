﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.User;
using NMS.Core.Enums;

namespace NMS.Core.IService
{
		
	public interface IUserService
	{
        Dictionary<string, string> GetUserBasicSearchColumns();
        
        List<SearchColumn> GetUserAdvanceSearchColumns();

		User GetUser(System.Int32 UserId);
		DataTransfer<List<GetOutput>> GetAll();
		User UpdateUser(User entity);
		bool DeleteUser(System.Int32 UserId);
		List<User> GetAllUser();
        List<User> GetUsers(TeamRoles teamRole);
		User InsertUser(User entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        PostOutput Login(PostInput input);
	}
	
	
}
