﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.VideoCutterIp;

namespace NMS.Core.IService
{
		
	public interface IVideoCutterIpService
	{
        Dictionary<string, string> GetVideoCutterIpBasicSearchColumns();
        
        List<SearchColumn> GetVideoCutterIpAdvanceSearchColumns();

		VideoCutterIp GetVideoCutterIp(System.Int32 VideoCutterIpId);
		DataTransfer<List<GetOutput>> GetAll();
		VideoCutterIp UpdateVideoCutterIp(VideoCutterIp entity);
		bool DeleteVideoCutterIp(System.Int32 VideoCutterIpId);
		List<VideoCutterIp> GetAllVideoCutterIp();
		VideoCutterIp InsertVideoCutterIp(VideoCutterIp entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<VideoCutterIp> GetVideoCutterIpByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);
	}
	
	
}
