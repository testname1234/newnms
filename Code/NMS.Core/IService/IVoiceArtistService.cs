﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.VoiceArtist;

namespace NMS.Core.IService
{
		
	public interface IVoiceArtistService
	{
        Dictionary<string, string> GetVoiceArtistBasicSearchColumns();
        
        List<SearchColumn> GetVoiceArtistAdvanceSearchColumns();

		VoiceArtist GetVoiceArtist(System.Int32 VoiceArtistId);
		DataTransfer<List<GetOutput>> GetAll();
		VoiceArtist UpdateVoiceArtist(VoiceArtist entity);
		bool DeleteVoiceArtist(System.Int32 VoiceArtistId);
		List<VoiceArtist> GetAllVoiceArtist();
		VoiceArtist InsertVoiceArtist(VoiceArtist entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
