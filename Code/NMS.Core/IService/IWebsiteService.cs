﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Website;

namespace NMS.Core.IService
{
		
	public interface IWebsiteService
	{
        Dictionary<string, string> GetWebsiteBasicSearchColumns();
        
        List<SearchColumn> GetWebsiteAdvanceSearchColumns();

		Website GetWebsite(System.Int32 WebsiteId);
		DataTransfer<List<GetOutput>> GetAll();
		Website UpdateWebsite(Website entity);
		bool DeleteWebsite(System.Int32 WebsiteId);
		List<Website> GetAllWebsite();
		Website InsertWebsite(Website entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
