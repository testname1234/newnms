﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.WorkRoleFolder;

namespace NMS.Core.IService
{
		
	public interface IWorkRoleFolderService
	{
        Dictionary<string, string> GetWorkRoleFolderBasicSearchColumns();
        
        List<SearchColumn> GetWorkRoleFolderAdvanceSearchColumns();

		List<WorkRoleFolder> GetWorkRoleFolderByFolderId(System.Int32 FolderId);
		WorkRoleFolder GetWorkRoleFolder(System.Int32 WorkRoleFolderId);
		DataTransfer<List<GetOutput>> GetAll();
		WorkRoleFolder UpdateWorkRoleFolder(WorkRoleFolder entity);
		bool DeleteWorkRoleFolder(System.Int32 WorkRoleFolderId);
		List<WorkRoleFolder> GetAllWorkRoleFolder();
		WorkRoleFolder InsertWorkRoleFolder(WorkRoleFolder entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
