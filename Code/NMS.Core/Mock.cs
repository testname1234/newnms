﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Enums;
using System.Linq;

namespace NMS.Core
{

    public class MockNewsList
    {
        [XmlElement("MockNews")]
        public List<MockNews> listMockNews = new List<MockNews>();
    }

    public class MockNews
    {
        public string title { get; set; }
        public string description { get; set; }
    }

    public static class Mock
    {
        public static List<MNews1> newsList = new List<MNews1>();
        public static List<Core.DataTransfer.News.GetOutput> GetMockNewsList()
        {
            if (newsList.Count == 0)
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(MockNewsList));
                TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "MockNews.xml");
                object obj = deserializer.Deserialize(reader);
                MockNewsList listMock = (MockNewsList)obj;

                bool isVerified = true, isrecommended = true;
                int count = 1;
                foreach (MockNews mockNews in listMock.listMockNews)
                {
                    isVerified = !isVerified;
                    isrecommended = !isrecommended;
                    MNews1 news = new MNews1();
                    news.Id = Guid.NewGuid();
                    news.CreationDate = DateTime.UtcNow.AddDays(count);
                    news.LastUpdateDate = DateTime.UtcNow.AddMonths(count);
                    news.IsVerified = isVerified;
                    news.IsRecommended = isrecommended;

                    news.Version = 1;
                    news.Title = mockNews.title;
                    news.Description = mockNews.description;

                    news.NewsType = NewsTypes.Story;
                    news.CategoryId = 1;
                    news.SubCategoryId = 2;
                    news.NewsSourceId = 1;
                    count++;
                    newsList.Add(news);
                }
            }
            List<Core.DataTransfer.News.GetOutput> output = new List<DataTransfer.News.GetOutput>();
            newsList = newsList.OrderByDescending(x=>x.CreationDate).ToList();
            output.CopyFrom(newsList);
            return output;
        }

        public static List<Core.DataTransfer.Categories.GetOutput> GetMockCategories()
        {
            List<DataTransfer.Categories.GetOutput> data = new List<DataTransfer.Categories.GetOutput>();
            DataTransfer.Categories.GetOutput d1 = new DataTransfer.Categories.GetOutput();
            DataTransfer.Categories.GetOutput d2 = new DataTransfer.Categories.GetOutput();
            DataTransfer.Categories.GetOutput d3 = new DataTransfer.Categories.GetOutput();
            d1._id = Guid.NewGuid();
            d1.Category = "Politics";

            d2._id = Guid.NewGuid();
            d2.Category = "Sports";

            d3._id = Guid.NewGuid();
            d3.Category = "Weather";

            data.Add(d1);
            data.Add(d2);
            data.Add(d3);


            return data;
        }

        public static List<Core.DataTransfer.Location.GetOutput> GetMockLocation()
        {
            List<DataTransfer.Location.GetOutput> data = new List<DataTransfer.Location.GetOutput>();
            DataTransfer.Location.GetOutput d1 = new DataTransfer.Location.GetOutput();
            DataTransfer.Location.GetOutput d2 = new DataTransfer.Location.GetOutput();
            DataTransfer.Location.GetOutput d3 = new DataTransfer.Location.GetOutput();
            d1.LocationId = 1;
            d1.Location = "Karachi";

            d2.LocationId = 2;
            d2.Location = "Lahore";

            d3.LocationId = 3;
            d3.Location = "Islamabad";

            data.Add(d1);
            data.Add(d2);
            data.Add(d3);


            return data;
        }

    }
}
