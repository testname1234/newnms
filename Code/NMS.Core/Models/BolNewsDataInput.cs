﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class BolNewsDataInput
    {
       
        public List<NMS.Core.DataTransfer.Filter.GetOutput> Filters { get; set; }

        [IgnoreDataMember]
        public System.DateTime NewsLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string NewsLastUpdateDateStr
        {
            get { return NewsLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NewsLastUpdateDate = date.ToUniversalTime(); } }
        }

        public System.Int32 PageSize { get; set; }

    }
}
