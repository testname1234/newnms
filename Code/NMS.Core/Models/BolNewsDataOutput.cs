﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class BolNewsDataOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.News.GetOutput> News { get; set; }

    }
}
