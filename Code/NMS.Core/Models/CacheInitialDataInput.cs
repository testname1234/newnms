﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using NMS.Core.Entities;

namespace NMS.Core.Models
{
    [DataContract]
    public class CacheInitialDataInput 
    {
         [DataMember(EmitDefaultValue = false)]
        public  List<NMS.Core.Entities.Mongo.MFilterCount> Data { get; set; }

         [DataMember(EmitDefaultValue = false)]
        public CacheInputRequest Input { get; set; }

        [IgnoreDataMember]
        public System.DateTime ActivityTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ActivityTimeToStr
        {
            get { return ActivityTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ActivityTime = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime AliveTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AliveTimeToStr
        {
            get { return AliveTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { AliveTime = date.ToUniversalTime(); } }
        }


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsExpired
        {
            get { return ((AliveTime.AddMinutes(60) < System.DateTime.UtcNow) ? true : false); }
           
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsNewDataRequired
        {
            get { return ((ActivityTime.AddMinutes(1) < System.DateTime.UtcNow) ? true : false); }

        }
    }

    public class CacheInputRequest
    {
        public System.DateTime From { get; set; }
        public System.DateTime To { get; set; }
        public List<Filter> Filters { get; set; }

        public string FromToStr
        {
            get { return From.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime(); } }
        }

        public string ToTimeToStr
        {
            get { return To.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime(); } }
        }

        public List<int> ProgramFilters { get; set; }
    }

}
