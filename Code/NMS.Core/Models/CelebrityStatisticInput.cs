﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class CelebrityStatisticInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int SlotTemplateScreenElementId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CelebrityId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public NewsStatisticsInput NewsStatisticsParam { get; set; }
    }
}
