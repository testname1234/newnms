﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class ChannelVideoOutput
    {
        public int? ReelMonitoringId { get; set; }
        public int? VideoLibraryId { get; set; }
        public int? ProgramId { get; set; }
        public string PhysicalPath { get; set; }
        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string VideoPath
        {
            get { return PhysicalPath; }
            set { PhysicalPath = value; }
        }
        public int? ChannelId { get; set; }
        public bool IsContent { get; set; }

        [IgnoreDataMember]
        public System.DateTime To { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string StartTimeStr
        {
            get { return From.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime From { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EndTimeStr
        {
            get { return To.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime(); } }
        }

        public System.DateTime CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); LastUpdateDate = date.ToUniversalTime(); } }            
        }

        public System.DateTime LastUpdateDate { get; set; }


    }
}
