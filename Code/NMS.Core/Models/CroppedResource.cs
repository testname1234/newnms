﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Models
{
    public class CroppedResource
    {
        public string ImageUrl { get; set; }
        public int BucketId { get; set; }
        public string ApiKey { get; set; }
        public List<PointInput> Points { get; set; }
    }
    public class PointInput
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
