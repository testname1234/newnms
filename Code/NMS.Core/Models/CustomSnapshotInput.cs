﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Models
{
    public class CustomSnapshotInput
    {
        public string Id { get; set; }

        public double TimeSpan { get; set; }

        public int ResourceTypeId { get; set; }

        public string FilePath { get; set; }
    }
}
