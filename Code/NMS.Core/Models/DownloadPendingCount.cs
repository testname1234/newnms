﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class DownloadPendingCount
    {
        public System.Int32 pendingCount { get; set; }
        public System.Int32 DownloadedCount { get; set; }
    }
}
