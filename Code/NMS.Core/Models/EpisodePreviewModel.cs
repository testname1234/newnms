﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class EpisodePreviewModel
    {
        public EpisodePreviewModel()
        {
            this.SlotPreviews = new Dictionary<int, string>();
        }
     
        public int EpisodeId { get; set; }
        public Guid? EpisodePreview { get; set; }
        public Dictionary<int, string> SlotPreviews { get; set; }

    }
}
