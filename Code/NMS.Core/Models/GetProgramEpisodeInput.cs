﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class GetProgramEpisodeInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int EpisodeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SlotId { get; set; }

        [IgnoreDataMember]
        public System.DateTime Date { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DateStr
        {
            get { return Date.ToString("yyyy-MM-dd HH:mm:ss.fff"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Date = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.Guest.PostInput> Guests { get; set; }
    }
}
