﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class HeadlinesInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int? UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageIndex { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SearchTerm { get; set; }

        [IgnoreDataMember]
        public System.DateTime ToDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ToDateStr
        {
            get { return ToDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ToDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime FromDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FromDateStr
        {
            get { return FromDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FromDate = date.ToUniversalTime(); } }
        }
    }
}
