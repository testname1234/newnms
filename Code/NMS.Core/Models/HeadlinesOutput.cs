﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class HeadlinesOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Episode.GetOutput> Episodes { get; set; }
    }
}
