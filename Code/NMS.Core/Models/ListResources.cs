﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Models
{
    public class ListResources
    {
        public string ListGuids{ get; set; }
        public string MetaTags { get; set; }
        public string ApiKey { get; set; }
        public string BucketId { get; set; }
        public string Slug { get; set; }
        public string ProgramId { get; set; }

    }
}
