﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class LoadChannelOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Channel.GetOutput> Channels { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Program.GetOutput> Programs { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.ChannelVideo.GetOutput> ChannelVideos { get; set; }

    
    }
}
