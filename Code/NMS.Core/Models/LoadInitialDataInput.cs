﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using NMS.Core.Enums;

namespace NMS.Core.Models
{
    [DataContract]
    public class LoadInitialDataInput
    {
        [DataMember(EmitDefaultValue = false)]
        public string Term { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReporterId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BunchId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.Filter.GetOutput> Filters { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.Filter.GetOutput> FiltersToDiscard { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int StartIndex { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserRole { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsArchivalSearch { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Token { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SlotScreenTemplateId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> MessageIds { get; set; }

        [IgnoreDataMember]
        public System.DateTime To { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ToStr
        {
            get { return To.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { To = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime From { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FromStr
        {
            get { return From.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { From = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime NewsFilterLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string NewsFilterLastUpdateDateStr
        {
            get { return NewsFilterLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NewsFilterLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime FilterCountLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FilterCountLastUpdateDateStr
        {
            get { return FilterCountLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FilterCountLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime NewsLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string NewsLastUpdateDateStr
        {
            get { return NewsLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NewsLastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<string> NewsIds { get; set; }

        [IgnoreDataMember]
        public System.DateTime CommentsLastUpdate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CommentsLastUpdateStr
        {
            get { return CommentsLastUpdate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CommentsLastUpdate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime NotificationsLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string NotificationsLastUpdateDateStr
        {
            get { return NotificationsLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NotificationsLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime AlertsLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AlertsLastUpdateDateStr
        {
            get { return AlertsLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { AlertsLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime TickerLastUpdateDate { get; set; }

        [IgnoreDataMember]
        public System.DateTime TickerLineLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerLastUpdateDateStr
        {
            get { return TickerLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerLastUpdateDate = date.ToUniversalTime(); } }
        }


        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerLineLastUpdateDateStr
        {
            get { return TickerLineLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerLineLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime TickerCategoryLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerCategoryLastUpdateDateStr
        {
            get { return TickerCategoryLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerCategoryLastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.TickerStatus.GetOutput> DiscardStatuses { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> ProgramFilterIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<MS.Core.DataTransfer.UserFavourites.PostOutput> UserFavourites { get; set; }


        [IgnoreDataMember]
        public System.DateTime TickerImageLastUpdateDate { get; set; }

        [IgnoreDataMember]
        public System.DateTime ResourceDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerImageLastUpdateDateStr
        {
            get { return TickerImageLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerImageLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime FilterLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FilterLastUpdateDateStr
        {
            get { return FilterLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FilterLastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int[] FolderIds { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int[] ProgramIdList { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> ProgramIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Modules[] Modules { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int[] TickerCategories { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PageOffset { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 WorkRoleId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NewsFileId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LanguageCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProgramRecordingId { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public bool IsBroadcastedcall { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int[]  EpisodeIds { get; set; }


        [IgnoreDataMember]
        public System.DateTime CommentLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CommentLastUpdateDateStr
        {
            get { return CommentLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CommentLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime EpisodeDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EpisodeDateStr
        {
            get { return EpisodeDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EpisodeDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public List<string> ProgramRecordingIdList { get; set; }
    }
}
