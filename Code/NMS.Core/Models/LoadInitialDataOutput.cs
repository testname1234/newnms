﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class LoadInitialDataOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.News.GetOutput> News { get; set; }

        public List<NMS.Core.DataTransfer.NewsFile.GetOutput> NewsFile { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.NewsFilter.GetOutput> NewsFilters { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Comment.GetOutput> Comments { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Filter.GetOutput> Filters { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.FilterCount.GetOutput> FilterCounts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Channel.GetOutput> Channels { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Program.GetOutput> Programs { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.ProgramSchedule.GetOutput> ProgramSchedules { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.ScreenTemplate.GetOutput> ScreenTemplates { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.ScreenElement.GetOutput> ScreenElements { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Message.GetOutput> Messages { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public NMS.Core.DataTransfer.User.GetOutput LogedInuser { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.User.GetOutput> users { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.EventInfo.GetOutput> Events { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Alert.GetOutput> Alerts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Notification.GetOutput> Notifications { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.OnAirTicker.GetOutput> Tickers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.OnAirTickerLine.GetOutput> TickerLines { get; set; }

        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public List<NMS.Core.DataTransfer.OnAirTicker.GetOutput> OnAirTickers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Bunch.GetOutput> Bunch { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Script.GetOutput> Scripts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Term { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.TickerCategory.GetOutput> TickerCategories { get; set; }
        

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsVideoCutterInstalled { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<MS.Core.DataTransfer.UserFavourites.PostOutput> UserFavourites { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Location.GetOutput> Locations { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.NewsBucket.GetOutput> NewsBuckets  { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TickerImage> TickerImages { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MS.Core.DataTransfer.Resource.ResourceOutput Resources { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Ticker.GetOutput> AllTickers { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.Entities.EditorialComment> EditorialComments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.Entities.NewsFile> BroadcastedNewsFiles { get; set; }

        [IgnoreDataMember]
        public System.DateTime ServerSyncTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ServerSyncTimeStr
        {
            get { return ServerSyncTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ServerSyncTime = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public bool? IsTagged { get; set; }
    }
}
