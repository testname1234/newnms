﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using NMS.Core.Enums;
using NMS.Core.Entities;

namespace NMS.Core.Models
{
    [DataContract]
    public class LoadInitialDataProgramRecording
    {
        [DataMember(EmitDefaultValue = false)]
        public List<ProgramRecording> ProgramRecordings { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<RecordingClips> RecordingClips { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Celebrity> Celebrity { get; set; }
    }
}
