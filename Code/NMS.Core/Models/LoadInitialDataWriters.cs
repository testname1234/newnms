﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using NMS.Core.Enums;
using NMS.Core.Entities;

namespace NMS.Core.Models
{
    [DataContract]
    public class LoadInitialDataWriters
    {
        [DataMember(EmitDefaultValue = false)]
        public List<FileDetail> newsFiles { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<TranslatedScript> translatedScrips { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Program> programDetails { get; set; }
    }
}
