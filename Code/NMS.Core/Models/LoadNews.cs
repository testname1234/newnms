﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class LoadNews
    {
        public List<Core.DataTransfer.News.GetOutput> News { get; set; }

        public List<Core.DataTransfer.Bunch.GetOutput> Bunchs { get; set; }
    }
}
