﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class LoadNewsPaperOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.NewsPaper.GetOutput> NewsPappers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.DailyNewsPaper.GetOutput> DailyNewsPapers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.NewsPaperPage.GetOutput> NewsPaperPages { get; set; }
    }
}
