﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class LoadRadioOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.RadioStation.GetOutput> RadioStations { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.RadioStream.GetOutput> RadioStreams { get; set; }            
    }
}
