﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class MMSLoadInitialDataLoginInput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LoginId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }
    }
}
