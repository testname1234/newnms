﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class MMSLoadInitialDataLoginOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ModuleWorkRole> Modules { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SessionKey { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int UserId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FullName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Metadata> MetaData { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EncriptedName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EncriptedUserId { get; set; }

    }


    [DataContract]
    public class ModuleWorkRole
    {
        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ModuleId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? WorkRoleId { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public System.String ModuleUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ModuleAbbriviation { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string WorkRoleName { get; set; }

    }

    [DataContract]
    public class Metadata
    {

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 MetaDataId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? MetaTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String MetaName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String MetaValue { get; set; }

    }
}
