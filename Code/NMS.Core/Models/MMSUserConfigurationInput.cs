﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class MMSUserConfigurationInput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SessionKey { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ModuleId { get; set; }
    }
}
