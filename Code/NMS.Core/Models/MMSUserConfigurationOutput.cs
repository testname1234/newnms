﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class MMSUserConfigurationOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<MMSMetaData> MetaData { get; set; }

    }

    [DataContract]
    public class MMSMetaData
    {

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 MetaDataId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? MetaTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String MetaName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.String MetaValue { get; set; }

    }	
}
