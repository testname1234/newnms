﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class NLELoadInitialDataInput
    {
        [IgnoreDataMember]
        public System.DateTime Date { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DateStr
        {
            get { return Date.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { Date = date.ToUniversalTime(); } }
        }
            
        [IgnoreDataMember]
        public System.DateTime LastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LastUpdateDateStr
        {
            get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        public int UserId { get; set; }

        public int TeamRoleId { get; set; }

        public List<int> MessageIds { get; set; }

        public int PageCount { get; set; }

        public int StartIndex { get; set; }

        public string SlotScreenTemplateId { get; set; }

        public DateTime CommentsLastUpdate { get; set; }

        public string CommentsLastUpdateStr 
        {
            get { return CommentsLastUpdate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CommentsLastUpdate = date.ToUniversalTime(); } }
        }

        public DateTime NotificationsLastUpdateDate { get; set; }

        public string NotificationsLastUpdateDateStr
        {
            get { return NotificationsLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { NotificationsLastUpdateDate = date.ToUniversalTime(); } }
        }
    }
}
