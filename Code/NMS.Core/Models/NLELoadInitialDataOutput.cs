﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace NMS.Core.Models
{
    public class NLELoadInitialDataOutput
    {
        public List<NMS.Core.DataTransfer.Episode.GetOutput> Episodes { get; set; }

        public List<NMS.Core.DataTransfer.Program.GetOutput> Programs { get; set; }

        public List<NMS.Core.DataTransfer.Message.GetOutput> Messages { get; set; }

        public List<NMS.Core.DataTransfer.User.GetOutput> users { get; set; }

        public List<NMS.Core.DataTransfer.Notification.GetOutput> Notifications { get; set; }
    }
}
