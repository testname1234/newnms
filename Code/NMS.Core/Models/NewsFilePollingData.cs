﻿using NMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class NewsFilePollingData
    {
        public List<NewsFile> Files { get; set; }
        public List<FileResource> FileResources { get; set; }
        public List<FileStatusHistory> FileStatusHistory { get; set; }
        public List<FileFolderHistory> FileFolderHistory { get; set; }
        public List<FileDetail> FileDetails { get; set; } 
    }
}
