﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class NewsStatisticsInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int ChannelId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChannelName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProgramName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int EpisodeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<int> CelebrityIds { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> SuggestedFlowKeys { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [IgnoreDataMember]
        public System.DateTime EpisodeStartTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EpisodeStartTimeStr
        {
            get { return EpisodeStartTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EpisodeStartTime = date.ToUniversalTime(); } }
        }
    }
}
