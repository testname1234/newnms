﻿using Newtonsoft.Json;
using NMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class ProgramNewsFileInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int NewsFileId{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string HighLights { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Comments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EditorId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LanguageCode { get; set; }


    }
}
