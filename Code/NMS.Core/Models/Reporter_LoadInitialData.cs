﻿using System;
using System.Collections.Generic;

namespace NMS.Core.Models
{
    public class Reporter_LoadInitialData
    {
        public List<Core.DataTransfer.News.GetOutput> News { get; set; }

        public List<Core.DataTransfer.Categories.GetOutput> NewsCategories { get; set; }

        public List<Core.DataTransfer.Location.GetOutput> Location { get; set; }

        public List<Core.DataTransfer.Tag.GetOutput> NewsTags { get; set; }

    }
}
