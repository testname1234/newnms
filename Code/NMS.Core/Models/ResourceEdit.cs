﻿using System;
using System.Collections.Generic;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using NMS.Core.Enums;

namespace NMS.Core.Models
{
    [BsonIgnoreExtraElements]
    public class ResourceEdit
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Guid ResourceGuid { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ChannelVideoId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ProgramId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ResourceTypeId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FileName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float Top { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float Left { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float Bottom { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float Right { get; set; }

        public List<FromTo> FromTos { get; set; }

        [BsonIgnore]
        public string Url { get; set; }

        public void GenenrateFileName()
        {
            string fileName = System.IO.Path.GetFileNameWithoutExtension(FileName);
            if (ResourceTypeId == (int)ResourceTypes.Image)
            {
                fileName += Top.ToString() + "-" + Bottom.ToString() + "-" + Left.ToString() + "-" + Right.ToString();
            }
            else
            {
                foreach (var fromTo in FromTos)
                    fileName += fromTo.From.ToString("00.00") + "-" + fromTo.To.ToString("00.00");
            }
            FileName = fileName + System.IO.Path.GetExtension(FileName);
        }
    }

    public class FromTo
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double From { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double To { get; set; }
    }
}
