﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NMS.Core.Models
{
    public class Rundown
    {
        [DataMember(EmitDefaultValue = false)]
        public NewsStatisticsInput NewsStatisticsParam { get; set; }
    }
}
