﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NMS.Core.Models
{
    public class SaveScreenTemplateInput
    {
        public List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput> Templates { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32 UserRoleId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public System.Int32? ProgramId { get; set; }
    }
}
