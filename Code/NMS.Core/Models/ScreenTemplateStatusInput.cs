﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class ScreenTemplateStatusInput
    {
        [DataMember(EmitDefaultValue=false)]
        public int SlotScreenTemplateId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? StatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? NLEStatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? StoryWriterStatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserRoleId { get; set; }
    }
}
