﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using NMS.Core.Entities;

namespace NMS.Core.Models
{
    public class SqlMigration
    {

        public List<NewsFilter> NewsFilters { get; set; }
        public List<NewsResource> NewsResources { get; set; }
        public List<NewsTag> NewsTags { get; set; }
        public List<NewsStatistics> NewsStatistics { get; set; }
        public List<NewsLocation> NewsLocations { get; set; }
        public List<NewsCategory> NewsCategories { get; set; }
        public Bunch SqlBunch { get; set; }
        public List<BunchFilterNews> SqlBunchFilterNews { get; set; }
        public List<BunchFilterNews> SqlBunchFilters { get; set; }
        public List<BunchNews> SqlBunchNews { get; set; }
        public List<NewsResourceEdit> newsResourceEdits { get; set; }
        public List<ResourceEditclipinfo> ResourceEditclipinfos { get; set; }

    }
}
