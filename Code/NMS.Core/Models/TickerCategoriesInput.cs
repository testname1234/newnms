﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Validation;

namespace NMS.Core.Models
{
    public class TickerCategoriesInput
	{
         [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.TickerCategory.PostInput> TickerCategories { get; set; }
	}	
}
