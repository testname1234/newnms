﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Validation;

namespace NMS.Core.Models
{
    public class TickerCategoryInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int TickerCategoryId { get; set; }

        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserRole { get; set; }


    }
}
