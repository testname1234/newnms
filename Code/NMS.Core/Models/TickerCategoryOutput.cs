﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerCategoryOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int TickerCateogoryId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int LanguageCode { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ParentCategoryId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int SequenceNumber { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.TickerLine.GetOutput> TickerLines { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.TickerCategory.GetOutput> TickerCategories { get; set; }
    }
}
