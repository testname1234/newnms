﻿using Newtonsoft.Json;
using NMS.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int? UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageIndex { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SearchTerm { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerStatusId { get; set; }

        [IgnoreDataMember]
        public System.DateTime ToDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ToDateStr
        {
            get { return ToDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ToDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime FromDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FromDateStr
        {
            get { return FromDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FromDate = date.ToUniversalTime(); } }
        }

        public List<NMS.Core.DataTransfer.TickerStatus.GetOutput> DiscardStatuses { get; set; }

        public string Text{get;set;}
        public int TickerCategoryId { get; set; }
        public int? NewsFileId { get; set; }
        public SortDirection Direction { get; set; }
        public int SequenceId { get; set; }
        public int TickerId { get; set; }
        public bool IsApproved { get; set; }
        public int StatusId { get; set; }
        public int UserRoleId { get; set; }
    }
}
