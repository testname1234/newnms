﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerInsertInput
    {
        [DataMember]
        public List<NMS.Core.DataTransfer.Ticker.PostInput> Tickers { get; set; }

        [DataMember]
        public int UserRole { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int TickerTypeId { get; set; }

        [DataMember]
        public List<int> RemovedTickers { get; set; }

      
    }
}
