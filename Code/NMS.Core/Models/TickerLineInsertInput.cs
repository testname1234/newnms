﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerLineInsertInput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.TickerLine.PostInput> TickerLines { get; set; }

        [DataMember]
        public int UserRole { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int TickerTypeId { get; set; }
    }
}
