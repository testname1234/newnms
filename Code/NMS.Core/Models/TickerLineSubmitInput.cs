﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerLineSubmitInput
    {
        [DataMember(EmitDefaultValue = false)]
        public string CMD { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerLineId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerTypeId { get; set; }

        [IgnoreDataMember]
        public System.DateTime TickerLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerLastUpdateDateStr
        {
            get { return TickerLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerLastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? SequenceId { get; set; }

      
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? BreakingStatusId { get; set; }

      
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? LatestStatusId { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? CategoryStatusId { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? BreakingSequenceId { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? LatestSequenceId { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? CategorySequenceId { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? RepeatCount { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? Severity { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? Frequency { get; set; }

        
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? CreatedBy { get; set; }
    }
}
