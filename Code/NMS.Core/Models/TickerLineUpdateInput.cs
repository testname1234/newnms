﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerLineUpdateInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int TickerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Guid NewsGuid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TickerGroupName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LocationId { get; set; }
    

        public List<Core.DataTransfer.TickerLine.PostInput> TickerLines { get; set; }


    }
}
