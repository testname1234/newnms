﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerLoadInitialDataInput
    {
        [IgnoreDataMember]
        public System.DateTime ApprovedTickerLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ApprovedTickerLastUpdateDateStr
        {
            get { return ApprovedTickerLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ApprovedTickerLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime OnAirTickerLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OnAirTickerLastUpdateDateStr
        {
            get { return OnAirTickerLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { OnAirTickerLastUpdateDate = date.ToUniversalTime(); } }
        }
        
        [IgnoreDataMember]
        public System.DateTime OnAiredTickerLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OnAiredTickerLastUpdateDateStr
        {
            get { return OnAiredTickerLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { OnAiredTickerLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime TickerCategoryLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerCategoryLastUpdateDateStr
        {
            get { return TickerCategoryLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerCategoryLastUpdateDate = date.ToUniversalTime(); } }
        }
        public List<NMS.Core.DataTransfer.TickerStatus.GetOutput> DiscardStatuses { get; set; }
        
    }
}
