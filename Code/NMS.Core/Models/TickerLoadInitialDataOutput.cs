﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerLoadInitialDataOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Ticker.GetOutput> ApprovedTickers { get; set; }

        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public List<NMS.Core.DataTransfer.Ticker.GetOutput> OnAirTickers { get; set; }

        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public List<NMS.Core.DataTransfer.Ticker.GetOutput> OnAiredTickers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.TickerCategory.GetOutput> TickerCategories { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Core.DataTransfer.User.GetOutput> Users { get; set; }
    }
}
