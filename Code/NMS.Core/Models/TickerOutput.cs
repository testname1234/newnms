﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerOutput
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.Ticker.GetOutput> Tickers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.TickerLine.GetOutput> TickerLines { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<NMS.Core.DataTransfer.TickerCategory.GetOutput> TickerCategories { get; set; }
    }
}
