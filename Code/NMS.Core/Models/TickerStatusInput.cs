﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerStatusInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int? TickerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerLineId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int StatusId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TickerPreviousStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerTypeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SequenceId { get; set; }
    }
}
