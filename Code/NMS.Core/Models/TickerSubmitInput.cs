﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerSubmitInput
    {
        [DataMember(EmitDefaultValue = false)]
        public string CMD { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerTypeId { get; set; }

        [IgnoreDataMember]
        public System.DateTime TickerLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerLastUpdateDateStr
        {
            get { return TickerLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerLastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.TickerLine.PostInput> TickerLines { get; set; }
    }
}
