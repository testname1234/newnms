﻿using NMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerSubmitOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.Ticker.GetOutput> Tickers { get; set; }
    }
}
