﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Core.Models
{
    public class TickerUpdateInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int? TickerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? TickerLineId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? Severity { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? Frequency { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? RepeatCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Text { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TickerPreviousStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TickerTypeId { get; set; }


    }
}
