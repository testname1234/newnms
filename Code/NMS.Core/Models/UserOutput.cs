﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Models
{
    public class UserOutput
    {
        public virtual System.Int32 UserId { get; set; }

        public virtual System.String LoginId { get; set; }

        public virtual System.String Password { get; set; }

        public virtual System.String Fullname { get; set; }

        public virtual System.String Department { get; set; }

        public virtual System.String Designation { get; set; }

        public virtual System.String EmailAddress { get; set; }

        public virtual System.Boolean? IsActive { get; set; }
    }
}
