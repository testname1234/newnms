﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMS.Core.Models
{
    public class UserWorkRoleOutput
    {
        public virtual System.Int32 WorkRoleId { get; set; }

        public virtual System.Int32 UserId { get; set; }
    }
}
