﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Core.Enums;

namespace NMS.Core.Models
{
   public class pcrMachinesDataOutput
    {
        public string Ip { get; set; }

        public string DeviceKey { get; set; }

        public int Port { get; set; }

        public int GroupId { get; set; }

        public bool IsMosDevice { get; set; }

        public int DeviceTypeId { get; set; }

        public DeviceType DeviceType
        {
            get
            {
                return (DeviceType)DeviceTypeId;
            }
            set
            {
                DeviceTypeId = (int)value;
            }
        }
    }
}
