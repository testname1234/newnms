﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using NMS.Core.Entities;

namespace NMS.Core
{
    public class NMSMongoContext
    {
        public MongoDatabase Database;

        public NMSMongoContext()
        {
            var client = new MongoClient(ConfigurationManager.ConnectionStrings["NMSMongoConnectionString"].ToString());
            var server = client.GetServer();
            Database = server.GetDatabase(ConfigurationManager.AppSettings["NMSDatabaseName"].ToString());
        
        }
    }
}
