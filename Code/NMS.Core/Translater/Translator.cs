﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Xml.Linq;
using System.Configuration;



namespace NMS.Core
{
    public class Translator
    {
        public static readonly string DatamarketAccessUri = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
        //public const string clientId = "lcp";        
        //public const string clientSecret = "Y81df2sTIo4oWpXrUUhzNsItnc2wsSBZ1Wm1//8lyvQ=";
        //public const string clientId = "Scrp";
        //public const string clientSecret = "DAS1azVGkKkIUPjkon5qd6/C7QDhfP8UTg5pthpfHDE=";
        public string googleApiKey = ConfigurationManager.AppSettings["googleApiKey"].ToString();
        public string clientId = ConfigurationManager.AppSettings["BINGSClientId"].ToString();
        public string clientSecret = ConfigurationManager.AppSettings["BINGSSecret"].ToString();

        //https://datamarket.azure.com/dataset/bing/microsofttranslator
        //Account Details
        //nmstranslater@hotmail.com
        //Axact123
        string request;
        static DateTime accessTokenGetTime = DateTime.UtcNow;
        static AdmAccessToken admToken;
        // public static List<Language> Languages;

        static Translator()
        {           
        }

        public string DetectLanguageBing(string message)
        {
            this.request = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=http://api.microsofttranslator.com", Uri.EscapeDataString(clientId), Uri.EscapeDataString(clientSecret));
            if (admToken == null || (DateTime.Now - accessTokenGetTime).Minutes > 8)
            {
                admToken = GetAccessToken();
                accessTokenGetTime = DateTime.Now;
            }
            string uri = "http://api.microsofttranslator.com/v2/Http.svc/Detect?appId=" + "&text=" + message;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add("Authorization", "Bearer " + admToken.access_token);
            WebResponse response = null;
            try
            {
                response = httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(Type.GetType("System.String"));
                    string languageDetected = (string)dcs.ReadObject(stream);
                    return languageDetected;
                }
            }
            catch (Exception ex)
            {                
                throw;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
        }

        public string Translate(string message, string FromLanguageCode, string ToLanguageCode)
        {
            try
            {
                var txt = TranslateUsingGoogleApi(message, FromLanguageCode, ToLanguageCode);
               // var txt = TranslateUsingBingApi(message, FromLanguageCode, ToLanguageCode);

                return txt;
            }
            catch (Exception exp)
            {

                exp.Log();
                try
                {

                    var txt = TranslateUsingGoogleApi(message, FromLanguageCode, ToLanguageCode);
                    return txt;
                }
                catch (Exception ex)
                {
                    ex.Log();
                    return "";
                }
            }
            return "";

        }

        public string DetectLanguage(string message)
        {
            try
            {                
                var txt = DetectLanguageBing(message);
                return txt;
            }
            catch (Exception exp)
            {

                exp.Log();
                try
                {
                    var txt = DetectLanguageGoogle(message);
                    return txt;
                }
                catch (Exception ex)
                {
                    ex.Log();
                    return "";
                }
            }
            return "";
        }

        public class detections
        {
            public string language { get; set; }
            public Boolean isReliable { get; set; }
            public double confidence { get; set; }
        }

        public class Translation
        {
            public string translatedText { get; set; }
        }

        public class Data
        {
            public List<Translation> translations { get; set; }
            public List<List<detections>> detections { get; set; }
        }

        public class GoogleLanguage
        {
            public Data data { get; set; }            
        }

        public string DetectLanguageGoogle(string message)
        {
            string uri = string.Format("https://www.googleapis.com/language/translate/v2/detect?q={1}&key={0}", googleApiKey, message);
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            WebResponse response = null;
            try
            {
                response = httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    string output = new StreamReader(stream).ReadToEnd();
                    var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<GoogleLanguage>(output);
                    return obj.data.detections[0][0].language;
                    //data t = new data();
                    //System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(data));
                    //data languageDetected = (data)dcs.ReadObject(stream);
                   // return "";
                }
            }
            catch (Exception ex)
            {
                throw;
                return message;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
        }


        private string TranslateUsingGoogleApi(string message, string fromLanguage, string toLanguage)
        {
            string uri = string.Format("https://www.googleapis.com/language/translate/v2?key={0}&q={1}&source={2}&target={3}", googleApiKey, message, fromLanguage, toLanguage);
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            WebResponse response = null;
            try
            {
                response = httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    string output = new StreamReader(stream).ReadToEnd();
                    var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<GoogleLanguage>(output);                    
                    return obj.data.translations[0].translatedText;
                }
            }
            catch (Exception ex)
            {
                throw;
                //XChatLiveProException exp = new XChatLiveProException(string.Format("Google Translation Error : {0}", ex.Message));
                //ExceptionHandler.LogElmahException(exp);


                return message;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
        }

        private string TranslateUsingBingApi(string message, string fromLanguage, string toLanguage)
        {
            //If clientid or client secret has special characters, encode before sending request
            this.request = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=http://api.microsofttranslator.com", Uri.EscapeDataString(clientId), Uri.EscapeDataString(clientSecret));

            if (admToken == null || (DateTime.Now - accessTokenGetTime).Minutes > 8)
            {
                admToken = GetAccessToken();
                accessTokenGetTime = DateTime.Now;
            }
            return GetTranslatedTextByAuthToken(message, "Bearer " + admToken.access_token, fromLanguage, toLanguage);
        }

        private string GetTranslatedTextByAuthToken(string textToTranslate, string authToken, string fromLanguage, string toLanguage)
        {
            string uri = "http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=" +
                            "&text=" + textToTranslate + "&from=" + fromLanguage + "&to=" + toLanguage;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add("Authorization", authToken);
            WebResponse response = null;
            try
            {
                response = httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(Type.GetType("System.String"));
                    string languageDetected = (string)dcs.ReadObject(stream);
                    return languageDetected;
                }
            }
            catch (Exception ex)
            {
                // Exception exp = new Exception(ex.Message + uri);                
                // exp.Message = 
                // exp.StackTrace = ex.StackTrace;
                throw;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
        }

        private AdmAccessToken GetAccessToken()
        {
            return HttpPost(DatamarketAccessUri, this.request);
        }

        private AdmAccessToken HttpPost(string DatamarketAccessUri, string requestDetails)
        {
            //Prepare OAuth request 
            WebRequest webRequest = WebRequest.Create(DatamarketAccessUri);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(requestDetails);
            webRequest.ContentLength = bytes.Length;
            using (Stream outputStream = webRequest.GetRequestStream())
            {
                outputStream.Write(bytes, 0, bytes.Length);
            }
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AdmAccessToken));
                //Get deserialized object from JSON stream
                AdmAccessToken token = (AdmAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
                return token;
            }
        }

        public string TranslateUsingBingApiPOST(string[] message, Language fromLanguage, Language toLanguage)
        {
            //If clientid or client secret has special characters, encode before sending request
            this.request = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=http://api.microsofttranslator.com", Uri.EscapeDataString(clientId), Uri.EscapeDataString(clientSecret));

            if (admToken == null || (DateTime.Now - accessTokenGetTime).Minutes > 8)
            {
                admToken = GetAccessToken();
                accessTokenGetTime = DateTime.Now;
            }
            return TranslateArrayMethod(message, "Bearer " + admToken.access_token, fromLanguage, toLanguage);
        }
        private static string TranslateArrayMethod(string[] translateArraySourceTexts, string authToken, Language fromLanguage, Language toLanguage)
        {
            string from = fromLanguage.langCode;
            string to = toLanguage.langCode;
            string translatedText = String.Empty;
            // translateArraySourceTexts = { "The answer lies in machine translation.", "the best machine translation technology cannot always provide translations tailored to a site or users like a human ", "Simply copy and paste a code snippet anywhere " };
            string uri = "http://api.microsofttranslator.com/v2/Http.svc/TranslateArray";
            string body = "<TranslateArrayRequest>" +
                             "<AppId />" +
                             "<From>{0}</From>" +
                             "<Options>" +
                                " <Category xmlns=\"http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2\" />" +
                                 "<ContentType xmlns=\"http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2\">{1}</ContentType>" +
                                 "<ReservedFlags xmlns=\"http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2\" />" +
                                 "<State xmlns=\"http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2\" />" +
                                 "<Uri xmlns=\"http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2\" />" +
                                 "<User xmlns=\"http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2\" />" +
                             "</Options>" +
                             "<Texts>" +
                                "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">{2}</string>" +
                             "</Texts>" +
                             "<To>{3}</To>" +
                          "</TranslateArrayRequest>";
            string reqBody = string.Format(body, from, "text/plain", translateArraySourceTexts[0], to);
            // create the request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Headers.Add("Authorization", authToken);
            request.ContentType = "text/xml";
            request.Method = "POST";

            using (System.IO.Stream stream = request.GetRequestStream())
            {
                byte[] arrBytes = System.Text.Encoding.UTF8.GetBytes(reqBody);
                stream.Write(arrBytes, 0, arrBytes.Length);
            }

            // Get the response
            WebResponse response = null;
            try
            {
                response = request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader rdr = new StreamReader(stream, System.Text.Encoding.UTF8))
                    {
                        // Deserialize the response
                        string strResponse = rdr.ReadToEnd();
                        //Console.WriteLine("Result of translate array method is:");
                        XDocument doc = XDocument.Parse(@strResponse);
                        XNamespace ns = "http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2";
                        //int soureceTextCounter = 0;
                        foreach (XElement xe in doc.Descendants(ns + "TranslateArrayResponse"))
                        {

                            foreach (var node in xe.Elements(ns + "TranslatedText"))
                            {
                                //Console.WriteLine("\n\nSource text: {0}\nTranslated Text: {1}", translateArraySourceTexts[soureceTextCounter], node.Value);
                                translatedText = node.Value;
                            }
                            //soureceTextCounter++;
                        }
                        //Console.WriteLine("Press any key to continue...");
                        //Console.ReadKey(true);
                    }
                }
            }
            catch (Exception ex)
            {
                translatedText = translateArraySourceTexts[0]; // "Error Occured while Translating this Message";
                //throw;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
            return translatedText;
        }
    }

    [DataContract]
    public class AdmAccessToken
    {
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public string token_type { get; set; }
        [DataMember]
        public string expires_in { get; set; }
        [DataMember]
        public string scope { get; set; }
    }

    public class Language
    {
        public string lang;
        public string langCode;

        public override string ToString()
        {
            return (lang);
        }
    }
}
