﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Integration.MCRTicker
{
    public class MCRTickerCategory
    {
        public int MCRTickerCategoryId { get; set; }
        public string MCRTickerCategoryName { get; set; }
        public bool IsManual { get; set; }
        public int ChannelId { get; set; }
    }
}
