﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMS.Core.DataTransfer;
using NMS.Core.DataInterfaces;
using NMS.Core;
using NMS.Core.Entities;
using System.Data;

namespace NMS.Integration.MCRTicker
{
    public class MCRTickerService
    {
        MySqlConnection conn;
        string tickerTableName;
        string crouselTableName;
        List<int> categoriesToNotDelete = new List<int>() { 1, 2, 3, 5 };
        private int CreatorId = 29;
        public MCRTickerService()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MySqlTicker"].ToString();
            tickerTableName = ConfigurationManager.AppSettings["MySqlTickerTable"].ToString();
            crouselTableName = ConfigurationManager.AppSettings["MCRCrouselTable"].ToString();
            conn = new MySqlConnection(connectionString);


        }

        public List<MCRTickerCategory> GetMCRTickerCategories()
        {
            var data = new List<MCRTickerCategory>();
            try
            {
                SafeOpen();

                var query = $"select * from mcr_tickers";
                var cmd = new MySqlCommand(query, conn);
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    data.Add(new MCRTickerCategory()
                    {
                        MCRTickerCategoryId = reader.GetInt32("tickerId"),
                        MCRTickerCategoryName = reader.GetString("tickerName"),
                        ChannelId = reader.GetInt32("channelID"),
                        IsManual = ColumnExists(reader, "tickerType") ? reader.GetString("tickerType") != "Auto" : categoriesToNotDelete.Contains(reader.GetInt32("tickerId"))

                    });
                }
                return data;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);

            }
            finally
            {
                SafeClose();
            }
            return data;
        }

        public int AddTicker(string text, MCRTickerCategory mcrTickerCategory, int priority, DateTime creationDate, DateTime lastUpdatedTime)
        {
            try
            {
                SafeOpen();
                //  var categoryId = GetCate(mcrTickerCategoryId);
                // // if (categoryId >= 0)
                mcrTickerCategory.ChannelId = 1;
                var query = $@"INSERT INTO {tickerTableName} (newstext, newsTicker, newsStatus,createDate, channelID, newsApprovalStatus, creator) 
                                VALUES('{text}', '{mcrTickerCategory.MCRTickerCategoryId},','{mcrTickerCategory.MCRTickerCategoryName}' , @CreationDate, '{mcrTickerCategory.ChannelId}' , 'Approved', {CreatorId});
                                SELECT LAST_INSERT_ID()";
                var cmd = new MySqlCommand(query, conn);
                cmd.Parameters.Add(new MySqlParameter("@CreationDate", creationDate));
                var tickerId = Convert.ToInt32(cmd.ExecuteScalar());
                AddCrousel(tickerId, mcrTickerCategory.MCRTickerCategoryId, priority, lastUpdatedTime);
                return tickerId;
                //}
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);

            }
            finally
            {
                SafeClose();
            }
            return -1;
        }

        private int AddCrousel(int tickerId, int mCRTickerCategoryId, int priority, DateTime lastUpdatedTime)
        {
            try
            {
                SafeOpen();
                //  var categoryId = GetCate(mcrTickerCategoryId);
                // // if (categoryId >= 0)
                var query = $@"INSERT INTO {crouselTableName} (tickerId, newsId, createDate, creator, priority, createTime) 
                                VALUES({mCRTickerCategoryId}, {tickerId}, @LastUpdatedDate, {CreatorId}, {priority}, @LastUpdatedDate);
                                SELECT LAST_INSERT_ID()";
                var cmd = new MySqlCommand(query, conn);
                cmd.Parameters.Add(new MySqlParameter("@LastUpdatedDate", lastUpdatedTime));
                var crouselId = Convert.ToInt32(cmd.ExecuteScalar());
                //}
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);

            }
            finally
            {
                SafeClose();
            }
            return -1;
        }

        public bool DeleteTicker(int mcrTickerId)
        {
            try
            {
                SafeOpen();

                var query = $@"DELETE FROM {tickerTableName} WHERE NEWSID = {mcrTickerId} ";
                var cmd = new MySqlCommand(query, conn);
                return Convert.ToInt32(cmd.ExecuteNonQuery()) > 0;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                return false;
            }
            finally
            {
                SafeClose();
            }

        }

        private int GetCategoryIdByName(string tickerCategoryName)
        {
            try
            {
                SafeOpen();

                var query = $"select tickerId from mcr_tickers where tickerName = '{tickerCategoryName}' limit 1";
                var cmd = new MySqlCommand(query, conn);
                var tickerCategoryId = cmd.ExecuteScalar();
                return tickerCategoryId != DBNull.Value && tickerCategoryId != null ? Convert.ToInt32(tickerCategoryId) : -1;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                return -1;
            }
            finally
            {
                SafeClose();
            }
        }

        private bool SafeOpen()
        {
            try
            {
                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }
                return conn.State == System.Data.ConnectionState.Open;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);

            }
            return false;
        }
        private bool SafeClose()
        {
            try
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                return conn.State == System.Data.ConnectionState.Closed;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);

            }
            return false;
        }

        public bool DeleteAllTicker()
        {
            try
            {
                var allCategories = GetMCRTickerCategories();

                SafeOpen();
                var categoriesToNotDelete = allCategories.Where(x => x.IsManual).Select(x => x.MCRTickerCategoryId).ToList();
                var query = $@"delete from {tickerTableName} where channelID = 1 and newsTicker not in (" + string.Join(",", categoriesToNotDelete.Select(x => "'" + x.ToString() + ",'")) + $@");
                    delete from { crouselTableName} where tickerid not in (" + string.Join(",", categoriesToNotDelete) + ");";
                var cmd = new MySqlCommand(query, conn);
                return Convert.ToInt32(cmd.ExecuteNonQuery()) > 0;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);

            }
            finally
            {
                SafeClose();
            }
            return false;
        }

        private bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i) == columnName)
                {
                    return true;
                }
            }

            return false;
        }

    }
}
