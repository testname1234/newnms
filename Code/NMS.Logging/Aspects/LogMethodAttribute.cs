﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMS.Logging.Enums;

namespace NMS.Logging.Aspects
{
    [Serializable]
    public class LogMethodAttribute : System.Attribute
    {
        public bool HasPrimaryKey { get; set; }
        public TableName Table { get; set; }

        public LogMethodAttribute()
        {
            HasPrimaryKey = false;
        }
    }
}
