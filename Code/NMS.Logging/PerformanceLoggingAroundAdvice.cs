﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AopAlliance.Intercept;
using NMS.Logging.Aspects;
using NMS.Logging.Enums;
using NMS.Core.Entities;
using System.Web;
using NMS.Core.IService;
using NMS.Core;
using System.Web.Script.Serialization;

namespace NMS.Logging
{
    public class PerformanceLoggingAroundAdvice : IMethodInterceptor
    {
       // private AdminUser _userAccount;
        public object Invoke(IMethodInvocation invocation)
        {
            bool hasprimarykey = false;
            object attr = null;
            try
            {
                DateTime currentTime = DateTime.UtcNow;
                var attrs = invocation.Method.GetCustomAttributes(true);
                var count = attrs.Count();
                object argumentEntity = null;
                attr = attrs.Where(x => x.GetType() == typeof(LogMethodAttribute)).FirstOrDefault();
                if (attr != null)
                {
                    var refrenceId = -1;
                    TableName tblName = new TableName();
                    if (((LogMethodAttribute)attr).HasPrimaryKey)
                    {
                        hasprimarykey = true;
                        var Pk_Property = invocation.Arguments.Where(x => x.GetType() == typeof(int)).FirstOrDefault();
                        if (Pk_Property != null)
                        {
                            refrenceId = (int)Pk_Property;
                        }
                        tblName = ((LogMethodAttribute)attr).Table;
                    }
                    else
                    {
                        argumentEntity =  invocation.Arguments.Where(x => typeof(EntityBase).IsAssignableFrom(x.GetType())).FirstOrDefault();
                        if (argumentEntity != null)
                        {
                            var PK_Property = argumentEntity.GetType().GetProperties().FirstOrDefault(b => b.GetCustomAttributes(true).Any(c => c.GetType() == typeof(PrimaryKeyAttribute)));
                            refrenceId = (int)PK_Property.GetValue(argumentEntity, null);
                            Enum.TryParse<TableName>(argumentEntity.GetType().Name, out tblName);
                        }
                    }
                    if (refrenceId > -1)
                    {
                        InserLogHistory(invocation, refrenceId, tblName, argumentEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.LogElmahException(new Exception(string.Format("error in Logging")));
            }

            object returnValue = invocation.Proceed();
            if (hasprimarykey)
            {
                try
                {
                    TableName tblName = new TableName();
                    tblName = ((LogMethodAttribute)attr).Table;
                    if (returnValue != null)
                    {
                        int refrenceId = 0;
                        var PK_Property = returnValue.GetType().GetProperties().FirstOrDefault(b => b.GetCustomAttributes(true).Any(c => c.GetType() == typeof(PrimaryKeyAttribute)));
                        refrenceId = (int)PK_Property.GetValue(returnValue, null);
                        InserLogHistory(invocation, refrenceId, tblName, returnValue);
                    }
                }
                catch
                { }
            }

            //bool enableLogging = false;
            //bool.TryParse(ConfigurationManager.AppSettings["enableLogging"].ToString(), out enableLogging);
            //if (enableLogging)
            //{
            //    double totalMilliSeconds = (DateTime.UtcNow - currentTime).TotalMilliseconds;
            //    string status = string.Empty;
            //    if (totalMilliSeconds < 400)
            //    {
            //        status = "Good";
            //        string log = string.Format("<{2}>Took {0} milliseconds to execute {1} method", totalMilliSeconds, invocation.Method.DeclaringType.FullName + "." + invocation.Method.Name, status);
            //        //File.AppendAllLines(AppDomain.CurrentDomain.BaseDirectory + string.Format("\\Logging_{0}.txt", status), new List<string>() { log });
            //    }
            //    else if (totalMilliSeconds < 800)
            //    {
            //        status = "Satisfactory";
            //        string log = string.Format("<{2}>Took {0} milliseconds to execute {1} method", totalMilliSeconds, invocation.Method.DeclaringType.FullName + "." + invocation.Method.Name, status);
            //        try
            //        {
            //            File.AppendAllLines(AppDomain.CurrentDomain.BaseDirectory + string.Format("\\Logging_{0}.txt", status), new List<string>() { log });
            //        }
            //        catch
            //        {
            //            File.AppendAllLines(AppDomain.CurrentDomain.BaseDirectory + string.Format("\\Logging_{0}_{1}.txt", status, Guid.NewGuid()), new List<string>() { log });
            //        }
            //    }
            //    else
            //    {
            //        status = "Poor";
            //        string log = string.Format("<{2}>Took {0} milliseconds to execute {1} method", totalMilliSeconds, invocation.Method.DeclaringType.FullName + "." + invocation.Method.Name, status);
            //        try
            //        {
            //            File.AppendAllLines(AppDomain.CurrentDomain.BaseDirectory + string.Format("\\Logging_{0}.txt", status), new List<string>() { log });
            //        }
            //        catch
            //        {
            //            File.AppendAllLines(AppDomain.CurrentDomain.BaseDirectory + string.Format("\\Logging_{0}_{1}.txt", status, Guid.NewGuid()), new List<string>() { log });
            //        }
            //    }
            //}
            return returnValue;
        }

        private void InserLogHistory(IMethodInvocation invocation, int refrenceId, TableName tblName, object entity)
        {
            //_userAccount = SessionHelper.GetSession(HttpContext.Current.User.Identity.Name);
            try
            {
                ILogHistoryService service = IoC.Resolve<ILogHistoryService>("LogHistoryService");
                
                LogHistory log_history = new LogHistory();
                DateTime date = DateTime.UtcNow;
                log_history.RefrenceId = Convert.ToInt32(refrenceId);
                log_history.CreationDate = date;
                log_history.UserId = Convert.ToInt32(HttpContext.Current.Request.Cookies["user-id"].Value);
                log_history.UserIp = HttpContext.Current.Request.UserHostAddress;
                log_history.Method = invocation.Method.Name;
                log_history.TableId = (int)tblName;
                log_history.IsActive = true;
                log_history.UserType = HttpContext.Current.Request.Cookies["role-id"].Value;
                var json = new JavaScriptSerializer().Serialize(entity);
                log_history.EntityJson = json;
              // log_history.j
                //  log_history.TableName = tblName.ToString();

                service.InsertLogHistory(log_history);
            }
            catch { }
        }


    }
}
