﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;

namespace NMS.MongoRepository
{
    public class MBunchFilterRepository : MRepository<MBunchFilter>, IMBunchFilterRepository
    {
        readonly NMSMongoContext Context;
        public MBunchFilterRepository()
            : base("BunchFilter")
        {
            Context = new NMSMongoContext();
        }
        public MBunchFilter GetBunchFilterCreateIfNotExist(MBunchFilter bunchFilter)
        {
            if (!CheckIfBunchFilterExists(bunchFilter))
            {
                base.Insert(bunchFilter);
                return base.GetById(bunchFilter._id);
            }
            return base.GetByQuery(Query<MBunchFilter>.EQ(x => x.FilterId, bunchFilter.FilterId)).FirstOrDefault();
        }

        public bool InsertBunchFilterNoReturn(MBunchFilter bunchFilter)
        {
            BsonValue id = base.Insert(bunchFilter);
            return true;
        }

        public bool InsertBunchFilter(MBunchFilter bunchFilter)
        {
            if (!CheckIfBunchFilterExists(bunchFilter))
            {
                BsonValue id = base.Insert(bunchFilter);
            }
            else
            {
                if (bunchFilter.NewsList != null && bunchFilter.NewsList.Count() > 0)
                {
                    var query1 = Query<MBunchFilter>.EQ(x => x.FilterId, bunchFilter.FilterId);
                    var query2 = Query<MBunchFilter>.EQ(x => x.BunchId, bunchFilter.BunchId);
                    var _bunchFilter = base.GetByQuery(Query.And(query1, query2)).FirstOrDefault();
                    _bunchFilter.NewsList.Add(bunchFilter.NewsList.First().GetAsUpdateNews());
                    _bunchFilter.LastUpdateDate = bunchFilter.NewsList.First().LastUpdateDate;

                    base.Update(Query<MBunchFilter>.EQ(x => x._id, _bunchFilter._id), Update<MBunchFilter>.Set(x => x.NewsList, _bunchFilter.NewsList).Combine(Update<MBunchFilter>.Set(x => x.LastUpdateDate, _bunchFilter.LastUpdateDate)));
                }
            }
            return true;
        }


        public bool DeleteOlderData(List<string> Bunches)
        {
            base.DeleteByQuery(Query<MBunchFilter>.In(x => x.BunchId, Bunches));
            return true;
        }

        public bool UpdateBunchFilterNewsList(MBunchFilter bunchFilter)
        {
            base.Update(Query<MBunchFilter>.EQ(x => x._id, bunchFilter._id), Update<MBunchFilter>.Set(x => x.NewsList, bunchFilter.NewsList).Combine(Update<MBunchFilter>.Set(x => x.LastUpdateDate, bunchFilter.LastUpdateDate)));
            return true;
        }

        public bool UpdateBunchGuidByOldBunchGuid(string OldBunchGuid, string BunchGuid)
        {
            base.Update(Query<MBunchFilter>.EQ(x => x.BunchId, OldBunchGuid), Update<MBunchFilter>.Set(x => x.BunchId, BunchGuid));
            return true;
        }

        public bool DeleteByBunchId(string bunchId)
        {
            base.DeleteByQuery(Query<MBunchFilter>.EQ(x => x.BunchId, new BaseEntity { _id = bunchId }._id));
            return true;
        }

        public bool CheckIfBunchFilterExists(MBunchFilter bunchFilter)
        {
            var query1 = Query<MBunchFilter>.EQ(x => x.FilterId, bunchFilter.FilterId);
            var query2 = Query<MBunchFilter>.EQ(x => x.BunchId, bunchFilter.BunchId);
            var count = base.GetCount(Query.And(query1, query2));
            if (count > 0)
                return true;
            return false;
        }

        public bool DeleteByBunchAndFilterId(MBunchFilter bunchFilter)
        {
            var query1 = Query<MBunchFilter>.EQ(x => x.FilterId, bunchFilter.FilterId);
            var query2 = Query<MBunchFilter>.EQ(x => x.BunchId, bunchFilter.BunchId);
            base.DeleteByQuery(Query.And(query1, query2));
            return true;

        }

        public List<MBunchFilter> GetAllBunches()
        {
            var query1 = Query<MBunchFilter>.LTE(x => x.FilterId, 999999);
            return base.GetByQuery(query1, Fields<MBunchFilter>.Include(x => x.BunchId));
        }

        public List<MBunchFilter> GetPagedBunchFilter(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }

        public List<MBunchFilter> GetByFilterIdAndBunchId(List<int> filterIds, string bunchId)
        {
            var query1 = Query<MBunchFilter>.In(x => x.FilterId, filterIds);
            var query2 = Query<MBunchFilter>.EQ(x => x.BunchId, new BaseEntity() { _id = bunchId }._id);

            MongoCursor<MBunchFilter> cursor = this.Collection.Find(Query.And(query1, query2));
            return cursor.ToList();
        }

        public List<MBunchFilter> GetByUpdatedDate(DateTime updatedDate)
        {
            var query1 = Query<MBunchFilter>.GT(x => x.LastUpdateDate, updatedDate);
            MongoCursor<MBunchFilter> cursor = this.Collection.Find(Query.And(query1));
            return cursor.ToList();
        }

        public List<MBunchFilter> GetByUpdatedDateForMigration(DateTime updatedDate)
        {
            var query1 = Query<MBunchFilter>.LTE(x => x.LastUpdateDate, updatedDate);
            return base.GetByQuery(query1);
        }

        public List<MBunchFilter> GteByBunchIds(List<string> Bunches)
        {
            var query1 = Query<MBunchFilter>.In(x => x.BunchId, Bunches);
            return base.GetByQuery(query1);
        }

        public List<MBunchFilter> GetByUpdatedDateTop1000(DateTime updatedDate)
        {
            var query1 = Query<MBunchFilter>.GT(x => x.LastUpdateDate, updatedDate);
            MongoCursor<MBunchFilter> cursor = this.Collection.Find(Query.And(query1));
            //cursor.OrderBy(x => x.LastUpdateDate);
            cursor.SetSortOrder(SortBy<MBunchFilter>.Ascending(x => x.LastUpdateDate));
            cursor.SetLimit(1000);
            return cursor.ToList();
        }

        public List<MBunchFilter> GetByBunchId(string bunchId)
        {
            var query1 = Query<MBunchFilter>.EQ(x => x.BunchId, new BaseEntity() { _id = bunchId }._id);

            MongoCursor<MBunchFilter> cursor = this.Collection.Find(query1);
            return cursor.ToList();
        }

        public void RemoveNewsFromBunchFilter(int filterId, string bunchId, string newsId)
        {
            var query = Query<MBunchFilter>.EQ(x => x.FilterId, filterId);
            query = Query.And(query, Query<MBunchFilter>.EQ(x => x.BunchId, new BaseEntity() { _id = bunchId }._id));
            MBunchFilter bunchFilter = base.GetByQuery(query).FirstOrDefault();
            if (bunchFilter != null)
            {
                bunchFilter.NewsList.RemoveAll(x => x._id == newsId);
                base.Update(Query<MBunchFilter>.EQ(x => x._id, bunchFilter._id), Update<MBunchFilter>.Set(x => x.NewsList, bunchFilter.NewsList).Combine(Update<MBunchFilter>.Set(x => x.LastUpdateDate, bunchFilter.LastUpdateDate)));
            }
        }


        public List<MBunchFilter> GetNewsByFilterIds(List<int> filterIds, int pageCount, int startIndex, DateTime from1, DateTime to1, string term)
        {
            var query1 = Query<MBunchFilter>.In(x => x.FilterId, filterIds);
            var query2 = Query<MBunchFilter>.GT(x => x.LastUpdateDate, from1);
            var query3 = Query<MBunchFilter>.LT(x => x.LastUpdateDate, to1);

            if (!string.IsNullOrEmpty(term))
                query3 = Query.And(query3, Query.ElemMatch("NewsList", Query.Or(Query.Matches("TranslatedTitle", new Regex(".*" + term + ".*", RegexOptions.IgnoreCase)), Query.Matches("Title", new Regex(".*" + term + ".*", RegexOptions.IgnoreCase)))));
            MongoCursor<MBunchFilter> cursor = this.Collection.Find(Query.And(query1, query2, query3));
            cursor.SetSortOrder(SortBy<MBunchFilter>.Descending(x => x.LastUpdateDate));
            cursor.SetSkip(startIndex);
            cursor.SetLimit(pageCount);
            return cursor.ToList();
        }

        public List<MBunchFilter> GetNewsByFilterIds(List<Filter> filters, int pageCount, int startIndex, DateTime from1, DateTime to1, string term, List<Filter> discaredFilterIds = null)
        {
            var query4 = Query<MBunchFilter>.GT(x => x.LastUpdateDate, from1);
            var query5 = Query<MBunchFilter>.LT(x => x.LastUpdateDate, to1);

            //if (filters.Where(x => x.FilterTypeId == (int)FilterTypes.AllNews || x.FilterTypeId == (int)FilterTypes.NewsFilter).Count() > 0)
            //    query5 = Query.Or(query5, Query<MBunchFilter>.In(x => x.FilterId, filters.Where(x => x.FilterTypeId == (int)FilterTypes.AllNews || x.FilterTypeId == (int)FilterTypes.NewsFilter).Select(x => x.FilterId).ToList()));
            //if (filters.Where(x => x.FilterTypeId == (int)FilterTypes.Category).Count() > 0)
            //    query5 = Query.Or(query5, Query<MBunchFilter>.In(x => x.FilterId, filters.Where(x => x.FilterTypeId == (int)FilterTypes.Category).Select(x => x.FilterId).ToList()));
            //if (filters.Where(x => x.FilterTypeId != (int)FilterTypes.AllNews && x.FilterTypeId != (int)FilterTypes.NewsFilter && x.FilterTypeId != (int)FilterTypes.Category).Count() > 0)
            //    query5 = Query.Or(query5, Query<MBunchFilter>.In(x => x.FilterId, filters.Where(x => x.FilterTypeId != (int)FilterTypes.AllNews && x.FilterTypeId != (int)FilterTypes.NewsFilter && x.FilterTypeId != (int)FilterTypes.Category).Select(x => x.FilterId).ToList()));

            if (filters.Count > 0)
                query5 = Query.And(query5, Query<MBunchFilter>.In(x => x.FilterId, filters.Select(x => x.FilterId).ToList()));

            if (discaredFilterIds != null)
            {
                query5 = Query.And(query5, Query<MBunchFilter>.NotIn(x => x.FilterId, discaredFilterIds.Select(x => x.FilterId).ToList()));
            }

            MongoCursor<MBunchFilter> cursor = this.Collection.Find(Query.And(query4, query5));
            cursor.SetSortOrder(SortBy<MBunchFilter>.Descending(x => x.LastUpdateDate));
            cursor.SetSkip(startIndex);
            cursor.SetLimit(pageCount);
            return cursor.ToList();
        }

        public List<MBunchFilter> GetByBunchIDs(List<string> BunchIDs)
        {
            var query = Query<MBunchFilter>.In(x => x.BunchId, BunchIDs);
            return base.GetByQuery(query).ToList();
        }



    }
}
