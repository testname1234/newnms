﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;


namespace NMS.MongoRepository
{
    public class MBunchNewsRepository : MRepository<MBunchNews>, IMBunchNewsRepository
    {
        readonly NMSMongoContext Context;
        public MBunchNewsRepository()
            : base("BunchNews")
        {
            Context = new NMSMongoContext();
        }
        public MBunchNews GetBunchNewsCreateIfNotExist(MBunchNews bunchNews)
        {
            if (!CheckIfBunchNewsExists(bunchNews))
            {
                base.Insert(bunchNews);
                return base.GetById(bunchNews._id);
            }
            return base.GetByQuery(Query<MBunchNews>.EQ(x => x.NewsId, bunchNews.NewsId)).FirstOrDefault();
        }

        public bool UpdateBunchGuidByOldBunchGuid(string OldBunchGuid, string BunchGuid)
        {
            base.Update(Query<MBunchNews>.EQ(x => x.BunchId, OldBunchGuid), Update<MBunchNews>.Set(x => x.BunchId, BunchGuid));
            return true;
        }

        public bool DeleteOlderData(List<string> Bunches)
        {
            base.DeleteByQuery(Query<MBunchNews>.In(x => x.BunchId, Bunches));
            return true;
        }

        public List<MBunchNews> GetBunchNewsByBunchId(string bunchId)
        {
            return base.GetByQuery(Query<MBunchNews>.EQ(x => x.BunchId, bunchId));
        }

        public bool InsertBunchNews(MBunchNews bunchNews)
        {
            if (!CheckIfBunchNewsExists(bunchNews))
            {
                BsonValue id = base.Insert(bunchNews);
            }
            return true;
        }

        public bool InsertBunchNewsNoReturn(MBunchNews bunchNews)
        {
            BsonValue id = base.Insert(bunchNews);
            return true;
        }

        public bool DeleteByBunchId(string bunchId)
        {
            base.DeleteByQuery(Query<MBunchNews>.EQ(x => x.BunchId, new BaseEntity { _id = bunchId }._id));
            return true;
        }

        public bool DeleteByBunchIdAndNewsId(string newsId, string bunchId)
        {
            var query = Query<MBunchNews>.EQ(x => x.NewsId, new BaseEntity { _id = newsId }._id);
            var query1 = Query<MBunchNews>.EQ(x => x.BunchId, new BaseEntity { _id = bunchId }._id);
            base.DeleteByQuery(Query.And(query, query1));
            return true;
        }

        public List<MBunchNews> GetByBunchIdAndNewsId(string newsId, string bunchId)
        {
            var query = Query<MBunchNews>.EQ(x => x.NewsId, new BaseEntity { _id = newsId }._id);
            var query1 = Query<MBunchNews>.EQ(x => x.BunchId, new BaseEntity { _id = bunchId }._id);
            return base.GetByQuery(Query.And(query, query1)); ;
        }

        public List<MBunchNews> GetByUpdatedDateForMigration(DateTime RemoveDate)
        {
            var query = Query<MBunchNews>.LTE(x => x.LastUpdateDate, RemoveDate);            
            return base.GetByQuery(query);
        }

        public List<MBunchNews> GetByBunchIds(List<string> Bunches)
        {
            var query = Query<MBunchNews>.In(x => x.BunchId, Bunches);
            return base.GetByQuery(query);
        }

        public bool CheckIfBunchNewsExists(MBunchNews bunchNews)
        {
            var query = Query<MBunchNews>.EQ(x => x.NewsId, bunchNews.NewsId);
            //var query2 = Query<MBunchNews>.EQ(x => x.BunchId, bunchNews.BunchId);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }

        public List<MBunchNews> GetPagedBunchNews(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }

        public List<MBunchCount> GetBunchNewsGroupByBunchId(DateTime LastUpdatDate)
        {

            List<MBunchCount> lstFilter = new List<MBunchCount>();

            var initial = new BsonDocument();
            initial.Add("Count", 0);

            var keyFunction = new GroupByDocument();
            keyFunction.Add("BunchId", 1);
            keyFunction.Add("LastUpdateDate", "1/1/1900");

            var reduce = @"function( curr, result) {
                     result.Count += 1;
                    if(curr.LastUpdateDate > result.LastUpdateDate )
                        result.LastUpdateDate =curr.LastUpdateDate;
    

                }";


            // filterIds = new List<int> { 78 };

            var queryclause1 = Query<MBunchNews>.GT(x => x.CreationDate, LastUpdatDate);
            var query = this.Collection.Find(Query.And(queryclause1)).SetFields(Fields<MBunchNews>.Include(x => x.BunchId)).Distinct<MBunchNews>();


            var fquery = Query<MBunchNews>.In(x => x.BunchId, query.Select(x => x.BunchId));

            //var bsonDocs = this.Collection.Group(Query<MNewsFilter>.In(x => x.NewsId, query.Select(x => x.NewsId)), keyFunction, initial, reduce, null).ToList();
            var bsonDocs = this.Collection.Group(fquery, keyFunction, initial, reduce, null).ToList();

            foreach (var item in bsonDocs)
            {
                MBunchCount objTemp = BsonSerializer.Deserialize<MBunchCount>(item);
                lstFilter.Add(objTemp);

            }

            return lstFilter.OrderByDescending(x => x.Count).ToList<MBunchCount>();



        }
    }
}
