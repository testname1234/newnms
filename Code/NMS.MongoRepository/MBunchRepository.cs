﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MBunchRepository : MRepository<MBunch>, IMBunchRepository
    {
        readonly NMSMongoContext Context;
        public MBunchRepository()
            : base("Bunch")
        {
            Context = new NMSMongoContext();
        }

        public MBunch InsertBunch(MBunch bunch)
        {
            if (!CheckIfBunchExists(bunch))
            {
                base.Insert(bunch);
            }
            return base.GetById(bunch._id);
        }

        public bool DeleteOlderData(List<string> BunchesToDelete)
        {
            base.DeleteByQuery(Query<MBunch>.In(x => x._id, BunchesToDelete));
            return true;
        }

        public MBunch InsertBunchNoCheck(MBunch bunch)
        {
            base.Insert(bunch);
            return base.GetById(bunch._id);
        }

        public List<MBunch> GetAllBunches()
        {
            var query1 = Query<MBunch>.LTE(x => x.NewsCount, 9999);
            return base.GetByQuery(query1, Fields<MBunch>.Include(x => x._id));
        }

        public List<MBunch> GetPagedBunch(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }

        public bool CheckIfBunchExists(MBunch bunch)
        {
            var query = Query<MBunch>.EQ(x => x._id, bunch._id);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }

        public bool DeleteByBunchId(string bunchId)
        {
            base.DeleteByQuery(Query<MBunch>.EQ(x => x._id, new BaseEntity { _id = bunchId }._id));
            return true;
        }

        public MBunch GetByGuid(string Guid)
        {
            var query = Query<MBunch>.EQ(x => x._id, Guid);
            return base.GetByQuery(query).FirstOrDefault();
        }


        public List<MBunch> GetByUpdatedDate(DateTime Updateddate)
        {
            var query = Query<MBunch>.GT(x => x.LastUpdateDate, Updateddate);
            return base.GetByQuery(query);
        }

        public List<MBunch> GetByUpdatedDateForMigration(DateTime Updateddate)
        {
            var query = Query<MBunch>.LTE(x => x.LastUpdateDate, Updateddate);
            return base.GetByQuery(query);
        }

        public void UpdateBunchNewsCount(string bunchId, int NewsCount)
        {
            var update1 = Update<MBunch>.Set(x => x.NewsCount, NewsCount);
            base.Update(Query<MNews>.EQ(x => x._id, bunchId), update1);
        }

        public List<MBunch> GetByIDs(List<string> bunchIds)
        {
            var query = Query<MBunch>.In(x => x._id, bunchIds);
            return base.GetByQuery(query).ToList();
        }

    }



}
