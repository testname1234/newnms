﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MBunchResourceRepository : MRepository<MBunchResource>, IMBunchResourceRepository
    {
        readonly NMSMongoContext Context;
        public MBunchResourceRepository()
            : base("BunchResource")
        { 
            Context= new NMSMongoContext();
        }
        public MBunchResource GetBunchResourceCreateIfNotExist(MBunchResource bunchResource)
        {
            if (!CheckIfBunchResourceExists(bunchResource))
            {
                base.Insert(bunchResource);
                return base.GetById(bunchResource._id);
            }
            return base.GetByQuery(Query<MBunchResource>.EQ(x => x.ResourceId, bunchResource.ResourceId)).FirstOrDefault();
        }

        public bool InsertBunchResource(MBunchResource bunchResource)
        {
            if (!CheckIfBunchResourceExists(bunchResource))
            {
                base.Insert(bunchResource);
            }
            return true;
        }

        public bool DeleteByBunchId(string bunchId)
        {
            base.DeleteByQuery(Query<MBunchResource>.EQ(x => x.BunchId, new BaseEntity { _id = bunchId }._id));
            return true;
        }

        public bool DeleteOlderData(List<string> Bunches)
        {
            base.DeleteByQuery(Query<MBunchResource>.In(x => x.BunchId, Bunches));
            return true;
        }

        public bool CheckIfBunchResourceExists(MBunchResource bunchResource)
        {
            var query = Query<MBunchResource>.EQ(x => x.ResourceId, bunchResource.ResourceId);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }

        public List<MBunchResource> GetPagedBunchResource(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }
    }
}
