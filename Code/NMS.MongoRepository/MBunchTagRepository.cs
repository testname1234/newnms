﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MBunchTagRepository : MRepository<MBunchTag>, IMBunchTagRepository
    {
        readonly NMSMongoContext Context;
        public MBunchTagRepository()
            : base("BunchTag")
        {
            Context = new NMSMongoContext();
        }
        public MBunchTag GetBunchTagCreateIfNotExist(MBunchTag bunchTag)
        {
            if (!CheckIfBunchTagExists(bunchTag))
            {
                base.Insert(bunchTag);
                return base.GetById(bunchTag._id);
            }
            var query1 = Query<MBunchTag>.EQ(x => x.TagId, bunchTag.TagId);
            var query2 = Query<MBunchTag>.EQ(x => x.BunchId, bunchTag.BunchId);
            return base.GetByQuery(Query.And(query1, query2)).FirstOrDefault();
        }

        public List<MBunchTag> GetByBunchId(string BunchId)
        {
            return base.GetByQuery(Query<MBunchTag>.EQ(x => x.BunchId, BunchId));
        }

        public bool InsertBunchTag(MBunchTag bunchTag)
        {
            if (!CheckIfBunchTagExists(bunchTag))
            {
                BsonValue id = base.Insert(bunchTag);
            }
            return true;
        }

        public bool DeleteOlderData(List<string> Bunches)
        {
            base.DeleteByQuery(Query<MBunchTag>.In(x => x.BunchId, Bunches));
            return true;
        }

        public bool InsertBunchTagNoReturn(MBunchTag bunchTag)
        {
            BsonValue id = base.Insert(bunchTag);
            return true;
        }

        public bool DeleteByBunchId(string bunchId)
        {
            base.DeleteByQuery(Query<MBunchTag>.EQ(x => x.BunchId, new BaseEntity { _id = bunchId }._id));
            return true;
        }

        public bool DeleteByTagId(string TagId)
        {
            base.DeleteByQuery(Query<MBunchTag>.EQ(x => x.TagId, new BaseEntity { _id = TagId }._id));
            return true;
        }


        public bool CheckIfBunchTagExists(MBunchTag bunchTag)
        {
            var query1 = Query<MBunchTag>.EQ(x => x.TagId, bunchTag.TagId);
            var query2 = Query<MBunchTag>.EQ(x => x.BunchId, bunchTag.BunchId);

            var count = base.GetCount(Query.And(query1, query2));
            if (count > 0)
                return true;
            return false;
        }

        public List<MBunchTag> GetPagedBunchTag(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }
    }
}
