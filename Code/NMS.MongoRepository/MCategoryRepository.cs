﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MCategoryRepository : MRepository<MCategory>, IMCategoryRepository
    {
        readonly NMSMongoContext Context;
        public MCategoryRepository()
            : base("Category")
        { 
            Context= new NMSMongoContext();
        }
        public MCategory GetCategoryCreateIfNotExist(MCategory category)
        {
            if (!CheckIfCategoryExists(category.Category))
            {
                base.Insert(category);
                return base.GetById(category._id);
            }
            return base.GetByQuery(Query<MCategory>.EQ(x => x.Category, category.Category)).FirstOrDefault();
        }

        public bool InsertCategory(MCategory category)
        {
            if (!CheckIfCategoryExists(category.Category))
            {
                BsonValue id = base.Insert(category);
            }
            return true;
        }


        public bool CheckIfCategoryExists(string name)
        {
            var query = Query<MCategory>.EQ(x => x.Category, name);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;

        }

        public List<MCategory> GetPagedCategory(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }
    }
}
