﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MCommentRepository : MRepository<MComment>, IMCommentRepository
    {
        readonly NMSMongoContext Context;
        public MCommentRepository()
            : base("Comment")
        {
            Context = new NMSMongoContext();
        }

        public bool InsertComment(MComment comment)
        {
            BsonValue id = base.Insert(comment);
            return true;
        }


        public List<MComment> GetCommentsByLastUpdate(DateTime lastUpdateDate, int? reporterId = null)
        {
            var query = Query<MComment>.GT(x => x.LastUpdateDate, lastUpdateDate);
            if (reporterId != null)
            {
                query = Query.And(query, Query<MComment>.EQ(x => x.ReporterId, reporterId));
            }
            return base.GetByQuery(query);
        }

        public List<MComment> GetCommentsByLastUpdateForMigration(DateTime lastUpdateDate)
        {
            var query = Query<MComment>.LTE(x => x.LastUpdateDate, lastUpdateDate);            
            return base.GetByQuery(query);
        }
    }
}
