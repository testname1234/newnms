﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MDescrepencyNewsRepository : MRepository<MDescrepencyNews>, IMDescrepencyNewsRepository
    {
        readonly NMSMongoContext Context;
        public MDescrepencyNewsRepository()
            : base("DescrepencyNews")
        {
            Context = new NMSMongoContext();
        }
        public new void Insert(MDescrepencyNews descrepencyNews)
        {
            if (!Exist(descrepencyNews))
                base.Insert(descrepencyNews);
        }

        public bool Exist(MDescrepencyNews descrepencyNews)
        {
            //    var query=Query<MDescrepencyNews>.EQ(x=>x.Title,descrepencyNews.Title);
            //    query = Query.And(Query<MDescrepencyNews>.EQ(x => x.Source, descrepencyNews.Source));

            var query1 = Query<MDescrepencyNews>.EQ(x => x.Title, descrepencyNews.Title);
            var query2 = Query<MDescrepencyNews>.EQ(x => x.Source, descrepencyNews.Source);
            var query = Query.And(query1, query2);
            return base.GetCount(query) > 0;
        }

        public List<MDescrepencyNews> GetPagedDescrepencyNews(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }

        public List<MDescrepencyNews> GetAllByDescrepancyType(int descrepencyType)
        {
            return base.GetByQuery(Query<MDescrepencyNews>.EQ(x => x.DescrepencyType, descrepencyType));
        }

        public List<MDescrepencyNews> GetDiscrepencyBystatus(int descrepencyType, int Status)
        {
            var query1 = Query<MDescrepencyNews>.EQ(x => x.DescrepencyType, descrepencyType);
            var query2 = Query<MDescrepencyNews>.EQ(x => x.DiscrepencyStatusCode, Status);
            return base.GetByQuery(Query.And(query1, query2));
        }

        public List<MDescrepencyNews> GetAllByDescrepancyType(int descrepencyType, string descrepancyValue)
        {

            var query1 = Query<MDescrepencyNews>.EQ(x => x.DescrepencyType, descrepencyType);
            var query2 = Query<MDescrepencyNews>.EQ(x => x.DescrepencyValue, descrepancyValue);
            return base.GetByQuery(Query.And(query1, query2));
        }

        public List<MDescrepencyNews> GetDescrepancyByTitleAndSource(string Title,string Source)
        {            
            var query1 = Query<MDescrepencyNews>.EQ(x => x.Title, Title);
            var query2 = Query<MDescrepencyNews>.EQ(x => x.Source, Source);
            return base.GetByQuery(Query.And(query1, query2));
        }


        public bool DeleteDescrepancyNews(string DescrepancyNewsID)
        {
            base.DeleteByQuery(Query<MDescrepencyNews>.EQ(x => x._id, new BaseEntity { _id = DescrepancyNewsID }._id));
            return true;
        }

        public void UpdateDescrepancyNewsByValue(int descrepencyType, string DescrepencyValue)
        {
            var query1 = Query<MDescrepencyNews>.EQ(x => x.DescrepencyType, descrepencyType);
            var query2 = Query<MDescrepencyNews>.EQ(x => x.DescrepencyValue, DescrepencyValue);
            var update1 = Update<MDescrepencyNews>.Set(x => x.DiscrepencyStatusCode, 1);
            base.Update(Query.And(query1, query2), update1);
        }

        public List<MDescrepencyNews> GetAllDescrepancyByStatus(int DescrepencyType)
        {
            var query1 = Query<MDescrepencyNews>.EQ(x => x.DescrepencyType, DescrepencyType);
            var query2 = Query<MDescrepencyNews>.NE(x => x.DiscrepencyStatusCode, 1);
            return base.GetByQuery(Query.And(query1, query2));
        }
    }
}
