﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MFilterCountRepository : MRepository<MFilterCount>, IMFilterCountRepository
    {
        readonly NMSMongoContext Context;
        public MFilterCountRepository()
            : base("FilterCount")
        { 
            Context= new NMSMongoContext();
        }

        public bool InsertOrUpdate(MFilterCount filterCount)
        {
            if (!CheckIfFilterCountExists(filterCount))
                base.Insert(filterCount);
            else
            {                
                var _filterCount = base.GetByQuery(Query<MFilterCount>.EQ(x => x.FilterId, filterCount.FilterId)).FirstOrDefault();
                _filterCount.Count = _filterCount.Count + filterCount.Count;
                _filterCount.LastUpdateDate = filterCount.LastUpdateDate;
                Update(_filterCount);
            }
            return true;
        }


        public bool InsertOrUpdate(List<MFilterCount> lstFilterCount)
        {
            foreach (MFilterCount item in lstFilterCount)
            {
                   InsertOrUpdate(item);
            }

            return true;
            
        }

        public bool Update(MFilterCount filterCount)
        {
            var update = Update<MFilterCount>.Set(x => x.Count, filterCount.Count);
            update.Combine(Update<MFilterCount>.Set(x => x.LastUpdateDate, filterCount.LastUpdateDate));
            base.Update(Query<MFilterCount>.EQ(x => x._id, new BaseEntity() { _id = filterCount._id }._id), update);
            return true;
        }

        public MFilterCount GetById(string Id)
        {
            return base.GetById(Id);
        }

        public bool CheckIfFilterCountExists(MFilterCount filterCount)
        {
            var query = Query<MFilterCount>.EQ(x => x.FilterId, filterCount.FilterId);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }


        public DateTime GetMaxLastUpdateDate()
        {
            var rec = base.GetMinMaxValue(SortBy<MFilterCount>.Descending(x => x.LastUpdateDate),Fields<MFilterCount>.Include(x=>x.LastUpdateDate));
            if (rec == null)
                return DateTime.UtcNow.AddYears(-100);
            else return rec.LastUpdateDate;
        }

        public DateTime GetMaxLastUpdateDate(int filterId)
        {
            var rec = base.GetMinMaxValue(SortBy<MFilterCount>.Descending(x => x.LastUpdateDate), Fields<MFilterCount>.Include(x => x.LastUpdateDate), Query<MFilterCount>.EQ(x => x.FilterId, filterId));
            if (rec == null)
                return DateTime.UtcNow.AddYears(-100);
            else return rec.LastUpdateDate;
        }

        public List<int> GetDistinctFilterIds()
        {
            return this.Collection.Distinct<int>("FilterId").ToList();
        }


        public List<MFilterCount> GetFilterCountByLastUpdateDate(DateTime lastUpdateDate)
        {
            return base.GetByQuery(Query<MFilterCount>.GT(x => x.LastUpdateDate, lastUpdateDate));
        }


        public List<MFilterCount> GetFilterCountByLastUpdateDate(List<int> filterIds, DateTime from, DateTime to, List<int> programFilterIds)
        {
            throw new NotImplementedException();
        }
        public List<MFilterCount> GetFilterCountByLastUpdateDate(List<Filter> filterIds, DateTime from, DateTime to, List<int> programFilterIds)
        {
            throw new NotImplementedException();
        }

        public void ProcessProducerDataPolling ()
        {
            throw new NotImplementedException();
        }
    }
}
