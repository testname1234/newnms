﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Enums;

namespace NMS.MongoRepository
{
    public class MFilterRepository : MRepository<MFilter>, IMFilterRepository
    {
        readonly NMSMongoContext Context;
        public MFilterRepository()
            : base("Filter")
        { 
            Context= new NMSMongoContext();
        }
        public MFilter GetFilterCreateIfNotExist(MFilter filter)
        {
            if (!CheckIfFilterExists(filter.Name, filter.FilterTypeId))
            {
                base.Insert(filter);
                return base.GetById(filter._id);
            }
            return base.GetByQuery(Query<MFilter>.EQ(x => x.Name, filter.Name)).FirstOrDefault();
        }

        public bool InsertFilter(MFilter filter)
        {
            if (!CheckIfFilterExists(filter.Name, filter.FilterTypeId))
            {
                BsonValue id = base.Insert(filter);
            }
            return true;
        }


        public MFilter GetFilterByFilterId(int filterId)
        {
            return base.GetByQuery(Query<MFilter>.EQ(x => x.FilterId, filterId)).FirstOrDefault();
        }

        public bool CheckIfFilterExists(string name, int filterTypeId)
        {
            var query1 = Query<MFilter>.EQ(x => x.Name, name);
            var query2 = Query<MFilter>.EQ(x => x.FilterTypeId, filterTypeId);
            var query = Query.And(query1, query2);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }

        public List<MFilter> GetPagedFilter(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }
    }
}
