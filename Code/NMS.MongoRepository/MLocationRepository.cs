﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MLocationRepository : MRepository<MLocation>, IMLocationRepository
    {
        readonly NMSMongoContext Context;
        public MLocationRepository()
            : base("Location")
        {
            Context = new NMSMongoContext();
        }

        public MLocation GetLocationCreateIfNotExist(MLocation location)
        {
            if (!CheckIfLocationExists(location.Location))
            {
                base.Insert(location);
                return base.GetById(location._id);
            }
            return base.GetByQuery(Query<MLocation>.EQ(x => x.Location, location.Location)).FirstOrDefault();
        }

        public bool InsertLocation(MLocation tag)
        {
            if (!CheckIfLocationExists(tag.Location))
            {
                BsonValue id = base.Insert(tag);
            }
            return true;
        }


        public bool CheckIfLocationExists(string name)
        {
            var query = Query<MLocation>.EQ(x => x.Location, name);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;

        }

        public List<MLocation> GetPagedLocation(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }
    }
}
