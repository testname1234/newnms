﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MMigrationRepository
    {
        readonly NMSMongoContext Context;
        public MMigrationRepository()
        {
            Context = new NMSMongoContext();
        }
        public List<T> GetEntitiesByDate<T>(DateTime dt, string columnName, string collectionName)
        {
            var query = Query.GT(columnName, dt);
            MongoCursor<T> cursor = Context.Database.GetCollection<T>(collectionName).Find(query);
            return cursor.ToList();
        }

        public List<T> GetEntitiesByDate<T>(DateTime dt, DateTime MaxDate, string columnName, string collectionName, int? RecordsLimit = null)
        {
            var query2 = Query.LTE(columnName, MaxDate);
            var query = Query.And(Query.GT(columnName, dt), query2);
            MongoCursor<T> cursor = null;
            if (RecordsLimit == null)
                cursor = Context.Database.GetCollection<T>(collectionName).Find(query);
            else
                cursor = Context.Database.GetCollection<T>(collectionName).Find(query).SetLimit(Convert.ToInt32(RecordsLimit));
            return cursor.ToList();
        }

        public List<T> GetEntitiesByDateAndIsCompleted<T>(DateTime dt, DateTime MaxDate, string columnName, bool isCompleted, string columnName1, string collectionName)
        {
            var query = Query.GT(columnName, dt);
            var query1 = Query.EQ(columnName1, isCompleted);
            var quer2 = Query.Or(query, query1);
            var quer3 = Query.And(Query.LTE(columnName, MaxDate), quer2);
            MongoCursor<T> cursor = Context.Database.GetCollection<T>(collectionName).Find(quer3);

            return cursor.ToList();
        }


        public List<T> GetEntitiesByBit<T>(bool MigrationStatus, string columnName, string collectionName)
        {
            var query = Query.EQ(columnName, MigrationStatus);
            MongoCursor<T> cursor = Context.Database.GetCollection<T>(collectionName).Find(query);
            return cursor.ToList();
        }

        public List<T> GetAllEntities<T>(string collectionName)
        {
            MongoCursor<T> cursor = Context.Database.GetCollection<T>(collectionName).FindAll();
            return cursor.ToList();
        }
    }
}
