﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.IService;

namespace NMS.MongoRepository
{
    public class MNewsFilterRepository : MRepository<MNewsFilter>, IMNewsFilterRepository
    {
        readonly NMSMongoContext Context;
        public MNewsFilterRepository()
            : base("NewsFilter")
        { 
            Context= new NMSMongoContext();
        }
        public MNewsFilter GetNewsFilterCreateIfNotExist(MNewsFilter newsFilter)
        {
            if (!CheckIfNewsFilterExists(newsFilter))
            {
                base.Insert(newsFilter);
                return base.GetById(newsFilter._id);
            }
            return base.GetByQuery(Query.And(Query<MNewsFilter>.EQ(x => x.NewsId, newsFilter.NewsId), Query<MNewsFilter>.EQ(x => x.FilterId, newsFilter.FilterId))).FirstOrDefault();
        }

        public bool InsertNewsFilter(MNewsFilter newsFilter)
        {
            if (!CheckIfNewsFilterExists(newsFilter))
            {
             base.Insert(newsFilter);
            }
            return true;
        }


        public bool DeleteOlderData(List<string> NewsStr)
        {
            base.DeleteByQuery(Query<MNewsFilter>.In(x => x.NewsId, NewsStr));
            return true;
        }

        public bool CheckIfNewsFilterExists(MNewsFilter newsFilter)
        {
            var query1 = Query<MNewsFilter>.EQ(x => x.NewsId, newsFilter.NewsId);
            var query2 = Query<MNewsFilter>.EQ(x => x.FilterId, newsFilter.FilterId);
            var query = Query.And(query1, query2);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }

        public bool DeleteNewsFilterByNewsAndFilterId(MNewsFilter newsFilter)
        {
            var query1 = Query<MNewsFilter>.EQ(x => x.NewsId, newsFilter.NewsId);
            var query2 = Query<MNewsFilter>.EQ(x => x.FilterId, newsFilter.FilterId);
            var query = Query.And(query1, query2);
            base.DeleteByQuery(query);
            return true;
           
        }


        public List<MNewsFilter> GetPagedNewsFilter(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }


        public List<MNewsFilter> GetUpdatedNewsFilters(DateTime lastUpdateDate, int? reporterId = null, List<string> newsIds = null,int? filterId=null)
        {
            var query = Query<MNewsFilter>.GT(x => x.CreationDate, lastUpdateDate);

            if (newsIds != null)
            {
                List<string> Ids = new List<string>();
                foreach (string str in newsIds)
                    Ids.Add(new BaseEntity() { _id = str }._id);

                query = Query.And(query, Query<MNewsFilter>.In(x => x.NewsId, Ids));
                return base.GetByQuery(query).ToList();
            }
            if (reporterId != null)
            {
                query = Query.And(query, Query<MNewsFilter>.EQ(x => x.ReporterId, reporterId));
            }
            if (filterId != null)
            {
                query = Query.And(query, Query<MNewsFilter>.EQ(x => x.FilterId, filterId));
            }
            return base.GetByQuery(query).ToList();
        }

        public List<MFilterCount> GroupByFilterId(DateTime from,DateTime to,List<int> catFilterIds,List<int> newsFilterIds)
        {

            List<MFilterCount> lstFilter = new List<MFilterCount>();
            var initial = new BsonDocument();
            initial.Add("Count", 0);

            //            var keyFunction = (BsonJavaScript)@"function(doc) { 
            //                                        return { 
            //                                            FilterId : doc.FilterId
            //                                        }; 
            //                                    }";

            var keyFunction = new GroupByDocument();
            keyFunction.Add("FilterId", 1);

            var reduce = @"function( curr, result) {
                     result.Count += 1;
                }";


           // filterIds = new List<int> { 78 };

            var queryclause1 = Query<MNewsFilter>.GT(x => x.CreationDate, from);
            var queryclause2 = Query<MNewsFilter>.LT(x => x.CreationDate, to);
            var queryclause3 = Query<MNewsFilter>.In(x => x.FilterId, catFilterIds);
            var queryclause4 = Query<MNewsFilter>.In(x => x.FilterId, newsFilterIds);
            var query = this.Collection.Find(Query.And(queryclause1, queryclause2, queryclause3, queryclause4)).SetFields(Fields<MNewsFilter>.Include(x => x.NewsId)).Distinct<MNewsFilter>();
            //var query = this.Collection.FindAll().SetFields(Fields<MNewsFilter>.Include(x => x.NewsId)).Distinct<MNewsFilter>();
            var bsonDocs = this.Collection.Group(Query<MNewsFilter>.In(x => x.NewsId, query.Select(x => x.NewsId)), keyFunction, initial, reduce, null).ToList();

            foreach (var item in bsonDocs)
                lstFilter.Add(BsonSerializer.Deserialize<MFilterCount>(item));


            return lstFilter;


        }

        public List<MFilterCount> GroupByFilterId(DateTime from, DateTime to, List<Filter> newsFilterIds,List<int> programFilterIds)
        {

            IFilterService objFilterService = IoC.Resolve<IFilterService>("FilterService");
            List<Filter> objAllFilters = objFilterService.GetFilterByFilterTypeId(13);

            List<int> catFilterIds = newsFilterIds.Where(x => x.FilterTypeId == 13).ToList<Filter>().Select(x => x.FilterId).ToList<int>();
            List<int> topMenuFilter = newsFilterIds.Where(x => x.FilterTypeId == 16).ToList<Filter>().Select(x => x.FilterId).ToList<int>();
            //List<int> catDBFilterIds = (from c in objAllFilters
            //                           where newsFilterIds.Any(p => p.FilterId == c.FilterId)
            //                           select c.FilterId).ToList<int>();

            List<int> catDBFilterIds = objAllFilters.Where(p => !newsFilterIds.Any(p2 => p2.FilterId == p.FilterId)).Select( x => x.FilterId).ToList<int>() ;

            List<MFilterCount> lstFilter = new List<MFilterCount>();
            var initial = new BsonDocument();
            initial.Add("Count", 0);


            var keyFunction = new GroupByDocument();
            keyFunction.Add("FilterId", 1);

            var reduce = @"function( curr, result) {
                     result.Count += 1;
                }";


            // filterIds = new List<int> { 78 };

            var queryclause1 = Query<MNewsFilter>.GT(x => x.CreationDate, from);
            var queryclause2 = Query<MNewsFilter>.LT(x => x.CreationDate, to);


            if (topMenuFilter.Count > 0)
            {
                if (programFilterIds != null && programFilterIds.Count > 0)
                    topMenuFilter.AddRange(programFilterIds);
                queryclause2 = Query.And(queryclause2, Query<MNewsFilter>.In(x => x.FilterId, topMenuFilter));
            }
            if (catFilterIds.Count > 0)
            {
                if (programFilterIds != null && programFilterIds.Count > 0)
                    catFilterIds.AddRange(programFilterIds);
                queryclause2 = Query.And(queryclause2, Query<MNewsFilter>.In(x => x.FilterId, catFilterIds));
            }

            var query = this.Collection.Find(Query.And(queryclause1, queryclause2)).SetFields(Fields<MNewsFilter>.Include(x => x.NewsId)).Distinct<MNewsFilter>();

      
            var fquery= Query<MNewsFilter>.In(x => x.NewsId, query.Select(x => x.NewsId));
            fquery = Query.And(fquery, queryclause1, Query<MNewsFilter>.LT(x => x.CreationDate, to));
            fquery = Query.Or(fquery, Query<MNewsFilter>.In(x => x.FilterId, catDBFilterIds));
            

            //var bsonDocs = this.Collection.Group(Query<MNewsFilter>.In(x => x.NewsId, query.Select(x => x.NewsId)), keyFunction, initial, reduce, null).ToList();
            var bsonDocs = this.Collection.Group(fquery, keyFunction, initial, reduce, null).ToList();

            foreach (var item in bsonDocs)
                lstFilter.Add(BsonSerializer.Deserialize<MFilterCount>(item));


            /////////// temporay testing logging here //////////

            List<Filter> lstAllFilters = objFilterService.GetAllFilter();

            List<MyCheckingEntity> objMyList = (from category in lstAllFilters
                                  join prod in lstFilter on category.FilterId equals prod.FilterId
                                  select new MyCheckingEntity { FilterId = category.FilterId, FilterTypeId = category.FilterTypeId, Count = prod.Count }).ToList<MyCheckingEntity>();

            //now group by 


            //List<MyCheckingEntityGroup>  objGroupResult = (from p in objMyList
            //              group p.FilterTypeId by p.Count into g
            //                                               select new MyCheckingEntityGroup { FilterTypeId = g.Key, Count = g.ToList() }).ToList < MyCheckingEntityGroup>();

            List<MyCheckingEntityGroup> objGroupResult = (from prod in objMyList
                                                          group prod by prod.FilterTypeId into grouping
                                                          select new MyCheckingEntityGroup
                                                          {
                                                              FilterTypeId = grouping.Key,
                                                              Count = grouping.Sum(p => p.Count)
                                                          }).ToList<MyCheckingEntityGroup>();

            //int sumofAllImages = dataSet.Where(x => x.AnchorId != -2).Sum(x => x.TotalImages) == 0 ? 1 : dataSet.Where(x => x.AnchorId != -2).Sum(x => x.TotalImages);



            ////////////////////////////////////////////////////////////


            return lstFilter;


        }
    }

    public class MyCheckingEntity
    {
        public int FilterId { get; set; }
        public int FilterTypeId { get; set; }
        public int Count { get; set; }
        

    }

    public class MyCheckingEntityGroup
    {
        public int FilterTypeId { get; set; }
        public int Count { get; set; }


    }
}
