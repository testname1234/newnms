﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NMS.MongoRepository
{
    public class MNewsRepository : MRepository<MNews>, IMNewsRepository
    {
        readonly NMSMongoContext Context;
        public MNewsRepository()
            : base("News")
        {
            Context = new NMSMongoContext();
        }
        public MNews GetNewsCreateIfNotExist(MNews news)
        {
            if (!CheckIfNewsExists(news))
            {
                base.Insert(news);
                return base.GetById(news._id);
            }
            return base.GetByQuery(Query<MNews>.EQ(x => x.Title, news.Title)).FirstOrDefault();
        }

        public bool InsertNews(MNews news)
        {
            if (!CheckIfNewsExists(news))
            {
                BsonValue id = base.Insert(news);
            }
            return true;
        }


        public bool CheckIfNewsExists(MNews news)
        {
            var query1 = Query<MNews>.EQ(x => x.Title, news.Title);
            //var query2 = Query<MNews>.EQ(x => x.PublishTime, news.PublishTime);
            var query3 = Query<MNews>.EQ(x => x.SourceTypeId, news.SourceTypeId);
            var query4 = Query<MNews>.EQ(x => x.SourceFilterId, news.SourceFilterId);
            var query = Query.And(query1, query3, query4);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }

        public bool DeleteNews(MNews news)
        {
            var query1 = Query<MNews>.EQ(x => x.Title, news.Title);
            var query3 = Query<MNews>.EQ(x => x.SourceTypeId, news.SourceTypeId);
            var query4 = Query<MNews>.EQ(x => x.SourceFilterId, news.SourceFilterId);
            var query = Query.And(query1, query3, query4);
            base.DeleteByQuery(query);
            return true;

        }

        public bool DeleteByNewsId(List<string> newsIds)
        {
            var query = Query<MNews>.In(x => x._id, newsIds);
            //var query1 = Query<MNews>.EQ(x => x._id, new BaseEntity() { _id = NewsId }._id);
            base.DeleteByQuery(query);
            return true;

        }

        public List<MNews> GetReportedNews(int reportedId, int pageCount, int startIndex)
        {
            var query1 = Query<MNews>.EQ(x => x.ReporterId, reportedId);
            var query2 = Query<MNews>.EQ(x => x.IsSearchable, true);
            return base.GetPagedItems(Query.And(query1, query2), pageCount, startIndex, GetShortNewsFields(), SortBy<MNews>.Descending(x => x.LastUpdateDate));
        }

        public List<MNews> GetAllBunches()
        {
            var query1 = Query<MNews>.LTE(x => x.ReporterId, 9999);
            return base.GetByQuery(query1, Fields<MNews>.Include(x => x.BunchGuid));
        }

        public bool BulkInsert(List<MNews> NewsList)
        {
            return base.BulkInsert(NewsList);
        }


        public List<MNews> GetAllBunchesByDate(DateTime date)
        {
            var query1 = Query<MNews>.LTE(x => x.LastUpdateDate, date);
            return base.GetByQuery(query1, Fields<MNews>.Include(x => x.BunchGuid));
        }

        public List<MNews> GetAllBunchesByDateAndFilters(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, List<int> discaredFilters)
        {
            try
            {

                var queries = new List<IMongoQuery>();

                foreach (int[] lst in GetFilterGroups(filters))
                {
                    queries.Add(Query.ElemMatch("Filters", Query<MFilter>.In(x => x.FilterId, lst)));
                }

                string discaredFilterscsv = String.Join(",", discaredFilters);
                var query3 = "{'Filters.FilterId': {$nin :[" + discaredFilterscsv + "]} ,'LastUpdateDate':{$gt:ISODate('" + from.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'),$lt:ISODate('" + to.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "')}}";
                BsonDocument query1 = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(query3);
                QueryDocument queryDoc = new QueryDocument(query1);
                IMongoQuery m = queryDoc;
                queries.Add(m);
                var query = Query.And(queries);
                MongoCursor<MNews> cursor = this.Collection.Find(query);
                cursor.SetSortOrder(SortBy<MNews>.Descending(x => x.LastUpdateDate));
                cursor.SetSkip(startIndex);
                cursor.SetLimit(pageCount);
                var result = cursor.ToList();
                foreach (var res in result)
                {
                    foreach (var filter in res.Filters)
                    {
                        filter.NewsId = res._id;
                    }
                }
                return result;

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<int[]> GetFilterGroups(List<Filter> filters)
        {
            List<int[]> groups = new List<int[]>();
            if (filters.Any(x => x.FilterTypeId == 13))
            {
                groups.Add(filters.Where(x => x.FilterTypeId == 13).Select(x => x.FilterId).ToArray());
            }
            if (filters.Any(x => x.FilterTypeId == 15 || x.FilterTypeId == 19 || x.FilterTypeId == 20 || x.FilterTypeId == 8))
            {
                groups.Add(filters.Where(x => x.FilterTypeId == 15 || x.FilterTypeId == 19 || x.FilterTypeId == 20 || x.FilterTypeId == 8).Select(x => x.FilterId).ToArray());
            }
            if (filters.Any(x => x.FilterTypeId != 13 && x.FilterTypeId != 15 && x.FilterTypeId != 19 && x.FilterTypeId != 20 && x.FilterTypeId != 18 && x.FilterTypeId != 8 && x.FilterTypeId != 16))
            {
                groups.Add(filters.Where(x => x.FilterTypeId != 13 && x.FilterTypeId != 15 && x.FilterTypeId != 19 && x.FilterTypeId != 20 && x.FilterTypeId != 18 && x.FilterTypeId != 8 && x.FilterTypeId != 16).Select(x => x.FilterId).ToArray());
            }
            else if (filters.Any(x => x.FilterTypeId == (int)FilterTypes.Program))
            {
                var lst = filters.Where(x => x.FilterTypeId == (int)FilterTypes.Program).Select(x => x.FilterId).ToList();
                if (filters.Any(x => x.FilterId == 78))
                    lst.Add(78);

                groups.Add(lst.ToArray());
            }
            else if (filters.Any(x => x.FilterId == 78))
            {
                groups.Add(new int[] { 78 });
            }
            return groups;
        }

        public List<MNews> GetAllNewsTags()
        {
            var query1 = Query<MNews>.LTE(x => x.ReporterId, 9999);
            return base.GetByQuery(query1, Fields<MNews>.Include(x => x.Tags));
        }

        public List<MNews> GetAllNewsForMIgration()
        {
            var query1 = Query<MNews>.LTE(x => x.ReporterId, 9999);
            return base.GetByQuery(query1,
                Fields<MNews>.Include(x => x.BunchGuid,
                                x => x.ParentNewsId,
                                x => x.ReferenceNewsId,
                                x => x.ThumbnailUrl,
                                x => x.IsVerified,
                                x => x.AddedToRundownCount,
                                x => x.OnAirCount,
                                x => x.OtherChannelExecutionCount,
                                x => x.NewsTickerCount));
        }

        public List<MNews> GetByDateForMigration(DateTime Date)
        {
            var query1 = Query<MNews>.LTE(x => x.LastUpdateDate, Date);
            return base.GetByQuery(query1);
        }

        public List<MNews> GetNewsWithoutResources()
        {
            var query1 = Query<MNews>.LTE(x => x.Resources.Count, 0);
            return base.GetByQuery(query1);
        }

        public List<MNews> GetByUpdatedDate(DateTime Updateddate)
        {
            var query = Query<MNews>.GT(x => x.LastUpdateDate, Updateddate);
            return base.GetPagedItems(query, GetSolarFields());
        }

        public List<MNews> GetByUpdatedDate2000(DateTime Updateddate, int StartIndex = 0, int RecordsCount = 100)
        {
            var query = Query<MNews>.GT(x => x.LastUpdateDate, Updateddate);
            return base.GetPagedItems(query, GetSolarFields(), 100, SortBy<MNews>.Ascending(x => x.LastUpdateDate));
        }

        public bool DeleteOlderData(List<string> Bunches)
        {
            base.DeleteByQuery(Query<MNews>.In(x => x.BunchGuid, Bunches));
            return true;
        }

        public List<MNews> GetByParentNewsId()
        {
            var query = Query<MNews>.NE(x => x.ReferenceNewsId, null);
            return base.GetByQuery(query);
        }

        public MNews GetNewsByID(string id)
        {
            return base.GetById(id);
        }

        public void UpdateNews(MNews news)
        {
            var update1 = Update<MNews>.Set(x => x.Updates, news.Updates);
            var update2 = Update<MNews>.Set(x => x.IsVerified, news.IsVerified);
            var update3 = Update<MNews>.Set(x => x.IsSearchable, news.IsSearchable);
            var update4 = Update<MNews>.Set(x => x.LinkNewsId, news.LinkNewsId);
            base.Update(Query<MNews>.EQ(x => x._id, news._id), update1.Combine(update2).Combine(update3).Combine(update4));
        }

       

        public void UpdateNewsStatistics(MNews news)
        {
            var update1 = Update<MNews>.Set(x => x.AddedToRundownCount, news.AddedToRundownCount);
            var update2 = Update<MNews>.Set(x => x.OnAirCount, news.OnAirCount);
            var update3 = Update<MNews>.Set(x => x.OtherChannelExecutionCount, news.OtherChannelExecutionCount);
            var update4 = Update<MNews>.Set(x => x.NewsStatistics, news.NewsStatistics);

            base.Update(Query<MNews>.EQ(x => x._id, news._id), update1.Combine(update2).Combine(update3).Combine(update4));
        }

        public void UpdateNewsTickerCount(MNews news)
        {
            var update1 = Update<MNews>.Set(x => x.NewsTickerCount, news.NewsTickerCount);
            base.Update(Query<MNews>.EQ(x => x._id, news._id), update1);
        }

        public void UpdateNewsResources(MNews news)
        {
            var update1 = Update<MNews>.Set(x => x.Resources, news.Resources);
            base.Update(Query<MNews>.EQ(x => x._id, news._id), update1);
        }

        public void UpdateNewsThumb(MNews news)
        {
            var update1 = Update<MNews>.Set(x => x.ThumbnailUrl, news.ThumbnailUrl);
            base.Update(Query<MNews>.EQ(x => x._id, news._id), update1);
        }

        public void UpdateFiltersWithDate(MNews news)
        {
            var update1 = Update<MNews>.Set(x => x.LastUpdateDate, news.LastUpdateDate);
            var update2 = Update<MNews>.Set(x => x.Filters, news.Filters);
            base.Update(Query<MNews>.EQ(x => x._id, news._id), update1.Combine(update2));
        }

        public void UpdateResourcesAndFilters(MNews news)
        {
            var update1 = Update<MNews>.Set(x => x.Resources, news.Resources);
            var update2 = Update<MNews>.Set(x => x.Filters, news.Filters);
            base.Update(Query<MNews>.EQ(x => x._id, news._id), update1.Combine(update2));
        }

        public void UpdateNewsBunch(string newsid, string bunchGuid)
        {
            var update1 = Update<MNews>.Set(x => x.BunchGuid, new BaseEntity() { _id = bunchGuid }._id);
            base.Update(Query<MNews>.EQ(x => x._id, new BaseEntity() { _id = newsid }._id), update1);
        }

        public void UpdateNewsBunchByNewsIds(List<string> newsids, string bunchGuid)
        {
            var update1 = Update<MNews>.Set(x => x.BunchGuid, new BaseEntity() { _id = bunchGuid }._id);
            base.Update(Query<MNews>.In(x => x._id, newsids), update1);
        }

        private IMongoFields GetShortNewsFields()
        {
            return Fields<MNews>.Include(x => x.Title,
                           x => x.ShortDescription,
                           x => x.Description,
                       x => x.IsVerified,
                       x => x.NewsTypeId,
                       x => x.ParentNewsId,
                       x => x.LastUpdateDate,
                       x => x.PublishTime,
                       x => x.LanguageCode,
                       x => x.BunchGuid,
                       x => x.ThumbnailUrl,
                       x => x.IsAired,
                       x => x.Resources,
                       x => x.Updates,
                       x => x.Filters,
                       x => x.Categories,
                       x => x.Comments,
                       x => x.NewsStatistics,
                       x => x.OnAirCount,
                       x => x.AddedToRundownCount,
                       x => x.OtherChannelExecutionCount,
                       x => x.NewsTickerCount,
                       x => x.Source,
                       x => x.SourceTypeId,
                       x => x.SourceFilterId,
                       x => x._id);
        }

        private IMongoFields GetSolarFields()
        {
            return Fields<MNews>.Include(x => x.Title,
                           x => x.Slug,
                           x => x.TranslatedSlug,
                           x => x.Description,
                           x => x.TranslatedTitle,
                           x => x.TranslatedDescription,
                           x => x.LastUpdateDate,
                           x => x.PublishTime,
                           x => x.LanguageCode,
                           x => x.Tags,
                           x => x.BunchGuid,
                           x => x.Filters,
                           x => x._id);
        }

        public List<MNews> GetNewsByIDs(List<string> newsIds, bool getFullNews = false)
        {
            var query = Query<MNews>.In(x => x._id, newsIds);
            query = Query.And(query, Query<MNews>.EQ(x => x.IsSearchable, true));
            if (!getFullNews)
                return base.GetByQuery(query, GetShortNewsFields()).ToList();
            else return base.GetByQuery(query).ToList();
        }


        public List<MNews> GetNewsByBunchId(string bunchId)
        {
            return base.GetByQuery(Query<MNews>.EQ(x => x.BunchGuid, new BaseEntity() { _id = bunchId }._id)).ToList();
        }

        public List<MNews> GetNewsByBunchId(string bunchId, List<Filter> filters)
        {

            var queries = new List<IMongoQuery>();
            foreach (int[] lst in GetFilterGroups(filters))
            {
                queries.Add(Query.ElemMatch("Filters", Query<MFilter>.In(x => x.FilterId, lst)));
            }

            queries.Add(Query<MNews>.EQ(x => x.BunchGuid, new BaseEntity() { _id = bunchId }._id));
            var query = Query.And(queries);
            var result = base.GetByQuery(query).ToList();
            foreach (var res in result)
            {
                foreach (var filter in res.Filters)
                {
                    filter.NewsId = res._id;
                }
            }
            return result;
        }


        public void UpdateThumb(string id, string resGuid)
        {
            base.Update(Query<MNews>.EQ(x => x._id, new BaseEntity() { _id = id }._id), Update<MNews>.Set(x => x.ThumbnailUrl, resGuid));
        }


        public List<MNewsFilter> MarkVerifyNews(string[] ids, MNewsFilter filter, bool isVerified)
        {
            List<MNewsFilter> newsFilterList = new List<MNewsFilter>();
            List<MNews> newsList = new List<MNews>();
            foreach (var id in ids)
            {
                MNews news = new MNews();
                news._id = id;
                newsList.Add(news);
            }
            List<MNews> mNews = base.GetByQuery(Query<MNews>.In(x => x._id, newsList.Select(x => x._id)), Fields<MNews>.Include(x => x.Filters, x => x._id, x => x.ReporterId));
            foreach (var news in mNews)
            {
                if (!news.Filters.Any(x => x.FilterId == filter.FilterId))
                {
                    MNewsFilter _filter = new MNewsFilter();
                    _filter.NewsId = news._id;
                    _filter.ReporterId = news.ReporterId;
                    _filter.CreationDate = filter.CreationDate;
                    _filter.FilterId = filter.FilterId;
                    newsFilterList.Add(_filter);
                    news.Filters.RemoveAll(x => x.FilterId == (int)NewsFilters.NotVerified || x.FilterId == (int)NewsFilters.Verified || x.FilterId == (int)NewsFilters.Rejected);
                    news.Filters.Add(_filter);
                    base.Update(Query<MNews>.EQ(x => x._id, news._id), Update<MNews>.Set(x => x.IsVerified, isVerified).Combine(Update<MNews>.Set(x => x.Filters, news.Filters)));
                }
            }
            return newsFilterList;
        }

        public List<MNews> GetNewsForSimilar(List<int> CategoryIDs, DateTime date)
        {
            var query = Query<MNews>.GT(x => x.PublishTime, date.AddDays(-3));
            query = Query.And(query, Query<MNews>.LT(x => x.PublishTime, date));
            query = Query.And(query, Query<MNews>.EQ(x => x.LanguageCode, "en"));
            query = Query.And(query, Query<MNews>.NE(x => x.BunchGuid, null));
            return base.GetByQuery(query).ToList();
        }

        public List<MNews> GetFullNewsbyDate(DateTime date)
        {
            var query = Query<MNews>.GT(x => x.LastUpdateDate, date);
            return base.GetByQuery(query).ToList();
        }

        public bool UpdateComment(MComment comment)
        {
            var news = base.GetById(comment.NewsId);
            if (news != null)
            {
                if (news.Comments == null)
                    news.Comments = new List<MComment>();
                comment.ReporterId = news.ReporterId;
                news.Comments.Add(comment);
                base.Update(Query<MNews>.EQ(x => x._id, news._id), Update<MNews>.Set(x => x.Comments, news.Comments));
                return true;
            }
            return false;
        }

        public List<MNews> GetNewsWithUpdatesAndRelatedById(string newsId, string bunchId)
        {
            var query1 = Query<MNews>.EQ(x => x.ParentNewsId, new BaseEntity() { _id = newsId }._id);
            var query2 = Query<MNews>.EQ(x => x._id, new BaseEntity() { _id = newsId }._id);
            List<MNews> news = base.GetByQuery(Query.Or(query1, query2));

            var query4 = Query<MNews>.EQ(x => x.BunchGuid, new BaseEntity() { _id = bunchId }._id);
            var query5 = Query<MNews>.NE(x => x._id, new BaseEntity() { _id = newsId }._id);
            var query6 = Query<MNews>.EQ(x => x.IsVerified, null);
            List<MNews> relatedNews = base.GetByQuery(Query.And(query4, query5, query6), GetShortNewsFields());
            if (relatedNews != null && relatedNews.Count > 0)
            {
                news.AddRange(relatedNews);
            }
            return news;
        }

        public List<MNews> GetNewsByTitle(string term)
        {
            var fields = Fields<MNews>.Include(x => x.Title,
                x => x.Description,
                x => x.Categories,
                x => x.Locations,
                x => x.PublishTime,
                x => x.Tags,
                x => x.ThumbnailUrl,
                x => x.Resources,
                x => x.ResourceEdit,
                x => x.Source,
                x => x.NewsTypeId,
                x => x.SourceTypeId,
                x => x.ReporterId,
                x => x.Filters,
                x => x.LanguageCode,
                           x => x._id);
            var query = Query<MNews>.Matches(x => x.Title, new Regex(".*" + term + ".*", RegexOptions.IgnoreCase));
            query = Query.Or(query, Query<MNews>.Matches(x => x.TranslatedTitle, new Regex(".*" + term + ".*", RegexOptions.IgnoreCase)));

            return base.GetByQuery(query, fields).Take(10).ToList();
            //List<MNews> cursor = GetPagedItems(query, PageSize, 0, null, null);
        }

        public List<MNews> GetNewsByFilterIds(List<Filter> filters, DateTime lastUpdatedDate, int PageSize)
        {
            var query4 = Query<MNews>.GT(x => x.LastUpdateDate, lastUpdatedDate);
            //var query5 = Query<MNews>.Where(x => x.Filters.Any(y => filters.Select(z => z.FilterId).Contains(y.FilterId)));            
            //var query5 = Query<MNews>.Where(x => x.Filters.Any(y => filters.Select(z => z.FilterId).Contains(y.FilterId)));            

            //MongoCursor<MNews> cursor = this.Collection.Find(Query.And(query4, query5));
            List<MNews> cursor = GetPagedItems(Query.And(query4), PageSize, 0, null, null);
            return cursor.ToList();
        }

        public List<MNews> GetByIDs(List<string> NewsIds)
        {
            var query = Query<MNews>.In(x => x._id, NewsIds);
            return base.GetByQuery(query).ToList();
        }

        public List<MNews> GetByBunchIDs(List<string> BunchIds)
        {
            var query = Query<MNews>.In(x => x.BunchGuid, BunchIds);
            return base.GetByQuery(query).ToList();
        }


        public List<MNews> GetByBunchGuidByBunchIDs(List<string> BunchIds)
        {
            var query = Query<MNews>.In(x => x.BunchGuid, BunchIds);
            return base.GetByQuery(query, Fields<MNews>.Include(x => x.BunchGuid)).ToList();
        }

        public List<MNews> GetByBunchIDsWithFilters(List<string> Bunches, List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, List<int> discaredFilters)
        {

            var queries = new List<IMongoQuery>();
            foreach (int[] lst in GetFilterGroups(filters))
            {
                queries.Add(Query.ElemMatch("Filters", Query<MFilter>.In(x => x.FilterId, lst)));
            }

            // var query1 = Query<MNews>.GT(x => x.LastUpdateDate, from);
            //  var query2 = Query<MNews>.LT(x => x.LastUpdateDate, to);

            string discaredFilterscsv = String.Join(",", discaredFilters);
            var query3 = "{'BunchGuid':{$in:[ObjectId('" + string.Join("'),ObjectId('", Bunches) + "')]}, 'Filters.FilterId': {$nin :[" + discaredFilterscsv + "]} ,'LastUpdateDate':{$gt:ISODate('" + from.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'),$lt:ISODate('" + to.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "')}}";
            BsonDocument query1 = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(query3);
            QueryDocument queryDoc = new QueryDocument(query1);
            IMongoQuery m = queryDoc;
            queries.Add(m);
            var query = Query.And(queries);
            MongoCursor<MNews> cursor = this.Collection.Find(query);
            // cursor.SetFields(Fields<MNews>.Include(x => x.BunchGuid)).ToList();
            cursor.SetSortOrder(SortBy<MNews>.Descending(x => x.LastUpdateDate));
            // cursor.SetSkip(startIndex);
            // cursor.SetLimit(pageCount);
            var result = cursor.ToList();
            foreach(var res in result)
            {
                foreach (var filter in res.Filters)
                {
                    filter.NewsId = res._id;
                }
            }
            return result;
        }

        public List<MFilterCount> GetNewsFilterCounts(List<Filter> filters, DateTime from, DateTime to)
        {
            try
            {
                IFilterService objFilterService = IoC.Resolve<IFilterService>("FilterService");
                IMFilterCountRepository filterCountRepository = IoC.Resolve<IMFilterCountRepository>("MFilterCountRepository");
                var queries = new List<IMongoQuery>();
                if (filters.Any(x => x.FilterTypeId == 13))
                {
                    queries.Add(Query.ElemMatch("Filters", Query<MFilter>.In(x => x.FilterId, filters.Where(x => x.FilterTypeId == 13).Select(x => x.FilterId).ToArray())));
                }
                
                List<MFilterCount> lstFilter = new List<MFilterCount>();
                var query3 = "{'LastUpdateDate':{$gt:ISODate('" + from.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'),$lt:ISODate('" + to.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "')}}";

                //Create main query and add above query

                List<string> StrQueryList = new List<string>();
                List<BsonDocument> LstBsonDocs = new List<BsonDocument>();

                foreach (IMongoQuery query in queries)
                {
                    //query3 += "," + query.ToBsonDocument().ToString();
                    StrQueryList.Add("{ $match:" + query.ToBsonDocument().ToString().Replace("\"", "'") + "}");
                }
                StrQueryList.Add("{ $match:" + query3 + "}");
                StrQueryList.Add("{ $group: { _id: '$_id', skills: { $addToSet : '$Filters.FilterId' }}}");
                StrQueryList.Add("{ $unwind :'$skills' }");
                StrQueryList.Add("{ $unwind :'$skills' }");
                StrQueryList.Add("{ $group : { _id : '$skills', count: { $sum : 1 }}}");
                foreach (string str in StrQueryList)
                {
                    LstBsonDocs.Add(MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(str));
                }
                List<IMongoQuery> lstMainquery = new List<IMongoQuery>();                                
                AggregateArgs args = new AggregateArgs();
                args.Pipeline = LstBsonDocs;
                List<BsonDocument> bsonDocs = this.Collection.Aggregate(args).ToList();

                foreach (var item in bsonDocs)
                {
                    lstFilter.Add(new MFilterCount() { FilterId = (int)item["_id"], Count = (int)item["count"] });
                }

                lstFilter.RemoveAll(x=>filters.Where(y=>y.FilterTypeId==13).Select(y=>y.FilterId).Contains(x.FilterId));
                var allNews = lstFilter.Where(x => x.FilterId == 78).FirstOrDefault();
                if (allNews != null)
                {
                    var myNewsCount = lstFilter.Where(x => x.FilterId == (int)NewsFilters.MyNews).FirstOrDefault();
                    if (myNewsCount != null)
                        allNews.Count += myNewsCount.Count;
                }
                if (filters.Any(x => x.FilterTypeId == 13))
                {
                    var data = filterCountRepository.GetFilterCountByLastUpdateDate(new List<Filter>() { new Filter() { FilterId = 78, FilterTypeId = 16 } }, from, to, filters.Where(x => x.FilterTypeId == (int)FilterTypes.Program).Select(x => x.FilterId).ToList());
                    if (data != null)
                    {
                        var categoryFilters = objFilterService.GetAllFilter().Where(y => y.FilterTypeId == 13);
                        var categoriesCounts = data.Where(x => categoryFilters.Select(y => y.FilterId).Contains(x.FilterId));
                        lstFilter.AddRange(categoriesCounts);
                    }
                }

                return lstFilter;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<MNews> GetByBunchIDsForMigration(List<string> BunchIds)
        {
            var query = Query<MNews>.In(x => x.BunchGuid, BunchIds);
            return base.GetByQuery(query, Fields<MNews>.Include(x => x.LastUpdateDate, x => x.BunchGuid));
        }


        public List<MNews> GetByBunchIDsForMigrationByDate(List<string> BunchIds, DateTime LastUpdateddate)
        {
            var query = Query<MNews>.In(x => x.BunchGuid, BunchIds);
            var query2 = Query<MNews>.GT(x => x.LastUpdateDate, LastUpdateddate);
            return base.GetByQuery(Query.And(query, query2), Fields<MNews>.Include(x => x.BunchGuid));
        }

        public List<MNews> GetAllNewsShortFields()
        {
            var query = Query<MNews>.LT(x => x.ReporterId, 99999);
            return base.GetByQuery(query, Fields<MNews>.Include(x => x.Filters, x => x.Resources));
        }

        public List<MNews> GetReportedVideoNews()
        {
            var query3 = "{'Filters.FilterId':{$in:[759],$in:[41]}}";
            BsonDocument query = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(query3);
            QueryDocument queryDoc = new QueryDocument(query);
            IMongoQuery m = queryDoc;
            MongoCursor<MNews> cursor = this.Collection.Find(m);
            cursor.SetFields(Fields<MNews>.Include(x => x.Resources));
            return cursor.ToList();
        }

        public bool UpdateFitlers(MNews news)
        {
            base.Update(Query<MNews>.EQ(x => x._id, news._id), Update<MNews>.Set(x => x.Filters, news.Filters));
            return true;
        }
    }
}
