﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MRepository<T>
    {
        private string _collectionName;
        readonly NMSMongoContext Context;
        public MRepository(string collectionName)
        {
            _collectionName = collectionName;
            Context = new NMSMongoContext();
        }

        public  MongoCollection<T> Collection
        {
            get
            {
                return Context.Database.GetCollection<T>(_collectionName);
            }
        }

        public BsonValue Insert(T item)
        {
            WriteConcernResult result = this.Collection.Insert<T>(item);
            return result.Upserted;
        }

        public T GetById(BsonValue _idValue)
        {
            var query = Query<BaseEntity>.EQ(x => x._id, new BaseEntity() { _id = _idValue.AsString }._id);
            return this.Collection.FindOne(query);
        }
      
        public List<T> GetByQuery(IMongoQuery query, IMongoFields fields = null)
        {
            MongoCursor<T> cursor = this.Collection.Find(query);
            if (fields == null)
                return cursor.ToList();
            else
            {
                return cursor.SetFields(fields).ToList();
            }
        }

        public bool BulkInsert(List<T> items)
        {
            BulkWriteOperation bulk = this.Collection.InitializeUnorderedBulkOperation();
            foreach (T type in items)
            {
                bulk.Insert(type);
            }
            bulk.Execute();
            return true;
        }

        //public bool BulkUpdate(List<T> items)
        //{
        //    BulkWriteOperation bulk = this.Collection.InitializeUnorderedBulkOperation();
        //    foreach (T type in items)
        //    {
        //        bulk.Insert(type);
        //    }
        //    bulk.Execute();
        //    return true;
        //}

        public void DeleteByQuery(IMongoQuery query)
        {
            this.Collection.Remove(query);
        }
        
        public T GetMinMaxValue(IMongoSortBy sortBy,IMongoFields fields,IMongoQuery query=null)
        {
            MongoCursor<T> cursor = (query == null) ? this.Collection.FindAll() : this.Collection.Find(query);
            cursor.SetFields(fields);
            cursor.SetLimit(1);
            cursor.SetSortOrder(sortBy);
            return cursor.ToList().FirstOrDefault();
        }

        public void Update(IMongoQuery query,IMongoUpdate update)
        {            
            this.Collection.Update(query, update,UpdateFlags.Multi);
        }

        public long GetCount(IMongoQuery query)
        {
            return this.Collection.Find(query).Count();
        }

        public List<T> GetPagedItems(IMongoQuery query,int pageCount, int startIndex,IMongoFields fields,IMongoSortBy sortBy)
        {
            MongoCursor<T> cursor = query == null ? this.Collection.FindAll() : this.Collection.Find(query);
            cursor.SetSkip(startIndex);
            cursor.SetLimit(pageCount);
            if(sortBy!=null)
                cursor.SetSortOrder(sortBy);
            if(fields!=null)
                cursor.SetFields(fields);
            return cursor.ToList();
        }

        public List<T> GetPagedItems(IMongoQuery query, IMongoFields fields, int RecordsLimit = 0, IMongoSortBy sortBy=null)
        {
            MongoCursor<T> cursor = query == null ? this.Collection.FindAll() : this.Collection.Find(query);
            if (fields != null)
                cursor.SetFields(fields);
            if (RecordsLimit != 0)
            {
                cursor.SetSortOrder(sortBy);
                cursor.SetLimit(RecordsLimit);                
            }
            return cursor.ToList();
        }
    }
}
