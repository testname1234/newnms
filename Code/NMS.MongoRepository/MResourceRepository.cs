﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MResourceRepository : MRepository<MResource>, IMResourceRepository
    {
        readonly NMSMongoContext Context;
        public MResourceRepository()
            : base("Resource")
        {
            Context = new NMSMongoContext();
        }
        public MResource GetResourceCreateIfNotExist(MResource resource)
        {
            if (!CheckIfResourceExists(resource.Guid))
            {
                base.Insert(resource);
                return base.GetById(resource._id);
            }
            return base.GetByQuery(Query<MResource>.EQ(x => x.Guid, resource.Guid)).FirstOrDefault();
        }

        public bool InsertResource(MResource resource)
        {
            if (!CheckIfResourceExists(resource.Guid))
            {
                BsonValue id = base.Insert(resource);
            }
            return true;
        }

        public bool DeleteOlderData(DateTime dt)
        {
            base.DeleteByQuery(Query<MResource>.LTE(x => x.LastUpdateDate, dt));
            return true;
        }

        public void UpdateResourceDuration(MResource resource)
        {
            var update1 = Update<MResource>.Set(x => x.Duration, resource.Duration);
            var update2 = Update<MResource>.Set(x => x.LastUpdateDate, resource.LastUpdateDate);
            base.Update(Query<MResource>.EQ(x => x.Guid, resource.Guid), update1.Combine(update2));
        }


        public bool CheckIfResourceExists(string guid)
        {
            var query = Query<MResource>.EQ(x => x.Guid, guid);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;

        }

        public List<MResource> GetPagedResource(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }

        public List<MResource> GetResourcesByDate(DateTime resourcedate)
        {
            var query = Query<MResource>.LTE(x => x.LastUpdateDate, resourcedate);
            return base.GetByQuery(query);
        }

        public List<T> GetEntitiesByDate<T>(DateTime dt, string columnName, string collectionName)
        {
            var query = Query.GT(columnName, dt);
            NMSMongoContext Context = new NMSMongoContext();
            MongoCursor<T> cursor = Context.Database.GetCollection<T>(collectionName).Find(query);
            return cursor.ToList();
        }

        public MResource GetById(string id)
        {
            return base.GetById(id);
        }

        public MResource GetByGuid(string Guid)
        {
            var query = Query<MResource>.EQ(x => x.Guid, Guid);
            return base.GetByQuery(query).FirstOrDefault();
        }

        public bool DeleteByResourceGuid(string resouceGuid)
        {
            base.DeleteByQuery(Query<MResource>.EQ(x => x.Guid, new BaseEntity { _id = resouceGuid }._id));
            return true;
        }
    }
}
