﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MScrapMaxDatesRepository : MRepository<MScrapMaxDates>, IMScrapMaxDatesRepository
    {
        readonly NMSMongoContext Context;
        public MScrapMaxDatesRepository()
            : base("ScrapMaxDates")
        { 
            Context= new NMSMongoContext();
        }

        public MScrapMaxDates GetBySourceName(string sourceName)
        {
            // base.Insert(scrapMaxDates);
            //return base.GetBId(scrapMaxDates._id).FirstOrDefault();
            return base.GetByQuery(Query<MScrapMaxDates>.EQ(x => x.SourceName, sourceName)).FirstOrDefault();
        }

        public MScrapMaxDates GetScrapDateCreateIfNotExist(MScrapMaxDates scrapMaxDates)
        {
            if (!CheckIfscrapMaxDateExists(scrapMaxDates))
            {                
                base.Insert(scrapMaxDates);
                return base.GetById(scrapMaxDates._id);
            }
            return base.GetByQuery(Query<MScrapMaxDates>.EQ(x => x.SourceName, scrapMaxDates.SourceName)).FirstOrDefault();
        }

        public void UpdateScrapDate(MScrapMaxDates scrapMaxDates)
        {
                base.Update(Query<MScrapMaxDates>.EQ(x => x._id, scrapMaxDates._id), Update<MScrapMaxDates>.Set(x => x.MaxUpdateDate, scrapMaxDates.MaxUpdateDate));
         
        }

        public bool CheckIfscrapMaxDateExists(MScrapMaxDates scrapMaxDates)
        {
            var query = Query<MScrapMaxDates>.EQ(x => x.SourceName, scrapMaxDates.SourceName);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;
        }


    }
}
