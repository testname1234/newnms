﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MTagResourceRepository : MRepository<MTagResource>, IMTagResourceRepository
    {
        readonly NMSMongoContext Context;
        public MTagResourceRepository()
            : base("TagResource")
        {
            Context = new NMSMongoContext();
        }
        public void CreateIfNotExist(MTagResource tagresource)
        {
            if (!CheckIfTagResourceExists(tagresource.TagId))
            {
                base.Insert(tagresource);
                //return base.GetById(tagresource._id);
            }
            else
            {
                if (tagresource.Resources != null && tagresource.Resources.Count > 0)
                {
                    var tagResource = base.GetByQuery(Query<MTagResource>.EQ(x => x.TagId, tagresource.TagId)).FirstOrDefault();
                    List<string> sourceGuids = tagResource.Resources.Select(x => x.Guid).ToList();
                    List<MResource> newRsources = null;
                    if (sourceGuids != null && sourceGuids.Count > 0)
                    {
                        newRsources = tagresource.Resources.Where(x => !sourceGuids.Contains(x.Guid)).ToList();
                    }
                    else
                    {
                        newRsources = tagresource.Resources;
                    }
                    if (newRsources != null && newRsources.Count > 0)
                    {
                        tagResource.Resources.AddRange(newRsources);
                        UpdateTagResource(tagResource);
                    }

                }
            }
            //return base.GetByQuery(Query<MTagResource>.EQ(x => x._id, tagresource._id)).FirstOrDefault();
        }

        public bool DeleteOlderData(DateTime dt)
        {
            base.DeleteByQuery(Query<MTagResource>.LTE(x => x.LastUpdateDate, dt));
            return true;
        }

        public bool InsertTagResource(MTagResource tagresource)
        {
            if (!CheckIfTagResourceExists(tagresource.TagId))
            {
                base.Insert(tagresource);
            }
            return true;
        }

        public void UpdateTagResource(MTagResource tagresource)
        {
            base.Update(Query<MTagResource>.EQ(x => x.TagId, tagresource.TagId), Update<MTagResource>.Set(x => x.Resources, tagresource.Resources));
        }

        public bool CheckIfTagResourceExists(string tagid)
        {
            var query1 = Query<MTagResource>.EQ(x => x.TagId, new BaseEntity { _id = tagid }._id);
            var count = base.GetCount(query1);
            if (count > 0)
                return true;
            else
            {
                return false;
            }

        }

        public bool DeleteByTagGuid(string tagGuid)
        {
            base.DeleteByQuery(Query<MTagResource>.EQ(x => x.TagId, new BaseEntity { _id = tagGuid }._id));
            return true;
        }
    }
}
