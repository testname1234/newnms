﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;

namespace NMS.MongoRepository
{
    public class MTagRepository : MRepository<MTag>, IMTagRepository
    {
        readonly NMSMongoContext Context;
        public MTagRepository()
            : base("Tag")
        {
            Context = new NMSMongoContext();
        }
        public MTag GetTagCreateIfNotExist(MTag tag)
        {
            if (!CheckIfTagExists(tag.Tag))
            {
                base.Insert(tag);
                return base.GetById(tag._id);
            }
            return base.GetByQuery(Query<MTag>.EQ(x => x.Tag, tag.Tag)).FirstOrDefault();
        }

        public MTag GetTagCreateIfNotExistOptimized(MTag tag)
        {
            MTag selTag = base.GetByQuery(Query<MTag>.EQ(x => x.Tag, tag.Tag)).FirstOrDefault();
            if (selTag == null)
            {
                base.Insert(tag);
                selTag = base.GetById(tag._id);
            }
            return selTag;
        }

        public bool DeleteOlderData(DateTime dt)
        {
            base.DeleteByQuery(Query<MTag>.LTE(x => x.LastUpdateDate, dt));
            return true;
        }

        public bool InsertTag(MTag tag)
        {
            if (!CheckIfTagExists(tag.Tag))
            {
                BsonValue id = base.Insert(tag);
            }
            return true;
        }


        public bool CheckIfTagExists(string name)
        {
            var query = Query<MTag>.EQ(x => x.Tag, name);
            var count = base.GetCount(query);
            if (count > 0)
                return true;
            return false;

        }

        public bool DeleteByTagGuid(string tagGuid)
        {
            base.DeleteByQuery(Query<MBunchTag>.EQ(x => x._id, new BaseEntity { _id = tagGuid }._id));
            return true;
        }

        public List<MTag> GetPagedTag(int pageCount, int startIndex)
        {
            return base.GetPagedItems(null, 1, 1, null, null);
        }


        public List<MTag> GetTagByTerm(string term)
        {
            return base.GetByQuery(Query<MTag>.Matches(x => x.Tag, new Regex("^" + term + "*"))).ToList();
        }
    }
}
