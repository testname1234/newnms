﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Repository;
using NMS.Service;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using ScrapingProject.Model;
using ScrappingLib;
using SelaniumLib;

namespace NMS.ProcessThreads
{
    public class ArchivalMigration : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        SqlConnection connArchival = null;
        SqlConnection connSource = null;

        public string Execute(string argument)
        {
            DateTime RemovalDate = new DateTime();
            if (!DateTime.TryParse(argument, out RemovalDate))
                RemovalDate = DateTime.Now.AddDays(-30);

            MigrationService mService = new MigrationService();
            INewsService newsService = IoC.Resolve<INewsService>("NewsService");

            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");

            try
            {
                //int MigrateCount = 0;
                int MigrateCount = mService.CompleteMongotoSql(RemovalDate);
                //mService.RemoveGarbage();

                logService.InsertSystemEventLog(string.Format("NMS Archival Thread, News Migrated: {0}", MigrateCount), "", EventCodes.Log);
                return "Success";
            }
            catch (Exception ex)
            {
                logService.InsertSystemEventLog(string.Format("NMS Archival Thread, Error Occured: {0}", ex.Message), ex.StackTrace, EventCodes.Error);
                return "Error";
            }

            //if (Convert.ToInt32(argument) == 1)//NMS to Archive NMS
            //{
            //    //connArchival = new SqlConnection(ConfigurationManager.ConnectionStrings["connSqlArchive"].ToString());
            //    //connSource = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
            //    //First Here should be mongo code
            //    //newsService.IndexNews();    //Make sure all bunch are updated                
            //     //Migrate all missing data from Mongo to Sql                
            //   // mService.DeleteCollectionDate(new DateTime(2015,1,1));
            //    //mService.DeleteQanews();
            //   // mService.CompleteSqltoSql(connArchival, connSource);    //Migrating from sql to sql

            //}
            //else if (Convert.ToInt32(argument) == 2) // Archive NMS to NMS
            //{
            //    connSource = new SqlConnection(ConfigurationManager.ConnectionStrings["connSqlArchive"].ToString());
            //    connArchival = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
            //    mService.CompleteSqltoSql(connArchival, connSource);
            //}
            //else if (Convert.ToInt32(argument) == 3) // Mongo To NMS
            //{
            //    //connSource = new SqlConnection(ConfigurationManager.ConnectionStrings["connSqlArchive"].ToString());
            //    // connArchival = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
            //    // CompleteSqltoSql();
            //}
            //else if (Convert.ToInt32(argument) == 3) // NMS To MOngo
            //{
            //    //  connSource = new SqlConnection(ConfigurationManager.ConnectionStrings["connSqlArchive"].ToString());
            //    // connArchival = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
            //    // CompleteSqltoSql();
            //}

        }

    }
}
