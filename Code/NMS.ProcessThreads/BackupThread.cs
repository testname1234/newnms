﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.ProcessThreads
{
    public class BackupThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            try
            {
                bool backupSql = false, backupMongo = false;
                if (!string.IsNullOrEmpty(argument))
                {
                    backupMongo = (argument == "mongo" || argument == "both") ? true : false;
                    backupSql = (argument == "sql" || argument == "both") ? true : false;
                }
                else
                {
                    backupSql = true;
                    backupMongo = true;
                }
                Console.WriteLine("Backup Sql {0}, Backup Mongo {1}", backupSql, backupMongo);
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");

                if (backupMongo)
                {
                    if (NMS.Core.Helper.DatabaseHelper.BackUpMongoDatabase("10.3.12.113", "27017", @"\\10.3.12.113\mongodumb\backup"))
                    {
                        logService.InsertSystemEventLog("BackupThread: Mongo backup done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: Mongo backup done");

                        if (File.Exists(@"\\10.3.12.113\mongodumb\mongodb.zip"))
                            File.Delete(@"\\10.3.12.113\mongodumb\mongodb.zip");
                        System.IO.Compression.ZipFile.CreateFromDirectory(@"\\10.3.12.113\mongodumb\backup", @"\\10.3.12.113\mongodumb\mongodb.zip");
                        logService.InsertSystemEventLog("BackupThread: Mongo backup compression done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: Mongo backup compression done");

                        File.Copy(@"\\10.3.12.113\mongodumb\mongodb.zip", @"\\10.3.12.119\DBBackup\nmsMONGO-" + DateTime.UtcNow.ToString("dd, MMM yyyy hh-mm-ss") + ".zip");
                        logService.InsertSystemEventLog("BackupThread: mongo copy done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: mongo copy done");

                        Directory.Delete(@"\\10.3.12.113\mongodumb\backup", true);
                        File.Delete(@"\\10.3.12.113\mongodumb\mongodb.zip");
                        logService.InsertSystemEventLog("BackupThread: mongo delete done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: mongo delete done");
                    }
                }

                if (backupSql)
                {
                    if (!Directory.Exists(@"\\10.3.12.123\DB Backup\nmssqlbackup"))
                        Directory.CreateDirectory(@"\\10.3.12.123\DB Backup\nmssqlbackup");
                    if (NMS.Core.Helper.DatabaseHelper.BackupDatabase("nms", @"C:\DB backup\nmssqlbackup\nms.bak", ConfigurationManager.ConnectionStrings["conn"].ToString()))
                    {
                        logService.InsertSystemEventLog("BackupThread: Sql backup done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: Sql backup done");

                        if (File.Exists(@"\\10.3.12.123\DB Backup\nmsdb.zip"))
                            File.Delete(@"\\10.3.12.123\DB Backup\nmsdb.zip");
                        System.IO.Compression.ZipFile.CreateFromDirectory(@"\\10.3.12.123\DB Backup\nmssqlbackup", @"\\10.3.12.123\DB Backup\nmsdb.zip");
                        logService.InsertSystemEventLog("BackupThread: Sql backup compression done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: Sql backup compression done");

                        File.Copy(@"\\10.3.12.123\DB Backup\nmsdb.zip", @"\\10.3.12.119\DBBackup\nmsSQL-" + DateTime.UtcNow.ToString("dd, MMM yyyy hh-mm-ss") + ".zip");
                        logService.InsertSystemEventLog("BackupThread: sql copy done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: sql copy done");

                        File.Delete(@"\\10.3.12.123\DB Backup\nmssqlbackup\nms.bak");
                        File.Delete(@"\\10.3.12.123\DB Backup\nmsdb.zip");
                        logService.InsertSystemEventLog("BackupThread: sql delete done", "", EventCodes.Log);
                        Console.WriteLine("BackupThread: sql delete done");
                    }
                }
                return "Successfull";
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in BackupThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
