﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using FFMPEGLib;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Repository;

namespace NMS.ProcessThreads
{
    public class ChannelVideoCreationThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            logService.InsertSystemEventLog("ChannelvideoCreation", "ChannelvideoCreation Thread Started", EventCodes.Log);
            try
            {
                INewsService newsServ = IoC.Resolve<INewsService>("NewsService");
                INewsResourceEditService newsResServ = IoC.Resolve<INewsResourceEditService>("NewsResourceEditService");
                IChannelVideoService channServ = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
                List <NewsResourceEdit> newsResourceEdits = newsResServ.GetActiveNewsResourceEdit();
                foreach(var resEdit in newsResourceEdits)
                {
                    if(!string.IsNullOrEmpty(resEdit.Url) && resEdit.ResourceGuid.HasValue && resEdit.Left.HasValue && resEdit.Right.HasValue && resEdit.Right != resEdit.Left)
                    {
                        Core.Models.ResourceEdit resourceEdit = new ResourceEdit();
                        resourceEdit.ResourceGuid = resEdit.ResourceGuid.Value;
                        resourceEdit.FromTos = new List<FromTo>();
                        resourceEdit.FromTos.Add(new FromTo() { From = resEdit.Left.Value, To = resEdit.Right.Value });
                        resourceEdit.Id = resEdit.NewsResourceEditId;
                        resourceEdit.ResourceTypeId = resEdit.ResourceTypeId.Value;
                        resourceEdit.Url = resEdit.Url;
                        resourceEdit.ChannelVideoId = resourceEdit.ChannelVideoId;

                        EditResourceVideo(resourceEdit);
                        channServ.MarkIsProcessed(resourceEdit.ChannelVideoId);
                        newsResServ.SetInactive(resEdit.NewsResourceEditId);
                        logService.InsertSystemEventLog(string.Format("ChannelVideoCreationThread: Generated video against GUID {0}", resourceEdit.ResourceGuid.ToString() + " --- "), "", EventCodes.Log);
                    }
                }
                return "Successfull";
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ChannelVideoCreationThread: {0}", exp.Message + " --- " + exp.StackTrace.ToString()), "", EventCodes.Error);
                return "Error";
            }
        }



        public void EditResourceVideo(ResourceEdit obj)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                string LiveVideoPort = ":" + ConfigurationManager.AppSettings["LiveVideoPort"];
                NMS.Core.IService.IResourceService _iResourceService = IoC.Resolve<NMS.Core.IService.IResourceService>("ResourceService");
                var physicalPath = obj.Url.Replace("http:", "").Replace("/", @"\");
                if (physicalPath.Contains(".ts"))
                {
                    physicalPath = physicalPath.Replace(LiveVideoPort, "");
                }

                CustomSnapshotInput input = new CustomSnapshotInput();
                input.Id = obj.ResourceGuid.ToString();
                input.ResourceTypeId = obj.ResourceTypeId;
                input.TimeSpan = obj.FromTos.First().From;
                input.FilePath = physicalPath;//obj.Url;
                _iResourceService.GenerateCustomSnapshotOnMediaServer(input);

                string _storageRoot = ConfigurationManager.AppSettings["mediaresourcesTemp"];
                List<string> files = new List<string>();
                foreach (var fromTos in obj.FromTos)
                {
                    string filename = _storageRoot + Guid.NewGuid().ToString() + ".mp4";
                    FFMPEGLib.FFMPEG.ClipVideo(physicalPath, TimeSpan.FromSeconds(fromTos.From), TimeSpan.FromSeconds(fromTos.To), filename);
                    files.Add(filename);
                }

                if (files.Count > 1)
                {
                    FFMPEGLib.FFMPEG.ConcatenateVideos(files, _storageRoot + obj.ResourceGuid.ToString() + ".mp4");
                    foreach (string file in files)
                    {
                        File.Delete(file);
                    }
                }
                else
                {
                    if(File.Exists(_storageRoot + obj.ResourceGuid.ToString() + ".mp4"))
                    {
                        File.Delete(_storageRoot + obj.ResourceGuid.ToString() + ".mp4");
                    }
                    File.Move(files[0], _storageRoot + obj.ResourceGuid.ToString() + ".mp4");
                }
                _iResourceService.UpdateResourceStatus(obj.ResourceGuid, 1);
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ChannelVideoCreationThread: {0}", exp.Message + " --- " + exp.StackTrace.ToString()), "", EventCodes.Error);
            }
        }
    }

  
}
