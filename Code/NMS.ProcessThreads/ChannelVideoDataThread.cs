﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using FFMPEGLib;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Repository;

namespace NMS.ProcessThreads
{
    public class ChannelVideoDataThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            
            
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            logService.InsertSystemEventLog("Channelvideodata", "ChannelVideo Data Thread Started", EventCodes.Log);
            
            try
            {
                INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                HttpWebRequestHelper helper = new HttpWebRequestHelper();

                string tempVideoConvertor = ConfigurationManager.AppSettings["TempContentLocation"].ToString();
                IChannelVideoService channnelVideoService = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
                string VideoDataAPi = ConfigurationManager.AppSettings["videoMatchingHost"] + "reelmonitoring/GetReelMonitoring";//?dt=2014-08-1
                List<int> channelIDs = new List<int>();
                ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                ScrapMaxDate.SourceName = "ChannelDataThread";
                ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-30);
                DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);
                DateTime dt = dtMaxScrapDate;
                string date = dt.ToString("yyyy-MM-dd");
                var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<ChannelVideoOutput>>>(VideoDataAPi + "?dt=" + date + "&ChannelID=0", null);

                if (output.IsSuccess && output.Data != null)
                {
                    List<ChannelVideoOutput> lstChannelData = new List<ChannelVideoOutput>();
                    lstChannelData = output.Data;
                    foreach (ChannelVideoOutput channelData in lstChannelData)
                    {
                        //
                        if (!string.IsNullOrEmpty(channelData.VideoPath))
                        {
                            ChannelVideo channelVideo = new ChannelVideo();
                            channelVideo.CopyFrom(channelData);

                            if(channelIDs.Contains(channelVideo.ChannelId.Value))
                            {

                            }
                            else
                            {
                                channelIDs.Add(channelVideo.ChannelId.Value);
                            }

                            FileInfo fileinfo = new FileInfo(channelData.PhysicalPath);
                            string tmpfilename = tempVideoConvertor + fileinfo.Name;
                            string Convertedfilename = channelData.PhysicalPath; //tempVideoConvertor + fileinfo.Name.Replace(".flv", ".mp4");
                            if (channelData.PhysicalPath.IndexOf(".mp4") > -1 && File.Exists(channelData.PhysicalPath))
                            {
                                ChannelVideoRepository cvRepo = new ChannelVideoRepository();
                                var alreadyExist = cvRepo.GetChannelVideoByProgramAndStartEnd(channelVideo.From.Value, channelVideo.To.Value, channelVideo.ProgramId.Value, channelVideo.ChannelId.Value);
                                if(alreadyExist == null)
                                {
                                    channelVideo.PhysicalPath = Convertedfilename;
                                    channelVideo.Url = "http:" + Convertedfilename.Replace(@"\", "/");

                                    channelVideo.CreationDate = DateTime.UtcNow;
                                    channelVideo.LastUpdateDate = channelVideo.CreationDate;
                                    channelVideo.IsActive = true;


                                    cvRepo.InsertChannelVideo(channelVideo);
                                    logService.InsertSystemEventLog(string.Format("ChannelVideoThread: Created channelvideo of channelid : " + channelVideo.ChannelId + " --- "), "", EventCodes.Log);
                                }
                                else
                                {

                                }
                            }
                            
                            ScrapMaxDate.MaxUpdateDate = DateTime.Parse(channelData.EndTimeStr);
                            newsService.UpdateScrapDate(ScrapMaxDate);
                        }


                                        
                    }
                    logService.InsertSystemEventLog(string.Format("ChannelVideoThread: Comepleted importing channelvideos --- "), "", EventCodes.Log);

                    foreach (var chan in channelIDs)
                    {
                        SyncPrograms(chan);
                    }

                    SyncChannels(channelIDs);

                    logService.InsertSystemEventLog(string.Format("ChannelVideoThread: Comepleted all its task"), "", EventCodes.Log);
                }
                else
                {
                }
                
                return "Successfull";
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ChannelVideoThread: {0}", exp.Message + " --- " + exp.StackTrace.ToString()), "", EventCodes.Error);
                return "Error";
            }
        }


        private void SyncPrograms(int channelId)
        {
            IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            MMS.Integration.FPC.FPCAPI api = new MMS.Integration.FPC.FPCAPI();
            DateTime maxDate = DateTime.Now.AddYears(-3);//programService.GetMaxLastUpdateDateByChannelId(channel.ChannelId, channel.ConnectionString);
            var dt = api.GetProgramByLastUpdateDate(maxDate, channelId);


            if (dt.IsSuccess && dt.Data != null)
            {
                foreach (var _program in dt.Data.OrderBy(x => x.LastUpdateDate).ToList())
                {
                    var alreadyExisting = programService.GetProgram(_program.ProgramChannelMappingId);
                    if (alreadyExisting == null)
                    {
                        NMS.Core.Entities.Program program = new NMS.Core.Entities.Program();
                        program.ChannelId = _program.ChannelId;
                        //program.ImagePath = _program.ThumbnailVirtualPath;
                        program.IsActive = true;
                        program.ProgramId = _program.ProgramChannelMappingId;
                        program.Name = _program.Name.Trim();
                        program.CreationDate = _program.CreationDate.Value;
                        program.LastUpdateDate = _program.LastUpdateDate.Value;
                        programService.InsertProgram(program);
                        logService.InsertSystemEventLog(string.Format("ChannelVideoThread: Created program : " + program.Name), "", EventCodes.Log);
                    }
                }
            }
        }

        private void SyncChannels(List<int> channelIds)
        {
            IChannelService channelService = IoC.Resolve<IChannelService>("ChannelService");
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            MMS.Integration.FPC.FPCAPI api = new MMS.Integration.FPC.FPCAPI();
            var dt = api.GetChannels();


            if (dt.IsSuccess && dt.Data != null)
            {
                var toInsertChannels = dt.Data.Where(x => channelIds.Contains(x.ChannelId)).ToList();
                foreach (var _channel in toInsertChannels)
                {
                    var alreadyExisting = channelService.GetChannel(_channel.ChannelId);
                    if (alreadyExisting == null)
                    {
                        NMS.Core.Entities.Channel channel = new NMS.Core.Entities.Channel();
                        channel.ChannelId = _channel.ChannelId;
                        //program.ImagePath = _program.ThumbnailVirtualPath;
                        channel.IsActive = true;
                        channel.ChannelId = _channel.ChannelId;
                        channel.Name = _channel.ChannelName.Trim();
                        channel.IsUpdated = false;
                        channel.IsOtherChannel = true;
                        channel.CreationDate = DateTime.UtcNow;
                        channel.LastUpdateDate = channel.CreationDate;
                        channelService.InsertChannel(channel);
                        logService.InsertSystemEventLog(string.Format("ChannelVideoThread: Created Channel : " + channel.Name), "", EventCodes.Log);
                    }
                }
            }
        }
    }
}


//List<ChannelVideo> tmpChannelVideo = channnelVideoService.GetAllChannelVideosBetweenDates(Convert.ToDateTime(channelVideo.From), Convert.ToDateTime(channelVideo.To));
//WebClient client = new WebClient();
//string filePath = channelVideo.PhysicalPath;
//if (File.Exists(filePath))
//{
//    FileInfo fileinfo = new FileInfo(filePath);
//    string tmpfilename = tempVideoConvertor + fileinfo.Name;
//    string Convertedfilename = tempVideoConvertor + fileinfo.Name.Replace(".flv", ".mp4");
//    if (!File.Exists(tmpfilename))
//    {
//        File.Copy(filePath, tmpfilename);
//        FFMPEGLib.FFMPEG.ConvertToMp4(tmpfilename, Convertedfilename);

//        MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
//        ScrapResource srapresource = new ScrapResource();
//        resource.ResourceStatusId = ((int)ResourceStatuses.Completed).ToString();
//        resource.Source = Convertedfilename;
//        if (Regex.Match(resource.Source, ".*(mp4|flv)", RegexOptions.IgnoreCase).Success)
//        {
//            resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
//        }
//        else if (Regex.Match(resource.Source, ".*(mp3|wav)", RegexOptions.IgnoreCase).Success)
//        {
//            resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Audio).ToString();
//        }
//        else
//        {
//            resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
//        }
//        resource.IsFromExternalSource = false;
//        srapresource.Source = resource.Source;
//        srapresource.StatusId = Convert.ToInt32(resource.ResourceStatusId);
//        srapresource.DownloadedFilePath = resource.Source;

//        HttpWebResponse response = helper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", resource, null);
//        using (TextReader treader = new StreamReader(response.GetResponseStream()))
//        {
//            string str = treader.ReadToEnd();
//            MS.Core.Entities.Resource res = JsonConvert.DeserializeObject<MS.Core.Entities.Resource>(str);
//            //ResourceGuid.Add(res.Guid.ToString());
//            srapresource.ResourceGuid = res.Guid;
//        }
//        response.Close();

//        // Upload Images

//        string ext = System.IO.Path.GetExtension(srapresource.Source).Trim('.');
//        string fileName = System.IO.Path.GetFileName(srapresource.Source);
//        byte[] data = File.ReadAllBytes(srapresource.DownloadedFilePath);// client.DownloadData(res.DownloadedFilePath);
//        client.Dispose();
//        helper.HttpUploadFile(NMS.Core.AppSettings.MediaServerUrl + "/PostMedia?Id=" + srapresource.ResourceGuid.ToString() + "&IsHD=true", fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());


//        if (tmpChannelVideo == null)
//        {
//            ScrapMaxDate.MaxUpdateDate = channelData.CreationDate;
//            channelVideo.Url = ConfigurationManager.AppSettings["MediaServerIp"].ToString() + "/getresource/" + srapresource.ResourceGuid;
//            channnelVideoService.InsertChannelVideo(channelVideo);
//        }

//    }
//}




//

//string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
//string ffmpegPath = arg.Context.Server.MapPath("~/ffmpeg/");
//FFMPEGLib.FFMPEG.InitialPath = ffmpegPath;
//List<string> files = new List<string>();

//string filename = _storageRoot + Guid.NewGuid().ToString() + ".mp4";
//FFMPEGLib.FFMPEG.ClipVideo(channelVideo.PhysicalPath, TimeSpan.FromSeconds(fromTos.From), TimeSpan.FromSeconds(fromTos.To), filename);
//files.Add(filename);  