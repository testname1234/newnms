﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Threading;
using System.Xml.Serialization;
using System.Net;
using NMS.Core.Helper;
using NMS.Repository;
using System.Data;
using NMS.Core.Entities;

namespace NMS.ProcessThreads
{
    public class ChecklistThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }


        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string message)
        {
            DataSet ds = CreateDataSet();
            return "successfull";
        }

        //Main DataSet Containing Checklist
        private DataSet CreateDataSet()
        {
            DataSet dataset = new DataSet();

            // Creating The NMSChecklist Table
            DataTable NMSChecklists = CreateNMSChecklistTable();
            dataset.Tables.Add(NMSChecklists);
            return dataset;
        }

        // Global Error Variables
        string servererrormsg;
        string interneterrormsg;
        string windowsserviceerrormessage;
        string googleliveerrormsg;
        string bingliveerrormsg;
        string googleqaerrormsg;
        string bingqaerrormsg;
        string servererrormsgnms;
        string googlemediaerrormsg;

        //DataTable Containing All Functions
        public DataTable CreateNMSChecklistTable()
        {
            DataTable NMSChecklists = new DataTable("NMSChecklist");

            // Adding Columns
            AddNewColumn(NMSChecklists, "System.Int32", "ChecklistID");
            AddNewColumn(NMSChecklists, "System.String", "Name");
            AddNewColumn(NMSChecklists, "System.Boolean", "Status");
            AddNewColumn(NMSChecklists, "System.String", "Issue");
            AddNewColumn(NMSChecklists, "System.DateTime", "CreationDate");
            AddNewColumn(NMSChecklists, "System.Boolean", "Priority");

            // API KEYS
            string serverhosturl = ConfigurationManager.AppSettings["serverhosturl"].ToString();
            string scrapdatahosturl = ConfigurationManager.AppSettings["scrapdatahosturl"].ToString();
            string hosturl = ConfigurationManager.AppSettings["hosturl"].ToString();
            string hosturlQANMS = ConfigurationManager.AppSettings["hosturlQANMS"].ToString();
            string hosturlLiveNMS = ConfigurationManager.AppSettings["hosturlLiveNMS"].ToString();
            string GoogleMedia = ConfigurationManager.AppSettings["GoogleMedia"].ToString();

            bool flag = AmazonServerConnectivity(serverhosturl);
            bool NMSControlPanelflag = AmazonControlPanel(hosturl);
            bool intrnt = InternetConnectionCheck();
            bool obj = WindowsServiceCheck(hosturl);
            KeyValuePair<bool, string> FinalScrapRes = GetScrappingData(scrapdatahosturl);
            bool scrapmed = GoogleMediaCheck(GoogleMedia);


            // From Google Translation Live Server
            string ResultTestString = "pakistan";
            string obj1 = "";
            bool GoogleResult = false;
            bool googlres = GoogleTranslationLiveCheck(GoogleResult, obj1, ResultTestString, hosturlLiveNMS);


            // From Google Translation QA Server  
            bool GoogleResultQA = false;
            bool googlqa = GoogleTranslationQACheck(GoogleResultQA, obj1, ResultTestString, hosturlQANMS);

            // From Bing Translation Live Server
            string ResultTestStringg = "pakistan";
            string obj2 = "";
            bool BingResult = false;
            bool bngliv = BingTranslationLiveCheck(BingResult, obj2, ResultTestStringg, hosturlLiveNMS);

            // From Bing Translation QA Server  
            bool BingResultQA = false;
            bool bngqa = BingTranslationQACheck(BingResultQA, obj2, ResultTestStringg, hosturlQANMS);


            string iserror;
            int i = 0;

            int priorty = 0;

            ScrapMaxDatesRepository Scrdt = new ScrapMaxDatesRepository();

            DataTable Scrapdt = Scrdt.GetResult();
            foreach (DataRow row in Scrapdt.Rows)
            {
                if (row["Status"].ToString() == "false")
                {
                    iserror = "Old Record";
                    priorty = 1;
                }
                else
                {
                    iserror = "";
                    priorty = 0;
                }
                AddNewRow(NMSChecklists, ++i, Convert.ToString(row["Source"]), Convert.ToString(row["Status"]), iserror, Convert.ToDateTime(row["sourcedate"]), 0);
            }

            // Adding Rows
            AddNewRow(NMSChecklists, ++i, "NMS Control Panel", Convert.ToString(NMSControlPanelflag), servererrormsgnms, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "Amazon Server Connnectivity", Convert.ToString(flag), servererrormsg, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "Amazon Server Internet", Convert.ToString(intrnt), interneterrormsg, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "Amazon Server HotSpot", Convert.ToString(obj), windowsserviceerrormessage, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "Live Bing Translation", Convert.ToString(bngliv), bingliveerrormsg, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "Live Google Translation", Convert.ToString(googlres), googleliveerrormsg, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "QA Bing Translation", Convert.ToString(bngqa), bingqaerrormsg, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "QA Google Translation", Convert.ToString(googlqa), googleqaerrormsg, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "Scrapping Data", Convert.ToString(FinalScrapRes.Key), FinalScrapRes.Value, DateTime.Now, 0);
            AddNewRow(NMSChecklists, ++i, "Google Media", Convert.ToString(scrapmed), googlemediaerrormsg, DateTime.Now, 0);

            return NMSChecklists;
        }

        //For Google Media Download
        private bool GoogleMediaCheck(string GoogleMedia)
        {
            bool scrapmed = false;
            string urll = string.Format(GoogleMedia + "GoogleMediaCheck");
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(urll);
            WebResponse newresponse = null;

            try
            {
                newresponse = httpWebRequest.GetResponse();
                using (Stream stream = newresponse.GetResponseStream())
                {
                    string output = new StreamReader(stream).ReadToEnd();
                    scrapmed = Newtonsoft.Json.JsonConvert.DeserializeObject<bool>(output);
                    if (scrapmed == false)
                    {
                        googlemediaerrormsg = "Error in downloading";
                    }
                }
            }
            catch (Exception ex)
            {
                scrapmed = false;
                googlemediaerrormsg = ex.Message;
            }

            return scrapmed;
        }

        //For Scrap txt Data
        private KeyValuePair<bool, string> GetScrappingData(string scrapdatahosturl)
        {
            List<KeyValuePair<bool, string>> FinalScrap = new List<KeyValuePair<bool, string>>();
            KeyValuePair<bool, string> FinalScrapRes = new KeyValuePair<bool, string>();
            string uri = string.Format(scrapdatahosturl + "ScrappingData/GetScrappingData");
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            WebResponse newresponse = null;

            try
            {
                newresponse = httpWebRequest.GetResponse();
                using (Stream stream = newresponse.GetResponseStream())
                {
                    string output = new StreamReader(stream).ReadToEnd();
                    FinalScrap = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<bool, string>>>(output);
                    if (FinalScrap != null && FinalScrap.Count > 0)
                    {
                        FinalScrap = FinalScrap.Where(x => !x.Key).ToList();
                        if (FinalScrap != null && FinalScrap.Count > 0)
                        {
                            string ReturnVal = String.Join(",", FinalScrap.Select(x => x.Value));
                            FinalScrapRes = new KeyValuePair<bool, string>(false, ReturnVal);
                        }
                        else
                        {
                            FinalScrapRes = new KeyValuePair<bool, string>(true, "Success");
                        }
                    }
                    else
                    {
                        FinalScrapRes = new KeyValuePair<bool, string>(false, "Could Not Connect to Server");
                    }
                }
            }
            catch (Exception ex)
            {
                FinalScrapRes = new KeyValuePair<bool, string>(false, ex.Message);
            }
            return FinalScrapRes;
        }

        //For Google Translation Live
        public bool GoogleTranslationLiveCheck(bool GoogleResult, string obj1, string ResultTestString, string hosturlLiveNMS)
        {
            string urii = string.Format(hosturlLiveNMS + "TranslateGoogle?message=پاکستان&fromLanguage=ur&toLanguage=en");
            HttpWebRequest httpWebRequestt = (HttpWebRequest)WebRequest.Create(urii);
            WebResponse newresponsee = null;
            try
            {
                newresponsee = httpWebRequestt.GetResponse();
                using (Stream stream = newresponsee.GetResponseStream())
                {
                    string outputt = new StreamReader(stream).ReadToEnd();
                    obj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(outputt);
                    if (obj1.ToLower() == ResultTestString)
                        GoogleResult = true;
                }
            }
            catch (Exception ex)
            {
                GoogleResult = false;
                googleliveerrormsg = ex.Message;
            }
            return GoogleResult;
        }

        //For Google Translation QA
        public bool GoogleTranslationQACheck(bool GoogleResultQA, string obj1, string ResultTestString, string hosturlQANMS)
        {
            string urii = string.Format(hosturlQANMS + "TranslateGoogle?message=پاکستان&fromLanguage=ur&toLanguage=en");
            HttpWebRequest httpWebRequestt = (HttpWebRequest)WebRequest.Create(urii);
            WebResponse newresponsee = null;
            try
            {
                newresponsee = httpWebRequestt.GetResponse();
                using (Stream stream = newresponsee.GetResponseStream())
                {
                    string outputt = new StreamReader(stream).ReadToEnd();
                    obj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(outputt);
                    if (obj1.ToLower() == ResultTestString)
                        GoogleResultQA = true;
                }
            }
            catch (Exception ex)
            {
                GoogleResultQA = false;
                googleqaerrormsg = ex.Message;
            }
            return GoogleResultQA;
        }

        //For Bing Translation Live
        public bool BingTranslationLiveCheck(bool BingResult, string obj2, string ResultTestStringg, string hosturlLiveNMS)
        {
            string uriii = string.Format(hosturlLiveNMS + "TranslateBing?message=پاکستان&fromLanguage=ur&toLanguage=en");
            HttpWebRequest httpWebReques = (HttpWebRequest)WebRequest.Create(uriii);
            WebResponse newrespons = null;
            try
            {
                newrespons = httpWebReques.GetResponse();
                using (Stream stream = newrespons.GetResponseStream())
                {
                    string outptt = new StreamReader(stream).ReadToEnd();
                    obj2 = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(outptt);
                    if (obj2.ToLower() == ResultTestStringg)
                        BingResult = true;
                }
            }
            catch (Exception ex)
            {
                BingResult = false;
                bingliveerrormsg = ex.Message;
            }
            return BingResult;
        }

        //For Bing Translation QA
        public bool BingTranslationQACheck(bool BingResultQA, string obj2, string ResultTestStringg, string hosturlQANMS)
        {
            string uriii = string.Format(hosturlQANMS + "TranslateBing?message=پاکستان&fromLanguage=ur&toLanguage=en");
            HttpWebRequest httpWebReques = (HttpWebRequest)WebRequest.Create(uriii);
            WebResponse newrespons = null;
            try
            {
                newrespons = httpWebReques.GetResponse();
                using (Stream stream = newrespons.GetResponseStream())
                {
                    string outptt = new StreamReader(stream).ReadToEnd();
                    obj2 = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(outptt);
                    if (obj2.ToLower() == ResultTestStringg)
                        BingResultQA = true;
                }
            }
            catch (Exception ex)
            {
                BingResultQA = false;
                bingqaerrormsg = ex.Message;
            }
            return BingResultQA;
        }

        // For Windows Service With Web API  
        private bool WindowsServiceCheck(string hosturl)
        {
            bool obj;
            string uri = string.Format(hosturl + "/amazon/ServicesThread");
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            WebResponse newresponse = null;
            try
            {
                newresponse = httpWebRequest.GetResponse();
                using (Stream stream = newresponse.GetResponseStream())
                {
                    string output = new StreamReader(stream).ReadToEnd();
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<bool>(output);
                }
            }
            catch (Exception ex)
            {
                obj = false;
                windowsserviceerrormessage = ex.Message;
            }
            return obj;
        }

        // For Internet Connection
        private bool InternetConnectionCheck()
        {
            bool intrnt;
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    intrnt = true;
                }
            }
            catch (Exception ex)
            {
                intrnt = false;
                interneterrormsg = ex.Message;
            }
            return intrnt;
        }

        // For NMS ControlPanel
        private bool AmazonControlPanel(string hosturl)
        {
            bool NMSControlPanelflag = false;
            try
            {
                WebClient myClientnms = new WebClient();
                Stream responsenms = myClientnms.OpenRead(hosturl + "/amazon/NMSControlPanel");
                string strnms = "";
                using (TextReader treader = new StreamReader(responsenms))
                {
                    strnms = treader.ReadToEnd();
                }
                responsenms.Close();
                if (strnms.Length > 0)
                {
                    NMSControlPanelflag = true;
                }
            }
            catch (Exception ex)
            {
                NMSControlPanelflag = false;
                servererrormsgnms = ex.Message;
            }
            return NMSControlPanelflag;
        }

        // For Server Connectivity
        private bool AmazonServerConnectivity(string serverhosturl)
        {
            bool flag = false;
            try
            {
                WebClient myClient = new WebClient();
                Stream response = myClient.OpenRead(serverhosturl);
                string str = "";
                using (TextReader treader = new StreamReader(response))
                {
                    str = treader.ReadToEnd();
                }
                response.Close();
                if (str.Length > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                servererrormsg = ex.Message;
            }
            return flag;
        }

        private void AddNewColumn(DataTable table, string columnType, string columnName)
        {
            DataColumn column = table.Columns.Add(columnName, Type.GetType(columnType));
        }

        int Priority1 = 0;

        //Adding Data Into The Table
        public void AddNewRow(DataTable table, int ChecklistID, string Name, string Status, string Issue, DateTime CreationDate, int Priority)
        {
            Priority1 = Priority;
            DataRow newrow = table.NewRow();
            newrow["ChecklistID"] = ChecklistID;
            newrow["Name"] = Name;
            newrow["Status"] = Status;
            newrow["Issue"] = Issue;
            newrow["CreationDate"] = CreationDate;
            newrow["Priority"] = Priority;
            table.Rows.Add(newrow);
            CheckListRepository ckrepo = new CheckListRepository();

            int abc = ckrepo.Selection(Name);
            if (abc == 1)
            {
                //UpdateModel
                CheckList ckListInsert = new CheckList();
                ckListInsert.ChecklistName = Name;
                ckListInsert.Status = Convert.ToBoolean(Status);
                ckListInsert.ErrorMessage = Issue;
                ckListInsert.CreationDate = CreationDate;
                ckListInsert.LastUpdatedDate = DateTime.UtcNow;
                ckrepo.UpdateResult(ckListInsert);
            }
            else
            {
                //insertModel
                CheckList ckList = new CheckList();
                ckList.ChecklistName = Name;
                ckList.Status = Convert.ToBoolean(Status);
                ckList.ErrorMessage = Issue;
                ckList.CreationDate = CreationDate;
                ckList.Priority = Convert.ToBoolean(Priority);
                ckrepo.InsertResult(ckList);
            }
        }
    }
}
