﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Helper;

namespace NMS.ProcessThreads
{
    public class DeleteOldResources : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            try
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
                var resources = resourceService.GetScrapResourceByDate(DateTime.UtcNow.AddDays(-30));
                if (resources != null && resources.Count > 0)
                {
                    int successfull = 0;
                    FileInfo fileinfo = null;
                    foreach (var res in resources)
                    {
                        try
                        {
                            string path = "C:/Services/NMSControlPanel/ExternalResources/" + res.ResourceGuid.ToString() + ".srsc";
                            fileinfo = new FileInfo(path);
                            fileinfo.Delete();
                            resourceService.DeleteScrapResource(res.ScrapResourceId);
                            successfull++;
                        }
                        catch (Exception exp)
                        {
                            logService.InsertSystemEventLog(string.Format("DeleteOldResources: Error Deleting resource {0}", exp.Message + res.ResourceGuid), exp.StackTrace, EventCodes.Error);
                        }
                    }
                    string msg = string.Format("DeleteOldResources: {0} of {1} files Deleted successfully", successfull, resources.Count);
                    logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                    return msg;
                }
                return "Successfull";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in DeleteOldResources: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
