﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NMS.ProcessThreads
{
    public class DiscrepencyThread : ISystemProcessThread
    {
        INewsFileService NewsFileService;
        ICategoryService categoryService;
        ILocationService locatoinService;
        
        IFilterService filterService;
        IFileCategoryService fileCategoryService;
        IFileLocationService fileLocationService;
        INewsFileFilterService newsFileFilterService;
        ISystemEventLogService logService;



        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            //int DiscrepencyType = Convert.ToInt32(argument);    
            try
            {
                

                NewsFileService = IoC.Resolve<INewsFileService>("NewsFileService");
                categoryService = IoC.Resolve<ICategoryService>("CategoryService");
                locatoinService = IoC.Resolve<ILocationService>("LocationService");
                logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                filterService = IoC.Resolve<IFilterService>("FilterService");
                fileCategoryService = IoC.Resolve<IFileCategoryService>("FileCategoryService");
                fileLocationService = IoC.Resolve<IFileLocationService>("FileLocationService");
                newsFileFilterService = IoC.Resolve<INewsFileFilterService>("NewsFileFilterService");

                logService.InsertSystemEventLog("DiscrepencyThread: Started", "", EventCodes.Log);
                GetAllDescrepancyNewsByCategoryDescrepancyValue(DescrepencyType.Category,2);
                GetAllDescrepancyNewsByLocationDescrepancyValue(DescrepencyType.Location,2);
                logService.InsertSystemEventLog("DiscrepencyThread: Completed successfully", "", EventCodes.Log);
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in DiscrepencyThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
            return "true";
        }

        private void GetAllDescrepancyNewsByCategoryDescrepancyValue(DescrepencyType type, int status)
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<MDescrepencyNews> lstDescrepancy = objNewsService.GetDiscrepencyBystatus(type, status);
            List<int> descripencyIds = new List<int>();

            // to insert the raw news
            try
            {
                foreach (MDescrepencyNews oMDNews in lstDescrepancy)
                {
                    NewsFile newsFile = NewsFileService.UpdateNewsFileCategoryLocationById(oMDNews.NewsFileId, oMDNews.CategoryId, oMDNews.DescrepencyType);
                    Category category = categoryService.GetCategory(newsFile.CategoryId.Value);
                    
                    Filter filter = new Filter();
                    filter.Name = category.Category;
                    filter.FilterTypeId = (int)FilterTypes.Category;
                    filter.CreationDate = DateTime.UtcNow;
                    filter.Value = category.CategoryId;
                    filter.LastUpdateDate = filter.CreationDate;
                    filter.IsActive = true;
                    filter.IsApproved = true;
                    if (category.ParentId.HasValue)
                    {
                        Category PCategory = categoryService.GetCategory(category.ParentId.Value);
                        Filter Parentfilter = new Filter();
                        Parentfilter.Name = PCategory.Category;
                        Parentfilter.FilterTypeId = (int)FilterTypes.Category;
                        Parentfilter.CreationDate = DateTime.UtcNow;
                        Parentfilter.Value = PCategory.CategoryId;
                        Parentfilter.LastUpdateDate = filter.CreationDate;
                        Parentfilter.IsActive = true;
                        Parentfilter.IsApproved = true;
                        Parentfilter = filterService.GetFilterCreateIfNotExist(filter);
                        if (Parentfilter != null)
                        {
                            filter.ParentId = Parentfilter.ParentId;
                        }
                    }
                    filter = filterService.GetFilterCreateIfNotExist(filter);
                    fileCategoryService.DeleteFileCategoryByNewsFileId(newsFile.NewsFileId);

                    FileCategory fileCat = new FileCategory();
                    fileCat.CategoryId = category.CategoryId;
                    fileCat.CreationDate = DateTime.UtcNow;
                    fileCat.NewsFileId = newsFile.NewsFileId;
                    fileCategoryService.InsertFileCategory(fileCat);

                    NewsFileFilter newsFilterFile= getNewsFilter(filter, newsFile.NewsFileId, DateTime.UtcNow);
                    newsFileFilterService.InsertNewsFileFilter(newsFilterFile);
                    descripencyIds.Add(oMDNews.DescrepencyNewsFileId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //delete the Descrepancy News
            try
            {
                foreach (int oMDNews in descripencyIds)
                {
                    objNewsService.DeleteDescrepancyNews(oMDNews);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetAllDescrepancyNewsByLocationDescrepancyValue(DescrepencyType type, int status)
        {
            INewsService objNewsService = IoC.Resolve<INewsService>("NewsService");
            List<MDescrepencyNews> lstDescrepancy = objNewsService.GetDiscrepencyBystatus(type, status);
            List<int> descripencyIds = new List<int>();

            // to insert the raw news
            try
            {
                foreach (MDescrepencyNews oMDNews in lstDescrepancy)
                {
                    NewsFile newsFile = NewsFileService.UpdateNewsFileCategoryLocationById(oMDNews.NewsFileId, oMDNews.CategoryId, oMDNews.DescrepencyType);
                    Location location = locatoinService.GetLocation(oMDNews.CategoryId);
                    fileLocationService.DeleteFileLocationByNewsFileId(newsFile.NewsFileId);
                    FileLocation fileLoc = new FileLocation();
                    fileLoc.LocationId = location.LocationId;
                    fileLoc.CreationDate = DateTime.UtcNow;
                    fileLoc.NewsFileId = newsFile.NewsFileId;
                    fileLocationService.InsertFileLocation(fileLoc);
                    descripencyIds.Add(oMDNews.DescrepencyNewsFileId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //delete the Descrepancy News
            try
            {
                foreach (int oMDNews in descripencyIds)
                {
                    objNewsService.DeleteDescrepancyNews(oMDNews);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private NewsFileFilter getNewsFilter(Filter filter, int newsId, DateTime dtNow)
        {
            NewsFileFilter newsFilter = new NewsFileFilter();
            newsFilter.CreationDate = dtNow;
            newsFilter.LastUpdateDate = dtNow;
            newsFilter.NewsFileId = newsId;
            newsFilter.FilterId = filter.FilterId;
            newsFilter.IsActive = true;
            return newsFilter;
        }
    }
}
