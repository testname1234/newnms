﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Helper;
using SelaniumLib;

namespace NMS.ProcessThreads
{
    public class DownloadExternalResourceThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }
        public class DownloadExternalResourceArgument
        {
            public string FolderPath { get; set; }
            public int? SystemTypeId { get; set; }
        }
        public string Execute(string argument)
        {
            // \\172.31.23.39\Resources
            string ResourceId = "";
            try
            {
                DownloadExternalResourceArgument arg = new DownloadExternalResourceArgument();
                string serverpath = @"\\172.31.23.39\Resources";
                arg.FolderPath = serverpath;
                arg.SystemTypeId = 0;
                //arg = JsonConvert.DeserializeObject<DownloadExternalResourceArgument>(argument);
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                WebClient client = new WebClient();
                HttpWebRequestHelper proxy = new HttpWebRequestHelper();
                IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
                // var resources = resourceService.GetScrapResourceByStatusIdAndSystemTypeId(null, arg.SystemTypeId);
                var resources = resourceService.GetScrapResourceByStatusIdAndSystemTypeId(null, 0);
                string Urlpath = "";

                if (resources != null && resources.Count > 0)
                {
                    int successfull = 0;
                    foreach (var res in resources)
                    {
                        client = new WebClient();
                        try
                        {
                            logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Test0 {0}", res.ResourceGuid), "", EventCodes.Error);

                            #region NoCode - Google and dailymotion vidoes downloading
                            //if (res.SystemType == 6 && !(res.Source.Contains(".png") || res.Source.Contains(".jpg") || res.Source.Contains(".gif") || res.Source.Contains(".bmp")))
                            //{
                            //        // Selenium GoogleResource DownLoader
                            //        SeleniumEngine objSlumn = new SeleniumEngine();
                            //        string VideoUrl = res.Source;

                            //        List<string> videoPath = objSlumn.StartSelinium(NMS.Core.AppSettings.SleniumTemplateUrl, VideoUrl);
                            //        if (videoPath != null && videoPath[0] != null && videoPath[0] != "" && videoPath[0] != " " && videoPath[0].Length > 20)
                            //            res.Source = videoPath[0];
                            //        else
                            //        {
                            //            logService.InsertSystemEventLog(string.Format("Google Video Downloadeder: Error downloading resource {0} {1}", "(" + res.ScrapResourceId + "_)" + res.Source, "No Resoponse File Provided. Attempt:" + res.StatusId.ToString()), "No Response File Provided", EventCodes.Error);
                            //            throw new Exception("No Resoponse File Provided. Attempt");
                            //        }
                            //        // Ended

                            //}
                            #endregion

                            logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Test1 {0}", res.ResourceGuid), "", EventCodes.Error);
                            string ext = System.IO.Path.GetExtension(res.Source).Trim('.');
                            string fileName = System.IO.Path.GetFileName(res.Source);
                            string path = arg.FolderPath + "/" + res.ResourceGuid.ToString() + ".srsc";
                            Urlpath = "http://{ScrapServerIp}/ResourceSite" + "/" + res.ResourceGuid.ToString() + ".srsc";
                            client.DownloadFile(res.Source, path);
                            //proxy.HttpUploadFile(NMS.Core.AppSettings.MediaServerUrl + "/PostMedia?Id=" + res.Guid.ToString() + "&IsHD=true", fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());
                            client.Dispose();
                            res.Status = ScrapResourceStatuses.Downloaded;
                            res.LastUpdateDate = DateTime.UtcNow;
                            res.DownloadedFilePath = Urlpath;
                            logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Test2 {0}", res.ResourceGuid), "", EventCodes.Error);
                            resourceService.UpdateScrapResource(res);
                            logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Test5 {0}", res.ResourceGuid), "", EventCodes.Error);
                            successfull++;
                        }
                        catch (Exception exp)
                        {
                            logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Test3 {0}", res.ResourceGuid), "", EventCodes.Error);
                            if (client != null)
                                client.Dispose();
                            logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Error downloading resource {0} {1}", "(" + res.ScrapResourceId + "_)" + res.Source, exp.Message), exp.StackTrace, EventCodes.Error);
                            res.StatusId++;
                            res.LastUpdateDate = DateTime.UtcNow;
                            resourceService.UpdateScrapResource(res);
                            logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Test4 {0}", res.ResourceGuid), "", EventCodes.Error);
                        }
                    }
                    string msg = string.Format("DownloadMediaThread: {0} of {1} files downloaded successfully", successfull, resources.Count);
                    logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                    return msg;
                }
                return "Successfull";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                // logService.InsertSystemEventLog(string.Format("Error in DownloadExternalResourceThread: {0}", exp.Message), "", EventCodes.Error);
                logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Error downloading resource {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                logService.InsertSystemEventLog(string.Format("DownloadExternalResourceThread: Test6 {0}", ""), "", EventCodes.Error);
                return "Error";
            }
        }
    }
}
