﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.Core.IService;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Enums;
using NMS.Core.Helper;

namespace NMS.ProcessThreads
{
    public class DownloadMediaThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            try
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                HttpWebRequestHelper proxy = new HttpWebRequestHelper();
                IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
                WebRequest.DefaultWebProxy = null;
                WebClient client = new WebClient();
                var resources = resourceService.GetDownloadedResources();
                if (resources != null && resources.Count > 0)
                {
                    int successfull = 0;
                    foreach (ScrapResource res in resources)
                    {
                        try
                        {
                            if (resourceService.GetResourceByGuidId(res.ResourceGuid) != null)
                            {
                                client = new WebClient();
                                string ext = System.IO.Path.GetExtension(res.Source).Trim('.');
                                string fileName = System.IO.Path.GetFileName(res.Source);
                                byte[] data = client.DownloadData(res.DownloadedFilePath.Replace("{ScrapServerIp}", ConfigurationManager.AppSettings["ScrapServerIp"].ToString()));
                                //logService.InsertSystemEventLog(string.Format("DownloadMediaThread: Log 2 {0}", res.ResourceGuid), "", EventCodes.Error);
                                client.Dispose();
                                MSApi MediaApi = new MSApi();
                                var httpStatus = MediaApi.PostMediaAndTranscode(res.ResourceGuid.ToString(), fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());
                                //resource.FilePath = dir3 + "/" + Path.GetFileName(resource.Source);
                                //resource.BucketId = ((int)NMSBucket.NMSBucket);
                                //resource.ApiKey = NMS.Core.AppSettings.MediaServerApiKey;                                 
                               // var httpStatus = proxy.HttpUploadFile(NMS.Core.AppSettings.MediaServerUrl + "/PostMedia?Id=" + res.ResourceGuid.ToString() + "&IsHD=true", fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());
                                if (httpStatus == HttpStatusCode.OK)
                                {
                                    res.Status = ScrapResourceStatuses.Completed;
                                    res.LastUpdateDate = DateTime.UtcNow;                                    
                                    successfull++;
                                }
                                else
                                {
                                    logService.InsertSystemEventLog(string.Format("DownloadMediaThread: Error downloading resource {0} Count{1}", "Error Uploading file", res.ResourceGuid.ToString()), "", EventCodes.Error);
                                    
                                    if (res.Status == ScrapResourceStatuses.Downloaded)
                                    res.Status = ScrapResourceStatuses.MSPendingtry1;
                                    else if (res.Status == ScrapResourceStatuses.MSPendingtry1)
                                        res.Status = ScrapResourceStatuses.MSPendingtry2;
                                    else if (res.Status == ScrapResourceStatuses.MSPendingtry2)
                                        res.Status = ScrapResourceStatuses.MSError;
                                    res.LastUpdateDate = DateTime.UtcNow;
                                  
                                }
                                resourceService.UpdateScrapResourceNoReturn(res);
                            }
                        }
                        catch (Exception exp)
                        {
                            logService.InsertSystemEventLog(string.Format("DownloadMediaThread: Error downloading resource {0} Count{1}", exp.Message, resources.Count), exp.StackTrace, EventCodes.Error);

                            if (res.Status == ScrapResourceStatuses.Downloaded)
                                res.Status = ScrapResourceStatuses.MSPendingtry1;
                            else if (res.Status == ScrapResourceStatuses.MSPendingtry1)
                                res.Status = ScrapResourceStatuses.MSPendingtry2;
                            else if (res.Status == ScrapResourceStatuses.MSPendingtry2)
                                res.Status = ScrapResourceStatuses.MSError;

                            res.LastUpdateDate = DateTime.UtcNow;
                            resourceService.UpdateScrapResourceNoReturn(res);                            
                        }
                    }
                    string msg = string.Format("DownloadMediaThread: {0} of {1} files downloaded successfully", successfull, resources.Count);
                    logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                    return msg;
                }
                return "Successfull";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in DownloadMediaThread: {0}", exp.Message), "", EventCodes.Error);
                return "Error";
            }
        }
    }
}
