﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Threading;
using System.Xml.Serialization;
using System.Net;
using NMS.Core.Helper;
using NMS.Repository;
using System.Data;
using NMS.Core.Entities;

namespace NMS.ProcessThreads
{
    public class EpisodeStatusSmsThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }
        MosActiveEpisodeRepository repo = new MosActiveEpisodeRepository(); 
       
        public string Execute(string message)
        {                        
            //List<MosActiveEpisode> asd = repo.GetAllMosEpisodeWithProgram().Where(x=>x.StatusCode).ToList().Equals(4);
            List<MosActiveEpisode> asd = repo.GetAllMosEpisodeStatusSmsThread();
            foreach (MosActiveEpisode row in asd)
            {
                message = row.ProgramName+ "\n" + row.StatusDescription;

                HttpWebRequestHelper webProxy = new HttpWebRequestHelper();
                string smsId = ConfigurationManager.AppSettings["SmsID"].ToString();
                string smsShortCode = ConfigurationManager.AppSettings["SmsShortCode"];
                string smsPassword = ConfigurationManager.AppSettings["SmsPassword"];
                List<string> phoneNumbers = new List<string>();
                phoneNumbers.Add(ConfigurationManager.AppSettings["phoneNumbers"]);

                for (int i = 0; i < phoneNumbers.Count; i++)
                {
                    try
                    {
                        //if (ConfigurationManager.AppSettings["test"] != Convert.ToString(1))
                        //{
                            string requestUrl = "http://bsms.ufone.com/bsms_app4/sendapi.jsp?id=" + smsId + "&message=" + message + "&shortcode=" + smsShortCode + "&lang=English&mobilenum=" + "92" + phoneNumbers[i].Trim().TrimStart('0') + "&password=" + smsPassword;
                            HttpWebResponse response = webProxy.PostRequest(requestUrl, null, null, false);
                            if (response != null)
                                response.Close();
                        //}
                    }
                    catch (Exception exp)
                    {
                        Console.WriteLine(exp.Message);
                        ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                        logService.InsertSystemEventLog(string.Format("Error in BackupThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                    }
                }
            }
            return "successfull";
        }        
        //public string mymessage(string message)
        //{

        //    return message;
        //}
    }
}
