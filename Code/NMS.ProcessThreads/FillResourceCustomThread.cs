﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.IService;
using MS.Repository;
using SolrManager;
using SolrManager.InputEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.ProcessThreads
{
    public class FillResourceCustomThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }
        public static SolrService<SolrResource> sService = new SolrService<SolrResource>();
        public string Initialize()
        {

            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {


            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in FillCacheThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }


            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
            BucketRepository bucketService = new BucketRepository();
            SolrService<SolrResource> solrService = new SolrService<SolrResource>();
            string lastupdatedate = "2014-06-03 23:30:23.757";
            DateTime firstDate = DateTime.Parse(lastupdatedate);


            try
            {
                string solrCollectionUrl = ConfigurationManager.AppSettings["solrUrl2"];

                while (true)
                {

                    DateTime lastDate = firstDate;
                    List<SolrResource> solrResouces = new List<SolrResource>();
                    List<MS.Core.Entities.Resource> resources = new List<Resource>();
                    
                    //NOTE: Code commented overlad error on service
                    //List<MS.Core.Entities.Resource> resources = resourceService.GetResourcesAfterThisDate(lastDate);

                    resources = resources.Where(x => !string.IsNullOrEmpty(x.Metas)).ToList();

                    foreach (Resource item in resources)
                    {

                        if (item.BucketId.HasValue)
                        {
                            Bucket bucket = bucketService.GetBucket(item.BucketId.Value);
                            if (bucket != null)
                            {
                                if (bucket.IsPrivate == true)
                                {
                                    item.IsPrivate = true;
                                }
                            }
                        }

                        firstDate = item.LastUpdateDate;
                    }

                    if (resources == null)
                    { break; }

                    if (resources != null && resources.Count > 0)
                    {
                        solrResouces.CopyFrom(resources);
                        solrService.AddRecords(solrResouces);
                    }
                }

               // string message = string.Format("FillSolrResourceThread: Number of resouces updated: {0}", numberOfResourcesUpdated);
              //  logService.InsertSystemEventLog(message, null, EventCodes.Log);
                return "";
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in FillSolrResourceThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
