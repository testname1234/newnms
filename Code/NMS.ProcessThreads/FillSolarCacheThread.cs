﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.IService;
using NMS.MongoRepository;
using ScrapingProject.Model;
using ScrappingLib;
using SolrManager;
using SolrManager.InputEntities;

namespace NMS.ProcessThreads
{
    public class FillSolarCacheThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public static SolrService<SolarNews> sService = new SolrService<SolarNews>();
        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public Argument argu;
        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            //IBunchFilterService bunchFilterService = IoC.Resolve<IBunchFilterService>("BunchFilterService");
            INewsService newsService = IoC.Resolve<INewsService>("NewsService");
            int MigrationType;
            int.TryParse(argument, out MigrationType);
            try
            {
                string solrCollectionUrl = ConfigurationManager.AppSettings["solrUrl1"].ToString();
                try
                {
                    sService.Connect(solrCollectionUrl);
                }
                catch (Exception ex)
                {

                }
                int BunchCount = newsService.SeNewsCache(sService, MigrationType);                
                logService.InsertSystemEventLog(string.Format("Records Updated FillSolarCacheThread: {0}", BunchCount), "", EventCodes.Log);

                return "Success";

            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in FillSolarCacheThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        public class Argument
        {
            public string InputFile { get; set; }
            public string OutputFile { get; set; }
        }


    }
}
