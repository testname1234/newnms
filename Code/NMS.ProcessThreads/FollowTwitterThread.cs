﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;
using LinqToTwitter;
using System.Threading.Tasks;

namespace NMS.ProcessThreads
{
    public class FollowTwitterThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {


            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            ITwitterAccountService twitterAccountService = IoC.Resolve<ITwitterAccountService>("TwitterAccountService");
            ISocialMediaAccountService socialMediaAccountService = IoC.Resolve<ISocialMediaAccountService>("SocialMediaAccountService");
            
            try
            {
                //List<TwitterAccount> twitterAccounts = twitterAccountService.GetByIsFollowed(false);
                List<SocialMediaAccount> twitterAccounts = socialMediaAccountService.GetByIsFollowed(false);
                twitterAccounts = twitterAccounts.Where(x => x.IsActive == true).ToList();
                if (twitterAccounts != null && twitterAccounts.Count > 0)
                {
                    var auth = new SingleUserAuthorizer
                    {
                        CredentialStore = new SingleUserInMemoryCredentialStore
                        {
                            ConsumerKey = ConfigurationManager.AppSettings["consumerKey"],
                            ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"],
                            AccessToken = ConfigurationManager.AppSettings["accessToken"],
                            AccessTokenSecret = ConfigurationManager.AppSettings["accessTokenSecret"]
                        }
                    };

                    var ctx = new TwitterContext(auth);

                    foreach (SocialMediaAccount twitterAccount in twitterAccounts)
                    {                        
                        var task = FollowFriend(ctx, twitterAccount);
                        task.Wait();
                        System.Threading.Thread.Sleep(2000);
                    }
                }
                return "Success";

            }
            catch (Exception exp)
            {
                logException(exp);
                return "Error";
            }
        }

        public void logException(Exception exp)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            logService.InsertSystemEventLog(string.Format("FollowTwitterThread:{0}", exp.Message), exp.StackTrace, EventCodes.Error);
            if (exp.InnerException != null) logException(exp.InnerException);
        }

        public async Task FollowFriend(TwitterContext ctx, SocialMediaAccount twitterAccount)
        {
            ISocialMediaAccountService socialMediaAccountService = IoC.Resolve<ISocialMediaAccountService>("SocialMediaAccountService");
            var user = await ctx.CreateFriendshipAsync(twitterAccount.UserName, true);
            if (user != null && user.Status != null)
            {
                socialMediaAccountService.UpdateTwitterAccountIsFollowed(twitterAccount.SocialMediaAccountId, true);
            }
        }



        public class DurationArgument
        {
            public string Source { get; set; }
        }
    }
}
