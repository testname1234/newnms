﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Helper;
using NMS.Core.IService;
using ScrapingProject.Model;
using ScrappingLib;


namespace NMS.ProcessThreads
{
    public class GetRunDownItems : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public Argument argu;
        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                
                HttpWebRequestHelper helper = new HttpWebRequestHelper();
                string apiPath = NMS.Core.AppSettings.PcrWebApi +"/getdata/getdevicesbygroupId/";
               // var output = helper.GetRequest<PCR.Core.DataTransfer.DataTransfer<List<pcrMachinesDataOutput>>>(apiPath + id, null);




                return "Service Started";
            }
            catch (Exception exp)
            {

                logService.InsertSystemEventLog(string.Format("Error in IndexNewsThread: {0}", exp.Message), "", EventCodes.Error);
                return "Error";
            }
        }

        public class Argument
        {
            public string InputFile { get; set; }
            public string OutputFile { get; set; }
        }


    }
}
