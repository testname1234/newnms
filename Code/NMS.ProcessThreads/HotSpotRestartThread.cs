﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Threading;

namespace NMS.ProcessThreads
{
    public class HotSpotRestartThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            try
            {
                string serviceName = "hshld";
                string serviceName2 = "HssTrayService";
                int timeoutMilliseconds = 25000;
                ServiceController service = new ServiceController(serviceName);
                ServiceController service2 = new ServiceController(serviceName2);
                try
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    service.Stop();
                    Thread.Sleep(20000);
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                    Thread.Sleep(25000);

                    service.Start();
                    Thread.Sleep(400000);
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                    //Delay
                    Thread.Sleep(40000);

                    service.Start();
                    Thread.Sleep(400000);
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                    //Delay
                    Thread.Sleep(40000);

                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                    Thread.Sleep(25000);

                    service2.Start();
                    service2.WaitForStatus(ServiceControllerStatus.Running, timeout);


                }
                catch
                {

                }

                return "Successfull";
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in BackupThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
