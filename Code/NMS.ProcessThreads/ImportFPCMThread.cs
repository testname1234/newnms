﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.MediaIntegration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.IService;
using NMS.Core.Entities;

namespace NMS.ProcessThreads
{
    public  class ImportFPCMThread :ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            try
            {
                IProgramSetMappingService _ProgramSetMappingService = IoC.Resolve<IProgramSetMappingService>("ProgramSetMappingService");
                _ProgramSetMappingService.SyncProgramSetMapping();
                IScreenTemplateService _iScreenTemplateService = IoC.Resolve<IScreenTemplateService>("ScreenTemplateService");
                _iScreenTemplateService.SyncScreenTemplate();
                 return "Successfull";
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in ImportFPCMThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
