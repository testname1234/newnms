﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using System.Configuration;
using Facebook;


namespace NMS.ProcessThreads
{
    public class ImportFacebookCredentialThread : ISystemProcessThread
    {



        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }


        public string Execute(string argument)
        {

            IFacebookCredentialService facebookCredentialService = IoC.Resolve<IFacebookCredentialService>("FacebookCredentialService");
            List<FacebookCredential> facebookCredentials = facebookCredentialService.GetAllFacebookCredential();
              

            FacebookClient client = new FacebookClient();
            string extendedToken = "";
            DateTime currentDate = DateTime.UtcNow;
            DateTime lastUpdate = (DateTime)facebookCredentials[0].LastUpdateDate;

            if (lastUpdate < DateTime.UtcNow.AddMonths(-1))
            {
                try
                {
                    dynamic result = client.Get("/oauth/access_token", new
                    {
                        grant_type = "client_credentials",
                        client_id = facebookCredentials[0].ClientId,
                        client_secret = facebookCredentials[0].ClientSecret,
                        //fb_exchange_token = facebookCredentials[0].AccessToken
                    });
                    extendedToken = result.access_token;

                    facebookCredentialService.UdpateAccessToken(extendedToken, currentDate, facebookCredentials[0].FacebookCredentialId);
                }

                catch
                {
                    extendedToken = facebookCredentials[0].AccessToken;
                }

               
            }

            return extendedToken;
            
        }


        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
        }
    }

}

