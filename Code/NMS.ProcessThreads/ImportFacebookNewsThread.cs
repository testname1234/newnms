﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using System.Configuration;
using Facebook;


namespace NMS.ProcessThreads
{
    public class ImportFacebookNewsThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string Execute(string argument)
        {
            string source = "Facebook";
            string testTitle = "";
            string FacebookUser = "";
            string AccessToken = "";
            try
            {
                ISocialMediaAccountService socialMediaAccountService = IoC.Resolve<ISocialMediaAccountService>("SocialMediaAccountService");
                ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
                INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
                IFacebookCredentialService facebookCredentialService = IoC.Resolve<IFacebookCredentialService>("FacebookCredentialService");
                NMS.Core.Translator tran = new NMS.Core.Translator();

                if (facebookCredentialService.GetAccessToken() != null)
                {
                    AccessToken = facebookCredentialService.GetAccessToken();
                    var facebookClient = new FacebookClient(AccessToken);


                    List<SocialMediaAccount> socialMediaAccounts = socialMediaAccountService.GetBySocialMediaType((int)SocialMediaType.FaceBook);

                    ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                    if (socialMediaAccounts != null && socialMediaAccounts.Count > 0)
                    {
                        int Newscounter = 0;
                        int DiscrepencyCatCount = 0;
                        int DiscrepencyLocCount = 0;
                        int RawNewsCount = 0;
                        foreach (SocialMediaAccount socialMediaAccount in socialMediaAccounts)
                        {
                            FacebookUser = socialMediaAccount.UserName;
                            ScrapMaxDate.SourceName = socialMediaAccount.SocialMediaAccountId.ToString();
                            ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-12);
                            DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);
                            var url = "/" + FacebookUser + "/posts";
                            IDictionary<string, object> feed = facebookClient.Get(url) as dynamic;
                            JsonArray posts = feed.First().Value as dynamic;
                            System.Threading.Thread.Sleep(5000);

                            if (posts != null && posts.Count > 0)
                            {
                                for (int i = 0; i < posts.Count; i++)
                                {
                                    JToken token = JObject.Parse(posts[i].ToString());
                                    string message;
                                    string MediaPicture;
                                    string MediaSource;
                                    string Link;

                                    if (token.SelectToken("message") != null)
                                        message = token.SelectToken("message").ToString();
                                    else
                                        message = "";

                                    if (token.SelectToken("picture") != null)
                                        MediaPicture = token.SelectToken("picture").ToString();
                                    else
                                        MediaPicture = "";

                                    if (token.SelectToken("source") != null)
                                        MediaSource = token.SelectToken("source").ToString();
                                    else
                                        MediaSource = "";

                                    if (token.SelectToken("link") != null)
                                        Link = token.SelectToken("link").ToString();
                                    else
                                        Link = "";

                                    string MediaType = token.SelectToken("type").ToString();
                                    DateTime created_time = (DateTime)token.SelectToken("created_time");
                                    if (!String.IsNullOrEmpty(message))
                                    {
                                        ScrapMaxDate.MaxUpdateDate = (DateTime)token.SelectToken("created_time");
                                        InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;
                                        string NewsTitle = "";
                                        string CelebrityName = "";

                                        RawNews news = new RawNews();
                                        Celebrity celebrity = celebrityService.GetBySocialMediaAccountUserName(socialMediaAccount.UserName, (int)SocialMediaType.FaceBook);
                                        news.Categories.Add(celebrity.Category);
                                        news.Location = celebrity.Location;
                                        CelebrityName = celebrity.Name + ": ";
                                        news.PublishTime = (DateTime)token.SelectToken("created_time");
                                        news.UpdateTime = (DateTime)token.SelectToken("created_time");

                                        news.Description = WebUtility.HtmlDecode(Regex.Replace(message, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                        NewsTitle = CelebrityName + WebUtility.HtmlDecode(Regex.Replace(message, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                        news.Title = NewsTitle;
                                        news.Source = source;
                                        news.SourceType = FilterTypes.SocialMedia;
                                        RawNewsCount++;
                                        if (news.Title.Length > 1 && !newsService.CheckIfNewsExists(news, out newsStatus))
                                        {
                                            if (!String.IsNullOrEmpty(MediaPicture) || !String.IsNullOrEmpty(source))
                                            {
                                                MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                                resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                                resource.Source = MediaPicture;
                                                if (!Regex.Match(resource.Source, RegExpressions.VideoExtraction, RegexOptions.IgnoreCase).Success)
                                                {
                                                    resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                                }
                                                else
                                                {
                                                    resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                                                }
                                                resource.IsFromExternalSource = true;
                                                string resourceTitle = news.Description;
                                                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                                                try
                                                {
                                                    Resource _res = resourceService.MediaPostResource(resource, source);
                                                    news.ImageGuids.Add(_res);
                                                }
                                                catch (Exception ex)
                                                {
                                                    logService.InsertSystemEventLog(NMS.Core.AppSettings.MediaServerUrl + "/PostResourceFaceBookError ____" + resource.Source + "Error Message: " + ex.Message, "Error Strace: " + ex.StackTrace, EventCodes.Error);
                                                }
                                                i++;
                                            }
                                            news.LanguageCode = tran.DetectLanguage(message);
                                            news.NewsType = NewsTypes.Story;
                                            news.Url = Link;
                                            newsStatus = newsService.InsertRawNews(news);
                                            if (newsStatus == InsertNewsStatus.DiscrepancyCategory)
                                                DiscrepencyCatCount++;
                                            else if (newsStatus == InsertNewsStatus.DiscrepancyLocation)
                                                DiscrepencyLocCount++;
                                            else if (newsStatus == InsertNewsStatus.Inserted)
                                                Newscounter++;
                                        }
                                    }
                                    newsService.UpdateScrapDate(ScrapMaxDate);
                                }
                            }
                            if (Newscounter > 0 || (DiscrepencyCatCount + DiscrepencyLocCount) > 0)
                            {
                                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                                string msg = string.Format("ImportFaceBookThread: {0}-> {1} of {2} ,DCC = {3},DLC = {4} News downloaded successfully", source + "_" + FacebookUser, Newscounter, RawNewsCount, DiscrepencyCatCount, DiscrepencyLocCount);
                                logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                            }
                        }
                    }
                }
                return "success";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ImportFaceBookThread: Error in {0} {1} {2}", source + "_" + FacebookUser, testTitle, exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
        }
     
    }
}
