﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using LinqToTwitter;
using System.Configuration;
using System.Xml;
using System.Web.Script.Serialization;
using NMS.Repository;

namespace NMS.ProcessThreads
{
    public class ImportGoogleNewsThread : ISystemProcessThread
    {
        private string _threadName; 
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string Execute(string argument)
        {
            string source = "Google News";
            ISystemEventLogService logService = null;
            try
            {
                INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
                IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
                logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                FilterRepository _iFilterRepository = new FilterRepository();
                ScrapMaxDatesRepository scrapMaxDaterepo = new ScrapMaxDatesRepository();

                //getting news from google
                var googlefeed = GetNewsFromGoogle(argument);

                ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                if (googlefeed != null)
                {
                    ScrapMaxDate.SourceName = source;
                    ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-1);
                    DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);

                    DateTime dtdate = new DateTime();
                    dtdate = DateTime.UtcNow;
                    if (dtdate > dtMaxScrapDate)
                    {
                        ScrapMaxDate.MaxUpdateDate = dtdate;

                        foreach (var feed in googlefeed.GoogleChannel.item)
                        {
                            if (!string.IsNullOrEmpty(feed.title))
                            {
                                NMS.Core.DataTransfer.News.PostInput file = new Core.DataTransfer.News.PostInput();
                                file.Title = feed.title;
                                file.NewsDate = DateTime.Parse(feed.pubDate);
                                file.Source = source;
                                file.FilterTypeId = (int)FilterTypes.Wire;
                                file.Url = feed.link;
                                file.CategoryIds = new List<int>();
                                file.CategoryIds.Add(427);  //national category
                                //file.Description = Regex.Replace(feed.description, "<.*?>", String.Empty);
                                file.Description = feed.description;
                                file.LanguageCode = "en";
                                if (file.Title.Length > 1 && !fileService.CheckIfNewsExistsSQL(file.Title, file.Source))
                                {
                                    HtmlDocument doc = new HtmlDocument();

                                    doc.LoadHtml(feed.description);
                                    var node = doc.DocumentNode.SelectNodes("//img/@src");

                                    if (node != null)
                                    {
                                        string imgtag = node.FirstOrDefault().OuterHtml.ToString();
                                        if (!string.IsNullOrEmpty(imgtag))
                                        {
                                            var matchString = Regex.Match(imgtag, "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase).Groups[1].Value;

                                            if (!string.IsNullOrEmpty(matchString))
                                            {
                                                matchString = "http:" + matchString;
                                                file.ImageGuids = new List<Resource>();
                                                
                                                MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                                resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                                resource.IsFromExternalSource = true;
                                                string resourceTitle = file.Title;

                                                try
                                                {
                                                    Filter filter = new Filter();
                                                    filter.Name = source;
                                                    filter.FilterTypeId = (int)FilterTypes.Wire;
                                                    filter.CreationDate = DateTime.UtcNow;
                                                    filter.LastUpdateDate = filter.CreationDate;
                                                    filter.IsActive = true;
                                                    filter.IsApproved = false;
                                                    filter = _iFilterRepository.GetFilterCreateIfNotExist(filter);

                                                    resource.SourceName = source;
                                                    resource.SourceTypeId = ((int)FilterTypes.Wire).ToString();
                                                    resource.SourceId = filter.FilterId.ToString();
                                                    resource.Source = matchString;

                                                    Resource _res = resourceService.MediaPostResource(resource, source);
                                                    file.ImageGuids.Add(_res);
                                                }
                                                catch (Exception ex)
                                                {
                                                    logService.InsertSystemEventLog(NMS.Core.AppSettings.MediaServerUrl + "/PostResourceError ____", resource.Source + "Error Message: " + ex.Message + "Error Strace: " + ex.StackTrace, EventCodes.Error);
                                                }
                                            }

                                        }
                                    }
                                    if (file.ImageGuids != null && file.ImageGuids.Count > 0)
                                    {
                                        file.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                                        file.Resources.CopyFrom(file.ImageGuids);
                                    }

                                    fileService.InsertNews(file);
                                }
                            }
                        }
                        scrapMaxDaterepo.UpdateScrapMaxDatesCustom(ScrapMaxDate);
                    }

                }
                logService.InsertSystemEventLog(string.Format("ImportGoogleNewsThread: Execution Completed in {0}", source), "", EventCodes.Log);
                return "success";
            }
            catch (Exception exp)
            {
                logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ImportGoogleNewsThread: Error in {0} {1}", source + "_" + exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
            
        }


        private NMS.Core.Helper.Rss GetNewsFromGoogle(string term)
        {
            string uri = string.Format("https://news.google.com/news/section?q={0}&output=rss", term);
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            WebResponse response = null;
            try
            {
                response = httpWebRequest.GetResponse();
                var xml = new XmlDocument();
                
                using (Stream stream = response.GetResponseStream())
                {
                    string output = new StreamReader(stream).ReadToEnd();
                    xml.LoadXml(output);
                    var obj = Newtonsoft.Json.JsonConvert.SerializeXmlNode(xml);
                    if (!string.IsNullOrEmpty(obj)) { obj = obj.Replace("channel", "GoogleChannel").Replace("\"@version\":\"2.0\",","").Replace("image","GoogleImage").Replace("#text","text").Replace("@isPermaLink", "isPermaLink").Replace("guid","GoogleGuid"); }
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    NMS.Core.Helper.GoogleRSS item = Newtonsoft.Json.JsonConvert.DeserializeObject<NMS.Core.Helper.GoogleRSS>(obj);
                    return item.rss;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //XChatLiveProException exp = new XChatLiveProException(string.Format("Google Translation Error : {0}", ex.Message));
                //ExceptionHandler.LogElmahException(exp);
                
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
        }

       
        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
        }
    }
}
