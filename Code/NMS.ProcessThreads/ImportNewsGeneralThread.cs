﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Repository;
using System.Configuration;
using System.Web.Script.Serialization;

namespace NMS.ProcessThreads
{
    public class ImportNewsGeneralThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        IDescrepencyNewsFileService descrepencyNewsFileService = null; 
        ICategoryService categoryservice = null; 
        ISystemEventLogService logService = null; 
        ILocationService locationservice = null; 
        
        

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }


        public string Execute(string argument)
        {
            descrepencyNewsFileService = IoC.Resolve<IDescrepencyNewsFileService>("DescrepencyNewsFileService");
            categoryservice =  IoC.Resolve<ICategoryService>("CategoryService");
            logService =  IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            locationservice =  IoC.Resolve<ILocationService>("LocationService");

            // INewsService newsService = IoC.Resolve<INewsService>("NewsService");
            string source = string.Empty;
            string testTitle = "";
            
            WebClient wc = new WebClient();
            FilterRepository _iFilterRepository = new FilterRepository();

            try
            {
                
                //Console.WriteLine(" Start Of Utility :" + DateTime.UtcNow);
                NewsSourceRepository newsSourceRepository = new NewsSourceRepository();
                ImportNewsThreadArgument arg = new ImportNewsThreadArgument();
                List<NewsSource> newsSources = newsSourceRepository.GetAllNewsSource().ToList();
                if (newsSources != null && newsSources.Count > 0)
                {
                    newsSources = newsSources.Where(x => x.IsActive == true).ToList();
                    INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                    INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
                    ScrapMaxDatesRepository scrapMaxDaterepo = new ScrapMaxDatesRepository();
                    IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
                    HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
                    ScrapMaxDates ScrapMaxDate = null;
                    foreach (NewsSource newsSource in newsSources)
                    {
                        try
                        {
                            //Console.WriteLine(" Start Of Utility Source:" + newsSource.Source +"__" + DateTime.UtcNow);
                            arg.Source = newsSource.Source;
                            arg.SourceType = Convert.ToInt32(newsSource.FilterTypeId);
                            arg.Language = newsSource.LanguageCode;
                            arg.FolderPath = newsSource.FolderPath;
                            arg.AddMediaResource = true;
                            // arg = JsonConvert.DeserializeObject<ImportNewsThreadArgument>(argument);
                            source = arg.Source;
                            Uri Uri = new System.Uri(arg.FolderPath);

                            string host = "";
                            if (ConfigurationManager.AppSettings["IsLocal"] != null)

                                host = Uri.Host + ":" + Uri.Port;
                            else
                                host = Uri.Host;

                            ScrapMaxDate = new ScrapMaxDates();
                            ScrapMaxDate.SourceName = arg.Source;
                            ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-1);
                            DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);

                            string path = arg.FolderPath + "//" + DateTime.UtcNow.ToString("MM dd yyyy") + "//";
                            string ListRegularExp = RegExpressions.ListExp;
                            string UrlExp = RegExpressions.UrlExp;

                            wc.Encoding = UTF8Encoding.UTF8;
                            HttpWebRequestHelper WebHelper = new HttpWebRequestHelper();
                            HtmlDocument document = new HtmlDocument();
                            try
                            {
                                document.LoadHtml(wc.DownloadString(path));
                            }
                            catch (Exception ex)
                            {
                                document = null;
                            }
                            if (document != null)
                            {
                                string[] HtmlArr = document.DocumentNode.InnerHtml.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                                if (HtmlArr != null && HtmlArr.Length > 0)
                                    HtmlArr = HtmlArr.Where(x => !x.Contains("html>")).ToArray();
                                string RawUrl = "";
                                string DateToCompare = "";
                                string pattern1 = "$1";
                                string pattern2 = "$2";
                                int Newscounter = 0;
                                int DiscrepencyCatCount = 0;
                                int DiscrepencyLocCount = 0;
                                int TotalCount = 0;
                               //InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;

                                foreach (string val in HtmlArr)
                                {
                                    RawUrl = "";
                                    DateToCompare = Regex.Replace(val, ListRegularExp, pattern1, RegexOptions.IgnoreCase).Trim();
                                    DateTime dtdate = new DateTime();
                                    if (DateTime.TryParse(DateToCompare, out dtdate) && dtdate > dtMaxScrapDate)
                                    {
                                        RawUrl = Regex.Replace(val, UrlExp, pattern2, RegexOptions.IgnoreCase);
                                        ScrapMaxDate.MaxUpdateDate = dtdate;
                                        dynamic jo = JsonConvert.DeserializeObject(wc.DownloadString("http://" + host + "//" + RawUrl));
                                        foreach (var _item in jo.Result)
                                        {

                                            TotalCount = jo.Result.Count;
                                            string NewsTitle = "";
                                            string NewsDesc = "";
                                            RawNews news = new RawNews();
                                            dynamic item = null;
                                            item = (_item is JArray && _item.Count > 0) ? _item[0] : _item;
                                            if (item != null && item["News_Heading"] != null && Convert.ToString(item["News_Heading"]).Length > 1 && ((item is JArray && item.Count > 0) || !(item is JArray)))
                                            {
                                                NewsTitle = WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                                                news.Title = NewsTitle.Trim();
                                                news.Source = arg.Source;
                                                news.SourceType = (FilterTypes)arg.SourceType;
                                                //if (news.Title.Length > 1 && newsService.GetDescrepancyByTitleAndSource(news.Title, arg.Source).Count == 0 && !newsService.CheckIfNewsExistsSQL(news, out newsStatus))
                                                if (news.Title.Length > 1 && !fileService.CheckIfNewsExistsSQL(news.Title, news.Source))
                                                {
                                                    //Console.WriteLine(" Start Of Utility News Found:" + arg.Source + "__" + DateTime.UtcNow);
                                                    #region
                                                    news.Categories = new List<string>();
                                                    news.Location = item["News_Location"];
                                                    if ((item["News_Category"] is JArray))
                                                    {
                                                        foreach (var category in (item["News_Category"] as JArray))
                                                        {
                                                            if (category.ToString().ToLower().Trim() != "home" && category.ToString().Trim() != string.Empty)
                                                                news.Categories.Add(category.ToString().Trim());
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (item["News_Category"].ToString().ToLower().Trim() != "home" && item["News_Category"].ToString().Trim() != string.Empty)
                                                            news.Categories.Add(item["News_Category"].ToString().Trim());
                                                    }
                                                    news.Location = news.Location.Trim().Length > 30 ? "" : news.Location.Trim();
                                                    HtmlDocument doc = new HtmlDocument();
                                                    HtmlNodeCollection nodecollections = null;
                                                    doc.LoadHtml(item["News_Body"].ToString());

                                                    HtmlNodeCollection delteNodes = doc.DocumentNode.SelectNodes("//img[contains(@style ,'display:none')]");
                                                    if (delteNodes != null && delteNodes.Count > 0)
                                                    {
                                                        foreach (HtmlNode htmlNode in delteNodes)
                                                        {
                                                            htmlNode.Remove();
                                                        }
                                                    }
                                                    //Code for Video Extraction                                            
                                                    HtmlNode VideoNode = doc.DocumentNode.SelectSingleNode("//div//script[1]");
                                                    if (VideoNode != null)
                                                    {
                                                        MatchCollection videoCollection = Regex.Matches(VideoNode.InnerHtml, "http://((?!><).)*?(\\.mp4|\\.flv)", RegexOptions.IgnoreCase);
                                                        if (videoCollection != null && videoCollection.Count > 0)
                                                        {
                                                            HtmlNode AddNode = new HtmlNode(HtmlNodeType.Element, doc, 0);
                                                            VideoNode.InnerHtml = "<video src=\"" + videoCollection[0].Value + "\"/>";
                                                            doc.DocumentNode.AppendChild(AddNode);
                                                        }
                                                    }

                                                    #endregion
                                                    
                                                    if (arg.AddMediaResource)    //For NewsFile Entity no resouces required 
                                                    {
                                                        #region Add Resources
                                                        nodecollections = doc.DocumentNode.SelectNodes("//*[@src]");

                                                        if (nodecollections == null)
                                                        {
                                                            HtmlDocument docs = new HtmlDocument();
                                                            if(item["News_Resources"]!= null)
                                                            {
                                                                    docs.LoadHtml(item["News_Resources"].ToString());
                                                                    nodecollections = docs.DocumentNode.SelectNodes("//*[@src]");
                                                            }
                                                            
                                                        }

                                                        if (nodecollections != null && nodecollections.Count > 0)
                                                        {
                                                            news.ImageGuids = new List<Resource>();
                                                            foreach (HtmlNode htmlNode in nodecollections)
                                                            {
                                                                MatchCollection matchCollection = Regex.Matches(htmlNode.OuterHtml, RegExpressions.MediaExtraction, RegexOptions.IgnoreCase);
                                                                if (matchCollection.Count > 0)
                                                                {
                                                                    foreach (Match match in matchCollection)
                                                                    {
                                                                        string urlPrefix = "";
                                                                        if (!match.ToString().Contains("www.") && !match.ToString().Contains("http"))
                                                                        {
                                                                            string sourceurl = jo.Url.ToString();
                                                                            if (arg.Source == "ARY News")
                                                                                urlPrefix = match.ToString().ToLower().Contains("http:") ? "" : "http:";
                                                                            else
                                                                            {
                                                                                Uri uri = new Uri(sourceurl);
                                                                                urlPrefix = "http://" + uri.Host;
                                                                            }
                                                                        }
                                                                        MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                                                        resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                                                        resource.Source = urlPrefix + match.ToString().Replace("src=\"", "").Replace("\"", "").Replace("src='", "").Replace("'", "");
                                                                        if (!Regex.Match(resource.Source, RegExpressions.VideoExtraction, RegexOptions.IgnoreCase).Success)
                                                                        {
                                                                            resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                                                        }
                                                                        else
                                                                        {
                                                                            resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                                                                        }
                                                                        resource.IsFromExternalSource = true;
                                                                        string resourceTitle = "";
                                                                        if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["alt"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["alt"].Value))
                                                                        {
                                                                            resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["alt"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                                                        }
                                                                        else if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["title"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["title"].Value))
                                                                        {
                                                                            resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["title"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                                                        }
                                                                        try
                                                                        {
                                                                            Filter filter = new Filter();
                                                                            filter.Name = arg.Source;
                                                                            filter.FilterTypeId = (int)arg.SourceType;
                                                                            filter.CreationDate = DateTime.UtcNow;
                                                                            filter.LastUpdateDate = filter.CreationDate;
                                                                            filter.IsActive = true;
                                                                            filter.IsApproved = false;
                                                                            filter = _iFilterRepository.GetFilterCreateIfNotExist(filter);

                                                                            resource.SourceName = arg.Source;
                                                                            resource.SourceTypeId = arg.SourceType.ToString();
                                                                            resource.SourceId = filter.FilterId.ToString();

                                                                            Resource _res = resourceService.MediaPostResource(resource, arg.Source);
                                                                            news.ImageGuids.Add(_res);
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            logService.InsertSystemEventLog(NMS.Core.AppSettings.MediaServerUrl + "/PostResourceError ____", resource.Source + "Error Message: " + ex.Message + "Error Strace: " + ex.StackTrace, EventCodes.Error);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        #endregion
                                                    }

                                                    #region

                                                    HtmlDocument docDawn = new HtmlDocument();
                                                    doc.LoadHtml(item["News_Body"].ToString());
                                                    HtmlNodeCollection removenodeCollection = docDawn.DocumentNode.SelectNodes("//a[@class='slideshow__prev'] | //a[@class='slideshow__next'] | //style | //script");
                                                    if (removenodeCollection != null && removenodeCollection.Count > 0)
                                                    {
                                                        foreach (HtmlNode node in removenodeCollection)
                                                        {
                                                            node.Remove();
                                                        }
                                                    }
                                                    NewsDesc = doc.DocumentNode.InnerHtml;
                                                    NewsDesc = Regex.Replace(NewsDesc.Replace("\n", "").Replace("\r", "").Replace("\t", ""), RegExpressions.ScriptsRemoval, "", RegexOptions.Multiline);
                                                    NewsDesc = WebUtility.HtmlDecode(Regex.Replace(NewsDesc, "(?!</?br?.?.>)(?!</?p>)<.*?>", "", RegexOptions.IgnoreCase));
                                                    if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                                                        news.PublishTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                                                    if (item["News_Updated_Time"] != null && !String.IsNullOrEmpty(item["News_Updated_Time"].ToString()))
                                                        news.UpdateTime = DateTime.Parse(item["News_Updated_Time"].ToString());
                                                    else if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                                                        news.UpdateTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                                                    news.Description = NewsDesc.Replace("Please turn on JavaScript. Media requires JavaScript to play.", "").Trim();

                                                    if (news.SourceType == FilterTypes.Wire)
                                                    {
                                                        if (news.Title.ToLower().Contains("pakistan") || news.Description.ToLower().Contains("pakistan"))
                                                        {
                                                            if (news.Categories != null && news.Categories.Count > 0 && (news.Categories.Contains("International") || news.Categories.Contains("international")))
                                                            {
                                                                news.Categories = new List<string>();
                                                                news.Categories.Add("National");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            news.Categories = new List<string>();
                                                            news.Categories.Add("International");
                                                        }
                                                    }
                                                    //news.Categories = 
                                                    if (news.Categories.Count <= 0)
                                                    {
                                                        news.Categories.Add("Misc");
                                                    }
                                                    news.PublishTime = news.PublishTime.ToUniversalTime();
                                                    news.UpdateTime = news.UpdateTime.ToUniversalTime();
                                                    news.Author = item["News_Author"];
                                                    news.LanguageCode = arg.Language;
                                                    news.NewsType = NewsTypes.Story;
                                                    news.Url = item["URL"];

                                                    #region Text Translation incase not English
                                                    news.DescriptionText = Regex.Replace(news.Description, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase);
                                                    if (news.LanguageCode != "en")
                                                    {
                                                        news = TranslateNews(news);
                                                    }
                                                    #endregion Text Translation incase not English

                                                    if (arg.AddMediaResource)    //For NewsFile Entity no resouces required 
                                                    {
                                                        #region Get Resources from Google
                                                        if (news.ImageGuids == null || news.ImageGuids.Count == 0)
                                                        {
                                                            System.Threading.Thread.Sleep(2000);
                                                            try
                                                            {
                                                                WebClient wcgoogle = new WebClient();
                                                                string GoogleTitle = news.LanguageCode != "en" ? news.TranslatedTitle : news.Title;
                                                                string GoogleSearch = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + GoogleTitle + "&start=0&rsz=2&safe=active";
                                                                string GoogleJson = wcgoogle.DownloadString(GoogleSearch);
                                                                wcgoogle.Dispose();
                                                                dynamic GoogleData = JsonConvert.DeserializeObject<dynamic>(GoogleJson);
                                                                if (GoogleData != null && GoogleData.responseData != null && GoogleData.responseData.results != null)
                                                                {
                                                                    foreach (var goog in GoogleData.responseData.results)
                                                                    {
                                                                        MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                                                        resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                                                        resource.Source = goog.unescapedUrl;
                                                                        resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                                                        resource.IsFromExternalSource = true;
                                                                        string resourceTitle = goog.titleNoFormatting;

                                                                        Filter filter = new Filter();
                                                                        filter.Name = arg.Source;
                                                                        filter.FilterTypeId = (int)arg.SourceType;
                                                                        filter.CreationDate = DateTime.UtcNow;
                                                                        filter.LastUpdateDate = filter.CreationDate;
                                                                        filter.IsActive = true;
                                                                        filter.IsApproved = false;
                                                                        filter = _iFilterRepository.GetFilterCreateIfNotExist(filter);

                                                                        resource.SourceName = arg.Source;
                                                                        resource.SourceTypeId = arg.SourceType.ToString();
                                                                        resource.SourceId = filter.FilterId.ToString();

                                                                        Resource _res = resourceService.MediaPostResource(resource, arg.Source);
                                                                        news.ImageGuids.Add(_res);
                                                                        string msg = string.Format("ImportNewsThread: Images Got Getting Google Media_1 {0}___{1}", arg.Source, news.Title);
                                                                        logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string msg = string.Format("ImportNewsThread: No Images from Getting Google Media_2 {0}___{1}", arg.Source, news.Title);
                                                                    logService.InsertSystemEventLog(msg, GoogleData.ToString(), EventCodes.Error);
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                string msg = string.Format("ImportNewsThread: Error Getting Google Media {0}___{1}", arg.Source, ex.Message);
                                                                logService.InsertSystemEventLog(msg, "", EventCodes.Error);
                                                            }
                                                        }
                                                        #endregion
                                                    }

                                                    #region News File Service Code
                                                    NMS.Core.DataTransfer.News.PostInput newsInput = new Core.DataTransfer.News.PostInput();
                                                    newsInput.CopyFrom(news);
                                                    if (news.ImageGuids != null && news.ImageGuids.Count > 0)
                                                    {
                                                        newsInput.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                                                        newsInput.Resources.CopyFrom(news.ImageGuids);
                                                    }
                                                    newsInput.NewsDate = news.PublishTime;
                                                    newsInput.Slug = news.Title;
                                                    newsInput.FilterTypeId = (int)news.SourceType;
                                                    DateTime dtNow = DateTime.UtcNow;

                                                    InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;
                                                    InsertNewsStatus newsStatusCat = InsertNewsStatus.Inserted;
                                                    InsertNewsStatus newsStatusLoc = InsertNewsStatus.Inserted;
                                                    List<DiscrepencyIds> DiscrepancyIds = new List<DiscrepencyIds>();

                                                    CheckNewsDescrepencyMarking(news, dtNow, out newsStatus, out newsStatusCat, out newsStatusLoc,out DiscrepancyIds);

                                                    if (newsStatusCat == InsertNewsStatus.DiscrepancyCategory)
                                                    {
                                                        news.Categories = new List<string>();
                                                        news.Categories.Add("Misc");
                                                        //news.Location = "Misc";
                                                    }
                                                    if (newsStatusLoc == InsertNewsStatus.DiscrepancyLocation)
                                                    {
                                                        news.Location = "Misc";
                                                    }
                                                    
                                                    #region Categories
                                                    newsInput.CategoryIds = new List<int>();
                                                    foreach (var cat in news.Categories)
                                                    {
                                                        string cCategory = string.Empty;
                                                        if (!string.IsNullOrEmpty(cat))
                                                            cCategory = Regex.Replace(cat, "<.*?>", "", RegexOptions.IgnoreCase).Trim();

                                                        if (cat.ToLower() == "pakistan" || cat.ToLower() == "پاکستان")
                                                            cCategory = "national";

                                                        if (cat == "انٹرٹئینمنٹ" || cat == "شوبز")
                                                            cCategory = "entertainment";

                                                        if (cat == "کھیل")
                                                            cCategory = "sports";

                                                        
                                                        Category category = new Category();
                                                        category.Category = cCategory;
                                                        category.CreationDate = dtNow;
                                                        category.LastUpdateDate = category.CreationDate;
                                                        category.IsActive = true;
                                                        category.IsApproved = false;
                                                        category = categoryservice.GetCategoryCreateIfNotExist(category);
                                                        newsInput.CategoryIds.Add(category.CategoryId);
                                                    }

                                                    #endregion Categories

                                                    #region Location
                                                    newsInput.LocationIds = new List<int>();
                                                    if (!string.IsNullOrEmpty(news.Location))
                                                    {
                                                        Location loc = new Location();
                                                        loc.CreationDate = dtNow;
                                                        loc.LastUpdateDate = loc.CreationDate;
                                                        loc.IsActive = true;
                                                        loc.Location = news.Location;
                                                        loc = locationservice.GetLocationCreateIfNotExist(loc);
                                                        newsInput.LocationIds.Add(new List<Location>() { loc }[0].LocationId);
                                                    }
                                                    #endregion Location

                                                    //#region Location
                                                    //if (!string.IsNullOrEmpty(news.Location))
                                                    //{
                                                    //    List<int> lstloc = new List<int>();
                                                    //    var loc = locationservice.GetLocationByName(news.Location);
                                                    //    if (loc == null)
                                                    //    {
                                                    //        Location location = new Location();
                                                    //        location.Location = news.Location.ToLower();
                                                    //        location.IsActive = true;
                                                    //        location.IsApproved = true;
                                                    //        location.CreationDate = DateTime.UtcNow;
                                                    //        location.LastUpdateDate = DateTime.UtcNow;

                                                    //        var local = locationservice.InsertLocation(location);
                                                    //        lstloc.Add(local.LocationId);
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        lstloc.Add(loc.LocationId);
                                                    //    }

                                                    //    newsInput.LocationIds = lstloc;
                                                    //}
                                                    //#endregion Location

                                                    //#region Categories

                                                    //if (news.Categories != null && news.Categories.Count > 0)
                                                    //{
                                                    //    List<int> categoryids = new List<int>();
                                                    //    for (int i = 0; i <= news.Categories.Count - 1; i++)
                                                    //    {
                                                    //        if (!string.IsNullOrEmpty(news.Categories[i]))
                                                    //            news.Categories[i] = Regex.Replace(news.Categories[i], "<.*?>", "", RegexOptions.IgnoreCase).Trim();

                                                    //        if (news.Categories[i].ToLower() == "pakistan" || news.Categories[i].ToLower() == "پاکستان")
                                                    //            news.Categories[i] = "national";

                                                    //        var catlst = categoryservice.GetCategoryByName(news.Categories[i].ToLower());
                                                    //        if (catlst == null)
                                                    //        {
                                                    //            Category cat = new Category();
                                                    //            cat.Category = news.Categories[i].ToLower();
                                                    //            cat.CreationDate = DateTime.UtcNow;
                                                    //            cat.IsActive = true;
                                                    //            cat.LastUpdateDate = DateTime.UtcNow;
                                                    //            cat.IsApproved = true;
                                                    //            var obj = categoryservice.InsertCategory(cat);

                                                    //            categoryids.Add(obj.CategoryId);
                                                    //        }
                                                    //        else
                                                    //        {
                                                    //            categoryids.Add(catlst.CategoryId);
                                                    //        }
                                                    //    }
                                                    //    newsInput.CategoryIds = categoryids;
                                                    //}

                                                    //#endregion Categories

                                                    NewsFile InsertedNews = fileService.InsertNews(newsInput);

                                                    #endregion

                                                    #region Old Code
                                                    // Console.WriteLine(" Start Of Utility News Insertion End:" + arg.Source + "__" + DateTime.UtcNow);
                                                    if (newsStatusCat == InsertNewsStatus.DiscrepancyCategory)
                                                    {
                                                        DiscrepencyIds lst = DiscrepancyIds.Where(x => x.DescrepencyType == (int)InsertNewsStatus.DiscrepancyCategory).FirstOrDefault();
                                                        foreach (var DId in lst.DescrepencyIds)
                                                        {
                                                            descrepencyNewsFileService.UpdateNewsFileId(DId, InsertedNews.NewsFileId);

                                                        }
                                                        DiscrepencyCatCount++;
                                                    }
                                                    if (newsStatusLoc == InsertNewsStatus.DiscrepancyLocation)
                                                    {
                                                        DiscrepencyIds lst = DiscrepancyIds.Where(x => x.DescrepencyType == (int)InsertNewsStatus.DiscrepancyLocation).FirstOrDefault();
                                                        foreach (var DId in lst.DescrepencyIds)
                                                        {
                                                            descrepencyNewsFileService.UpdateNewsFileId(DId, InsertedNews.NewsFileId);

                                                        }
                                                        DiscrepencyLocCount++;
                                                    }
                                                    if (newsStatus == InsertNewsStatus.Inserted && newsStatusCat == InsertNewsStatus.Inserted && newsStatusCat == InsertNewsStatus.Inserted)
                                                        Newscounter++;



                                                    // output.Add(news);

                                                    #endregion

                                                    #endregion
                                                }
                                            }
                                        }
                                        scrapMaxDaterepo.UpdateScrapMaxDatesCustom(ScrapMaxDate);
                                    }
                                }
                                if (Newscounter > 0 || (DiscrepencyCatCount + DiscrepencyLocCount) > 0)
                                {
                                    string msg = string.Format("ImportNewsGeneralThread: {0}-> {1} of {2} ,DCC = {3},DLC = {4} News downloaded successfully", arg.Source, Newscounter, TotalCount, DiscrepencyCatCount, DiscrepencyLocCount);
                                    logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                                }
                            }
                        }
                        catch (Exception ex)
                        
                        {
                            logService.InsertSystemEventLog(string.Format("ImportNewsGeneralThread : Error in {0} {1} {2}", arg.Source, testTitle, ex.Message), ex.StackTrace, EventCodes.Error);
                        }
                        logService.InsertSystemEventLog(string.Format("ImportNewsGeneralThread: Execution Completed in {0} {1}", arg.Source, testTitle, ""), "", EventCodes.Log);
                    }
                }


            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("ImportNewsGeneralThread: Error in {0} {1} {2}", source, testTitle, exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
            finally
            {
                wc.Dispose();
            }
            // Console.ReadKey();
            return "Successfull";
        }



        private bool CheckNewsDescrepencyMarking(RawNews rawNews, DateTime dtNow, out InsertNewsStatus newsStatus, out InsertNewsStatus newsStatusCat, out InsertNewsStatus newsStatusLoc, out List<DiscrepencyIds> DiscrepancyIds)
        {
            var serializer = new JavaScriptSerializer();
            string serializedNews = serializer.Serialize(rawNews);
            newsStatus = InsertNewsStatus.Inserted;
            newsStatusCat = InsertNewsStatus.Inserted;
            newsStatusLoc = InsertNewsStatus.Inserted;
            DiscrepancyIds = new List<DiscrepencyIds>();
            DiscrepencyIds grpCat = new DiscrepencyIds();
            grpCat.DescrepencyType = (int)InsertNewsStatus.DiscrepancyCategory;
            foreach (var category in rawNews.Categories)
            {
                var cat = categoryservice.GetByAlias(category);
                if (cat == null)
                {
                    DescrepencyNewsFile descrepency = new DescrepencyNewsFile();
                    descrepency.RawNews = serializedNews;
                    descrepency.CreationDate = dtNow;
                    descrepency.LastUpdateDate = descrepency.CreationDate;
                    descrepency.DescrepencyType = (int)DescrepencyType.Category;
                    descrepency.DescrepencyValue = category;
                    descrepency.Title = rawNews.Title;
                    descrepency.DiscrepencyStatusCode = 1;
                    descrepency.Source = rawNews.Source;
                    descrepency.IsInserted = true;
                    NMS.Core.DataTransfer.DescrepencyNewsFile.PostInput newsInput = new Core.DataTransfer.DescrepencyNewsFile.PostInput();
                    newsInput.CopyFrom(descrepency);
                    NMS.Core.DataTransfer.DataTransfer<Core.DataTransfer.DescrepencyNewsFile.PostOutput> output = new NMS.Core.DataTransfer.DataTransfer<Core.DataTransfer.DescrepencyNewsFile.PostOutput>();
                    output =   descrepencyNewsFileService.Insert(newsInput);
                    newsStatusCat = InsertNewsStatus.DiscrepancyCategory;
                    grpCat.DescrepencyIds = new List<int>();
                    grpCat.DescrepencyIds.Add(output.Data.DescrepencyNewsFileId);
                    //return false;
                }
                DiscrepancyIds.Add(grpCat);
            }
            DiscrepencyIds grpLoc = new DiscrepencyIds();
            grpLoc.DescrepencyType = (int)InsertNewsStatus.DiscrepancyLocation;
            if (!string.IsNullOrEmpty(rawNews.Location))
            {
                var loc = locationservice.GetByAlias(rawNews.Location);
                if (loc == null)
                {
                    DescrepencyNewsFile descrepency = new DescrepencyNewsFile();
                    descrepency.RawNews = serializedNews;
                    descrepency.CreationDate = dtNow;
                    descrepency.Title = rawNews.Title;
                    descrepency.Source = rawNews.Source;
                    descrepency.DiscrepencyStatusCode = 1;
                    descrepency.LastUpdateDate = descrepency.CreationDate;
                    descrepency.DescrepencyType = (int)DescrepencyType.Location;
                    descrepency.DescrepencyValue = rawNews.Location;
                    descrepency.IsInserted = true;
                    NMS.Core.DataTransfer.DescrepencyNewsFile.PostInput newsInput = new Core.DataTransfer.DescrepencyNewsFile.PostInput();
                    newsInput.CopyFrom(descrepency);
                    NMS.Core.DataTransfer.DataTransfer<Core.DataTransfer.DescrepencyNewsFile.PostOutput> output = new NMS.Core.DataTransfer.DataTransfer<Core.DataTransfer.DescrepencyNewsFile.PostOutput>();
                    output = descrepencyNewsFileService.Insert(newsInput);
                    newsStatusLoc = InsertNewsStatus.DiscrepancyLocation;
                    grpLoc.DescrepencyIds = new List<int>();
                    grpLoc.DescrepencyIds.Add(output.Data.DescrepencyNewsFileId);
                    DiscrepancyIds.Add(grpLoc);
                    //return false;
                }
            }
            return true;
        }

        private RawNews TranslateNews(RawNews mongoNews)
        {
            NMS.Core.Translator NewsTranslater = new NMS.Core.Translator();
            mongoNews.TranslatedTitle = NewsTranslater.Translate(mongoNews.Title, mongoNews.LanguageCode, "en");
            if (!String.IsNullOrEmpty(mongoNews.Description))
            {
                mongoNews.TranslatedDescription = NewsTranslater.Translate(mongoNews.Description, mongoNews.LanguageCode, "en");
                mongoNews.DescriptionText = Regex.Replace(mongoNews.TranslatedDescription, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase);
            }
            return mongoNews;
        }

        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
            public bool AddMediaResource { get; set; }
        }

        public class GoogleResult
        {
            public string GsearchResultClass { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string imageId { get; set; }
            public string tbWidth { get; set; }
            public string tbHeight { get; set; }
            public string unescapedUrl { get; set; }
            public string url { get; set; }
            public string visibleUrl { get; set; }
            public string title { get; set; }
            public string titleNoFormatting { get; set; }
            public string originalContextUrl { get; set; }
            public string content { get; set; }
            public string contentNoFormatting { get; set; }
            public string tbUrl { get; set; }
        }

        public class Page
        {
            public string start { get; set; }
            public int label { get; set; }
        }

        public class Cursor
        {
            public string resultCount { get; set; }
            public List<Page> pages { get; set; }
            public string estimatedResultCount { get; set; }
            public int currentPageIndex { get; set; }
            public string moreResultsUrl { get; set; }
            public string searchResultTime { get; set; }
        }

        public class ResponseData
        {
            public List<GoogleResult> results { get; set; }
            public Cursor cursor { get; set; }
        }

        public class RootObject
        {
            public ResponseData responseData { get; set; }
            public object responseDetails { get; set; }
            public int responseStatus { get; set; }
        }

        public class DiscrepencyIds
        {
            public  int  DescrepencyType { get; set; }
            public List<int> DescrepencyIds { get; set; }
        }
    }
}
