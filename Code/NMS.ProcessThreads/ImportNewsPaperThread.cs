﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;

namespace NMS.ProcessThreads
{
    public class ImportNewsPaperThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string Execute(string argument)
        {
            ImportNewsThreadArgument arg = new ImportNewsThreadArgument();
            arg = JsonConvert.DeserializeObject<ImportNewsThreadArgument>(argument);
            IDailyNewsPaperService dailyNewsPaperService = IoC.Resolve<IDailyNewsPaperService>("DailyNewsPaperService");
            INewsPaperPageService newsPaperPageService = IoC.Resolve<INewsPaperPageService>("NewsPaperPageService");
            INewsPaperPagesPartService newsPaperPagesPartService = IoC.Resolve<INewsPaperPagesPartService>("NewsPaperPagesPartService");
            INewsPaperPagesPartsDetailService newsPaperPagesPartsDetailService = IoC.Resolve<INewsPaperPagesPartsDetailService>("NewsPaperPagesPartsDetailService");


            try
            {
                Uri Uri = new System.Uri(arg.FolderPath);
                string host = Uri.Host;
                //Add Dawn Epaper
                string path = arg.FolderPath + "//" + DateTime.UtcNow.ToString("MM dd yyyy") + "//";
                string ListRegularExp = RegExpressions.ListExp;
                string UrlExp = RegExpressions.UrlExp;
                WebClient wc = new WebClient();
                wc.Encoding = UTF8Encoding.UTF8;

                HttpWebRequestHelper WebHelper = new HttpWebRequestHelper();
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(wc.DownloadString(path));
                wc.Dispose();

                string[] HtmlArr = document.DocumentNode.InnerHtml.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                string RawUrl = "";
                string DateToCompare = "";
                string pattern1 = "$1";
                string pattern2 = "$2";
                int newspaperId = 2;
                foreach (string val in HtmlArr)
                {
                    RawUrl = "";
                    DateToCompare = Regex.Replace(val, ListRegularExp, pattern1, RegexOptions.IgnoreCase);
                    try
                    {
                        DateTime dtdate = new DateTime();
                        if (DateTime.TryParse(DateToCompare, out dtdate))
                        {
                            RawUrl = Regex.Replace(val, UrlExp, pattern2, RegexOptions.IgnoreCase);
                            if (!String.IsNullOrEmpty(RawUrl))
                            {
                                dynamic jo = JsonConvert.DeserializeObject(wc.DownloadString("http://" + host + "//" + RawUrl));
                                wc.Dispose();
                                if (dailyNewsPaperService.GeByDateAndNewsPaperId(DateTime.UtcNow.Date, DateTime.UtcNow.Date, newspaperId) == null)
                                {
                                    DateTime dt = DateTime.UtcNow;
                                    DailyNewsPaper dailyNewsPaper = new DailyNewsPaper();
                                    dailyNewsPaper.Date = dt.Date;
                                    dailyNewsPaper.CreationDate = dt;
                                    dailyNewsPaper.LastUpdateDate = dt;
                                    dailyNewsPaper.IsActive = true;
                                    dailyNewsPaper.NewsPaperId = newspaperId;
                                    DailyNewsPaper dailyNewsPaperNew = dailyNewsPaperService.InsertDailyNewsPaper(dailyNewsPaper);

                                    foreach (var _item in jo.Result)
                                    {
                                        dynamic item = null;
                                        item = (_item is JArray && _item.Count > 0) ? _item[0] : _item;
                                        if (item != null && ((item is JArray && item.Count > 0) || !(item is JArray)))
                                        {              
                                            #region item iteration
                                            string pagecategory = item["News_Category"].ToString();
                                            string ImageUrl = "";
                                            string pageurl = "";
                                            string pageImageUrl = "";
                                            string partImageCoord = "";
                                            List<string> NewsPartsDetail = new List<string>();

                                            int partImageCoordLeft = 0;
                                            int partImageCoordTop = 0;
                                            int partImageCoordWidth = 0;
                                            int partImageCoordheight = 0;


                                            if ((item["News_Main_Url"] is JArray))
                                            {
                                                foreach (var News_Main_Url in (item["News_Main_Url"] as JArray))
                                                {
                                                    if ((News_Main_Url["News_Frame_Url"] is JArray))
                                                    {
                                                        NewsPaperPage newsPaperPagenew = null;
                                                        foreach (var News_Frame_Url in (News_Main_Url["News_Frame_Url"] as JArray))
                                                        {                                                            
                                                            if (News_Frame_Url["News_Parts_Coordinates"] == null)
                                                            {
                                                                ImageUrl = News_Frame_Url["News_Main_Image"].ToString();
                                                                pageurl = News_Frame_Url["URL"].ToString();
                                                                pageImageUrl = pageurl.Substring(0, pageurl.LastIndexOf("/") + 1) + ImageUrl;

                                                                #region Resource Uploading Code
                                                                Guid resourceGuid = PostResource(arg, pageImageUrl);
                                                                #endregion

                                                                NewsPaperPage newsPaperPage = new NewsPaperPage();
                                                                newsPaperPage.CreationDate = dt;
                                                                newsPaperPage.LastUpdateDate = dt;
                                                                newsPaperPage.IsActive = true;
                                                                newsPaperPage.DailyNewsPaperId = dailyNewsPaperNew.DailyNewsPaperId;
                                                                newsPaperPage.PageTitle = pagecategory;
                                                                newsPaperPage.ImageGuid = resourceGuid; //Upload Main Page Image
                                                                newsPaperPagenew = newsPaperPageService.InsertNewsPaperPage(newsPaperPage);
                                                            }
                                                            else
                                                            {
                                                                partImageCoord = News_Frame_Url["News_Parts_Coordinates"].ToString();
                                                                string[] tmparray = partImageCoord.Replace(";", "").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                                                                partImageCoordLeft = Convert.ToInt32(tmparray[0]);
                                                                partImageCoordTop = Convert.ToInt32(tmparray[1]);
                                                                partImageCoordWidth = partImageCoordLeft + Convert.ToInt32(tmparray[2]);
                                                                partImageCoordheight = partImageCoordTop + Convert.ToInt32(tmparray[3]);

                                                                NewsPaperPagesPart newsPaperPagesPart = new NewsPaperPagesPart();
                                                                newsPaperPagesPart.CreationDate = dt;
                                                                newsPaperPagesPart.LastUpdateDate = dt;
                                                                newsPaperPagesPart.IsActive = true;
                                                                newsPaperPagesPart.NewsPaperPageId = newsPaperPagenew.NewsPaperPageId;
                                                                newsPaperPagesPart.Bottom = partImageCoordWidth;
                                                                newsPaperPagesPart.Left = partImageCoordLeft;
                                                                newsPaperPagesPart.Right = partImageCoordheight;
                                                                newsPaperPagesPart.Top = partImageCoordTop;
                                                                NewsPaperPagesPart newsPaperPagesPartnew = newsPaperPagesPartService.InsertNewsPaperPagesPart(newsPaperPagesPart);

                                                                if ((News_Frame_Url["News_Parts_Image_Url"] is JArray))
                                                                {
                                                                    foreach (var News_Parts_Image_Url in (News_Frame_Url["News_Parts_Image_Url"] as JArray))
                                                                    {
                                                                        if ((News_Parts_Image_Url["News_Parts_Page_Source"] is JArray))
                                                                        {
                                                                            foreach (var News_Parts_Page_Source in (News_Parts_Image_Url["News_Parts_Page_Source"] as JArray))
                                                                            {                                                                                
                                                                                #region Resource Uploading Code
                                                                                Guid imagePartGuid = PostResource(arg, News_Parts_Page_Source.ToString().Trim());
                                                                                #endregion
                                                                                NewsPaperPagesPartsDetail newsPaperPagesPartsDetail = new NewsPaperPagesPartsDetail();
                                                                                newsPaperPagesPartsDetail.ImageGuid = imagePartGuid.ToString();
                                                                                newsPaperPagesPartsDetail.CreationDate = dt;
                                                                                newsPaperPagesPartsDetail.LastUpdateDate = dt;
                                                                                newsPaperPagesPartsDetail.IsActive = true;
                                                                                newsPaperPagesPartsDetail.NewsPaperPagesPartId = newsPaperPagesPartnew.NewsPaperPagesPartId;
                                                                                newsPaperPagesPartsDetailService.InsertNewsPaperPagesPartsDetail(newsPaperPagesPartsDetail);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //Loop against items
                                            //Check if newspaper is not inserted
                                            //Dawn Id hard Coded

                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                        logService.InsertSystemEventLog(string.Format("ImportNewsPaperThread: Error in {0} {1}", arg.Source, ex.Message), ex.StackTrace, EventCodes.Error);
                    }
                }
                return "Successfull";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ImportNewsPaperThread: Error in {0} {1}", arg.Source, exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        private Guid PostResource(ImportNewsThreadArgument arg, string pageImageUrl)
        {
            Guid resoureguid = new Guid();
            try
            {
                HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
                MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                resource.Source = pageImageUrl;
                if (!Regex.Match(resource.Source, RegExpressions.VideoExtraction, RegexOptions.IgnoreCase).Success)
                {
                    resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                }
                resource.IsFromExternalSource = true;
                IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
                Resource res = resourceService.MediaPostResource(resource, arg.Source);                
                //HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", resource, null);
                //using (TextReader treader = new StreamReader(response.GetResponseStream()))
                //{
                //    string str = treader.ReadToEnd();
                //    MS.Core.Entities.Resource res = JsonConvert.DeserializeObject<MS.Core.Entities.Resource>(str);
                //    resoureguid = res.Guid;
                //}
                //response.Close();
                return new Guid(res.Guid);

            }
            catch (Exception ex)
            {

                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ImportNewsPaperThread: Error in {0} {1}", arg.Source, ex.Message), ex.StackTrace, EventCodes.Error);
                return resoureguid;
            }
        }

        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
        }
    }
}
