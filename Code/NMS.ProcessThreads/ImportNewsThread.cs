﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using System.Configuration;

namespace NMS.ProcessThreads
{
    public class ImportNewsThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }


        public string Execute(string argument)
        {
            string source = string.Empty;
            string testTitle = "";
            try
            {
                ImportNewsThreadArgument arg = new ImportNewsThreadArgument();
                arg = JsonConvert.DeserializeObject<ImportNewsThreadArgument>(argument);
                source = arg.Source;
                ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
                INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
                IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
                HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
                Uri Uri = new System.Uri(arg.FolderPath);
                string host = "";
                if (ConfigurationManager.AppSettings["IsLocal"] != null)

                    host = Uri.Host + ":" + Uri.Port;
                else
                    host = Uri.Host;
                ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                ScrapMaxDate.SourceName = arg.Source;
                ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-1);
                DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);

                string path = arg.FolderPath + "//" + DateTime.UtcNow.ToString("MM dd yyyy") + "//";
                string ListRegularExp = RegExpressions.ListExp;
                string UrlExp = RegExpressions.UrlExp;
                WebClient wc = new WebClient();
                wc.Encoding = UTF8Encoding.UTF8;
                HttpWebRequestHelper WebHelper = new HttpWebRequestHelper();
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(wc.DownloadString(path));
                wc.Dispose();

                string[] HtmlArr = document.DocumentNode.InnerHtml.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                string RawUrl = "";
                string DateToCompare = "";
                string pattern1 = "$1";
                string pattern2 = "$2";
                foreach (string val in HtmlArr)
                {
                    RawUrl = "";
                    int Newscounter = 0;
                    int DiscrepencyCatCount = 0;
                    int DiscrepencyLocCount = 0;
                    int TotalCount = 0;
                    DateToCompare = Regex.Replace(val, ListRegularExp, pattern1, RegexOptions.IgnoreCase);
                    try
                    {
                        DateTime dtdate = new DateTime();
                        if (DateTime.TryParse(DateToCompare, out dtdate))
                        {
                            if (Convert.ToDateTime(DateToCompare) > dtMaxScrapDate)
                            {
                                RawUrl = Regex.Replace(val, UrlExp, pattern2, RegexOptions.IgnoreCase);
                            }
                            if (!String.IsNullOrEmpty(RawUrl))
                            {
                                ScrapMaxDate.MaxUpdateDate = dtdate;
                                dynamic jo = JsonConvert.DeserializeObject(wc.DownloadString("http://" + host + "//" + RawUrl));
                                wc.Dispose();
                                InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;
                                foreach (var _item in jo.Result)
                                {
                                    TotalCount = jo.Result.Count;
                                    string NewsTitle = "";
                                    string NewsTitleOrg = "";
                                    string NewsDesc = "";
                                    RawNews news = new RawNews();
                                    dynamic item = null;
                                    item = (_item is JArray && _item.Count > 0) ? _item[0] : _item;
                                    if (item != null && item["News_Heading"] != null && Convert.ToString(item["News_Heading"]).Length > 1 && ((item is JArray && item.Count > 0) || !(item is JArray)))
                                    {

                                        NewsTitleOrg = WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                                        NewsTitle = WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                                        news.Title = NewsTitle.Trim();
                                        news.Source = arg.Source;
                                        news.SourceType = (FilterTypes)arg.SourceType;
                                        testTitle = NewsTitleOrg;
                                        List<MDescrepencyNews> DiscrepencyList = new List<MDescrepencyNews>();
                                        //News File Service Check if news Exist
                                       // var DiscrepencyList = newsService.GetDescrepancyByTitleAndSource(news.Title, arg.Source);

                                        if (news.Title.Length > 1 && DiscrepencyList.Count == 0 && !newsService.CheckIfNewsExistsSQL(news, out newsStatus))
                                        {
                                            news.Categories = new List<string>();
                                            news.Location = item["News_Location"];
                                            if ((item["News_Category"] is JArray))
                                            {
                                                foreach (var category in (item["News_Category"] as JArray))
                                                {
                                                    if (category.ToString().ToLower().Trim() != "home" && category.ToString().Trim() != string.Empty)
                                                        news.Categories.Add(category.ToString().Trim());
                                                }
                                            }
                                            else
                                            {
                                                if (item["News_Category"].ToString().ToLower().Trim() != "home" && item["News_Category"].ToString().Trim() != string.Empty)
                                                    news.Categories.Add(item["News_Category"].ToString().Trim());
                                            }
                                            news.Location = news.Location.Trim().Length > 30 ? "" : news.Location.Trim();

                                            HtmlDocument doc = new HtmlDocument();

                                            #region Resource Region
                                            if (arg.AddMediaResource)    //For NewsFile Entity no resouces required 
                                            {

                                                HtmlNodeCollection nodecollections = null;
                                                doc.LoadHtml(item["News_Body"].ToString());

                                                HtmlNodeCollection delteNodes = doc.DocumentNode.SelectNodes("//img[contains(@style ,'display:none')]");
                                                if (delteNodes != null && delteNodes.Count > 0)
                                                {
                                                    foreach (HtmlNode htmlNode in delteNodes)
                                                    {
                                                        htmlNode.Remove();
                                                    }
                                                }
                                                //Code for Video Extraction                                            
                                                HtmlNode VideoNode = doc.DocumentNode.SelectSingleNode("//div//script[1]");
                                                if (VideoNode != null)
                                                {
                                                    MatchCollection videoCollection = Regex.Matches(VideoNode.InnerHtml, "http://((?!><).)*?(\\.mp4|\\.flv)", RegexOptions.IgnoreCase);
                                                    if (videoCollection != null && videoCollection.Count > 0)
                                                    {
                                                        HtmlNode AddNode = new HtmlNode(HtmlNodeType.Element, doc, 0);
                                                        VideoNode.InnerHtml = "<video src=\"" + videoCollection[0].Value + "\"/>";
                                                        doc.DocumentNode.AppendChild(AddNode);
                                                    }
                                                }
                                                nodecollections = doc.DocumentNode.SelectNodes("//*[@src]");
                                                if (nodecollections != null && nodecollections.Count > 0)
                                                {
                                                    news.ImageGuids = new List<Resource>();
                                                    foreach (HtmlNode htmlNode in nodecollections)
                                                    {
                                                        MatchCollection matchCollection = Regex.Matches(htmlNode.OuterHtml, RegExpressions.MediaExtraction, RegexOptions.IgnoreCase);
                                                        if (matchCollection.Count > 0)
                                                        {
                                                            foreach (Match match in matchCollection)
                                                            {
                                                                string urlPrefix = "";
                                                                if (!match.ToString().Contains("www.") && !match.ToString().Contains("http"))
                                                                {
                                                                    string sourceurl = jo.Url.ToString();
                                                                    if (arg.Source == "ARY News")
                                                                        urlPrefix = match.ToString().ToLower().Contains("http:") ? "" : "http:";
                                                                    else
                                                                    {
                                                                        Uri uri = new Uri(sourceurl);
                                                                        urlPrefix = "http://" + uri.Host;
                                                                    }
                                                                }
                                                                MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                                                resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                                                resource.Source = urlPrefix + match.ToString().Replace("src=\"", "").Replace("\"", "").Replace("src='", "").Replace("'", "");
                                                                if (!Regex.Match(resource.Source, RegExpressions.VideoExtraction, RegexOptions.IgnoreCase).Success)
                                                                {
                                                                    resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                                                }
                                                                else
                                                                {
                                                                    resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                                                                }
                                                                resource.IsFromExternalSource = true;
                                                                string resourceTitle = "";
                                                                if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["alt"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["alt"].Value))
                                                                {
                                                                    resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["alt"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                                                }
                                                                else if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["title"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["title"].Value))
                                                                {
                                                                    resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["title"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                                                }
                                                                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                                                                try
                                                                {
                                                                    Resource _res = resourceService.MediaPostResource(resource, arg.Source);
                                                                    news.ImageGuids.Add(_res);
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    logService.InsertSystemEventLog(NMS.Core.AppSettings.MediaServerUrl + "/PostResourceError ____", resource.Source + "Error Message: " + ex.Message + "Error Strace: " + ex.StackTrace, EventCodes.Error);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                #region Get Resources from Google
                                                // if (news.ImageGuids == null || news.ImageGuids.Count == 0)
                                                // {
                                                //     int i = 0;
                                                //     ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                                                //// RerunSearch:
                                                //     System.Threading.Thread.Sleep(1000);
                                                //     try
                                                //     {
                                                //         WebClient wcgoogle = new WebClient();
                                                //         string GoogleSearch = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + news.Title + "&start=0&rsz=2&safe=active";
                                                //         string GoogleJson = wcgoogle.DownloadString(GoogleSearch);
                                                //         wcgoogle.Dispose();
                                                //         //RootObject GoogleData = JsonConvert.DeserializeObject<RootObject>(GoogleJson);
                                                //         dynamic GoogleData = JsonConvert.DeserializeObject<dynamic>(GoogleJson);
                                                //         if (GoogleData != null && GoogleData.responseData != null && GoogleData.responseData.results != null)
                                                //         {
                                                //             foreach (var goog in GoogleData)
                                                //             {
                                                //                 MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                                //                 resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                                //                 resource.Source = goog.unescapedUrl;
                                                //                 resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                                //                 resource.IsFromExternalSource = true;
                                                //                 string resourceTitle = goog.titleNoFormatting.Trim();
                                                //                 Resource _res = resourceService.MediaPostResource(resource, arg.Source);
                                                //                 news.ImageGuids.Add(_res);
                                                //                 string msg = string.Format("ImportNewsThread: Images Got Getting Google Media_1 {0}___{1}", arg.Source, news.Title);
                                                //                 logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                                                //             }
                                                //         }
                                                //         else
                                                //         {
                                                //             string msg = string.Format("ImportNewsThread: No Images from Getting Google Media_2 {0}___{1}", arg.Source, news.Title);
                                                //             logService.InsertSystemEventLog(msg, GoogleData.ToString(), EventCodes.Error);
                                                //             //i++;
                                                //             //if (i <= 3)
                                                //             //    goto RerunSearch;
                                                //         }
                                                //     }
                                                //     catch (Exception ex)
                                                //     {
                                                //         string msg = string.Format("ImportNewsThread: Error Getting Google Media {0}___{1}", arg.Source, ex.Message);
                                                //         logService.InsertSystemEventLog(msg, "", EventCodes.Error);
                                                //         //i++;
                                                //         //if (i <= 3)
                                                //         //    goto RerunSearch;

                                                //     }
                                                // }
                                                #endregion

                                            }

                                            #endregion
                                            //if (arg.Source == "Dawn News")
                                            //{
                                            HtmlDocument docDawn = new HtmlDocument();
                                            doc.LoadHtml(item["News_Body"].ToString());
                                            HtmlNodeCollection removenodeCollection = docDawn.DocumentNode.SelectNodes("//a[@class='slideshow__prev'] | //a[@class='slideshow__next'] | //style | //script");
                                            if (removenodeCollection != null && removenodeCollection.Count > 0)
                                            {
                                                foreach (HtmlNode node in removenodeCollection)
                                                {
                                                    node.Remove();
                                                }
                                            }
                                            NewsDesc = doc.DocumentNode.InnerHtml;
                                            NewsDesc = Regex.Replace(NewsDesc.Replace("\n", "").Replace("\r", "").Replace("\t", ""), RegExpressions.ScriptsRemoval, "", RegexOptions.Multiline);
                                            //}

                                            //NewsDesc = WebUtility.HtmlDecode(Regex.Replace(NewsDesc, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                                            NewsDesc = WebUtility.HtmlDecode(Regex.Replace(NewsDesc, "(?!</?br?.?.>)(?!</?p>)<.*?>", "", RegexOptions.IgnoreCase));
                                            if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                                                news.PublishTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                                            if (item["News_Updated_Time"] != null && !String.IsNullOrEmpty(item["News_Updated_Time"].ToString()))
                                                news.UpdateTime = DateTime.Parse(item["News_Updated_Time"].ToString());
                                            else if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                                                news.UpdateTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                                            news.Description = NewsDesc.Replace("Please turn on JavaScript. Media requires JavaScript to play.", "").Trim();

                                            if (news.SourceType == FilterTypes.Wire)
                                            {
                                                if (news.Title.ToLower().Contains("pakistan") || news.Description.ToLower().Contains("pakistan"))
                                                {
                                                    if (news.Categories != null && news.Categories.Count > 0 && (news.Categories.Contains("International") || news.Categories.Contains("international")))
                                                    {
                                                        news.Categories = new List<string>();
                                                        news.Categories.Add("National");
                                                    }
                                                }
                                                else
                                                {
                                                    news.Categories = new List<string>();
                                                    news.Categories.Add("International");
                                                }

                                            }

                                            //news.Categories = 
                                            if (news.Categories.Count <= 0)
                                            {
                                                news.Categories.Add("Misc");
                                            }
                                            news.PublishTime = news.PublishTime.ToUniversalTime();
                                            news.UpdateTime = news.UpdateTime.ToUniversalTime();
                                            news.Author = item["News_Author"];
                                            news.LanguageCode = arg.Language;
                                            news.NewsType = NewsTypes.Story;
                                            news.Url = item["URL"];
                                            // newsStatus = newsService.InsertRawNews(news);



                                            #region News File Service Code


                                            NMS.Core.DataTransfer.News.PostInput newsInput = new Core.DataTransfer.News.PostInput();

                                            
                                            newsInput.CopyFrom(news);
                                            newsInput.NewsDate = news.PublishTime;
                                            newsInput.Slug = news.Title;
                                           // newsInput.CategoryIds = news.Categories;
                                            fileService.InsertNews(newsInput);
                                            #endregion




                                           // newsStatus = newsService.InsertRawNews(news);


                                            //if (newsStatus == InsertNewsStatus.DiscrepancyCategory)
                                            //    DiscrepencyCatCount++;
                                            //else if (newsStatus == InsertNewsStatus.DiscrepancyLocation)
                                            //    DiscrepencyLocCount++;
                                            //else if (newsStatus == InsertNewsStatus.Inserted)
                                            Newscounter++;
                                            //output.Add(news);
                                        }
                                    }
                                }
                            }
                        }
                        if (Newscounter > 0 || (DiscrepencyCatCount + DiscrepencyLocCount) > 0)
                        {
                            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                            string msg = string.Format("ImportNewsThread: {0}-> {1} of {2} ,DCC = {3},DLC = {4} News downloaded successfully", arg.Source, Newscounter, TotalCount, DiscrepencyCatCount, DiscrepencyLocCount);
                            logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                        }
                    }
                    catch (Exception ex)
                    {
                        ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                        logService.InsertSystemEventLog(string.Format("ImportNewsThread: Error in {0} {1} {2}", arg.Source, testTitle, ex.Message), ex.StackTrace, EventCodes.Error);
                    }
                }
                newsService.UpdateScrapDate(ScrapMaxDate);
                return "Successfull";

            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ImportNewsThread: Error in {0} {1} {2}", source, testTitle, exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
            public bool AddMediaResource { get; set; }
        }

        public class GoogleResult
        {
            public string GsearchResultClass { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string imageId { get; set; }
            public string tbWidth { get; set; }
            public string tbHeight { get; set; }
            public string unescapedUrl { get; set; }
            public string url { get; set; }
            public string visibleUrl { get; set; }
            public string title { get; set; }
            public string titleNoFormatting { get; set; }
            public string originalContextUrl { get; set; }
            public string content { get; set; }
            public string contentNoFormatting { get; set; }
            public string tbUrl { get; set; }
        }

        public class Page
        {
            public string start { get; set; }
            public int label { get; set; }
        }

        public class Cursor
        {
            public string resultCount { get; set; }
            public List<Page> pages { get; set; }
            public string estimatedResultCount { get; set; }
            public int currentPageIndex { get; set; }
            public string moreResultsUrl { get; set; }
            public string searchResultTime { get; set; }
        }

        public class ResponseData
        {
            public List<GoogleResult> results { get; set; }
            public Cursor cursor { get; set; }
        }

        public class RootObject
        {
            public ResponseData responseData { get; set; }
            public object responseDetails { get; set; }
            public int responseStatus { get; set; }
        }
    }
}
