﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MMS.Integration.VMS;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Service;

namespace NMS.ProcessThreads
{
    public class ImportReelThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.Now.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IChannelService channelService = IoC.Resolve<IChannelService>("ChannelService");
            IChannelVideoService channelVideoService = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
            IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
            

            try
            {

                List<Channel> channels = channelService.GetAllChannel();
                int streamProgramId = 0;
                channels = channels.Where(x => !string.IsNullOrEmpty(x.ImportVideosPath)).ToList();

                foreach (var channel in channels)
                {
                    try
                    {
                        try
                        {
                            streamProgramId = programService.GetProgramByChannelId(channel.ChannelId).Where(x => x.Name == "Channel Stream").ToList().First().ProgramId;
                        }
                        catch (Exception exp)
                        {
                            MMS.Integration.FPC.FPCAPI api = new MMS.Integration.FPC.FPCAPI();
                            MMS.Integration.FPC.DataTransfer.Program myProgram = new MMS.Integration.FPC.DataTransfer.Program();
                            myProgram.ChannelId = Convert.ToInt32(channel.ChannelId);
                            myProgram.Name = "Channel Stream";
                            MMS.Integration.DataTransfer<MMS.Integration.FPC.DataTransfer.Program> returnedProgram = api.InsertProgram(myProgram);

                            if (returnedProgram.IsSuccess)
                            {
                                Program prog = new Program();
                                prog.ProgramId = returnedProgram.Data.ProgramChannelMappingId;
                                prog.ChannelId = channel.ChannelId;
                                prog.Name = "Channel Stream";
                                prog.CreationDate = DateTime.UtcNow;
                                prog.LastUpdateDate = prog.CreationDate;
                                prog.IsActive = true;
                                prog = programService.InsertProgram(prog);
                                streamProgramId = prog.ProgramId;
                            }
                            
                        }                     


                        int count = channelService.InsertNewReels(channel.ChannelId, streamProgramId);
                        if (count > 0)
                            logService.InsertSystemEventLog(string.Format("ImportReelThread: {0} reels imported for {1}", count, channel.Name), "", EventCodes.Log);
                    }
                    catch (Exception exp)
                    {
                        Console.WriteLine(exp.Message);
                        logService.InsertSystemEventLog(string.Format("Error in ImportReelThread: Channel:{0}, Exception:{1}", channel.Name, exp.Message), exp.StackTrace, EventCodes.Error);
                    }
                }

                return "Success";
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                logService.InsertSystemEventLog(string.Format("Error in ImportReelThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}
