﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using FPCM.Core.DataTransfer;
using FPCM.Core.Entities;
using FPCM.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Repository;
using ScrapingProject.Model;
using ScrappingLib;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace NMS.ProcessThreads
{
    public class ImportTemplateThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public Argument argu;

        #region Generate Image Work
        public void Save(string SourceGuid, int maxWidth, int maxHeight, int quality, string filePath)
        {
            Bitmap image = new System.Drawing.Bitmap("g:/PCR Images/source/bg.jpg");
            // Get the image's original width and height
            int originalWidth = image.Width;
            int originalHeight = image.Height;

            // To preserve the aspect ratio
            float ratioX = (float)maxWidth / (float)originalWidth;
            float ratioY = (float)maxHeight / (float)originalHeight;
            float ratio = Math.Min(ratioX, ratioY);

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            // Get an ImageCodecInfo object that represents the JPEG codec.
            ImageCodecInfo imageCodecInfo = this.GetEncoderInfo(ImageFormat.Jpeg);

            // Create an Encoder object for the Quality parameter.
            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
            // Create an EncoderParameters object. 

            EncoderParameters encoderParameters = new EncoderParameters(1);

            // Save the image as a JPEG file with quality level.
            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;
            newImage.Save(filePath, imageCodecInfo, encoderParameters);
        }

        /// <summary>
        /// Method to get encoder infor for given image format.
        /// </summary>
        /// <param name="format">Image format</param>
        /// <returns>image codec info.</returns>
        private ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public string generateimage()
        {
            Save("b8930b3c-ff95-4df1-963f-ed79ffb1e9b2", 540, 342, 100, "g:/PCR Images/bg_source.jpg");
            return "";
        }

        #endregion

        public string Execute(string argument)
        {
            // generateimage();

            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                double tmplateWidth = 1920;
                double tmplateHeight = 1080;

                AllDataList lst = new AllDataList();
                DataTransfer<AllDataList> lstData = new DataTransfer<AllDataList>();

                FlashTemplate listFlashTemplate = new FlashTemplate();
                FlashTemplateWindow listFlashTemplateWindow = new FlashTemplateWindow();
                FlashTemplateKey listFlashTemplateKey = new FlashTemplateKey();
                TemplateType listTemplateType = new TemplateType();

                HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
                string apiUrl = "http://10.3.12.102:82/api/getdata/get?ftd=2010-10-10&ftwd=2010-10-10&ftkd=2010-10-10&ttd=2010-10-10&csd=2010-10-10&TemplateTypeId=";
                lstData = requestHelper.GetRequest<DataTransfer<AllDataList>>(apiUrl, null);

                CasperTemplateTypeRepository casperTemplateTypeRepository = new CasperTemplateTypeRepository();
                CasperTemplateRepository casperTemplateRepository = new CasperTemplateRepository();
                CasperTemplateItemRepository casperTemplateItemRepository = new CasperTemplateItemRepository();
                CasperItemTransformationRepository casperItemTransformationRepository = new CasperItemTransformationRepository();
                CasperTemplateKeyRepository casperTemplateKeyRepository = new CasperTemplateKeyRepository();
                CasperTemplateWindowTypeRepository casperTemplateWindowTypeRepository = new CasperTemplateWindowTypeRepository();

                ProgramRepository programRepository = new ProgramRepository();
                ScreenTemplateRepository screenTemplateRepository = new ScreenTemplateRepository();
                TemplateScreenElementRepository templateScreenElementRepository = new TemplateScreenElementRepository();
                ScreenTemplatekeyRepository screenTemplatekeyRepository = new ScreenTemplatekeyRepository();
                ScrapMaxDatesRepository scrapMaxDatesRepository = new ScrapMaxDatesRepository();
                #region Raw list for NMS templates
                List<CasperTemplate> ctemplatesList = new List<CasperTemplate>();
                List<CasperTemplateItem> casperTemplateItemList = new List<CasperTemplateItem>();
                List<NMS.Core.Entities.ScreenTemplate> ScreenTemplateList = new List<Core.Entities.ScreenTemplate>();
                #endregion

                if (lstData.IsSuccess)
                {
                    DateTime NMSDate = new DateTime(2014, 1, 1);
                    DateTime ApiMaxDate = lstData.Data.ListFlashTemplate.OrderByDescending(x => x.LastUpdateDate).Select(x => x.LastUpdateDate).FirstOrDefault();
                    ScrapMaxDates scrapMaxDates = scrapMaxDatesRepository.GetScrapMaxDatesBySource("CasperTemplate");
                    if (scrapMaxDates != null)
                    {
                        NMSDate = scrapMaxDates.MaxUpdateDate;
                    }
                    else
                    {
                        scrapMaxDates = new ScrapMaxDates();
                        scrapMaxDates.SourceName = "CasperTemplate";
                    }

                    int templatesInserted = 0;
                    int elementsInserted = 0;
                    int templatetypesInserted = 0;
                    int keyfillInserted = 0;
                    int casperItemsInserted = 0;
                    int templatesUpdated = 0;
                    int transformationsInserted = 0;
                    int casperkeyfillInserted = 0;
                    if (NMSDate < ApiMaxDate)
                    {
                        List<FlashTemplate> ListFlashTemplate = lstData.Data.ListFlashTemplate.Where(x => x.LastUpdateDate > NMSDate).ToList();
                        List<FlashTemplateWindow> ListFlashTemplateWindow = lstData.Data.ListFlashTemplateWindow.Where(x => x.CreationDate > NMSDate).ToList();
                        List<FlashTemplateKey> ListFlashTemplateKey = lstData.Data.ListFlashTemplateKey != null ? lstData.Data.ListFlashTemplateKey.Where(x => x.CreationDate > NMSDate).ToList() : new List<FlashTemplateKey>();


                        foreach (TemplateType templatetype in lstData.Data.ListTemplateType)
                        {
                            CasperTemplateType casperTemplateType = new CasperTemplateType();
                            casperTemplateType.TemplateTypeId = templatetype.TemplateTypeId;
                            casperTemplateType.Name = templatetype.Name;
                            if (casperTemplateTypeRepository.GetByName(templatetype.Name) == null)
                            {
                                casperTemplateTypeRepository.InsertCasperTemplateType(casperTemplateType);
                                templatetypesInserted++;
                            }
                        }

                        foreach (FlashTemplate flashTemplate in ListFlashTemplate)
                        {
                            var program = programRepository.GetProgram(flashTemplate.ProgramId);
                            if (program != null)
                            {
                                var oldTemplate = screenTemplateRepository.GetScreenTemplateByFlashTemplateId(program.ProgramId, flashTemplate.FlashTemplateId);
                                if (oldTemplate == null)
                                {
                                    


                                    CasperTemplate casperTemplate = new CasperTemplate();
                                    casperTemplate.FlashTemplateId = flashTemplate.FlashTemplateId;
                                    casperTemplate.Template = flashTemplate.Name;
                                    casperTemplate.TemplateTypeId = flashTemplate.TemplateTypeId;
                                    casperTemplate.Width = flashTemplate.Width;
                                    casperTemplate.Height = flashTemplate.Height;
                                    if (flashTemplate.ResourceGuid == "B8E2C045-2022-4AC7-B08B-80C76013A04D")
                                        casperTemplate.ResourceGuid = "a5f91c14-2591-4fb4-a601-4144a359f86e";
                                    else
                                        casperTemplate.ResourceGuid = flashTemplate.ResourceGuid;
                                    if (casperTemplateRepository.GetBYFlashTemplateId(flashTemplate.FlashTemplateId) == null)
                                        casperTemplateRepository.InsertCasperTemplate(casperTemplate);

                                    CasperTemplateItem flashTemplateWindow = new CasperTemplateItem();
                                    flashTemplateWindow.Flashtemplateid = flashTemplate.FlashTemplateId;
                                    flashTemplateWindow.Name = flashTemplate.Name;
                                    flashTemplateWindow.FlashWindowTypeId = flashTemplate.TemplateTypeId;
                                    flashTemplateWindow.FlashTemplateGuid = flashTemplate.ResourceGuid;
                                    flashTemplateWindow.Flashtemplatetypeid = flashTemplate.TemplateTypeId;
                                    flashTemplateWindow.Type = "TEMPLATE";
                                    flashTemplateWindow.Label = flashTemplate.Name;
                                    flashTemplateWindow.ItemName = "Template";
                                    flashTemplateWindow.Videolayer = 1;
                                    flashTemplateWindow.Devicename = "Local CasparCG";
                                    flashTemplateWindow.Channel = "1";
                                    if (casperTemplateItemRepository.GetByLabel(flashTemplate.Name) == null)
                                        casperTemplateItemRepository.InsertCasperTemplateItem(flashTemplateWindow);


                                    NMS.Core.Entities.ScreenTemplate stemplate = new NMS.Core.Entities.ScreenTemplate();
                                    if (flashTemplate.Name.ToLower().Contains("bkk"))
                                        stemplate.Name = flashTemplate.Name.Substring(9);
                                    else
                                        stemplate.Name = flashTemplate.Name;
                                    stemplate.ProgramId = flashTemplate.ProgramId;
                                    stemplate.ThumbGuid = new Guid(flashTemplate.ResourceGuid);
                                    if (flashTemplate.ResourceGuid.ToString().ToLower() == "B8E2C045-2022-4AC7-B08B-80C76013A04D".ToLower())
                                        stemplate.ThumbGuid = new Guid("a5f91c14-2591-4fb4-a601-4144a359f86e");
                                    else
                                        stemplate.ThumbGuid = new Guid(flashTemplate.ResourceGuid);
                                    stemplate.IsDefault = false;
                                    stemplate.Flashtemplateid = flashTemplate.FlashTemplateId;
                                    stemplate.BackgroundImageUrl = new Guid("6f03c855-2cf0-400b-b41c-deafcd8da0a2");
                                    stemplate.Html = "html";
                                    stemplate.GroupId = "";
                                    stemplate.CreatonDate = flashTemplate.CreationDate;
                                    stemplate.LastUpdateDate = flashTemplate.LastUpdateDate;
                                    stemplate.IsActive = flashTemplate.IsActive;
                                    if (!stemplate.Name.ToLower().Contains("head"))
                                    {
                                        ScreenTemplateList.Add(screenTemplateRepository.InsertScreenTemplate(stemplate));
                                        templatesInserted++;
                                    }
                                }
                                else
                                {
                                    oldTemplate.ThumbGuid = new Guid(flashTemplate.ResourceGuid);
                                    oldTemplate.BackgroundImageUrl = new Guid("6f03c855-2cf0-400b-b41c-deafcd8da0a2");
                                    oldTemplate.LastUpdateDate = flashTemplate.LastUpdateDate;
                                    oldTemplate.IsActive = flashTemplate.IsActive;
                                    screenTemplateRepository.UpdateScreenTemplate(oldTemplate);
                                    templatesUpdated++;
                                }
                            }
                        }

                        List<KeyValuePair<string, int>> ElementGroupList = new List<KeyValuePair<string, int>>();
                        foreach (FlashTemplateWindow flashTemplate in ListFlashTemplateWindow)
                        {
                            ElementGroupList.Clear();

                            CasperTemplateItem flashTemplateWindow = new CasperTemplateItem();
                            flashTemplateWindow.Flashtemplateid = flashTemplate.FlashTemplateId;
                            flashTemplateWindow.Flashtemplatewindowid = flashTemplate.FlashTemplateWindowId;
                            flashTemplateWindow.FlashWindowTypeId = flashTemplate.FlashWindowTypeId;
                            flashTemplateWindow.Name = flashTemplate.Name;
                            flashTemplateWindow.Portnumber = flashTemplate.PortId;
                            flashTemplateWindow.Videolayer = flashTemplate.FlashTemplateWindowId;


                            flashTemplateWindow.Devicename = "Local CasparCG";
                            flashTemplateWindow.Channel = "1";
                            int screenElement = 1;
                            if (flashTemplate.FlashWindowTypeId == 1)
                            {
                                flashTemplateWindow.ItemName = "Anchor";
                                flashTemplateWindow.Type = "DECKLINKINPUT";
                                flashTemplateWindow.Label = "DECKLINKINPUT";
                                screenElement = 1;
                            }
                            else if (flashTemplate.FlashWindowTypeId == 2)
                            {
                                flashTemplateWindow.ItemName = "Video";
                                flashTemplateWindow.Type = "MOVIE";
                                flashTemplateWindow.Label = "MOVIE";
                                screenElement = 3;
                            }
                            else if (flashTemplate.FlashWindowTypeId == 3)
                            {
                                flashTemplateWindow.ItemName = "Graphics";
                                flashTemplateWindow.Type = "Graphics";
                                flashTemplateWindow.Label = "Graphics";
                                screenElement = 4;
                            }
                            else if (flashTemplate.FlashWindowTypeId == 4)
                            {
                                flashTemplateWindow.ItemName = "Guest";
                                flashTemplateWindow.Type = "DECKLINKINPUT";
                                flashTemplateWindow.Label = "DECKLINKINPUT";
                                screenElement = 8;
                            }
                            else if (flashTemplate.FlashWindowTypeId == 5)
                            {
                                flashTemplateWindow.ItemName = "Picture";
                                flashTemplateWindow.Type = "STILL";
                                flashTemplateWindow.Label = "STILL";
                                screenElement = 13;
                            }
                            //  flashTemplateWindow.Width = flashTemplate.Width;
                            // flashTemplateWindow.TopLeftx = flashTemplate.TopLeftx;
                            // flashTemplateWindow.Height = flashTemplate.Height;
                            // flashTemplateWindow.BottomRighty = flashTemplate.BottomRighty;
                            // flashTemplateWindow.BottomRighty = flashTemplate.BottomRighty;
                            if (casperTemplateItemRepository.GetByFlashTemplateWindowId(flashTemplate.FlashTemplateWindowId) == null)
                            {
                                casperTemplateItemRepository.InsertCasperTemplateItem(flashTemplateWindow);
                                casperItemsInserted++;
                            }

                            NMS.Core.Entities.TemplateScreenElement templateScreenElement = new NMS.Core.Entities.TemplateScreenElement();
                            templateScreenElement.ScreenElementId = screenElement;
                            //templateScreenElement.Top = (flashTemplate.TopLeftx * 0.316);
                            //templateScreenElement.Bottom = (flashTemplate.BottomRighty * 0.316);
                            //templateScreenElement.Left = (flashTemplate.Width * 0.28125);
                            //templateScreenElement.Right = (flashTemplate.Height * 0.28125);

                            templateScreenElement.Top = (flashTemplate.BottomRighty) * 0.316;
                            templateScreenElement.Left = (flashTemplate.TopLeftx) * 0.28125;
                            templateScreenElement.Bottom = (flashTemplate.Height + flashTemplate.BottomRighty) * 0.316;
                            templateScreenElement.Right = (flashTemplate.TopLeftx + flashTemplate.Width) * 0.28125;

                            templateScreenElement.Zindex = 0;
                            templateScreenElement.CreationDate = DateTime.UtcNow;
                            templateScreenElement.LastUpdateDate = DateTime.UtcNow;
                            templateScreenElement.IsActive = true;
                            templateScreenElement.Flashtemplatewindowid = flashTemplate.FlashTemplateWindowId;
                            templateScreenElement.ScreenTemplateId = ScreenTemplateList.Where(x => x.Flashtemplateid == flashTemplate.FlashTemplateId).Select(x => x.ScreenTemplateId).FirstOrDefault();
                            if (templateScreenElement.ScreenTemplateId.HasValue && templateScreenElement.ScreenTemplateId.Value > 0)
                            {
                                templateScreenElementRepository.InsertTemplateScreenElement(templateScreenElement);
                                elementsInserted++;
                            }
                        }
                        //Update group id
                        foreach (NMS.Core.Entities.ScreenTemplate screenTemplate in ScreenTemplateList)
                        {
                            var tscreenelements = templateScreenElementRepository.GetTemplateScreenElementByScreenTemplateId(screenTemplate.ScreenTemplateId);
                            if (tscreenelements != null && tscreenelements.Count > 0)
                            {
                                List<int> groupIds = tscreenelements.Select(x => x.ScreenElementId).OrderBy(x => x).ToList();
                                string group = "T_";
                                int i = 0;
                                foreach (int id in groupIds)
                                {
                                    if (i == 0)
                                        group += id;
                                    else
                                        group += "_" + id;
                                    i++;
                                }
                                screenTemplateRepository.UpdateScreenTemplateGroup(group, Convert.ToInt32(screenTemplate.Flashtemplateid));
                            }
                            else
                            {

                            }

                        }

                        foreach (FlashTemplateKey flashTemplate in ListFlashTemplateKey)
                        {
                            CasperTemplateKey flashTemplateWindow = new CasperTemplateKey();
                            flashTemplateWindow.FlashTemplateId = flashTemplate.FlashTemplateId;
                            flashTemplateWindow.FlashTemplateKeyId = flashTemplate.FlashTemplateKeyId;
                            flashTemplateWindow.FlashKey = flashTemplate.FlashKey;

                            if (casperTemplateKeyRepository.GetByFlashTemplateKeyId(flashTemplate.FlashTemplateKeyId) == null)
                            {
                                casperTemplateKeyRepository.InsertCasperTemplateKey(flashTemplateWindow);
                                casperkeyfillInserted++;
                            }

                            if (flashTemplate.FlashTemplateId != 50 && flashTemplate.FlashTemplateId != 55)
                            {
                                ScreenTemplatekey screenTemplatekey = new ScreenTemplatekey();
                                screenTemplatekey.KeyName = flashTemplate.FlashKey;
                                screenTemplatekey.FlashTemplateKeyId = flashTemplate.FlashTemplateKeyId;
                                screenTemplatekey.ScreenTemplateId = ScreenTemplateList.Where(x => x.Flashtemplateid == flashTemplate.FlashTemplateId).Select(x => x.ScreenTemplateId).FirstOrDefault();
                                screenTemplatekey.CreationDate = DateTime.UtcNow;
                                screenTemplatekey.IsActive = true;
                                if (screenTemplatekey.ScreenTemplateId.HasValue && screenTemplatekey.ScreenTemplateId.Value > 0)
                                {
                                    screenTemplatekeyRepository.InsertScreenTemplatekey(screenTemplatekey);
                                    keyfillInserted++;
                                }
                            }

                        }

                        foreach (FlashTemplateWindow flashTemplate in ListFlashTemplateWindow)
                        {
                            CasperItemTransformation flashTemplateWindow = new CasperItemTransformation();
                            flashTemplateWindow.Flashtemplatewindowid = flashTemplate.FlashTemplateWindowId;
                            flashTemplateWindow.FlashWindowTypeId = flashTemplate.FlashWindowTypeId;
                            flashTemplateWindow.Name = "Transformation";
                            flashTemplateWindow.Type = "TRANSFORMATION";
                            flashTemplateWindow.Label = "TRANSFORMATION";
                            flashTemplateWindow.Videolayer = flashTemplate.FlashTemplateWindowId;
                            flashTemplateWindow.Positionx = (flashTemplate.TopLeftx / tmplateWidth);
                            flashTemplateWindow.Positiony = (flashTemplate.BottomRighty / tmplateHeight);
                            flashTemplateWindow.Scalex = (flashTemplate.Width / tmplateWidth);
                            flashTemplateWindow.Scaley = (flashTemplate.Height / tmplateHeight);
                            flashTemplateWindow.Devicename = "Local CasparCG";
                            flashTemplateWindow.Channel = "1";

                            if (casperItemTransformationRepository.GetByFlashtemplatewindowid(flashTemplate.FlashTemplateWindowId) == null)
                            {
                                casperItemTransformationRepository.InsertCasperItemTransformation(flashTemplateWindow);
                                transformationsInserted++;
                            }
                        }

                        //Add in screen templates
                        scrapMaxDates.MaxUpdateDate = ApiMaxDate;
                        if (scrapMaxDates.ScrapMaxDatesId > 0)
                            scrapMaxDatesRepository.UpdateScrapMaxDates(scrapMaxDates);
                        else scrapMaxDatesRepository.InsertScrapMaxDates(scrapMaxDates);

                        string msg = string.Empty;
                        if (templatesInserted > 0) msg += " Templates:" + templatesInserted;
                        if (elementsInserted > 0) msg += " Elements:" + elementsInserted;
                        if (templatetypesInserted > 0) msg += " CasperTemplateTypes:" + templatetypesInserted;
                        if (keyfillInserted > 0) msg += " KeyFills:" + keyfillInserted;
                        if (casperItemsInserted > 0) msg += " CasperItems:" + casperItemsInserted;
                        if (templatesUpdated > 0) msg += " TemplatesUpdated:" + templatesUpdated;
                        if (transformationsInserted > 0) msg += " Transformation:" + transformationsInserted;
                        if (casperkeyfillInserted > 0) msg += " CasperKeyFill:" + casperkeyfillInserted;

                        if (!string.IsNullOrEmpty(msg))
                        {
                            logService.InsertSystemEventLog(string.Format("ImportTemplateThread: {0}", msg), "", EventCodes.Log);
                        }
                    }
                }
                else
                {
                    throw new Exception("API Returned Error: " + lstData.Errors[0]);
                }
                return "Service Started";
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ImportTemplateThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }

        }

        public class Argument
        {
            public string InputFile { get; set; }
            public string OutputFile { get; set; }
        }


    }
}
