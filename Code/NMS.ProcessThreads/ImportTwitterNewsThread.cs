﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using LinqToTwitter;
using System.Configuration;

namespace NMS.ProcessThreads
{
    public class ImportTwitterNewsThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string Execute(string argument)
        {
            string source = "Twitter";
            string testTitle = "";
            string TwitterUser = "";
            try
            {
                var auth = new SingleUserAuthorizer
                {
                    CredentialStore = new SingleUserInMemoryCredentialStore
                    {
                        ConsumerKey = ConfigurationManager.AppSettings["consumerKey"],
                        ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"],
                        AccessToken = ConfigurationManager.AppSettings["accessToken"],
                        AccessTokenSecret = ConfigurationManager.AppSettings["accessTokenSecret"]
                    }
                };

                var ctx = new TwitterContext(auth);
                ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
                ITwitterAccountService twitterAccountService = IoC.Resolve<ITwitterAccountService>("TwitterAccountService");
                INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
                IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
                ICategoryRepository categoryrepo = IoC.Resolve<ICategoryRepository>("CategoryRepository");
                ILocationRepository locationrepo = IoC.Resolve<ILocationRepository>("LocationRepository");

                //code to get celebrities from twiiter account
                List<TwitterAccount> LstTwitterAccounts = twitterAccountService.GetByIsFollowed(true);
                ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                if (LstTwitterAccounts != null && LstTwitterAccounts.Count > 0)
                {
                    int Newscounter = 0;
                    int DiscrepencyCatCount = 0;
                    int DiscrepencyLocCount = 0;
                    int RawNewsCount = 0;
                    foreach (TwitterAccount twitterAccount in LstTwitterAccounts)
                    {
                        TwitterUser = twitterAccount.UserName;
                        ScrapMaxDate.SourceName = twitterAccount.TwitterAccountId.ToString();
                        ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-12);
                        DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);

                        var tweets = (from tweet in ctx.Status
                                      where
                                          //tweet.Type == StatusType.Home                                      
                                      tweet.Type == StatusType.User
                                      && tweet.ScreenName == twitterAccount.UserName
                                          //&& tweet.User.ScreenNameResponse == twitterAccount.UserName 
                                      && tweet.Count == 50 && tweet.CreatedAt.Date >= dtMaxScrapDate
                                      //orderby tweet.CreatedAt ascending
                                      select tweet).ToArray();

                        if (tweets != null && tweets.Length > 0)
                        {

                            foreach (Status Celebtweet in tweets)
                            {
                                ScrapMaxDate.MaxUpdateDate = Celebtweet.CreatedAt;
                                InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;
                                string NewsTitle = "";
                                string CelebrityName = "";
                                RawNews news = new RawNews();

                                Celebrity celebrity = new Celebrity();

                                if(twitterAccount.CelebrityId.HasValue)
                                celebrity = celebrityService.GetBySocialMediaAccountUserName(twitterAccount.UserName, (int)SocialMediaType.Twitter);

                                else
                                celebrity = celebrityService.GetBySocialMediaAccountUserNameNoCeleb(twitterAccount.UserName);


                                if (celebrity != null)
                                {
                                    if (news.Categories == null)
                                        news.Categories = new List<string>();


                                    news.Categories.Add(celebrity.Category);
                                    news.Location = celebrity.Location;
                                    CelebrityName = celebrity.Name + ": ";
                                    news.PublishTime = Celebtweet.CreatedAt;
                                    news.UpdateTime = Celebtweet.CreatedAt;

                                    news.Description = WebUtility.HtmlDecode(Regex.Replace(Celebtweet.Text, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                    NewsTitle = CelebrityName + WebUtility.HtmlDecode(Regex.Replace(Celebtweet.Text, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                    news.Title = NewsTitle;
                                    news.Source = source;
                                    news.SourceType = FilterTypes.SocialMedia;
                                    
                                    RawNewsCount++;
                                    if (news.Title.Length > 1 && !newsService.CheckIfNewsExists(news, out newsStatus))
                                    {
                                        int i = 0;
                                        if (Celebtweet.Entities.MediaEntities.Count > 0)
                                        {
                                            MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                            resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                            resource.Source = Celebtweet.Entities.MediaEntities[i].MediaUrl;
                                            if (!Regex.Match(resource.Source, RegExpressions.VideoExtraction, RegexOptions.IgnoreCase).Success)
                                            {
                                                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                            }
                                            else
                                            {
                                                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                                            }
                                            resource.IsFromExternalSource = true;
                                            string resourceTitle = news.Description;
                                            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                                            try
                                            {
                                                Resource _res = resourceService.MediaPostResource(resource, source);
                                                news.ImageGuids.Add(_res);
                                            }
                                            catch (Exception ex)
                                            {
                                                logService.InsertSystemEventLog(NMS.Core.AppSettings.MediaServerUrl + "/PostResourceTwitterError ____" + resource.Source + "Error Message: " + ex.Message, "Error Strace: " + ex.StackTrace, EventCodes.Error);
                                            }
                                            i++;
                                        }
                                        news.LanguageCode = Celebtweet.Lang != null && Celebtweet.Lang != "und" ? Celebtweet.Lang : "en";
                                        news.NewsType = NewsTypes.Story;
                                        news.Url = "https://twitter.com/" + Celebtweet.ScreenName + "/status/" + Celebtweet.StatusID;


                                        //  newsStatus = newsService.InsertRawNews(news);
                                        NMS.Core.DataTransfer.News.PostInput newsInput = new Core.DataTransfer.News.PostInput();
                                        newsInput.CopyFrom(news);

                                        newsInput.FilterTypeId = (int)FilterTypes.SocialMedia;
                                        newsInput.CelebrityId = celebrity.CelebrityId.ToString();

                                        if (newsInput.LocationIds == null)
                                            newsInput.LocationIds = new List<int>();

                                        if (newsInput.CategoryIds == null)
                                            newsInput.CategoryIds = new List<int>();

                                        var loc = locationrepo.GetLocationByName(news.Location.ToLower());
                                        if (loc == null)
                                        {
                                            NMS.Core.Entities.Location location = new NMS.Core.Entities.Location();
                                            location.Location = news.Location.ToLower();
                                            location.IsActive = true;
                                            location.IsApproved = true;
                                            location.CreationDate = DateTime.UtcNow;
                                            location.LastUpdateDate = DateTime.UtcNow;

                                            var local = locationrepo.InsertLocation(location);
                                            newsInput.LocationIds.Add(local.LocationId);
                                        }
                                        else
                                        {
                                            newsInput.LocationIds.Add(loc.LocationId);
                                        }
                                        

                                        for(int j = 0; j <= news.Categories.Count - 1; j++)
                                        {
                                            var cat = categoryrepo.GetCategoryByName(news.Categories[j].ToLower());
                                            if(cat == null)
                                            {
                                                NMS.Core.Entities.Category catg = new NMS.Core.Entities.Category();
                                                catg.Category = news.Categories[i].ToLower();
                                                catg.CreationDate = DateTime.UtcNow;
                                                catg.IsActive = true;
                                                catg.LastUpdateDate = DateTime.UtcNow;
                                                catg.IsApproved = true;

                                                var obj = categoryrepo.InsertCategory(catg);

                                                newsInput.CategoryIds.Add(obj.CategoryId);
                                            }
                                            else
                                            {
                                                newsInput.CategoryIds.Add(cat.CategoryId);
                                            }
                                        }
                                        
                                        fileService.InsertNews(newsInput);



                                        //if (newsStatus == InsertNewsStatus.DiscrepancyCategory)
                                        //    DiscrepencyCatCount++;
                                        //else if (newsStatus == InsertNewsStatus.DiscrepancyLocation)
                                        //    DiscrepencyLocCount++;
                                        //else if (newsStatus == InsertNewsStatus.Inserted)
                                        //    Newscounter++;
                                    }
                                }
                            }
                            newsService.UpdateScrapDate(ScrapMaxDate);
                        }
                    }
                    if (Newscounter > 0 || (DiscrepencyCatCount + DiscrepencyLocCount) > 0)
                    {
                        ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                        string msg = string.Format("ImportTwitterThread: {0}-> {1} of {2} ,DCC = {3},DLC = {4} News downloaded successfully", source + "_" + TwitterUser, Newscounter, RawNewsCount, DiscrepencyCatCount, DiscrepencyLocCount);
                        logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                    }
                }
                return "success";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ImportTwitterThread: Error in {0} {1} {2}", source + "_" + TwitterUser, testTitle, exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
        }
    }
}
