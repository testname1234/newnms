﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Enums;
using Newtonsoft.Json;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Repository;

namespace NMS.ProcessThreads
{
    public class InsertReelForChannelReporterThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.Now.ToString("MMM dd,yyyy hh:mm:ss tt");
        }


        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IChannelService channelService = IoC.Resolve<IChannelService>("ChannelService");
            ReelRepository reelrepository = new ReelRepository();
            ChannelVideoRepository cvRepo = new ChannelVideoRepository();
            ProgramRepository programRepository = new ProgramRepository();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = null;
            WebClient client = new WebClient();
            int successCount = 0;

            try
            {
                List<int> channelIds = argument.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList();
                List<Channel> channels = channelService.GetAllChannel();
                //string liveConnectionString = ConfigurationManager.ConnectionStrings["LiveConnection"].ConnectionString;
                //List<Channel> channels = channelService.GetChannelsFromVideoMatching("");

                channels = channels.Where(x => !string.IsNullOrEmpty(x.VideosPath)).ToList();
                var AccessUrl = ConfigurationManager.AppSettings["AccessUrl"];
                var SavePath = ConfigurationManager.AppSettings["StreamSavePath"];
                

                List<Reel> Reels = reelrepository.GetReelByStatusAndChannelId(ReelStatuses.Created, channelIds);

                if (Reels != null)
                {
                    foreach (var reel in Reels)
                    {                        
                        ChannelVideo cVideo = new ChannelVideo();
                        cVideo.ChannelId = reel.ChannelId;
                        cVideo.From = reel.StartTime.ToUniversalTime();
                        cVideo.To = reel.EndTime.ToUniversalTime();
                        cVideo.ProgramId = programRepository.GetProgramByChannelId(reel.ChannelId).Where(x => x.Name == "Channel Stream").ToList().First().ProgramId;
                        cVideo.Url = reel.Path;
                        cVideo.PhysicalPath = reel.Path.Replace(AccessUrl, SavePath).Replace('/','\\');
                        cVideo.CreationDate = DateTime.UtcNow;
                        cVideo.LastUpdateDate = cVideo.CreationDate;
                        cVideo.IsActive = true;
                        cvRepo.InsertChannelVideo(cVideo);
                        //cvRepo.InsertChannelVideo(cVideo, liveConnectionString);

                        reel.StatusId = (int)ReelStatuses.Completed;
                        reelrepository.UpdateReel(reel);

                        successCount++;
                    }
                }
                if (successCount > 0)
                    logService.InsertSystemEventLog(string.Format("InsertReelForChannelReporterThread: {0} reels processed", successCount), "", EventCodes.Log);

                return "Success";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                logService.InsertSystemEventLog(string.Format("Error in InsertReelForChannelReporterThread: {0}", ex.Message), ex.StackTrace, EventCodes.Error);
                return "Fail";
            }
        }
    }
}
