﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;

namespace NMS.ProcessThreads
{
    public class MediaDuration : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {

            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            HttpWebRequestHelper proxy = new HttpWebRequestHelper();
            try
            {
               // DurationArgument arg = new DurationArgument();
               // arg = JsonConvert.DeserializeObject<DurationArgument>(argument);
                INewsService newsService = IoC.Resolve<INewsService>("NewsService");

                NMS.Core.IService.IResourceService nmsResourceService = IoC.Resolve<NMS.Core.IService.IResourceService>("ResourceService");

                ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                //ScrapMaxDate.SourceName = arg.Source;
                //DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);
                ScrapMaxDate.SourceName = "Media Duration";
                DateTime dtMaxScrapDate = new DateTime(2012, 1, 1);
                ScrapMaxDate.MaxUpdateDate = dtMaxScrapDate;
                
                MS.Core.DataTransfer.Resource.ResourceOutput msResource = null;
                HttpWebRequestHelper helper = new HttpWebRequestHelper();

                HttpWebResponse response = helper.GetRequest(NMS.Core.AppSettings.MediaServerUrl + "/GetResourceforDuration/?ResourceDate=" + ScrapMaxDate.MaxUpdateDate, null);
                using (TextReader treader = new StreamReader(response.GetResponseStream()))
                {
                    string str = treader.ReadToEnd();
                    msResource = JsonConvert.DeserializeObject<MS.Core.DataTransfer.Resource.ResourceOutput>(str);
                    //MSRes = msResource;
                }
                response.Close();

                if (msResource.Resources != null && msResource.Resources.Count > 0)
                {
                    foreach (MS.Core.DataTransfer.Resource.GetOutput mResource in msResource.Resources)
                    {
                        if (mResource != null)
                        {
                            NMS.Core.Entities.MResource nmsMResource = new NMS.Core.Entities.MResource();
                            nmsMResource.Guid = mResource.Guid.ToString();
                            nmsMResource.Duration = Convert.ToDouble(mResource.Duration);
                            nmsMResource.LastUpdateDate = DateTime.UtcNow;
                            nmsResourceService.UpdateResourceDuration(nmsMResource);
                            ScrapMaxDate.MaxUpdateDate = mResource.CreationDate;
                        }
                    }
                    newsService.UpdateScrapDate(ScrapMaxDate);
                }

                return "Successfull";
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("MediaDurationThread: Error downloading resource {0}", exp.Message), "", EventCodes.Error);
                return "Error";
            }
        }

        public class DurationArgument
        {
            public string Source { get; set; }
        }
    }
}
