﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Helper;
using NMS.Core.IService;
using System.Data;
using MS.Repository;
using System.Data.SqlClient;
using NMS.Core.DataInterfaces.Mongo;
using System.ComponentModel;


namespace NMS.ProcessThreads
{
    public class MongoToSQLDataSyncThread : ISystemProcessThread
    {

        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            try
            {
               string sqlConnection = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ToString();
               
                UpdateRecords("NewsFilter", "CreationDate", string.Empty, sqlConnection);

                return "Successfull";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in MongoToSQLDataSyncThread : {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        bool UpdateRecords(string tblName, string dtColumnName, string sourceDBConn, string destinationDBConn)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                DateTime dtLastUpdateDate = GetMaxDate(dtColumnName, tblName, destinationDBConn);

                DataTable dt = GetAllRecords(tblName, dtLastUpdateDate);

                if (dt.Rows.Count > 0)
                {
                    BulkInsert(dt, tblName, destinationDBConn);
                    logService.InsertSystemEventLog(string.Format("MongoToSQLDataSync Thread {0}, {1} " + tblName + " Inserted", tblName, dt.Rows.Count), "", EventCodes.Log);
                }
                return true;
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("MongoToSQLDataSync Thread {0} " + tblName + " Insert Failed {1}", tblName, exp.Message), "", EventCodes.Error);
            }
            return false;
        }

        DateTime GetMaxDate(string columnName, string tblName, string connectionString)
        {
            string sql = "select max(" + columnName + ") from " + tblName + " (nolock)";
            DataSet ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return DateTime.UtcNow.AddYears(-1);
            if (ds.Tables[0].Rows[0][0] == DBNull.Value)
                return DateTime.UtcNow.AddYears(-1);
            return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
        }

        bool HasNewRecords(string columnName, DateTime columnValue, string tblName, string connectionString)
        {

            //it will return individual data
            string sql = "select count(*) from " + tblName + " (nolock) where " + columnName + ">@LastUpdateTime";
            return Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, CommandType.Text, sql, new SqlParameter[] { new SqlParameter("@LastUpdateTime", columnValue) })) > 0;
        }

        DataTable GetAllRecords(string tblName, DateTime columnValue)
        {

            //call individitual service and return the data table 
            if (tblName == "NewsFilter")
            {
                INewsService service = IoC.Resolve<INewsService>("NewsService");
                List<NMS.Core.Entities.Mongo.MNewsFilter> objData = service.GetUpdatedNewsFilters(columnValue);
                return objData.ToDataTable();
            }


            /*
            string sql = "select * from " + tblName + " (nolock)";
            DataSet ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sql, null);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
                return ds.Tables[0];
            else
             */ 
                return null;
        }


        bool BulkInsert(DataTable dt, string tblName, string connectionString)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");

            try
            {
                using (SqlBulkCopy s = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.KeepIdentity))
                {
                   
                    
                   List<SqlBulkCopyColumnMapping> objColsMapping = MapBulkCopyColumns (tblName);

                    foreach(SqlBulkCopyColumnMapping col in objColsMapping)
                    {
                        s.ColumnMappings.Add(col);
                    }


                    s.DestinationTableName = tblName;
                    s.WriteToServer(dt);
                }
                return true;
            }
            catch (Exception ex){
                logService.InsertSystemEventLog(string.Format("MongoToSQLDataSync BulkInsert() Thread {0} Insert Failed {1}", tblName, ex.Message), "", EventCodes.Error);
                return false; 
            
            }
        }


        private List<SqlBulkCopyColumnMapping> MapBulkCopyColumns(string tblName)
        {
            List<SqlBulkCopyColumnMapping> objList = new List<SqlBulkCopyColumnMapping>();
            if (tblName == "NewsFilter")
            {
               
                

                SqlBulkCopyColumnMapping mapId = new SqlBulkCopyColumnMapping("_id", "_id");
                objList.Add(mapId);

                SqlBulkCopyColumnMapping mapFilterId = new SqlBulkCopyColumnMapping("FilterId", "FilterId");
                objList.Add(mapFilterId);

                SqlBulkCopyColumnMapping mapNewsId = new SqlBulkCopyColumnMapping("NewsId", "NewsId");
                objList.Add(mapNewsId);

                SqlBulkCopyColumnMapping mapCreationDate = new SqlBulkCopyColumnMapping("CreationDate", "CreationDate");
                objList.Add(mapCreationDate);

                SqlBulkCopyColumnMapping mapLastUpdateDate = new SqlBulkCopyColumnMapping("CreationDate", "LastUpdateDate");
                objList.Add(mapLastUpdateDate);

                SqlBulkCopyColumnMapping mapReporterId = new SqlBulkCopyColumnMapping("ReporterId", "ReporterId");
                objList.Add(mapReporterId);

            }
            return objList;
        }
    }

    public static class ExtensionMethods
    {
        /// <summary>
        /// Converts a List to a datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dt = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor property = properties[i];
                dt.Columns.Add(property.Name, property.PropertyType);
            }
            object[] values = new object[properties.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }
                dt.Rows.Add(values);
            }
            return dt;
        }
    }
}
