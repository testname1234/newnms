﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using CasparCGLib.Entities;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using MS.Core.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Repository;
using NMS.Service;
using PCRGatewayLib;
using System.Web.Script.Serialization;



namespace NMS.ProcessThreads
{
    public class MosGeneration : ControlPanel.Core.ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string MosDeviceId;
        public string MosDeviceIP;
        public string MosDevicePort;
        public string NCID;
        public int messageID;
        public string CasperDeviceIP;
        public string CasperDevicePort;
        private const int ReceiveTimeout = 20000;
        private const int SendTimeout = 20000;

        public string remoteMediaImagePath;
        public string remoteMediaVideoPath;


        public string ProcessMosByEpisode(int EpisodeId, string mosDeviceId, string MosDeviceIp, string CasperDeviceIp, string mosDevicePort, string NcId, bool IsTeleprompter, bool isFromTest = false)
        {
            NCID = NcId;
            MosDeviceId = mosDevicePort;
            messageID = Convert.ToInt32(ConfigurationManager.AppSettings["MessageID"]);
            MosDevicePort = mosDevicePort;
            ProcessMosNew(EpisodeId, IsTeleprompter, isFromTest);
            return "";
        }

        public string Execute(string argument)
        {
            ProcessMosNew(IsTeleprompter: true);
            return "Success";
        }

        private void ProcessMosNew(int? TestEpisodeId = null, bool IsTeleprompter = true, bool isFromTest = false)
        {
            string TelepropterDevices = string.Empty;
            string CasperMosPlayout = string.Empty;
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                if (TestEpisodeId == null)
                {
                    NCID = ConfigurationManager.AppSettings["Ncid"];
                    MosDeviceId = ConfigurationManager.AppSettings["MosDeviceId"];
                    messageID = Convert.ToInt32(ConfigurationManager.AppSettings["MessageID"]);
                    MosDevicePort = ConfigurationManager.AppSettings["MosDevicePort"];
                }

                IMosActiveItemService mosActivecitemService = ControlPanel.Core.IoC.Resolve<IMosActiveItemService>("MosActiveItemService");
                IMosActiveEpisodeService mosActiveEpisodeService = ControlPanel.Core.IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
                IEpisodeService episodeService = ControlPanel.Core.IoC.Resolve<IEpisodeService>("EpisodeService");
                ISlotScreenTemplateResourceService slotScreenTemplateResourceRepository = ControlPanel.Core.IoC.Resolve<ISlotScreenTemplateResourceService>("SlotScreenTemplateResourceService");
                IResourceService resourceService = ControlPanel.Core.IoC.Resolve<IResourceService>("ResourceService");
                ISlotScreenTemplateService slotscreenTemplateService = ControlPanel.Core.IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
                ISlotTemplateScreenElementService slotTemplateScreenElementService = ControlPanel.Core.IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
                ICasperTemplateService casperTemplateService = ControlPanel.Core.IoC.Resolve<ICasperTemplateService>("CasperTemplateService");
                CasperTemplateRepository casperTemplateRepository = new CasperTemplateRepository();
                ICasperTemplateItemService casperTemplatecitemService = ControlPanel.Core.IoC.Resolve<ICasperTemplateItemService>("CasperTemplateItemService");
                ICasperItemTransformationService caspercitemTransformationService = ControlPanel.Core.IoC.Resolve<ICasperItemTransformationService>("CasperItemTransformationService");
                CasperItemTransformationRepository CasperItemTransformationRepository = new CasperItemTransformationRepository();
                ITemplateScreenElementService templateScreenElementService = ControlPanel.Core.IoC.Resolve<ITemplateScreenElementService>("TemplateScreenElementService");
                EpisodePcrMosRepository mosrepository = new EpisodePcrMosRepository();

                TemplateScreenElementRepository templateScreenElementRepository = new TemplateScreenElementRepository();
                SlotScreenTemplatekeyRepository slotScreenTemplatekeyRepository = new SlotScreenTemplatekeyRepository();

                List<MosActiveEpisode> MosActiveEpisodes = null;
                if (TestEpisodeId != null)
                    MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByEpisodeId(TestEpisodeId);//Inseert                
                else
                    MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByStatus(null);//Inseert

                if (MosActiveEpisodes != null && MosActiveEpisodes.Count > 0)
                {
                    foreach (MosActiveEpisode mosEpisode in MosActiveEpisodes)
                    {
                        bool TeleprompterStatus = false;
                        bool PlayOutStatus = false;
                        bool FourWindowStatus = false;
                        string PCRMessage = "";
                        try
                        {
                            if (mosEpisode.StatusCode == 1 || mosEpisode.StatusCode == 2)
                            {
                                int EpisodeId = Convert.ToInt32(mosEpisode.EpisodeId);

                                //Code here to get set id\
                                //if setid is null default setid = 1
                                List<int> LstSetId = episodeService.GetProgramSetMappingID(EpisodeId);
                                if (LstSetId == null || LstSetId.Count == 0)
                                {
                                    LstSetId = new List<int>();
                                    LstSetId.Add(13);
                                }

                                //code end here
                                int StatusCode = Convert.ToInt32(mosEpisode.StatusCode);
                                List<SlotTemplateScreenElement> slotTemplateScreenElements = new List<SlotTemplateScreenElement>();
                                List<SlotScreenTemplateResource> slotScreenTemplateResources = slotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByEpisode(EpisodeId);
                                if (slotScreenTemplateResources != null && slotScreenTemplateResources.Count > 0)
                                {
                                    foreach (SlotScreenTemplateResource slotScreenTemplateResource in slotScreenTemplateResources)
                                    {
                                        //SlotScreenTemplate s = slotscreenTemplateService.GetSlotScreenTemplate(slotScreenTemplateResource.SlotScreenTemplateId);
                                        slotTemplateScreenElements.AddRange(slotTemplateScreenElementService.GetBySlotScreenTemplateIdWithFilter(slotScreenTemplateResource.SlotScreenTemplateId));
                                    }
                                }
                                //citems With Transformation                             
                                List<SlotScreenTemplate> slotScreenTemplates = slotscreenTemplateService.GetByEpisodeId(EpisodeId);
                                List<CasparItem> citems = new List<CasparItem>();

                                int j = 1;
                                int SequenceNumber = 0;
                                if (slotScreenTemplates != null && slotScreenTemplates.Count > 0)
                                {
                                    foreach (SlotScreenTemplate slotScreenTemplate in slotScreenTemplates)
                                    {
                                        if (slotScreenTemplate != null)
                                        {
                                            //CasperTemplate cTemplate = casperTemplateRepository.GetBYFlashTemplateId(slotScreenTemplate.flas);                                            
                                            CasperTemplate cTemplate = casperTemplateRepository.GetCasperTemplateByTemplateName(slotScreenTemplate.Name);
                                            if (cTemplate != null)
                                            {
                                                //List<CasperTemplatecitem> cTemplatecitems = casperTemplatecitemService.GetCasperTemplatecitemByCasperTemplateId(cTemplate.CasperTemlateId);
                                                List<EpisodePcrMos> cTemplatecitems = mosrepository.GetEpisodePcrMosBycasperTemplateId(Convert.ToInt32(cTemplate.FlashTemplateId), slotScreenTemplate.SlotScreenTemplateId, slotScreenTemplate.SlotId).OrderByDescending(x => x.Type).ToList();

                                                int parentTemplate = 0;
                                                int layer = 0;

                                                foreach (var cT in cTemplatecitems)
                                                {
                                                    layer++;
                                                    SequenceNumber++;
                                                    CasparItem citem = new CasparItem();
                                                    citem.CopyFrom(cT);
                                                    citem.Storyid = slotScreenTemplate.SlotId.ToString();
                                                    if (citem.Type.ToLower() == "template")
                                                    {
                                                        parentTemplate = citem.Id;
                                                        citem.FlashTemplateTypeId = cT.FlashTemplateTypeId.ToString();
                                                        citem.Label = citem.Name;
                                                        citem.Name = cT.FlashTemplateGuid;
                                                        if (!String.IsNullOrEmpty(cT.Channel) && Convert.ToInt32(cT.Channel) > 0)
                                                        {
                                                            citem.VideoWallId = cT.Channel;
                                                        }

                                                        //  Keys work commented

                                                        List<SlotScreenTemplatekey> keys = slotScreenTemplatekeyRepository.GetSlotScreenTemplatekeyBySlotScreenTemplateId(slotScreenTemplate.SlotScreenTemplateId);
                                                        if (keys != null)
                                                        {

                                                            TemplatesData data = new TemplatesData();
                                                            data.ComponentDataList = new List<ComponentData>();
                                                            foreach (SlotScreenTemplatekey key in keys)
                                                            {
                                                                ComponentData comkeys = new ComponentData();
                                                                comkeys.Id = key.KeyName;
                                                                comkeys.Value = String.IsNullOrEmpty(key.KeyValue) ? " " : key.KeyValue;
                                                                data.ComponentDataList.Add(comkeys);
                                                            }
                                                            citem.TemplateData = data;
                                                        }
                                                    }
                                                    else if (cT.ItemName.ToLower().Contains("video") || cT.ItemName.ToLower().Contains("picture") || cT.ItemName.ToLower().Contains("still"))
                                                    {
                                                        if (cT.ItemName.ToLower().Contains("video"))
                                                        {
                                                            citem.Loop = "true";
                                                        }

                                                        if (!String.IsNullOrEmpty(cT.Channel) && Convert.ToInt32(cT.Channel) > 0)
                                                        {
                                                            citem.VideoWallId = cT.Channel;
                                                        }
                                                        citem.SequenceOrder = SequenceNumber.ToString();

                                                        if (cT.IsActive == false)
                                                        {
                                                            citem.Url = ConfigurationManager.AppSettings["MediaServerUrl"] + "/getresource/" + citem.Name + "?isHD=true";
                                                        }
                                                        else
                                                        {
                                                            if (slotTemplateScreenElements != null && slotTemplateScreenElements.Count > 0)
                                                            {
                                                                //mplateScreenElement tel=templateScreenElementRepository.GetByScreenTemplateIdAndflashtemplatewindowid(slotScreenTemplate.ScreenTemplateId, Convert.ToInt32(cT.Flashtemplatewindowid));
                                                                SlotTemplateScreenElement el = slotTemplateScreenElements.Where(x => x.SlotScreenTemplateId == slotScreenTemplate.SlotScreenTemplateId && x.Flashtemplatewindowid == Convert.ToInt32(cT.Flashtemplatewindowid)).FirstOrDefault();
                                                                //SlotTemplateScreenElement el = slotTemplateScreenElements.Where(x => x.ScreenElementId == tel.ScreenElementId).FirstOrDefault();
                                                                //SlotTemplateScreenElement el = slotTemplateScreenElements[0];
                                                                if (el != null)
                                                                {
                                                                    citem.Url = ConfigurationManager.AppSettings["MediaServerUrl"] + "/getresource/" + el.ResourceGuid.ToString() + "?isHD=true";
                                                                    citem.Name = el.ResourceGuid.ToString();
                                                                }
                                                            }
                                                        }
                                                        if (slotScreenTemplateResources != null)
                                                        {
                                                            decimal? duration = slotScreenTemplateResources.Where(x => x.ResourceGuid == new Guid(citem.Name)).Select(y => y.Duration).FirstOrDefault();
                                                            string Caption = slotScreenTemplateResources.Where(x => x.ResourceGuid == new Guid(citem.Name)).Select(y => y.Caption).FirstOrDefault();
                                                            if (duration != null && duration > 0)
                                                                citem.Duration = Convert.ToInt32(duration * 1000).ToString();
                                                            else
                                                                citem.Duration = "10000";
                                                            citem.Label = string.IsNullOrEmpty(Caption) ? "Media" : Caption;
                                                        }
                                                        else
                                                        {
                                                            citem.Duration = "10000";
                                                            citem.Label = "Media";
                                                        }
                                                    }
                                                    else if (citem.Type == "DECKLINKINPUT")
                                                    {
                                                        citem.Label = EpisodeId + "_" + cT.EpisodePcrMosId;
                                                        citem.Name = cT.Portnumber.ToString();
                                                        j++;
                                                    }
                                                    int isSingle = 0;
                                                    int? FlashTemplateTypeId = (int?)Convert.ToInt32(citem.FlashTemplateTypeId);
                                                    if (citem.Type.ToLower() != "template")
                                                    {
                                                        citem.Flashtemplateid = null;
                                                        citem.FlashTemplateTypeId = null;
                                                        citem.Videolayer = layer.ToString();
                                                        citem.Templateid = (parentTemplate == 0 ? null : (int?)parentTemplate).ToString();
                                                        if (FlashTemplateTypeId == 4)
                                                        {
                                                            citem.Templateid = null;
                                                            citem.Flashtemplateid = null;
                                                            citem.FlashTemplateTypeId = null;
                                                            citem.Flashtemplatewindowid = null;
                                                            citem.Videolayer = "50";
                                                            isSingle = 1;

                                                        }
                                                    }

                                                    if (citem.Type.ToLower() == "template" && FlashTemplateTypeId == 4)
                                                    {
                                                        //citem = citem;
                                                        citem.Templateid = null;
                                                        //  parentTemplate = 0;
                                                    }
                                                    else
                                                        citems.Add(citem);
                                                    CasperItemTransformation citemTransformation = CasperItemTransformationRepository.GetByFlashtemplatewindowid(Convert.ToInt32(cT.Flashtemplatewindowid));
                                                    //CaspercitemTransformation ccitemTransformation = caspercitemTransformationService.GetCaspercitemTransformationByCasperTemplateItemId(Convert.ToInt32(cT.CasperTemplatItemId));
                                                    if (citemTransformation != null && isSingle == 0)
                                                    {
                                                        CasparItem citemT = new CasparItem();
                                                        citemT.CopyFrom(citemTransformation);
                                                        citemT.VideoWallId = citem.VideoWallId;
                                                        citemT.Storyid = slotScreenTemplate.SlotId.ToString();
                                                        citemT.Videolayer = citem.Videolayer;
                                                        citemT.Id = citemTransformation.CasperItemTransformationId;
                                                        //citemT.Templateid = cT.Templateid;
                                                        citemT.Templateid = parentTemplate.ToString();
                                                        // citemT.Flashtemplateid = cT.Flashtemplateid;
                                                        citemT.Flashtemplatewindowid = cT.Flashtemplatewindowid.ToString();
                                                        citems.Add(citemT);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                IProgramService programService = ControlPanel.Core.IoC.Resolve<IProgramService>("ProgramService");
                                Episode episode = episodeService.GetEpisode(EpisodeId);
                                NMS.Core.Entities.Program program = programService.GetProgram(episode.ProgramId);
                                List<MOS> mosObj = new List<MOS>();
                                if (citems.Count > 0)
                                {
                                    int mosEpisodeId = mosEpisode.MosActiveEpisodeId;
                                    string message = string.Empty;
                                    //NCID = "NMSRundownService";
                                    ISlotService slotService = ControlPanel.Core.IoC.Resolve<ISlotService>("slotService");
                                    ISlotScreenTemplateService slotScreenTemplateService = ControlPanel.Core.IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
                                    List<Slot> slots = slotService.GetSlotByEpisode(Convert.ToInt32(episode.EpisodeId));
                                    if (slots != null && slots.Count > 0)
                                    {
                                        int Hour = Convert.ToInt32(episode.To.Subtract(episode.From).TotalHours);
                                        int Minutes = Convert.ToInt32(episode.To.Subtract(episode.From).Minutes);
                                        int Seconds = Convert.ToInt32(episode.To.Subtract(episode.From).Seconds);
                                        List<CasparItem> Casparcitem = new List<CasparItem>();
                                        Casparcitem = citems;
                                        PCRGateway pg = new PCRGateway();
                                        MOS mos = GetROCreate(mosEpisodeId.ToString(), program.Name, episode.From, Hour.ToString() + ":" + Minutes.ToString() + ":" + Seconds.ToString(), slots);
                                        //  MOS CasperMos = new MOS();                                
                                        //Telepromper Ro                                    
                                        mos.ncsID = NCID;
                                        mos.mosID = MosDeviceId;
                                        mos.messageID = messageID;
                                        try
                                        {
                                            if (IsTeleprompter)
                                            {
                                                mos.groupID = "TELEPROMPTER";
                                                mos.SetId = LstSetId;
                                                MOS TeleMos = pg.ReceiveRunDown(mos);
                                                if (TeleMos != null)
                                                {
                                                    ROAck RoStatus = (ROAck)TeleMos.command;

                                                    //serialize
                                                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                                                    var serializedResult = serializer.Serialize(RoStatus);
                                                    TelepropterDevices = serializedResult;
                                                    TeleprompterStatus = true;

                                                    //Deserialize
                                                    //ROAck DeserializedTeleDevices = serializer.Deserialize<ROAck>(serializedResult);

                                                    //if (RoStatus.roStatus[0].ToLower() == "ok")
                                                    //    mosObj.Add(TeleMos);
                                                    //else
                                                    //    throw new Exception("Unsuccessfull Response Received From Telepropmpter" + RoStatus.roStatus);
                                                }
                                                else
                                                    throw new Exception("No Response From Telepropmpter");
                                                //if(TeleMos.command.GetType() == Notification)
                                                //TeleprompterStatus = 
                                            }
                                            if (IsTeleprompter)
                                            {
                                                foreach (Slot slot in slots)
                                                {
                                                    List<string> scripts = new List<string>();
                                                    List<SlotScreenTemplate> sSTemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slot.SlotId);
                                                    if (sSTemplates != null && sSTemplates.Count > 0)
                                                    {
                                                        scripts = sSTemplates.Select(x => Regex.Replace(x.Script, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).ToList();
                                                        // NewsDesc = WebUtility.HtmlDecode();
                                                        //MOS TeleprompterSlotsMos = GetStroySend(slot.SlotId.ToString(), slot.SequnceNumber.ToString(), mosEpisodeId.ToString(), slot.Title, scripts, slot.LanguageCode == "en" ? "1" : "2");

                                                        MOS TeleprompterSlotsMos = GetStroySend(slot.SlotId.ToString(), slot.SequnceNumber.ToString(), mosEpisodeId.ToString(), slot.Title, sSTemplates, slot.LanguageCode == "en" ? "1" : "2");
                                                        TeleprompterSlotsMos.groupID = "TELEPROMPTER";
                                                        TeleprompterSlotsMos.SetId = LstSetId;
                                                        TeleprompterSlotsMos.mosID = MosDeviceId;
                                                        TeleprompterSlotsMos.messageID = messageID;
                                                        TeleprompterSlotsMos.ncsID = NCID;
                                                        MOS TeleDetail = pg.ReceiveRunDown(TeleprompterSlotsMos);
                                                        if (TeleDetail != null)
                                                        {
                                                            ROAck RoStatus = (ROAck)TeleDetail.command;

                                                            //JavaScriptSerializer serializer = new JavaScriptSerializer();
                                                            //var serializedResult = serializer.Serialize(RoStatus);
                                                            //TelepropterSlots = serializedResult;

                                                            //ROAck DeserializedTeleSlots = serializer.Deserialize<ROAck>(serializedResult);


                                                            //if (RoStatus.roStatus.ToLower() == "ok")
                                                            //    mosObj.Add(TeleDetail);
                                                            //else
                                                            //    throw new Exception("Unsuccessfull Response Received From Telepropmpter Story Detail" + RoStatus.roStatus);
                                                        }
                                                        else
                                                            throw new Exception("No Response From Telepropmpter Story Detail");
                                                    }
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            PCRMessage += ex.Message + "::StackTrace= " + ex.StackTrace;
                                            if (ex.InnerException != null)
                                                PCRMessage += ex.InnerException.Message + "::StackTrace= " + ex.InnerException.StackTrace;
                                            TeleprompterStatus = false;
                                        }
                                        //Casper MOs                                                                            
                                        MOS CasperMos = GetROCreate(mosEpisodeId.ToString(), program.Name, episode.From, Hour.ToString() + ":" + Minutes.ToString() + ":" + Seconds.ToString(), slots, Casparcitem);
                                        CasperMos.groupID = "COMPUTERGRAPHICS";
                                        CasperMos.SetId = LstSetId;
                                        CasperMos.ncsID = NCID;
                                        CasperMos.mosID = MosDeviceId;
                                        CasperMos.messageID = messageID;
                                        try
                                        {
                                            MOS casperMos = pg.ReceiveRunDown(CasperMos);
                                            if (casperMos != null)
                                            {
                                                mosObj.Add(casperMos);
                                                JavaScriptSerializer serializer = new JavaScriptSerializer();
                                                ROAck RoStatus = (ROAck)casperMos.command;
                                                if (RoStatus != null)
                                                {
                                                    var serializedResult = serializer.Serialize(RoStatus);
                                                    CasperMosPlayout = serializedResult;
                                                }
                                                PlayOutStatus = true;
                                                FourWindowStatus = true;
                                            }

                                            else
                                            {
                                                throw new Exception("No Response From CG Server");
                                                PlayOutStatus = false;
                                                FourWindowStatus = false;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            PCRMessage += ex.Message + "::StackTrace= " + ex.StackTrace;
                                            if (ex.InnerException != null)
                                                PCRMessage += ex.InnerException.Message + "::StackTrace= " + ex.InnerException.StackTrace;
                                            PlayOutStatus = false;
                                            FourWindowStatus = false;
                                        }
                                    }
                                    if (mosObj.Count > 0)
                                    {
                                        // StoreRunOrderAndTelepromterLog(mosObj);
                                    }
                                }
                            }
                            if (FourWindowStatus && PlayOutStatus && TeleprompterStatus)
                                mosEpisode.StatusCode = (int)MosEpisodeStatus.SentToPcr;
                            else
                                mosEpisode.StatusCode = (int)MosEpisodeStatus.Error;
                            mosEpisode.StatusDescription = PCRMessage;
                            mosEpisode.PcrFourWindowStatus = FourWindowStatus;
                            mosEpisode.PcrPlayoutStatus = PlayOutStatus;
                            mosEpisode.PcrTelePropterStatus = TeleprompterStatus;
                            mosEpisode.LastUpdateDate = DateTime.UtcNow;

                            //Add your three three variables here 
                            mosEpisode.PcrTelepropterJson = TelepropterDevices;
                            mosEpisode.PcrPlayoutJson = CasperMosPlayout;

                            if (!isFromTest)
                                mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
                        }
                        catch (Exception exp)
                        {
                            mosEpisode.StatusCode = (int)MosEpisodeStatus.Error;
                            mosEpisode.StatusDescription = PCRMessage;
                            mosEpisode.PcrFourWindowStatus = FourWindowStatus;
                            mosEpisode.PcrPlayoutStatus = PlayOutStatus;
                            mosEpisode.PcrTelePropterStatus = TeleprompterStatus;
                            mosEpisode.LastUpdateDate = DateTime.UtcNow;

                            mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
                            logService.InsertSystemEventLog(string.Format("Error in MosGeneration: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                logException(exp);
                logService.InsertSystemEventLog(string.Format("Error in MosGeneration: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
            }
        }


        //public static byte[] Serialize(List<Telepropter> tData)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        ProtoBuf.Serializer.Serialize(ms, tData);
        //        return ms.ToArray();
        //    }
        //}

        public void logException(Exception exp)
        {
            File.AppendAllLines("c:\\logService.txt", new string[] { exp.Message, exp.StackTrace });
            if (exp.InnerException != null) logException(exp.InnerException);
        }

        public MOS GetStroySend(string storyID, string storyNumber, string runDownId, string storyTitle, List<SlotScreenTemplate> SlotScreenTemplates, string LanguageCode)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROStorySend roStorySend = new ROStorySend();
            roStorySend.roID = runDownId;
            roStorySend.storyID = storyID;
            roStorySend.storySlug = storyTitle;
            roStorySend.storyNum = storyNumber;
            List<StoryBody> storyBodies = new List<StoryBody>();


            foreach (SlotScreenTemplate sTemplete in SlotScreenTemplates)
            {
                StoryBody storyBody = new StoryBody();
                storyBody.ParagraphWithInstructions = String.IsNullOrEmpty(sTemplete.Instructions) ? "OC" : sTemplete.Instructions;
                storyBody.Paragraph = Regex.Replace(sTemplete.Script, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase).ToString();
                storyBody.LanguageCode = LanguageCode;
                storyBodies.Add(storyBody);
            }
            roStorySend.storyBodies = storyBodies;
            command.command = roStorySend;
            return command;
        }

        public void StoreRunOrderAndTelepromterLog(List<MOS> mos)
        {
            IRunOrderLogService runOrderLogService = ControlPanel.Core.IoC.Resolve<IRunOrderLogService>("RunOrderLogService");
            IRunOrderStoriesService runOrderStoriesService = ControlPanel.Core.IoC.Resolve<IRunOrderStoriesService>("RunOrderStoriesService");
            if (mos != null && mos.Count > 0)
            {
                foreach (MOS obj1 in mos)
                {
                    ROAck ro = new ROAck();
                    ro.CopyFrom(obj1.command);
                    RunOrderLog roLOG = new RunOrderLog();
                    roLOG.CopyFrom(obj1);
                    roLOG.RoId = Convert.ToInt32(ro.roID);
                    roLOG.RoStatus = ro.roStatus;
                    roLOG.MosId = obj1.mosID;
                    roLOG.MessageId = obj1.messageID;
                    roLOG.NcsId = obj1.ncsID;
                    if (ro.roAckStories != null && ro.roAckStories.Count > 0)
                    {
                        roLOG.RoStoriesCount = ro.roAckStories.Count();
                    }
                    else
                    {
                        roLOG.RoStoriesCount = 0;
                    }
                    RunOrderLog roLog = runOrderLogService.InsertRunOrderLog(roLOG);

                    if (ro.roAckStories != null && ro.roAckStories.Count > 0)
                    {
                        foreach (ROAckStory roStory in ro.roAckStories)
                        {
                            RunOrderStories roStories = new RunOrderStories();
                            roStories.CopyFrom(roStory);
                            roStories.RunOrderLogId = roLog.RunOrderLogId;
                            roStories.ItemChannel = roStory.itemChannel;
                            roStories.ItemId = Convert.ToInt32(roStory.itemID);
                            roStories.ObjId = Convert.ToInt32(roStory.objID);
                            roStories.Status = Convert.ToString(roStory.status);
                            roStories.StoryId = Convert.ToInt32(roStory.storyID);
                            roStories.StoryStatus = Convert.ToString(roStory.storyStatus);
                            runOrderStoriesService.InsertRunOrderStories(roStories);
                        }
                    }
                }
            }
        }

        public MOS GetROCreate(string runDownId, string ProgramName, DateTime StartTime, String EndTime, List<Slot> slots, List<CasparItem> casparcitems = null)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROCreate ro = new ROCreate();
            ro.roID = runDownId;
            ro.roSlug = ProgramName + "_" + StartTime.ToString();        //Episode Nam and Duration
            ro.roEdStart = StartTime.ToString();
            ro.roEdDur = EndTime; //00:59:00
            ro.roTrigger = "TIMED";
            if (casparcitems != null)
                ro.casparItems = casparcitems;
            ro.story = new System.Collections.Generic.List<Story>();

            foreach (Slot slot in slots)
            {

                Story story = new Story();
                story.storyID = slot.SlotId.ToString();    //Slot pk
                story.storySlug = slot.Title;   //Slot Title
                story.LanguageCode = slot.LanguageCode == "en" ? "1" : "2";
                story.storyNum = slot.SequnceNumber.ToString();
                ro.story.Add(story);
            }
            command.command = ro;
            return command;
        }

        public static string GetStringForMOSCommand<T>(T command, string RootArgument)
        {
            StringBuilder stringBuilder = new StringBuilder();

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.OmitXmlDeclaration = true;

            XmlSerializerNamespaces xmlEmptyNameSpace = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });

            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlWriterSettings);

            XmlRootAttribute root = new XmlRootAttribute(RootArgument);
            XmlSerializer xml = new XmlSerializer(typeof(T), root);
            xml.Serialize(xmlWriter, command, xmlEmptyNameSpace);

            return stringBuilder.ToString();
        }


    }
}
