﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using CasparCGLib.Entities;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using MS.Core.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Repository;
using NMS.Service;
using PCRGatewayLib;
using System.Web.Script.Serialization;
using NMS.Core;
using NMS.Core.DataTransfer.NewsFile.GET;



namespace NMS.ProcessThreads
{
    public class MosGenerationOptimized : ControlPanel.Core.ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string MosDeviceId;
        public string MosDeviceIP;
        public string MosDevicePort;
        public string NCID;
        public int messageID;
        public string CasperDeviceIP;
        public string CasperDevicePort;
        private const int ReceiveTimeout = 20000;
        private const int SendTimeout = 20000;

        public string remoteMediaImagePath;
        public string remoteMediaVideoPath;


        public string ProcessMosByEpisode(int EpisodeId, string mosDeviceId, string MosDeviceIp, string CasperDeviceIp, string mosDevicePort, string NcId, bool IsTeleprompter, bool isFromTest = false)
        {
            NCID = NcId;
            MosDeviceId = mosDevicePort;
            messageID = Convert.ToInt32(ConfigurationManager.AppSettings["MessageID"]);
            MosDevicePort = mosDevicePort;
            ProcessMosNew(EpisodeId, IsTeleprompter, isFromTest);
            return "";
        }

        public string Execute(string argument)
        {
            ProcessMosNew(IsTeleprompter: true);
            return "Success";
        }

        //public string PCRType(int elementId)
        //{
        //    switch(elementId)
        //    {case 1:
        //        return "DECKLINKINPUT";
        //            break;
        //    }
        //    MOVIE = 3,
        //    Graphic = 4,
        //    DECKLINKINPUT = 8,
        //    STILL = 13,
        //    Logo = 19,
        //    Headline = 20,
        //    Ticker = 21,
        //    Wall = 22

        //}

        private void ProcessMosNew(int? TestEpisodeId = null, bool IsTeleprompter = true, bool isFromTest = false)
        {
            string TelepropterDevices = string.Empty;
            string CasperMosPlayout = string.Empty;
            ISystemEventLogService logService = ControlPanel.Core.IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                if (TestEpisodeId == null)
                {
                    NCID = ConfigurationManager.AppSettings["Ncid"];
                    MosDeviceId = ConfigurationManager.AppSettings["MosDeviceId"];
                    messageID = Convert.ToInt32(ConfigurationManager.AppSettings["MessageID"]);
                    MosDevicePort = ConfigurationManager.AppSettings["MosDevicePort"];
                }

                IMosActiveItemService mosActivecitemService = ControlPanel.Core.IoC.Resolve<IMosActiveItemService>("MosActiveItemService");
                IMosActiveEpisodeService mosActiveEpisodeService = ControlPanel.Core.IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
                IEpisodeService episodeService = ControlPanel.Core.IoC.Resolve<IEpisodeService>("EpisodeService");
                ISlotScreenTemplateResourceService slotScreenTemplateResourceRepository = ControlPanel.Core.IoC.Resolve<ISlotScreenTemplateResourceService>("SlotScreenTemplateResourceService");
                IResourceService resourceService = ControlPanel.Core.IoC.Resolve<IResourceService>("ResourceService");
                ISlotTemplateScreenElementService slotTemplateScreenElementService = ControlPanel.Core.IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
                ICasperTemplateService casperTemplateService = ControlPanel.Core.IoC.Resolve<ICasperTemplateService>("CasperTemplateService");
                CasperTemplateRepository casperTemplateRepository = new CasperTemplateRepository();
                ICasperTemplateItemService casperTemplatecitemService = ControlPanel.Core.IoC.Resolve<ICasperTemplateItemService>("CasperTemplateItemService");
                ICasperItemTransformationService caspercitemTransformationService = ControlPanel.Core.IoC.Resolve<ICasperItemTransformationService>("CasperItemTransformationService");
                CasperItemTransformationRepository CasperItemTransformationRepository = new CasperItemTransformationRepository();
                ITemplateScreenElementService templateScreenElementService = ControlPanel.Core.IoC.Resolve<ITemplateScreenElementService>("TemplateScreenElementService");
                TemplateScreenElementRepository templateScreenElementRepository = new TemplateScreenElementRepository();
                SlotScreenTemplatekeyRepository slotScreenTemplatekeyRepository = new SlotScreenTemplatekeyRepository();
                SlotScreenTemplateRepository slotScreenTemplateRepository = new SlotScreenTemplateRepository();
                SlotTemplateScreenElementResourceRepository slotTemplateScreenElementResourceRepository = new SlotTemplateScreenElementResourceRepository();
                ScreenTemplateRepository screenTemplateRepository = new ScreenTemplateRepository();
                INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
                FileDetailRepository fileDetailRepo = new FileDetailRepository();


                List<MosActiveEpisode> MosActiveEpisodes = null;
                if (TestEpisodeId != null)
                    MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByEpisodeId(TestEpisodeId);//Inseert                
                else
                    MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByStatus(null);//Inseert

                if (MosActiveEpisodes != null && MosActiveEpisodes.Count > 0)
                {
                    foreach (MosActiveEpisode mosEpisode in MosActiveEpisodes)
                    {
                        bool TeleprompterStatus = false;
                        bool PlayOutStatus = false;
                        bool FourWindowStatus = false;
                        string PCRMessage = "";
                        try
                        {
                            if (mosEpisode.StatusCode == 1 || mosEpisode.StatusCode == 2)
                            {
                                int EpisodeId = Convert.ToInt32(mosEpisode.EpisodeId);
                                List<int> LstSetId = episodeService.GetProgramSetMappingID(EpisodeId);
                                if (LstSetId == null || LstSetId.Count == 0)
                                {
                                    LstSetId = new List<int>();
                                    LstSetId.Add(13);
                                }
                                NMSPollingOutput newsFolder = new NMSPollingOutput();
                                newsFolder = fileService.GetRundownByEpisodeIdFromDB(Convert.ToInt32(EpisodeId));
                                List<SlotScreenTemplate> slotScreenTemplates = slotScreenTemplateRepository.GetByEpisodeId(EpisodeId);
                                List<CasparItem> citems = new List<CasparItem>();
                                int SequenceNumber = 0;

                                if (newsFolder != null && newsFolder.FileResource.Count > 0)
                                {

                                    foreach (NMS.Core.DataTransfer.FileResource.GetOutput fileresource in newsFolder.FileResource)
                                    {
                                        SequenceNumber++;
                                        CasparItem citemElement = new CasparItem();
                                        citemElement.Id = fileresource.FileResourceId;
                                        citemElement.Storyid = Convert.ToString(fileresource.NewsFileId);
                                        citemElement.SequenceOrder = SequenceNumber.ToString();
                                        citemElement.Url = ConfigurationManager.AppSettings["MediaServerUrl"] + "/getresource/" + fileresource.Guid.ToString() + "?isHD=true";
                                        citemElement.Name = fileresource.Guid.ToString();
                                        decimal? duration = 0;
                                        string Caption = fileresource.Caption;
                                        if (duration != null && duration > 0)
                                            citemElement.Duration = Convert.ToInt32(duration * 1000).ToString();
                                        else
                                            citemElement.Duration = "10000";
                                        citemElement.Label = string.IsNullOrEmpty(Caption) ? "Media" : Caption;
                                        // if (!((screentemplate.GroupId == "T_3" && screentemplate.RatioTypeId == null) || screentemplate.GroupId == "T_13"))
                                        //    citemElement.Templateid = slotScreenTemplate.SlotScreenTemplateId.ToString();
                                        // citemElement.Flashtemplatewindowid = slotTemplateScreenElement.Flashtemplatewindowid == null ? "0" : slotTemplateScreenElement.Flashtemplatewindowid.ToString();
                                        if (fileresource.ResourceTypeId == 1)
                                            citemElement.Type = "STILL";
                                        else if (fileresource.ResourceTypeId == 2)
                                            citemElement.Type = "MOVIE";
                                        else
                                            citemElement.Type = "STILL";
                                        citems.Add(citemElement);   //Addition of Element
                                    }

                                }

                                IProgramService programService = ControlPanel.Core.IoC.Resolve<IProgramService>("ProgramService");
                                Episode episode = episodeService.GetEpisode(EpisodeId);
                                NMS.Core.Entities.Program program = programService.GetProgram(episode.ProgramId);
                                List<MOS> mosObj = new List<MOS>();
                                if (citems.Count > 0)
                                {
                                    int mosEpisodeId = mosEpisode.MosActiveEpisodeId;
                                    string message = string.Empty;
                                    if (newsFolder != null && newsFolder.NewsFile.Count > 0)
                                    {
                                        int Hour = Convert.ToInt32(episode.To.Subtract(episode.From).TotalHours);
                                        int Minutes = Convert.ToInt32(episode.To.Subtract(episode.From).Minutes);
                                        int Seconds = Convert.ToInt32(episode.To.Subtract(episode.From).Seconds);
                                        List<CasparItem> Casparcitem = new List<CasparItem>();
                                        Casparcitem = citems;
                                        PCRGateway pg = new PCRGateway();
                                        MOS mos = GetROCreate(mosEpisodeId.ToString(), program.Name, episode.From, Hour.ToString() + ":" + Minutes.ToString() + ":" + Seconds.ToString(), newsFolder.NewsFile);
                                        mos.ncsID = NCID;
                                        mos.mosID = MosDeviceId;
                                        mos.messageID = messageID;
                                        try
                                        {
                                            if (IsTeleprompter)
                                            {
                                                mos.groupID = "TELEPROMPTER";
                                                mos.SetId = LstSetId;
                                                MOS TeleMos = pg.ReceiveRunDown(mos);
                                                if (TeleMos != null)
                                                {
                                                    ROAck RoStatus = (ROAck)TeleMos.command;
                                                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                                                    var serializedResult = serializer.Serialize(RoStatus);
                                                    TelepropterDevices = serializedResult;
                                                    TeleprompterStatus = true;
                                                }
                                                else
                                                    throw new Exception("No Response From Telepropmpter");
                                            }
                                            if (IsTeleprompter)
                                            {
                                                foreach (NMS.Core.DataTransfer.NewsFile.GetOutput newsFile in newsFolder.NewsFile)
                                                {
                                                    List<string> scripts = new List<string>();
                                                    List<FileDetail> newsFileDetailLst = fileDetailRepo.GetFileDetailByNewsFileId(newsFile.NewsFileId).ToList();
                                                    if (newsFileDetailLst != null && newsFileDetailLst.Count > 0)
                                                    {
                                                        scripts = newsFileDetailLst.Select(x => Regex.Replace(x.Text, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).ToList();
                                                        MOS TeleprompterSlotsMos = GetStroySend(newsFile.NewsFileId.ToString(), newsFile.SequenceNo.ToString(), mosEpisodeId.ToString(), newsFile.Title, newsFileDetailLst, newsFile.LanguageCode == "en" ? "1" : "2");
                                                        TeleprompterSlotsMos.groupID = "TELEPROMPTER";
                                                        TeleprompterSlotsMos.SetId = LstSetId;
                                                        TeleprompterSlotsMos.mosID = MosDeviceId;
                                                        TeleprompterSlotsMos.messageID = messageID;
                                                        TeleprompterSlotsMos.ncsID = NCID;
                                                        MOS TeleDetail = pg.ReceiveRunDown(TeleprompterSlotsMos);
                                                        if (TeleDetail != null)
                                                        {
                                                            ROAck RoStatus = (ROAck)TeleDetail.command;
                                                        }
                                                        else
                                                            throw new Exception("No Response From Telepropmpter Story Detail");
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            PCRMessage += ex.Message + "::StackTrace= " + ex.StackTrace;
                                            if (ex.InnerException != null)
                                                PCRMessage += ex.InnerException.Message + "::StackTrace= " + ex.InnerException.StackTrace;
                                            TeleprompterStatus = false;
                                        }

                                        //Casper MOs                                                                            
                                        MOS CasperMos = GetROCreate(mosEpisodeId.ToString(), program.Name, episode.From, Hour.ToString() + ":" + Minutes.ToString() + ":" + Seconds.ToString(), newsFolder.NewsFile, Casparcitem);
                                        CasperMos.groupID = "COMPUTERGRAPHICS";
                                        CasperMos.SetId = LstSetId;
                                        CasperMos.ncsID = NCID;
                                        CasperMos.mosID = MosDeviceId;
                                        CasperMos.messageID = messageID;
                                        try
                                        {
                                            MOS casperMos = pg.ReceiveRunDown(CasperMos);
                                            if (casperMos != null)
                                            {
                                                mosObj.Add(casperMos);
                                                JavaScriptSerializer serializer = new JavaScriptSerializer();
                                                ROAck RoStatus = (ROAck)casperMos.command;
                                                if (RoStatus != null)
                                                {
                                                    var serializedResult = serializer.Serialize(RoStatus);
                                                    CasperMosPlayout = serializedResult;
                                                }
                                                PlayOutStatus = true;
                                                FourWindowStatus = true;
                                            }
                                            else
                                            {
                                                throw new Exception("No Response From CG Server");
                                                PlayOutStatus = false;
                                                FourWindowStatus = false;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            PCRMessage += ex.Message + "::StackTrace= " + ex.StackTrace;
                                            if (ex.InnerException != null)
                                                PCRMessage += ex.InnerException.Message + "::StackTrace= " + ex.InnerException.StackTrace;
                                            PlayOutStatus = false;
                                            FourWindowStatus = false;
                                        }
                                    }
                                    if (mosObj.Count > 0)
                                    {
                                        // StoreRunOrderAndTelepromterLog(mosObj);
                                    }
                                }
                                else
                                {
                                    //InCoplete Or Invalid Rundown KIndly Verify Rundown.
                                    PCRMessage = "InComplete Or Invalid Rundown Kindly Verify your Rundown.";
                                }
                            }
                            if (FourWindowStatus && PlayOutStatus && TeleprompterStatus)
                                mosEpisode.StatusCode = (int)MosEpisodeStatus.SentToPcr;
                            else
                                mosEpisode.StatusCode = (int)MosEpisodeStatus.Error;
                            mosEpisode.StatusDescription = PCRMessage;
                            mosEpisode.PcrFourWindowStatus = FourWindowStatus;
                            mosEpisode.PcrPlayoutStatus = PlayOutStatus;
                            mosEpisode.PcrTelePropterStatus = TeleprompterStatus;
                            mosEpisode.LastUpdateDate = DateTime.UtcNow;

                            //Add your three three variables here 
                            mosEpisode.PcrTelepropterJson = TelepropterDevices;
                            mosEpisode.PcrPlayoutJson = CasperMosPlayout;

                            if (!isFromTest)
                                mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
                        }
                        catch (Exception exp)
                        {
                            mosEpisode.StatusCode = (int)MosEpisodeStatus.Error;
                            mosEpisode.StatusDescription = PCRMessage;
                            mosEpisode.PcrFourWindowStatus = FourWindowStatus;
                            mosEpisode.PcrPlayoutStatus = PlayOutStatus;
                            mosEpisode.PcrTelePropterStatus = TeleprompterStatus;
                            mosEpisode.LastUpdateDate = DateTime.UtcNow;

                            mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
                            logService.InsertSystemEventLog(string.Format("Error in MosGeneration: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                logException(exp);
                logService.InsertSystemEventLog(string.Format("Error in MosGeneration: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
            }
        }

        public void logException(Exception exp)
        {
            File.AppendAllLines("c:\\logService.txt", new string[] { exp.Message, exp.StackTrace });
            if (exp.InnerException != null) logException(exp.InnerException);
        }

        public MOS GetStroySend(string storyID, string storyNumber, string runDownId, string storyTitle, List<FileDetail> fileDetails, string LanguageCode)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROStorySend roStorySend = new ROStorySend();
            roStorySend.roID = runDownId;
            roStorySend.storyID = storyID;
            roStorySend.storySlug = storyTitle;
            roStorySend.storyNum = storyNumber;
            List<StoryBody> storyBodies = new List<StoryBody>();
            var newList = fileDetails.OrderByDescending(x => x.CreationDate).ToList();
            foreach (FileDetail sTemplete in newList)
            {
                StoryBody storyBody = new StoryBody();
                storyBody.ParagraphWithInstructions = "OC";
                string desc = Regex.Replace(sTemplete.Text, "(?!<br?.?.>)(?!<p>)<.*?>", "", RegexOptions.IgnoreCase);     //remove html except selected line break tags
                desc = Regex.Replace(sTemplete.Text, "<.*?>", "\r\n", RegexOptions.IgnoreCase);
                storyBody.Paragraph = desc;
                storyBody.LanguageCode = LanguageCode;
                storyBodies.Add(storyBody);
                break;
            }
            roStorySend.storyBodies = storyBodies;
            command.command = roStorySend;
            return command;
        }

        public MOS GetROCreate(string runDownId, string ProgramName, DateTime StartTime, String EndTime, List<NMS.Core.DataTransfer.NewsFile.GetOutput> slots, List<CasparItem> casparcitems = null)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROCreate ro = new ROCreate();
            ro.roID = runDownId;
            ro.roSlug = ProgramName + "_" + StartTime.ToString();        //Episode Nam and Duration
            ro.roEdStart = StartTime.ToString();
            ro.roEdDur = EndTime; //00:59:00
            ro.roTrigger = "TIMED";
            if (casparcitems != null)
                ro.casparItems = casparcitems;
            ro.story = new System.Collections.Generic.List<Story>();
            int i = 0;
            foreach (NMS.Core.DataTransfer.NewsFile.GetOutput slot in slots)
            {
                i = +1;
                Story story = new Story();
                story.storyID = slot.NewsFileId.ToString();    //Slot pk
                story.storySlug = slot.Slug;   //Slot Title
                story.LanguageCode = slot.LanguageCode == "en" ? "1" : "2";
                story.storyNum = i.ToString();//slot.SequenceNo.ToString();
                ro.story.Add(story);
            }
            command.command = ro;
            return command;
        }

        public static string GetStringForMOSCommand<T>(T command, string RootArgument)
        {
            StringBuilder stringBuilder = new StringBuilder();

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.OmitXmlDeclaration = true;

            XmlSerializerNamespaces xmlEmptyNameSpace = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });

            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlWriterSettings);

            XmlRootAttribute root = new XmlRootAttribute(RootArgument);
            XmlSerializer xml = new XmlSerializer(typeof(T), root);
            xml.Serialize(xmlWriter, command, xmlEmptyNameSpace);

            return stringBuilder.ToString();
        }

    }
}
