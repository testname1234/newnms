﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Repository;

namespace NMS.ProcessThreads
{
    public class NewsFeedsThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }


        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            ICategoryService categoryservice = IoC.Resolve<ICategoryService>("CategoryService");
            ILocationService locationservice = IoC.Resolve<ILocationService>("LocationService");
            try
            {
                ImportNewsThreadArgument arg = new ImportNewsThreadArgument();
              //  arg = JsonConvert.DeserializeObject<ImportNewsThreadArgument>(argument);

                INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
                IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
                FilterRepository _iFilterRepository = new FilterRepository();

                string ReuterPath = @"\\10.3.12.143\\Reuters\\XML";
                string RueterMediaPath = @"\\10.3.12.143\\Reuters\\SD";
                // string AptPath = @"\\10.3.12.143\\Reuters\\XML";

                arg.Source = "Reuters";
                arg.Language = "en";
                arg.SourceType = 9;

                //arg.Source = "APTN";
               // arg.Language = "en";
               // arg.SourceType = 9;
                arg.FolderPath = ReuterPath;

                ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                ScrapMaxDate.SourceName = arg.Source;
                ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-30);
                DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);
                int Newscounter = 0;
                int TotalCount = 0;
                DateTime dtNow = DateTime.UtcNow;
                DirectoryInfo info = new DirectoryInfo(arg.FolderPath);
                FileInfo[] files = info.GetFiles("*.xml").Where(p => p.CreationTime >= dtMaxScrapDate).ToArray();
                if (files != null && files.Length > 0)
                {
                    files = files.OrderBy(x => x.CreationTime).ToArray();
                    TotalCount = files.Length;
                    foreach (FileInfo file in files)
                    {
                        RawNews news = new RawNews();
                        HtmlDocument doc = new HtmlDocument();
                        InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;
                        doc.Load(file.FullName);
                        string NewsTitle = string.Empty;
                        if (arg.Source == "Reuters")
                        {
                            NewsTitle = doc.DocumentNode.SelectSingleNode("//headline").InnerText;
                        }
                        else
                        {
                            NewsTitle = doc.DocumentNode.SelectSingleNode("//entry//title").InnerText;
                        }
                        news.Title = NewsTitle;
                        news.Source = arg.Source;
                        Console.WriteLine("News Ready to Check " + news.Title);
                        //fileService.CheckIfNewsExistsSQL(news.Title, news.Source);
                        //var DiscrepencyList = newsService.GetDescrepancyByTitleAndSource(news.Title, arg.Source);
                        if (news.Title.Length > 1 && !fileService.CheckIfNewsExistsSQL(news.Title, news.Source))
                        {
                            string NewsDate = string.Empty;
                            string NewsSlug = string.Empty;
                            string NewsLocation = string.Empty;
                            string Newscategory = string.Empty;
                            string NewsDesc = string.Empty;
                            string NewsVideoResource = string.Empty;
                            string ResourceFormat = string.Empty;
                            if (arg.Source == "Reuters")
                            {
                                NewsDate = doc.DocumentNode.SelectSingleNode("//versioncreated") == null ? "" : doc.DocumentNode.SelectSingleNode("//versioncreated").InnerText;
                                NewsSlug = doc.DocumentNode.SelectSingleNode("//slugline") == null ? "" : doc.DocumentNode.SelectSingleNode("//slugline").InnerText;
                                Newscategory = doc.DocumentNode.SelectSingleNode("//subject//name") == null ? "" : doc.DocumentNode.SelectSingleNode("//subject//name").InnerText;
                                NewsLocation = doc.DocumentNode.SelectSingleNode("//located//name") == null ? "" : doc.DocumentNode.SelectSingleNode("//located//name").InnerText;
                                NewsDesc = doc.DocumentNode.SelectSingleNode("//contentset//inlinexml//body") == null ? "" : doc.DocumentNode.SelectSingleNode("//contentset//inlinexml//body").InnerHtml;
                                HtmlNode resourceNode = doc.DocumentNode.SelectSingleNode("//remotecontent[@contenttype='video/mp4']//*[contains(@type,'Len65')]");
                                NewsVideoResource = "";
                                if (resourceNode != null)
                                    NewsVideoResource = resourceNode.InnerText;
                                ResourceFormat = ".mpg";
                                //For Resources
                            }
                            else//APTN
                            {
                                NewsDate = doc.DocumentNode.SelectSingleNode("//published") == null ? "" : doc.DocumentNode.SelectSingleNode("//published").InnerText;
                                NewsSlug = doc.DocumentNode.SelectSingleNode("//*[name()='apcm:slugline']") == null ? "" : doc.DocumentNode.SelectSingleNode("//*[name()='apcm:slugline']").InnerText;
                                Newscategory = doc.DocumentNode.SelectSingleNode("//*[name()='apcm:seriesline']") == null ? "" : doc.DocumentNode.SelectSingleNode("//*[name()='apcm:seriesline']").InnerText;
                                NewsLocation = doc.DocumentNode.SelectSingleNode("//*[name()='apcm:dateline']") == null ? "" : doc.DocumentNode.SelectSingleNode(("//*[name()='apcm:dateline']")).InnerText;
                                if (NewsLocation.IndexOf("-") != -1)
                                    NewsLocation = NewsLocation.Substring(0, NewsLocation.IndexOf("-")).Trim();
                                NewsDesc = doc.DocumentNode.SelectSingleNode("//content//*[name()='apxh:div']") == null ? "" : doc.DocumentNode.SelectSingleNode("//content//*[name()='apxh:div']").InnerHtml;
                                if (!String.IsNullOrEmpty(NewsDesc))
                                {
                                    NewsDesc = NewsDesc.Replace("apxh:p", "p");
                                }
                                string ResourceId = doc.DocumentNode.SelectSingleNode("//*[@field='StoryNumber']") == null ? "" : doc.DocumentNode.SelectSingleNode("//*[@field='StoryNumber']").Attributes["Id"].Value;
                                ResourceFormat = doc.DocumentNode.SelectSingleNode("//link[last()]") == null ? "" : doc.DocumentNode.SelectSingleNode("//link[last()]").Attributes["href"].Value;
                                logService.InsertSystemEventLog(string.Format("Log in NewsFeedsThread: {0}", ResourceId + ResourceFormat), NewsTitle, EventCodes.Log);
                                if (!String.IsNullOrEmpty(ResourceId))
                                {
                                    logService.InsertSystemEventLog(string.Format("Log in NewsFeedsThread 2: {0}", ResourceId + ResourceFormat), NewsTitle, EventCodes.Log);
                                    if (string.IsNullOrEmpty(ResourceFormat))
                                        ResourceFormat = ".mpg";
                                    else
                                    {
                                        int charlength = ResourceFormat.LastIndexOf(".");
                                        ResourceFormat = ResourceFormat.Substring(charlength, ResourceFormat.Length - charlength);
                                    }
                                    if (string.IsNullOrEmpty(ResourceFormat))
                                        ResourceFormat = ".mpg";
                                    NewsVideoResource = ResourceId + "_" + NewsSlug + ResourceFormat;

                                    if (!File.Exists(arg.FolderPath + @"\" + NewsVideoResource))
                                    {
                                        System.Threading.Thread.Sleep(60000 * 15);  //Sleep to find resource
                                        ResourceFormat = ".mpg";
                                        NewsVideoResource = ResourceId + "_" + NewsSlug + ResourceFormat;
                                        logService.InsertSystemEventLog(string.Format("Log in NewsFeedsThread 3: {0}", NewsVideoResource), NewsTitle, EventCodes.Log);
                                        if (!File.Exists(arg.FolderPath + @"\" + NewsVideoResource))
                                        {
                                            ResourceFormat = ".mpg";
                                            NewsVideoResource = ResourceId + "_" + NewsSlug + ResourceFormat;
                                            logService.InsertSystemEventLog(string.Format("Log in NewsFeedsThread 4: {0}", NewsVideoResource), NewsTitle, EventCodes.Log);
                                            if (!File.Exists(arg.FolderPath + @"\" + NewsVideoResource))
                                            {
                                                ResourceFormat = ".wmv";
                                                NewsVideoResource = ResourceId + "_" + NewsSlug + ResourceFormat;
                                                logService.InsertSystemEventLog(string.Format("Log in NewsFeedsThread 5: {0}", NewsVideoResource), NewsTitle, EventCodes.Log);
                                                if (!File.Exists(arg.FolderPath + @"\" + NewsVideoResource))
                                                {
                                                    NewsVideoResource = "";
                                                }
                                            }
                                        }
                                    }
                                    logService.InsertSystemEventLog(string.Format("Log in NewsFeedsThread 6: {0}", NewsVideoResource), NewsTitle, EventCodes.Log);
                                }
                            }

                            news.Title = NewsTitle;
                            news.Slug = NewsSlug;
                            news.Location = NewsLocation;
                            news.Categories = new List<string>();
                            if (!String.IsNullOrEmpty(Newscategory))
                            {
                                news.Categories.Add(Newscategory.Trim());
                            }
                            if (!String.IsNullOrEmpty(NewsVideoResource))
                            {
                                Console.WriteLine("News Resource Found ");
                                string urlPrefix = arg.FolderPath;
                                MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                resource.Source = RueterMediaPath + @"\" + NewsVideoResource;
                                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                                resource.IsFromExternalSource = false;
                                resource.Caption = NewsSlug;

                                Filter filter = new Filter();
                                filter.Name = arg.Source;
                                filter.FilterTypeId = (int)arg.SourceType;
                                filter.CreationDate = DateTime.Now;
                                filter.LastUpdateDate = filter.CreationDate;
                                filter.IsActive = true;
                                filter.IsApproved = false;
                                filter = _iFilterRepository.GetFilterCreateIfNotExist(filter);

                                resource.SourceName = arg.Source;
                                resource.SourceTypeId = arg.SourceType.ToString();
                                resource.SourceId = filter.FilterId.ToString();
                                Resource _res = resourceService.MediaPostResource(resource, arg.Source);
                                File.Copy(RueterMediaPath + @"\" + NewsVideoResource.Replace(".mp4",".mpg"), @"\\10.3.12.117\MediaResources\" + _res.Guid + ResourceFormat);
                                news.ImageGuids.Add(_res);
                            }

                            NewsDesc = WebUtility.HtmlDecode(Regex.Replace(NewsDesc, "(?!</?br?.?.>)(?!</?p>)<.*?>", "", RegexOptions.IgnoreCase));
                            news.Description = NewsDesc;
                            news.Slug = NewsSlug;
                            news.DescriptionText = Regex.Replace(news.Description, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase);
                            news.PublishTime = DateTime.Parse(NewsDate);
                            news.UpdateTime = DateTime.Parse(NewsDate);

                          
                            if (news.Categories.Count <= 0)
                            {
                                news.Categories.Add("Misc");
                            }
                            news.PublishTime = news.PublishTime.ToUniversalTime();
                            news.UpdateTime = news.UpdateTime.ToUniversalTime();
                            news.Author = "";
                            news.SourceType = (FilterTypes)arg.SourceType;
                            news.LanguageCode = arg.Language;
                            news.NewsType = NewsTypes.Story;
                            news.Url = file.FullName;
                            

                            NMS.Core.DataTransfer.News.PostInput newsfile = new Core.DataTransfer.News.PostInput();
                            newsfile.CopyFrom(news);
                            newsfile.FilterTypeId = (int)news.SourceType;

                            if (news.ImageGuids != null && news.ImageGuids.Count > 0)
                            {
                                newsfile.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                                newsfile.Resources.CopyFrom(news.ImageGuids);
                            }


                            #region Categories
                            newsfile.CategoryIds = new List<int>();
                            foreach (var cat in news.Categories)
                            {
                                string cCategory = string.Empty;
                                if (!string.IsNullOrEmpty(cat))
                                    cCategory = Regex.Replace(cat, "<.*?>", "", RegexOptions.IgnoreCase).Trim();

                                var ct = categoryservice.GetCategoryByName(cCategory.ToLower());
                                if (ct == null)
                                {
                                    Category obj = new Category();
                                    obj.Category = cCategory.ToLower();
                                    obj.CreationDate = dtNow;
                                    obj.LastUpdateDate = obj.CreationDate;
                                    obj.IsActive = true;
                                    obj.IsApproved = false;
                                    var c = categoryservice.InsertCategory(obj);

                                    newsfile.CategoryIds.Add(c.CategoryId);
                                }
                                else
                                {
                                    newsfile.CategoryIds.Add(ct.CategoryId);
                                }
                            }

                            #endregion Categories

                            #region Location
                            newsfile.LocationIds = new List<int>();
                            if (!string.IsNullOrEmpty(news.Location))
                            {
                                var l = locationservice.GetLocationByName(news.Location.ToLower());

                                if(l == null)
                                {
                                    Location loc = new Location();
                                    loc.CreationDate = dtNow;
                                    loc.LastUpdateDate = loc.CreationDate;
                                    loc.IsActive = true;
                                    loc.IsApproved = false;
                                    loc.Location = news.Location.ToLower();
                                    var obj = locationservice.InsertLocation(loc);

                                    newsfile.LocationIds.Add(obj.LocationId);
                                }
                                else
                                {
                                    newsfile.LocationIds.Add(l.LocationId);
                                }
                                
                            }
                            #endregion Location
                            
                            fileService.InsertNews(newsfile);


                         //   newsStatus = newsService.InsertRawNews(news);  //news insert
                            Console.WriteLine(Newscounter + "News Inserted ");
                            Newscounter++;
                        }
                        else
                            Console.WriteLine("News AlReady Inserted/Invalid " + news.Title);
                        ScrapMaxDate.MaxUpdateDate = file.CreationTime;
                        newsService.UpdateScrapDate(ScrapMaxDate);
                    }
                }

                logService.InsertSystemEventLog(string.Format("NewsFeedsThread: Execution Completed in {0}", arg.Source), "", EventCodes.Log);
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in NewsFeedsThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
            }

            return "Success";
        }

        public class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
        }

    }
}
