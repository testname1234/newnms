﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using CasparCGLib.Entities;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using MS.Core.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Service;



namespace NMS.ProcessThreads
{
    public class RunDownGeneration : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }



        public string MosDeviceId;
        public string MosDeviceIP;
        public string MosDevicePort;
        public string NCID;
        int messageId = 1;
        MOSClient mosClient = null;
        public string CasperDeviceIP;
        public string CasperDevicePort;
        private const int ReceiveTimeout = 20000;
        private const int SendTimeout = 20000;

        public string remoteMediaImagePath;
        public string remoteMediaVideoPath;

        public void test()
        {
            //PcrMachine m = new PcrMachine();
          //  PCRGateway g = new PCRGateway();
            MOS mos = new MOS();
            
            //g.ReceiveRunDown(mos);

        }

        public string Execute(string argument)
        {
            IMosActiveItemService mosActiveItemService = IoC.Resolve<IMosActiveItemService>("MosActiveItemService");
            IMosActiveEpisodeService mosActiveEpisodeService = IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
            List<MosActiveEpisode> MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByStatus(null);//Inseert
            if (MosActiveEpisodes != null && MosActiveEpisodes.Count > 0)
            {
                foreach (MosActiveEpisode mosEpisode in MosActiveEpisodes)
                {
                    if (mosEpisode.StatusCode == 1 || mosEpisode.StatusCode == 2)
                    {
                        //Call API
                        List<pcrMachinesDataOutput> lstPcrMachinesData = new List<pcrMachinesDataOutput>();
                        int id = 3;
                        lstPcrMachinesData = FillDevices(id);
                        foreach (var pcrMachines in lstPcrMachinesData)
                        {
                            // pcrMachines.IP = "10.1.20.33";
                            if (pcrMachines.DeviceTypeId == Convert.ToInt32(NMS.Core.Enums.DeviceType.Playout)) //playout server  // Caspar
                            {
                                remoteMediaVideoPath = @"\\" + Convert.ToString(pcrMachines.Ip) + @"\media\VideoUser\";
                                remoteMediaImagePath = @"\\" + Convert.ToString(pcrMachines.Ip) + @"\media\VideoUser\";
                                CasperDeviceIP = pcrMachines.Ip;
                                CasperDevicePort = Convert.ToString(pcrMachines.Port);
                                MosDeviceId = pcrMachines.DeviceKey;
                                MosDeviceIP = pcrMachines.Ip;

                                MosDevicePort = Convert.ToString(pcrMachines.Port);
                                GenerateCasperRundown(Convert.ToInt32(mosEpisode.EpisodeId), Convert.ToInt32(mosEpisode.StatusCode), pcrMachines.DeviceTypeId);
                            }

                            if (pcrMachines.DeviceTypeId == Convert.ToInt32(NMS.Core.Enums.DeviceType.WindowGraphics)) //4 WIndow // Caspar
                            {
                                //remoteMediaImagePath = @"\\" + Convert.ToString(pcrMachines.IP) + @"\media\ImageUser\";                               
                                CasperDeviceIP = pcrMachines.Ip;
                                CasperDevicePort = Convert.ToString(pcrMachines.Port);
                                MosDeviceId = pcrMachines.DeviceKey;
                                MosDeviceIP = pcrMachines.Ip;
                                MosDevicePort = Convert.ToString(pcrMachines.Port);
                                GenerateCasperRundown(Convert.ToInt32(mosEpisode.EpisodeId), Convert.ToInt32(mosEpisode.StatusCode), pcrMachines.DeviceTypeId);
                            }
                            if (pcrMachines.DeviceTypeId == Convert.ToInt32(NMS.Core.Enums.DeviceType.Teleprompter)) //Teleprompter
                            {
                                MosDeviceId = pcrMachines.DeviceKey;
                                MosDeviceIP = pcrMachines.Ip;
                                MosDevicePort = Convert.ToString(pcrMachines.Port);
                                SendMosTelePrompter(Convert.ToInt32(mosEpisode.EpisodeId), Convert.ToInt32(mosEpisode.StatusCode));
                            }
                        }

                        mosEpisode.StatusCode = 3;
                        mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
                    }
                }
            }
            return "";
        }

        public List<pcrMachinesDataOutput> FillDevices(int id)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            string apiPath = "http://10.1.20.57:83/api/device/getdevicesbygroupId/";
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<pcrMachinesDataOutput>>>(apiPath + id, null);
            List<pcrMachinesDataOutput> lstPcrMachinesData = new List<pcrMachinesDataOutput>();
            if (output.IsSuccess && output.Data != null)
            {
                lstPcrMachinesData = output.Data;
            }
            return lstPcrMachinesData;
        }



        public MOS GenerateCasperRundown(int EpisodeId, int StatusCode, int deviceTypeId)
        {
            MOS obj1 = null;
            IMosActiveEpisodeService mosActiveEpisodeService = IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
            IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
            IMosActiveItemService mosActiveItemService = IoC.Resolve<IMosActiveItemService>("MosActiveItemService");
            ISlotScreenTemplateResourceService slotScreenTemplateResourceRepository = IoC.Resolve<ISlotScreenTemplateResourceService>("SlotScreenTemplateResourceService");
            IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
            ISlotScreenTemplateService slotscreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
            ICasperTemplateService casperTemplateService = IoC.Resolve<ICasperTemplateService>("CasperTemplateService");
            ICasperTemplateItemService casperTemplateItemService = IoC.Resolve<ICasperTemplateItemService>("CasperTemplateItemService");
            ICasperItemTransformationService casperItemTransformationService = IoC.Resolve<ICasperItemTransformationService>("CasperItemTransformationService");
            ITemplateScreenElementService templateScreenElementService = IoC.Resolve<ITemplateScreenElementService>("TemplateScreenElementService");



            DataSet ds = new DataSet();
            List<MResource> mRVideos = new List<MResource>();
            List<MResource> mRImages = new List<MResource>();
            if (EpisodeId != null)
            {

                if (StatusCode == 1 || StatusCode == 2)
                {
                    List<SlotScreenTemplateResource> slotScreenTemplateResources = slotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByEpisode(EpisodeId);

                    #region DownloadResources
                    WebClient client = new WebClient();
                    string fileName = "";

                    if (slotScreenTemplateResources != null && slotScreenTemplateResources.Count > 0)
                    {
                        try
                        {
                            int i = 1;
                            foreach (SlotScreenTemplateResource slotScreenTemplateResource in slotScreenTemplateResources)
                            {
                                SlotScreenTemplate s = slotscreenTemplateService.GetSlotScreenTemplate(slotScreenTemplateResource.SlotScreenTemplateId);
                                MResource mResource = resourceService.GetMResourceByGuid(slotScreenTemplateResource.ResourceGuid.ToString());
                                SlotScreenTemplateResource sstR = slotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByResourceGuid(mResource.Guid);
                                string ext = "";
                                fileName = EpisodeId + "_" + sstR.SlotScreenTemplateResourceId;
                                if (mResource != null)
                                {
                                    if (mResource.ResourceTypeId == 1)
                                    {
                                        ext = ".jpg";
                                        if (deviceTypeId == Convert.ToInt32(NMS.Core.Enums.DeviceType.WindowGraphics) || deviceTypeId == Convert.ToInt32(NMS.Core.Enums.DeviceType.Playout))
                                        {
                                            client.DownloadFile(NMS.Core.AppSettings.MediaServerUrl + "/getresource/" + slotScreenTemplateResource.ResourceGuid.ToString(), remoteMediaImagePath + fileName + ext);
                                            mRImages.Add(mResource);
                                        }
                                    }
                                    else if (mResource.ResourceTypeId == 2)
                                    {
                                        ext = ".mp4";
                                        if (deviceTypeId == Convert.ToInt32(NMS.Core.Enums.DeviceType.Playout))
                                        {
                                            client.DownloadFile(NMS.Core.AppSettings.MediaServerUrl + "/getresource/" + slotScreenTemplateResource.ResourceGuid.ToString(), remoteMediaVideoPath + fileName + ext);
                                            mRVideos.Add(mResource);
                                        }
                                    }
                                    else if (mResource.ResourceTypeId == 3)
                                    {
                                        ext = ".mp3";
                                        if (deviceTypeId == Convert.ToInt32(NMS.Core.Enums.DeviceType.Playout))
                                        {
                                            client.DownloadFile(NMS.Core.AppSettings.MediaServerUrl + "/getresource/" + slotScreenTemplateResource.ResourceGuid.ToString(), remoteMediaVideoPath + fileName + ext);
                                        }
                                    }
                                }
                                client.Dispose();
                                i++;
                            }
                        }
                        catch (Exception ex)
                        {
                            if (client != null)
                                client.Dispose();
                        }
                    }
                    #endregion

                    //Items With Transformation     
                    #region Create Casper Items

                    List<SlotScreenTemplate> slotScreenTemplates = slotscreenTemplateService.GetByEpisodeId(EpisodeId);
                    List<item> items = new List<item>();
                    if (deviceTypeId == (int)NMS.Core.Enums.DeviceType.Playout)
                    {
                        //count on video resource and 
                        if (mRVideos != null && mRVideos.Count > 0)
                        {
                            int i = 1;
                            foreach (MResource mr in mRVideos)
                            {
                                string type = "MOVIE";
                                CasperTemplateItem cTemplateVideoItem = casperTemplateItemService.GetCasperTemplateItemByitemType(type);
                                SlotScreenTemplateResource sstR = slotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByResourceGuid(mr.Guid);
                                item item = new item();
                                item.CopyFrom(cTemplateVideoItem);
                                item.Label = EpisodeId + "_" + sstR.SlotScreenTemplateResourceId;
                                item.Name = EpisodeId + "_" + sstR.SlotScreenTemplateResourceId;
                                items.Add(item);
                                i++;
                            }
                        }

                        if (mRImages != null && mRImages.Count > 0)
                        {
                            int i = 1;
                            foreach (MResource mr in mRImages)
                            {
                                string type = "STILL";
                                CasperTemplateItem cTemplateVideoItem = casperTemplateItemService.GetCasperTemplateItemByitemType(type);
                                SlotScreenTemplateResource sstR = slotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByResourceGuid(mr.Guid);
                                item item = new item();
                                item.CopyFrom(cTemplateVideoItem);
                                item.Label = EpisodeId + "_" + sstR.SlotScreenTemplateResourceId;
                                item.Name = EpisodeId + "_" + sstR.SlotScreenTemplateResourceId;
                                items.Add(item);
                                i++;
                            }
                        }
                    }
                    else if (deviceTypeId == (int)NMS.Core.Enums.DeviceType.WindowGraphics)
                    {
                        int i = 1;
                        if (slotScreenTemplates != null && slotScreenTemplates.Count > 0)
                        {
                            foreach (SlotScreenTemplate slotScreenTemplate in slotScreenTemplates)
                            {
                                if (slotScreenTemplate != null)
                                {
                                    CasperTemplate cTemplate = casperTemplateService.GetCasperTemplateByTemplateName(slotScreenTemplate.Name);
                                    if (cTemplate != null)
                                    {
                                        List<CasperTemplateItem> cTemplateItems = casperTemplateItemService.GetCasperTemplateItemByCasperTemplateId(cTemplate.CasperTemlateId);

                                        foreach (var cT in cTemplateItems)
                                        {
                                            if (cT.Type == "DECKLINK INPUT")
                                                cT.Type = "DECKLINKINPUT";
                                            else if (cT.Type == "Template")
                                            {
                                                cT.Type = "TEMPLATE";
                                                cT.Videolayer = 0;
                                            }
                                            else if (cT.Type == "Video")
                                                cT.Type = "MOVIE";
                                            else if (cT.Type == "Transformation")
                                                cT.Type = "TRANSFORMATION";
                                            item item = new item();
                                            item.CopyFrom(cT);
                                            if (cT.Type == "DECKLINKINPUT")
                                            {
                                                item.Label = EpisodeId + "_" + cT.CasperTemplatItemId;
                                                item.Name = EpisodeId + "_" + cT.CasperTemplatItemId;
                                                i++;
                                            }
                                            items.Add(item);
                                            CasperItemTransformation cItemTransformation = casperItemTransformationService.GetCasperItemTransformationByCasperTemplateItemId(cT.CasperTemplatItemId);
                                            if (cItemTransformation != null)
                                            {
                                                item itemT = new item();
                                                if (cItemTransformation.Type == "Transformation")
                                                    cItemTransformation.Type = "TRANSFORMATION";
                                                itemT.CopyFrom(cItemTransformation);
                                                items.Add(itemT);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (items.Count > 0)
                    {
                        List<MosActiveEpisode> mosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByEpisodeId(EpisodeId);//Inseert
                        if (mosActiveEpisodes != null && mosActiveEpisodes.Count > 0)
                        {
                            Episode episode = episodeService.GetEpisode(EpisodeId);
                            obj1 = SendMosCasper(episode, mosActiveEpisodes[0].MosActiveEpisodeId, items);
                            if (obj1 != null)
                            {
                                List<MOS> mosObj = new List<MOS>();
                                mosObj.Add(obj1);
                                // Store RunOrderLog in Database
                                StoreRunOrderAndTelepromterLog(mosObj);
                            }
                        }
                    }

                    #endregion
                    //List<MosActiveItem> MosActiveItems = mosActiveItemService.GetMosActiveItemWithRunDownByEpisodeId(EpisodeId);
                    //if (MosActiveItems != null && MosActiveItems.Count > 0)
                    //{
                    //    MosActiveItems.Where(w => w.Type == "MOVIE").ToList().ForEach(s => s.Label = fileName);
                    //    MosActiveItems.Where(w => w.Type == "MOVIE").ToList().ForEach(s => s.Name = fileName);
                    //    List<item> items = new List<item>();
                    //    items.CopyFrom(MosActiveItems);
                    //    WriteXml(remotePath, EpisodeId, items);
                    //}
                }
                // mosEpisode.StatusCode = 3;
                //mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
            }

            return obj1;
        }

        public void SendMosTelePrompter(int episodeId, int statusCode)
        {
            string message = string.Empty;
            NCID = "NMSRundownService";
            mosClient = new MOSClient(MosDeviceId, MosDeviceIP, int.Parse(MosDevicePort), NCID);
            ISlotService slotService = IoC.Resolve<ISlotService>("slotService");
            IMosActiveEpisodeService mosActiveEpisodeService = IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
            ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
            IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
            IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
            List<MosActiveEpisode> MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByEpisodeId(episodeId);//Inseert            
            if (MosActiveEpisodes != null && MosActiveEpisodes.Count > 0)
            {
                foreach (MosActiveEpisode mosEpisode in MosActiveEpisodes)
                {
                    if (mosEpisode.StatusCode == 1 || mosEpisode.StatusCode == 2)
                    {
                        List<Slot> slots = slotService.GetSlotByEpisode(Convert.ToInt32(mosEpisode.EpisodeId));
                        if (slots != null && slots.Count > 0)
                        {
                            Episode episode = episodeService.GetEpisode(Convert.ToInt32(mosEpisode.EpisodeId));
                            NMS.Core.Entities.Program program = programService.GetProgram(episode.ProgramId);

                            #region MOS Integration
                            List<MOS> mosObjs = new List<MOS>();
                            MOS obj = null;
                            if (mosClient.IsConnected)
                            {
                            }
                            else
                                mosClient.Connect(out message);
                            System.Threading.Thread.Sleep(1000);
                            obj = mosClient.SendHeartBeat(out message);
                            System.Threading.Thread.Sleep(1000);
                            int Hour = Convert.ToInt32(episode.To.Subtract(episode.From).TotalHours);
                            int Minutes = Convert.ToInt32(episode.To.Subtract(episode.From).Minutes);
                            int Seconds = Convert.ToInt32(episode.To.Subtract(episode.From).Seconds);
                            obj = SendROCreate(out message, mosEpisode.MosActiveEpisodeId.ToString(), program.Name, episode.From, Hour.ToString() + ":" + Minutes.ToString() + ":" + Seconds.ToString(), slots);
                            mosObjs.Add(obj);

                            foreach (Slot slot in slots)
                            {
                                List<string> scripts = new List<string>();
                                List<SlotScreenTemplate> slotScreenTemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slot.SlotId);
                                if (slotScreenTemplates != null && slotScreenTemplates.Count > 0)
                                {
                                    scripts = slotScreenTemplates.Select(x => x.Script).ToList();
                                    obj = SendROStorySend(out message, slot.SlotId.ToString(), slot.SequnceNumber.ToString(), mosEpisode.MosActiveEpisodeId.ToString(), slot.Title, scripts);
                                    mosObjs.Add(obj);
                                }
                            }
                            if (mosObjs != null && mosObjs.Count > 0)
                            {
                                StoreRunOrderAndTelepromterLog(mosObjs);
                            }
                            #endregion
                        }
                    }
                    mosClient.Disconnect(out message);
                }
            }
        }
        public void StoreRunOrderAndTelepromterLog(List<MOS> mos)
        {
            IRunOrderLogService runOrderLogService = IoC.Resolve<IRunOrderLogService>("RunOrderLogService");
            IRunOrderStoriesService runOrderStoriesService = IoC.Resolve<IRunOrderStoriesService>("RunOrderStoriesService");
            if (mos != null && mos.Count > 0)
            {
                foreach (MOS obj1 in mos)
                {
                    ROAck ro = new ROAck();
                    ro.CopyFrom(obj1.command);
                    RunOrderLog roLOG = new RunOrderLog();
                    roLOG.CopyFrom(obj1);
                    roLOG.RoId = Convert.ToInt32(ro.roID);
                    roLOG.RoStatus = ro.roStatus;
                    roLOG.MosId = obj1.mosID;
                    roLOG.MessageId = obj1.messageID;
                    roLOG.NcsId = obj1.ncsID;
                    if (ro.roAckStories != null && ro.roAckStories.Count > 0)
                    {
                        roLOG.RoStoriesCount = ro.roAckStories.Count();
                    }
                    else
                    {
                        roLOG.RoStoriesCount = 0;
                    }
                    RunOrderLog roLog = runOrderLogService.InsertRunOrderLog(roLOG);

                    if (ro.roAckStories != null && ro.roAckStories.Count > 0)
                    {
                        foreach (ROAckStory roStory in ro.roAckStories)
                        {
                            RunOrderStories roStories = new RunOrderStories();
                            roStories.CopyFrom(roStory);
                            roStories.RunOrderLogId = roLog.RunOrderLogId;
                            roStories.ItemChannel = roStory.itemChannel;
                            roStories.ItemId = Convert.ToInt32(roStory.itemID);
                            roStories.ObjId = Convert.ToInt32(roStory.objID);
                            roStories.Status = Convert.ToString(roStory.status);
                            roStories.StoryId = Convert.ToInt32(roStory.storyID);
                            roStories.StoryStatus = Convert.ToString(roStory.storyStatus);
                            runOrderStoriesService.InsertRunOrderStories(roStories);
                        }
                    }
                }
            }
        }


        public MOS SendMosCasper(Episode episode, int mosEpisodeId, List<item> items)
        {
            MOS objMos = null;
            string message = string.Empty;
            NCID = "NMSRundownService";
            mosClient = new MOSClient(MosDeviceId, MosDeviceIP, int.Parse(MosDevicePort), NCID);
            ISlotService slotService = IoC.Resolve<ISlotService>("slotService");
            ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
            IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
            List<Slot> slots = slotService.GetSlotByEpisode(Convert.ToInt32(episode.EpisodeId));
            if (slots != null && slots.Count > 0)
            {
                NMS.Core.Entities.Program program = programService.GetProgram(episode.ProgramId);

                #region MOS Integration

                MOS obj = null;
                if (mosClient.IsConnected)
                {
                }
                else
                    mosClient.Connect(out message);
                System.Threading.Thread.Sleep(1000);
                obj = mosClient.SendHeartBeat(out message);
                System.Threading.Thread.Sleep(1000);
                int Hour = Convert.ToInt32(episode.To.Subtract(episode.From).TotalHours);
                int Minutes = Convert.ToInt32(episode.To.Subtract(episode.From).Minutes);
                int Seconds = Convert.ToInt32(episode.To.Subtract(episode.From).Seconds);
                List<CasparItem> CasparItem = new List<CasparItem>();
                CasparItem.CopyFrom(items);
                obj = SendROCreate(out message, mosEpisodeId.ToString(), program.Name, episode.From, Hour.ToString() + ":" + Minutes.ToString() + ":" + Seconds.ToString(), slots, CasparItem);
                objMos = obj;

                #endregion
            }
            mosClient.Disconnect(out message);
            return objMos;
        }

        public MOS GetROCreate(string runDownId, string ProgramName, DateTime StartTime, String EndTime, List<Slot> slots, List<CasparItem> casparItems = null)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROCreate ro = new ROCreate();
            ro.roID = runDownId;
            ro.roSlug = ProgramName;        //Episode Nam and Duration
            ro.roEdStart = StartTime.ToString();
            ro.roEdDur = EndTime; //00:59:00
            ro.roTrigger = "TIMED";
            if (casparItems != null)
                ro.casparItems = casparItems;
            ro.story = new System.Collections.Generic.List<Story>();

            //for (int i = 1; i <= slots.Count; i++)

            foreach (Slot slot in slots)
            {
                Story story = new Story();
                story.storyID = slot.SlotId.ToString();    //Slot pk
                story.storySlug = slot.Title;   //Slot Title
                story.storyNum = slot.SequnceNumber.ToString();
                ro.story.Add(story);
            }
            command.command = ro;

            return command;
        }

        public MOS GetStroySend(string storyID, string storyNumber, string runDownId, string storyTitle, List<string> StoryScripts)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROStorySend roStorySend = new ROStorySend();
            roStorySend.roID = runDownId;
            roStorySend.storyID = storyID;
            roStorySend.storySlug = storyTitle;
            roStorySend.storyNum = storyNumber;
            //roStorySend.storyBodies = new List<StoryBody>();
            List<StoryBody> storyBodies = new List<StoryBody>();
            int i = 0;
            foreach (string body in StoryScripts)
            {
                //storyBody.ParagraphWithInstructions = new Paragraph();
                //storyBody.ParagraphWithInstructions.ParagraphInstruction = "OC";
                StoryBody storyBody = new StoryBody();
                storyBody.ParagraphWithInstructions = "OC";
                storyBody.Paragraph = body;
                storyBodies.Add(storyBody);
                i++;
            }
            roStorySend.storyBodies = storyBodies;

            command.command = roStorySend;

            return command;
        }

        public MOS SendROCreate(out string message, string runDownId, string ProgramName, DateTime StartTime, String EndTime, List<Slot> slots, List<CasparItem> casparItems = null)
        {
            message = string.Empty;
            MOS mosResponse = new MOS();
            try
            {
                if (mosClient != null && mosClient.IsConnected)
                {
                    MOS mos = GetROCreate(runDownId, ProgramName, StartTime, EndTime, slots, casparItems);
                    mosResponse = mosClient.SendMOSCommand(mos, out message);
                }
                else
                {
                    message = "Error: Not Connected.";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return mosResponse;
        }

        public MOS SendROStorySend(out string message, string storyID, string storyNumber, string rundDownId, string storyTitle, List<string> StoryScript)
        {
            message = string.Empty;
            MOS mosResponse = new MOS();

            try
            {
                if (mosClient != null && mosClient.IsConnected)
                {
                    MOS mos = GetStroySend(storyID, storyNumber, rundDownId, storyTitle, StoryScript);
                    mosResponse = mosClient.SendMOSCommand(mos, out message);
                }
                else
                {
                    message = "Error: Not Connected.";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return mosResponse;
        }

        public static string GetStringForMOSCommand<T>(T command, string RootArgument)
        {
            StringBuilder stringBuilder = new StringBuilder();

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.OmitXmlDeclaration = true;

            XmlSerializerNamespaces xmlEmptyNameSpace = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });

            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlWriterSettings);

            XmlRootAttribute root = new XmlRootAttribute(RootArgument);
            XmlSerializer xml = new XmlSerializer(typeof(T), root);
            xml.Serialize(xmlWriter, command, xmlEmptyNameSpace);

            return stringBuilder.ToString();
        }


    }
}
