﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using ScrapingProject.Model;
using ScrappingLib;
using SelaniumLib;

namespace NMS.ProcessThreads
{
    public class ScrapNewsThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public Argument argu;
        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {

                argu = JSONhelper.GetObject<Argument>(argument);
                TemplateProject project = new TemplateProject();
                Console.WriteLine(Directory.GetCurrentDirectory());
                using (TextReader reader = new StreamReader(argu.InputFile))
                {
                    XmlSerializer xs = new XmlSerializer(project.GetType());
                    project = (TemplateProject)xs.Deserialize(reader);
                }
                if (argu.SeleniumInputFile != null && argu.SeleniumInputFile.Length > 1)
                {
                    SeleniumEngine objSlumn = new SeleniumEngine();
                    List<string> HtmlSources = objSlumn.StartSelinium(argu.SeleniumInputFile);

                    if (HtmlSources != null && HtmlSources.Count > 0)
                    {
                        var url = project.TemplatePackages[0].Urls[0];
                        int i = 0;
                        foreach (string str in HtmlSources)
                        {
                            if (project.Is_MultiPackage)
                            {
                                Url _url = (Url)url.Clone();
                                _url.HtmlSource = str;
                                project.TemplatePackages[i].Urls.Add(_url);
                                project.TemplatePackages[i].Urls.RemoveAt(0);
                            }
                            else
                            {
                                Url _url = (Url)url.Clone();
                                _url.HtmlSource = str;
                                project.TemplatePackages[0].Urls.Add(_url);
                                if (i == 0)
                                    project.TemplatePackages[0].Urls.RemoveAt(0);
                            }
                            i++;
                        }
                    }
                }

                ScrappingEngine engine = new ScrappingEngine(project);

                engine.OnOutputRecieved += engine_OnOutputRecieved;
                engine.Start();


                //WebClient wc = new WebClient();
                //wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows; Windows NT 5.1; rv:1.9.2.4) Gecko/20100611 Firefox/3.6.4");
                //HtmlDocument hd = new HtmlDocument();
                //hd.LoadHtml(wc.DownloadString("http://www.whatismyip.com/"));
                //wc.Dispose();

                //HtmlNode node = hd.DocumentNode.SelectSingleNode("//div[@class='center-ip'] | div[@class='ip-box']");
                //logService.InsertSystemEventLog(string.Format("System Ip: {0}", node.InnerHtml) ,"",EventCodes.Error);

                return "Service Started";
            }
            catch (Exception ex)
            {
                logService.InsertSystemEventLog(string.Format("ScrapNewsThread: Error in {0} {1}", argu != null ? argu.InputFile : "null", ex.Message), ex.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        public class Argument
        {
            public string InputFile { get; set; }
            public string OutputFile { get; set; }
            public string SeleniumInputFile { get; set; }

        }

        //static void engine_OnOutputRecieved(System.Collections.Generic.List<System.Collections.Generic.Dictionary<string, object>> output, string url)
        public void engine_OnOutputRecieved(ProjectResult output, string url)
        {
            try
            {
                string jsonoutput = JsonConvert.SerializeObject(output);
                string outputfile = argu.OutputFile + "\\" + DateTime.UtcNow.ToString("MM dd yyyy");
                if (!Directory.Exists(outputfile))
                {
                    Directory.CreateDirectory(outputfile);
                }
                #region Loging
                WriteSuccessUrls(url, "URL Executed Successfull");

                #endregion loging

                int counter = 0;
                int expIndex = 0;
                int expIncIndex = 0;
                int expIncIndexMax = 0;

                DirectoryInfo di = new DirectoryInfo(outputfile);
                foreach (FileInfo filinfo in di.GetFiles())
                {
                    //FileInfo[] lstFileinfo = di.GetFiles();
                    if (filinfo.Name.Contains("@@"))
                    {
                        expIndex = filinfo.Name.IndexOf("@@") > 0 ? filinfo.Name.IndexOf("@@") : 0;
                        expIncIndex = Convert.ToInt32(filinfo.Name.Substring(expIndex + 2, filinfo.Name.LastIndexOf(".") - (expIndex + 2)));
                        expIncIndexMax = expIncIndexMax < expIncIndex ? expIncIndex : expIncIndexMax;
                    }
                    counter++;
                }
                expIncIndexMax++;

                if (url.Contains("http://54.187.177.10:82"))
                {
                    url = url.Replace("http://54.187.177.10:82/", "");
                }

                string filename = ScrappingLib.ScrappingEngine.getFileNameFromUrl(url);
                int urlindex = filename.IndexOf(".") > 0 ? filename.IndexOf(".") : filename.Length;
                //filename = filename.Substring(0, urlindex);
                if (counter > 0)
                    System.IO.File.WriteAllText(outputfile + "/" + filename + "@@" + expIncIndexMax + ".txt", jsonoutput);
                else
                    System.IO.File.WriteAllText(outputfile + "/" + filename + ".txt", jsonoutput);

            }
            catch (Exception ex)
            {
                WriteError(url, ex);
            }

        }

        private void WriteError(string url, Exception ex)
        {
            try
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in ScrapNewsThread: {0} {1}", url, ex.Message), "", EventCodes.Error);
            }
            catch (Exception ex1)
            {
                // WriteLogError(ex.Message);
            }
        }
        private void WriteSuccessUrls(string url, String Message)
        {
            try
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ScrapNewsThread Logs: {0} {1}", url, Message), "", EventCodes.Log);
            }
            catch (Exception ex1)
            {
                // WriteLogError(ex.Message);
            }
        }


    }
}
