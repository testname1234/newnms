﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Threading;
using System.Xml.Serialization;
using System.Net;
using NMS.Core.Helper;
using NMS.Repository;
using System.Data;
using NMS.Core.Entities;

namespace NMS.ProcessThreads
{
    public class SendErrorSmsThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }


        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string message)
        {
            //Checklist Execute Method
            //CreateDataSet();
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                MosActiveEpisodeRepository mosactiveepisoderepo = new MosActiveEpisodeRepository();
                CheckListRepository checklistrepo = new CheckListRepository();                
                List<string> ListMessages = new List<string>();
                int Hour = Convert.ToInt32(StartRange);
                int Minute = Convert.ToInt32(EndRange);
                DateTime dtCustomStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Hour, Minute, 1);
                DateTime dtCustomEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Hour, Minute + 1, 1);
                if (DateTime.Now > dtCustomStart && DateTime.Now < dtCustomEnd)
                {
                    List<CheckList> CheckListErrors = checklistrepo.GetNameErrorMessageCheckList();
                    if (CheckListErrors != null && CheckListErrors.Count > 0)
                    {
                        ListMessages.AddRange(CheckListErrors.Select(x => x.ChecklistName + " \n " + x.ErrorMessage).ToList());
                        SendSmsFunction(ListMessages);
                        logService.InsertSystemEventLog(string.Format("Send Sms Thread: Check List Sms Sent"), "", EventCodes.Log);
                    }
                    System.Threading.Thread.Sleep(30000);
                }
                ListMessages.Clear();
                List<MosActiveEpisode> EpisodeErrors = mosactiveepisoderepo.GetAllMosEpisodeStatusSmsThread();
                if (EpisodeErrors != null && EpisodeErrors.Count > 0)
                {
                    ListMessages.AddRange(EpisodeErrors.Select(x => x.ProgramName + " \n " + x.StatusDescription).ToList());
                    SendSmsFunction(ListMessages);
                    logService.InsertSystemEventLog(string.Format("Send Sms Thread: Episode Sms Sent"), "", EventCodes.Log);
                    foreach (MosActiveEpisode episode in EpisodeErrors)
                    {
                        episode.IsAcknowledged = 1;
                        mosactiveepisoderepo.UpdateMosActiveEpisodeIsAcknowledged(episode);
                    }
                }
            }
            catch (Exception ex)
            {
                logService.InsertSystemEventLog(string.Format("Send Sms Thread: Error in {0} ", ex.Message), ex.StackTrace, EventCodes.Error);
            }
           
            return "successfull";
        }

        private static void SendSmsFunction(List<string> ListMessages)
        {
            foreach (string row in ListMessages)
            {
                HttpWebRequestHelper webProxy = new HttpWebRequestHelper();
                string smsId = ConfigurationManager.AppSettings["SmsID"].ToString();
                string smsShortCode = ConfigurationManager.AppSettings["SmsShortCode"];
                string smsPassword = ConfigurationManager.AppSettings["SmsPassword"];
                List<string> phoneNumbers = new List<string>();
                phoneNumbers.Add(ConfigurationManager.AppSettings["phoneNumbers"]);

                for (int i = 0; i < phoneNumbers.Count; i++)
                {
                    try
                    {
                        //if (ConfigurationManager.AppSettings["test"] != Convert.ToString(1))
                        //{
                        string requestUrl = "http://bsms.ufone.com/bsms_app4/sendapi.jsp?id=" + smsId + "&message=" + row + "&shortcode=" + smsShortCode + "&lang=English&mobilenum=" + "92" + phoneNumbers[i].Trim().TrimStart('0') + "&password=" + smsPassword;
                        HttpWebResponse response = webProxy.PostRequest(requestUrl, null, null, false);
                        if (response != null)
                            response.Close();
                        //}
                    }
                    catch (Exception exp)
                    {
                        Console.WriteLine(exp.Message);
                        ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                        logService.InsertSystemEventLog(string.Format("Error in BackupThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                    }
                }
            }
        }
    }
}
