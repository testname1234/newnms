﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MS.Core.Enums;
using MS.Core.IService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Helper;

namespace NMS.ProcessThreads
{
    public class SetMediaDuration : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            try
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                WebClient client = new WebClient();
                HttpWebRequestHelper proxy = new HttpWebRequestHelper();
                IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
                var resources =  resourceService.GetScrapResourceByStatusId(ScrapResourceStatuses.Downloaded);
                if (resources != null && resources.Count > 0)
                {
                    int successfull = 0;
                    foreach (var res in resources)
                    {
                        try
                        {
            
                            HttpWebRequestHelper helper = new HttpWebRequestHelper();
                            HttpWebResponse response = helper.GetRequest(NMS.Core.AppSettings.MediaServerUrl + "/getresource/?ResourceDate="+DateTime.UtcNow, null);
                            using (TextReader treader = new StreamReader(response.GetResponseStream()))
                            {
                                string str = treader.ReadToEnd();
                                List<MS.Core.Entities.Resource> MSRes = JsonConvert.DeserializeObject<List<MS.Core.Entities.Resource>>(str);                              
                            }
                            response.Close();


                            if (response != null)
                            {
                                res.Status = ScrapResourceStatuses.Completed;
                                res.LastUpdateDate = DateTime.UtcNow;
                              //  resourceService.UpdateScrapResource(res);
                                successfull++;
                            }                           
                        }
                        catch (Exception exp)
                        {

                            logService.InsertSystemEventLog(string.Format("SetMediaDurationThread: Error downloading resource {0}", exp.Message), "", EventCodes.Error);
                        }
                    }
                    string msg = string.Format("SetMediaDurationThread: {0} of {1} files downloaded successfully", successfull, resources.Count);
                    logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                    return msg;
                }
                return "Successfull";
            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("Error in SetMediaDurationThread: {0}", exp.Message), "", EventCodes.Error);
                return "Error";
            }
        }
    }
}
