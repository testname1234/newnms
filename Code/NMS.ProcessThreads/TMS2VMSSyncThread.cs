﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using MMS.Integration.VMS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMS.Core.Entities;
using TMS.Core.Enums;
using TMS.Core.IService;

namespace NMS.ProcessThreads
{
    public class TMS2VMSSyncThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                VMSAPI api = new VMSAPI();
                var channelService = IoC.Resolve<IChannelService>("TMSChannelService");
                var videoService = IoC.Resolve<IVideoService>("TMSVideoService");
                var channels = channelService.GetAllChannel();
                foreach (var channel in channels)
                {
                    var dtmax = channelService.GetMaxChannelDate(channel.ChannelId);
                    var transfer = api.GetReelsAfterThisDate(dtmax, channel.ChannelId);
                    if (transfer.Data != null && transfer.IsSuccess)
                    {
                        int counter = 0;
                        foreach (var reel in transfer.Data)
                        {
                            if (!videoService.Exist(reel.ChannelId, reel.StartTime, reel.EndTime))
                            {
                                var video = new Video();
                                video.CreationDate = DateTime.UtcNow;
                                video.LastUpdateDate = video.CreationDate;
                                video.IsActive = true;
                                video.StartTime = reel.StartTime;
                                video.EndTime = reel.EndTime;
                                video.ChannelId = reel.ChannelId;
                                video.Url = reel.Path;
                                video.VideoStatus = VideoStatuses.Pending;
                                videoService.InsertVideo(video);
                                counter++;
                            }
                        }
                        logService.InsertSystemEventLog(string.Format("TMS2VMSSyncThread: {0}->{1}", channel.Name, counter), "", EventCodes.Log);
                    }
                }
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in TMS2VMSSyncThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
            }
            return "Success";
        }
    }
}
