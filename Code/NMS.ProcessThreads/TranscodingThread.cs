﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.IService;
using NMS.Repository;

namespace NMS.ProcessThreads
{
    public class TranscodingThread : ISystemProcessThread
    {
        private string _threadName;
        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;
        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.Now.ToString("MMM dd,yyyy hh:mm:ss tt");
        }


        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            IChannelService channelService = IoC.Resolve<IChannelService>("ChannelService");
            ReelRepository reelrepository = new ReelRepository();

            try
            {
                int successCount = 0;
                List<int> channelIds = argument.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList();
                List<Channel> channels = channelService.GetAllChannel();
                channels = channels.Where(x => !string.IsNullOrEmpty(x.VideosPath)).ToList();

                List<Reel> Reels = reelrepository.GetReelByStatusAndChannelId(ReelStatuses.TranscodingRequired, channelIds);
                
                if (Reels != null)
                {
                    foreach (var reel in Reels)
                    {
                        if (File.Exists(reel.OriginalPath))
                        {
                            var VideoName = Path.GetFileName(reel.OriginalPath);
                            VideoName = VideoName.Replace(".ts", ".mp4");

                            var SavePath = ConfigurationManager.AppSettings["StreamSavePath"] + "\\" + channels.Where(x => x.ChannelId == reel.ChannelId).First().Name + "\\" + reel.StartTime.ToString("yyyy-MM-dd");
                            var AccessUrl = ConfigurationManager.AppSettings["AccessUrl"] + "/" + channels.Where(x => x.ChannelId == reel.ChannelId).First().Name + "/" + reel.StartTime.ToString("yyyy-MM-dd");

                            if (!Directory.Exists(SavePath))
                                Directory.CreateDirectory(SavePath);

                            FFMPEGLib.FFMPEG.ConvertToMP4ForChrome(reel.OriginalPath, SavePath + "\\" + VideoName);

                            if (File.Exists(SavePath + "\\" + VideoName))
                            {
                                reel.StatusId = (int)ReelStatuses.Created;
                                reel.Path = AccessUrl + "/" + VideoName;
                                //reel.Path = AccessUrl + "/" + VideoName;
                                reelrepository.UpdateReel(reel);
                                successCount++;
                            }
                        }
                        else
                        {
                            reel.StatusId = (int)ReelStatuses.Completed;
                            reelrepository.UpdateReel(reel);
                        }
                    }
                }
                if (successCount > 0)
                    logService.InsertSystemEventLog(string.Format("TranscodingThread: {0} reels processed", successCount), "", EventCodes.Log);

                return "Success";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                logService.InsertSystemEventLog(string.Format("Error in TranscodingThread: {0}", ex.Message), ex.StackTrace, EventCodes.Error);
                return "Fail";
            }
        }
    }
}
