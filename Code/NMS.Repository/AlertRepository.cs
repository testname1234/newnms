﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{	
	public partial class AlertRepository: AlertRepositoryBase, IAlertRepository
	{
        public List<Alert> GetLatestAlerts(DateTime? lastUpdateDate = null, int rowCount = 10)
        {            
            string sql = string.Format("select top ({0}) * from [Alert] with (nolock)", rowCount.ToString());

            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@Date", lastUpdateDate ?? DateTime.UtcNow.AddDays(-1))
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Alert>(ds, AlertFromDataRow);
        }
	}
}
