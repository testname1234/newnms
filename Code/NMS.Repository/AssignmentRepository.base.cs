﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class AssignmentRepositoryBase : Repository, IAssignmentRepositoryBase 
	{
        
        public AssignmentRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("AssignmentId",new SearchColumn(){Name="AssignmentId",Title="AssignmentId",SelectClause="AssignmentId",WhereClause="AllRecords.AssignmentId",DataType="System.Int32",IsForeignColumn=false,PropertyName="AssignmentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Slug",new SearchColumn(){Name="Slug",Title="Slug",SelectClause="Slug",WhereClause="AllRecords.Slug",DataType="System.String",IsForeignColumn=false,PropertyName="Slug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AssignmentType",new SearchColumn(){Name="AssignmentType",Title="AssignmentType",SelectClause="AssignmentType",WhereClause="AllRecords.AssignmentType",DataType="System.Int32?",IsForeignColumn=false,PropertyName="AssignmentType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Picture",new SearchColumn(){Name="Picture",Title="Picture",SelectClause="Picture",WhereClause="AllRecords.Picture",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Picture",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Video",new SearchColumn(){Name="Video",Title="Video",SelectClause="Video",WhereClause="AllRecords.Video",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Video",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Audio",new SearchColumn(){Name="Audio",Title="Audio",SelectClause="Audio",WhereClause="AllRecords.Audio",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Audio",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Document",new SearchColumn(){Name="Document",Title="Document",SelectClause="Document",WhereClause="AllRecords.Document",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Document",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Barcode",new SearchColumn(){Name="Barcode",Title="Barcode",SelectClause="Barcode",WhereClause="AllRecords.Barcode",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Barcode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetAssignmentSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetAssignmentBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetAssignmentAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetAssignmentSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Assignment].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Assignment].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Assignment GetAssignment(System.Int32 AssignmentId,string SelectClause=null)
		{
			string sql=string.IsNullOrEmpty(SelectClause)?GetAssignmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Assignment] with (nolock)  where AssignmentId=@AssignmentId ";
			SqlParameter parameter=new SqlParameter("@AssignmentId",AssignmentId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return AssignmentFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Assignment> GetAssignmentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{
			string sql=string.IsNullOrEmpty(SelectClause)?GetAssignmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Assignment] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Assignment>(ds,AssignmentFromDataRow);
		}

		public virtual List<Assignment> GetAllAssignment(string SelectClause=null)
		{
			string sql=string.IsNullOrEmpty(SelectClause)?GetAssignmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Assignment] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Assignment>(ds, AssignmentFromDataRow);
		}

		public virtual List<Assignment> GetPagedAssignment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{
			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetAssignmentCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [AssignmentId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([AssignmentId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [AssignmentId] ";
            tempsql += " FROM [Assignment] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("AssignmentId"))
					tempsql += " , (AllRecords.[AssignmentId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[AssignmentId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetAssignmentSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Assignment] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Assignment].[AssignmentId] = PageIndex.[AssignmentId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Assignment>(ds, AssignmentFromDataRow);
			}else{ return null;}
		}

		private int GetAssignmentCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Assignment as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Assignment as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Assignment))]
		public virtual Assignment InsertAssignment(Assignment entity)
		{

			Assignment other=new Assignment();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Assignment ( [Slug]
				,[LocationId]
				,[CategoryId]
				,[AssignmentType]
				,[CreationDate]
				,[Picture]
				,[Video]
				,[Audio]
				,[Document]
				,[Barcode] )
				Values
				( @Slug
				, @LocationId
				, @CategoryId
				, @AssignmentType
				, @CreationDate
				, @Picture
				, @Video
				, @Audio
				, @Document
				, @Barcode );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@AssignmentType",entity.AssignmentType ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@Picture",entity.Picture ?? (object)DBNull.Value)
					, new SqlParameter("@Video",entity.Video ?? (object)DBNull.Value)
					, new SqlParameter("@Audio",entity.Audio ?? (object)DBNull.Value)
					, new SqlParameter("@Document",entity.Document ?? (object)DBNull.Value)
					, new SqlParameter("@Barcode",entity.Barcode ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetAssignment(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Assignment))]
		public virtual Assignment UpdateAssignment(Assignment entity)
		{

			if (entity.IsTransient()) return entity;
			Assignment other = GetAssignment(entity.AssignmentId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Assignment set  [Slug]=@Slug
							, [LocationId]=@LocationId
							, [CategoryId]=@CategoryId
							, [AssignmentType]=@AssignmentType
							, [CreationDate]=@CreationDate
							, [Picture]=@Picture
							, [Video]=@Video
							, [Audio]=@Audio
							, [Document]=@Document
							, [Barcode]=@Barcode 
							 where AssignmentId=@AssignmentId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@AssignmentType",entity.AssignmentType ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@Picture",entity.Picture ?? (object)DBNull.Value)
					, new SqlParameter("@Video",entity.Video ?? (object)DBNull.Value)
					, new SqlParameter("@Audio",entity.Audio ?? (object)DBNull.Value)
					, new SqlParameter("@Document",entity.Document ?? (object)DBNull.Value)
					, new SqlParameter("@Barcode",entity.Barcode ?? (object)DBNull.Value)
					, new SqlParameter("@AssignmentId",entity.AssignmentId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetAssignment(entity.AssignmentId);
		}

		public virtual bool DeleteAssignment(System.Int32 AssignmentId)
		{

			string sql="delete from Assignment where AssignmentId=@AssignmentId";
			SqlParameter parameter=new SqlParameter("@AssignmentId",AssignmentId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Assignment))]
		public virtual Assignment DeleteAssignment(Assignment entity)
		{
			this.DeleteAssignment(entity.AssignmentId);
			return entity;
		}

		public virtual Assignment AssignmentFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Assignment entity=new Assignment();
			if (dr.Table.Columns.Contains("AssignmentId"))
			{
			entity.AssignmentId = (System.Int32)dr["AssignmentId"];
			}
			if (dr.Table.Columns.Contains("Slug"))
			{
			entity.Slug = dr["Slug"].ToString();
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("AssignmentType"))
			{
			entity.AssignmentType = dr["AssignmentType"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["AssignmentType"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("Picture"))
			{
			entity.Picture = dr["Picture"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Picture"];
			}
			if (dr.Table.Columns.Contains("Video"))
			{
			entity.Video = dr["Video"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Video"];
			}
			if (dr.Table.Columns.Contains("Audio"))
			{
			entity.Audio = dr["Audio"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Audio"];
			}
			if (dr.Table.Columns.Contains("Document"))
			{
			entity.Document = dr["Document"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Document"];
			}
			if (dr.Table.Columns.Contains("Barcode"))
			{
			entity.Barcode = dr["Barcode"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Barcode"];
			}
			return entity;
		}

	}
	
	
}
