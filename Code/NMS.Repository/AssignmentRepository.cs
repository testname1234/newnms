﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class AssignmentRepository: AssignmentRepositoryBase, IAssignmentRepository
	{

        public virtual Assignment GetByBarcode(int Barcode)
        {
            string sql = @"select * from [Assignment] with (nolock)  where Barcode=@Barcode";
            SqlParameter parameter = new SqlParameter("@Barcode", Barcode);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return AssignmentFromDataRow(ds.Tables[0].Rows[0]);
        }
	}
	
	
}
