﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class BunchFilterNewsRepositoryBase : Repository, IBunchFilterNewsRepositoryBase 
	{
        
        public BunchFilterNewsRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("BunchFilterNewsId",new SearchColumn(){Name="BunchFilterNewsId",Title="BunchFilterNewsId",SelectClause="BunchFilterNewsId",WhereClause="AllRecords.BunchFilterNewsId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BunchFilterNewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("isactive",new SearchColumn(){Name="isactive",Title="isactive",SelectClause="isactive",WhereClause="AllRecords.isactive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Isactive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchGuid",new SearchColumn(){Name="BunchGuid",Title="BunchGuid",SelectClause="BunchGuid",WhereClause="AllRecords.BunchGuid",DataType="System.String",IsForeignColumn=false,PropertyName="BunchGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterId",new SearchColumn(){Name="FilterId",Title="FilterId",SelectClause="FilterId",WhereClause="AllRecords.FilterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Newsguid",new SearchColumn(){Name="Newsguid",Title="Newsguid",SelectClause="Newsguid",WhereClause="AllRecords.Newsguid",DataType="System.String",IsForeignColumn=false,PropertyName="Newsguid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetBunchFilterNewsSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetBunchFilterNewsBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetBunchFilterNewsAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetBunchFilterNewsSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[BunchFilterNews].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[BunchFilterNews].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual BunchFilterNews GetBunchFilterNews(System.Int32 BunchFilterNewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BunchFilterNews] with (nolock)  where BunchFilterNewsId=@BunchFilterNewsId ";
			SqlParameter parameter=new SqlParameter("@BunchFilterNewsId",BunchFilterNewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return BunchFilterNewsFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<BunchFilterNews> GetBunchFilterNewsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [BunchFilterNews] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilterNews>(ds,BunchFilterNewsFromDataRow);
		}

		public virtual List<BunchFilterNews> GetAllBunchFilterNews(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BunchFilterNews] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilterNews>(ds, BunchFilterNewsFromDataRow);
		}

		public virtual List<BunchFilterNews> GetPagedBunchFilterNews(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetBunchFilterNewsCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [BunchFilterNewsId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([BunchFilterNewsId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [BunchFilterNewsId] ";
            tempsql += " FROM [BunchFilterNews] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("BunchFilterNewsId"))
					tempsql += " , (AllRecords.[BunchFilterNewsId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[BunchFilterNewsId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetBunchFilterNewsSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [BunchFilterNews] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [BunchFilterNews].[BunchFilterNewsId] = PageIndex.[BunchFilterNewsId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilterNews>(ds, BunchFilterNewsFromDataRow);
			}else{ return null;}
		}

		private int GetBunchFilterNewsCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM BunchFilterNews as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM BunchFilterNews as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(BunchFilterNews))]
		public virtual BunchFilterNews InsertBunchFilterNews(BunchFilterNews entity)
		{

			BunchFilterNews other=new BunchFilterNews();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into BunchFilterNews ( [CreationDate]
				,[LastUpdateDate]
				,[isactive]
				,[BunchGuid]
				,[FilterId]
				,[Newsguid] )
				Values
				( @CreationDate
				, @LastUpdateDate
				, @isactive
				, @BunchGuid
				, @FilterId
				, @Newsguid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@Newsguid",entity.Newsguid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetBunchFilterNews(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(BunchFilterNews))]
		public virtual BunchFilterNews UpdateBunchFilterNews(BunchFilterNews entity)
		{

			if (entity.IsTransient()) return entity;
			BunchFilterNews other = GetBunchFilterNews(entity.BunchFilterNewsId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update BunchFilterNews set  [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive
							, [BunchGuid]=@BunchGuid
							, [FilterId]=@FilterId
							, [Newsguid]=@Newsguid 
							 where BunchFilterNewsId=@BunchFilterNewsId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@Newsguid",entity.Newsguid ?? (object)DBNull.Value)
					, new SqlParameter("@BunchFilterNewsId",entity.BunchFilterNewsId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetBunchFilterNews(entity.BunchFilterNewsId);
		}

		public virtual bool DeleteBunchFilterNews(System.Int32 BunchFilterNewsId)
		{

			string sql="delete from BunchFilterNews where BunchFilterNewsId=@BunchFilterNewsId";
			SqlParameter parameter=new SqlParameter("@BunchFilterNewsId",BunchFilterNewsId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(BunchFilterNews))]
		public virtual BunchFilterNews DeleteBunchFilterNews(BunchFilterNews entity)
		{
			this.DeleteBunchFilterNews(entity.BunchFilterNewsId);
			return entity;
		}


		public virtual BunchFilterNews BunchFilterNewsFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			BunchFilterNews entity=new BunchFilterNews();
			if (dr.Table.Columns.Contains("BunchFilterNewsId"))
			{
			entity.BunchFilterNewsId = (System.Int32)dr["BunchFilterNewsId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("isactive"))
			{
			entity.Isactive = dr["isactive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["isactive"];
			}
			if (dr.Table.Columns.Contains("BunchGuid"))
			{
			entity.BunchGuid = dr["BunchGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("FilterId"))
			{
			entity.FilterId = dr["FilterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FilterId"];
			}
			if (dr.Table.Columns.Contains("Newsguid"))
			{
			entity.Newsguid = dr["Newsguid"].ToString();
			}
			return entity;
		}

	}
	
	
}
