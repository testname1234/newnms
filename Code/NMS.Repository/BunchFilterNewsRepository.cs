﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class BunchFilterNewsRepository : BunchFilterNewsRepositoryBase, IBunchFilterNewsRepository
    {
        public BunchFilterNewsRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public BunchFilterNewsRepository()
        {
        }

        public virtual List<BunchFilterNews> GetByNewsGuid(string NewsGuid ,string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchFilterNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [BunchFilterNews] with (nolock) where NewsGuid = @NewsGuid ";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<BunchFilterNews>(ds, BunchFilterNewsFromDataRow);
        }

        public virtual List<BunchFilterNews> GetByDateAndfilters(string csvfilters, int pagecount, int startIndex, DateTime from, DateTime to, string csvDiscardedfilters, string SelectClause = null)
        {
            startIndex = startIndex < 1 ? 1 : startIndex;
            string Filters = csvfilters.Length > 0 ? "FilterId in (" + csvfilters + ")" : string.Empty;
            string DiscardedFilters = string.Empty;
            if (Filters.Length > 0)
                DiscardedFilters = DiscardedFilters.Length > 0 ? " And FilterId not in (" + DiscardedFilters + ")" : string.Empty;
            else
                DiscardedFilters = DiscardedFilters.Length > 0 ? "FilterId not in (" + DiscardedFilters + ")" : string.Empty;

            string sql = @"select * from bunchfilternews bf (nolock) 
                        inner join news n (nolock) on n.[guid] = bf.newsguid and isarchival=1
                        where " + Filters + DiscardedFilters + @"
                        and bf.LastUpdateDate >= @fromdate and bf.LastUpdateDate <= @ToDate
                        order by bf.LastUpdateDate desc
                        OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
                        FETCH NEXT @RecordsPerPage ROWS ONLY";
            SqlParameter parameter1 = new SqlParameter("@RecordsPerPage", pagecount);
            SqlParameter parameter2 = new SqlParameter("@PageNo", startIndex);
            SqlParameter parameter3 = new SqlParameter("@fromdate", from);
            SqlParameter parameter4 = new SqlParameter("@ToDate", to);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2, parameter3, parameter4 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<BunchFilterNews>(ds, BunchFilterNewsFromDataRow);
        }

        public virtual List<BunchFilterNews> GetFiltersByNewsGuid(string NewsGuid, string SelectClause = null)
        {

            string sql = "select distinct BunchGuid,FilterId,CreationDate,LastUpdateDate,Isactive from [BunchFilterNews] with (nolock) where NewsGuid = @NewsGuid ";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<BunchFilterNews>(ds, BunchFilterNewsFromDataRow);
        }

        public virtual List<BunchFilterNews> GetByBunchGuid(string BunchGuid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchFilterNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [BunchFilterNews] with (nolock) where BunchGuid = @BunchGuid ";
            SqlParameter parameter = new SqlParameter("@BunchGuid", BunchGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<BunchFilterNews>(ds, BunchFilterNewsFromDataRow);
        }

//        public virtual List<BunchFilterNews> GetByBunchIdAndFilterId(System.Int32? BunchId, System.Int32? FilterId, string SelectClause = null)
//        {
//            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchFilterNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
//            sql += "from [BunchFilterNews] with (nolock)  where FilterId=@FilterId And BunchId=@BunchId  ";
//            SqlParameter parameter1 = new SqlParameter("@FilterId", FilterId);
//            SqlParameter parameter2 = new SqlParameter("@BunchId", BunchId);
//            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
//            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
//            return CollectionFromDataSet<BunchFilterNews>(ds, BunchFilterNewsFromDataRow);
//        }

//        public virtual void InsertBunchFilterNewsWithPk(BunchFilterNews entity)
//        {
//            string sql = @"
//                set identity_insert BunchFilterNews on
//                Insert into BunchFilterNews (
//                BunchFilterNewsId    
//                ,[BunchFilterId]
//				,[NewsId]
//				,[CreationDate]
//				,[LastUpdateDate]
//				,[isactive] )
//				Values
//				( 
//                  @BunchFilterNewsId  
//                , @BunchFilterId
//				, @NewsId
//				, @CreationDate
//				, @LastUpdateDate
//				, @isactive );
//                set identity_insert BunchFilterNews off
//				Select scope_identity()";
//            SqlParameter[] parameterArray = new SqlParameter[]{
//                     new SqlParameter("@BunchFilterNewsId",entity.BunchFilterNewsId)
//                    , new SqlParameter("@BunchFilterId",entity.BunchFilterId ?? (object)DBNull.Value)
//                    , new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
//                    , new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
//                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
//                    , new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)};
//            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
//            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");

//        }

//        public virtual void UpdateBunchFilterNewsNoReturn(BunchFilterNews entity)
//        {            
//            string sql = @"Update BunchFilterNews 
//                              set  [BunchFilterId]=@BunchFilterId
//							, [NewsId]=@NewsId
//							, [CreationDate]=@CreationDate
//							, [LastUpdateDate]=@LastUpdateDate
//							, [isactive]=@isactive 
//							 where BunchFilterNewsId=@BunchFilterNewsId";
//            SqlParameter[] parameterArray = new SqlParameter[]{
//                     new SqlParameter("@BunchFilterId",entity.BunchFilterId ?? (object)DBNull.Value)
//                    , new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
//                    , new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
//                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
//                    , new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
//                    , new SqlParameter("@BunchFilterNewsId",entity.BunchFilterNewsId)};
//            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
//        }
    }


}
