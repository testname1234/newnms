﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class BunchFilterRepositoryBase : Repository, IBunchFilterRepositoryBase 
	{
        
        public BunchFilterRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("BunchFilterId",new SearchColumn(){Name="BunchFilterId",Title="BunchFilterId",SelectClause="BunchFilterId",WhereClause="AllRecords.BunchFilterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BunchFilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterId",new SearchColumn(){Name="FilterId",Title="FilterId",SelectClause="FilterId",WhereClause="AllRecords.FilterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchId",new SearchColumn(){Name="BunchId",Title="BunchId",SelectClause="BunchId",WhereClause="AllRecords.BunchId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BunchId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetBunchFilterSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetBunchFilterBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetBunchFilterAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetBunchFilterSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "BunchFilter."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",BunchFilter."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<BunchFilter> GetBunchFilterByFilterId(System.Int32 FilterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchFilter with (nolock)  where FilterId=@FilterId  ";
			SqlParameter parameter=new SqlParameter("@FilterId",FilterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilter>(ds,BunchFilterFromDataRow);
		}

		public virtual List<BunchFilter> GetBunchFilterByBunchId(System.Int32 BunchId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchFilter with (nolock)  where BunchId=@BunchId  ";
			SqlParameter parameter=new SqlParameter("@BunchId",BunchId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilter>(ds,BunchFilterFromDataRow);
		}


		public virtual BunchFilter GetBunchFilter(System.Int32 BunchFilterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchFilter with (nolock)  where BunchFilterId=@BunchFilterId ";
			SqlParameter parameter=new SqlParameter("@BunchFilterId",BunchFilterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return BunchFilterFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<BunchFilter> GetBunchFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from BunchFilter with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilter>(ds,BunchFilterFromDataRow);
		}

		public virtual List<BunchFilter> GetAllBunchFilter(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchFilter with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilter>(ds, BunchFilterFromDataRow);
		}

		public virtual List<BunchFilter> GetPagedBunchFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetBunchFilterCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [BunchFilterId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([BunchFilterId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [BunchFilterId] ";
            tempsql += " FROM [BunchFilter] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("BunchFilterId"))
					tempsql += " , (AllRecords.[BunchFilterId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[BunchFilterId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetBunchFilterSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [BunchFilter] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [BunchFilter].[BunchFilterId] = PageIndex.[BunchFilterId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchFilter>(ds, BunchFilterFromDataRow);
			}else{ return null;}
		}

		private int GetBunchFilterCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM BunchFilter as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM BunchFilter as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(BunchFilter))]
		public virtual BunchFilter InsertBunchFilter(BunchFilter entity)
		{

			BunchFilter other=new BunchFilter();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into BunchFilter ( [FilterId]
				,[BunchId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @FilterId
				, @BunchId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetBunchFilter(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(BunchFilter))]
		public virtual BunchFilter UpdateBunchFilter(BunchFilter entity)
		{

			if (entity.IsTransient()) return entity;
			BunchFilter other = GetBunchFilter(entity.BunchFilterId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update BunchFilter set  [FilterId]=@FilterId
							, [BunchId]=@BunchId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where BunchFilterId=@BunchFilterId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@BunchFilterId",entity.BunchFilterId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetBunchFilter(entity.BunchFilterId);
		}

		public virtual bool DeleteBunchFilter(System.Int32 BunchFilterId)
		{

			string sql="delete from BunchFilter where BunchFilterId=@BunchFilterId";
			SqlParameter parameter=new SqlParameter("@BunchFilterId",BunchFilterId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(BunchFilter))]
		public virtual BunchFilter DeleteBunchFilter(BunchFilter entity)
		{
			this.DeleteBunchFilter(entity.BunchFilterId);
			return entity;
		}


		public virtual BunchFilter BunchFilterFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			BunchFilter entity=new BunchFilter();
			if (dr.Table.Columns.Contains("BunchFilterId"))
			{
			entity.BunchFilterId = (System.Int32)dr["BunchFilterId"];
			}
			if (dr.Table.Columns.Contains("FilterId"))
			{
			entity.FilterId = (System.Int32)dr["FilterId"];
			}
			if (dr.Table.Columns.Contains("BunchId"))
			{
			entity.BunchId = (System.Int32)dr["BunchId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
