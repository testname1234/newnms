﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class BunchFilterRepository : BunchFilterRepositoryBase, IBunchFilterRepository
    {
        public BunchFilterRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public BunchFilterRepository()
        {
        }
        public virtual BunchFilter GetByFilterIdAndBunchId(System.Int32 FilterId, System.Int32 BunchId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchFilterSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from BunchFilter with (nolock)  where FilterId=@FilterId And BunchId=@BunchId";
            SqlParameter parameter1 = new SqlParameter("@FilterId", FilterId);
            SqlParameter parameter2 = new SqlParameter("@BunchId", BunchId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return BunchFilterFromDataRow(ds.Tables[0].Rows[0]);
        }


 

        public virtual bool DeleteByFilterIdAndBunchId(System.Int32 FilterId, System.Int32 BunchId)
        {

            string sql = "delete from BunchFilter with (nolock) where BunchId=@BunchId And FilterId=@FilterId";
            SqlParameter parameter1 = new SqlParameter("@FilterId", FilterId);
            SqlParameter parameter2 = new SqlParameter("@BunchId", BunchId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual void InsertBunchFilterNoReturn(BunchFilter entity)
        {
            string sql = @"Insert into BunchFilter ( [FilterId]
				,[BunchId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @FilterId
				, @BunchId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual void InsertBunchFilterWithtPk(BunchFilter entity)
        {
            string sql = @"
                set identity_insert BunchFilter on
                Insert into BunchFilter ( 
                BunchFilterId
                ,[FilterId]
				,[BunchId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				(
                  @BunchFilterId
                , @FilterId
				, @BunchId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
                set identity_insert BunchFilter off
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
                new SqlParameter("@BunchFilterId",entity.BunchFilterId)
					 ,new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual void UpdateBunchFilterNoReturn(BunchFilter entity)
        {
            string sql = @"Update BunchFilter set  [FilterId]=@FilterId
							, [BunchId]=@BunchId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where BunchFilterId=@BunchFilterId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@BunchFilterId",entity.BunchFilterId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);

        }
    }


}
