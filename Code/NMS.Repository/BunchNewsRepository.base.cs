﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class BunchNewsRepositoryBase : Repository, IBunchNewsRepositoryBase 
	{
        
        public BunchNewsRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("BunchNewsId",new SearchColumn(){Name="BunchNewsId",Title="BunchNewsId",SelectClause="BunchNewsId",WhereClause="AllRecords.BunchNewsId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BunchNewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchId",new SearchColumn(){Name="BunchId",Title="BunchId",SelectClause="BunchId",WhereClause="AllRecords.BunchId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="BunchId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchGuid",new SearchColumn(){Name="BunchGuid",Title="BunchGuid",SelectClause="BunchGuid",WhereClause="AllRecords.BunchGuid",DataType="System.String",IsForeignColumn=false,PropertyName="BunchGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetBunchNewsSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetBunchNewsBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetBunchNewsAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetBunchNewsSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[BunchNews].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[BunchNews].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<BunchNews> GetBunchNewsByBunchId(System.Int32? BunchId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BunchNews] with (nolock)  where BunchId=@BunchId  ";
			SqlParameter parameter=new SqlParameter("@BunchId",BunchId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchNews>(ds,BunchNewsFromDataRow);
		}

		public virtual List<BunchNews> GetBunchNewsByNewsId(System.Int32? NewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BunchNews] with (nolock)  where NewsId=@NewsId  ";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchNews>(ds,BunchNewsFromDataRow);
		}

		public virtual BunchNews GetBunchNews(System.Int32 BunchNewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BunchNews] with (nolock)  where BunchNewsId=@BunchNewsId ";
			SqlParameter parameter=new SqlParameter("@BunchNewsId",BunchNewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return BunchNewsFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<BunchNews> GetBunchNewsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [BunchNews] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchNews>(ds,BunchNewsFromDataRow);
		}

		public virtual List<BunchNews> GetAllBunchNews(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [BunchNews] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchNews>(ds, BunchNewsFromDataRow);
		}

		public virtual List<BunchNews> GetPagedBunchNews(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetBunchNewsCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [BunchNewsId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([BunchNewsId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [BunchNewsId] ";
            tempsql += " FROM [BunchNews] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("BunchNewsId"))
					tempsql += " , (AllRecords.[BunchNewsId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[BunchNewsId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetBunchNewsSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [BunchNews] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [BunchNews].[BunchNewsId] = PageIndex.[BunchNewsId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchNews>(ds, BunchNewsFromDataRow);
			}else{ return null;}
		}

		private int GetBunchNewsCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM BunchNews as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM BunchNews as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(BunchNews))]
		public virtual BunchNews InsertBunchNews(BunchNews entity)
		{

			BunchNews other=new BunchNews();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into BunchNews ( [BunchId]
				,[NewsId]
				,[CreationDate]
				,[IsActive]
				,[BunchGuid]
				,[NewsGuid] )
				Values
				( @BunchId
				, @NewsId
				, @CreationDate
				, @IsActive
				, @BunchGuid
				, @NewsGuid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@BunchId",entity.BunchId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetBunchNews(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(BunchNews))]
		public virtual BunchNews UpdateBunchNews(BunchNews entity)
		{

			if (entity.IsTransient()) return entity;
			BunchNews other = GetBunchNews(entity.BunchNewsId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update BunchNews set  [BunchId]=@BunchId
							, [NewsId]=@NewsId
							, [CreationDate]=@CreationDate
							, [IsActive]=@IsActive
							, [BunchGuid]=@BunchGuid
							, [NewsGuid]=@NewsGuid 
							 where BunchNewsId=@BunchNewsId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@BunchId",entity.BunchId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@BunchNewsId",entity.BunchNewsId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetBunchNews(entity.BunchNewsId);
		}

		public virtual bool DeleteBunchNews(System.Int32 BunchNewsId)
		{

			string sql="delete from BunchNews where BunchNewsId=@BunchNewsId";
			SqlParameter parameter=new SqlParameter("@BunchNewsId",BunchNewsId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(BunchNews))]
		public virtual BunchNews DeleteBunchNews(BunchNews entity)
		{
			this.DeleteBunchNews(entity.BunchNewsId);
			return entity;
		}


		public virtual BunchNews BunchNewsFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			BunchNews entity=new BunchNews();
			if (dr.Table.Columns.Contains("BunchNewsId"))
			{
			entity.BunchNewsId = (System.Int32)dr["BunchNewsId"];
			}
			if (dr.Table.Columns.Contains("BunchId"))
			{
			entity.BunchId = dr["BunchId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["BunchId"];
			}
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = dr["NewsId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("BunchGuid"))
			{
			entity.BunchGuid = dr["BunchGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			return entity;
		}

	}
	
	
}
