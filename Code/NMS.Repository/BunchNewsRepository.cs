﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
    public partial class BunchNewsRepository : BunchNewsRepositoryBase, IBunchNewsRepository
    {
        public BunchNewsRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        //public virtual List<BunchNews> GetBunchNewsByBunchId(System.Int32? BunchId, string SelectClause = null)
        //{

        //    string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
        //    sql += "from BunchNews with (nolock)  where BunchId=@BunchId  ";
        //    SqlParameter parameter = new SqlParameter("@BunchId", BunchId);
        //    DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
        //    if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
        //    return CollectionFromDataSet<BunchNews>(ds, BunchNewsFromDataRow);
        //}

        public virtual List<BunchNews> GetByBunchIdAndNewsid(System.Int32 BunchId, System.Int32 NewsId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from BunchNews with (nolock)  where BunchId=@BunchId and NewsId=@NewsId  ";
            SqlParameter parameter = new SqlParameter("@BunchId", BunchId);
            SqlParameter parameter1 = new SqlParameter("@NewsId", NewsId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<BunchNews>(ds, BunchNewsFromDataRow);
        }

        public virtual List<BunchNews> GetByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from BunchNews with (nolock)  where NewsGuid=@NewsGuid";
            SqlParameter parameter1 = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<BunchNews>(ds, BunchNewsFromDataRow);
        }

        public BunchNewsRepository()
        {
        }
        public virtual bool DeleteByBunchId(System.Int32 BunchId)
        {

            string sql = "delete from BunchNews where BunchId=@BunchId";
            SqlParameter parameter = new SqlParameter("@BunchId", BunchId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual bool DeleteByNewsId(System.Int32 NewsId)
        {
            string sql = "delete from BunchNews where NewsId=@NewsId";
            SqlParameter parameter = new SqlParameter("@NewsId", NewsId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }


        public virtual void InsertBunchNewsWithPk(BunchNews entity)
        {

                string sql = @"
set identity_insert BunchNews on
                Insert into BunchNews ( 
BunchNewsId
,[BunchId]
				,[NewsId]
				,[CreationDate]
				,[IsActive] )
				Values
				( @BunchNewsId,@BunchId
				, @NewsId
				, @CreationDate
				, @IsActive );
set identity_insert BunchNews off
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
                    new SqlParameter("@BunchNewsId",entity.BunchNewsId)
					 ,new SqlParameter("@BunchId",entity.BunchId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
                var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");                          
        }

        public virtual void UpdateBunchNewsNoReturn(BunchNews entity)
        {            
            string sql = @"Update BunchNews set  [BunchId]=@BunchId
							, [NewsId]=@NewsId
							, [CreationDate]=@CreationDate
							, [IsActive]=@IsActive 
							 where BunchNewsId=@BunchNewsId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BunchId",entity.BunchId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@BunchNewsId",entity.BunchNewsId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }
    }
}
