﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{

    public abstract partial class BunchRepositoryBase : Repository, IBunchRepositoryBase
    {

        public BunchRepositoryBase()
        {
            this.SearchColumns = new Dictionary<string, SearchColumn>();

            this.SearchColumns.Add("BunchId", new SearchColumn() { Name = "BunchId", Title = "BunchId", SelectClause = "BunchId", WhereClause = "AllRecords.BunchId", DataType = "System.Int32", IsForeignColumn = false, PropertyName = "BunchId", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("Guid", new SearchColumn() { Name = "Guid", Title = "Guid", SelectClause = "Guid", WhereClause = "AllRecords.Guid", DataType = "System.String", IsForeignColumn = false, PropertyName = "Guid", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("CreationDate", new SearchColumn() { Name = "CreationDate", Title = "CreationDate", SelectClause = "CreationDate", WhereClause = "AllRecords.CreationDate", DataType = "System.DateTime", IsForeignColumn = false, PropertyName = "CreationDate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("LastUpdateDate", new SearchColumn() { Name = "LastUpdateDate", Title = "LastUpdateDate", SelectClause = "LastUpdateDate", WhereClause = "AllRecords.LastUpdateDate", DataType = "System.DateTime", IsForeignColumn = false, PropertyName = "LastUpdateDate", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
            this.SearchColumns.Add("IsActive", new SearchColumn() { Name = "IsActive", Title = "IsActive", SelectClause = "IsActive", WhereClause = "AllRecords.IsActive", DataType = "System.Boolean", IsForeignColumn = false, PropertyName = "IsActive", IsAdvanceSearchColumn = false, IsBasicSearchColumm = false });
        }

        public virtual List<SearchColumn> GetBunchSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }



        public virtual Dictionary<string, string> GetBunchBasicSearchColumns()
        {
            Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
                    keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetBunchAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
                    keyValuePair.Value.Value = string.Empty;
                    searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }


        public virtual string GetBunchSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery = string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
                    if (keyValuePair.Value.IsForeignColumn)
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                        else
                        {
                            selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(selectQuery))
                        {
                            selectQuery = "Bunch." + keyValuePair.Key;
                        }
                        else
                        {
                            selectQuery += ",Bunch." + keyValuePair.Key;
                        }
                    }
                }
            }
            return "Select " + selectQuery + " ";
        }


        public virtual Bunch GetBunch(System.Int32 BunchId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from Bunch with (nolock)  where BunchId=@BunchId ";
            SqlParameter parameter = new SqlParameter("@BunchId", BunchId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return BunchFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Bunch> GetBunchByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += string.Format("from Bunch with (nolock)  where {0} {1} '{2}' ", Key, operand.ToOperandString(), Value);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bunch>(ds, BunchFromDataRow);
        }

        public virtual List<Bunch> GetAllBunch(string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from Bunch with (nolock)  ";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bunch>(ds, BunchFromDataRow);
        }

       

        public virtual List<Bunch> GetPagedBunch(string orderByClause, int pageSize, int startIndex, out int count, List<SearchColumn> searchColumns, string SelectClause = null)
        {

            string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
            if (!String.IsNullOrEmpty(orderByClause))
            {
                KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
            }

            count = GetBunchCount(whereClause, searchColumns);
            if (count > 0)
            {
                if (count < startIndex) startIndex = (count / pageSize) * pageSize;

                int PageLowerBound = startIndex;
                int PageUpperBound = PageLowerBound + pageSize;
                string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [BunchId] int				   
				            );";

                //Insert into the temp table
                string tempsql = "INSERT INTO #PageIndex ([BunchId])";
                tempsql += " SELECT ";
                if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
                tempsql += " [BunchId] ";
                tempsql += " FROM [Bunch] AllRecords with (NOLOCK)";
                if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
                if (orderByClause.Length > 0)
                {
                    tempsql += " ORDER BY " + orderByClause;
                    if (!orderByClause.Contains("BunchId"))
                        tempsql += " , (AllRecords.[BunchId])";
                }
                else
                {
                    tempsql += " ORDER BY (AllRecords.[BunchId])";
                }

                // Return paged results
                string pagedResultsSql =
                    (string.IsNullOrEmpty(SelectClause) ? GetBunchSelectClause() : (string.Format("Select {0} ", SelectClause))) + @" FROM [Bunch] , #PageIndex PageIndex WHERE ";
                pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString();
                pagedResultsSql += @" AND [Bunch].[BunchId] = PageIndex.[BunchId] 
				                  ORDER BY PageIndex.IndexId;";
                pagedResultsSql += " drop table #PageIndex";
                sql = sql + tempsql + pagedResultsSql;
                sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<Bunch>(ds, BunchFromDataRow);
            }
            else { return null; }
        }

        private int GetBunchCount(string whereClause, List<SearchColumn> searchColumns)
        {

            string sql = string.Empty;
            if (string.IsNullOrEmpty(whereClause))
                sql = "SELECT Count(*) FROM Bunch as AllRecords  ";
            else
                sql = "SELECT Count(*) FROM Bunch as AllRecords  where  " + whereClause;
            var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
            return rowCount == DBNull.Value ? 0 : (int)rowCount;
        }

        [MOLog(AuditOperations.Create, typeof(Bunch))]
        public virtual Bunch InsertBunch(Bunch entity)
        {

            Bunch other = new Bunch();
            other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into Bunch ( [Guid]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Guid
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
                var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
                return GetBunch(Convert.ToInt32(identity));
            }
            return entity;
        }

        [MOLog(AuditOperations.Create, typeof(Bunch))]
        public virtual void InsertBunchNoReturn(Bunch entity)
        {
            string sql = @"Insert into Bunch ( [Guid]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Guid
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        [MOLog(AuditOperations.Update, typeof(Bunch))]
        public virtual Bunch UpdateBunch(Bunch entity)
        {

            if (entity.IsTransient()) return entity;
            Bunch other = GetBunch(entity.BunchId);
            if (entity.Equals(other)) return entity;
            string sql = @"Update Bunch set  [Guid]=@Guid
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where BunchId=@BunchId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@BunchId",entity.BunchId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetBunch(entity.BunchId);
        }

        public virtual bool DeleteBunch(System.Int32 BunchId)
        {

            string sql = "delete from Bunch where BunchId=@BunchId";
            SqlParameter parameter = new SqlParameter("@BunchId", BunchId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        [MOLog(AuditOperations.Delete, typeof(Bunch))]
        public virtual Bunch DeleteBunch(Bunch entity)
        {
            this.DeleteBunch(entity.BunchId);
            return entity;
        }


        public virtual Bunch BunchFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            Bunch entity = new Bunch();
            if (dr.Table.Columns.Contains("BunchId"))
            {
                entity.BunchId = (System.Int32)dr["BunchId"];
            }
            if (dr.Table.Columns.Contains("Guid"))
            {
                entity.Guid = dr["Guid"].ToString();
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("IsActive"))
            {
                entity.IsActive = (System.Boolean)dr["IsActive"];
            }
            return entity;
        }

    }


}
