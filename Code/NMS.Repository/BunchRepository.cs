﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Models;

namespace NMS.Repository
{

    public partial class BunchRepository : BunchRepositoryBase, IBunchRepository
    {
        public BunchRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public BunchRepository()
        {
        }


        public virtual Bunch GetBunchByGuid(System.String BunchGuid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += " from [Bunch] where Bunch.[Guid] = @BunchGuid";
            //string sql = "select top 1 * from [Bunch] where Bunch.[Guid] = @BunchGuid";
            SqlParameter parameter = new SqlParameter("@BunchGuid", BunchGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return BunchFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual List<Bunch> GetByBunchIds(string bunches, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from Bunch with (nolock) where Guid in (" + bunches + ")";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bunch>(ds, BunchFromDataRow);
        }

        public virtual bool DeleteBunchByGuid(System.String Guid)
        {

            string sql = "delete from Bunch where Guid=@Guid";
            SqlParameter parameter = new SqlParameter("@Guid", Guid);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual DateTime GetLastUpdatedDate()
        {

            string sql = "select LastUpdateddate from Bunch with (nolock) order by 1 desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return Convert.ToDateTime("1/1/2014");
            return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
        }

        public virtual List<Bunch> GetByUpdateddate(string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from Bunch with (nolock)  ";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Bunch>(ds, BunchFromDataRow);
        }

        public virtual void InsertBunchWIthPK(Bunch entity)
        {
            string sql = @"
                set identity_insert Bunch on
                Insert into Bunch ( 
                 BunchId
                ,[Guid]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				(
                  @BunchId   
                , @Guid
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );

                set identity_insert Bunch off
				Select scope_identity()
                ";
            SqlParameter[] parameterArray = new SqlParameter[]{
                      new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");            
        }

        public virtual void UpdateBunchNoReturn(Bunch entity)
        {
            string sql = @"Update Bunch set  [Guid]=@Guid
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where BunchId=@BunchId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@BunchId",entity.BunchId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
          
        }
    }


}
