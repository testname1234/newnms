﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class BunchResourceRepository : BunchResourceRepositoryBase, IBunchResourceRepository
    {
        public BunchResourceRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public BunchResourceRepository()
        {
        }
        
        public virtual void InsertBunchResourceWithPk(BunchResource entity)
        {
                string sql = @"
set identity_insert BunchResource on
Insert into BunchResource (BunchResourceId, [BunchId]
				,[ResourceId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @BunchResourceId,@BunchId
				, @ResourceId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
set identity_insert BunchResource off
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@BunchResourceId",entity.BunchResourceId)
                    , new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@ResourceId",entity.ResourceId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
                var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
               
        }
        
        public virtual void UpdateBunchResourceNoReturn(BunchResource entity)
        {      
            string sql = @"Update BunchResource set 
                              [BunchId]=@BunchId
							, [ResourceId]=@ResourceId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where BunchResourceId=@BunchResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{                      
					 new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@ResourceId",entity.ResourceId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@BunchResourceId",entity.BunchResourceId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }
    }


}
