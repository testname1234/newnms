﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class BunchTagRepositoryBase : Repository, IBunchTagRepositoryBase 
	{
        
        public BunchTagRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("BunchTagId",new SearchColumn(){Name="BunchTagId",Title="BunchTagId",SelectClause="BunchTagId",WhereClause="AllRecords.BunchTagId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BunchTagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchId",new SearchColumn(){Name="BunchId",Title="BunchId",SelectClause="BunchId",WhereClause="AllRecords.BunchId",DataType="System.Int32",IsForeignColumn=false,PropertyName="BunchId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TagId",new SearchColumn(){Name="TagId",Title="TagId",SelectClause="TagId",WhereClause="AllRecords.TagId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetBunchTagSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetBunchTagBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetBunchTagAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetBunchTagSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "BunchTag."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",BunchTag."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<BunchTag> GetBunchTagByBunchId(System.Int32 BunchId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchTag with (nolock)  where BunchId=@BunchId  ";
			SqlParameter parameter=new SqlParameter("@BunchId",BunchId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchTag>(ds,BunchTagFromDataRow);
		}

		public virtual List<BunchTag> GetBunchTagByTagId(System.Int32 TagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchTag with (nolock)  where TagId=@TagId  ";
			SqlParameter parameter=new SqlParameter("@TagId",TagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchTag>(ds,BunchTagFromDataRow);
		}

		public virtual BunchTag GetBunchTag(System.Int32 BunchTagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchTag with (nolock)  where BunchTagId=@BunchTagId ";
			SqlParameter parameter=new SqlParameter("@BunchTagId",BunchTagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return BunchTagFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<BunchTag> GetBunchTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from BunchTag with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchTag>(ds,BunchTagFromDataRow);
		}

		public virtual List<BunchTag> GetAllBunchTag(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetBunchTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from BunchTag with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchTag>(ds, BunchTagFromDataRow);
		}

		public virtual List<BunchTag> GetPagedBunchTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetBunchTagCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [BunchTagId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([BunchTagId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [BunchTagId] ";
            tempsql += " FROM [BunchTag] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("BunchTagId"))
					tempsql += " , (AllRecords.[BunchTagId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[BunchTagId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetBunchTagSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [BunchTag] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [BunchTag].[BunchTagId] = PageIndex.[BunchTagId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<BunchTag>(ds, BunchTagFromDataRow);
			}else{ return null;}
		}

		private int GetBunchTagCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM BunchTag as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM BunchTag as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(BunchTag))]
		public virtual BunchTag InsertBunchTag(BunchTag entity)
		{

			BunchTag other=new BunchTag();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into BunchTag ( [BunchId]
				,[TagId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @BunchId
				, @TagId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetBunchTag(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(BunchTag))]
		public virtual BunchTag UpdateBunchTag(BunchTag entity)
		{

			if (entity.IsTransient()) return entity;
			BunchTag other = GetBunchTag(entity.BunchTagId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update BunchTag set  [BunchId]=@BunchId
							, [TagId]=@TagId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where BunchTagId=@BunchTagId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@BunchTagId",entity.BunchTagId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetBunchTag(entity.BunchTagId);
		}

		public virtual bool DeleteBunchTag(System.Int32 BunchTagId)
		{

			string sql="delete from BunchTag with (nolock) where BunchTagId=@BunchTagId";
			SqlParameter parameter=new SqlParameter("@BunchTagId",BunchTagId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(BunchTag))]
		public virtual BunchTag DeleteBunchTag(BunchTag entity)
		{
			this.DeleteBunchTag(entity.BunchTagId);
			return entity;
		}


		public virtual BunchTag BunchTagFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			BunchTag entity=new BunchTag();
			if (dr.Table.Columns.Contains("BunchTagId"))
			{
			entity.BunchTagId = (System.Int32)dr["BunchTagId"];
			}
			if (dr.Table.Columns.Contains("BunchId"))
			{
			entity.BunchId = (System.Int32)dr["BunchId"];
			}
			if (dr.Table.Columns.Contains("TagId"))
			{
			entity.TagId = (System.Int32)dr["TagId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
