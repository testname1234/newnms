﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{

    public partial class BunchTagRepository : BunchTagRepositoryBase, IBunchTagRepository
    {
        public BunchTagRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public BunchTagRepository()
        {
        }
        public virtual BunchTag GetByBunchIdAndTagId(System.Int32 BunchId, System.Int32 TagId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetBunchTagSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from BunchTag with (nolock)  where BunchId=@BunchId And TagId=@TagId ";
            SqlParameter parameter1 = new SqlParameter("@BunchId", BunchId);
            SqlParameter parameter2 = new SqlParameter("@TagId", TagId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return BunchTagFromDataRow(ds.Tables[0].Rows[0]);
        }


        public virtual bool InsertBunchTagNoReturn(BunchTag entity)
        {
            string sql = @"Insert into BunchTag ( [BunchId]
				,[TagId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @BunchId
				, @TagId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value)
            {
                return false;
                throw new DataException("Identity column was null as a result of the insert operation.");
            }
            return true;
        }

        public virtual void InsertBunchTagWithPk(BunchTag entity)
        {

            string sql = @"
set identity_insert BunchTag on
Insert into BunchTag (BunchTagId, [BunchId]
				,[TagId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @BunchTagId,@BunchId
				, @TagId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
set identity_insert BunchTag off
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
                new SqlParameter("@BunchTagId",entity.BunchTagId)
					, new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value)
            {             
                throw new DataException("Identity column was null as a result of the insert operation.");
            }           
        }

        
        public virtual void UpdateBunchTagNoReturn(BunchTag entity)
        {            
            string sql = @"Update BunchTag set  [BunchId]=@BunchId
							, [TagId]=@TagId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where BunchTagId=@BunchTagId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BunchId",entity.BunchId)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@BunchTagId",entity.BunchTagId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }
    }


}
