﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CasperFlashTemplateWindowRepositoryBase : Repository, ICasperFlashTemplateWindowRepositoryBase 
	{
        
        public CasperFlashTemplateWindowRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CasperFlashTemplateWindowId",new SearchColumn(){Name="CasperFlashTemplateWindowId",Title="CasperFlashTemplateWindowId",SelectClause="CasperFlashTemplateWindowId",WhereClause="AllRecords.CasperFlashTemplateWindowId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CasperFlashTemplateWindowId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashTemplateId",new SearchColumn(){Name="FlashTemplateId",Title="FlashTemplateId",SelectClause="FlashTemplateId",WhereClause="AllRecords.FlashTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TopLeftx",new SearchColumn(){Name="TopLeftx",Title="TopLeftx",SelectClause="TopLeftx",WhereClause="AllRecords.TopLeftx",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="TopLeftx",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BottomRighty",new SearchColumn(){Name="BottomRighty",Title="BottomRighty",SelectClause="BottomRighty",WhereClause="AllRecords.BottomRighty",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="BottomRighty",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Width",new SearchColumn(){Name="Width",Title="Width",SelectClause="Width",WhereClause="AllRecords.Width",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="Width",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Height",new SearchColumn(){Name="Height",Title="Height",SelectClause="Height",WhereClause="AllRecords.Height",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="Height",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PortId",new SearchColumn(){Name="PortId",Title="PortId",SelectClause="PortId",WhereClause="AllRecords.PortId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="PortId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashWindowTypeId",new SearchColumn(){Name="FlashWindowTypeId",Title="FlashWindowTypeId",SelectClause="FlashWindowTypeId",WhereClause="AllRecords.FlashWindowTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashWindowTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCasperFlashTemplateWindowSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCasperFlashTemplateWindowBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCasperFlashTemplateWindowAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCasperFlashTemplateWindowSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CasperFlashTemplateWindow].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CasperFlashTemplateWindow].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual CasperFlashTemplateWindow GetCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperFlashTemplateWindowSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperFlashTemplateWindow] with (nolock)  where CasperFlashTemplateWindowId=@CasperFlashTemplateWindowId ";
			SqlParameter parameter=new SqlParameter("@CasperFlashTemplateWindowId",CasperFlashTemplateWindowId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CasperFlashTemplateWindowFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CasperFlashTemplateWindow> GetCasperFlashTemplateWindowByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperFlashTemplateWindowSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CasperFlashTemplateWindow] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperFlashTemplateWindow>(ds,CasperFlashTemplateWindowFromDataRow);
		}

		public virtual List<CasperFlashTemplateWindow> GetAllCasperFlashTemplateWindow(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperFlashTemplateWindowSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperFlashTemplateWindow] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperFlashTemplateWindow>(ds, CasperFlashTemplateWindowFromDataRow);
		}

		public virtual List<CasperFlashTemplateWindow> GetPagedCasperFlashTemplateWindow(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCasperFlashTemplateWindowCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CasperFlashTemplateWindowId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CasperFlashTemplateWindowId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CasperFlashTemplateWindowId] ";
            tempsql += " FROM [CasperFlashTemplateWindow] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CasperFlashTemplateWindowId"))
					tempsql += " , (AllRecords.[CasperFlashTemplateWindowId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CasperFlashTemplateWindowId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCasperFlashTemplateWindowSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CasperFlashTemplateWindow] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CasperFlashTemplateWindow].[CasperFlashTemplateWindowId] = PageIndex.[CasperFlashTemplateWindowId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperFlashTemplateWindow>(ds, CasperFlashTemplateWindowFromDataRow);
			}else{ return null;}
		}

		private int GetCasperFlashTemplateWindowCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CasperFlashTemplateWindow as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CasperFlashTemplateWindow as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CasperFlashTemplateWindow))]
		public virtual CasperFlashTemplateWindow InsertCasperFlashTemplateWindow(CasperFlashTemplateWindow entity)
		{

			CasperFlashTemplateWindow other=new CasperFlashTemplateWindow();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CasperFlashTemplateWindow ( [FlashTemplateId]
				,[Name]
				,[TopLeftx]
				,[BottomRighty]
				,[Width]
				,[Height]
				,[PortId]
				,[FlashWindowTypeId]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @FlashTemplateId
				, @Name
				, @TopLeftx
				, @BottomRighty
				, @Width
				, @Height
				, @PortId
				, @FlashWindowTypeId
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FlashTemplateId",entity.FlashTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@TopLeftx",entity.TopLeftx ?? (object)DBNull.Value)
					, new SqlParameter("@BottomRighty",entity.BottomRighty ?? (object)DBNull.Value)
					, new SqlParameter("@Width",entity.Width ?? (object)DBNull.Value)
					, new SqlParameter("@Height",entity.Height ?? (object)DBNull.Value)
					, new SqlParameter("@PortId",entity.PortId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashWindowTypeId",entity.FlashWindowTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCasperFlashTemplateWindow(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CasperFlashTemplateWindow))]
		public virtual CasperFlashTemplateWindow UpdateCasperFlashTemplateWindow(CasperFlashTemplateWindow entity)
		{

			if (entity.IsTransient()) return entity;
			CasperFlashTemplateWindow other = GetCasperFlashTemplateWindow(entity.CasperFlashTemplateWindowId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CasperFlashTemplateWindow set  [FlashTemplateId]=@FlashTemplateId
							, [Name]=@Name
							, [TopLeftx]=@TopLeftx
							, [BottomRighty]=@BottomRighty
							, [Width]=@Width
							, [Height]=@Height
							, [PortId]=@PortId
							, [FlashWindowTypeId]=@FlashWindowTypeId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where CasperFlashTemplateWindowId=@CasperFlashTemplateWindowId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FlashTemplateId",entity.FlashTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@TopLeftx",entity.TopLeftx ?? (object)DBNull.Value)
					, new SqlParameter("@BottomRighty",entity.BottomRighty ?? (object)DBNull.Value)
					, new SqlParameter("@Width",entity.Width ?? (object)DBNull.Value)
					, new SqlParameter("@Height",entity.Height ?? (object)DBNull.Value)
					, new SqlParameter("@PortId",entity.PortId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashWindowTypeId",entity.FlashWindowTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@CasperFlashTemplateWindowId",entity.CasperFlashTemplateWindowId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCasperFlashTemplateWindow(entity.CasperFlashTemplateWindowId);
		}

		public virtual bool DeleteCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId)
		{

			string sql="delete from CasperFlashTemplateWindow where CasperFlashTemplateWindowId=@CasperFlashTemplateWindowId";
			SqlParameter parameter=new SqlParameter("@CasperFlashTemplateWindowId",CasperFlashTemplateWindowId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CasperFlashTemplateWindow))]
		public virtual CasperFlashTemplateWindow DeleteCasperFlashTemplateWindow(CasperFlashTemplateWindow entity)
		{
			this.DeleteCasperFlashTemplateWindow(entity.CasperFlashTemplateWindowId);
			return entity;
		}


		public virtual CasperFlashTemplateWindow CasperFlashTemplateWindowFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CasperFlashTemplateWindow entity=new CasperFlashTemplateWindow();
			if (dr.Table.Columns.Contains("CasperFlashTemplateWindowId"))
			{
			entity.CasperFlashTemplateWindowId = (System.Int32)dr["CasperFlashTemplateWindowId"];
			}
			if (dr.Table.Columns.Contains("FlashTemplateId"))
			{
			entity.FlashTemplateId = dr["FlashTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashTemplateId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("TopLeftx"))
			{
			entity.TopLeftx = dr["TopLeftx"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["TopLeftx"];
			}
			if (dr.Table.Columns.Contains("BottomRighty"))
			{
			entity.BottomRighty = dr["BottomRighty"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["BottomRighty"];
			}
			if (dr.Table.Columns.Contains("Width"))
			{
			entity.Width = dr["Width"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["Width"];
			}
			if (dr.Table.Columns.Contains("Height"))
			{
			entity.Height = dr["Height"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["Height"];
			}
			if (dr.Table.Columns.Contains("PortId"))
			{
			entity.PortId = dr["PortId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["PortId"];
			}
			if (dr.Table.Columns.Contains("FlashWindowTypeId"))
			{
			entity.FlashWindowTypeId = dr["FlashWindowTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashWindowTypeId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
