﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CasperItemTransformationRepositoryBase : Repository, ICasperItemTransformationRepositoryBase 
	{
        
        public CasperItemTransformationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CasperItemTransformationId",new SearchColumn(){Name="CasperItemTransformationId",Title="CasperItemTransformationId",SelectClause="CasperItemTransformationId",WhereClause="AllRecords.CasperItemTransformationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CasperItemTransformationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CasperTemplateItemId",new SearchColumn(){Name="CasperTemplateItemId",Title="CasperTemplateItemId",SelectClause="CasperTemplateItemId",WhereClause="AllRecords.CasperTemplateItemId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CasperTemplateItemId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Label",new SearchColumn(){Name="Label",Title="Label",SelectClause="Label",WhereClause="AllRecords.Label",DataType="System.String",IsForeignColumn=false,PropertyName="Label",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Channel",new SearchColumn(){Name="Channel",Title="Channel",SelectClause="Channel",WhereClause="AllRecords.Channel",DataType="System.String",IsForeignColumn=false,PropertyName="Channel",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Videolayer",new SearchColumn(){Name="Videolayer",Title="Videolayer",SelectClause="Videolayer",WhereClause="AllRecords.Videolayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Videolayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Devicename",new SearchColumn(){Name="Devicename",Title="Devicename",SelectClause="Devicename",WhereClause="AllRecords.Devicename",DataType="System.String",IsForeignColumn=false,PropertyName="Devicename",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Allowgpi",new SearchColumn(){Name="Allowgpi",Title="Allowgpi",SelectClause="Allowgpi",WhereClause="AllRecords.Allowgpi",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Allowgpi",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Allowremotetriggering",new SearchColumn(){Name="Allowremotetriggering",Title="Allowremotetriggering",SelectClause="Allowremotetriggering",WhereClause="AllRecords.Allowremotetriggering",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Allowremotetriggering",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Remotetriggerid",new SearchColumn(){Name="Remotetriggerid",Title="Remotetriggerid",SelectClause="Remotetriggerid",WhereClause="AllRecords.Remotetriggerid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Remotetriggerid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Tween",new SearchColumn(){Name="Tween",Title="Tween",SelectClause="Tween",WhereClause="AllRecords.Tween",DataType="System.String",IsForeignColumn=false,PropertyName="Tween",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("triggeronnext",new SearchColumn(){Name="triggeronnext",Title="triggeronnext",SelectClause="triggeronnext",WhereClause="AllRecords.triggeronnext",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Triggeronnext",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Positionx",new SearchColumn(){Name="Positionx",Title="Positionx",SelectClause="Positionx",WhereClause="AllRecords.Positionx",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positionx",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Positiony",new SearchColumn(){Name="Positiony",Title="Positiony",SelectClause="Positiony",WhereClause="AllRecords.Positiony",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positiony",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Scalex",new SearchColumn(){Name="Scalex",Title="Scalex",SelectClause="Scalex",WhereClause="AllRecords.Scalex",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scalex",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Scaley",new SearchColumn(){Name="Scaley",Title="Scaley",SelectClause="Scaley",WhereClause="AllRecords.Scaley",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scaley",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("defer",new SearchColumn(){Name="defer",Title="defer",SelectClause="defer",WhereClause="AllRecords.defer",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Defer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashWindowTypeId",new SearchColumn(){Name="FlashWindowTypeId",Title="FlashWindowTypeId",SelectClause="FlashWindowTypeId",WhereClause="AllRecords.FlashWindowTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashWindowTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Flashtemplatewindowid",new SearchColumn(){Name="Flashtemplatewindowid",Title="Flashtemplatewindowid",SelectClause="Flashtemplatewindowid",WhereClause="AllRecords.Flashtemplatewindowid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Flashtemplatewindowid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCasperItemTransformationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCasperItemTransformationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCasperItemTransformationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCasperItemTransformationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CasperItemTransformation].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CasperItemTransformation].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual CasperItemTransformation GetCasperItemTransformation(System.Int32 CasperItemTransformationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperItemTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperItemTransformation] with (nolock)  where CasperItemTransformationId=@CasperItemTransformationId ";
			SqlParameter parameter=new SqlParameter("@CasperItemTransformationId",CasperItemTransformationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CasperItemTransformationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CasperItemTransformation> GetCasperItemTransformationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperItemTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CasperItemTransformation] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperItemTransformation>(ds,CasperItemTransformationFromDataRow);
		}

		public virtual List<CasperItemTransformation> GetAllCasperItemTransformation(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperItemTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperItemTransformation] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperItemTransformation>(ds, CasperItemTransformationFromDataRow);
		}

		public virtual List<CasperItemTransformation> GetPagedCasperItemTransformation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCasperItemTransformationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CasperItemTransformationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CasperItemTransformationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CasperItemTransformationId] ";
            tempsql += " FROM [CasperItemTransformation] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CasperItemTransformationId"))
					tempsql += " , (AllRecords.[CasperItemTransformationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CasperItemTransformationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCasperItemTransformationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CasperItemTransformation] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CasperItemTransformation].[CasperItemTransformationId] = PageIndex.[CasperItemTransformationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperItemTransformation>(ds, CasperItemTransformationFromDataRow);
			}else{ return null;}
		}

		private int GetCasperItemTransformationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CasperItemTransformation as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CasperItemTransformation as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CasperItemTransformation))]
		public virtual CasperItemTransformation InsertCasperItemTransformation(CasperItemTransformation entity)
		{

			CasperItemTransformation other=new CasperItemTransformation();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CasperItemTransformation ( [CasperTemplateItemId]
				,[Type]
				,[Label]
				,[Name]
				,[Channel]
				,[Videolayer]
				,[Devicename]
				,[Allowgpi]
				,[Allowremotetriggering]
				,[Remotetriggerid]
				,[Tween]
				,[triggeronnext]
				,[Positionx]
				,[Positiony]
				,[Scalex]
				,[Scaley]
				,[defer]
				,[FlashWindowTypeId]
				,[Flashtemplatewindowid] )
				Values
				( @CasperTemplateItemId
				, @Type
				, @Label
				, @Name
				, @Channel
				, @Videolayer
				, @Devicename
				, @Allowgpi
				, @Allowremotetriggering
				, @Remotetriggerid
				, @Tween
				, @triggeronnext
				, @Positionx
				, @Positiony
				, @Scalex
				, @Scaley
				, @defer
				, @FlashWindowTypeId
				, @Flashtemplatewindowid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CasperTemplateItemId",entity.CasperTemplateItemId ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@Videolayer",entity.Videolayer ?? (object)DBNull.Value)
					, new SqlParameter("@Devicename",entity.Devicename ?? (object)DBNull.Value)
					, new SqlParameter("@Allowgpi",entity.Allowgpi ?? (object)DBNull.Value)
					, new SqlParameter("@Allowremotetriggering",entity.Allowremotetriggering ?? (object)DBNull.Value)
					, new SqlParameter("@Remotetriggerid",entity.Remotetriggerid ?? (object)DBNull.Value)
					, new SqlParameter("@Tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@Positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@Positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@Scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@Scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@FlashWindowTypeId",entity.FlashWindowTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Flashtemplatewindowid",entity.Flashtemplatewindowid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCasperItemTransformation(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CasperItemTransformation))]
		public virtual CasperItemTransformation UpdateCasperItemTransformation(CasperItemTransformation entity)
		{

			if (entity.IsTransient()) return entity;
			CasperItemTransformation other = GetCasperItemTransformation(entity.CasperItemTransformationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CasperItemTransformation set  [CasperTemplateItemId]=@CasperTemplateItemId
							, [Type]=@Type
							, [Label]=@Label
							, [Name]=@Name
							, [Channel]=@Channel
							, [Videolayer]=@Videolayer
							, [Devicename]=@Devicename
							, [Allowgpi]=@Allowgpi
							, [Allowremotetriggering]=@Allowremotetriggering
							, [Remotetriggerid]=@Remotetriggerid
							, [Tween]=@Tween
							, [triggeronnext]=@triggeronnext
							, [Positionx]=@Positionx
							, [Positiony]=@Positiony
							, [Scalex]=@Scalex
							, [Scaley]=@Scaley
							, [defer]=@defer
							, [FlashWindowTypeId]=@FlashWindowTypeId
							, [Flashtemplatewindowid]=@Flashtemplatewindowid 
							 where CasperItemTransformationId=@CasperItemTransformationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CasperTemplateItemId",entity.CasperTemplateItemId ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@Videolayer",entity.Videolayer ?? (object)DBNull.Value)
					, new SqlParameter("@Devicename",entity.Devicename ?? (object)DBNull.Value)
					, new SqlParameter("@Allowgpi",entity.Allowgpi ?? (object)DBNull.Value)
					, new SqlParameter("@Allowremotetriggering",entity.Allowremotetriggering ?? (object)DBNull.Value)
					, new SqlParameter("@Remotetriggerid",entity.Remotetriggerid ?? (object)DBNull.Value)
					, new SqlParameter("@Tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@Positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@Positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@Scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@Scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@FlashWindowTypeId",entity.FlashWindowTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Flashtemplatewindowid",entity.Flashtemplatewindowid ?? (object)DBNull.Value)
					, new SqlParameter("@CasperItemTransformationId",entity.CasperItemTransformationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCasperItemTransformation(entity.CasperItemTransformationId);
		}

		public virtual bool DeleteCasperItemTransformation(System.Int32 CasperItemTransformationId)
		{

			string sql="delete from CasperItemTransformation where CasperItemTransformationId=@CasperItemTransformationId";
			SqlParameter parameter=new SqlParameter("@CasperItemTransformationId",CasperItemTransformationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CasperItemTransformation))]
		public virtual CasperItemTransformation DeleteCasperItemTransformation(CasperItemTransformation entity)
		{
			this.DeleteCasperItemTransformation(entity.CasperItemTransformationId);
			return entity;
		}


		public virtual CasperItemTransformation CasperItemTransformationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CasperItemTransformation entity=new CasperItemTransformation();
			if (dr.Table.Columns.Contains("CasperItemTransformationId"))
			{
			entity.CasperItemTransformationId = (System.Int32)dr["CasperItemTransformationId"];
			}
			if (dr.Table.Columns.Contains("CasperTemplateItemId"))
			{
			entity.CasperTemplateItemId = dr["CasperTemplateItemId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CasperTemplateItemId"];
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"].ToString();
			}
			if (dr.Table.Columns.Contains("Label"))
			{
			entity.Label = dr["Label"].ToString();
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("Channel"))
			{
			entity.Channel = dr["Channel"].ToString();
			}
			if (dr.Table.Columns.Contains("Videolayer"))
			{
			entity.Videolayer = dr["Videolayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Videolayer"];
			}
			if (dr.Table.Columns.Contains("Devicename"))
			{
			entity.Devicename = dr["Devicename"].ToString();
			}
			if (dr.Table.Columns.Contains("Allowgpi"))
			{
			entity.Allowgpi = dr["Allowgpi"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Allowgpi"];
			}
			if (dr.Table.Columns.Contains("Allowremotetriggering"))
			{
			entity.Allowremotetriggering = dr["Allowremotetriggering"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Allowremotetriggering"];
			}
			if (dr.Table.Columns.Contains("Remotetriggerid"))
			{
			entity.Remotetriggerid = dr["Remotetriggerid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Remotetriggerid"];
			}
			if (dr.Table.Columns.Contains("Tween"))
			{
			entity.Tween = dr["Tween"].ToString();
			}
			if (dr.Table.Columns.Contains("triggeronnext"))
			{
			entity.Triggeronnext = dr["triggeronnext"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["triggeronnext"];
			}
			if (dr.Table.Columns.Contains("Positionx"))
			{
			entity.Positionx = dr["Positionx"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Positionx"];
			}
			if (dr.Table.Columns.Contains("Positiony"))
			{
			entity.Positiony = dr["Positiony"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Positiony"];
			}
			if (dr.Table.Columns.Contains("Scalex"))
			{
			entity.Scalex = dr["Scalex"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Scalex"];
			}
			if (dr.Table.Columns.Contains("Scaley"))
			{
			entity.Scaley = dr["Scaley"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Scaley"];
			}
			if (dr.Table.Columns.Contains("defer"))
			{
			entity.Defer = dr["defer"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["defer"];
			}
			if (dr.Table.Columns.Contains("FlashWindowTypeId"))
			{
			entity.FlashWindowTypeId = dr["FlashWindowTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashWindowTypeId"];
			}
			if (dr.Table.Columns.Contains("Flashtemplatewindowid"))
			{
			entity.Flashtemplatewindowid = dr["Flashtemplatewindowid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Flashtemplatewindowid"];
			}
			return entity;
		}

	}
	
	
}
