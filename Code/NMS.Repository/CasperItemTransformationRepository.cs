﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class CasperItemTransformationRepository: CasperItemTransformationRepositoryBase, ICasperItemTransformationRepository
	{
        public CasperItemTransformation GetCasperItemTransformationByCasperTemplateItemId(System.Int32 CasperTemplateItemId, string SelectClause = null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperItemTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
            sql += "from [CasperItemTransformation] with (nolock)  where CasperTemplateItemId=@CasperTemplateItemId ";
            SqlParameter parameter = new SqlParameter("@CasperTemplateItemId", CasperTemplateItemId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CasperItemTransformationFromDataRow(ds.Tables[0].Rows[0]);
		}

        public CasperItemTransformation GetByFlashtemplatewindowid(System.Int32 Flashtemplatewindowid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperItemTransformationSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperItemTransformation] with (nolock)  where Flashtemplatewindowid=@Flashtemplatewindowid ";
            SqlParameter parameter = new SqlParameter("@Flashtemplatewindowid", Flashtemplatewindowid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperItemTransformationFromDataRow(ds.Tables[0].Rows[0]);
        }


	}
	
	
}
