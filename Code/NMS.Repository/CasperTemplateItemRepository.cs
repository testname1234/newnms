﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class CasperTemplateItemRepository: CasperTemplateItemRepositoryBase, ICasperTemplateItemRepository
	{

        public virtual List<CasperTemplateItem> GetCasperTemplateItemByCasperTemplateId(System.Int32 CasperTemplateId,string SelectClause = null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateItemSelectClause():(string.Format("Select {0} ",SelectClause));
            sql += "from [CasperTemplateItem] with (nolock)  where casperTemplateId=@casperTemplateId ";
            SqlParameter parameter = new SqlParameter("@casperTemplateId", CasperTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<CasperTemplateItem>(ds, CasperTemplateItemFromDataRow);

		}

        public virtual CasperTemplateItem GetCasperTemplateItemByCasperTemplateIdAndType(System.Int32 CasperTemplateId,string type, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateItemSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplateItem] with (nolock)  where casperTemplateId=@casperTemplateId and  Type=@Type";
            SqlParameter parameter = new SqlParameter("@casperTemplateId", CasperTemplateId);
            SqlParameter parameter1 = new SqlParameter("@Type", type);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateItemFromDataRow(ds.Tables[0].Rows[0]);
        }


        public virtual CasperTemplateItem GetCasperTemplateItemByitemType(string type, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateItemSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplateItem] with (nolock)  where ItemName=@ItemName";
            SqlParameter parameter = new SqlParameter("@ItemName", type);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter});
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateItemFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual CasperTemplateItem GetByFlashTemplateWindowId(int FlashTemplateWindowId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateItemSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplateItem] with (nolock)  where FlashTemplateWindowId=@FlashTemplateWindowId";
            SqlParameter parameter = new SqlParameter("@FlashTemplateWindowId", FlashTemplateWindowId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateItemFromDataRow(ds.Tables[0].Rows[0]);
        }
        public virtual CasperTemplateItem GetByItemName(string name, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateItemSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplateItem] with (nolock)  where Name=@name";
            SqlParameter parameter = new SqlParameter("@name", name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateItemFromDataRow(ds.Tables[0].Rows[0]);
        }
        public virtual CasperTemplateItem GetByLabel(string name, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateItemSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplateItem] with (nolock)  where Label=@name";
            SqlParameter parameter = new SqlParameter("@name", name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateItemFromDataRow(ds.Tables[0].Rows[0]);
        }

        
        

	}
	
	
}
