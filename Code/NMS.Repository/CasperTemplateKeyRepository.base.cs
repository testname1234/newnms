﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CasperTemplateKeyRepositoryBase : Repository, ICasperTemplateKeyRepositoryBase 
	{
        
        public CasperTemplateKeyRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CasperTemplateKeyId",new SearchColumn(){Name="CasperTemplateKeyId",Title="CasperTemplateKeyId",SelectClause="CasperTemplateKeyId",WhereClause="AllRecords.CasperTemplateKeyId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CasperTemplateKeyId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashTemplateId",new SearchColumn(){Name="FlashTemplateId",Title="FlashTemplateId",SelectClause="FlashTemplateId",WhereClause="AllRecords.FlashTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashKey",new SearchColumn(){Name="FlashKey",Title="FlashKey",SelectClause="FlashKey",WhereClause="AllRecords.FlashKey",DataType="System.String",IsForeignColumn=false,PropertyName="FlashKey",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashTemplateKeyId",new SearchColumn(){Name="FlashTemplateKeyId",Title="FlashTemplateKeyId",SelectClause="FlashTemplateKeyId",WhereClause="AllRecords.FlashTemplateKeyId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashTemplateKeyId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCasperTemplateKeySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCasperTemplateKeyBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCasperTemplateKeyAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCasperTemplateKeySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CasperTemplateKey].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CasperTemplateKey].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual CasperTemplateKey GetCasperTemplateKey(System.Int32 CasperTemplateKeyId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateKeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperTemplateKey] with (nolock)  where CasperTemplateKeyId=@CasperTemplateKeyId ";
			SqlParameter parameter=new SqlParameter("@CasperTemplateKeyId",CasperTemplateKeyId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CasperTemplateKeyFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CasperTemplateKey> GetCasperTemplateKeyByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateKeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CasperTemplateKey] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplateKey>(ds,CasperTemplateKeyFromDataRow);
		}

		public virtual List<CasperTemplateKey> GetAllCasperTemplateKey(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateKeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperTemplateKey] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplateKey>(ds, CasperTemplateKeyFromDataRow);
		}

		public virtual List<CasperTemplateKey> GetPagedCasperTemplateKey(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCasperTemplateKeyCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CasperTemplateKeyId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CasperTemplateKeyId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CasperTemplateKeyId] ";
            tempsql += " FROM [CasperTemplateKey] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CasperTemplateKeyId"))
					tempsql += " , (AllRecords.[CasperTemplateKeyId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CasperTemplateKeyId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCasperTemplateKeySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CasperTemplateKey] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CasperTemplateKey].[CasperTemplateKeyId] = PageIndex.[CasperTemplateKeyId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplateKey>(ds, CasperTemplateKeyFromDataRow);
			}else{ return null;}
		}

		private int GetCasperTemplateKeyCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CasperTemplateKey as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CasperTemplateKey as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CasperTemplateKey))]
		public virtual CasperTemplateKey InsertCasperTemplateKey(CasperTemplateKey entity)
		{

			CasperTemplateKey other=new CasperTemplateKey();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CasperTemplateKey ( [FlashTemplateId]
				,[FlashKey]
				,[CreationDate]
				,[FlashTemplateKeyId] )
				Values
				( @FlashTemplateId
				, @FlashKey
				, @CreationDate
				, @FlashTemplateKeyId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FlashTemplateId",entity.FlashTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashKey",entity.FlashKey ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateKeyId",entity.FlashTemplateKeyId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCasperTemplateKey(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CasperTemplateKey))]
		public virtual CasperTemplateKey UpdateCasperTemplateKey(CasperTemplateKey entity)
		{

			if (entity.IsTransient()) return entity;
			CasperTemplateKey other = GetCasperTemplateKey(entity.CasperTemplateKeyId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CasperTemplateKey set  [FlashTemplateId]=@FlashTemplateId
							, [FlashKey]=@FlashKey
							, [CreationDate]=@CreationDate
							, [FlashTemplateKeyId]=@FlashTemplateKeyId 
							 where CasperTemplateKeyId=@CasperTemplateKeyId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FlashTemplateId",entity.FlashTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashKey",entity.FlashKey ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateKeyId",entity.FlashTemplateKeyId ?? (object)DBNull.Value)
					, new SqlParameter("@CasperTemplateKeyId",entity.CasperTemplateKeyId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCasperTemplateKey(entity.CasperTemplateKeyId);
		}

		public virtual bool DeleteCasperTemplateKey(System.Int32 CasperTemplateKeyId)
		{

			string sql="delete from CasperTemplateKey where CasperTemplateKeyId=@CasperTemplateKeyId";
			SqlParameter parameter=new SqlParameter("@CasperTemplateKeyId",CasperTemplateKeyId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CasperTemplateKey))]
		public virtual CasperTemplateKey DeleteCasperTemplateKey(CasperTemplateKey entity)
		{
			this.DeleteCasperTemplateKey(entity.CasperTemplateKeyId);
			return entity;
		}


		public virtual CasperTemplateKey CasperTemplateKeyFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CasperTemplateKey entity=new CasperTemplateKey();
			if (dr.Table.Columns.Contains("CasperTemplateKeyId"))
			{
			entity.CasperTemplateKeyId = (System.Int32)dr["CasperTemplateKeyId"];
			}
			if (dr.Table.Columns.Contains("FlashTemplateId"))
			{
			entity.FlashTemplateId = dr["FlashTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashTemplateId"];
			}
			if (dr.Table.Columns.Contains("FlashKey"))
			{
			entity.FlashKey = dr["FlashKey"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("FlashTemplateKeyId"))
			{
			entity.FlashTemplateKeyId = dr["FlashTemplateKeyId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashTemplateKeyId"];
			}
			return entity;
		}

	}
	
	
}
