﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class CasperTemplateKeyRepository: CasperTemplateKeyRepositoryBase, ICasperTemplateKeyRepository
	{

        public virtual CasperTemplateKey GetByFlashTemplateKeyId(System.Int32 FlashTemplateKeyId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateKeySelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplateKey] with (nolock)  where FlashTemplateKeyId=@FlashTemplateKeyId ";
            SqlParameter parameter = new SqlParameter("@FlashTemplateKeyId", FlashTemplateKeyId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateKeyFromDataRow(ds.Tables[0].Rows[0]);
        }
		
	}
	
	
}
