﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CasperTemplateRepositoryBase : Repository, ICasperTemplateRepositoryBase 
	{
        
        public CasperTemplateRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CasperTemlateId",new SearchColumn(){Name="CasperTemlateId",Title="CasperTemlateId",SelectClause="CasperTemlateId",WhereClause="AllRecords.CasperTemlateId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CasperTemlateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Template",new SearchColumn(){Name="Template",Title="Template",SelectClause="Template",WhereClause="AllRecords.Template",DataType="System.String",IsForeignColumn=false,PropertyName="Template",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ItemCount",new SearchColumn(){Name="ItemCount",Title="ItemCount",SelectClause="ItemCount",WhereClause="AllRecords.ItemCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ItemCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashTemplateId",new SearchColumn(){Name="FlashTemplateId",Title="FlashTemplateId",SelectClause="FlashTemplateId",WhereClause="AllRecords.FlashTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TemplateTypeId",new SearchColumn(){Name="TemplateTypeId",Title="TemplateTypeId",SelectClause="TemplateTypeId",WhereClause="AllRecords.TemplateTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TemplateTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Width",new SearchColumn(){Name="Width",Title="Width",SelectClause="Width",WhereClause="AllRecords.Width",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="Width",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Height",new SearchColumn(){Name="Height",Title="Height",SelectClause="Height",WhereClause="AllRecords.Height",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="Height",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCasperTemplateSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCasperTemplateBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCasperTemplateAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCasperTemplateSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CasperTemplate].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CasperTemplate].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual CasperTemplate GetCasperTemplate(System.Int32 CasperTemlateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperTemplate] with (nolock)  where CasperTemlateId=@CasperTemlateId ";
			SqlParameter parameter=new SqlParameter("@CasperTemlateId",CasperTemlateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CasperTemplateFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CasperTemplate> GetCasperTemplateByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CasperTemplate] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplate>(ds,CasperTemplateFromDataRow);
		}

		public virtual List<CasperTemplate> GetAllCasperTemplate(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperTemplate] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplate>(ds, CasperTemplateFromDataRow);
		}

		public virtual List<CasperTemplate> GetPagedCasperTemplate(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCasperTemplateCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CasperTemlateId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CasperTemlateId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CasperTemlateId] ";
            tempsql += " FROM [CasperTemplate] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CasperTemlateId"))
					tempsql += " , (AllRecords.[CasperTemlateId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CasperTemlateId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCasperTemplateSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CasperTemplate] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CasperTemplate].[CasperTemlateId] = PageIndex.[CasperTemlateId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplate>(ds, CasperTemplateFromDataRow);
			}else{ return null;}
		}

		private int GetCasperTemplateCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CasperTemplate as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CasperTemplate as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CasperTemplate))]
		public virtual CasperTemplate InsertCasperTemplate(CasperTemplate entity)
		{

			CasperTemplate other=new CasperTemplate();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CasperTemplate ( [Template]
				,[ItemCount]
				,[FlashTemplateId]
				,[TemplateTypeId]
				,[Width]
				,[Height]
				,[ResourceGuid] )
				Values
				( @Template
				, @ItemCount
				, @FlashTemplateId
				, @TemplateTypeId
				, @Width
				, @Height
				, @ResourceGuid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Template",entity.Template)
					, new SqlParameter("@ItemCount",entity.ItemCount ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateId",entity.FlashTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@TemplateTypeId",entity.TemplateTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Width",entity.Width ?? (object)DBNull.Value)
					, new SqlParameter("@Height",entity.Height ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCasperTemplate(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CasperTemplate))]
		public virtual CasperTemplate UpdateCasperTemplate(CasperTemplate entity)
		{

			if (entity.IsTransient()) return entity;
			CasperTemplate other = GetCasperTemplate(entity.CasperTemlateId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CasperTemplate set  [Template]=@Template
							, [ItemCount]=@ItemCount
							, [FlashTemplateId]=@FlashTemplateId
							, [TemplateTypeId]=@TemplateTypeId
							, [Width]=@Width
							, [Height]=@Height
							, [ResourceGuid]=@ResourceGuid 
							 where CasperTemlateId=@CasperTemlateId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Template",entity.Template)
					, new SqlParameter("@ItemCount",entity.ItemCount ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateId",entity.FlashTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@TemplateTypeId",entity.TemplateTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Width",entity.Width ?? (object)DBNull.Value)
					, new SqlParameter("@Height",entity.Height ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@CasperTemlateId",entity.CasperTemlateId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCasperTemplate(entity.CasperTemlateId);
		}

		public virtual bool DeleteCasperTemplate(System.Int32 CasperTemlateId)
		{

			string sql="delete from CasperTemplate where CasperTemlateId=@CasperTemlateId";
			SqlParameter parameter=new SqlParameter("@CasperTemlateId",CasperTemlateId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CasperTemplate))]
		public virtual CasperTemplate DeleteCasperTemplate(CasperTemplate entity)
		{
			this.DeleteCasperTemplate(entity.CasperTemlateId);
			return entity;
		}


		public virtual CasperTemplate CasperTemplateFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CasperTemplate entity=new CasperTemplate();
			if (dr.Table.Columns.Contains("CasperTemlateId"))
			{
			entity.CasperTemlateId = (System.Int32)dr["CasperTemlateId"];
			}
			if (dr.Table.Columns.Contains("Template"))
			{
			entity.Template = dr["Template"].ToString();
			}
			if (dr.Table.Columns.Contains("ItemCount"))
			{
			entity.ItemCount = dr["ItemCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ItemCount"];
			}
			if (dr.Table.Columns.Contains("FlashTemplateId"))
			{
			entity.FlashTemplateId = dr["FlashTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashTemplateId"];
			}
			if (dr.Table.Columns.Contains("TemplateTypeId"))
			{
			entity.TemplateTypeId = dr["TemplateTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TemplateTypeId"];
			}
			if (dr.Table.Columns.Contains("Width"))
			{
			entity.Width = dr["Width"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["Width"];
			}
			if (dr.Table.Columns.Contains("Height"))
			{
			entity.Height = dr["Height"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["Height"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = dr["ResourceGuid"].ToString();
			}
			return entity;
		}

	}
	
	
}
