﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public partial class CasperTemplateRepository: CasperTemplateRepositoryBase, ICasperTemplateRepository
	{
        public virtual CasperTemplate GetCasperTemplateByTemplateName(string Template, string SelectClause = null)
        {
            if (Template.ToLower().Contains("wall"))
            {
                string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateSelectClause() : (string.Format("Select {0} ", SelectClause));
                sql += "from [CasperTemplate] with (nolock)  where Template=@Template ";
                SqlParameter parameter = new SqlParameter("@Template", Template);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
                return CasperTemplateFromDataRow(ds.Tables[0].Rows[0]);
            }
            else
            {
                string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateSelectClause() : (string.Format("Select {0} ", SelectClause));
                sql += "from [CasperTemplate] with (nolock)  where SUBSTRING(Template,10,100)=@Template ";
                SqlParameter parameter = new SqlParameter("@Template", Template);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
                return CasperTemplateFromDataRow(ds.Tables[0].Rows[0]);
            }
        }

        public virtual CasperTemplate GetBYFlashTemplateId(int FlashTemplateId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplate] with (nolock)  where FlashTemplateId=@FlashTemplateId ";
            SqlParameter parameter = new SqlParameter("@FlashTemplateId", FlashTemplateId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateFromDataRow(ds.Tables[0].Rows[0]);
        }


	}
	
	
}
