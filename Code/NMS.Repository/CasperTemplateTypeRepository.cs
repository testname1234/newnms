﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class CasperTemplateTypeRepository: CasperTemplateTypeRepositoryBase, ICasperTemplateTypeRepository
	{
        public CasperTemplateType GetByName(System.String Name, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetCasperTemplateTypeSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CasperTemplateType] with (nolock)  where Name=@Name ";
            SqlParameter parameter = new SqlParameter("@Name", Name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CasperTemplateTypeFromDataRow(ds.Tables[0].Rows[0]);
        }
	}
	
	
}
