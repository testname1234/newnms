﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CasperTemplateWindowTypeRepositoryBase : Repository, ICasperTemplateWindowTypeRepositoryBase 
	{
        
        public CasperTemplateWindowTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CasperTemplateWindowTypeId",new SearchColumn(){Name="CasperTemplateWindowTypeId",Title="CasperTemplateWindowTypeId",SelectClause="CasperTemplateWindowTypeId",WhereClause="AllRecords.CasperTemplateWindowTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CasperTemplateWindowTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashWindowTypeId",new SearchColumn(){Name="FlashWindowTypeId",Title="FlashWindowTypeId",SelectClause="FlashWindowTypeId",WhereClause="AllRecords.FlashWindowTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashWindowTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashWindowType",new SearchColumn(){Name="FlashWindowType",Title="FlashWindowType",SelectClause="FlashWindowType",WhereClause="AllRecords.FlashWindowType",DataType="System.String",IsForeignColumn=false,PropertyName="FlashWindowType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCasperTemplateWindowTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCasperTemplateWindowTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCasperTemplateWindowTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCasperTemplateWindowTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CasperTemplateWindowType].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CasperTemplateWindowType].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual CasperTemplateWindowType GetCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateWindowTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperTemplateWindowType] with (nolock)  where CasperTemplateWindowTypeId=@CasperTemplateWindowTypeId ";
			SqlParameter parameter=new SqlParameter("@CasperTemplateWindowTypeId",CasperTemplateWindowTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CasperTemplateWindowTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CasperTemplateWindowType> GetCasperTemplateWindowTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateWindowTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CasperTemplateWindowType] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplateWindowType>(ds,CasperTemplateWindowTypeFromDataRow);
		}

		public virtual List<CasperTemplateWindowType> GetAllCasperTemplateWindowType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCasperTemplateWindowTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CasperTemplateWindowType] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplateWindowType>(ds, CasperTemplateWindowTypeFromDataRow);
		}

		public virtual List<CasperTemplateWindowType> GetPagedCasperTemplateWindowType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCasperTemplateWindowTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CasperTemplateWindowTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CasperTemplateWindowTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CasperTemplateWindowTypeId] ";
            tempsql += " FROM [CasperTemplateWindowType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CasperTemplateWindowTypeId"))
					tempsql += " , (AllRecords.[CasperTemplateWindowTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CasperTemplateWindowTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCasperTemplateWindowTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CasperTemplateWindowType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CasperTemplateWindowType].[CasperTemplateWindowTypeId] = PageIndex.[CasperTemplateWindowTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CasperTemplateWindowType>(ds, CasperTemplateWindowTypeFromDataRow);
			}else{ return null;}
		}

		private int GetCasperTemplateWindowTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CasperTemplateWindowType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CasperTemplateWindowType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CasperTemplateWindowType))]
		public virtual CasperTemplateWindowType InsertCasperTemplateWindowType(CasperTemplateWindowType entity)
		{

			CasperTemplateWindowType other=new CasperTemplateWindowType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CasperTemplateWindowType ( [FlashWindowTypeId]
				,[FlashWindowType]
				,[CreationDate] )
				Values
				( @FlashWindowTypeId
				, @FlashWindowType
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FlashWindowTypeId",entity.FlashWindowTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashWindowType",entity.FlashWindowType ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCasperTemplateWindowType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CasperTemplateWindowType))]
		public virtual CasperTemplateWindowType UpdateCasperTemplateWindowType(CasperTemplateWindowType entity)
		{

			if (entity.IsTransient()) return entity;
			CasperTemplateWindowType other = GetCasperTemplateWindowType(entity.CasperTemplateWindowTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CasperTemplateWindowType set  [FlashWindowTypeId]=@FlashWindowTypeId
							, [FlashWindowType]=@FlashWindowType
							, [CreationDate]=@CreationDate 
							 where CasperTemplateWindowTypeId=@CasperTemplateWindowTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FlashWindowTypeId",entity.FlashWindowTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashWindowType",entity.FlashWindowType ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@CasperTemplateWindowTypeId",entity.CasperTemplateWindowTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCasperTemplateWindowType(entity.CasperTemplateWindowTypeId);
		}

		public virtual bool DeleteCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId)
		{

			string sql="delete from CasperTemplateWindowType where CasperTemplateWindowTypeId=@CasperTemplateWindowTypeId";
			SqlParameter parameter=new SqlParameter("@CasperTemplateWindowTypeId",CasperTemplateWindowTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CasperTemplateWindowType))]
		public virtual CasperTemplateWindowType DeleteCasperTemplateWindowType(CasperTemplateWindowType entity)
		{
			this.DeleteCasperTemplateWindowType(entity.CasperTemplateWindowTypeId);
			return entity;
		}


		public virtual CasperTemplateWindowType CasperTemplateWindowTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CasperTemplateWindowType entity=new CasperTemplateWindowType();
			if (dr.Table.Columns.Contains("CasperTemplateWindowTypeId"))
			{
			entity.CasperTemplateWindowTypeId = (System.Int32)dr["CasperTemplateWindowTypeId"];
			}
			if (dr.Table.Columns.Contains("FlashWindowTypeId"))
			{
			entity.FlashWindowTypeId = dr["FlashWindowTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashWindowTypeId"];
			}
			if (dr.Table.Columns.Contains("FlashWindowType"))
			{
			entity.FlashWindowType = dr["FlashWindowType"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
