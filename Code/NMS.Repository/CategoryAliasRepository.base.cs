﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CategoryAliasRepositoryBase : Repository, ICategoryAliasRepositoryBase 
	{
        
        public CategoryAliasRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CategoryAliasId",new SearchColumn(){Name="CategoryAliasId",Title="CategoryAliasId",SelectClause="CategoryAliasId",WhereClause="AllRecords.CategoryAliasId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CategoryAliasId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Alias",new SearchColumn(){Name="Alias",Title="Alias",SelectClause="Alias",WhereClause="AllRecords.Alias",DataType="System.String",IsForeignColumn=false,PropertyName="Alias",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCategoryAliasSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCategoryAliasBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCategoryAliasAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCategoryAliasSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "CategoryAlias."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",CategoryAlias."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<CategoryAlias> GetCategoryAliasByCategoryId(System.Int32 CategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCategoryAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from CategoryAlias with (nolock)  where CategoryId=@CategoryId  ";
			SqlParameter parameter=new SqlParameter("@CategoryId",CategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CategoryAlias>(ds,CategoryAliasFromDataRow);
		}

		public virtual CategoryAlias GetCategoryAlias(System.Int32 CategoryAliasId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCategoryAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from CategoryAlias with (nolock)  where CategoryAliasId=@CategoryAliasId ";
			SqlParameter parameter=new SqlParameter("@CategoryAliasId",CategoryAliasId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CategoryAliasFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CategoryAlias> GetCategoryAliasByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCategoryAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from CategoryAlias with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CategoryAlias>(ds,CategoryAliasFromDataRow);
		}

		public virtual List<CategoryAlias> GetAllCategoryAlias(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCategoryAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from CategoryAlias with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CategoryAlias>(ds, CategoryAliasFromDataRow);
		}

		public virtual List<CategoryAlias> GetPagedCategoryAlias(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCategoryAliasCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CategoryAliasId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CategoryAliasId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CategoryAliasId] ";
            tempsql += " FROM [CategoryAlias] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CategoryAliasId"))
					tempsql += " , (AllRecords.[CategoryAliasId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CategoryAliasId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCategoryAliasSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CategoryAlias] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CategoryAlias].[CategoryAliasId] = PageIndex.[CategoryAliasId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CategoryAlias>(ds, CategoryAliasFromDataRow);
			}else{ return null;}
		}

		private int GetCategoryAliasCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CategoryAlias as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CategoryAlias as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CategoryAlias))]
		public virtual CategoryAlias InsertCategoryAlias(CategoryAlias entity)
		{

			CategoryAlias other=new CategoryAlias();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CategoryAlias ( [CategoryId]
				,[Alias]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @CategoryId
				, @Alias
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@Alias",entity.Alias)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCategoryAlias(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CategoryAlias))]
		public virtual CategoryAlias UpdateCategoryAlias(CategoryAlias entity)
		{

			if (entity.IsTransient()) return entity;
			CategoryAlias other = GetCategoryAlias(entity.CategoryAliasId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CategoryAlias set  [CategoryId]=@CategoryId
							, [Alias]=@Alias
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where CategoryAliasId=@CategoryAliasId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@Alias",entity.Alias)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CategoryAliasId",entity.CategoryAliasId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCategoryAlias(entity.CategoryAliasId);
		}

		public virtual bool DeleteCategoryAlias(System.Int32 CategoryAliasId)
		{

			string sql="delete from CategoryAlias with (nolock) where CategoryAliasId=@CategoryAliasId";
			SqlParameter parameter=new SqlParameter("@CategoryAliasId",CategoryAliasId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CategoryAlias))]
		public virtual CategoryAlias DeleteCategoryAlias(CategoryAlias entity)
		{
			this.DeleteCategoryAlias(entity.CategoryAliasId);
			return entity;
		}


		public virtual CategoryAlias CategoryAliasFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CategoryAlias entity=new CategoryAlias();
			if (dr.Table.Columns.Contains("CategoryAliasId"))
			{
			entity.CategoryAliasId = (System.Int32)dr["CategoryAliasId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = (System.Int32)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("Alias"))
			{
			entity.Alias = dr["Alias"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
