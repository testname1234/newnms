﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class CategoryAliasRepository: CategoryAliasRepositoryBase, ICategoryAliasRepository
	{

        public CategoryAlias GetByAlias(string alias)
        {
            string sql = GetCategoryAliasSelectClause() + " from CategoryAlias with (nolock)  where Alias = @alias";
            SqlParameter parameter = new SqlParameter("@alias", alias);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CategoryAliasFromDataRow(ds.Tables[0].Rows[0]);
        }

        public bool Exist(CategoryAlias entity)
        {
            string sql = "select count(1) from CategoryAlias with (nolock)  where Alias = @Name";
            SqlParameter parameter = new SqlParameter("@Name", entity.Alias);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter })) > 0;
        }
    }
	
	
}
