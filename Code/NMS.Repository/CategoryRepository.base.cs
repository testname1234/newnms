﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CategoryRepositoryBase : Repository, ICategoryRepositoryBase 
	{
        
        public CategoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Category",new SearchColumn(){Name="Category",Title="Category",SelectClause="Category",WhereClause="AllRecords.Category",DataType="System.String",IsForeignColumn=false,PropertyName="Category",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentId",new SearchColumn(){Name="ParentId",Title="ParentId",SelectClause="ParentId",WhereClause="AllRecords.ParentId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsApproved",new SearchColumn(){Name="IsApproved",Title="IsApproved",SelectClause="IsApproved",WhereClause="AllRecords.IsApproved",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsApproved",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryInUrdu",new SearchColumn(){Name="CategoryInUrdu",Title="CategoryInUrdu",SelectClause="CategoryInUrdu",WhereClause="AllRecords.CategoryInUrdu",DataType="System.String",IsForeignColumn=false,PropertyName="CategoryInUrdu",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCategorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCategoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCategoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCategorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Category].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Category].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Category GetCategory(System.Int32 CategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Category] with (nolock)  where CategoryId=@CategoryId ";
			SqlParameter parameter=new SqlParameter("@CategoryId",CategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CategoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Category> GetCategoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Category] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Category>(ds,CategoryFromDataRow);
		}

		public virtual List<Category> GetAllCategory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Category] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Category>(ds, CategoryFromDataRow);
		}

		public virtual List<Category> GetPagedCategory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCategoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CategoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CategoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CategoryId] ";
            tempsql += " FROM [Category] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CategoryId"))
					tempsql += " , (AllRecords.[CategoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CategoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCategorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Category] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Category].[CategoryId] = PageIndex.[CategoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Category>(ds, CategoryFromDataRow);
			}else{ return null;}
		}

		private int GetCategoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Category as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Category as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Category))]
		public virtual Category InsertCategory(Category entity)
		{

			Category other=new Category();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Category ( [Category]
				,[ParentId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsApproved]
				,[CategoryInUrdu] )
				Values
				( @Category
				, @ParentId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsApproved
				, @CategoryInUrdu );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Category",entity.Category)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryInUrdu",entity.CategoryInUrdu ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCategory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Category))]
		public virtual Category UpdateCategory(Category entity)
		{

			if (entity.IsTransient()) return entity;
			Category other = GetCategory(entity.CategoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Category set  [Category]=@Category
							, [ParentId]=@ParentId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsApproved]=@IsApproved
							, [CategoryInUrdu]=@CategoryInUrdu 
							 where CategoryId=@CategoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Category",entity.Category)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryInUrdu",entity.CategoryInUrdu ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCategory(entity.CategoryId);
		}

		public virtual bool DeleteCategory(System.Int32 CategoryId)
		{

			string sql="delete from Category where CategoryId=@CategoryId";
			SqlParameter parameter=new SqlParameter("@CategoryId",CategoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Category))]
		public virtual Category DeleteCategory(Category entity)
		{
			this.DeleteCategory(entity.CategoryId);
			return entity;
		}


		public virtual Category CategoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Category entity=new Category();
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = (System.Int32)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("Category"))
			{
			entity.Category = dr["Category"].ToString();
			}
			if (dr.Table.Columns.Contains("ParentId"))
			{
			entity.ParentId = dr["ParentId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("IsApproved"))
			{
			entity.IsApproved = dr["IsApproved"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsApproved"];
			}
			if (dr.Table.Columns.Contains("CategoryInUrdu"))
			{
			entity.CategoryInUrdu = dr["CategoryInUrdu"].ToString();
			}
			return entity;
		}

	}
	
	
}
