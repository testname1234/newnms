﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class CategoryRepository: CategoryRepositoryBase, ICategoryRepository
	{
        ICategoryAliasRepository _iCategoryAliasRepository;
        public CategoryRepository(ICategoryAliasRepository iCategoryAliasRepository)
        {
            _iCategoryAliasRepository = iCategoryAliasRepository;
        }

        public Category GetCategoryCreateIfNotExist(Category category)
        {
            CategoryAlias alias = GetByAlias(category.Category);
            if (alias == null)
            {
                Category cat = InsertCategory(category);
                CategoryAlias _alias = new CategoryAlias();
                _alias.CategoryId = cat.CategoryId;
                _alias.Alias = cat.Category;
                _alias.CreationDate = cat.CreationDate;
                _alias.LastUpdateDate = _alias.CreationDate;
                _alias.IsActive = true;
                _iCategoryAliasRepository.InsertCategoryAlias(_alias);
                return cat;
            }
            else
                return GetCategory(alias.CategoryId);
        }

        public Category InsertCategoryIfNotExists(Category category)
        {
            // check if the name is exists in category or categoryalias exists don't add else add category

            bool isAliasExits = _iCategoryAliasRepository.Exist(new CategoryAlias { Alias=category.Category });

            if (!isAliasExits && !Exist(category))
                 return InsertCategory(category);
             else
                 return GetCategoryByName(category.Category);
        }

        public bool Exist(Category category)
        {
            string sql = "select count(1) from Category with (nolock)  where Category = @Name";
            SqlParameter parameter = new SqlParameter("@Name", category.Category);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter })) > 0;
        }


        public CategoryAlias GetByAlias(string alias)
        {
            return _iCategoryAliasRepository.GetByAlias(alias);
        }

        public Category GetCategoryByName(string name)
        {
            string sql = GetCategorySelectClause();
            sql += "from Category with (nolock)  where Category=@Name ";
            SqlParameter parameter = new SqlParameter("@Name", name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CategoryFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Category> GetCategoryByTerm(string term)
        {
            string sql = "select top 10 * from Category with (nolock)  where Category like @Name+'%' and IsApproved=1";
            SqlParameter parameter = new SqlParameter("@Name", term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Category>(ds, CategoryFromDataRow);
        }

        public List<Category> GetByDate(DateTime LastUpdateDate)
        {
            string sql = "select * from Category with (nolock)  where LastUpdateDate >= @LastUpdateDate and IsApproved=1";
            SqlParameter parameter = new SqlParameter("@LastUpdateDate", LastUpdateDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Category>(ds, CategoryFromDataRow);
        }
	}
	
	
}
