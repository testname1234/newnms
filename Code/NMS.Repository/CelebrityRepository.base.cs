﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CelebrityRepositoryBase : Repository, ICelebrityRepositoryBase 
	{
        
        public CelebrityRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImageGuid",new SearchColumn(){Name="ImageGuid",Title="ImageGuid",SelectClause="ImageGuid",WhereClause="AllRecords.ImageGuid",DataType="System.Guid?",IsForeignColumn=false,PropertyName="ImageGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Designation",new SearchColumn(){Name="Designation",Title="Designation",SelectClause="Designation",WhereClause="AllRecords.Designation",DataType="System.String",IsForeignColumn=false,PropertyName="Designation",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("WordFrequency",new SearchColumn(){Name="WordFrequency",Title="WordFrequency",SelectClause="WordFrequency",WhereClause="AllRecords.WordFrequency",DataType="System.Int32?",IsForeignColumn=false,PropertyName="WordFrequency",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AddedToRundown",new SearchColumn(){Name="AddedToRundown",Title="AddedToRundown",SelectClause="AddedToRundown",WhereClause="AllRecords.AddedToRundown",DataType="System.Int32?",IsForeignColumn=false,PropertyName="AddedToRundown",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OnAired",new SearchColumn(){Name="OnAired",Title="OnAired",SelectClause="OnAired",WhereClause="AllRecords.OnAired",DataType="System.Int32?",IsForeignColumn=false,PropertyName="OnAired",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ExecutedOnOtherChannels",new SearchColumn(){Name="ExecutedOnOtherChannels",Title="ExecutedOnOtherChannels",SelectClause="ExecutedOnOtherChannels",WhereClause="AllRecords.ExecutedOnOtherChannels",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ExecutedOnOtherChannels",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCelebritySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCelebrityBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCelebrityAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCelebritySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Celebrity].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Celebrity].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Celebrity> GetCelebrityByCategoryId(System.Int32 CategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Celebrity] with (nolock)  where CategoryId=@CategoryId  ";
			SqlParameter parameter=new SqlParameter("@CategoryId",CategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Celebrity>(ds,CelebrityFromDataRow);
		}

		public virtual Celebrity GetCelebrity(System.Int32 CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Celebrity] with (nolock)  where CelebrityId=@CelebrityId ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CelebrityFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Celebrity> GetCelebrityByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Celebrity] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Celebrity>(ds,CelebrityFromDataRow);
		}

		public virtual List<Celebrity> GetAllCelebrity(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Celebrity] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Celebrity>(ds, CelebrityFromDataRow);
		}

		public virtual List<Celebrity> GetPagedCelebrity(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCelebrityCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CelebrityId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CelebrityId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CelebrityId] ";
            tempsql += " FROM [Celebrity] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CelebrityId"))
					tempsql += " , (AllRecords.[CelebrityId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CelebrityId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCelebritySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Celebrity] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Celebrity].[CelebrityId] = PageIndex.[CelebrityId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Celebrity>(ds, CelebrityFromDataRow);
			}else{ return null;}
		}

		private int GetCelebrityCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Celebrity as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Celebrity as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Celebrity))]
		public virtual Celebrity InsertCelebrity(Celebrity entity)
		{

			Celebrity other=new Celebrity();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Celebrity ( [Name]
				,[ImageGuid]
				,[Designation]
				,[LocationId]
				,[CategoryId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[WordFrequency]
				,[AddedToRundown]
				,[OnAired]
				,[ExecutedOnOtherChannels] )
				Values
				( @Name
				, @ImageGuid
				, @Designation
				, @LocationId
				, @CategoryId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @WordFrequency
				, @AddedToRundown
				, @OnAired
				, @ExecutedOnOtherChannels );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ImageGuid",entity.ImageGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Designation",entity.Designation ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@WordFrequency",entity.WordFrequency ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundown",entity.AddedToRundown ?? (object)DBNull.Value)
					, new SqlParameter("@OnAired",entity.OnAired ?? (object)DBNull.Value)
					, new SqlParameter("@ExecutedOnOtherChannels",entity.ExecutedOnOtherChannels ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCelebrity(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Celebrity))]
		public virtual Celebrity UpdateCelebrity(Celebrity entity)
		{

			if (entity.IsTransient()) return entity;
			Celebrity other = GetCelebrity(entity.CelebrityId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Celebrity set  [Name]=@Name
							, [ImageGuid]=@ImageGuid
							, [Designation]=@Designation
							, [LocationId]=@LocationId
							, [CategoryId]=@CategoryId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [WordFrequency]=@WordFrequency
							, [AddedToRundown]=@AddedToRundown
							, [OnAired]=@OnAired
							, [ExecutedOnOtherChannels]=@ExecutedOnOtherChannels 
							 where CelebrityId=@CelebrityId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ImageGuid",entity.ImageGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Designation",entity.Designation ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@WordFrequency",entity.WordFrequency ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundown",entity.AddedToRundown ?? (object)DBNull.Value)
					, new SqlParameter("@OnAired",entity.OnAired ?? (object)DBNull.Value)
					, new SqlParameter("@ExecutedOnOtherChannels",entity.ExecutedOnOtherChannels ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityId",entity.CelebrityId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCelebrity(entity.CelebrityId);
		}

		public virtual bool DeleteCelebrity(System.Int32 CelebrityId)
		{

			string sql="delete from Celebrity where CelebrityId=@CelebrityId";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Celebrity))]
		public virtual Celebrity DeleteCelebrity(Celebrity entity)
		{
			this.DeleteCelebrity(entity.CelebrityId);
			return entity;
		}


		public virtual Celebrity CelebrityFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Celebrity entity=new Celebrity();
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = (System.Int32)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("ImageGuid"))
			{
			entity.ImageGuid = dr["ImageGuid"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["ImageGuid"];
			}
			if (dr.Table.Columns.Contains("Designation"))
			{
			entity.Designation = dr["Designation"].ToString();
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = (System.Int32)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("WordFrequency"))
			{
			entity.WordFrequency = dr["WordFrequency"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["WordFrequency"];
			}
			if (dr.Table.Columns.Contains("AddedToRundown"))
			{
			entity.AddedToRundown = dr["AddedToRundown"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["AddedToRundown"];
			}
			if (dr.Table.Columns.Contains("OnAired"))
			{
			entity.OnAired = dr["OnAired"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["OnAired"];
			}
			if (dr.Table.Columns.Contains("ExecutedOnOtherChannels"))
			{
			entity.ExecutedOnOtherChannels = dr["ExecutedOnOtherChannels"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ExecutedOnOtherChannels"];
			}
			return entity;
		}

	}
	
	
}
