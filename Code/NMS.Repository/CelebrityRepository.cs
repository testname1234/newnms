﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core;

namespace NMS.Repository
{
    public partial class CelebrityRepository : CelebrityRepositoryBase, ICelebrityRepository
    {
        public Celebrity GetBySocialMediaAccountUserName(string userName, int socialMediaType)
        {
            string sql = @"select c.*,cat.Category,l.Location from Socialmediaaccount t inner join Celebrity c on t.CelebrityId=c.CelebrityId
                           inner join Category cat on cat.CategoryId=c.CategoryId
                           inner join Location l on l.LocationId=c.LocationId
                           where t.UserName=@UserName and t.SocialMediaType= @SocialMediaType";
            SqlParameter parameter = new SqlParameter("@UserName", userName);
            SqlParameter parameter1 = new SqlParameter("@SocialMediaType", socialMediaType);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CelebrityFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Celebrity GetBySocialMediaAccountUserNameNoCeleb(string userName)
        {
            string sql = @"select sm.UserName as 'Name',c.Category,l.Location from twitterAccount sm inner join  Category c on c.CategoryId = sm.categoryid inner join Location l on l.LocationId = sm.locationid where sm.UserName = @UserName";
            SqlParameter parameter = new SqlParameter("@UserName", userName);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter});
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CelebrityFromDataRow(ds.Tables[0].Rows[0]);
        }
        

        public override Celebrity CelebrityFromDataRow(DataRow dr)
        {
            Celebrity celeb = base.CelebrityFromDataRow(dr);
            if (dr.Table.Columns.Contains("Category"))
                celeb.Category = dr["Category"].ToString();
            if (dr.Table.Columns.Contains("Location"))
                celeb.Location = dr["Location"].ToString();
            return celeb;
        }

        public List<Celebrity> GetCelebrityByTerm(string term)
        {
            string sql = GetCelebritySelectClause();
            sql += "from Celebrity with (nolock)  where Name like @Name+'%' and ISNULL(IsActive,0) = 1";
            SqlParameter parameter = new SqlParameter("@Name", term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Celebrity>(ds, CelebrityFromDataRow);
        }

        [MOLog(AuditOperations.Update, typeof(Celebrity))]
        public Celebrity UpdateCelebrityStatistics(Celebrity entity)
        {
            string sql = @"Update [Celebrity] set 
                            AddedToRundown = @AddedToRundown, 
                            OnAired = @OnAired, 
                            ExecutedOnOtherChannels = @ExecutedOnOtherChannels 
                        where CelebrityId = @CelebrityId";

            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@CelebrityId", entity.CelebrityId),
                new SqlParameter("@AddedToRundown", entity.AddedToRundown ?? 0),
                new SqlParameter("@OnAired", entity.OnAired ?? 0),
                new SqlParameter("@ExecutedOnOtherChannels", entity.ExecutedOnOtherChannels ?? 0)
            };

            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetCelebrity(entity.CelebrityId);
        }

        public List<Celebrity> GetCelebrityByIds(List<int> celebrityIds)
        {
            if (celebrityIds.Count > 0)
            {
                string sql = GetCelebritySelectClause();
                sql += "from Celebrity with (nolock)  where celebrityid in (" + string.Join(",", celebrityIds) + ")";
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { });
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<Celebrity>();
                return CollectionFromDataSet<Celebrity>(ds, CelebrityFromDataRow);
            }
            else
            {
                return new List<Celebrity>();
            }
        }
    }
}
