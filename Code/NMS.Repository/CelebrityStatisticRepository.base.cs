﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CelebrityStatisticRepositoryBase : Repository, ICelebrityStatisticRepositoryBase 
	{
        
        public CelebrityStatisticRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CelebrityStatisticId",new SearchColumn(){Name="CelebrityStatisticId",Title="CelebrityStatisticId",SelectClause="CelebrityStatisticId",WhereClause="AllRecords.CelebrityStatisticId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CelebrityStatisticId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramName",new SearchColumn(){Name="ProgramName",Title="ProgramName",SelectClause="ProgramName",WhereClause="AllRecords.ProgramName",DataType="System.String",IsForeignColumn=false,PropertyName="ProgramName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelName",new SearchColumn(){Name="ChannelName",Title="ChannelName",SelectClause="ChannelName",WhereClause="AllRecords.ChannelName",DataType="System.String",IsForeignColumn=false,PropertyName="ChannelName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EpisodeId",new SearchColumn(){Name="EpisodeId",Title="EpisodeId",SelectClause="EpisodeId",WhereClause="AllRecords.EpisodeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="EpisodeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EpisodeTime",new SearchColumn(){Name="EpisodeTime",Title="EpisodeTime",SelectClause="EpisodeTime",WhereClause="AllRecords.EpisodeTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="EpisodeTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatisticType",new SearchColumn(){Name="StatisticType",Title="StatisticType",SelectClause="StatisticType",WhereClause="AllRecords.StatisticType",DataType="System.Int32?",IsForeignColumn=false,PropertyName="StatisticType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCelebrityStatisticSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCelebrityStatisticBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCelebrityStatisticAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCelebrityStatisticSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CelebrityStatistic].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CelebrityStatistic].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<CelebrityStatistic> GetCelebrityStatisticByCelebrityId(System.Int32? CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityStatisticSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CelebrityStatistic] with (nolock)  where CelebrityId=@CelebrityId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityStatistic>(ds,CelebrityStatisticFromDataRow);
		}

		public virtual CelebrityStatistic GetCelebrityStatistic(System.Int32 CelebrityStatisticId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityStatisticSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CelebrityStatistic] with (nolock)  where CelebrityStatisticId=@CelebrityStatisticId ";
			SqlParameter parameter=new SqlParameter("@CelebrityStatisticId",CelebrityStatisticId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CelebrityStatisticFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CelebrityStatistic> GetCelebrityStatisticByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityStatisticSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CelebrityStatistic] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityStatistic>(ds,CelebrityStatisticFromDataRow);
		}

		public virtual List<CelebrityStatistic> GetAllCelebrityStatistic(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityStatisticSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CelebrityStatistic] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityStatistic>(ds, CelebrityStatisticFromDataRow);
		}

		public virtual List<CelebrityStatistic> GetPagedCelebrityStatistic(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCelebrityStatisticCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CelebrityStatisticId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CelebrityStatisticId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CelebrityStatisticId] ";
            tempsql += " FROM [CelebrityStatistic] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CelebrityStatisticId"))
					tempsql += " , (AllRecords.[CelebrityStatisticId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CelebrityStatisticId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCelebrityStatisticSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CelebrityStatistic] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CelebrityStatistic].[CelebrityStatisticId] = PageIndex.[CelebrityStatisticId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityStatistic>(ds, CelebrityStatisticFromDataRow);
			}else{ return null;}
		}

		private int GetCelebrityStatisticCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CelebrityStatistic as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CelebrityStatistic as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CelebrityStatistic))]
		public virtual CelebrityStatistic InsertCelebrityStatistic(CelebrityStatistic entity)
		{

			CelebrityStatistic other=new CelebrityStatistic();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CelebrityStatistic ( [ProgramId]
				,[ProgramName]
				,[ChannelId]
				,[ChannelName]
				,[EpisodeId]
				,[EpisodeTime]
				,[StatisticType]
				,[CelebrityId]
				,[CreationDate]
				,[LastUpdatedDate]
				,[IsActive] )
				Values
				( @ProgramId
				, @ProgramName
				, @ChannelId
				, @ChannelName
				, @EpisodeId
				, @EpisodeTime
				, @StatisticType
				, @CelebrityId
				, @CreationDate
				, @LastUpdatedDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramName",entity.ProgramName ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelName",entity.ChannelName ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeTime",entity.EpisodeTime ?? (object)DBNull.Value)
					, new SqlParameter("@StatisticType",entity.StatisticType ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCelebrityStatistic(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CelebrityStatistic))]
		public virtual CelebrityStatistic UpdateCelebrityStatistic(CelebrityStatistic entity)
		{

			if (entity.IsTransient()) return entity;
			CelebrityStatistic other = GetCelebrityStatistic(entity.CelebrityStatisticId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CelebrityStatistic set  [ProgramId]=@ProgramId
							, [ProgramName]=@ProgramName
							, [ChannelId]=@ChannelId
							, [ChannelName]=@ChannelName
							, [EpisodeId]=@EpisodeId
							, [EpisodeTime]=@EpisodeTime
							, [StatisticType]=@StatisticType
							, [CelebrityId]=@CelebrityId
							, [CreationDate]=@CreationDate
							, [LastUpdatedDate]=@LastUpdatedDate
							, [IsActive]=@IsActive 
							 where CelebrityStatisticId=@CelebrityStatisticId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramName",entity.ProgramName ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelName",entity.ChannelName ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeTime",entity.EpisodeTime ?? (object)DBNull.Value)
					, new SqlParameter("@StatisticType",entity.StatisticType ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityStatisticId",entity.CelebrityStatisticId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCelebrityStatistic(entity.CelebrityStatisticId);
		}

		public virtual bool DeleteCelebrityStatistic(System.Int32 CelebrityStatisticId)
		{

			string sql="delete from CelebrityStatistic where CelebrityStatisticId=@CelebrityStatisticId";
			SqlParameter parameter=new SqlParameter("@CelebrityStatisticId",CelebrityStatisticId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CelebrityStatistic))]
		public virtual CelebrityStatistic DeleteCelebrityStatistic(CelebrityStatistic entity)
		{
			this.DeleteCelebrityStatistic(entity.CelebrityStatisticId);
			return entity;
		}


		public virtual CelebrityStatistic CelebrityStatisticFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CelebrityStatistic entity=new CelebrityStatistic();
			if (dr.Table.Columns.Contains("CelebrityStatisticId"))
			{
			entity.CelebrityStatisticId = (System.Int32)dr["CelebrityStatisticId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("ProgramName"))
			{
			entity.ProgramName = dr["ProgramName"].ToString();
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = dr["ChannelId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("ChannelName"))
			{
			entity.ChannelName = dr["ChannelName"].ToString();
			}
			if (dr.Table.Columns.Contains("EpisodeId"))
			{
			entity.EpisodeId = dr["EpisodeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["EpisodeId"];
			}
			if (dr.Table.Columns.Contains("EpisodeTime"))
			{
			entity.EpisodeTime = dr["EpisodeTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["EpisodeTime"];
			}
			if (dr.Table.Columns.Contains("StatisticType"))
			{
			entity.StatisticType = dr["StatisticType"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["StatisticType"];
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = dr["CelebrityId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = dr["LastUpdatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdatedDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
