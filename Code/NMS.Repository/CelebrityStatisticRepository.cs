﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using NMS.Core.Enums;
using System.Data;

namespace NMS.Repository
{
		
	public partial class CelebrityStatisticRepository: CelebrityStatisticRepositoryBase, ICelebrityStatisticRepository
	{
        public bool DeleteCelebrityStatisticByEpisodeId(System.Int32 episodeId)
        {
            string sql = "delete from CelebrityStatistic where EpisodeId=@EpisodeId";
            SqlParameter parameter = new SqlParameter("@EpisodeId", episodeId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) > 0 ? true : false;
        }

        public bool UpdateCelebrityStatistic(System.Int32 celebrityId)
        {
            string sql = string.Format(@"update Celebrity set 
                            AddedToRundown = (select count(*) from CelebrityStatistic where CelebrityId = @CelebrityId and StatisticType = {0}), 
                            OnAired = (select count(*) from CelebrityStatistic where CelebrityId = @CelebrityId and StatisticType = {1}), 
                            ExecutedOnOtherChannels = (select count(*) from CelebrityStatistic where CelebrityId = @CelebrityId and StatisticType = {2})
                        where 
                            CelebrityId=@CelebrityId", (int)NewsStatisticType.AddedToRundown, (int)NewsStatisticType.OnAired, (int)NewsStatisticType.ExecutedOnOtherChannels);

            SqlParameter parameter = new SqlParameter("@CelebrityId", celebrityId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
	}
	
	
}
