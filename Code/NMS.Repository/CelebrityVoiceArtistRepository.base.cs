﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CelebrityVoiceArtistRepositoryBase : Repository, ICelebrityVoiceArtistRepositoryBase 
	{
        
        public CelebrityVoiceArtistRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CelebrityVoiceArtistId",new SearchColumn(){Name="CelebrityVoiceArtistId",Title="CelebrityVoiceArtistId",SelectClause="CelebrityVoiceArtistId",WhereClause="AllRecords.CelebrityVoiceArtistId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CelebrityVoiceArtistId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VoiceArtistId",new SearchColumn(){Name="VoiceArtistId",Title="VoiceArtistId",SelectClause="VoiceArtistId",WhereClause="AllRecords.VoiceArtistId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="VoiceArtistId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Language",new SearchColumn(){Name="Language",Title="Language",SelectClause="Language",WhereClause="AllRecords.Language",DataType="System.String",IsForeignColumn=false,PropertyName="Language",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCelebrityVoiceArtistSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCelebrityVoiceArtistBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCelebrityVoiceArtistAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCelebrityVoiceArtistSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CelebrityVoiceArtist].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CelebrityVoiceArtist].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByCelebrityId(System.Int32? CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityVoiceArtistSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CelebrityVoiceArtist] with (nolock)  where CelebrityId=@CelebrityId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityVoiceArtist>(ds,CelebrityVoiceArtistFromDataRow);
		}

		public virtual List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByVoiceArtistId(System.Int32? VoiceArtistId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityVoiceArtistSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CelebrityVoiceArtist] with (nolock)  where VoiceArtistId=@VoiceArtistId  ";
			SqlParameter parameter=new SqlParameter("@VoiceArtistId",VoiceArtistId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityVoiceArtist>(ds,CelebrityVoiceArtistFromDataRow);
		}

		public virtual CelebrityVoiceArtist GetCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityVoiceArtistSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CelebrityVoiceArtist] with (nolock)  where CelebrityVoiceArtistId=@CelebrityVoiceArtistId ";
			SqlParameter parameter=new SqlParameter("@CelebrityVoiceArtistId",CelebrityVoiceArtistId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CelebrityVoiceArtistFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityVoiceArtistSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CelebrityVoiceArtist] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityVoiceArtist>(ds,CelebrityVoiceArtistFromDataRow);
		}

		public virtual List<CelebrityVoiceArtist> GetAllCelebrityVoiceArtist(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCelebrityVoiceArtistSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CelebrityVoiceArtist] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityVoiceArtist>(ds, CelebrityVoiceArtistFromDataRow);
		}

		public virtual List<CelebrityVoiceArtist> GetPagedCelebrityVoiceArtist(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCelebrityVoiceArtistCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CelebrityVoiceArtistId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CelebrityVoiceArtistId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CelebrityVoiceArtistId] ";
            tempsql += " FROM [CelebrityVoiceArtist] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CelebrityVoiceArtistId"))
					tempsql += " , (AllRecords.[CelebrityVoiceArtistId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CelebrityVoiceArtistId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCelebrityVoiceArtistSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CelebrityVoiceArtist] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CelebrityVoiceArtist].[CelebrityVoiceArtistId] = PageIndex.[CelebrityVoiceArtistId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CelebrityVoiceArtist>(ds, CelebrityVoiceArtistFromDataRow);
			}else{ return null;}
		}

		private int GetCelebrityVoiceArtistCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CelebrityVoiceArtist as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CelebrityVoiceArtist as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CelebrityVoiceArtist))]
		public virtual CelebrityVoiceArtist InsertCelebrityVoiceArtist(CelebrityVoiceArtist entity)
		{

			CelebrityVoiceArtist other=new CelebrityVoiceArtist();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CelebrityVoiceArtist ( [CelebrityId]
				,[VoiceArtistId]
				,[Language]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @CelebrityId
				, @VoiceArtistId
				, @Language
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@VoiceArtistId",entity.VoiceArtistId ?? (object)DBNull.Value)
					, new SqlParameter("@Language",entity.Language ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCelebrityVoiceArtist(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CelebrityVoiceArtist))]
		public virtual CelebrityVoiceArtist UpdateCelebrityVoiceArtist(CelebrityVoiceArtist entity)
		{

			if (entity.IsTransient()) return entity;
			CelebrityVoiceArtist other = GetCelebrityVoiceArtist(entity.CelebrityVoiceArtistId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CelebrityVoiceArtist set  [CelebrityId]=@CelebrityId
							, [VoiceArtistId]=@VoiceArtistId
							, [Language]=@Language
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where CelebrityVoiceArtistId=@CelebrityVoiceArtistId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@VoiceArtistId",entity.VoiceArtistId ?? (object)DBNull.Value)
					, new SqlParameter("@Language",entity.Language ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityVoiceArtistId",entity.CelebrityVoiceArtistId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCelebrityVoiceArtist(entity.CelebrityVoiceArtistId);
		}

		public virtual bool DeleteCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId)
		{

			string sql="delete from CelebrityVoiceArtist where CelebrityVoiceArtistId=@CelebrityVoiceArtistId";
			SqlParameter parameter=new SqlParameter("@CelebrityVoiceArtistId",CelebrityVoiceArtistId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CelebrityVoiceArtist))]
		public virtual CelebrityVoiceArtist DeleteCelebrityVoiceArtist(CelebrityVoiceArtist entity)
		{
			this.DeleteCelebrityVoiceArtist(entity.CelebrityVoiceArtistId);
			return entity;
		}


		public virtual CelebrityVoiceArtist CelebrityVoiceArtistFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CelebrityVoiceArtist entity=new CelebrityVoiceArtist();
			if (dr.Table.Columns.Contains("CelebrityVoiceArtistId"))
			{
			entity.CelebrityVoiceArtistId = (System.Int32)dr["CelebrityVoiceArtistId"];
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = dr["CelebrityId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("VoiceArtistId"))
			{
			entity.VoiceArtistId = dr["VoiceArtistId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["VoiceArtistId"];
			}
			if (dr.Table.Columns.Contains("Language"))
			{
			entity.Language = dr["Language"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
