﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ChannelCelebrityRepositoryBase : Repository, IChannelCelebrityRepositoryBase 
	{
        
        public ChannelCelebrityRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ChannelCelebrityId",new SearchColumn(){Name="ChannelCelebrityId",Title="ChannelCelebrityId",SelectClause="ChannelCelebrityId",WhereClause="AllRecords.ChannelCelebrityId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelCelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityTypeId",new SearchColumn(){Name="CelebrityTypeId",Title="CelebrityTypeId",SelectClause="CelebrityTypeId",WhereClause="AllRecords.CelebrityTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CelebrityTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetChannelCelebritySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetChannelCelebrityBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetChannelCelebrityAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetChannelCelebritySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ChannelCelebrity].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ChannelCelebrity].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ChannelCelebrity> GetChannelCelebrityByChannelId(System.Int32 ChannelId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelCelebrity] with (nolock)  where ChannelId=@ChannelId  ";
			SqlParameter parameter=new SqlParameter("@ChannelId",ChannelId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelCelebrity>(ds,ChannelCelebrityFromDataRow);
		}

		public virtual List<ChannelCelebrity> GetChannelCelebrityByCelebrityId(System.Int32 CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelCelebrity] with (nolock)  where CelebrityId=@CelebrityId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelCelebrity>(ds,ChannelCelebrityFromDataRow);
		}

		public virtual List<ChannelCelebrity> GetChannelCelebrityByCelebrityTypeId(System.Int32 CelebrityTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelCelebrity] with (nolock)  where CelebrityTypeId=@CelebrityTypeId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityTypeId",CelebrityTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelCelebrity>(ds,ChannelCelebrityFromDataRow);
		}

		public virtual ChannelCelebrity GetChannelCelebrity(System.Int32 ChannelCelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelCelebrity] with (nolock)  where ChannelCelebrityId=@ChannelCelebrityId ";
			SqlParameter parameter=new SqlParameter("@ChannelCelebrityId",ChannelCelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ChannelCelebrityFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ChannelCelebrity> GetChannelCelebrityByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ChannelCelebrity] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelCelebrity>(ds,ChannelCelebrityFromDataRow);
		}

		public virtual List<ChannelCelebrity> GetAllChannelCelebrity(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelCelebritySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelCelebrity] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelCelebrity>(ds, ChannelCelebrityFromDataRow);
		}

		public virtual List<ChannelCelebrity> GetPagedChannelCelebrity(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetChannelCelebrityCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ChannelCelebrityId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ChannelCelebrityId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ChannelCelebrityId] ";
            tempsql += " FROM [ChannelCelebrity] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ChannelCelebrityId"))
					tempsql += " , (AllRecords.[ChannelCelebrityId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ChannelCelebrityId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetChannelCelebritySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ChannelCelebrity] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ChannelCelebrity].[ChannelCelebrityId] = PageIndex.[ChannelCelebrityId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelCelebrity>(ds, ChannelCelebrityFromDataRow);
			}else{ return null;}
		}

		private int GetChannelCelebrityCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ChannelCelebrity as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ChannelCelebrity as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ChannelCelebrity))]
		public virtual ChannelCelebrity InsertChannelCelebrity(ChannelCelebrity entity)
		{

			ChannelCelebrity other=new ChannelCelebrity();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ChannelCelebrity ( [ChannelId]
				,[CelebrityId]
				,[CelebrityTypeId]
				,[IsActive]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @ChannelId
				, @CelebrityId
				, @CelebrityTypeId
				, @IsActive
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@CelebrityId",entity.CelebrityId)
					, new SqlParameter("@CelebrityTypeId",entity.CelebrityTypeId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetChannelCelebrity(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ChannelCelebrity))]
		public virtual ChannelCelebrity UpdateChannelCelebrity(ChannelCelebrity entity)
		{

			if (entity.IsTransient()) return entity;
			ChannelCelebrity other = GetChannelCelebrity(entity.ChannelCelebrityId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ChannelCelebrity set  [ChannelId]=@ChannelId
							, [CelebrityId]=@CelebrityId
							, [CelebrityTypeId]=@CelebrityTypeId
							, [IsActive]=@IsActive
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where ChannelCelebrityId=@ChannelCelebrityId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@CelebrityId",entity.CelebrityId)
					, new SqlParameter("@CelebrityTypeId",entity.CelebrityTypeId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelCelebrityId",entity.ChannelCelebrityId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetChannelCelebrity(entity.ChannelCelebrityId);
		}

		public virtual bool DeleteChannelCelebrity(System.Int32 ChannelCelebrityId)
		{

			string sql="delete from ChannelCelebrity where ChannelCelebrityId=@ChannelCelebrityId";
			SqlParameter parameter=new SqlParameter("@ChannelCelebrityId",ChannelCelebrityId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ChannelCelebrity))]
		public virtual ChannelCelebrity DeleteChannelCelebrity(ChannelCelebrity entity)
		{
			this.DeleteChannelCelebrity(entity.ChannelCelebrityId);
			return entity;
		}


		public virtual ChannelCelebrity ChannelCelebrityFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ChannelCelebrity entity=new ChannelCelebrity();
			if (dr.Table.Columns.Contains("ChannelCelebrityId"))
			{
			entity.ChannelCelebrityId = (System.Int32)dr["ChannelCelebrityId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = (System.Int32)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = (System.Int32)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("CelebrityTypeId"))
			{
			entity.CelebrityTypeId = (System.Int32)dr["CelebrityTypeId"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
