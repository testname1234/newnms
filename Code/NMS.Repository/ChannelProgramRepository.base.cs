﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ChannelProgramRepositoryBase : Repository, IChannelProgramRepositoryBase 
	{
        
        public ChannelProgramRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ChannelProgramId",new SearchColumn(){Name="ChannelProgramId",Title="ChannelProgramId",SelectClause="ChannelProgramId",WhereClause="AllRecords.ChannelProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetChannelProgramSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetChannelProgramBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetChannelProgramAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetChannelProgramSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ChannelProgram."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",ChannelProgram."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ChannelProgram> GetChannelProgramByChannelId(System.Int32 ChannelId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ChannelProgram with (nolock)  where ChannelId=@ChannelId  ";
			SqlParameter parameter=new SqlParameter("@ChannelId",ChannelId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelProgram>(ds,ChannelProgramFromDataRow);
		}

		public virtual List<ChannelProgram> GetChannelProgramByProgramId(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ChannelProgram with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelProgram>(ds,ChannelProgramFromDataRow);
		}

		public virtual ChannelProgram GetChannelProgram(System.Int32 ChannelProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ChannelProgram with (nolock)  where ChannelProgramId=@ChannelProgramId ";
			SqlParameter parameter=new SqlParameter("@ChannelProgramId",ChannelProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ChannelProgramFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ChannelProgram> GetChannelProgramByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ChannelProgram with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelProgram>(ds,ChannelProgramFromDataRow);
		}

		public virtual List<ChannelProgram> GetAllChannelProgram(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ChannelProgram with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelProgram>(ds, ChannelProgramFromDataRow);
		}

		public virtual List<ChannelProgram> GetPagedChannelProgram(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetChannelProgramCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ChannelProgramId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ChannelProgramId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ChannelProgramId] ";
            tempsql += " FROM [ChannelProgram] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ChannelProgramId"))
					tempsql += " , (AllRecords.[ChannelProgramId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ChannelProgramId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetChannelProgramSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ChannelProgram] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ChannelProgram].[ChannelProgramId] = PageIndex.[ChannelProgramId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelProgram>(ds, ChannelProgramFromDataRow);
			}else{ return null;}
		}

		private int GetChannelProgramCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ChannelProgram as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ChannelProgram as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ChannelProgram))]
		public virtual ChannelProgram InsertChannelProgram(ChannelProgram entity)
		{

			ChannelProgram other=new ChannelProgram();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ChannelProgram ( [ChannelId]
				,[ProgramId]
				,[CreationDate]
				,[IsActive] )
				Values
				( @ChannelId
				, @ProgramId
				, @CreationDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetChannelProgram(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ChannelProgram))]
		public virtual ChannelProgram UpdateChannelProgram(ChannelProgram entity)
		{

			if (entity.IsTransient()) return entity;
			ChannelProgram other = GetChannelProgram(entity.ChannelProgramId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ChannelProgram set  [ChannelId]=@ChannelId
							, [ProgramId]=@ProgramId
							, [CreationDate]=@CreationDate
							, [IsActive]=@IsActive 
							 where ChannelProgramId=@ChannelProgramId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ChannelProgramId",entity.ChannelProgramId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetChannelProgram(entity.ChannelProgramId);
		}

		public virtual bool DeleteChannelProgram(System.Int32 ChannelProgramId)
		{

			string sql="delete from ChannelProgram with (nolock) where ChannelProgramId=@ChannelProgramId";
			SqlParameter parameter=new SqlParameter("@ChannelProgramId",ChannelProgramId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ChannelProgram))]
		public virtual ChannelProgram DeleteChannelProgram(ChannelProgram entity)
		{
			this.DeleteChannelProgram(entity.ChannelProgramId);
			return entity;
		}


		public virtual ChannelProgram ChannelProgramFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ChannelProgram entity=new ChannelProgram();
			if (dr.Table.Columns.Contains("ChannelProgramId"))
			{
			entity.ChannelProgramId = (System.Int32)dr["ChannelProgramId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = (System.Int32)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
