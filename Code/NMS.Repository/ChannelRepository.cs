﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class ChannelRepository: ChannelRepositoryBase, IChannelRepository
	{

        public List<Channel> GetChannelsByOtherChannelBit(bool flag)
        {
            return base.GetChannelByKeyValue("IsOtherChannel", Convert.ToInt32(flag).ToString(), Operands.Equal);
        }

        public List<Channel> GetChannelsByUserId(int userId)
        {
            string sql = "select distinct c.* from Team t inner join Program p on t.programid=p.programid inner join channel c on p.channelid = c.channelid where userid = @userId";
            SqlParameter parameter = new SqlParameter("@userId", userId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Channel>(ds, ChannelFromDataRow);
        }
    }
	
	
}
