﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ChannelVideoRepositoryBase : Repository, IChannelVideoRepositoryBase 
	{
        
        public ChannelVideoRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ChannelVideoId",new SearchColumn(){Name="ChannelVideoId",Title="ChannelVideoId",SelectClause="ChannelVideoId",WhereClause="AllRecords.ChannelVideoId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelVideoId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("From",new SearchColumn(){Name="From",Title="From",SelectClause="From",WhereClause="AllRecords.From",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="From",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("To",new SearchColumn(){Name="To",Title="To",SelectClause="To",WhereClause="AllRecords.To",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="To",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PhysicalPath",new SearchColumn(){Name="PhysicalPath",Title="PhysicalPath",SelectClause="PhysicalPath",WhereClause="AllRecords.PhysicalPath",DataType="System.String",IsForeignColumn=false,PropertyName="PhysicalPath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Url",new SearchColumn(){Name="Url",Title="Url",SelectClause="Url",WhereClause="AllRecords.Url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsProcessed",new SearchColumn(){Name="IsProcessed",Title="IsProcessed",SelectClause="IsProcessed",WhereClause="AllRecords.IsProcessed",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsProcessed",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetChannelVideoSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetChannelVideoBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetChannelVideoAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetChannelVideoSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ChannelVideo.["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",ChannelVideo.["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ChannelVideo GetChannelVideo(System.Int32 ChannelVideoId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelVideoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ChannelVideo with (nolock)  where ChannelVideoId=@ChannelVideoId ";
			SqlParameter parameter=new SqlParameter("@ChannelVideoId",ChannelVideoId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ChannelVideoFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ChannelVideo> GetChannelVideoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelVideoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ChannelVideo with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelVideo>(ds,ChannelVideoFromDataRow);
		}

		public virtual List<ChannelVideo> GetAllChannelVideo(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelVideoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ChannelVideo with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelVideo>(ds, ChannelVideoFromDataRow);
		}

		public virtual List<ChannelVideo> GetPagedChannelVideo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetChannelVideoCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ChannelVideoId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ChannelVideoId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ChannelVideoId] ";
            tempsql += " FROM [ChannelVideo] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ChannelVideoId"))
					tempsql += " , (AllRecords.[ChannelVideoId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ChannelVideoId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetChannelVideoSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ChannelVideo] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ChannelVideo].[ChannelVideoId] = PageIndex.[ChannelVideoId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelVideo>(ds, ChannelVideoFromDataRow);
			}else{ return null;}
		}

		private int GetChannelVideoCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ChannelVideo as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ChannelVideo as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ChannelVideo))]
		public virtual ChannelVideo InsertChannelVideo(ChannelVideo entity)
		{

			ChannelVideo other=new ChannelVideo();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ChannelVideo ( [ChannelId]
				,[From]
				,[To]
				,[ProgramId]
				,[PhysicalPath]
				,[Url]
				,[IsProcessed]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @ChannelId
				, @From
				, @To
				, @ProgramId
				, @PhysicalPath
				, @Url
				, @IsProcessed
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@PhysicalPath",entity.PhysicalPath ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@IsProcessed",entity.IsProcessed)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetChannelVideo(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ChannelVideo))]
		public virtual ChannelVideo UpdateChannelVideo(ChannelVideo entity)
		{

			if (entity.IsTransient()) return entity;
			ChannelVideo other = GetChannelVideo(entity.ChannelVideoId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ChannelVideo set  [ChannelId]=@ChannelId
							, [From]=@From
							, [To]=@To
							, [ProgramId]=@ProgramId
							, [PhysicalPath]=@PhysicalPath
							, [Url]=@Url
							, [IsProcessed]=@IsProcessed
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ChannelVideoId=@ChannelVideoId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@PhysicalPath",entity.PhysicalPath ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@IsProcessed",entity.IsProcessed)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ChannelVideoId",entity.ChannelVideoId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetChannelVideo(entity.ChannelVideoId);
		}

		public virtual bool DeleteChannelVideo(System.Int32 ChannelVideoId)
		{

			string sql="delete from ChannelVideo where ChannelVideoId=@ChannelVideoId";
			SqlParameter parameter=new SqlParameter("@ChannelVideoId",ChannelVideoId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ChannelVideo))]
		public virtual ChannelVideo DeleteChannelVideo(ChannelVideo entity)
		{
			this.DeleteChannelVideo(entity.ChannelVideoId);
			return entity;
		}


		public virtual ChannelVideo ChannelVideoFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ChannelVideo entity=new ChannelVideo();
			if (dr.Table.Columns.Contains("ChannelVideoId"))
			{
			entity.ChannelVideoId = (System.Int32)dr["ChannelVideoId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = dr["ChannelId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("From"))
			{
			entity.From = dr["From"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["From"];
			}
			if (dr.Table.Columns.Contains("To"))
			{
			entity.To = dr["To"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["To"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("PhysicalPath"))
			{
			entity.PhysicalPath = dr["PhysicalPath"].ToString();
			}
			if (dr.Table.Columns.Contains("Url"))
			{
			entity.Url = dr["Url"].ToString();
			}
			if (dr.Table.Columns.Contains("IsProcessed"))
			{
			entity.IsProcessed = (System.Boolean)dr["IsProcessed"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
