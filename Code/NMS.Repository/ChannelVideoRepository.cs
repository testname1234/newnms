﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class ChannelVideoRepository: ChannelVideoRepositoryBase, IChannelVideoRepository
	{
        public bool ChannelVideoExist(int channelId, DateTime dt)
        {
            string sql = "select count(1) from [ChannelVideo] with (nolock) where channelid=@channelid and [From]=@dt";
            SqlParameter parameter = new SqlParameter("@channelid", (int)channelId);
            SqlParameter parameter1 = new SqlParameter("@dt", dt);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 })) > 0;
        }

        public List<ChannelVideo> GetAllChannelVideosBetweenDates(DateTime dtFrom, DateTime dtTo)
        {
            string sql = GetChannelVideoSelectClause();
            sql += "from ChannelVideo with (nolock)  where [From] >= @From and [To] <= @To ";
            SqlParameter parameter1 = new SqlParameter("@From", dtFrom);
            SqlParameter parameter2 = new SqlParameter("@To", dtTo);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ChannelVideo>(ds, ChannelVideoFromDataRow);
        }

        public List<ChannelVideo> GetChannelVideoByProgramAndStartEnd(DateTime fr,DateTime to,int programId,int ChannelId)
        {
            string sql = "select * from ChannelVideo with (nolock)  where ChannelId=@ChannelId and ProgramId=@pid and [From] = @From and [To] = @To ";
            SqlParameter parameter1 = new SqlParameter("@From", fr);
            SqlParameter parameter2 = new SqlParameter("@To", to);
            SqlParameter parameter3 = new SqlParameter("@pid", programId);
            SqlParameter parameter4 = new SqlParameter("@ChannelId", ChannelId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2,parameter3, parameter4 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ChannelVideo>(ds, ChannelVideoFromDataRow);
        }

        public List<ChannelVideo> GetDistinctPrograms()
        {
            string sql = "SELECT distinct(programid) * FROM channelvideo";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ChannelVideo>(ds, ChannelVideoFromDataRow);
        }
        public ChannelVideo GetLastChannelVideo(int ChannelId)
        {
            string sql = "SELECT TOP 1 * FROM channelvideo Where ChannelId=@ChannelId order by [To] desc";
            SqlParameter parameter = new SqlParameter("@ChannelId", ChannelId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ChannelVideoFromDataRow(ds.Tables[0].Rows[0]);
        }

        public ChannelVideo GetChannelVideoByStartEndTime(DateTime StartDate, DateTime EndTime, int ChannelId)
        {
            string sql = GetChannelVideoSelectClause();
            sql += "From ChannelVideo Where [From]=@from And [To]=@to And ChannelId=@ChannelId";
            SqlParameter parameter1 = new SqlParameter("@from", StartDate);
            SqlParameter parameter2 = new SqlParameter("@to", EndTime);
            SqlParameter parameter3 = new SqlParameter("@ChannelId", ChannelId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2, parameter3 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ChannelVideoFromDataRow(ds.Tables[0].Rows[0]);
        }

        public void MarkIsProcessed(int id)
        {
            string sql = "update ChannelVideo set IsProcessed=1 where ChannelVideoId=@Id";
            SqlParameter parameter = new SqlParameter("@Id", id);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
        }

        public virtual ChannelVideo InsertChannelVideo(ChannelVideo entity, string connectionString)
        {

            ChannelVideo other = new ChannelVideo();
            other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into ChannelVideo ( [ChannelId]
				,[From]
				,[To]
				,[ProgramId]
				,[PhysicalPath]
				,[Url]
				,[IsProcessed]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @ChannelId
				, @From
				, @To
				, @ProgramId
				, @PhysicalPath
				, @Url
				, @IsProcessed
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@PhysicalPath",entity.PhysicalPath ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@IsProcessed",entity.IsProcessed)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
                var identity = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
                return GetChannelVideo(Convert.ToInt32(identity));
            }
            return entity;
        }
    }
	
	
}
