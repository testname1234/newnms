﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ChatMessageRepositoryBase : Repository, IChatMessageRepositoryBase 
	{
        
        public ChatMessageRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MessageId",new SearchColumn(){Name="MessageId",Title="MessageId",SelectClause="MessageId",WhereClause="AllRecords.MessageId",DataType="System.Int32",IsForeignColumn=false,PropertyName="MessageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("From",new SearchColumn(){Name="From",Title="From",SelectClause="From",WhereClause="AllRecords.From",DataType="System.Int32",IsForeignColumn=false,PropertyName="From",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("To",new SearchColumn(){Name="To",Title="To",SelectClause="To",WhereClause="AllRecords.To",DataType="System.Int32",IsForeignColumn=false,PropertyName="To",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Message",new SearchColumn(){Name="Message",Title="Message",SelectClause="Message",WhereClause="AllRecords.Message",DataType="System.String",IsForeignColumn=false,PropertyName="Message",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsRecieved",new SearchColumn(){Name="IsRecieved",Title="IsRecieved",SelectClause="IsRecieved",WhereClause="AllRecords.IsRecieved",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsRecieved",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsSentToGroup",new SearchColumn(){Name="IsSentToGroup",Title="IsSentToGroup",SelectClause="IsSentToGroup",WhereClause="AllRecords.IsSentToGroup",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsSentToGroup",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetChatMessageSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetChatMessageBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetChatMessageAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetChatMessageSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ChatMessage].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ChatMessage].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ChatMessage GetChatMessage(System.Int32 MessageId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChatMessageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChatMessage] with (nolock)  where MessageId=@MessageId ";
			SqlParameter parameter=new SqlParameter("@MessageId",MessageId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ChatMessageFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ChatMessage> GetChatMessageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChatMessageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ChatMessage] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChatMessage>(ds,ChatMessageFromDataRow);
		}

		public virtual List<ChatMessage> GetAllChatMessage(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChatMessageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChatMessage] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChatMessage>(ds, ChatMessageFromDataRow);
		}

		public virtual List<ChatMessage> GetPagedChatMessage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetChatMessageCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MessageId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MessageId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MessageId] ";
            tempsql += " FROM [ChatMessage] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MessageId"))
					tempsql += " , (AllRecords.[MessageId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MessageId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetChatMessageSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ChatMessage] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ChatMessage].[MessageId] = PageIndex.[MessageId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChatMessage>(ds, ChatMessageFromDataRow);
			}else{ return null;}
		}

		private int GetChatMessageCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ChatMessage as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ChatMessage as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ChatMessage))]
		public virtual ChatMessage InsertChatMessage(ChatMessage entity)
		{

			ChatMessage other=new ChatMessage();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ChatMessage ( [From]
				,[To]
				,[Message]
				,[CreationDate]
				,[IsRecieved]
				,[IsSentToGroup] )
				Values
				( @From
				, @To
				, @Message
				, @CreationDate
				, @IsRecieved
				, @IsSentToGroup );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@From",entity.From)
					, new SqlParameter("@To",entity.To)
					, new SqlParameter("@Message",entity.Message)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@IsRecieved",entity.IsRecieved)
					, new SqlParameter("@IsSentToGroup",entity.IsSentToGroup ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetChatMessage(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ChatMessage))]
		public virtual ChatMessage UpdateChatMessage(ChatMessage entity)
		{

			if (entity.IsTransient()) return entity;
			ChatMessage other = GetChatMessage(entity.MessageId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ChatMessage set  [From]=@From
							, [To]=@To
							, [Message]=@Message
							, [CreationDate]=@CreationDate
							, [IsRecieved]=@IsRecieved
							, [IsSentToGroup]=@IsSentToGroup 
							 where MessageId=@MessageId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@From",entity.From)
					, new SqlParameter("@To",entity.To)
					, new SqlParameter("@Message",entity.Message)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@IsRecieved",entity.IsRecieved)
					, new SqlParameter("@IsSentToGroup",entity.IsSentToGroup ?? (object)DBNull.Value)
					, new SqlParameter("@MessageId",entity.MessageId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetChatMessage(entity.MessageId);
		}

		public virtual bool DeleteChatMessage(System.Int32 MessageId)
		{

			string sql="delete from ChatMessage where MessageId=@MessageId";
			SqlParameter parameter=new SqlParameter("@MessageId",MessageId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ChatMessage))]
		public virtual ChatMessage DeleteChatMessage(ChatMessage entity)
		{
			this.DeleteChatMessage(entity.MessageId);
			return entity;
		}


		public virtual ChatMessage ChatMessageFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ChatMessage entity=new ChatMessage();
			if (dr.Table.Columns.Contains("MessageId"))
			{
			entity.MessageId = (System.Int32)dr["MessageId"];
			}
			if (dr.Table.Columns.Contains("From"))
			{
			entity.From = (System.Int32)dr["From"];
			}
			if (dr.Table.Columns.Contains("To"))
			{
			entity.To = (System.Int32)dr["To"];
			}
			if (dr.Table.Columns.Contains("Message"))
			{
			entity.Message = dr["Message"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("IsRecieved"))
			{
			entity.IsRecieved = (System.Boolean)dr["IsRecieved"];
			}
			if (dr.Table.Columns.Contains("IsSentToGroup"))
			{
			entity.IsSentToGroup = dr["IsSentToGroup"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsSentToGroup"];
			}
			return entity;
		}

	}
	
	
}
