﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class ChatMessageRepository: ChatMessageRepositoryBase, IChatMessageRepository
	{
        public List<ChatMessage> GetChatMessagesForUser(int UserId,DateTime date)
        {
            string sql = "select * from [ChatMessage] with (nolock) where (chatmessage.[From]=@UserId or chatmessage.[To]=@UserId) and CreationDate>=@date and isSentTogroup is null";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@UserId",UserId)
					, new SqlParameter("@date",date)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ChatMessage>(ds, ChatMessageFromDataRow);
        }

        public List<ChatMessage> GetAllGroupMessages(DateTime date)
        {
            string sql = "select * from [ChatMessage] with (nolock) where isSentTogroup is not null and isSentTogroup=1 and CreationDate>=@date";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@date",date)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ChatMessage>(ds, ChatMessageFromDataRow);
        }
	}
	
	
}
