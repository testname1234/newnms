﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CheckListRepositoryBase : Repository, ICheckListRepositoryBase 
	{
        
        public CheckListRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ChecklistId",new SearchColumn(){Name="ChecklistId",Title="ChecklistId",SelectClause="ChecklistId",WhereClause="AllRecords.ChecklistId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChecklistId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChecklistName",new SearchColumn(){Name="ChecklistName",Title="ChecklistName",SelectClause="ChecklistName",WhereClause="AllRecords.ChecklistName",DataType="System.String",IsForeignColumn=false,PropertyName="ChecklistName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Status",new SearchColumn(){Name="Status",Title="Status",SelectClause="Status",WhereClause="AllRecords.Status",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Status",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ErrorMessage",new SearchColumn(){Name="ErrorMessage",Title="ErrorMessage",SelectClause="ErrorMessage",WhereClause="AllRecords.ErrorMessage",DataType="System.String",IsForeignColumn=false,PropertyName="ErrorMessage",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Priority",new SearchColumn(){Name="Priority",Title="Priority",SelectClause="Priority",WhereClause="AllRecords.Priority",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Priority",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCheckListSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCheckListBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCheckListAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCheckListSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[CheckList].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[CheckList].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual CheckList GetCheckList(System.Int32 ChecklistId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCheckListSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CheckList] with (nolock)  where ChecklistId=@ChecklistId ";
			SqlParameter parameter=new SqlParameter("@ChecklistId",ChecklistId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CheckListFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<CheckList> GetCheckListByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCheckListSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [CheckList] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CheckList>(ds,CheckListFromDataRow);
		}

		public virtual List<CheckList> GetAllCheckList(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCheckListSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [CheckList] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CheckList>(ds, CheckListFromDataRow);
		}

		public virtual List<CheckList> GetPagedCheckList(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCheckListCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ChecklistId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ChecklistId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ChecklistId] ";
            tempsql += " FROM [CheckList] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ChecklistId"))
					tempsql += " , (AllRecords.[ChecklistId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ChecklistId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCheckListSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [CheckList] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [CheckList].[ChecklistId] = PageIndex.[ChecklistId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<CheckList>(ds, CheckListFromDataRow);
			}else{ return null;}
		}

		private int GetCheckListCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM CheckList as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM CheckList as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(CheckList))]
		public virtual CheckList InsertCheckList(CheckList entity)
		{

			CheckList other=new CheckList();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into CheckList ( [ChecklistName]
				,[Status]
				,[ErrorMessage]
				,[CreationDate]
				,[Priority]
				,[LastUpdatedDate] )
				Values
				( @ChecklistName
				, @Status
				, @ErrorMessage
				, @CreationDate
				, @Priority
				, @LastUpdatedDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChecklistName",entity.ChecklistName ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@ErrorMessage",entity.ErrorMessage ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@Priority",entity.Priority ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetCheckList(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(CheckList))]
		public virtual CheckList UpdateCheckList(CheckList entity)
		{

			if (entity.IsTransient()) return entity;
			CheckList other = GetCheckList(entity.ChecklistId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update CheckList set  [ChecklistName]=@ChecklistName
							, [Status]=@Status
							, [ErrorMessage]=@ErrorMessage
							, [CreationDate]=@CreationDate
							, [Priority]=@Priority
							, [LastUpdatedDate]=@LastUpdatedDate 
							 where ChecklistId=@ChecklistId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChecklistName",entity.ChecklistName ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@ErrorMessage",entity.ErrorMessage ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@Priority",entity.Priority ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@ChecklistId",entity.ChecklistId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetCheckList(entity.ChecklistId);
		}

		public virtual bool DeleteCheckList(System.Int32 ChecklistId)
		{

			string sql="delete from CheckList where ChecklistId=@ChecklistId";
			SqlParameter parameter=new SqlParameter("@ChecklistId",ChecklistId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(CheckList))]
		public virtual CheckList DeleteCheckList(CheckList entity)
		{
			this.DeleteCheckList(entity.ChecklistId);
			return entity;
		}


		public virtual CheckList CheckListFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			CheckList entity=new CheckList();
			if (dr.Table.Columns.Contains("ChecklistId"))
			{
			entity.ChecklistId = (System.Int32)dr["ChecklistId"];
			}
			if (dr.Table.Columns.Contains("ChecklistName"))
			{
			entity.ChecklistName = dr["ChecklistName"].ToString();
			}
			if (dr.Table.Columns.Contains("Status"))
			{
			entity.Status = dr["Status"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Status"];
			}
			if (dr.Table.Columns.Contains("ErrorMessage"))
			{
			entity.ErrorMessage = dr["ErrorMessage"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("Priority"))
			{
			entity.Priority = dr["Priority"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Priority"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = dr["LastUpdatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdatedDate"];
			}
			return entity;
		}

	}
	
	
}
