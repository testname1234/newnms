﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace NMS.Repository
{

    public partial class CheckListRepository : CheckListRepositoryBase, ICheckListRepository
    {
        SqlConnection connecLocal = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());

        public List<CheckList> GetNameErrorMessageCheckList()
        {
            //create ur connection obj            
            string sql = @"select * from checklist where Status = 0 AND (Priority = 0 AND datediff(DD,LastUpdatedDate,SYSUTCDATETIME()) < = 1)";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<CheckList>(ds, CheckListFromDataRow);
        }

        public void UpdatePriority(string Name, bool Priority)
        {
            string sql = @"Update CheckList set                              
							  [Priority]=@Priority 
                            , [LastUpdatedDate]=@LastUpdatedDate 
							 where [ChecklistName]=@ChecklistName";
            SqlParameter[] parameterArray = new SqlParameter[]{					 
					 new SqlParameter("@ChecklistName",Name)
					, new SqlParameter("@Priority",Priority)
                    , new SqlParameter("@LastUpdatedDate",DateTime.Now)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);

        }

        public List<CheckList> GetPriority()
        {
            string sql = @"select * from checklist order by priority,status";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<CheckList>(ds, CheckListFromDataRow);
        }


        //        public int Selection(string Name)
        //        {
        //            SqlConnection connec = new SqlConnection(@"Data Source=10.1.20.57\axact;Initial Catalog=NMS;user id=sa; password=Axact123");
        //            string sql = @"IF EXISTS (SELECT TOP 1 1 FROM CheckList WHERE ChecklistName = @ChecklistName)
        //BEGIN
        //SELECT 1
        //END
        //ELSE
        //BEGIN
        //SELECT 0
        //end";
        //            SqlParameter parameterArray = new SqlParameter("@ChecklistName", Name);
        //            DataSet ds = SqlHelper.ExecuteDataset(connec, CommandType.Text, sql);
        //            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return 0;
        //            return CheckListFromDataRow(ds.Tables[0].Rows[0]).ChecklistId;
        //        }


        public int Selection(string Name)
        {
            {
                int result;
                try
                {
                    SqlCommand cmd1 = new SqlCommand();
                    //cmd1 = new SqlCommand(); cmd1.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["CommandTimeout"] == null ? "12000000" : ConfigurationManager.AppSettings["CommandTimeout"]);
                    cmd1.Connection = connecLocal;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.CommandText = @"IF EXISTS (SELECT 1 FROM CheckList (nolock) WHERE ChecklistName = @Name)
                        BEGIN
                        SELECT 1
                        END
                        ELSE
                        BEGIN
                        SELECT 0
                        end";
                    cmd1.Parameters.AddWithValue("@Name", Name);
                    connecLocal.Open();
                    result = Convert.ToInt32(cmd1.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (connecLocal.State == ConnectionState.Open)
                        connecLocal.Close();
                }
                return result;
            }
        }

        public CheckList InsertResult(CheckList entity)
        {
            string sql = @"Insert into CheckList (ChecklistName,Status,ErrorMessage,CreationDate,LastUpdatedDate,Priority) 
                    Values (@ChecklistName,@Status,@ErrorMessage,@CreationDate,@LastUpdatedDate,@Priority)";

            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@ChecklistName",entity.ChecklistName ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status)
					, new SqlParameter("@ErrorMessage",entity.ErrorMessage ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Priority",entity.Priority)
                    , new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate?? DateTime.UtcNow)};
            var identity = SqlHelper.ExecuteNonQuery(connecLocal, CommandType.Text, sql, parameterArray);
            return GetCheckListLocal(entity.ChecklistId);
        }

        public CheckList UpdateResult(CheckList entity)
        {
            string sql = @"update CheckList set ChecklistName=@ChecklistName,Status=@Status,ErrorMessage=@ErrorMessage,CreationDate=@CreationDate,LastUpdatedDate=@LastUpdatedDate where ChecklistName=@ChecklistName";

            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@ChecklistName",entity.ChecklistName ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status)
					, new SqlParameter("@ErrorMessage",entity.ErrorMessage ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
                    , new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate?? DateTime.UtcNow)};
            var identity = SqlHelper.ExecuteNonQuery(connecLocal, CommandType.Text, sql, parameterArray);
            return GetCheckListLocal(entity.ChecklistId);
        }

        public virtual CheckList GetCheckListLocal(System.Int32 ChecklistId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetCheckListSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [CheckList] with (nolock)  where ChecklistId=@ChecklistId ";
            SqlParameter parameter = new SqlParameter("@ChecklistId", ChecklistId);
            DataSet ds = SqlHelper.ExecuteDataset(connecLocal, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CheckListFromDataRow(ds.Tables[0].Rows[0]);
        }

        //        public void UpdateResult(string Name, bool Status, string ErrorMessage, DateTime CreationDate)
        //        {            
        //            string sql = @"update CheckList set ChecklistName=@Name,Status=@Status,ErrorMessage=@ErrorMessage,CreationDate=@CreationDate where ChecklistName=@Name";
        //            cmd1.Parameters.AddWithValue("@Name", Name);
        //            cmd1.Parameters.AddWithValue("@Status", Status);
        //            SqlParameter ErrorMessageParam = cmd1.Parameters.AddWithValue("@ErrorMessage", ErrorMessage);
        //            if (ErrorMessage == null)
        //            {
        //                ErrorMessageParam.Value = DBNull.Value;
        //            }
        //            cmd1.Parameters.AddWithValue("@CreationDate", CreationDate);
        //        }

    }
}
