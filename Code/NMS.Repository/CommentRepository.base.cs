﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class CommentRepositoryBase : Repository, ICommentRepositoryBase 
	{
        
        public CommentRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("Commentid",new SearchColumn(){Name="Commentid",Title="Commentid",SelectClause="Commentid",WhereClause="AllRecords.Commentid",DataType="System.Int32",IsForeignColumn=false,PropertyName="Commentid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("isactive",new SearchColumn(){Name="isactive",Title="isactive",SelectClause="isactive",WhereClause="AllRecords.isactive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Isactive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CommentTypeId",new SearchColumn(){Name="CommentTypeId",Title="CommentTypeId",SelectClause="CommentTypeId",WhereClause="AllRecords.CommentTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CommentTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Guid",new SearchColumn(){Name="Guid",Title="Guid",SelectClause="Guid",WhereClause="AllRecords.Guid",DataType="System.String",IsForeignColumn=false,PropertyName="Guid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReporterId",new SearchColumn(){Name="ReporterId",Title="ReporterId",SelectClause="ReporterId",WhereClause="AllRecords.ReporterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ReporterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Comment",new SearchColumn(){Name="Comment",Title="Comment",SelectClause="Comment",WhereClause="AllRecords.Comment",DataType="System.String",IsForeignColumn=false,PropertyName="Comment",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetCommentSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetCommentBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetCommentAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetCommentSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Comment].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Comment].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Comment GetComment(System.Int32 Commentid,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCommentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Comment] with (nolock)  where Commentid=@Commentid ";
			SqlParameter parameter=new SqlParameter("@Commentid",Commentid);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return CommentFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Comment> GetCommentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCommentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Comment] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Comment>(ds,CommentFromDataRow);
		}

		public virtual List<Comment> GetAllComment(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetCommentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Comment] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Comment>(ds, CommentFromDataRow);
		}

		public virtual List<Comment> GetPagedComment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetCommentCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [Commentid] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([Commentid])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [Commentid] ";
            tempsql += " FROM [Comment] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("Commentid"))
					tempsql += " , (AllRecords.[Commentid])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[Commentid])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetCommentSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Comment] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Comment].[Commentid] = PageIndex.[Commentid] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Comment>(ds, CommentFromDataRow);
			}else{ return null;}
		}

		private int GetCommentCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Comment as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Comment as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Comment))]
		public virtual Comment InsertComment(Comment entity)
		{

			Comment other=new Comment();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Comment ( [NewsGuid]
				,[UserId]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive]
				,[CommentTypeId]
				,[Guid]
				,[ReporterId]
				,[ResourceGuid]
				,[Comment] )
				Values
				( @NewsGuid
				, @UserId
				, @CreationDate
				, @LastUpdateDate
				, @isactive
				, @CommentTypeId
				, @Guid
				, @ReporterId
				, @ResourceGuid
				, @Comment );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@CommentTypeId",entity.CommentTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Guid",entity.Guid ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Comment",entity.Comment ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetComment(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Comment))]
		public virtual Comment UpdateComment(Comment entity)
		{

			if (entity.IsTransient()) return entity;
			Comment other = GetComment(entity.Commentid);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Comment set  [NewsGuid]=@NewsGuid
							, [UserId]=@UserId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive
							, [CommentTypeId]=@CommentTypeId
							, [Guid]=@Guid
							, [ReporterId]=@ReporterId
							, [ResourceGuid]=@ResourceGuid
							, [Comment]=@Comment 
							 where Commentid=@Commentid";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@CommentTypeId",entity.CommentTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Guid",entity.Guid ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Comment",entity.Comment ?? (object)DBNull.Value)
					, new SqlParameter("@Commentid",entity.Commentid)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetComment(entity.Commentid);
		}

		public virtual bool DeleteComment(System.Int32 Commentid)
		{

			string sql="delete from Comment where Commentid=@Commentid";
			SqlParameter parameter=new SqlParameter("@Commentid",Commentid);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Comment))]
		public virtual Comment DeleteComment(Comment entity)
		{
			this.DeleteComment(entity.Commentid);
			return entity;
		}


		public virtual Comment CommentFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Comment entity=new Comment();
			if (dr.Table.Columns.Contains("Commentid"))
			{
			entity.Commentid = (System.Int32)dr["Commentid"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = dr["UserId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("isactive"))
			{
			entity.Isactive = dr["isactive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["isactive"];
			}
			if (dr.Table.Columns.Contains("CommentTypeId"))
			{
			entity.CommentTypeId = dr["CommentTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CommentTypeId"];
			}
			if (dr.Table.Columns.Contains("Guid"))
			{
			entity.Guid = dr["Guid"].ToString();
			}
			if (dr.Table.Columns.Contains("ReporterId"))
			{
			entity.ReporterId = dr["ReporterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ReporterId"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = dr["ResourceGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("Comment"))
			{
			entity.Comment = dr["Comment"].ToString();
			}
			return entity;
		}

	}
	
	
}
