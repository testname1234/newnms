﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class CommentRepository: CommentRepositoryBase, ICommentRepository
	{
          public CommentRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

          public CommentRepository()
        {
        }
        public virtual Comment GetByGuid(string Guid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetCommentSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += " from Comment with (nolock) where Guid = @Guid order by 1 desc";
            SqlParameter parameter = new SqlParameter("@Guid", Guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CommentFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual Comment GetByNewsGuid(string NewsGuid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetCommentSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += " from Comment with (nolock) where NewsGuid = @NewsGuid";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CommentFromDataRow(ds.Tables[0].Rows[0]);
        }
        
        public virtual void InsertCommentWithPk(Comment entity)
        {
                string sql = @"
                set identity_insert Comment on
                Insert into Comment ( 
                 CommentId   
                ,[NewsGuid]
				,[UserId]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive]
				,[CommentTypeId]
				,[Guid] )
				Values
				(@CommentId   , @NewsGuid
				, @UserId
				, @CreationDate
				, @LastUpdateDate
				, @isactive
				, @CommentTypeId
				, @Guid );
            set identity_insert Comment off
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@CommentId",entity.Commentid)
                     ,new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@CommentTypeId",entity.CommentTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Guid",entity.Guid ?? (object)DBNull.Value)};
                var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");                
        }
        
        public virtual void UpdateCommentNoReturn(Comment entity)
        {            
            string sql = @"Update Comment set  [NewsGuid]=@NewsGuid
							, [UserId]=@UserId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive
							, [CommentTypeId]=@CommentTypeId
							, [Guid]=@Guid 
							 where Commentid=@Commentid";
            SqlParameter[] parameterArray = new SqlParameter[]{                     
					 new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@CommentTypeId",entity.CommentTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@Guid",entity.Guid ?? (object)DBNull.Value)
					, new SqlParameter("@Commentid",entity.Commentid)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);         
        }



	}
	
	
}
