﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Repository
{
    public partial class ConnectionString : IConnectionString
    {
        private string conString = ConfigurationManager.ConnectionStrings["conn"].ToString();
        public virtual string ConnectionStringVar
        {
            get { return conString; }
            set { conString = value; }
        }
    }
}
