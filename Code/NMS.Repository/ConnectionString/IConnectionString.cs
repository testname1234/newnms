﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Repository
{
    public interface IConnectionString
    {
        string ConnectionStringVar { get; set; }
    }
}
