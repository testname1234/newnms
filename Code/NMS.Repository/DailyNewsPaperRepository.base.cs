﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class DailyNewsPaperRepositoryBase : Repository, IDailyNewsPaperRepositoryBase 
	{
        
        public DailyNewsPaperRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("DailyNewsPaperId",new SearchColumn(){Name="DailyNewsPaperId",Title="DailyNewsPaperId",SelectClause="DailyNewsPaperId",WhereClause="AllRecords.DailyNewsPaperId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DailyNewsPaperId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsPaperId",new SearchColumn(){Name="NewsPaperId",Title="NewsPaperId",SelectClause="NewsPaperId",WhereClause="AllRecords.NewsPaperId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsPaperId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Date",new SearchColumn(){Name="Date",Title="Date",SelectClause="Date",WhereClause="AllRecords.Date",DataType="System.DateTime",IsForeignColumn=false,PropertyName="Date",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetDailyNewsPaperSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetDailyNewsPaperBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetDailyNewsPaperAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetDailyNewsPaperSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[DailyNewsPaper].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[DailyNewsPaper].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<DailyNewsPaper> GetDailyNewsPaperByNewsPaperId(System.Int32 NewsPaperId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDailyNewsPaperSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from DailyNewsPaper with (nolock)  where NewsPaperId=@NewsPaperId  ";
			SqlParameter parameter=new SqlParameter("@NewsPaperId",NewsPaperId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DailyNewsPaper>(ds,DailyNewsPaperFromDataRow);
		}

		public virtual DailyNewsPaper GetDailyNewsPaper(System.Int32 DailyNewsPaperId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDailyNewsPaperSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from DailyNewsPaper with (nolock)  where DailyNewsPaperId=@DailyNewsPaperId ";
			SqlParameter parameter=new SqlParameter("@DailyNewsPaperId",DailyNewsPaperId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return DailyNewsPaperFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<DailyNewsPaper> GetDailyNewsPaperByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDailyNewsPaperSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from DailyNewsPaper with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DailyNewsPaper>(ds,DailyNewsPaperFromDataRow);
		}

		public virtual List<DailyNewsPaper> GetAllDailyNewsPaper(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDailyNewsPaperSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from DailyNewsPaper with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DailyNewsPaper>(ds, DailyNewsPaperFromDataRow);
		}

		public virtual List<DailyNewsPaper> GetPagedDailyNewsPaper(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetDailyNewsPaperCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [DailyNewsPaperId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([DailyNewsPaperId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [DailyNewsPaperId] ";
            tempsql += " FROM [DailyNewsPaper] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("DailyNewsPaperId"))
					tempsql += " , (AllRecords.[DailyNewsPaperId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[DailyNewsPaperId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetDailyNewsPaperSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [DailyNewsPaper] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [DailyNewsPaper].[DailyNewsPaperId] = PageIndex.[DailyNewsPaperId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DailyNewsPaper>(ds, DailyNewsPaperFromDataRow);
			}else{ return null;}
		}

		private int GetDailyNewsPaperCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM DailyNewsPaper as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM DailyNewsPaper as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(DailyNewsPaper))]
		public virtual DailyNewsPaper InsertDailyNewsPaper(DailyNewsPaper entity)
		{

			DailyNewsPaper other=new DailyNewsPaper();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into DailyNewsPaper ( [NewsPaperId]
				,[Date]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @NewsPaperId
				, @Date
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsPaperId",entity.NewsPaperId)
					, new SqlParameter("@Date",entity.Date)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetDailyNewsPaper(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(DailyNewsPaper))]
		public virtual DailyNewsPaper UpdateDailyNewsPaper(DailyNewsPaper entity)
		{

			if (entity.IsTransient()) return entity;
			DailyNewsPaper other = GetDailyNewsPaper(entity.DailyNewsPaperId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update DailyNewsPaper set  [NewsPaperId]=@NewsPaperId
							, [Date]=@Date
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where DailyNewsPaperId=@DailyNewsPaperId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsPaperId",entity.NewsPaperId)
					, new SqlParameter("@Date",entity.Date)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@DailyNewsPaperId",entity.DailyNewsPaperId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetDailyNewsPaper(entity.DailyNewsPaperId);
		}

		public virtual bool DeleteDailyNewsPaper(System.Int32 DailyNewsPaperId)
		{

			string sql="delete from DailyNewsPaper with (nolock) where DailyNewsPaperId=@DailyNewsPaperId";
			SqlParameter parameter=new SqlParameter("@DailyNewsPaperId",DailyNewsPaperId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(DailyNewsPaper))]
		public virtual DailyNewsPaper DeleteDailyNewsPaper(DailyNewsPaper entity)
		{
			this.DeleteDailyNewsPaper(entity.DailyNewsPaperId);
			return entity;
		}


		public virtual DailyNewsPaper DailyNewsPaperFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			DailyNewsPaper entity=new DailyNewsPaper();
			if (dr.Table.Columns.Contains("DailyNewsPaperId"))
			{
			entity.DailyNewsPaperId = (System.Int32)dr["DailyNewsPaperId"];
			}
			if (dr.Table.Columns.Contains("NewsPaperId"))
			{
			entity.NewsPaperId = (System.Int32)dr["NewsPaperId"];
			}
			if (dr.Table.Columns.Contains("Date"))
			{
			entity.Date = (System.DateTime)dr["Date"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
