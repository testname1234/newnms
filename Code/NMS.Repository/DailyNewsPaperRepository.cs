﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class DailyNewsPaperRepository: DailyNewsPaperRepositoryBase, IDailyNewsPaperRepository
	{
        public virtual List<DailyNewsPaper> GetDailyNewsPaperByDate(System.DateTime fromDate, System.DateTime toDate, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetDailyNewsPaperSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from DailyNewsPaper with (nolock)  where [date] between @fromDate and @toDate";
            SqlParameter parameter1 = new SqlParameter("@fromDate", fromDate);
            SqlParameter parameter2 = new SqlParameter("@toDate", toDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<DailyNewsPaper>(ds, DailyNewsPaperFromDataRow);
        }

        public virtual List<DailyNewsPaper> GeByDateAndNewsPaperId(System.DateTime fromDate, System.DateTime toDate, int NewsPaperId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetDailyNewsPaperSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from DailyNewsPaper with (nolock)  where [date] between @fromDate and @toDate and NewsPaperId = @NewsPaperId";
            SqlParameter parameter1 = new SqlParameter("@fromDate", fromDate);
            SqlParameter parameter2 = new SqlParameter("@toDate", toDate);
            SqlParameter parameter3 = new SqlParameter("@NewsPaperId", NewsPaperId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2,parameter3 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<DailyNewsPaper>(ds, DailyNewsPaperFromDataRow);
        }

	}
	
	
}
