﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class DescrepencyNewsFileRepositoryBase : Repository, IDescrepencyNewsFileRepositoryBase 
	{
        
        public DescrepencyNewsFileRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("DescrepencyNewsFileId",new SearchColumn(){Name="DescrepencyNewsFileId",Title="DescrepencyNewsFileId",SelectClause="DescrepencyNewsFileId",WhereClause="AllRecords.DescrepencyNewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DescrepencyNewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Title",new SearchColumn(){Name="Title",Title="Title",SelectClause="Title",WhereClause="AllRecords.Title",DataType="System.String",IsForeignColumn=false,PropertyName="Title",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Source",new SearchColumn(){Name="Source",Title="Source",SelectClause="Source",WhereClause="AllRecords.Source",DataType="System.String",IsForeignColumn=false,PropertyName="Source",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DescrepencyValue",new SearchColumn(){Name="DescrepencyValue",Title="DescrepencyValue",SelectClause="DescrepencyValue",WhereClause="AllRecords.DescrepencyValue",DataType="System.String",IsForeignColumn=false,PropertyName="DescrepencyValue",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DescrepencyType",new SearchColumn(){Name="DescrepencyType",Title="DescrepencyType",SelectClause="DescrepencyType",WhereClause="AllRecords.DescrepencyType",DataType="System.Int32?",IsForeignColumn=false,PropertyName="DescrepencyType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DiscrepencyStatusCode",new SearchColumn(){Name="DiscrepencyStatusCode",Title="DiscrepencyStatusCode",SelectClause="DiscrepencyStatusCode",WhereClause="AllRecords.DiscrepencyStatusCode",DataType="System.Int32?",IsForeignColumn=false,PropertyName="DiscrepencyStatusCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsInserted",new SearchColumn(){Name="IsInserted",Title="IsInserted",SelectClause="IsInserted",WhereClause="AllRecords.IsInserted",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsInserted",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RawNews",new SearchColumn(){Name="RawNews",Title="RawNews",SelectClause="RawNews",WhereClause="AllRecords.RawNews",DataType="System.String",IsForeignColumn=false,PropertyName="RawNews",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetDescrepencyNewsFileSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetDescrepencyNewsFileBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetDescrepencyNewsFileAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetDescrepencyNewsFileSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[DescrepencyNewsFile].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[DescrepencyNewsFile].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual DescrepencyNewsFile GetDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDescrepencyNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DescrepencyNewsFile] with (nolock)  where DescrepencyNewsFileId=@DescrepencyNewsFileId ";
			SqlParameter parameter=new SqlParameter("@DescrepencyNewsFileId",DescrepencyNewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return DescrepencyNewsFileFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<DescrepencyNewsFile> GetDescrepencyNewsFileByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDescrepencyNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [DescrepencyNewsFile] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DescrepencyNewsFile>(ds,DescrepencyNewsFileFromDataRow);
		}

		public virtual List<DescrepencyNewsFile> GetAllDescrepencyNewsFile(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDescrepencyNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DescrepencyNewsFile] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DescrepencyNewsFile>(ds, DescrepencyNewsFileFromDataRow);
		}

		public virtual List<DescrepencyNewsFile> GetPagedDescrepencyNewsFile(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetDescrepencyNewsFileCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [DescrepencyNewsFileId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([DescrepencyNewsFileId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [DescrepencyNewsFileId] ";
            tempsql += " FROM [DescrepencyNewsFile] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("DescrepencyNewsFileId"))
					tempsql += " , (AllRecords.[DescrepencyNewsFileId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[DescrepencyNewsFileId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetDescrepencyNewsFileSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [DescrepencyNewsFile] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [DescrepencyNewsFile].[DescrepencyNewsFileId] = PageIndex.[DescrepencyNewsFileId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DescrepencyNewsFile>(ds, DescrepencyNewsFileFromDataRow);
			}else{ return null;}
		}

		private int GetDescrepencyNewsFileCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM DescrepencyNewsFile as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM DescrepencyNewsFile as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(DescrepencyNewsFile))]
		public virtual DescrepencyNewsFile InsertDescrepencyNewsFile(DescrepencyNewsFile entity)
		{

			DescrepencyNewsFile other=new DescrepencyNewsFile();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into DescrepencyNewsFile ( [Title]
				,[Source]
				,[DescrepencyValue]
				,[DescrepencyType]
				,[CreationDate]
				,[LastUpdateDate]
				,[DiscrepencyStatusCode]
				,[IsInserted]
				,[RawNews]
				,[NewsFileId]
				,[CategoryId] )
				Values
				( @Title
				, @Source
				, @DescrepencyValue
				, @DescrepencyType
				, @CreationDate
				, @LastUpdateDate
				, @DiscrepencyStatusCode
				, @IsInserted
				, @RawNews
				, @NewsFileId
				, @CategoryId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@DescrepencyValue",entity.DescrepencyValue ?? (object)DBNull.Value)
					, new SqlParameter("@DescrepencyType",entity.DescrepencyType ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@DiscrepencyStatusCode",entity.DiscrepencyStatusCode ?? (object)DBNull.Value)
					, new SqlParameter("@IsInserted",entity.IsInserted ?? (object)DBNull.Value)
					, new SqlParameter("@RawNews",entity.RawNews ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetDescrepencyNewsFile(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(DescrepencyNewsFile))]
		public virtual DescrepencyNewsFile UpdateDescrepencyNewsFile(DescrepencyNewsFile entity)
		{

			if (entity.IsTransient()) return entity;
			DescrepencyNewsFile other = GetDescrepencyNewsFile(entity.DescrepencyNewsFileId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update DescrepencyNewsFile set  [Title]=@Title
							, [Source]=@Source
							, [DescrepencyValue]=@DescrepencyValue
							, [DescrepencyType]=@DescrepencyType
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [DiscrepencyStatusCode]=@DiscrepencyStatusCode
							, [IsInserted]=@IsInserted
							, [RawNews]=@RawNews
							, [NewsFileId]=@NewsFileId
							, [CategoryId]=@CategoryId 
							 where DescrepencyNewsFileId=@DescrepencyNewsFileId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@DescrepencyValue",entity.DescrepencyValue ?? (object)DBNull.Value)
					, new SqlParameter("@DescrepencyType",entity.DescrepencyType ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@DiscrepencyStatusCode",entity.DiscrepencyStatusCode ?? (object)DBNull.Value)
					, new SqlParameter("@IsInserted",entity.IsInserted ?? (object)DBNull.Value)
					, new SqlParameter("@RawNews",entity.RawNews ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@DescrepencyNewsFileId",entity.DescrepencyNewsFileId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetDescrepencyNewsFile(entity.DescrepencyNewsFileId);
		}

		public virtual bool DeleteDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId)
		{

			string sql="delete from DescrepencyNewsFile where DescrepencyNewsFileId=@DescrepencyNewsFileId";
			SqlParameter parameter=new SqlParameter("@DescrepencyNewsFileId",DescrepencyNewsFileId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(DescrepencyNewsFile))]
		public virtual DescrepencyNewsFile DeleteDescrepencyNewsFile(DescrepencyNewsFile entity)
		{
			this.DeleteDescrepencyNewsFile(entity.DescrepencyNewsFileId);
			return entity;
		}


		public virtual DescrepencyNewsFile DescrepencyNewsFileFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			DescrepencyNewsFile entity=new DescrepencyNewsFile();
			if (dr.Table.Columns.Contains("DescrepencyNewsFileId"))
			{
			entity.DescrepencyNewsFileId = (System.Int32)dr["DescrepencyNewsFileId"];
			}
			if (dr.Table.Columns.Contains("Title"))
			{
			entity.Title = dr["Title"].ToString();
			}
			if (dr.Table.Columns.Contains("Source"))
			{
			entity.Source = dr["Source"].ToString();
			}
			if (dr.Table.Columns.Contains("DescrepencyValue"))
			{
			entity.DescrepencyValue = dr["DescrepencyValue"].ToString();
			}
			if (dr.Table.Columns.Contains("DescrepencyType"))
			{
			entity.DescrepencyType = dr["DescrepencyType"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["DescrepencyType"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("DiscrepencyStatusCode"))
			{
			entity.DiscrepencyStatusCode = dr["DiscrepencyStatusCode"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["DiscrepencyStatusCode"];
			}
			if (dr.Table.Columns.Contains("IsInserted"))
			{
			entity.IsInserted = dr["IsInserted"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsInserted"];
			}
			if (dr.Table.Columns.Contains("RawNews"))
			{
			entity.RawNews = dr["RawNews"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = dr["NewsFileId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			return entity;
		}

	}
	
	
}
