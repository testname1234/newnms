﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class DescrepencyNewsFileRepository: DescrepencyNewsFileRepositoryBase, IDescrepencyNewsFileRepository
	{
        public virtual List<DescrepencyNewsFile> GetAllDescrepencyByStatus(int DescreypencyType)
        {
            string sql = "select * from DescrepencyNewsFile where descrepencytype =@DescreypencyType and DiscrepencyStatusCode=1";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@DescreypencyType", DescreypencyType)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<DescrepencyNewsFile>(ds, DescrepencyNewsFileFromDataRow);
        }

        public bool UpdateDescrepencyNewsByValue(int DescrepencyType, string DescrepencyValue)
        {
            string sql = "Update DescrepencyNewsFile set DiscrepencyStatusCode=2,LastUpdatedate=@LastUpdatedate where DescrepencyType = @DescrepencyType and DescrepencyValue=@DescrepencyValue";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@DescrepencyType", DescrepencyType)
                    , new SqlParameter("@DescrepencyValue",DescrepencyValue)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual List<DescrepencyNewsFile> GetAllByDescrepancyType(int DescreypencyType,string DescrepencyValue)
        {
            string sql = "select * from DescrepencyNewsFile where descrepencytype =@DescreypencyType and DescrepencyValue=@DescrepencyValue";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@DescreypencyType", DescreypencyType),
                     new SqlParameter("@DescrepencyValue", DescrepencyValue)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<DescrepencyNewsFile>(ds, DescrepencyNewsFileFromDataRow);
        }

        public bool UpdateNewsFileId(int id, int NewFileId)
        {
            string sql = "Update DescrepencyNewsFile set NewsFileId=@NewsFileId,LastUpdatedate=@LastUpdatedate where DescrepencyNewsFileId = @DescrepencyNewsFileId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@NewsFileId", NewFileId)
                    , new SqlParameter("@DescrepencyNewsFileId",id)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
        public virtual  List<DescrepencyNewsFile> GetDiscrepencyBystatus(int descrepancyTypeId, int Status)
        {
            string sql = "select * from DescrepencyNewsFile where descrepencytype =@DescreypencyType and DiscrepencyStatusCode=@DiscrepencyStatusCode";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@DescreypencyType", descrepancyTypeId),
                     new SqlParameter("@DiscrepencyStatusCode", Status)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<DescrepencyNewsFile>(ds, DescrepencyNewsFileFromDataRow);
        }

        public bool UpdateDescrepencyCategory(string id, int CategoryId)
        {
            string sql = "Update DescrepencyNewsFile set CategoryId=@CategoryId,LastUpdatedate=@LastUpdatedate where DescrepencyNewsFileId = @DescrepencyNewsFileId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@CategoryId", CategoryId)
                    , new SqlParameter("@DescrepencyNewsFileId",id)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public List<int> GetDescrepencyNewsFileIdByDescrepencyValue(string DescrepencyValue)
        {
            string sql = "select DescrepencyNewsFileId from DescrepencyNewsFile where DescrepencyValue=@DescrepencyValue";
            SqlParameter[] parameterArray = new SqlParameter[]{new SqlParameter("@DescrepencyValue", DescrepencyValue)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<int>(ds, ListOfIntFromDataRow);
        }

        public bool UpdateDescrepencyValueByDescrepencyNewsFileId(string DescrepencyNewsFileId, string DescrepencyValue)
        {
            string sql = "Update DescrepencyNewsFile set DescrepencyValue=@DescrepencyValue,LastUpdatedate=@LastUpdatedate where DescrepencyNewsFileId = @DescrepencyNewsFileId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@DescrepencyValue", DescrepencyValue)
                    , new SqlParameter("@DescrepencyNewsFileId",DescrepencyNewsFileId)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        
        public virtual int ListOfIntFromDataRow(DataRow dr)
        {
            if (dr == null) return 0;

            int id = 0;
            if (dr.Table.Columns.Contains("DescrepencyNewsFileId"))
            {
                id = (System.Int32)dr["DescrepencyNewsFileId"];
            }
            return id;
        }

    }
	
	
}
