﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class EditorialCommentRepositoryBase : Repository, IEditorialCommentRepositoryBase 
	{
        
        public EditorialCommentRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("EditorialCommentId",new SearchColumn(){Name="EditorialCommentId",Title="EditorialCommentId",SelectClause="EditorialCommentId",WhereClause="AllRecords.EditorialCommentId",DataType="System.Int32",IsForeignColumn=false,PropertyName="EditorialCommentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Comment",new SearchColumn(){Name="Comment",Title="Comment",SelectClause="Comment",WhereClause="AllRecords.Comment",DataType="System.String",IsForeignColumn=false,PropertyName="Comment",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EditorId",new SearchColumn(){Name="EditorId",Title="EditorId",SelectClause="EditorId",WhereClause="AllRecords.EditorId",DataType="System.Int32",IsForeignColumn=false,PropertyName="EditorId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotId",new SearchColumn(){Name="SlotId",Title="SlotId",SelectClause="SlotId",WhereClause="AllRecords.SlotId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlotId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetEditorialCommentSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetEditorialCommentBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetEditorialCommentAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetEditorialCommentSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[EditorialComment].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[EditorialComment].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual EditorialComment GetEditorialComment(System.Int32 EditorialCommentId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEditorialCommentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EditorialComment] with (nolock)  where EditorialCommentId=@EditorialCommentId ";
			SqlParameter parameter=new SqlParameter("@EditorialCommentId",EditorialCommentId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return EditorialCommentFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<EditorialComment> GetEditorialCommentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEditorialCommentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [EditorialComment] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EditorialComment>(ds,EditorialCommentFromDataRow);
		}

		public virtual List<EditorialComment> GetAllEditorialComment(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEditorialCommentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EditorialComment] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EditorialComment>(ds, EditorialCommentFromDataRow);
		}

		public virtual List<EditorialComment> GetPagedEditorialComment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetEditorialCommentCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [EditorialCommentId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([EditorialCommentId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [EditorialCommentId] ";
            tempsql += " FROM [EditorialComment] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("EditorialCommentId"))
					tempsql += " , (AllRecords.[EditorialCommentId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[EditorialCommentId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetEditorialCommentSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [EditorialComment] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [EditorialComment].[EditorialCommentId] = PageIndex.[EditorialCommentId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EditorialComment>(ds, EditorialCommentFromDataRow);
			}else{ return null;}
		}

		private int GetEditorialCommentCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM EditorialComment as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM EditorialComment as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(EditorialComment))]
		public virtual EditorialComment InsertEditorialComment(EditorialComment entity)
		{

			EditorialComment other=new EditorialComment();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into EditorialComment ( [Comment]
				,[EditorId]
				,[NewsFileId]
				,[IsActive]
				,[CreationDate]
				,[LastUpdateDate]
				,[SlotId]
				,[TickerId]
				,[LanguageCode] )
				Values
				( @Comment
				, @EditorId
				, @NewsFileId
				, @IsActive
				, @CreationDate
				, @LastUpdateDate
				, @SlotId
				, @TickerId
				, @LanguageCode );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Comment",entity.Comment ?? (object)DBNull.Value)
					, new SqlParameter("@EditorId",entity.EditorId)
					, new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@SlotId",entity.SlotId ?? (object)DBNull.Value)
					, new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetEditorialComment(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(EditorialComment))]
		public virtual EditorialComment UpdateEditorialComment(EditorialComment entity)
		{

			if (entity.IsTransient()) return entity;
			EditorialComment other = GetEditorialComment(entity.EditorialCommentId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update EditorialComment set  [Comment]=@Comment
							, [EditorId]=@EditorId
							, [NewsFileId]=@NewsFileId
							, [IsActive]=@IsActive
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [SlotId]=@SlotId
							, [TickerId]=@TickerId
							, [LanguageCode]=@LanguageCode 
							 where EditorialCommentId=@EditorialCommentId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Comment",entity.Comment ?? (object)DBNull.Value)
					, new SqlParameter("@EditorId",entity.EditorId)
					, new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@SlotId",entity.SlotId ?? (object)DBNull.Value)
					, new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@EditorialCommentId",entity.EditorialCommentId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetEditorialComment(entity.EditorialCommentId);
		}

		public virtual bool DeleteEditorialComment(System.Int32 EditorialCommentId)
		{

			string sql="delete from EditorialComment where EditorialCommentId=@EditorialCommentId";
			SqlParameter parameter=new SqlParameter("@EditorialCommentId",EditorialCommentId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(EditorialComment))]
		public virtual EditorialComment DeleteEditorialComment(EditorialComment entity)
		{
			this.DeleteEditorialComment(entity.EditorialCommentId);
			return entity;
		}


		public virtual EditorialComment EditorialCommentFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			EditorialComment entity=new EditorialComment();
			if (dr.Table.Columns.Contains("EditorialCommentId"))
			{
			entity.EditorialCommentId = (System.Int32)dr["EditorialCommentId"];
			}
			if (dr.Table.Columns.Contains("Comment"))
			{
			entity.Comment = dr["Comment"].ToString();
			}
			if (dr.Table.Columns.Contains("EditorId"))
			{
			entity.EditorId = (System.Int32)dr["EditorId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("SlotId"))
			{
			entity.SlotId = dr["SlotId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlotId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = dr["TickerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LanguageCode"];
			}
			return entity;
		}

	}
	
	
}
