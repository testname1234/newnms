﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class EditorialCommentRepository : EditorialCommentRepositoryBase, IEditorialCommentRepository
    {
        public List<EditorialComment> GetEditorialCommentByNewsFileId(int NewsFileId)
        {
            string sql = "select * from editorialcomment where newsfileId =@NewsFileId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@NewsFileId", NewsFileId)
                   };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<EditorialComment>(ds, EditorialCommentFromDataRow);
        }

        public List<EditorialComment> GetEditorialCommentBySlotId(int slotId)
        {
            string sql = "select * from editorialcomment where slotId =@slotId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@slotId", slotId)
                   };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<EditorialComment>(ds, EditorialCommentFromDataRow);
        }

        public List<EditorialComment> GetCommentsBySlotIdAndLastUpdateDate(DateTime lastUpdateDate, int slotId)
        {
            string sql = "select * from editorialcomment where slotId =@slotId and LastUpdateDate >@lastUpdateDate";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@slotId", slotId)
                     ,new SqlParameter("@lastUpdateDate", lastUpdateDate)
                   };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<EditorialComment>(ds, EditorialCommentFromDataRow);
        }

        public List<EditorialComment> GetEditorialCommentByTickerId(int tickerId, int languageId)
        {
            string sql = "select * from editorialcomment where TickerId =@TickerId and LanguageCode = @LanguageId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@TickerId", tickerId),
                      new SqlParameter("@LanguageId", languageId)
                   };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<EditorialComment>();
            return CollectionFromDataSet<EditorialComment>(ds, EditorialCommentFromDataRow);
        }
    }


}
