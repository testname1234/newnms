﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class EpisodePcrMosRepositoryBase : Repository, IEpisodePcrMosRepositoryBase 
	{
        
        public EpisodePcrMosRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("EpisodePcrMosId",new SearchColumn(){Name="EpisodePcrMosId",Title="EpisodePcrMosId",SelectClause="EpisodePcrMosId",WhereClause="AllRecords.EpisodePcrMosId",DataType="System.Int32",IsForeignColumn=false,PropertyName="EpisodePcrMosId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ItemName",new SearchColumn(){Name="ItemName",Title="ItemName",SelectClause="ItemName",WhereClause="AllRecords.ItemName",DataType="System.String",IsForeignColumn=false,PropertyName="ItemName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Label",new SearchColumn(){Name="Label",Title="Label",SelectClause="Label",WhereClause="AllRecords.Label",DataType="System.String",IsForeignColumn=false,PropertyName="Label",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Channel",new SearchColumn(){Name="Channel",Title="Channel",SelectClause="Channel",WhereClause="AllRecords.Channel",DataType="System.String",IsForeignColumn=false,PropertyName="Channel",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Videolayer",new SearchColumn(){Name="Videolayer",Title="Videolayer",SelectClause="Videolayer",WhereClause="AllRecords.Videolayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Videolayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Devicename",new SearchColumn(){Name="Devicename",Title="Devicename",SelectClause="Devicename",WhereClause="AllRecords.Devicename",DataType="System.String",IsForeignColumn=false,PropertyName="Devicename",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Delay",new SearchColumn(){Name="Delay",Title="Delay",SelectClause="Delay",WhereClause="AllRecords.Delay",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Delay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Allowgpi",new SearchColumn(){Name="Allowgpi",Title="Allowgpi",SelectClause="Allowgpi",WhereClause="AllRecords.Allowgpi",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Allowgpi",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Allowremotetriggering",new SearchColumn(){Name="Allowremotetriggering",Title="Allowremotetriggering",SelectClause="Allowremotetriggering",WhereClause="AllRecords.Allowremotetriggering",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Allowremotetriggering",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Remotetriggerid",new SearchColumn(){Name="Remotetriggerid",Title="Remotetriggerid",SelectClause="Remotetriggerid",WhereClause="AllRecords.Remotetriggerid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Remotetriggerid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Flashlayer",new SearchColumn(){Name="Flashlayer",Title="Flashlayer",SelectClause="Flashlayer",WhereClause="AllRecords.Flashlayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Flashlayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Invoke",new SearchColumn(){Name="Invoke",Title="Invoke",SelectClause="Invoke",WhereClause="AllRecords.Invoke",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Invoke",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Usestoreddata",new SearchColumn(){Name="Usestoreddata",Title="Usestoreddata",SelectClause="Usestoreddata",WhereClause="AllRecords.Usestoreddata",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Usestoreddata",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Useuppercasedata",new SearchColumn(){Name="Useuppercasedata",Title="Useuppercasedata",SelectClause="Useuppercasedata",WhereClause="AllRecords.Useuppercasedata",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Useuppercasedata",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Color",new SearchColumn(){Name="Color",Title="Color",SelectClause="Color",WhereClause="AllRecords.Color",DataType="System.String",IsForeignColumn=false,PropertyName="Color",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Transition",new SearchColumn(){Name="Transition",Title="Transition",SelectClause="Transition",WhereClause="AllRecords.Transition",DataType="System.String",IsForeignColumn=false,PropertyName="Transition",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Transitionduration",new SearchColumn(){Name="Transitionduration",Title="Transitionduration",SelectClause="Transitionduration",WhereClause="AllRecords.Transitionduration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Transitionduration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Tween",new SearchColumn(){Name="Tween",Title="Tween",SelectClause="Tween",WhereClause="AllRecords.Tween",DataType="System.String",IsForeignColumn=false,PropertyName="Tween",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Direction",new SearchColumn(){Name="Direction",Title="Direction",SelectClause="Direction",WhereClause="AllRecords.Direction",DataType="System.String",IsForeignColumn=false,PropertyName="Direction",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Seek",new SearchColumn(){Name="Seek",Title="Seek",SelectClause="Seek",WhereClause="AllRecords.Seek",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Seek",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Length",new SearchColumn(){Name="Length",Title="Length",SelectClause="Length",WhereClause="AllRecords.Length",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Length",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Loop",new SearchColumn(){Name="Loop",Title="Loop",SelectClause="Loop",WhereClause="AllRecords.Loop",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Loop",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Freezeonload",new SearchColumn(){Name="Freezeonload",Title="Freezeonload",SelectClause="Freezeonload",WhereClause="AllRecords.Freezeonload",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Freezeonload",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Triggeronnext",new SearchColumn(){Name="Triggeronnext",Title="Triggeronnext",SelectClause="Triggeronnext",WhereClause="AllRecords.Triggeronnext",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Triggeronnext",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Autoplay",new SearchColumn(){Name="Autoplay",Title="Autoplay",SelectClause="Autoplay",WhereClause="AllRecords.Autoplay",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Autoplay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Timecode",new SearchColumn(){Name="Timecode",Title="Timecode",SelectClause="Timecode",WhereClause="AllRecords.Timecode",DataType="System.String",IsForeignColumn=false,PropertyName="Timecode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Defer",new SearchColumn(){Name="Defer",Title="Defer",SelectClause="Defer",WhereClause="AllRecords.Defer",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Defer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Device",new SearchColumn(){Name="Device",Title="Device",SelectClause="Device",WhereClause="AllRecords.Device",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Device",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Format",new SearchColumn(){Name="Format",Title="Format",SelectClause="Format",WhereClause="AllRecords.Format",DataType="System.String",IsForeignColumn=false,PropertyName="Format",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Showmask",new SearchColumn(){Name="Showmask",Title="Showmask",SelectClause="Showmask",WhereClause="AllRecords.Showmask",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Showmask",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Blur",new SearchColumn(){Name="Blur",Title="Blur",SelectClause="Blur",WhereClause="AllRecords.Blur",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Blur",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Key",new SearchColumn(){Name="Key",Title="Key",SelectClause="Key",WhereClause="AllRecords.Key",DataType="System.String",IsForeignColumn=false,PropertyName="Key",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Spread",new SearchColumn(){Name="Spread",Title="Spread",SelectClause="Spread",WhereClause="AllRecords.Spread",DataType="System.Double?",IsForeignColumn=false,PropertyName="Spread",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Spill",new SearchColumn(){Name="Spill",Title="Spill",SelectClause="Spill",WhereClause="AllRecords.Spill",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Spill",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Threshold",new SearchColumn(){Name="Threshold",Title="Threshold",SelectClause="Threshold",WhereClause="AllRecords.Threshold",DataType="System.Double?",IsForeignColumn=false,PropertyName="Threshold",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("userType",new SearchColumn(){Name="userType",Title="userType",SelectClause="userType",WhereClause="AllRecords.userType",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("casperTransformationId",new SearchColumn(){Name="casperTransformationId",Title="casperTransformationId",SelectClause="casperTransformationId",WhereClause="AllRecords.casperTransformationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CasperTransformationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("flashtemplateid",new SearchColumn(){Name="flashtemplateid",Title="flashtemplateid",SelectClause="flashtemplateid",WhereClause="AllRecords.flashtemplateid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Flashtemplateid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("templateid",new SearchColumn(){Name="templateid",Title="templateid",SelectClause="templateid",WhereClause="AllRecords.templateid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Templateid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("flashtemplatewindowid",new SearchColumn(){Name="flashtemplatewindowid",Title="flashtemplatewindowid",SelectClause="flashtemplatewindowid",WhereClause="AllRecords.flashtemplatewindowid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Flashtemplatewindowid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashTemplateTypeId",new SearchColumn(){Name="FlashTemplateTypeId",Title="FlashTemplateTypeId",SelectClause="FlashTemplateTypeId",WhereClause="AllRecords.FlashTemplateTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashTemplateTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("videoid",new SearchColumn(){Name="videoid",Title="videoid",SelectClause="videoid",WhereClause="AllRecords.videoid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Videoid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("sequenceorder",new SearchColumn(){Name="sequenceorder",Title="sequenceorder",SelectClause="sequenceorder",WhereClause="AllRecords.sequenceorder",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Sequenceorder",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("casperTemplateId",new SearchColumn(){Name="casperTemplateId",Title="casperTemplateId",SelectClause="casperTemplateId",WhereClause="AllRecords.casperTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CasperTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CasperTemplatItemId",new SearchColumn(){Name="CasperTemplatItemId",Title="CasperTemplatItemId",SelectClause="CasperTemplatItemId",WhereClause="AllRecords.CasperTemplatItemId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CasperTemplatItemId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("parentid",new SearchColumn(){Name="parentid",Title="parentid",SelectClause="parentid",WhereClause="AllRecords.parentid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Parentid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("url",new SearchColumn(){Name="url",Title="url",SelectClause="url",WhereClause="AllRecords.url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("portnumber",new SearchColumn(){Name="portnumber",Title="portnumber",SelectClause="portnumber",WhereClause="AllRecords.portnumber",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Portnumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("flashTemplateGuid",new SearchColumn(){Name="flashTemplateGuid",Title="flashTemplateGuid",SelectClause="flashTemplateGuid",WhereClause="AllRecords.flashTemplateGuid",DataType="System.String",IsForeignColumn=false,PropertyName="FlashTemplateGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetEpisodePcrMosSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetEpisodePcrMosBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetEpisodePcrMosAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetEpisodePcrMosSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[EpisodePcrMos].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[EpisodePcrMos].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual EpisodePcrMos GetEpisodePcrMos(System.Int32 EpisodePcrMosId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEpisodePcrMosSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EpisodePcrMos] with (nolock)  where EpisodePcrMosId=@EpisodePcrMosId ";
			SqlParameter parameter=new SqlParameter("@EpisodePcrMosId",EpisodePcrMosId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return EpisodePcrMosFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<EpisodePcrMos> GetEpisodePcrMosByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEpisodePcrMosSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [EpisodePcrMos] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EpisodePcrMos>(ds,EpisodePcrMosFromDataRow);
		}

		public virtual List<EpisodePcrMos> GetAllEpisodePcrMos(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEpisodePcrMosSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EpisodePcrMos] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EpisodePcrMos>(ds, EpisodePcrMosFromDataRow);
		}

		public virtual List<EpisodePcrMos> GetPagedEpisodePcrMos(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetEpisodePcrMosCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [EpisodePcrMosId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([EpisodePcrMosId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [EpisodePcrMosId] ";
            tempsql += " FROM [EpisodePcrMos] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("EpisodePcrMosId"))
					tempsql += " , (AllRecords.[EpisodePcrMosId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[EpisodePcrMosId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetEpisodePcrMosSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [EpisodePcrMos] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [EpisodePcrMos].[EpisodePcrMosId] = PageIndex.[EpisodePcrMosId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EpisodePcrMos>(ds, EpisodePcrMosFromDataRow);
			}else{ return null;}
		}

		private int GetEpisodePcrMosCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM EpisodePcrMos as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM EpisodePcrMos as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(EpisodePcrMos))]
		public virtual EpisodePcrMos InsertEpisodePcrMos(EpisodePcrMos entity)
		{

			EpisodePcrMos other=new EpisodePcrMos();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into EpisodePcrMos ( [ItemName]
				,[Type]
				,[Label]
				,[Name]
				,[Channel]
				,[Videolayer]
				,[Devicename]
				,[Delay]
				,[Duration]
				,[Allowgpi]
				,[Allowremotetriggering]
				,[Remotetriggerid]
				,[Flashlayer]
				,[Invoke]
				,[Usestoreddata]
				,[Useuppercasedata]
				,[Color]
				,[Transition]
				,[Transitionduration]
				,[Tween]
				,[Direction]
				,[Seek]
				,[Length]
				,[Loop]
				,[Freezeonload]
				,[Triggeronnext]
				,[Autoplay]
				,[Timecode]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[Defer]
				,[Device]
				,[Format]
				,[Showmask]
				,[Blur]
				,[Key]
				,[Spread]
				,[Spill]
				,[Threshold]
				,[userType]
				,[casperTransformationId]
				,[flashtemplateid]
				,[templateid]
				,[flashtemplatewindowid]
				,[FlashTemplateTypeId]
				,[videoid]
				,[sequenceorder]
				,[casperTemplateId]
				,[CasperTemplatItemId]
				,[parentid]
				,[url]
				,[portnumber]
				,[flashTemplateGuid] )
				Values
				( @ItemName
				, @Type
				, @Label
				, @Name
				, @Channel
				, @Videolayer
				, @Devicename
				, @Delay
				, @Duration
				, @Allowgpi
				, @Allowremotetriggering
				, @Remotetriggerid
				, @Flashlayer
				, @Invoke
				, @Usestoreddata
				, @Useuppercasedata
				, @Color
				, @Transition
				, @Transitionduration
				, @Tween
				, @Direction
				, @Seek
				, @Length
				, @Loop
				, @Freezeonload
				, @Triggeronnext
				, @Autoplay
				, @Timecode
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @Defer
				, @Device
				, @Format
				, @Showmask
				, @Blur
				, @Key
				, @Spread
				, @Spill
				, @Threshold
				, @userType
				, @casperTransformationId
				, @flashtemplateid
				, @templateid
				, @flashtemplatewindowid
				, @FlashTemplateTypeId
				, @videoid
				, @sequenceorder
				, @casperTemplateId
				, @CasperTemplatItemId
				, @parentid
				, @url
				, @portnumber
				, @flashTemplateGuid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ItemName",entity.ItemName ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@Videolayer",entity.Videolayer ?? (object)DBNull.Value)
					, new SqlParameter("@Devicename",entity.Devicename ?? (object)DBNull.Value)
					, new SqlParameter("@Delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@Allowgpi",entity.Allowgpi ?? (object)DBNull.Value)
					, new SqlParameter("@Allowremotetriggering",entity.Allowremotetriggering ?? (object)DBNull.Value)
					, new SqlParameter("@Remotetriggerid",entity.Remotetriggerid ?? (object)DBNull.Value)
					, new SqlParameter("@Flashlayer",entity.Flashlayer ?? (object)DBNull.Value)
					, new SqlParameter("@Invoke",entity.Invoke ?? (object)DBNull.Value)
					, new SqlParameter("@Usestoreddata",entity.Usestoreddata ?? (object)DBNull.Value)
					, new SqlParameter("@Useuppercasedata",entity.Useuppercasedata ?? (object)DBNull.Value)
					, new SqlParameter("@Color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@Transition",entity.Transition ?? (object)DBNull.Value)
					, new SqlParameter("@Transitionduration",entity.Transitionduration ?? (object)DBNull.Value)
					, new SqlParameter("@Tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@Direction",entity.Direction ?? (object)DBNull.Value)
					, new SqlParameter("@Seek",entity.Seek ?? (object)DBNull.Value)
					, new SqlParameter("@Length",entity.Length ?? (object)DBNull.Value)
					, new SqlParameter("@Loop",entity.Loop ?? (object)DBNull.Value)
					, new SqlParameter("@Freezeonload",entity.Freezeonload ?? (object)DBNull.Value)
					, new SqlParameter("@Triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@Autoplay",entity.Autoplay ?? (object)DBNull.Value)
					, new SqlParameter("@Timecode",entity.Timecode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@Defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@Device",entity.Device ?? (object)DBNull.Value)
					, new SqlParameter("@Format",entity.Format ?? (object)DBNull.Value)
					, new SqlParameter("@Showmask",entity.Showmask ?? (object)DBNull.Value)
					, new SqlParameter("@Blur",entity.Blur ?? (object)DBNull.Value)
					, new SqlParameter("@Key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@Spread",entity.Spread ?? (object)DBNull.Value)
					, new SqlParameter("@Spill",entity.Spill ?? (object)DBNull.Value)
					, new SqlParameter("@Threshold",entity.Threshold ?? (object)DBNull.Value)
					, new SqlParameter("@userType",entity.UserType ?? (object)DBNull.Value)
					, new SqlParameter("@casperTransformationId",entity.CasperTransformationId ?? (object)DBNull.Value)
					, new SqlParameter("@flashtemplateid",entity.Flashtemplateid ?? (object)DBNull.Value)
					, new SqlParameter("@templateid",entity.Templateid ?? (object)DBNull.Value)
					, new SqlParameter("@flashtemplatewindowid",entity.Flashtemplatewindowid ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateTypeId",entity.FlashTemplateTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@videoid",entity.Videoid ?? (object)DBNull.Value)
					, new SqlParameter("@sequenceorder",entity.Sequenceorder ?? (object)DBNull.Value)
					, new SqlParameter("@casperTemplateId",entity.CasperTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@CasperTemplatItemId",entity.CasperTemplatItemId ?? (object)DBNull.Value)
					, new SqlParameter("@parentid",entity.Parentid ?? (object)DBNull.Value)
					, new SqlParameter("@url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@portnumber",entity.Portnumber ?? (object)DBNull.Value)
					, new SqlParameter("@flashTemplateGuid",entity.FlashTemplateGuid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetEpisodePcrMos(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(EpisodePcrMos))]
		public virtual EpisodePcrMos UpdateEpisodePcrMos(EpisodePcrMos entity)
		{

			if (entity.IsTransient()) return entity;
			EpisodePcrMos other = GetEpisodePcrMos(entity.EpisodePcrMosId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update EpisodePcrMos set  [ItemName]=@ItemName
							, [Type]=@Type
							, [Label]=@Label
							, [Name]=@Name
							, [Channel]=@Channel
							, [Videolayer]=@Videolayer
							, [Devicename]=@Devicename
							, [Delay]=@Delay
							, [Duration]=@Duration
							, [Allowgpi]=@Allowgpi
							, [Allowremotetriggering]=@Allowremotetriggering
							, [Remotetriggerid]=@Remotetriggerid
							, [Flashlayer]=@Flashlayer
							, [Invoke]=@Invoke
							, [Usestoreddata]=@Usestoreddata
							, [Useuppercasedata]=@Useuppercasedata
							, [Color]=@Color
							, [Transition]=@Transition
							, [Transitionduration]=@Transitionduration
							, [Tween]=@Tween
							, [Direction]=@Direction
							, [Seek]=@Seek
							, [Length]=@Length
							, [Loop]=@Loop
							, [Freezeonload]=@Freezeonload
							, [Triggeronnext]=@Triggeronnext
							, [Autoplay]=@Autoplay
							, [Timecode]=@Timecode
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [Defer]=@Defer
							, [Device]=@Device
							, [Format]=@Format
							, [Showmask]=@Showmask
							, [Blur]=@Blur
							, [Key]=@Key
							, [Spread]=@Spread
							, [Spill]=@Spill
							, [Threshold]=@Threshold
							, [userType]=@userType
							, [casperTransformationId]=@casperTransformationId
							, [flashtemplateid]=@flashtemplateid
							, [templateid]=@templateid
							, [flashtemplatewindowid]=@flashtemplatewindowid
							, [FlashTemplateTypeId]=@FlashTemplateTypeId
							, [videoid]=@videoid
							, [sequenceorder]=@sequenceorder
							, [casperTemplateId]=@casperTemplateId
							, [CasperTemplatItemId]=@CasperTemplatItemId
							, [parentid]=@parentid
							, [url]=@url
							, [portnumber]=@portnumber
							, [flashTemplateGuid]=@flashTemplateGuid 
							 where EpisodePcrMosId=@EpisodePcrMosId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ItemName",entity.ItemName ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@Videolayer",entity.Videolayer ?? (object)DBNull.Value)
					, new SqlParameter("@Devicename",entity.Devicename ?? (object)DBNull.Value)
					, new SqlParameter("@Delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@Allowgpi",entity.Allowgpi ?? (object)DBNull.Value)
					, new SqlParameter("@Allowremotetriggering",entity.Allowremotetriggering ?? (object)DBNull.Value)
					, new SqlParameter("@Remotetriggerid",entity.Remotetriggerid ?? (object)DBNull.Value)
					, new SqlParameter("@Flashlayer",entity.Flashlayer ?? (object)DBNull.Value)
					, new SqlParameter("@Invoke",entity.Invoke ?? (object)DBNull.Value)
					, new SqlParameter("@Usestoreddata",entity.Usestoreddata ?? (object)DBNull.Value)
					, new SqlParameter("@Useuppercasedata",entity.Useuppercasedata ?? (object)DBNull.Value)
					, new SqlParameter("@Color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@Transition",entity.Transition ?? (object)DBNull.Value)
					, new SqlParameter("@Transitionduration",entity.Transitionduration ?? (object)DBNull.Value)
					, new SqlParameter("@Tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@Direction",entity.Direction ?? (object)DBNull.Value)
					, new SqlParameter("@Seek",entity.Seek ?? (object)DBNull.Value)
					, new SqlParameter("@Length",entity.Length ?? (object)DBNull.Value)
					, new SqlParameter("@Loop",entity.Loop ?? (object)DBNull.Value)
					, new SqlParameter("@Freezeonload",entity.Freezeonload ?? (object)DBNull.Value)
					, new SqlParameter("@Triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@Autoplay",entity.Autoplay ?? (object)DBNull.Value)
					, new SqlParameter("@Timecode",entity.Timecode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@Defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@Device",entity.Device ?? (object)DBNull.Value)
					, new SqlParameter("@Format",entity.Format ?? (object)DBNull.Value)
					, new SqlParameter("@Showmask",entity.Showmask ?? (object)DBNull.Value)
					, new SqlParameter("@Blur",entity.Blur ?? (object)DBNull.Value)
					, new SqlParameter("@Key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@Spread",entity.Spread ?? (object)DBNull.Value)
					, new SqlParameter("@Spill",entity.Spill ?? (object)DBNull.Value)
					, new SqlParameter("@Threshold",entity.Threshold ?? (object)DBNull.Value)
					, new SqlParameter("@userType",entity.UserType ?? (object)DBNull.Value)
					, new SqlParameter("@casperTransformationId",entity.CasperTransformationId ?? (object)DBNull.Value)
					, new SqlParameter("@flashtemplateid",entity.Flashtemplateid ?? (object)DBNull.Value)
					, new SqlParameter("@templateid",entity.Templateid ?? (object)DBNull.Value)
					, new SqlParameter("@flashtemplatewindowid",entity.Flashtemplatewindowid ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateTypeId",entity.FlashTemplateTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@videoid",entity.Videoid ?? (object)DBNull.Value)
					, new SqlParameter("@sequenceorder",entity.Sequenceorder ?? (object)DBNull.Value)
					, new SqlParameter("@casperTemplateId",entity.CasperTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@CasperTemplatItemId",entity.CasperTemplatItemId ?? (object)DBNull.Value)
					, new SqlParameter("@parentid",entity.Parentid ?? (object)DBNull.Value)
					, new SqlParameter("@url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@portnumber",entity.Portnumber ?? (object)DBNull.Value)
					, new SqlParameter("@flashTemplateGuid",entity.FlashTemplateGuid ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodePcrMosId",entity.EpisodePcrMosId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetEpisodePcrMos(entity.EpisodePcrMosId);
		}

		public virtual bool DeleteEpisodePcrMos(System.Int32 EpisodePcrMosId)
		{

			string sql="delete from EpisodePcrMos where EpisodePcrMosId=@EpisodePcrMosId";
			SqlParameter parameter=new SqlParameter("@EpisodePcrMosId",EpisodePcrMosId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(EpisodePcrMos))]
		public virtual EpisodePcrMos DeleteEpisodePcrMos(EpisodePcrMos entity)
		{
			this.DeleteEpisodePcrMos(entity.EpisodePcrMosId);
			return entity;
		}


		public virtual EpisodePcrMos EpisodePcrMosFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			EpisodePcrMos entity=new EpisodePcrMos();
			if (dr.Table.Columns.Contains("EpisodePcrMosId"))
			{
			entity.EpisodePcrMosId = (System.Int32)dr["EpisodePcrMosId"];
			}
			if (dr.Table.Columns.Contains("ItemName"))
			{
			entity.ItemName = dr["ItemName"].ToString();
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"].ToString();
			}
			if (dr.Table.Columns.Contains("Label"))
			{
			entity.Label = dr["Label"].ToString();
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("Channel"))
			{
			entity.Channel = dr["Channel"].ToString();
			}
			if (dr.Table.Columns.Contains("Videolayer"))
			{
			entity.Videolayer = dr["Videolayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Videolayer"];
			}
			if (dr.Table.Columns.Contains("Devicename"))
			{
			entity.Devicename = dr["Devicename"].ToString();
			}
			if (dr.Table.Columns.Contains("Delay"))
			{
			entity.Delay = dr["Delay"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Delay"];
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("Allowgpi"))
			{
			entity.Allowgpi = dr["Allowgpi"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Allowgpi"];
			}
			if (dr.Table.Columns.Contains("Allowremotetriggering"))
			{
			entity.Allowremotetriggering = dr["Allowremotetriggering"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Allowremotetriggering"];
			}
			if (dr.Table.Columns.Contains("Remotetriggerid"))
			{
			entity.Remotetriggerid = dr["Remotetriggerid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Remotetriggerid"];
			}
			if (dr.Table.Columns.Contains("Flashlayer"))
			{
			entity.Flashlayer = dr["Flashlayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Flashlayer"];
			}
			if (dr.Table.Columns.Contains("Invoke"))
			{
			entity.Invoke = dr["Invoke"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Invoke"];
			}
			if (dr.Table.Columns.Contains("Usestoreddata"))
			{
			entity.Usestoreddata = dr["Usestoreddata"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Usestoreddata"];
			}
			if (dr.Table.Columns.Contains("Useuppercasedata"))
			{
			entity.Useuppercasedata = dr["Useuppercasedata"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Useuppercasedata"];
			}
			if (dr.Table.Columns.Contains("Color"))
			{
			entity.Color = dr["Color"].ToString();
			}
			if (dr.Table.Columns.Contains("Transition"))
			{
			entity.Transition = dr["Transition"].ToString();
			}
			if (dr.Table.Columns.Contains("Transitionduration"))
			{
			entity.Transitionduration = dr["Transitionduration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Transitionduration"];
			}
			if (dr.Table.Columns.Contains("Tween"))
			{
			entity.Tween = dr["Tween"].ToString();
			}
			if (dr.Table.Columns.Contains("Direction"))
			{
			entity.Direction = dr["Direction"].ToString();
			}
			if (dr.Table.Columns.Contains("Seek"))
			{
			entity.Seek = dr["Seek"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Seek"];
			}
			if (dr.Table.Columns.Contains("Length"))
			{
			entity.Length = dr["Length"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Length"];
			}
			if (dr.Table.Columns.Contains("Loop"))
			{
			entity.Loop = dr["Loop"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Loop"];
			}
			if (dr.Table.Columns.Contains("Freezeonload"))
			{
			entity.Freezeonload = dr["Freezeonload"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Freezeonload"];
			}
			if (dr.Table.Columns.Contains("Triggeronnext"))
			{
			entity.Triggeronnext = dr["Triggeronnext"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Triggeronnext"];
			}
			if (dr.Table.Columns.Contains("Autoplay"))
			{
			entity.Autoplay = dr["Autoplay"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Autoplay"];
			}
			if (dr.Table.Columns.Contains("Timecode"))
			{
			entity.Timecode = dr["Timecode"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("Defer"))
			{
			entity.Defer = dr["Defer"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Defer"];
			}
			if (dr.Table.Columns.Contains("Device"))
			{
			entity.Device = dr["Device"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Device"];
			}
			if (dr.Table.Columns.Contains("Format"))
			{
			entity.Format = dr["Format"].ToString();
			}
			if (dr.Table.Columns.Contains("Showmask"))
			{
			entity.Showmask = dr["Showmask"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Showmask"];
			}
			if (dr.Table.Columns.Contains("Blur"))
			{
			entity.Blur = dr["Blur"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Blur"];
			}
			if (dr.Table.Columns.Contains("Key"))
			{
			entity.Key = dr["Key"].ToString();
			}
			if (dr.Table.Columns.Contains("Spread"))
			{
			entity.Spread = dr["Spread"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Spread"];
			}
			if (dr.Table.Columns.Contains("Spill"))
			{
			entity.Spill = dr["Spill"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Spill"];
			}
			if (dr.Table.Columns.Contains("Threshold"))
			{
			entity.Threshold = dr["Threshold"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Threshold"];
			}
			if (dr.Table.Columns.Contains("userType"))
			{
			entity.UserType = dr["userType"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["userType"];
			}
			if (dr.Table.Columns.Contains("casperTransformationId"))
			{
			entity.CasperTransformationId = dr["casperTransformationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["casperTransformationId"];
			}
			if (dr.Table.Columns.Contains("flashtemplateid"))
			{
			entity.Flashtemplateid = dr["flashtemplateid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["flashtemplateid"];
			}
			if (dr.Table.Columns.Contains("templateid"))
			{
			entity.Templateid = dr["templateid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["templateid"];
			}
			if (dr.Table.Columns.Contains("flashtemplatewindowid"))
			{
			entity.Flashtemplatewindowid = dr["flashtemplatewindowid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["flashtemplatewindowid"];
			}
			if (dr.Table.Columns.Contains("FlashTemplateTypeId"))
			{
			entity.FlashTemplateTypeId = dr["FlashTemplateTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashTemplateTypeId"];
			}
			if (dr.Table.Columns.Contains("videoid"))
			{
			entity.Videoid = dr["videoid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["videoid"];
			}
			if (dr.Table.Columns.Contains("sequenceorder"))
			{
			entity.Sequenceorder = dr["sequenceorder"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["sequenceorder"];
			}
			if (dr.Table.Columns.Contains("casperTemplateId"))
			{
			entity.CasperTemplateId = dr["casperTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["casperTemplateId"];
			}
			if (dr.Table.Columns.Contains("CasperTemplatItemId"))
			{
			entity.CasperTemplatItemId = dr["CasperTemplatItemId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CasperTemplatItemId"];
			}
			if (dr.Table.Columns.Contains("parentid"))
			{
			entity.Parentid = dr["parentid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["parentid"];
			}
			if (dr.Table.Columns.Contains("url"))
			{
			entity.Url = dr["url"].ToString();
			}
			if (dr.Table.Columns.Contains("portnumber"))
			{
			entity.Portnumber = dr["portnumber"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["portnumber"];
			}
			if (dr.Table.Columns.Contains("flashTemplateGuid"))
			{
			entity.FlashTemplateGuid = dr["flashTemplateGuid"].ToString();
			}
			return entity;
		}

	}
	
	
}
