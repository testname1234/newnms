﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class EpisodePcrMosRepository: EpisodePcrMosRepositoryBase, IEpisodePcrMosRepository
	{
        public virtual List<EpisodePcrMos> GetEpisodePcrMosBycasperTemplateId(int casperTemplateId,int SlotScreenTemplateId, int SlotId, string SelectClause = null)
        {
            string sql = "Get_EpisodeMos";
            SqlParameter parameter1 = new SqlParameter("@casperTemplateId", casperTemplateId);
            SqlParameter parameter2 = new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId);
            SqlParameter parameter3 = new SqlParameter("@SlotId", SlotId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.StoredProcedure, sql, new SqlParameter[] { parameter1,parameter2,parameter3 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<EpisodePcrMos>(ds, EpisodePcrMosFromDataRow);
        }

	}
	
	
}
