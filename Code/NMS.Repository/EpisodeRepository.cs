﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Enums;

namespace NMS.Repository
{
    public partial class EpisodeRepository : EpisodeRepositoryBase, IEpisodeRepository
    {
        public List<Episode> GetEpisodeByProgramIdAndDate(int programId, DateTime date)
        {
            string sql = GetEpisodeSelectClause();
            sql += "from Episode with (nolock)  where ProgramId=@ProgramId and convert(date,[From])=convert(date, @Date)";
            SqlParameter parameter = new SqlParameter("@ProgramId", programId);
            SqlParameter parameter1 = new SqlParameter("@Date", date);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Episode>(ds, EpisodeFromDataRow);
        }

        public List<Episode> GetProgramEpisodeByDate(int userId, Core.Enums.TeamRoles teamRoles, DateTime dateTime, DateTime lastUpdateDate)
        {
            string sql = @"select distinct e.* from team t (nolock) inner join episode e (nolock) on e.ProgramId=t.ProgramId
                    inner join segment sg (nolock) on sg.episodeid=e.EpisodeId 
                    inner join slot s (nolock) on s.SegmentId=sg.SegmentId
                    inner join SlotScreenTemplate sst (nolock) on sst.SlotId=s.slotid
                     where t.userid=@userId and teamroleid=@teamroleid and ((@teamroleid=@nleteamroleid and sst.IsAssignedToNLE=1) or (@teamroleid=@storywriterteamroleid and sst.IsAssignedToStoryWriter=1))
                     and convert(date,e.[From])>convert(date,@Date) and sst.LastUpdateDate>@lastUpdateDate";
            SqlParameter[] parameters = new SqlParameter[]{
              new SqlParameter("@userId", userId),
              new SqlParameter("@nleteamroleid", (int)Core.Enums.TeamRoles.NLE),
              new SqlParameter("@storywriterteamroleid", (int)Core.Enums.TeamRoles.StoryWriter),
              new SqlParameter("@teamroleid", (int)teamRoles),
              new SqlParameter("@Date", dateTime),
            new SqlParameter("@lastUpdateDate", lastUpdateDate)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Episode>(ds, EpisodeFromDataRow);
        }

        public List<int> GetProgramSetMappingID(int EpisodeId)
        {
            List<int> lstSets = new List<int>();
            string sql = @"select p.SetId as SetMappingId from ProgramSetMapping p 
            inner join Episode e on p.ProgramId = e.ProgramId
            where e.EpisodeId = @EpisodeId and p.IsActive=1";
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@EpisodeId", EpisodeId)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count ==0 || ds.Tables[0].Rows.Count == 0) return null;  
            foreach(DataRow dr in ds.Tables[0].Rows)
            {
                lstSets.Add(Convert.ToInt32(dr[0]));
            }
            return lstSets;
        }

        public List<NMS.Core.DataTransfer.Episode.GetOutput> GetEpisodeDetailByProgram(List<int> lstprogramId, DateTime from, DateTime to)
        {
            
            string sql = "select ep.EpisodeId,p.Name,p.ProgramId, ep.[From],ep.[To],count(distinct s.SlotId) as 'TotalSlot',count(distinct ec.SlotId) as 'Done' from episode ep inner join Segment sg on ep.EpisodeId = sg.EpisodeId inner join program p on p.ProgramId = ep.ProgramId inner join slot s on s.SegmentId = sg.SegmentId left join EditorialComment ec on ec.slotid = s.slotid where ep.programid in (" + string.Join(",", lstprogramId) + " ) and ep.[From] > @from and ep.[From] < dateadd(day, 1, @to) group by ep.EpisodeId,p.Name,p.ProgramId, ep.[From],ep.[To]";
            //SqlParameter parameter = new SqlParameter("@ProgramId", lstprogramId);
            SqlParameter parameter1 = new SqlParameter("@from", from);
            SqlParameter parameter2 = new SqlParameter("@to", to);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            
            List<NMS.Core.DataTransfer.Episode.GetOutput> list = new List<NMS.Core.DataTransfer.Episode.GetOutput>(ds.Tables[0].Rows.Count);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                NMS.Core.DataTransfer.Episode.GetOutput obj = new Core.DataTransfer.Episode.GetOutput();
                obj.EpisodeId = dr.Field<int>("EpisodeId");
                obj.Name = dr.Field<string>("Name");
                obj.ProgramId = dr.Field<int>("ProgramId");
                obj.From = dr.Field<DateTime>("From");
                obj.To = dr.Field<DateTime>("To");
                obj.TotalSlot = dr.Field<int>("TotalSlot");
                obj.Done = dr.Field<int>("Done");
                list.Add(obj);
            }

            return list;
            
        }
        
        public List<Episode> GetProgramEpisodeByEpisodeId(int EpisodeId)
        {
            string sql = GetEpisodeSelectClause();
            sql += "from Episode with (nolock)  where EpisodeId=@EpisodeId";
            SqlParameter parameter = new SqlParameter("@EpisodeId", EpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Episode>(ds, EpisodeFromDataRow);
        }
        

        public Episode GetEpisodeBySlotId(System.Int32 SlotId)
        {
            string sql = @"select a.*
                        from
                        	Episode a inner join Segment b on a.EpisodeId = b.EpisodeId
                        	inner join Slot c on c.SegmentId = b.SegmentId
                        where
                        	SlotId = @SlotId";

            SqlParameter parameter = new SqlParameter("@SlotId", SlotId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return EpisodeFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Episode GetEpisodeBySegmentId(System.Int32 SegmentId)
        {
            string sql = @"select a.*
                        from
                        	Episode a inner join Segment b on a.EpisodeId = b.EpisodeId                        	
                            where b.SegmentId = @SegmentId";
            SqlParameter parameter = new SqlParameter("@SegmentId", SegmentId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return EpisodeFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Episode> GetEpisodesByProgramType(ProgramTypes programType, string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null)
        {
            string sql = string.Format(@"select distinct a.* 
    from 
        Episode a inner join Segment b (nolock) on isnull(a.EpisodeId,0) = isnull(b.EpisodeId,0)
        inner join Slot c (nolock) on isnull(b.SegmentId,0) = isnull(c.SegmentId,0)
    where 
            isnull(a.ProgramId,0) in (select isnull(t.Programid,0) from Team  t  (nolock)  where t.[UserId] = @UserId)
			and(c.[Title] like '%{0}%' OR c.[Description] like '%{0}%')
			and                                            (convert(date, a.[From]) between convert(date, @FromDate) and convert(date, @ToDate))
    order by a.[EpisodeId] desc
                                        OFFSET {1} ROWS
                                        FETCH NEXT {2} ROWS ONLY", searchTerm, pageIndex.ToString(), pageCount.ToString());

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@UserId", userId ?? (object)DBNull.Value),
                new SqlParameter("@SearchTerm", string.IsNullOrEmpty(searchTerm) ? (object)DBNull.Value : searchTerm),
                new SqlParameter("@FromDate", fromDate),
                new SqlParameter("@ToDate", toDate),
                new SqlParameter("@ProgramTypeId", (int)programType)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Episode>(ds, EpisodeFromDataRow);
        }

        public virtual Episode GetEpisodeWithProgramName(System.Int32 EpisodeId)
        {
            string sql = "select *,(select Name from Program where Program.ProgramId=[Episode].ProgramId) as ProgramName from [Episode] with (nolock)  where EpisodeId=@EpisodeId ";
            SqlParameter parameter = new SqlParameter("@EpisodeId", EpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return EpisodeFromDataRow(ds.Tables[0].Rows[0]);
        }

        public override Episode EpisodeFromDataRow(DataRow dr)
        {
            var entity = base.EpisodeFromDataRow(dr);
            if (dr.Table.Columns.Contains("ProgramName"))
            {
                entity.ProgramName = dr["ProgramName"] == DBNull.Value ? string.Empty : dr["ProgramName"].ToString();
            }
            return entity;
        }

    }
}
