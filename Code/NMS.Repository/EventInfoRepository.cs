﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;

namespace NMS.Repository
{

    public partial class EventInfoRepository : EventInfoRepositoryBase, IEventInfoRepository
    {
        public List<EventInfo> GetUpcommingEvents()
        {
            string sql = GetEventInfoSelectClause();
            sql += " from [EventInfo] with (nolock)  where CONVERT(date, StartTime) >= CONVERT(date, GETUTCDATE()) OR EndTime > GETUTCDATE(); ";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<EventInfo>(ds, EventInfoFromDataRow);
        }
    }


}
