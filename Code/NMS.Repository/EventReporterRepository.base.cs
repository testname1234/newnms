﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class EventReporterRepositoryBase : Repository, IEventReporterRepositoryBase 
	{
        
        public EventReporterRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("EventReporterId",new SearchColumn(){Name="EventReporterId",Title="EventReporterId",SelectClause="EventReporterId",WhereClause="AllRecords.EventReporterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="EventReporterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReporterId",new SearchColumn(){Name="ReporterId",Title="ReporterId",SelectClause="ReporterId",WhereClause="AllRecords.ReporterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ReporterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetEventReporterSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetEventReporterBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetEventReporterAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetEventReporterSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[EventReporter].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[EventReporter].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<EventReporter> GetEventReporterByNewsFileId(System.Int32? NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventReporterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EventReporter] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventReporter>(ds,EventReporterFromDataRow);
		}

		public virtual EventReporter GetEventReporter(System.Int32 EventReporterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventReporterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EventReporter] with (nolock)  where EventReporterId=@EventReporterId ";
			SqlParameter parameter=new SqlParameter("@EventReporterId",EventReporterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return EventReporterFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<EventReporter> GetEventReporterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventReporterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [EventReporter] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventReporter>(ds,EventReporterFromDataRow);
		}

		public virtual List<EventReporter> GetAllEventReporter(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventReporterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EventReporter] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventReporter>(ds, EventReporterFromDataRow);
		}

		public virtual List<EventReporter> GetPagedEventReporter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetEventReporterCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [EventReporterId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([EventReporterId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [EventReporterId] ";
            tempsql += " FROM [EventReporter] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("EventReporterId"))
					tempsql += " , (AllRecords.[EventReporterId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[EventReporterId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetEventReporterSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [EventReporter] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [EventReporter].[EventReporterId] = PageIndex.[EventReporterId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventReporter>(ds, EventReporterFromDataRow);
			}else{ return null;}
		}

		private int GetEventReporterCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM EventReporter as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM EventReporter as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(EventReporter))]
		public virtual EventReporter InsertEventReporter(EventReporter entity)
		{

			EventReporter other=new EventReporter();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into EventReporter ( [ReporterId]
				,[NewsFileId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @ReporterId
				, @NewsFileId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ReporterId",entity.ReporterId)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetEventReporter(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(EventReporter))]
		public virtual EventReporter UpdateEventReporter(EventReporter entity)
		{

			if (entity.IsTransient()) return entity;
			EventReporter other = GetEventReporter(entity.EventReporterId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update EventReporter set  [ReporterId]=@ReporterId
							, [NewsFileId]=@NewsFileId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where EventReporterId=@EventReporterId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ReporterId",entity.ReporterId)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@EventReporterId",entity.EventReporterId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetEventReporter(entity.EventReporterId);
		}

		public virtual bool DeleteEventReporter(System.Int32 EventReporterId)
		{

			string sql="delete from EventReporter where EventReporterId=@EventReporterId";
			SqlParameter parameter=new SqlParameter("@EventReporterId",EventReporterId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(EventReporter))]
		public virtual EventReporter DeleteEventReporter(EventReporter entity)
		{
			this.DeleteEventReporter(entity.EventReporterId);
			return entity;
		}


		public virtual EventReporter EventReporterFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			EventReporter entity=new EventReporter();
			if (dr.Table.Columns.Contains("EventReporterId"))
			{
			entity.EventReporterId = (System.Int32)dr["EventReporterId"];
			}
			if (dr.Table.Columns.Contains("ReporterId"))
			{
			entity.ReporterId = (System.Int32)dr["ReporterId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = dr["NewsFileId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
