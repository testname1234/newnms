﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class EventReporterRepository: EventReporterRepositoryBase, IEventReporterRepository
	{   
        public virtual bool DeleteEventReporterByNewsFileId(System.Int32 NewsFileId)
        {
            string sql = "delete from EventReporter where NewsFileId=@Id";
            SqlParameter parameter = new SqlParameter("@Id", NewsFileId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

    }
	
	
}
