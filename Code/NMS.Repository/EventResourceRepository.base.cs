﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class EventResourceRepositoryBase : Repository, IEventResourceRepositoryBase 
	{
        
        public EventResourceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("EventResourceId",new SearchColumn(){Name="EventResourceId",Title="EventResourceId",SelectClause="EventResourceId",WhereClause="AllRecords.EventResourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="EventResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EventResourceType",new SearchColumn(){Name="EventResourceType",Title="EventResourceType",SelectClause="EventResourceType",WhereClause="AllRecords.EventResourceType",DataType="System.Int32",IsForeignColumn=false,PropertyName="EventResourceType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetEventResourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetEventResourceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetEventResourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetEventResourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[EventResource].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[EventResource].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<EventResource> GetEventResourceByNewsFileId(System.Int32? NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EventResource] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventResource>(ds,EventResourceFromDataRow);
		}

		public virtual EventResource GetEventResource(System.Int32 EventResourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EventResource] with (nolock)  where EventResourceId=@EventResourceId ";
			SqlParameter parameter=new SqlParameter("@EventResourceId",EventResourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return EventResourceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<EventResource> GetEventResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [EventResource] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventResource>(ds,EventResourceFromDataRow);
		}

		public virtual List<EventResource> GetAllEventResource(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetEventResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [EventResource] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventResource>(ds, EventResourceFromDataRow);
		}

		public virtual List<EventResource> GetPagedEventResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetEventResourceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [EventResourceId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([EventResourceId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [EventResourceId] ";
            tempsql += " FROM [EventResource] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("EventResourceId"))
					tempsql += " , (AllRecords.[EventResourceId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[EventResourceId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetEventResourceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [EventResource] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [EventResource].[EventResourceId] = PageIndex.[EventResourceId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<EventResource>(ds, EventResourceFromDataRow);
			}else{ return null;}
		}

		private int GetEventResourceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM EventResource as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM EventResource as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(EventResource))]
		public virtual EventResource InsertEventResource(EventResource entity)
		{

			EventResource other=new EventResource();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into EventResource ( [EventResourceType]
				,[NewsFileId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @EventResourceType
				, @NewsFileId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@EventResourceType",entity.EventResourceType)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetEventResource(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(EventResource))]
		public virtual EventResource UpdateEventResource(EventResource entity)
		{

			if (entity.IsTransient()) return entity;
			EventResource other = GetEventResource(entity.EventResourceId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update EventResource set  [EventResourceType]=@EventResourceType
							, [NewsFileId]=@NewsFileId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where EventResourceId=@EventResourceId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@EventResourceType",entity.EventResourceType)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@EventResourceId",entity.EventResourceId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetEventResource(entity.EventResourceId);
		}

		public virtual bool DeleteEventResource(System.Int32 EventResourceId)
		{

			string sql="delete from EventResource where EventResourceId=@EventResourceId";
			SqlParameter parameter=new SqlParameter("@EventResourceId",EventResourceId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(EventResource))]
		public virtual EventResource DeleteEventResource(EventResource entity)
		{
			this.DeleteEventResource(entity.EventResourceId);
			return entity;
		}


		public virtual EventResource EventResourceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			EventResource entity=new EventResource();
			if (dr.Table.Columns.Contains("EventResourceId"))
			{
			entity.EventResourceId = (System.Int32)dr["EventResourceId"];
			}
			if (dr.Table.Columns.Contains("EventResourceType"))
			{
			entity.EventResourceType = (System.Int32)dr["EventResourceType"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = dr["NewsFileId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
