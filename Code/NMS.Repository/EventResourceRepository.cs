﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class EventResourceRepository: EventResourceRepositoryBase, IEventResourceRepository
	{
        public virtual bool DeleteEventResourceByNewsFileId(System.Int32 Id)
        {
            string sql = "delete from EventResource where NewsFileId=@Id";
            SqlParameter parameter = new SqlParameter("@Id", Id);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
    }
	
	
}
