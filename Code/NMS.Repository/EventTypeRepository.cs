﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class EventTypeRepository: EventTypeRepositoryBase, IEventTypeRepository
	{
        public List<EventType> GetEventTypeByTerm(string term)
        {
            string sql = GetEventTypeSelectClause();
            sql += "from EventType with (nolock) where Name like '%'+@Name+'%' and ISNULL(IsActive,0) = 1";
            SqlParameter parameter = new SqlParameter("@Name", term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<EventType>(ds, EventTypeFromDataRow);
        }

        public EventType GetEventTypeByExactTerm(string Name)
        {   
            string sql = "select * from [EventType] with (nolock) where Name=@Name ";
            SqlParameter parameter = new SqlParameter("@Name", Name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return EventTypeFromDataRow(ds.Tables[0].Rows[0]);
        }

        
    }
	
	
}
