﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Models;
using System.Configuration;

namespace NMS.Repository
{
    public static class ExceptionExtention
    {
        public static void Log(this Exception exp)
        {
            string sql = @"Insert into ELMAH_Error ( [Application],[Host],[Type],[Source],[Message],[User],[StatusCode],[TimeUtc],[AllXml])
                                                                Values
                                                                ( @Application, @Host, @Type, @Source, @Message, @User, 0, @TimeUtc, @AllXml);
                                                                Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
                                                                                 new SqlParameter("@Application","/LM/W3SVC/1/ROOT")
                                                                                , new SqlParameter("@Host",System.Environment.MachineName)
                , new SqlParameter("@Type",exp.GetType().ToString())
                , new SqlParameter("@Source",string.IsNullOrEmpty(ConfigurationManager.AppSettings["SystemProcess"])?System.Environment.MachineName:ConfigurationManager.AppSettings["SystemProcess"])
                , new SqlParameter("@Message",exp.Message)
                , new SqlParameter("@User",System.Environment.MachineName)
                , new SqlParameter("@TimeUtc",DateTime.UtcNow)
                , new SqlParameter("@AllXml",exp.StackTrace)
                };
            SqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["ElmahConnectionString"].ToString(), CommandType.Text, sql, parameterArray);
        }
    }
}
