﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FacebookCredentialRepositoryBase : Repository, IFacebookCredentialRepositoryBase 
	{
        
        public FacebookCredentialRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FacebookCredentialID",new SearchColumn(){Name="FacebookCredentialID",Title="FacebookCredentialID",SelectClause="FacebookCredentialID",WhereClause="AllRecords.FacebookCredentialID",DataType="System.Int32",IsForeignColumn=false,PropertyName="FacebookCredentialId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ClientID",new SearchColumn(){Name="ClientID",Title="ClientID",SelectClause="ClientID",WhereClause="AllRecords.ClientID",DataType="System.String",IsForeignColumn=false,PropertyName="ClientId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ClientSecret",new SearchColumn(){Name="ClientSecret",Title="ClientSecret",SelectClause="ClientSecret",WhereClause="AllRecords.ClientSecret",DataType="System.String",IsForeignColumn=false,PropertyName="ClientSecret",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AccessToken",new SearchColumn(){Name="AccessToken",Title="AccessToken",SelectClause="AccessToken",WhereClause="AllRecords.AccessToken",DataType="System.String",IsForeignColumn=false,PropertyName="AccessToken",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFacebookCredentialSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFacebookCredentialBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFacebookCredentialAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFacebookCredentialSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[FacebookCredential].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[FacebookCredential].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual FacebookCredential GetFacebookCredential(System.Int32 FacebookCredentialId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFacebookCredentialSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FacebookCredential] with (nolock)  where FacebookCredentialID=@FacebookCredentialID ";
			SqlParameter parameter=new SqlParameter("@FacebookCredentialID",FacebookCredentialId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FacebookCredentialFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<FacebookCredential> GetFacebookCredentialByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFacebookCredentialSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [FacebookCredential] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FacebookCredential>(ds,FacebookCredentialFromDataRow);
		}

		public virtual List<FacebookCredential> GetAllFacebookCredential(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFacebookCredentialSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FacebookCredential] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FacebookCredential>(ds, FacebookCredentialFromDataRow);
		}

		public virtual List<FacebookCredential> GetPagedFacebookCredential(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFacebookCredentialCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FacebookCredentialID] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FacebookCredentialID])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FacebookCredentialID] ";
            tempsql += " FROM [FacebookCredential] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FacebookCredentialID"))
					tempsql += " , (AllRecords.[FacebookCredentialID])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FacebookCredentialID])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFacebookCredentialSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [FacebookCredential] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [FacebookCredential].[FacebookCredentialID] = PageIndex.[FacebookCredentialID] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FacebookCredential>(ds, FacebookCredentialFromDataRow);
			}else{ return null;}
		}

		private int GetFacebookCredentialCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM FacebookCredential as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM FacebookCredential as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(FacebookCredential))]
		public virtual FacebookCredential InsertFacebookCredential(FacebookCredential entity)
		{

			FacebookCredential other=new FacebookCredential();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into FacebookCredential ( [ClientID]
				,[ClientSecret]
				,[AccessToken]
				,[LastUpdateDate] )
				Values
				( @ClientID
				, @ClientSecret
				, @AccessToken
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ClientID",entity.ClientId ?? (object)DBNull.Value)
					, new SqlParameter("@ClientSecret",entity.ClientSecret ?? (object)DBNull.Value)
					, new SqlParameter("@AccessToken",entity.AccessToken ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFacebookCredential(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(FacebookCredential))]
		public virtual FacebookCredential UpdateFacebookCredential(FacebookCredential entity)
		{

			if (entity.IsTransient()) return entity;
			FacebookCredential other = GetFacebookCredential(entity.FacebookCredentialId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update FacebookCredential set  [ClientID]=@ClientID
							, [ClientSecret]=@ClientSecret
							, [AccessToken]=@AccessToken
							, [LastUpdateDate]=@LastUpdateDate 
							 where FacebookCredentialID=@FacebookCredentialID";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ClientID",entity.ClientId ?? (object)DBNull.Value)
					, new SqlParameter("@ClientSecret",entity.ClientSecret ?? (object)DBNull.Value)
					, new SqlParameter("@AccessToken",entity.AccessToken ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@FacebookCredentialID",entity.FacebookCredentialId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFacebookCredential(entity.FacebookCredentialId);
		}

		public virtual bool DeleteFacebookCredential(System.Int32 FacebookCredentialId)
		{

			string sql="delete from FacebookCredential where FacebookCredentialID=@FacebookCredentialID";
			SqlParameter parameter=new SqlParameter("@FacebookCredentialID",FacebookCredentialId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(FacebookCredential))]
		public virtual FacebookCredential DeleteFacebookCredential(FacebookCredential entity)
		{
			this.DeleteFacebookCredential(entity.FacebookCredentialId);
			return entity;
		}


		public virtual FacebookCredential FacebookCredentialFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			FacebookCredential entity=new FacebookCredential();
			if (dr.Table.Columns.Contains("FacebookCredentialID"))
			{
			entity.FacebookCredentialId = (System.Int32)dr["FacebookCredentialID"];
			}
			if (dr.Table.Columns.Contains("ClientID"))
			{
			entity.ClientId = dr["ClientID"].ToString();
			}
			if (dr.Table.Columns.Contains("ClientSecret"))
			{
			entity.ClientSecret = dr["ClientSecret"].ToString();
			}
			if (dr.Table.Columns.Contains("AccessToken"))
			{
			entity.AccessToken = dr["AccessToken"].ToString();
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
