﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class FacebookCredentialRepository: FacebookCredentialRepositoryBase, IFacebookCredentialRepository
	{


        public void UdpateAccessToken(string AccessToken, DateTime LastUpdateDate, int CredentialID )
        {
            string sql = "update facebookcredential set accesstoken = @AccessToken ,  LastUpdateDate = @LastUpdateDate where FacebookCredentialID = @CredentialID";
            SqlParameter parameter1 = new SqlParameter("@Accesstoken", AccessToken);
            SqlParameter parameter2 = new SqlParameter("@LastUpdateDate", LastUpdateDate);
            SqlParameter parameter3 = new SqlParameter("@CredentialID", CredentialID);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2, parameter3 });
        }

        public string GetAccessToken()
        {
            string sql = "select accesstoken from facebookcredential";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if(ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ds.Tables[0].Rows[0][0].ToString(); 
        }
		
	}
	
	
}
