﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FileCategoryRepositoryBase : Repository, IFileCategoryRepositoryBase 
	{
        
        public FileCategoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FileCategoryId",new SearchColumn(){Name="FileCategoryId",Title="FileCategoryId",SelectClause="FileCategoryId",WhereClause="AllRecords.FileCategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FileCategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFileCategorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFileCategoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFileCategoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFileCategorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[FileCategory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[FileCategory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<FileCategory> GetFileCategoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileCategory] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileCategory>(ds,FileCategoryFromDataRow);
		}

		public virtual List<FileCategory> GetFileCategoryByCategoryId(System.Int32 CategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileCategory] with (nolock)  where CategoryId=@CategoryId  ";
			SqlParameter parameter=new SqlParameter("@CategoryId",CategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileCategory>(ds,FileCategoryFromDataRow);
		}

		public virtual FileCategory GetFileCategory(System.Int32 FileCategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileCategory] with (nolock)  where FileCategoryId=@FileCategoryId ";
			SqlParameter parameter=new SqlParameter("@FileCategoryId",FileCategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FileCategoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<FileCategory> GetFileCategoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [FileCategory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileCategory>(ds,FileCategoryFromDataRow);
		}

		public virtual List<FileCategory> GetAllFileCategory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileCategory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileCategory>(ds, FileCategoryFromDataRow);
		}

		public virtual List<FileCategory> GetPagedFileCategory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFileCategoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FileCategoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FileCategoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FileCategoryId] ";
            tempsql += " FROM [FileCategory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FileCategoryId"))
					tempsql += " , (AllRecords.[FileCategoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FileCategoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFileCategorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [FileCategory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [FileCategory].[FileCategoryId] = PageIndex.[FileCategoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileCategory>(ds, FileCategoryFromDataRow);
			}else{ return null;}
		}

		private int GetFileCategoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM FileCategory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM FileCategory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(FileCategory))]
		public virtual FileCategory InsertFileCategory(FileCategory entity)
		{

			FileCategory other=new FileCategory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into FileCategory ( [NewsFileId]
				,[CategoryId]
				,[CreationDate] )
				Values
				( @NewsFileId
				, @CategoryId
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFileCategory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(FileCategory))]
		public virtual FileCategory UpdateFileCategory(FileCategory entity)
		{

			if (entity.IsTransient()) return entity;
			FileCategory other = GetFileCategory(entity.FileCategoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update FileCategory set  [NewsFileId]=@NewsFileId
							, [CategoryId]=@CategoryId
							, [CreationDate]=@CreationDate 
							 where FileCategoryId=@FileCategoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@FileCategoryId",entity.FileCategoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFileCategory(entity.FileCategoryId);
		}

		public virtual bool DeleteFileCategory(System.Int32 FileCategoryId)
		{

			string sql="delete from FileCategory where FileCategoryId=@FileCategoryId";
			SqlParameter parameter=new SqlParameter("@FileCategoryId",FileCategoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(FileCategory))]
		public virtual FileCategory DeleteFileCategory(FileCategory entity)
		{
			this.DeleteFileCategory(entity.FileCategoryId);
			return entity;
		}


		public virtual FileCategory FileCategoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			FileCategory entity=new FileCategory();
			if (dr.Table.Columns.Contains("FileCategoryId"))
			{
			entity.FileCategoryId = (System.Int32)dr["FileCategoryId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = (System.Int32)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
