﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FileDetailRepositoryBase : Repository, IFileDetailRepositoryBase 
	{
        
        public FileDetailRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FileDetailId",new SearchColumn(){Name="FileDetailId",Title="FileDetailId",SelectClause="FileDetailId",WhereClause="AllRecords.FileDetailId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FileDetailId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedBy",new SearchColumn(){Name="CreatedBy",Title="CreatedBy",SelectClause="CreatedBy",WhereClause="AllRecords.CreatedBy",DataType="System.Int32",IsForeignColumn=false,PropertyName="CreatedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReportedBy",new SearchColumn(){Name="ReportedBy",Title="ReportedBy",SelectClause="ReportedBy",WhereClause="AllRecords.ReportedBy",DataType="System.Int32",IsForeignColumn=false,PropertyName="ReportedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Slug",new SearchColumn(){Name="Slug",Title="Slug",SelectClause="Slug",WhereClause="AllRecords.Slug",DataType="System.String",IsForeignColumn=false,PropertyName="Slug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CGValues",new SearchColumn(){Name="CGValues",Title="CGValues",SelectClause="CGValues",WhereClause="AllRecords.CGValues",DataType="System.String",IsForeignColumn=false,PropertyName="CgValues",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlugId",new SearchColumn(){Name="SlugId",Title="SlugId",SelectClause="SlugId",WhereClause="AllRecords.SlugId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlugId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Title",new SearchColumn(){Name="Title",Title="Title",SelectClause="Title",WhereClause="AllRecords.Title",DataType="System.String",IsForeignColumn=false,PropertyName="Title",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsDate",new SearchColumn(){Name="NewsDate",Title="NewsDate",SelectClause="NewsDate",WhereClause="AllRecords.NewsDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="NewsDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DescriptionText",new SearchColumn(){Name="DescriptionText",Title="DescriptionText",SelectClause="DescriptionText",WhereClause="AllRecords.DescriptionText",DataType="System.String",IsForeignColumn=false,PropertyName="DescriptionText",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFileDetailSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFileDetailBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFileDetailAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFileDetailSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[FileDetail].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[FileDetail].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<FileDetail> GetFileDetailByNewsFileId(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileDetail] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileDetail>(ds,FileDetailFromDataRow);
		}

		public virtual List<FileDetail> GetFileDetailBySlugId(System.Int32? SlugId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileDetail] with (nolock)  where SlugId=@SlugId  ";
			SqlParameter parameter=new SqlParameter("@SlugId",SlugId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileDetail>(ds,FileDetailFromDataRow);
		}

		public virtual FileDetail GetFileDetail(System.Int32 FileDetailId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileDetail] with (nolock)  where FileDetailId=@FileDetailId ";
			SqlParameter parameter=new SqlParameter("@FileDetailId",FileDetailId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FileDetailFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<FileDetail> GetFileDetailByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [FileDetail] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileDetail>(ds,FileDetailFromDataRow);
		}

		public virtual List<FileDetail> GetAllFileDetail(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileDetailSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileDetail] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileDetail>(ds, FileDetailFromDataRow);
		}

		public virtual List<FileDetail> GetPagedFileDetail(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFileDetailCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FileDetailId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FileDetailId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FileDetailId] ";
            tempsql += " FROM [FileDetail] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FileDetailId"))
					tempsql += " , (AllRecords.[FileDetailId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FileDetailId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFileDetailSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [FileDetail] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [FileDetail].[FileDetailId] = PageIndex.[FileDetailId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileDetail>(ds, FileDetailFromDataRow);
			}else{ return null;}
		}

		private int GetFileDetailCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM FileDetail as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM FileDetail as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(FileDetail))]
		public virtual FileDetail InsertFileDetail(FileDetail entity)
		{

			FileDetail other=new FileDetail();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into FileDetail ( [NewsFileId]
				,[Text]
				,[CreatedBy]
				,[CreationDate]
				,[ReportedBy]
				,[LastUpdateDate]
				,[Slug]
				,[CGValues]
				,[SlugId]
				,[Title]
				,[NewsDate]
				,[DescriptionText] )
				Values
				( @NewsFileId
				, @Text
				, @CreatedBy
				, @CreationDate
				, @ReportedBy
				, @LastUpdateDate
				, @Slug
				, @CGValues
				, @SlugId
				, @Title
				, @NewsDate
				, @DescriptionText );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@ReportedBy",entity.ReportedBy)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@CGValues",entity.CgValues ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@NewsDate",entity.NewsDate ?? (object)DBNull.Value)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFileDetail(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(FileDetail))]
		public virtual FileDetail UpdateFileDetail(FileDetail entity)
		{

			if (entity.IsTransient()) return entity;
			FileDetail other = GetFileDetail(entity.FileDetailId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update FileDetail set  [NewsFileId]=@NewsFileId
							, [Text]=@Text
							, [CreationDate]=@CreationDate
							, [ReportedBy]=@ReportedBy
							, [LastUpdateDate]=@LastUpdateDate
							, [Slug]=@Slug
							, [CGValues]=@CGValues
							, [SlugId]=@SlugId
							, [Title]=@Title
							, [NewsDate]=@NewsDate
							, [DescriptionText]=@DescriptionText 
							 where FileDetailId=@FileDetailId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@ReportedBy",entity.ReportedBy)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@CGValues",entity.CgValues ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@NewsDate",entity.NewsDate ?? (object)DBNull.Value)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)
					, new SqlParameter("@FileDetailId",entity.FileDetailId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFileDetail(entity.FileDetailId);
		}

		public virtual bool DeleteFileDetail(System.Int32 FileDetailId)
		{

			string sql="delete from FileDetail where FileDetailId=@FileDetailId";
			SqlParameter parameter=new SqlParameter("@FileDetailId",FileDetailId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(FileDetail))]
		public virtual FileDetail DeleteFileDetail(FileDetail entity)
		{
			this.DeleteFileDetail(entity.FileDetailId);
			return entity;
		}


		public virtual FileDetail FileDetailFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			FileDetail entity=new FileDetail();
			if (dr.Table.Columns.Contains("FileDetailId"))
			{
			entity.FileDetailId = (System.Int32)dr["FileDetailId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("CreatedBy"))
			{
			entity.CreatedBy = (System.Int32)dr["CreatedBy"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("ReportedBy"))
			{
			entity.ReportedBy = (System.Int32)dr["ReportedBy"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("Slug"))
			{
			entity.Slug = dr["Slug"].ToString();
			}
			if (dr.Table.Columns.Contains("CGValues"))
			{
			entity.CgValues = dr["CGValues"].ToString();
			}
			if (dr.Table.Columns.Contains("SlugId"))
			{
			entity.SlugId = dr["SlugId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlugId"];
			}
			if (dr.Table.Columns.Contains("Title"))
			{
			entity.Title = dr["Title"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsDate"))
			{
			entity.NewsDate = dr["NewsDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["NewsDate"];
			}
			if (dr.Table.Columns.Contains("DescriptionText"))
			{
			entity.DescriptionText = dr["DescriptionText"].ToString();
			}
			return entity;
		}

	}
	
	
}
