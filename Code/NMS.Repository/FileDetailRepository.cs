﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;


namespace NMS.Repository
{
		
	public partial class FileDetailRepository: FileDetailRepositoryBase, IFileDetailRepository
	{
        public virtual List<FileDetail> GetFileDetailByCreationDate(DateTime dt)
        {
            string sql = "select * from [FileDetail] with (nolock)  where CreationDate>@dt";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileDetail>(ds, FileDetailFromDataRow);
        }


        public List<FileDetail> GetFileDetailWithFilterFromToAndSearch(string from, string to, string searchText)
        {
            string sql = "select newsfileid from filedetail where CreationDate > @from and CreationDate <= @to and descriptiontext like '%"+ searchText + "%'";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@from", from)
                    , new SqlParameter("@to",to)
                    ,new SqlParameter("@searchtext",searchText) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileDetail>(ds, FileDetailFromDataRow);
        }


        public List<FileDetail> GetFileDetailByLastUpdateDate(DateTime dt)
        {
            string sql = "select * from [FileDetail] with (nolock)  where LastUpdateDate>@dt";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileDetail>(ds, FileDetailFromDataRow);
        }

        public List<FileDetail> getNewsFileDetailsByNewsFileIds(int FolderId, int StoryCount)
        {
            string sql = "select * from [FileDetail] with (nolock)  where newsfileId in (select TOP (@StoryCount) NewsFileId from newsfile where FolderId =@FolderId and IsDeleted=0 and ParentId is not null ORDER BY SequenceNo asc)";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@FolderId", FolderId)
                    , new SqlParameter("@StoryCount",StoryCount)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileDetail>(ds, FileDetailFromDataRow);
        }

        public virtual List<FileDetail> GetApprovedNewsfile(int[] programIds)
        {
            string sql = "select nf.NewsFileId,nf.Highlights as Title,fd.[Text],nf.ProgramId from NewsFile nf with(nolock) inner join FileDetail fd on nf.NewsFileId = fd.NewsFileId where nf.StatusId = @statusId and nf.parentid is not null and nf.LastUpdateDate >@From and nf.LastUpdateDate <=@To and nf.IsDeleted = 0 and nf.ProgramId in (" + string.Join(",", programIds) + ")  order by nf.lastupdatedate desc OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@statusId", 2)
                    , new SqlParameter("@From",DateTime.UtcNow.AddMonths(-1))
                    , new SqlParameter("@To",DateTime.UtcNow.Date.AddDays(1).AddMinutes(-1))};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileDetail>(ds, NewsFileWithFileDetailFromDataRow);
        }

        public virtual FileDetail NewsFileWithFileDetailFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            FileDetail entity = new FileDetail();
            if (dr.Table.Columns.Contains("FileDetailId"))
            {
                entity.FileDetailId = (System.Int32)dr["FileDetailId"];
            }
            if (dr.Table.Columns.Contains("NewsFileId"))
            {
                entity.NewsFileId = (System.Int32)dr["NewsFileId"];
            }

            if (dr.Table.Columns.Contains("ProgramId"))
            {
                entity.ProgramId = (System.Int32)dr["ProgramId"];
            }

            if (dr.Table.Columns.Contains("Text"))
            {
                entity.Text = dr["Text"].ToString();
            }
            if (dr.Table.Columns.Contains("CreatedBy"))
            {
                entity.CreatedBy = (System.Int32)dr["CreatedBy"];
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("ReportedBy"))
            {
                entity.ReportedBy = (System.Int32)dr["ReportedBy"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("Slug"))
            {
                entity.Slug = dr["Slug"].ToString();
            }
            if (dr.Table.Columns.Contains("CGValues"))
            {
                entity.CgValues = dr["CGValues"].ToString();
            }
            if (dr.Table.Columns.Contains("SlugId"))
            {
                entity.SlugId = dr["SlugId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SlugId"];
            }
            if (dr.Table.Columns.Contains("Title"))
            {
                entity.Title = dr["Title"].ToString();
            }
            if (dr.Table.Columns.Contains("NewsDate"))
            {
                entity.NewsDate = dr["NewsDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["NewsDate"];
            }
            if (dr.Table.Columns.Contains("DescriptionText"))
            {
                entity.DescriptionText = dr["DescriptionText"].ToString();
            }
            return entity;
        }
    }
	
	
}
