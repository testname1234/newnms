﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FileFolderHistoryRepositoryBase : Repository, IFileFolderHistoryRepositoryBase 
	{
        
        public FileFolderHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FileFolderHistoryId",new SearchColumn(){Name="FileFolderHistoryId",Title="FileFolderHistoryId",SelectClause="FileFolderHistoryId",WhereClause="AllRecords.FileFolderHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FileFolderHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FolderId",new SearchColumn(){Name="FolderId",Title="FolderId",SelectClause="FolderId",WhereClause="AllRecords.FolderId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FolderId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFileFolderHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFileFolderHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFileFolderHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFileFolderHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[FileFolderHistory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[FileFolderHistory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<FileFolderHistory> GetFileFolderHistoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileFolderHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileFolderHistory] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileFolderHistory>(ds,FileFolderHistoryFromDataRow);
		}

		public virtual List<FileFolderHistory> GetFileFolderHistoryByFolderId(System.Int32 FolderId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileFolderHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileFolderHistory] with (nolock)  where FolderId=@FolderId  ";
			SqlParameter parameter=new SqlParameter("@FolderId",FolderId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileFolderHistory>(ds,FileFolderHistoryFromDataRow);
		}

		public virtual FileFolderHistory GetFileFolderHistory(System.Int32 FileFolderHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileFolderHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileFolderHistory] with (nolock)  where FileFolderHistoryId=@FileFolderHistoryId ";
			SqlParameter parameter=new SqlParameter("@FileFolderHistoryId",FileFolderHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FileFolderHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<FileFolderHistory> GetFileFolderHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileFolderHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [FileFolderHistory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileFolderHistory>(ds,FileFolderHistoryFromDataRow);
		}

		public virtual List<FileFolderHistory> GetAllFileFolderHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileFolderHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileFolderHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileFolderHistory>(ds, FileFolderHistoryFromDataRow);
		}

		public virtual List<FileFolderHistory> GetPagedFileFolderHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFileFolderHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FileFolderHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FileFolderHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FileFolderHistoryId] ";
            tempsql += " FROM [FileFolderHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FileFolderHistoryId"))
					tempsql += " , (AllRecords.[FileFolderHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FileFolderHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFileFolderHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [FileFolderHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [FileFolderHistory].[FileFolderHistoryId] = PageIndex.[FileFolderHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileFolderHistory>(ds, FileFolderHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetFileFolderHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM FileFolderHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM FileFolderHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(FileFolderHistory))]
		public virtual FileFolderHistory InsertFileFolderHistory(FileFolderHistory entity)
		{

			FileFolderHistory other=new FileFolderHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into FileFolderHistory ( [NewsFileId]
				,[UserId]
				,[FolderId]
				,[StatusId]
				,[CreationDate] )
				Values
				( @NewsFileId
				, @UserId
				, @FolderId
				, @StatusId
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@FolderId",entity.FolderId)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFileFolderHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(FileFolderHistory))]
		public virtual FileFolderHistory UpdateFileFolderHistory(FileFolderHistory entity)
		{

			if (entity.IsTransient()) return entity;
			FileFolderHistory other = GetFileFolderHistory(entity.FileFolderHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update FileFolderHistory set  [NewsFileId]=@NewsFileId
							, [UserId]=@UserId
							, [FolderId]=@FolderId
							, [StatusId]=@StatusId
							, [CreationDate]=@CreationDate 
							 where FileFolderHistoryId=@FileFolderHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@FolderId",entity.FolderId)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@FileFolderHistoryId",entity.FileFolderHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFileFolderHistory(entity.FileFolderHistoryId);
		}

		public virtual bool DeleteFileFolderHistory(System.Int32 FileFolderHistoryId)
		{

			string sql="delete from FileFolderHistory where FileFolderHistoryId=@FileFolderHistoryId";
			SqlParameter parameter=new SqlParameter("@FileFolderHistoryId",FileFolderHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(FileFolderHistory))]
		public virtual FileFolderHistory DeleteFileFolderHistory(FileFolderHistory entity)
		{
			this.DeleteFileFolderHistory(entity.FileFolderHistoryId);
			return entity;
		}


		public virtual FileFolderHistory FileFolderHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			FileFolderHistory entity=new FileFolderHistory();
			if (dr.Table.Columns.Contains("FileFolderHistoryId"))
			{
			entity.FileFolderHistoryId = (System.Int32)dr["FileFolderHistoryId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = (System.Int32)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("FolderId"))
			{
			entity.FolderId = (System.Int32)dr["FolderId"];
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
