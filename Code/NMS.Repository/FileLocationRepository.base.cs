﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FileLocationRepositoryBase : Repository, IFileLocationRepositoryBase 
	{
        
        public FileLocationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FileLocationId",new SearchColumn(){Name="FileLocationId",Title="FileLocationId",SelectClause="FileLocationId",WhereClause="AllRecords.FileLocationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FileLocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFileLocationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFileLocationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFileLocationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFileLocationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[FileLocation].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[FileLocation].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<FileLocation> GetFileLocationByNewsFileId(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileLocation] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileLocation>(ds,FileLocationFromDataRow);
		}

		public virtual List<FileLocation> GetFileLocationByLocationId(System.Int32 LocationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileLocation] with (nolock)  where LocationId=@LocationId  ";
			SqlParameter parameter=new SqlParameter("@LocationId",LocationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileLocation>(ds,FileLocationFromDataRow);
		}

		public virtual FileLocation GetFileLocation(System.Int32 FileLocationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileLocation] with (nolock)  where FileLocationId=@FileLocationId ";
			SqlParameter parameter=new SqlParameter("@FileLocationId",FileLocationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FileLocationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<FileLocation> GetFileLocationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [FileLocation] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileLocation>(ds,FileLocationFromDataRow);
		}

		public virtual List<FileLocation> GetAllFileLocation(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileLocation] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileLocation>(ds, FileLocationFromDataRow);
		}

		public virtual List<FileLocation> GetPagedFileLocation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFileLocationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FileLocationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FileLocationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FileLocationId] ";
            tempsql += " FROM [FileLocation] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FileLocationId"))
					tempsql += " , (AllRecords.[FileLocationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FileLocationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFileLocationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [FileLocation] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [FileLocation].[FileLocationId] = PageIndex.[FileLocationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileLocation>(ds, FileLocationFromDataRow);
			}else{ return null;}
		}

		private int GetFileLocationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM FileLocation as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM FileLocation as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(FileLocation))]
		public virtual FileLocation InsertFileLocation(FileLocation entity)
		{

			FileLocation other=new FileLocation();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into FileLocation ( [NewsFileId]
				,[LocationId]
				,[CreationDate] )
				Values
				( @NewsFileId
				, @LocationId
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@LocationId",entity.LocationId)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFileLocation(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(FileLocation))]
		public virtual FileLocation UpdateFileLocation(FileLocation entity)
		{

			if (entity.IsTransient()) return entity;
			FileLocation other = GetFileLocation(entity.FileLocationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update FileLocation set  [NewsFileId]=@NewsFileId
							, [LocationId]=@LocationId
							, [CreationDate]=@CreationDate 
							 where FileLocationId=@FileLocationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@LocationId",entity.LocationId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@FileLocationId",entity.FileLocationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFileLocation(entity.FileLocationId);
		}

		public virtual bool DeleteFileLocation(System.Int32 FileLocationId)
		{

			string sql="delete from FileLocation where FileLocationId=@FileLocationId";
			SqlParameter parameter=new SqlParameter("@FileLocationId",FileLocationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(FileLocation))]
		public virtual FileLocation DeleteFileLocation(FileLocation entity)
		{
			this.DeleteFileLocation(entity.FileLocationId);
			return entity;
		}


		public virtual FileLocation FileLocationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			FileLocation entity=new FileLocation();
			if (dr.Table.Columns.Contains("FileLocationId"))
			{
			entity.FileLocationId = (System.Int32)dr["FileLocationId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = (System.Int32)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
