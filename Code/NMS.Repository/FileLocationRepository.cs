﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class FileLocationRepository: FileLocationRepositoryBase, IFileLocationRepository
	{

        public bool DeleteFileLocationByNewsFileId(int newsFileId)
        {
            string sql = "delete from FileLocation where newsFileId=@newsFileId";
            SqlParameter parameter = new SqlParameter("@newsFileId", newsFileId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
    }
	
	
}
