﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public partial class FileResourceRepository: FileResourceRepositoryBase, IFileResourceRepository
	{
        public virtual List<FileResource> GetFileResourceByCreationDate(DateTime dt)
        {
            string sql = "select * from [FileResource] with (nolock)  where creationDate>@dt";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileResource>(ds, FileResourceFromDataRow);
        }

        public FileResource GetFileResourceByGuid(Guid guid)
        {
            string sql = "select * from [FileResource] with (nolock)  where Guid=@guid";
            SqlParameter parameter = new SqlParameter("@guid", guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return FileResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public bool MarkFileResourceInActiveByGuid(string guid)
        {
            string sql = "update FileResource set isActive=0,lastupdatedate=GETUTCDATE() where guid=@guid";
            SqlParameter parameter = new SqlParameter("@guid", guid);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public List<FileResource> GetFileResourceByLastUpdateDate(DateTime dt)
        {
            string sql = "select * from [FileResource] with (nolock)  where LastUpdateDate>@dt and isActive=1";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileResource>(ds, FileResourceFromDataRow);
        }


        public bool DeleteFileResourceByNewsFileId(int newsFileId)
        {
            string sql = "delete from FileResource where newsFileId=@newsFileId";
            SqlParameter parameter = new SqlParameter("@newsFileId", newsFileId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
    }
	
	
}
