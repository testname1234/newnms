﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FileStatusHistoryRepositoryBase : Repository, IFileStatusHistoryRepositoryBase 
	{
        
        public FileStatusHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FileStatusHistoryId",new SearchColumn(){Name="FileStatusHistoryId",Title="FileStatusHistoryId",SelectClause="FileStatusHistoryId",WhereClause="AllRecords.FileStatusHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FileStatusHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFileStatusHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFileStatusHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFileStatusHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFileStatusHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[FileStatusHistory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[FileStatusHistory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<FileStatusHistory> GetFileStatusHistoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileStatusHistory] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileStatusHistory>(ds,FileStatusHistoryFromDataRow);
		}

		public virtual FileStatusHistory GetFileStatusHistory(System.Int32 FileStatusHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileStatusHistory] with (nolock)  where FileStatusHistoryId=@FileStatusHistoryId ";
			SqlParameter parameter=new SqlParameter("@FileStatusHistoryId",FileStatusHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FileStatusHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<FileStatusHistory> GetFileStatusHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [FileStatusHistory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileStatusHistory>(ds,FileStatusHistoryFromDataRow);
		}

		public virtual List<FileStatusHistory> GetAllFileStatusHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileStatusHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileStatusHistory>(ds, FileStatusHistoryFromDataRow);
		}

		public virtual List<FileStatusHistory> GetPagedFileStatusHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFileStatusHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FileStatusHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FileStatusHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FileStatusHistoryId] ";
            tempsql += " FROM [FileStatusHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FileStatusHistoryId"))
					tempsql += " , (AllRecords.[FileStatusHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FileStatusHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFileStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [FileStatusHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [FileStatusHistory].[FileStatusHistoryId] = PageIndex.[FileStatusHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileStatusHistory>(ds, FileStatusHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetFileStatusHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM FileStatusHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM FileStatusHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(FileStatusHistory))]
		public virtual FileStatusHistory InsertFileStatusHistory(FileStatusHistory entity)
		{

			FileStatusHistory other=new FileStatusHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into FileStatusHistory ( [NewsFileId]
				,[StatusId]
				,[UserId]
				,[CreationDate] )
				Values
				( @NewsFileId
				, @StatusId
				, @UserId
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFileStatusHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(FileStatusHistory))]
		public virtual FileStatusHistory UpdateFileStatusHistory(FileStatusHistory entity)
		{

			if (entity.IsTransient()) return entity;
			FileStatusHistory other = GetFileStatusHistory(entity.FileStatusHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update FileStatusHistory set  [NewsFileId]=@NewsFileId
							, [StatusId]=@StatusId
							, [UserId]=@UserId
							, [CreationDate]=@CreationDate 
							 where FileStatusHistoryId=@FileStatusHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@FileStatusHistoryId",entity.FileStatusHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFileStatusHistory(entity.FileStatusHistoryId);
		}

		public virtual bool DeleteFileStatusHistory(System.Int32 FileStatusHistoryId)
		{

			string sql="delete from FileStatusHistory where FileStatusHistoryId=@FileStatusHistoryId";
			SqlParameter parameter=new SqlParameter("@FileStatusHistoryId",FileStatusHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(FileStatusHistory))]
		public virtual FileStatusHistory DeleteFileStatusHistory(FileStatusHistory entity)
		{
			this.DeleteFileStatusHistory(entity.FileStatusHistoryId);
			return entity;
		}


		public virtual FileStatusHistory FileStatusHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			FileStatusHistory entity=new FileStatusHistory();
			if (dr.Table.Columns.Contains("FileStatusHistoryId"))
			{
			entity.FileStatusHistoryId = (System.Int32)dr["FileStatusHistoryId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = (System.Int32)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
