﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;


namespace NMS.Repository
{
		
	public partial class FileStatusHistoryRepository: FileStatusHistoryRepositoryBase, IFileStatusHistoryRepository
	{

        public virtual List<FileStatusHistory> GetFileStatusHistoryByCreationDate(DateTime dt)
        {
            string sql = "select * from [FileStatusHistory] with (nolock)  where CreationDate>@dt";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<FileStatusHistory>(ds, FileStatusHistoryFromDataRow);
        }
	}
	
	
}
