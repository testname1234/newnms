﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FileTagRepositoryBase : Repository, IFileTagRepositoryBase 
	{
        
        public FileTagRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FileTagId",new SearchColumn(){Name="FileTagId",Title="FileTagId",SelectClause="FileTagId",WhereClause="AllRecords.FileTagId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FileTagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TagId",new SearchColumn(){Name="TagId",Title="TagId",SelectClause="TagId",WhereClause="AllRecords.TagId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Rank",new SearchColumn(){Name="Rank",Title="Rank",SelectClause="Rank",WhereClause="AllRecords.Rank",DataType="System.Double",IsForeignColumn=false,PropertyName="Rank",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFileTagSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFileTagBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFileTagAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFileTagSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[FileTag].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[FileTag].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<FileTag> GetFileTagByNewsFileId(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileTag] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileTag>(ds,FileTagFromDataRow);
		}

		public virtual List<FileTag> GetFileTagByTagId(System.Int32 TagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileTag] with (nolock)  where TagId=@TagId  ";
			SqlParameter parameter=new SqlParameter("@TagId",TagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileTag>(ds,FileTagFromDataRow);
		}

		public virtual FileTag GetFileTag(System.Int32 FileTagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileTag] with (nolock)  where FileTagId=@FileTagId ";
			SqlParameter parameter=new SqlParameter("@FileTagId",FileTagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FileTagFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<FileTag> GetFileTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [FileTag] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileTag>(ds,FileTagFromDataRow);
		}

		public virtual List<FileTag> GetAllFileTag(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFileTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [FileTag] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileTag>(ds, FileTagFromDataRow);
		}

		public virtual List<FileTag> GetPagedFileTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFileTagCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FileTagId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FileTagId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FileTagId] ";
            tempsql += " FROM [FileTag] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FileTagId"))
					tempsql += " , (AllRecords.[FileTagId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FileTagId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFileTagSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [FileTag] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [FileTag].[FileTagId] = PageIndex.[FileTagId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<FileTag>(ds, FileTagFromDataRow);
			}else{ return null;}
		}

		private int GetFileTagCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM FileTag as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM FileTag as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(FileTag))]
		public virtual FileTag InsertFileTag(FileTag entity)
		{

			FileTag other=new FileTag();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into FileTag ( [NewsFileId]
				,[TagId]
				,[Rank]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @NewsFileId
				, @TagId
				, @Rank
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@Rank",entity.Rank)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFileTag(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(FileTag))]
		public virtual FileTag UpdateFileTag(FileTag entity)
		{

			if (entity.IsTransient()) return entity;
			FileTag other = GetFileTag(entity.FileTagId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update FileTag set  [NewsFileId]=@NewsFileId
							, [TagId]=@TagId
							, [Rank]=@Rank
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where FileTagId=@FileTagId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@Rank",entity.Rank)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@FileTagId",entity.FileTagId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFileTag(entity.FileTagId);
		}

		public virtual bool DeleteFileTag(System.Int32 FileTagId)
		{

			string sql="delete from FileTag where FileTagId=@FileTagId";
			SqlParameter parameter=new SqlParameter("@FileTagId",FileTagId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(FileTag))]
		public virtual FileTag DeleteFileTag(FileTag entity)
		{
			this.DeleteFileTag(entity.FileTagId);
			return entity;
		}


		public virtual FileTag FileTagFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			FileTag entity=new FileTag();
			if (dr.Table.Columns.Contains("FileTagId"))
			{
			entity.FileTagId = (System.Int32)dr["FileTagId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("TagId"))
			{
			entity.TagId = (System.Int32)dr["TagId"];
			}
			if (dr.Table.Columns.Contains("Rank"))
			{
			entity.Rank = (System.Double)dr["Rank"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
