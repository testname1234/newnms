﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FilterRepositoryBase : Repository, IFilterRepositoryBase 
	{
        
        public FilterRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FilterId",new SearchColumn(){Name="FilterId",Title="FilterId",SelectClause="FilterId",WhereClause="AllRecords.FilterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Value",new SearchColumn(){Name="Value",Title="Value",SelectClause="Value",WhereClause="AllRecords.Value",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Value",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterTypeId",new SearchColumn(){Name="FilterTypeId",Title="FilterTypeId",SelectClause="FilterTypeId",WhereClause="AllRecords.FilterTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FilterTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentId",new SearchColumn(){Name="ParentId",Title="ParentId",SelectClause="ParentId",WhereClause="AllRecords.ParentId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CssClass",new SearchColumn(){Name="CssClass",Title="CssClass",SelectClause="CssClass",WhereClause="AllRecords.CssClass",DataType="System.String",IsForeignColumn=false,PropertyName="CssClass",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsApproved",new SearchColumn(){Name="IsApproved",Title="IsApproved",SelectClause="IsApproved",WhereClause="AllRecords.IsApproved",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsApproved",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFilterSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFilterBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFilterAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFilterSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "Filter."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",Filter."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Filter> GetFilterByFilterTypeId(System.Int32 FilterTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from Filter with (nolock)  where FilterTypeId=@FilterTypeId";
			SqlParameter parameter=new SqlParameter("@FilterTypeId",FilterTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Filter>(ds,FilterFromDataRow);
		}

		public virtual Filter GetFilter(System.Int32 FilterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from Filter with (nolock)  where FilterId=@FilterId ";
			SqlParameter parameter=new SqlParameter("@FilterId",FilterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FilterFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Filter> GetFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from Filter with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Filter>(ds,FilterFromDataRow);
		}

		public virtual List<Filter> GetAllFilter(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from Filter with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Filter>(ds, FilterFromDataRow);
		}

		public virtual List<Filter> GetPagedFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFilterCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FilterId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FilterId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FilterId] ";
            tempsql += " FROM [Filter] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FilterId"))
					tempsql += " , (AllRecords.[FilterId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FilterId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFilterSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Filter] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Filter].[FilterId] = PageIndex.[FilterId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Filter>(ds, FilterFromDataRow);
			}else{ return null;}
		}

		private int GetFilterCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Filter as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Filter as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Filter))]
		public virtual Filter InsertFilter(Filter entity)
		{

			Filter other=new Filter();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Filter ( [Name]
				,[Value]
				,[FilterTypeId]
				,[ParentId]
				,[CssClass]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsApproved] )
				Values
				( @Name
				, @Value
				, @FilterTypeId
				, @ParentId
				, @CssClass
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsApproved );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Value",entity.Value ?? (object)DBNull.Value)
					, new SqlParameter("@FilterTypeId",entity.FilterTypeId)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CssClass",entity.CssClass ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFilter(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Filter))]
		public virtual Filter UpdateFilter(Filter entity)
		{

			if (entity.IsTransient()) return entity;
			Filter other = GetFilter(entity.FilterId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Filter set  [Name]=@Name
							, [Value]=@Value
							, [FilterTypeId]=@FilterTypeId
							, [ParentId]=@ParentId
							, [CssClass]=@CssClass
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsApproved]=@IsApproved 
							 where FilterId=@FilterId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Value",entity.Value ?? (object)DBNull.Value)
					, new SqlParameter("@FilterTypeId",entity.FilterTypeId)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CssClass",entity.CssClass ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFilter(entity.FilterId);
		}

		public virtual bool DeleteFilter(System.Int32 FilterId)
		{

			string sql="delete from Filter with (nolock) where FilterId=@FilterId";
			SqlParameter parameter=new SqlParameter("@FilterId",FilterId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Filter))]
		public virtual Filter DeleteFilter(Filter entity)
		{
			this.DeleteFilter(entity.FilterId);
			return entity;
		}


		public virtual Filter FilterFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Filter entity=new Filter();
			if (dr.Table.Columns.Contains("FilterId"))
			{
			entity.FilterId = (System.Int32)dr["FilterId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("Value"))
			{
			entity.Value = dr["Value"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Value"];
			}
			if (dr.Table.Columns.Contains("FilterTypeId"))
			{
			entity.FilterTypeId = (System.Int32)dr["FilterTypeId"];
			}
			if (dr.Table.Columns.Contains("ParentId"))
			{
			entity.ParentId = dr["ParentId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentId"];
			}
			if (dr.Table.Columns.Contains("CssClass"))
			{
			entity.CssClass = dr["CssClass"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("IsApproved"))
			{
			entity.IsApproved = dr["IsApproved"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsApproved"];
			}
			return entity;
		}

	}
	
	
}
