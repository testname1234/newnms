﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Enums;

namespace NMS.Repository
{

    public partial class FilterRepository : FilterRepositoryBase, IFilterRepository
    {
        public FilterRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public FilterRepository()
        {
        }

        public Filter GetFilterCreateIfNotExist(Filter filter)
        {
            if (!Exist(filter))
            {
                return InsertFilter(filter);
            }
            return GetFilterByNameAndFilterType((FilterTypes)filter.FilterTypeId, filter.Name);
        }

        public Filter GetFilterCreateIfNotExistByValue(Filter filter)
        {
            return GetFilterByFilterTypeAndValue((int)filter.FilterTypeId, filter.FilterId);
        }

        public bool Exist(Filter filter)
        {
            string sql = "select count(1) from Filter with (nolock)  where FilterTypeId=@FilterTypeId and Name =@Name";
            SqlParameter parameter = new SqlParameter("@FilterTypeId", filter.FilterTypeId);
            SqlParameter parameter1 = new SqlParameter("@Name", filter.Name);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 })) > 0;
        }

        public Filter GetFilterByNameAndFilterType(FilterTypes filterType, string name)
        {
            string sql = GetFilterSelectClause();
            sql += "from Filter with (nolock)  where FilterTypeId=@FilterTypeId and Name =@Name ";
            SqlParameter parameter = new SqlParameter("@FilterTypeId", (int)filterType);
            SqlParameter parameter1 = new SqlParameter("@Name", name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return FilterFromDataRow(ds.Tables[0].Rows[0]);
        }


        public Filter GetFilterByFilterTypeAndValue(int filterTypeId, int value)
        {
            string sql = GetFilterSelectClause();
            sql += "from Filter with (nolock)  where FilterTypeId=@FilterTypeId and Value =@Value ";
            SqlParameter parameter = new SqlParameter("@FilterTypeId", filterTypeId);
            SqlParameter parameter1 = new SqlParameter("@Value", value);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return FilterFromDataRow(ds.Tables[0].Rows[0]);
        }



        public List<int> GetDistinctFilterIds()
        {
            string sql = "Select distinct FilterId ";
            sql += "from Filter with (nolock)";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            List<int> result = new List<int>();
            foreach (DataRow dr in ds.Tables[0].Rows)
                result.Add(Convert.ToInt32(dr["FilterId"]));
            return result;
        }


        public List<Filter> GetAllParentFilters()
        {
            string sql = GetFilterSelectClause();
            sql += "from Filter with (nolock)  where ParentID is null";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Filter>(ds, FilterFromDataRow);
        }

        public virtual void InsertFilterWithPk(Filter entity)
        {

            string sql = @"
                set identity_insert Filter on
                Insert into Filter (FilterId, [Name]
				,[Value]
				,[FilterTypeId]
				,[ParentId]
				,[CssClass]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsApproved] )
				Values
				( @FilterId, 
                , @Name
				, @Value
				, @FilterTypeId
				, @ParentId
				, @CssClass
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsApproved );
                set identity_insert Filter off
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
                      new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Value",entity.Value ?? (object)DBNull.Value)
					, new SqlParameter("@FilterTypeId",entity.FilterTypeId)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CssClass",entity.CssClass ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual void UpdateFilterWithNoReturn(Filter entity)
        {
            string sql = @"Update Filter set  [Name]=@Name
							, [Value]=@Value
							, [FilterTypeId]=@FilterTypeId
							, [ParentId]=@ParentId
							, [CssClass]=@CssClass
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsApproved]=@IsApproved 
							 where FilterId=@FilterId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Value",entity.Value ?? (object)DBNull.Value)
					, new SqlParameter("@FilterTypeId",entity.FilterTypeId)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CssClass",entity.CssClass ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public List<Filter> GetFilterByLastUpdateDate(DateTime filterLastUpdateDate)
        {
            string sql = GetFilterSelectClause();
            sql += "from Filter with (nolock)  where lastupdatedate > @lastupdatedate order by lastupdatedate";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[]{
                     new SqlParameter("@lastupdatedate",filterLastUpdateDate) });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Filter>(ds, FilterFromDataRow);
        }
    }


}
