﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class FilterTypeRepository : FilterTypeRepositoryBase, IFilterTypeRepository
    {
        public FilterTypeRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public FilterTypeRepository()
        {
        }

        public virtual void InsertFilterTypeWithPk(FilterType entity)
        {
            string sql = @"set identity_insert FilterType on
                Insert into FilterType (FilterTypeId, [FilterType]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @FilterTypeId,
                , @FilterType
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
            set identity_insert FilterType off
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@FilterTypeId",entity.FilterTypeId)
                    , new SqlParameter("@FilterType",entity.FilterType)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual void UpdateFilterTypeNoReturn(FilterType entity)
        {
            string sql = @"Update FilterType set  [FilterType]=@FilterType
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where FilterTypeId=@FilterTypeId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@FilterType",entity.FilterType)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@FilterTypeId",entity.FilterTypeId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }
    }


}
