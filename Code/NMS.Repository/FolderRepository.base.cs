﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class FolderRepositoryBase : Repository, IFolderRepositoryBase 
	{
        
        public FolderRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("FolderId",new SearchColumn(){Name="FolderId",Title="FolderId",SelectClause="FolderId",WhereClause="AllRecords.FolderId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FolderId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentFolderId",new SearchColumn(){Name="ParentFolderId",Title="ParentFolderId",SelectClause="ParentFolderId",WhereClause="AllRecords.ParentFolderId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentFolderId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsRundown",new SearchColumn(){Name="IsRundown",Title="IsRundown",SelectClause="IsRundown",WhereClause="AllRecords.IsRundown",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsRundown",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsPrivateFolder",new SearchColumn(){Name="IsPrivateFolder",Title="IsPrivateFolder",SelectClause="IsPrivateFolder",WhereClause="AllRecords.IsPrivateFolder",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsPrivateFolder",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EpisodeId",new SearchColumn(){Name="EpisodeId",Title="EpisodeId",SelectClause="EpisodeId",WhereClause="AllRecords.EpisodeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="EpisodeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StartTime",new SearchColumn(){Name="StartTime",Title="StartTime",SelectClause="StartTime",WhereClause="AllRecords.StartTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="StartTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EndTime",new SearchColumn(){Name="EndTime",Title="EndTime",SelectClause="EndTime",WhereClause="AllRecords.EndTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="EndTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetFolderSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetFolderBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetFolderAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetFolderSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Folder].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Folder].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Folder GetFolder(System.Int32 FolderId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFolderSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Folder] with (nolock)  where FolderId=@FolderId ";
			SqlParameter parameter=new SqlParameter("@FolderId",FolderId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return FolderFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Folder> GetFolderByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFolderSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Folder] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Folder>(ds,FolderFromDataRow);
		}

		public virtual List<Folder> GetAllFolder(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetFolderSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Folder] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Folder>(ds, FolderFromDataRow);
		}

		public virtual List<Folder> GetPagedFolder(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetFolderCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [FolderId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([FolderId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [FolderId] ";
            tempsql += " FROM [Folder] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("FolderId"))
					tempsql += " , (AllRecords.[FolderId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[FolderId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetFolderSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Folder] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Folder].[FolderId] = PageIndex.[FolderId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Folder>(ds, FolderFromDataRow);
			}else{ return null;}
		}

		private int GetFolderCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Folder as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Folder as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Folder))]
		public virtual Folder InsertFolder(Folder entity)
		{

			Folder other=new Folder();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Folder ( [Name]
				,[CategoryId]
				,[LocationId]
				,[CreationDate]
				,[ParentFolderId]
				,[IsRundown]
				,[ProgramId]
				,[IsPrivateFolder]
				,[EpisodeId]
				,[StartTime]
				,[EndTime] )
				Values
				( @Name
				, @CategoryId
				, @LocationId
				, @CreationDate
				, @ParentFolderId
				, @IsRundown
				, @ProgramId
				, @IsPrivateFolder
				, @EpisodeId
				, @StartTime
				, @EndTime );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@ParentFolderId",entity.ParentFolderId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRundown",entity.IsRundown)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@IsPrivateFolder",entity.IsPrivateFolder ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@StartTime",entity.StartTime ?? (object)DBNull.Value)
					, new SqlParameter("@EndTime",entity.EndTime ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetFolder(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Folder))]
		public virtual Folder UpdateFolder(Folder entity)
		{

			if (entity.IsTransient()) return entity;
			Folder other = GetFolder(entity.FolderId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Folder set  [Name]=@Name
							, [CategoryId]=@CategoryId
							, [LocationId]=@LocationId
							, [CreationDate]=@CreationDate
							, [ParentFolderId]=@ParentFolderId
							, [IsRundown]=@IsRundown
							, [ProgramId]=@ProgramId
							, [IsPrivateFolder]=@IsPrivateFolder
							, [EpisodeId]=@EpisodeId
							, [StartTime]=@StartTime
							, [EndTime]=@EndTime 
							 where FolderId=@FolderId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@ParentFolderId",entity.ParentFolderId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRundown",entity.IsRundown)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@IsPrivateFolder",entity.IsPrivateFolder ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@StartTime",entity.StartTime ?? (object)DBNull.Value)
					, new SqlParameter("@EndTime",entity.EndTime ?? (object)DBNull.Value)
					, new SqlParameter("@FolderId",entity.FolderId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetFolder(entity.FolderId);
		}

		public virtual bool DeleteFolder(System.Int32 FolderId)
		{

			string sql="delete from Folder where FolderId=@FolderId";
			SqlParameter parameter=new SqlParameter("@FolderId",FolderId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Folder))]
		public virtual Folder DeleteFolder(Folder entity)
		{
			this.DeleteFolder(entity.FolderId);
			return entity;
		}


		public virtual Folder FolderFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Folder entity=new Folder();
			if (dr.Table.Columns.Contains("FolderId"))
			{
			entity.FolderId = (System.Int32)dr["FolderId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("ParentFolderId"))
			{
			entity.ParentFolderId = dr["ParentFolderId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentFolderId"];
			}
			if (dr.Table.Columns.Contains("IsRundown"))
			{
			entity.IsRundown = (System.Boolean)dr["IsRundown"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("IsPrivateFolder"))
			{
			entity.IsPrivateFolder = dr["IsPrivateFolder"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsPrivateFolder"];
			}
			if (dr.Table.Columns.Contains("EpisodeId"))
			{
			entity.EpisodeId = dr["EpisodeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["EpisodeId"];
			}
			if (dr.Table.Columns.Contains("StartTime"))
			{
			entity.StartTime = dr["StartTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["StartTime"];
			}
			if (dr.Table.Columns.Contains("EndTime"))
			{
			entity.EndTime = dr["EndTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["EndTime"];
			}
			return entity;
		}

	}
	
	
}
