﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{

    public partial class FolderRepository : FolderRepositoryBase, IFolderRepository
    {
        public List<Folder> GetFolderByCreationDate(DateTime dt)
        {
            string sql = "select * from [Folder] with (nolock) where CreationDate>@dt and (EndTime is null or EndTime>getutcdate())";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Folder>(ds, FolderFromDataRow);
        }

        public List<Folder> GetAllFolder()
        {
            string sql = "select *,(select Location from Location where Location.LocationId=[Folder].LocationId) as Location,(select Category from Category where Category.CategoryId=[Folder].CategoryId) as Category from [Folder] with (nolock)  ";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Folder>(ds, FolderFromDataRow);
        }

        public bool EpisodeFolderExist(int episodeId)
        {
            string sql = "select count(1) from Folder where episodeid=" + episodeId;
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, null)) > 0;
        }

        public List<Folder> GetByEpisodeId(int episodeId)
        {
            string sql = "select * from [Folder] with (nolock)  with (nolock)  where EpisodeId=@EpisodeId";
            SqlParameter parameter = new SqlParameter("@EpisodeId", episodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Folder>(ds, FolderFromDataRow);
        }

        public override Folder FolderFromDataRow(DataRow dr)
        {
            var entity = base.FolderFromDataRow(dr);
            if (dr.Table.Columns.Contains("Location"))
            {
                entity.Location = dr["Location"] == DBNull.Value ? "" : dr["Location"].ToString();
            }
            if (dr.Table.Columns.Contains("Category"))
            {
                entity.Category = dr["Category"] == DBNull.Value ? "" : dr["Category"].ToString();
            }
            return entity;
        }

        public List<Folder> GetFolderByEpisodeId(int episodeId)
        {
            string sql = "select * from [Folder] with (nolock) where EpisodeId=@EpisodeId and isRundown=1";
            SqlParameter parameter = new SqlParameter("@EpisodeId", episodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Folder>(ds, FolderFromDataRow);
        }


        public List<Folder> GetFolderByProgramId(int ProgramId)
        {
            string sql = "select * from [Folder] with (nolock) where ProgramId=@ProgramId";
            SqlParameter parameter = new SqlParameter("@ProgramId", ProgramId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Folder>(ds, FolderFromDataRow);
        }

    }
	
	
}
