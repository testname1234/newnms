﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class GroupUserRepositoryBase : Repository, IGroupUserRepositoryBase 
	{
        
        public GroupUserRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("GroupId",new SearchColumn(){Name="GroupId",Title="GroupId",SelectClause="GroupId",WhereClause="AllRecords.GroupId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserID",new SearchColumn(){Name="UserID",Title="UserID",SelectClause="UserID",WhereClause="AllRecords.UserID",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("GroupUserId",new SearchColumn(){Name="GroupUserId",Title="GroupUserId",SelectClause="GroupUserId",WhereClause="AllRecords.GroupUserId",DataType="System.Int32",IsForeignColumn=false,PropertyName="GroupUserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetGroupUserSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetGroupUserBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetGroupUserAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetGroupUserSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[GroupUser].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[GroupUser].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual GroupUser GetGroupUser(System.Int32 GroupUserId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGroupUserSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [GroupUser] with (nolock)  where GroupUserId=@GroupUserId ";
			SqlParameter parameter=new SqlParameter("@GroupUserId",GroupUserId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return GroupUserFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<GroupUser> GetGroupUserByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGroupUserSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [GroupUser] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<GroupUser>(ds,GroupUserFromDataRow);
		}

		public virtual List<GroupUser> GetAllGroupUser(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGroupUserSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [GroupUser] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<GroupUser>(ds, GroupUserFromDataRow);
		}

		public virtual List<GroupUser> GetPagedGroupUser(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetGroupUserCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [GroupUserId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([GroupUserId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [GroupUserId] ";
            tempsql += " FROM [GroupUser] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("GroupUserId"))
					tempsql += " , (AllRecords.[GroupUserId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[GroupUserId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetGroupUserSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [GroupUser] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [GroupUser].[GroupUserId] = PageIndex.[GroupUserId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<GroupUser>(ds, GroupUserFromDataRow);
			}else{ return null;}
		}

		private int GetGroupUserCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM GroupUser as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM GroupUser as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(GroupUser))]
		public virtual GroupUser InsertGroupUser(GroupUser entity)
		{

			GroupUser other=new GroupUser();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into GroupUser ( [GroupId]
				,[UserID] )
				Values
				( @GroupId
				, @UserID );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@GroupId",entity.GroupId ?? (object)DBNull.Value)
					, new SqlParameter("@UserID",entity.UserId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetGroupUser(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(GroupUser))]
		public virtual GroupUser UpdateGroupUser(GroupUser entity)
		{

			if (entity.IsTransient()) return entity;
			GroupUser other = GetGroupUser(entity.GroupUserId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update GroupUser set  [GroupId]=@GroupId
							, [UserID]=@UserID 
							 where GroupUserId=@GroupUserId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@GroupId",entity.GroupId ?? (object)DBNull.Value)
					, new SqlParameter("@UserID",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@GroupUserId",entity.GroupUserId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetGroupUser(entity.GroupUserId);
		}

		public virtual bool DeleteGroupUser(System.Int32 GroupUserId)
		{

			string sql="delete from GroupUser where GroupUserId=@GroupUserId";
			SqlParameter parameter=new SqlParameter("@GroupUserId",GroupUserId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(GroupUser))]
		public virtual GroupUser DeleteGroupUser(GroupUser entity)
		{
			this.DeleteGroupUser(entity.GroupUserId);
			return entity;
		}


		public virtual GroupUser GroupUserFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			GroupUser entity=new GroupUser();
			if (dr.Table.Columns.Contains("GroupId"))
			{
			entity.GroupId = dr["GroupId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["GroupId"];
			}
			if (dr.Table.Columns.Contains("UserID"))
			{
			entity.UserId = dr["UserID"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserID"];
			}
			if (dr.Table.Columns.Contains("GroupUserId"))
			{
			entity.GroupUserId = (System.Int32)dr["GroupUserId"];
			}
			return entity;
		}

	}
	
	
}
