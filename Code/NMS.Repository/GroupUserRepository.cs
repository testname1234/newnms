﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using System.Data;
using System.Data.SqlClient;
using NMS.Core.DataInterfaces;

namespace NMS.Repository
{
		
	public partial class GroupUserRepository: GroupUserRepositoryBase, IGroupUserRepository
	{

        public void RemoveUserFromGroup(int userId, int GroupId)
        {
            string sql = "delete from GroupUser where UserId=@userId and GroupId=@GroupId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@userId",userId)
					, new SqlParameter("@GroupId",GroupId)};
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public bool DeleteGroupUserByGroupId(int groupId)
        {
            string sql = "delete from GroupUser where GroupId=@GroupId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@GroupId",groupId)};
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
		
	}
	
	
}
