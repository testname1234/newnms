﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class GuestRepositoryBase : Repository, IGuestRepositoryBase 
	{
        
        public GuestRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("GuestId",new SearchColumn(){Name="GuestId",Title="GuestId",SelectClause="GuestId",WhereClause="AllRecords.GuestId",DataType="System.Int32",IsForeignColumn=false,PropertyName="GuestId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotId",new SearchColumn(){Name="SlotId",Title="SlotId",SelectClause="SlotId",WhereClause="AllRecords.SlotId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationType",new SearchColumn(){Name="LocationType",Title="LocationType",SelectClause="LocationType",WhereClause="AllRecords.LocationType",DataType="System.Int32",IsForeignColumn=false,PropertyName="LocationType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Question",new SearchColumn(){Name="Question",Title="Question",SelectClause="Question",WhereClause="AllRecords.Question",DataType="System.String",IsForeignColumn=false,PropertyName="Question",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetGuestSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetGuestBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetGuestAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetGuestSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Guest].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Guest].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Guest GetGuest(System.Int32 GuestId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGuestSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Guest] with (nolock)  where GuestId=@GuestId ";
			SqlParameter parameter=new SqlParameter("@GuestId",GuestId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return GuestFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Guest> GetGuestByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGuestSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Guest] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Guest>(ds,GuestFromDataRow);
		}

		public virtual List<Guest> GetAllGuest(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGuestSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Guest] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Guest>(ds, GuestFromDataRow);
		}

		public virtual List<Guest> GetPagedGuest(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetGuestCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [GuestId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([GuestId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [GuestId] ";
            tempsql += " FROM [Guest] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("GuestId"))
					tempsql += " , (AllRecords.[GuestId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[GuestId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetGuestSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Guest] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Guest].[GuestId] = PageIndex.[GuestId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Guest>(ds, GuestFromDataRow);
			}else{ return null;}
		}

		private int GetGuestCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Guest as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Guest as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Guest))]
		public virtual Guest InsertGuest(Guest entity)
		{

			Guest other=new Guest();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Guest ( [SlotId]
				,[LocationType]
				,[Question]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[CelebrityId] )
				Values
				( @SlotId
				, @LocationType
				, @Question
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @CelebrityId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotId",entity.SlotId)
					, new SqlParameter("@LocationType",entity.LocationType)
					, new SqlParameter("@Question",entity.Question ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetGuest(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Guest))]
		public virtual Guest UpdateGuest(Guest entity)
		{

			if (entity.IsTransient()) return entity;
			Guest other = GetGuest(entity.GuestId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Guest set  [SlotId]=@SlotId
							, [LocationType]=@LocationType
							, [Question]=@Question
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [CelebrityId]=@CelebrityId 
							 where GuestId=@GuestId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotId",entity.SlotId)
					, new SqlParameter("@LocationType",entity.LocationType)
					, new SqlParameter("@Question",entity.Question ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@GuestId",entity.GuestId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetGuest(entity.GuestId);
		}

		public virtual bool DeleteGuest(System.Int32 GuestId)
		{

			string sql="delete from Guest where GuestId=@GuestId";
			SqlParameter parameter=new SqlParameter("@GuestId",GuestId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Guest))]
		public virtual Guest DeleteGuest(Guest entity)
		{
			this.DeleteGuest(entity.GuestId);
			return entity;
		}


		public virtual Guest GuestFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Guest entity=new Guest();
			if (dr.Table.Columns.Contains("GuestId"))
			{
			entity.GuestId = (System.Int32)dr["GuestId"];
			}
			if (dr.Table.Columns.Contains("SlotId"))
			{
			entity.SlotId = (System.Int32)dr["SlotId"];
			}
			if (dr.Table.Columns.Contains("LocationType"))
			{
			entity.LocationType = (System.Int32)dr["LocationType"];
			}
			if (dr.Table.Columns.Contains("Question"))
			{
			entity.Question = dr["Question"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = dr["CelebrityId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CelebrityId"];
			}
			return entity;
		}

	}
	
	
}
