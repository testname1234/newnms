﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class GuestRepository: GuestRepositoryBase, IGuestRepository
	{

        public virtual List<Guest> GetAllGuestBySlotId(int SlotId)
        {

            string sql = "select * from [Guest] with (nolock) where IsActive = 1 and SlotId = " + SlotId;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Guest>(ds, GuestFromDataRow);
        }

        public virtual bool DeleteAllGuestBySlotId(System.Int32 SlotId)
        {

            string sql = "delete from Guest where SlotId=@SlotId";
            SqlParameter parameter = new SqlParameter("@SlotId", SlotId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public List<Guest> GetGuestEpisodeReport()
        {
            //string sql = "select c.Name as 'guestName',p.Name as 'programName',g.Question as 'question', cast(g.locationtype as varchar(20)) as 'medium', ep.[From] as 'programDatetime',DATEADD(minute,isnull((select sum(s.Duration) from segment s  with(nolock) where s.EpisodeId=sg.EpisodeId and s.SequnceNumber<sg.SequnceNumber  group by s.EpisodeId),0),ep.[from]) as guestStartTime, cast(sg.Duration as int) as guestduration from Episode ep with(nolock) inner join Segment sg with(nolock) on ep.EpisodeId = sg.EpisodeId inner join Program p with(nolock) on ep.ProgramId = p.ProgramId inner join Slot st with(nolock) on st.SegmentId = sg.SegmentId inner join Guest g with(nolock) on g.SlotId = st.SlotId inner join Celebrity c with(nolock) on c.CelebrityId = g.CelebrityId where ep.[From] > GETUTCDATE()";
            string sql = "select c.Name as 'guestName',gi.value as 'guestContactNumber', p.Name as 'programName',g.Question as 'question', cast(g.locationtype as varchar(20)) as 'medium', ep.[From] as 'programDatetime',DATEADD(minute,isnull((select sum(s.Duration) from segment s  with(nolock) where s.EpisodeId=sg.EpisodeId and s.SequnceNumber<sg.SequnceNumber  group by s.EpisodeId),0),ep.[from]) as guestStartTime,   cast((sg.Duration * 60) as int) as guestduration from Episode   ep with(nolock) inner join Segment sg with(nolock) on   ep.EpisodeId = sg.EpisodeId inner join Program p with(nolock)   on ep.ProgramId = p.ProgramId inner join Slot st with(nolock)    on st.SegmentId = sg.SegmentId inner join Guest g with(nolock)    on g.SlotId = st.SlotId inner join Celebrity c with(nolock) 	on c.CelebrityId = g.CelebrityId	left join guestcontactinfo gi with(nolock) on gi.celebrityid = c.CelebrityId where (gi.contactinfotype = '1' or gi.contactinfotype is null) and ep.[From] > GETUTCDATE()";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Guest>(ds, GuestFromDataRow);
        }


        public override Guest GuestFromDataRow(DataRow dr)
        {
            var entity = base.GuestFromDataRow(dr);
            if (dr.Table.Columns.Contains("guestName"))
            {
                entity.guestName = (string)dr["guestName"];
            }
            if (dr.Table.Columns.Contains("programName"))
            {
                entity.programName = (string)dr["programName"];
            }
            if (dr.Table.Columns.Contains("programDatetime"))
            {
                entity.programDatetime = (DateTime)dr["programDatetime"];
            }
            if (dr.Table.Columns.Contains("guestduration"))
            {
                entity.guestduration = (int)dr["guestduration"];
            }
            if (dr.Table.Columns.Contains("medium"))
            {
                entity.medium = (string)dr["medium"];
            }
            if (dr.Table.Columns.Contains("guestStartTime"))
            {
                entity.guestStartTime = (DateTime)dr["guestStartTime"];
            }
            if (dr.Table.Columns.Contains("guestEndTime"))
            {
                entity.guestEndTime = (DateTime)dr["guestEndTime"];
            }
            if (dr.Table.Columns.Contains("guestContactNumber"))
            {
                if(dr["guestContactNumber"] == DBNull.Value)
                    entity.guestContactNumber = string.Empty;
                
                else
                entity.guestContactNumber = (string)dr["guestContactNumber"];
            }
            return entity;
        }


    }
	
	
}
