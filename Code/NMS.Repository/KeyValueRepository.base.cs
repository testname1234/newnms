﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class KeyValueRepositoryBase : Repository, IKeyValueRepositoryBase 
	{
        
        public KeyValueRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("KeyValueId",new SearchColumn(){Name="KeyValueId",Title="KeyValueId",SelectClause="KeyValueId",WhereClause="AllRecords.KeyValueId",DataType="System.Int32",IsForeignColumn=false,PropertyName="KeyValueId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Key",new SearchColumn(){Name="Key",Title="Key",SelectClause="Key",WhereClause="AllRecords.Key",DataType="System.String",IsForeignColumn=false,PropertyName="Key",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Value",new SearchColumn(){Name="Value",Title="Value",SelectClause="Value",WhereClause="AllRecords.Value",DataType="System.String",IsForeignColumn=false,PropertyName="Value",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetKeyValueSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetKeyValueBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetKeyValueAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetKeyValueSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[KeyValue].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[KeyValue].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual KeyValue GetKeyValue(System.Int32 KeyValueId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetKeyValueSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [KeyValue] with (nolock)  where KeyValueId=@KeyValueId ";
			SqlParameter parameter=new SqlParameter("@KeyValueId",KeyValueId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return KeyValueFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<KeyValue> GetKeyValueByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetKeyValueSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [KeyValue] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<KeyValue>(ds,KeyValueFromDataRow);
		}

		public virtual List<KeyValue> GetAllKeyValue(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetKeyValueSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [KeyValue] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<KeyValue>(ds, KeyValueFromDataRow);
		}

		public virtual List<KeyValue> GetPagedKeyValue(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetKeyValueCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [KeyValueId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([KeyValueId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [KeyValueId] ";
            tempsql += " FROM [KeyValue] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("KeyValueId"))
					tempsql += " , (AllRecords.[KeyValueId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[KeyValueId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetKeyValueSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [KeyValue] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [KeyValue].[KeyValueId] = PageIndex.[KeyValueId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<KeyValue>(ds, KeyValueFromDataRow);
			}else{ return null;}
		}

		private int GetKeyValueCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM KeyValue as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM KeyValue as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(KeyValue))]
		public virtual KeyValue InsertKeyValue(KeyValue entity)
		{

			KeyValue other=new KeyValue();
			other = entity;
				string sql=@"Insert into KeyValue ( [KeyValueId]
				,[Key]
				,[Value] )
				Values
				( @KeyValueId
				, @Key
				, @Value );
";
				SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@KeyValueId",entity.KeyValueId)
					, new SqlParameter("@Key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@Value",entity.Value ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetKeyValue(Convert.ToInt32(identity));
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(KeyValue))]
		public virtual KeyValue UpdateKeyValue(KeyValue entity)
		{

			KeyValue other = GetKeyValue(entity.KeyValueId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update KeyValue set  [Key]=@Key
							, [Value]=@Value 
							 where KeyValueId=@KeyValueId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@KeyValueId",entity.KeyValueId)
					, new SqlParameter("@Key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@Value",entity.Value ?? (object)DBNull.Value)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetKeyValue(entity.KeyValueId);
		}

		public virtual bool DeleteKeyValue(System.Int32 KeyValueId)
		{

			string sql="delete from KeyValue where KeyValueId=@KeyValueId";
			SqlParameter parameter=new SqlParameter("@KeyValueId",KeyValueId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(KeyValue))]
		public virtual KeyValue DeleteKeyValue(KeyValue entity)
		{
			this.DeleteKeyValue(entity.KeyValueId);
			return entity;
		}


		public virtual KeyValue KeyValueFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			KeyValue entity=new KeyValue();
			if (dr.Table.Columns.Contains("KeyValueId"))
			{
			entity.KeyValueId = (System.Int32)dr["KeyValueId"];
			}
			if (dr.Table.Columns.Contains("Key"))
			{
			entity.Key = dr["Key"].ToString();
			}
			if (dr.Table.Columns.Contains("Value"))
			{
			entity.Value = dr["Value"].ToString();
			}
			return entity;
		}

	}
	
	
}
