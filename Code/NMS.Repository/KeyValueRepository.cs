﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;

namespace NMS.Repository
{

    public partial class KeyValueRepository : KeyValueRepositoryBase, IKeyValueRepository
    {
        public string GetValueByKey(string key)
        {
            var sql = "select [Value] from keyvalue where [key] = @Key";
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@Key", key));

            var value = SqlHelper.ExecuteScalar(this.ConnectionString, System.Data.CommandType.Text, sql, param.ToArray());
            return value == DBNull.Value ? null : Convert.ToString(value);
        }

        public bool SetValueByKey(string key, string value)
        {
            var sql = @"if exists (select 1 from KeyValue where [key] = @Key)
begin
    update KeyValue set [Value] = @Value where[key] = @Key
end
else 
begin
    insert into KeyValue( [Key], [Value])
	values(@Key, @Value)
end";
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@Key", key));
            param.Add(new SqlParameter("@Value", value));

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, System.Data.CommandType.Text, sql, param.ToArray()) > 0;
        }
    }


}
