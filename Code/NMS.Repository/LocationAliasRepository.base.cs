﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class LocationAliasRepositoryBase : Repository, ILocationAliasRepositoryBase 
	{
        
        public LocationAliasRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("LocationAliasId",new SearchColumn(){Name="LocationAliasId",Title="LocationAliasId",SelectClause="LocationAliasId",WhereClause="AllRecords.LocationAliasId",DataType="System.Int32",IsForeignColumn=false,PropertyName="LocationAliasId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Alias",new SearchColumn(){Name="Alias",Title="Alias",SelectClause="Alias",WhereClause="AllRecords.Alias",DataType="System.String",IsForeignColumn=false,PropertyName="Alias",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetLocationAliasSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetLocationAliasBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetLocationAliasAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetLocationAliasSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[LocationAlias].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[LocationAlias].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<LocationAlias> GetLocationAliasByLocationId(System.Int32 LocationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetLocationAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from LocationAlias with (nolock)  where LocationId=@LocationId  ";
			SqlParameter parameter=new SqlParameter("@LocationId",LocationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<LocationAlias>(ds,LocationAliasFromDataRow);
		}

		public virtual LocationAlias GetLocationAlias(System.Int32 LocationAliasId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetLocationAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from LocationAlias with (nolock)  where LocationAliasId=@LocationAliasId ";
			SqlParameter parameter=new SqlParameter("@LocationAliasId",LocationAliasId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return LocationAliasFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<LocationAlias> GetLocationAliasByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetLocationAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from LocationAlias with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<LocationAlias>(ds,LocationAliasFromDataRow);
		}

		public virtual List<LocationAlias> GetAllLocationAlias(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetLocationAliasSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from LocationAlias with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<LocationAlias>(ds, LocationAliasFromDataRow);
		}

		public virtual List<LocationAlias> GetPagedLocationAlias(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetLocationAliasCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [LocationAliasId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([LocationAliasId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [LocationAliasId] ";
            tempsql += " FROM [LocationAlias] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("LocationAliasId"))
					tempsql += " , (AllRecords.[LocationAliasId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[LocationAliasId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetLocationAliasSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [LocationAlias] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [LocationAlias].[LocationAliasId] = PageIndex.[LocationAliasId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<LocationAlias>(ds, LocationAliasFromDataRow);
			}else{ return null;}
		}

		private int GetLocationAliasCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM LocationAlias as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM LocationAlias as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}


        

		[MOLog(AuditOperations.Create,typeof(LocationAlias))]
		public virtual LocationAlias InsertLocationAlias(LocationAlias entity)
		{

			LocationAlias other=new LocationAlias();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into LocationAlias ( [LocationId]
				,[Alias]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @LocationId
				, @Alias
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@LocationId",entity.LocationId)
					, new SqlParameter("@Alias",entity.Alias)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetLocationAlias(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(LocationAlias))]
		public virtual LocationAlias UpdateLocationAlias(LocationAlias entity)
		{

			if (entity.IsTransient()) return entity;
			LocationAlias other = GetLocationAlias(entity.LocationAliasId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update LocationAlias set  [LocationId]=@LocationId
							, [Alias]=@Alias
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where LocationAliasId=@LocationAliasId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@LocationId",entity.LocationId)
					, new SqlParameter("@Alias",entity.Alias)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@LocationAliasId",entity.LocationAliasId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetLocationAlias(entity.LocationAliasId);
		}

		public virtual bool DeleteLocationAlias(System.Int32 LocationAliasId)
		{

			string sql="delete from LocationAlias with (nolock) where LocationAliasId=@LocationAliasId";
			SqlParameter parameter=new SqlParameter("@LocationAliasId",LocationAliasId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(LocationAlias))]
		public virtual LocationAlias DeleteLocationAlias(LocationAlias entity)
		{
			this.DeleteLocationAlias(entity.LocationAliasId);
			return entity;
		}


		public virtual LocationAlias LocationAliasFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			LocationAlias entity=new LocationAlias();
			if (dr.Table.Columns.Contains("LocationAliasId"))
			{
			entity.LocationAliasId = (System.Int32)dr["LocationAliasId"];
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = (System.Int32)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("Alias"))
			{
			entity.Alias = dr["Alias"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
