﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class LocationAliasRepository : LocationAliasRepositoryBase, ILocationAliasRepository
    {
         public LocationAliasRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

         public LocationAliasRepository()
        {
        }

        public LocationAlias GetByAlias(string alias)
        {
            string sql = GetLocationAliasSelectClause() + " from LocationAlias with (nolock)  where Alias = @alias";
            SqlParameter parameter = new SqlParameter("@alias", alias);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return LocationAliasFromDataRow(ds.Tables[0].Rows[0]);
        }

        public bool Exists(LocationAlias entity)
        {
            string sql = "select count(1) from LocationAlias with (nolock)  where Alias = @Name";
            SqlParameter parameter = new SqlParameter("@Name", entity.Alias);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter })) > 0;
        }

        public virtual void InsertLocationAliasWithPk(LocationAlias entity)
        {
            string sql = @"set identity_insert LocationAlias on 
                Insert into LocationAlias (LocationAliasId, [LocationId]
				,[Alias]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @LocationAliasId,
                @LocationId
				, @Alias
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
set identity_insert LocationAlias off
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@LocationAliasId",entity.LocationAliasId)
                     ,new SqlParameter("@LocationId",entity.LocationId)
					, new SqlParameter("@Alias",entity.Alias)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");

        }

        public virtual void UpdateLocationAliasNoReturn(LocationAlias entity)
        {
            string sql = @"Update LocationAlias set  [LocationId]=@LocationId
							, [Alias]=@Alias
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where LocationAliasId=@LocationAliasId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@LocationId",entity.LocationId)
					, new SqlParameter("@Alias",entity.Alias)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@LocationAliasId",entity.LocationAliasId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }
    }
}
