﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using System.Linq;

namespace NMS.Repository
{

    public partial class LocationRepository : LocationRepositoryBase, ILocationRepository
    {
        ILocationAliasRepository _iLocationAliasRepository;
        public LocationRepository(ILocationAliasRepository iLocationAliasRepository)
        {
            _iLocationAliasRepository = iLocationAliasRepository;
        }


        public LocationRepository(IConnectionString iConnectionString, ILocationAliasRepository iLocationAliasRepository)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
            _iLocationAliasRepository = iLocationAliasRepository;
        }

        public Location GetLocationCreateIfNotExist(Location location)
        {
            LocationAlias alias = GetByAlias(location.Location);
            if (alias == null)
            {
                Location loc = InsertLocation(location);
                LocationAlias _alias = new LocationAlias();
                _alias.LocationId = loc.LocationId;
                _alias.Alias = loc.Location;
                _alias.CreationDate = loc.CreationDate;
                _alias.LastUpdateDate = _alias.CreationDate;
                _alias.IsActive = true;
                _iLocationAliasRepository.InsertLocationAlias(_alias);
                return loc;
            }
            else
                return GetLocation(alias.LocationId);
        }

        public bool Exist(Category category)
        {
            string sql = "select count(1) from Category with (nolock)  where Category = @Name";
            SqlParameter parameter = new SqlParameter("@Name", category.Category);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter })) > 0;
        }

        public LocationAlias GetByAlias(string alias)
        {
            return _iLocationAliasRepository.GetByAlias(alias);
        }

        public bool Exist(Location location)
        {
            string sql = "select count(1) from Location with (nolock)  where Location = @Name";
            SqlParameter parameter = new SqlParameter("@Name", location.Location);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter })) > 0;
        }

        public Location GetLocationByName(string name)
        {
            string sql = GetLocationSelectClause();
            sql += "from Location with (nolock)  where Location=@Name ";
            SqlParameter parameter = new SqlParameter("@Name", name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return LocationFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<Location> GetLocationByTerm(string term)
        {
            string sql = "select top 10 * from Location with (nolock)  where Location like @Name+'%' and IsApproved=1 and IsSearchable=1";
            SqlParameter parameter = new SqlParameter("@Name", term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Location>(ds, LocationFromDataRow);
        }

        public List<Location> GetByDate(DateTime LastUpdateDate)
        {
            string sql = "select * from Location with (nolock)  where LastUpdateDate >= @LastUpdateDate and IsApproved=1";
            SqlParameter parameter = new SqlParameter("@LastUpdateDate", LastUpdateDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Location>(ds, LocationFromDataRow);
        }

        public Location InsertLocationIfNotExists(Location entity)
        {
            bool isAliasExits = _iLocationAliasRepository.Exists(new LocationAlias { Alias = entity.Location });

            if (!isAliasExits && !Exist(entity))
                return InsertLocation(entity);
            else
                return GetLocationByName(entity.Location);

        }

        public virtual void InsertLocationWithPk(Location entity)
        {
            string sql = @"
set identity_insert Location on 
Insert into Location (LocationId, [Location]
				,[ParentId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsApproved] )
				Values
				( @LocationId,@Location
				, @ParentId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsApproved );
            set identity_insert Location off
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
                      new SqlParameter("@LocationId",entity.LocationId)
                     ,new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
                    , new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
                    , new SqlParameter("@CreationDate",entity.CreationDate)
                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
                    , new SqlParameter("@IsActive",entity.IsActive)
                    , new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual void UpdateLocationNoReturn(Location entity)
        {
            string sql = @"Update Location set  [Location]=@Location
							, [ParentId]=@ParentId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsApproved]=@IsApproved 
							 where LocationId=@LocationId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
                    , new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
                    , new SqlParameter("@CreationDate",entity.CreationDate)
                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
                    , new SqlParameter("@IsActive",entity.IsActive)
                    , new SqlParameter("@IsApproved",entity.IsApproved ?? (object)DBNull.Value)
                    , new SqlParameter("@LocationId",entity.LocationId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public List<int> GetAllChildLocations(int locationId)
        {
            string sql = @"select LocationId from location 
where 
locationid = @locationId 
or locationid in (select locationid from location where ParentId  = @locationId)
or locationid in (select locationid from location where ParentId  in ( select locationid from location where parentid = @locationId))";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@locationId",locationId)};
            var dt = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray).Tables[0];

            var data = dt.AsEnumerable()
                .Select(x => Convert.ToInt32(x["LocationId"])).ToList();
            return data;
        }
    }


}
