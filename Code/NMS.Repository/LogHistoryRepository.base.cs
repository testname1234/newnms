﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class LogHistoryRepositoryBase : Repository, ILogHistoryRepositoryBase 
	{
        
        public LogHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("LogHistoryId",new SearchColumn(){Name="LogHistoryId",Title="LogHistoryId",SelectClause="LogHistoryId",WhereClause="AllRecords.LogHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="LogHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TableId",new SearchColumn(){Name="TableId",Title="TableId",SelectClause="TableId",WhereClause="AllRecords.TableId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TableId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RefrenceId",new SearchColumn(){Name="RefrenceId",Title="RefrenceId",SelectClause="RefrenceId",WhereClause="AllRecords.RefrenceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RefrenceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Method",new SearchColumn(){Name="Method",Title="Method",SelectClause="Method",WhereClause="AllRecords.Method",DataType="System.String",IsForeignColumn=false,PropertyName="Method",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserIp",new SearchColumn(){Name="UserIp",Title="UserIp",SelectClause="UserIp",WhereClause="AllRecords.UserIp",DataType="System.String",IsForeignColumn=false,PropertyName="UserIp",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserType",new SearchColumn(){Name="UserType",Title="UserType",SelectClause="UserType",WhereClause="AllRecords.UserType",DataType="System.String",IsForeignColumn=false,PropertyName="UserType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EntityJson",new SearchColumn(){Name="EntityJson",Title="EntityJson",SelectClause="EntityJson",WhereClause="AllRecords.EntityJson",DataType="System.String",IsForeignColumn=false,PropertyName="EntityJson",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetLogHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetLogHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetLogHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetLogHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[LogHistory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[LogHistory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual LogHistory GetLogHistory(System.Int32 LogHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetLogHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [LogHistory] with (nolock)  where LogHistoryId=@LogHistoryId ";
			SqlParameter parameter=new SqlParameter("@LogHistoryId",LogHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return LogHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<LogHistory> GetLogHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetLogHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [LogHistory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<LogHistory>(ds,LogHistoryFromDataRow);
		}

		public virtual List<LogHistory> GetAllLogHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetLogHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [LogHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<LogHistory>(ds, LogHistoryFromDataRow);
		}

		public virtual List<LogHistory> GetPagedLogHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetLogHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [LogHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([LogHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [LogHistoryId] ";
            tempsql += " FROM [LogHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("LogHistoryId"))
					tempsql += " , (AllRecords.[LogHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[LogHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetLogHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [LogHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [LogHistory].[LogHistoryId] = PageIndex.[LogHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<LogHistory>(ds, LogHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetLogHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM LogHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM LogHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(LogHistory))]
		public virtual LogHistory InsertLogHistory(LogHistory entity)
		{

			LogHistory other=new LogHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into LogHistory ( [TableId]
				,[RefrenceId]
				,[CreationDate]
				,[Method]
				,[UserId]
				,[UserIp]
				,[IsActive]
				,[UserType]
				,[EntityJson] )
				Values
				( @TableId
				, @RefrenceId
				, @CreationDate
				, @Method
				, @UserId
				, @UserIp
				, @IsActive
				, @UserType
				, @EntityJson );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TableId",entity.TableId)
					, new SqlParameter("@RefrenceId",entity.RefrenceId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Method",entity.Method)
					, new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@UserIp",entity.UserIp)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@UserType",entity.UserType ?? (object)DBNull.Value)
					, new SqlParameter("@EntityJson",entity.EntityJson ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetLogHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(LogHistory))]
		public virtual LogHistory UpdateLogHistory(LogHistory entity)
		{

			if (entity.IsTransient()) return entity;
			LogHistory other = GetLogHistory(entity.LogHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update LogHistory set  [TableId]=@TableId
							, [RefrenceId]=@RefrenceId
							, [CreationDate]=@CreationDate
							, [Method]=@Method
							, [UserId]=@UserId
							, [UserIp]=@UserIp
							, [IsActive]=@IsActive
							, [UserType]=@UserType
							, [EntityJson]=@EntityJson 
							 where LogHistoryId=@LogHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TableId",entity.TableId)
					, new SqlParameter("@RefrenceId",entity.RefrenceId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Method",entity.Method)
					, new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@UserIp",entity.UserIp)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@UserType",entity.UserType ?? (object)DBNull.Value)
					, new SqlParameter("@EntityJson",entity.EntityJson ?? (object)DBNull.Value)
					, new SqlParameter("@LogHistoryId",entity.LogHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetLogHistory(entity.LogHistoryId);
		}

		public virtual bool DeleteLogHistory(System.Int32 LogHistoryId)
		{

			string sql="delete from LogHistory where LogHistoryId=@LogHistoryId";
			SqlParameter parameter=new SqlParameter("@LogHistoryId",LogHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(LogHistory))]
		public virtual LogHistory DeleteLogHistory(LogHistory entity)
		{
			this.DeleteLogHistory(entity.LogHistoryId);
			return entity;
		}


		public virtual LogHistory LogHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			LogHistory entity=new LogHistory();
			if (dr.Table.Columns.Contains("LogHistoryId"))
			{
			entity.LogHistoryId = (System.Int32)dr["LogHistoryId"];
			}
			if (dr.Table.Columns.Contains("TableId"))
			{
			entity.TableId = (System.Int32)dr["TableId"];
			}
			if (dr.Table.Columns.Contains("RefrenceId"))
			{
			entity.RefrenceId = (System.Int32)dr["RefrenceId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("Method"))
			{
			entity.Method = dr["Method"].ToString();
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = (System.Int32)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("UserIp"))
			{
			entity.UserIp = dr["UserIp"].ToString();
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("UserType"))
			{
			entity.UserType = dr["UserType"].ToString();
			}
			if (dr.Table.Columns.Contains("EntityJson"))
			{
			entity.EntityJson = dr["EntityJson"].ToString();
			}
			return entity;
		}

	}
	
	
}
