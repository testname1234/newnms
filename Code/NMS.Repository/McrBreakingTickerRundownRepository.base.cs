﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class McrBreakingTickerRundownRepositoryBase : Repository, IMcrBreakingTickerRundownRepositoryBase 
	{
        
        public McrBreakingTickerRundownRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MCRBreakingTickerRundownId",new SearchColumn(){Name="MCRBreakingTickerRundownId",Title="MCRBreakingTickerRundownId",SelectClause="MCRBreakingTickerRundownId",WhereClause="AllRecords.MCRBreakingTickerRundownId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrBreakingTickerRundownId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceNumber",new SearchColumn(){Name="SequenceNumber",Title="SequenceNumber",SelectClause="SequenceNumber",WhereClause="AllRecords.SequenceNumber",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SequenceNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerLineId",new SearchColumn(){Name="TickerLineId",Title="TickerLineId",SelectClause="TickerLineId",WhereClause="AllRecords.TickerLineId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerLineId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMcrBreakingTickerRundownSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMcrBreakingTickerRundownBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMcrBreakingTickerRundownAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMcrBreakingTickerRundownSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MCRBreakingTickerRundown].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MCRBreakingTickerRundown].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual McrBreakingTickerRundown GetMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrBreakingTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRBreakingTickerRundown] with (nolock)  where MCRBreakingTickerRundownId=@MCRBreakingTickerRundownId ";
			SqlParameter parameter=new SqlParameter("@MCRBreakingTickerRundownId",McrBreakingTickerRundownId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return McrBreakingTickerRundownFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<McrBreakingTickerRundown> GetMcrBreakingTickerRundownByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrBreakingTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MCRBreakingTickerRundown] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrBreakingTickerRundown>(ds,McrBreakingTickerRundownFromDataRow);
		}

		public virtual List<McrBreakingTickerRundown> GetAllMcrBreakingTickerRundown(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrBreakingTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRBreakingTickerRundown] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrBreakingTickerRundown>(ds, McrBreakingTickerRundownFromDataRow);
		}

		public virtual List<McrBreakingTickerRundown> GetPagedMcrBreakingTickerRundown(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMcrBreakingTickerRundownCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MCRBreakingTickerRundownId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MCRBreakingTickerRundownId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MCRBreakingTickerRundownId] ";
            tempsql += " FROM [MCRBreakingTickerRundown] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MCRBreakingTickerRundownId"))
					tempsql += " , (AllRecords.[MCRBreakingTickerRundownId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MCRBreakingTickerRundownId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMcrBreakingTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MCRBreakingTickerRundown] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MCRBreakingTickerRundown].[MCRBreakingTickerRundownId] = PageIndex.[MCRBreakingTickerRundownId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrBreakingTickerRundown>(ds, McrBreakingTickerRundownFromDataRow);
			}else{ return null;}
		}

		private int GetMcrBreakingTickerRundownCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MCRBreakingTickerRundown as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MCRBreakingTickerRundown as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(McrBreakingTickerRundown))]
		public virtual McrBreakingTickerRundown InsertMcrBreakingTickerRundown(McrBreakingTickerRundown entity)
		{

			McrBreakingTickerRundown other=new McrBreakingTickerRundown();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MCRBreakingTickerRundown ( [TickerId]
				,[Text]
				,[SequenceNumber]
				,[LanguageCode]
				,[CreationDate]
				,[TickerLineId] )
				Values
				( @TickerId
				, @Text
				, @SequenceNumber
				, @LanguageCode
				, @CreationDate
				, @TickerLineId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMcrBreakingTickerRundown(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(McrBreakingTickerRundown))]
		public virtual McrBreakingTickerRundown UpdateMcrBreakingTickerRundown(McrBreakingTickerRundown entity)
		{

			if (entity.IsTransient()) return entity;
			McrBreakingTickerRundown other = GetMcrBreakingTickerRundown(entity.McrBreakingTickerRundownId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MCRBreakingTickerRundown set  [TickerId]=@TickerId
							, [Text]=@Text
							, [SequenceNumber]=@SequenceNumber
							, [LanguageCode]=@LanguageCode
							, [CreationDate]=@CreationDate
							, [TickerLineId]=@TickerLineId 
							 where MCRBreakingTickerRundownId=@MCRBreakingTickerRundownId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)
					, new SqlParameter("@MCRBreakingTickerRundownId",entity.McrBreakingTickerRundownId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMcrBreakingTickerRundown(entity.McrBreakingTickerRundownId);
		}

		public virtual bool DeleteMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId)
		{

			string sql="delete from MCRBreakingTickerRundown where MCRBreakingTickerRundownId=@MCRBreakingTickerRundownId";
			SqlParameter parameter=new SqlParameter("@MCRBreakingTickerRundownId",McrBreakingTickerRundownId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(McrBreakingTickerRundown))]
		public virtual McrBreakingTickerRundown DeleteMcrBreakingTickerRundown(McrBreakingTickerRundown entity)
		{
			this.DeleteMcrBreakingTickerRundown(entity.McrBreakingTickerRundownId);
			return entity;
		}


		public virtual McrBreakingTickerRundown McrBreakingTickerRundownFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			McrBreakingTickerRundown entity=new McrBreakingTickerRundown();
			if (dr.Table.Columns.Contains("MCRBreakingTickerRundownId"))
			{
			entity.McrBreakingTickerRundownId = (System.Int32)dr["MCRBreakingTickerRundownId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = dr["TickerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("SequenceNumber"))
			{
			entity.SequenceNumber = dr["SequenceNumber"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SequenceNumber"];
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("TickerLineId"))
			{
			entity.TickerLineId = dr["TickerLineId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerLineId"];
			}
			return entity;
		}

	}
	
	
}
