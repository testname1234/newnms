﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class McrCategoryTickerRundownRepositoryBase : Repository, IMcrCategoryTickerRundownRepositoryBase 
	{
        
        public McrCategoryTickerRundownRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MCRCategoryTickerRundownId",new SearchColumn(){Name="MCRCategoryTickerRundownId",Title="MCRCategoryTickerRundownId",SelectClause="MCRCategoryTickerRundownId",WhereClause="AllRecords.MCRCategoryTickerRundownId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrCategoryTickerRundownId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryName",new SearchColumn(){Name="CategoryName",Title="CategoryName",SelectClause="CategoryName",WhereClause="AllRecords.CategoryName",DataType="System.String",IsForeignColumn=false,PropertyName="CategoryName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceNumber",new SearchColumn(){Name="SequenceNumber",Title="SequenceNumber",SelectClause="SequenceNumber",WhereClause="AllRecords.SequenceNumber",DataType="System.Int32",IsForeignColumn=false,PropertyName="SequenceNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerLineId",new SearchColumn(){Name="TickerLineId",Title="TickerLineId",SelectClause="TickerLineId",WhereClause="AllRecords.TickerLineId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerLineId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMcrCategoryTickerRundownSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMcrCategoryTickerRundownBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMcrCategoryTickerRundownAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMcrCategoryTickerRundownSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MCRCategoryTickerRundown].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MCRCategoryTickerRundown].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual McrCategoryTickerRundown GetMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrCategoryTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRCategoryTickerRundown] with (nolock)  where MCRCategoryTickerRundownId=@MCRCategoryTickerRundownId ";
			SqlParameter parameter=new SqlParameter("@MCRCategoryTickerRundownId",McrCategoryTickerRundownId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return McrCategoryTickerRundownFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<McrCategoryTickerRundown> GetMcrCategoryTickerRundownByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrCategoryTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MCRCategoryTickerRundown] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrCategoryTickerRundown>(ds,McrCategoryTickerRundownFromDataRow);
		}

		public virtual List<McrCategoryTickerRundown> GetAllMcrCategoryTickerRundown(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrCategoryTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRCategoryTickerRundown] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrCategoryTickerRundown>(ds, McrCategoryTickerRundownFromDataRow);
		}

		public virtual List<McrCategoryTickerRundown> GetPagedMcrCategoryTickerRundown(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMcrCategoryTickerRundownCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MCRCategoryTickerRundownId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MCRCategoryTickerRundownId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MCRCategoryTickerRundownId] ";
            tempsql += " FROM [MCRCategoryTickerRundown] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MCRCategoryTickerRundownId"))
					tempsql += " , (AllRecords.[MCRCategoryTickerRundownId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MCRCategoryTickerRundownId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMcrCategoryTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MCRCategoryTickerRundown] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MCRCategoryTickerRundown].[MCRCategoryTickerRundownId] = PageIndex.[MCRCategoryTickerRundownId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrCategoryTickerRundown>(ds, McrCategoryTickerRundownFromDataRow);
			}else{ return null;}
		}

		private int GetMcrCategoryTickerRundownCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MCRCategoryTickerRundown as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MCRCategoryTickerRundown as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(McrCategoryTickerRundown))]
		public virtual McrCategoryTickerRundown InsertMcrCategoryTickerRundown(McrCategoryTickerRundown entity)
		{

			McrCategoryTickerRundown other=new McrCategoryTickerRundown();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MCRCategoryTickerRundown ( [TickerId]
				,[Text]
				,[CategoryName]
				,[CategoryId]
				,[SequenceNumber]
				,[LanguageCode]
				,[CreationDate]
				,[TickerLineId] )
				Values
				( @TickerId
				, @Text
				, @CategoryName
				, @CategoryId
				, @SequenceNumber
				, @LanguageCode
				, @CreationDate
				, @TickerLineId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@CategoryName",entity.CategoryName)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMcrCategoryTickerRundown(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(McrCategoryTickerRundown))]
		public virtual McrCategoryTickerRundown UpdateMcrCategoryTickerRundown(McrCategoryTickerRundown entity)
		{

			if (entity.IsTransient()) return entity;
			McrCategoryTickerRundown other = GetMcrCategoryTickerRundown(entity.McrCategoryTickerRundownId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MCRCategoryTickerRundown set  [TickerId]=@TickerId
							, [Text]=@Text
							, [CategoryName]=@CategoryName
							, [CategoryId]=@CategoryId
							, [SequenceNumber]=@SequenceNumber
							, [LanguageCode]=@LanguageCode
							, [CreationDate]=@CreationDate
							, [TickerLineId]=@TickerLineId 
							 where MCRCategoryTickerRundownId=@MCRCategoryTickerRundownId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@CategoryName",entity.CategoryName)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)
					, new SqlParameter("@MCRCategoryTickerRundownId",entity.McrCategoryTickerRundownId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMcrCategoryTickerRundown(entity.McrCategoryTickerRundownId);
		}

		public virtual bool DeleteMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId)
		{

			string sql="delete from MCRCategoryTickerRundown where MCRCategoryTickerRundownId=@MCRCategoryTickerRundownId";
			SqlParameter parameter=new SqlParameter("@MCRCategoryTickerRundownId",McrCategoryTickerRundownId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(McrCategoryTickerRundown))]
		public virtual McrCategoryTickerRundown DeleteMcrCategoryTickerRundown(McrCategoryTickerRundown entity)
		{
			this.DeleteMcrCategoryTickerRundown(entity.McrCategoryTickerRundownId);
			return entity;
		}


		public virtual McrCategoryTickerRundown McrCategoryTickerRundownFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			McrCategoryTickerRundown entity=new McrCategoryTickerRundown();
			if (dr.Table.Columns.Contains("MCRCategoryTickerRundownId"))
			{
			entity.McrCategoryTickerRundownId = (System.Int32)dr["MCRCategoryTickerRundownId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = (System.Int32)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("CategoryName"))
			{
			entity.CategoryName = dr["CategoryName"].ToString();
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = (System.Int32)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("SequenceNumber"))
			{
			entity.SequenceNumber = (System.Int32)dr["SequenceNumber"];
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("TickerLineId"))
			{
			entity.TickerLineId = dr["TickerLineId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerLineId"];
			}
			return entity;
		}

	}
	
	
}
