﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class McrLatestTickerRundownRepositoryBase : Repository, IMcrLatestTickerRundownRepositoryBase 
	{
        
        public McrLatestTickerRundownRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MCRLatestTickerRundownId",new SearchColumn(){Name="MCRLatestTickerRundownId",Title="MCRLatestTickerRundownId",SelectClause="MCRLatestTickerRundownId",WhereClause="AllRecords.MCRLatestTickerRundownId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrLatestTickerRundownId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceNumber",new SearchColumn(){Name="SequenceNumber",Title="SequenceNumber",SelectClause="SequenceNumber",WhereClause="AllRecords.SequenceNumber",DataType="System.Int32",IsForeignColumn=false,PropertyName="SequenceNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerLineId",new SearchColumn(){Name="TickerLineId",Title="TickerLineId",SelectClause="TickerLineId",WhereClause="AllRecords.TickerLineId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerLineId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMcrLatestTickerRundownSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMcrLatestTickerRundownBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMcrLatestTickerRundownAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMcrLatestTickerRundownSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MCRLatestTickerRundown].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MCRLatestTickerRundown].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual McrLatestTickerRundown GetMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrLatestTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRLatestTickerRundown] with (nolock)  where MCRLatestTickerRundownId=@MCRLatestTickerRundownId ";
			SqlParameter parameter=new SqlParameter("@MCRLatestTickerRundownId",McrLatestTickerRundownId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return McrLatestTickerRundownFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<McrLatestTickerRundown> GetMcrLatestTickerRundownByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrLatestTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MCRLatestTickerRundown] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrLatestTickerRundown>(ds,McrLatestTickerRundownFromDataRow);
		}

		public virtual List<McrLatestTickerRundown> GetAllMcrLatestTickerRundown(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrLatestTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRLatestTickerRundown] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrLatestTickerRundown>(ds, McrLatestTickerRundownFromDataRow);
		}

		public virtual List<McrLatestTickerRundown> GetPagedMcrLatestTickerRundown(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMcrLatestTickerRundownCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MCRLatestTickerRundownId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MCRLatestTickerRundownId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MCRLatestTickerRundownId] ";
            tempsql += " FROM [MCRLatestTickerRundown] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MCRLatestTickerRundownId"))
					tempsql += " , (AllRecords.[MCRLatestTickerRundownId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MCRLatestTickerRundownId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMcrLatestTickerRundownSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MCRLatestTickerRundown] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MCRLatestTickerRundown].[MCRLatestTickerRundownId] = PageIndex.[MCRLatestTickerRundownId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrLatestTickerRundown>(ds, McrLatestTickerRundownFromDataRow);
			}else{ return null;}
		}

		private int GetMcrLatestTickerRundownCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MCRLatestTickerRundown as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MCRLatestTickerRundown as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(McrLatestTickerRundown))]
		public virtual McrLatestTickerRundown InsertMcrLatestTickerRundown(McrLatestTickerRundown entity)
		{

			McrLatestTickerRundown other=new McrLatestTickerRundown();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MCRLatestTickerRundown ( [TickerId]
				,[Text]
				,[SequenceNumber]
				,[LanguageCode]
				,[CreationDate]
				,[TickerLineId] )
				Values
				( @TickerId
				, @Text
				, @SequenceNumber
				, @LanguageCode
				, @CreationDate
				, @TickerLineId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMcrLatestTickerRundown(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(McrLatestTickerRundown))]
		public virtual McrLatestTickerRundown UpdateMcrLatestTickerRundown(McrLatestTickerRundown entity)
		{

			if (entity.IsTransient()) return entity;
			McrLatestTickerRundown other = GetMcrLatestTickerRundown(entity.McrLatestTickerRundownId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MCRLatestTickerRundown set  [TickerId]=@TickerId
							, [Text]=@Text
							, [SequenceNumber]=@SequenceNumber
							, [LanguageCode]=@LanguageCode
							, [CreationDate]=@CreationDate
							, [TickerLineId]=@TickerLineId 
							 where MCRLatestTickerRundownId=@MCRLatestTickerRundownId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)
					, new SqlParameter("@MCRLatestTickerRundownId",entity.McrLatestTickerRundownId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMcrLatestTickerRundown(entity.McrLatestTickerRundownId);
		}

		public virtual bool DeleteMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId)
		{

			string sql="delete from MCRLatestTickerRundown where MCRLatestTickerRundownId=@MCRLatestTickerRundownId";
			SqlParameter parameter=new SqlParameter("@MCRLatestTickerRundownId",McrLatestTickerRundownId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(McrLatestTickerRundown))]
		public virtual McrLatestTickerRundown DeleteMcrLatestTickerRundown(McrLatestTickerRundown entity)
		{
			this.DeleteMcrLatestTickerRundown(entity.McrLatestTickerRundownId);
			return entity;
		}


		public virtual McrLatestTickerRundown McrLatestTickerRundownFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			McrLatestTickerRundown entity=new McrLatestTickerRundown();
			if (dr.Table.Columns.Contains("MCRLatestTickerRundownId"))
			{
			entity.McrLatestTickerRundownId = (System.Int32)dr["MCRLatestTickerRundownId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = (System.Int32)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("SequenceNumber"))
			{
			entity.SequenceNumber = (System.Int32)dr["SequenceNumber"];
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("TickerLineId"))
			{
			entity.TickerLineId = dr["TickerLineId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerLineId"];
			}
			return entity;
		}

	}
	
	
}
