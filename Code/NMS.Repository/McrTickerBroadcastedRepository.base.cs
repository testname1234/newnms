﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class McrTickerBroadcastedRepositoryBase : Repository, IMcrTickerBroadcastedRepositoryBase 
	{
        
        public McrTickerBroadcastedRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("McrTickerBroadcastedId",new SearchColumn(){Name="McrTickerBroadcastedId",Title="McrTickerBroadcastedId",SelectClause="McrTickerBroadcastedId",WhereClause="AllRecords.McrTickerBroadcastedId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrTickerBroadcastedId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Hour",new SearchColumn(){Name="Hour",Title="Hour",SelectClause="Hour",WhereClause="AllRecords.Hour",DataType="System.Int32",IsForeignColumn=false,PropertyName="Hour",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerCategoryId",new SearchColumn(){Name="TickerCategoryId",Title="TickerCategoryId",SelectClause="TickerCategoryId",WhereClause="AllRecords.TickerCategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerCategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Limit",new SearchColumn(){Name="Limit",Title="Limit",SelectClause="Limit",WhereClause="AllRecords.Limit",DataType="System.Int32",IsForeignColumn=false,PropertyName="Limit",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Broadcasted",new SearchColumn(){Name="Broadcasted",Title="Broadcasted",SelectClause="Broadcasted",WhereClause="AllRecords.Broadcasted",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Broadcasted",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMcrTickerBroadcastedSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMcrTickerBroadcastedBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMcrTickerBroadcastedAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMcrTickerBroadcastedSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[McrTickerBroadcasted].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[McrTickerBroadcasted].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<McrTickerBroadcasted> GetMcrTickerBroadcastedByTickerCategoryId(System.Int32 TickerCategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerBroadcastedSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [McrTickerBroadcasted] with (nolock)  where TickerCategoryId=@TickerCategoryId  ";
			SqlParameter parameter=new SqlParameter("@TickerCategoryId",TickerCategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerBroadcasted>(ds,McrTickerBroadcastedFromDataRow);
		}

		public virtual McrTickerBroadcasted GetMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerBroadcastedSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [McrTickerBroadcasted] with (nolock)  where McrTickerBroadcastedId=@McrTickerBroadcastedId ";
			SqlParameter parameter=new SqlParameter("@McrTickerBroadcastedId",McrTickerBroadcastedId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return McrTickerBroadcastedFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<McrTickerBroadcasted> GetMcrTickerBroadcastedByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerBroadcastedSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [McrTickerBroadcasted] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerBroadcasted>(ds,McrTickerBroadcastedFromDataRow);
		}

		public virtual List<McrTickerBroadcasted> GetAllMcrTickerBroadcasted(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerBroadcastedSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [McrTickerBroadcasted] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerBroadcasted>(ds, McrTickerBroadcastedFromDataRow);
		}

		public virtual List<McrTickerBroadcasted> GetPagedMcrTickerBroadcasted(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMcrTickerBroadcastedCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [McrTickerBroadcastedId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([McrTickerBroadcastedId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [McrTickerBroadcastedId] ";
            tempsql += " FROM [McrTickerBroadcasted] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("McrTickerBroadcastedId"))
					tempsql += " , (AllRecords.[McrTickerBroadcastedId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[McrTickerBroadcastedId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMcrTickerBroadcastedSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [McrTickerBroadcasted] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [McrTickerBroadcasted].[McrTickerBroadcastedId] = PageIndex.[McrTickerBroadcastedId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerBroadcasted>(ds, McrTickerBroadcastedFromDataRow);
			}else{ return null;}
		}

		private int GetMcrTickerBroadcastedCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM McrTickerBroadcasted as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM McrTickerBroadcasted as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(McrTickerBroadcasted))]
		public virtual McrTickerBroadcasted InsertMcrTickerBroadcasted(McrTickerBroadcasted entity)
		{

			McrTickerBroadcasted other=new McrTickerBroadcasted();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into McrTickerBroadcasted ( [Hour]
				,[TickerCategoryId]
				,[Limit]
				,[Broadcasted]
				,[CreationDate] )
				Values
				( @Hour
				, @TickerCategoryId
				, @Limit
				, @Broadcasted
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Hour",entity.Hour)
					, new SqlParameter("@TickerCategoryId",entity.TickerCategoryId)
					, new SqlParameter("@Limit",entity.Limit)
					, new SqlParameter("@Broadcasted",entity.Broadcasted ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMcrTickerBroadcasted(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(McrTickerBroadcasted))]
		public virtual McrTickerBroadcasted UpdateMcrTickerBroadcasted(McrTickerBroadcasted entity)
		{

			if (entity.IsTransient()) return entity;
			McrTickerBroadcasted other = GetMcrTickerBroadcasted(entity.McrTickerBroadcastedId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update McrTickerBroadcasted set  [Hour]=@Hour
							, [TickerCategoryId]=@TickerCategoryId
							, [Limit]=@Limit
							, [Broadcasted]=@Broadcasted
							, [CreationDate]=@CreationDate 
							 where McrTickerBroadcastedId=@McrTickerBroadcastedId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Hour",entity.Hour)
					, new SqlParameter("@TickerCategoryId",entity.TickerCategoryId)
					, new SqlParameter("@Limit",entity.Limit)
					, new SqlParameter("@Broadcasted",entity.Broadcasted ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@McrTickerBroadcastedId",entity.McrTickerBroadcastedId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMcrTickerBroadcasted(entity.McrTickerBroadcastedId);
		}

		public virtual bool DeleteMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId)
		{

			string sql="delete from McrTickerBroadcasted where McrTickerBroadcastedId=@McrTickerBroadcastedId";
			SqlParameter parameter=new SqlParameter("@McrTickerBroadcastedId",McrTickerBroadcastedId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(McrTickerBroadcasted))]
		public virtual McrTickerBroadcasted DeleteMcrTickerBroadcasted(McrTickerBroadcasted entity)
		{
			this.DeleteMcrTickerBroadcasted(entity.McrTickerBroadcastedId);
			return entity;
		}


		public virtual McrTickerBroadcasted McrTickerBroadcastedFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			McrTickerBroadcasted entity=new McrTickerBroadcasted();
			if (dr.Table.Columns.Contains("McrTickerBroadcastedId"))
			{
			entity.McrTickerBroadcastedId = (System.Int32)dr["McrTickerBroadcastedId"];
			}
			if (dr.Table.Columns.Contains("Hour"))
			{
			entity.Hour = (System.Int32)dr["Hour"];
			}
			if (dr.Table.Columns.Contains("TickerCategoryId"))
			{
			entity.TickerCategoryId = (System.Int32)dr["TickerCategoryId"];
			}
			if (dr.Table.Columns.Contains("Limit"))
			{
			entity.Limit = (System.Int32)dr["Limit"];
			}
			if (dr.Table.Columns.Contains("Broadcasted"))
			{
			entity.Broadcasted = dr["Broadcasted"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Broadcasted"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
