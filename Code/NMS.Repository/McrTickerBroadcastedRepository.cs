﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class McrTickerBroadcastedRepository: McrTickerBroadcastedRepositoryBase, IMcrTickerBroadcastedRepository
	{
        public List<McrTickerBroadcasted> GetMcrTickerBroadcastedByDate(DateTime From, DateTime To)
        {   
            string sql = "select mc.*,tc.Name  from McrTickerBroadcasted mc inner join TickerCategory tc on mc.TickerCategoryId = tc.TickerCategoryId where mc.creationdate > @From and mc.creationdate <= @To";
            SqlParameter parameter1 = new SqlParameter("@From", From);
            SqlParameter parameter2 = new SqlParameter("@To", To);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<McrTickerBroadcasted>(ds, McrTickerBroadcastedFromDataRow);
        }


        public override McrTickerBroadcasted McrTickerBroadcastedFromDataRow(DataRow dr)
        {
            var entity = base.McrTickerBroadcastedFromDataRow(dr);
            if (dr.Table.Columns.Contains("Name"))
            {
                entity.Name = (string)dr["Name"];
            }
           
            return entity;
        }


    }
	
	
}
