﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class McrTickerHistoryRepositoryBase : Repository, IMcrTickerHistoryRepositoryBase 
	{
        
        public McrTickerHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("McrTickerHistoryId",new SearchColumn(){Name="McrTickerHistoryId",Title="McrTickerHistoryId",SelectClause="McrTickerHistoryId",WhereClause="AllRecords.McrTickerHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrTickerHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerCategoryId",new SearchColumn(){Name="TickerCategoryId",Title="TickerCategoryId",SelectClause="TickerCategoryId",WhereClause="AllRecords.TickerCategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerCategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("McrTickerBroadcastedId",new SearchColumn(){Name="McrTickerBroadcastedId",Title="McrTickerBroadcastedId",SelectClause="McrTickerBroadcastedId",WhereClause="AllRecords.McrTickerBroadcastedId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrTickerBroadcastedId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMcrTickerHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMcrTickerHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMcrTickerHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMcrTickerHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[McrTickerHistory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[McrTickerHistory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<McrTickerHistory> GetMcrTickerHistoryByTickerId(System.Int32? TickerId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [McrTickerHistory] with (nolock)  where TickerId=@TickerId  ";
			SqlParameter parameter=new SqlParameter("@TickerId",TickerId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerHistory>(ds,McrTickerHistoryFromDataRow);
		}

		public virtual List<McrTickerHistory> GetMcrTickerHistoryByTickerCategoryId(System.Int32? TickerCategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [McrTickerHistory] with (nolock)  where TickerCategoryId=@TickerCategoryId  ";
			SqlParameter parameter=new SqlParameter("@TickerCategoryId",TickerCategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerHistory>(ds,McrTickerHistoryFromDataRow);
		}

		public virtual McrTickerHistory GetMcrTickerHistory(System.Int32 McrTickerHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [McrTickerHistory] with (nolock)  where McrTickerHistoryId=@McrTickerHistoryId ";
			SqlParameter parameter=new SqlParameter("@McrTickerHistoryId",McrTickerHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return McrTickerHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<McrTickerHistory> GetMcrTickerHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [McrTickerHistory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerHistory>(ds,McrTickerHistoryFromDataRow);
		}

		public virtual List<McrTickerHistory> GetAllMcrTickerHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [McrTickerHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerHistory>(ds, McrTickerHistoryFromDataRow);
		}

		public virtual List<McrTickerHistory> GetPagedMcrTickerHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMcrTickerHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [McrTickerHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([McrTickerHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [McrTickerHistoryId] ";
            tempsql += " FROM [McrTickerHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("McrTickerHistoryId"))
					tempsql += " , (AllRecords.[McrTickerHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[McrTickerHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMcrTickerHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [McrTickerHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [McrTickerHistory].[McrTickerHistoryId] = PageIndex.[McrTickerHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerHistory>(ds, McrTickerHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetMcrTickerHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM McrTickerHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM McrTickerHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(McrTickerHistory))]
		public virtual McrTickerHistory InsertMcrTickerHistory(McrTickerHistory entity)
		{

			McrTickerHistory other=new McrTickerHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into McrTickerHistory ( [TickerId]
				,[Text]
				,[TickerCategoryId]
				,[CreationDate]
				,[McrTickerBroadcastedId]
				,[Type] )
				Values
				( @TickerId
				, @Text
				, @TickerCategoryId
				, @CreationDate
				, @McrTickerBroadcastedId
				, @Type );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@TickerCategoryId",entity.TickerCategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@McrTickerBroadcastedId",entity.McrTickerBroadcastedId)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMcrTickerHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(McrTickerHistory))]
		public virtual McrTickerHistory UpdateMcrTickerHistory(McrTickerHistory entity)
		{

			if (entity.IsTransient()) return entity;
			McrTickerHistory other = GetMcrTickerHistory(entity.McrTickerHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update McrTickerHistory set  [TickerId]=@TickerId
							, [Text]=@Text
							, [TickerCategoryId]=@TickerCategoryId
							, [CreationDate]=@CreationDate
							, [McrTickerBroadcastedId]=@McrTickerBroadcastedId
							, [Type]=@Type 
							 where McrTickerHistoryId=@McrTickerHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@Text",entity.Text)
					, new SqlParameter("@TickerCategoryId",entity.TickerCategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@McrTickerBroadcastedId",entity.McrTickerBroadcastedId)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@McrTickerHistoryId",entity.McrTickerHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMcrTickerHistory(entity.McrTickerHistoryId);
		}

		public virtual bool DeleteMcrTickerHistory(System.Int32 McrTickerHistoryId)
		{

			string sql="delete from McrTickerHistory where McrTickerHistoryId=@McrTickerHistoryId";
			SqlParameter parameter=new SqlParameter("@McrTickerHistoryId",McrTickerHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(McrTickerHistory))]
		public virtual McrTickerHistory DeleteMcrTickerHistory(McrTickerHistory entity)
		{
			this.DeleteMcrTickerHistory(entity.McrTickerHistoryId);
			return entity;
		}


		public virtual McrTickerHistory McrTickerHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			McrTickerHistory entity=new McrTickerHistory();
			if (dr.Table.Columns.Contains("McrTickerHistoryId"))
			{
			entity.McrTickerHistoryId = (System.Int32)dr["McrTickerHistoryId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = dr["TickerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("TickerCategoryId"))
			{
			entity.TickerCategoryId = dr["TickerCategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerCategoryId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("McrTickerBroadcastedId"))
			{
			entity.McrTickerBroadcastedId = (System.Int32)dr["McrTickerBroadcastedId"];
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"].ToString();
			}
			return entity;
		}

	}
	
	
}
