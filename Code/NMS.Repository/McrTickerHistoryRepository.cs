﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class McrTickerHistoryRepository : McrTickerHistoryRepositoryBase, IMcrTickerHistoryRepository
    {
        public List<McrTickerHistory> GetLastBroadcastedTickers(int tickerCategoryId, int size, List<int> tickerIds)
        {
            var sql = $@"
				select distinct top (@Size) mcrh.tickerid, max(mcrh.[text]) [Text], max(mcrh.creationdate) [creationdate] from [McrTickerHistory] mcrh with (nolock) 
                join Ticker t with (nolock) on t.tickerid = mcrh.tickerid and t.Isactive = 1
                 where mcrh.TickerCategoryId = @TickerCategoryId
                {(tickerIds.Count > 0 ? "and mcrh.tickerid not in (" + string.Join(",", tickerIds) + ")" : "")}
				group by mcrh.tickerid
				order by max(mcrh.creationdate) desc";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerCategoryId",tickerCategoryId),
                new SqlParameter("@Size",size)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<McrTickerHistory>();
            return CollectionFromDataSet<McrTickerHistory>(ds, McrTickerHistoryFromDataRow);
        }

        public List<McrTickerHistory> GetMcrTickerHistoryByBroadcastedId(int Id)
        {
            string sql = "select * from [McrTickerHistory] with (nolock)  where McrTickerBroadcastedId=@TickerBroadcastedId  ";
            SqlParameter parameter = new SqlParameter("@TickerBroadcastedId", Id);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<McrTickerHistory>(ds, McrTickerHistoryFromDataRow);
        }

        
    }


}
