﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class McrTickerRundownStatusRepositoryBase : Repository, IMcrTickerRundownStatusRepositoryBase 
	{
        
        public McrTickerRundownStatusRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MCRTickerRundownStatusId",new SearchColumn(){Name="MCRTickerRundownStatusId",Title="MCRTickerRundownStatusId",SelectClause="MCRTickerRundownStatusId",WhereClause="AllRecords.MCRTickerRundownStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrTickerRundownStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMcrTickerRundownStatusSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMcrTickerRundownStatusBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMcrTickerRundownStatusAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMcrTickerRundownStatusSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MCRTickerRundownStatus].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MCRTickerRundownStatus].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<McrTickerRundownStatus> GetMcrTickerRundownStatusByStatusId(System.Int32 StatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerRundownStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRTickerRundownStatus] with (nolock)  where StatusId=@StatusId  ";
			SqlParameter parameter=new SqlParameter("@StatusId",StatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerRundownStatus>(ds,McrTickerRundownStatusFromDataRow);
		}

		public virtual McrTickerRundownStatus GetMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerRundownStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRTickerRundownStatus] with (nolock)  where MCRTickerRundownStatusId=@MCRTickerRundownStatusId ";
			SqlParameter parameter=new SqlParameter("@MCRTickerRundownStatusId",McrTickerRundownStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return McrTickerRundownStatusFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<McrTickerRundownStatus> GetMcrTickerRundownStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerRundownStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MCRTickerRundownStatus] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerRundownStatus>(ds,McrTickerRundownStatusFromDataRow);
		}

		public virtual List<McrTickerRundownStatus> GetAllMcrTickerRundownStatus(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerRundownStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRTickerRundownStatus] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerRundownStatus>(ds, McrTickerRundownStatusFromDataRow);
		}

		public virtual List<McrTickerRundownStatus> GetPagedMcrTickerRundownStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMcrTickerRundownStatusCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MCRTickerRundownStatusId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MCRTickerRundownStatusId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MCRTickerRundownStatusId] ";
            tempsql += " FROM [MCRTickerRundownStatus] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MCRTickerRundownStatusId"))
					tempsql += " , (AllRecords.[MCRTickerRundownStatusId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MCRTickerRundownStatusId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMcrTickerRundownStatusSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MCRTickerRundownStatus] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MCRTickerRundownStatus].[MCRTickerRundownStatusId] = PageIndex.[MCRTickerRundownStatusId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerRundownStatus>(ds, McrTickerRundownStatusFromDataRow);
			}else{ return null;}
		}

		private int GetMcrTickerRundownStatusCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MCRTickerRundownStatus as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MCRTickerRundownStatus as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(McrTickerRundownStatus))]
		public virtual McrTickerRundownStatus InsertMcrTickerRundownStatus(McrTickerRundownStatus entity)
		{

			McrTickerRundownStatus other=new McrTickerRundownStatus();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MCRTickerRundownStatus ( [StatusId]
				,[LastUpdateDate] )
				Values
				( @StatusId
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMcrTickerRundownStatus(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(McrTickerRundownStatus))]
		public virtual McrTickerRundownStatus UpdateMcrTickerRundownStatus(McrTickerRundownStatus entity)
		{

			if (entity.IsTransient()) return entity;
			McrTickerRundownStatus other = GetMcrTickerRundownStatus(entity.McrTickerRundownStatusId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MCRTickerRundownStatus set  [StatusId]=@StatusId
							, [LastUpdateDate]=@LastUpdateDate 
							 where MCRTickerRundownStatusId=@MCRTickerRundownStatusId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@MCRTickerRundownStatusId",entity.McrTickerRundownStatusId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMcrTickerRundownStatus(entity.McrTickerRundownStatusId);
		}

		public virtual bool DeleteMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId)
		{

			string sql="delete from MCRTickerRundownStatus where MCRTickerRundownStatusId=@MCRTickerRundownStatusId";
			SqlParameter parameter=new SqlParameter("@MCRTickerRundownStatusId",McrTickerRundownStatusId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(McrTickerRundownStatus))]
		public virtual McrTickerRundownStatus DeleteMcrTickerRundownStatus(McrTickerRundownStatus entity)
		{
			this.DeleteMcrTickerRundownStatus(entity.McrTickerRundownStatusId);
			return entity;
		}


		public virtual McrTickerRundownStatus McrTickerRundownStatusFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			McrTickerRundownStatus entity=new McrTickerRundownStatus();
			if (dr.Table.Columns.Contains("MCRTickerRundownStatusId"))
			{
			entity.McrTickerRundownStatusId = (System.Int32)dr["MCRTickerRundownStatusId"];
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
