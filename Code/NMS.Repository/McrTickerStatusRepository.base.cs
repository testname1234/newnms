﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class McrTickerStatusRepositoryBase : Repository, IMcrTickerStatusRepositoryBase 
	{
        
        public McrTickerStatusRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MCRTickerStatusId",new SearchColumn(){Name="MCRTickerStatusId",Title="MCRTickerStatusId",SelectClause="MCRTickerStatusId",WhereClause="AllRecords.MCRTickerStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="McrTickerStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Status",new SearchColumn(){Name="Status",Title="Status",SelectClause="Status",WhereClause="AllRecords.Status",DataType="System.String",IsForeignColumn=false,PropertyName="Status",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMcrTickerStatusSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMcrTickerStatusBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMcrTickerStatusAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMcrTickerStatusSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MCRTickerStatus].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MCRTickerStatus].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual McrTickerStatus GetMcrTickerStatus(System.Int32 McrTickerStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRTickerStatus] with (nolock)  where MCRTickerStatusId=@MCRTickerStatusId ";
			SqlParameter parameter=new SqlParameter("@MCRTickerStatusId",McrTickerStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return McrTickerStatusFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<McrTickerStatus> GetMcrTickerStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MCRTickerStatus] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerStatus>(ds,McrTickerStatusFromDataRow);
		}

		public virtual List<McrTickerStatus> GetAllMcrTickerStatus(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMcrTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MCRTickerStatus] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerStatus>(ds, McrTickerStatusFromDataRow);
		}

		public virtual List<McrTickerStatus> GetPagedMcrTickerStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMcrTickerStatusCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MCRTickerStatusId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MCRTickerStatusId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MCRTickerStatusId] ";
            tempsql += " FROM [MCRTickerStatus] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MCRTickerStatusId"))
					tempsql += " , (AllRecords.[MCRTickerStatusId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MCRTickerStatusId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMcrTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MCRTickerStatus] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MCRTickerStatus].[MCRTickerStatusId] = PageIndex.[MCRTickerStatusId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<McrTickerStatus>(ds, McrTickerStatusFromDataRow);
			}else{ return null;}
		}

		private int GetMcrTickerStatusCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MCRTickerStatus as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MCRTickerStatus as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(McrTickerStatus))]
		public virtual McrTickerStatus InsertMcrTickerStatus(McrTickerStatus entity)
		{

			McrTickerStatus other=new McrTickerStatus();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MCRTickerStatus ( [Status] )
				Values
				( @Status );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Status",entity.Status)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMcrTickerStatus(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(McrTickerStatus))]
		public virtual McrTickerStatus UpdateMcrTickerStatus(McrTickerStatus entity)
		{

			if (entity.IsTransient()) return entity;
			McrTickerStatus other = GetMcrTickerStatus(entity.McrTickerStatusId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MCRTickerStatus set  [Status]=@Status 
							 where MCRTickerStatusId=@MCRTickerStatusId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Status",entity.Status)
					, new SqlParameter("@MCRTickerStatusId",entity.McrTickerStatusId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMcrTickerStatus(entity.McrTickerStatusId);
		}

		public virtual bool DeleteMcrTickerStatus(System.Int32 McrTickerStatusId)
		{

			string sql="delete from MCRTickerStatus where MCRTickerStatusId=@MCRTickerStatusId";
			SqlParameter parameter=new SqlParameter("@MCRTickerStatusId",McrTickerStatusId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(McrTickerStatus))]
		public virtual McrTickerStatus DeleteMcrTickerStatus(McrTickerStatus entity)
		{
			this.DeleteMcrTickerStatus(entity.McrTickerStatusId);
			return entity;
		}


		public virtual McrTickerStatus McrTickerStatusFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			McrTickerStatus entity=new McrTickerStatus();
			if (dr.Table.Columns.Contains("MCRTickerStatusId"))
			{
			entity.McrTickerStatusId = (System.Int32)dr["MCRTickerStatusId"];
			}
			if (dr.Table.Columns.Contains("Status"))
			{
			entity.Status = dr["Status"].ToString();
			}
			return entity;
		}

	}
	
	
}
