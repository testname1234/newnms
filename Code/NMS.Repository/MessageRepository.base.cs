﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class MessageRepositoryBase : Repository, IMessageRepositoryBase 
	{
        
        public MessageRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MessageId",new SearchColumn(){Name="MessageId",Title="MessageId",SelectClause="MessageId",WhereClause="AllRecords.MessageId",DataType="System.Int32",IsForeignColumn=false,PropertyName="MessageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Message",new SearchColumn(){Name="Message",Title="Message",SelectClause="Message",WhereClause="AllRecords.Message",DataType="System.String",IsForeignColumn=false,PropertyName="Message",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("To",new SearchColumn(){Name="To",Title="To",SelectClause="To",WhereClause="AllRecords.To",DataType="System.Int32",IsForeignColumn=false,PropertyName="To",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("From",new SearchColumn(){Name="From",Title="From",SelectClause="From",WhereClause="AllRecords.From",DataType="System.Int32",IsForeignColumn=false,PropertyName="From",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsRecieved",new SearchColumn(){Name="IsRecieved",Title="IsRecieved",SelectClause="IsRecieved",WhereClause="AllRecords.IsRecieved",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsRecieved",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotScreenTemplateId",new SearchColumn(){Name="SlotScreenTemplateId",Title="SlotScreenTemplateId",SelectClause="SlotScreenTemplateId",WhereClause="AllRecords.SlotScreenTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlotScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMessageSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMessageBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMessageAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMessageSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Message].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Message].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Message GetMessage(System.Int32 MessageId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMessageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Message] with (nolock)  where MessageId=@MessageId ";
			SqlParameter parameter=new SqlParameter("@MessageId",MessageId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return MessageFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Message> GetMessageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMessageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Message] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Message>(ds,MessageFromDataRow);
		}

		public virtual List<Message> GetAllMessage(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMessageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Message] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Message>(ds, MessageFromDataRow);
		}

		public virtual List<Message> GetPagedMessage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMessageCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MessageId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MessageId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MessageId] ";
            tempsql += " FROM [Message] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MessageId"))
					tempsql += " , (AllRecords.[MessageId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MessageId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMessageSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Message] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Message].[MessageId] = PageIndex.[MessageId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Message>(ds, MessageFromDataRow);
			}else{ return null;}
		}

		private int GetMessageCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Message as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Message as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Message))]
		public virtual Message InsertMessage(Message entity)
		{

			Message other=new Message();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Message ( [Message]
				,[To]
				,[From]
				,[IsRecieved]
				,[CreationDate]
				,[LastUpdateDate]
				,[SlotScreenTemplateId] )
				Values
				( @Message
				, @To
				, @From
				, @IsRecieved
				, @CreationDate
				, @LastUpdateDate
				, @SlotScreenTemplateId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Message",entity.Message)
					, new SqlParameter("@To",entity.To)
					, new SqlParameter("@From",entity.From)
					, new SqlParameter("@IsRecieved",entity.IsRecieved)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMessage(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Message))]
		public virtual Message UpdateMessage(Message entity)
		{

			if (entity.IsTransient()) return entity;
			Message other = GetMessage(entity.MessageId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Message set  [Message]=@Message
							, [To]=@To
							, [From]=@From
							, [IsRecieved]=@IsRecieved
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [SlotScreenTemplateId]=@SlotScreenTemplateId 
							 where MessageId=@MessageId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Message",entity.Message)
					, new SqlParameter("@To",entity.To)
					, new SqlParameter("@From",entity.From)
					, new SqlParameter("@IsRecieved",entity.IsRecieved)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@MessageId",entity.MessageId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMessage(entity.MessageId);
		}

		public virtual bool DeleteMessage(System.Int32 MessageId)
		{

			string sql="delete from Message where MessageId=@MessageId";
			SqlParameter parameter=new SqlParameter("@MessageId",MessageId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Message))]
		public virtual Message DeleteMessage(Message entity)
		{
			this.DeleteMessage(entity.MessageId);
			return entity;
		}


		public virtual Message MessageFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Message entity=new Message();
			if (dr.Table.Columns.Contains("MessageId"))
			{
			entity.MessageId = (System.Int32)dr["MessageId"];
			}
			if (dr.Table.Columns.Contains("Message"))
			{
			entity.Message = dr["Message"].ToString();
			}
			if (dr.Table.Columns.Contains("To"))
			{
			entity.To = (System.Int32)dr["To"];
			}
			if (dr.Table.Columns.Contains("From"))
			{
			entity.From = (System.Int32)dr["From"];
			}
			if (dr.Table.Columns.Contains("IsRecieved"))
			{
			entity.IsRecieved = (System.Boolean)dr["IsRecieved"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("SlotScreenTemplateId"))
			{
			entity.SlotScreenTemplateId = dr["SlotScreenTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlotScreenTemplateId"];
			}
			return entity;
		}

	}
	
	
}
