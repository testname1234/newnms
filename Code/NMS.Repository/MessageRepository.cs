﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class MessageRepository: MessageRepositoryBase, IMessageRepository
	{

        public List<Message> GetAllUserMessages(int UserId, DateTime LastUpdatedDate)
        {
            string sql = GetMessageSelectClause();
            sql += "from [Message] with (nolock)  where ( [To]=@userID Or [From] =@userID) and CreationDate > @LastUpdatedDate";
            SqlParameter parameter = new SqlParameter("@userID", UserId);
            SqlParameter parameter2 = new SqlParameter("@LastUpdatedDate", LastUpdatedDate);


            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Message>(ds, MessageFromDataRow);

        }
        
        
        public void UpdateMessagesRecievedStatus(List<int> MessageIds)
        {
            string strMessageIds = string.Join( ", ", MessageIds );

            string sql = @"Update Message set  [IsRecieved]=1
							 where MessageId in (" + strMessageIds + ")";
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql);
            

        }


        public List<Message> GetAllUserPendingMessages(int UserId)
        {
            string sql = GetMessageSelectClause();
           sql += "from [Message] with (nolock)  where ([To]= @userID Or [From] = @userID) and IsRecieved=0";
            SqlParameter parameter = new SqlParameter("@userID", UserId);


            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter});
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Message>(ds, MessageFromDataRow);
        }


        public List<Message> GetAllUserPendingMessagesBySlotScreenTemplateId(int SlotScreenTemplateId)
        {
            string sql = GetMessageSelectClause();
            sql += "from [Message] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Message>(ds, MessageFromDataRow);
        }

        public List<Message> GetMessages(int SlotScreenTemplateId, DateTime lastUpdateDate)
        {
            string sql = GetMessageSelectClause();
            sql += " from [Message] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId and CreationDate > @Date";

            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId),
                new SqlParameter("@Date", lastUpdateDate)};

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Message>(ds, MessageFromDataRow);
        }
    }
	
	
}
