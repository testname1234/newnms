﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class MigrationBunchTempRepositoryBase : Repository, IMigrationBunchTempRepositoryBase 
	{
        
        public MigrationBunchTempRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MigrationBunchTempId",new SearchColumn(){Name="MigrationBunchTempId",Title="MigrationBunchTempId",SelectClause="MigrationBunchTempId",WhereClause="AllRecords.MigrationBunchTempId",DataType="System.Int32",IsForeignColumn=false,PropertyName="MigrationBunchTempId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchGuid",new SearchColumn(){Name="BunchGuid",Title="BunchGuid",SelectClause="BunchGuid",WhereClause="AllRecords.BunchGuid",DataType="System.String",IsForeignColumn=false,PropertyName="BunchGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedDate",new SearchColumn(){Name="CreatedDate",Title="CreatedDate",SelectClause="CreatedDate",WhereClause="AllRecords.CreatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMigrationBunchTempSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMigrationBunchTempBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMigrationBunchTempAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMigrationBunchTempSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MigrationBunchTemp].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MigrationBunchTemp].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual MigrationBunchTemp GetMigrationBunchTemp(System.Int32 MigrationBunchTempId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMigrationBunchTempSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MigrationBunchTemp] with (nolock)  where MigrationBunchTempId=@MigrationBunchTempId ";
			SqlParameter parameter=new SqlParameter("@MigrationBunchTempId",MigrationBunchTempId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return MigrationBunchTempFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<MigrationBunchTemp> GetMigrationBunchTempByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMigrationBunchTempSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MigrationBunchTemp] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MigrationBunchTemp>(ds,MigrationBunchTempFromDataRow);
		}

		public virtual List<MigrationBunchTemp> GetAllMigrationBunchTemp(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMigrationBunchTempSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MigrationBunchTemp] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MigrationBunchTemp>(ds, MigrationBunchTempFromDataRow);
		}

		public virtual List<MigrationBunchTemp> GetPagedMigrationBunchTemp(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMigrationBunchTempCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MigrationBunchTempId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MigrationBunchTempId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MigrationBunchTempId] ";
            tempsql += " FROM [MigrationBunchTemp] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MigrationBunchTempId"))
					tempsql += " , (AllRecords.[MigrationBunchTempId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MigrationBunchTempId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMigrationBunchTempSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MigrationBunchTemp] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MigrationBunchTemp].[MigrationBunchTempId] = PageIndex.[MigrationBunchTempId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MigrationBunchTemp>(ds, MigrationBunchTempFromDataRow);
			}else{ return null;}
		}

		private int GetMigrationBunchTempCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MigrationBunchTemp as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MigrationBunchTemp as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(MigrationBunchTemp))]
		public virtual MigrationBunchTemp InsertMigrationBunchTemp(MigrationBunchTemp entity)
		{

			MigrationBunchTemp other=new MigrationBunchTemp();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MigrationBunchTemp ( [BunchGuid]
				,[CreatedDate] )
				Values
				( @BunchGuid
				, @CreatedDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedDate",entity.CreatedDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMigrationBunchTemp(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(MigrationBunchTemp))]
		public virtual MigrationBunchTemp UpdateMigrationBunchTemp(MigrationBunchTemp entity)
		{

			if (entity.IsTransient()) return entity;
			MigrationBunchTemp other = GetMigrationBunchTemp(entity.MigrationBunchTempId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MigrationBunchTemp set  [BunchGuid]=@BunchGuid 
							 where MigrationBunchTempId=@MigrationBunchTempId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedDate",entity.CreatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@MigrationBunchTempId",entity.MigrationBunchTempId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMigrationBunchTemp(entity.MigrationBunchTempId);
		}

		public virtual bool DeleteMigrationBunchTemp(System.Int32 MigrationBunchTempId)
		{

			string sql="delete from MigrationBunchTemp where MigrationBunchTempId=@MigrationBunchTempId";
			SqlParameter parameter=new SqlParameter("@MigrationBunchTempId",MigrationBunchTempId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(MigrationBunchTemp))]
		public virtual MigrationBunchTemp DeleteMigrationBunchTemp(MigrationBunchTemp entity)
		{
			this.DeleteMigrationBunchTemp(entity.MigrationBunchTempId);
			return entity;
		}


		public virtual MigrationBunchTemp MigrationBunchTempFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			MigrationBunchTemp entity=new MigrationBunchTemp();
			if (dr.Table.Columns.Contains("MigrationBunchTempId"))
			{
			entity.MigrationBunchTempId = (System.Int32)dr["MigrationBunchTempId"];
			}
			if (dr.Table.Columns.Contains("BunchGuid"))
			{
			entity.BunchGuid = dr["BunchGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("CreatedDate"))
			{
			entity.CreatedDate = dr["CreatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreatedDate"];
			}
			return entity;
		}

	}
	
	
}
