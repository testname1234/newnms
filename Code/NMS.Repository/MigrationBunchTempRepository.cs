﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;

namespace NMS.Repository
{
		
	public partial class MigrationBunchTempRepository: MigrationBunchTempRepositoryBase, IMigrationBunchTempRepository
	{
        public virtual bool DeleteAll()
        {
            string sql = "delete from MigrationBunchTemp ";            
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, null);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
	}
	
	
}
