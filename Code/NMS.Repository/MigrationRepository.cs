﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.DataInterfaces;
using NMS.Core;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Reflection;
using System.Linq;

namespace NMS.Repository
{

    public partial class MigrationRepository
    {

        public virtual string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["conn"].ToString();
            }
        }

        public DateTime GetUpdatedDate(string columnName, string collectionName)
        {
            string sql = "select top 1 " + columnName + " from [" + collectionName + "] with (nolock) order by 1 desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return new DateTime(2000, 1, 1);
            return Convert.ToDateTime(ds.Tables[0].Rows[0][columnName]);
        }

        public DateTime GetUpdatedDate(string columnName, string collectionName, SqlConnection ConnectionString)
        {
            string sql = "select top 1 " + columnName + " from [" + collectionName + "] with (nolock) order by 1 desc";
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return new DateTime(2000, 1, 1);
            return Convert.ToDateTime(ds.Tables[0].Rows[0][columnName]);
        }

        public List<T> GetByDate<T>(DateTime UpdatedDate, string collectionName)
        {
            string sql = "select * from [" + collectionName + "] with (nolock) where LastUpdateDate >@UpdatedDate";
            SqlParameter parameter = new SqlParameter("@UpdatedDate", UpdatedDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return ds.Tables[0].ToList<T>();
        }

        public List<T> GetByDateWithParam<T>(string DateColumn, DateTime UpdatedDate, DateTime MaxDate, string collectionName)
        {
            string sql = "select * from [" + collectionName + "] with (nolock) where " + DateColumn + "> @UpdatedDate and " + DateColumn + "<=@MaxDate";
            SqlParameter parameter = new SqlParameter("@UpdatedDate", UpdatedDate);
            SqlParameter parameter2 = new SqlParameter("@MaxDate", MaxDate);
            //SqlParameter parameter2 = new SqlParameter("@MaxDate", MaxDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return ds.Tables[0].ToList<T>();
        }

        public List<T> GetByDateWithParam<T>(string DateColumn, DateTime UpdatedDate, DateTime MaxDate, string collectionName, SqlConnection ConnectionString)
        {
            string sql = "select * from [" + collectionName + "] with (nolock) where " + DateColumn + "> @UpdatedDate and " + DateColumn + "<=@MaxDate";
            SqlParameter parameter = new SqlParameter("@UpdatedDate", UpdatedDate);
            SqlParameter parameter2 = new SqlParameter("@MaxDate", MaxDate);
            //SqlParameter parameter2 = new SqlParameter("@MaxDate", MaxDate);
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return ds.Tables[0].ToList<T>();
        }

        public List<T> GetAll<T>(string collectionName)
        {
            string sql = "select * from " + collectionName + " with (nolock)";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return ds.Tables[0].ToList<T>();
        }

        public void BulkUpdateNews<T>(List<T> list, string TabelName, List<string> ignoreColumnList)
        {
            //DataTable dt = new DataTable("MyTable");
            DataTable dt = list.ToDataTable(ignoreColumnList);
            //ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("", conn))
                {
                    try
                    {
                        conn.Open();
                        //Creating temp table on database
                        command.CommandText = @"CREATE TABLE TmpTable(Guid varchar(50),BunchGuid varchar(50),ParentNewsGuid varchar(50),
                                                ReferenceNewsGuid varchar(50),ThumbnailId varchar(50),IsVerified bit,AddedToRundownCount int,OnAirCount int,
                                                OtherChannelExecutionCount int,NewsTickerCount int,IsCompleted bit,IsArchival bit,MigrationStatus int)";
                        command.ExecuteNonQuery();
                        //Bulk insert into temp table

                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                        {
                            SqlBulkCopyColumnMappingCollection columnCollection = bulkcopy.ColumnMappings;
                            SqlBulkCopyColumnMapping sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "Guid"; sqlcolumnmap.DestinationColumn = "Guid";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "BunchGuid"; sqlcolumnmap.DestinationColumn = "BunchGuid";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "ParentNewsGuid"; sqlcolumnmap.DestinationColumn = "ParentNewsGuid";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "ReferenceNewsGuid"; sqlcolumnmap.DestinationColumn = "ReferenceNewsGuid";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "ThumbnailId"; sqlcolumnmap.DestinationColumn = "ThumbnailId";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "IsVerified"; sqlcolumnmap.DestinationColumn = "IsVerified";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "AddedToRundownCount"; sqlcolumnmap.DestinationColumn = "AddedToRundownCount";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "OnAirCount"; sqlcolumnmap.DestinationColumn = "OnAirCount";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "OtherChannelExecutionCount"; sqlcolumnmap.DestinationColumn = "OtherChannelExecutionCount";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "NewsTickerCount"; sqlcolumnmap.DestinationColumn = "NewsTickerCount";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "IsCompleted"; sqlcolumnmap.DestinationColumn = "IsCompleted";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "IsArchival"; sqlcolumnmap.DestinationColumn = "IsArchival";
                            columnCollection.Add(sqlcolumnmap);
                            sqlcolumnmap = new SqlBulkCopyColumnMapping();
                            sqlcolumnmap.SourceColumn = "MigrationStatus"; sqlcolumnmap.DestinationColumn = "MigrationStatus";
                            columnCollection.Add(sqlcolumnmap);

                            bulkcopy.BulkCopyTimeout = 660;
                            bulkcopy.DestinationTableName = "TmpTable";
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }
                        // Updating destination table, and dropping temp table
                        command.CommandTimeout = 300;
                        command.CommandText = @"UPDATE t SET t.BunchGuid = Temp.BunchGuid,
							                            t.ParentNewsGuid = Temp.ParentNewsGuid,
							                            t.ReferenceNewsGuid = Temp.ReferenceNewsGuid,
							                            t.ThumbnailId = Temp.ThumbnailId,
							                            t.IsVerified = Temp.IsVerified,
							                            t.AddedToRundownCount = Temp.AddedToRundownCount,
							                            t.OnAirCount = Temp.OnAirCount,
							                            t.OtherChannelExecutionCount = Temp.OtherChannelExecutionCount,
							                            t.NewsTickerCount = Temp.NewsTickerCount,
							                            t.IsCompleted = Temp.IsCompleted ,
                                                        t.IsArchival = Temp.IsArchival, 
                                                        t.MigrationStatus = Temp.MigrationStatus 
                                                        FROM " + TabelName + " t INNER JOIN TmpTable Temp ON t.Guid = Temp.Guid; DROP TABLE TmpTable;";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        // Handle exception properly
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }


        }

        public void BulkInsert<T>(List<T> obj, string collectionName, List<string> ignoreColumnList)
        {
            ignoreColumnList.Remove("Category");
            ignoreColumnList.Remove("ParentId");
            ignoreColumnList.Remove("IsApproved");
            ignoreColumnList.Remove("CategoryInUrdu");
            ignoreColumnList.Remove("Location");
            ignoreColumnList.Remove("Guid");
            ignoreColumnList.Remove("ResourceTypeId");
            ignoreColumnList.Remove("Duration");
            ignoreColumnList.Remove("Caption");
            ignoreColumnList.Remove("Category");
            ignoreColumnList.Remove("Location");
            ignoreColumnList.Remove("IgnoreMeta");
            

            if (collectionName == "NewsCategory" || collectionName == "NewsLocation")
            {
                ignoreColumnList.Add("Category");
                ignoreColumnList.Add("ParentId");
                ignoreColumnList.Add("IsApproved");
                ignoreColumnList.Add("CategoryInUrdu");
                ignoreColumnList.Add("Location");              
            }
            else if (collectionName == "NewsResource")
            {
                ignoreColumnList.Add("Guid");
                ignoreColumnList.Add("ResourceTypeId");
                ignoreColumnList.Add("Duration");
                ignoreColumnList.Add("Caption");
                ignoreColumnList.Add("Category");
                ignoreColumnList.Add("Location");
            }

            DataTable table = obj.ToDataTable(ignoreColumnList);
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.ConnectionString, SqlBulkCopyOptions.Default))
            {
                SqlBulkCopyColumnMappingCollection columnCollection = bulkCopy.ColumnMappings;
                foreach (DataColumn column in table.Columns)
                {
                    SqlBulkCopyColumnMapping sqlcolumnmap = new SqlBulkCopyColumnMapping();
                    sqlcolumnmap.SourceColumn = column.ColumnName;
                    sqlcolumnmap.DestinationColumn = column.ColumnName;
                    columnCollection.Add(sqlcolumnmap);
                }
                bulkCopy.DestinationTableName = "dbo." + collectionName;
                bulkCopy.BatchSize = 10000;
                bulkCopy.WriteToServer(table);
            }
        }

        public void InsertEntityNoReturn<T>(object obj, string collectionName)
        {

            List<string> ignoreColumnList = new List<string>();
            ignoreColumnList.Add("CreationDateStr");
            ignoreColumnList.Add("LastUpdateDateStr");
            ignoreColumnList.Add("IsUpdated");
            ignoreColumnList.Add("PublishTimeStr");
            ignoreColumnList.Add("UpdateTimeStr");
            ignoreColumnList.Add("FromStr");
            ignoreColumnList.Add("ToStr");
            ignoreColumnList.Add("DateStr");
            ignoreColumnList.Add("Segments");
            ignoreColumnList.Add("MaxUpdateDateStr");
            ignoreColumnList.Add("CreatonDateStr");


            string sql = @"insert into [" + collectionName + "]";
            string tableColumns = "(";
            string values = "values(";
            var aT = Activator.CreateInstance<T>();
            aT.CopyFrom(obj);
            int i = 1;
            List<SqlParameter> parameters = new List<SqlParameter>();
            foreach (PropertyInfo propertyInfo in aT.GetType().GetProperties())
            {
                if (ignoreColumnList.IndexOf(propertyInfo.Name) == -1)
                {
                    if (propertyInfo.Name.ToLower() != (collectionName + "Id").ToLower())
                    {
                        if (i != 1)
                        {
                            tableColumns += ",[" + propertyInfo.Name + "]";
                            values += "," + "@" + propertyInfo.Name;
                        }
                        else
                        {
                            tableColumns += "[" + propertyInfo.Name + "]";
                            values += "@" + propertyInfo.Name;
                        }
                        i++;

                        object value = propertyInfo.GetValue(aT) ?? (object)DBNull.Value;
                        SqlParameter param = new SqlParameter("@" + propertyInfo.Name, value);
                        parameters.Add(param);
                    }
                }
            }
            sql += tableColumns + ")" + values + ")";
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameters.ToArray());
        }

        public void InsertEntity<T>(object obj, string collectionName, SqlConnection ConnectionString, List<string> ignoreColumnList)
        {
            string sql = @"set identity_insert [" + collectionName + "] on insert into [" + collectionName + "]";
            string tableColumns = "(";
            string values = "values(";
            var aT = Activator.CreateInstance<T>();
            aT.CopyFrom(obj);
            int i = 1;
            List<SqlParameter> parameters = new List<SqlParameter>();
            foreach (PropertyInfo propertyInfo in aT.GetType().GetProperties())
            {
                //if(propertyInfo.Name ignoreColumnList
                if (ignoreColumnList.IndexOf(propertyInfo.Name) == -1)
                {
                    if (i != 1)
                    {
                        tableColumns += ",[" + propertyInfo.Name + "]";
                        values += "," + "@" + propertyInfo.Name;
                    }
                    else
                    {
                        tableColumns += "[" + propertyInfo.Name + "]";
                        values += "@" + propertyInfo.Name;
                    }
                    i++;

                    object value = propertyInfo.GetValue(aT) ?? (object)DBNull.Value;
                    if (collectionName == "ScreenTemplate" && propertyInfo.Name == "Name")
                        value = "";
                    SqlParameter param = new SqlParameter("@" + propertyInfo.Name, value);
                    parameters.Add(param);
                }
            }
            sql += tableColumns + ")" + values + ") set identity_insert [" + collectionName + "] off";
            var identity = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, sql, parameters.ToArray());
        }


        public void UpdateEntity<T>(object obj, string collectionName, SqlConnection ConnectionString, List<string> ignoreColumnList)
        {
            string sql = @"update [" + collectionName + "]";
            string setColumns = " set ";
            string where = " where ";
            var aT = Activator.CreateInstance<T>();
            aT.CopyFrom(obj);
            int i = 1;
            List<SqlParameter> parameters = new List<SqlParameter>();
            foreach (PropertyInfo propertyInfo in aT.GetType().GetProperties())
            {
                //if(propertyInfo.Name ignoreColumnList
                if (ignoreColumnList.IndexOf(propertyInfo.Name) == -1)
                {
                    if (propertyInfo.Name.ToLower() == (collectionName + "Id").ToLower())
                    {
                        where += propertyInfo.Name + " = @" + propertyInfo.Name;
                    }
                    else
                    {
                        if (i != 1)
                        {
                            setColumns += ",[" + propertyInfo.Name + "] = @" + propertyInfo.Name;
                        }
                        else
                        {
                            setColumns += "[" + propertyInfo.Name + "] = @" + propertyInfo.Name;
                        }
                        i++;
                    }
                    object value = propertyInfo.GetValue(aT) ?? (object)DBNull.Value;
                    SqlParameter param = new SqlParameter("@" + propertyInfo.Name, value);
                    parameters.Add(param);

                }
            }
            sql += setColumns + where;
            var identity = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, sql, parameters.ToArray());
        }

        public int? GetEntity<T>(object obj, string collectionName, SqlConnection ConnectionString)
        {
            List<string> ignoreColumnList = new List<string>();
            ignoreColumnList.Add("CreationDateStr");
            ignoreColumnList.Add("LastUpdateDateStr");
            ignoreColumnList.Add("IsUpdated");

            string sql = @"select count(1) from [" + collectionName + "] (nolock)";
            string where = " where ";
            var aT = Activator.CreateInstance<T>();
            aT.CopyFrom(obj);
            int i = 1;
            List<SqlParameter> parameters = new List<SqlParameter>();
            foreach (PropertyInfo propertyInfo in aT.GetType().GetProperties())
            {
                //if(propertyInfo.Name ignoreColumnList
                if (ignoreColumnList.IndexOf(propertyInfo.Name) == -1)
                {
                    if (propertyInfo.Name.ToLower() == (collectionName + "Id").ToLower())
                    {
                        where += propertyInfo.Name + " = @" + propertyInfo.Name;
                        SqlParameter param = new SqlParameter("@" + propertyInfo.Name, propertyInfo.GetValue(aT));
                        parameters.Add(param);
                        break;
                    }
                }
            }
            sql += where;
            int identity = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, sql, parameters.ToArray()));
            if (identity > 0)
            {
                return identity;
            }
            else
                return null;
        }


    }
}
