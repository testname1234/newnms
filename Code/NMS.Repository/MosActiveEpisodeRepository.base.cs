﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class MosActiveEpisodeRepositoryBase : Repository, IMosActiveEpisodeRepositoryBase 
	{
        
        public MosActiveEpisodeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MosActiveEpisodeId",new SearchColumn(){Name="MosActiveEpisodeId",Title="MosActiveEpisodeId",SelectClause="MosActiveEpisodeId",WhereClause="AllRecords.MosActiveEpisodeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="MosActiveEpisodeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EpisodeId",new SearchColumn(){Name="EpisodeId",Title="EpisodeId",SelectClause="EpisodeId",WhereClause="AllRecords.EpisodeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="EpisodeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusCode",new SearchColumn(){Name="StatusCode",Title="StatusCode",SelectClause="StatusCode",WhereClause="AllRecords.StatusCode",DataType="System.Int32?",IsForeignColumn=false,PropertyName="StatusCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PCRTelePropterStatus",new SearchColumn(){Name="PCRTelePropterStatus",Title="PCRTelePropterStatus",SelectClause="PCRTelePropterStatus",WhereClause="AllRecords.PCRTelePropterStatus",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="PcrTelePropterStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PCRPlayoutStatus",new SearchColumn(){Name="PCRPlayoutStatus",Title="PCRPlayoutStatus",SelectClause="PCRPlayoutStatus",WhereClause="AllRecords.PCRPlayoutStatus",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="PcrPlayoutStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PCRFourWindowStatus",new SearchColumn(){Name="PCRFourWindowStatus",Title="PCRFourWindowStatus",SelectClause="PCRFourWindowStatus",WhereClause="AllRecords.PCRFourWindowStatus",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="PcrFourWindowStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusDescription",new SearchColumn(){Name="StatusDescription",Title="StatusDescription",SelectClause="StatusDescription",WhereClause="AllRecords.StatusDescription",DataType="System.String",IsForeignColumn=false,PropertyName="StatusDescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PCRTelepropterJson",new SearchColumn(){Name="PCRTelepropterJson",Title="PCRTelepropterJson",SelectClause="PCRTelepropterJson",WhereClause="AllRecords.PCRTelepropterJson",DataType="System.String",IsForeignColumn=false,PropertyName="PcrTelepropterJson",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PCRPlayoutJson",new SearchColumn(){Name="PCRPlayoutJson",Title="PCRPlayoutJson",SelectClause="PCRPlayoutJson",WhereClause="AllRecords.PCRPlayoutJson",DataType="System.String",IsForeignColumn=false,PropertyName="PcrPlayoutJson",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PCRFourWindowJson",new SearchColumn(){Name="PCRFourWindowJson",Title="PCRFourWindowJson",SelectClause="PCRFourWindowJson",WhereClause="AllRecords.PCRFourWindowJson",DataType="System.String",IsForeignColumn=false,PropertyName="PcrFourWindowJson",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsAcknowledged",new SearchColumn(){Name="IsAcknowledged",Title="IsAcknowledged",SelectClause="IsAcknowledged",WhereClause="AllRecords.IsAcknowledged",DataType="System.Int32?",IsForeignColumn=false,PropertyName="IsAcknowledged",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMosActiveEpisodeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMosActiveEpisodeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMosActiveEpisodeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMosActiveEpisodeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MosActiveEpisode].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MosActiveEpisode].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<MosActiveEpisode> GetMosActiveEpisodeByEpisodeId(System.Int32? EpisodeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveEpisodeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveEpisode] with (nolock)  where EpisodeId=@EpisodeId  ";
			SqlParameter parameter=new SqlParameter("@EpisodeId",EpisodeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveEpisode>(ds,MosActiveEpisodeFromDataRow);
		}

		public virtual MosActiveEpisode GetMosActiveEpisode(System.Int32 MosActiveEpisodeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveEpisodeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveEpisode] with (nolock)  where MosActiveEpisodeId=@MosActiveEpisodeId ";
			SqlParameter parameter=new SqlParameter("@MosActiveEpisodeId",MosActiveEpisodeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return MosActiveEpisodeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<MosActiveEpisode> GetMosActiveEpisodeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveEpisodeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MosActiveEpisode] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveEpisode>(ds,MosActiveEpisodeFromDataRow);
		}

		public virtual List<MosActiveEpisode> GetAllMosActiveEpisode(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveEpisodeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveEpisode] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveEpisode>(ds, MosActiveEpisodeFromDataRow);
		}

		public virtual List<MosActiveEpisode> GetPagedMosActiveEpisode(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMosActiveEpisodeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MosActiveEpisodeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MosActiveEpisodeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MosActiveEpisodeId] ";
            tempsql += " FROM [MosActiveEpisode] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MosActiveEpisodeId"))
					tempsql += " , (AllRecords.[MosActiveEpisodeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MosActiveEpisodeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMosActiveEpisodeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MosActiveEpisode] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MosActiveEpisode].[MosActiveEpisodeId] = PageIndex.[MosActiveEpisodeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveEpisode>(ds, MosActiveEpisodeFromDataRow);
			}else{ return null;}
		}

		private int GetMosActiveEpisodeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MosActiveEpisode as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MosActiveEpisode as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(MosActiveEpisode))]
		public virtual MosActiveEpisode InsertMosActiveEpisode(MosActiveEpisode entity)
		{

			MosActiveEpisode other=new MosActiveEpisode();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MosActiveEpisode ( [EpisodeId]
				,[StatusCode]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[PCRTelePropterStatus]
				,[PCRPlayoutStatus]
				,[PCRFourWindowStatus]
				,[StatusDescription]
				,[PCRTelepropterJson]
				,[PCRPlayoutJson]
				,[PCRFourWindowJson]
				,[IsAcknowledged] )
				Values
				( @EpisodeId
				, @StatusCode
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @PCRTelePropterStatus
				, @PCRPlayoutStatus
				, @PCRFourWindowStatus
				, @StatusDescription
				, @PCRTelepropterJson
				, @PCRPlayoutJson
				, @PCRFourWindowJson
				, @IsAcknowledged );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@StatusCode",entity.StatusCode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@PCRTelePropterStatus",entity.PcrTelePropterStatus ?? (object)DBNull.Value)
					, new SqlParameter("@PCRPlayoutStatus",entity.PcrPlayoutStatus ?? (object)DBNull.Value)
					, new SqlParameter("@PCRFourWindowStatus",entity.PcrFourWindowStatus ?? (object)DBNull.Value)
					, new SqlParameter("@StatusDescription",entity.StatusDescription ?? (object)DBNull.Value)
					, new SqlParameter("@PCRTelepropterJson",entity.PcrTelepropterJson ?? (object)DBNull.Value)
					, new SqlParameter("@PCRPlayoutJson",entity.PcrPlayoutJson ?? (object)DBNull.Value)
					, new SqlParameter("@PCRFourWindowJson",entity.PcrFourWindowJson ?? (object)DBNull.Value)
					, new SqlParameter("@IsAcknowledged",entity.IsAcknowledged ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMosActiveEpisode(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(MosActiveEpisode))]
		public virtual MosActiveEpisode UpdateMosActiveEpisode(MosActiveEpisode entity)
		{

			if (entity.IsTransient()) return entity;
			MosActiveEpisode other = GetMosActiveEpisode(entity.MosActiveEpisodeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MosActiveEpisode set  [EpisodeId]=@EpisodeId
							, [StatusCode]=@StatusCode
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [PCRTelePropterStatus]=@PCRTelePropterStatus
							, [PCRPlayoutStatus]=@PCRPlayoutStatus
							, [PCRFourWindowStatus]=@PCRFourWindowStatus
							, [StatusDescription]=@StatusDescription
							, [PCRTelepropterJson]=@PCRTelepropterJson
							, [PCRPlayoutJson]=@PCRPlayoutJson
							, [PCRFourWindowJson]=@PCRFourWindowJson
							, [IsAcknowledged]=@IsAcknowledged 
							 where MosActiveEpisodeId=@MosActiveEpisodeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@StatusCode",entity.StatusCode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@PCRTelePropterStatus",entity.PcrTelePropterStatus ?? (object)DBNull.Value)
					, new SqlParameter("@PCRPlayoutStatus",entity.PcrPlayoutStatus ?? (object)DBNull.Value)
					, new SqlParameter("@PCRFourWindowStatus",entity.PcrFourWindowStatus ?? (object)DBNull.Value)
					, new SqlParameter("@StatusDescription",entity.StatusDescription ?? (object)DBNull.Value)
					, new SqlParameter("@PCRTelepropterJson",entity.PcrTelepropterJson ?? (object)DBNull.Value)
					, new SqlParameter("@PCRPlayoutJson",entity.PcrPlayoutJson ?? (object)DBNull.Value)
					, new SqlParameter("@PCRFourWindowJson",entity.PcrFourWindowJson ?? (object)DBNull.Value)
					, new SqlParameter("@IsAcknowledged",entity.IsAcknowledged ?? (object)DBNull.Value)
					, new SqlParameter("@MosActiveEpisodeId",entity.MosActiveEpisodeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMosActiveEpisode(entity.MosActiveEpisodeId);
		}

		public virtual bool DeleteMosActiveEpisode(System.Int32 MosActiveEpisodeId)
		{

			string sql="delete from MosActiveEpisode where MosActiveEpisodeId=@MosActiveEpisodeId";
			SqlParameter parameter=new SqlParameter("@MosActiveEpisodeId",MosActiveEpisodeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(MosActiveEpisode))]
		public virtual MosActiveEpisode DeleteMosActiveEpisode(MosActiveEpisode entity)
		{
			this.DeleteMosActiveEpisode(entity.MosActiveEpisodeId);
			return entity;
		}


		public virtual MosActiveEpisode MosActiveEpisodeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			MosActiveEpisode entity=new MosActiveEpisode();
			if (dr.Table.Columns.Contains("MosActiveEpisodeId"))
			{
			entity.MosActiveEpisodeId = (System.Int32)dr["MosActiveEpisodeId"];
			}
			if (dr.Table.Columns.Contains("EpisodeId"))
			{
			entity.EpisodeId = dr["EpisodeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["EpisodeId"];
			}
			if (dr.Table.Columns.Contains("StatusCode"))
			{
			entity.StatusCode = dr["StatusCode"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["StatusCode"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("PCRTelePropterStatus"))
			{
			entity.PcrTelePropterStatus = dr["PCRTelePropterStatus"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["PCRTelePropterStatus"];
			}
			if (dr.Table.Columns.Contains("PCRPlayoutStatus"))
			{
			entity.PcrPlayoutStatus = dr["PCRPlayoutStatus"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["PCRPlayoutStatus"];
			}
			if (dr.Table.Columns.Contains("PCRFourWindowStatus"))
			{
			entity.PcrFourWindowStatus = dr["PCRFourWindowStatus"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["PCRFourWindowStatus"];
			}
			if (dr.Table.Columns.Contains("StatusDescription"))
			{
			entity.StatusDescription = dr["StatusDescription"].ToString();
			}
			if (dr.Table.Columns.Contains("PCRTelepropterJson"))
			{
			entity.PcrTelepropterJson = dr["PCRTelepropterJson"].ToString();
			}
			if (dr.Table.Columns.Contains("PCRPlayoutJson"))
			{
			entity.PcrPlayoutJson = dr["PCRPlayoutJson"].ToString();
			}
			if (dr.Table.Columns.Contains("PCRFourWindowJson"))
			{
			entity.PcrFourWindowJson = dr["PCRFourWindowJson"].ToString();
			}
			if (dr.Table.Columns.Contains("IsAcknowledged"))
			{
			entity.IsAcknowledged = dr["IsAcknowledged"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["IsAcknowledged"];
			}
			return entity;
		}

	}
	
	
}
