﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class MosActiveEpisodeRepository: MosActiveEpisodeRepositoryBase, IMosActiveEpisodeRepository
	{
        public virtual List<MosActiveEpisode> GetMosActiveEpisodeByStatus(System.Int32? StatusCode, string SelectClause = null)
        {
            if (StatusCode == null)
            {
                string sql = string.IsNullOrEmpty(SelectClause) ? GetMosActiveEpisodeSelectClause() : (string.Format("Select {0} ", SelectClause));
                sql += "from [MosActiveEpisode] with (nolock)  where StatusCode in (1,2)";
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<MosActiveEpisode>(ds, MosActiveEpisodeFromDataRow);
            }
            else
            {
                string sql = string.IsNullOrEmpty(SelectClause) ? GetMosActiveEpisodeSelectClause() : (string.Format("Select {0} ", SelectClause));
                sql += "from [MosActiveEpisode] with (nolock)  where StatusCodein (@StatusCode) ";
                SqlParameter parameter = new SqlParameter("@StatusId", StatusCode);
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<MosActiveEpisode>(ds, MosActiveEpisodeFromDataRow);
            }
        }
        public MosActiveEpisode GetMosActiveEpisodeByDetail(int MosActiveEpisodeId)
        {

            //string sql = string.IsNullOrEmpty(SelectClause) ? GetMosActiveEpisodeSelectClause() : (string.Format("Select {0} ", SelectClause));
            string sql = @"from [MosActiveEpisode] with (nolock)  where MosActiveEpisodeId=@MosActiveEpisodeId ";
            SqlParameter parameter = new SqlParameter("@MosActiveEpisodeId", MosActiveEpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
           // if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return MosActiveEpisodeFromDataRow(ds.Tables[0].Rows[0]);
        }

        public void UpdateStatus(int StatusCode, int MosActiveEpisodeId, string PCRTelePropterJson, string PCRPlayoutJson)
        {

            string sql = @"Update MosActiveEpisode set  
                            [StatusCode]=@StatusCode
							, [LastUpdateDate]=@LastUpdateDate
                            , [PCRTelePropterJson]=@PCRTelePropterJson
                            , [PCRPlayoutJson]=@PCRPlayoutJson,
                            PCRTelePropterStatus = Null,
                            PCRPlayoutStatus = Null,
                            PCRFourWindowStatus = Null,
                            StatusDescription = ''
							 where MosActiveEpisodeId=@MosActiveEpisodeId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@StatusCode",StatusCode)
					, new SqlParameter("@LastUpdateDate",DateTime.UtcNow)
					, new SqlParameter("@MosActiveEpisodeId",MosActiveEpisodeId)
                    , new SqlParameter("@PCRTelePropterJson",PCRTelePropterJson)
                    , new SqlParameter("@PCRPlayoutJson",PCRPlayoutJson)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        
        }
        public List<MosActiveEpisode> GetAllMosEpisodeWithProgram(string SelectClause = null)
        {

            string sql = @"select moa.*, e.[From] as EpisodeStartTime, p.Name as ProgramName from program p inner join Episode e on p.ProgramId = e.ProgramId inner join MosActiveEpisode moa on moa.EpisodeId = e.EpisodeId order by moa.LastUpdateDate";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            
            return CollectionFromDataSet<MosActiveEpisode>(ds, MosActiveEpisodeFromDataRowcustmzd);
        }
        public List<MosActiveEpisode> GetAllMosEpisodeStatusSmsThread(string SelectClause = null)
        {            

            string sql = @"select moa.*, e.[From] as EpisodeStartTime, p.Name as ProgramName 
from program p inner join Episode e on (p.ProgramId = e.ProgramId) inner join 
MosActiveEpisode moa on (moa.EpisodeId = e.EpisodeId) 
where datediff(DD,moa.LastUpdateDate,SYSUTCDATETIME()) <= 1 AND StatusCode = 4 AND isnull(IsAcknowledged,0) = 0 order by moa.LastUpdateDate";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;

            return CollectionFromDataSet<MosActiveEpisode>(ds, MosActiveEpisodeFromDataRowcustmzd);
        }


        public MosActiveEpisode MosActiveEpisodeFromDataRowcustmzd(DataRow dr)
        {
            if (dr == null) return null;
            MosActiveEpisode entity = new MosActiveEpisode();
            if (dr.Table.Columns.Contains("MosActiveEpisodeId"))
            {
                entity.MosActiveEpisodeId = (System.Int32)dr["MosActiveEpisodeId"];
            }
            if (dr.Table.Columns.Contains("EpisodeId"))
            {
                entity.EpisodeId = dr["EpisodeId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["EpisodeId"];
            }
            if (dr.Table.Columns.Contains("StatusCode"))
            {
                entity.StatusCode = dr["StatusCode"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["StatusCode"];
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = dr["CreationDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = dr["LastUpdateDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("IsActive"))
            {
                entity.IsActive = dr["IsActive"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsActive"];
            }
            if (dr.Table.Columns.Contains("PCRTelePropterStatus"))
            {
                entity.PcrTelePropterStatus = dr["PCRTelePropterStatus"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["PCRTelePropterStatus"];
            }
            if (dr.Table.Columns.Contains("PCRPlayoutStatus"))
            {
                entity.PcrPlayoutStatus = dr["PCRPlayoutStatus"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["PCRPlayoutStatus"];
            }
            if (dr.Table.Columns.Contains("PCRFourWindowStatus"))
            {
                entity.PcrFourWindowStatus = dr["PCRFourWindowStatus"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["PCRFourWindowStatus"];
            }
            if (dr.Table.Columns.Contains("StatusDescription"))
            {
                entity.StatusDescription = dr["StatusDescription"].ToString();
            }
            if (dr.Table.Columns.Contains("PCRTelePropterJson"))
            {
                entity.PcrTelepropterJson = dr["PCRTelePropterJson"].ToString();
            }
            if (dr.Table.Columns.Contains("PCRPlayoutJson"))
            {
                entity.PcrPlayoutJson = dr["PCRPlayoutJson"].ToString();
            }
            if (dr.Table.Columns.Contains("PCRFourWindowJson"))
            {
                entity.PcrFourWindowJson = dr["PCRFourWindowJson"].ToString();
            }
            if (dr.Table.Columns.Contains("ProgramName"))
            {
                entity.ProgramName = dr["ProgramName"].ToString();
            }
            if (dr.Table.Columns.Contains("EpisodeStartTime"))
            {
                entity.EpisodeStartTime = dr["EpisodeStartTime"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["EpisodeStartTime"];                
            }

            return entity;
        }


        public void UpdateMosActiveEpisodeIsAcknowledged(MosActiveEpisode entity)
        {
            string sql = @"update MosActiveEpisode set IsAcknowledged = @IsAcknowledged where MosActiveEpisodeId = @MosActiveEpisodeId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@IsAcknowledged",entity.IsAcknowledged ?? (object)DBNull.Value)
					, new SqlParameter("@MosActiveEpisodeId",entity.MosActiveEpisodeId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }





        public MosActiveEpisode MosActiveEpisodeFromDataRowCustom(DataRow dr)
        {
            if (dr == null) return null;
            MosActiveEpisode entity = new MosActiveEpisode();
            if (dr.Table.Columns.Contains("MosActiveEpisodeId"))
            {
                entity.MosActiveEpisodeId = (System.Int32)dr["MosActiveEpisodeId"];
            }
            if (dr.Table.Columns.Contains("EpisodeId"))
            {
                entity.EpisodeId = dr["EpisodeId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["EpisodeId"];
            }
            if (dr.Table.Columns.Contains("StatusCode"))
            {
                entity.StatusCode = dr["StatusCode"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["StatusCode"];
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = dr["CreationDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = dr["LastUpdateDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("IsActive"))
            {
                entity.IsActive = dr["IsActive"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsActive"];
            }
            if (dr.Table.Columns.Contains("PCRTelePropterStatus"))
            {
                entity.PcrTelePropterStatus = dr["PCRTelePropterStatus"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["PCRTelePropterStatus"];
            }
            if (dr.Table.Columns.Contains("PCRPlayoutStatus"))
            {
                entity.PcrPlayoutStatus = dr["PCRPlayoutStatus"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["PCRPlayoutStatus"];
            }
            if (dr.Table.Columns.Contains("PCRFourWindowStatus"))
            {
                entity.PcrFourWindowStatus = dr["PCRFourWindowStatus"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["PCRFourWindowStatus"];
            }
            if (dr.Table.Columns.Contains("StatusDescription"))
            {
                entity.StatusDescription = dr["StatusDescription"].ToString();
            }
            if (dr.Table.Columns.Contains("ProgramName"))
            {
                entity.ProgramName = dr["ProgramName"].ToString();
            }
            if (dr.Table.Columns.Contains("EpisodeStartTime"))
            {
                entity.EpisodeStartTime = (System.DateTime)dr["EpisodeStartTime"];
            }
            if (dr.Table.Columns.Contains("ProgramName"))
            {
                entity.ProgramName = dr["ProgramName"].ToString();
            }
            if (dr.Table.Columns.Contains("EpisodeStartTime"))
            {
                entity.EpisodeStartTime = (System.DateTime)dr["EpisodeStartTime"];
            }
            return entity;
        }
	}
	
	
}
