﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class MosActiveItemRepositoryBase : Repository, IMosActiveItemRepositoryBase 
	{
        
        public MosActiveItemRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MosActiveItemId",new SearchColumn(){Name="MosActiveItemId",Title="MosActiveItemId",SelectClause="MosActiveItemId",WhereClause="AllRecords.MosActiveItemId",DataType="System.Int32",IsForeignColumn=false,PropertyName="MosActiveItemId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EpisodeId",new SearchColumn(){Name="EpisodeId",Title="EpisodeId",SelectClause="EpisodeId",WhereClause="AllRecords.EpisodeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="EpisodeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("type",new SearchColumn(){Name="type",Title="type",SelectClause="type",WhereClause="AllRecords.type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("devicename",new SearchColumn(){Name="devicename",Title="devicename",SelectClause="devicename",WhereClause="AllRecords.devicename",DataType="System.String",IsForeignColumn=false,PropertyName="Devicename",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("label",new SearchColumn(){Name="label",Title="label",SelectClause="label",WhereClause="AllRecords.label",DataType="System.String",IsForeignColumn=false,PropertyName="Label",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("name",new SearchColumn(){Name="name",Title="name",SelectClause="name",WhereClause="AllRecords.name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("channel",new SearchColumn(){Name="channel",Title="channel",SelectClause="channel",WhereClause="AllRecords.channel",DataType="System.String",IsForeignColumn=false,PropertyName="Channel",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("videolayer",new SearchColumn(){Name="videolayer",Title="videolayer",SelectClause="videolayer",WhereClause="AllRecords.videolayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Videolayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("delay",new SearchColumn(){Name="delay",Title="delay",SelectClause="delay",WhereClause="AllRecords.delay",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Delay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("duration",new SearchColumn(){Name="duration",Title="duration",SelectClause="duration",WhereClause="AllRecords.duration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("allowgpi",new SearchColumn(){Name="allowgpi",Title="allowgpi",SelectClause="allowgpi",WhereClause="AllRecords.allowgpi",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Allowgpi",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("allowremotetriggering",new SearchColumn(){Name="allowremotetriggering",Title="allowremotetriggering",SelectClause="allowremotetriggering",WhereClause="AllRecords.allowremotetriggering",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Allowremotetriggering",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("remotetriggerid",new SearchColumn(){Name="remotetriggerid",Title="remotetriggerid",SelectClause="remotetriggerid",WhereClause="AllRecords.remotetriggerid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Remotetriggerid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("flashlayer",new SearchColumn(){Name="flashlayer",Title="flashlayer",SelectClause="flashlayer",WhereClause="AllRecords.flashlayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Flashlayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("invoke",new SearchColumn(){Name="invoke",Title="invoke",SelectClause="invoke",WhereClause="AllRecords.invoke",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Invoke",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("usestoreddata",new SearchColumn(){Name="usestoreddata",Title="usestoreddata",SelectClause="usestoreddata",WhereClause="AllRecords.usestoreddata",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Usestoreddata",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("useuppercasedata",new SearchColumn(){Name="useuppercasedata",Title="useuppercasedata",SelectClause="useuppercasedata",WhereClause="AllRecords.useuppercasedata",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Useuppercasedata",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("color",new SearchColumn(){Name="color",Title="color",SelectClause="color",WhereClause="AllRecords.color",DataType="System.String",IsForeignColumn=false,PropertyName="Color",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("transition",new SearchColumn(){Name="transition",Title="transition",SelectClause="transition",WhereClause="AllRecords.transition",DataType="System.String",IsForeignColumn=false,PropertyName="Transition",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("transitionduration",new SearchColumn(){Name="transitionduration",Title="transitionduration",SelectClause="transitionduration",WhereClause="AllRecords.transitionduration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Transitionduration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("tween",new SearchColumn(){Name="tween",Title="tween",SelectClause="tween",WhereClause="AllRecords.tween",DataType="System.String",IsForeignColumn=false,PropertyName="Tween",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("direction",new SearchColumn(){Name="direction",Title="direction",SelectClause="direction",WhereClause="AllRecords.direction",DataType="System.String",IsForeignColumn=false,PropertyName="Direction",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("seek",new SearchColumn(){Name="seek",Title="seek",SelectClause="seek",WhereClause="AllRecords.seek",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Seek",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("length",new SearchColumn(){Name="length",Title="length",SelectClause="length",WhereClause="AllRecords.length",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Length",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("loop",new SearchColumn(){Name="loop",Title="loop",SelectClause="loop",WhereClause="AllRecords.loop",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Loop",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("freezeonload",new SearchColumn(){Name="freezeonload",Title="freezeonload",SelectClause="freezeonload",WhereClause="AllRecords.freezeonload",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Freezeonload",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("triggeronnext",new SearchColumn(){Name="triggeronnext",Title="triggeronnext",SelectClause="triggeronnext",WhereClause="AllRecords.triggeronnext",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Triggeronnext",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("autoplay",new SearchColumn(){Name="autoplay",Title="autoplay",SelectClause="autoplay",WhereClause="AllRecords.autoplay",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Autoplay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("timecode",new SearchColumn(){Name="timecode",Title="timecode",SelectClause="timecode",WhereClause="AllRecords.timecode",DataType="System.String",IsForeignColumn=false,PropertyName="Timecode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("positionx",new SearchColumn(){Name="positionx",Title="positionx",SelectClause="positionx",WhereClause="AllRecords.positionx",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positionx",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("positiony",new SearchColumn(){Name="positiony",Title="positiony",SelectClause="positiony",WhereClause="AllRecords.positiony",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positiony",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("scalex",new SearchColumn(){Name="scalex",Title="scalex",SelectClause="scalex",WhereClause="AllRecords.scalex",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scalex",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("scaley",new SearchColumn(){Name="scaley",Title="scaley",SelectClause="scaley",WhereClause="AllRecords.scaley",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scaley",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("defer",new SearchColumn(){Name="defer",Title="defer",SelectClause="defer",WhereClause="AllRecords.defer",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Defer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("device",new SearchColumn(){Name="device",Title="device",SelectClause="device",WhereClause="AllRecords.device",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Device",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("format",new SearchColumn(){Name="format",Title="format",SelectClause="format",WhereClause="AllRecords.format",DataType="System.String",IsForeignColumn=false,PropertyName="Format",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("showmask",new SearchColumn(){Name="showmask",Title="showmask",SelectClause="showmask",WhereClause="AllRecords.showmask",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Showmask",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("blur",new SearchColumn(){Name="blur",Title="blur",SelectClause="blur",WhereClause="AllRecords.blur",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Blur",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("key",new SearchColumn(){Name="key",Title="key",SelectClause="key",WhereClause="AllRecords.key",DataType="System.String",IsForeignColumn=false,PropertyName="Key",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("spread",new SearchColumn(){Name="spread",Title="spread",SelectClause="spread",WhereClause="AllRecords.spread",DataType="System.Double?",IsForeignColumn=false,PropertyName="Spread",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("spill",new SearchColumn(){Name="spill",Title="spill",SelectClause="spill",WhereClause="AllRecords.spill",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Spill",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("threshold",new SearchColumn(){Name="threshold",Title="threshold",SelectClause="threshold",WhereClause="AllRecords.threshold",DataType="System.Double?",IsForeignColumn=false,PropertyName="Threshold",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMosActiveItemSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMosActiveItemBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMosActiveItemAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMosActiveItemSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MosActiveItem].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MosActiveItem].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<MosActiveItem> GetMosActiveItemByEpisodeId(System.Int32? EpisodeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveItemSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveItem] with (nolock)  where EpisodeId=@EpisodeId  ";
			SqlParameter parameter=new SqlParameter("@EpisodeId",EpisodeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveItem>(ds,MosActiveItemFromDataRow);
		}

		public virtual MosActiveItem GetMosActiveItem(System.Int32 MosActiveItemId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveItemSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveItem] with (nolock)  where MosActiveItemId=@MosActiveItemId ";
			SqlParameter parameter=new SqlParameter("@MosActiveItemId",MosActiveItemId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return MosActiveItemFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<MosActiveItem> GetMosActiveItemByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveItemSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MosActiveItem] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveItem>(ds,MosActiveItemFromDataRow);
		}

		public virtual List<MosActiveItem> GetAllMosActiveItem(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveItemSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveItem] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveItem>(ds, MosActiveItemFromDataRow);
		}

		public virtual List<MosActiveItem> GetPagedMosActiveItem(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMosActiveItemCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MosActiveItemId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MosActiveItemId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MosActiveItemId] ";
            tempsql += " FROM [MosActiveItem] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MosActiveItemId"))
					tempsql += " , (AllRecords.[MosActiveItemId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MosActiveItemId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMosActiveItemSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MosActiveItem] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MosActiveItem].[MosActiveItemId] = PageIndex.[MosActiveItemId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveItem>(ds, MosActiveItemFromDataRow);
			}else{ return null;}
		}

		private int GetMosActiveItemCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MosActiveItem as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MosActiveItem as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(MosActiveItem))]
		public virtual MosActiveItem InsertMosActiveItem(MosActiveItem entity)
		{

			MosActiveItem other=new MosActiveItem();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MosActiveItem ( [EpisodeId]
				,[type]
				,[devicename]
				,[label]
				,[name]
				,[channel]
				,[videolayer]
				,[delay]
				,[duration]
				,[allowgpi]
				,[allowremotetriggering]
				,[remotetriggerid]
				,[flashlayer]
				,[invoke]
				,[usestoreddata]
				,[useuppercasedata]
				,[color]
				,[transition]
				,[transitionduration]
				,[tween]
				,[direction]
				,[seek]
				,[length]
				,[loop]
				,[freezeonload]
				,[triggeronnext]
				,[autoplay]
				,[timecode]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[positionx]
				,[positiony]
				,[scalex]
				,[scaley]
				,[defer]
				,[device]
				,[format]
				,[showmask]
				,[blur]
				,[key]
				,[spread]
				,[spill]
				,[threshold] )
				Values
				( @EpisodeId
				, @type
				, @devicename
				, @label
				, @name
				, @channel
				, @videolayer
				, @delay
				, @duration
				, @allowgpi
				, @allowremotetriggering
				, @remotetriggerid
				, @flashlayer
				, @invoke
				, @usestoreddata
				, @useuppercasedata
				, @color
				, @transition
				, @transitionduration
				, @tween
				, @direction
				, @seek
				, @length
				, @loop
				, @freezeonload
				, @triggeronnext
				, @autoplay
				, @timecode
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @positionx
				, @positiony
				, @scalex
				, @scaley
				, @defer
				, @device
				, @format
				, @showmask
				, @blur
				, @key
				, @spread
				, @spill
				, @threshold );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@devicename",entity.Devicename ?? (object)DBNull.Value)
					, new SqlParameter("@label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@videolayer",entity.Videolayer ?? (object)DBNull.Value)
					, new SqlParameter("@delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@allowgpi",entity.Allowgpi ?? (object)DBNull.Value)
					, new SqlParameter("@allowremotetriggering",entity.Allowremotetriggering ?? (object)DBNull.Value)
					, new SqlParameter("@remotetriggerid",entity.Remotetriggerid ?? (object)DBNull.Value)
					, new SqlParameter("@flashlayer",entity.Flashlayer ?? (object)DBNull.Value)
					, new SqlParameter("@invoke",entity.Invoke ?? (object)DBNull.Value)
					, new SqlParameter("@usestoreddata",entity.Usestoreddata ?? (object)DBNull.Value)
					, new SqlParameter("@useuppercasedata",entity.Useuppercasedata ?? (object)DBNull.Value)
					, new SqlParameter("@color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@transition",entity.Transition ?? (object)DBNull.Value)
					, new SqlParameter("@transitionduration",entity.Transitionduration ?? (object)DBNull.Value)
					, new SqlParameter("@tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@direction",entity.Direction ?? (object)DBNull.Value)
					, new SqlParameter("@seek",entity.Seek ?? (object)DBNull.Value)
					, new SqlParameter("@length",entity.Length ?? (object)DBNull.Value)
					, new SqlParameter("@loop",entity.Loop ?? (object)DBNull.Value)
					, new SqlParameter("@freezeonload",entity.Freezeonload ?? (object)DBNull.Value)
					, new SqlParameter("@triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@autoplay",entity.Autoplay ?? (object)DBNull.Value)
					, new SqlParameter("@timecode",entity.Timecode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@device",entity.Device ?? (object)DBNull.Value)
					, new SqlParameter("@format",entity.Format ?? (object)DBNull.Value)
					, new SqlParameter("@showmask",entity.Showmask ?? (object)DBNull.Value)
					, new SqlParameter("@blur",entity.Blur ?? (object)DBNull.Value)
					, new SqlParameter("@key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@spread",entity.Spread ?? (object)DBNull.Value)
					, new SqlParameter("@spill",entity.Spill ?? (object)DBNull.Value)
					, new SqlParameter("@threshold",entity.Threshold ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMosActiveItem(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(MosActiveItem))]
		public virtual MosActiveItem UpdateMosActiveItem(MosActiveItem entity)
		{

			if (entity.IsTransient()) return entity;
			MosActiveItem other = GetMosActiveItem(entity.MosActiveItemId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MosActiveItem set  [EpisodeId]=@EpisodeId
							, [type]=@type
							, [devicename]=@devicename
							, [label]=@label
							, [name]=@name
							, [channel]=@channel
							, [videolayer]=@videolayer
							, [delay]=@delay
							, [duration]=@duration
							, [allowgpi]=@allowgpi
							, [allowremotetriggering]=@allowremotetriggering
							, [remotetriggerid]=@remotetriggerid
							, [flashlayer]=@flashlayer
							, [invoke]=@invoke
							, [usestoreddata]=@usestoreddata
							, [useuppercasedata]=@useuppercasedata
							, [color]=@color
							, [transition]=@transition
							, [transitionduration]=@transitionduration
							, [tween]=@tween
							, [direction]=@direction
							, [seek]=@seek
							, [length]=@length
							, [loop]=@loop
							, [freezeonload]=@freezeonload
							, [triggeronnext]=@triggeronnext
							, [autoplay]=@autoplay
							, [timecode]=@timecode
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [positionx]=@positionx
							, [positiony]=@positiony
							, [scalex]=@scalex
							, [scaley]=@scaley
							, [defer]=@defer
							, [device]=@device
							, [format]=@format
							, [showmask]=@showmask
							, [blur]=@blur
							, [key]=@key
							, [spread]=@spread
							, [spill]=@spill
							, [threshold]=@threshold 
							 where MosActiveItemId=@MosActiveItemId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@devicename",entity.Devicename ?? (object)DBNull.Value)
					, new SqlParameter("@label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@videolayer",entity.Videolayer ?? (object)DBNull.Value)
					, new SqlParameter("@delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@allowgpi",entity.Allowgpi ?? (object)DBNull.Value)
					, new SqlParameter("@allowremotetriggering",entity.Allowremotetriggering ?? (object)DBNull.Value)
					, new SqlParameter("@remotetriggerid",entity.Remotetriggerid ?? (object)DBNull.Value)
					, new SqlParameter("@flashlayer",entity.Flashlayer ?? (object)DBNull.Value)
					, new SqlParameter("@invoke",entity.Invoke ?? (object)DBNull.Value)
					, new SqlParameter("@usestoreddata",entity.Usestoreddata ?? (object)DBNull.Value)
					, new SqlParameter("@useuppercasedata",entity.Useuppercasedata ?? (object)DBNull.Value)
					, new SqlParameter("@color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@transition",entity.Transition ?? (object)DBNull.Value)
					, new SqlParameter("@transitionduration",entity.Transitionduration ?? (object)DBNull.Value)
					, new SqlParameter("@tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@direction",entity.Direction ?? (object)DBNull.Value)
					, new SqlParameter("@seek",entity.Seek ?? (object)DBNull.Value)
					, new SqlParameter("@length",entity.Length ?? (object)DBNull.Value)
					, new SqlParameter("@loop",entity.Loop ?? (object)DBNull.Value)
					, new SqlParameter("@freezeonload",entity.Freezeonload ?? (object)DBNull.Value)
					, new SqlParameter("@triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@autoplay",entity.Autoplay ?? (object)DBNull.Value)
					, new SqlParameter("@timecode",entity.Timecode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@device",entity.Device ?? (object)DBNull.Value)
					, new SqlParameter("@format",entity.Format ?? (object)DBNull.Value)
					, new SqlParameter("@showmask",entity.Showmask ?? (object)DBNull.Value)
					, new SqlParameter("@blur",entity.Blur ?? (object)DBNull.Value)
					, new SqlParameter("@key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@spread",entity.Spread ?? (object)DBNull.Value)
					, new SqlParameter("@spill",entity.Spill ?? (object)DBNull.Value)
					, new SqlParameter("@threshold",entity.Threshold ?? (object)DBNull.Value)
					, new SqlParameter("@MosActiveItemId",entity.MosActiveItemId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMosActiveItem(entity.MosActiveItemId);
		}

		public virtual bool DeleteMosActiveItem(System.Int32 MosActiveItemId)
		{

			string sql="delete from MosActiveItem where MosActiveItemId=@MosActiveItemId";
			SqlParameter parameter=new SqlParameter("@MosActiveItemId",MosActiveItemId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(MosActiveItem))]
		public virtual MosActiveItem DeleteMosActiveItem(MosActiveItem entity)
		{
			this.DeleteMosActiveItem(entity.MosActiveItemId);
			return entity;
		}


		public virtual MosActiveItem MosActiveItemFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			MosActiveItem entity=new MosActiveItem();
			if (dr.Table.Columns.Contains("MosActiveItemId"))
			{
			entity.MosActiveItemId = (System.Int32)dr["MosActiveItemId"];
			}
			if (dr.Table.Columns.Contains("EpisodeId"))
			{
			entity.EpisodeId = dr["EpisodeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["EpisodeId"];
			}
			if (dr.Table.Columns.Contains("type"))
			{
			entity.Type = dr["type"].ToString();
			}
			if (dr.Table.Columns.Contains("devicename"))
			{
			entity.Devicename = dr["devicename"].ToString();
			}
			if (dr.Table.Columns.Contains("label"))
			{
			entity.Label = dr["label"].ToString();
			}
			if (dr.Table.Columns.Contains("name"))
			{
			entity.Name = dr["name"].ToString();
			}
			if (dr.Table.Columns.Contains("channel"))
			{
			entity.Channel = dr["channel"].ToString();
			}
			if (dr.Table.Columns.Contains("videolayer"))
			{
			entity.Videolayer = dr["videolayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["videolayer"];
			}
			if (dr.Table.Columns.Contains("delay"))
			{
			entity.Delay = dr["delay"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["delay"];
			}
			if (dr.Table.Columns.Contains("duration"))
			{
			entity.Duration = dr["duration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["duration"];
			}
			if (dr.Table.Columns.Contains("allowgpi"))
			{
			entity.Allowgpi = dr["allowgpi"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["allowgpi"];
			}
			if (dr.Table.Columns.Contains("allowremotetriggering"))
			{
			entity.Allowremotetriggering = dr["allowremotetriggering"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["allowremotetriggering"];
			}
			if (dr.Table.Columns.Contains("remotetriggerid"))
			{
			entity.Remotetriggerid = dr["remotetriggerid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["remotetriggerid"];
			}
			if (dr.Table.Columns.Contains("flashlayer"))
			{
			entity.Flashlayer = dr["flashlayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["flashlayer"];
			}
			if (dr.Table.Columns.Contains("invoke"))
			{
			entity.Invoke = dr["invoke"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["invoke"];
			}
			if (dr.Table.Columns.Contains("usestoreddata"))
			{
			entity.Usestoreddata = dr["usestoreddata"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["usestoreddata"];
			}
			if (dr.Table.Columns.Contains("useuppercasedata"))
			{
			entity.Useuppercasedata = dr["useuppercasedata"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["useuppercasedata"];
			}
			if (dr.Table.Columns.Contains("color"))
			{
			entity.Color = dr["color"].ToString();
			}
			if (dr.Table.Columns.Contains("transition"))
			{
			entity.Transition = dr["transition"].ToString();
			}
			if (dr.Table.Columns.Contains("transitionduration"))
			{
			entity.Transitionduration = dr["transitionduration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["transitionduration"];
			}
			if (dr.Table.Columns.Contains("tween"))
			{
			entity.Tween = dr["tween"].ToString();
			}
			if (dr.Table.Columns.Contains("direction"))
			{
			entity.Direction = dr["direction"].ToString();
			}
			if (dr.Table.Columns.Contains("seek"))
			{
			entity.Seek = dr["seek"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["seek"];
			}
			if (dr.Table.Columns.Contains("length"))
			{
			entity.Length = dr["length"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["length"];
			}
			if (dr.Table.Columns.Contains("loop"))
			{
			entity.Loop = dr["loop"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["loop"];
			}
			if (dr.Table.Columns.Contains("freezeonload"))
			{
			entity.Freezeonload = dr["freezeonload"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["freezeonload"];
			}
			if (dr.Table.Columns.Contains("triggeronnext"))
			{
			entity.Triggeronnext = dr["triggeronnext"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["triggeronnext"];
			}
			if (dr.Table.Columns.Contains("autoplay"))
			{
			entity.Autoplay = dr["autoplay"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["autoplay"];
			}
			if (dr.Table.Columns.Contains("timecode"))
			{
			entity.Timecode = dr["timecode"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("positionx"))
			{
			entity.Positionx = dr["positionx"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["positionx"];
			}
			if (dr.Table.Columns.Contains("positiony"))
			{
			entity.Positiony = dr["positiony"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["positiony"];
			}
			if (dr.Table.Columns.Contains("scalex"))
			{
			entity.Scalex = dr["scalex"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["scalex"];
			}
			if (dr.Table.Columns.Contains("scaley"))
			{
			entity.Scaley = dr["scaley"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["scaley"];
			}
			if (dr.Table.Columns.Contains("defer"))
			{
			entity.Defer = dr["defer"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["defer"];
			}
			if (dr.Table.Columns.Contains("device"))
			{
			entity.Device = dr["device"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["device"];
			}
			if (dr.Table.Columns.Contains("format"))
			{
			entity.Format = dr["format"].ToString();
			}
			if (dr.Table.Columns.Contains("showmask"))
			{
			entity.Showmask = dr["showmask"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["showmask"];
			}
			if (dr.Table.Columns.Contains("blur"))
			{
			entity.Blur = dr["blur"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["blur"];
			}
			if (dr.Table.Columns.Contains("key"))
			{
			entity.Key = dr["key"].ToString();
			}
			if (dr.Table.Columns.Contains("spread"))
			{
			entity.Spread = dr["spread"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["spread"];
			}
			if (dr.Table.Columns.Contains("spill"))
			{
			entity.Spill = dr["spill"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["spill"];
			}
			if (dr.Table.Columns.Contains("threshold"))
			{
			entity.Threshold = dr["threshold"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["threshold"];
			}
			return entity;
		}

	}
	
	
}
