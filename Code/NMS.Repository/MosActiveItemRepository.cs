﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class MosActiveItemRepository: MosActiveItemRepositoryBase, IMosActiveItemRepository
	{
        public virtual List<MosActiveItem> GetMosActiveItemWithRunDownByEpisodeId(System.Int32? EpisodeId, string SelectClause = null)
        {
            string sql = "Sp_GenerateRunDown";
            SqlParameter parameter = new SqlParameter("@EpisodeId", EpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.StoredProcedure, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<MosActiveItem>(ds, MosActiveItemFromDataRow);
        }
	}
	
	
}
