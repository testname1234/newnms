﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class MosActiveTransformationRepositoryBase : Repository, IMosActiveTransformationRepositoryBase 
	{
        
        public MosActiveTransformationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("MosActiveDetailId",new SearchColumn(){Name="MosActiveDetailId",Title="MosActiveDetailId",SelectClause="MosActiveDetailId",WhereClause="AllRecords.MosActiveDetailId",DataType="System.Int32",IsForeignColumn=false,PropertyName="MosActiveDetailId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MosActiveItemId",new SearchColumn(){Name="MosActiveItemId",Title="MosActiveItemId",SelectClause="MosActiveItemId",WhereClause="AllRecords.MosActiveItemId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="MosActiveItemId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DeviceName",new SearchColumn(){Name="DeviceName",Title="DeviceName",SelectClause="DeviceName",WhereClause="AllRecords.DeviceName",DataType="System.String",IsForeignColumn=false,PropertyName="DeviceName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Label",new SearchColumn(){Name="Label",Title="Label",SelectClause="Label",WhereClause="AllRecords.Label",DataType="System.String",IsForeignColumn=false,PropertyName="Label",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Channel",new SearchColumn(){Name="Channel",Title="Channel",SelectClause="Channel",WhereClause="AllRecords.Channel",DataType="System.String",IsForeignColumn=false,PropertyName="Channel",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoLayer",new SearchColumn(){Name="VideoLayer",Title="VideoLayer",SelectClause="VideoLayer",WhereClause="AllRecords.VideoLayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="VideoLayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Delay",new SearchColumn(){Name="Delay",Title="Delay",SelectClause="Delay",WhereClause="AllRecords.Delay",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Delay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowGpi",new SearchColumn(){Name="AllowGpi",Title="AllowGpi",SelectClause="AllowGpi",WhereClause="AllRecords.AllowGpi",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="AllowGpi",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowRemoteTriggering",new SearchColumn(){Name="AllowRemoteTriggering",Title="AllowRemoteTriggering",SelectClause="AllowRemoteTriggering",WhereClause="AllRecords.AllowRemoteTriggering",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="AllowRemoteTriggering",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RemoteTriggerId",new SearchColumn(){Name="RemoteTriggerId",Title="RemoteTriggerId",SelectClause="RemoteTriggerId",WhereClause="AllRecords.RemoteTriggerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="RemoteTriggerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("positionx",new SearchColumn(){Name="positionx",Title="positionx",SelectClause="positionx",WhereClause="AllRecords.positionx",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positionx",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("positiony",new SearchColumn(){Name="positiony",Title="positiony",SelectClause="positiony",WhereClause="AllRecords.positiony",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positiony",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("scalex",new SearchColumn(){Name="scalex",Title="scalex",SelectClause="scalex",WhereClause="AllRecords.scalex",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scalex",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("scaley",new SearchColumn(){Name="scaley",Title="scaley",SelectClause="scaley",WhereClause="AllRecords.scaley",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scaley",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("transtitionDuration",new SearchColumn(){Name="transtitionDuration",Title="transtitionDuration",SelectClause="transtitionDuration",WhereClause="AllRecords.transtitionDuration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TranstitionDuration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("defer",new SearchColumn(){Name="defer",Title="defer",SelectClause="defer",WhereClause="AllRecords.defer",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Defer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("color",new SearchColumn(){Name="color",Title="color",SelectClause="color",WhereClause="AllRecords.color",DataType="System.String",IsForeignColumn=false,PropertyName="Color",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMosActiveTransformationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMosActiveTransformationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMosActiveTransformationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMosActiveTransformationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MosActiveTransformation].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MosActiveTransformation].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<MosActiveTransformation> GetMosActiveTransformationByMosActiveItemId(System.Int32? MosActiveItemId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveTransformation] with (nolock)  where MosActiveItemId=@MosActiveItemId  ";
			SqlParameter parameter=new SqlParameter("@MosActiveItemId",MosActiveItemId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveTransformation>(ds,MosActiveTransformationFromDataRow);
		}

		public virtual MosActiveTransformation GetMosActiveTransformation(System.Int32 MosActiveDetailId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveTransformation] with (nolock)  where MosActiveDetailId=@MosActiveDetailId ";
			SqlParameter parameter=new SqlParameter("@MosActiveDetailId",MosActiveDetailId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return MosActiveTransformationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<MosActiveTransformation> GetMosActiveTransformationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MosActiveTransformation] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveTransformation>(ds,MosActiveTransformationFromDataRow);
		}

		public virtual List<MosActiveTransformation> GetAllMosActiveTransformation(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosActiveTransformationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosActiveTransformation] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveTransformation>(ds, MosActiveTransformationFromDataRow);
		}

		public virtual List<MosActiveTransformation> GetPagedMosActiveTransformation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMosActiveTransformationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [MosActiveDetailId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([MosActiveDetailId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [MosActiveDetailId] ";
            tempsql += " FROM [MosActiveTransformation] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("MosActiveDetailId"))
					tempsql += " , (AllRecords.[MosActiveDetailId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[MosActiveDetailId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMosActiveTransformationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MosActiveTransformation] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MosActiveTransformation].[MosActiveDetailId] = PageIndex.[MosActiveDetailId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosActiveTransformation>(ds, MosActiveTransformationFromDataRow);
			}else{ return null;}
		}

		private int GetMosActiveTransformationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MosActiveTransformation as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MosActiveTransformation as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(MosActiveTransformation))]
		public virtual MosActiveTransformation InsertMosActiveTransformation(MosActiveTransformation entity)
		{

			MosActiveTransformation other=new MosActiveTransformation();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MosActiveTransformation ( [MosActiveItemId]
				,[Type]
				,[DeviceName]
				,[Label]
				,[Name]
				,[Channel]
				,[VideoLayer]
				,[Delay]
				,[Duration]
				,[AllowGpi]
				,[AllowRemoteTriggering]
				,[RemoteTriggerId]
				,[positionx]
				,[positiony]
				,[scalex]
				,[scaley]
				,[transtitionDuration]
				,[defer]
				,[color]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @MosActiveItemId
				, @Type
				, @DeviceName
				, @Label
				, @Name
				, @Channel
				, @VideoLayer
				, @Delay
				, @Duration
				, @AllowGpi
				, @AllowRemoteTriggering
				, @RemoteTriggerId
				, @positionx
				, @positiony
				, @scalex
				, @scaley
				, @transtitionDuration
				, @defer
				, @color
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@MosActiveItemId",entity.MosActiveItemId ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@DeviceName",entity.DeviceName ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@VideoLayer",entity.VideoLayer ?? (object)DBNull.Value)
					, new SqlParameter("@Delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@AllowGpi",entity.AllowGpi ?? (object)DBNull.Value)
					, new SqlParameter("@AllowRemoteTriggering",entity.AllowRemoteTriggering ?? (object)DBNull.Value)
					, new SqlParameter("@RemoteTriggerId",entity.RemoteTriggerId ?? (object)DBNull.Value)
					, new SqlParameter("@positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@transtitionDuration",entity.TranstitionDuration ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMosActiveTransformation(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(MosActiveTransformation))]
		public virtual MosActiveTransformation UpdateMosActiveTransformation(MosActiveTransformation entity)
		{

			if (entity.IsTransient()) return entity;
			MosActiveTransformation other = GetMosActiveTransformation(entity.MosActiveDetailId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MosActiveTransformation set  [MosActiveItemId]=@MosActiveItemId
							, [Type]=@Type
							, [DeviceName]=@DeviceName
							, [Label]=@Label
							, [Name]=@Name
							, [Channel]=@Channel
							, [VideoLayer]=@VideoLayer
							, [Delay]=@Delay
							, [Duration]=@Duration
							, [AllowGpi]=@AllowGpi
							, [AllowRemoteTriggering]=@AllowRemoteTriggering
							, [RemoteTriggerId]=@RemoteTriggerId
							, [positionx]=@positionx
							, [positiony]=@positiony
							, [scalex]=@scalex
							, [scaley]=@scaley
							, [transtitionDuration]=@transtitionDuration
							, [defer]=@defer
							, [color]=@color
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where MosActiveDetailId=@MosActiveDetailId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@MosActiveItemId",entity.MosActiveItemId ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@DeviceName",entity.DeviceName ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@VideoLayer",entity.VideoLayer ?? (object)DBNull.Value)
					, new SqlParameter("@Delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@AllowGpi",entity.AllowGpi ?? (object)DBNull.Value)
					, new SqlParameter("@AllowRemoteTriggering",entity.AllowRemoteTriggering ?? (object)DBNull.Value)
					, new SqlParameter("@RemoteTriggerId",entity.RemoteTriggerId ?? (object)DBNull.Value)
					, new SqlParameter("@positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@transtitionDuration",entity.TranstitionDuration ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@MosActiveDetailId",entity.MosActiveDetailId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMosActiveTransformation(entity.MosActiveDetailId);
		}

		public virtual bool DeleteMosActiveTransformation(System.Int32 MosActiveDetailId)
		{

			string sql="delete from MosActiveTransformation where MosActiveDetailId=@MosActiveDetailId";
			SqlParameter parameter=new SqlParameter("@MosActiveDetailId",MosActiveDetailId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(MosActiveTransformation))]
		public virtual MosActiveTransformation DeleteMosActiveTransformation(MosActiveTransformation entity)
		{
			this.DeleteMosActiveTransformation(entity.MosActiveDetailId);
			return entity;
		}


		public virtual MosActiveTransformation MosActiveTransformationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			MosActiveTransformation entity=new MosActiveTransformation();
			if (dr.Table.Columns.Contains("MosActiveDetailId"))
			{
			entity.MosActiveDetailId = (System.Int32)dr["MosActiveDetailId"];
			}
			if (dr.Table.Columns.Contains("MosActiveItemId"))
			{
			entity.MosActiveItemId = dr["MosActiveItemId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["MosActiveItemId"];
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"].ToString();
			}
			if (dr.Table.Columns.Contains("DeviceName"))
			{
			entity.DeviceName = dr["DeviceName"].ToString();
			}
			if (dr.Table.Columns.Contains("Label"))
			{
			entity.Label = dr["Label"].ToString();
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("Channel"))
			{
			entity.Channel = dr["Channel"].ToString();
			}
			if (dr.Table.Columns.Contains("VideoLayer"))
			{
			entity.VideoLayer = dr["VideoLayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["VideoLayer"];
			}
			if (dr.Table.Columns.Contains("Delay"))
			{
			entity.Delay = dr["Delay"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Delay"];
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("AllowGpi"))
			{
			entity.AllowGpi = dr["AllowGpi"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["AllowGpi"];
			}
			if (dr.Table.Columns.Contains("AllowRemoteTriggering"))
			{
			entity.AllowRemoteTriggering = dr["AllowRemoteTriggering"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["AllowRemoteTriggering"];
			}
			if (dr.Table.Columns.Contains("RemoteTriggerId"))
			{
			entity.RemoteTriggerId = dr["RemoteTriggerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["RemoteTriggerId"];
			}
			if (dr.Table.Columns.Contains("positionx"))
			{
			entity.Positionx = dr["positionx"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["positionx"];
			}
			if (dr.Table.Columns.Contains("positiony"))
			{
			entity.Positiony = dr["positiony"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["positiony"];
			}
			if (dr.Table.Columns.Contains("scalex"))
			{
			entity.Scalex = dr["scalex"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["scalex"];
			}
			if (dr.Table.Columns.Contains("scaley"))
			{
			entity.Scaley = dr["scaley"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["scaley"];
			}
			if (dr.Table.Columns.Contains("transtitionDuration"))
			{
			entity.TranstitionDuration = dr["transtitionDuration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["transtitionDuration"];
			}
			if (dr.Table.Columns.Contains("defer"))
			{
			entity.Defer = dr["defer"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["defer"];
			}
			if (dr.Table.Columns.Contains("color"))
			{
			entity.Color = dr["color"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
