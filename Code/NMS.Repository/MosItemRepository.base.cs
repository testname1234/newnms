﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class MosItemRepositoryBase : Repository, IMosItemRepositoryBase 
	{
        
        public MosItemRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("CasperMosItemId",new SearchColumn(){Name="CasperMosItemId",Title="CasperMosItemId",SelectClause="CasperMosItemId",WhereClause="AllRecords.CasperMosItemId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CasperMosItemId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DeviceName",new SearchColumn(){Name="DeviceName",Title="DeviceName",SelectClause="DeviceName",WhereClause="AllRecords.DeviceName",DataType="System.String",IsForeignColumn=false,PropertyName="DeviceName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Label",new SearchColumn(){Name="Label",Title="Label",SelectClause="Label",WhereClause="AllRecords.Label",DataType="System.String",IsForeignColumn=false,PropertyName="Label",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Channel",new SearchColumn(){Name="Channel",Title="Channel",SelectClause="Channel",WhereClause="AllRecords.Channel",DataType="System.String",IsForeignColumn=false,PropertyName="Channel",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoLayer",new SearchColumn(){Name="VideoLayer",Title="VideoLayer",SelectClause="VideoLayer",WhereClause="AllRecords.VideoLayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="VideoLayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Delay",new SearchColumn(){Name="Delay",Title="Delay",SelectClause="Delay",WhereClause="AllRecords.Delay",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Delay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowGpi",new SearchColumn(){Name="AllowGpi",Title="AllowGpi",SelectClause="AllowGpi",WhereClause="AllRecords.AllowGpi",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="AllowGpi",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowRemoteTriggering",new SearchColumn(){Name="AllowRemoteTriggering",Title="AllowRemoteTriggering",SelectClause="AllowRemoteTriggering",WhereClause="AllRecords.AllowRemoteTriggering",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="AllowRemoteTriggering",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RemoteTriggerId",new SearchColumn(){Name="RemoteTriggerId",Title="RemoteTriggerId",SelectClause="RemoteTriggerId",WhereClause="AllRecords.RemoteTriggerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="RemoteTriggerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashLayer",new SearchColumn(){Name="FlashLayer",Title="FlashLayer",SelectClause="FlashLayer",WhereClause="AllRecords.FlashLayer",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashLayer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Invoke",new SearchColumn(){Name="Invoke",Title="Invoke",SelectClause="Invoke",WhereClause="AllRecords.Invoke",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Invoke",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UseStoredData",new SearchColumn(){Name="UseStoredData",Title="UseStoredData",SelectClause="UseStoredData",WhereClause="AllRecords.UseStoredData",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="UseStoredData",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("useuppercasedata",new SearchColumn(){Name="useuppercasedata",Title="useuppercasedata",SelectClause="useuppercasedata",WhereClause="AllRecords.useuppercasedata",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Useuppercasedata",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("color",new SearchColumn(){Name="color",Title="color",SelectClause="color",WhereClause="AllRecords.color",DataType="System.String",IsForeignColumn=false,PropertyName="Color",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("transition",new SearchColumn(){Name="transition",Title="transition",SelectClause="transition",WhereClause="AllRecords.transition",DataType="System.String",IsForeignColumn=false,PropertyName="Transition",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("transitionDuration",new SearchColumn(){Name="transitionDuration",Title="transitionDuration",SelectClause="transitionDuration",WhereClause="AllRecords.transitionDuration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TransitionDuration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("tween",new SearchColumn(){Name="tween",Title="tween",SelectClause="tween",WhereClause="AllRecords.tween",DataType="System.String",IsForeignColumn=false,PropertyName="Tween",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("direction",new SearchColumn(){Name="direction",Title="direction",SelectClause="direction",WhereClause="AllRecords.direction",DataType="System.String",IsForeignColumn=false,PropertyName="Direction",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("seek",new SearchColumn(){Name="seek",Title="seek",SelectClause="seek",WhereClause="AllRecords.seek",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Seek",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("length",new SearchColumn(){Name="length",Title="length",SelectClause="length",WhereClause="AllRecords.length",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Length",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("loop",new SearchColumn(){Name="loop",Title="loop",SelectClause="loop",WhereClause="AllRecords.loop",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Loop",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("freezeonload",new SearchColumn(){Name="freezeonload",Title="freezeonload",SelectClause="freezeonload",WhereClause="AllRecords.freezeonload",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Freezeonload",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("triggeronnext",new SearchColumn(){Name="triggeronnext",Title="triggeronnext",SelectClause="triggeronnext",WhereClause="AllRecords.triggeronnext",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Triggeronnext",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("autoplay",new SearchColumn(){Name="autoplay",Title="autoplay",SelectClause="autoplay",WhereClause="AllRecords.autoplay",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Autoplay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("timecode",new SearchColumn(){Name="timecode",Title="timecode",SelectClause="timecode",WhereClause="AllRecords.timecode",DataType="System.String",IsForeignColumn=false,PropertyName="Timecode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("positionx",new SearchColumn(){Name="positionx",Title="positionx",SelectClause="positionx",WhereClause="AllRecords.positionx",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positionx",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("positiony",new SearchColumn(){Name="positiony",Title="positiony",SelectClause="positiony",WhereClause="AllRecords.positiony",DataType="System.Double?",IsForeignColumn=false,PropertyName="Positiony",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("scalex",new SearchColumn(){Name="scalex",Title="scalex",SelectClause="scalex",WhereClause="AllRecords.scalex",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scalex",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("scaley",new SearchColumn(){Name="scaley",Title="scaley",SelectClause="scaley",WhereClause="AllRecords.scaley",DataType="System.Double?",IsForeignColumn=false,PropertyName="Scaley",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("defer",new SearchColumn(){Name="defer",Title="defer",SelectClause="defer",WhereClause="AllRecords.defer",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Defer",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("device",new SearchColumn(){Name="device",Title="device",SelectClause="device",WhereClause="AllRecords.device",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Device",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("format",new SearchColumn(){Name="format",Title="format",SelectClause="format",WhereClause="AllRecords.format",DataType="System.String",IsForeignColumn=false,PropertyName="Format",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("showmask",new SearchColumn(){Name="showmask",Title="showmask",SelectClause="showmask",WhereClause="AllRecords.showmask",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Showmask",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("blur",new SearchColumn(){Name="blur",Title="blur",SelectClause="blur",WhereClause="AllRecords.blur",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Blur",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("key",new SearchColumn(){Name="key",Title="key",SelectClause="key",WhereClause="AllRecords.key",DataType="System.String",IsForeignColumn=false,PropertyName="Key",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("spread",new SearchColumn(){Name="spread",Title="spread",SelectClause="spread",WhereClause="AllRecords.spread",DataType="System.Double?",IsForeignColumn=false,PropertyName="Spread",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("spill",new SearchColumn(){Name="spill",Title="spill",SelectClause="spill",WhereClause="AllRecords.spill",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Spill",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("threshold",new SearchColumn(){Name="threshold",Title="threshold",SelectClause="threshold",WhereClause="AllRecords.threshold",DataType="System.Double?",IsForeignColumn=false,PropertyName="Threshold",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetMosItemSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetMosItemBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetMosItemAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetMosItemSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[MosItem].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[MosItem].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual MosItem GetMosItem(System.Int32 CasperMosItemId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosItemSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosItem] with (nolock)  where CasperMosItemId=@CasperMosItemId ";
			SqlParameter parameter=new SqlParameter("@CasperMosItemId",CasperMosItemId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return MosItemFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<MosItem> GetMosItemByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosItemSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [MosItem] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosItem>(ds,MosItemFromDataRow);
		}

		public virtual List<MosItem> GetAllMosItem(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetMosItemSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [MosItem] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosItem>(ds, MosItemFromDataRow);
		}

		public virtual List<MosItem> GetPagedMosItem(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetMosItemCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [CasperMosItemId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([CasperMosItemId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [CasperMosItemId] ";
            tempsql += " FROM [MosItem] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("CasperMosItemId"))
					tempsql += " , (AllRecords.[CasperMosItemId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[CasperMosItemId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetMosItemSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [MosItem] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [MosItem].[CasperMosItemId] = PageIndex.[CasperMosItemId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<MosItem>(ds, MosItemFromDataRow);
			}else{ return null;}
		}

		private int GetMosItemCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM MosItem as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM MosItem as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(MosItem))]
		public virtual MosItem InsertMosItem(MosItem entity)
		{

			MosItem other=new MosItem();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into MosItem ( [Type]
				,[DeviceName]
				,[Label]
				,[Name]
				,[Channel]
				,[VideoLayer]
				,[Delay]
				,[Duration]
				,[AllowGpi]
				,[AllowRemoteTriggering]
				,[RemoteTriggerId]
				,[FlashLayer]
				,[Invoke]
				,[UseStoredData]
				,[useuppercasedata]
				,[color]
				,[transition]
				,[transitionDuration]
				,[tween]
				,[direction]
				,[seek]
				,[length]
				,[loop]
				,[freezeonload]
				,[triggeronnext]
				,[autoplay]
				,[timecode]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[positionx]
				,[positiony]
				,[scalex]
				,[scaley]
				,[defer]
				,[device]
				,[format]
				,[showmask]
				,[blur]
				,[key]
				,[spread]
				,[spill]
				,[threshold] )
				Values
				( @Type
				, @DeviceName
				, @Label
				, @Name
				, @Channel
				, @VideoLayer
				, @Delay
				, @Duration
				, @AllowGpi
				, @AllowRemoteTriggering
				, @RemoteTriggerId
				, @FlashLayer
				, @Invoke
				, @UseStoredData
				, @useuppercasedata
				, @color
				, @transition
				, @transitionDuration
				, @tween
				, @direction
				, @seek
				, @length
				, @loop
				, @freezeonload
				, @triggeronnext
				, @autoplay
				, @timecode
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @positionx
				, @positiony
				, @scalex
				, @scaley
				, @defer
				, @device
				, @format
				, @showmask
				, @blur
				, @key
				, @spread
				, @spill
				, @threshold );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@DeviceName",entity.DeviceName ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@VideoLayer",entity.VideoLayer ?? (object)DBNull.Value)
					, new SqlParameter("@Delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@AllowGpi",entity.AllowGpi ?? (object)DBNull.Value)
					, new SqlParameter("@AllowRemoteTriggering",entity.AllowRemoteTriggering ?? (object)DBNull.Value)
					, new SqlParameter("@RemoteTriggerId",entity.RemoteTriggerId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashLayer",entity.FlashLayer ?? (object)DBNull.Value)
					, new SqlParameter("@Invoke",entity.Invoke ?? (object)DBNull.Value)
					, new SqlParameter("@UseStoredData",entity.UseStoredData ?? (object)DBNull.Value)
					, new SqlParameter("@useuppercasedata",entity.Useuppercasedata ?? (object)DBNull.Value)
					, new SqlParameter("@color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@transition",entity.Transition ?? (object)DBNull.Value)
					, new SqlParameter("@transitionDuration",entity.TransitionDuration ?? (object)DBNull.Value)
					, new SqlParameter("@tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@direction",entity.Direction ?? (object)DBNull.Value)
					, new SqlParameter("@seek",entity.Seek ?? (object)DBNull.Value)
					, new SqlParameter("@length",entity.Length ?? (object)DBNull.Value)
					, new SqlParameter("@loop",entity.Loop ?? (object)DBNull.Value)
					, new SqlParameter("@freezeonload",entity.Freezeonload ?? (object)DBNull.Value)
					, new SqlParameter("@triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@autoplay",entity.Autoplay ?? (object)DBNull.Value)
					, new SqlParameter("@timecode",entity.Timecode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@device",entity.Device ?? (object)DBNull.Value)
					, new SqlParameter("@format",entity.Format ?? (object)DBNull.Value)
					, new SqlParameter("@showmask",entity.Showmask ?? (object)DBNull.Value)
					, new SqlParameter("@blur",entity.Blur ?? (object)DBNull.Value)
					, new SqlParameter("@key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@spread",entity.Spread ?? (object)DBNull.Value)
					, new SqlParameter("@spill",entity.Spill ?? (object)DBNull.Value)
					, new SqlParameter("@threshold",entity.Threshold ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetMosItem(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(MosItem))]
		public virtual MosItem UpdateMosItem(MosItem entity)
		{

			if (entity.IsTransient()) return entity;
			MosItem other = GetMosItem(entity.CasperMosItemId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update MosItem set  [Type]=@Type
							, [DeviceName]=@DeviceName
							, [Label]=@Label
							, [Name]=@Name
							, [Channel]=@Channel
							, [VideoLayer]=@VideoLayer
							, [Delay]=@Delay
							, [Duration]=@Duration
							, [AllowGpi]=@AllowGpi
							, [AllowRemoteTriggering]=@AllowRemoteTriggering
							, [RemoteTriggerId]=@RemoteTriggerId
							, [FlashLayer]=@FlashLayer
							, [Invoke]=@Invoke
							, [UseStoredData]=@UseStoredData
							, [useuppercasedata]=@useuppercasedata
							, [color]=@color
							, [transition]=@transition
							, [transitionDuration]=@transitionDuration
							, [tween]=@tween
							, [direction]=@direction
							, [seek]=@seek
							, [length]=@length
							, [loop]=@loop
							, [freezeonload]=@freezeonload
							, [triggeronnext]=@triggeronnext
							, [autoplay]=@autoplay
							, [timecode]=@timecode
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [positionx]=@positionx
							, [positiony]=@positiony
							, [scalex]=@scalex
							, [scaley]=@scaley
							, [defer]=@defer
							, [device]=@device
							, [format]=@format
							, [showmask]=@showmask
							, [blur]=@blur
							, [key]=@key
							, [spread]=@spread
							, [spill]=@spill
							, [threshold]=@threshold 
							 where CasperMosItemId=@CasperMosItemId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@DeviceName",entity.DeviceName ?? (object)DBNull.Value)
					, new SqlParameter("@Label",entity.Label ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@Channel",entity.Channel ?? (object)DBNull.Value)
					, new SqlParameter("@VideoLayer",entity.VideoLayer ?? (object)DBNull.Value)
					, new SqlParameter("@Delay",entity.Delay ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@AllowGpi",entity.AllowGpi ?? (object)DBNull.Value)
					, new SqlParameter("@AllowRemoteTriggering",entity.AllowRemoteTriggering ?? (object)DBNull.Value)
					, new SqlParameter("@RemoteTriggerId",entity.RemoteTriggerId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashLayer",entity.FlashLayer ?? (object)DBNull.Value)
					, new SqlParameter("@Invoke",entity.Invoke ?? (object)DBNull.Value)
					, new SqlParameter("@UseStoredData",entity.UseStoredData ?? (object)DBNull.Value)
					, new SqlParameter("@useuppercasedata",entity.Useuppercasedata ?? (object)DBNull.Value)
					, new SqlParameter("@color",entity.Color ?? (object)DBNull.Value)
					, new SqlParameter("@transition",entity.Transition ?? (object)DBNull.Value)
					, new SqlParameter("@transitionDuration",entity.TransitionDuration ?? (object)DBNull.Value)
					, new SqlParameter("@tween",entity.Tween ?? (object)DBNull.Value)
					, new SqlParameter("@direction",entity.Direction ?? (object)DBNull.Value)
					, new SqlParameter("@seek",entity.Seek ?? (object)DBNull.Value)
					, new SqlParameter("@length",entity.Length ?? (object)DBNull.Value)
					, new SqlParameter("@loop",entity.Loop ?? (object)DBNull.Value)
					, new SqlParameter("@freezeonload",entity.Freezeonload ?? (object)DBNull.Value)
					, new SqlParameter("@triggeronnext",entity.Triggeronnext ?? (object)DBNull.Value)
					, new SqlParameter("@autoplay",entity.Autoplay ?? (object)DBNull.Value)
					, new SqlParameter("@timecode",entity.Timecode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@positionx",entity.Positionx ?? (object)DBNull.Value)
					, new SqlParameter("@positiony",entity.Positiony ?? (object)DBNull.Value)
					, new SqlParameter("@scalex",entity.Scalex ?? (object)DBNull.Value)
					, new SqlParameter("@scaley",entity.Scaley ?? (object)DBNull.Value)
					, new SqlParameter("@defer",entity.Defer ?? (object)DBNull.Value)
					, new SqlParameter("@device",entity.Device ?? (object)DBNull.Value)
					, new SqlParameter("@format",entity.Format ?? (object)DBNull.Value)
					, new SqlParameter("@showmask",entity.Showmask ?? (object)DBNull.Value)
					, new SqlParameter("@blur",entity.Blur ?? (object)DBNull.Value)
					, new SqlParameter("@key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@spread",entity.Spread ?? (object)DBNull.Value)
					, new SqlParameter("@spill",entity.Spill ?? (object)DBNull.Value)
					, new SqlParameter("@threshold",entity.Threshold ?? (object)DBNull.Value)
					, new SqlParameter("@CasperMosItemId",entity.CasperMosItemId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetMosItem(entity.CasperMosItemId);
		}

		public virtual bool DeleteMosItem(System.Int32 CasperMosItemId)
		{

			string sql="delete from MosItem where CasperMosItemId=@CasperMosItemId";
			SqlParameter parameter=new SqlParameter("@CasperMosItemId",CasperMosItemId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(MosItem))]
		public virtual MosItem DeleteMosItem(MosItem entity)
		{
			this.DeleteMosItem(entity.CasperMosItemId);
			return entity;
		}


		public virtual MosItem MosItemFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			MosItem entity=new MosItem();
			if (dr.Table.Columns.Contains("CasperMosItemId"))
			{
			entity.CasperMosItemId = (System.Int32)dr["CasperMosItemId"];
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"].ToString();
			}
			if (dr.Table.Columns.Contains("DeviceName"))
			{
			entity.DeviceName = dr["DeviceName"].ToString();
			}
			if (dr.Table.Columns.Contains("Label"))
			{
			entity.Label = dr["Label"].ToString();
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("Channel"))
			{
			entity.Channel = dr["Channel"].ToString();
			}
			if (dr.Table.Columns.Contains("VideoLayer"))
			{
			entity.VideoLayer = dr["VideoLayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["VideoLayer"];
			}
			if (dr.Table.Columns.Contains("Delay"))
			{
			entity.Delay = dr["Delay"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Delay"];
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("AllowGpi"))
			{
			entity.AllowGpi = dr["AllowGpi"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["AllowGpi"];
			}
			if (dr.Table.Columns.Contains("AllowRemoteTriggering"))
			{
			entity.AllowRemoteTriggering = dr["AllowRemoteTriggering"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["AllowRemoteTriggering"];
			}
			if (dr.Table.Columns.Contains("RemoteTriggerId"))
			{
			entity.RemoteTriggerId = dr["RemoteTriggerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["RemoteTriggerId"];
			}
			if (dr.Table.Columns.Contains("FlashLayer"))
			{
			entity.FlashLayer = dr["FlashLayer"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashLayer"];
			}
			if (dr.Table.Columns.Contains("Invoke"))
			{
			entity.Invoke = dr["Invoke"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Invoke"];
			}
			if (dr.Table.Columns.Contains("UseStoredData"))
			{
			entity.UseStoredData = dr["UseStoredData"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["UseStoredData"];
			}
			if (dr.Table.Columns.Contains("useuppercasedata"))
			{
			entity.Useuppercasedata = dr["useuppercasedata"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["useuppercasedata"];
			}
			if (dr.Table.Columns.Contains("color"))
			{
			entity.Color = dr["color"].ToString();
			}
			if (dr.Table.Columns.Contains("transition"))
			{
			entity.Transition = dr["transition"].ToString();
			}
			if (dr.Table.Columns.Contains("transitionDuration"))
			{
			entity.TransitionDuration = dr["transitionDuration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["transitionDuration"];
			}
			if (dr.Table.Columns.Contains("tween"))
			{
			entity.Tween = dr["tween"].ToString();
			}
			if (dr.Table.Columns.Contains("direction"))
			{
			entity.Direction = dr["direction"].ToString();
			}
			if (dr.Table.Columns.Contains("seek"))
			{
			entity.Seek = dr["seek"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["seek"];
			}
			if (dr.Table.Columns.Contains("length"))
			{
			entity.Length = dr["length"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["length"];
			}
			if (dr.Table.Columns.Contains("loop"))
			{
			entity.Loop = dr["loop"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["loop"];
			}
			if (dr.Table.Columns.Contains("freezeonload"))
			{
			entity.Freezeonload = dr["freezeonload"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["freezeonload"];
			}
			if (dr.Table.Columns.Contains("triggeronnext"))
			{
			entity.Triggeronnext = dr["triggeronnext"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["triggeronnext"];
			}
			if (dr.Table.Columns.Contains("autoplay"))
			{
			entity.Autoplay = dr["autoplay"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["autoplay"];
			}
			if (dr.Table.Columns.Contains("timecode"))
			{
			entity.Timecode = dr["timecode"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("positionx"))
			{
			entity.Positionx = dr["positionx"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["positionx"];
			}
			if (dr.Table.Columns.Contains("positiony"))
			{
			entity.Positiony = dr["positiony"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["positiony"];
			}
			if (dr.Table.Columns.Contains("scalex"))
			{
			entity.Scalex = dr["scalex"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["scalex"];
			}
			if (dr.Table.Columns.Contains("scaley"))
			{
			entity.Scaley = dr["scaley"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["scaley"];
			}
			if (dr.Table.Columns.Contains("defer"))
			{
			entity.Defer = dr["defer"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["defer"];
			}
			if (dr.Table.Columns.Contains("device"))
			{
			entity.Device = dr["device"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["device"];
			}
			if (dr.Table.Columns.Contains("format"))
			{
			entity.Format = dr["format"].ToString();
			}
			if (dr.Table.Columns.Contains("showmask"))
			{
			entity.Showmask = dr["showmask"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["showmask"];
			}
			if (dr.Table.Columns.Contains("blur"))
			{
			entity.Blur = dr["blur"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["blur"];
			}
			if (dr.Table.Columns.Contains("key"))
			{
			entity.Key = dr["key"].ToString();
			}
			if (dr.Table.Columns.Contains("spread"))
			{
			entity.Spread = dr["spread"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["spread"];
			}
			if (dr.Table.Columns.Contains("spill"))
			{
			entity.Spill = dr["spill"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["spill"];
			}
			if (dr.Table.Columns.Contains("threshold"))
			{
			entity.Threshold = dr["threshold"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["threshold"];
			}
			return entity;
		}

	}
	
	
}
