﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{

    public partial class NewsBucketRepository : NewsBucketRepositoryBase, INewsBucketRepository
    {

        public List<NewsBucket> GetNewsBucketByProgramIds(string CommaSepratedIds)
        {
            string sql = "select * from newsbucket with (nolock) where programid in(" + CommaSepratedIds + ")";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsBucket>(ds, NewsBucketFromDataRow);
        }

        public NewsBucket UpdateSequence(NewsBucket item)
        {
            string sql = @"update NewsBucket set SequnceNumber=@SequnceNumber where NewsBucketId =@NewsBucketId; ";
            sql += "select * from NewsBucket  where NewsBucketId =@NewsBucketId";
            SqlParameter []  parameterArray = new SqlParameter [] {
                new SqlParameter("@SequnceNumber", item.SequnceNumber),
                new SqlParameter("@NewsBucketId", item.NewsBucketId)
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql,parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return NewsBucketFromDataRow(ds.Tables[0].Rows[0]);
        }
    }


}
