﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class NewsCategoryRepository : NewsCategoryRepositoryBase, INewsCategoryRepository
    {
        public NewsCategoryRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public NewsCategoryRepository()
        {

        }

        public virtual List<NewsCategory> GetByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {
            string sql = @"select c.CategoryId,c.Category,c.ParentId,c.IsApproved,cat.CreationDate,cat.LastUpdateDate from [NewsCategory] cat with (nolock)  
                         inner join Category c on cat.CategoryId = c.CategoryId
                         where NewsGuid=@NewsGuid";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsCategory>(ds, NewsCategoryFromDataRowCustom);
        }


        public virtual NewsCategory NewsCategoryFromDataRowCustom(DataRow dr)
        {
            if (dr == null) return null;
            NewsCategory entity = new NewsCategory();
            if (dr.Table.Columns.Contains("NewsCategoryId"))
            {
                entity.NewsCategoryId = (System.Int32)dr["NewsCategoryId"];
            }
            if (dr.Table.Columns.Contains("NewsId"))
            {
                entity.NewsId = (System.Int32)dr["NewsId"];
            }
            if (dr.Table.Columns.Contains("CategoryId"))
            {
                entity.CategoryId = (System.Int32)dr["CategoryId"];
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("NewsGuid"))
            {
                entity.NewsGuid = dr["NewsGuid"].ToString();
            }
            if (dr.Table.Columns.Contains("Category"))
            {
                entity.NewsGuid = dr["Category"].ToString();
            }
            if (dr.Table.Columns.Contains("ParentId"))
            {
                entity.NewsGuid = dr["ParentId"].ToString();
            }
            if (dr.Table.Columns.Contains("IsApproved"))
            {
                entity.NewsGuid = dr["IsApproved"].ToString();
            }
         
            return entity;
        }


        public virtual bool DeleteNewsCategoryByNewsId(System.Int32 NewsId)
        {
            string sql = "delete from NewsCategory where NewsId=@NewsId";
            SqlParameter parameter = new SqlParameter("@NewsId", NewsId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual void InsertNewsCategoryWithPk(NewsCategory entity)
        {
            string sql = @"
set identity_insert NewsCategory on 
Insert into NewsCategory ( NewsCategoryId,[NewsId]
				,[CategoryId]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @NewsCategoryId
                , @NewsId
				, @CategoryId
				, @CreationDate
				, @LastUpdateDate );
set identity_insert NewsCategory off 
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@NewsCategoryId",entity.NewsCategoryId)
                    , new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual void UpdateNewsCategoryNoReturn(NewsCategory entity)
        {
            string sql = @"Update NewsCategory set  [NewsId]=@NewsId
							, [CategoryId]=@CategoryId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where NewsCategoryId=@NewsCategoryId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CategoryId",entity.CategoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@NewsCategoryId",entity.NewsCategoryId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }

    }


}
