﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsFileFilterRepositoryBase : Repository, INewsFileFilterRepositoryBase 
	{
        
        public NewsFileFilterRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsFileFilterId",new SearchColumn(){Name="NewsFileFilterId",Title="NewsFileFilterId",SelectClause="NewsFileFilterId",WhereClause="AllRecords.NewsFileFilterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileFilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterId",new SearchColumn(){Name="FilterId",Title="FilterId",SelectClause="FilterId",WhereClause="AllRecords.FilterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsFileFilterSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsFileFilterBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsFileFilterAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsFileFilterSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsFileFilter].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsFileFilter].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsFileFilter> GetNewsFileFilterByFilterId(System.Int32? FilterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileFilter] with (nolock)  where FilterId=@FilterId  ";
			SqlParameter parameter=new SqlParameter("@FilterId",FilterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileFilter>(ds,NewsFileFilterFromDataRow);
		}

		public virtual List<NewsFileFilter> GetNewsFileFilterByNewsFileId(System.Int32? NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileFilter] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileFilter>(ds,NewsFileFilterFromDataRow);
		}

		public virtual NewsFileFilter GetNewsFileFilter(System.Int32 NewsFileFilterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileFilter] with (nolock)  where NewsFileFilterId=@NewsFileFilterId ";
			SqlParameter parameter=new SqlParameter("@NewsFileFilterId",NewsFileFilterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsFileFilterFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsFileFilter> GetNewsFileFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsFileFilter] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileFilter>(ds,NewsFileFilterFromDataRow);
		}

		public virtual List<NewsFileFilter> GetAllNewsFileFilter(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileFilter] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileFilter>(ds, NewsFileFilterFromDataRow);
		}

		public virtual List<NewsFileFilter> GetPagedNewsFileFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsFileFilterCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsFileFilterId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsFileFilterId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsFileFilterId] ";
            tempsql += " FROM [NewsFileFilter] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsFileFilterId"))
					tempsql += " , (AllRecords.[NewsFileFilterId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsFileFilterId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsFileFilterSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsFileFilter] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsFileFilter].[NewsFileFilterId] = PageIndex.[NewsFileFilterId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileFilter>(ds, NewsFileFilterFromDataRow);
			}else{ return null;}
		}

		private int GetNewsFileFilterCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsFileFilter as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsFileFilter as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsFileFilter))]
		public virtual NewsFileFilter InsertNewsFileFilter(NewsFileFilter entity)
		{

			NewsFileFilter other=new NewsFileFilter();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsFileFilter ( [FilterId]
				,[NewsFileId]
				,[IsActive]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @FilterId
				, @NewsFileId
				, @IsActive
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsFileFilter(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsFileFilter))]
		public virtual NewsFileFilter UpdateNewsFileFilter(NewsFileFilter entity)
		{

			if (entity.IsTransient()) return entity;
			NewsFileFilter other = GetNewsFileFilter(entity.NewsFileFilterId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsFileFilter set  [FilterId]=@FilterId
							, [NewsFileId]=@NewsFileId
							, [IsActive]=@IsActive
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where NewsFileFilterId=@NewsFileFilterId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileFilterId",entity.NewsFileFilterId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsFileFilter(entity.NewsFileFilterId);
		}

		public virtual bool DeleteNewsFileFilter(System.Int32 NewsFileFilterId)
		{

			string sql="delete from NewsFileFilter where NewsFileFilterId=@NewsFileFilterId";
			SqlParameter parameter=new SqlParameter("@NewsFileFilterId",NewsFileFilterId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsFileFilter))]
		public virtual NewsFileFilter DeleteNewsFileFilter(NewsFileFilter entity)
		{
			this.DeleteNewsFileFilter(entity.NewsFileFilterId);
			return entity;
		}


		public virtual NewsFileFilter NewsFileFilterFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsFileFilter entity=new NewsFileFilter();
			if (dr.Table.Columns.Contains("NewsFileFilterId"))
			{
			entity.NewsFileFilterId = (System.Int32)dr["NewsFileFilterId"];
			}
			if (dr.Table.Columns.Contains("FilterId"))
			{
			entity.FilterId = dr["FilterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FilterId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = dr["NewsFileId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
