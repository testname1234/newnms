﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class NewsFileFilterRepository: NewsFileFilterRepositoryBase, INewsFileFilterRepository
	{
        public NewsFileFilter GetNewsFileFilterOfNewsFileByFilterType(int NewsFileId,int FilterTypeId)
        {
            string sql = "select * from newsfilefilter nf inner join filter f on nf.filterid = f.FilterId where nf.NewsFileId = "+NewsFileId+" and f.FilterTypeId = "+FilterTypeId;
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return NewsFileFilterFromDataRow(ds.Tables[0].Rows[0]);
        }


        public virtual bool DeleteNewsFileFilterByNewsFileId(System.Int32 NewsFileId)
        {

            string sql = "delete from NewsFileFilter where NewsFileId=@NewsFileId";
            SqlParameter parameter = new SqlParameter("@NewsFileId", NewsFileId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        

    }
	
	
}
