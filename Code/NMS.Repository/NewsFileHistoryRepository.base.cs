﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsFileHistoryRepositoryBase : Repository, INewsFileHistoryRepositoryBase 
	{
        
        public NewsFileHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsFileHistoryId",new SearchColumn(){Name="NewsFileHistoryId",Title="NewsFileHistoryId",SelectClause="NewsFileHistoryId",WhereClause="AllRecords.NewsFileHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileJson",new SearchColumn(){Name="NewsFileJson",Title="NewsFileJson",SelectClause="NewsFileJson",WhereClause="AllRecords.NewsFileJson",DataType="System.String",IsForeignColumn=false,PropertyName="NewsFileJson",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsFileHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsFileHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsFileHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsFileHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsFileHistory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsFileHistory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsFileHistory> GetNewsFileHistoryByNewsFileId(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileHistory] with (nolock)  where NewsFileId=@NewsFileId  ";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileHistory>(ds,NewsFileHistoryFromDataRow);
		}

		public virtual NewsFileHistory GetNewsFileHistory(System.Int32 NewsFileHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileHistory] with (nolock)  where NewsFileHistoryId=@NewsFileHistoryId ";
			SqlParameter parameter=new SqlParameter("@NewsFileHistoryId",NewsFileHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsFileHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsFileHistory> GetNewsFileHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsFileHistory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileHistory>(ds,NewsFileHistoryFromDataRow);
		}

		public virtual List<NewsFileHistory> GetAllNewsFileHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileHistory>(ds, NewsFileHistoryFromDataRow);
		}

		public virtual List<NewsFileHistory> GetPagedNewsFileHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsFileHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsFileHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsFileHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsFileHistoryId] ";
            tempsql += " FROM [NewsFileHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsFileHistoryId"))
					tempsql += " , (AllRecords.[NewsFileHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsFileHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsFileHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsFileHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsFileHistory].[NewsFileHistoryId] = PageIndex.[NewsFileHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileHistory>(ds, NewsFileHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetNewsFileHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsFileHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsFileHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsFileHistory))]
		public virtual NewsFileHistory InsertNewsFileHistory(NewsFileHistory entity)
		{

			NewsFileHistory other=new NewsFileHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsFileHistory ( [NewsFileId]
				,[NewsFileJson]
				,[CreationDate] )
				Values
				( @NewsFileId
				, @NewsFileJson
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@NewsFileJson",entity.NewsFileJson ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsFileHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsFileHistory))]
		public virtual NewsFileHistory UpdateNewsFileHistory(NewsFileHistory entity)
		{

			if (entity.IsTransient()) return entity;
			NewsFileHistory other = GetNewsFileHistory(entity.NewsFileHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsFileHistory set  [NewsFileId]=@NewsFileId
							, [NewsFileJson]=@NewsFileJson
							, [CreationDate]=@CreationDate 
							 where NewsFileHistoryId=@NewsFileHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@NewsFileJson",entity.NewsFileJson ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileHistoryId",entity.NewsFileHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsFileHistory(entity.NewsFileHistoryId);
		}

		public virtual bool DeleteNewsFileHistory(System.Int32 NewsFileHistoryId)
		{

			string sql="delete from NewsFileHistory where NewsFileHistoryId=@NewsFileHistoryId";
			SqlParameter parameter=new SqlParameter("@NewsFileHistoryId",NewsFileHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsFileHistory))]
		public virtual NewsFileHistory DeleteNewsFileHistory(NewsFileHistory entity)
		{
			this.DeleteNewsFileHistory(entity.NewsFileHistoryId);
			return entity;
		}


		public virtual NewsFileHistory NewsFileHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsFileHistory entity=new NewsFileHistory();
			if (dr.Table.Columns.Contains("NewsFileHistoryId"))
			{
			entity.NewsFileHistoryId = (System.Int32)dr["NewsFileHistoryId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("NewsFileJson"))
			{
			entity.NewsFileJson = dr["NewsFileJson"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
