﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsFileOrganizationRepositoryBase : Repository, INewsFileOrganizationRepositoryBase 
	{
        
        public NewsFileOrganizationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsFileOrganizationId",new SearchColumn(){Name="NewsFileOrganizationId",Title="NewsFileOrganizationId",SelectClause="NewsFileOrganizationId",WhereClause="AllRecords.NewsFileOrganizationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileOrganizationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OrganizationId",new SearchColumn(){Name="OrganizationId",Title="OrganizationId",SelectClause="OrganizationId",WhereClause="AllRecords.OrganizationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="OrganizationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsFileOrganizationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsFileOrganizationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsFileOrganizationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsFileOrganizationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsFileOrganization].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsFileOrganization].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual NewsFileOrganization GetNewsFileOrganization(System.Int32 NewsFileOrganizationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileOrganizationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileOrganization] with (nolock)  where NewsFileOrganizationId=@NewsFileOrganizationId ";
			SqlParameter parameter=new SqlParameter("@NewsFileOrganizationId",NewsFileOrganizationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsFileOrganizationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsFileOrganization> GetNewsFileOrganizationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileOrganizationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsFileOrganization] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileOrganization>(ds,NewsFileOrganizationFromDataRow);
		}

		public virtual List<NewsFileOrganization> GetAllNewsFileOrganization(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileOrganizationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFileOrganization] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileOrganization>(ds, NewsFileOrganizationFromDataRow);
		}

		public virtual List<NewsFileOrganization> GetPagedNewsFileOrganization(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsFileOrganizationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsFileOrganizationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsFileOrganizationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsFileOrganizationId] ";
            tempsql += " FROM [NewsFileOrganization] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsFileOrganizationId"))
					tempsql += " , (AllRecords.[NewsFileOrganizationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsFileOrganizationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsFileOrganizationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsFileOrganization] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsFileOrganization].[NewsFileOrganizationId] = PageIndex.[NewsFileOrganizationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFileOrganization>(ds, NewsFileOrganizationFromDataRow);
			}else{ return null;}
		}

		private int GetNewsFileOrganizationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsFileOrganization as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsFileOrganization as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsFileOrganization))]
		public virtual NewsFileOrganization InsertNewsFileOrganization(NewsFileOrganization entity)
		{

			NewsFileOrganization other=new NewsFileOrganization();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsFileOrganization ( [OrganizationId]
				,[NewsFileId]
				,[IsActive]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @OrganizationId
				, @NewsFileId
				, @IsActive
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@OrganizationId",entity.OrganizationId)
					, new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsFileOrganization(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsFileOrganization))]
		public virtual NewsFileOrganization UpdateNewsFileOrganization(NewsFileOrganization entity)
		{

			if (entity.IsTransient()) return entity;
			NewsFileOrganization other = GetNewsFileOrganization(entity.NewsFileOrganizationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsFileOrganization set  [OrganizationId]=@OrganizationId
							, [NewsFileId]=@NewsFileId
							, [IsActive]=@IsActive
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where NewsFileOrganizationId=@NewsFileOrganizationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@OrganizationId",entity.OrganizationId)
					, new SqlParameter("@NewsFileId",entity.NewsFileId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileOrganizationId",entity.NewsFileOrganizationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsFileOrganization(entity.NewsFileOrganizationId);
		}

		public virtual bool DeleteNewsFileOrganization(System.Int32 NewsFileOrganizationId)
		{

			string sql="delete from NewsFileOrganization where NewsFileOrganizationId=@NewsFileOrganizationId";
			SqlParameter parameter=new SqlParameter("@NewsFileOrganizationId",NewsFileOrganizationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsFileOrganization))]
		public virtual NewsFileOrganization DeleteNewsFileOrganization(NewsFileOrganization entity)
		{
			this.DeleteNewsFileOrganization(entity.NewsFileOrganizationId);
			return entity;
		}


		public virtual NewsFileOrganization NewsFileOrganizationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsFileOrganization entity=new NewsFileOrganization();
			if (dr.Table.Columns.Contains("NewsFileOrganizationId"))
			{
			entity.NewsFileOrganizationId = (System.Int32)dr["NewsFileOrganizationId"];
			}
			if (dr.Table.Columns.Contains("OrganizationId"))
			{
			entity.OrganizationId = (System.Int32)dr["OrganizationId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
