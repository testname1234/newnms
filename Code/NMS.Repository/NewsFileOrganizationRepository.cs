﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class NewsFileOrganizationRepository: NewsFileOrganizationRepositoryBase, INewsFileOrganizationRepository
	{
        public List<NewsFileOrganization> GetNewsFileOrganizationByNewsFileId(System.Int32 NewsFileId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsFileOrganizationSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsFileOrganization] with (nolock)  where NewsFileId=@NewsFileId ";
            SqlParameter parameter = new SqlParameter("@NewsFileId", NewsFileId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFileOrganization>(ds, NewsFileOrganizationFromDataRow);
        }
        

        public bool DeleteNewsFileOrganizationByNewsFileId(System.Int32 NewsFileId)
        {
            string sql = "delete from NewsFileOrganization where NewsFileId=@NewsFileId";
            SqlParameter parameter = new SqlParameter("@NewsFileId", NewsFileId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
        



    }
	
	
}
