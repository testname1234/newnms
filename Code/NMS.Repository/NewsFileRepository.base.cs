﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsFileRepositoryBase : Repository, INewsFileRepositoryBase 
	{
        
        public NewsFileRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceNo",new SearchColumn(){Name="SequenceNo",Title="SequenceNo",SelectClause="SequenceNo",WhereClause="AllRecords.SequenceNo",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SequenceNo",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Slug",new SearchColumn(){Name="Slug",Title="Slug",SelectClause="Slug",WhereClause="AllRecords.Slug",DataType="System.String",IsForeignColumn=false,PropertyName="Slug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedBy",new SearchColumn(){Name="CreatedBy",Title="CreatedBy",SelectClause="CreatedBy",WhereClause="AllRecords.CreatedBy",DataType="System.Int32",IsForeignColumn=false,PropertyName="CreatedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FolderId",new SearchColumn(){Name="FolderId",Title="FolderId",SelectClause="FolderId",WhereClause="AllRecords.FolderId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FolderId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Title",new SearchColumn(){Name="Title",Title="Title",SelectClause="Title",WhereClause="AllRecords.Title",DataType="System.String",IsForeignColumn=false,PropertyName="Title",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsPaperDescription",new SearchColumn(){Name="NewsPaperDescription",Title="NewsPaperDescription",SelectClause="NewsPaperDescription",WhereClause="AllRecords.NewsPaperDescription",DataType="System.String",IsForeignColumn=false,PropertyName="NewsPaperDescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PublishTime",new SearchColumn(){Name="PublishTime",Title="PublishTime",SelectClause="PublishTime",WhereClause="AllRecords.PublishTime",DataType="System.DateTime",IsForeignColumn=false,PropertyName="PublishTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Source",new SearchColumn(){Name="Source",Title="Source",SelectClause="Source",WhereClause="AllRecords.Source",DataType="System.String",IsForeignColumn=false,PropertyName="Source",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceTypeId",new SearchColumn(){Name="SourceTypeId",Title="SourceTypeId",SelectClause="SourceTypeId",WhereClause="AllRecords.SourceTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SourceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceNewsUrl",new SearchColumn(){Name="SourceNewsUrl",Title="SourceNewsUrl",SelectClause="SourceNewsUrl",WhereClause="AllRecords.SourceNewsUrl",DataType="System.String",IsForeignColumn=false,PropertyName="SourceNewsUrl",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceFilterId",new SearchColumn(){Name="SourceFilterId",Title="SourceFilterId",SelectClause="SourceFilterId",WhereClause="AllRecords.SourceFilterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SourceFilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentId",new SearchColumn(){Name="ParentId",Title="ParentId",SelectClause="ParentId",WhereClause="AllRecords.ParentId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlugId",new SearchColumn(){Name="SlugId",Title="SlugId",SelectClause="SlugId",WhereClause="AllRecords.SlugId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlugId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AssignedTo",new SearchColumn(){Name="AssignedTo",Title="AssignedTo",SelectClause="AssignedTo",WhereClause="AllRecords.AssignedTo",DataType="System.Int32?",IsForeignColumn=false,PropertyName="AssignedTo",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsStatus",new SearchColumn(){Name="NewsStatus",Title="NewsStatus",SelectClause="NewsStatus",WhereClause="AllRecords.NewsStatus",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Highlights",new SearchColumn(){Name="Highlights",Title="Highlights",SelectClause="Highlights",WhereClause="AllRecords.Highlights",DataType="System.String",IsForeignColumn=false,PropertyName="Highlights",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SearchableText",new SearchColumn(){Name="SearchableText",Title="SearchableText",SelectClause="SearchableText",WhereClause="AllRecords.SearchableText",DataType="System.String",IsForeignColumn=false,PropertyName="SearchableText",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BroadcastedCount",new SearchColumn(){Name="BroadcastedCount",Title="BroadcastedCount",SelectClause="BroadcastedCount",WhereClause="AllRecords.BroadcastedCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="BroadcastedCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsDeleted",new SearchColumn(){Name="IsDeleted",Title="IsDeleted",SelectClause="IsDeleted",WhereClause="AllRecords.IsDeleted",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsDeleted",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsVerified",new SearchColumn(){Name="IsVerified",Title="IsVerified",SelectClause="IsVerified",WhereClause="AllRecords.IsVerified",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsVerified",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsTagged",new SearchColumn(){Name="IsTagged",Title="IsTagged",SelectClause="IsTagged",WhereClause="AllRecords.IsTagged",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsTagged",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EcCount",new SearchColumn(){Name="EcCount",Title="EcCount",SelectClause="EcCount",WhereClause="AllRecords.EcCount",DataType="System.Int32",IsForeignColumn=false,PropertyName="EcCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EventType",new SearchColumn(){Name="EventType",Title="EventType",SelectClause="EventType",WhereClause="AllRecords.EventType",DataType="System.Int32?",IsForeignColumn=false,PropertyName="EventType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Coverage",new SearchColumn(){Name="Coverage",Title="Coverage",SelectClause="Coverage",WhereClause="AllRecords.Coverage",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Coverage",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReporterBureauId",new SearchColumn(){Name="ReporterBureauId",Title="ReporterBureauId",SelectClause="ReporterBureauId",WhereClause="AllRecords.ReporterBureauId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ReporterBureauId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TaggedBy",new SearchColumn(){Name="TaggedBy",Title="TaggedBy",SelectClause="TaggedBy",WhereClause="AllRecords.TaggedBy",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TaggedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("InsertedBy",new SearchColumn(){Name="InsertedBy",Title="InsertedBy",SelectClause="InsertedBy",WhereClause="AllRecords.InsertedBy",DataType="System.Int32?",IsForeignColumn=false,PropertyName="InsertedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsLive",new SearchColumn(){Name="IsLive",Title="IsLive",SelectClause="IsLive",WhereClause="AllRecords.IsLive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsLive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsFileSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsFileBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsFileAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsFileSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsFile].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsFile].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsFile> GetNewsFileByFolderId(System.Int32 FolderId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFile] with (nolock)  where FolderId=@FolderId   AND IsDeleted='false'";
			SqlParameter parameter=new SqlParameter("@FolderId",FolderId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFile>(ds,NewsFileFromDataRow);
		}

		public virtual List<NewsFile> GetNewsFileBySlugId(System.Int32? SlugId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFile] with (nolock)  where SlugId=@SlugId   AND IsDeleted='false'";
			SqlParameter parameter=new SqlParameter("@SlugId",SlugId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFile>(ds,NewsFileFromDataRow);
		}

		public virtual List<NewsFile> GetNewsFileByProgramId(System.Int32? ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFile] with (nolock)  where ProgramId=@ProgramId   AND IsDeleted='false'";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFile>(ds,NewsFileFromDataRow);
		}

		public virtual NewsFile GetNewsFile(System.Int32 NewsFileId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFile] with (nolock)  where NewsFileId=@NewsFileId  AND IsDeleted='false'";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsFileFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsFile> GetNewsFileByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsFile] with (nolock)  where {0} {1} '{2}'  AND IsDeleted='false'",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFile>(ds,NewsFileFromDataRow);
		}

		public virtual List<NewsFile> GetAllNewsFile(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFileSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFile] with (nolock)   WHERE IsDeleted='false'";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
		}

		public virtual List<NewsFile> GetPagedNewsFile(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsFileCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsFileId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsFileId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsFileId] ";
            tempsql += " FROM [NewsFile] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsFileId"))
					tempsql += " , (AllRecords.[NewsFileId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsFileId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsFileSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsFile] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsFile].[NewsFileId] = PageIndex.[NewsFileId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
			}else{ return null;}
		}

		private int GetNewsFileCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsFile as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsFile as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsFile))]
		public virtual NewsFile InsertNewsFile(NewsFile entity)
		{

			NewsFile other=new NewsFile();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsFile ( [SequenceNo]
				,[Slug]
				,[CreatedBy]
				,[StatusId]
				,[FolderId]
				,[LocationId]
				,[CategoryId]
				,[CreationDate]
				,[LastUpdateDate]
				,[Title]
				,[NewsPaperDescription]
				,[LanguageCode]
				,[PublishTime]
				,[Source]
				,[SourceTypeId]
				,[SourceNewsUrl]
				,[SourceFilterId]
				,[ParentId]
				,[SlugId]
				,[ProgramId]
				,[AssignedTo]
				,[ResourceGuid]
				,[NewsStatus]
				,[Highlights]
				,[SearchableText]
				,[BroadcastedCount]
				,[IsDeleted]
				,[IsVerified]
				,[IsTagged]
				,[EcCount]
				,[EventType]
				,[Coverage]
				,[ReporterBureauId]
				,[TaggedBy]
				,[InsertedBy]
				,[IsLive] )
				Values
				( @SequenceNo
				, @Slug
				, @CreatedBy
				, @StatusId
				, @FolderId
				, @LocationId
				, @CategoryId
				, @CreationDate
				, @LastUpdateDate
				, @Title
				, @NewsPaperDescription
				, @LanguageCode
				, @PublishTime
				, @Source
				, @SourceTypeId
				, @SourceNewsUrl
				, @SourceFilterId
				, @ParentId
				, @SlugId
				, @ProgramId
				, @AssignedTo
				, @ResourceGuid
				, @NewsStatus
				, @Highlights
				, @SearchableText
				, @BroadcastedCount
				, @IsDeleted
				, @IsVerified
				, @IsTagged
				, @EcCount
				, @EventType
				, @Coverage
				, @ReporterBureauId
				, @TaggedBy
				, @InsertedBy
				, @IsLive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SequenceNo",entity.SequenceNo ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@CreatedBy",entity.CreatedBy)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@FolderId",entity.FolderId)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperDescription",entity.NewsPaperDescription ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@PublishTime",entity.PublishTime)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@AssignedTo",entity.AssignedTo ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsStatus",entity.NewsStatus ?? (object)DBNull.Value)
					, new SqlParameter("@Highlights",entity.Highlights ?? (object)DBNull.Value)
					, new SqlParameter("@SearchableText",entity.SearchableText ?? (object)DBNull.Value)
					, new SqlParameter("@BroadcastedCount",entity.BroadcastedCount ?? (object)DBNull.Value)
					, new SqlParameter("@IsDeleted",entity.IsDeleted)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsTagged",entity.IsTagged ?? (object)DBNull.Value)
					, new SqlParameter("@EcCount",entity.EcCount)
					, new SqlParameter("@EventType",entity.EventType ?? (object)DBNull.Value)
					, new SqlParameter("@Coverage",entity.Coverage ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterBureauId",entity.ReporterBureauId ?? (object)DBNull.Value)
					, new SqlParameter("@TaggedBy",entity.TaggedBy ?? (object)DBNull.Value)
					, new SqlParameter("@InsertedBy",entity.InsertedBy ?? (object)DBNull.Value)
					, new SqlParameter("@IsLive",entity.IsLive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsFile(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsFile))]
		public virtual NewsFile UpdateNewsFile(NewsFile entity)
		{

			if (entity.IsTransient()) return entity;
			NewsFile other = GetNewsFile(entity.NewsFileId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsFile set  [SequenceNo]=@SequenceNo
							, [Slug]=@Slug
							, [StatusId]=@StatusId
							, [FolderId]=@FolderId
							, [LocationId]=@LocationId
							, [CategoryId]=@CategoryId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [Title]=@Title
							, [NewsPaperDescription]=@NewsPaperDescription
							, [LanguageCode]=@LanguageCode
							, [PublishTime]=@PublishTime
							, [Source]=@Source
							, [SourceTypeId]=@SourceTypeId
							, [SourceNewsUrl]=@SourceNewsUrl
							, [SourceFilterId]=@SourceFilterId
							, [ParentId]=@ParentId
							, [SlugId]=@SlugId
							, [ProgramId]=@ProgramId
							, [AssignedTo]=@AssignedTo
							, [ResourceGuid]=@ResourceGuid
							, [NewsStatus]=@NewsStatus
							, [Highlights]=@Highlights
							, [SearchableText]=@SearchableText
							, [BroadcastedCount]=@BroadcastedCount
							, [IsDeleted]=@IsDeleted
							, [IsVerified]=@IsVerified
							, [IsTagged]=@IsTagged
							, [EcCount]=@EcCount
							, [EventType]=@EventType
							, [Coverage]=@Coverage
							, [ReporterBureauId]=@ReporterBureauId
							, [TaggedBy]=@TaggedBy
							, [InsertedBy]=@InsertedBy
							, [IsLive]=@IsLive 
							 where NewsFileId=@NewsFileId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SequenceNo",entity.SequenceNo ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug)
					, new SqlParameter("@CreatedBy",entity.CreatedBy)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@FolderId",entity.FolderId)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperDescription",entity.NewsPaperDescription ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode)
					, new SqlParameter("@PublishTime",entity.PublishTime)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@AssignedTo",entity.AssignedTo ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsStatus",entity.NewsStatus ?? (object)DBNull.Value)
					, new SqlParameter("@Highlights",entity.Highlights ?? (object)DBNull.Value)
					, new SqlParameter("@SearchableText",entity.SearchableText ?? (object)DBNull.Value)
					, new SqlParameter("@BroadcastedCount",entity.BroadcastedCount ?? (object)DBNull.Value)
					, new SqlParameter("@IsDeleted",entity.IsDeleted)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsTagged",entity.IsTagged ?? (object)DBNull.Value)
					, new SqlParameter("@EcCount",entity.EcCount)
					, new SqlParameter("@EventType",entity.EventType ?? (object)DBNull.Value)
					, new SqlParameter("@Coverage",entity.Coverage ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterBureauId",entity.ReporterBureauId ?? (object)DBNull.Value)
					, new SqlParameter("@TaggedBy",entity.TaggedBy ?? (object)DBNull.Value)
					, new SqlParameter("@InsertedBy",entity.InsertedBy ?? (object)DBNull.Value)
					, new SqlParameter("@IsLive",entity.IsLive)
					, new SqlParameter("@NewsFileId",entity.NewsFileId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsFile(entity.NewsFileId);
		}

		public virtual bool DeleteNewsFile(System.Int32 NewsFileId)
		{

			string sql="delete from NewsFile where NewsFileId=@NewsFileId";
			SqlParameter parameter=new SqlParameter("@NewsFileId",NewsFileId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsFile))]
		public virtual NewsFile DeleteNewsFile(NewsFile entity)
		{
			this.DeleteNewsFile(entity.NewsFileId);
			return entity;
		}


		public virtual NewsFile NewsFileFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsFile entity=new NewsFile();
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = (System.Int32)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("SequenceNo"))
			{
			entity.SequenceNo = dr["SequenceNo"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SequenceNo"];
			}
			if (dr.Table.Columns.Contains("Slug"))
			{
			entity.Slug = dr["Slug"].ToString();
			}
			if (dr.Table.Columns.Contains("CreatedBy"))
			{
			entity.CreatedBy = (System.Int32)dr["CreatedBy"];
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("FolderId"))
			{
			entity.FolderId = (System.Int32)dr["FolderId"];
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("Title"))
			{
			entity.Title = dr["Title"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsPaperDescription"))
			{
			entity.NewsPaperDescription = dr["NewsPaperDescription"].ToString();
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("PublishTime"))
			{
			entity.PublishTime = (System.DateTime)dr["PublishTime"];
			}
			if (dr.Table.Columns.Contains("Source"))
			{
			entity.Source = dr["Source"].ToString();
			}
			if (dr.Table.Columns.Contains("SourceTypeId"))
			{
			entity.SourceTypeId = dr["SourceTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SourceTypeId"];
			}
			if (dr.Table.Columns.Contains("SourceNewsUrl"))
			{
			entity.SourceNewsUrl = dr["SourceNewsUrl"].ToString();
			}
			if (dr.Table.Columns.Contains("SourceFilterId"))
			{
			entity.SourceFilterId = dr["SourceFilterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SourceFilterId"];
			}
			if (dr.Table.Columns.Contains("ParentId"))
			{
			entity.ParentId = dr["ParentId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentId"];
			}
			if (dr.Table.Columns.Contains("SlugId"))
			{
			entity.SlugId = dr["SlugId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlugId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("AssignedTo"))
			{
			entity.AssignedTo = dr["AssignedTo"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["AssignedTo"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = dr["ResourceGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsStatus"))
			{
			entity.NewsStatus = dr["NewsStatus"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsStatus"];
			}
			if (dr.Table.Columns.Contains("Highlights"))
			{
			entity.Highlights = dr["Highlights"].ToString();
			}
			if (dr.Table.Columns.Contains("SearchableText"))
			{
			entity.SearchableText = dr["SearchableText"].ToString();
			}
			if (dr.Table.Columns.Contains("BroadcastedCount"))
			{
			entity.BroadcastedCount = dr["BroadcastedCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["BroadcastedCount"];
			}
			if (dr.Table.Columns.Contains("IsDeleted"))
			{
			entity.IsDeleted = (System.Boolean)dr["IsDeleted"];
			}
			if (dr.Table.Columns.Contains("IsVerified"))
			{
			entity.IsVerified = dr["IsVerified"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsVerified"];
			}
			if (dr.Table.Columns.Contains("IsTagged"))
			{
			entity.IsTagged = dr["IsTagged"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsTagged"];
			}
			if (dr.Table.Columns.Contains("EcCount"))
			{
			entity.EcCount = (System.Int32)dr["EcCount"];
			}
			if (dr.Table.Columns.Contains("EventType"))
			{
			entity.EventType = dr["EventType"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["EventType"];
			}
			if (dr.Table.Columns.Contains("Coverage"))
			{
			entity.Coverage = dr["Coverage"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["Coverage"];
			}
			if (dr.Table.Columns.Contains("ReporterBureauId"))
			{
			entity.ReporterBureauId = dr["ReporterBureauId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ReporterBureauId"];
			}
			if (dr.Table.Columns.Contains("TaggedBy"))
			{
			entity.TaggedBy = dr["TaggedBy"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TaggedBy"];
			}
			if (dr.Table.Columns.Contains("InsertedBy"))
			{
			entity.InsertedBy = dr["InsertedBy"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["InsertedBy"];
			}
			if (dr.Table.Columns.Contains("IsLive"))
			{
			entity.IsLive = (System.Boolean)dr["IsLive"];
			}
			return entity;
		}

	}
	
	
}
