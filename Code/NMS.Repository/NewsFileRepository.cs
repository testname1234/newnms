﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;
using System.Linq;
using System.Text;

namespace NMS.Repository
{

    public partial class NewsFileRepository : NewsFileRepositoryBase, INewsFileRepository
    {
        public bool UpdateIsLiveBitNewsFile(int programid)
        {
            try
            {
                string sql = "update newsfile set islive = 0 where NewsFileId in (select distinct ParentId from NewsFile where ProgramId = @programid and cast(CreationDate as date) = cast(getdate() as date)and cast(cast(creationdate as datetime) + cast('05:00' as datetime) as time) < cast((select Episode.[From] from Episode  where ProgramId = @programid and cast(Episode.[From] as date) = cast(getdate() as date) and cast(Episode.[From] as time) < cast(getdate() as time)) as time))";
                SqlParameter parameter = new SqlParameter("@programid", programid);
                var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public virtual List<NewsFile> GetNewsFilebyCreationDate(DateTime dt)
        {
            string sql = "Select * from [NewsFile] with (nolock)  where CreationDate > @dt";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public List<NewsFile> GetNewsFileForTaggReport()
        {
            //string sql = "select p.*,c.ProgramId as ChildProgramId,program.Name as ProgramName,program.Interval as ProgramInterval from newsfile p inner join newsfile c on c.parentid=p.NewsFileId and p.IsTagged=1 inner join program on c.programid = program.programid where p.isdeleted = 0";
            string sql = "select c.NewsFileId as NewsFileId,c.isdeleted as ChildNewsStatus,p.NewsFileId as ParentNewsId,p.SequenceNo,p.Slug,p.CreatedBy,p.StatusId,p.FolderId,p.LocationId,p.CategoryId,p.CreationDate,p.LastUpdateDate,p.Title,p.NewsPaperDescription,p.LanguageCode,p.PublishTime,p.Source,p.SourceTypeId,p.SourceNewsUrl,p.SourceFilterId,p.ParentId,p.SlugId,p.ProgramId,p.AssignedTo,p.ResourceGuid,p.NewsStatus,p.Highlights,p.SearchableText,p.BroadcastedCount,p.IsDeleted,p.IsVerified,p.IsTagged,p.EcCount,p.ProgramId,c.ProgramId,program.Name as ProgramName,program.Interval as ProgramInterval from newsfile p inner join newsfile c on c.parentid=p.NewsFileId and p.IsTagged=1 inner join program on c.programid = program.programid where p.isdeleted = 0";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public List<NewsFile> GetBroadcastedNewsFiles(DateTime from, DateTime to)
        {
            string sql = "select * from NewsFile where NewsFileId in (select ParentId from newsfile where newsfileid in (select NewsFileId from NewsFileBroadcastedHistory where CreationDate > @from and CreationDate <= @to)) and parentid is null order by CreationDate";
            // string sql = "select p.Name as 'ProgramName', nb.CreationDate as 'BroadcastDate' , n.* from NewsFile n inner join NewsFileBroadcastedHistory nb on n.NewsFileId = nb.NewsFileId inner join Program p on n.ProgramId = p.ProgramId where nb.CreationDate > @from and nb.CreationDate <= @to order by CreationDate desc";
            SqlParameter parameter = new SqlParameter("@from", from);
            SqlParameter parameter2 = new SqlParameter("@to", to);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public List<NewsFile> TaggReportPolling(DateTime lstUpdate)
        {
            string sql = "select c.NewsFileId as NewsFileId,c.isdeleted as ChildNewsStatus,p.NewsFileId as ParentNewsId,p.SequenceNo,p.Slug,p.CreatedBy,p.StatusId,p.FolderId,p.LocationId,p.CategoryId,p.CreationDate,c.LastUpdateDate,p.Title,p.NewsPaperDescription,p.LanguageCode,p.PublishTime,p.Source,p.SourceTypeId,p.SourceNewsUrl,p.SourceFilterId,p.ParentId,p.SlugId,p.ProgramId,p.AssignedTo,p.ResourceGuid,p.NewsStatus,p.Highlights,p.SearchableText,p.BroadcastedCount,p.IsDeleted,p.IsVerified,p.IsTagged,p.EcCount,p.ProgramId,c.ProgramId,program.Name as ProgramName,program.Interval as ProgramInterval from newsfile p inner join newsfile c on c.parentid=p.NewsFileId and p.IsTagged=1 inner join program on c.programid = program.programid where p.isdeleted = 0 and c.LastUpdateDate > @date";
            SqlParameter parameter = new SqlParameter("@date", lstUpdate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }



        public virtual bool CheckIfNewsExistsSQL(string title, string source)
        {
            string sql = "Select * from [NewsFile] with (nolock)  where source = @source and title = @title";
            SqlParameter parameter = new SqlParameter("@source", source);
            SqlParameter parameter2 = new SqlParameter("@title", title);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter2 });
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0) return false; else return true;
            //return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }


        public bool MarkTitleVoiceOverStatus(NMS.Core.DataTransfer.FileDetail.PostOutput input)
        {
            try
            {
                string sql = "update newsfile set statusId = @statusId,lastupdatedate = @lastupdatedate where NewsFileId = @NewsFileId ";
                SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@NewsFileId", input.NewsFileId)
                    , new SqlParameter("@statusId",input.StatusId)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)};
                var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public virtual List<NewsFile> GetNewsFilebyLastUpdateDate(DateTime dt)
        {
            string sql = "Select * from [NewsFile] with (nolock)  where LastUpdateDate > @dt";
            SqlParameter parameter = new SqlParameter("@dt", dt);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public bool UpdateNewsFileStatusAndLastUpdate(int Id, int status, DateTime LastUpdatedate)
        {
            string sql = "Update [NewsFile] set StatusId=@status,LastUpdatedate=@LastUpdatedate where NewsfileId = @Id";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Id", Id)
                    , new SqlParameter("@status",status)
                    , new SqlParameter("@LastUpdatedate",LastUpdatedate)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }



        public bool UpdateNewsFileSequence(int Id, int sequenceNo)
        {
            string sql = "Update [NewsFile] set SequenceNo=@sequenceNo,LastUpdateDate=@LastUpdatedate where NewsfileId = @Id";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Id", Id)
                    , new SqlParameter("@sequenceNo",sequenceNo)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)};
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public NewsFile UpdateNewsFileSequenceByNewsFileId(int Id, int sequenceNo)
        {
            string sql = "Update [NewsFile] set SequenceNo=@sequenceNo,LastUpdateDate=@LastUpdatedate where NewsfileId = @Id";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Id", Id)
                    , new SqlParameter("@sequenceNo",sequenceNo)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)};
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetNewsFile(Id);
        }

        public List<NewsFile> GetReporterNewsFile(int rId, DateTime dt, int startIndex)
        {
            string sql = "Select * from [NewsFile] with (nolock)  where assignedto =@id or createdby = @id and creationdate >= @LastUpdatedate order by creationdate desc";

            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@id", rId)
                    , new SqlParameter("@LastUpdatedate",dt)
                    };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public List<NewsFile> GetNewsFileAfterDateTime(DateTime dt, int rId)
        {
            string sql = "Select * from [NewsFile] with (nolock)  where assignedto =@id or createdby = @id and creationdate > @LastUpdatedate order by creationdate desc";

            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@id", rId)
                    , new SqlParameter("@LastUpdatedate",dt)
                    };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }


        public List<NewsFile> GetBroadcatedNewsFileDetail(int Id)
        {
            string sql = "select p.Name as 'ProgramName', nb.CreationDate from NewsFileBroadcastedHistory nb inner join NewsFile n on nb.NewsFileId = n.NewsFileId inner join Program p on p.ProgramId = n.ProgramId where n.ParentId = @id";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@id", Id)
                    };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }


        public bool UpdateNewsFileStatusById(int Id, int status, DateTime LastUpdatedate)
        {
            string sql = "Update [NewsFile] set NewsStatus=@status,LastUpdatedate=@LastUpdatedate where NewsfileId = @Id";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Id", Id)
                    , new SqlParameter("@status",status)
                    , new SqlParameter("@LastUpdatedate",LastUpdatedate)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
        public bool MarkNewsVerificationStatus(int Id, bool status, DateTime LastUpdatedate)
        {
            string sql = "Update [NewsFile] set IsVerified=@status,LastUpdatedate=@LastUpdatedate where NewsfileId = @Id";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Id", Id)
                    , new SqlParameter("@status",status)
                    , new SqlParameter("@LastUpdatedate",LastUpdatedate)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public List<NewsFile> GetNewsFileWithFilterFromTo(List<Filter> filters, DateTime from, int[] folderids, string term, int PageOffset, int PageSize, DateTime? to)
        {
            var filter1 = string.Join(",", filters.Where(x => x.FilterTypeId != 13).Select(x => x.FilterId).ToArray());
            var filter2 = string.Join(",", filters.Where(x => x.FilterTypeId == 13).Select(x => x.FilterId).ToArray());
            string sql = string.Empty;

            if (!string.IsNullOrEmpty(filter2))
            {
                if (folderids != null && folderids.Count() > 0 && PageOffset == 0)
                    sql += "select fd.text, c.category, l.Location as 'BureauLocation', nf.* from newsfile nf left join filedetail fd on fd.newsfileid = nf.newsfileid left join location l on nf.ReporterBureauId = l.locationid left join Category c on c.CategoryId = nf.CategoryId where nf.folderid in (" + string.Join(",", folderids) + ") and nf.parentid is not null and nf.islive = 0 and nf.LastUpdateDate > @from "+(to.HasValue ? " and nf.IsDeleted = 0 ":"") + " order by nf.lastupdatedate desc;";

                sql += " select fd.text, c.category, l.Location as 'BureauLocation', nf.* from newsfile nf left join filedetail fd on fd.newsfileid = nf.newsfileid left join location l on nf.ReporterBureauId = l.locationid left join Category c on c.CategoryId = nf.CategoryId where nf.newsfileid in (select distinct a.NewsFileId from (select NewsFileId from NewsFileFilter where FilterId in (" + filter1 + ")) a,";
                sql += " (select NewsFileId from NewsFileFilter where FilterId in (" + filter2 + ")) b where a.NewsFileId = b.NewsFileId)" + (string.IsNullOrEmpty(term) ? "" : " and nf.SearchableText like '%" + term + "%'") + " and nf.islive = 0 and nf.LastUpdateDate > @from " + ((to.HasValue) ? " and nf.LastUpdateDate <= @to and nf.IsDeleted = 0" : "") + " order by nf.lastupdatedate desc OFFSET " + PageOffset + " ROWS FETCH NEXT " + PageSize + " ROWS ONLY;";
            }
            else
            {
                if (folderids != null && folderids.Count() > 0 && PageOffset == 0)
                    sql += "select fd.text, c.category, l.Location as 'BureauLocation', nf.* from newsfile nf left join filedetail fd on fd.newsfileid = nf.newsfileid left join location l on nf.ReporterBureauId = l.locationid left join Category c on c.CategoryId = nf.CategoryId where nf.folderid in (" + string.Join(",", folderids) + ") and nf.parentid is not null and nf.islive = 0 and nf.LastUpdateDate > @from " + (to.HasValue ? " and nf.IsDeleted = 0 " : "") + " order by nf.lastupdatedate desc;";
                sql += "select f.text, ct.category, lo.Location as 'BureauLocation', n.* from newsfile n left join filedetail f on f.newsfileid = n.newsfileid left join location lo on n.ReporterBureauId = lo.locationid left join Category ct on ct.CategoryId = n.CategoryId where n.newsfileid in (select NewsFileId from NewsFileFilter where FilterId in (" + filter1 + "))" + (string.IsNullOrEmpty(term) ? "" : " and n.SearchableText like '%" + term + "%'") + " and n.islive = 0 and n.LastUpdateDate > @from " + ((to.HasValue) ? " and n.LastUpdateDate <= @to and n.IsDeleted = 0" : "") + " order by n.lastupdatedate desc OFFSET @PageOffset ROWS FETCH NEXT @PageSize ROWS ONLY;";
            }

            List<SqlParameter> parameters = new List<SqlParameter>() {
                new SqlParameter("@from", from),
                new SqlParameter("@to", to ?? (object)DBNull.Value),
                 new SqlParameter("@PageOffset", PageOffset),
                 new SqlParameter("@PageSize", PageSize)
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters.ToArray());
            if (ds != null)
            {
                var lst = new List<NewsFile>();
                foreach (DataTable table in ds.Tables)
                {
                    var result = CollectionFromDataSet<NewsFile>(table, NewsFileFromDataRow);
                    if (result != null)
                        lst.AddRange(result);
                }
                return lst;
            }
            return null;
        }


        public List<NewsFile> GetSocialMediaNewsFileWithFilterFromTo(List<Filter> filters, DateTime from, bool includeProgramRelatedNews, string term, string PageOffset, string PageSize, DateTime? to, string SocialMediaFilterId)
        {
            var filter2 = string.Join(",", filters.Where(x => x.FilterTypeId == 13).Select(x => x.FilterId).ToArray());
            string sql = string.Empty;
            if (!string.IsNullOrEmpty(filter2))
            {
                if (includeProgramRelatedNews)
                    sql += "select * from newsfile where newsfileid in (select distinct newsfileid from NewsFileFilter where FilterId = " + SocialMediaFilterId + ") and programid is not null and parentid is not null and LastUpdateDate > @from and IsDeleted = 0 order by lastupdatedate desc;";

                sql += " select * from newsfile where newsfileid in (select distinct a.NewsFileId from (select NewsFileId from NewsFileFilter where FilterId =" + SocialMediaFilterId + ") a,";
                sql += " (select NewsFileId from NewsFileFilter where FilterId in (" + filter2 + ")) b where a.NewsFileId = b.NewsFileId)" + (string.IsNullOrEmpty(term) ? "" : " and SearchableText like '%" + term + "%'") + " and LastUpdateDate > @from " + ((to.HasValue) ? " and LastUpdateDate <= @to" : "") + " and IsDeleted = 0 order by lastupdatedate desc OFFSET " + PageOffset + " ROWS FETCH NEXT " + PageSize + " ROWS ONLY;";
            }
            else
            {
                if (includeProgramRelatedNews)
                    sql += "select * from newsfile where newsfileid in (select distinct newsfileid from NewsFileFilter where FilterId = " + SocialMediaFilterId + ") and programid is not null and parentid is not null and LastUpdateDate > @from and IsDeleted = 0 order by lastupdatedate desc;";
                sql += "select * from newsfile where (" + (includeProgramRelatedNews ? "(programid is not null and parentid is not null) or " : "") + " (newsfileid in (select NewsFileId from NewsFileFilter where FilterId in (" + SocialMediaFilterId + "))" + (string.IsNullOrEmpty(term) ? "" : " and SearchableText like '%" + term + "%'") + ")) and LastUpdateDate > @from " + ((to.HasValue) ? " and LastUpdateDate <= @to" : "") + " and IsDeleted = 0 order by lastupdatedate desc OFFSET " + PageOffset + " ROWS FETCH NEXT " + PageSize + " ROWS ONLY;";
            }

            List<SqlParameter> parameters = new List<SqlParameter>() {
                new SqlParameter("@from", from),
                new SqlParameter("@to", to ?? (object)DBNull.Value),
                 new SqlParameter("@PageOffset", PageOffset),
                 new SqlParameter("@PageSize", PageSize)
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters.ToArray());
            if (ds != null)
            {
                var lst = new List<NewsFile>();
                foreach (DataTable table in ds.Tables)
                {
                    var result = CollectionFromDataSet<NewsFile>(table, NewsFileFromDataRow);
                    if (result != null)
                        lst.AddRange(result);
                }
                return lst;
            }
            return null;
        }


        //public List<NewsFile> GetNewsFileWithFilterFromTo(List<Filter> filters, DateTime from, DateTime to, bool includeProgramRelatedNews, string term, string PageOffset, string PageSize)
        //{
        //    var filter1 = string.Join(",", filters.Where(x => x.FilterTypeId != 13).Select(x => x.FilterId).ToArray());
        //    var filter2 = string.Join(",", filters.Where(x => x.FilterTypeId == 13).Select(x => x.FilterId).ToArray());
        //    string sql = string.Empty;
        //    if (!string.IsNullOrEmpty(filter2))
        //    {
        //        sql = "select * from newsfile where (" + (includeProgramRelatedNews ? "(programid is not null and parentid is not null) or " : "") + " (newsfileid in (select distinct a.NewsFileId from (select NewsFileId from NewsFileFilter where FilterId in (" + filter1 + ")) a,";
        //        sql += " (select NewsFileId from NewsFileFilter where FilterId in (" + filter2 + ")) b where a.NewsFileId = b.NewsFileId)" + (string.IsNullOrEmpty(term) ? "" : " and SearchableText like '%" + term + "%'") + ")) and LastUpdateDate > @from and LastUpdateDate <= @to and isDeleted = 0 order by lastupdatedate desc OFFSET @PageSize * (@PageOffset - 1) ROWS FETCH NEXT @PageSize ROWS ONLY";
        //    }
        //    else
        //    {
        //        sql = "select * from newsfile where (" + (includeProgramRelatedNews ? "(programid is not null and parentid is not null) or " : "") + " (newsfileid in (select NewsFileId from NewsFileFilter where FilterId in (" + filter1 + "))" + (string.IsNullOrEmpty(term) ? "" : " and SearchableText like '%" + term + "%'") + ")) and LastUpdateDate > @from and LastUpdateDate <= @to and isDeleted = 0 order by lastupdatedate desc OFFSET PageSize * (@PageOffset - 1) ROWS FETCH NEXT PageSize ROWS ONLY";
        //    }
        //    List<SqlParameter> parameterArray = new List<SqlParameter> {
        //             new SqlParameter("@from", from)
        //            , new SqlParameter("@to",to)
        //            ,new SqlParameter("@PageSize",PageSize)
        //             ,new SqlParameter("@PageOffset",PageOffset)};
        //    DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray.ToArray());
        //    if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
        //    return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        //}


        public List<NewsFile> GetNewsFileWithFilterFromTo(string From, string To, int rId)
        {
            string sql = "select * from newsfile where assignedto =@id or createdby = @id and CreationDate > @from and CreationDate <= @to order by CreationDate desc";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@from", From)
                    , new SqlParameter("@to",To)
                    , new SqlParameter("@id",rId)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }


        public List<NewsFile> GetNewsFileWithFilterFromToAndSearch(string From, string To, string SearchText, int rId)
        {
            string sql = "select * from newsfile where assignedto =@id or createdby = @id and CreationDate > @from and CreationDate <= @to and slug like '%" + SearchText + "%' order by CreationDate desc";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@from", From)
                    , new SqlParameter("@to",To)
                    ,new SqlParameter("@searchtext",SearchText)
                    , new SqlParameter("@id",rId)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public List<NewsFile> GetNewsFileByProgramIdwithstatus(int ProgramId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsFileSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsFile] with (nolock) where ProgramId=@ProgramId and parentid is not null and isDeleted = 0";
            SqlParameter parameter = new SqlParameter("@ProgramId", ProgramId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public virtual List<NewsFile> GetLastInsertNewsFile(string SelectClause = null)
        {

            string sql = "select top 1 * from NewsFile order by NewsFileId desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public List<NewsFile> Getnewsfilebytitleandsource(string Title, string source, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsFileSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsFile] with (nolock) where title=@Title and source=@source";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Title", Title)
                    , new SqlParameter("@source",source) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public int GetMaxSequenceNumByFolderId(System.Int32 FolderId)
        {
            string sql = "select max(SequenceNo) from [NewsFile] with (nolock)  where FolderId=@FolderId and IsDeleted = 0 and parentid is not null";
            SqlParameter parameter = new SqlParameter("@FolderId", FolderId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return -1;
            return ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0]) : -1;
        }

        public virtual List<NewsFile> GetNewsFilesByFolderId(int FolderId, int StoryCount)
        {
            string sql = "select TOP (@StoryCount) * from newsfile where FolderId =@FolderId and IsDeleted=0 and ParentId is not null ORDER BY SequenceNo asc";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@FolderId", FolderId)
                    , new SqlParameter("@StoryCount",StoryCount)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public override NewsFile NewsFileFromDataRow(DataRow dr)
        {
            var entity = base.NewsFileFromDataRow(dr);
            if (dr.Table.Columns.Contains("ProgramName"))
            {
                entity.ProgramName = (string)dr["ProgramName"];
            }
            if (dr.Table.Columns.Contains("ProgramInterval"))
            {
                entity.ProgramInterval = (int)dr["ProgramInterval"];
            }
            if (dr.Table.Columns.Contains("ChildProgramId"))
            {
                entity.ChildProgramId = (int)dr["ChildProgramId"];
            }
            if (dr.Table.Columns.Contains("ParentNewsId"))
            {
                entity.ParentNewsId = (int)dr["ParentNewsId"];
            }
            if (dr.Table.Columns.Contains("ChildNewsStatus"))
            {
                entity.ChildNewsStatus = (bool)dr["ChildNewsStatus"];
            }
            if (dr.Table.Columns.Contains("BroadcastDate"))
            {
                entity.BroadcastDate = (DateTime)dr["BroadcastDate"];
            }
            if (dr.Table.Columns.Contains("BureauLocation"))
            {
                if (dr["BureauLocation"] == DBNull.Value)
                    entity.BureauLocation = string.Empty;

                else
                    entity.BureauLocation = (string)dr["BureauLocation"];
            }
            if (dr.Table.Columns.Contains("Category"))
            {
                if (dr["Category"] == DBNull.Value)
                    entity.Category = string.Empty;

                else
                entity.Category = (string)dr["Category"];
            }
            if (dr.Table.Columns.Contains("text"))
            {
                if (dr["text"] == DBNull.Value || string.IsNullOrEmpty(dr["text"].ToString()))
                    entity.VoiceOver = false;

                else
                    entity.VoiceOver = true;
            }
            if (dr.Table.Columns.Contains("Highlights"))
            {
                if (dr["Highlights"] == DBNull.Value || string.IsNullOrEmpty(dr["Highlights"].ToString()))
                    entity.IsTitle = false;

                else
                    entity.IsTitle = true;
            }

            return entity;
        }

        public bool UpdateProgramCountAndHistory(int prgramId)
        {
            string sql = @"
            declare @MaxCount int = 0
            select top 1 @MaxCount = isnull(MaxStoryCount,0) from Program where ProgramId = @ProgramId
			
			select top(@MaxCount) nf.NewsFileId into #childnews  from NewsFile nf with(nolock)
                inner join FileDetail fd with(nolock) on nf.newsfileid = fd.newsfileid
                where 
                    nf.ProgramId = @ProgramId 
                    and nf.IsDeleted = 0
                    and nullif(nf.Highlights, '') is not null
				order by 
					nf.SequenceNo
				
					
			update nf set 
				nf.BroadcastedCount = case when nullif(nf.Highlights, '') = null
					then nf.BroadcastedCount else 
					isnull(nf.BroadcastedCount, 0) + 1 
					end, 
					nf.LastUpdateDate = @lastUpdatedDate 
				from NewsFile nf with (nolock)
				where nf.NewsFileId in (select NewsFileId from #childnews)

            update nfp set 
				nfp.BroadcastedCount = case when nullif(nf.Highlights, '') = null
					then nfp.BroadcastedCount else 
					isnull(nfp.BroadcastedCount, 0) + 1 
					end, 
					nfp.LastUpdateDate = @lastUpdatedDate 
				from NewsFile nf with (nolock)
				join NewsFile nfp with (nolock) on nfp.NewsFileId = nf.ParentId
				where nf.NewsFileId in (select NewsFileId from #childnews)
				

					
			drop table #childnews

                insert into [NewsFileBroadcastedHistory]([NewsFileId], [Highlights], [Text], [CreationDate])
				select * from 
                (
				select top(@MaxCount) nf.NewsFileId, nf.Highlights,fd.[Text], @lastUpdatedDate lastUpdatedDate  from NewsFile nf with(nolock)
                inner join FileDetail fd with(nolock) on nf.newsfileid = fd.newsfileid
				
                where 
                nf.ProgramId = @ProgramId 
                    and nf.IsDeleted = 0
				order by nf.SequenceNo
				) tbl
				where 
					nullif(tbl.Highlights, '') is not null";

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@ProgramId", prgramId));
            param.Add(new SqlParameter("@lastUpdatedDate", DateTime.UtcNow));

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, param.ToArray()) > 0;
        }

        public virtual List<NewsFile> GetAllDescrepencyByStatus(int DescreypencyType)
        {
            string sql = "select * from newsfile where descrepencytype =@DescreypencyType and DescrepencyStatusCode=1";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@descrepencytype", DescreypencyType)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFile>(ds, NewsFileFromDataRow);
        }

        public bool UpdateDescrepancyNewsByValue(int DescrepencyType, string DescrepencyValue)
        {
            string sql = "Update MDescrepencyNews set DescrepencyStatusCode=1,LastUpdatedate=@LastUpdatedate where DescrepencyType = @DescrepencyType and DescrepencyValue=@DescrepencyValue";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@DescrepencyType", DescrepencyType)
                    , new SqlParameter("@DescrepencyValue",DescrepencyValue)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public NewsFile UpdateNewsFileCategoryById(int NewsFileId, int CategoryId)
        {
            string sql = "Update newsfile set CategoryId=@CategoryId,LastUpdatedate=@LastUpdatedate where NewsFileId = @NewsFileId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@NewsFileId", NewsFileId)
                    , new SqlParameter("@CategoryId",CategoryId)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetNewsFile(NewsFileId);
        }
        public NewsFile UpdateNewsFileLocationById(int NewsFileId, int LocationId)
        {
            string sql = "Update newsfile set LocationId=@LocationId,LastUpdatedate=@LastUpdatedate where NewsFileId = @NewsFileId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@NewsFileId", NewsFileId)
                    , new SqlParameter("@LocationId",LocationId)
                    , new SqlParameter("@LastUpdatedate",DateTime.UtcNow)
                    };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetNewsFile(NewsFileId);
        }

        public List<NewsFile> GetNewsByLocationIds(List<int> locationIds, DateTime lastSyncTime)
        {
            string sql = @"select NewsFileId, Slug, Title from NewsFile where locationid in (" + (locationIds.Count > 0 ? string.Join(",", locationIds) : "0") + @") 
                and parentid is null
                and CreationDate >= @LastUpdatedate";
            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@LastUpdatedate", lastSyncTime)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<NewsFile>();
            return ds.Tables[0].AsEnumerable().Select(x => new NewsFile()
            {
                NewsFileId = Convert.ToInt32(x["NewsFileId"]),
                Slug = Convert.ToString(x["Slug"]),
                Title = Convert.ToString(x["Title"])
            }).ToList();
        }

        public List<NewsFile> GetNewsByTagAndEntityIds(List<int> tagIds, List<int> entityIds, DateTime lastSyncTime)
        {
            string sql = @"select nf.NewsFileId, Slug, Title from newsfile nf with (nolock)
left join FileTag ft with (nolock) on nf.NewsFileId = ft.NewsFileId
left join NewsFileOrganization fe with (nolock) on nf.NewsFileId = fe.NewsFileId and fe.IsActive = 1
where 
(ft.TagId in (" + (tagIds.Count > 0 ?  string.Join(",", tagIds) : "0") + @")
or fe.OrganizationId in ("+ (entityIds.Count > 0 ? string.Join(",", entityIds) : "0") + @"))
and nf.ParentId is null
and nf.CreationDate >= @LastUpdatedate";
            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@LastUpdatedate", lastSyncTime)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<NewsFile>();
            return ds.Tables[0].AsEnumerable().Select(x => new NewsFile()
            {
                NewsFileId = Convert.ToInt32(x["NewsFileId"]),
                Slug = Convert.ToString(x["Slug"]),
                Title = Convert.ToString(x["Title"])
            }).ToList();
        }

        public List<NewsFile> GetByProgramPriority(Dictionary<int, int> programsAndCount)
        {
            StringBuilder sb = new StringBuilder();
            foreach( var pair in programsAndCount)
            {
                sb.Append($@"insert into @temp
                        select distinct top {pair.Value}  ParentId, ProgramId, SequenceNo from newsfile where 
                        
                        folderid = (select top 1 folderid from folder where programid = {pair.Key}) 
                        and  parentid is not null and IsDeleted = 0 and isnull(BroadcastedCount, 0) >= 0
                        order by SequenceNo
                        ");
            }
            string sql = $@"
declare @temp table (ParentId int, ProgramId int, SequenceNo int)
{sb.ToString()}
select * from @temp";
            SqlParameter[] parameterArray = new SqlParameter[] {
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<NewsFile>();
            return ds.Tables[0].AsEnumerable().Select(x => new NewsFile()
            {
                ProgramId = Convert.ToInt32(x["ProgramId"]),
                ParentId = Convert.ToInt32(x["ParentId"])
            }).ToList();
        }

        public List<NewsFile> GetNewsByIdsForTicker(List<int> newsIds)
        {
            string sql = @"select NewsFileId, Slug, Title from NewsFile with (nolock) where NewsFileId in (" + (newsIds.Count > 0 ?  string.Join(", ", newsIds) : "0") +")";
            SqlParameter[] parameterArray = new SqlParameter[] {
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<NewsFile>();
            return ds.Tables[0].AsEnumerable().Select(x => new NewsFile()
            {
                NewsFileId = Convert.ToInt32(x["NewsFileId"]),
                Slug = Convert.ToString(x["Slug"]),
                Title = Convert.ToString(x["Title"])
            }).ToList();
        }

        public List<NewsFile> GetNewsForFreshTicker(DateTime lastSyncTime)
        {
            string sql = @"select NewsFileId, Slug, Title, creationdate from NewsFile with (nolock) where parentid is null and NewsStatus in (1,2) and creationdate >= @LastUpdatedate order by creationdate";
            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@LastUpdatedate", lastSyncTime)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<NewsFile>();
            return ds.Tables[0].AsEnumerable().Select(x => new NewsFile()
            {
                NewsFileId = Convert.ToInt32(x["NewsFileId"]),
                Slug = Convert.ToString(x["Slug"]),
                Title = Convert.ToString(x["Title"]),
                CreationDate = Convert.ToDateTime(x["CreationDate"])
            }).ToList();
        }
    }


}
