﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsFilterRepositoryBase : Repository, INewsFilterRepositoryBase 
	{
        
        public NewsFilterRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsFilterId",new SearchColumn(){Name="NewsFilterId",Title="NewsFilterId",SelectClause="NewsFilterId",WhereClause="AllRecords.NewsFilterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsFilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterId",new SearchColumn(){Name="FilterId",Title="FilterId",SelectClause="FilterId",WhereClause="AllRecords.FilterId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReporterId",new SearchColumn(){Name="ReporterId",Title="ReporterId",SelectClause="ReporterId",WhereClause="AllRecords.ReporterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ReporterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsFilterSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsFilterBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsFilterAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsFilterSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsFilter].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsFilter].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsFilter> GetNewsFilterByFilterId(System.Int32 FilterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFilter] with (nolock)  where FilterId=@FilterId  ";
			SqlParameter parameter=new SqlParameter("@FilterId",FilterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFilter>(ds,NewsFilterFromDataRow);
		}

		public virtual List<NewsFilter> GetNewsFilterByNewsId(System.Int32 NewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFilter] with (nolock)  where NewsId=@NewsId  ";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFilter>(ds,NewsFilterFromDataRow);
		}

        public virtual List<NewsFilter> GetNewsFilterByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsFilterSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsFilter] with (nolock)  where NewsGuid=@NewsGuid  ";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsFilter>(ds, NewsFilterFromDataRow);
        }

		public virtual NewsFilter GetNewsFilter(System.Int32 NewsFilterId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFilter] with (nolock)  where NewsFilterId=@NewsFilterId ";
			SqlParameter parameter=new SqlParameter("@NewsFilterId",NewsFilterId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsFilterFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsFilter> GetNewsFilterByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsFilter] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFilter>(ds,NewsFilterFromDataRow);
		}

		public virtual List<NewsFilter> GetAllNewsFilter(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsFilterSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsFilter] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFilter>(ds, NewsFilterFromDataRow);
		}

		public virtual List<NewsFilter> GetPagedNewsFilter(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsFilterCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsFilterId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsFilterId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsFilterId] ";
            tempsql += " FROM [NewsFilter] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsFilterId"))
					tempsql += " , (AllRecords.[NewsFilterId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsFilterId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsFilterSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsFilter] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsFilter].[NewsFilterId] = PageIndex.[NewsFilterId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsFilter>(ds, NewsFilterFromDataRow);
			}else{ return null;}
		}

		private int GetNewsFilterCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsFilter as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsFilter as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsFilter))]
		public virtual NewsFilter InsertNewsFilter(NewsFilter entity)
		{

			NewsFilter other=new NewsFilter();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsFilter ( [FilterId]
				,[NewsId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[NewsGuid]
				,[ReporterId] )
				Values
				( @FilterId
				, @NewsId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @NewsGuid
				, @ReporterId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsFilter(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsFilter))]
		public virtual NewsFilter UpdateNewsFilter(NewsFilter entity)
		{

			if (entity.IsTransient()) return entity;
			NewsFilter other = GetNewsFilter(entity.NewsFilterId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsFilter set  [FilterId]=@FilterId
							, [NewsId]=@NewsId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [NewsGuid]=@NewsGuid
							, [ReporterId]=@ReporterId 
							 where NewsFilterId=@NewsFilterId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFilterId",entity.NewsFilterId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsFilter(entity.NewsFilterId);
		}

		public virtual bool DeleteNewsFilter(System.Int32 NewsFilterId)
		{

			string sql="delete from NewsFilter where NewsFilterId=@NewsFilterId";
			SqlParameter parameter=new SqlParameter("@NewsFilterId",NewsFilterId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsFilter))]
		public virtual NewsFilter DeleteNewsFilter(NewsFilter entity)
		{
			this.DeleteNewsFilter(entity.NewsFilterId);
			return entity;
		}


		public virtual NewsFilter NewsFilterFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsFilter entity=new NewsFilter();
			if (dr.Table.Columns.Contains("NewsFilterId"))
			{
			entity.NewsFilterId = (System.Int32)dr["NewsFilterId"];
			}
			if (dr.Table.Columns.Contains("FilterId"))
			{
			entity.FilterId = (System.Int32)dr["FilterId"];
			}
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = (System.Int32)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("ReporterId"))
			{
			entity.ReporterId = dr["ReporterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ReporterId"];
			}
			return entity;
		}

	}
	
	
}
