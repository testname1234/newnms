﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class NewsFilterRepository: NewsFilterRepositoryBase, INewsFilterRepository
	{
         public NewsFilterRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

         public NewsFilterRepository()
        {
        }

        

        public virtual NewsFilter GetByFilterIdAndNewsId(System.Int32 FilterId, System.Int32 NewsId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsFilterSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsFilter] with (nolock)  where FilterId=@FilterId And Newsid=@Newsid ";
            SqlParameter parameter1 = new SqlParameter("@FilterId", FilterId);
            SqlParameter parameter2 = new SqlParameter("@Newsid", NewsId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1,parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return NewsFilterFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual bool DeleteNewsFilterByNewsId(System.Int32 NewsId)
        {
            string sql = "delete from NewsFilter where newsid=@newsid";
            SqlParameter parameter = new SqlParameter("@newsid", NewsId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
        
        public virtual void InsertNewsFilterWithPk(NewsFilter entity)
        {
            string sql = @"
set identity_insert NewsCategory on
Insert into NewsFilter (NewsFilterId, [FilterId]
				,[NewsId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @NewsFilterId,@FilterId
				, @NewsId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
set identity_insert NewsCategory off
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@NewsFilterId",entity.NewsFilterId)
                    , new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
                var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");

        }
        
        public virtual void UpdateNewsFilterNoReturn(NewsFilter entity)
        {
            string sql = @"Update NewsFilter set  [FilterId]=@FilterId
							, [NewsId]=@NewsId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where NewsFilterId=@NewsFilterId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@FilterId",entity.FilterId)
					, new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFilterId",entity.NewsFilterId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }

	}
	
	
}
