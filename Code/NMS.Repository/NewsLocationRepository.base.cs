﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsLocationRepositoryBase : Repository, INewsLocationRepositoryBase 
	{
        
        public NewsLocationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsLocationid",new SearchColumn(){Name="NewsLocationid",Title="NewsLocationid",SelectClause="NewsLocationid",WhereClause="AllRecords.NewsLocationid",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsLocationid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("isactive",new SearchColumn(){Name="isactive",Title="isactive",SelectClause="isactive",WhereClause="AllRecords.isactive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Isactive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsLocationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsLocationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsLocationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsLocationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsLocation].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsLocation].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsLocation> GetNewsLocationByNewsId(System.Int32? NewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsLocation] with (nolock)  where NewsId=@NewsId  ";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsLocation>(ds,NewsLocationFromDataRow);
		}

        public virtual List<NewsLocation> GetNewsLocationByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsLocationSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsLocation] with (nolock)  where NewsGuid=@NewsGuid  ";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsLocation>(ds, NewsLocationFromDataRow);
        }

		public virtual List<NewsLocation> GetNewsLocationByLocationId(System.Int32? LocationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsLocation] with (nolock)  where LocationId=@LocationId  ";
			SqlParameter parameter=new SqlParameter("@LocationId",LocationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsLocation>(ds,NewsLocationFromDataRow);
		}

		public virtual NewsLocation GetNewsLocation(System.Int32 NewsLocationid,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsLocation] with (nolock)  where NewsLocationid=@NewsLocationid ";
			SqlParameter parameter=new SqlParameter("@NewsLocationid",NewsLocationid);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsLocationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsLocation> GetNewsLocationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsLocation] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsLocation>(ds,NewsLocationFromDataRow);
		}

		public virtual List<NewsLocation> GetAllNewsLocation(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsLocation] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsLocation>(ds, NewsLocationFromDataRow);
		}

		public virtual List<NewsLocation> GetPagedNewsLocation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsLocationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsLocationid] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsLocationid])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsLocationid] ";
            tempsql += " FROM [NewsLocation] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsLocationid"))
					tempsql += " , (AllRecords.[NewsLocationid])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsLocationid])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsLocationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsLocation] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsLocation].[NewsLocationid] = PageIndex.[NewsLocationid] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsLocation>(ds, NewsLocationFromDataRow);
			}else{ return null;}
		}

		private int GetNewsLocationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsLocation as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsLocation as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsLocation))]
		public virtual NewsLocation InsertNewsLocation(NewsLocation entity)
		{

			NewsLocation other=new NewsLocation();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsLocation ( [NewsId]
				,[LocationId]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive]
				,[NewsGuid] )
				Values
				( @NewsId
				, @LocationId
				, @CreationDate
				, @LastUpdateDate
				, @isactive
				, @NewsGuid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsLocation(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsLocation))]
		public virtual NewsLocation UpdateNewsLocation(NewsLocation entity)
		{

			if (entity.IsTransient()) return entity;
			NewsLocation other = GetNewsLocation(entity.NewsLocationid);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsLocation set  [NewsId]=@NewsId
							, [LocationId]=@LocationId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive
							, [NewsGuid]=@NewsGuid 
							 where NewsLocationid=@NewsLocationid";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsLocationid",entity.NewsLocationid)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsLocation(entity.NewsLocationid);
		}

		public virtual bool DeleteNewsLocation(System.Int32 NewsLocationid)
		{

			string sql="delete from NewsLocation where NewsLocationid=@NewsLocationid";
			SqlParameter parameter=new SqlParameter("@NewsLocationid",NewsLocationid);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsLocation))]
		public virtual NewsLocation DeleteNewsLocation(NewsLocation entity)
		{
			this.DeleteNewsLocation(entity.NewsLocationid);
			return entity;
		}


		public virtual NewsLocation NewsLocationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsLocation entity=new NewsLocation();
			if (dr.Table.Columns.Contains("NewsLocationid"))
			{
			entity.NewsLocationid = (System.Int32)dr["NewsLocationid"];
			}
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = dr["NewsId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("isactive"))
			{
			entity.Isactive = dr["isactive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["isactive"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			return entity;
		}

	}
	
	
}
