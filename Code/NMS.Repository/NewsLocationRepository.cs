﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class NewsLocationRepository : NewsLocationRepositoryBase, INewsLocationRepository
    {
        public NewsLocationRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public NewsLocationRepository()
        {
        }

        public virtual List<NewsLocation> GetByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {
            string sql = @"select l.LocationId,l.Location,l.ParentId,l.IsApproved,nl.CreationDate,nl.LastUpdateDate from [NewsLocation] nl with (nolock)  
                            inner join Location l on nl.LocationId = l.LocationId
                            where NewsGuid=@NewsGuid";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsLocation>(ds, NewsLocationFromDataRowCustom);
        }
        public virtual NewsLocation NewsLocationFromDataRowCustom(DataRow dr)
        {
            if (dr == null) return null;
            NewsLocation entity = new NewsLocation();
            if (dr.Table.Columns.Contains("NewsLocationid"))
            {
                entity.NewsLocationid = (System.Int32)dr["NewsLocationid"];
            }
            if (dr.Table.Columns.Contains("NewsId"))
            {
                entity.NewsId = dr["NewsId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["NewsId"];
            }
            if (dr.Table.Columns.Contains("LocationId"))
            {
                entity.LocationId = dr["LocationId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["LocationId"];
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = dr["CreationDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = dr["LastUpdateDate"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("isactive"))
            {
                entity.Isactive = dr["isactive"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["isactive"];
            }
            if (dr.Table.Columns.Contains("NewsGuid"))
            {
                entity.NewsGuid = dr["NewsGuid"].ToString();
            }

            if (dr.Table.Columns.Contains("Location"))
            {
                entity.NewsGuid = dr["Location"].ToString();
            }
            if (dr.Table.Columns.Contains("ParentId"))
            {
                entity.NewsGuid = dr["ParentId"].ToString();
            }
            if (dr.Table.Columns.Contains("IsApproved"))
            {
                entity.NewsGuid = dr["IsApproved"].ToString();
            }
            return entity;
        }

        public virtual bool DeleteNewsLocationByNewsId(System.Int32 Newsid)
        {

            string sql = "delete from NewsLocation where Newsid=@Newsid";
            SqlParameter parameter = new SqlParameter("@Newsid", Newsid);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }


        public virtual void InsertNewsLocationWithPk(NewsLocation entity)
        {
            string sql = @"
                set identity_insert NewsLocation on 
                Insert into NewsLocation ( NewsLocationId,[NewsId]
				,[LocationId]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive] )
				Values
				( @NewsLocationId
                , @NewsId
				, @LocationId
				, @CreationDate
				, @LastUpdateDate
				, @isactive );
                set identity_insert NewsLocation off
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@NewsLocationId",entity.NewsLocationid)
                     ,new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual void UpdateNewsLocationNoReturn(NewsLocation entity)
        {
            string sql = @"Update NewsLocation set 
                             [NewsId]=@NewsId                            
							, [LocationId]=@LocationId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive 
							 where NewsLocationid=@NewsLocationid";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  
                     new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsLocationid",entity.NewsLocationid)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }
    }


}
