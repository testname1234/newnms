﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsPaperPageRepositoryBase : Repository, INewsPaperPageRepositoryBase 
	{
        
        public NewsPaperPageRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsPaperPageId",new SearchColumn(){Name="NewsPaperPageId",Title="NewsPaperPageId",SelectClause="NewsPaperPageId",WhereClause="AllRecords.NewsPaperPageId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsPaperPageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DailyNewsPaperId",new SearchColumn(){Name="DailyNewsPaperId",Title="DailyNewsPaperId",SelectClause="DailyNewsPaperId",WhereClause="AllRecords.DailyNewsPaperId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DailyNewsPaperId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImageGuid",new SearchColumn(){Name="ImageGuid",Title="ImageGuid",SelectClause="ImageGuid",WhereClause="AllRecords.ImageGuid",DataType="System.Guid?",IsForeignColumn=false,PropertyName="ImageGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PageTitle",new SearchColumn(){Name="PageTitle",Title="PageTitle",SelectClause="PageTitle",WhereClause="AllRecords.PageTitle",DataType="System.String",IsForeignColumn=false,PropertyName="PageTitle",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsProcessed",new SearchColumn(){Name="IsProcessed",Title="IsProcessed",SelectClause="IsProcessed",WhereClause="AllRecords.IsProcessed",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsProcessed",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsPaperPageSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsPaperPageBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsPaperPageAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsPaperPageSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsPaperPage].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsPaperPage].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsPaperPage> GetNewsPaperPageByDailyNewsPaperId(System.Int32 DailyNewsPaperId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsPaperPage with (nolock)  where DailyNewsPaperId=@DailyNewsPaperId  ";
			SqlParameter parameter=new SqlParameter("@DailyNewsPaperId",DailyNewsPaperId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPage>(ds,NewsPaperPageFromDataRow);
		}

		public virtual NewsPaperPage GetNewsPaperPage(System.Int32 NewsPaperPageId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsPaperPage with (nolock)  where NewsPaperPageId=@NewsPaperPageId ";
			SqlParameter parameter=new SqlParameter("@NewsPaperPageId",NewsPaperPageId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsPaperPageFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsPaperPage> GetNewsPaperPageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from NewsPaperPage with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPage>(ds,NewsPaperPageFromDataRow);
		}

		public virtual List<NewsPaperPage> GetAllNewsPaperPage(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsPaperPage with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPage>(ds, NewsPaperPageFromDataRow);
		}

		public virtual List<NewsPaperPage> GetPagedNewsPaperPage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsPaperPageCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsPaperPageId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsPaperPageId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsPaperPageId] ";
            tempsql += " FROM [NewsPaperPage] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsPaperPageId"))
					tempsql += " , (AllRecords.[NewsPaperPageId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsPaperPageId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsPaperPageSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsPaperPage] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsPaperPage].[NewsPaperPageId] = PageIndex.[NewsPaperPageId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPage>(ds, NewsPaperPageFromDataRow);
			}else{ return null;}
		}

		private int GetNewsPaperPageCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsPaperPage as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsPaperPage as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsPaperPage))]
		public virtual NewsPaperPage InsertNewsPaperPage(NewsPaperPage entity)
		{

			NewsPaperPage other=new NewsPaperPage();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsPaperPage ( [DailyNewsPaperId]
				,[ImageGuid]
				,[PageTitle]
				,[IsProcessed]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @DailyNewsPaperId
				, @ImageGuid
				, @PageTitle
				, @IsProcessed
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@DailyNewsPaperId",entity.DailyNewsPaperId)
					, new SqlParameter("@ImageGuid",entity.ImageGuid ?? (object)DBNull.Value)
					, new SqlParameter("@PageTitle",entity.PageTitle)
					, new SqlParameter("@IsProcessed",entity.IsProcessed)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsPaperPage(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsPaperPage))]
		public virtual NewsPaperPage UpdateNewsPaperPage(NewsPaperPage entity)
		{

			if (entity.IsTransient()) return entity;
			NewsPaperPage other = GetNewsPaperPage(entity.NewsPaperPageId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsPaperPage set  [DailyNewsPaperId]=@DailyNewsPaperId
							, [ImageGuid]=@ImageGuid
							, [PageTitle]=@PageTitle
							, [IsProcessed]=@IsProcessed
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where NewsPaperPageId=@NewsPaperPageId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@DailyNewsPaperId",entity.DailyNewsPaperId)
					, new SqlParameter("@ImageGuid",entity.ImageGuid ?? (object)DBNull.Value)
					, new SqlParameter("@PageTitle",entity.PageTitle)
					, new SqlParameter("@IsProcessed",entity.IsProcessed)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@NewsPaperPageId",entity.NewsPaperPageId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsPaperPage(entity.NewsPaperPageId);
		}

		public virtual bool DeleteNewsPaperPage(System.Int32 NewsPaperPageId)
		{

			string sql="delete from NewsPaperPage with (nolock) where NewsPaperPageId=@NewsPaperPageId";
			SqlParameter parameter=new SqlParameter("@NewsPaperPageId",NewsPaperPageId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsPaperPage))]
		public virtual NewsPaperPage DeleteNewsPaperPage(NewsPaperPage entity)
		{
			this.DeleteNewsPaperPage(entity.NewsPaperPageId);
			return entity;
		}


		public virtual NewsPaperPage NewsPaperPageFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsPaperPage entity=new NewsPaperPage();
			if (dr.Table.Columns.Contains("NewsPaperPageId"))
			{
			entity.NewsPaperPageId = (System.Int32)dr["NewsPaperPageId"];
			}
			if (dr.Table.Columns.Contains("DailyNewsPaperId"))
			{
			entity.DailyNewsPaperId = (System.Int32)dr["DailyNewsPaperId"];
			}
			if (dr.Table.Columns.Contains("ImageGuid"))
			{
			entity.ImageGuid = dr["ImageGuid"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["ImageGuid"];
			}
			if (dr.Table.Columns.Contains("PageTitle"))
			{
			entity.PageTitle = dr["PageTitle"].ToString();
			}
			if (dr.Table.Columns.Contains("IsProcessed"))
			{
			entity.IsProcessed = (System.Boolean)dr["IsProcessed"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
