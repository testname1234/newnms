﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsPaperPagesPartRepositoryBase : Repository, INewsPaperPagesPartRepositoryBase 
	{
        
        public NewsPaperPagesPartRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsPaperPagesPartId",new SearchColumn(){Name="NewsPaperPagesPartId",Title="NewsPaperPagesPartId",SelectClause="NewsPaperPagesPartId",WhereClause="AllRecords.NewsPaperPagesPartId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsPaperPagesPartId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsPaperPageId",new SearchColumn(){Name="NewsPaperPageId",Title="NewsPaperPageId",SelectClause="NewsPaperPageId",WhereClause="AllRecords.NewsPaperPageId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsPaperPageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PartNumber",new SearchColumn(){Name="PartNumber",Title="PartNumber",SelectClause="PartNumber",WhereClause="AllRecords.PartNumber",DataType="System.Int32?",IsForeignColumn=false,PropertyName="PartNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Bottom",new SearchColumn(){Name="Bottom",Title="Bottom",SelectClause="Bottom",WhereClause="AllRecords.Bottom",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Bottom",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Top",new SearchColumn(){Name="Top",Title="Top",SelectClause="Top",WhereClause="AllRecords.Top",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Top",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Left",new SearchColumn(){Name="Left",Title="Left",SelectClause="Left",WhereClause="AllRecords.Left",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Left",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Right",new SearchColumn(){Name="Right",Title="Right",SelectClause="Right",WhereClause="AllRecords.Right",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Right",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("isActive",new SearchColumn(){Name="isActive",Title="isActive",SelectClause="isActive",WhereClause="AllRecords.isActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsPaperPagesPartSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsPaperPagesPartBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsPaperPagesPartAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsPaperPagesPartSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsPaperPagesPart].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsPaperPagesPart].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsPaperPagesPart> GetNewsPaperPagesPartByNewsPaperPageId(System.Int32? NewsPaperPageId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPagesPartSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsPaperPagesPart with (nolock)  where NewsPaperPageId=@NewsPaperPageId  ";
			SqlParameter parameter=new SqlParameter("@NewsPaperPageId",NewsPaperPageId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPagesPart>(ds,NewsPaperPagesPartFromDataRow);
		}

		public virtual NewsPaperPagesPart GetNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPagesPartSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsPaperPagesPart with (nolock)  where NewsPaperPagesPartId=@NewsPaperPagesPartId ";
			SqlParameter parameter=new SqlParameter("@NewsPaperPagesPartId",NewsPaperPagesPartId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsPaperPagesPartFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsPaperPagesPart> GetNewsPaperPagesPartByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPagesPartSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from NewsPaperPagesPart with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPagesPart>(ds,NewsPaperPagesPartFromDataRow);
		}

		public virtual List<NewsPaperPagesPart> GetAllNewsPaperPagesPart(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsPaperPagesPartSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsPaperPagesPart with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPagesPart>(ds, NewsPaperPagesPartFromDataRow);
		}

		public virtual List<NewsPaperPagesPart> GetPagedNewsPaperPagesPart(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsPaperPagesPartCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsPaperPagesPartId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsPaperPagesPartId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsPaperPagesPartId] ";
            tempsql += " FROM [NewsPaperPagesPart] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsPaperPagesPartId"))
					tempsql += " , (AllRecords.[NewsPaperPagesPartId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsPaperPagesPartId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsPaperPagesPartSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsPaperPagesPart] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsPaperPagesPart].[NewsPaperPagesPartId] = PageIndex.[NewsPaperPagesPartId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsPaperPagesPart>(ds, NewsPaperPagesPartFromDataRow);
			}else{ return null;}
		}

		private int GetNewsPaperPagesPartCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsPaperPagesPart as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsPaperPagesPart as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsPaperPagesPart))]
		public virtual NewsPaperPagesPart InsertNewsPaperPagesPart(NewsPaperPagesPart entity)
		{

			NewsPaperPagesPart other=new NewsPaperPagesPart();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsPaperPagesPart ( [NewsPaperPageId]
				,[PartNumber]
				,[Bottom]
				,[Top]
				,[Left]
				,[Right]
				,[CreationDate]
				,[LastUpdateDate]
				,[isActive] )
				Values
				( @NewsPaperPageId
				, @PartNumber
				, @Bottom
				, @Top
				, @Left
				, @Right
				, @CreationDate
				, @LastUpdateDate
				, @isActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsPaperPageId",entity.NewsPaperPageId ?? (object)DBNull.Value)
					, new SqlParameter("@PartNumber",entity.PartNumber ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isActive",entity.IsActive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsPaperPagesPart(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsPaperPagesPart))]
		public virtual NewsPaperPagesPart UpdateNewsPaperPagesPart(NewsPaperPagesPart entity)
		{

			if (entity.IsTransient()) return entity;
			NewsPaperPagesPart other = GetNewsPaperPagesPart(entity.NewsPaperPagesPartId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsPaperPagesPart set  [NewsPaperPageId]=@NewsPaperPageId
							, [PartNumber]=@PartNumber
							, [Bottom]=@Bottom
							, [Top]=@Top
							, [Left]=@Left
							, [Right]=@Right
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isActive]=@isActive 
							 where NewsPaperPagesPartId=@NewsPaperPagesPartId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsPaperPageId",entity.NewsPaperPageId ?? (object)DBNull.Value)
					, new SqlParameter("@PartNumber",entity.PartNumber ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperPagesPartId",entity.NewsPaperPagesPartId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsPaperPagesPart(entity.NewsPaperPagesPartId);
		}

		public virtual bool DeleteNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId)
		{

			string sql="delete from NewsPaperPagesPart with (nolock) where NewsPaperPagesPartId=@NewsPaperPagesPartId";
			SqlParameter parameter=new SqlParameter("@NewsPaperPagesPartId",NewsPaperPagesPartId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsPaperPagesPart))]
		public virtual NewsPaperPagesPart DeleteNewsPaperPagesPart(NewsPaperPagesPart entity)
		{
			this.DeleteNewsPaperPagesPart(entity.NewsPaperPagesPartId);
			return entity;
		}


		public virtual NewsPaperPagesPart NewsPaperPagesPartFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsPaperPagesPart entity=new NewsPaperPagesPart();
			if (dr.Table.Columns.Contains("NewsPaperPagesPartId"))
			{
			entity.NewsPaperPagesPartId = (System.Int32)dr["NewsPaperPagesPartId"];
			}
			if (dr.Table.Columns.Contains("NewsPaperPageId"))
			{
			entity.NewsPaperPageId = dr["NewsPaperPageId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsPaperPageId"];
			}
			if (dr.Table.Columns.Contains("PartNumber"))
			{
			entity.PartNumber = dr["PartNumber"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["PartNumber"];
			}
			if (dr.Table.Columns.Contains("Bottom"))
			{
			entity.Bottom = dr["Bottom"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Bottom"];
			}
			if (dr.Table.Columns.Contains("Top"))
			{
			entity.Top = dr["Top"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Top"];
			}
			if (dr.Table.Columns.Contains("Left"))
			{
			entity.Left = dr["Left"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Left"];
			}
			if (dr.Table.Columns.Contains("Right"))
			{
			entity.Right = dr["Right"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Right"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("isActive"))
			{
			entity.IsActive = dr["isActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["isActive"];
			}
			return entity;
		}

	}
	
	
}
