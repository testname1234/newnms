﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class NewsPaperPagesPartsDetailRepository: NewsPaperPagesPartsDetailRepositoryBase, INewsPaperPagesPartsDetailRepository
	{
        public List<NewsPaperPagesPartsDetail> GetNewsPaperPagesPartsDetailByDailyNewsPaperDate(DateTime fromDate, DateTime toDate)
        {
            string sql = GetNewsPaperPagesPartsDetailSelectClause();
            sql += "from DailyNewsPaper d inner join NewsPaperPage n on d.DailyNewsPaperId=n.DailyNewsPaperId inner join NewsPaperPagesPart p on n.NewsPaperPageId=p.NewsPaperPageId inner join NewsPaperPagesPartsDetail on p.NewsPaperPagesPartId=NewsPaperPagesPartsDetail.NewsPaperPagesPartId where d.[date] between @fromDate and @toDate";
            SqlParameter parameter1 = new SqlParameter("@fromDate", fromDate);
            SqlParameter parameter2 = new SqlParameter("@toDate", toDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsPaperPagesPartsDetail>(ds, NewsPaperPagesPartsDetailFromDataRow);
        }
	}
	
	
}
