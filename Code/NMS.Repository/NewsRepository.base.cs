﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsRepositoryBase : Repository, INewsRepositoryBase 
	{
        
        public NewsRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Guid",new SearchColumn(){Name="Guid",Title="Guid",SelectClause="Guid",WhereClause="AllRecords.Guid",DataType="System.String",IsForeignColumn=false,PropertyName="Guid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TranslatedDescription",new SearchColumn(){Name="TranslatedDescription",Title="TranslatedDescription",SelectClause="TranslatedDescription",WhereClause="AllRecords.TranslatedDescription",DataType="System.String",IsForeignColumn=false,PropertyName="TranslatedDescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TranslatedTitle",new SearchColumn(){Name="TranslatedTitle",Title="TranslatedTitle",SelectClause="TranslatedTitle",WhereClause="AllRecords.TranslatedTitle",DataType="System.String",IsForeignColumn=false,PropertyName="TranslatedTitle",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Title",new SearchColumn(){Name="Title",Title="Title",SelectClause="Title",WhereClause="AllRecords.Title",DataType="System.String",IsForeignColumn=false,PropertyName="Title",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchGuid",new SearchColumn(){Name="BunchGuid",Title="BunchGuid",SelectClause="BunchGuid",WhereClause="AllRecords.BunchGuid",DataType="System.String",IsForeignColumn=false,PropertyName="BunchGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Description",new SearchColumn(){Name="Description",Title="Description",SelectClause="Description",WhereClause="AllRecords.Description",DataType="System.String",IsForeignColumn=false,PropertyName="Description",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Author",new SearchColumn(){Name="Author",Title="Author",SelectClause="Author",WhereClause="AllRecords.Author",DataType="System.String",IsForeignColumn=false,PropertyName="Author",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UpdateTime",new SearchColumn(){Name="UpdateTime",Title="UpdateTime",SelectClause="UpdateTime",WhereClause="AllRecords.UpdateTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="UpdateTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LinkNewsId",new SearchColumn(){Name="LinkNewsId",Title="LinkNewsId",SelectClause="LinkNewsId",WhereClause="AllRecords.LinkNewsId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LinkNewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentNewsId",new SearchColumn(){Name="ParentNewsId",Title="ParentNewsId",SelectClause="ParentNewsId",WhereClause="AllRecords.ParentNewsId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentNewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PreviousVersionId",new SearchColumn(){Name="PreviousVersionId",Title="PreviousVersionId",SelectClause="PreviousVersionId",WhereClause="AllRecords.PreviousVersionId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="PreviousVersionId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PublishTime",new SearchColumn(){Name="PublishTime",Title="PublishTime",SelectClause="PublishTime",WhereClause="AllRecords.PublishTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="PublishTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ThumbnailId",new SearchColumn(){Name="ThumbnailId",Title="ThumbnailId",SelectClause="ThumbnailId",WhereClause="AllRecords.ThumbnailId",DataType="System.String",IsForeignColumn=false,PropertyName="ThumbnailId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VersionNumber",new SearchColumn(){Name="VersionNumber",Title="VersionNumber",SelectClause="VersionNumber",WhereClause="AllRecords.VersionNumber",DataType="System.Int32?",IsForeignColumn=false,PropertyName="VersionNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsIndexed",new SearchColumn(){Name="IsIndexed",Title="IsIndexed",SelectClause="IsIndexed",WhereClause="AllRecords.IsIndexed",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsIndexed",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsTypeId",new SearchColumn(){Name="NewsTypeId",Title="NewsTypeId",SelectClause="NewsTypeId",WhereClause="AllRecords.NewsTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Source",new SearchColumn(){Name="Source",Title="Source",SelectClause="Source",WhereClause="AllRecords.Source",DataType="System.String",IsForeignColumn=false,PropertyName="Source",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceTypeId",new SearchColumn(){Name="SourceTypeId",Title="SourceTypeId",SelectClause="SourceTypeId",WhereClause="AllRecords.SourceTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SourceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceNewsUrl",new SearchColumn(){Name="SourceNewsUrl",Title="SourceNewsUrl",SelectClause="SourceNewsUrl",WhereClause="AllRecords.SourceNewsUrl",DataType="System.String",IsForeignColumn=false,PropertyName="SourceNewsUrl",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceFilterId",new SearchColumn(){Name="SourceFilterId",Title="SourceFilterId",SelectClause="SourceFilterId",WhereClause="AllRecords.SourceFilterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SourceFilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DescriptionText",new SearchColumn(){Name="DescriptionText",Title="DescriptionText",SelectClause="DescriptionText",WhereClause="AllRecords.DescriptionText",DataType="System.String",IsForeignColumn=false,PropertyName="DescriptionText",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsDirection",new SearchColumn(){Name="NewsDirection",Title="NewsDirection",SelectClause="NewsDirection",WhereClause="AllRecords.NewsDirection",DataType="System.String",IsForeignColumn=false,PropertyName="NewsDirection",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ShortDescription",new SearchColumn(){Name="ShortDescription",Title="ShortDescription",SelectClause="ShortDescription",WhereClause="AllRecords.ShortDescription",DataType="System.String",IsForeignColumn=false,PropertyName="ShortDescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Version",new SearchColumn(){Name="Version",Title="Version",SelectClause="Version",WhereClause="AllRecords.Version",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Version",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReporterId",new SearchColumn(){Name="ReporterId",Title="ReporterId",SelectClause="ReporterId",WhereClause="AllRecords.ReporterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ReporterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsVerified",new SearchColumn(){Name="IsVerified",Title="IsVerified",SelectClause="IsVerified",WhereClause="AllRecords.IsVerified",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsVerified",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsAired",new SearchColumn(){Name="IsAired",Title="IsAired",SelectClause="IsAired",WhereClause="AllRecords.IsAired",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsAired",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsSearchable",new SearchColumn(){Name="IsSearchable",Title="IsSearchable",SelectClause="IsSearchable",WhereClause="AllRecords.IsSearchable",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsSearchable",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("HasResourceEdit",new SearchColumn(){Name="HasResourceEdit",Title="HasResourceEdit",SelectClause="HasResourceEdit",WhereClause="AllRecords.HasResourceEdit",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="HasResourceEdit",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReferenceNewsId",new SearchColumn(){Name="ReferenceNewsId",Title="ReferenceNewsId",SelectClause="ReferenceNewsId",WhereClause="AllRecords.ReferenceNewsId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ReferenceNewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsCompleted",new SearchColumn(){Name="IsCompleted",Title="IsCompleted",SelectClause="IsCompleted",WhereClause="AllRecords.IsCompleted",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsCompleted",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("HasNoResource",new SearchColumn(){Name="HasNoResource",Title="HasNoResource",SelectClause="HasNoResource",WhereClause="AllRecords.HasNoResource",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="HasNoResource",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AddedToRundownCount",new SearchColumn(){Name="AddedToRundownCount",Title="AddedToRundownCount",SelectClause="AddedToRundownCount",WhereClause="AllRecords.AddedToRundownCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="AddedToRundownCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OnAirCount",new SearchColumn(){Name="OnAirCount",Title="OnAirCount",SelectClause="OnAirCount",WhereClause="AllRecords.OnAirCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="OnAirCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OtherChannelExecutionCount",new SearchColumn(){Name="OtherChannelExecutionCount",Title="OtherChannelExecutionCount",SelectClause="OtherChannelExecutionCount",WhereClause="AllRecords.OtherChannelExecutionCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="OtherChannelExecutionCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsTickerCount",new SearchColumn(){Name="NewsTickerCount",Title="NewsTickerCount",SelectClause="NewsTickerCount",WhereClause="AllRecords.NewsTickerCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsTickerCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReferenceNewsGuid",new SearchColumn(){Name="ReferenceNewsGuid",Title="ReferenceNewsGuid",SelectClause="ReferenceNewsGuid",WhereClause="AllRecords.ReferenceNewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ReferenceNewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentNewsGuid",new SearchColumn(){Name="ParentNewsGuid",Title="ParentNewsGuid",SelectClause="ParentNewsGuid",WhereClause="AllRecords.ParentNewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ParentNewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsArchival",new SearchColumn(){Name="IsArchival",Title="IsArchival",SelectClause="IsArchival",WhereClause="AllRecords.IsArchival",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsArchival",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Slug",new SearchColumn(){Name="Slug",Title="Slug",SelectClause="Slug",WhereClause="AllRecords.Slug",DataType="System.String",IsForeignColumn=false,PropertyName="Slug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsPaperdescription",new SearchColumn(){Name="NewsPaperdescription",Title="NewsPaperdescription",SelectClause="NewsPaperdescription",WhereClause="AllRecords.NewsPaperdescription",DataType="System.String",IsForeignColumn=false,PropertyName="NewsPaperdescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Suggestions",new SearchColumn(){Name="Suggestions",Title="Suggestions",SelectClause="Suggestions",WhereClause="AllRecords.Suggestions",DataType="System.String",IsForeignColumn=false,PropertyName="Suggestions",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MigrationStatus",new SearchColumn(){Name="MigrationStatus",Title="MigrationStatus",SelectClause="MigrationStatus",WhereClause="AllRecords.MigrationStatus",DataType="System.Int32?",IsForeignColumn=false,PropertyName="MigrationStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TranslatedSlug",new SearchColumn(){Name="TranslatedSlug",Title="TranslatedSlug",SelectClause="TranslatedSlug",WhereClause="AllRecords.TranslatedSlug",DataType="System.String",IsForeignColumn=false,PropertyName="TranslatedSlug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlugId",new SearchColumn(){Name="SlugId",Title="SlugId",SelectClause="SlugId",WhereClause="AllRecords.SlugId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlugId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[News].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[News].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<News> GetNewsByLocationId(System.Int32? LocationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [News] with (nolock)  where LocationId=@LocationId  ";
			SqlParameter parameter=new SqlParameter("@LocationId",LocationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<News>(ds,NewsFromDataRow);
		}

		public virtual List<News> GetNewsByNewsTypeId(System.Int32 NewsTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [News] with (nolock)  where NewsTypeId=@NewsTypeId  ";
			SqlParameter parameter=new SqlParameter("@NewsTypeId",NewsTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<News>(ds,NewsFromDataRow);
		}

		public virtual News GetNews(System.Int32 NewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [News] with (nolock)  where NewsId=@NewsId ";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<News> GetNewsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [News] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<News>(ds,NewsFromDataRow);
		}

		public virtual List<News> GetAllNews(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [News] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<News>(ds, NewsFromDataRow);
		}

		public virtual List<News> GetPagedNews(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsId] ";
            tempsql += " FROM [News] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsId"))
					tempsql += " , (AllRecords.[NewsId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [News] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [News].[NewsId] = PageIndex.[NewsId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<News>(ds, NewsFromDataRow);
			}else{ return null;}
		}

		private int GetNewsCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM News as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM News as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(News))]
		public virtual News InsertNews(News entity)
		{

			News other=new News();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into News ( [Guid]
				,[TranslatedDescription]
				,[TranslatedTitle]
				,[Title]
				,[BunchGuid]
				,[Description]
				,[LocationId]
				,[Author]
				,[UpdateTime]
				,[LanguageCode]
				,[LinkNewsId]
				,[ParentNewsId]
				,[PreviousVersionId]
				,[PublishTime]
				,[ThumbnailId]
				,[VersionNumber]
				,[IsIndexed]
				,[NewsTypeId]
				,[Source]
				,[SourceTypeId]
				,[SourceNewsUrl]
				,[SourceFilterId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[DescriptionText]
				,[NewsDirection]
				,[ShortDescription]
				,[Version]
				,[ReporterId]
				,[IsVerified]
				,[IsAired]
				,[IsSearchable]
				,[HasResourceEdit]
				,[ReferenceNewsId]
				,[IsCompleted]
				,[HasNoResource]
				,[AddedToRundownCount]
				,[OnAirCount]
				,[OtherChannelExecutionCount]
				,[NewsTickerCount]
				,[ReferenceNewsGuid]
				,[ParentNewsGuid]
				,[IsArchival]
				,[Slug]
				,[NewsPaperdescription]
				,[Suggestions]
				,[MigrationStatus]
				,[TranslatedSlug]
				,[SlugId] )
				Values
				( @Guid
				, @TranslatedDescription
				, @TranslatedTitle
				, @Title
				, @BunchGuid
				, @Description
				, @LocationId
				, @Author
				, @UpdateTime
				, @LanguageCode
				, @LinkNewsId
				, @ParentNewsId
				, @PreviousVersionId
				, @PublishTime
				, @ThumbnailId
				, @VersionNumber
				, @IsIndexed
				, @NewsTypeId
				, @Source
				, @SourceTypeId
				, @SourceNewsUrl
				, @SourceFilterId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @DescriptionText
				, @NewsDirection
				, @ShortDescription
				, @Version
				, @ReporterId
				, @IsVerified
				, @IsAired
				, @IsSearchable
				, @HasResourceEdit
				, @ReferenceNewsId
				, @IsCompleted
				, @HasNoResource
				, @AddedToRundownCount
				, @OnAirCount
				, @OtherChannelExecutionCount
				, @NewsTickerCount
				, @ReferenceNewsGuid
				, @ParentNewsGuid
				, @IsArchival
				, @Slug
				, @NewsPaperdescription
				, @Suggestions
				, @MigrationStatus
				, @TranslatedSlug
				, @SlugId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@Author",entity.Author ?? (object)DBNull.Value)
					, new SqlParameter("@UpdateTime",entity.UpdateTime ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@LinkNewsId",entity.LinkNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsId",entity.ParentNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@PreviousVersionId",entity.PreviousVersionId ?? (object)DBNull.Value)
					, new SqlParameter("@PublishTime",entity.PublishTime ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbnailId",entity.ThumbnailId ?? (object)DBNull.Value)
					, new SqlParameter("@VersionNumber",entity.VersionNumber ?? (object)DBNull.Value)
					, new SqlParameter("@IsIndexed",entity.IsIndexed ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTypeId",entity.NewsTypeId)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)
					, new SqlParameter("@NewsDirection",entity.NewsDirection ?? (object)DBNull.Value)
					, new SqlParameter("@ShortDescription",entity.ShortDescription ?? (object)DBNull.Value)
					, new SqlParameter("@Version",entity.Version ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsAired",entity.IsAired ?? (object)DBNull.Value)
					, new SqlParameter("@IsSearchable",entity.IsSearchable ?? (object)DBNull.Value)
					, new SqlParameter("@HasResourceEdit",entity.HasResourceEdit ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsId",entity.ReferenceNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@IsCompleted",entity.IsCompleted ?? (object)DBNull.Value)
					, new SqlParameter("@HasNoResource",entity.HasNoResource ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundownCount",entity.AddedToRundownCount ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirCount",entity.OnAirCount ?? (object)DBNull.Value)
					, new SqlParameter("@OtherChannelExecutionCount",entity.OtherChannelExecutionCount ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTickerCount",entity.NewsTickerCount ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsGuid",entity.ReferenceNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsGuid",entity.ParentNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@IsArchival",entity.IsArchival ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperdescription",entity.NewsPaperdescription ?? (object)DBNull.Value)
					, new SqlParameter("@Suggestions",entity.Suggestions ?? (object)DBNull.Value)
					, new SqlParameter("@MigrationStatus",entity.MigrationStatus ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedSlug",entity.TranslatedSlug ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNews(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(News))]
		public virtual News UpdateNews(News entity)
		{

			if (entity.IsTransient()) return entity;
			News other = GetNews(entity.NewsId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update News set  [Guid]=@Guid
							, [TranslatedDescription]=@TranslatedDescription
							, [TranslatedTitle]=@TranslatedTitle
							, [Title]=@Title
							, [BunchGuid]=@BunchGuid
							, [Description]=@Description
							, [LocationId]=@LocationId
							, [Author]=@Author
							, [UpdateTime]=@UpdateTime
							, [LanguageCode]=@LanguageCode
							, [LinkNewsId]=@LinkNewsId
							, [ParentNewsId]=@ParentNewsId
							, [PreviousVersionId]=@PreviousVersionId
							, [PublishTime]=@PublishTime
							, [ThumbnailId]=@ThumbnailId
							, [VersionNumber]=@VersionNumber
							, [IsIndexed]=@IsIndexed
							, [NewsTypeId]=@NewsTypeId
							, [Source]=@Source
							, [SourceTypeId]=@SourceTypeId
							, [SourceNewsUrl]=@SourceNewsUrl
							, [SourceFilterId]=@SourceFilterId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [DescriptionText]=@DescriptionText
							, [NewsDirection]=@NewsDirection
							, [ShortDescription]=@ShortDescription
							, [Version]=@Version
							, [ReporterId]=@ReporterId
							, [IsVerified]=@IsVerified
							, [IsAired]=@IsAired
							, [IsSearchable]=@IsSearchable
							, [HasResourceEdit]=@HasResourceEdit
							, [ReferenceNewsId]=@ReferenceNewsId
							, [IsCompleted]=@IsCompleted
							, [HasNoResource]=@HasNoResource
							, [AddedToRundownCount]=@AddedToRundownCount
							, [OnAirCount]=@OnAirCount
							, [OtherChannelExecutionCount]=@OtherChannelExecutionCount
							, [NewsTickerCount]=@NewsTickerCount
							, [ReferenceNewsGuid]=@ReferenceNewsGuid
							, [ParentNewsGuid]=@ParentNewsGuid
							, [IsArchival]=@IsArchival
							, [Slug]=@Slug
							, [NewsPaperdescription]=@NewsPaperdescription
							, [Suggestions]=@Suggestions
							, [MigrationStatus]=@MigrationStatus
							, [TranslatedSlug]=@TranslatedSlug
							, [SlugId]=@SlugId 
							 where NewsId=@NewsId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@Author",entity.Author ?? (object)DBNull.Value)
					, new SqlParameter("@UpdateTime",entity.UpdateTime ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@LinkNewsId",entity.LinkNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsId",entity.ParentNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@PreviousVersionId",entity.PreviousVersionId ?? (object)DBNull.Value)
					, new SqlParameter("@PublishTime",entity.PublishTime ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbnailId",entity.ThumbnailId ?? (object)DBNull.Value)
					, new SqlParameter("@VersionNumber",entity.VersionNumber ?? (object)DBNull.Value)
					, new SqlParameter("@IsIndexed",entity.IsIndexed ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTypeId",entity.NewsTypeId)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)
					, new SqlParameter("@NewsDirection",entity.NewsDirection ?? (object)DBNull.Value)
					, new SqlParameter("@ShortDescription",entity.ShortDescription ?? (object)DBNull.Value)
					, new SqlParameter("@Version",entity.Version ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsAired",entity.IsAired ?? (object)DBNull.Value)
					, new SqlParameter("@IsSearchable",entity.IsSearchable ?? (object)DBNull.Value)
					, new SqlParameter("@HasResourceEdit",entity.HasResourceEdit ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsId",entity.ReferenceNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@IsCompleted",entity.IsCompleted ?? (object)DBNull.Value)
					, new SqlParameter("@HasNoResource",entity.HasNoResource ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundownCount",entity.AddedToRundownCount ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirCount",entity.OnAirCount ?? (object)DBNull.Value)
					, new SqlParameter("@OtherChannelExecutionCount",entity.OtherChannelExecutionCount ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTickerCount",entity.NewsTickerCount ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsGuid",entity.ReferenceNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsGuid",entity.ParentNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@IsArchival",entity.IsArchival ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperdescription",entity.NewsPaperdescription ?? (object)DBNull.Value)
					, new SqlParameter("@Suggestions",entity.Suggestions ?? (object)DBNull.Value)
					, new SqlParameter("@MigrationStatus",entity.MigrationStatus ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedSlug",entity.TranslatedSlug ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNews(entity.NewsId);
		}

		public virtual bool DeleteNews(System.Int32 NewsId)
		{

			string sql="delete from News where NewsId=@NewsId";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(News))]
		public virtual News DeleteNews(News entity)
		{
			this.DeleteNews(entity.NewsId);
			return entity;
		}


		public virtual News NewsFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			News entity=new News();
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = (System.Int32)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("Guid"))
			{
			entity.Guid = dr["Guid"].ToString();
			}
			if (dr.Table.Columns.Contains("TranslatedDescription"))
			{
			entity.TranslatedDescription = dr["TranslatedDescription"].ToString();
			}
			if (dr.Table.Columns.Contains("TranslatedTitle"))
			{
			entity.TranslatedTitle = dr["TranslatedTitle"].ToString();
			}
			if (dr.Table.Columns.Contains("Title"))
			{
			entity.Title = dr["Title"].ToString();
			}
			if (dr.Table.Columns.Contains("BunchGuid"))
			{
			entity.BunchGuid = dr["BunchGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("Description"))
			{
			entity.Description = dr["Description"].ToString();
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("Author"))
			{
			entity.Author = dr["Author"].ToString();
			}
			if (dr.Table.Columns.Contains("UpdateTime"))
			{
			entity.UpdateTime = dr["UpdateTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["UpdateTime"];
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("LinkNewsId"))
			{
			entity.LinkNewsId = dr["LinkNewsId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LinkNewsId"];
			}
			if (dr.Table.Columns.Contains("ParentNewsId"))
			{
			entity.ParentNewsId = dr["ParentNewsId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentNewsId"];
			}
			if (dr.Table.Columns.Contains("PreviousVersionId"))
			{
			entity.PreviousVersionId = dr["PreviousVersionId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["PreviousVersionId"];
			}
			if (dr.Table.Columns.Contains("PublishTime"))
			{
			entity.PublishTime = dr["PublishTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["PublishTime"];
			}
			if (dr.Table.Columns.Contains("ThumbnailId"))
			{
			entity.ThumbnailId = dr["ThumbnailId"].ToString();
			}
			if (dr.Table.Columns.Contains("VersionNumber"))
			{
			entity.VersionNumber = dr["VersionNumber"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["VersionNumber"];
			}
			if (dr.Table.Columns.Contains("IsIndexed"))
			{
			entity.IsIndexed = dr["IsIndexed"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsIndexed"];
			}
			if (dr.Table.Columns.Contains("NewsTypeId"))
			{
			entity.NewsTypeId = (System.Int32)dr["NewsTypeId"];
			}
			if (dr.Table.Columns.Contains("Source"))
			{
			entity.Source = dr["Source"].ToString();
			}
			if (dr.Table.Columns.Contains("SourceTypeId"))
			{
			entity.SourceTypeId = dr["SourceTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SourceTypeId"];
			}
			if (dr.Table.Columns.Contains("SourceNewsUrl"))
			{
			entity.SourceNewsUrl = dr["SourceNewsUrl"].ToString();
			}
			if (dr.Table.Columns.Contains("SourceFilterId"))
			{
			entity.SourceFilterId = dr["SourceFilterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SourceFilterId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("DescriptionText"))
			{
			entity.DescriptionText = dr["DescriptionText"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsDirection"))
			{
			entity.NewsDirection = dr["NewsDirection"].ToString();
			}
			if (dr.Table.Columns.Contains("ShortDescription"))
			{
			entity.ShortDescription = dr["ShortDescription"].ToString();
			}
			if (dr.Table.Columns.Contains("Version"))
			{
			entity.Version = dr["Version"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Version"];
			}
			if (dr.Table.Columns.Contains("ReporterId"))
			{
			entity.ReporterId = dr["ReporterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ReporterId"];
			}
			if (dr.Table.Columns.Contains("IsVerified"))
			{
			entity.IsVerified = dr["IsVerified"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsVerified"];
			}
			if (dr.Table.Columns.Contains("IsAired"))
			{
			entity.IsAired = dr["IsAired"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsAired"];
			}
			if (dr.Table.Columns.Contains("IsSearchable"))
			{
			entity.IsSearchable = dr["IsSearchable"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsSearchable"];
			}
			if (dr.Table.Columns.Contains("HasResourceEdit"))
			{
			entity.HasResourceEdit = dr["HasResourceEdit"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["HasResourceEdit"];
			}
			if (dr.Table.Columns.Contains("ReferenceNewsId"))
			{
			entity.ReferenceNewsId = dr["ReferenceNewsId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ReferenceNewsId"];
			}
			if (dr.Table.Columns.Contains("IsCompleted"))
			{
			entity.IsCompleted = dr["IsCompleted"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsCompleted"];
			}
			if (dr.Table.Columns.Contains("HasNoResource"))
			{
			entity.HasNoResource = dr["HasNoResource"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["HasNoResource"];
			}
			if (dr.Table.Columns.Contains("AddedToRundownCount"))
			{
			entity.AddedToRundownCount = dr["AddedToRundownCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["AddedToRundownCount"];
			}
			if (dr.Table.Columns.Contains("OnAirCount"))
			{
			entity.OnAirCount = dr["OnAirCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["OnAirCount"];
			}
			if (dr.Table.Columns.Contains("OtherChannelExecutionCount"))
			{
			entity.OtherChannelExecutionCount = dr["OtherChannelExecutionCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["OtherChannelExecutionCount"];
			}
			if (dr.Table.Columns.Contains("NewsTickerCount"))
			{
			entity.NewsTickerCount = dr["NewsTickerCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsTickerCount"];
			}
			if (dr.Table.Columns.Contains("ReferenceNewsGuid"))
			{
			entity.ReferenceNewsGuid = dr["ReferenceNewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("ParentNewsGuid"))
			{
			entity.ParentNewsGuid = dr["ParentNewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("IsArchival"))
			{
			entity.IsArchival = dr["IsArchival"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsArchival"];
			}
			if (dr.Table.Columns.Contains("Slug"))
			{
			entity.Slug = dr["Slug"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsPaperdescription"))
			{
			entity.NewsPaperdescription = dr["NewsPaperdescription"].ToString();
			}
			if (dr.Table.Columns.Contains("Suggestions"))
			{
			entity.Suggestions = dr["Suggestions"].ToString();
			}
			if (dr.Table.Columns.Contains("MigrationStatus"))
			{
			entity.MigrationStatus = dr["MigrationStatus"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["MigrationStatus"];
			}
			if (dr.Table.Columns.Contains("TranslatedSlug"))
			{
			entity.TranslatedSlug = dr["TranslatedSlug"].ToString();
			}
			if (dr.Table.Columns.Contains("SlugId"))
			{
			entity.SlugId = dr["SlugId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlugId"];
			}
			return entity;
		}

	}
	
	
}
