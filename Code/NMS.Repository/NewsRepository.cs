﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;
using NMS.Core.Models;
using NMS.Core.Extensions;
using NMS.Core.Helper;
using System.Configuration;

namespace NMS.Repository
{

    public partial class NewsRepository : NewsRepositoryBase, INewsRepository
    {
        public NewsRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public NewsRepository()
        {
        }
        
        public virtual void UpdateCompleteNews(News entity)
        {
            string sql = @"Update News set  [Guid]=@Guid
							, [TranslatedDescription]=@TranslatedDescription
							, [TranslatedTitle]=@TranslatedTitle
							, [Title]=@Title
							, [BunchGuid]=@BunchGuid
							, [Description]=@Description
							, [LocationId]=@LocationId
							, [Author]=@Author
							, [UpdateTime]=@UpdateTime
							, [LanguageCode]=@LanguageCode
							, [LinkNewsId]=@LinkNewsId
							, [ParentNewsId]=@ParentNewsId
							, [PreviousVersionId]=@PreviousVersionId
							, [PublishTime]=@PublishTime
							, [ThumbnailId]=@ThumbnailId
							, [VersionNumber]=@VersionNumber
							, [IsIndexed]=@IsIndexed
							, [NewsTypeId]=@NewsTypeId
							, [Source]=@Source
							, [SourceTypeId]=@SourceTypeId
							, [SourceNewsUrl]=@SourceNewsUrl
							, [SourceFilterId]=@SourceFilterId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [DescriptionText]=@DescriptionText
							, [NewsDirection]=@NewsDirection
							, [ShortDescription]=@ShortDescription
							, [Version]=@Version
							, [ReporterId]=@ReporterId
							, [IsVerified]=@IsVerified
							, [IsAired]=@IsAired
							, [IsSearchable]=@IsSearchable
							, [HasResourceEdit]=@HasResourceEdit
							, [ReferenceNewsId]=@ReferenceNewsId
							, [IsCompleted]=@IsCompleted
							, [HasNoResource]=@HasNoResource
							, [AddedToRundownCount]=@AddedToRundownCount
							, [OnAirCount]=@OnAirCount
							, [OtherChannelExecutionCount]=@OtherChannelExecutionCount
							, [NewsTickerCount]=@NewsTickerCount
							, [ReferenceNewsGuid]=@ReferenceNewsGuid
							, [ParentNewsGuid]=@ParentNewsGuid
							, [IsArchival]=@IsArchival
							, [Slug]=@Slug
							, [NewsPaperdescription]=@NewsPaperdescription
							, [Suggestions]=@Suggestions
							, [MigrationStatus]=@MigrationStatus
							, [TranslatedSlug]=@TranslatedSlug
							, [SlugId]=@SlugId 
							 where NewsId=@NewsId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@Author",entity.Author ?? (object)DBNull.Value)
					, new SqlParameter("@UpdateTime",entity.UpdateTime ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@LinkNewsId",entity.LinkNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsId",entity.ParentNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@PreviousVersionId",entity.PreviousVersionId ?? (object)DBNull.Value)
					, new SqlParameter("@PublishTime",entity.PublishTime ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbnailId",entity.ThumbnailId ?? (object)DBNull.Value)
					, new SqlParameter("@VersionNumber",entity.VersionNumber ?? (object)DBNull.Value)
					, new SqlParameter("@IsIndexed",entity.IsIndexed ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTypeId",entity.NewsTypeId)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)
					, new SqlParameter("@NewsDirection",entity.NewsDirection ?? (object)DBNull.Value)
					, new SqlParameter("@ShortDescription",entity.ShortDescription ?? (object)DBNull.Value)
					, new SqlParameter("@Version",entity.Version ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsAired",entity.IsAired ?? (object)DBNull.Value)
					, new SqlParameter("@IsSearchable",entity.IsSearchable ?? (object)DBNull.Value)
					, new SqlParameter("@HasResourceEdit",entity.HasResourceEdit ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsId",entity.ReferenceNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@IsCompleted",entity.IsCompleted ?? (object)DBNull.Value)
					, new SqlParameter("@HasNoResource",entity.HasNoResource ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundownCount",entity.AddedToRundownCount ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirCount",entity.OnAirCount ?? (object)DBNull.Value)
					, new SqlParameter("@OtherChannelExecutionCount",entity.OtherChannelExecutionCount ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTickerCount",entity.NewsTickerCount ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsGuid",entity.ReferenceNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsGuid",entity.ParentNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@IsArchival",entity.IsArchival ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperdescription",entity.NewsPaperdescription ?? (object)DBNull.Value)
					, new SqlParameter("@Suggestions",entity.Suggestions ?? (object)DBNull.Value)
					, new SqlParameter("@MigrationStatus",entity.MigrationStatus ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedSlug",entity.TranslatedSlug ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }

        public virtual SqlMigration GetAllFromArchivalByNews(System.String NewsGuid, string SelectClause = null)
        {
            SqlMigration sqlmi = new SqlMigration();
            string sql = @"
            select * from [NewsFilter] with (nolock)  where NewsGuid=@NewsGuid  
            select * from [NewsTag] with (nolock)  where NewsGuid=@NewsGuid              
            select * from [NewsResourceEdit] with (nolock)  where NewsGuid=@NewsGuid  
            select * from [NewsStatistics] with (nolock)  where NewsGuid=@NewsGuid
            select r.Guid,r.ResourceTypeId,Caption,Category,Location,Duration,nr.CreationDate,nr.LastUpdateDate from [NewsResource] nr (nolock)  
                inner join Resource r on r.Guid = nr.ResourceGuid
                where NewsGuid=@NewsGuid
            select l.LocationId,l.Location,l.ParentId,l.IsApproved,nl.CreationDate,nl.LastUpdateDate from [NewsLocation] nl with (nolock)  
                inner join Location l on nl.LocationId = l.LocationId
                where NewsGuid=@NewsGuid
            select c.CategoryId,c.Category,c.ParentId,c.IsApproved,cat.CreationDate,cat.LastUpdateDate from [NewsCategory] cat with (nolock)  
                inner join Category c on cat.CategoryId = c.CategoryId
                where NewsGuid=@NewsGuid
            select * from [NewsResourceEdit] with (nolock)  where NewsGuid=@NewsGuid
            select rei.* from [ResourceEditclipinfo]  rei (nolock)
                inner join NewsResourceEdit nre (nolock) on nre.NewsResourceEditId = rei.NewsResourceEditId
                where nre.NewsGuid = @NewsGuid
            
";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count < 1 || ds.Tables[0].Rows.Count == 0) return null;
            else
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    sqlmi.NewsFilters = ds.Tables[0].ToList<NewsFilter>();
                }
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    sqlmi.NewsTags = ds.Tables[1].ToList<NewsTag>();
                }
                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    sqlmi.newsResourceEdits = ds.Tables[2].ToList<NewsResourceEdit>();
                }
                if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                {
                    sqlmi.NewsStatistics = ds.Tables[3].ToList<NewsStatistics>();
                }
                if (ds.Tables[4] != null && ds.Tables[4].Rows.Count > 0)
                {
                    sqlmi.NewsResources = ds.Tables[4].ToList<NewsResource>();
                }
                if (ds.Tables[5] != null && ds.Tables[5].Rows.Count > 0)
                {
                    sqlmi.NewsLocations = ds.Tables[5].ToList<NewsLocation>();
                }
                if (ds.Tables[6] != null && ds.Tables[6].Rows.Count > 0)
                {
                    sqlmi.NewsCategories = ds.Tables[6].ToList<NewsCategory>();
                }
                if (ds.Tables[7] != null && ds.Tables[7].Rows.Count > 0)
                {
                    sqlmi.newsResourceEdits = ds.Tables[7].ToList<NewsResourceEdit>();
                }
                if (ds.Tables[8] != null && ds.Tables[8].Rows.Count > 0)
                {
                    sqlmi.ResourceEditclipinfos = ds.Tables[8].ToList<ResourceEditclipinfo>();
                }
            }
            return sqlmi;
        }

        public virtual SqlMigration GetAllFromArchivalByNewsBunch(System.String BunchGuid, string SelectClause = null)
        {
            SqlMigration sqlmi = new SqlMigration();
            //select Distinct BunchGuid,FilterId,CreationDate,LastUpdateDate from [BunchFilterNews] with (nolock)  where BunchGuid=@BunchGuid  
            //select * from [BunchFilterNews] with (nolock)  where BunchGuid=@BunchGuid  
            //select * from [BunchNews] with (nolock)  where BunchGuid=@BunchGuid";
            string sql = @"select * from [Bunch] with (nolock)  where Guid=@BunchGuid";
            SqlParameter parameter = new SqlParameter("@BunchGuid", BunchGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count < 1 || ds.Tables[0].Rows.Count == 0) return null;
            else
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    sqlmi.SqlBunch = ds.Tables[0].ToList<Bunch>()[0];
                }
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    sqlmi.SqlBunchFilters = ds.Tables[1].ToList<BunchFilterNews>();
                }
                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    sqlmi.SqlBunchFilterNews = ds.Tables[2].ToList<BunchFilterNews>();
                }
                if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                {
                    sqlmi.SqlBunchNews = ds.Tables[3].ToList<BunchNews>();
                }
            }
            return sqlmi;
        }

        public virtual List<News> GetByBunchIds(string bunches, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from News with (nolock) where BunchGuid in (" + bunches + ")";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual List<News> GetCustomByBunchIds(string bunches, string SelectClause = null)
        {
            string sql = "select BunchGuid,IsArchival from News with (nolock) where BunchGuid in (" + bunches + ")";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual List<News> GetBunchesByNewsGuids(string NewsGuids, string SelectClause = null)
        {
            string sql = "select Distinct BunchGuid from News with (nolock) where [Guid] in (" + NewsGuids + ")";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual List<News> GetByNewsGuids(string NewsGuids, string SelectClause = null)
        {
            string sql = "select * from News with (nolock) where [Guid] in (" + NewsGuids + ")";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual List<News> GetNonIndexedNews()
        {
            string sql = GetNewsSelectClause();
            sql += "from News with (nolock)  where ISNULL(isindexed,0)=0 and ISNULL(BunchGuid,'') <>''";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual List<News> GetNonIndexedNewsOptimized()
        {
            string sql = "select NewsId,Guid,BunchGuid,ThumbnailId from News with (nolock) where ISNULL(isindexed,0)=0 and ISNULL(BunchGuid,'') <>''";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual void UpdateNewsForMigration(News entity)
        {
            string sql = @"Update News set  
							 [BunchGuid]=@BunchGuid							
                            , [ThumbnailId]=@ThumbnailId
                            , [IsVerified]=@IsVerified						
                            , [IsCompleted]=@IsCompleted		
	                        , [IsArchival]=@IsArchival					
                            , [AddedToRundownCount]=@AddedToRundownCount
							, [OnAirCount]=@OnAirCount                            
							, [OtherChannelExecutionCount]=@OtherChannelExecutionCount
							, [NewsTickerCount]=@NewsTickerCount
							, [ReferenceNewsGuid]=@ReferenceNewsGuid
							, [ParentNewsGuid]=@ParentNewsGuid 
							 where Guid=@Guid";
            SqlParameter[] parameterArray = new SqlParameter[]{					 
                new SqlParameter("@Guid",entity.BunchGuid ?? (object)DBNull.Value)
					 ,new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbnailId",entity.ThumbnailId ?? (object)DBNull.Value)					
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)					
					, new SqlParameter("@IsCompleted",entity.IsCompleted ?? (object)DBNull.Value)
					, new SqlParameter("@IsArchival",entity.IsArchival ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundownCount",entity.AddedToRundownCount ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirCount",entity.OnAirCount ?? (object)DBNull.Value)
					, new SqlParameter("@OtherChannelExecutionCount",entity.OtherChannelExecutionCount ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTickerCount",entity.NewsTickerCount ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsGuid",entity.ReferenceNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsGuid",entity.ParentNewsGuid ?? (object)DBNull.Value)
					};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public void UpdateNewsByBunchId(int NewsId, string BunchGuid)
        {
            string sql = @"Update News set BunchGuid=@BunchGuid                     
                                           where [NewsId]=@NewsId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                                  new SqlParameter("@NewsId",NewsId)                                  
                                  , new SqlParameter("@BunchGuid",BunchGuid)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public void UpdateNewsMigrationStatus(string NewsIds)
        {
            string sql = @"Update News set MigrationStatus=2 where [NewsId] in(" + NewsIds + ")";
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, null);
        }


        public virtual List<News> GetbyIsCompleted()
        {
            string sql = GetNewsSelectClause();
            sql += "from News with (nolock)  where ISNULL(iscompleted,0)=0";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual string GetBunchByNewsId(System.Int32 NewsId)
        {
            string sql = "select top 1 BunchGuid ";
            sql += "from semanticsimilaritytable( News,*,@NewsID) INNER JOIN News ON NewsId = matched_document_key and score >= .3 where IsIndexed = 1  ORDER BY score DESC";
            SqlParameter parameter = new SqlParameter("@NewsID", NewsId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0)
                return "";
            return ds.Tables[0].Rows[0]["BunchGuid"].ToString();
        }

        public virtual List<string> GetRelatedNews(System.Int32 NewsId)
        {
            List<string> lst = new List<string>();
            string sql = "select BunchGuid ";
            sql += @"from semanticsimilaritytable( News,*,@NewsID) 
            INNER JOIN News ON NewsId = matched_document_key and score >= .3 where IsIndexed = 1  ORDER BY score DESC";
            SqlParameter parameter = new SqlParameter("@NewsID", NewsId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                return null;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lst.Add(dr[0].ToString());
            }
            return lst;
        }

        public virtual List<string> GetRelatedNewsV2(System.Int32 NewsId, string BunchGuid)
        {
            List<string> lst = new List<string>();
            string sql = "GetRelatedBunch";
            SqlParameter parameter1 = new SqlParameter("@NewsID", NewsId);
            SqlParameter parameter2 = new SqlParameter("@BunchGuid", BunchGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.StoredProcedure, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                return null;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lst.Add(dr[0].ToString());
            }
            return lst;
        }

        public virtual List<string> GetRelatedNewsV2Load(string NewsGuid, string BunchGuid)
        {
            List<string> lst = new List<string>();
            string sql = "GetrelatedbunchOnLoad";
            SqlParameter parameter1 = new SqlParameter("@NewsGuid", NewsGuid);
            SqlParameter parameter2 = new SqlParameter("@BunchGuid", BunchGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.StoredProcedure, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                return null;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lst.Add(dr[0].ToString());
            }
            return lst;
        }


        public virtual void InsertNewsNoReturn(News entity)
        {
            string sql = @"Insert into News ( [Guid]
				,[TranslatedDescription]
				,[TranslatedTitle]
				,[Title]
				,[BunchGuid]
				,[Description]
				,[LocationId]
				,[Author]
				,[UpdateTime]
				,[LanguageCode]
				,[LinkNewsId]
				,[ParentNewsId]
				,[PreviousVersionId]
				,[PublishTime]
				,[ThumbnailId]
				,[VersionNumber]
				,[IsIndexed]
				,[NewsTypeId]
				,[Source]
				,[SourceTypeId]
				,[SourceNewsUrl]
				,[SourceFilterId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[DescriptionText]
				,[NewsDirection]
				,[ShortDescription]
				,[Version]
				,[ReporterId]
				,[IsVerified]
				,[IsAired]
				,[IsSearchable]
				,[HasResourceEdit]
				,[ReferenceNewsId]
				,[IsCompleted]
				,[HasNoResource]
				,[IsArchival]
				,[AddedToRundownCount]
				,[OnAirCount]
				,[OtherChannelExecutionCount]
				,[NewsTickerCount]
				,[ReferenceNewsGuid]
				,[ParentNewsGuid]
				,[Slug]
				,[NewsPaperdescription]
				,[Suggestions]
				,[MigrationStatus]
				,[TranslatedSlug] )
				Values
				( @Guid
				, @TranslatedDescription
				, @TranslatedTitle
				, @Title
				, @BunchGuid
				, @Description
				, @LocationId
				, @Author
				, @UpdateTime
				, @LanguageCode
				, @LinkNewsId
				, @ParentNewsId
				, @PreviousVersionId
				, @PublishTime
				, @ThumbnailId
				, @VersionNumber
				, @IsIndexed
				, @NewsTypeId
				, @Source
				, @SourceTypeId
				, @SourceNewsUrl
				, @SourceFilterId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @DescriptionText
				, @NewsDirection
				, @ShortDescription
				, @Version
				, @ReporterId
				, @IsVerified
				, @IsAired
				, @IsSearchable
				, @HasResourceEdit
				, @ReferenceNewsId
				, @IsCompleted
				, @HasNoResource
				, @IsArchival
				, @AddedToRundownCount
				, @OnAirCount
				, @OtherChannelExecutionCount
				, @NewsTickerCount
				, @ReferenceNewsGuid
				, @ParentNewsGuid
				, @Slug
				, @NewsPaperdescription
				, @Suggestions
				, @MigrationStatus
				, @TranslatedSlug );
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@Author",entity.Author ?? (object)DBNull.Value)
					, new SqlParameter("@UpdateTime",entity.UpdateTime ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@LinkNewsId",entity.LinkNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsId",entity.ParentNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@PreviousVersionId",entity.PreviousVersionId ?? (object)DBNull.Value)
					, new SqlParameter("@PublishTime",entity.PublishTime ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbnailId",entity.ThumbnailId ?? (object)DBNull.Value)
					, new SqlParameter("@VersionNumber",entity.VersionNumber ?? (object)DBNull.Value)
					, new SqlParameter("@IsIndexed",entity.IsIndexed ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTypeId",entity.NewsTypeId)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)
					, new SqlParameter("@NewsDirection",entity.NewsDirection ?? (object)DBNull.Value)
					, new SqlParameter("@ShortDescription",entity.ShortDescription ?? (object)DBNull.Value)
					, new SqlParameter("@Version",entity.Version ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsAired",entity.IsAired ?? (object)DBNull.Value)
					, new SqlParameter("@IsSearchable",entity.IsSearchable ?? (object)DBNull.Value)
					, new SqlParameter("@HasResourceEdit",entity.HasResourceEdit ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsId",entity.ReferenceNewsId ?? (object)DBNull.Value)
					, new SqlParameter("@IsCompleted",entity.IsCompleted ?? (object)DBNull.Value)
					, new SqlParameter("@HasNoResource",entity.HasNoResource ?? (object)DBNull.Value)
					, new SqlParameter("@IsArchival",entity.IsArchival ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundownCount",entity.AddedToRundownCount ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirCount",entity.OnAirCount ?? (object)DBNull.Value)
					, new SqlParameter("@OtherChannelExecutionCount",entity.OtherChannelExecutionCount ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTickerCount",entity.NewsTickerCount ?? (object)DBNull.Value)
					, new SqlParameter("@ReferenceNewsGuid",entity.ReferenceNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ParentNewsGuid",entity.ParentNewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperdescription",entity.NewsPaperdescription ?? (object)DBNull.Value)
					, new SqlParameter("@Suggestions",entity.Suggestions ?? (object)DBNull.Value)
					, new SqlParameter("@MigrationStatus",entity.MigrationStatus ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedSlug",entity.TranslatedSlug ?? (object)DBNull.Value)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
        }

        public virtual bool CheckNewsMatchedStatus(System.Int32 SourceNewsId, int DestNewsId)
        {
            string sql = "select BunchGuid ";
            sql += @"from semanticsimilaritytable( News,*,@NewsID) 
            INNER JOIN News ON NewsId = matched_document_key 
            where IsIndexed = 1 and NewsId = @DestNewsId ORDER BY score DESC";
            SqlParameter parameter1 = new SqlParameter("@SourceNewsId", SourceNewsId);
            SqlParameter parameter2 = new SqlParameter("@DestNewsId", DestNewsId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                return false;
            return true;
        }


        public virtual News GetByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [News] with (nolock)  where Guid=@Guid  ";
            SqlParameter parameter = new SqlParameter("@Guid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return NewsFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual List<News> GetByBunchGuid(System.String BunchGuid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [News] with (nolock)  where BunchGuid=@BunchGuid  ";
            SqlParameter parameter = new SqlParameter("@BunchGuid", BunchGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual List<News> GetByNewsCountBunchGuid(System.String BunchGuid, string SelectClause = null)
        {

            string sql = "select [guid] from [News] with (nolock)  where BunchGuid=@BunchGuid";
            SqlParameter parameter = new SqlParameter("@BunchGuid", BunchGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual bool DeletebyNewsId(System.Int32 NewsId)
        {
            string sql = "delete from BunchNews where NewsId=@NewsId";
            sql += " delete from BunchFilterNews where NewsId=@NewsId";
            sql += " delete from NewsFilter where NewsId=@NewsId";
            sql += " delete from NewsResourceEdit where NewsId=@NewsId";
            sql += " delete from NewsResource where NewsId=@NewsId";
            sql += " delete from NewsCategory where NewsId=@NewsId";
            sql += " delete from NewsLocation where NewsId=@NewsId";
            sql += " delete from NewsTag where NewsId=@NewsId";
            sql += " delete from News where NewsId=@NewsId";
            SqlParameter parameter = new SqlParameter("@NewsId", NewsId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual News GetByNewsId(System.Int32 NewsId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [News] with (nolock)  where NewsId=@NewsId  ";
            SqlParameter parameter = new SqlParameter("@NewsId", NewsId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return NewsFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual News GetByNewsGuidAndIsComplete(System.String NewsGuid, bool isComplete, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [News] with (nolock)  where Guid=@Guid and isCompleted=@isComplete  ";
            SqlParameter[] parameter = new SqlParameter[]{
               new SqlParameter("@Guid", NewsGuid),
               new SqlParameter("@isComplete",isComplete)  };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameter);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return NewsFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual bool CheckIfNewsExistsSQL(System.String Title, string Source, string SelectClause = null)
        {
            string sql = "select 1 from [News] with (nolock) where Title=@Title and Source = @Source";
            SqlParameter[] parameter = new SqlParameter[]{
               new SqlParameter("@Title", Title),               
               new SqlParameter("@Source",Source) 
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameter);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return false;
            return true;
        }


        public virtual DateTime GetLastNewsDate()
        {
            string sql = "select top 1 CreationDate ";
            sql += "from news ORDER BY NewsId DESC";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0)
                return DateTime.UtcNow;
            return Convert.ToDateTime(ds.Tables[0].Rows[0]["CreationDate"].ToString());
        }

        public virtual List<News> GetDisitnctNewsSource()
        {
            string sql = "select distinct Source,max(creationdate) creationdate from News with (nolock) where isnull(SourceNewsUrl,'') <> '' group by source";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }

        public virtual List<News> GetFromDate(DateTime newsDate)
        {
            string sql = @"select top 1000 isnull(TranslatedTitle,'') as TranslatedTitle,Title,isnull(TranslatedDescription,'') as TranslatedDescription,Description,IsArchival,Guid,BunchGuid,LastUpdateDate,PublishTime,
                    stuff((Select ',' +convert(varchar,rm.FilterId) from NewsFilter rm (nolock) where News.Guid = rm.NewsGuid for xml path('')), 1, 1, '') as FilterId 
                    from News with (nolock) where LastUpdateDate >@LastUpdateDate and isnull(IsArchival,0) = 1 order by LastUpdateDate";
            SqlParameter[] parameter = new SqlParameter[]{
               new SqlParameter("@LastUpdateDate", newsDate)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameter);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRowCustom);
        }

        public virtual List<News> GetFromDateWithStatus()
        {
            string sql = @"select TranslatedSlug,Slug,NewsId,isnull(TranslatedTitle,'') as TranslatedTitle,Title,isnull(TranslatedDescription,'') as TranslatedDescription,Description,IsArchival,Guid,BunchGuid,LastUpdateDate,PublishTime,
                isnull(stuff((Select ',' +convert(varchar,rm.FilterId) from NewsFilter rm (nolock) where News.Guid = rm.NewsGuid for xml path('')), 1, 1, ''),'') as FilterId, 
                isnull(stuff((Select ' ' +convert(varchar,nt.tag) from newstag nt (nolock) where News.Guid = nt.NewsGuid for xml path('')), 1, 1, ''),'') as Tags 
                from News with (nolock) where isnull(MigrationStatus,0) =1";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRowCustom);
        }

        public virtual List<News> GetByGuidForCache(string NewsGuid = null)
        {
            if (NewsGuid == null)
            {
                string sql = @"select top 1000 isnull(TranslatedTitle,'') as TranslatedTitle,Title,isnull(TranslatedDescription,'') as TranslatedDescription,Description,IsArchival,Guid,BunchGuid,LastUpdateDate,PublishTime,
                    stuff((Select ',' +convert(varchar,rm.FilterId) from NewsFilter rm (nolock) where News.Guid = rm.NewsGuid for xml path('')), 1, 1, '') as FilterId 
                    from News with (nolock) where isnull(IsArchival,0) = 1 order by LastUpdateDate";
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<News>(ds, NewsFromDataRowCustom);
            }
            else
            {
                string sql = @"
					declare @NewsId int
					select @NewsId= NewsId  from News nn with (nolock) where [Guid] = @NewsGuid 
                    select top 1000 TranslatedTitle,Title,TranslatedDescription,Description,IsArchival,Guid,BunchGuid,LastUpdateDate,PublishTime,
                    stuff((Select ',' +convert(varchar,rm.FilterId) from NewsFilter rm (nolock) where News.Guid = rm.NewsGuid for xml path('')), 1, 1, '') as FilterId 
                    from News with (nolock) where NewsId >@NewsId and isnull(IsArchival,0)= 1  
					order by LastUpdateDate";
                SqlParameter[] parameter = new SqlParameter[]{
               new SqlParameter("@NewsGuid", NewsGuid)};
                DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameter);
                if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
                return CollectionFromDataSet<News>(ds, NewsFromDataRowCustom);
            }
        }


        public virtual void UpdateNewsIndex(News entity)
        {
            entity.ThumbnailId = String.IsNullOrEmpty(entity.ThumbnailId) ? string.Empty : entity.ThumbnailId;
            string sql = @"Update News set 
							[IsIndexed]=@IsIndexed,
                            [ThumbnailId]=@ThumbnailId						
							 where NewsId=@NewsId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@IsIndexed",entity.IsIndexed ?? (object)DBNull.Value)					
					, new SqlParameter("@NewsId",entity.NewsId)
                    , new SqlParameter("@ThumbnailId",entity.ThumbnailId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }


        public virtual void UpdateNewsBunch(News entity)
        {
            string sql = @"Update News set 
							[BunchGuid]=@BunchGuid				
							 where NewsId=@NewsId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)					
					, new SqlParameter("@NewsId",entity.NewsId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public virtual void UpdateNewsBunch(string NewsGuid, string BunchGuid)
        {
            string sql = @"Update News set 
							[BunchGuid]=@BunchGuid				
							 where Guid=@NewsGuid";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BunchGuid",BunchGuid ?? (object)DBNull.Value)					
					, new SqlParameter("@NewsGuid",NewsGuid)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public virtual void UpdateNewsBunchbyNewsIds(string NewsGuidCsv, string BunchGuid)
        {
            string sql = @"Update News set 
							[BunchGuid]=@BunchGuid				
							 where Guid in (" + NewsGuidCsv + ")";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@BunchGuid",BunchGuid ?? (object)DBNull.Value)					};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public virtual void UpdateNewsTickerCount(string NewsGuid, int TickerCount)
        {
            string sql = @"Update News set 
							[TickerCount]=@TickerCount
							 where Guid=@NewsGuid";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@TickerCount",TickerCount)					
					, new SqlParameter("@NewsGuid",NewsGuid)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public virtual List<TickerImage> GetTickerImages(DateTime lastUpdateDate)
        {
            string conString1 = ConfigurationManager.ConnectionStrings["connTMS"].ToString();
            string sql = @"select * from TickerImage where CreationDate>@CreationDate";
            SqlParameter parameter1 = new SqlParameter("@CreationDate", DateTime.UtcNow.AddMinutes(-3));
            DataSet ds = SqlHelper.ExecuteDataset(conString1, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerImage>(ds, TickerImagesFromDataRowCustom);
        }

        public virtual List<TickerImage> GetTickerImagesByLastUpdateDate(DateTime lastUpdateDate)
        {
            string conString1 = ConfigurationManager.ConnectionStrings["connTMS"].ToString();
            string sql = @"select * from TickerImage where CreationDate>@LastUpadteDate";
            SqlParameter parameter1 = new SqlParameter("@LastUpadteDate", lastUpdateDate);
            DataSet ds = SqlHelper.ExecuteDataset(conString1, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerImage>(ds, TickerImagesFromDataRowCustom);
        }
        
        public virtual List<News> GetByDateAndfilters(string csvfilters, int pagecount, int startIndex, DateTime from, DateTime to, string csvDiscardedfilters, string SelectClause = null)
        {
            startIndex = startIndex < 1 ? 1 : startIndex;
            string Filters = csvfilters.Length > 0 ? "FilterId in (" + csvfilters + ")" : string.Empty;
            string DiscardedFilters = string.Empty;
            if (Filters.Length > 0)
                DiscardedFilters = csvDiscardedfilters.Length > 0 ? " And FilterId not in (" + csvDiscardedfilters + ")" : string.Empty;
            else
                DiscardedFilters = csvDiscardedfilters.Length > 0 ? "FilterId not in (" + csvDiscardedfilters + ")" : string.Empty;

            string sql = @"select n.* from newsfilter nf (nolock) 
                        inner join news n (nolock) on n.[guid] = nf.newsguid and isarchival=1
                        where " + Filters + DiscardedFilters + @"
                        and nf.LastUpdateDate >= @fromdate and nf.LastUpdateDate <= @ToDate
                        order by nf.LastUpdateDate desc
                        OFFSET ( @PageNo - 1 ) * @RecordsPerPage ROWS
                        FETCH NEXT @RecordsPerPage ROWS ONLY";
            SqlParameter parameter1 = new SqlParameter("@RecordsPerPage", pagecount);
            SqlParameter parameter2 = new SqlParameter("@PageNo", startIndex);
            SqlParameter parameter3 = new SqlParameter("@fromdate", from);
            SqlParameter parameter4 = new SqlParameter("@ToDate", to);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2, parameter3, parameter4 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<News>(ds, NewsFromDataRow);
        }


        public virtual News NewsFromDataRowCustom(DataRow dr)
        {
            if (dr == null) return null;
            News entity = new News();
            if (dr.Table.Columns.Contains("NewsId"))
            {
                entity.NewsId = (System.Int32)dr["NewsId"];
            }
            if (dr.Table.Columns.Contains("Guid"))
            {
                entity.Guid = dr["Guid"].ToString();
            }
            if (dr.Table.Columns.Contains("TranslatedDescription"))
            {
                entity.TranslatedDescription = dr["TranslatedDescription"].ToString();
            }
            if (dr.Table.Columns.Contains("TranslatedTitle"))
            {
                entity.TranslatedTitle = dr["TranslatedTitle"].ToString();
            }
            if (dr.Table.Columns.Contains("Title"))
            {
                entity.Title = dr["Title"].ToString();
            }
            if (dr.Table.Columns.Contains("BunchGuid"))
            {
                entity.BunchGuid = dr["BunchGuid"].ToString();
            }
            if (dr.Table.Columns.Contains("Description"))
            {
                entity.Description = dr["Description"].ToString();
            }
            if (dr.Table.Columns.Contains("LocationId"))
            {
                entity.LocationId = dr["LocationId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["LocationId"];
            }
            if (dr.Table.Columns.Contains("Author"))
            {
                entity.Author = dr["Author"].ToString();
            }
            if (dr.Table.Columns.Contains("UpdateTime"))
            {
                entity.UpdateTime = dr["UpdateTime"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["UpdateTime"];
            }
            if (dr.Table.Columns.Contains("LanguageCode"))
            {
                entity.LanguageCode = dr["LanguageCode"].ToString();
            }
            if (dr.Table.Columns.Contains("LinkNewsId"))
            {
                entity.LinkNewsId = dr["LinkNewsId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["LinkNewsId"];
            }
            if (dr.Table.Columns.Contains("ParentNewsId"))
            {
                entity.ParentNewsId = dr["ParentNewsId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["ParentNewsId"];
            }
            if (dr.Table.Columns.Contains("PreviousVersionId"))
            {
                entity.PreviousVersionId = dr["PreviousVersionId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["PreviousVersionId"];
            }
            if (dr.Table.Columns.Contains("PublishTime"))
            {
                entity.PublishTime = dr["PublishTime"] == DBNull.Value ? (System.DateTime?)null : (System.DateTime?)dr["PublishTime"];
            }
            if (dr.Table.Columns.Contains("ThumbnailId"))
            {
                entity.ThumbnailId = dr["ThumbnailId"].ToString();
            }
            if (dr.Table.Columns.Contains("VersionNumber"))
            {
                entity.VersionNumber = dr["VersionNumber"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["VersionNumber"];
            }
            if (dr.Table.Columns.Contains("IsIndexed"))
            {
                entity.IsIndexed = dr["IsIndexed"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsIndexed"];
            }
            if (dr.Table.Columns.Contains("NewsTypeId"))
            {
                entity.NewsTypeId = (System.Int32)dr["NewsTypeId"];
            }
            if (dr.Table.Columns.Contains("Source"))
            {
                entity.Source = dr["Source"].ToString();
            }
            if (dr.Table.Columns.Contains("SourceTypeId"))
            {
                entity.SourceTypeId = dr["SourceTypeId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SourceTypeId"];
            }
            if (dr.Table.Columns.Contains("SourceNewsUrl"))
            {
                entity.SourceNewsUrl = dr["SourceNewsUrl"].ToString();
            }
            if (dr.Table.Columns.Contains("SourceFilterId"))
            {
                entity.SourceFilterId = dr["SourceFilterId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["SourceFilterId"];
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("IsActive"))
            {
                entity.IsActive = (System.Boolean)dr["IsActive"];
            }
            if (dr.Table.Columns.Contains("DescriptionText"))
            {
                entity.DescriptionText = dr["DescriptionText"].ToString();
            }
            if (dr.Table.Columns.Contains("NewsDirection"))
            {
                entity.NewsDirection = dr["NewsDirection"].ToString();
            }
            if (dr.Table.Columns.Contains("ShortDescription"))
            {
                entity.ShortDescription = dr["ShortDescription"].ToString();
            }
            if (dr.Table.Columns.Contains("Version"))
            {
                entity.Version = dr["Version"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["Version"];
            }
            if (dr.Table.Columns.Contains("ReporterId"))
            {
                entity.ReporterId = dr["ReporterId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["ReporterId"];
            }
            if (dr.Table.Columns.Contains("IsVerified"))
            {
                entity.IsVerified = dr["IsVerified"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsVerified"];
            }
            if (dr.Table.Columns.Contains("IsAired"))
            {
                entity.IsAired = dr["IsAired"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsAired"];
            }
            if (dr.Table.Columns.Contains("IsSearchable"))
            {
                entity.IsSearchable = dr["IsSearchable"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsSearchable"];
            }
            if (dr.Table.Columns.Contains("HasResourceEdit"))
            {
                entity.HasResourceEdit = dr["HasResourceEdit"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["HasResourceEdit"];
            }
            if (dr.Table.Columns.Contains("ReferenceNewsId"))
            {
                entity.ReferenceNewsId = dr["ReferenceNewsId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["ReferenceNewsId"];
            }
            if (dr.Table.Columns.Contains("IsCompleted"))
            {
                entity.IsCompleted = dr["IsCompleted"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsCompleted"];
            }
            if (dr.Table.Columns.Contains("HasNoResource"))
            {
                entity.HasNoResource = dr["HasNoResource"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["HasNoResource"];
            }
            if (dr.Table.Columns.Contains("IsArchival"))
            {
                entity.IsArchival = dr["IsArchival"] == DBNull.Value ? (System.Boolean?)null : (System.Boolean?)dr["IsArchival"];
            }
            if (dr.Table.Columns.Contains("AddedToRundownCount"))
            {
                entity.AddedToRundownCount = dr["AddedToRundownCount"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["AddedToRundownCount"];
            }
            if (dr.Table.Columns.Contains("OnAirCount"))
            {
                entity.OnAirCount = dr["OnAirCount"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["OnAirCount"];
            }
            if (dr.Table.Columns.Contains("OtherChannelExecutionCount"))
            {
                entity.OtherChannelExecutionCount = dr["OtherChannelExecutionCount"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["OtherChannelExecutionCount"];
            }
            if (dr.Table.Columns.Contains("NewsTickerCount"))
            {
                entity.NewsTickerCount = dr["NewsTickerCount"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["NewsTickerCount"];
            }
            if (dr.Table.Columns.Contains("ReferenceNewsGuid"))
            {
                entity.ReferenceNewsGuid = dr["ReferenceNewsGuid"].ToString();
            }
            if (dr.Table.Columns.Contains("ParentNewsGuid"))
            {
                entity.ParentNewsGuid = dr["ParentNewsGuid"].ToString();
            }
            if (dr.Table.Columns.Contains("FilterId"))
            {
                entity.filters = dr["FilterId"].ToString();
            }
            if (dr.Table.Columns.Contains("Tags"))
            {
                entity.tags = dr["Tags"].ToString();
            }
            return entity;
        }

        public virtual TickerImage TickerImagesFromDataRowCustom(DataRow dr)
        {
            if (dr == null) return null;
            TickerImage entity = new TickerImage();
            if (dr.Table.Columns.Contains("ChannelId"))
            {
                entity.ChannelId = (System.Int32)dr["ChannelId"];
            }
            if (dr.Table.Columns.Contains("TickerImageId"))
            {
                entity.TickerImageId = (System.Int32)dr["TickerImageId"];
            }
            if (dr.Table.Columns.Contains("ImagePath"))
            {
                entity.ImagePath = dr["ImagePath"].ToString();
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("ChannelTickerLocationId"))
            {
                entity.ChannelTickerLocationId = (System.Int32)dr["ChannelTickerLocationId"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            return entity;
        }


    }


}
