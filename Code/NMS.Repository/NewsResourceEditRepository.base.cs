﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsResourceEditRepositoryBase : Repository, INewsResourceEditRepositoryBase 
	{
        
        public NewsResourceEditRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();


			this.SearchColumns.Add("NewsResourceEditId",new SearchColumn(){Name="NewsResourceEditId",Title="NewsResourceEditId",SelectClause="NewsResourceEditId",WhereClause="AllRecords.NewsResourceEditId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsResourceEditId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.Guid?",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceTypeId",new SearchColumn(){Name="ResourceTypeId",Title="ResourceTypeId",SelectClause="ResourceTypeId",WhereClause="AllRecords.ResourceTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ResourceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FileName",new SearchColumn(){Name="FileName",Title="FileName",SelectClause="FileName",WhereClause="AllRecords.FileName",DataType="System.String",IsForeignColumn=false,PropertyName="FileName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Top",new SearchColumn(){Name="Top",Title="Top",SelectClause="Top",WhereClause="AllRecords.Top",DataType="System.Double?",IsForeignColumn=false,PropertyName="Top",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Left",new SearchColumn(){Name="Left",Title="Left",SelectClause="Left",WhereClause="AllRecords.Left",DataType="System.Double?",IsForeignColumn=false,PropertyName="Left",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Bottom",new SearchColumn(){Name="Bottom",Title="Bottom",SelectClause="Bottom",WhereClause="AllRecords.Bottom",DataType="System.Double?",IsForeignColumn=false,PropertyName="Bottom",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Right",new SearchColumn(){Name="Right",Title="Right",SelectClause="Right",WhereClause="AllRecords.Right",DataType="System.Double?",IsForeignColumn=false,PropertyName="Right",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Url",new SearchColumn(){Name="Url",Title="Url",SelectClause="Url",WhereClause="AllRecords.Url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("isactive",new SearchColumn(){Name="isactive",Title="isactive",SelectClause="isactive",WhereClause="AllRecords.isactive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Isactive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelVideoId",new SearchColumn(){Name="ChannelVideoId",Title="ChannelVideoId",SelectClause="ChannelVideoId",WhereClause="AllRecords.ChannelVideoId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ChannelVideoId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsResourceEditSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsResourceEditBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsResourceEditAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsResourceEditSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsResourceEdit].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsResourceEdit].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsResourceEdit> GetNewsResourceEditByNewsId(System.Int32? NewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceEditSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsResourceEdit] with (nolock)  where NewsId=@NewsId  ";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResourceEdit>(ds,NewsResourceEditFromDataRow);
		}

		public virtual NewsResourceEdit GetNewsResourceEdit(System.Int32 NewsResourceEditId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceEditSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsResourceEdit] with (nolock)  where NewsResourceEditId=@NewsResourceEditId ";
			SqlParameter parameter=new SqlParameter("@NewsResourceEditId",NewsResourceEditId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsResourceEditFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsResourceEdit> GetNewsResourceEditByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceEditSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsResourceEdit] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResourceEdit>(ds,NewsResourceEditFromDataRow);
		}

		public virtual List<NewsResourceEdit> GetAllNewsResourceEdit(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceEditSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsResourceEdit] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResourceEdit>(ds, NewsResourceEditFromDataRow);
		}

		public virtual List<NewsResourceEdit> GetPagedNewsResourceEdit(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsResourceEditCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsResourceEditId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsResourceEditId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsResourceEditId] ";
            tempsql += " FROM [NewsResourceEdit] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsResourceEditId"))
					tempsql += " , (AllRecords.[NewsResourceEditId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsResourceEditId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsResourceEditSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsResourceEdit] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsResourceEdit].[NewsResourceEditId] = PageIndex.[NewsResourceEditId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResourceEdit>(ds, NewsResourceEditFromDataRow);
			}else{ return null;}
		}

		private int GetNewsResourceEditCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsResourceEdit as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsResourceEdit as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsResourceEdit))]
		public virtual NewsResourceEdit InsertNewsResourceEdit(NewsResourceEdit entity)
		{

			NewsResourceEdit other=new NewsResourceEdit();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsResourceEdit ( [ResourceGuid]
				,[NewsId]
				,[ResourceTypeId]
				,[FileName]
				,[Top]
				,[Left]
				,[Bottom]
				,[Right]
				,[Url]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive]
				,[NewsGuid]
				,[ChannelVideoId] )
				Values
				( @ResourceGuid
				, @NewsId
				, @ResourceTypeId
				, @FileName
				, @Top
				, @Left
				, @Bottom
				, @Right
				, @Url
				, @CreationDate
				, @LastUpdateDate
				, @isactive
				, @NewsGuid
				, @ChannelVideoId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@FileName",entity.FileName ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelVideoId",entity.ChannelVideoId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsResourceEdit(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsResourceEdit))]
		public virtual NewsResourceEdit UpdateNewsResourceEdit(NewsResourceEdit entity)
		{

			if (entity.IsTransient()) return entity;
			NewsResourceEdit other = GetNewsResourceEdit(entity.NewsResourceEditId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsResourceEdit set  [ResourceGuid]=@ResourceGuid
							, [NewsId]=@NewsId
							, [ResourceTypeId]=@ResourceTypeId
							, [FileName]=@FileName
							, [Top]=@Top
							, [Left]=@Left
							, [Bottom]=@Bottom
							, [Right]=@Right
							, [Url]=@Url
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive
							, [NewsGuid]=@NewsGuid
							, [ChannelVideoId]=@ChannelVideoId 
							 where NewsResourceEditId=@NewsResourceEditId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@FileName",entity.FileName ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelVideoId",entity.ChannelVideoId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsResourceEditId",entity.NewsResourceEditId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsResourceEdit(entity.NewsResourceEditId);
		}

		public virtual bool DeleteNewsResourceEdit(System.Int32 NewsResourceEditId)
		{

			string sql="delete from NewsResourceEdit where NewsResourceEditId=@NewsResourceEditId";
			SqlParameter parameter=new SqlParameter("@NewsResourceEditId",NewsResourceEditId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsResourceEdit))]
		public virtual NewsResourceEdit DeleteNewsResourceEdit(NewsResourceEdit entity)
		{
			this.DeleteNewsResourceEdit(entity.NewsResourceEditId);
			return entity;
		}


		public virtual NewsResourceEdit NewsResourceEditFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsResourceEdit entity=new NewsResourceEdit();
			if (dr.Table.Columns.Contains("NewsResourceEditId"))
			{
			entity.NewsResourceEditId = (System.Int32)dr["NewsResourceEditId"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = dr["ResourceGuid"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["ResourceGuid"];
			}
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = dr["NewsId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("ResourceTypeId"))
			{
			entity.ResourceTypeId = dr["ResourceTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ResourceTypeId"];
			}
			if (dr.Table.Columns.Contains("FileName"))
			{
			entity.FileName = dr["FileName"].ToString();
			}
			if (dr.Table.Columns.Contains("Top"))
			{
			entity.Top = dr["Top"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Top"];
			}
			if (dr.Table.Columns.Contains("Left"))
			{
			entity.Left = dr["Left"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Left"];
			}
			if (dr.Table.Columns.Contains("Bottom"))
			{
			entity.Bottom = dr["Bottom"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Bottom"];
			}
			if (dr.Table.Columns.Contains("Right"))
			{
			entity.Right = dr["Right"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Right"];
			}
			if (dr.Table.Columns.Contains("Url"))
			{
			entity.Url = dr["Url"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("isactive"))
			{
			entity.Isactive = dr["isactive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["isactive"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("ChannelVideoId"))
			{
			entity.ChannelVideoId = dr["ChannelVideoId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ChannelVideoId"];
			}
			return entity;
		}

	}
	
	
}
