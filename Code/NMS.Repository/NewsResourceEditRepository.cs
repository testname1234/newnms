﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public partial class NewsResourceEditRepository: NewsResourceEditRepositoryBase, INewsResourceEditRepository
	{
        public List<NewsResourceEdit> GetActiveNewsResourceEdit()
        {

            string sql = "select * from [NewsResourceEdit] with (nolock) where isactive=1 order by 1";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsResourceEdit>(ds, NewsResourceEditFromDataRow);
        }

        public virtual List<NewsResourceEdit> GetAllNewsResourceEditWithURL(string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsResourceEditSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsResourceEdit] with (nolock) where url is not null";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsResourceEdit>(ds, NewsResourceEditFromDataRow);
        }

        public bool SetInactive(int id)
        {
            string sql = "update [NewsResourceEdit] set isactive=0 where NewsResourceEditId=@id";
            SqlParameter parameter = new SqlParameter("@id", id);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

    }
	
	
}
