﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsResourceRepositoryBase : Repository, INewsResourceRepositoryBase 
	{
        
        public NewsResourceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsResourceId",new SearchColumn(){Name="NewsResourceId",Title="NewsResourceId",SelectClause="NewsResourceId",WhereClause="AllRecords.NewsResourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceId",new SearchColumn(){Name="ResourceId",Title="ResourceId",SelectClause="ResourceId",WhereClause="AllRecords.ResourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsResourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsResourceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsResourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsResourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsResource].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsResource].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsResource> GetNewsResourceByNewsId(System.Int32 NewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsResource] with (nolock)  where NewsId=@NewsId  ";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResource>(ds,NewsResourceFromDataRow);
		}

		public virtual List<NewsResource> GetNewsResourceByResourceId(System.Int32 ResourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsResource] with (nolock)  where ResourceId=@ResourceId  ";
			SqlParameter parameter=new SqlParameter("@ResourceId",ResourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResource>(ds,NewsResourceFromDataRow);
		}

		public virtual NewsResource GetNewsResource(System.Int32 NewsResourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsResource] with (nolock)  where NewsResourceId=@NewsResourceId ";
			SqlParameter parameter=new SqlParameter("@NewsResourceId",NewsResourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsResourceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsResource> GetNewsResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsResource] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResource>(ds,NewsResourceFromDataRow);
		}

		public virtual List<NewsResource> GetAllNewsResource(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsResource] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResource>(ds, NewsResourceFromDataRow);
		}

		public virtual List<NewsResource> GetPagedNewsResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsResourceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsResourceId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsResourceId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsResourceId] ";
            tempsql += " FROM [NewsResource] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsResourceId"))
					tempsql += " , (AllRecords.[NewsResourceId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsResourceId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsResourceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsResource] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsResource].[NewsResourceId] = PageIndex.[NewsResourceId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsResource>(ds, NewsResourceFromDataRow);
			}else{ return null;}
		}

		private int GetNewsResourceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsResource as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsResource as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsResource))]
		public virtual NewsResource InsertNewsResource(NewsResource entity)
		{

			NewsResource other=new NewsResource();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsResource ( [NewsId]
				,[ResourceId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[NewsGuid]
				,[ResourceGuid] )
				Values
				( @NewsId
				, @ResourceId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @NewsGuid
				, @ResourceGuid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@ResourceId",entity.ResourceId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsResource(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsResource))]
		public virtual NewsResource UpdateNewsResource(NewsResource entity)
		{

			if (entity.IsTransient()) return entity;
			NewsResource other = GetNewsResource(entity.NewsResourceId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsResource set  [NewsId]=@NewsId
							, [ResourceId]=@ResourceId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [NewsGuid]=@NewsGuid
							, [ResourceGuid]=@ResourceGuid 
							 where NewsResourceId=@NewsResourceId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@ResourceId",entity.ResourceId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsResourceId",entity.NewsResourceId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsResource(entity.NewsResourceId);
		}

		public virtual bool DeleteNewsResource(System.Int32 NewsResourceId)
		{

			string sql="delete from NewsResource where NewsResourceId=@NewsResourceId";
			SqlParameter parameter=new SqlParameter("@NewsResourceId",NewsResourceId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsResource))]
		public virtual NewsResource DeleteNewsResource(NewsResource entity)
		{
			this.DeleteNewsResource(entity.NewsResourceId);
			return entity;
		}


		public virtual NewsResource NewsResourceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsResource entity=new NewsResource();
			if (dr.Table.Columns.Contains("NewsResourceId"))
			{
			entity.NewsResourceId = (System.Int32)dr["NewsResourceId"];
			}
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = (System.Int32)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("ResourceId"))
			{
			entity.ResourceId = (System.Int32)dr["ResourceId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = dr["ResourceGuid"].ToString();
			}
			return entity;
		}

	}
	
	
}
