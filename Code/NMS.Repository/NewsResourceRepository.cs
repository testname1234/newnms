﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class NewsResourceRepository : NewsResourceRepositoryBase, INewsResourceRepository
    {
        public NewsResourceRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public NewsResourceRepository()
        {
        }
        public virtual bool DeleteNewsResourceByNewsId(System.Int32 NewsId)
        {

            string sql = "delete from NewsResource where NewsId=@NewsId";
            SqlParameter parameter = new SqlParameter("@NewsId", NewsId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual List<NewsResource> GetByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {
            string sql = @"select r.Guid,r.ResourceTypeId,Caption,Category,Location,Duration,nr.CreationDate,nr.LastUpdateDate from [NewsResource] nr (nolock)  
                         inner join Resource r on r.Guid = nr.ResourceGuid
                         where NewsGuid=@NewsGuid ";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsResource>(ds, NewsResourceFromDataRowCustom);
        }

        public virtual NewsResource NewsResourceFromDataRowCustom(DataRow dr)
        {
            if (dr == null) return null;
            NewsResource entity = new NewsResource();
            if (dr.Table.Columns.Contains("NewsResourceId"))
            {
                entity.NewsResourceId = (System.Int32)dr["NewsResourceId"];
            }
            if (dr.Table.Columns.Contains("NewsId"))
            {
                entity.NewsId = (System.Int32)dr["NewsId"];
            }
            if (dr.Table.Columns.Contains("ResourceId"))
            {
                entity.ResourceId = (System.Int32)dr["ResourceId"];
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("IsActive"))
            {
                entity.IsActive = (System.Boolean)dr["IsActive"];
            }
            if (dr.Table.Columns.Contains("NewsGuid"))
            {
                entity.NewsGuid = dr["NewsGuid"].ToString();
            }
            if (dr.Table.Columns.Contains("ResourceGuid"))
            {
                entity.ResourceGuid = dr["ResourceGuid"].ToString();
            }

            if (dr.Table.Columns.Contains("Guid"))
            {
                entity.Guid = dr["Guid"].ToString();
            }
            if (dr.Table.Columns.Contains("ResourceTypeId"))
            {
                entity.ResourceTypeId = Convert.ToInt32(dr["ResourceTypeId"].ToString());
            }
            if (dr.Table.Columns.Contains("Duration"))
            {
                if (dr["Duration"].ToString() != string.Empty)
                    entity.Duration = Convert.ToDouble(dr["Duration"].ToString());
            }
            if (dr.Table.Columns.Contains("Caption"))
            {
                entity.Caption = dr["Caption"].ToString();
            }
            if (dr.Table.Columns.Contains("Category"))
            {
                entity.Category = dr["Category"].ToString();
            }
            if (dr.Table.Columns.Contains("Location"))
            {
                entity.Location = dr["Location"].ToString();
            }
            return entity;
        }
    }


}
