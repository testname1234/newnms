﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsSourceRepositoryBase : Repository, INewsSourceRepositoryBase 
	{
        
        public NewsSourceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsSourceId",new SearchColumn(){Name="NewsSourceId",Title="NewsSourceId",SelectClause="NewsSourceId",WhereClause="AllRecords.NewsSourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsSourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Source",new SearchColumn(){Name="Source",Title="Source",SelectClause="Source",WhereClause="AllRecords.Source",DataType="System.String",IsForeignColumn=false,PropertyName="Source",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterTypeId",new SearchColumn(){Name="FilterTypeId",Title="FilterTypeId",SelectClause="FilterTypeId",WhereClause="AllRecords.FilterTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FilterTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterId",new SearchColumn(){Name="FilterId",Title="FilterId",SelectClause="FilterId",WhereClause="AllRecords.FilterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedDate",new SearchColumn(){Name="CreatedDate",Title="CreatedDate",SelectClause="CreatedDate",WhereClause="AllRecords.CreatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateddate",new SearchColumn(){Name="LastUpdateddate",Title="LastUpdateddate",SelectClause="LastUpdateddate",WhereClause="AllRecords.LastUpdateddate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateddate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("languageCode",new SearchColumn(){Name="languageCode",Title="languageCode",SelectClause="languageCode",WhereClause="AllRecords.languageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FolderPath",new SearchColumn(){Name="FolderPath",Title="FolderPath",SelectClause="FolderPath",WhereClause="AllRecords.FolderPath",DataType="System.String",IsForeignColumn=false,PropertyName="FolderPath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsSourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsSourceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsSourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsSourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsSource].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsSource].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual NewsSource GetNewsSource(System.Int32 NewsSourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsSource] with (nolock)  where NewsSourceId=@NewsSourceId ";
			SqlParameter parameter=new SqlParameter("@NewsSourceId",NewsSourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsSourceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsSource> GetNewsSourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsSource] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsSource>(ds,NewsSourceFromDataRow);
		}

		public virtual List<NewsSource> GetAllNewsSource(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsSourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsSource] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsSource>(ds, NewsSourceFromDataRow);
		}

		public virtual List<NewsSource> GetPagedNewsSource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsSourceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsSourceId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsSourceId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsSourceId] ";
            tempsql += " FROM [NewsSource] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsSourceId"))
					tempsql += " , (AllRecords.[NewsSourceId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsSourceId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsSourceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsSource] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsSource].[NewsSourceId] = PageIndex.[NewsSourceId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsSource>(ds, NewsSourceFromDataRow);
			}else{ return null;}
		}

		private int GetNewsSourceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsSource as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsSource as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsSource))]
		public virtual NewsSource InsertNewsSource(NewsSource entity)
		{

			NewsSource other=new NewsSource();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsSource ( [Source]
				,[FilterTypeId]
				,[FilterId]
				,[CreatedDate]
				,[LastUpdateddate]
				,[IsActive]
				,[languageCode]
				,[FolderPath] )
				Values
				( @Source
				, @FilterTypeId
				, @FilterId
				, @CreatedDate
				, @LastUpdateddate
				, @IsActive
				, @languageCode
				, @FolderPath );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@FilterTypeId",entity.FilterTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedDate",entity.CreatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateddate",entity.LastUpdateddate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@languageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@FolderPath",entity.FolderPath ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsSource(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsSource))]
		public virtual NewsSource UpdateNewsSource(NewsSource entity)
		{

			if (entity.IsTransient()) return entity;
			NewsSource other = GetNewsSource(entity.NewsSourceId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsSource set  [Source]=@Source
							, [FilterTypeId]=@FilterTypeId
							, [FilterId]=@FilterId
							, [LastUpdateddate]=@LastUpdateddate
							, [IsActive]=@IsActive
							, [languageCode]=@languageCode
							, [FolderPath]=@FolderPath 
							 where NewsSourceId=@NewsSourceId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@FilterTypeId",entity.FilterTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedDate",entity.CreatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateddate",entity.LastUpdateddate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@languageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@FolderPath",entity.FolderPath ?? (object)DBNull.Value)
					, new SqlParameter("@NewsSourceId",entity.NewsSourceId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsSource(entity.NewsSourceId);
		}

		public virtual bool DeleteNewsSource(System.Int32 NewsSourceId)
		{

			string sql="delete from NewsSource where NewsSourceId=@NewsSourceId";
			SqlParameter parameter=new SqlParameter("@NewsSourceId",NewsSourceId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsSource))]
		public virtual NewsSource DeleteNewsSource(NewsSource entity)
		{
			this.DeleteNewsSource(entity.NewsSourceId);
			return entity;
		}


		public virtual NewsSource NewsSourceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsSource entity=new NewsSource();
			if (dr.Table.Columns.Contains("NewsSourceId"))
			{
			entity.NewsSourceId = (System.Int32)dr["NewsSourceId"];
			}
			if (dr.Table.Columns.Contains("Source"))
			{
			entity.Source = dr["Source"].ToString();
			}
			if (dr.Table.Columns.Contains("FilterTypeId"))
			{
			entity.FilterTypeId = dr["FilterTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FilterTypeId"];
			}
			if (dr.Table.Columns.Contains("FilterId"))
			{
			entity.FilterId = dr["FilterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FilterId"];
			}
			if (dr.Table.Columns.Contains("CreatedDate"))
			{
			entity.CreatedDate = dr["CreatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreatedDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateddate"))
			{
			entity.LastUpdateddate = dr["LastUpdateddate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateddate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("languageCode"))
			{
			entity.LanguageCode = dr["languageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("FolderPath"))
			{
			entity.FolderPath = dr["FolderPath"].ToString();
			}
			return entity;
		}

	}
	
	
}
