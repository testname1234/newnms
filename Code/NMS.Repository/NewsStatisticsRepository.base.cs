﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsStatisticsRepositoryBase : Repository, INewsStatisticsRepositoryBase 
	{
        
        public NewsStatisticsRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsStatisticsId",new SearchColumn(){Name="NewsStatisticsId",Title="NewsStatisticsId",SelectClause="NewsStatisticsId",WhereClause="AllRecords.NewsStatisticsId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsStatisticsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelName",new SearchColumn(){Name="ChannelName",Title="ChannelName",SelectClause="ChannelName",WhereClause="AllRecords.ChannelName",DataType="System.String",IsForeignColumn=false,PropertyName="ChannelName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramName",new SearchColumn(){Name="ProgramName",Title="ProgramName",SelectClause="ProgramName",WhereClause="AllRecords.ProgramName",DataType="System.String",IsForeignColumn=false,PropertyName="ProgramName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EpisodeId",new SearchColumn(){Name="EpisodeId",Title="EpisodeId",SelectClause="EpisodeId",WhereClause="AllRecords.EpisodeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="EpisodeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Time",new SearchColumn(){Name="Time",Title="Time",SelectClause="Time",WhereClause="AllRecords.Time",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="Time",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsStatisticsSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsStatisticsBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsStatisticsAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsStatisticsSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsStatistics].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsStatistics].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual NewsStatistics GetNewsStatistics(System.Int32 NewsStatisticsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsStatisticsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsStatistics] with (nolock)  where NewsStatisticsId=@NewsStatisticsId ";
			SqlParameter parameter=new SqlParameter("@NewsStatisticsId",NewsStatisticsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsStatisticsFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsStatistics> GetNewsStatisticsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsStatisticsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsStatistics] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsStatistics>(ds,NewsStatisticsFromDataRow);
		}

		public virtual List<NewsStatistics> GetAllNewsStatistics(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsStatisticsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsStatistics] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsStatistics>(ds, NewsStatisticsFromDataRow);
		}

		public virtual List<NewsStatistics> GetPagedNewsStatistics(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsStatisticsCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsStatisticsId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsStatisticsId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsStatisticsId] ";
            tempsql += " FROM [NewsStatistics] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsStatisticsId"))
					tempsql += " , (AllRecords.[NewsStatisticsId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsStatisticsId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsStatisticsSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsStatistics] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsStatistics].[NewsStatisticsId] = PageIndex.[NewsStatisticsId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsStatistics>(ds, NewsStatisticsFromDataRow);
			}else{ return null;}
		}

		private int GetNewsStatisticsCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsStatistics as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsStatistics as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsStatistics))]
		public virtual NewsStatistics InsertNewsStatistics(NewsStatistics entity)
		{

			NewsStatistics other=new NewsStatistics();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsStatistics ( [ChannelName]
				,[ProgramId]
				,[ProgramName]
				,[EpisodeId]
				,[Time]
				,[Type]
				,[NewsGuid] )
				Values
				( @ChannelName
				, @ProgramId
				, @ProgramName
				, @EpisodeId
				, @Time
				, @Type
				, @NewsGuid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelName",entity.ChannelName ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramName",entity.ProgramName ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@Time",entity.Time ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsStatistics(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsStatistics))]
		public virtual NewsStatistics UpdateNewsStatistics(NewsStatistics entity)
		{

			if (entity.IsTransient()) return entity;
			NewsStatistics other = GetNewsStatistics(entity.NewsStatisticsId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsStatistics set  [ChannelName]=@ChannelName
							, [ProgramId]=@ProgramId
							, [ProgramName]=@ProgramName
							, [EpisodeId]=@EpisodeId
							, [Time]=@Time
							, [Type]=@Type
							, [NewsGuid]=@NewsGuid 
							 where NewsStatisticsId=@NewsStatisticsId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelName",entity.ChannelName ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramName",entity.ProgramName ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@Time",entity.Time ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsStatisticsId",entity.NewsStatisticsId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsStatistics(entity.NewsStatisticsId);
		}

		public virtual bool DeleteNewsStatistics(System.Int32 NewsStatisticsId)
		{

			string sql="delete from NewsStatistics where NewsStatisticsId=@NewsStatisticsId";
			SqlParameter parameter=new SqlParameter("@NewsStatisticsId",NewsStatisticsId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsStatistics))]
		public virtual NewsStatistics DeleteNewsStatistics(NewsStatistics entity)
		{
			this.DeleteNewsStatistics(entity.NewsStatisticsId);
			return entity;
		}


		public virtual NewsStatistics NewsStatisticsFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsStatistics entity=new NewsStatistics();
			if (dr.Table.Columns.Contains("NewsStatisticsId"))
			{
			entity.NewsStatisticsId = (System.Int32)dr["NewsStatisticsId"];
			}
			if (dr.Table.Columns.Contains("ChannelName"))
			{
			entity.ChannelName = dr["ChannelName"].ToString();
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("ProgramName"))
			{
			entity.ProgramName = dr["ProgramName"].ToString();
			}
			if (dr.Table.Columns.Contains("EpisodeId"))
			{
			entity.EpisodeId = dr["EpisodeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["EpisodeId"];
			}
			if (dr.Table.Columns.Contains("Time"))
			{
			entity.Time = dr["Time"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["Time"];
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Type"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			return entity;
		}

	}
	
	
}
