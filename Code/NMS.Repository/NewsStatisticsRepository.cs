﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{

    public partial class NewsStatisticsRepository : NewsStatisticsRepositoryBase, INewsStatisticsRepository
    {

        public virtual List<NewsStatistics> GetByNewsGuid(string NewsGuid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsStatisticsSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsStatistics] with (nolock) where NewsGuid = @NewsGuid ";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsStatistics>(ds, NewsStatisticsFromDataRow);
        }
    }


}
