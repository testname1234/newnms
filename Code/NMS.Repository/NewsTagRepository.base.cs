﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsTagRepositoryBase : Repository, INewsTagRepositoryBase 
	{
        
        public NewsTagRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsTagId",new SearchColumn(){Name="NewsTagId",Title="NewsTagId",SelectClause="NewsTagId",WhereClause="AllRecords.NewsTagId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsTagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Rank",new SearchColumn(){Name="Rank",Title="Rank",SelectClause="Rank",WhereClause="AllRecords.Rank",DataType="System.Double?",IsForeignColumn=false,PropertyName="Rank",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TagId",new SearchColumn(){Name="TagId",Title="TagId",SelectClause="TagId",WhereClause="AllRecords.TagId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TagGuid",new SearchColumn(){Name="TagGuid",Title="TagGuid",SelectClause="TagGuid",WhereClause="AllRecords.TagGuid",DataType="System.String",IsForeignColumn=false,PropertyName="TagGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Tag",new SearchColumn(){Name="Tag",Title="Tag",SelectClause="Tag",WhereClause="AllRecords.Tag",DataType="System.String",IsForeignColumn=false,PropertyName="Tag",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsTagSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsTagBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsTagAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsTagSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsTag].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsTag].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<NewsTag> GetNewsTagByTagId(System.Int32 TagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsTag] with (nolock)  where TagId=@TagId  ";
			SqlParameter parameter=new SqlParameter("@TagId",TagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsTag>(ds,NewsTagFromDataRow);
		}

		public virtual List<NewsTag> GetNewsTagByNewsId(System.Int32 NewsId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsTag] with (nolock)  where NewsId=@NewsId  ";
			SqlParameter parameter=new SqlParameter("@NewsId",NewsId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsTag>(ds,NewsTagFromDataRow);
		}

		public virtual NewsTag GetNewsTag(System.Int32 NewsTagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsTag] with (nolock)  where NewsTagId=@NewsTagId ";
			SqlParameter parameter=new SqlParameter("@NewsTagId",NewsTagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsTagFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsTag> GetNewsTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsTag] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsTag>(ds,NewsTagFromDataRow);
		}

		public virtual List<NewsTag> GetAllNewsTag(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsTag] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsTag>(ds, NewsTagFromDataRow);
		}

		public virtual List<NewsTag> GetPagedNewsTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsTagCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsTagId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsTagId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsTagId] ";
            tempsql += " FROM [NewsTag] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsTagId"))
					tempsql += " , (AllRecords.[NewsTagId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsTagId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsTagSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsTag] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsTag].[NewsTagId] = PageIndex.[NewsTagId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsTag>(ds, NewsTagFromDataRow);
			}else{ return null;}
		}

		private int GetNewsTagCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsTag as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsTag as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsTag))]
		public virtual NewsTag InsertNewsTag(NewsTag entity)
		{

			NewsTag other=new NewsTag();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsTag ( [Rank]
				,[TagId]
				,[NewsId]
				,[CreationDate]
				,[LastUpdateDate]
				,[NewsGuid]
				,[TagGuid]
				,[Tag] )
				Values
				( @Rank
				, @TagId
				, @NewsId
				, @CreationDate
				, @LastUpdateDate
				, @NewsGuid
				, @TagGuid
				, @Tag );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Rank",entity.Rank ?? (object)DBNull.Value)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@TagGuid",entity.TagGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Tag",entity.Tag ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsTag(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsTag))]
		public virtual NewsTag UpdateNewsTag(NewsTag entity)
		{

			if (entity.IsTransient()) return entity;
			NewsTag other = GetNewsTag(entity.NewsTagId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsTag set  [Rank]=@Rank
							, [TagId]=@TagId
							, [NewsId]=@NewsId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [NewsGuid]=@NewsGuid
							, [TagGuid]=@TagGuid
							, [Tag]=@Tag 
							 where NewsTagId=@NewsTagId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Rank",entity.Rank ?? (object)DBNull.Value)
					, new SqlParameter("@TagId",entity.TagId)
					, new SqlParameter("@NewsId",entity.NewsId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@TagGuid",entity.TagGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Tag",entity.Tag ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTagId",entity.NewsTagId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsTag(entity.NewsTagId);
		}

		public virtual bool DeleteNewsTag(System.Int32 NewsTagId)
		{

			string sql="delete from NewsTag where NewsTagId=@NewsTagId";
			SqlParameter parameter=new SqlParameter("@NewsTagId",NewsTagId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsTag))]
		public virtual NewsTag DeleteNewsTag(NewsTag entity)
		{
			this.DeleteNewsTag(entity.NewsTagId);
			return entity;
		}


		public virtual NewsTag NewsTagFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsTag entity=new NewsTag();
			if (dr.Table.Columns.Contains("NewsTagId"))
			{
			entity.NewsTagId = (System.Int32)dr["NewsTagId"];
			}
			if (dr.Table.Columns.Contains("Rank"))
			{
			entity.Rank = dr["Rank"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Rank"];
			}
			if (dr.Table.Columns.Contains("TagId"))
			{
			entity.TagId = (System.Int32)dr["TagId"];
			}
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = (System.Int32)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("TagGuid"))
			{
			entity.TagGuid = dr["TagGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("Tag"))
			{
			entity.Tag = dr["Tag"].ToString();
			}
			return entity;
		}

	}
	
	
}
