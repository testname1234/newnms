﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class NewsTagRepository: NewsTagRepositoryBase, INewsTagRepository
	{
         public NewsTagRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

         public NewsTagRepository()
        {
        }
        public virtual bool DeleteNewsTagBYNewsId(System.Int32 NewsId)
        {

            string sql = "delete from NewsTag where NewsId=@NewsId";
            SqlParameter parameter = new SqlParameter("@NewsId", NewsId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual List<NewsTag> GetByNewsGuid(System.String NewsGuid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsTagSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsTag] with (nolock)  where NewsGuid=@NewsGuid";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<NewsTag>(ds, NewsTagFromDataRow);
        }
	}
	
	
}
