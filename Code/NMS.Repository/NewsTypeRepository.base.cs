﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsTypeRepositoryBase : Repository, INewsTypeRepositoryBase 
	{
        
        public NewsTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsTypeId",new SearchColumn(){Name="NewsTypeId",Title="NewsTypeId",SelectClause="NewsTypeId",WhereClause="AllRecords.NewsTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsType",new SearchColumn(){Name="NewsType",Title="NewsType",SelectClause="NewsType",WhereClause="AllRecords.NewsType",DataType="System.String",IsForeignColumn=false,PropertyName="NewsType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "NewsType."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",NewsType."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual NewsType GetNewsType(System.Int32 NewsTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsType with (nolock)  where NewsTypeId=@NewsTypeId ";
			SqlParameter parameter=new SqlParameter("@NewsTypeId",NewsTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsType> GetNewsTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from NewsType with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsType>(ds,NewsTypeFromDataRow);
		}

		public virtual List<NewsType> GetAllNewsType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from NewsType with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsType>(ds, NewsTypeFromDataRow);
		}

		public virtual List<NewsType> GetPagedNewsType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsTypeId] ";
            tempsql += " FROM [NewsType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsTypeId"))
					tempsql += " , (AllRecords.[NewsTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsType].[NewsTypeId] = PageIndex.[NewsTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsType>(ds, NewsTypeFromDataRow);
			}else{ return null;}
		}

		private int GetNewsTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsType))]
		public virtual NewsType InsertNewsType(NewsType entity)
		{

			NewsType other=new NewsType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsType ( [NewsType] )
				Values
				( @NewsType );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsType",entity.NewsType)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsType))]
		public virtual NewsType UpdateNewsType(NewsType entity)
		{

			if (entity.IsTransient()) return entity;
			NewsType other = GetNewsType(entity.NewsTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsType set  [NewsType]=@NewsType 
							 where NewsTypeId=@NewsTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsType",entity.NewsType)
					, new SqlParameter("@NewsTypeId",entity.NewsTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsType(entity.NewsTypeId);
		}

		public virtual bool DeleteNewsType(System.Int32 NewsTypeId)
		{

			string sql="delete from NewsType with (nolock) where NewsTypeId=@NewsTypeId";
			SqlParameter parameter=new SqlParameter("@NewsTypeId",NewsTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsType))]
		public virtual NewsType DeleteNewsType(NewsType entity)
		{
			this.DeleteNewsType(entity.NewsTypeId);
			return entity;
		}


		public virtual NewsType NewsTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsType entity=new NewsType();
			if (dr.Table.Columns.Contains("NewsTypeId"))
			{
			entity.NewsTypeId = (System.Int32)dr["NewsTypeId"];
			}
			if (dr.Table.Columns.Contains("NewsType"))
			{
			entity.NewsType = dr["NewsType"].ToString();
			}
			return entity;
		}

	}
	
	
}
