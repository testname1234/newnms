﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NewsUpdateHistoryRepositoryBase : Repository, INewsUpdateHistoryRepositoryBase 
	{
        
        public NewsUpdateHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NewsUpdateHistoryId",new SearchColumn(){Name="NewsUpdateHistoryId",Title="NewsUpdateHistoryId",SelectClause="NewsUpdateHistoryId",WhereClause="AllRecords.NewsUpdateHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NewsUpdateHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsId",new SearchColumn(){Name="NewsId",Title="NewsId",SelectClause="NewsId",WhereClause="AllRecords.NewsId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TranslatedDescription",new SearchColumn(){Name="TranslatedDescription",Title="TranslatedDescription",SelectClause="TranslatedDescription",WhereClause="AllRecords.TranslatedDescription",DataType="System.String",IsForeignColumn=false,PropertyName="TranslatedDescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TranslatedTitle",new SearchColumn(){Name="TranslatedTitle",Title="TranslatedTitle",SelectClause="TranslatedTitle",WhereClause="AllRecords.TranslatedTitle",DataType="System.String",IsForeignColumn=false,PropertyName="TranslatedTitle",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Title",new SearchColumn(){Name="Title",Title="Title",SelectClause="Title",WhereClause="AllRecords.Title",DataType="System.String",IsForeignColumn=false,PropertyName="Title",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BunchGuid",new SearchColumn(){Name="BunchGuid",Title="BunchGuid",SelectClause="BunchGuid",WhereClause="AllRecords.BunchGuid",DataType="System.String",IsForeignColumn=false,PropertyName="BunchGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Description",new SearchColumn(){Name="Description",Title="Description",SelectClause="Description",WhereClause="AllRecords.Description",DataType="System.String",IsForeignColumn=false,PropertyName="Description",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DescriptionText",new SearchColumn(){Name="DescriptionText",Title="DescriptionText",SelectClause="DescriptionText",WhereClause="AllRecords.DescriptionText",DataType="System.String",IsForeignColumn=false,PropertyName="DescriptionText",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ShortDescription",new SearchColumn(){Name="ShortDescription",Title="ShortDescription",SelectClause="ShortDescription",WhereClause="AllRecords.ShortDescription",DataType="System.String",IsForeignColumn=false,PropertyName="ShortDescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Author",new SearchColumn(){Name="Author",Title="Author",SelectClause="Author",WhereClause="AllRecords.Author",DataType="System.String",IsForeignColumn=false,PropertyName="Author",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReporterId",new SearchColumn(){Name="ReporterId",Title="ReporterId",SelectClause="ReporterId",WhereClause="AllRecords.ReporterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ReporterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsVerified",new SearchColumn(){Name="IsVerified",Title="IsVerified",SelectClause="IsVerified",WhereClause="AllRecords.IsVerified",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsVerified",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsAired",new SearchColumn(){Name="IsAired",Title="IsAired",SelectClause="IsAired",WhereClause="AllRecords.IsAired",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsAired",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AddedToRundownCount",new SearchColumn(){Name="AddedToRundownCount",Title="AddedToRundownCount",SelectClause="AddedToRundownCount",WhereClause="AllRecords.AddedToRundownCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="AddedToRundownCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OnAirCount",new SearchColumn(){Name="OnAirCount",Title="OnAirCount",SelectClause="OnAirCount",WhereClause="AllRecords.OnAirCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="OnAirCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OtherChannelExecutionCount",new SearchColumn(){Name="OtherChannelExecutionCount",Title="OtherChannelExecutionCount",SelectClause="OtherChannelExecutionCount",WhereClause="AllRecords.OtherChannelExecutionCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="OtherChannelExecutionCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsTickerCount",new SearchColumn(){Name="NewsTickerCount",Title="NewsTickerCount",SelectClause="NewsTickerCount",WhereClause="AllRecords.NewsTickerCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsTickerCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Slug",new SearchColumn(){Name="Slug",Title="Slug",SelectClause="Slug",WhereClause="AllRecords.Slug",DataType="System.String",IsForeignColumn=false,PropertyName="Slug",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsPaperdescription",new SearchColumn(){Name="NewsPaperdescription",Title="NewsPaperdescription",SelectClause="NewsPaperdescription",WhereClause="AllRecords.NewsPaperdescription",DataType="System.String",IsForeignColumn=false,PropertyName="NewsPaperdescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Suggestions",new SearchColumn(){Name="Suggestions",Title="Suggestions",SelectClause="Suggestions",WhereClause="AllRecords.Suggestions",DataType="System.String",IsForeignColumn=false,PropertyName="Suggestions",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Source",new SearchColumn(){Name="Source",Title="Source",SelectClause="Source",WhereClause="AllRecords.Source",DataType="System.String",IsForeignColumn=false,PropertyName="Source",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceTypeId",new SearchColumn(){Name="SourceTypeId",Title="SourceTypeId",SelectClause="SourceTypeId",WhereClause="AllRecords.SourceTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SourceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceNewsUrl",new SearchColumn(){Name="SourceNewsUrl",Title="SourceNewsUrl",SelectClause="SourceNewsUrl",WhereClause="AllRecords.SourceNewsUrl",DataType="System.String",IsForeignColumn=false,PropertyName="SourceNewsUrl",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceFilterId",new SearchColumn(){Name="SourceFilterId",Title="SourceFilterId",SelectClause="SourceFilterId",WhereClause="AllRecords.SourceFilterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SourceFilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ThumbnailId",new SearchColumn(){Name="ThumbnailId",Title="ThumbnailId",SelectClause="ThumbnailId",WhereClause="AllRecords.ThumbnailId",DataType="System.String",IsForeignColumn=false,PropertyName="ThumbnailId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsTypeId",new SearchColumn(){Name="NewsTypeId",Title="NewsTypeId",SelectClause="NewsTypeId",WhereClause="AllRecords.NewsTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PublishTime",new SearchColumn(){Name="PublishTime",Title="PublishTime",SelectClause="PublishTime",WhereClause="AllRecords.PublishTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="PublishTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlugId",new SearchColumn(){Name="SlugId",Title="SlugId",SelectClause="SlugId",WhereClause="AllRecords.SlugId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlugId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNewsUpdateHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNewsUpdateHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNewsUpdateHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNewsUpdateHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NewsUpdateHistory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NewsUpdateHistory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual NewsUpdateHistory GetNewsUpdateHistory(System.Int32 NewsUpdateHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsUpdateHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsUpdateHistory] with (nolock)  where NewsUpdateHistoryId=@NewsUpdateHistoryId ";
			SqlParameter parameter=new SqlParameter("@NewsUpdateHistoryId",NewsUpdateHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NewsUpdateHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NewsUpdateHistory> GetNewsUpdateHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsUpdateHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NewsUpdateHistory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsUpdateHistory>(ds,NewsUpdateHistoryFromDataRow);
		}

		public virtual List<NewsUpdateHistory> GetAllNewsUpdateHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNewsUpdateHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NewsUpdateHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsUpdateHistory>(ds, NewsUpdateHistoryFromDataRow);
		}

		public virtual List<NewsUpdateHistory> GetPagedNewsUpdateHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNewsUpdateHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NewsUpdateHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NewsUpdateHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NewsUpdateHistoryId] ";
            tempsql += " FROM [NewsUpdateHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NewsUpdateHistoryId"))
					tempsql += " , (AllRecords.[NewsUpdateHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NewsUpdateHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNewsUpdateHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NewsUpdateHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NewsUpdateHistory].[NewsUpdateHistoryId] = PageIndex.[NewsUpdateHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NewsUpdateHistory>(ds, NewsUpdateHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetNewsUpdateHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NewsUpdateHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NewsUpdateHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NewsUpdateHistory))]
		public virtual NewsUpdateHistory InsertNewsUpdateHistory(NewsUpdateHistory entity)
		{

			NewsUpdateHistory other=new NewsUpdateHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NewsUpdateHistory ( [NewsGuid]
				,[NewsId]
				,[TranslatedDescription]
				,[TranslatedTitle]
				,[Title]
				,[BunchGuid]
				,[Description]
				,[DescriptionText]
				,[ShortDescription]
				,[LanguageCode]
				,[Author]
				,[ReporterId]
				,[IsVerified]
				,[IsAired]
				,[AddedToRundownCount]
				,[OnAirCount]
				,[OtherChannelExecutionCount]
				,[NewsTickerCount]
				,[Slug]
				,[NewsPaperdescription]
				,[Suggestions]
				,[Source]
				,[SourceTypeId]
				,[SourceNewsUrl]
				,[SourceFilterId]
				,[ThumbnailId]
				,[NewsTypeId]
				,[CreationDate]
				,[LastUpdateDate]
				,[PublishTime]
				,[IsActive]
				,[SlugId] )
				Values
				( @NewsGuid
				, @NewsId
				, @TranslatedDescription
				, @TranslatedTitle
				, @Title
				, @BunchGuid
				, @Description
				, @DescriptionText
				, @ShortDescription
				, @LanguageCode
				, @Author
				, @ReporterId
				, @IsVerified
				, @IsAired
				, @AddedToRundownCount
				, @OnAirCount
				, @OtherChannelExecutionCount
				, @NewsTickerCount
				, @Slug
				, @NewsPaperdescription
				, @Suggestions
				, @Source
				, @SourceTypeId
				, @SourceNewsUrl
				, @SourceFilterId
				, @ThumbnailId
				, @NewsTypeId
				, @CreationDate
				, @LastUpdateDate
				, @PublishTime
				, @IsActive
				, @SlugId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)
					, new SqlParameter("@ShortDescription",entity.ShortDescription ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@Author",entity.Author ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsAired",entity.IsAired ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundownCount",entity.AddedToRundownCount ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirCount",entity.OnAirCount ?? (object)DBNull.Value)
					, new SqlParameter("@OtherChannelExecutionCount",entity.OtherChannelExecutionCount ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTickerCount",entity.NewsTickerCount ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperdescription",entity.NewsPaperdescription ?? (object)DBNull.Value)
					, new SqlParameter("@Suggestions",entity.Suggestions ?? (object)DBNull.Value)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbnailId",entity.ThumbnailId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTypeId",entity.NewsTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@PublishTime",entity.PublishTime ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNewsUpdateHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NewsUpdateHistory))]
		public virtual NewsUpdateHistory UpdateNewsUpdateHistory(NewsUpdateHistory entity)
		{

			if (entity.IsTransient()) return entity;
			NewsUpdateHistory other = GetNewsUpdateHistory(entity.NewsUpdateHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NewsUpdateHistory set  [NewsGuid]=@NewsGuid
							, [NewsId]=@NewsId
							, [TranslatedDescription]=@TranslatedDescription
							, [TranslatedTitle]=@TranslatedTitle
							, [Title]=@Title
							, [BunchGuid]=@BunchGuid
							, [Description]=@Description
							, [DescriptionText]=@DescriptionText
							, [ShortDescription]=@ShortDescription
							, [LanguageCode]=@LanguageCode
							, [Author]=@Author
							, [ReporterId]=@ReporterId
							, [IsVerified]=@IsVerified
							, [IsAired]=@IsAired
							, [AddedToRundownCount]=@AddedToRundownCount
							, [OnAirCount]=@OnAirCount
							, [OtherChannelExecutionCount]=@OtherChannelExecutionCount
							, [NewsTickerCount]=@NewsTickerCount
							, [Slug]=@Slug
							, [NewsPaperdescription]=@NewsPaperdescription
							, [Suggestions]=@Suggestions
							, [Source]=@Source
							, [SourceTypeId]=@SourceTypeId
							, [SourceNewsUrl]=@SourceNewsUrl
							, [SourceFilterId]=@SourceFilterId
							, [ThumbnailId]=@ThumbnailId
							, [NewsTypeId]=@NewsTypeId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [PublishTime]=@PublishTime
							, [IsActive]=@IsActive
							, [SlugId]=@SlugId 
							 where NewsUpdateHistoryId=@NewsUpdateHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@NewsId",entity.NewsId ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@BunchGuid",entity.BunchGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@DescriptionText",entity.DescriptionText ?? (object)DBNull.Value)
					, new SqlParameter("@ShortDescription",entity.ShortDescription ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@Author",entity.Author ?? (object)DBNull.Value)
					, new SqlParameter("@ReporterId",entity.ReporterId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVerified",entity.IsVerified ?? (object)DBNull.Value)
					, new SqlParameter("@IsAired",entity.IsAired ?? (object)DBNull.Value)
					, new SqlParameter("@AddedToRundownCount",entity.AddedToRundownCount ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirCount",entity.OnAirCount ?? (object)DBNull.Value)
					, new SqlParameter("@OtherChannelExecutionCount",entity.OtherChannelExecutionCount ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTickerCount",entity.NewsTickerCount ?? (object)DBNull.Value)
					, new SqlParameter("@Slug",entity.Slug ?? (object)DBNull.Value)
					, new SqlParameter("@NewsPaperdescription",entity.NewsPaperdescription ?? (object)DBNull.Value)
					, new SqlParameter("@Suggestions",entity.Suggestions ?? (object)DBNull.Value)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@SourceTypeId",entity.SourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SourceNewsUrl",entity.SourceNewsUrl ?? (object)DBNull.Value)
					, new SqlParameter("@SourceFilterId",entity.SourceFilterId ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbnailId",entity.ThumbnailId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsTypeId",entity.NewsTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@PublishTime",entity.PublishTime ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@SlugId",entity.SlugId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsUpdateHistoryId",entity.NewsUpdateHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNewsUpdateHistory(entity.NewsUpdateHistoryId);
		}

		public virtual bool DeleteNewsUpdateHistory(System.Int32 NewsUpdateHistoryId)
		{

			string sql="delete from NewsUpdateHistory where NewsUpdateHistoryId=@NewsUpdateHistoryId";
			SqlParameter parameter=new SqlParameter("@NewsUpdateHistoryId",NewsUpdateHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NewsUpdateHistory))]
		public virtual NewsUpdateHistory DeleteNewsUpdateHistory(NewsUpdateHistory entity)
		{
			this.DeleteNewsUpdateHistory(entity.NewsUpdateHistoryId);
			return entity;
		}


		public virtual NewsUpdateHistory NewsUpdateHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NewsUpdateHistory entity=new NewsUpdateHistory();
			if (dr.Table.Columns.Contains("NewsUpdateHistoryId"))
			{
			entity.NewsUpdateHistoryId = (System.Int32)dr["NewsUpdateHistoryId"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsId"))
			{
			entity.NewsId = dr["NewsId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsId"];
			}
			if (dr.Table.Columns.Contains("TranslatedDescription"))
			{
			entity.TranslatedDescription = dr["TranslatedDescription"].ToString();
			}
			if (dr.Table.Columns.Contains("TranslatedTitle"))
			{
			entity.TranslatedTitle = dr["TranslatedTitle"].ToString();
			}
			if (dr.Table.Columns.Contains("Title"))
			{
			entity.Title = dr["Title"].ToString();
			}
			if (dr.Table.Columns.Contains("BunchGuid"))
			{
			entity.BunchGuid = dr["BunchGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("Description"))
			{
			entity.Description = dr["Description"].ToString();
			}
			if (dr.Table.Columns.Contains("DescriptionText"))
			{
			entity.DescriptionText = dr["DescriptionText"].ToString();
			}
			if (dr.Table.Columns.Contains("ShortDescription"))
			{
			entity.ShortDescription = dr["ShortDescription"].ToString();
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("Author"))
			{
			entity.Author = dr["Author"].ToString();
			}
			if (dr.Table.Columns.Contains("ReporterId"))
			{
			entity.ReporterId = dr["ReporterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ReporterId"];
			}
			if (dr.Table.Columns.Contains("IsVerified"))
			{
			entity.IsVerified = dr["IsVerified"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsVerified"];
			}
			if (dr.Table.Columns.Contains("IsAired"))
			{
			entity.IsAired = dr["IsAired"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsAired"];
			}
			if (dr.Table.Columns.Contains("AddedToRundownCount"))
			{
			entity.AddedToRundownCount = dr["AddedToRundownCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["AddedToRundownCount"];
			}
			if (dr.Table.Columns.Contains("OnAirCount"))
			{
			entity.OnAirCount = dr["OnAirCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["OnAirCount"];
			}
			if (dr.Table.Columns.Contains("OtherChannelExecutionCount"))
			{
			entity.OtherChannelExecutionCount = dr["OtherChannelExecutionCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["OtherChannelExecutionCount"];
			}
			if (dr.Table.Columns.Contains("NewsTickerCount"))
			{
			entity.NewsTickerCount = dr["NewsTickerCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsTickerCount"];
			}
			if (dr.Table.Columns.Contains("Slug"))
			{
			entity.Slug = dr["Slug"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsPaperdescription"))
			{
			entity.NewsPaperdescription = dr["NewsPaperdescription"].ToString();
			}
			if (dr.Table.Columns.Contains("Suggestions"))
			{
			entity.Suggestions = dr["Suggestions"].ToString();
			}
			if (dr.Table.Columns.Contains("Source"))
			{
			entity.Source = dr["Source"].ToString();
			}
			if (dr.Table.Columns.Contains("SourceTypeId"))
			{
			entity.SourceTypeId = dr["SourceTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SourceTypeId"];
			}
			if (dr.Table.Columns.Contains("SourceNewsUrl"))
			{
			entity.SourceNewsUrl = dr["SourceNewsUrl"].ToString();
			}
			if (dr.Table.Columns.Contains("SourceFilterId"))
			{
			entity.SourceFilterId = dr["SourceFilterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SourceFilterId"];
			}
			if (dr.Table.Columns.Contains("ThumbnailId"))
			{
			entity.ThumbnailId = dr["ThumbnailId"].ToString();
			}
			if (dr.Table.Columns.Contains("NewsTypeId"))
			{
			entity.NewsTypeId = dr["NewsTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsTypeId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("PublishTime"))
			{
			entity.PublishTime = dr["PublishTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["PublishTime"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("SlugId"))
			{
			entity.SlugId = dr["SlugId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlugId"];
			}
			return entity;
		}

	}
	
	
}
