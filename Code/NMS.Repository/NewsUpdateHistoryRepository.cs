﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class NewsUpdateHistoryRepository: NewsUpdateHistoryRepositoryBase, INewsUpdateHistoryRepository
	{
        public virtual List<NewsUpdateHistory> GetByGuid(System.String NewsGuid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetNewsUpdateHistorySelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [NewsUpdateHistory] with (nolock)  where NewsGuid=@NewsGuid ";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return CollectionFromDataSet<NewsUpdateHistory>(ds, NewsUpdateHistoryFromDataRow);
        }
	}
	
	
}
