﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NotificationRepositoryBase : Repository, INotificationRepositoryBase 
	{
        
        public NotificationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NotificationId",new SearchColumn(){Name="NotificationId",Title="NotificationId",SelectClause="NotificationId",WhereClause="AllRecords.NotificationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NotificationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NotificationTypeId",new SearchColumn(){Name="NotificationTypeId",Title="NotificationTypeId",SelectClause="NotificationTypeId",WhereClause="AllRecords.NotificationTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NotificationTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Title",new SearchColumn(){Name="Title",Title="Title",SelectClause="Title",WhereClause="AllRecords.Title",DataType="System.String",IsForeignColumn=false,PropertyName="Title",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Argument",new SearchColumn(){Name="Argument",Title="Argument",SelectClause="Argument",WhereClause="AllRecords.Argument",DataType="System.String",IsForeignColumn=false,PropertyName="Argument",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RecipientId",new SearchColumn(){Name="RecipientId",Title="RecipientId",SelectClause="RecipientId",WhereClause="AllRecords.RecipientId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="RecipientId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SenderId",new SearchColumn(){Name="SenderId",Title="SenderId",SelectClause="SenderId",WhereClause="AllRecords.SenderId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SenderId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsRead",new SearchColumn(){Name="IsRead",Title="IsRead",SelectClause="IsRead",WhereClause="AllRecords.IsRead",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsRead",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("slotid",new SearchColumn(){Name="slotid",Title="slotid",SelectClause="slotid",WhereClause="AllRecords.slotid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Slotid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("slotscreentemplateid",new SearchColumn(){Name="slotscreentemplateid",Title="slotscreentemplateid",SelectClause="slotscreentemplateid",WhereClause="AllRecords.slotscreentemplateid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Slotscreentemplateid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNotificationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNotificationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNotificationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNotificationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Notification].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Notification].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Notification> GetNotificationByNotificationTypeId(System.Int32 NotificationTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNotificationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Notification] with (nolock)  where NotificationTypeId=@NotificationTypeId  ";
			SqlParameter parameter=new SqlParameter("@NotificationTypeId",NotificationTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Notification>(ds,NotificationFromDataRow);
		}

		public virtual Notification GetNotification(System.Int32 NotificationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNotificationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Notification] with (nolock)  where NotificationId=@NotificationId ";
			SqlParameter parameter=new SqlParameter("@NotificationId",NotificationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NotificationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Notification> GetNotificationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNotificationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Notification] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Notification>(ds,NotificationFromDataRow);
		}

		public virtual List<Notification> GetAllNotification(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNotificationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Notification] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Notification>(ds, NotificationFromDataRow);
		}

		public virtual List<Notification> GetPagedNotification(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNotificationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NotificationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NotificationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NotificationId] ";
            tempsql += " FROM [Notification] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NotificationId"))
					tempsql += " , (AllRecords.[NotificationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NotificationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNotificationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Notification] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Notification].[NotificationId] = PageIndex.[NotificationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Notification>(ds, NotificationFromDataRow);
			}else{ return null;}
		}

		private int GetNotificationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Notification as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Notification as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Notification))]
		public virtual Notification InsertNotification(Notification entity)
		{

			Notification other=new Notification();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Notification ( [NotificationTypeId]
				,[Title]
				,[Argument]
				,[RecipientId]
				,[SenderId]
				,[IsRead]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[slotid]
				,[slotscreentemplateid] )
				Values
				( @NotificationTypeId
				, @Title
				, @Argument
				, @RecipientId
				, @SenderId
				, @IsRead
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @slotid
				, @slotscreentemplateid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NotificationTypeId",entity.NotificationTypeId)
					, new SqlParameter("@Title",entity.Title)
					, new SqlParameter("@Argument",entity.Argument ?? (object)DBNull.Value)
					, new SqlParameter("@RecipientId",entity.RecipientId ?? (object)DBNull.Value)
					, new SqlParameter("@SenderId",entity.SenderId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRead",entity.IsRead ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@slotid",entity.Slotid ?? (object)DBNull.Value)
					, new SqlParameter("@slotscreentemplateid",entity.Slotscreentemplateid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNotification(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Notification))]
		public virtual Notification UpdateNotification(Notification entity)
		{

			if (entity.IsTransient()) return entity;
			Notification other = GetNotification(entity.NotificationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Notification set  [NotificationTypeId]=@NotificationTypeId
							, [Title]=@Title
							, [Argument]=@Argument
							, [RecipientId]=@RecipientId
							, [SenderId]=@SenderId
							, [IsRead]=@IsRead
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [slotid]=@slotid
							, [slotscreentemplateid]=@slotscreentemplateid 
							 where NotificationId=@NotificationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NotificationTypeId",entity.NotificationTypeId)
					, new SqlParameter("@Title",entity.Title)
					, new SqlParameter("@Argument",entity.Argument ?? (object)DBNull.Value)
					, new SqlParameter("@RecipientId",entity.RecipientId ?? (object)DBNull.Value)
					, new SqlParameter("@SenderId",entity.SenderId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRead",entity.IsRead ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@slotid",entity.Slotid ?? (object)DBNull.Value)
					, new SqlParameter("@slotscreentemplateid",entity.Slotscreentemplateid ?? (object)DBNull.Value)
					, new SqlParameter("@NotificationId",entity.NotificationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNotification(entity.NotificationId);
		}

		public virtual bool DeleteNotification(System.Int32 NotificationId)
		{

			string sql="delete from Notification where NotificationId=@NotificationId";
			SqlParameter parameter=new SqlParameter("@NotificationId",NotificationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Notification))]
		public virtual Notification DeleteNotification(Notification entity)
		{
			this.DeleteNotification(entity.NotificationId);
			return entity;
		}


		public virtual Notification NotificationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Notification entity=new Notification();
			if (dr.Table.Columns.Contains("NotificationId"))
			{
			entity.NotificationId = (System.Int32)dr["NotificationId"];
			}
			if (dr.Table.Columns.Contains("NotificationTypeId"))
			{
			entity.NotificationTypeId = (System.Int32)dr["NotificationTypeId"];
			}
			if (dr.Table.Columns.Contains("Title"))
			{
			entity.Title = dr["Title"].ToString();
			}
			if (dr.Table.Columns.Contains("Argument"))
			{
			entity.Argument = dr["Argument"].ToString();
			}
			if (dr.Table.Columns.Contains("RecipientId"))
			{
			entity.RecipientId = dr["RecipientId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["RecipientId"];
			}
			if (dr.Table.Columns.Contains("SenderId"))
			{
			entity.SenderId = dr["SenderId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SenderId"];
			}
			if (dr.Table.Columns.Contains("IsRead"))
			{
			entity.IsRead = dr["IsRead"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsRead"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("slotid"))
			{
			entity.Slotid = dr["slotid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["slotid"];
			}
			if (dr.Table.Columns.Contains("slotscreentemplateid"))
			{
			entity.Slotscreentemplateid = dr["slotscreentemplateid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["slotscreentemplateid"];
			}
			return entity;
		}

	}
	
	
}
