﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
    public partial class NotificationRepository : NotificationRepositoryBase, INotificationRepository
    {
        public List<Notification> GetNotifications(int userId, DateTime? lastUpdateDate = null, int rowCount = 10)
        {
            string sql = string.Format("select top ({0}) * from [Notification] with (nolock) where RecipientId = @UserId and CreationDate > @Date order by creationdate desc", rowCount.ToString());

            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@UserId", userId),
                new SqlParameter("@Date", lastUpdateDate ?? DateTime.UtcNow.AddDays(-1))
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Notification>(ds, NotificationFromDataRow);
        }

        public bool MarkAsRead(System.Int32 NotificationId)
        {
            string sql = "update Notification set IsRead = 1, LastUpdateDate = GETUTCDATE() where NotificationId=@NotificationId";
            SqlParameter parameter = new SqlParameter("@NotificationId", NotificationId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) > 0;
        }

        public bool MarkAllAsRead(int userId)
        {
            string sql = "update Notification set IsRead = 1, LastUpdateDate = GETUTCDATE() where RecipientId=@UserId and IsRead = 0";
            SqlParameter parameter = new SqlParameter("@UserId", userId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) > 0;
        }


        public bool DeleteByJsonSlotId(int slotId)
        {
            string sql = "delete from Notification where CONVERT(int,SUBSTRING(argument,11,CONVERT(int,CHARINDEX(',',argument)-11))) = @slotid";
            SqlParameter parameter = new SqlParameter("@slotid", slotId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) > 0;
        }

        public bool DeleteBySlotId(int slotId)
        {
            string sql = "delete from Notification where slotid = @slotId";
            SqlParameter parameter = new SqlParameter("@slotid", slotId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) > 0;
        }

        public bool DeleteBySlotScreenTemplateId(int Slotscreentemplateid)
        {
            string sql = "delete from Notification where slotscreentemplateid = @Slotscreentemplateid";
            SqlParameter parameter = new SqlParameter("@Slotscreentemplateid", Slotscreentemplateid);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) > 0;
        }
    }
}
