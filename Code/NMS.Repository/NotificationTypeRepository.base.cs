﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class NotificationTypeRepositoryBase : Repository, INotificationTypeRepositoryBase 
	{
        
        public NotificationTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("NotificationTypeId",new SearchColumn(){Name="NotificationTypeId",Title="NotificationTypeId",SelectClause="NotificationTypeId",WhereClause="AllRecords.NotificationTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="NotificationTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Template",new SearchColumn(){Name="Template",Title="Template",SelectClause="Template",WhereClause="AllRecords.Template",DataType="System.String",IsForeignColumn=false,PropertyName="Template",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Argument",new SearchColumn(){Name="Argument",Title="Argument",SelectClause="Argument",WhereClause="AllRecords.Argument",DataType="System.String",IsForeignColumn=false,PropertyName="Argument",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetNotificationTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetNotificationTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetNotificationTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetNotificationTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[NotificationType].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[NotificationType].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual NotificationType GetNotificationType(System.Int32 NotificationTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNotificationTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NotificationType] with (nolock)  where NotificationTypeId=@NotificationTypeId ";
			SqlParameter parameter=new SqlParameter("@NotificationTypeId",NotificationTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return NotificationTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<NotificationType> GetNotificationTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNotificationTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [NotificationType] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NotificationType>(ds,NotificationTypeFromDataRow);
		}

		public virtual List<NotificationType> GetAllNotificationType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetNotificationTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [NotificationType] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NotificationType>(ds, NotificationTypeFromDataRow);
		}

		public virtual List<NotificationType> GetPagedNotificationType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetNotificationTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [NotificationTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([NotificationTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [NotificationTypeId] ";
            tempsql += " FROM [NotificationType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("NotificationTypeId"))
					tempsql += " , (AllRecords.[NotificationTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[NotificationTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetNotificationTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [NotificationType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [NotificationType].[NotificationTypeId] = PageIndex.[NotificationTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<NotificationType>(ds, NotificationTypeFromDataRow);
			}else{ return null;}
		}

		private int GetNotificationTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM NotificationType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM NotificationType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(NotificationType))]
		public virtual NotificationType InsertNotificationType(NotificationType entity)
		{

			NotificationType other=new NotificationType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into NotificationType ( [Name]
				,[Template]
				,[Argument] )
				Values
				( @Name
				, @Template
				, @Argument );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Template",entity.Template ?? (object)DBNull.Value)
					, new SqlParameter("@Argument",entity.Argument ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetNotificationType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(NotificationType))]
		public virtual NotificationType UpdateNotificationType(NotificationType entity)
		{

			if (entity.IsTransient()) return entity;
			NotificationType other = GetNotificationType(entity.NotificationTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update NotificationType set  [Name]=@Name
							, [Template]=@Template
							, [Argument]=@Argument 
							 where NotificationTypeId=@NotificationTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@Template",entity.Template ?? (object)DBNull.Value)
					, new SqlParameter("@Argument",entity.Argument ?? (object)DBNull.Value)
					, new SqlParameter("@NotificationTypeId",entity.NotificationTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetNotificationType(entity.NotificationTypeId);
		}

		public virtual bool DeleteNotificationType(System.Int32 NotificationTypeId)
		{

			string sql="delete from NotificationType where NotificationTypeId=@NotificationTypeId";
			SqlParameter parameter=new SqlParameter("@NotificationTypeId",NotificationTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(NotificationType))]
		public virtual NotificationType DeleteNotificationType(NotificationType entity)
		{
			this.DeleteNotificationType(entity.NotificationTypeId);
			return entity;
		}


		public virtual NotificationType NotificationTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			NotificationType entity=new NotificationType();
			if (dr.Table.Columns.Contains("NotificationTypeId"))
			{
			entity.NotificationTypeId = (System.Int32)dr["NotificationTypeId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("Template"))
			{
			entity.Template = dr["Template"].ToString();
			}
			if (dr.Table.Columns.Contains("Argument"))
			{
			entity.Argument = dr["Argument"].ToString();
			}
			return entity;
		}

	}
	
	
}
