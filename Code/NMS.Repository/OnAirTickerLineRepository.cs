﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Enums;

namespace NMS.Repository
{

    public partial class OnAirTickerLineRepository : OnAirTickerLineRepositoryBase, IOnAirTickerLineRepository
    {
        public List<OnAirTickerLine> SubmitOnAirTickerLineToMcrBreaking(OnAirTickerLine OnAirTickerLine)
        {
            string sql = @"IF EXISTS (select * from OnAirTickerLine where TickerId =  @OnAirTickerId and  IsShow = 1)
                               Begin
                               insert into [MCRBreakingTickerRundown] 
                                            (TickerId, TickerLineId, [Text], LanguageCode, CreationDate, SequenceNumber)
                                        select 
                                            b.TickerId,0, b.TickerGroupName,'ur', GetUTCDate(),0
                                        from [OnAirTicker] b where b.TickerId = @OnAirTickerId and b.TickerGroupName != N' ' order by b.SequenceId;

                                        insert into [MCRBreakingTickerRundown] 
                                            (TickerId, TickerLineId, [Text], LanguageCode, CreationDate, SequenceNumber)
                                        select 
                                            b.TickerId, b.TickerLineId, b.[Text], b.LanguageCode, GetUTCDate(), b.OperatorNumber
                                        from [OnAirTickerLine] b where b.IsShow = 1 and b.TickerId = @OnAirTickerId order by b.SequenceId;
		                                
                                        select * from OnAirTickerLine where IsShow = 1 and TickerId = @OnAirTickerId  order by SequenceId;
                                END
                            ELSE
                               Begin 
                                select * from OnAirTicker where TickerId  = @OnAirTickerId
                               END";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerId", OnAirTickerLine.TickerId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTickerLine>(ds, OnAirTickerLineFromDataRow);
        }

        public List<OnAirTickerLine> SubmitOnAirTickerLineToMcrLatest(OnAirTickerLine OnAirTickerLine)
        {
            string sql = @"IF EXISTS (select * from OnAirTickerLine where TickerId =  @OnAirTickerId and  IsShow = 1 )
                            Begin
                                insert into [MCRLatestTickerRundown] (TickerId, TickerLineId, [Text], LanguageCode, CreationDate, SequenceNumber)
                                select 
                                    b.TickerId, b.TickerLineId, b.[Text], b.LanguageCode, GetUTCDate(), b.OperatorNumber
                                from [OnAirTickerLine] b where b.IsShow = 1 and b.TickerId = @OnAirTickerId order by b.SequenceId;

						      
                                select * from OnAirTickerLine where IsShow = 1 and TickerId = @OnAirTickerId  order by SequenceId;
                            END
                           ELSE
                              Begin 
                                select * from OnAirTicker where TickerId  = @OnAirTickerId
                              END";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerId", OnAirTickerLine.TickerId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTickerLine>(ds, OnAirTickerLineFromDataRow);

        }


        public List<OnAirTickerLine> SubmitOnAirTickerLineToMcrCategory(OnAirTickerLine temp)
        {

            string sql = @"IF EXISTS (select * from OnAirTickerLine where TickerId =  @OnAirTickerId and IsShow = 1)
                           Begin
                                insert into [MCRCategoryTickerRundown] (TickerId, TickerLineId, [Text],CategoryName,CategoryId, LanguageCode, CreationDate, SequenceNumber)
                                select 
                                    b.TickerId,0, b.TickerGroupName,b.TickerGroupName,1,'ur', GetUTCDate(),0
                                from [OnAirTicker] b where b.TickerId = @OnAirTickerId order by b.SequenceId;

                                insert into [MCRCategoryTickerRundown] 
                                    (TickerId, TickerLineId, [Text],CategoryName,CategoryId, LanguageCode, CreationDate, SequenceNumber)
                                select 
                                    b.TickerId, b.TickerLineId, b.[Text],a.TickerGroupName,1, b.LanguageCode, GetUTCDate(), b.OperatorNumber
                                from [OnAirTickerLine] b inner join OnAirTicker a on  a.TickerId = b.TickerId  where b.IsShow = 1 and b.TickerId = @OnAirTickerId order by b.SequenceId;

						     
                                select * from OnAirTickerLine where IsShow = 1 and TickerId = @OnAirTickerId  order by SequenceId;
                                END
                            ELSE
                              Begin 
                                select * from OnAirTicker where TickerId  = @OnAirTickerId
                              END";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerId", temp.TickerId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTickerLine>(ds, OnAirTickerLineFromDataRow);

        }
        public bool DeleteOnAirTickerLineFromMCRBreaking(OnAirTickerLine tempObj)
        {
            string sql = @"delete from MCRBreakingTickerRundown where TickerLineId = @TickerLineId;
                           IF NOT EXISTS (select * from MCRBreakingTickerRundown with(NOLOCK) where TickerId = @TickerId and TickerLineId <> 0)
	                        Begin
	                            delete from MCRBreakingTickerRundown where TickerLineId = 0 and TickerId =  @TickerId
	                        END
                          ELSE
	                        Begin
							  select * from  MCRBreakingTickerRundown where TickerId =  @TickerId
	                        END";
            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@TickerId", tempObj.TickerId),
                new SqlParameter("@TickerLineId", tempObj.TickerLineId)
            };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
        public bool DeleteOnAirTickerLineFromMCRLatest(OnAirTickerLine tempObj)
        {

            string sql = @"delete from MCRLatestTickerRundown where TickerLineId = @TickerLineId;
                           IF NOT EXISTS (select * from MCRLatestTickerRundown with(NOLOCK) where TickerId = @TickerId and TickerLineId <> 0)
	                        Begin
	                            delete from MCRLatestTickerRundown where TickerLineId = 0 and TickerId =  @TickerId
	                        END
                          ELSE
	                        Begin
							  select * from  MCRLatestTickerRundown where TickerId =  @TickerId
	                        END";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@TickerId", tempObj.TickerId),
                new SqlParameter("@TickerLineId", tempObj.TickerLineId)
            };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public bool DeleteOnAirTickerLineFromMCRCategory(OnAirTickerLine tempObj)
        {

            string sql = @"delete from MCRCategoryTickerRundown where TickerLineId = @TickerLineId;
                           IF NOT EXISTS (select * from MCRCategoryTickerRundown with(NOLOCK) where TickerId = @TickerId and TickerLineId <> 0)
	                        Begin
	                            delete from MCRCategoryTickerRundown where TickerLineId = 0 and TickerId =  @TickerId
	                        END
                          ELSE
	                        Begin
							  select * from  MCRCategoryTickerRundown where TickerId =  @TickerId
	                        END";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@TickerId", tempObj.TickerId),
                new SqlParameter("@TickerLineId", tempObj.TickerLineId)
            };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }


        public bool MigrateDataToOnAiredTable(OnAirTickerLine tempObj, int type)
        {
            string sql = @"Declare @TickerId int;
                           IF EXISTS (select * from Ticker with(NOLOCK) where TickerGroupName  =(select TickerGroupName from OnAirTicker where TickerId = @OnAirTickerId))
	                            Begin

	                            set  @TickerId  =  (select TickerId from Ticker with(NOLOCK) where TickerGroupName  = (select TickerGroupName from OnAirTicker where TickerId = @OnAirTickerId))
	                            Insert into TickerLine ([Text],LanguageCode,TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId)
	                            select [Text],LanguageCode,@TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId from OnAirTickerLine with(NOLOCK)  where TickerLineId =  @OnAirTickerLineId

	                            END
                            ELSE
	                            Begin

	                            insert into Ticker (CategoryId,NewsGuid,UserId,OnAiredTime,SequenceId,CreationDate,LastUpdatedDate,IsActive,CreatedBy,TickerGroupName,LocationId,OnAirRefId)
	                            select CategoryId,NewsGuid,UserId,OnAiredTime,SequenceId,CreationDate,LastUpdatedDate,IsActive,CreatedBy,TickerGroupName,LocationId,@OnAirTickerId from OnAirTicker with(NOLOCK) where TickerId =  @OnAirTickerId 
	                            set @TickerId  =  SCOPE_IDENTITY()
	                            Insert into TickerLine ([Text],LanguageCode,TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId)
	                            select [Text],LanguageCode,@TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId from OnAirTickerLine with(NOLOCK)  where TickerLineId =  @OnAirTickerLineId


	                            END

	                            Delete from OnAirTickerLine where TickerLineId = @OnAirTickerLineId;";
                                

            if (type == (int)TickerTypes.Breaking)
                sql += "Delete from MCRBreakingTickerRundown where TickerLineId = @OnAirTickerLineId";
            else if (type == (int)TickerTypes.Latest)
                sql += "Delete from MCRLatestTickerRundown where TickerLineId = @OnAirTickerLineId";
            else if (type == (int)TickerTypes.Category)
                sql += "Delete from MCRCategoryTickerRundown where TickerLineId = @OnAirTickerLineId";


            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerId", tempObj.TickerId),
                new SqlParameter("@OnAirTickerLineId", tempObj.TickerLineId)
            };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) > 0 ? true : false;
        }

        public bool MigrateGroupToOnAiredTable(OnAirTickerLine tempObj, int type)
        {
            string sql = @"Declare @TickerId int;
                           IF EXISTS (select * from Ticker with(NOLOCK) where TickerGroupName  =(select TickerGroupName from OnAirTicker where TickerId = @OnAirTickerId))
	                            Begin

	                            set  @TickerId  =  (select TickerId from Ticker with(NOLOCK) where TickerGroupName  = (select TickerGroupName from OnAirTicker where TickerId = @OnAirTickerId))
	                            Insert into TickerLine ([Text],LanguageCode,TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId)
	                            select [Text],LanguageCode,@TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId from OnAirTickerLine with(NOLOCK) where TickerId =  @OnAirTickerId
	                            END
                            ELSE
	                            Begin

	                            insert into Ticker (CategoryId,NewsGuid,UserId,OnAiredTime,SequenceId,CreationDate,LastUpdatedDate,IsActive,CreatedBy,TickerGroupName,LocationId,OnAirRefId)
	                            select CategoryId,NewsGuid,UserId,OnAiredTime,SequenceId,CreationDate,LastUpdatedDate,IsActive,CreatedBy,TickerGroupName,LocationId,@OnAirTickerId  from OnAirTicker with(NOLOCK) where TickerId =  @OnAirTickerId 
	                            set @TickerId  =  SCOPE_IDENTITY()
	                            Insert into TickerLine ([Text],LanguageCode,TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId)
	                            select [Text],LanguageCode,@TickerId,CreationDate,LastUpdatedDate,IsActive,SequenceId from OnAirTickerLine with(NOLOCK)  where TickerId =  @OnAirTickerId


	                            END

	                            Delete from OnAirTickerLine where TickerId = @OnAirTickerId
	                            Delete from OnAirTicker where TickerId = @OnAirTickerId;";

            if (type == (int)TickerTypes.Breaking)
                sql += "Delete from MCRBreakingTickerRundown where TickerId = @OnAirTickerId";
            else if (type == (int)TickerTypes.Latest)
                sql += "Delete from MCRLatestTickerRundown where TickerId = @OnAirTickerId";
            else if (type == (int)TickerTypes.Category)
                sql += "Delete from MCRCategoryTickerRundown where TickerId = @OnAirTickerId";


            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerId", tempObj.TickerId)
            };
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) > 0 ? true : false;
        }



        public OnAirTickerLine GetTickerLineBySeqNumAndTypeAndOnAirTickerId(int operatorNo, int tickerId, int type)
        {
            string sql = @"select * from OnAirTickerLine where TickerId =  @OnAirTickerId and OperatorNumber = @OperatorNumber;";
            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerId", tickerId),
                new SqlParameter("@OperatorNumber", operatorNo)
                
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTickerLine>(ds, OnAirTickerLineFromDataRow)[0];
        }


        public OnAirTickerLine UpdateOnAirTickerLineSequenceNumber(OnAirTickerLine item)
        {
            //, LastUpdatedDate = @LastUpdateDate 
              //new SqlParameter("@LastUpdateDate", DateTime.UtcNow)
            string sql = @"update OnAirTickerLine set SequenceId = @seqId  where TickerLineId =  @OnAirTickerLineId and TickerId =@OnAirTickerId ; select * from OnAirTickerLine where TickerLineId =  @OnAirTickerLineId and TickerId = @OnAirTickerId;";
            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerLineId", item.TickerLineId),
                new SqlParameter("@seqId", item.SequenceId),
                new SqlParameter("@OnAirTickerId", item.TickerId)
               
                
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTickerLine>(ds, OnAirTickerLineFromDataRow)[0];
        }


        public List<OnAirTickerLine> GetLatestOnAirTickerLine(DateTime dateTime)
        {
            string sql = @"select tl.*,t.TickerGroupName,t.SequenceId As TickerSequenceId from OnAirTickerLine tl 
                          inner join OnAirTicker t  on t.TickerId =  tl.TickerId where tl.LastUpdatedDate >  @LastUpdatedDate ";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@LastUpdatedDate", dateTime)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTickerLine>(ds, OnAirTickerLineFromDataRow);
        }
    }


}
