﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class OnAirTickerRepository : OnAirTickerRepositoryBase, IOnAirTickerRepository
    {

        public OnAirTicker UpdateOnAirTickerSequenceNumber(OnAirTicker item)
        {
            string sql = @"update OnAirTicker set SequenceId = @seqId ,  LastUpdatedDate = @LastUpdateDate where TickerId =  @OnAirTickerId; select * from OnAirTicker where TickerId =  @OnAirTickerId;";
            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@OnAirTickerId", item.TickerId),
                new SqlParameter("@seqId", item.SequenceId),
                new SqlParameter("@LastUpdateDate", DateTime.UtcNow),
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTicker>(ds, OnAirTickerFromDataRow)[0];
        }



        public Dictionary<string, int> GetSequenceNumbers()
        {
            Dictionary<string, int> sequenceNumber = new Dictionary<string, int>();
            string sql = string.Format(@"select max(SequenceId) as onAirSeqId from OnAirTicker");

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                sequenceNumber["onAirSeqId"] = ds.Tables[0].Rows[0][0] == DBNull.Value ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            return sequenceNumber;
        }


        public List<OnAirTicker> GetLatestOnAirTicker(DateTime dateTime)
        {
            string sql = @"select * from OnAirTicker where LastUpdatedDate > @LastUpdatedDate ";
            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@LastUpdatedDate", dateTime)
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<OnAirTicker>(ds, OnAirTickerFromDataRow);
        }
    }


}
