﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class OrganizationRepository : OrganizationRepositoryBase, IOrganizationRepository
    {
        public List<Organization> GetOrganizationByTerm(string term)
        {
            string sql = GetOrganizationSelectClause();
            sql += "from Organization with (nolock) where Name like '%'+@Name+'%' and ISNULL(IsActive,0) = 1";
            SqlParameter parameter = new SqlParameter("@Name", term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Organization>(ds, OrganizationFromDataRow);
        }

        public Organization GetOrganizationByName(string Name)
        {
            string sql = GetOrganizationSelectClause();
            sql += "from Organization with (nolock) where Name=@Name and IsActive = 1";
            SqlParameter parameter = new SqlParameter("@Name", Name);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return OrganizationFromDataRow(ds.Tables[0].Rows[0]);
        }

    }
	
	
}
