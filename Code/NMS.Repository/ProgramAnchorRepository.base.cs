﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramAnchorRepositoryBase : Repository, IProgramAnchorRepositoryBase 
	{
        
        public ProgramAnchorRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramAnchorId",new SearchColumn(){Name="ProgramAnchorId",Title="ProgramAnchorId",SelectClause="ProgramAnchorId",WhereClause="AllRecords.ProgramAnchorId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramAnchorId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramAnchorSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramAnchorBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramAnchorAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramAnchorSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramAnchor].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramAnchor].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ProgramAnchor> GetProgramAnchorByProgramId(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramAnchorSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramAnchor] with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramAnchor>(ds,ProgramAnchorFromDataRow);
		}

		public virtual List<ProgramAnchor> GetProgramAnchorByCelebrityId(System.Int32 CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramAnchorSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramAnchor] with (nolock)  where CelebrityId=@CelebrityId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramAnchor>(ds,ProgramAnchorFromDataRow);
		}

		public virtual ProgramAnchor GetProgramAnchor(System.Int32 ProgramAnchorId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramAnchorSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramAnchor] with (nolock)  where ProgramAnchorId=@ProgramAnchorId ";
			SqlParameter parameter=new SqlParameter("@ProgramAnchorId",ProgramAnchorId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramAnchorFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramAnchor> GetProgramAnchorByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramAnchorSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ProgramAnchor] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramAnchor>(ds,ProgramAnchorFromDataRow);
		}

		public virtual List<ProgramAnchor> GetAllProgramAnchor(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramAnchorSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramAnchor] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramAnchor>(ds, ProgramAnchorFromDataRow);
		}

		public virtual List<ProgramAnchor> GetPagedProgramAnchor(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramAnchorCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramAnchorId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramAnchorId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramAnchorId] ";
            tempsql += " FROM [ProgramAnchor] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramAnchorId"))
					tempsql += " , (AllRecords.[ProgramAnchorId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramAnchorId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramAnchorSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramAnchor] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramAnchor].[ProgramAnchorId] = PageIndex.[ProgramAnchorId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramAnchor>(ds, ProgramAnchorFromDataRow);
			}else{ return null;}
		}

		private int GetProgramAnchorCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramAnchor as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramAnchor as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramAnchor))]
		public virtual ProgramAnchor InsertProgramAnchor(ProgramAnchor entity)
		{

			ProgramAnchor other=new ProgramAnchor();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramAnchor ( [ProgramId]
				,[CelebrityId]
				,[IsActive]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @ProgramId
				, @CelebrityId
				, @IsActive
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@CelebrityId",entity.CelebrityId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramAnchor(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramAnchor))]
		public virtual ProgramAnchor UpdateProgramAnchor(ProgramAnchor entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramAnchor other = GetProgramAnchor(entity.ProgramAnchorId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramAnchor set  [ProgramId]=@ProgramId
							, [CelebrityId]=@CelebrityId
							, [IsActive]=@IsActive
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where ProgramAnchorId=@ProgramAnchorId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@CelebrityId",entity.CelebrityId)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramAnchorId",entity.ProgramAnchorId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramAnchor(entity.ProgramAnchorId);
		}

		public virtual bool DeleteProgramAnchor(System.Int32 ProgramAnchorId)
		{

			string sql="delete from ProgramAnchor where ProgramAnchorId=@ProgramAnchorId";
			SqlParameter parameter=new SqlParameter("@ProgramAnchorId",ProgramAnchorId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramAnchor))]
		public virtual ProgramAnchor DeleteProgramAnchor(ProgramAnchor entity)
		{
			this.DeleteProgramAnchor(entity.ProgramAnchorId);
			return entity;
		}


		public virtual ProgramAnchor ProgramAnchorFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramAnchor entity=new ProgramAnchor();
			if (dr.Table.Columns.Contains("ProgramAnchorId"))
			{
			entity.ProgramAnchorId = (System.Int32)dr["ProgramAnchorId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = (System.Int32)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
