﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramConfigurationRepositoryBase : Repository, IProgramConfigurationRepositoryBase 
	{
        
        public ProgramConfigurationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramConfigurationId",new SearchColumn(){Name="ProgramConfigurationId",Title="ProgramConfigurationId",SelectClause="ProgramConfigurationId",WhereClause="AllRecords.ProgramConfigurationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramConfigurationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SegmentTypeId",new SearchColumn(){Name="SegmentTypeId",Title="SegmentTypeId",SelectClause="SegmentTypeId",WhereClause="AllRecords.SegmentTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SegmentTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Url",new SearchColumn(){Name="Url",Title="Url",SelectClause="Url",WhereClause="AllRecords.Url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramConfigurationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramConfigurationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramConfigurationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramConfigurationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramConfiguration].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramConfiguration].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ProgramConfiguration> GetProgramConfigurationByProgramId(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramConfigurationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramConfiguration] with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramConfiguration>(ds,ProgramConfigurationFromDataRow);
		}

		public virtual List<ProgramConfiguration> GetProgramConfigurationBySegmentTypeId(System.Int32 SegmentTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramConfigurationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramConfiguration] with (nolock)  where SegmentTypeId=@SegmentTypeId  ";
			SqlParameter parameter=new SqlParameter("@SegmentTypeId",SegmentTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramConfiguration>(ds,ProgramConfigurationFromDataRow);
		}

		public virtual ProgramConfiguration GetProgramConfiguration(System.Int32 ProgramConfigurationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramConfigurationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramConfiguration] with (nolock)  where ProgramConfigurationId=@ProgramConfigurationId ";
			SqlParameter parameter=new SqlParameter("@ProgramConfigurationId",ProgramConfigurationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramConfigurationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramConfiguration> GetProgramConfigurationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramConfigurationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ProgramConfiguration] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramConfiguration>(ds,ProgramConfigurationFromDataRow);
		}

		public virtual List<ProgramConfiguration> GetAllProgramConfiguration(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramConfigurationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramConfiguration] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramConfiguration>(ds, ProgramConfigurationFromDataRow);
		}

		public virtual List<ProgramConfiguration> GetPagedProgramConfiguration(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramConfigurationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramConfigurationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramConfigurationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramConfigurationId] ";
            tempsql += " FROM [ProgramConfiguration] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramConfigurationId"))
					tempsql += " , (AllRecords.[ProgramConfigurationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramConfigurationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramConfigurationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramConfiguration] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramConfiguration].[ProgramConfigurationId] = PageIndex.[ProgramConfigurationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramConfiguration>(ds, ProgramConfigurationFromDataRow);
			}else{ return null;}
		}

		private int GetProgramConfigurationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramConfiguration as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramConfiguration as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramConfiguration))]
		public virtual ProgramConfiguration InsertProgramConfiguration(ProgramConfiguration entity)
		{

			ProgramConfiguration other=new ProgramConfiguration();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramConfiguration ( [ProgramId]
				,[SegmentTypeId]
				,[Url] )
				Values
				( @ProgramId
				, @SegmentTypeId
				, @Url );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@SegmentTypeId",entity.SegmentTypeId)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramConfiguration(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramConfiguration))]
		public virtual ProgramConfiguration UpdateProgramConfiguration(ProgramConfiguration entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramConfiguration other = GetProgramConfiguration(entity.ProgramConfigurationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramConfiguration set  [ProgramId]=@ProgramId
							, [SegmentTypeId]=@SegmentTypeId
							, [Url]=@Url 
							 where ProgramConfigurationId=@ProgramConfigurationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@SegmentTypeId",entity.SegmentTypeId)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramConfigurationId",entity.ProgramConfigurationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramConfiguration(entity.ProgramConfigurationId);
		}

		public virtual bool DeleteProgramConfiguration(System.Int32 ProgramConfigurationId)
		{

			string sql="delete from ProgramConfiguration where ProgramConfigurationId=@ProgramConfigurationId";
			SqlParameter parameter=new SqlParameter("@ProgramConfigurationId",ProgramConfigurationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramConfiguration))]
		public virtual ProgramConfiguration DeleteProgramConfiguration(ProgramConfiguration entity)
		{
			this.DeleteProgramConfiguration(entity.ProgramConfigurationId);
			return entity;
		}


		public virtual ProgramConfiguration ProgramConfigurationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramConfiguration entity=new ProgramConfiguration();
			if (dr.Table.Columns.Contains("ProgramConfigurationId"))
			{
			entity.ProgramConfigurationId = (System.Int32)dr["ProgramConfigurationId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("SegmentTypeId"))
			{
			entity.SegmentTypeId = (System.Int32)dr["SegmentTypeId"];
			}
			if (dr.Table.Columns.Contains("Url"))
			{
			entity.Url = dr["Url"].ToString();
			}
			return entity;
		}

	}
	
	
}
