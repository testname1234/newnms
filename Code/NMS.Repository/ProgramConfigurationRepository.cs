﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class ProgramConfigurationRepository: ProgramConfigurationRepositoryBase, IProgramConfigurationRepository
	{

        public bool DeleteProgramConfigurationByProgramId(ProgramConfiguration entity)
        {

            string sql = "delete from ProgramConfiguration where ProgramId=@ProgramId and SegmentTypeId=@SegmentTypeId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@SegmentTypeId",entity.SegmentTypeId)};
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
	}
	
	
}
