﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramRecordingRepositoryBase : Repository, IProgramRecordingRepositoryBase 
	{
        
        public ProgramRecordingRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramRecordingId",new SearchColumn(){Name="ProgramRecordingId",Title="ProgramRecordingId",SelectClause="ProgramRecordingId",WhereClause="AllRecords.ProgramRecordingId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramRecordingId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Path",new SearchColumn(){Name="Path",Title="Path",SelectClause="Path",WhereClause="AllRecords.Path",DataType="System.String",IsForeignColumn=false,PropertyName="Path",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Status",new SearchColumn(){Name="Status",Title="Status",SelectClause="Status",WhereClause="AllRecords.Status",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Status",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("From",new SearchColumn(){Name="From",Title="From",SelectClause="From",WhereClause="AllRecords.From",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="From",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("To",new SearchColumn(){Name="To",Title="To",SelectClause="To",WhereClause="AllRecords.To",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="To",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramRecordingSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramRecordingBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramRecordingAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramRecordingSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramRecording].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramRecording].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ProgramRecording> GetProgramRecordingByProgramId(System.Int32? ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramRecordingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramRecording] with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramRecording>(ds,ProgramRecordingFromDataRow);
		}

		public virtual ProgramRecording GetProgramRecording(System.Int32 ProgramRecordingId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramRecordingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramRecording] with (nolock)  where ProgramRecordingId=@ProgramRecordingId ";
			SqlParameter parameter=new SqlParameter("@ProgramRecordingId",ProgramRecordingId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramRecordingFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramRecording> GetProgramRecordingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramRecordingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ProgramRecording] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramRecording>(ds,ProgramRecordingFromDataRow);
		}

		public virtual List<ProgramRecording> GetAllProgramRecording(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramRecordingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramRecording] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramRecording>(ds, ProgramRecordingFromDataRow);
		}

		public virtual List<ProgramRecording> GetPagedProgramRecording(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramRecordingCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramRecordingId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramRecordingId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramRecordingId] ";
            tempsql += " FROM [ProgramRecording] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramRecordingId"))
					tempsql += " , (AllRecords.[ProgramRecordingId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramRecordingId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramRecordingSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramRecording] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramRecording].[ProgramRecordingId] = PageIndex.[ProgramRecordingId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramRecording>(ds, ProgramRecordingFromDataRow);
			}else{ return null;}
		}

		private int GetProgramRecordingCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramRecording as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramRecording as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramRecording))]
		public virtual ProgramRecording InsertProgramRecording(ProgramRecording entity)
		{

			ProgramRecording other=new ProgramRecording();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramRecording ( [ProgramId]
				,[Path]
				,[Status]
				,[From]
				,[To]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @ProgramId
				, @Path
				, @Status
				, @From
				, @To
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@Path",entity.Path ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramRecording(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramRecording))]
		public virtual ProgramRecording UpdateProgramRecording(ProgramRecording entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramRecording other = GetProgramRecording(entity.ProgramRecordingId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramRecording set  [ProgramId]=@ProgramId
							, [Path]=@Path
							, [Status]=@Status
							, [From]=@From
							, [To]=@To
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where ProgramRecordingId=@ProgramRecordingId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@Path",entity.Path ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramRecordingId",entity.ProgramRecordingId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramRecording(entity.ProgramRecordingId);
		}

		public virtual bool DeleteProgramRecording(System.Int32 ProgramRecordingId)
		{

			string sql="delete from ProgramRecording where ProgramRecordingId=@ProgramRecordingId";
			SqlParameter parameter=new SqlParameter("@ProgramRecordingId",ProgramRecordingId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramRecording))]
		public virtual ProgramRecording DeleteProgramRecording(ProgramRecording entity)
		{
			this.DeleteProgramRecording(entity.ProgramRecordingId);
			return entity;
		}


		public virtual ProgramRecording ProgramRecordingFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramRecording entity=new ProgramRecording();
			if (dr.Table.Columns.Contains("ProgramRecordingId"))
			{
			entity.ProgramRecordingId = (System.Int32)dr["ProgramRecordingId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("Path"))
			{
			entity.Path = dr["Path"].ToString();
			}
			if (dr.Table.Columns.Contains("Status"))
			{
			entity.Status = dr["Status"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Status"];
			}
			if (dr.Table.Columns.Contains("From"))
			{
			entity.From = dr["From"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["From"];
			}
			if (dr.Table.Columns.Contains("To"))
			{
			entity.To = dr["To"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["To"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
