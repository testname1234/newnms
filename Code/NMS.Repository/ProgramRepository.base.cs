﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramRepositoryBase : Repository, IProgramRepositoryBase 
	{
        
        public ProgramRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsApproved",new SearchColumn(){Name="IsApproved",Title="IsApproved",SelectClause="IsApproved",WhereClause="AllRecords.IsApproved",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsApproved",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MinStoryCount",new SearchColumn(){Name="MinStoryCount",Title="MinStoryCount",SelectClause="MinStoryCount",WhereClause="AllRecords.MinStoryCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="MinStoryCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MaxStoryCount",new SearchColumn(){Name="MaxStoryCount",Title="MaxStoryCount",SelectClause="MaxStoryCount",WhereClause="AllRecords.MaxStoryCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="MaxStoryCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramTypeId",new SearchColumn(){Name="ProgramTypeId",Title="ProgramTypeId",SelectClause="ProgramTypeId",WhereClause="AllRecords.ProgramTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BucketID",new SearchColumn(){Name="BucketID",Title="BucketID",SelectClause="BucketID",WhereClause="AllRecords.BucketID",DataType="System.Int32?",IsForeignColumn=false,PropertyName="BucketId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FilterId",new SearchColumn(){Name="FilterId",Title="FilterId",SelectClause="FilterId",WhereClause="AllRecords.FilterId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FilterId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ApiKey",new SearchColumn(){Name="ApiKey",Title="ApiKey",SelectClause="ApiKey",WhereClause="AllRecords.ApiKey",DataType="System.String",IsForeignColumn=false,PropertyName="ApiKey",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Interval",new SearchColumn(){Name="Interval",Title="Interval",SelectClause="Interval",WhereClause="AllRecords.Interval",DataType="System.Int32",IsForeignColumn=false,PropertyName="Interval",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Program].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Program].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Program> GetProgramByChannelId(System.Int32? ChannelId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Program] with (nolock)  where ChannelId=@ChannelId  ";
			SqlParameter parameter=new SqlParameter("@ChannelId",ChannelId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Program>(ds,ProgramFromDataRow);
		}

		public virtual Program GetProgram(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Program] with (nolock)  where ProgramId=@ProgramId ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Program> GetProgramByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Program] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Program>(ds,ProgramFromDataRow);
		}

		public virtual List<Program> GetAllProgram(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Program] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Program>(ds, ProgramFromDataRow);
		}

		public virtual List<Program> GetPagedProgram(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramId] ";
            tempsql += " FROM [Program] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramId"))
					tempsql += " , (AllRecords.[ProgramId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Program] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Program].[ProgramId] = PageIndex.[ProgramId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Program>(ds, ProgramFromDataRow);
			}else{ return null;}
		}

		private int GetProgramCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Program as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Program as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Program))]
		public virtual Program InsertProgram(Program entity)
		{

			Program other=new Program();
			other = entity;
				string sql=@"Insert into Program ( [ProgramId]
				,[Name]
				,[ChannelId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsApproved]
				,[MinStoryCount]
				,[MaxStoryCount]
				,[ProgramTypeId]
				,[BucketID]
				,[FilterId]
				,[ApiKey]
				,[Interval] )
				Values
				( @ProgramId
				, @Name
				, @ChannelId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsApproved
				, @MinStoryCount
				, @MaxStoryCount
				, @ProgramTypeId
				, @BucketID
				, @FilterId
				, @ApiKey
				, @Interval );
";
				SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved)
					, new SqlParameter("@MinStoryCount",entity.MinStoryCount ?? (object)DBNull.Value)
					, new SqlParameter("@MaxStoryCount",entity.MaxStoryCount ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramTypeId",entity.ProgramTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@BucketID",entity.BucketId ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@ApiKey",entity.ApiKey ?? (object)DBNull.Value)
					, new SqlParameter("@Interval",entity.Interval)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgram(Convert.ToInt32(identity));
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Program))]
		public virtual Program UpdateProgram(Program entity)
		{

			Program other = GetProgram(entity.ProgramId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Program set  [Name]=@Name
							, [ChannelId]=@ChannelId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsApproved]=@IsApproved
							, [MinStoryCount]=@MinStoryCount
							, [MaxStoryCount]=@MaxStoryCount
							, [ProgramTypeId]=@ProgramTypeId
							, [BucketID]=@BucketID
							, [FilterId]=@FilterId
							, [ApiKey]=@ApiKey
							, [Interval]=@Interval 
							 where ProgramId=@ProgramId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved)
					, new SqlParameter("@MinStoryCount",entity.MinStoryCount ?? (object)DBNull.Value)
					, new SqlParameter("@MaxStoryCount",entity.MaxStoryCount ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramTypeId",entity.ProgramTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@BucketID",entity.BucketId ?? (object)DBNull.Value)
					, new SqlParameter("@FilterId",entity.FilterId ?? (object)DBNull.Value)
					, new SqlParameter("@ApiKey",entity.ApiKey ?? (object)DBNull.Value)
					, new SqlParameter("@Interval",entity.Interval)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgram(entity.ProgramId);
		}

		public virtual bool DeleteProgram(System.Int32 ProgramId)
		{

			string sql="delete from Program where ProgramId=@ProgramId";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Program))]
		public virtual Program DeleteProgram(Program entity)
		{
			this.DeleteProgram(entity.ProgramId);
			return entity;
		}


		public virtual Program ProgramFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Program entity=new Program();
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = dr["ChannelId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("IsApproved"))
			{
			entity.IsApproved = (System.Boolean)dr["IsApproved"];
			}
			if (dr.Table.Columns.Contains("MinStoryCount"))
			{
			entity.MinStoryCount = dr["MinStoryCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["MinStoryCount"];
			}
			if (dr.Table.Columns.Contains("MaxStoryCount"))
			{
			entity.MaxStoryCount = dr["MaxStoryCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["MaxStoryCount"];
			}
			if (dr.Table.Columns.Contains("ProgramTypeId"))
			{
			entity.ProgramTypeId = dr["ProgramTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramTypeId"];
			}
			if (dr.Table.Columns.Contains("BucketID"))
			{
			entity.BucketId = dr["BucketID"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["BucketID"];
			}
			if (dr.Table.Columns.Contains("FilterId"))
			{
			entity.FilterId = dr["FilterId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FilterId"];
			}
			if (dr.Table.Columns.Contains("ApiKey"))
			{
			entity.ApiKey = dr["ApiKey"].ToString();
			}
			if (dr.Table.Columns.Contains("Interval"))
			{
			entity.Interval = (System.Int32)dr["Interval"];
			}
			return entity;
		}

	}
	
	
}
