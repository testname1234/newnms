﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Extensions;

namespace NMS.Repository
{

    public partial class ProgramRepository : ProgramRepositoryBase, IProgramRepository
    {

        public List<Program> GetProgramByUserTeam(int userid)
        {
            string sql = @"select p.* from team t inner join program p on t.programid=p.programid
                            where t.UserId=@userId";
            SqlParameter parameter = new SqlParameter("@userId", userid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Program>(ds, ProgramFromDataRow);
        }

        public List<Program> GetProgramByChannelIdAndUserId(int ChannelId, int UserId)
        {
            string sql = "Select p.*, (Select top 1 Url From ProgramConfiguration pc where p.ProgramId = pc.ProgramId and pc.SegmentTypeId = 3) as StartupURL,(Select top 1 Url From ProgramConfiguration pc where p.ProgramId = pc.ProgramId and pc.SegmentTypeId = 4) as CreditsURL From Team t Inner Join Program p on t.ProgramId = p.ProgramId Where t.UserId = @UserId and p.Channelid = @Channelid";
            SqlParameter parameter1 = new SqlParameter("@UserId", UserId);
            SqlParameter parameter2 = new SqlParameter("@Channelid", ChannelId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Program>(ds, ProgramFromDataRow);
        }

        public override Program GetProgram(System.Int32 ProgramId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetProgramSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from Program with (nolock)  where ProgramId=@ProgramId ";
            SqlParameter parameter = new SqlParameter("@ProgramId", ProgramId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ProgramFromDataRow(ds.Tables[0].Rows[0]);
        }
        //
        public List<Program> GetAllProgramsForReporter(string SelectClause = null)
        {

            string sql = "select * from Program with (nolock) where IsActive=1 and programid in (select programid from folder) order by 1 desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Program>(ds, ProgramFromDataRow);
        }
        public override List<Program> GetAllProgram(string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetProgramSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from Program with (nolock) order by 1 desc ";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Program>(ds, ProgramFromDataRow);
        }

        public override bool DeleteProgram(System.Int32 ProgramId)
        {
            // Updated by Saqib
            // Delete -> isActive=0
            //string sql = "delete from Program where ProgramId=@ProgramId";

            string sql = "Update Program Set IsActive=0 where ProgramId=@ProgramId";
            SqlParameter parameter = new SqlParameter("@ProgramId", ProgramId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public List<Program> GetProgramByMultipleKeyValue(string Key1, string Value1, string Key2, string Value2, Operands operand, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetProgramSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += string.Format("from Program with (nolock)  where {0} {1} '{2}' and {3} {1} '{4}' ", Key1, operand.ToOperandString(), Value1, Key2, Value2);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Program>(ds, ProgramFromDataRow);
        }

        public override Program ProgramFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            Program entity = base.ProgramFromDataRow(dr);

            if (dr.Table.Columns.Contains("StartupURL"))
            {
                entity.StartupUrl = dr["StartupURL"].ToString();
            }
            if (dr.Table.Columns.Contains("CreditsURL"))
            {
                entity.CreditsUrl = dr["CreditsURL"].ToString();
            }

            return entity;
        }

        public override Program InsertProgram(Program entity)
        {

            Program other = new Program();
            other = entity;

            string sql = @"Insert into Program ( [ProgramId]
                ,[Name]
				,[ChannelId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsApproved]
				,[MinStoryCount]
				,[MaxStoryCount] )
				Values
				( @ProgramId
                , @Name
				, @ChannelId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsApproved
				, @MinStoryCount
				, @MaxStoryCount );";
            SqlParameter[] parameterArray = new SqlParameter[]{
                    new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsApproved",entity.IsApproved)
					, new SqlParameter("@MinStoryCount",entity.MinStoryCount ?? (object)DBNull.Value)
					, new SqlParameter("@MaxStoryCount",entity.MaxStoryCount ?? (object)DBNull.Value)};
            SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);

            return entity;
        }

        public int GetProgramIdByBroadcastedTime(int second, int interval)
        {

            string sql = @"select 
	                        ProgramId 
                        from 
	                        Program with (nolock)
                        where
	                        Program.IsActive = 1
	                        and @Second >=Program.Interval and @Second - Program.Interval < @Interval";
            SqlParameter[] parameterArray = new SqlParameter[]{
                    new SqlParameter("@Second",second),
                    new SqlParameter("@Interval",interval)
            };
            var programId = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);

            return Convert.ToInt32(programId);
        }
    }


}
