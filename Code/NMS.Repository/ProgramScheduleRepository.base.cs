﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramScheduleRepositoryBase : Repository, IProgramScheduleRepositoryBase 
	{
        
        public ProgramScheduleRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramScheduleId",new SearchColumn(){Name="ProgramScheduleId",Title="ProgramScheduleId",SelectClause="ProgramScheduleId",WhereClause="AllRecords.ProgramScheduleId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramScheduleId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("WeekDayId",new SearchColumn(){Name="WeekDayId",Title="WeekDayId",SelectClause="WeekDayId",WhereClause="AllRecords.WeekDayId",DataType="System.Int32",IsForeignColumn=false,PropertyName="WeekDayId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StartTime",new SearchColumn(){Name="StartTime",Title="StartTime",SelectClause="StartTime",WhereClause="AllRecords.StartTime",DataType="System.TimeSpan",IsForeignColumn=false,PropertyName="StartTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EndTime",new SearchColumn(){Name="EndTime",Title="EndTime",SelectClause="EndTime",WhereClause="AllRecords.EndTime",DataType="System.TimeSpan",IsForeignColumn=false,PropertyName="EndTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RecurringTypeId",new SearchColumn(){Name="RecurringTypeId",Title="RecurringTypeId",SelectClause="RecurringTypeId",WhereClause="AllRecords.RecurringTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RecurringTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramScheduleSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramScheduleBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramScheduleAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramScheduleSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ProgramSchedule."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",ProgramSchedule."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ProgramSchedule> GetProgramScheduleByProgramId(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScheduleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ProgramSchedule with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSchedule>(ds,ProgramScheduleFromDataRow);
		}

		public virtual List<ProgramSchedule> GetProgramScheduleByRecurringTypeId(System.Int32 RecurringTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScheduleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ProgramSchedule with (nolock)  where RecurringTypeId=@RecurringTypeId  ";
			SqlParameter parameter=new SqlParameter("@RecurringTypeId",RecurringTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSchedule>(ds,ProgramScheduleFromDataRow);
		}

		public virtual ProgramSchedule GetProgramSchedule(System.Int32 ProgramScheduleId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScheduleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ProgramSchedule with (nolock)  where ProgramScheduleId=@ProgramScheduleId ";
			SqlParameter parameter=new SqlParameter("@ProgramScheduleId",ProgramScheduleId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramScheduleFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramSchedule> GetProgramScheduleByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScheduleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ProgramSchedule with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSchedule>(ds,ProgramScheduleFromDataRow);
		}

		public virtual List<ProgramSchedule> GetAllProgramSchedule(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScheduleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ProgramSchedule with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSchedule>(ds, ProgramScheduleFromDataRow);
		}

		public virtual List<ProgramSchedule> GetPagedProgramSchedule(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramScheduleCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramScheduleId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramScheduleId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramScheduleId] ";
            tempsql += " FROM [ProgramSchedule] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramScheduleId"))
					tempsql += " , (AllRecords.[ProgramScheduleId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramScheduleId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramScheduleSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramSchedule] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramSchedule].[ProgramScheduleId] = PageIndex.[ProgramScheduleId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSchedule>(ds, ProgramScheduleFromDataRow);
			}else{ return null;}
		}

		private int GetProgramScheduleCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramSchedule as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramSchedule as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramSchedule))]
		public virtual ProgramSchedule InsertProgramSchedule(ProgramSchedule entity)
		{

			ProgramSchedule other=new ProgramSchedule();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramSchedule ( [WeekDayId]
				,[StartTime]
				,[EndTime]
				,[ProgramId]
				,[RecurringTypeId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @WeekDayId
				, @StartTime
				, @EndTime
				, @ProgramId
				, @RecurringTypeId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@WeekDayId",entity.WeekDayId)
					, new SqlParameter("@StartTime",entity.StartTime)
					, new SqlParameter("@EndTime",entity.EndTime)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@RecurringTypeId",entity.RecurringTypeId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramSchedule(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramSchedule))]
		public virtual ProgramSchedule UpdateProgramSchedule(ProgramSchedule entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramSchedule other = GetProgramSchedule(entity.ProgramScheduleId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramSchedule set  [WeekDayId]=@WeekDayId
							, [StartTime]=@StartTime
							, [EndTime]=@EndTime
							, [ProgramId]=@ProgramId
							, [RecurringTypeId]=@RecurringTypeId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ProgramScheduleId=@ProgramScheduleId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@WeekDayId",entity.WeekDayId)
					, new SqlParameter("@StartTime",entity.StartTime)
					, new SqlParameter("@EndTime",entity.EndTime)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@RecurringTypeId",entity.RecurringTypeId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ProgramScheduleId",entity.ProgramScheduleId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramSchedule(entity.ProgramScheduleId);
		}

		public virtual bool DeleteProgramSchedule(System.Int32 ProgramScheduleId)
		{

			string sql="delete from ProgramSchedule with (nolock) where ProgramScheduleId=@ProgramScheduleId";
			SqlParameter parameter=new SqlParameter("@ProgramScheduleId",ProgramScheduleId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramSchedule))]
		public virtual ProgramSchedule DeleteProgramSchedule(ProgramSchedule entity)
		{
			this.DeleteProgramSchedule(entity.ProgramScheduleId);
			return entity;
		}


		public virtual ProgramSchedule ProgramScheduleFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramSchedule entity=new ProgramSchedule();
			if (dr.Table.Columns.Contains("ProgramScheduleId"))
			{
			entity.ProgramScheduleId = (System.Int32)dr["ProgramScheduleId"];
			}
			if (dr.Table.Columns.Contains("WeekDayId"))
			{
			entity.WeekDayId = (System.Int32)dr["WeekDayId"];
			}
			if (dr.Table.Columns.Contains("StartTime"))
			{
			entity.StartTime = (TimeSpan)dr["StartTime"];
			}
			if (dr.Table.Columns.Contains("EndTime"))
			{
			entity.EndTime = (TimeSpan)dr["EndTime"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("RecurringTypeId"))
			{
			entity.RecurringTypeId = (System.Int32)dr["RecurringTypeId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
