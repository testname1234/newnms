﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class ProgramScheduleRepository: ProgramScheduleRepositoryBase, IProgramScheduleRepository
	{
        public bool ifExistProgramSchedule(ProgramSchedule entity, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetProgramScheduleSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from ProgramSchedule with (nolock)  where ProgramId=@ProgramId and WeekDayId=@WeekDayId and @StartTime between StartTime and EndTime";
            SqlParameter[] parameterArray = new SqlParameter[]{
			         new SqlParameter("@ProgramId",entity.ProgramId)		 
                    ,new SqlParameter("@WeekDayId",entity.WeekDayId)
					, new SqlParameter("@StartTime",entity.StartTime)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return false;
            return true;
        }

        public override bool DeleteProgramSchedule(System.Int32 ProgramScheduleId)
        {

            string sql = "delete from ProgramSchedule  where ProgramScheduleId=@ProgramScheduleId";
            SqlParameter parameter = new SqlParameter("@ProgramScheduleId", ProgramScheduleId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
	}
	
	
}
