﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramScreenElementRepositoryBase : Repository, IProgramScreenElementRepositoryBase 
	{
        
        public ProgramScreenElementRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramScreenElementId",new SearchColumn(){Name="ProgramScreenElementId",Title="ProgramScreenElementId",SelectClause="ProgramScreenElementId",WhereClause="AllRecords.ProgramScreenElementId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramScreenElementId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScreenElementId",new SearchColumn(){Name="ScreenElementId",Title="ScreenElementId",SelectClause="ScreenElementId",WhereClause="AllRecords.ScreenElementId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScreenElementId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImageGuid",new SearchColumn(){Name="ImageGuid",Title="ImageGuid",SelectClause="ImageGuid",WhereClause="AllRecords.ImageGuid",DataType="System.Guid",IsForeignColumn=false,PropertyName="ImageGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramScreenElementSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramScreenElementBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramScreenElementAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramScreenElementSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramScreenElement].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramScreenElement].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ProgramScreenElement> GetProgramScreenElementByProgramId(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ProgramScreenElement with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramScreenElement>(ds,ProgramScreenElementFromDataRow);
		}

		public virtual ProgramScreenElement GetProgramScreenElement(System.Int32 ProgramScreenElementId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ProgramScreenElement with (nolock)  where ProgramScreenElementId=@ProgramScreenElementId ";
			SqlParameter parameter=new SqlParameter("@ProgramScreenElementId",ProgramScreenElementId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramScreenElementFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramScreenElement> GetProgramScreenElementByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ProgramScreenElement with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramScreenElement>(ds,ProgramScreenElementFromDataRow);
		}

		public virtual List<ProgramScreenElement> GetAllProgramScreenElement(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ProgramScreenElement with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramScreenElement>(ds, ProgramScreenElementFromDataRow);
		}

		public virtual List<ProgramScreenElement> GetPagedProgramScreenElement(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramScreenElementCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramScreenElementId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramScreenElementId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramScreenElementId] ";
            tempsql += " FROM [ProgramScreenElement] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramScreenElementId"))
					tempsql += " , (AllRecords.[ProgramScreenElementId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramScreenElementId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramScreenElementSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramScreenElement] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramScreenElement].[ProgramScreenElementId] = PageIndex.[ProgramScreenElementId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramScreenElement>(ds, ProgramScreenElementFromDataRow);
			}else{ return null;}
		}

		private int GetProgramScreenElementCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramScreenElement as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramScreenElement as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramScreenElement))]
		public virtual ProgramScreenElement InsertProgramScreenElement(ProgramScreenElement entity)
		{

			ProgramScreenElement other=new ProgramScreenElement();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramScreenElement ( [ScreenElementId]
				,[ImageGuid]
				,[ProgramId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @ScreenElementId
				, @ImageGuid
				, @ProgramId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenElementId",entity.ScreenElementId)
					, new SqlParameter("@ImageGuid",entity.ImageGuid)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramScreenElement(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramScreenElement))]
		public virtual ProgramScreenElement UpdateProgramScreenElement(ProgramScreenElement entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramScreenElement other = GetProgramScreenElement(entity.ProgramScreenElementId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramScreenElement set  [ScreenElementId]=@ScreenElementId
							, [ImageGuid]=@ImageGuid
							, [ProgramId]=@ProgramId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ProgramScreenElementId=@ProgramScreenElementId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenElementId",entity.ScreenElementId)
					, new SqlParameter("@ImageGuid",entity.ImageGuid)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ProgramScreenElementId",entity.ProgramScreenElementId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramScreenElement(entity.ProgramScreenElementId);
		}

		public virtual bool DeleteProgramScreenElement(System.Int32 ProgramScreenElementId)
		{

			string sql="delete from ProgramScreenElement with (nolock) where ProgramScreenElementId=@ProgramScreenElementId";
			SqlParameter parameter=new SqlParameter("@ProgramScreenElementId",ProgramScreenElementId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramScreenElement))]
		public virtual ProgramScreenElement DeleteProgramScreenElement(ProgramScreenElement entity)
		{
			this.DeleteProgramScreenElement(entity.ProgramScreenElementId);
			return entity;
		}


		public virtual ProgramScreenElement ProgramScreenElementFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramScreenElement entity=new ProgramScreenElement();
			if (dr.Table.Columns.Contains("ProgramScreenElementId"))
			{
			entity.ProgramScreenElementId = (System.Int32)dr["ProgramScreenElementId"];
			}
			if (dr.Table.Columns.Contains("ScreenElementId"))
			{
			entity.ScreenElementId = (System.Int32)dr["ScreenElementId"];
			}
			if (dr.Table.Columns.Contains("ImageGuid"))
			{
			entity.ImageGuid = (System.Guid)dr["ImageGuid"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
