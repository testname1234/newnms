﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramSegmentRepositoryBase : Repository, IProgramSegmentRepositoryBase 
	{
        
        public ProgramSegmentRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramSegmentId",new SearchColumn(){Name="ProgramSegmentId",Title="ProgramSegmentId",SelectClause="ProgramSegmentId",WhereClause="AllRecords.ProgramSegmentId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramSegmentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SegmentTypeId",new SearchColumn(){Name="SegmentTypeId",Title="SegmentTypeId",SelectClause="SegmentTypeId",WhereClause="AllRecords.SegmentTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SegmentTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceNumber",new SearchColumn(){Name="SequenceNumber",Title="SequenceNumber",SelectClause="SequenceNumber",WhereClause="AllRecords.SequenceNumber",DataType="System.Int32",IsForeignColumn=false,PropertyName="SequenceNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Double?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StoryCount",new SearchColumn(){Name="StoryCount",Title="StoryCount",SelectClause="StoryCount",WhereClause="AllRecords.StoryCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="StoryCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramSegmentSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramSegmentBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramSegmentAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramSegmentSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramSegment].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramSegment].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ProgramSegment> GetProgramSegmentByProgramId(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSegmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramSegment] with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSegment>(ds,ProgramSegmentFromDataRow);
		}

		public virtual List<ProgramSegment> GetProgramSegmentBySegmentTypeId(System.Int32 SegmentTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSegmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramSegment] with (nolock)  where SegmentTypeId=@SegmentTypeId  ";
			SqlParameter parameter=new SqlParameter("@SegmentTypeId",SegmentTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSegment>(ds,ProgramSegmentFromDataRow);
		}

		public virtual ProgramSegment GetProgramSegment(System.Int32 ProgramSegmentId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSegmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramSegment] with (nolock)  where ProgramSegmentId=@ProgramSegmentId ";
			SqlParameter parameter=new SqlParameter("@ProgramSegmentId",ProgramSegmentId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramSegmentFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramSegment> GetProgramSegmentByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSegmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ProgramSegment] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSegment>(ds,ProgramSegmentFromDataRow);
		}

		public virtual List<ProgramSegment> GetAllProgramSegment(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSegmentSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramSegment] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSegment>(ds, ProgramSegmentFromDataRow);
		}

		public virtual List<ProgramSegment> GetPagedProgramSegment(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramSegmentCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramSegmentId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramSegmentId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramSegmentId] ";
            tempsql += " FROM [ProgramSegment] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramSegmentId"))
					tempsql += " , (AllRecords.[ProgramSegmentId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramSegmentId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramSegmentSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramSegment] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramSegment].[ProgramSegmentId] = PageIndex.[ProgramSegmentId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSegment>(ds, ProgramSegmentFromDataRow);
			}else{ return null;}
		}

		private int GetProgramSegmentCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramSegment as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramSegment as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramSegment))]
		public virtual ProgramSegment InsertProgramSegment(ProgramSegment entity)
		{

			ProgramSegment other=new ProgramSegment();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramSegment ( [ProgramId]
				,[SegmentTypeId]
				,[SequenceNumber]
				,[Duration]
				,[StoryCount]
				,[Name]
				,[CreationDate]
				,[LastUpdatedDate]
				,[IsActive] )
				Values
				( @ProgramId
				, @SegmentTypeId
				, @SequenceNumber
				, @Duration
				, @StoryCount
				, @Name
				, @CreationDate
				, @LastUpdatedDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@SegmentTypeId",entity.SegmentTypeId)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@StoryCount",entity.StoryCount ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramSegment(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramSegment))]
		public virtual ProgramSegment UpdateProgramSegment(ProgramSegment entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramSegment other = GetProgramSegment(entity.ProgramSegmentId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramSegment set  [ProgramId]=@ProgramId
							, [SegmentTypeId]=@SegmentTypeId
							, [SequenceNumber]=@SequenceNumber
							, [Duration]=@Duration
							, [StoryCount]=@StoryCount
							, [Name]=@Name
							, [CreationDate]=@CreationDate
							, [LastUpdatedDate]=@LastUpdatedDate
							, [IsActive]=@IsActive 
							 where ProgramSegmentId=@ProgramSegmentId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@SegmentTypeId",entity.SegmentTypeId)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@StoryCount",entity.StoryCount ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ProgramSegmentId",entity.ProgramSegmentId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramSegment(entity.ProgramSegmentId);
		}

		public virtual bool DeleteProgramSegment(System.Int32 ProgramSegmentId)
		{

			string sql="delete from ProgramSegment where ProgramSegmentId=@ProgramSegmentId";
			SqlParameter parameter=new SqlParameter("@ProgramSegmentId",ProgramSegmentId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramSegment))]
		public virtual ProgramSegment DeleteProgramSegment(ProgramSegment entity)
		{
			this.DeleteProgramSegment(entity.ProgramSegmentId);
			return entity;
		}


		public virtual ProgramSegment ProgramSegmentFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramSegment entity=new ProgramSegment();
			if (dr.Table.Columns.Contains("ProgramSegmentId"))
			{
			entity.ProgramSegmentId = (System.Int32)dr["ProgramSegmentId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("SegmentTypeId"))
			{
			entity.SegmentTypeId = (System.Int32)dr["SegmentTypeId"];
			}
			if (dr.Table.Columns.Contains("SequenceNumber"))
			{
			entity.SequenceNumber = (System.Int32)dr["SequenceNumber"];
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("StoryCount"))
			{
			entity.StoryCount = dr["StoryCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["StoryCount"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = (System.DateTime)dr["LastUpdatedDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
