﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class ProgramSegmentRepository: ProgramSegmentRepositoryBase, IProgramSegmentRepository
	{
        public bool DeleteProgramSegmentByProgramId(System.Int32 ProgramId)
        {

            string sql = "delete from ProgramSegment where ProgramId=@ProgramId";
            SqlParameter parameter = new SqlParameter("@ProgramId", ProgramId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
	}
	
	
}
