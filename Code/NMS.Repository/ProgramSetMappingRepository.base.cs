﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramSetMappingRepositoryBase : Repository, IProgramSetMappingRepositoryBase 
	{
        
        public ProgramSetMappingRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramSetMappingId",new SearchColumn(){Name="ProgramSetMappingId",Title="ProgramSetMappingId",SelectClause="ProgramSetMappingId",WhereClause="AllRecords.ProgramSetMappingId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramSetMappingId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SetId",new SearchColumn(){Name="SetId",Title="SetId",SelectClause="SetId",WhereClause="AllRecords.SetId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SetId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramSetMappingSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramSetMappingBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramSetMappingAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramSetMappingSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramSetMapping].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramSetMapping].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ProgramSetMapping GetProgramSetMapping(System.Int32 ProgramSetMappingId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSetMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramSetMapping] with (nolock)  where ProgramSetMappingId=@ProgramSetMappingId ";
			SqlParameter parameter=new SqlParameter("@ProgramSetMappingId",ProgramSetMappingId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramSetMappingFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramSetMapping> GetProgramSetMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSetMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ProgramSetMapping] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSetMapping>(ds,ProgramSetMappingFromDataRow);
		}

		public virtual List<ProgramSetMapping> GetAllProgramSetMapping(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramSetMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramSetMapping] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSetMapping>(ds, ProgramSetMappingFromDataRow);
		}

		public virtual List<ProgramSetMapping> GetPagedProgramSetMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramSetMappingCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramSetMappingId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramSetMappingId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramSetMappingId] ";
            tempsql += " FROM [ProgramSetMapping] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramSetMappingId"))
					tempsql += " , (AllRecords.[ProgramSetMappingId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramSetMappingId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramSetMappingSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramSetMapping] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramSetMapping].[ProgramSetMappingId] = PageIndex.[ProgramSetMappingId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramSetMapping>(ds, ProgramSetMappingFromDataRow);
			}else{ return null;}
		}

		private int GetProgramSetMappingCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramSetMapping as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramSetMapping as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramSetMapping))]
		public virtual ProgramSetMapping InsertProgramSetMapping(ProgramSetMapping entity)
		{

			ProgramSetMapping other=new ProgramSetMapping();
			other = entity;
				string sql=@"Insert into ProgramSetMapping ( [ProgramSetMappingId]
				,[ProgramId]
				,[SetId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @ProgramSetMappingId
				, @ProgramId
				, @SetId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
";
				SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@ProgramSetMappingId",entity.ProgramSetMappingId)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@SetId",entity.SetId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramSetMapping(Convert.ToInt32(identity));
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramSetMapping))]
		public virtual ProgramSetMapping UpdateProgramSetMapping(ProgramSetMapping entity)
		{

			ProgramSetMapping other = GetProgramSetMapping(entity.ProgramSetMappingId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramSetMapping set  [ProgramId]=@ProgramId
							, [SetId]=@SetId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ProgramSetMappingId=@ProgramSetMappingId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@ProgramSetMappingId",entity.ProgramSetMappingId)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@SetId",entity.SetId)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramSetMapping(entity.ProgramSetMappingId);
		}

		public virtual bool DeleteProgramSetMapping(System.Int32 ProgramSetMappingId)
		{

			string sql="delete from ProgramSetMapping where ProgramSetMappingId=@ProgramSetMappingId";
			SqlParameter parameter=new SqlParameter("@ProgramSetMappingId",ProgramSetMappingId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramSetMapping))]
		public virtual ProgramSetMapping DeleteProgramSetMapping(ProgramSetMapping entity)
		{
			this.DeleteProgramSetMapping(entity.ProgramSetMappingId);
			return entity;
		}


		public virtual ProgramSetMapping ProgramSetMappingFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramSetMapping entity=new ProgramSetMapping();
			if (dr.Table.Columns.Contains("ProgramSetMappingId"))
			{
			entity.ProgramSetMappingId = (System.Int32)dr["ProgramSetMappingId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("SetId"))
			{
			entity.SetId = (System.Int32)dr["SetId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
