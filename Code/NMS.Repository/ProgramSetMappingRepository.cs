﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;

namespace NMS.Repository
{

    public partial class ProgramSetMappingRepository : ProgramSetMappingRepositoryBase, IProgramSetMappingRepository
    {

        public virtual ProgramSetMapping GetProgramSetMappingByLastUpdateDate()
        {
           string sql = @"select Top 1 * from ProgramSetMapping Order By LastUpdateDate desc";
           DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql,null);
           if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
           return ProgramSetMappingFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual ProgramSetMapping InsertProgramSetMapping(ProgramSetMapping entity)
        {

            List<ProgramSetMapping>  entities = GetProgramSetMappingByKeyValue("ProgramSetMappingId", entity.ProgramSetMappingId.ToString(), Operands.Equal,null);
            ProgramSetMapping _newEntity = new ProgramSetMapping();

            if (entities != null && entities.Count == 1)
            {
                entities[0].ProgramId = entity.ProgramId;
                entities[0].SetId = entity.ProgramId;
                entities[0].CreationDate = entity.CreationDate;
                entities[0].LastUpdateDate = entity.LastUpdateDate;
                entities[0].IsActive = entity.IsActive;
                _newEntity = base.UpdateProgramSetMapping(entity);
            }
            else
               _newEntity = base.InsertProgramSetMapping(entity);

            return _newEntity;
        }
    }


}
