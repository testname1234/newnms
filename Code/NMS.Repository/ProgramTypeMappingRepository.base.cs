﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramTypeMappingRepositoryBase : Repository, IProgramTypeMappingRepositoryBase 
	{
        
        public ProgramTypeMappingRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramTypeMappingId",new SearchColumn(){Name="ProgramTypeMappingId",Title="ProgramTypeMappingId",SelectClause="ProgramTypeMappingId",WhereClause="AllRecords.ProgramTypeMappingId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramTypeMappingId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramTypeId",new SearchColumn(){Name="ProgramTypeId",Title="ProgramTypeId",SelectClause="ProgramTypeId",WhereClause="AllRecords.ProgramTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramTypeMappingSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramTypeMappingBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramTypeMappingAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramTypeMappingSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramTypeMapping].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramTypeMapping].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ProgramTypeMapping> GetProgramTypeMappingByProgramId(System.Int32? ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramTypeMapping] with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramTypeMapping>(ds,ProgramTypeMappingFromDataRow);
		}

		public virtual List<ProgramTypeMapping> GetProgramTypeMappingByProgramTypeId(System.Int32? ProgramTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramTypeMapping] with (nolock)  where ProgramTypeId=@ProgramTypeId  ";
			SqlParameter parameter=new SqlParameter("@ProgramTypeId",ProgramTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramTypeMapping>(ds,ProgramTypeMappingFromDataRow);
		}

		public virtual ProgramTypeMapping GetProgramTypeMapping(System.Int32 ProgramTypeMappingId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramTypeMapping] with (nolock)  where ProgramTypeMappingId=@ProgramTypeMappingId ";
			SqlParameter parameter=new SqlParameter("@ProgramTypeMappingId",ProgramTypeMappingId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramTypeMappingFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramTypeMapping> GetProgramTypeMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ProgramTypeMapping] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramTypeMapping>(ds,ProgramTypeMappingFromDataRow);
		}

		public virtual List<ProgramTypeMapping> GetAllProgramTypeMapping(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramTypeMapping] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramTypeMapping>(ds, ProgramTypeMappingFromDataRow);
		}

		public virtual List<ProgramTypeMapping> GetPagedProgramTypeMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramTypeMappingCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramTypeMappingId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramTypeMappingId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramTypeMappingId] ";
            tempsql += " FROM [ProgramTypeMapping] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramTypeMappingId"))
					tempsql += " , (AllRecords.[ProgramTypeMappingId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramTypeMappingId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramTypeMappingSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramTypeMapping] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramTypeMapping].[ProgramTypeMappingId] = PageIndex.[ProgramTypeMappingId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramTypeMapping>(ds, ProgramTypeMappingFromDataRow);
			}else{ return null;}
		}

		private int GetProgramTypeMappingCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramTypeMapping as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramTypeMapping as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramTypeMapping))]
		public virtual ProgramTypeMapping InsertProgramTypeMapping(ProgramTypeMapping entity)
		{

			ProgramTypeMapping other=new ProgramTypeMapping();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramTypeMapping ( [ProgramId]
				,[ProgramTypeId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @ProgramId
				, @ProgramTypeId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramTypeId",entity.ProgramTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramTypeMapping(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramTypeMapping))]
		public virtual ProgramTypeMapping UpdateProgramTypeMapping(ProgramTypeMapping entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramTypeMapping other = GetProgramTypeMapping(entity.ProgramTypeMappingId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramTypeMapping set  [ProgramId]=@ProgramId
							, [ProgramTypeId]=@ProgramTypeId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ProgramTypeMappingId=@ProgramTypeMappingId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramTypeId",entity.ProgramTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramTypeMappingId",entity.ProgramTypeMappingId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramTypeMapping(entity.ProgramTypeMappingId);
		}

		public virtual bool DeleteProgramTypeMapping(System.Int32 ProgramTypeMappingId)
		{

			string sql="delete from ProgramTypeMapping where ProgramTypeMappingId=@ProgramTypeMappingId";
			SqlParameter parameter=new SqlParameter("@ProgramTypeMappingId",ProgramTypeMappingId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramTypeMapping))]
		public virtual ProgramTypeMapping DeleteProgramTypeMapping(ProgramTypeMapping entity)
		{
			this.DeleteProgramTypeMapping(entity.ProgramTypeMappingId);
			return entity;
		}


		public virtual ProgramTypeMapping ProgramTypeMappingFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramTypeMapping entity=new ProgramTypeMapping();
			if (dr.Table.Columns.Contains("ProgramTypeMappingId"))
			{
			entity.ProgramTypeMappingId = (System.Int32)dr["ProgramTypeMappingId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("ProgramTypeId"))
			{
			entity.ProgramTypeId = dr["ProgramTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramTypeId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
