﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ProgramTypeRepositoryBase : Repository, IProgramTypeRepositoryBase 
	{
        
        public ProgramTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ProgramTypeId",new SearchColumn(){Name="ProgramTypeId",Title="ProgramTypeId",SelectClause="ProgramTypeId",WhereClause="AllRecords.ProgramTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramTypeName",new SearchColumn(){Name="ProgramTypeName",Title="ProgramTypeName",SelectClause="ProgramTypeName",WhereClause="AllRecords.ProgramTypeName",DataType="System.String",IsForeignColumn=false,PropertyName="ProgramTypeName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetProgramTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetProgramTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetProgramTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetProgramTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ProgramType].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ProgramType].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ProgramType GetProgramType(System.Int32 ProgramTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramType] with (nolock)  where ProgramTypeId=@ProgramTypeId ";
			SqlParameter parameter=new SqlParameter("@ProgramTypeId",ProgramTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ProgramTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ProgramType> GetProgramTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ProgramType] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramType>(ds,ProgramTypeFromDataRow);
		}

		public virtual List<ProgramType> GetAllProgramType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetProgramTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ProgramType] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramType>(ds, ProgramTypeFromDataRow);
		}

		public virtual List<ProgramType> GetPagedProgramType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetProgramTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ProgramTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ProgramTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ProgramTypeId] ";
            tempsql += " FROM [ProgramType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ProgramTypeId"))
					tempsql += " , (AllRecords.[ProgramTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ProgramTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetProgramTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ProgramType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ProgramType].[ProgramTypeId] = PageIndex.[ProgramTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ProgramType>(ds, ProgramTypeFromDataRow);
			}else{ return null;}
		}

		private int GetProgramTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ProgramType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ProgramType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ProgramType))]
		public virtual ProgramType InsertProgramType(ProgramType entity)
		{

			ProgramType other=new ProgramType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ProgramType ( [ProgramTypeName] )
				Values
				( @ProgramTypeName );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramTypeName",entity.ProgramTypeName ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetProgramType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ProgramType))]
		public virtual ProgramType UpdateProgramType(ProgramType entity)
		{

			if (entity.IsTransient()) return entity;
			ProgramType other = GetProgramType(entity.ProgramTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ProgramType set  [ProgramTypeName]=@ProgramTypeName 
							 where ProgramTypeId=@ProgramTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ProgramTypeName",entity.ProgramTypeName ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramTypeId",entity.ProgramTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetProgramType(entity.ProgramTypeId);
		}

		public virtual bool DeleteProgramType(System.Int32 ProgramTypeId)
		{

			string sql="delete from ProgramType where ProgramTypeId=@ProgramTypeId";
			SqlParameter parameter=new SqlParameter("@ProgramTypeId",ProgramTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ProgramType))]
		public virtual ProgramType DeleteProgramType(ProgramType entity)
		{
			this.DeleteProgramType(entity.ProgramTypeId);
			return entity;
		}


		public virtual ProgramType ProgramTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ProgramType entity=new ProgramType();
			if (dr.Table.Columns.Contains("ProgramTypeId"))
			{
			entity.ProgramTypeId = (System.Int32)dr["ProgramTypeId"];
			}
			if (dr.Table.Columns.Contains("ProgramTypeName"))
			{
			entity.ProgramTypeName = dr["ProgramTypeName"].ToString();
			}
			return entity;
		}

	}
	
	
}
