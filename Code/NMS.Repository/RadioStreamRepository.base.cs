﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class RadioStreamRepositoryBase : Repository, IRadioStreamRepositoryBase 
	{
        
        public RadioStreamRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("RadioStreamId",new SearchColumn(){Name="RadioStreamId",Title="RadioStreamId",SelectClause="RadioStreamId",WhereClause="AllRecords.RadioStreamId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RadioStreamId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RadioStationId",new SearchColumn(){Name="RadioStationId",Title="RadioStationId",SelectClause="RadioStationId",WhereClause="AllRecords.RadioStationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RadioStationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PhysicalPath",new SearchColumn(){Name="PhysicalPath",Title="PhysicalPath",SelectClause="PhysicalPath",WhereClause="AllRecords.PhysicalPath",DataType="System.String",IsForeignColumn=false,PropertyName="PhysicalPath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Url",new SearchColumn(){Name="Url",Title="Url",SelectClause="Url",WhereClause="AllRecords.Url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("To",new SearchColumn(){Name="To",Title="To",SelectClause="To",WhereClause="AllRecords.To",DataType="System.DateTime",IsForeignColumn=false,PropertyName="To",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("From",new SearchColumn(){Name="From",Title="From",SelectClause="From",WhereClause="AllRecords.From",DataType="System.DateTime",IsForeignColumn=false,PropertyName="From",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsProcessed",new SearchColumn(){Name="IsProcessed",Title="IsProcessed",SelectClause="IsProcessed",WhereClause="AllRecords.IsProcessed",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsProcessed",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetRadioStreamSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetRadioStreamBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetRadioStreamAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetRadioStreamSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                            selectQuery = "RadioStream.[" + keyValuePair.Key + "]";
                    	}
                    	else
                    	{
                            selectQuery += ",RadioStream.[" + keyValuePair.Key + "]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<RadioStream> GetRadioStreamByRadioStationId(System.Int32 RadioStationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRadioStreamSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from RadioStream with (nolock)  where RadioStationId=@RadioStationId  ";
			SqlParameter parameter=new SqlParameter("@RadioStationId",RadioStationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RadioStream>(ds,RadioStreamFromDataRow);
		}

		public virtual RadioStream GetRadioStream(System.Int32 RadioStreamId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRadioStreamSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from RadioStream with (nolock)  where RadioStreamId=@RadioStreamId ";
			SqlParameter parameter=new SqlParameter("@RadioStreamId",RadioStreamId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return RadioStreamFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<RadioStream> GetRadioStreamByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRadioStreamSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from RadioStream with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RadioStream>(ds,RadioStreamFromDataRow);
		}

		public virtual List<RadioStream> GetAllRadioStream(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRadioStreamSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from RadioStream with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RadioStream>(ds, RadioStreamFromDataRow);
		}

		public virtual List<RadioStream> GetPagedRadioStream(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetRadioStreamCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [RadioStreamId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([RadioStreamId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [RadioStreamId] ";
            tempsql += " FROM [RadioStream] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("RadioStreamId"))
					tempsql += " , (AllRecords.[RadioStreamId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[RadioStreamId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetRadioStreamSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [RadioStream] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [RadioStream].[RadioStreamId] = PageIndex.[RadioStreamId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RadioStream>(ds, RadioStreamFromDataRow);
			}else{ return null;}
		}

		private int GetRadioStreamCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM RadioStream as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM RadioStream as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(RadioStream))]
		public virtual RadioStream InsertRadioStream(RadioStream entity)
		{

			RadioStream other=new RadioStream();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into RadioStream ( [RadioStationId]
				,[PhysicalPath]
				,[ProgramId]
				,[Url]
				,[To]
				,[From]
				,[IsProcessed]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @RadioStationId
				, @PhysicalPath
				, @ProgramId
				, @Url
				, @To
				, @From
				, @IsProcessed
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@RadioStationId",entity.RadioStationId)
					, new SqlParameter("@PhysicalPath",entity.PhysicalPath)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@Url",entity.Url)
					, new SqlParameter("@To",entity.To)
					, new SqlParameter("@From",entity.From)
					, new SqlParameter("@IsProcessed",entity.IsProcessed)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetRadioStream(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(RadioStream))]
		public virtual RadioStream UpdateRadioStream(RadioStream entity)
		{

			if (entity.IsTransient()) return entity;
			RadioStream other = GetRadioStream(entity.RadioStreamId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update RadioStream set  [RadioStationId]=@RadioStationId
							, [PhysicalPath]=@PhysicalPath
							, [ProgramId]=@ProgramId
							, [Url]=@Url
							, [To]=@To
							, [From]=@From
							, [IsProcessed]=@IsProcessed
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where RadioStreamId=@RadioStreamId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@RadioStationId",entity.RadioStationId)
					, new SqlParameter("@PhysicalPath",entity.PhysicalPath)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@Url",entity.Url)
					, new SqlParameter("@To",entity.To)
					, new SqlParameter("@From",entity.From)
					, new SqlParameter("@IsProcessed",entity.IsProcessed)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@RadioStreamId",entity.RadioStreamId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetRadioStream(entity.RadioStreamId);
		}

		public virtual bool DeleteRadioStream(System.Int32 RadioStreamId)
		{

			string sql="delete from RadioStream with (nolock) where RadioStreamId=@RadioStreamId";
			SqlParameter parameter=new SqlParameter("@RadioStreamId",RadioStreamId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(RadioStream))]
		public virtual RadioStream DeleteRadioStream(RadioStream entity)
		{
			this.DeleteRadioStream(entity.RadioStreamId);
			return entity;
		}


		public virtual RadioStream RadioStreamFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			RadioStream entity=new RadioStream();
			if (dr.Table.Columns.Contains("RadioStreamId"))
			{
			entity.RadioStreamId = (System.Int32)dr["RadioStreamId"];
			}
			if (dr.Table.Columns.Contains("RadioStationId"))
			{
			entity.RadioStationId = (System.Int32)dr["RadioStationId"];
			}
			if (dr.Table.Columns.Contains("PhysicalPath"))
			{
			entity.PhysicalPath = dr["PhysicalPath"].ToString();
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("Url"))
			{
			entity.Url = dr["Url"].ToString();
			}
			if (dr.Table.Columns.Contains("To"))
			{
			entity.To = (System.DateTime)dr["To"];
			}
			if (dr.Table.Columns.Contains("From"))
			{
			entity.From = (System.DateTime)dr["From"];
			}
			if (dr.Table.Columns.Contains("IsProcessed"))
			{
			entity.IsProcessed = (System.Boolean)dr["IsProcessed"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
