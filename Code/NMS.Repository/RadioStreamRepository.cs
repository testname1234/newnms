﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class RadioStreamRepository: RadioStreamRepositoryBase, IRadioStreamRepository
	{
        public List<RadioStream> GetRadioStreamBetweenDates(System.DateTime fromDate, System.DateTime toDate, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetRadioStreamSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from RadioStream with (nolock) where [from]>=@from and [to]<=@to";
            SqlParameter parameter1 = new SqlParameter("@from", fromDate);
            SqlParameter parameter2 = new SqlParameter("@to", toDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<RadioStream>(ds, RadioStreamFromDataRow);
        }

        public void MarkIsProcessed(int id)
        {
            string sql = "update RadioStream set IsProcessed=1 where RadioStreamid=@Id";
            SqlParameter parameter = new SqlParameter("@Id", id);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
        }

	}
	
	
}
