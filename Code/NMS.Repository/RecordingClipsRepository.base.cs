﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class RecordingClipsRepositoryBase : Repository, IRecordingClipsRepositoryBase 
	{
        
        public RecordingClipsRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("RecordingClipId",new SearchColumn(){Name="RecordingClipId",Title="RecordingClipId",SelectClause="RecordingClipId",WhereClause="AllRecords.RecordingClipId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RecordingClipId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Script",new SearchColumn(){Name="Script",Title="Script",SelectClause="Script",WhereClause="AllRecords.Script",DataType="System.String",IsForeignColumn=false,PropertyName="Script",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramRecordingId",new SearchColumn(){Name="ProgramRecordingId",Title="ProgramRecordingId",SelectClause="ProgramRecordingId",WhereClause="AllRecords.ProgramRecordingId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramRecordingId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("From",new SearchColumn(){Name="From",Title="From",SelectClause="From",WhereClause="AllRecords.From",DataType="System.String",IsForeignColumn=false,PropertyName="From",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("To",new SearchColumn(){Name="To",Title="To",SelectClause="To",WhereClause="AllRecords.To",DataType="System.String",IsForeignColumn=false,PropertyName="To",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedBy",new SearchColumn(){Name="CreatedBy",Title="CreatedBy",SelectClause="CreatedBy",WhereClause="AllRecords.CreatedBy",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CreatedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Status",new SearchColumn(){Name="Status",Title="Status",SelectClause="Status",WhereClause="AllRecords.Status",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Status",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageId",new SearchColumn(){Name="LanguageId",Title="LanguageId",SelectClause="LanguageId",WhereClause="AllRecords.LanguageId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LanguageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentId",new SearchColumn(){Name="ParentId",Title="ParentId",SelectClause="ParentId",WhereClause="AllRecords.ParentId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetRecordingClipsSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetRecordingClipsBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetRecordingClipsAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetRecordingClipsSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[RecordingClips].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[RecordingClips].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<RecordingClips> GetRecordingClipsByCelebrityId(System.Int32? CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecordingClipsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RecordingClips] with (nolock)  where CelebrityId=@CelebrityId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecordingClips>(ds,RecordingClipsFromDataRow);
		}

		public virtual List<RecordingClips> GetRecordingClipsByProgramRecordingId(System.Int32? ProgramRecordingId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecordingClipsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RecordingClips] with (nolock)  where ProgramRecordingId=@ProgramRecordingId  ";
			SqlParameter parameter=new SqlParameter("@ProgramRecordingId",ProgramRecordingId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecordingClips>(ds,RecordingClipsFromDataRow);
		}

		public virtual RecordingClips GetRecordingClips(System.Int32 RecordingClipId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecordingClipsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RecordingClips] with (nolock)  where RecordingClipId=@RecordingClipId ";
			SqlParameter parameter=new SqlParameter("@RecordingClipId",RecordingClipId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return RecordingClipsFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<RecordingClips> GetRecordingClipsByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecordingClipsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [RecordingClips] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecordingClips>(ds,RecordingClipsFromDataRow);
		}

		public virtual List<RecordingClips> GetAllRecordingClips(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecordingClipsSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RecordingClips] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecordingClips>(ds, RecordingClipsFromDataRow);
		}

		public virtual List<RecordingClips> GetPagedRecordingClips(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetRecordingClipsCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [RecordingClipId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([RecordingClipId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [RecordingClipId] ";
            tempsql += " FROM [RecordingClips] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("RecordingClipId"))
					tempsql += " , (AllRecords.[RecordingClipId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[RecordingClipId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetRecordingClipsSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [RecordingClips] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [RecordingClips].[RecordingClipId] = PageIndex.[RecordingClipId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecordingClips>(ds, RecordingClipsFromDataRow);
			}else{ return null;}
		}

		private int GetRecordingClipsCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM RecordingClips as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM RecordingClips as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(RecordingClips))]
		public virtual RecordingClips InsertRecordingClips(RecordingClips entity)
		{

			RecordingClips other=new RecordingClips();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into RecordingClips ( [Script]
				,[CelebrityId]
				,[ProgramRecordingId]
				,[From]
				,[To]
				,[CreatedBy]
				,[CreationDate]
				,[LastUpdateDate]
				,[Status]
				,[LanguageId]
				,[ParentId] )
				Values
				( @Script
				, @CelebrityId
				, @ProgramRecordingId
				, @From
				, @To
				, @CreatedBy
				, @CreationDate
				, @LastUpdateDate
				, @Status
				, @LanguageId
				, @ParentId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Script",entity.Script ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramRecordingId",entity.ProgramRecordingId ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageId",entity.LanguageId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetRecordingClips(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(RecordingClips))]
		public virtual RecordingClips UpdateRecordingClips(RecordingClips entity)
		{

			if (entity.IsTransient()) return entity;
			RecordingClips other = GetRecordingClips(entity.RecordingClipId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update RecordingClips set  [Script]=@Script
							, [CelebrityId]=@CelebrityId
							, [ProgramRecordingId]=@ProgramRecordingId
							, [From]=@From
							, [To]=@To
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [Status]=@Status
							, [LanguageId]=@LanguageId
							, [ParentId]=@ParentId 
							 where RecordingClipId=@RecordingClipId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Script",entity.Script ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramRecordingId",entity.ProgramRecordingId ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageId",entity.LanguageId ?? (object)DBNull.Value)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@RecordingClipId",entity.RecordingClipId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetRecordingClips(entity.RecordingClipId);
		}

		public virtual bool DeleteRecordingClips(System.Int32 RecordingClipId)
		{

			string sql="delete from RecordingClips where RecordingClipId=@RecordingClipId";
			SqlParameter parameter=new SqlParameter("@RecordingClipId",RecordingClipId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(RecordingClips))]
		public virtual RecordingClips DeleteRecordingClips(RecordingClips entity)
		{
			this.DeleteRecordingClips(entity.RecordingClipId);
			return entity;
		}


		public virtual RecordingClips RecordingClipsFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			RecordingClips entity=new RecordingClips();
			if (dr.Table.Columns.Contains("RecordingClipId"))
			{
			entity.RecordingClipId = (System.Int32)dr["RecordingClipId"];
			}
			if (dr.Table.Columns.Contains("Script"))
			{
			entity.Script = dr["Script"].ToString();
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = dr["CelebrityId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("ProgramRecordingId"))
			{
			entity.ProgramRecordingId = dr["ProgramRecordingId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramRecordingId"];
			}
			if (dr.Table.Columns.Contains("From"))
			{
			entity.From = dr["From"].ToString();
			}
			if (dr.Table.Columns.Contains("To"))
			{
			entity.To = dr["To"].ToString();
			}
			if (dr.Table.Columns.Contains("CreatedBy"))
			{
			entity.CreatedBy = dr["CreatedBy"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CreatedBy"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("Status"))
			{
			entity.Status = dr["Status"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Status"];
			}
			if (dr.Table.Columns.Contains("LanguageId"))
			{
			entity.LanguageId = dr["LanguageId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LanguageId"];
			}
			if (dr.Table.Columns.Contains("ParentId"))
			{
			entity.ParentId = dr["ParentId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentId"];
			}
			return entity;
		}

	}
	
	
}
