﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class RecordingClipsRepository: RecordingClipsRepositoryBase, IRecordingClipsRepository
	{
        public List<RecordingClips> GetAllSameRecordings(RecordingClips entity)
        {
            string sql = "Select * from [RecordingClips] with (nolock) where ProgramRecordingId=@ProgramRecordingId and [From]=@From and [To]=@To ";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@ProgramRecordingId",entity.ProgramRecordingId ?? (object)DBNull.Value)
                    , new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
                    , new SqlParameter("@To",entity.To ?? (object)DBNull.Value)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<RecordingClips>(ds, RecordingClipsFromDataRow);
        }

        public List<RecordingClips> GetAllRecordingsByParentId(int id)
        {
            string sql = "Select * from [RecordingClips] with (nolock) where ParentId=@id";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@id",id)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<RecordingClips>(ds, RecordingClipsFromDataRow);
        }

        
        public RecordingClips UpdateRecordingClipsByParentId(RecordingClips entity,int parentID)
        {
            if (entity.IsTransient()) return entity;
            RecordingClips other = GetRecordingClips(entity.RecordingClipId);
            if (entity.Equals(other)) return entity;
            string sql = @"Update RecordingClips set  [Script]=@Script
							, [CelebrityId]=@CelebrityId
							, [From]=@From
							, [To]=@To
							, [LastUpdateDate]=@LastUpdateDate
							, [Status]=@Status
							 where ParentId=@ParentId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Script",entity.Script ?? (object)DBNull.Value)
                    , new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
                    , new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
                    , new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
                    , new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
                    , new SqlParameter("@ParentId",parentID)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetRecordingClips(entity.RecordingClipId);
        }
    }
	
	
}
