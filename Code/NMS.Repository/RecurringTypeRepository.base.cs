﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class RecurringTypeRepositoryBase : Repository, IRecurringTypeRepositoryBase 
	{
        
        public RecurringTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("RecurringTypeId",new SearchColumn(){Name="RecurringTypeId",Title="RecurringTypeId",SelectClause="RecurringTypeId",WhereClause="AllRecords.RecurringTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RecurringTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetRecurringTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetRecurringTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetRecurringTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetRecurringTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[RecurringType].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[RecurringType].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual RecurringType GetRecurringType(System.Int32 RecurringTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecurringTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RecurringType] with (nolock)  where RecurringTypeId=@RecurringTypeId ";
			SqlParameter parameter=new SqlParameter("@RecurringTypeId",RecurringTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return RecurringTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<RecurringType> GetRecurringTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecurringTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [RecurringType] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecurringType>(ds,RecurringTypeFromDataRow);
		}

		public virtual List<RecurringType> GetAllRecurringType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRecurringTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RecurringType] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecurringType>(ds, RecurringTypeFromDataRow);
		}

		public virtual List<RecurringType> GetPagedRecurringType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetRecurringTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [RecurringTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([RecurringTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [RecurringTypeId] ";
            tempsql += " FROM [RecurringType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("RecurringTypeId"))
					tempsql += " , (AllRecords.[RecurringTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[RecurringTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetRecurringTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [RecurringType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [RecurringType].[RecurringTypeId] = PageIndex.[RecurringTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RecurringType>(ds, RecurringTypeFromDataRow);
			}else{ return null;}
		}

		private int GetRecurringTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM RecurringType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM RecurringType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(RecurringType))]
		public virtual RecurringType InsertRecurringType(RecurringType entity)
		{

			RecurringType other=new RecurringType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into RecurringType ( [Type] )
				Values
				( @Type );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Type",entity.Type)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetRecurringType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(RecurringType))]
		public virtual RecurringType UpdateRecurringType(RecurringType entity)
		{

			if (entity.IsTransient()) return entity;
			RecurringType other = GetRecurringType(entity.RecurringTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update RecurringType set  [Type]=@Type 
							 where RecurringTypeId=@RecurringTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Type",entity.Type)
					, new SqlParameter("@RecurringTypeId",entity.RecurringTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetRecurringType(entity.RecurringTypeId);
		}

		public virtual bool DeleteRecurringType(System.Int32 RecurringTypeId)
		{

			string sql="delete from RecurringType where RecurringTypeId=@RecurringTypeId";
			SqlParameter parameter=new SqlParameter("@RecurringTypeId",RecurringTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(RecurringType))]
		public virtual RecurringType DeleteRecurringType(RecurringType entity)
		{
			this.DeleteRecurringType(entity.RecurringTypeId);
			return entity;
		}


		public virtual RecurringType RecurringTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			RecurringType entity=new RecurringType();
			if (dr.Table.Columns.Contains("RecurringTypeId"))
			{
			entity.RecurringTypeId = (System.Int32)dr["RecurringTypeId"];
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"].ToString();
			}
			return entity;
		}

	}
	
	
}
