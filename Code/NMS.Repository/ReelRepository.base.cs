﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ReelRepositoryBase : Repository, IReelRepositoryBase 
	{
        
        public ReelRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ReelId",new SearchColumn(){Name="ReelId",Title="ReelId",SelectClause="ReelId",WhereClause="AllRecords.ReelId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ReelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Path",new SearchColumn(){Name="Path",Title="Path",SelectClause="Path",WhereClause="AllRecords.Path",DataType="System.String",IsForeignColumn=false,PropertyName="Path",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StartTime",new SearchColumn(){Name="StartTime",Title="StartTime",SelectClause="StartTime",WhereClause="AllRecords.StartTime",DataType="System.DateTime",IsForeignColumn=false,PropertyName="StartTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EndTime",new SearchColumn(){Name="EndTime",Title="EndTime",SelectClause="EndTime",WhereClause="AllRecords.EndTime",DataType="System.DateTime",IsForeignColumn=false,PropertyName="EndTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OriginalPath",new SearchColumn(){Name="OriginalPath",Title="OriginalPath",SelectClause="OriginalPath",WhereClause="AllRecords.OriginalPath",DataType="System.String",IsForeignColumn=false,PropertyName="OriginalPath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetReelSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetReelBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetReelAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetReelSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Reel].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Reel].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Reel> GetReelByStatusId(System.Int32 StatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetReelSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Reel] with (nolock)  where StatusId=@StatusId  ";
			SqlParameter parameter=new SqlParameter("@StatusId",StatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Reel>(ds,ReelFromDataRow);
		}

		public virtual Reel GetReel(System.Int32 ReelId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetReelSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Reel] with (nolock)  where ReelId=@ReelId ";
			SqlParameter parameter=new SqlParameter("@ReelId",ReelId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ReelFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Reel> GetReelByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetReelSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Reel] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Reel>(ds,ReelFromDataRow);
		}

		public virtual List<Reel> GetAllReel(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetReelSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Reel] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Reel>(ds, ReelFromDataRow);
		}

		public virtual List<Reel> GetPagedReel(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetReelCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ReelId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ReelId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ReelId] ";
            tempsql += " FROM [Reel] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ReelId"))
					tempsql += " , (AllRecords.[ReelId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ReelId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetReelSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Reel] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Reel].[ReelId] = PageIndex.[ReelId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Reel>(ds, ReelFromDataRow);
			}else{ return null;}
		}

		private int GetReelCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Reel as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Reel as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Reel))]
		public virtual Reel InsertReel(Reel entity)
		{

			Reel other=new Reel();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Reel ( [ChannelId]
				,[Path]
				,[StatusId]
				,[StartTime]
				,[EndTime]
				,[CreationDate]
				,[OriginalPath] )
				Values
				( @ChannelId
				, @Path
				, @StatusId
				, @StartTime
				, @EndTime
				, @CreationDate
				, @OriginalPath );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@Path",entity.Path ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@StartTime",entity.StartTime)
					, new SqlParameter("@EndTime",entity.EndTime)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@OriginalPath",entity.OriginalPath ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetReel(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Reel))]
		public virtual Reel UpdateReel(Reel entity)
		{

			if (entity.IsTransient()) return entity;
			Reel other = GetReel(entity.ReelId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Reel set  [ChannelId]=@ChannelId
							, [Path]=@Path
							, [StatusId]=@StatusId
							, [StartTime]=@StartTime
							, [EndTime]=@EndTime
							, [CreationDate]=@CreationDate
							, [OriginalPath]=@OriginalPath 
							 where ReelId=@ReelId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@Path",entity.Path ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@StartTime",entity.StartTime)
					, new SqlParameter("@EndTime",entity.EndTime)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@OriginalPath",entity.OriginalPath ?? (object)DBNull.Value)
					, new SqlParameter("@ReelId",entity.ReelId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetReel(entity.ReelId);
		}

		public virtual bool DeleteReel(System.Int32 ReelId)
		{

			string sql="delete from Reel where ReelId=@ReelId";
			SqlParameter parameter=new SqlParameter("@ReelId",ReelId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Reel))]
		public virtual Reel DeleteReel(Reel entity)
		{
			this.DeleteReel(entity.ReelId);
			return entity;
		}


		public virtual Reel ReelFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Reel entity=new Reel();
			if (dr.Table.Columns.Contains("ReelId"))
			{
			entity.ReelId = (System.Int32)dr["ReelId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = (System.Int32)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("Path"))
			{
			entity.Path = dr["Path"].ToString();
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("StartTime"))
			{
			entity.StartTime = (System.DateTime)dr["StartTime"];
			}
			if (dr.Table.Columns.Contains("EndTime"))
			{
			entity.EndTime = (System.DateTime)dr["EndTime"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("OriginalPath"))
			{
			entity.OriginalPath = dr["OriginalPath"].ToString();
			}
			return entity;
		}

	}
	
	
}
