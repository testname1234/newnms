﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Enums;

namespace NMS.Repository
{
		
	public partial class ReelRepository: ReelRepositoryBase, IReelRepository
	{
        public bool ReelExist(int channelId, DateTime dt)
        {
            string sql = "select count(1) from [Reel] with (nolock) where channelid=@channelid and starttime=@dt";
            SqlParameter parameter = new SqlParameter("@channelid", (int)channelId);
            SqlParameter parameter1 = new SqlParameter("@dt", dt);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 })) > 0;
        }

        public virtual List<Reel> GetReelByStatusAndChannelId(ReelStatuses reelStatuses, List<int> ChannelIds)
        {

            string sql = GetReelSelectClause();
            sql += string.Format("from [Reel] with (nolock)  where ChannelId in ({0}) and StatusId=@reelStatuses", string.Join(",", ChannelIds));
            SqlParameter parameteStatusId = new SqlParameter("@reelStatuses", (int)reelStatuses);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameteStatusId });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Reel>(ds, ReelFromDataRow);
        }
	}
	
	
}
