﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ResourceEditclipinfoRepositoryBase : Repository, IResourceEditclipinfoRepositoryBase 
	{
        
        public ResourceEditclipinfoRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ResourceEditclipinfoId",new SearchColumn(){Name="ResourceEditclipinfoId",Title="ResourceEditclipinfoId",SelectClause="ResourceEditclipinfoId",WhereClause="AllRecords.ResourceEditclipinfoId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ResourceEditclipinfoId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsResourceEditId",new SearchColumn(){Name="NewsResourceEditId",Title="NewsResourceEditId",SelectClause="NewsResourceEditId",WhereClause="AllRecords.NewsResourceEditId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsResourceEditId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("From",new SearchColumn(){Name="From",Title="From",SelectClause="From",WhereClause="AllRecords.From",DataType="System.Double?",IsForeignColumn=false,PropertyName="From",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("To",new SearchColumn(){Name="To",Title="To",SelectClause="To",WhereClause="AllRecords.To",DataType="System.Double?",IsForeignColumn=false,PropertyName="To",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("isactive",new SearchColumn(){Name="isactive",Title="isactive",SelectClause="isactive",WhereClause="AllRecords.isactive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Isactive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetResourceEditclipinfoSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetResourceEditclipinfoBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetResourceEditclipinfoAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetResourceEditclipinfoSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ResourceEditclipinfo].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ResourceEditclipinfo].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ResourceEditclipinfo> GetResourceEditclipinfoByNewsResourceEditId(System.Int32? NewsResourceEditId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceEditclipinfoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ResourceEditclipinfo] with (nolock)  where NewsResourceEditId=@NewsResourceEditId  ";
			SqlParameter parameter=new SqlParameter("@NewsResourceEditId",NewsResourceEditId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceEditclipinfo>(ds,ResourceEditclipinfoFromDataRow);
		}

        public virtual List<ResourceEditclipinfo> GetByNewsguid(System.String NewsGuid, string SelectClause = null)
        {

            string sql = @"select rei.* from [ResourceEditclipinfo]  rei (nolock)
                            inner join NewsResourceEdit nre (nolock) on nre.NewsResourceEditId = rei.NewsResourceEditId
                            where nre.NewsGuid = @NewsGuid";
            SqlParameter parameter = new SqlParameter("@NewsGuid", NewsGuid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ResourceEditclipinfo>(ds, ResourceEditclipinfoFromDataRow);
        }

		public virtual ResourceEditclipinfo GetResourceEditclipinfo(System.Int32 ResourceEditclipinfoId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceEditclipinfoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ResourceEditclipinfo] with (nolock)  where ResourceEditclipinfoId=@ResourceEditclipinfoId ";
			SqlParameter parameter=new SqlParameter("@ResourceEditclipinfoId",ResourceEditclipinfoId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ResourceEditclipinfoFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ResourceEditclipinfo> GetResourceEditclipinfoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceEditclipinfoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ResourceEditclipinfo] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceEditclipinfo>(ds,ResourceEditclipinfoFromDataRow);
		}

		public virtual List<ResourceEditclipinfo> GetAllResourceEditclipinfo(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceEditclipinfoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ResourceEditclipinfo] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceEditclipinfo>(ds, ResourceEditclipinfoFromDataRow);
		}

		public virtual List<ResourceEditclipinfo> GetPagedResourceEditclipinfo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetResourceEditclipinfoCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ResourceEditclipinfoId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ResourceEditclipinfoId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ResourceEditclipinfoId] ";
            tempsql += " FROM [ResourceEditclipinfo] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ResourceEditclipinfoId"))
					tempsql += " , (AllRecords.[ResourceEditclipinfoId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ResourceEditclipinfoId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetResourceEditclipinfoSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ResourceEditclipinfo] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ResourceEditclipinfo].[ResourceEditclipinfoId] = PageIndex.[ResourceEditclipinfoId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceEditclipinfo>(ds, ResourceEditclipinfoFromDataRow);
			}else{ return null;}
		}

		private int GetResourceEditclipinfoCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ResourceEditclipinfo as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ResourceEditclipinfo as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ResourceEditclipinfo))]
		public virtual ResourceEditclipinfo InsertResourceEditclipinfo(ResourceEditclipinfo entity)
		{

			ResourceEditclipinfo other=new ResourceEditclipinfo();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ResourceEditclipinfo ( [NewsResourceEditId]
				,[From]
				,[To]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive] )
				Values
				( @NewsResourceEditId
				, @From
				, @To
				, @CreationDate
				, @LastUpdateDate
				, @isactive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsResourceEditId",entity.NewsResourceEditId ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetResourceEditclipinfo(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ResourceEditclipinfo))]
		public virtual ResourceEditclipinfo UpdateResourceEditclipinfo(ResourceEditclipinfo entity)
		{

			if (entity.IsTransient()) return entity;
			ResourceEditclipinfo other = GetResourceEditclipinfo(entity.ResourceEditclipinfoId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ResourceEditclipinfo set  [NewsResourceEditId]=@NewsResourceEditId
							, [From]=@From
							, [To]=@To
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive 
							 where ResourceEditclipinfoId=@ResourceEditclipinfoId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsResourceEditId",entity.NewsResourceEditId ?? (object)DBNull.Value)
					, new SqlParameter("@From",entity.From ?? (object)DBNull.Value)
					, new SqlParameter("@To",entity.To ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceEditclipinfoId",entity.ResourceEditclipinfoId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetResourceEditclipinfo(entity.ResourceEditclipinfoId);
		}

		public virtual bool DeleteResourceEditclipinfo(System.Int32 ResourceEditclipinfoId)
		{

			string sql="delete from ResourceEditclipinfo where ResourceEditclipinfoId=@ResourceEditclipinfoId";
			SqlParameter parameter=new SqlParameter("@ResourceEditclipinfoId",ResourceEditclipinfoId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ResourceEditclipinfo))]
		public virtual ResourceEditclipinfo DeleteResourceEditclipinfo(ResourceEditclipinfo entity)
		{
			this.DeleteResourceEditclipinfo(entity.ResourceEditclipinfoId);
			return entity;
		}


		public virtual ResourceEditclipinfo ResourceEditclipinfoFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ResourceEditclipinfo entity=new ResourceEditclipinfo();
			if (dr.Table.Columns.Contains("ResourceEditclipinfoId"))
			{
			entity.ResourceEditclipinfoId = (System.Int32)dr["ResourceEditclipinfoId"];
			}
			if (dr.Table.Columns.Contains("NewsResourceEditId"))
			{
			entity.NewsResourceEditId = dr["NewsResourceEditId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsResourceEditId"];
			}
			if (dr.Table.Columns.Contains("From"))
			{
			entity.From = dr["From"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["From"];
			}
			if (dr.Table.Columns.Contains("To"))
			{
			entity.To = dr["To"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["To"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("isactive"))
			{
			entity.Isactive = dr["isactive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["isactive"];
			}
			return entity;
		}

	}
	
	
}
