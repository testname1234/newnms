﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class ResourceEditclipinfoRepository: ResourceEditclipinfoRepositoryBase, IResourceEditclipinfoRepository
	{
        public virtual bool DeleteByNewsResourceEditId(System.Int32 NewsResourceEditId)
        {

            string sql = "delete from ResourceEditclipinfo where NewsResourceEditId=@NewsResourceEditId";
            SqlParameter parameter = new SqlParameter("@NewsResourceEditId", NewsResourceEditId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
	}
	
	
}
