﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Repository;

namespace NMS.Repository
{

    public partial class ResourceRepository : ResourceRepositoryBase, IResourceRepository
    {
        public ResourceRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

        public ResourceRepository()
        {
        }

        public virtual DateTime GetLastResourceDate()
        {

            string sql = "select top 1 CreationDate from Resource with (nolock) order by 1 desc";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return Convert.ToDateTime("1/1/2014");
            return Convert.ToDateTime(ds.Tables[0].Rows[0]["CreationDate"]);
        }

        public virtual Resource GetResourceBuGuid(string Guid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += " from Resource with (nolock) where Guid = @Guid order by 1 desc";
            SqlParameter parameter = new SqlParameter("@Guid", Guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual List<Resource> GetByUpdatedDate(System.DateTime UpdatedDate, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from Resource with (nolock)  where LastUpdateDate > @UpdatedDate";
            SqlParameter parameter = new SqlParameter("@UpdatedDate", UpdatedDate);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Resource>(ds, ResourceFromDataRow);
        }

        public virtual void InsertResourceNoReturn(Resource entity)
        {
            string sql = @"Insert into Resource ( [Guid]
				,[ResourceTypeId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Guid
				, @ResourceTypeId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");

        }

        public virtual void UpdateResourceNoReturn(Resource entity)
        {            
            string sql = @"Update Resource set  [Guid]=@Guid
							, [ResourceTypeId]=@ResourceTypeId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ResourceId=@ResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ResourceId",entity.ResourceId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

    }


}
