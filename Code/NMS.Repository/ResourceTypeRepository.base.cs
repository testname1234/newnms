﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ResourceTypeRepositoryBase : Repository, IResourceTypeRepositoryBase 
	{
        
        public ResourceTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ResourceTypeId",new SearchColumn(){Name="ResourceTypeId",Title="ResourceTypeId",SelectClause="ResourceTypeId",WhereClause="AllRecords.ResourceTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ResourceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceType",new SearchColumn(){Name="ResourceType",Title="ResourceType",SelectClause="ResourceType",WhereClause="AllRecords.ResourceType",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetResourceTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetResourceTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetResourceTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetResourceTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ResourceType."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",ResourceType."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ResourceType GetResourceType(System.Int32 ResourceTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ResourceType with (nolock)  where ResourceTypeId=@ResourceTypeId ";
			SqlParameter parameter=new SqlParameter("@ResourceTypeId",ResourceTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ResourceTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ResourceType> GetResourceTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ResourceType with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceType>(ds,ResourceTypeFromDataRow);
		}

		public virtual List<ResourceType> GetAllResourceType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetResourceTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ResourceType with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceType>(ds, ResourceTypeFromDataRow);
		}

		public virtual List<ResourceType> GetPagedResourceType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetResourceTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ResourceTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ResourceTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ResourceTypeId] ";
            tempsql += " FROM [ResourceType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ResourceTypeId"))
					tempsql += " , (AllRecords.[ResourceTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ResourceTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetResourceTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ResourceType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ResourceType].[ResourceTypeId] = PageIndex.[ResourceTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ResourceType>(ds, ResourceTypeFromDataRow);
			}else{ return null;}
		}

		private int GetResourceTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ResourceType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ResourceType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ResourceType))]
		public virtual ResourceType InsertResourceType(ResourceType entity)
		{

			ResourceType other=new ResourceType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ResourceType ( [ResourceType] )
				Values
				( @ResourceType );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceType",entity.ResourceType)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetResourceType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ResourceType))]
		public virtual ResourceType UpdateResourceType(ResourceType entity)
		{

			if (entity.IsTransient()) return entity;
			ResourceType other = GetResourceType(entity.ResourceTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ResourceType set  [ResourceType]=@ResourceType 
							 where ResourceTypeId=@ResourceTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ResourceType",entity.ResourceType)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetResourceType(entity.ResourceTypeId);
		}

		public virtual bool DeleteResourceType(System.Int32 ResourceTypeId)
		{

			string sql="delete from ResourceType with (nolock) where ResourceTypeId=@ResourceTypeId";
			SqlParameter parameter=new SqlParameter("@ResourceTypeId",ResourceTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ResourceType))]
		public virtual ResourceType DeleteResourceType(ResourceType entity)
		{
			this.DeleteResourceType(entity.ResourceTypeId);
			return entity;
		}


		public virtual ResourceType ResourceTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ResourceType entity=new ResourceType();
			if (dr.Table.Columns.Contains("ResourceTypeId"))
			{
			entity.ResourceTypeId = (System.Int32)dr["ResourceTypeId"];
			}
			if (dr.Table.Columns.Contains("ResourceType"))
			{
			entity.ResourceType = dr["ResourceType"].ToString();
			}
			return entity;
		}

	}
	
	
}
