﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class RunOrderLogRepositoryBase : Repository, IRunOrderLogRepositoryBase 
	{
        
        public RunOrderLogRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("RunOrderLogId",new SearchColumn(){Name="RunOrderLogId",Title="RunOrderLogId",SelectClause="RunOrderLogId",WhereClause="AllRecords.RunOrderLogId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RunOrderLogId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("groupId",new SearchColumn(){Name="groupId",Title="groupId",SelectClause="groupId",WhereClause="AllRecords.groupId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("messageId",new SearchColumn(){Name="messageId",Title="messageId",SelectClause="messageId",WhereClause="AllRecords.messageId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="MessageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("mosId",new SearchColumn(){Name="mosId",Title="mosId",SelectClause="mosId",WhereClause="AllRecords.mosId",DataType="System.String",IsForeignColumn=false,PropertyName="MosId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ncsId",new SearchColumn(){Name="ncsId",Title="ncsId",SelectClause="ncsId",WhereClause="AllRecords.ncsId",DataType="System.String",IsForeignColumn=false,PropertyName="NcsId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("roStoriesCount",new SearchColumn(){Name="roStoriesCount",Title="roStoriesCount",SelectClause="roStoriesCount",WhereClause="AllRecords.roStoriesCount",DataType="System.Int32?",IsForeignColumn=false,PropertyName="RoStoriesCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("roStatus",new SearchColumn(){Name="roStatus",Title="roStatus",SelectClause="roStatus",WhereClause="AllRecords.roStatus",DataType="System.String",IsForeignColumn=false,PropertyName="RoStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("roId",new SearchColumn(){Name="roId",Title="roId",SelectClause="roId",WhereClause="AllRecords.roId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="RoId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetRunOrderLogSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetRunOrderLogBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetRunOrderLogAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetRunOrderLogSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[RunOrderLog].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[RunOrderLog].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual RunOrderLog GetRunOrderLog(System.Int32 RunOrderLogId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRunOrderLogSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RunOrderLog] with (nolock)  where RunOrderLogId=@RunOrderLogId ";
			SqlParameter parameter=new SqlParameter("@RunOrderLogId",RunOrderLogId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return RunOrderLogFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<RunOrderLog> GetRunOrderLogByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRunOrderLogSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [RunOrderLog] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrderLog>(ds,RunOrderLogFromDataRow);
		}

		public virtual List<RunOrderLog> GetAllRunOrderLog(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRunOrderLogSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RunOrderLog] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrderLog>(ds, RunOrderLogFromDataRow);
		}

		public virtual List<RunOrderLog> GetPagedRunOrderLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetRunOrderLogCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [RunOrderLogId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([RunOrderLogId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [RunOrderLogId] ";
            tempsql += " FROM [RunOrderLog] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("RunOrderLogId"))
					tempsql += " , (AllRecords.[RunOrderLogId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[RunOrderLogId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetRunOrderLogSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [RunOrderLog] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [RunOrderLog].[RunOrderLogId] = PageIndex.[RunOrderLogId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrderLog>(ds, RunOrderLogFromDataRow);
			}else{ return null;}
		}

		private int GetRunOrderLogCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM RunOrderLog as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM RunOrderLog as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(RunOrderLog))]
		public virtual RunOrderLog InsertRunOrderLog(RunOrderLog entity)
		{

			RunOrderLog other=new RunOrderLog();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into RunOrderLog ( [groupId]
				,[messageId]
				,[mosId]
				,[ncsId]
				,[roStoriesCount]
				,[roStatus]
				,[roId] )
				Values
				( @groupId
				, @messageId
				, @mosId
				, @ncsId
				, @roStoriesCount
				, @roStatus
				, @roId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@groupId",entity.GroupId ?? (object)DBNull.Value)
					, new SqlParameter("@messageId",entity.MessageId ?? (object)DBNull.Value)
					, new SqlParameter("@mosId",entity.MosId ?? (object)DBNull.Value)
					, new SqlParameter("@ncsId",entity.NcsId ?? (object)DBNull.Value)
					, new SqlParameter("@roStoriesCount",entity.RoStoriesCount ?? (object)DBNull.Value)
					, new SqlParameter("@roStatus",entity.RoStatus ?? (object)DBNull.Value)
					, new SqlParameter("@roId",entity.RoId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetRunOrderLog(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(RunOrderLog))]
		public virtual RunOrderLog UpdateRunOrderLog(RunOrderLog entity)
		{

			if (entity.IsTransient()) return entity;
			RunOrderLog other = GetRunOrderLog(entity.RunOrderLogId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update RunOrderLog set  [groupId]=@groupId
							, [messageId]=@messageId
							, [mosId]=@mosId
							, [ncsId]=@ncsId
							, [roStoriesCount]=@roStoriesCount
							, [roStatus]=@roStatus
							, [roId]=@roId 
							 where RunOrderLogId=@RunOrderLogId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@groupId",entity.GroupId ?? (object)DBNull.Value)
					, new SqlParameter("@messageId",entity.MessageId ?? (object)DBNull.Value)
					, new SqlParameter("@mosId",entity.MosId ?? (object)DBNull.Value)
					, new SqlParameter("@ncsId",entity.NcsId ?? (object)DBNull.Value)
					, new SqlParameter("@roStoriesCount",entity.RoStoriesCount ?? (object)DBNull.Value)
					, new SqlParameter("@roStatus",entity.RoStatus ?? (object)DBNull.Value)
					, new SqlParameter("@roId",entity.RoId ?? (object)DBNull.Value)
					, new SqlParameter("@RunOrderLogId",entity.RunOrderLogId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetRunOrderLog(entity.RunOrderLogId);
		}

		public virtual bool DeleteRunOrderLog(System.Int32 RunOrderLogId)
		{

			string sql="delete from RunOrderLog where RunOrderLogId=@RunOrderLogId";
			SqlParameter parameter=new SqlParameter("@RunOrderLogId",RunOrderLogId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(RunOrderLog))]
		public virtual RunOrderLog DeleteRunOrderLog(RunOrderLog entity)
		{
			this.DeleteRunOrderLog(entity.RunOrderLogId);
			return entity;
		}


		public virtual RunOrderLog RunOrderLogFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			RunOrderLog entity=new RunOrderLog();
			if (dr.Table.Columns.Contains("RunOrderLogId"))
			{
			entity.RunOrderLogId = (System.Int32)dr["RunOrderLogId"];
			}
			if (dr.Table.Columns.Contains("groupId"))
			{
			entity.GroupId = dr["groupId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["groupId"];
			}
			if (dr.Table.Columns.Contains("messageId"))
			{
			entity.MessageId = dr["messageId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["messageId"];
			}
			if (dr.Table.Columns.Contains("mosId"))
			{
			entity.MosId = dr["mosId"].ToString();
			}
			if (dr.Table.Columns.Contains("ncsId"))
			{
			entity.NcsId = dr["ncsId"].ToString();
			}
			if (dr.Table.Columns.Contains("roStoriesCount"))
			{
			entity.RoStoriesCount = dr["roStoriesCount"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["roStoriesCount"];
			}
			if (dr.Table.Columns.Contains("roStatus"))
			{
			entity.RoStatus = dr["roStatus"].ToString();
			}
			if (dr.Table.Columns.Contains("roId"))
			{
			entity.RoId = dr["roId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["roId"];
			}
			return entity;
		}

	}
	
	
}
