﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class RunOrderStoriesRepositoryBase : Repository, IRunOrderStoriesRepositoryBase 
	{
        
        public RunOrderStoriesRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("RunOrderStoryId",new SearchColumn(){Name="RunOrderStoryId",Title="RunOrderStoryId",SelectClause="RunOrderStoryId",WhereClause="AllRecords.RunOrderStoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RunOrderStoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RunOrderLogId",new SearchColumn(){Name="RunOrderLogId",Title="RunOrderLogId",SelectClause="RunOrderLogId",WhereClause="AllRecords.RunOrderLogId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RunOrderLogId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ItemChannel",new SearchColumn(){Name="ItemChannel",Title="ItemChannel",SelectClause="ItemChannel",WhereClause="AllRecords.ItemChannel",DataType="System.String",IsForeignColumn=false,PropertyName="ItemChannel",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ItemId",new SearchColumn(){Name="ItemId",Title="ItemId",SelectClause="ItemId",WhereClause="AllRecords.ItemId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ItemId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ObjId",new SearchColumn(){Name="ObjId",Title="ObjId",SelectClause="ObjId",WhereClause="AllRecords.ObjId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ObjId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Status",new SearchColumn(){Name="Status",Title="Status",SelectClause="Status",WhereClause="AllRecords.Status",DataType="System.String",IsForeignColumn=false,PropertyName="Status",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StoryId",new SearchColumn(){Name="StoryId",Title="StoryId",SelectClause="StoryId",WhereClause="AllRecords.StoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="StoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StoryStatus",new SearchColumn(){Name="StoryStatus",Title="StoryStatus",SelectClause="StoryStatus",WhereClause="AllRecords.StoryStatus",DataType="System.String",IsForeignColumn=false,PropertyName="StoryStatus",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetRunOrderStoriesSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetRunOrderStoriesBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetRunOrderStoriesAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetRunOrderStoriesSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[RunOrderStories].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[RunOrderStories].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<RunOrderStories> GetRunOrderStoriesByRunOrderLogId(System.Int32 RunOrderLogId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRunOrderStoriesSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RunOrderStories] with (nolock)  where RunOrderLogId=@RunOrderLogId  ";
			SqlParameter parameter=new SqlParameter("@RunOrderLogId",RunOrderLogId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrderStories>(ds,RunOrderStoriesFromDataRow);
		}

		public virtual RunOrderStories GetRunOrderStories(System.Int32 RunOrderStoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRunOrderStoriesSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RunOrderStories] with (nolock)  where RunOrderStoryId=@RunOrderStoryId ";
			SqlParameter parameter=new SqlParameter("@RunOrderStoryId",RunOrderStoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return RunOrderStoriesFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<RunOrderStories> GetRunOrderStoriesByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRunOrderStoriesSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [RunOrderStories] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrderStories>(ds,RunOrderStoriesFromDataRow);
		}

		public virtual List<RunOrderStories> GetAllRunOrderStories(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetRunOrderStoriesSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [RunOrderStories] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrderStories>(ds, RunOrderStoriesFromDataRow);
		}

		public virtual List<RunOrderStories> GetPagedRunOrderStories(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetRunOrderStoriesCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [RunOrderStoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([RunOrderStoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [RunOrderStoryId] ";
            tempsql += " FROM [RunOrderStories] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("RunOrderStoryId"))
					tempsql += " , (AllRecords.[RunOrderStoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[RunOrderStoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetRunOrderStoriesSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [RunOrderStories] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [RunOrderStories].[RunOrderStoryId] = PageIndex.[RunOrderStoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<RunOrderStories>(ds, RunOrderStoriesFromDataRow);
			}else{ return null;}
		}

		private int GetRunOrderStoriesCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM RunOrderStories as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM RunOrderStories as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(RunOrderStories))]
		public virtual RunOrderStories InsertRunOrderStories(RunOrderStories entity)
		{

			RunOrderStories other=new RunOrderStories();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into RunOrderStories ( [RunOrderLogId]
				,[ItemChannel]
				,[ItemId]
				,[ObjId]
				,[Status]
				,[StoryId]
				,[StoryStatus] )
				Values
				( @RunOrderLogId
				, @ItemChannel
				, @ItemId
				, @ObjId
				, @Status
				, @StoryId
				, @StoryStatus );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@RunOrderLogId",entity.RunOrderLogId)
					, new SqlParameter("@ItemChannel",entity.ItemChannel ?? (object)DBNull.Value)
					, new SqlParameter("@ItemId",entity.ItemId ?? (object)DBNull.Value)
					, new SqlParameter("@ObjId",entity.ObjId ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@StoryId",entity.StoryId ?? (object)DBNull.Value)
					, new SqlParameter("@StoryStatus",entity.StoryStatus ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetRunOrderStories(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(RunOrderStories))]
		public virtual RunOrderStories UpdateRunOrderStories(RunOrderStories entity)
		{

			if (entity.IsTransient()) return entity;
			RunOrderStories other = GetRunOrderStories(entity.RunOrderStoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update RunOrderStories set  [RunOrderLogId]=@RunOrderLogId
							, [ItemChannel]=@ItemChannel
							, [ItemId]=@ItemId
							, [ObjId]=@ObjId
							, [Status]=@Status
							, [StoryId]=@StoryId
							, [StoryStatus]=@StoryStatus 
							 where RunOrderStoryId=@RunOrderStoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@RunOrderLogId",entity.RunOrderLogId)
					, new SqlParameter("@ItemChannel",entity.ItemChannel ?? (object)DBNull.Value)
					, new SqlParameter("@ItemId",entity.ItemId ?? (object)DBNull.Value)
					, new SqlParameter("@ObjId",entity.ObjId ?? (object)DBNull.Value)
					, new SqlParameter("@Status",entity.Status ?? (object)DBNull.Value)
					, new SqlParameter("@StoryId",entity.StoryId ?? (object)DBNull.Value)
					, new SqlParameter("@StoryStatus",entity.StoryStatus ?? (object)DBNull.Value)
					, new SqlParameter("@RunOrderStoryId",entity.RunOrderStoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetRunOrderStories(entity.RunOrderStoryId);
		}

		public virtual bool DeleteRunOrderStories(System.Int32 RunOrderStoryId)
		{

			string sql="delete from RunOrderStories where RunOrderStoryId=@RunOrderStoryId";
			SqlParameter parameter=new SqlParameter("@RunOrderStoryId",RunOrderStoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(RunOrderStories))]
		public virtual RunOrderStories DeleteRunOrderStories(RunOrderStories entity)
		{
			this.DeleteRunOrderStories(entity.RunOrderStoryId);
			return entity;
		}


		public virtual RunOrderStories RunOrderStoriesFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			RunOrderStories entity=new RunOrderStories();
			if (dr.Table.Columns.Contains("RunOrderStoryId"))
			{
			entity.RunOrderStoryId = (System.Int32)dr["RunOrderStoryId"];
			}
			if (dr.Table.Columns.Contains("RunOrderLogId"))
			{
			entity.RunOrderLogId = (System.Int32)dr["RunOrderLogId"];
			}
			if (dr.Table.Columns.Contains("ItemChannel"))
			{
			entity.ItemChannel = dr["ItemChannel"].ToString();
			}
			if (dr.Table.Columns.Contains("ItemId"))
			{
			entity.ItemId = dr["ItemId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ItemId"];
			}
			if (dr.Table.Columns.Contains("ObjId"))
			{
			entity.ObjId = dr["ObjId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ObjId"];
			}
			if (dr.Table.Columns.Contains("Status"))
			{
			entity.Status = dr["Status"].ToString();
			}
			if (dr.Table.Columns.Contains("StoryId"))
			{
			entity.StoryId = dr["StoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["StoryId"];
			}
			if (dr.Table.Columns.Contains("StoryStatus"))
			{
			entity.StoryStatus = dr["StoryStatus"].ToString();
			}
			return entity;
		}

	}
	
	
}
