﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ScrapMaxDatesRepositoryBase : Repository, IScrapMaxDatesRepositoryBase 
	{
        
        public ScrapMaxDatesRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ScrapMaxDatesId",new SearchColumn(){Name="ScrapMaxDatesId",Title="ScrapMaxDatesId",SelectClause="ScrapMaxDatesId",WhereClause="AllRecords.ScrapMaxDatesId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScrapMaxDatesId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MaxUpdateDate",new SearchColumn(){Name="MaxUpdateDate",Title="MaxUpdateDate",SelectClause="MaxUpdateDate",WhereClause="AllRecords.MaxUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="MaxUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SourceName",new SearchColumn(){Name="SourceName",Title="SourceName",SelectClause="SourceName",WhereClause="AllRecords.SourceName",DataType="System.String",IsForeignColumn=false,PropertyName="SourceName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetScrapMaxDatesSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetScrapMaxDatesBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetScrapMaxDatesAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetScrapMaxDatesSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "ScrapMaxDates."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",ScrapMaxDates."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ScrapMaxDates GetScrapMaxDates(System.Int32 ScrapMaxDatesId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScrapMaxDatesSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ScrapMaxDates with (nolock)  where ScrapMaxDatesId=@ScrapMaxDatesId ";
			SqlParameter parameter=new SqlParameter("@ScrapMaxDatesId",ScrapMaxDatesId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ScrapMaxDatesFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ScrapMaxDates> GetScrapMaxDatesByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScrapMaxDatesSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ScrapMaxDates with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScrapMaxDates>(ds,ScrapMaxDatesFromDataRow);
		}

		public virtual List<ScrapMaxDates> GetAllScrapMaxDates(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScrapMaxDatesSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ScrapMaxDates with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScrapMaxDates>(ds, ScrapMaxDatesFromDataRow);
		}

		public virtual List<ScrapMaxDates> GetPagedScrapMaxDates(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetScrapMaxDatesCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ScrapMaxDatesId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ScrapMaxDatesId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ScrapMaxDatesId] ";
            tempsql += " FROM [ScrapMaxDates] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ScrapMaxDatesId"))
					tempsql += " , (AllRecords.[ScrapMaxDatesId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ScrapMaxDatesId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetScrapMaxDatesSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ScrapMaxDates] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ScrapMaxDates].[ScrapMaxDatesId] = PageIndex.[ScrapMaxDatesId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScrapMaxDates>(ds, ScrapMaxDatesFromDataRow);
			}else{ return null;}
		}

		private int GetScrapMaxDatesCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ScrapMaxDates as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ScrapMaxDates as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ScrapMaxDates))]
		public virtual ScrapMaxDates InsertScrapMaxDates(ScrapMaxDates entity)
		{

			ScrapMaxDates other=new ScrapMaxDates();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ScrapMaxDates ( [MaxUpdateDate]
				,[SourceName] )
				Values
				( @MaxUpdateDate
				, @SourceName );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@MaxUpdateDate",entity.MaxUpdateDate)
					, new SqlParameter("@SourceName",entity.SourceName)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetScrapMaxDates(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ScrapMaxDates))]
		public virtual ScrapMaxDates UpdateScrapMaxDates(ScrapMaxDates entity)
		{

			if (entity.IsTransient()) return entity;
			ScrapMaxDates other = GetScrapMaxDates(entity.ScrapMaxDatesId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ScrapMaxDates set  [MaxUpdateDate]=@MaxUpdateDate
							, [SourceName]=@SourceName 
							 where ScrapMaxDatesId=@ScrapMaxDatesId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@MaxUpdateDate",entity.MaxUpdateDate)
					, new SqlParameter("@SourceName",entity.SourceName)
					, new SqlParameter("@ScrapMaxDatesId",entity.ScrapMaxDatesId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetScrapMaxDates(entity.ScrapMaxDatesId);
		}

		public virtual bool DeleteScrapMaxDates(System.Int32 ScrapMaxDatesId)
		{

			string sql="delete from ScrapMaxDates with (nolock) where ScrapMaxDatesId=@ScrapMaxDatesId";
			SqlParameter parameter=new SqlParameter("@ScrapMaxDatesId",ScrapMaxDatesId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ScrapMaxDates))]
		public virtual ScrapMaxDates DeleteScrapMaxDates(ScrapMaxDates entity)
		{
			this.DeleteScrapMaxDates(entity.ScrapMaxDatesId);
			return entity;
		}


		public virtual ScrapMaxDates ScrapMaxDatesFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ScrapMaxDates entity=new ScrapMaxDates();
			if (dr.Table.Columns.Contains("ScrapMaxDatesId"))
			{
			entity.ScrapMaxDatesId = (System.Int32)dr["ScrapMaxDatesId"];
			}
			if (dr.Table.Columns.Contains("MaxUpdateDate"))
			{
			entity.MaxUpdateDate = (System.DateTime)dr["MaxUpdateDate"];
			}
			if (dr.Table.Columns.Contains("SourceName"))
			{
			entity.SourceName = dr["SourceName"].ToString();
			}
			return entity;
		}

	}
	
	
}
