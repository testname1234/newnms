﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class ScrapMaxDatesRepository : ScrapMaxDatesRepositoryBase, IScrapMaxDatesRepository
    {
        public virtual ScrapMaxDates GetScrapMaxDatesBySource(System.String Source, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetScrapMaxDatesSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from ScrapMaxDates with (nolock)  where SourceName=@SourceName ";
            SqlParameter parameter = new SqlParameter("@SourceName", Source);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ScrapMaxDatesFromDataRow(ds.Tables[0].Rows[0]);
        }


        public virtual void UpdateScrapMaxDatesCustom(ScrapMaxDates entity)
        {
            string sql = @"Update ScrapMaxDates set  
                                [MaxUpdateDate]=@MaxUpdateDate
							 where SourceName=@SourceName";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@MaxUpdateDate",entity.MaxUpdateDate)					
					, new SqlParameter("@SourceName",entity.SourceName)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public virtual ScrapMaxDates UpdateScrapMaxDates(ScrapMaxDates entity)
        {
            string sql = @"Update ScrapMaxDates 
                        set  [MaxUpdateDate]=@MaxUpdateDate							
							 where SourceName=@SourceName";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@MaxUpdateDate",entity.MaxUpdateDate)
					, new SqlParameter("@SourceName",entity.SourceName)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetScrapMaxDatesBySource(entity.SourceName);
        }

        //public ScrapMaxDates GetSourceMaxDateInfo()
//        public List<ScrapMaxDates> GetResult()
//        {

//            string sql = @"select Source,sourcedate, CASE when datediff(hh,sourcedate,GETUTCDATE()) >= 4  
//                    THEN 'false' ELSE 'true'  END as Status from
//                    (select Source, max(LastUpdateDate) sourcedate from news (nolock) where isnull(ReporterId,0) = 0
//                    group by Source)t where Source not in ('karachi','Twitter') order by status";
//            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
//            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
//            return CollectionFromDataSet<ScrapMaxDates>(ds, ScrapMaxDatesFromDataRow);
//        }

        public DataTable GetResult()
        {
            DataTable table = new DataTable();
            SqlConnection consql = new SqlConnection(@"Data Source=10.3.12.123;Initial Catalog=NMS; user id=sa; password=Axact123");
            try
            {
                
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = consql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"select Source,sourcedate, CASE when datediff(hh,sourcedate,GETUTCDATE()) >= 4  
                    THEN 'false' ELSE 'true'  END as Status from
                    (select Source, max(LastUpdateDate) sourcedate from news (nolock) where isnull(ReporterId,0) = 0
                    group by Source)t where Source not in ('karachi','Twitter') order by status";
                consql.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (consql.State == ConnectionState.Open)
                    consql.Close();
            }

        }
    }
}
