﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ScreenElementRepositoryBase : Repository, IScreenElementRepositoryBase 
	{
        
        public ScreenElementRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ScreenElementId",new SearchColumn(){Name="ScreenElementId",Title="ScreenElementId",SelectClause="ScreenElementId",WhereClause="AllRecords.ScreenElementId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScreenElementId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ThumbGuid",new SearchColumn(){Name="ThumbGuid",Title="ThumbGuid",SelectClause="ThumbGuid",WhereClause="AllRecords.ThumbGuid",DataType="System.Guid",IsForeignColumn=false,PropertyName="ThumbGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImageGuid",new SearchColumn(){Name="ImageGuid",Title="ImageGuid",SelectClause="ImageGuid",WhereClause="AllRecords.ImageGuid",DataType="System.Guid?",IsForeignColumn=false,PropertyName="ImageGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowDisplay",new SearchColumn(){Name="AllowDisplay",Title="AllowDisplay",SelectClause="AllowDisplay",WhereClause="AllRecords.AllowDisplay",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="AllowDisplay",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetScreenElementSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetScreenElementBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetScreenElementAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetScreenElementSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ScreenElement].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ScreenElement].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ScreenElement GetScreenElement(System.Int32 ScreenElementId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ScreenElement with (nolock)  where ScreenElementId=@ScreenElementId ";
			SqlParameter parameter=new SqlParameter("@ScreenElementId",ScreenElementId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ScreenElementFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ScreenElement> GetScreenElementByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from ScreenElement with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenElement>(ds,ScreenElementFromDataRow);
		}

		public virtual List<ScreenElement> GetAllScreenElement(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from ScreenElement with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenElement>(ds, ScreenElementFromDataRow);
		}

		public virtual List<ScreenElement> GetPagedScreenElement(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetScreenElementCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ScreenElementId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ScreenElementId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ScreenElementId] ";
            tempsql += " FROM [ScreenElement] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ScreenElementId"))
					tempsql += " , (AllRecords.[ScreenElementId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ScreenElementId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetScreenElementSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ScreenElement] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ScreenElement].[ScreenElementId] = PageIndex.[ScreenElementId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenElement>(ds, ScreenElementFromDataRow);
			}else{ return null;}
		}

		private int GetScreenElementCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ScreenElement as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ScreenElement as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ScreenElement))]
		public virtual ScreenElement InsertScreenElement(ScreenElement entity)
		{

			ScreenElement other=new ScreenElement();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ScreenElement ( [Name]
				,[ThumbGuid]
				,[ImageGuid]
				,[AllowDisplay]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Name
				, @ThumbGuid
				, @ImageGuid
				, @AllowDisplay
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid)
					, new SqlParameter("@ImageGuid",entity.ImageGuid ?? (object)DBNull.Value)
					, new SqlParameter("@AllowDisplay",entity.AllowDisplay ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetScreenElement(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ScreenElement))]
		public virtual ScreenElement UpdateScreenElement(ScreenElement entity)
		{

			if (entity.IsTransient()) return entity;
			ScreenElement other = GetScreenElement(entity.ScreenElementId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ScreenElement set  [Name]=@Name
							, [ThumbGuid]=@ThumbGuid
							, [ImageGuid]=@ImageGuid
							, [AllowDisplay]=@AllowDisplay
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ScreenElementId=@ScreenElementId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid)
					, new SqlParameter("@ImageGuid",entity.ImageGuid ?? (object)DBNull.Value)
					, new SqlParameter("@AllowDisplay",entity.AllowDisplay ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ScreenElementId",entity.ScreenElementId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetScreenElement(entity.ScreenElementId);
		}

		public virtual bool DeleteScreenElement(System.Int32 ScreenElementId)
		{

			string sql="delete from ScreenElement with (nolock) where ScreenElementId=@ScreenElementId";
			SqlParameter parameter=new SqlParameter("@ScreenElementId",ScreenElementId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ScreenElement))]
		public virtual ScreenElement DeleteScreenElement(ScreenElement entity)
		{
			this.DeleteScreenElement(entity.ScreenElementId);
			return entity;
		}


		public virtual ScreenElement ScreenElementFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ScreenElement entity=new ScreenElement();
			if (dr.Table.Columns.Contains("ScreenElementId"))
			{
			entity.ScreenElementId = (System.Int32)dr["ScreenElementId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("ThumbGuid"))
			{
			entity.ThumbGuid = (System.Guid)dr["ThumbGuid"];
			}
			if (dr.Table.Columns.Contains("ImageGuid"))
			{
			entity.ImageGuid = dr["ImageGuid"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["ImageGuid"];
			}
			if (dr.Table.Columns.Contains("AllowDisplay"))
			{
			entity.AllowDisplay = dr["AllowDisplay"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["AllowDisplay"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
