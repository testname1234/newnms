﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ScreenTemplateRepositoryBase : Repository, IScreenTemplateRepositoryBase 
	{
        
        public ScreenTemplateRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ScreenTemplateId",new SearchColumn(){Name="ScreenTemplateId",Title="ScreenTemplateId",SelectClause="ScreenTemplateId",WhereClause="AllRecords.ScreenTemplateId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsDefault",new SearchColumn(){Name="IsDefault",Title="IsDefault",SelectClause="IsDefault",WhereClause="AllRecords.IsDefault",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsDefault",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Html",new SearchColumn(){Name="Html",Title="Html",SelectClause="Html",WhereClause="AllRecords.Html",DataType="System.String",IsForeignColumn=false,PropertyName="Html",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Description",new SearchColumn(){Name="Description",Title="Description",SelectClause="Description",WhereClause="AllRecords.Description",DataType="System.String",IsForeignColumn=false,PropertyName="Description",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ThumbGuid",new SearchColumn(){Name="ThumbGuid",Title="ThumbGuid",SelectClause="ThumbGuid",WhereClause="AllRecords.ThumbGuid",DataType="System.Guid",IsForeignColumn=false,PropertyName="ThumbGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("GroupId",new SearchColumn(){Name="GroupId",Title="GroupId",SelectClause="GroupId",WhereClause="AllRecords.GroupId",DataType="System.String",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AssetId",new SearchColumn(){Name="AssetId",Title="AssetId",SelectClause="AssetId",WhereClause="AllRecords.AssetId",DataType="System.String",IsForeignColumn=false,PropertyName="AssetId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BackgroundColor",new SearchColumn(){Name="BackgroundColor",Title="BackgroundColor",SelectClause="BackgroundColor",WhereClause="AllRecords.BackgroundColor",DataType="System.String",IsForeignColumn=false,PropertyName="BackgroundColor",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BackgroundImageUrl",new SearchColumn(){Name="BackgroundImageUrl",Title="BackgroundImageUrl",SelectClause="BackgroundImageUrl",WhereClause="AllRecords.BackgroundImageUrl",DataType="System.Guid?",IsForeignColumn=false,PropertyName="BackgroundImageUrl",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BackgroundRepeat",new SearchColumn(){Name="BackgroundRepeat",Title="BackgroundRepeat",SelectClause="BackgroundRepeat",WhereClause="AllRecords.BackgroundRepeat",DataType="System.String",IsForeignColumn=false,PropertyName="BackgroundRepeat",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatonDate",new SearchColumn(){Name="CreatonDate",Title="CreatonDate",SelectClause="CreatonDate",WhereClause="AllRecords.CreatonDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreatonDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("flashtemplateid",new SearchColumn(){Name="flashtemplateid",Title="flashtemplateid",SelectClause="flashtemplateid",WhereClause="AllRecords.flashtemplateid",DataType="System.Int32",IsForeignColumn=false,PropertyName="Flashtemplateid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RatioTypeId",new SearchColumn(){Name="RatioTypeId",Title="RatioTypeId",SelectClause="RatioTypeId",WhereClause="AllRecords.RatioTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="RatioTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsVideoWallTemplate",new SearchColumn(){Name="IsVideoWallTemplate",Title="IsVideoWallTemplate",SelectClause="IsVideoWallTemplate",WhereClause="AllRecords.IsVideoWallTemplate",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsVideoWallTemplate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetScreenTemplateSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetScreenTemplateBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetScreenTemplateAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetScreenTemplateSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ScreenTemplate].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ScreenTemplate].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ScreenTemplate> GetScreenTemplateByProgramId(System.Int32 ProgramId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplate] with (nolock)  where ProgramId=@ProgramId  ";
			SqlParameter parameter=new SqlParameter("@ProgramId",ProgramId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplate>(ds,ScreenTemplateFromDataRow);
		}

		public virtual ScreenTemplate GetScreenTemplate(System.Int32 ScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplate] with (nolock)  where ScreenTemplateId=@ScreenTemplateId ";
			SqlParameter parameter=new SqlParameter("@ScreenTemplateId",ScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ScreenTemplateFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ScreenTemplate> GetScreenTemplateByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ScreenTemplate] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplate>(ds,ScreenTemplateFromDataRow);
		}

		public virtual List<ScreenTemplate> GetAllScreenTemplate(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplate] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplate>(ds, ScreenTemplateFromDataRow);
		}

		public virtual List<ScreenTemplate> GetPagedScreenTemplate(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetScreenTemplateCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ScreenTemplateId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ScreenTemplateId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ScreenTemplateId] ";
            tempsql += " FROM [ScreenTemplate] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ScreenTemplateId"))
					tempsql += " , (AllRecords.[ScreenTemplateId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ScreenTemplateId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ScreenTemplate] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ScreenTemplate].[ScreenTemplateId] = PageIndex.[ScreenTemplateId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplate>(ds, ScreenTemplateFromDataRow);
			}else{ return null;}
		}

		private int GetScreenTemplateCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ScreenTemplate as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ScreenTemplate as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ScreenTemplate))]
		public virtual ScreenTemplate InsertScreenTemplate(ScreenTemplate entity)
		{

			ScreenTemplate other=new ScreenTemplate();
			other = entity;
				string sql=@"Insert into ScreenTemplate ( [ScreenTemplateId]
				,[ProgramId]
				,[Name]
				,[IsDefault]
				,[Html]
				,[Description]
				,[Duration]
				,[ThumbGuid]
				,[GroupId]
				,[AssetId]
				,[BackgroundColor]
				,[BackgroundImageUrl]
				,[BackgroundRepeat]
				,[CreatonDate]
				,[LastUpdateDate]
				,[IsActive]
				,[flashtemplateid]
				,[RatioTypeId]
				,[IsVideoWallTemplate] )
				Values
				( @ScreenTemplateId
				, @ProgramId
				, @Name
				, @IsDefault
				, @Html
				, @Description
				, @Duration
				, @ThumbGuid
				, @GroupId
				, @AssetId
				, @BackgroundColor
				, @BackgroundImageUrl
				, @BackgroundRepeat
				, @CreatonDate
				, @LastUpdateDate
				, @IsActive
				, @flashtemplateid
				, @RatioTypeId
				, @IsVideoWallTemplate );
";
				SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@IsDefault",entity.IsDefault)
					, new SqlParameter("@Html",entity.Html)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@AssetId",entity.AssetId ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundColor",entity.BackgroundColor ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundImageUrl",entity.BackgroundImageUrl ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundRepeat",entity.BackgroundRepeat ?? (object)DBNull.Value)
					, new SqlParameter("@CreatonDate",entity.CreatonDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@flashtemplateid",entity.Flashtemplateid)
					, new SqlParameter("@RatioTypeId",entity.RatioTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVideoWallTemplate",entity.IsVideoWallTemplate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetScreenTemplate(Convert.ToInt32(identity));
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ScreenTemplate))]
		public virtual ScreenTemplate UpdateScreenTemplate(ScreenTemplate entity)
		{

			ScreenTemplate other = GetScreenTemplate(entity.ScreenTemplateId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ScreenTemplate set  [ProgramId]=@ProgramId
							, [Name]=@Name
							, [IsDefault]=@IsDefault
							, [Html]=@Html
							, [Description]=@Description
							, [Duration]=@Duration
							, [ThumbGuid]=@ThumbGuid
							, [GroupId]=@GroupId
							, [AssetId]=@AssetId
							, [BackgroundColor]=@BackgroundColor
							, [BackgroundImageUrl]=@BackgroundImageUrl
							, [BackgroundRepeat]=@BackgroundRepeat
							, [CreatonDate]=@CreatonDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [flashtemplateid]=@flashtemplateid
							, [RatioTypeId]=@RatioTypeId
							, [IsVideoWallTemplate]=@IsVideoWallTemplate 
							 where ScreenTemplateId=@ScreenTemplateId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId)
					, new SqlParameter("@ProgramId",entity.ProgramId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@IsDefault",entity.IsDefault)
					, new SqlParameter("@Html",entity.Html)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@AssetId",entity.AssetId ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundColor",entity.BackgroundColor ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundImageUrl",entity.BackgroundImageUrl ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundRepeat",entity.BackgroundRepeat ?? (object)DBNull.Value)
					, new SqlParameter("@CreatonDate",entity.CreatonDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@flashtemplateid",entity.Flashtemplateid)
					, new SqlParameter("@RatioTypeId",entity.RatioTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVideoWallTemplate",entity.IsVideoWallTemplate ?? (object)DBNull.Value)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetScreenTemplate(entity.ScreenTemplateId);
		}

		public virtual bool DeleteScreenTemplate(System.Int32 ScreenTemplateId)
		{

			string sql="delete from ScreenTemplate where ScreenTemplateId=@ScreenTemplateId";
			SqlParameter parameter=new SqlParameter("@ScreenTemplateId",ScreenTemplateId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ScreenTemplate))]
		public virtual ScreenTemplate DeleteScreenTemplate(ScreenTemplate entity)
		{
			this.DeleteScreenTemplate(entity.ScreenTemplateId);
			return entity;
		}


		public virtual ScreenTemplate ScreenTemplateFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ScreenTemplate entity=new ScreenTemplate();
			if (dr.Table.Columns.Contains("ScreenTemplateId"))
			{
			entity.ScreenTemplateId = (System.Int32)dr["ScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = (System.Int32)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("IsDefault"))
			{
			entity.IsDefault = (System.Boolean)dr["IsDefault"];
			}
			if (dr.Table.Columns.Contains("Html"))
			{
			entity.Html = dr["Html"].ToString();
			}
			if (dr.Table.Columns.Contains("Description"))
			{
			entity.Description = dr["Description"].ToString();
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("ThumbGuid"))
			{
			entity.ThumbGuid = (System.Guid)dr["ThumbGuid"];
			}
			if (dr.Table.Columns.Contains("GroupId"))
			{
			entity.GroupId = dr["GroupId"].ToString();
			}
			if (dr.Table.Columns.Contains("AssetId"))
			{
			entity.AssetId = dr["AssetId"].ToString();
			}
			if (dr.Table.Columns.Contains("BackgroundColor"))
			{
			entity.BackgroundColor = dr["BackgroundColor"].ToString();
			}
			if (dr.Table.Columns.Contains("BackgroundImageUrl"))
			{
			entity.BackgroundImageUrl = dr["BackgroundImageUrl"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["BackgroundImageUrl"];
			}
			if (dr.Table.Columns.Contains("BackgroundRepeat"))
			{
			entity.BackgroundRepeat = dr["BackgroundRepeat"].ToString();
			}
			if (dr.Table.Columns.Contains("CreatonDate"))
			{
			entity.CreatonDate = (System.DateTime)dr["CreatonDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("flashtemplateid"))
			{
			entity.Flashtemplateid = (System.Int32)dr["flashtemplateid"];
			}
			if (dr.Table.Columns.Contains("RatioTypeId"))
			{
			entity.RatioTypeId = dr["RatioTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["RatioTypeId"];
			}
			if (dr.Table.Columns.Contains("IsVideoWallTemplate"))
			{
			entity.IsVideoWallTemplate = dr["IsVideoWallTemplate"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsVideoWallTemplate"];
			}
			return entity;
		}

	}
	
	
}
