﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class ScreenTemplateRepository: ScreenTemplateRepositoryBase, IScreenTemplateRepository
	{
        public virtual void UpdateScreenTemplateGroup(string GroupId, int flashtemplateid)
        {
            string sql = @"Update ScreenTemplate set  [GroupId]=@GroupId							
							 where flashtemplateid=@flashtemplateid";
            SqlParameter[] parameterArray = new SqlParameter[]{				
					new SqlParameter("@GroupId",GroupId)				
					, new SqlParameter("@flashtemplateid",flashtemplateid)};				
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }

        public override List<ScreenTemplate> GetScreenTemplateByProgramId(System.Int32 ProgramId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetScreenTemplateSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [ScreenTemplate] with (nolock)  where ProgramId=@ProgramId and isactive=1";
            SqlParameter parameter = new SqlParameter("@ProgramId", ProgramId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ScreenTemplate>(ds, ScreenTemplateFromDataRow);
        }

        public List<ScreenTemplate> GetScreenTemplate(int ProgramId, string groupId)
        {
            string sql = GetScreenTemplateSelectClause();
            sql += string.Format(" from [ScreenTemplate] with (nolock)  where ProgramId = @ProgramId and GroupId = '{0}' and isactive = 1", groupId);
            SqlParameter parameter = new SqlParameter("@ProgramId", ProgramId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<ScreenTemplate>(ds, ScreenTemplateFromDataRow);
        }

        public ScreenTemplate GetScreenTemplateByFlashTemplateId(int programId,int flashTemplateID)
        {
            string sql = "select * from [ScreenTemplate] with (nolock)  where ProgramId=@ProgramId and flashTemplateID=@flashTemplateID and isactive=1";
            SqlParameter parameter = new SqlParameter("@ProgramId", programId);
            SqlParameter parameter1 = new SqlParameter("@flashTemplateID", flashTemplateID);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ScreenTemplateFromDataRow(ds.Tables[0].Rows[0]);
        }

        public ScreenTemplate GetbyScreenTemplateId(int screenTemplateId)
        {
            string sql = "select * from [ScreenTemplate] with (nolock)  where ScreenTemplateId=@ScreenTemplateId and isactive=1";
            SqlParameter parameter = new SqlParameter("@ScreenTemplateId", screenTemplateId);            
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter});
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ScreenTemplateFromDataRow(ds.Tables[0].Rows[0]);
        }


        public DateTime GetMaxLastUpdateDate()
        {
            string sql = "select max(LastUpdateDate) from [ScreenTemplate]";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return DateTime.UtcNow.AddYears(-1);
            return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
        }
    }
	
	
}
