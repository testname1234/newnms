﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ScreenTemplateStatusRepositoryBase : Repository, IScreenTemplateStatusRepositoryBase 
	{
        
        public ScreenTemplateStatusRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ScreenTemplateStatusId",new SearchColumn(){Name="ScreenTemplateStatusId",Title="ScreenTemplateStatusId",SelectClause="ScreenTemplateStatusId",WhereClause="AllRecords.ScreenTemplateStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScreenTemplateStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetScreenTemplateStatusSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetScreenTemplateStatusBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetScreenTemplateStatusAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetScreenTemplateStatusSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ScreenTemplateStatus].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ScreenTemplateStatus].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ScreenTemplateStatus GetScreenTemplateStatus(System.Int32 ScreenTemplateStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplateStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplateStatus] with (nolock)  where ScreenTemplateStatusId=@ScreenTemplateStatusId ";
			SqlParameter parameter=new SqlParameter("@ScreenTemplateStatusId",ScreenTemplateStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ScreenTemplateStatusFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ScreenTemplateStatus> GetScreenTemplateStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplateStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ScreenTemplateStatus] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplateStatus>(ds,ScreenTemplateStatusFromDataRow);
		}

		public virtual List<ScreenTemplateStatus> GetAllScreenTemplateStatus(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplateStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplateStatus] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplateStatus>(ds, ScreenTemplateStatusFromDataRow);
		}

		public virtual List<ScreenTemplateStatus> GetPagedScreenTemplateStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetScreenTemplateStatusCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ScreenTemplateStatusId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ScreenTemplateStatusId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ScreenTemplateStatusId] ";
            tempsql += " FROM [ScreenTemplateStatus] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ScreenTemplateStatusId"))
					tempsql += " , (AllRecords.[ScreenTemplateStatusId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ScreenTemplateStatusId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetScreenTemplateStatusSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ScreenTemplateStatus] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ScreenTemplateStatus].[ScreenTemplateStatusId] = PageIndex.[ScreenTemplateStatusId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplateStatus>(ds, ScreenTemplateStatusFromDataRow);
			}else{ return null;}
		}

		private int GetScreenTemplateStatusCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ScreenTemplateStatus as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ScreenTemplateStatus as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ScreenTemplateStatus))]
		public virtual ScreenTemplateStatus InsertScreenTemplateStatus(ScreenTemplateStatus entity)
		{

			ScreenTemplateStatus other=new ScreenTemplateStatus();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ScreenTemplateStatus ( [Name] )
				Values
				( @Name );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetScreenTemplateStatus(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ScreenTemplateStatus))]
		public virtual ScreenTemplateStatus UpdateScreenTemplateStatus(ScreenTemplateStatus entity)
		{

			if (entity.IsTransient()) return entity;
			ScreenTemplateStatus other = GetScreenTemplateStatus(entity.ScreenTemplateStatusId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ScreenTemplateStatus set  [Name]=@Name 
							 where ScreenTemplateStatusId=@ScreenTemplateStatusId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@ScreenTemplateStatusId",entity.ScreenTemplateStatusId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetScreenTemplateStatus(entity.ScreenTemplateStatusId);
		}

		public virtual bool DeleteScreenTemplateStatus(System.Int32 ScreenTemplateStatusId)
		{

			string sql="delete from ScreenTemplateStatus where ScreenTemplateStatusId=@ScreenTemplateStatusId";
			SqlParameter parameter=new SqlParameter("@ScreenTemplateStatusId",ScreenTemplateStatusId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ScreenTemplateStatus))]
		public virtual ScreenTemplateStatus DeleteScreenTemplateStatus(ScreenTemplateStatus entity)
		{
			this.DeleteScreenTemplateStatus(entity.ScreenTemplateStatusId);
			return entity;
		}


		public virtual ScreenTemplateStatus ScreenTemplateStatusFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ScreenTemplateStatus entity=new ScreenTemplateStatus();
			if (dr.Table.Columns.Contains("ScreenTemplateStatusId"))
			{
			entity.ScreenTemplateStatusId = (System.Int32)dr["ScreenTemplateStatusId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			return entity;
		}

	}
	
	
}
