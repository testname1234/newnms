﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ScreenTemplatekeyRepositoryBase : Repository, IScreenTemplatekeyRepositoryBase 
	{
        
        public ScreenTemplatekeyRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ScreenTemplatekeyId",new SearchColumn(){Name="ScreenTemplatekeyId",Title="ScreenTemplatekeyId",SelectClause="ScreenTemplatekeyId",WhereClause="AllRecords.ScreenTemplatekeyId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScreenTemplatekeyId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScreenTemplateId",new SearchColumn(){Name="ScreenTemplateId",Title="ScreenTemplateId",SelectClause="ScreenTemplateId",WhereClause="AllRecords.ScreenTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FlashTemplateKeyId",new SearchColumn(){Name="FlashTemplateKeyId",Title="FlashTemplateKeyId",SelectClause="FlashTemplateKeyId",WhereClause="AllRecords.FlashTemplateKeyId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="FlashTemplateKeyId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("KeyName",new SearchColumn(){Name="KeyName",Title="KeyName",SelectClause="KeyName",WhereClause="AllRecords.KeyName",DataType="System.String",IsForeignColumn=false,PropertyName="KeyName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("creationDate",new SearchColumn(){Name="creationDate",Title="creationDate",SelectClause="creationDate",WhereClause="AllRecords.creationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("updateddate",new SearchColumn(){Name="updateddate",Title="updateddate",SelectClause="updateddate",WhereClause="AllRecords.updateddate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="Updateddate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Top",new SearchColumn(){Name="Top",Title="Top",SelectClause="Top",WhereClause="AllRecords.Top",DataType="System.Double?",IsForeignColumn=false,PropertyName="Top",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Bottom",new SearchColumn(){Name="Bottom",Title="Bottom",SelectClause="Bottom",WhereClause="AllRecords.Bottom",DataType="System.Double?",IsForeignColumn=false,PropertyName="Bottom",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("left",new SearchColumn(){Name="left",Title="left",SelectClause="left",WhereClause="AllRecords.left",DataType="System.Double?",IsForeignColumn=false,PropertyName="Left",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Right",new SearchColumn(){Name="Right",Title="Right",SelectClause="Right",WhereClause="AllRecords.Right",DataType="System.Double?",IsForeignColumn=false,PropertyName="Right",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetScreenTemplatekeySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetScreenTemplatekeyBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetScreenTemplatekeyAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetScreenTemplatekeySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ScreenTemplatekey].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ScreenTemplatekey].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ScreenTemplatekey> GetScreenTemplatekeyByScreenTemplateId(System.Int32? ScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplatekey] with (nolock)  where ScreenTemplateId=@ScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@ScreenTemplateId",ScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplatekey>(ds,ScreenTemplatekeyFromDataRow);
		}

		public virtual ScreenTemplatekey GetScreenTemplatekey(System.Int32 ScreenTemplatekeyId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplatekey] with (nolock)  where ScreenTemplatekeyId=@ScreenTemplatekeyId ";
			SqlParameter parameter=new SqlParameter("@ScreenTemplatekeyId",ScreenTemplatekeyId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ScreenTemplatekeyFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ScreenTemplatekey> GetScreenTemplatekeyByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ScreenTemplatekey] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplatekey>(ds,ScreenTemplatekeyFromDataRow);
		}

		public virtual List<ScreenTemplatekey> GetAllScreenTemplatekey(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ScreenTemplatekey] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplatekey>(ds, ScreenTemplatekeyFromDataRow);
		}

		public virtual List<ScreenTemplatekey> GetPagedScreenTemplatekey(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetScreenTemplatekeyCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ScreenTemplatekeyId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ScreenTemplatekeyId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ScreenTemplatekeyId] ";
            tempsql += " FROM [ScreenTemplatekey] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ScreenTemplatekeyId"))
					tempsql += " , (AllRecords.[ScreenTemplatekeyId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ScreenTemplatekeyId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ScreenTemplatekey] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ScreenTemplatekey].[ScreenTemplatekeyId] = PageIndex.[ScreenTemplatekeyId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ScreenTemplatekey>(ds, ScreenTemplatekeyFromDataRow);
			}else{ return null;}
		}

		private int GetScreenTemplatekeyCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ScreenTemplatekey as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ScreenTemplatekey as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ScreenTemplatekey))]
		public virtual ScreenTemplatekey InsertScreenTemplatekey(ScreenTemplatekey entity)
		{

			ScreenTemplatekey other=new ScreenTemplatekey();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ScreenTemplatekey ( [ScreenTemplateId]
				,[FlashTemplateKeyId]
				,[KeyName]
				,[creationDate]
				,[updateddate]
				,[IsActive]
				,[Top]
				,[Bottom]
				,[left]
				,[Right] )
				Values
				( @ScreenTemplateId
				, @FlashTemplateKeyId
				, @KeyName
				, @creationDate
				, @updateddate
				, @IsActive
				, @Top
				, @Bottom
				, @left
				, @Right );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateKeyId",entity.FlashTemplateKeyId ?? (object)DBNull.Value)
					, new SqlParameter("@KeyName",entity.KeyName ?? (object)DBNull.Value)
					, new SqlParameter("@creationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@updateddate",entity.Updateddate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetScreenTemplatekey(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ScreenTemplatekey))]
		public virtual ScreenTemplatekey UpdateScreenTemplatekey(ScreenTemplatekey entity)
		{

			if (entity.IsTransient()) return entity;
			ScreenTemplatekey other = GetScreenTemplatekey(entity.ScreenTemplatekeyId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ScreenTemplatekey set  [ScreenTemplateId]=@ScreenTemplateId
							, [FlashTemplateKeyId]=@FlashTemplateKeyId
							, [KeyName]=@KeyName
							, [creationDate]=@creationDate
							, [updateddate]=@updateddate
							, [IsActive]=@IsActive
							, [Top]=@Top
							, [Bottom]=@Bottom
							, [left]=@left
							, [Right]=@Right 
							 where ScreenTemplatekeyId=@ScreenTemplatekeyId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@FlashTemplateKeyId",entity.FlashTemplateKeyId ?? (object)DBNull.Value)
					, new SqlParameter("@KeyName",entity.KeyName ?? (object)DBNull.Value)
					, new SqlParameter("@creationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@updateddate",entity.Updateddate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)
					, new SqlParameter("@ScreenTemplatekeyId",entity.ScreenTemplatekeyId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetScreenTemplatekey(entity.ScreenTemplatekeyId);
		}

		public virtual bool DeleteScreenTemplatekey(System.Int32 ScreenTemplatekeyId)
		{

			string sql="delete from ScreenTemplatekey where ScreenTemplatekeyId=@ScreenTemplatekeyId";
			SqlParameter parameter=new SqlParameter("@ScreenTemplatekeyId",ScreenTemplatekeyId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ScreenTemplatekey))]
		public virtual ScreenTemplatekey DeleteScreenTemplatekey(ScreenTemplatekey entity)
		{
			this.DeleteScreenTemplatekey(entity.ScreenTemplatekeyId);
			return entity;
		}


		public virtual ScreenTemplatekey ScreenTemplatekeyFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ScreenTemplatekey entity=new ScreenTemplatekey();
			if (dr.Table.Columns.Contains("ScreenTemplatekeyId"))
			{
			entity.ScreenTemplatekeyId = (System.Int32)dr["ScreenTemplatekeyId"];
			}
			if (dr.Table.Columns.Contains("ScreenTemplateId"))
			{
			entity.ScreenTemplateId = dr["ScreenTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("FlashTemplateKeyId"))
			{
			entity.FlashTemplateKeyId = dr["FlashTemplateKeyId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["FlashTemplateKeyId"];
			}
			if (dr.Table.Columns.Contains("KeyName"))
			{
			entity.KeyName = dr["KeyName"].ToString();
			}
			if (dr.Table.Columns.Contains("creationDate"))
			{
			entity.CreationDate = dr["creationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["creationDate"];
			}
			if (dr.Table.Columns.Contains("updateddate"))
			{
			entity.Updateddate = dr["updateddate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["updateddate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("Top"))
			{
			entity.Top = dr["Top"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Top"];
			}
			if (dr.Table.Columns.Contains("Bottom"))
			{
			entity.Bottom = dr["Bottom"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Bottom"];
			}
			if (dr.Table.Columns.Contains("left"))
			{
			entity.Left = dr["left"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["left"];
			}
			if (dr.Table.Columns.Contains("Right"))
			{
			entity.Right = dr["Right"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Right"];
			}
			return entity;
		}

	}
	
	
}
