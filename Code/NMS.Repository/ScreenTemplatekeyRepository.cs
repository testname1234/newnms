﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class ScreenTemplatekeyRepository: ScreenTemplatekeyRepositoryBase, IScreenTemplatekeyRepository
	{

        public void DeleteScreenTemplatekeyByScreenTemplateID(int screenTemplateId)
        {
            string sql = "delete from [ScreenTemplatekey] where ScreenTemplateId=@ScreenTemplateId ";
            SqlParameter parameter = new SqlParameter("@ScreenTemplateId", screenTemplateId);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
        }
    }
	
	
}
