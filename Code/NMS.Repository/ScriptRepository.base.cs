﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class ScriptRepositoryBase : Repository, IScriptRepositoryBase 
	{
        
        public ScriptRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ScriptId",new SearchColumn(){Name="ScriptId",Title="ScriptId",SelectClause="ScriptId",WhereClause="AllRecords.ScriptId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScriptId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Script",new SearchColumn(){Name="Script",Title="Script",SelectClause="Script",WhereClause="AllRecords.Script",DataType="System.String",IsForeignColumn=false,PropertyName="Script",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsExecuted",new SearchColumn(){Name="IsExecuted",Title="IsExecuted",SelectClause="IsExecuted",WhereClause="AllRecords.IsExecuted",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsExecuted",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Token",new SearchColumn(){Name="Token",Title="Token",SelectClause="Token",WhereClause="AllRecords.Token",DataType="System.String",IsForeignColumn=false,PropertyName="Token",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetScriptSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetScriptBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetScriptAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetScriptSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Script].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Script].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Script GetScript(System.Int32 ScriptId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScriptSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Script] with (nolock)  where ScriptId=@ScriptId ";
			SqlParameter parameter=new SqlParameter("@ScriptId",ScriptId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ScriptFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Script> GetScriptByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScriptSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Script] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Script>(ds,ScriptFromDataRow);
		}

		public virtual List<Script> GetAllScript(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetScriptSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Script] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Script>(ds, ScriptFromDataRow);
		}

		public virtual List<Script> GetPagedScript(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetScriptCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ScriptId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ScriptId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ScriptId] ";
            tempsql += " FROM [Script] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ScriptId"))
					tempsql += " , (AllRecords.[ScriptId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ScriptId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetScriptSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Script] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Script].[ScriptId] = PageIndex.[ScriptId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Script>(ds, ScriptFromDataRow);
			}else{ return null;}
		}

		private int GetScriptCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Script as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Script as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Script))]
		public virtual Script InsertScript(Script entity)
		{

			Script other=new Script();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Script ( [UserId]
				,[Script]
				,[IsExecuted]
				,[CreationDate]
				,[LastUpdateDate]
				,[Token] )
				Values
				( @UserId
				, @Script
				, @IsExecuted
				, @CreationDate
				, @LastUpdateDate
				, @Token );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@Script",entity.Script)
					, new SqlParameter("@IsExecuted",entity.IsExecuted)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@Token",entity.Token ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetScript(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Script))]
		public virtual Script UpdateScript(Script entity)
		{

			if (entity.IsTransient()) return entity;
			Script other = GetScript(entity.ScriptId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Script set  [UserId]=@UserId
							, [Script]=@Script
							, [IsExecuted]=@IsExecuted
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [Token]=@Token 
							 where ScriptId=@ScriptId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@Script",entity.Script)
					, new SqlParameter("@IsExecuted",entity.IsExecuted)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@Token",entity.Token ?? (object)DBNull.Value)
					, new SqlParameter("@ScriptId",entity.ScriptId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetScript(entity.ScriptId);
		}

		public virtual bool DeleteScript(System.Int32 ScriptId)
		{

			string sql="delete from Script where ScriptId=@ScriptId";
			SqlParameter parameter=new SqlParameter("@ScriptId",ScriptId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Script))]
		public virtual Script DeleteScript(Script entity)
		{
			this.DeleteScript(entity.ScriptId);
			return entity;
		}


		public virtual Script ScriptFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Script entity=new Script();
			if (dr.Table.Columns.Contains("ScriptId"))
			{
			entity.ScriptId = (System.Int32)dr["ScriptId"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = (System.Int32)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("Script"))
			{
			entity.Script = dr["Script"].ToString();
			}
			if (dr.Table.Columns.Contains("IsExecuted"))
			{
			entity.IsExecuted = (System.Boolean)dr["IsExecuted"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("Token"))
			{
			entity.Token = dr["Token"].ToString();
			}
			return entity;
		}

	}
	
	
}
