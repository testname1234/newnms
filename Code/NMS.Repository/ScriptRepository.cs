﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class ScriptRepository: ScriptRepositoryBase, IScriptRepository
	{

        public bool MarkAsExecuted(int id)
        {
            string sql = "update script set IsExecuted=1,lastupdatedate=@date where ScriptId=@ScriptId";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@date", DateTime.UtcNow), new SqlParameter("@ScriptId", id) };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }


        public List<Script> GetAllUnExecutedScripts(string token)
        {
            string sql = string.Format("select * from [Script] with (nolock) where token=@token");
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { new SqlParameter("@token", token) });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Script>(ds, ScriptFromDataRow);
        }
    }
	
	
}
