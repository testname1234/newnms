﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SegmentStoryRepositoryBase : Repository, ISegmentStoryRepositoryBase 
	{
        
        public SegmentStoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SegmentStoryId",new SearchColumn(){Name="SegmentStoryId",Title="SegmentStoryId",SelectClause="SegmentStoryId",WhereClause="AllRecords.SegmentStoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SegmentStoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SegmentId",new SearchColumn(){Name="SegmentId",Title="SegmentId",SelectClause="SegmentId",WhereClause="AllRecords.SegmentId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SegmentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StoryId",new SearchColumn(){Name="StoryId",Title="StoryId",SelectClause="StoryId",WhereClause="AllRecords.StoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSegmentStorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSegmentStoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSegmentStoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSegmentStorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SegmentStory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SegmentStory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SegmentStory> GetSegmentStoryBySegmentId(System.Int32 SegmentId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentStorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from SegmentStory with (nolock)  where SegmentId=@SegmentId  ";
			SqlParameter parameter=new SqlParameter("@SegmentId",SegmentId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentStory>(ds,SegmentStoryFromDataRow);
		}

		public virtual List<SegmentStory> GetSegmentStoryByStoryId(System.Int32 StoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentStorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from SegmentStory with (nolock)  where StoryId=@StoryId  ";
			SqlParameter parameter=new SqlParameter("@StoryId",StoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentStory>(ds,SegmentStoryFromDataRow);
		}

		public virtual SegmentStory GetSegmentStory(System.Int32 SegmentStoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentStorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from SegmentStory with (nolock)  where SegmentStoryId=@SegmentStoryId ";
			SqlParameter parameter=new SqlParameter("@SegmentStoryId",SegmentStoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SegmentStoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SegmentStory> GetSegmentStoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentStorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from SegmentStory with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentStory>(ds,SegmentStoryFromDataRow);
		}

		public virtual List<SegmentStory> GetAllSegmentStory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentStorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from SegmentStory with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentStory>(ds, SegmentStoryFromDataRow);
		}

		public virtual List<SegmentStory> GetPagedSegmentStory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSegmentStoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SegmentStoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SegmentStoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SegmentStoryId] ";
            tempsql += " FROM [SegmentStory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SegmentStoryId"))
					tempsql += " , (AllRecords.[SegmentStoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SegmentStoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSegmentStorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SegmentStory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SegmentStory].[SegmentStoryId] = PageIndex.[SegmentStoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentStory>(ds, SegmentStoryFromDataRow);
			}else{ return null;}
		}

		private int GetSegmentStoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SegmentStory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SegmentStory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SegmentStory))]
		public virtual SegmentStory InsertSegmentStory(SegmentStory entity)
		{

			SegmentStory other=new SegmentStory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SegmentStory ( [SegmentId]
				,[StoryId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @SegmentId
				, @StoryId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SegmentId",entity.SegmentId)
					, new SqlParameter("@StoryId",entity.StoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSegmentStory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SegmentStory))]
		public virtual SegmentStory UpdateSegmentStory(SegmentStory entity)
		{

			if (entity.IsTransient()) return entity;
			SegmentStory other = GetSegmentStory(entity.SegmentStoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SegmentStory set  [SegmentId]=@SegmentId
							, [StoryId]=@StoryId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where SegmentStoryId=@SegmentStoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SegmentId",entity.SegmentId)
					, new SqlParameter("@StoryId",entity.StoryId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@SegmentStoryId",entity.SegmentStoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSegmentStory(entity.SegmentStoryId);
		}

		public virtual bool DeleteSegmentStory(System.Int32 SegmentStoryId)
		{

			string sql="delete from SegmentStory with (nolock) where SegmentStoryId=@SegmentStoryId";
			SqlParameter parameter=new SqlParameter("@SegmentStoryId",SegmentStoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SegmentStory))]
		public virtual SegmentStory DeleteSegmentStory(SegmentStory entity)
		{
			this.DeleteSegmentStory(entity.SegmentStoryId);
			return entity;
		}


		public virtual SegmentStory SegmentStoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SegmentStory entity=new SegmentStory();
			if (dr.Table.Columns.Contains("SegmentStoryId"))
			{
			entity.SegmentStoryId = (System.Int32)dr["SegmentStoryId"];
			}
			if (dr.Table.Columns.Contains("SegmentId"))
			{
			entity.SegmentId = (System.Int32)dr["SegmentId"];
			}
			if (dr.Table.Columns.Contains("StoryId"))
			{
			entity.StoryId = (System.Int32)dr["StoryId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
