﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SegmentTypeRepositoryBase : Repository, ISegmentTypeRepositoryBase 
	{
        
        public SegmentTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SegmentTypeId",new SearchColumn(){Name="SegmentTypeId",Title="SegmentTypeId",SelectClause="SegmentTypeId",WhereClause="AllRecords.SegmentTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SegmentTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Segment",new SearchColumn(){Name="Segment",Title="Segment",SelectClause="Segment",WhereClause="AllRecords.Segment",DataType="System.String",IsForeignColumn=false,PropertyName="Segment",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSegmentTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSegmentTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSegmentTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSegmentTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SegmentType].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SegmentType].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual SegmentType GetSegmentType(System.Int32 SegmentTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from SegmentType with (nolock)  where SegmentTypeId=@SegmentTypeId ";
			SqlParameter parameter=new SqlParameter("@SegmentTypeId",SegmentTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SegmentTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SegmentType> GetSegmentTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from SegmentType with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentType>(ds,SegmentTypeFromDataRow);
		}

		public virtual List<SegmentType> GetAllSegmentType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSegmentTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from SegmentType with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentType>(ds, SegmentTypeFromDataRow);
		}

		public virtual List<SegmentType> GetPagedSegmentType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSegmentTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SegmentTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SegmentTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SegmentTypeId] ";
            tempsql += " FROM [SegmentType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SegmentTypeId"))
					tempsql += " , (AllRecords.[SegmentTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SegmentTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSegmentTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SegmentType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SegmentType].[SegmentTypeId] = PageIndex.[SegmentTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SegmentType>(ds, SegmentTypeFromDataRow);
			}else{ return null;}
		}

		private int GetSegmentTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SegmentType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SegmentType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SegmentType))]
		public virtual SegmentType InsertSegmentType(SegmentType entity)
		{

			SegmentType other=new SegmentType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SegmentType ( [Segment] )
				Values
				( @Segment );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Segment",entity.Segment)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSegmentType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SegmentType))]
		public virtual SegmentType UpdateSegmentType(SegmentType entity)
		{

			if (entity.IsTransient()) return entity;
			SegmentType other = GetSegmentType(entity.SegmentTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SegmentType set  [Segment]=@Segment 
							 where SegmentTypeId=@SegmentTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Segment",entity.Segment)
					, new SqlParameter("@SegmentTypeId",entity.SegmentTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSegmentType(entity.SegmentTypeId);
		}

		public virtual bool DeleteSegmentType(System.Int32 SegmentTypeId)
		{

			string sql="delete from SegmentType with (nolock) where SegmentTypeId=@SegmentTypeId";
			SqlParameter parameter=new SqlParameter("@SegmentTypeId",SegmentTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SegmentType))]
		public virtual SegmentType DeleteSegmentType(SegmentType entity)
		{
			this.DeleteSegmentType(entity.SegmentTypeId);
			return entity;
		}


		public virtual SegmentType SegmentTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SegmentType entity=new SegmentType();
			if (dr.Table.Columns.Contains("SegmentTypeId"))
			{
			entity.SegmentTypeId = (System.Int32)dr["SegmentTypeId"];
			}
			if (dr.Table.Columns.Contains("Segment"))
			{
			entity.Segment = dr["Segment"].ToString();
			}
			return entity;
		}

	}
	
	
}
