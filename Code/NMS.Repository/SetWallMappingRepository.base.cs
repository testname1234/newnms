﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SetWallMappingRepositoryBase : Repository, ISetWallMappingRepositoryBase 
	{
        
        public SetWallMappingRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SetWallMappingId",new SearchColumn(){Name="SetWallMappingId",Title="SetWallMappingId",SelectClause="SetWallMappingId",WhereClause="AllRecords.SetWallMappingId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SetWallMappingId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SetId",new SearchColumn(){Name="SetId",Title="SetId",SelectClause="SetId",WhereClause="AllRecords.SetId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SetId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("WallId",new SearchColumn(){Name="WallId",Title="WallId",SelectClause="WallId",WhereClause="AllRecords.WallId",DataType="System.Int32",IsForeignColumn=false,PropertyName="WallId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RatioTypeId",new SearchColumn(){Name="RatioTypeId",Title="RatioTypeId",SelectClause="RatioTypeId",WhereClause="AllRecords.RatioTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RatioTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSetWallMappingSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSetWallMappingBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSetWallMappingAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSetWallMappingSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SetWallMapping].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SetWallMapping].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual SetWallMapping GetSetWallMapping(System.Int32 SetWallMappingId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSetWallMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SetWallMapping] with (nolock)  where SetWallMappingId=@SetWallMappingId ";
			SqlParameter parameter=new SqlParameter("@SetWallMappingId",SetWallMappingId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SetWallMappingFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SetWallMapping> GetSetWallMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSetWallMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SetWallMapping] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SetWallMapping>(ds,SetWallMappingFromDataRow);
		}

		public virtual List<SetWallMapping> GetAllSetWallMapping(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSetWallMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SetWallMapping] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SetWallMapping>(ds, SetWallMappingFromDataRow);
		}

		public virtual List<SetWallMapping> GetPagedSetWallMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSetWallMappingCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SetWallMappingId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SetWallMappingId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SetWallMappingId] ";
            tempsql += " FROM [SetWallMapping] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SetWallMappingId"))
					tempsql += " , (AllRecords.[SetWallMappingId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SetWallMappingId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSetWallMappingSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SetWallMapping] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SetWallMapping].[SetWallMappingId] = PageIndex.[SetWallMappingId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SetWallMapping>(ds, SetWallMappingFromDataRow);
			}else{ return null;}
		}

		private int GetSetWallMappingCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SetWallMapping as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SetWallMapping as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SetWallMapping))]
		public virtual SetWallMapping InsertSetWallMapping(SetWallMapping entity)
		{

			SetWallMapping other=new SetWallMapping();
			other = entity;
				string sql=@"Insert into SetWallMapping ( [SetWallMappingId]
				,[SetId]
				,[WallId]
				,[RatioTypeId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @SetWallMappingId
				, @SetId
				, @WallId
				, @RatioTypeId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
";
				SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@SetWallMappingId",entity.SetWallMappingId)
					, new SqlParameter("@SetId",entity.SetId)
					, new SqlParameter("@WallId",entity.WallId)
					, new SqlParameter("@RatioTypeId",entity.RatioTypeId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSetWallMapping(Convert.ToInt32(identity));
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SetWallMapping))]
		public virtual SetWallMapping UpdateSetWallMapping(SetWallMapping entity)
		{

			SetWallMapping other = GetSetWallMapping(entity.SetWallMappingId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SetWallMapping set  [SetId]=@SetId
							, [WallId]=@WallId
							, [RatioTypeId]=@RatioTypeId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where SetWallMappingId=@SetWallMappingId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					new SqlParameter("@SetWallMappingId",entity.SetWallMappingId)
					, new SqlParameter("@SetId",entity.SetId)
					, new SqlParameter("@WallId",entity.WallId)
					, new SqlParameter("@RatioTypeId",entity.RatioTypeId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSetWallMapping(entity.SetWallMappingId);
		}

		public virtual bool DeleteSetWallMapping(System.Int32 SetWallMappingId)
		{

			string sql="delete from SetWallMapping where SetWallMappingId=@SetWallMappingId";
			SqlParameter parameter=new SqlParameter("@SetWallMappingId",SetWallMappingId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SetWallMapping))]
		public virtual SetWallMapping DeleteSetWallMapping(SetWallMapping entity)
		{
			this.DeleteSetWallMapping(entity.SetWallMappingId);
			return entity;
		}


		public virtual SetWallMapping SetWallMappingFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SetWallMapping entity=new SetWallMapping();
			if (dr.Table.Columns.Contains("SetWallMappingId"))
			{
			entity.SetWallMappingId = (System.Int32)dr["SetWallMappingId"];
			}
			if (dr.Table.Columns.Contains("SetId"))
			{
			entity.SetId = (System.Int32)dr["SetId"];
			}
			if (dr.Table.Columns.Contains("WallId"))
			{
			entity.WallId = (System.Int32)dr["WallId"];
			}
			if (dr.Table.Columns.Contains("RatioTypeId"))
			{
			entity.RatioTypeId = (System.Int32)dr["RatioTypeId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
