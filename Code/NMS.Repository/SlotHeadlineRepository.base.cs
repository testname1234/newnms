﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotHeadlineRepositoryBase : Repository, ISlotHeadlineRepositoryBase 
	{
        
        public SlotHeadlineRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotHeadlineId",new SearchColumn(){Name="SlotHeadlineId",Title="SlotHeadlineId",SelectClause="SlotHeadlineId",WhereClause="AllRecords.SlotHeadlineId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotHeadlineId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotId",new SearchColumn(){Name="SlotId",Title="SlotId",SelectClause="SlotId",WhereClause="AllRecords.SlotId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Headline",new SearchColumn(){Name="Headline",Title="Headline",SelectClause="Headline",WhereClause="AllRecords.Headline",DataType="System.String",IsForeignColumn=false,PropertyName="Headline",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotHeadlineSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotHeadlineBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotHeadlineAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotHeadlineSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlotHeadline].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlotHeadline].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SlotHeadline> GetSlotHeadlineBySlotId(System.Int32 SlotId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotHeadlineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotHeadline] with (nolock)  where SlotId=@SlotId  ";
			SqlParameter parameter=new SqlParameter("@SlotId",SlotId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotHeadline>(ds,SlotHeadlineFromDataRow);
		}

		public virtual SlotHeadline GetSlotHeadline(System.Int32 SlotHeadlineId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotHeadlineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotHeadline] with (nolock)  where SlotHeadlineId=@SlotHeadlineId ";
			SqlParameter parameter=new SqlParameter("@SlotHeadlineId",SlotHeadlineId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotHeadlineFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlotHeadline> GetSlotHeadlineByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotHeadlineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlotHeadline] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotHeadline>(ds,SlotHeadlineFromDataRow);
		}

		public virtual List<SlotHeadline> GetAllSlotHeadline(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotHeadlineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotHeadline] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotHeadline>(ds, SlotHeadlineFromDataRow);
		}

		public virtual List<SlotHeadline> GetPagedSlotHeadline(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotHeadlineCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotHeadlineId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotHeadlineId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotHeadlineId] ";
            tempsql += " FROM [SlotHeadline] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotHeadlineId"))
					tempsql += " , (AllRecords.[SlotHeadlineId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotHeadlineId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotHeadlineSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlotHeadline] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlotHeadline].[SlotHeadlineId] = PageIndex.[SlotHeadlineId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotHeadline>(ds, SlotHeadlineFromDataRow);
			}else{ return null;}
		}

		private int GetSlotHeadlineCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlotHeadline as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlotHeadline as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlotHeadline))]
		public virtual SlotHeadline InsertSlotHeadline(SlotHeadline entity)
		{

			SlotHeadline other=new SlotHeadline();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlotHeadline ( [SlotId]
				,[Headline]
				,[LanguageCode]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @SlotId
				, @Headline
				, @LanguageCode
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotId",entity.SlotId)
					, new SqlParameter("@Headline",entity.Headline)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlotHeadline(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlotHeadline))]
		public virtual SlotHeadline UpdateSlotHeadline(SlotHeadline entity)
		{

			if (entity.IsTransient()) return entity;
			SlotHeadline other = GetSlotHeadline(entity.SlotHeadlineId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlotHeadline set  [SlotId]=@SlotId
							, [Headline]=@Headline
							, [LanguageCode]=@LanguageCode
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where SlotHeadlineId=@SlotHeadlineId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotId",entity.SlotId)
					, new SqlParameter("@Headline",entity.Headline)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@SlotHeadlineId",entity.SlotHeadlineId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlotHeadline(entity.SlotHeadlineId);
		}

		public virtual bool DeleteSlotHeadline(System.Int32 SlotHeadlineId)
		{

			string sql="delete from SlotHeadline where SlotHeadlineId=@SlotHeadlineId";
			SqlParameter parameter=new SqlParameter("@SlotHeadlineId",SlotHeadlineId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlotHeadline))]
		public virtual SlotHeadline DeleteSlotHeadline(SlotHeadline entity)
		{
			this.DeleteSlotHeadline(entity.SlotHeadlineId);
			return entity;
		}


		public virtual SlotHeadline SlotHeadlineFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlotHeadline entity=new SlotHeadline();
			if (dr.Table.Columns.Contains("SlotHeadlineId"))
			{
			entity.SlotHeadlineId = (System.Int32)dr["SlotHeadlineId"];
			}
			if (dr.Table.Columns.Contains("SlotId"))
			{
			entity.SlotId = (System.Int32)dr["SlotId"];
			}
			if (dr.Table.Columns.Contains("Headline"))
			{
			entity.Headline = dr["Headline"].ToString();
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
