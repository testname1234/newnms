﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class SlotHeadlineRepository: SlotHeadlineRepositoryBase, ISlotHeadlineRepository
	{
        public bool DeleteSlotHeadlineBySlotId(int slotid)
        {
            string sql = "delete from SlotHeadline where SlotId=@slotid";
            SqlParameter parameter = new SqlParameter("@slotid", slotid);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
        
    }
	
	
}
