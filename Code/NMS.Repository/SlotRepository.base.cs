﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotRepositoryBase : Repository, ISlotRepositoryBase 
	{
        
        public SlotRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotId",new SearchColumn(){Name="SlotId",Title="SlotId",SelectClause="SlotId",WhereClause="AllRecords.SlotId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Title",new SearchColumn(){Name="Title",Title="Title",SelectClause="Title",WhereClause="AllRecords.Title",DataType="System.String",IsForeignColumn=false,PropertyName="Title",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Description",new SearchColumn(){Name="Description",Title="Description",SelectClause="Description",WhereClause="AllRecords.Description",DataType="System.String",IsForeignColumn=false,PropertyName="Description",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ThumbGuid",new SearchColumn(){Name="ThumbGuid",Title="ThumbGuid",SelectClause="ThumbGuid",WhereClause="AllRecords.ThumbGuid",DataType="System.Guid?",IsForeignColumn=false,PropertyName="ThumbGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequnceNumber",new SearchColumn(){Name="SequnceNumber",Title="SequnceNumber",SelectClause="SequnceNumber",WhereClause="AllRecords.SequnceNumber",DataType="System.Int32",IsForeignColumn=false,PropertyName="SequnceNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotTypeId",new SearchColumn(){Name="SlotTypeId",Title="SlotTypeId",SelectClause="SlotTypeId",WhereClause="AllRecords.SlotTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SegmentId",new SearchColumn(){Name="SegmentId",Title="SegmentId",SelectClause="SegmentId",WhereClause="AllRecords.SegmentId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SegmentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TranslatedDescription",new SearchColumn(){Name="TranslatedDescription",Title="TranslatedDescription",SelectClause="TranslatedDescription",WhereClause="AllRecords.TranslatedDescription",DataType="System.String",IsForeignColumn=false,PropertyName="TranslatedDescription",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TranslatedTitle",new SearchColumn(){Name="TranslatedTitle",Title="TranslatedTitle",SelectClause="TranslatedTitle",WhereClause="AllRecords.TranslatedTitle",DataType="System.String",IsForeignColumn=false,PropertyName="TranslatedTitle",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("PreviewGuid",new SearchColumn(){Name="PreviewGuid",Title="PreviewGuid",SelectClause="PreviewGuid",WhereClause="AllRecords.PreviewGuid",DataType="System.Guid?",IsForeignColumn=false,PropertyName="PreviewGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IncludeInRundown",new SearchColumn(){Name="IncludeInRundown",Title="IncludeInRundown",SelectClause="IncludeInRundown",WhereClause="AllRecords.IncludeInRundown",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IncludeInRundown",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResearchText",new SearchColumn(){Name="ResearchText",Title="ResearchText",SelectClause="ResearchText",WhereClause="AllRecords.ResearchText",DataType="System.String",IsForeignColumn=false,PropertyName="ResearchText",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AnchorId",new SearchColumn(){Name="AnchorId",Title="AnchorId",SelectClause="AnchorId",WhereClause="AllRecords.AnchorId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="AnchorId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Script",new SearchColumn(){Name="Script",Title="Script",SelectClause="Script",WhereClause="AllRecords.Script",DataType="System.String",IsForeignColumn=false,PropertyName="Script",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Slot].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Slot].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Slot> GetSlotBySlotTypeId(System.Int32 SlotTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Slot] with (nolock)  where SlotTypeId=@SlotTypeId  ";
			SqlParameter parameter=new SqlParameter("@SlotTypeId",SlotTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Slot>(ds,SlotFromDataRow);
		}

		public virtual List<Slot> GetSlotBySegmentId(System.Int32 SegmentId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Slot] with (nolock)  where SegmentId=@SegmentId  ";
			SqlParameter parameter=new SqlParameter("@SegmentId",SegmentId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Slot>(ds,SlotFromDataRow);
		}

		public virtual List<Slot> GetSlotByCategoryId(System.Int32? CategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Slot] with (nolock)  where CategoryId=@CategoryId  ";
			SqlParameter parameter=new SqlParameter("@CategoryId",CategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Slot>(ds,SlotFromDataRow);
		}

		public virtual Slot GetSlot(System.Int32 SlotId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Slot] with (nolock)  where SlotId=@SlotId ";
			SqlParameter parameter=new SqlParameter("@SlotId",SlotId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Slot> GetSlotByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Slot] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Slot>(ds,SlotFromDataRow);
		}

		public virtual List<Slot> GetAllSlot(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Slot] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Slot>(ds, SlotFromDataRow);
		}

		public virtual List<Slot> GetPagedSlot(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotId] ";
            tempsql += " FROM [Slot] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotId"))
					tempsql += " , (AllRecords.[SlotId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Slot] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Slot].[SlotId] = PageIndex.[SlotId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Slot>(ds, SlotFromDataRow);
			}else{ return null;}
		}

		private int GetSlotCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Slot as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Slot as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Slot))]
		public virtual Slot InsertSlot(Slot entity)
		{

			Slot other=new Slot();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Slot ( [NewsGuid]
				,[Title]
				,[Description]
				,[ThumbGuid]
				,[SequnceNumber]
				,[SlotTypeId]
				,[SegmentId]
				,[CategoryId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[TranslatedDescription]
				,[TranslatedTitle]
				,[LanguageCode]
				,[PreviewGuid]
				,[UserId]
				,[TickerId]
				,[IncludeInRundown]
				,[NewsFileId]
				,[ResearchText]
				,[AnchorId]
				,[Script] )
				Values
				( @NewsGuid
				, @Title
				, @Description
				, @ThumbGuid
				, @SequnceNumber
				, @SlotTypeId
				, @SegmentId
				, @CategoryId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @TranslatedDescription
				, @TranslatedTitle
				, @LanguageCode
				, @PreviewGuid
				, @UserId
				, @TickerId
				, @IncludeInRundown
				, @NewsFileId
				, @ResearchText
				, @AnchorId
				, @Script );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid ?? (object)DBNull.Value)
					, new SqlParameter("@SequnceNumber",entity.SequnceNumber)
					, new SqlParameter("@SlotTypeId",entity.SlotTypeId)
					, new SqlParameter("@SegmentId",entity.SegmentId)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@PreviewGuid",entity.PreviewGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@IncludeInRundown",entity.IncludeInRundown ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@ResearchText",entity.ResearchText ?? (object)DBNull.Value)
					, new SqlParameter("@AnchorId",entity.AnchorId ?? (object)DBNull.Value)
					, new SqlParameter("@Script",entity.Script ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlot(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Slot))]
		public virtual Slot UpdateSlot(Slot entity)
		{

			if (entity.IsTransient()) return entity;
			Slot other = GetSlot(entity.SlotId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Slot set  [NewsGuid]=@NewsGuid
							, [Title]=@Title
							, [Description]=@Description
							, [ThumbGuid]=@ThumbGuid
							, [SequnceNumber]=@SequnceNumber
							, [SlotTypeId]=@SlotTypeId
							, [SegmentId]=@SegmentId
							, [CategoryId]=@CategoryId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [TranslatedDescription]=@TranslatedDescription
							, [TranslatedTitle]=@TranslatedTitle
							, [LanguageCode]=@LanguageCode
							, [PreviewGuid]=@PreviewGuid
							, [UserId]=@UserId
							, [TickerId]=@TickerId
							, [IncludeInRundown]=@IncludeInRundown
							, [NewsFileId]=@NewsFileId
							, [ResearchText]=@ResearchText
							, [AnchorId]=@AnchorId
							, [Script]=@Script 
							 where SlotId=@SlotId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@Title",entity.Title ?? (object)DBNull.Value)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid ?? (object)DBNull.Value)
					, new SqlParameter("@SequnceNumber",entity.SequnceNumber)
					, new SqlParameter("@SlotTypeId",entity.SlotTypeId)
					, new SqlParameter("@SegmentId",entity.SegmentId)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@TranslatedDescription",entity.TranslatedDescription ?? (object)DBNull.Value)
					, new SqlParameter("@TranslatedTitle",entity.TranslatedTitle ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@PreviewGuid",entity.PreviewGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@IncludeInRundown",entity.IncludeInRundown ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@ResearchText",entity.ResearchText ?? (object)DBNull.Value)
					, new SqlParameter("@AnchorId",entity.AnchorId ?? (object)DBNull.Value)
					, new SqlParameter("@Script",entity.Script ?? (object)DBNull.Value)
					, new SqlParameter("@SlotId",entity.SlotId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlot(entity.SlotId);
		}

		public virtual bool DeleteSlot(System.Int32 SlotId)
		{

			string sql="delete from Slot where SlotId=@SlotId";
			SqlParameter parameter=new SqlParameter("@SlotId",SlotId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Slot))]
		public virtual Slot DeleteSlot(Slot entity)
		{
			this.DeleteSlot(entity.SlotId);
			return entity;
		}


		public virtual Slot SlotFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Slot entity=new Slot();
			if (dr.Table.Columns.Contains("SlotId"))
			{
			entity.SlotId = (System.Int32)dr["SlotId"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("Title"))
			{
			entity.Title = dr["Title"].ToString();
			}
			if (dr.Table.Columns.Contains("Description"))
			{
			entity.Description = dr["Description"].ToString();
			}
			if (dr.Table.Columns.Contains("ThumbGuid"))
			{
			entity.ThumbGuid = dr["ThumbGuid"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["ThumbGuid"];
			}
			if (dr.Table.Columns.Contains("SequnceNumber"))
			{
			entity.SequnceNumber = (System.Int32)dr["SequnceNumber"];
			}
			if (dr.Table.Columns.Contains("SlotTypeId"))
			{
			entity.SlotTypeId = (System.Int32)dr["SlotTypeId"];
			}
			if (dr.Table.Columns.Contains("SegmentId"))
			{
			entity.SegmentId = (System.Int32)dr["SegmentId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("TranslatedDescription"))
			{
			entity.TranslatedDescription = dr["TranslatedDescription"].ToString();
			}
			if (dr.Table.Columns.Contains("TranslatedTitle"))
			{
			entity.TranslatedTitle = dr["TranslatedTitle"].ToString();
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("PreviewGuid"))
			{
			entity.PreviewGuid = dr["PreviewGuid"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["PreviewGuid"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = dr["UserId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = dr["TickerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("IncludeInRundown"))
			{
			entity.IncludeInRundown = dr["IncludeInRundown"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IncludeInRundown"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = dr["NewsFileId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("ResearchText"))
			{
			entity.ResearchText = dr["ResearchText"].ToString();
			}
			if (dr.Table.Columns.Contains("AnchorId"))
			{
			entity.AnchorId = dr["AnchorId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["AnchorId"];
			}
			if (dr.Table.Columns.Contains("Script"))
			{
			entity.Script = dr["Script"].ToString();
			}
			return entity;
		}

	}
	
	
}
