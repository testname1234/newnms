﻿using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
    public partial class SlotRepository : SlotRepositoryBase, ISlotRepository
    {
        public virtual Slot UpdateSlotSequence(Slot entity)
        {
            string sql = @"Update Slot
                            set
                               [SequnceNumber]=@SequnceNumber,
                               [SegmentId]=@SegmentId,
                               [UserId]=@UserId,
							   [LastUpdateDate]=@LastUpdateDate,
                               [NewsFileId]=@NewsFileId,
                               [Title]=@Title,
                               [Description]=@Description,
                               [ResearchText]=@ResearchText,
                                [AnchorId]=@AnchorId
							 where SlotId=@SlotId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                    new SqlParameter("@SequnceNumber",entity.SequnceNumber)
                    ,new SqlParameter("@SegmentId",entity.SegmentId)
                    , new SqlParameter("@UserId", entity.UserId ?? (object)DBNull.Value)
                    , new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
                    , new SqlParameter("@SlotId",entity.SlotId)
                    , new SqlParameter("@NewsFileId",entity.NewsFileId)
                    , new SqlParameter("@Title",entity.Title)
                    , new SqlParameter("@Description",string.IsNullOrEmpty(entity.Description)?string.Empty: entity.Description)
                    , new SqlParameter("@ResearchText",string.IsNullOrEmpty(entity.ResearchText)?string.Empty: entity.ResearchText)
             , new SqlParameter("@AnchorId", entity.AnchorId ?? (object)DBNull.Value)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetSlot(entity.SlotId);
        }

        public List<Slot> GetSlotBySegmentAndNews(System.Int32 SegmentId, System.String NewsGuid, int? TickerId)
        {
            string sql = GetSlotSelectClause();
            sql += "from Slot with (nolock)  where SegmentId=@SegmentId and NewsGuid=@NewsGuid and TickerId = @TickerId";

            SqlParameter parameter1 = new SqlParameter("@SegmentId", SegmentId);
            SqlParameter parameter2 = new SqlParameter("@NewsGuid", string.IsNullOrEmpty(NewsGuid) ? (object)DBNull.Value : NewsGuid);
            SqlParameter parameter3 = new SqlParameter("@TickerId", TickerId ?? (object)DBNull.Value);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2, parameter3 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Slot>(ds, SlotFromDataRow);
        }


        public List<Slot> GetSlotsByEpisodeId(int EpisodeId)
        {
            string sql = "select * from slot s inner join segment sg on sg.segmentid = s.segmentid where episodeid = @id";
            SqlParameter parameter1 = new SqlParameter("@Id", EpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Slot>(ds, SlotFromDataRow);
        }


        public Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, System.String NewsGuid)
        {
            string sql = " select top 1 s.* from slot s (nolock) inner join Segment sg (nolock) on sg.SegmentId = s.SegmentId where sg.EpisodeId = @EpisodeId and s.NewsGuid = @NewsGuid";

            SqlParameter parameter1 = new SqlParameter("@EpisodeId", EpisodeId);
            SqlParameter parameter2 = new SqlParameter("@NewsGuid", NewsGuid);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return SlotFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Slot GetSlotByEpisodeAndNewsFileId(System.Int32 EpisodeId, System.Int32 NewsFileId)
        {
            string sql = " select top 1 s.* from slot s (nolock) inner join Segment sg (nolock) on sg.SegmentId = s.SegmentId where sg.EpisodeId = @EpisodeId and s.NewsFileId = @NewsFileId";

            SqlParameter parameter1 = new SqlParameter("@EpisodeId", EpisodeId);
            SqlParameter parameter2 = new SqlParameter("@NewsFileId", NewsFileId);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return SlotFromDataRow(ds.Tables[0].Rows[0]);
        }

        public Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, int TickerId)
        {
            string sql = " select top 1 s.* from slot s (nolock) inner join Segment sg (nolock) on sg.SegmentId = s.SegmentId where sg.EpisodeId = @EpisodeId and s.TickerId = @TickerId";

            SqlParameter parameter1 = new SqlParameter("@EpisodeId", EpisodeId);
            SqlParameter parameter2 = new SqlParameter("@TickerId", TickerId);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return SlotFromDataRow(ds.Tables[0].Rows[0]);
        }

        public virtual List<Slot> GetSlotByEpisode(System.Int32 EpisodeId, string SelectClause = null)
        {
            string sql = "Slot_GetByEpisodeId";
            SqlParameter parameter1 = new SqlParameter("@EpisodeId", EpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.StoredProcedure, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Slot>(ds, SlotFromDataRow);
        }


        public int GetEpisodeId(int slotId)
        {
            string sql = "select episodeid from segment where segmentid =(select segmentid from slot where slotid=@slotId)";
            SqlParameter parameter = new SqlParameter("@slotId", slotId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return -1;
            return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
        }
    }
}