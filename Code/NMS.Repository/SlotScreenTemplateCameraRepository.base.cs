﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotScreenTemplateCameraRepositoryBase : Repository, ISlotScreenTemplateCameraRepositoryBase 
	{
        
        public SlotScreenTemplateCameraRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotScreenTemplateCameraId",new SearchColumn(){Name="SlotScreenTemplateCameraId",Title="SlotScreenTemplateCameraId",SelectClause="SlotScreenTemplateCameraId",WhereClause="AllRecords.SlotScreenTemplateCameraId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplateCameraId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotScreenTemplateId",new SearchColumn(){Name="SlotScreenTemplateId",Title="SlotScreenTemplateId",SelectClause="SlotScreenTemplateId",WhereClause="AllRecords.SlotScreenTemplateId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CameraTypeId",new SearchColumn(){Name="CameraTypeId",Title="CameraTypeId",SelectClause="CameraTypeId",WhereClause="AllRecords.CameraTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="CameraTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsOn",new SearchColumn(){Name="IsOn",Title="IsOn",SelectClause="IsOn",WhereClause="AllRecords.IsOn",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsOn",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotScreenTemplateCameraSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotScreenTemplateCameraBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotScreenTemplateCameraAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotScreenTemplateCameraSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlotScreenTemplateCamera].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlotScreenTemplateCamera].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateCameraSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateCamera] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateId",SlotScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateCamera>(ds,SlotScreenTemplateCameraFromDataRow);
		}

		public virtual List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraByCameraTypeId(System.Int32 CameraTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateCameraSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateCamera] with (nolock)  where CameraTypeId=@CameraTypeId  ";
			SqlParameter parameter=new SqlParameter("@CameraTypeId",CameraTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateCamera>(ds,SlotScreenTemplateCameraFromDataRow);
		}

		public virtual SlotScreenTemplateCamera GetSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateCameraSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateCamera] with (nolock)  where SlotScreenTemplateCameraId=@SlotScreenTemplateCameraId ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateCameraId",SlotScreenTemplateCameraId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotScreenTemplateCameraFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateCameraSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlotScreenTemplateCamera] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateCamera>(ds,SlotScreenTemplateCameraFromDataRow);
		}

		public virtual List<SlotScreenTemplateCamera> GetAllSlotScreenTemplateCamera(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateCameraSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateCamera] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateCamera>(ds, SlotScreenTemplateCameraFromDataRow);
		}

		public virtual List<SlotScreenTemplateCamera> GetPagedSlotScreenTemplateCamera(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotScreenTemplateCameraCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotScreenTemplateCameraId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotScreenTemplateCameraId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotScreenTemplateCameraId] ";
            tempsql += " FROM [SlotScreenTemplateCamera] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotScreenTemplateCameraId"))
					tempsql += " , (AllRecords.[SlotScreenTemplateCameraId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotScreenTemplateCameraId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotScreenTemplateCameraSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlotScreenTemplateCamera] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlotScreenTemplateCamera].[SlotScreenTemplateCameraId] = PageIndex.[SlotScreenTemplateCameraId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateCamera>(ds, SlotScreenTemplateCameraFromDataRow);
			}else{ return null;}
		}

		private int GetSlotScreenTemplateCameraCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlotScreenTemplateCamera as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlotScreenTemplateCamera as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlotScreenTemplateCamera))]
		public virtual SlotScreenTemplateCamera InsertSlotScreenTemplateCamera(SlotScreenTemplateCamera entity)
		{

			SlotScreenTemplateCamera other=new SlotScreenTemplateCamera();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlotScreenTemplateCamera ( [SlotScreenTemplateId]
				,[CameraTypeId]
				,[Name]
				,[IsOn]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @SlotScreenTemplateId
				, @CameraTypeId
				, @Name
				, @IsOn
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)
					, new SqlParameter("@CameraTypeId",entity.CameraTypeId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@IsOn",entity.IsOn)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlotScreenTemplateCamera(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlotScreenTemplateCamera))]
		public virtual SlotScreenTemplateCamera UpdateSlotScreenTemplateCamera(SlotScreenTemplateCamera entity)
		{

			if (entity.IsTransient()) return entity;
			SlotScreenTemplateCamera other = GetSlotScreenTemplateCamera(entity.SlotScreenTemplateCameraId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlotScreenTemplateCamera set  [SlotScreenTemplateId]=@SlotScreenTemplateId
							, [CameraTypeId]=@CameraTypeId
							, [Name]=@Name
							, [IsOn]=@IsOn
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where SlotScreenTemplateCameraId=@SlotScreenTemplateCameraId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)
					, new SqlParameter("@CameraTypeId",entity.CameraTypeId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@IsOn",entity.IsOn)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@SlotScreenTemplateCameraId",entity.SlotScreenTemplateCameraId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlotScreenTemplateCamera(entity.SlotScreenTemplateCameraId);
		}

		public virtual bool DeleteSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId)
		{

			string sql="delete from SlotScreenTemplateCamera where SlotScreenTemplateCameraId=@SlotScreenTemplateCameraId";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateCameraId",SlotScreenTemplateCameraId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlotScreenTemplateCamera))]
		public virtual SlotScreenTemplateCamera DeleteSlotScreenTemplateCamera(SlotScreenTemplateCamera entity)
		{
			this.DeleteSlotScreenTemplateCamera(entity.SlotScreenTemplateCameraId);
			return entity;
		}


		public virtual SlotScreenTemplateCamera SlotScreenTemplateCameraFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlotScreenTemplateCamera entity=new SlotScreenTemplateCamera();
			if (dr.Table.Columns.Contains("SlotScreenTemplateCameraId"))
			{
			entity.SlotScreenTemplateCameraId = (System.Int32)dr["SlotScreenTemplateCameraId"];
			}
			if (dr.Table.Columns.Contains("SlotScreenTemplateId"))
			{
			entity.SlotScreenTemplateId = (System.Int32)dr["SlotScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("CameraTypeId"))
			{
			entity.CameraTypeId = (System.Int32)dr["CameraTypeId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("IsOn"))
			{
			entity.IsOn = (System.Boolean)dr["IsOn"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
