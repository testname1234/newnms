﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotScreenTemplateMicRepositoryBase : Repository, ISlotScreenTemplateMicRepositoryBase 
	{
        
        public SlotScreenTemplateMicRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotScreenTemplateMicId",new SearchColumn(){Name="SlotScreenTemplateMicId",Title="SlotScreenTemplateMicId",SelectClause="SlotScreenTemplateMicId",WhereClause="AllRecords.SlotScreenTemplateMicId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplateMicId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotScreenTemplateId",new SearchColumn(){Name="SlotScreenTemplateId",Title="SlotScreenTemplateId",SelectClause="SlotScreenTemplateId",WhereClause="AllRecords.SlotScreenTemplateId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MicNo",new SearchColumn(){Name="MicNo",Title="MicNo",SelectClause="MicNo",WhereClause="AllRecords.MicNo",DataType="System.Int32",IsForeignColumn=false,PropertyName="MicNo",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsOn",new SearchColumn(){Name="IsOn",Title="IsOn",SelectClause="IsOn",WhereClause="AllRecords.IsOn",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsOn",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotScreenTemplateMicSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotScreenTemplateMicBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotScreenTemplateMicAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotScreenTemplateMicSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlotScreenTemplateMic].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlotScreenTemplateMic].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SlotScreenTemplateMic> GetSlotScreenTemplateMicBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateMicSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateMic] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateId",SlotScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateMic>(ds,SlotScreenTemplateMicFromDataRow);
		}

		public virtual SlotScreenTemplateMic GetSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateMicSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateMic] with (nolock)  where SlotScreenTemplateMicId=@SlotScreenTemplateMicId ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateMicId",SlotScreenTemplateMicId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotScreenTemplateMicFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlotScreenTemplateMic> GetSlotScreenTemplateMicByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateMicSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlotScreenTemplateMic] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateMic>(ds,SlotScreenTemplateMicFromDataRow);
		}

		public virtual List<SlotScreenTemplateMic> GetAllSlotScreenTemplateMic(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateMicSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateMic] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateMic>(ds, SlotScreenTemplateMicFromDataRow);
		}

		public virtual List<SlotScreenTemplateMic> GetPagedSlotScreenTemplateMic(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotScreenTemplateMicCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotScreenTemplateMicId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotScreenTemplateMicId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotScreenTemplateMicId] ";
            tempsql += " FROM [SlotScreenTemplateMic] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotScreenTemplateMicId"))
					tempsql += " , (AllRecords.[SlotScreenTemplateMicId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotScreenTemplateMicId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotScreenTemplateMicSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlotScreenTemplateMic] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlotScreenTemplateMic].[SlotScreenTemplateMicId] = PageIndex.[SlotScreenTemplateMicId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateMic>(ds, SlotScreenTemplateMicFromDataRow);
			}else{ return null;}
		}

		private int GetSlotScreenTemplateMicCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlotScreenTemplateMic as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlotScreenTemplateMic as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlotScreenTemplateMic))]
		public virtual SlotScreenTemplateMic InsertSlotScreenTemplateMic(SlotScreenTemplateMic entity)
		{

			SlotScreenTemplateMic other=new SlotScreenTemplateMic();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlotScreenTemplateMic ( [SlotScreenTemplateId]
				,[MicNo]
				,[IsOn]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @SlotScreenTemplateId
				, @MicNo
				, @IsOn
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)
					, new SqlParameter("@MicNo",entity.MicNo)
					, new SqlParameter("@IsOn",entity.IsOn)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlotScreenTemplateMic(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlotScreenTemplateMic))]
		public virtual SlotScreenTemplateMic UpdateSlotScreenTemplateMic(SlotScreenTemplateMic entity)
		{

			if (entity.IsTransient()) return entity;
			SlotScreenTemplateMic other = GetSlotScreenTemplateMic(entity.SlotScreenTemplateMicId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlotScreenTemplateMic set  [SlotScreenTemplateId]=@SlotScreenTemplateId
							, [MicNo]=@MicNo
							, [IsOn]=@IsOn
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where SlotScreenTemplateMicId=@SlotScreenTemplateMicId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)
					, new SqlParameter("@MicNo",entity.MicNo)
					, new SqlParameter("@IsOn",entity.IsOn)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@SlotScreenTemplateMicId",entity.SlotScreenTemplateMicId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlotScreenTemplateMic(entity.SlotScreenTemplateMicId);
		}

		public virtual bool DeleteSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId)
		{

			string sql="delete from SlotScreenTemplateMic where SlotScreenTemplateMicId=@SlotScreenTemplateMicId";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateMicId",SlotScreenTemplateMicId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlotScreenTemplateMic))]
		public virtual SlotScreenTemplateMic DeleteSlotScreenTemplateMic(SlotScreenTemplateMic entity)
		{
			this.DeleteSlotScreenTemplateMic(entity.SlotScreenTemplateMicId);
			return entity;
		}


		public virtual SlotScreenTemplateMic SlotScreenTemplateMicFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlotScreenTemplateMic entity=new SlotScreenTemplateMic();
			if (dr.Table.Columns.Contains("SlotScreenTemplateMicId"))
			{
			entity.SlotScreenTemplateMicId = (System.Int32)dr["SlotScreenTemplateMicId"];
			}
			if (dr.Table.Columns.Contains("SlotScreenTemplateId"))
			{
			entity.SlotScreenTemplateId = (System.Int32)dr["SlotScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("MicNo"))
			{
			entity.MicNo = (System.Int32)dr["MicNo"];
			}
			if (dr.Table.Columns.Contains("IsOn"))
			{
			entity.IsOn = (System.Boolean)dr["IsOn"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
