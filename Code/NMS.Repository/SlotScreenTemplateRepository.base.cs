﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotScreenTemplateRepositoryBase : Repository, ISlotScreenTemplateRepositoryBase 
	{
        
        public SlotScreenTemplateRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotScreenTemplateId",new SearchColumn(){Name="SlotScreenTemplateId",Title="SlotScreenTemplateId",SelectClause="SlotScreenTemplateId",WhereClause="AllRecords.SlotScreenTemplateId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotId",new SearchColumn(){Name="SlotId",Title="SlotId",SelectClause="SlotId",WhereClause="AllRecords.SlotId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScreenTemplateId",new SearchColumn(){Name="ScreenTemplateId",Title="ScreenTemplateId",SelectClause="ScreenTemplateId",WhereClause="AllRecords.ScreenTemplateId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsDefault",new SearchColumn(){Name="IsDefault",Title="IsDefault",SelectClause="IsDefault",WhereClause="AllRecords.IsDefault",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsDefault",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Html",new SearchColumn(){Name="Html",Title="Html",SelectClause="Html",WhereClause="AllRecords.Html",DataType="System.String",IsForeignColumn=false,PropertyName="Html",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Description",new SearchColumn(){Name="Description",Title="Description",SelectClause="Description",WhereClause="AllRecords.Description",DataType="System.String",IsForeignColumn=false,PropertyName="Description",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ThumbGuid",new SearchColumn(){Name="ThumbGuid",Title="ThumbGuid",SelectClause="ThumbGuid",WhereClause="AllRecords.ThumbGuid",DataType="System.Guid",IsForeignColumn=false,PropertyName="ThumbGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("GroupId",new SearchColumn(){Name="GroupId",Title="GroupId",SelectClause="GroupId",WhereClause="AllRecords.GroupId",DataType="System.String",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AssetId",new SearchColumn(){Name="AssetId",Title="AssetId",SelectClause="AssetId",WhereClause="AllRecords.AssetId",DataType="System.String",IsForeignColumn=false,PropertyName="AssetId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BackgroundColor",new SearchColumn(){Name="BackgroundColor",Title="BackgroundColor",SelectClause="BackgroundColor",WhereClause="AllRecords.BackgroundColor",DataType="System.String",IsForeignColumn=false,PropertyName="BackgroundColor",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BackgroundImageUrl",new SearchColumn(){Name="BackgroundImageUrl",Title="BackgroundImageUrl",SelectClause="BackgroundImageUrl",WhereClause="AllRecords.BackgroundImageUrl",DataType="System.Guid?",IsForeignColumn=false,PropertyName="BackgroundImageUrl",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("BackgroundRepeat",new SearchColumn(){Name="BackgroundRepeat",Title="BackgroundRepeat",SelectClause="BackgroundRepeat",WhereClause="AllRecords.BackgroundRepeat",DataType="System.String",IsForeignColumn=false,PropertyName="BackgroundRepeat",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatonDate",new SearchColumn(){Name="CreatonDate",Title="CreatonDate",SelectClause="CreatonDate",WhereClause="AllRecords.CreatonDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreatonDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Script",new SearchColumn(){Name="Script",Title="Script",SelectClause="Script",WhereClause="AllRecords.Script",DataType="System.String",IsForeignColumn=false,PropertyName="Script",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceNumber",new SearchColumn(){Name="SequenceNumber",Title="SequenceNumber",SelectClause="SequenceNumber",WhereClause="AllRecords.SequenceNumber",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SequenceNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsAssignedToNLE",new SearchColumn(){Name="IsAssignedToNLE",Title="IsAssignedToNLE",SelectClause="IsAssignedToNLE",WhereClause="AllRecords.IsAssignedToNLE",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsAssignedToNle",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsAssignedToStoryWriter",new SearchColumn(){Name="IsAssignedToStoryWriter",Title="IsAssignedToStoryWriter",SelectClause="IsAssignedToStoryWriter",WhereClause="AllRecords.IsAssignedToStoryWriter",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsAssignedToStoryWriter",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoDuration",new SearchColumn(){Name="VideoDuration",Title="VideoDuration",SelectClause="VideoDuration",WhereClause="AllRecords.VideoDuration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="VideoDuration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScriptDuration",new SearchColumn(){Name="ScriptDuration",Title="ScriptDuration",SelectClause="ScriptDuration",WhereClause="AllRecords.ScriptDuration",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ScriptDuration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Instructions",new SearchColumn(){Name="Instructions",Title="Instructions",SelectClause="Instructions",WhereClause="AllRecords.Instructions",DataType="System.String",IsForeignColumn=false,PropertyName="Instructions",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentId",new SearchColumn(){Name="ParentId",Title="ParentId",SelectClause="ParentId",WhereClause="AllRecords.ParentId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoWallId",new SearchColumn(){Name="VideoWallId",Title="VideoWallId",SelectClause="VideoWallId",WhereClause="AllRecords.VideoWallId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="VideoWallId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsVideoWallTemplate",new SearchColumn(){Name="IsVideoWallTemplate",Title="IsVideoWallTemplate",SelectClause="IsVideoWallTemplate",WhereClause="AllRecords.IsVideoWallTemplate",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsVideoWallTemplate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NLEStatusId",new SearchColumn(){Name="NLEStatusId",Title="NLEStatusId",SelectClause="NLEStatusId",WhereClause="AllRecords.NLEStatusId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NleStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StoryWriterStatusId",new SearchColumn(){Name="StoryWriterStatusId",Title="StoryWriterStatusId",SelectClause="StoryWriterStatusId",WhereClause="AllRecords.StoryWriterStatusId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="StoryWriterStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotScreenTemplateSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotScreenTemplateBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotScreenTemplateAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotScreenTemplateSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlotScreenTemplate].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlotScreenTemplate].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SlotScreenTemplate> GetSlotScreenTemplateBySlotId(System.Int32 SlotId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplate] with (nolock)  where SlotId=@SlotId  ";
			SqlParameter parameter=new SqlParameter("@SlotId",SlotId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplate>(ds,SlotScreenTemplateFromDataRow);
		}

		public virtual List<SlotScreenTemplate> GetSlotScreenTemplateByScreenTemplateId(System.Int32 ScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplate] with (nolock)  where ScreenTemplateId=@ScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@ScreenTemplateId",ScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplate>(ds,SlotScreenTemplateFromDataRow);
		}

		public virtual SlotScreenTemplate GetSlotScreenTemplate(System.Int32 SlotScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplate] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateId",SlotScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotScreenTemplateFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlotScreenTemplate> GetSlotScreenTemplateByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlotScreenTemplate] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplate>(ds,SlotScreenTemplateFromDataRow);
		}

		public virtual List<SlotScreenTemplate> GetAllSlotScreenTemplate(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplate] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplate>(ds, SlotScreenTemplateFromDataRow);
		}

		public virtual List<SlotScreenTemplate> GetPagedSlotScreenTemplate(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotScreenTemplateCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotScreenTemplateId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotScreenTemplateId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotScreenTemplateId] ";
            tempsql += " FROM [SlotScreenTemplate] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotScreenTemplateId"))
					tempsql += " , (AllRecords.[SlotScreenTemplateId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotScreenTemplateId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotScreenTemplateSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlotScreenTemplate] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlotScreenTemplate].[SlotScreenTemplateId] = PageIndex.[SlotScreenTemplateId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplate>(ds, SlotScreenTemplateFromDataRow);
			}else{ return null;}
		}

		private int GetSlotScreenTemplateCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlotScreenTemplate as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlotScreenTemplate as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlotScreenTemplate))]
		public virtual SlotScreenTemplate InsertSlotScreenTemplate(SlotScreenTemplate entity)
		{

			SlotScreenTemplate other=new SlotScreenTemplate();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlotScreenTemplate ( [SlotId]
				,[ScreenTemplateId]
				,[Name]
				,[IsDefault]
				,[Html]
				,[Description]
				,[Duration]
				,[ThumbGuid]
				,[GroupId]
				,[AssetId]
				,[BackgroundColor]
				,[BackgroundImageUrl]
				,[BackgroundRepeat]
				,[CreatonDate]
				,[LastUpdateDate]
				,[IsActive]
				,[Script]
				,[SequenceNumber]
				,[IsAssignedToNLE]
				,[IsAssignedToStoryWriter]
				,[VideoDuration]
				,[ScriptDuration]
				,[Instructions]
				,[ParentId]
				,[VideoWallId]
				,[IsVideoWallTemplate]
				,[StatusId]
				,[NLEStatusId]
				,[StoryWriterStatusId] )
				Values
				( @SlotId
				, @ScreenTemplateId
				, @Name
				, @IsDefault
				, @Html
				, @Description
				, @Duration
				, @ThumbGuid
				, @GroupId
				, @AssetId
				, @BackgroundColor
				, @BackgroundImageUrl
				, @BackgroundRepeat
				, @CreatonDate
				, @LastUpdateDate
				, @IsActive
				, @Script
				, @SequenceNumber
				, @IsAssignedToNLE
				, @IsAssignedToStoryWriter
				, @VideoDuration
				, @ScriptDuration
				, @Instructions
				, @ParentId
				, @VideoWallId
				, @IsVideoWallTemplate
				, @StatusId
				, @NLEStatusId
				, @StoryWriterStatusId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotId",entity.SlotId)
					, new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@IsDefault",entity.IsDefault)
					, new SqlParameter("@Html",entity.Html)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@AssetId",entity.AssetId ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundColor",entity.BackgroundColor ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundImageUrl",entity.BackgroundImageUrl ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundRepeat",entity.BackgroundRepeat ?? (object)DBNull.Value)
					, new SqlParameter("@CreatonDate",entity.CreatonDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@Script",entity.Script ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
					, new SqlParameter("@IsAssignedToNLE",entity.IsAssignedToNle ?? (object)DBNull.Value)
					, new SqlParameter("@IsAssignedToStoryWriter",entity.IsAssignedToStoryWriter ?? (object)DBNull.Value)
					, new SqlParameter("@VideoDuration",entity.VideoDuration ?? (object)DBNull.Value)
					, new SqlParameter("@ScriptDuration",entity.ScriptDuration ?? (object)DBNull.Value)
					, new SqlParameter("@Instructions",entity.Instructions ?? (object)DBNull.Value)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@VideoWallId",entity.VideoWallId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVideoWallTemplate",entity.IsVideoWallTemplate ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId ?? (object)DBNull.Value)
					, new SqlParameter("@NLEStatusId",entity.NleStatusId ?? (object)DBNull.Value)
					, new SqlParameter("@StoryWriterStatusId",entity.StoryWriterStatusId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlotScreenTemplate(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlotScreenTemplate))]
		public virtual SlotScreenTemplate UpdateSlotScreenTemplate(SlotScreenTemplate entity)
		{

			if (entity.IsTransient()) return entity;
			SlotScreenTemplate other = GetSlotScreenTemplate(entity.SlotScreenTemplateId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlotScreenTemplate set  [SlotId]=@SlotId
							, [ScreenTemplateId]=@ScreenTemplateId
							, [Name]=@Name
							, [IsDefault]=@IsDefault
							, [Html]=@Html
							, [Description]=@Description
							, [Duration]=@Duration
							, [ThumbGuid]=@ThumbGuid
							, [GroupId]=@GroupId
							, [AssetId]=@AssetId
							, [BackgroundColor]=@BackgroundColor
							, [BackgroundImageUrl]=@BackgroundImageUrl
							, [BackgroundRepeat]=@BackgroundRepeat
							, [CreatonDate]=@CreatonDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [Script]=@Script
							, [SequenceNumber]=@SequenceNumber
							, [IsAssignedToNLE]=@IsAssignedToNLE
							, [IsAssignedToStoryWriter]=@IsAssignedToStoryWriter
							, [VideoDuration]=@VideoDuration
							, [ScriptDuration]=@ScriptDuration
							, [Instructions]=@Instructions
							, [ParentId]=@ParentId
							, [VideoWallId]=@VideoWallId
							, [IsVideoWallTemplate]=@IsVideoWallTemplate
							, [StatusId]=@StatusId
							, [NLEStatusId]=@NLEStatusId
							, [StoryWriterStatusId]=@StoryWriterStatusId 
							 where SlotScreenTemplateId=@SlotScreenTemplateId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotId",entity.SlotId)
					, new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId)
					, new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@IsDefault",entity.IsDefault)
					, new SqlParameter("@Html",entity.Html)
					, new SqlParameter("@Description",entity.Description ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbGuid",entity.ThumbGuid)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@AssetId",entity.AssetId ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundColor",entity.BackgroundColor ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundImageUrl",entity.BackgroundImageUrl ?? (object)DBNull.Value)
					, new SqlParameter("@BackgroundRepeat",entity.BackgroundRepeat ?? (object)DBNull.Value)
					, new SqlParameter("@CreatonDate",entity.CreatonDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@Script",entity.Script ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
					, new SqlParameter("@IsAssignedToNLE",entity.IsAssignedToNle ?? (object)DBNull.Value)
					, new SqlParameter("@IsAssignedToStoryWriter",entity.IsAssignedToStoryWriter ?? (object)DBNull.Value)
					, new SqlParameter("@VideoDuration",entity.VideoDuration ?? (object)DBNull.Value)
					, new SqlParameter("@ScriptDuration",entity.ScriptDuration ?? (object)DBNull.Value)
					, new SqlParameter("@Instructions",entity.Instructions ?? (object)DBNull.Value)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@VideoWallId",entity.VideoWallId ?? (object)DBNull.Value)
					, new SqlParameter("@IsVideoWallTemplate",entity.IsVideoWallTemplate ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId ?? (object)DBNull.Value)
					, new SqlParameter("@NLEStatusId",entity.NleStatusId ?? (object)DBNull.Value)
					, new SqlParameter("@StoryWriterStatusId",entity.StoryWriterStatusId ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlotScreenTemplate(entity.SlotScreenTemplateId);
		}

		public virtual bool DeleteSlotScreenTemplate(System.Int32 SlotScreenTemplateId)
		{

			string sql="delete from SlotScreenTemplate where SlotScreenTemplateId=@SlotScreenTemplateId";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateId",SlotScreenTemplateId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlotScreenTemplate))]
		public virtual SlotScreenTemplate DeleteSlotScreenTemplate(SlotScreenTemplate entity)
		{
			this.DeleteSlotScreenTemplate(entity.SlotScreenTemplateId);
			return entity;
		}


		public virtual SlotScreenTemplate SlotScreenTemplateFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlotScreenTemplate entity=new SlotScreenTemplate();
			if (dr.Table.Columns.Contains("SlotScreenTemplateId"))
			{
			entity.SlotScreenTemplateId = (System.Int32)dr["SlotScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("SlotId"))
			{
			entity.SlotId = (System.Int32)dr["SlotId"];
			}
			if (dr.Table.Columns.Contains("ScreenTemplateId"))
			{
			entity.ScreenTemplateId = (System.Int32)dr["ScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("IsDefault"))
			{
			entity.IsDefault = (System.Boolean)dr["IsDefault"];
			}
			if (dr.Table.Columns.Contains("Html"))
			{
			entity.Html = dr["Html"].ToString();
			}
			if (dr.Table.Columns.Contains("Description"))
			{
			entity.Description = dr["Description"].ToString();
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("ThumbGuid"))
			{
			entity.ThumbGuid = (System.Guid)dr["ThumbGuid"];
			}
			if (dr.Table.Columns.Contains("GroupId"))
			{
			entity.GroupId = dr["GroupId"].ToString();
			}
			if (dr.Table.Columns.Contains("AssetId"))
			{
			entity.AssetId = dr["AssetId"].ToString();
			}
			if (dr.Table.Columns.Contains("BackgroundColor"))
			{
			entity.BackgroundColor = dr["BackgroundColor"].ToString();
			}
			if (dr.Table.Columns.Contains("BackgroundImageUrl"))
			{
			entity.BackgroundImageUrl = dr["BackgroundImageUrl"]==DBNull.Value?(System.Guid?)null:(System.Guid?)dr["BackgroundImageUrl"];
			}
			if (dr.Table.Columns.Contains("BackgroundRepeat"))
			{
			entity.BackgroundRepeat = dr["BackgroundRepeat"].ToString();
			}
			if (dr.Table.Columns.Contains("CreatonDate"))
			{
			entity.CreatonDate = (System.DateTime)dr["CreatonDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("Script"))
			{
			entity.Script = dr["Script"].ToString();
			}
			if (dr.Table.Columns.Contains("SequenceNumber"))
			{
			entity.SequenceNumber = dr["SequenceNumber"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SequenceNumber"];
			}
			if (dr.Table.Columns.Contains("IsAssignedToNLE"))
			{
			entity.IsAssignedToNle = dr["IsAssignedToNLE"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsAssignedToNLE"];
			}
			if (dr.Table.Columns.Contains("IsAssignedToStoryWriter"))
			{
			entity.IsAssignedToStoryWriter = dr["IsAssignedToStoryWriter"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsAssignedToStoryWriter"];
			}
			if (dr.Table.Columns.Contains("VideoDuration"))
			{
			entity.VideoDuration = dr["VideoDuration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["VideoDuration"];
			}
			if (dr.Table.Columns.Contains("ScriptDuration"))
			{
			entity.ScriptDuration = dr["ScriptDuration"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ScriptDuration"];
			}
			if (dr.Table.Columns.Contains("Instructions"))
			{
			entity.Instructions = dr["Instructions"].ToString();
			}
			if (dr.Table.Columns.Contains("ParentId"))
			{
			entity.ParentId = dr["ParentId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentId"];
			}
			if (dr.Table.Columns.Contains("VideoWallId"))
			{
			entity.VideoWallId = dr["VideoWallId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["VideoWallId"];
			}
			if (dr.Table.Columns.Contains("IsVideoWallTemplate"))
			{
			entity.IsVideoWallTemplate = dr["IsVideoWallTemplate"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsVideoWallTemplate"];
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = dr["StatusId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("NLEStatusId"))
			{
			entity.NleStatusId = dr["NLEStatusId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NLEStatusId"];
			}
			if (dr.Table.Columns.Contains("StoryWriterStatusId"))
			{
			entity.StoryWriterStatusId = dr["StoryWriterStatusId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["StoryWriterStatusId"];
			}
			return entity;
		}

	}
	
	
}
