﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Logging.Aspects;
using NMS.Logging.Enums;

namespace NMS.Repository
{
    public partial class SlotScreenTemplateRepository : SlotScreenTemplateRepositoryBase, ISlotScreenTemplateRepository
    {
        [LogMethod(HasPrimaryKey = true, Table = TableName.SlotScreenTemplate)]
        public override SlotScreenTemplate InsertSlotScreenTemplate(SlotScreenTemplate entity)
        {
            return base.InsertSlotScreenTemplate(entity);
        }

        [LogMethod(Table = TableName.SlotScreenTemplate)]
        public override SlotScreenTemplate UpdateSlotScreenTemplate(SlotScreenTemplate entity)
        {
            return base.UpdateSlotScreenTemplate(entity);
        }

        [LogMethod(Table = TableName.SlotScreenTemplate)]
        public override bool DeleteSlotScreenTemplate(System.Int32 SlotScreenTemplateId)
        {
            return base.DeleteSlotScreenTemplate(SlotScreenTemplateId);
        }

        [LogMethod(Table = TableName.SlotScreenTemplate)]
        public override SlotScreenTemplate DeleteSlotScreenTemplate(SlotScreenTemplate entity)
        {
            return base.DeleteSlotScreenTemplate(entity);
        }


        public virtual List<SlotScreenTemplate> GetSlotScreenTemplateBySlotAndScreenTemplateId(System.Int32 ScreenTemplateId, System.Int32 SlotId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetSlotScreenTemplateSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from SlotScreenTemplate with (nolock)  where SlotId=@SlotId and ScreenTemplateId=@ScreenTemplateId";
            SqlParameter parameter1 = new SqlParameter("@SlotId", SlotId);
            SqlParameter parameter2 = new SqlParameter("@ScreenTemplateId", ScreenTemplateId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SlotScreenTemplate>(ds, SlotScreenTemplateFromDataRow);
        }

        public virtual List<SlotScreenTemplate> GetByEpisodeId(System.Int32 episodeId, string SelectClause = null)
        {
            string sql = "GetSLotScreenTemplateByEpisodeId";
            SqlParameter parameter1 = new SqlParameter("@EpisodeId", episodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.StoredProcedure, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SlotScreenTemplate>(ds, SlotScreenTemplateFromDataRow);
        }

        public virtual int GetProgramIdBySlotScreenTemplateId(System.Int32 slotScreenTemplateId)
        {
            string sql = @"select d.ProgramId
                        from SlotScreenTemplate a inner join Slot b on a.SlotId = b.SlotId
	                        inner join Segment c on c.SegmentId = b.SegmentId
	                        inner join Episode d on d.EpisodeId = c.EpisodeId
                        where SlotScreenTemplateId = @SlotScreenTemplateId";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", slotScreenTemplateId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return 0;
            return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
        }

        public virtual int GetVideoWallId(int SetId, int slotScreenTemplateId)
        {
            string sql = @"	select isnull(VideoWallId,isnull(WallId,0)) as VideoWallId
            from SlotScreenTemplate (nolock) sst 
            inner join SetWallMapping (nolock) sw on sw.SetId = @setId
            inner join ScreenTemplate (nolock) st on st.ScreenTemplateId = sst.ScreenTemplateId and sw.RatioTypeId = st.RatioTypeId and isnull(st.IsVideoWallTemplate,0) = 1 and st.IsActive = 1
            where sst.SlotScreenTemplateId = @SlotScreenTemplateId";
            SqlParameter parameter1 = new SqlParameter("@SlotScreenTemplateId", slotScreenTemplateId);
            SqlParameter parameter2 = new SqlParameter("@SetId", SetId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return 0;
            if (ds.Tables[0].Rows[0][0] == null || Convert.ToInt32(ds.Tables[0].Rows[0][0]) == 0)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
        }

        public virtual SlotScreenTemplate UpdateSlotScreenTemplateSequenceAndScript(SlotScreenTemplate entity)
        {
            if (entity.IsTransient()) return entity;
            SlotScreenTemplate other = GetSlotScreenTemplate(entity.SlotScreenTemplateId);
            if (entity.Equals(other)) return entity;
            string sql = @"Update SlotScreenTemplate set 
							[SequenceNumber]=@SequenceNumber ,
                            [Script]=@Script,
                            [Duration]=@Duration,
                            [VideoDuration]=@VideoDuration,
                            [ScriptDuration]=@ScriptDuration,
                            [IsAssignedToStoryWriter]=@IsAssignedToStoryWriter,
                            [IsAssignedToNle]=@IsAssignedToNle
							where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
                      ,new SqlParameter("@Script",entity.Script?? string.Empty)
                      ,new SqlParameter("@Duration",entity.Duration?? 0)
                      ,new SqlParameter("@VideoDuration",entity.VideoDuration ?? 0)
                      ,new SqlParameter("@ScriptDuration",entity.ScriptDuration ?? 0)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)
                    , new SqlParameter("@IsAssignedToNle",entity.IsAssignedToNle??false)
                    , new SqlParameter("@IsAssignedToStoryWriter",entity.IsAssignedToStoryWriter)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetSlotScreenTemplate(entity.SlotScreenTemplateId);
        }

        public virtual SlotScreenTemplate UpdateSlotScreenTemplateSequence(SlotScreenTemplate entity)
        {
            if (entity.IsTransient()) return entity;
            SlotScreenTemplate other = GetSlotScreenTemplate(entity.SlotScreenTemplateId);
            if (entity.Equals(other)) return entity;
            string sql = @"Update SlotScreenTemplate set 
							[SequenceNumber]=@SequenceNumber 
							 where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetSlotScreenTemplate(entity.SlotScreenTemplateId);
        }

        public virtual SlotScreenTemplate UpdateSlotScreenTemplateThumb(SlotScreenTemplate entity)
        {
            string sql = @"Update SlotScreenTemplate set 
							ThumbGuid=@ThumbGuid                            
							 where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@ThumbGuid",entity.ThumbGuid)                      
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetSlotScreenTemplate(entity.SlotScreenTemplateId);
        }

        public virtual bool DeleteSlotScreenTemplatebySlotId(System.Int32 SlotId)
        {
            string sql = "delete from SlotScreenTemplate where SlotId=@SlotId";
            SqlParameter parameter = new SqlParameter("@SlotId", SlotId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public void UpdateSlotScreenScript(int slotScreenTemplateId, string script)
        {
            string sql = @"Update SlotScreenTemplate set 
                            [Script]=@Script
							where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter[] parameterArray = new SqlParameter[]{
                      new SqlParameter("@Script",script),
					 new SqlParameter("@SlotScreenTemplateId",slotScreenTemplateId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
        }

        public List<SlotScreenTemplate> GetSlotScreenTemplateByParentId(int parentId)
        {
            string sql = GetSlotScreenTemplateSelectClause();
            sql += " from [SlotScreenTemplate] with (nolock)  where ParentId = @ParentId";
            SqlParameter parameter = new SqlParameter("@ParentId", parentId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SlotScreenTemplate>(ds, SlotScreenTemplateFromDataRow);
        }

        public bool UpdateSlotScreenTemplateStatus(int slotScreenTemplateId, int? statusId, int? nleStatusId, int? storyWriterStatusId)
        {
            string sql = @"Update SlotScreenTemplate 
                           set 
                                [NLEStatusId] = ISNULL(@NLEStatusId, [NLEStatusId]),
                                [StoryWriterStatusId] = ISNULL(@StoryWriterStatusId, [StoryWriterStatusId]),
                                LastUpdateDate = GETUTCDATE()
                           where 
                                SlotScreenTemplateId = @SlotScreenTemplateId";
            
            SqlParameter[] parameterArray = new SqlParameter[]
            {
                new SqlParameter("@NLEStatusId", nleStatusId.HasValue ? nleStatusId.Value : (object)DBNull.Value),
                new SqlParameter("@StoryWriterStatusId", storyWriterStatusId.HasValue ? storyWriterStatusId.Value : (object)DBNull.Value),
				new SqlParameter("@SlotScreenTemplateId", slotScreenTemplateId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }
    }
}
