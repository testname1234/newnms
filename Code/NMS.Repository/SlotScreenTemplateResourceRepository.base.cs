﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotScreenTemplateResourceRepositoryBase : Repository, ISlotScreenTemplateResourceRepositoryBase 
	{
        
        public SlotScreenTemplateResourceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotScreenTemplateResourceId",new SearchColumn(){Name="SlotScreenTemplateResourceId",Title="SlotScreenTemplateResourceId",SelectClause="SlotScreenTemplateResourceId",WhereClause="AllRecords.SlotScreenTemplateResourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplateResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotScreenTemplateId",new SearchColumn(){Name="SlotScreenTemplateId",Title="SlotScreenTemplateId",SelectClause="SlotScreenTemplateId",WhereClause="AllRecords.SlotScreenTemplateId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.Guid",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Comment",new SearchColumn(){Name="Comment",Title="Comment",SelectClause="Comment",WhereClause="AllRecords.Comment",DataType="System.String",IsForeignColumn=false,PropertyName="Comment",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsUploadedFromNLE",new SearchColumn(){Name="IsUploadedFromNLE",Title="IsUploadedFromNLE",SelectClause="IsUploadedFromNLE",WhereClause="AllRecords.IsUploadedFromNLE",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsUploadedFromNle",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Caption",new SearchColumn(){Name="Caption",Title="Caption",SelectClause="Caption",WhereClause="AllRecords.Caption",DataType="System.String",IsForeignColumn=false,PropertyName="Caption",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Category",new SearchColumn(){Name="Category",Title="Category",SelectClause="Category",WhereClause="AllRecords.Category",DataType="System.String",IsForeignColumn=false,PropertyName="Category",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Location",new SearchColumn(){Name="Location",Title="Location",SelectClause="Location",WhereClause="AllRecords.Location",DataType="System.String",IsForeignColumn=false,PropertyName="Location",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceTypeId",new SearchColumn(){Name="ResourceTypeId",Title="ResourceTypeId",SelectClause="ResourceTypeId",WhereClause="AllRecords.ResourceTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ResourceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotScreenTemplateResourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotScreenTemplateResourceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotScreenTemplateResourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotScreenTemplateResourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlotScreenTemplateResource].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlotScreenTemplateResource].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateResource] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateId",SlotScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateResource>(ds,SlotScreenTemplateResourceFromDataRow);
		}

		public virtual SlotScreenTemplateResource GetSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateResource] with (nolock)  where SlotScreenTemplateResourceId=@SlotScreenTemplateResourceId ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateResourceId",SlotScreenTemplateResourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotScreenTemplateResourceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlotScreenTemplateResource] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateResource>(ds,SlotScreenTemplateResourceFromDataRow);
		}

		public virtual List<SlotScreenTemplateResource> GetAllSlotScreenTemplateResource(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplateResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplateResource] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateResource>(ds, SlotScreenTemplateResourceFromDataRow);
		}

		public virtual List<SlotScreenTemplateResource> GetPagedSlotScreenTemplateResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotScreenTemplateResourceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotScreenTemplateResourceId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotScreenTemplateResourceId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotScreenTemplateResourceId] ";
            tempsql += " FROM [SlotScreenTemplateResource] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotScreenTemplateResourceId"))
					tempsql += " , (AllRecords.[SlotScreenTemplateResourceId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotScreenTemplateResourceId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotScreenTemplateResourceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlotScreenTemplateResource] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlotScreenTemplateResource].[SlotScreenTemplateResourceId] = PageIndex.[SlotScreenTemplateResourceId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplateResource>(ds, SlotScreenTemplateResourceFromDataRow);
			}else{ return null;}
		}

		private int GetSlotScreenTemplateResourceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlotScreenTemplateResource as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlotScreenTemplateResource as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlotScreenTemplateResource))]
		public virtual SlotScreenTemplateResource InsertSlotScreenTemplateResource(SlotScreenTemplateResource entity)
		{

			SlotScreenTemplateResource other=new SlotScreenTemplateResource();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlotScreenTemplateResource ( [SlotScreenTemplateId]
				,[ResourceGuid]
				,[Comment]
				,[IsUploadedFromNLE]
				,[CreationDate]
				,[Caption]
				,[Category]
				,[Location]
				,[Duration]
				,[ResourceTypeId] )
				Values
				( @SlotScreenTemplateId
				, @ResourceGuid
				, @Comment
				, @IsUploadedFromNLE
				, @CreationDate
				, @Caption
				, @Category
				, @Location
				, @Duration
				, @ResourceTypeId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@Comment",entity.Comment ?? (object)DBNull.Value)
					, new SqlParameter("@IsUploadedFromNLE",entity.IsUploadedFromNle)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Caption",entity.Caption ?? (object)DBNull.Value)
					, new SqlParameter("@Category",entity.Category ?? (object)DBNull.Value)
					, new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlotScreenTemplateResource(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlotScreenTemplateResource))]
		public virtual SlotScreenTemplateResource UpdateSlotScreenTemplateResource(SlotScreenTemplateResource entity)
		{

			if (entity.IsTransient()) return entity;
			SlotScreenTemplateResource other = GetSlotScreenTemplateResource(entity.SlotScreenTemplateResourceId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlotScreenTemplateResource set  [SlotScreenTemplateId]=@SlotScreenTemplateId
							, [ResourceGuid]=@ResourceGuid
							, [Comment]=@Comment
							, [IsUploadedFromNLE]=@IsUploadedFromNLE
							, [CreationDate]=@CreationDate
							, [Caption]=@Caption
							, [Category]=@Category
							, [Location]=@Location
							, [Duration]=@Duration
							, [ResourceTypeId]=@ResourceTypeId 
							 where SlotScreenTemplateResourceId=@SlotScreenTemplateResourceId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid)
					, new SqlParameter("@Comment",entity.Comment ?? (object)DBNull.Value)
					, new SqlParameter("@IsUploadedFromNLE",entity.IsUploadedFromNle)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@Caption",entity.Caption ?? (object)DBNull.Value)
					, new SqlParameter("@Category",entity.Category ?? (object)DBNull.Value)
					, new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplateResourceId",entity.SlotScreenTemplateResourceId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlotScreenTemplateResource(entity.SlotScreenTemplateResourceId);
		}

		public virtual bool DeleteSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId)
		{

			string sql="delete from SlotScreenTemplateResource where SlotScreenTemplateResourceId=@SlotScreenTemplateResourceId";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateResourceId",SlotScreenTemplateResourceId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlotScreenTemplateResource))]
		public virtual SlotScreenTemplateResource DeleteSlotScreenTemplateResource(SlotScreenTemplateResource entity)
		{
			this.DeleteSlotScreenTemplateResource(entity.SlotScreenTemplateResourceId);
			return entity;
		}


		public virtual SlotScreenTemplateResource SlotScreenTemplateResourceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlotScreenTemplateResource entity=new SlotScreenTemplateResource();
			if (dr.Table.Columns.Contains("SlotScreenTemplateResourceId"))
			{
			entity.SlotScreenTemplateResourceId = (System.Int32)dr["SlotScreenTemplateResourceId"];
			}
			if (dr.Table.Columns.Contains("SlotScreenTemplateId"))
			{
			entity.SlotScreenTemplateId = (System.Int32)dr["SlotScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = (System.Guid)dr["ResourceGuid"];
			}
			if (dr.Table.Columns.Contains("Comment"))
			{
			entity.Comment = dr["Comment"].ToString();
			}
			if (dr.Table.Columns.Contains("IsUploadedFromNLE"))
			{
			entity.IsUploadedFromNle = (System.Boolean)dr["IsUploadedFromNLE"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("Caption"))
			{
			entity.Caption = dr["Caption"].ToString();
			}
			if (dr.Table.Columns.Contains("Category"))
			{
			entity.Category = dr["Category"].ToString();
			}
			if (dr.Table.Columns.Contains("Location"))
			{
			entity.Location = dr["Location"].ToString();
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("ResourceTypeId"))
			{
			entity.ResourceTypeId = dr["ResourceTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ResourceTypeId"];
			}
			return entity;
		}

	}
	
	
}
