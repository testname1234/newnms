﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class SlotScreenTemplateResourceRepository : SlotScreenTemplateResourceRepositoryBase, ISlotScreenTemplateResourceRepository
    {

        public void DeleteAllBySlotScreenTemplateId(int slotScreenTemplateId)
        {
            string sql = "delete from SlotScreenTemplateResource where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", slotScreenTemplateId);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
        }

        public virtual List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceByEpisode(System.Int32 EpisodeId, string SelectClause = null)
        {
            string sql = "Sp_SlotScreenTemplateResourceByEpisode";
            SqlParameter parameter = new SqlParameter("@EpisodeId", EpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.StoredProcedure, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SlotScreenTemplateResource>(ds, SlotScreenTemplateResourceFromDataRow);
        }

        public virtual SlotScreenTemplateResource GetSlotScreenTemplateResourceByResourceGuid(string Guid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetSlotScreenTemplateResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [SlotScreenTemplateResource] with (nolock)  where ResourceGuid=@ResourceGuid ";
            SqlParameter parameter = new SqlParameter("@ResourceGuid", Guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return SlotScreenTemplateResourceFromDataRow(ds.Tables[0].Rows[0]);
        }


        
    }


}
