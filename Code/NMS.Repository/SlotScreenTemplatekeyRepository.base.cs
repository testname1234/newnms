﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotScreenTemplatekeyRepositoryBase : Repository, ISlotScreenTemplatekeyRepositoryBase 
	{
        
        public SlotScreenTemplatekeyRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotScreenTemplatekeyId",new SearchColumn(){Name="SlotScreenTemplatekeyId",Title="SlotScreenTemplatekeyId",SelectClause="SlotScreenTemplatekeyId",WhereClause="AllRecords.SlotScreenTemplatekeyId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotScreenTemplatekeyId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScreenTemplatekeyId",new SearchColumn(){Name="ScreenTemplatekeyId",Title="ScreenTemplatekeyId",SelectClause="ScreenTemplatekeyId",WhereClause="AllRecords.ScreenTemplatekeyId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ScreenTemplatekeyId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotScreenTemplateId",new SearchColumn(){Name="SlotScreenTemplateId",Title="SlotScreenTemplateId",SelectClause="SlotScreenTemplateId",WhereClause="AllRecords.SlotScreenTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlotScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("KeyName",new SearchColumn(){Name="KeyName",Title="KeyName",SelectClause="KeyName",WhereClause="AllRecords.KeyName",DataType="System.String",IsForeignColumn=false,PropertyName="KeyName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("KeyValue",new SearchColumn(){Name="KeyValue",Title="KeyValue",SelectClause="KeyValue",WhereClause="AllRecords.KeyValue",DataType="System.String",IsForeignColumn=false,PropertyName="KeyValue",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("creationDate",new SearchColumn(){Name="creationDate",Title="creationDate",SelectClause="creationDate",WhereClause="AllRecords.creationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("updateddate",new SearchColumn(){Name="updateddate",Title="updateddate",SelectClause="updateddate",WhereClause="AllRecords.updateddate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="Updateddate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("languagecode",new SearchColumn(){Name="languagecode",Title="languagecode",SelectClause="languagecode",WhereClause="AllRecords.languagecode",DataType="System.String",IsForeignColumn=false,PropertyName="Languagecode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Top",new SearchColumn(){Name="Top",Title="Top",SelectClause="Top",WhereClause="AllRecords.Top",DataType="System.Double?",IsForeignColumn=false,PropertyName="Top",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Bottom",new SearchColumn(){Name="Bottom",Title="Bottom",SelectClause="Bottom",WhereClause="AllRecords.Bottom",DataType="System.Double?",IsForeignColumn=false,PropertyName="Bottom",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("left",new SearchColumn(){Name="left",Title="left",SelectClause="left",WhereClause="AllRecords.left",DataType="System.Double?",IsForeignColumn=false,PropertyName="Left",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Right",new SearchColumn(){Name="Right",Title="Right",SelectClause="Right",WhereClause="AllRecords.Right",DataType="System.Double?",IsForeignColumn=false,PropertyName="Right",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotScreenTemplatekeySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotScreenTemplatekeyBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotScreenTemplatekeyAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotScreenTemplatekeySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlotScreenTemplatekey].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlotScreenTemplatekey].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyByScreenTemplatekeyId(System.Int32? ScreenTemplatekeyId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplatekey] with (nolock)  where ScreenTemplatekeyId=@ScreenTemplatekeyId  ";
			SqlParameter parameter=new SqlParameter("@ScreenTemplatekeyId",ScreenTemplatekeyId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplatekey>(ds,SlotScreenTemplatekeyFromDataRow);
		}

		public virtual List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyBySlotScreenTemplateId(System.Int32? SlotScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplatekey] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateId",SlotScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplatekey>(ds,SlotScreenTemplatekeyFromDataRow);
		}

		public virtual SlotScreenTemplatekey GetSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplatekey] with (nolock)  where SlotScreenTemplatekeyId=@SlotScreenTemplatekeyId ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplatekeyId",SlotScreenTemplatekeyId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotScreenTemplatekeyFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlotScreenTemplatekey] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplatekey>(ds,SlotScreenTemplatekeyFromDataRow);
		}

		public virtual List<SlotScreenTemplatekey> GetAllSlotScreenTemplatekey(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotScreenTemplatekey] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplatekey>(ds, SlotScreenTemplatekeyFromDataRow);
		}

		public virtual List<SlotScreenTemplatekey> GetPagedSlotScreenTemplatekey(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotScreenTemplatekeyCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotScreenTemplatekeyId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotScreenTemplatekeyId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotScreenTemplatekeyId] ";
            tempsql += " FROM [SlotScreenTemplatekey] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotScreenTemplatekeyId"))
					tempsql += " , (AllRecords.[SlotScreenTemplatekeyId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotScreenTemplatekeyId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotScreenTemplatekeySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlotScreenTemplatekey] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlotScreenTemplatekey].[SlotScreenTemplatekeyId] = PageIndex.[SlotScreenTemplatekeyId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotScreenTemplatekey>(ds, SlotScreenTemplatekeyFromDataRow);
			}else{ return null;}
		}

		private int GetSlotScreenTemplatekeyCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlotScreenTemplatekey as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlotScreenTemplatekey as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlotScreenTemplatekey))]
		public virtual SlotScreenTemplatekey InsertSlotScreenTemplatekey(SlotScreenTemplatekey entity)
		{

			SlotScreenTemplatekey other=new SlotScreenTemplatekey();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlotScreenTemplatekey ( [ScreenTemplatekeyId]
				,[SlotScreenTemplateId]
				,[KeyName]
				,[KeyValue]
				,[creationDate]
				,[updateddate]
				,[IsActive]
				,[languagecode]
				,[Top]
				,[Bottom]
				,[left]
				,[Right] )
				Values
				( @ScreenTemplatekeyId
				, @SlotScreenTemplateId
				, @KeyName
				, @KeyValue
				, @creationDate
				, @updateddate
				, @IsActive
				, @languagecode
				, @Top
				, @Bottom
				, @left
				, @Right );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenTemplatekeyId",entity.ScreenTemplatekeyId ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@KeyName",entity.KeyName ?? (object)DBNull.Value)
					, new SqlParameter("@KeyValue",entity.KeyValue ?? (object)DBNull.Value)
					, new SqlParameter("@creationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@updateddate",entity.Updateddate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@languagecode",entity.Languagecode ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlotScreenTemplatekey(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlotScreenTemplatekey))]
		public virtual SlotScreenTemplatekey UpdateSlotScreenTemplatekey(SlotScreenTemplatekey entity)
		{

			if (entity.IsTransient()) return entity;
			SlotScreenTemplatekey other = GetSlotScreenTemplatekey(entity.SlotScreenTemplatekeyId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlotScreenTemplatekey set  [ScreenTemplatekeyId]=@ScreenTemplatekeyId
							, [SlotScreenTemplateId]=@SlotScreenTemplateId
							, [KeyName]=@KeyName
							, [KeyValue]=@KeyValue
							, [creationDate]=@creationDate
							, [updateddate]=@updateddate
							, [IsActive]=@IsActive
							, [languagecode]=@languagecode
							, [Top]=@Top
							, [Bottom]=@Bottom
							, [left]=@left
							, [Right]=@Right 
							 where SlotScreenTemplatekeyId=@SlotScreenTemplatekeyId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenTemplatekeyId",entity.ScreenTemplatekeyId ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@KeyName",entity.KeyName ?? (object)DBNull.Value)
					, new SqlParameter("@KeyValue",entity.KeyValue ?? (object)DBNull.Value)
					, new SqlParameter("@creationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@updateddate",entity.Updateddate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@languagecode",entity.Languagecode ?? (object)DBNull.Value)
					, new SqlParameter("@Top",entity.Top ?? (object)DBNull.Value)
					, new SqlParameter("@Bottom",entity.Bottom ?? (object)DBNull.Value)
					, new SqlParameter("@left",entity.Left ?? (object)DBNull.Value)
					, new SqlParameter("@Right",entity.Right ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplatekeyId",entity.SlotScreenTemplatekeyId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlotScreenTemplatekey(entity.SlotScreenTemplatekeyId);
		}

		public virtual bool DeleteSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId)
		{

			string sql="delete from SlotScreenTemplatekey where SlotScreenTemplatekeyId=@SlotScreenTemplatekeyId";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplatekeyId",SlotScreenTemplatekeyId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlotScreenTemplatekey))]
		public virtual SlotScreenTemplatekey DeleteSlotScreenTemplatekey(SlotScreenTemplatekey entity)
		{
			this.DeleteSlotScreenTemplatekey(entity.SlotScreenTemplatekeyId);
			return entity;
		}


		public virtual SlotScreenTemplatekey SlotScreenTemplatekeyFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlotScreenTemplatekey entity=new SlotScreenTemplatekey();
			if (dr.Table.Columns.Contains("SlotScreenTemplatekeyId"))
			{
			entity.SlotScreenTemplatekeyId = (System.Int32)dr["SlotScreenTemplatekeyId"];
			}
			if (dr.Table.Columns.Contains("ScreenTemplatekeyId"))
			{
			entity.ScreenTemplatekeyId = dr["ScreenTemplatekeyId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ScreenTemplatekeyId"];
			}
			if (dr.Table.Columns.Contains("SlotScreenTemplateId"))
			{
			entity.SlotScreenTemplateId = dr["SlotScreenTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlotScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("KeyName"))
			{
			entity.KeyName = dr["KeyName"].ToString();
			}
			if (dr.Table.Columns.Contains("KeyValue"))
			{
			entity.KeyValue = dr["KeyValue"].ToString();
			}
			if (dr.Table.Columns.Contains("creationDate"))
			{
			entity.CreationDate = dr["creationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["creationDate"];
			}
			if (dr.Table.Columns.Contains("updateddate"))
			{
			entity.Updateddate = dr["updateddate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["updateddate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("languagecode"))
			{
			entity.Languagecode = dr["languagecode"].ToString();
			}
			if (dr.Table.Columns.Contains("Top"))
			{
			entity.Top = dr["Top"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Top"];
			}
			if (dr.Table.Columns.Contains("Bottom"))
			{
			entity.Bottom = dr["Bottom"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Bottom"];
			}
			if (dr.Table.Columns.Contains("left"))
			{
			entity.Left = dr["left"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["left"];
			}
			if (dr.Table.Columns.Contains("Right"))
			{
			entity.Right = dr["Right"]==DBNull.Value?(System.Double?)null:(System.Double?)dr["Right"];
			}
			return entity;
		}

	}
	
	
}
