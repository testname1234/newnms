﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class SlotScreenTemplatekeyRepository: SlotScreenTemplatekeyRepositoryBase, ISlotScreenTemplatekeyRepository
	{

        public virtual bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            string sql = "delete from SlotScreenTemplatekey where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }
	}
	
	
}
