﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class SlotTemplateScreenElementRepository: SlotTemplateScreenElementRepositoryBase, ISlotTemplateScreenElementRepository
	{
        public virtual bool DeleteSlotTemplateScreenElementBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {

            string sql = "delete from SlotTemplateScreenElement where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public SlotTemplateScreenElement UpdateSlotTemplateScreenElementResourceGuid(int slotTemplateScreenElementId, Guid? resourceGuid)
        {
            string sql = @"Update SlotTemplateScreenElement set  
							[ResourceGuid]=@ResourceGuid
							,[LastUpdateDate]=@LastUpdateDate
							 where SlotTemplateScreenElementId=@SlotTemplateScreenElementId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@ResourceGuid",resourceGuid?? (object)DBNull.Value)
                    , new SqlParameter("@LastUpdateDate", DateTime.UtcNow)
					, new SqlParameter("@SlotTemplateScreenElementId",slotTemplateScreenElementId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetSlotTemplateScreenElement(slotTemplateScreenElementId);
        }

        public bool DeleteSlotTemplateScreenElementBySlotId(System.Int32 SlotId)
        {

            string sql = "delete from SlotTemplateScreenElement where SlotId=@SlotId";
            SqlParameter parameter = new SqlParameter("@SlotId", SlotId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public List<SlotTemplateScreenElement> GetBySlotScreenTemplateIdWithFilter(System.Int32 SlotScreenTemplateId, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetSlotTemplateScreenElementSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from SlotTemplateScreenElement with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId and screenelementid in (3,13,8,22)";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SlotTemplateScreenElement>(ds, SlotTemplateScreenElementFromDataRow);
        }

        public SlotTemplateScreenElement UpdateSlotScreenTemplateElementCelebrity(int slotTemplateScreenElementId, int celebrityId)
        {
            string sql = @"Update SlotTemplateScreenElement set  
                            [CelebrityId]=@CelebrityId
							,[LastUpdateDate]=@LastUpdateDate
							 where SlotTemplateScreenElementId=@SlotTemplateScreenElementId";

            SqlParameter[] parameterArray = new SqlParameter[]{
					new SqlParameter("@CelebrityId", celebrityId)
                    , new SqlParameter("@LastUpdateDate", DateTime.UtcNow)
					, new SqlParameter("@SlotTemplateScreenElementId",slotTemplateScreenElementId)};

            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return GetSlotTemplateScreenElement(slotTemplateScreenElementId);
        }

        public List<SlotTemplateScreenElement> GetSlotTemplateScreenElementByEpisodeId(System.Int32 EpisodeId)
        {
            string sql = @"select t1.*
                        from
                        	SlotTemplateScreenElement t1 inner join SlotScreenTemplate t2 on t1.SlotScreenTemplateId = t2.SlotScreenTemplateId
                        	inner join Slot t3 on t2.SlotId = t3.SlotId
                        	inner join Segment t4 on t3.SegmentId = t4.SegmentId
                        	inner join Episode t5 on t4.EpisodeId = t5.EpisodeId
                        where
                        	t5.EpisodeId = @EpisodeId";
            SqlParameter parameter = new SqlParameter("@EpisodeId", EpisodeId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SlotTemplateScreenElement>(ds, SlotTemplateScreenElementFromDataRow);
        }
	}
	
	
}
