﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SlotTemplateScreenElementResourceRepositoryBase : Repository, ISlotTemplateScreenElementResourceRepositoryBase 
	{
        
        public SlotTemplateScreenElementResourceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SlotTemplateScreenElementResourceId",new SearchColumn(){Name="SlotTemplateScreenElementResourceId",Title="SlotTemplateScreenElementResourceId",SelectClause="SlotTemplateScreenElementResourceId",WhereClause="AllRecords.SlotTemplateScreenElementResourceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SlotTemplateScreenElementResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotTemplateScreenElementId",new SearchColumn(){Name="SlotTemplateScreenElementId",Title="SlotTemplateScreenElementId",SelectClause="SlotTemplateScreenElementId",WhereClause="AllRecords.SlotTemplateScreenElementId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlotTemplateScreenElementId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SlotScreenTemplateId",new SearchColumn(){Name="SlotScreenTemplateId",Title="SlotScreenTemplateId",SelectClause="SlotScreenTemplateId",WhereClause="AllRecords.SlotScreenTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SlotScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceGuid",new SearchColumn(){Name="ResourceGuid",Title="ResourceGuid",SelectClause="ResourceGuid",WhereClause="AllRecords.ResourceGuid",DataType="System.String",IsForeignColumn=false,PropertyName="ResourceGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Caption",new SearchColumn(){Name="Caption",Title="Caption",SelectClause="Caption",WhereClause="AllRecords.Caption",DataType="System.String",IsForeignColumn=false,PropertyName="Caption",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Category",new SearchColumn(){Name="Category",Title="Category",SelectClause="Category",WhereClause="AllRecords.Category",DataType="System.String",IsForeignColumn=false,PropertyName="Category",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Location",new SearchColumn(){Name="Location",Title="Location",SelectClause="Location",WhereClause="AllRecords.Location",DataType="System.String",IsForeignColumn=false,PropertyName="Location",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Duration",new SearchColumn(){Name="Duration",Title="Duration",SelectClause="Duration",WhereClause="AllRecords.Duration",DataType="System.Decimal?",IsForeignColumn=false,PropertyName="Duration",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceTypeId",new SearchColumn(){Name="ResourceTypeId",Title="ResourceTypeId",SelectClause="ResourceTypeId",WhereClause="AllRecords.ResourceTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ResourceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSlotTemplateScreenElementResourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSlotTemplateScreenElementResourceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSlotTemplateScreenElementResourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSlotTemplateScreenElementResourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SlotTemplateScreenElementResource].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SlotTemplateScreenElementResource].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(System.Int32? SlotTemplateScreenElementId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotTemplateScreenElementResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotTemplateScreenElementResource] with (nolock)  where SlotTemplateScreenElementId=@SlotTemplateScreenElementId  ";
			SqlParameter parameter=new SqlParameter("@SlotTemplateScreenElementId",SlotTemplateScreenElementId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotTemplateScreenElementResource>(ds,SlotTemplateScreenElementResourceFromDataRow);
		}

		public virtual List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceBySlotScreenTemplateId(System.Int32? SlotScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotTemplateScreenElementResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotTemplateScreenElementResource] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@SlotScreenTemplateId",SlotScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotTemplateScreenElementResource>(ds,SlotTemplateScreenElementResourceFromDataRow);
		}

		public virtual SlotTemplateScreenElementResource GetSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotTemplateScreenElementResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotTemplateScreenElementResource] with (nolock)  where SlotTemplateScreenElementResourceId=@SlotTemplateScreenElementResourceId ";
			SqlParameter parameter=new SqlParameter("@SlotTemplateScreenElementResourceId",SlotTemplateScreenElementResourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SlotTemplateScreenElementResourceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotTemplateScreenElementResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SlotTemplateScreenElementResource] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotTemplateScreenElementResource>(ds,SlotTemplateScreenElementResourceFromDataRow);
		}

		public virtual List<SlotTemplateScreenElementResource> GetAllSlotTemplateScreenElementResource(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSlotTemplateScreenElementResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SlotTemplateScreenElementResource] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotTemplateScreenElementResource>(ds, SlotTemplateScreenElementResourceFromDataRow);
		}

		public virtual List<SlotTemplateScreenElementResource> GetPagedSlotTemplateScreenElementResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSlotTemplateScreenElementResourceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SlotTemplateScreenElementResourceId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SlotTemplateScreenElementResourceId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SlotTemplateScreenElementResourceId] ";
            tempsql += " FROM [SlotTemplateScreenElementResource] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SlotTemplateScreenElementResourceId"))
					tempsql += " , (AllRecords.[SlotTemplateScreenElementResourceId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SlotTemplateScreenElementResourceId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSlotTemplateScreenElementResourceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SlotTemplateScreenElementResource] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SlotTemplateScreenElementResource].[SlotTemplateScreenElementResourceId] = PageIndex.[SlotTemplateScreenElementResourceId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SlotTemplateScreenElementResource>(ds, SlotTemplateScreenElementResourceFromDataRow);
			}else{ return null;}
		}

		private int GetSlotTemplateScreenElementResourceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SlotTemplateScreenElementResource as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SlotTemplateScreenElementResource as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SlotTemplateScreenElementResource))]
		public virtual SlotTemplateScreenElementResource InsertSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity)
		{

			SlotTemplateScreenElementResource other=new SlotTemplateScreenElementResource();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SlotTemplateScreenElementResource ( [SlotTemplateScreenElementId]
				,[SlotScreenTemplateId]
				,[ResourceGuid]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[Caption]
				,[Category]
				,[Location]
				,[Duration]
				,[ResourceTypeId] )
				Values
				( @SlotTemplateScreenElementId
				, @SlotScreenTemplateId
				, @ResourceGuid
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @Caption
				, @Category
				, @Location
				, @Duration
				, @ResourceTypeId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotTemplateScreenElementId",entity.SlotTemplateScreenElementId ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@Caption",entity.Caption ?? (object)DBNull.Value)
					, new SqlParameter("@Category",entity.Category ?? (object)DBNull.Value)
					, new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSlotTemplateScreenElementResource(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SlotTemplateScreenElementResource))]
		public virtual SlotTemplateScreenElementResource UpdateSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity)
		{

			if (entity.IsTransient()) return entity;
			SlotTemplateScreenElementResource other = GetSlotTemplateScreenElementResource(entity.SlotTemplateScreenElementResourceId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SlotTemplateScreenElementResource set  [SlotTemplateScreenElementId]=@SlotTemplateScreenElementId
							, [SlotScreenTemplateId]=@SlotScreenTemplateId
							, [ResourceGuid]=@ResourceGuid
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [Caption]=@Caption
							, [Category]=@Category
							, [Location]=@Location
							, [Duration]=@Duration
							, [ResourceTypeId]=@ResourceTypeId 
							 where SlotTemplateScreenElementResourceId=@SlotTemplateScreenElementResourceId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@SlotTemplateScreenElementId",entity.SlotTemplateScreenElementId ?? (object)DBNull.Value)
					, new SqlParameter("@SlotScreenTemplateId",entity.SlotScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceGuid",entity.ResourceGuid ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@Caption",entity.Caption ?? (object)DBNull.Value)
					, new SqlParameter("@Category",entity.Category ?? (object)DBNull.Value)
					, new SqlParameter("@Location",entity.Location ?? (object)DBNull.Value)
					, new SqlParameter("@Duration",entity.Duration ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@SlotTemplateScreenElementResourceId",entity.SlotTemplateScreenElementResourceId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSlotTemplateScreenElementResource(entity.SlotTemplateScreenElementResourceId);
		}

		public virtual bool DeleteSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId)
		{

			string sql="delete from SlotTemplateScreenElementResource where SlotTemplateScreenElementResourceId=@SlotTemplateScreenElementResourceId";
			SqlParameter parameter=new SqlParameter("@SlotTemplateScreenElementResourceId",SlotTemplateScreenElementResourceId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SlotTemplateScreenElementResource))]
		public virtual SlotTemplateScreenElementResource DeleteSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity)
		{
			this.DeleteSlotTemplateScreenElementResource(entity.SlotTemplateScreenElementResourceId);
			return entity;
		}


		public virtual SlotTemplateScreenElementResource SlotTemplateScreenElementResourceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SlotTemplateScreenElementResource entity=new SlotTemplateScreenElementResource();
			if (dr.Table.Columns.Contains("SlotTemplateScreenElementResourceId"))
			{
			entity.SlotTemplateScreenElementResourceId = (System.Int32)dr["SlotTemplateScreenElementResourceId"];
			}
			if (dr.Table.Columns.Contains("SlotTemplateScreenElementId"))
			{
			entity.SlotTemplateScreenElementId = dr["SlotTemplateScreenElementId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlotTemplateScreenElementId"];
			}
			if (dr.Table.Columns.Contains("SlotScreenTemplateId"))
			{
			entity.SlotScreenTemplateId = dr["SlotScreenTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SlotScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("ResourceGuid"))
			{
			entity.ResourceGuid = dr["ResourceGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("Caption"))
			{
			entity.Caption = dr["Caption"].ToString();
			}
			if (dr.Table.Columns.Contains("Category"))
			{
			entity.Category = dr["Category"].ToString();
			}
			if (dr.Table.Columns.Contains("Location"))
			{
			entity.Location = dr["Location"].ToString();
			}
			if (dr.Table.Columns.Contains("Duration"))
			{
			entity.Duration = dr["Duration"]==DBNull.Value?(System.Decimal?)null:(System.Decimal?)dr["Duration"];
			}
			if (dr.Table.Columns.Contains("ResourceTypeId"))
			{
			entity.ResourceTypeId = dr["ResourceTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ResourceTypeId"];
			}
			return entity;
		}

	}
	
	
}
