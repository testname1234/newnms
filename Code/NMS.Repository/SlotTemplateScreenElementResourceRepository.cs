﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class SlotTemplateScreenElementResourceRepository: SlotTemplateScreenElementResourceRepositoryBase, ISlotTemplateScreenElementResourceRepository
	{
        public virtual bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            string sql = "delete from SlotTemplateScreenElementResource where SlotScreenTemplateId=@SlotScreenTemplateId";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual bool DeleteBySlotTemplateScreenElementId(System.Int32 SlotTemplateScreenElementId)
        {
            string sql = "delete from SlotTemplateScreenElementResource where SlotTemplateScreenElementId=@SlotTemplateScreenElementId";
            SqlParameter parameter = new SqlParameter("@SlotTemplateScreenElementId", SlotTemplateScreenElementId);
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public virtual List<SlotTemplateScreenElementResource> GetBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetSlotTemplateScreenElementResourceSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [SlotTemplateScreenElementResource] with (nolock)  where SlotScreenTemplateId=@SlotScreenTemplateId  ";
            SqlParameter parameter = new SqlParameter("@SlotScreenTemplateId", SlotScreenTemplateId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SlotTemplateScreenElementResource>(ds, SlotTemplateScreenElementResourceFromDataRow);
        }
	}
	
	
}
