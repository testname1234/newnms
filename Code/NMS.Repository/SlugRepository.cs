﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class SlugRepository: SlugRepositoryBase, ISlugRepository
	{
        public virtual Slug GetBySlug(System.String Slug, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetSlugSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Slug] with (nolock)  where Slug=@Slug ";
            SqlParameter parameter = new SqlParameter("@Slug", Slug);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return SlugFromDataRow(ds.Tables[0].Rows[0]);
        }
	}
	
	
}
