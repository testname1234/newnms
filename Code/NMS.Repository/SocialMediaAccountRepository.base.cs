﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SocialMediaAccountRepositoryBase : Repository, ISocialMediaAccountRepositoryBase 
	{
        
        public SocialMediaAccountRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SocialMediaAccountId",new SearchColumn(){Name="SocialMediaAccountId",Title="SocialMediaAccountId",SelectClause="SocialMediaAccountId",WhereClause="AllRecords.SocialMediaAccountId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SocialMediaAccountId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserName",new SearchColumn(){Name="UserName",Title="UserName",SelectClause="UserName",WhereClause="AllRecords.UserName",DataType="System.String",IsForeignColumn=false,PropertyName="UserName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Url",new SearchColumn(){Name="Url",Title="Url",SelectClause="Url",WhereClause="AllRecords.Url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsFollowed",new SearchColumn(){Name="IsFollowed",Title="IsFollowed",SelectClause="IsFollowed",WhereClause="AllRecords.IsFollowed",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsFollowed",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Type",new SearchColumn(){Name="Type",Title="Type",SelectClause="Type",WhereClause="AllRecords.Type",DataType="System.String",IsForeignColumn=false,PropertyName="Type",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SocialMediaType",new SearchColumn(){Name="SocialMediaType",Title="SocialMediaType",SelectClause="SocialMediaType",WhereClause="AllRecords.SocialMediaType",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SocialMediaType",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSocialMediaAccountSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSocialMediaAccountBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSocialMediaAccountAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSocialMediaAccountSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SocialMediaAccount].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SocialMediaAccount].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<SocialMediaAccount> GetSocialMediaAccountByCelebrityId(System.Int32? CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSocialMediaAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SocialMediaAccount] with (nolock)  where CelebrityId=@CelebrityId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SocialMediaAccount>(ds,SocialMediaAccountFromDataRow);
		}

		public virtual SocialMediaAccount GetSocialMediaAccount(System.Int32 SocialMediaAccountId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSocialMediaAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SocialMediaAccount] with (nolock)  where SocialMediaAccountId=@SocialMediaAccountId ";
			SqlParameter parameter=new SqlParameter("@SocialMediaAccountId",SocialMediaAccountId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SocialMediaAccountFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SocialMediaAccount> GetSocialMediaAccountByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSocialMediaAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SocialMediaAccount] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SocialMediaAccount>(ds,SocialMediaAccountFromDataRow);
		}

		public virtual List<SocialMediaAccount> GetAllSocialMediaAccount(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSocialMediaAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SocialMediaAccount] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SocialMediaAccount>(ds, SocialMediaAccountFromDataRow);
		}

		public virtual List<SocialMediaAccount> GetPagedSocialMediaAccount(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSocialMediaAccountCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SocialMediaAccountId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SocialMediaAccountId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SocialMediaAccountId] ";
            tempsql += " FROM [SocialMediaAccount] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SocialMediaAccountId"))
					tempsql += " , (AllRecords.[SocialMediaAccountId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SocialMediaAccountId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSocialMediaAccountSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SocialMediaAccount] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SocialMediaAccount].[SocialMediaAccountId] = PageIndex.[SocialMediaAccountId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SocialMediaAccount>(ds, SocialMediaAccountFromDataRow);
			}else{ return null;}
		}

		private int GetSocialMediaAccountCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SocialMediaAccount as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SocialMediaAccount as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SocialMediaAccount))]
		public virtual SocialMediaAccount InsertSocialMediaAccount(SocialMediaAccount entity)
		{

			SocialMediaAccount other=new SocialMediaAccount();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SocialMediaAccount ( [UserName]
				,[CelebrityId]
				,[Url]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsFollowed]
				,[Type]
				,[SocialMediaType] )
				Values
				( @UserName
				, @CelebrityId
				, @Url
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsFollowed
				, @Type
				, @SocialMediaType );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserName",entity.UserName ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@IsFollowed",entity.IsFollowed ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@SocialMediaType",entity.SocialMediaType ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSocialMediaAccount(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SocialMediaAccount))]
		public virtual SocialMediaAccount UpdateSocialMediaAccount(SocialMediaAccount entity)
		{

			if (entity.IsTransient()) return entity;
			SocialMediaAccount other = GetSocialMediaAccount(entity.SocialMediaAccountId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SocialMediaAccount set  [UserName]=@UserName
							, [CelebrityId]=@CelebrityId
							, [Url]=@Url
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsFollowed]=@IsFollowed
							, [Type]=@Type
							, [SocialMediaType]=@SocialMediaType 
							 where SocialMediaAccountId=@SocialMediaAccountId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserName",entity.UserName ?? (object)DBNull.Value)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@IsFollowed",entity.IsFollowed ?? (object)DBNull.Value)
					, new SqlParameter("@Type",entity.Type ?? (object)DBNull.Value)
					, new SqlParameter("@SocialMediaType",entity.SocialMediaType ?? (object)DBNull.Value)
					, new SqlParameter("@SocialMediaAccountId",entity.SocialMediaAccountId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSocialMediaAccount(entity.SocialMediaAccountId);
		}

		public virtual bool DeleteSocialMediaAccount(System.Int32 SocialMediaAccountId)
		{

			string sql="delete from SocialMediaAccount where SocialMediaAccountId=@SocialMediaAccountId";
			SqlParameter parameter=new SqlParameter("@SocialMediaAccountId",SocialMediaAccountId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SocialMediaAccount))]
		public virtual SocialMediaAccount DeleteSocialMediaAccount(SocialMediaAccount entity)
		{
			this.DeleteSocialMediaAccount(entity.SocialMediaAccountId);
			return entity;
		}


		public virtual SocialMediaAccount SocialMediaAccountFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SocialMediaAccount entity=new SocialMediaAccount();
			if (dr.Table.Columns.Contains("SocialMediaAccountId"))
			{
			entity.SocialMediaAccountId = (System.Int32)dr["SocialMediaAccountId"];
			}
			if (dr.Table.Columns.Contains("UserName"))
			{
			entity.UserName = dr["UserName"].ToString();
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = dr["CelebrityId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("Url"))
			{
			entity.Url = dr["Url"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("IsFollowed"))
			{
			entity.IsFollowed = dr["IsFollowed"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsFollowed"];
			}
			if (dr.Table.Columns.Contains("Type"))
			{
			entity.Type = dr["Type"].ToString();
			}
			if (dr.Table.Columns.Contains("SocialMediaType"))
			{
			entity.SocialMediaType = dr["SocialMediaType"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SocialMediaType"];
			}
			return entity;
		}

	}
	
	
}
