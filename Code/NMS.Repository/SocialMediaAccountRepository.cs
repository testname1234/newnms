﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class SocialMediaAccountRepository: SocialMediaAccountRepositoryBase, ISocialMediaAccountRepository
	{
        public virtual List<SocialMediaAccount> GetBySocialMediaType(System.Int32 SocialMediaType, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetSocialMediaAccountSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [SocialMediaAccount] with (nolock)  where SocialMediaType=@SocialMediaType  ";
            SqlParameter parameter = new SqlParameter("@SocialMediaType", SocialMediaType);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SocialMediaAccount>(ds, SocialMediaAccountFromDataRow);
        }

        public virtual List<SocialMediaAccount> GetByIsFollowed(System.Boolean IsFollowed, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetSocialMediaAccountSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [SocialMediaAccount] with (nolock)  where IsFollowed=@IsFollowed  ";
            SqlParameter parameter = new SqlParameter("@IsFollowed", IsFollowed);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SocialMediaAccount>(ds, SocialMediaAccountFromDataRow);
        }

        public virtual void  UpdateTwitterAccountIsFollowed(int SocialMediaAccountId , Boolean IsFollowed)
        {
            string sql = @"Update SocialMediaAccount set 
							 [IsFollowed]=@IsFollowed
							 where SocialMediaAccountId=@SocialMediaAccountId";
            SqlParameter parameter = new SqlParameter("@SocialMediaAccountId", SocialMediaAccountId);
            SqlParameter parameter1 = new SqlParameter("@IsFollowed", IsFollowed);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
           
        }
	}
	
	
}
