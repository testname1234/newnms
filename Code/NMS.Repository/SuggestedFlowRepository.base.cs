﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class SuggestedFlowRepositoryBase : Repository, ISuggestedFlowRepositoryBase 
	{
        
        public SuggestedFlowRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("SuggestedFlowId",new SearchColumn(){Name="SuggestedFlowId",Title="SuggestedFlowId",SelectClause="SuggestedFlowId",WhereClause="AllRecords.SuggestedFlowId",DataType="System.Int32",IsForeignColumn=false,PropertyName="SuggestedFlowId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Key",new SearchColumn(){Name="Key",Title="Key",SelectClause="Key",WhereClause="AllRecords.Key",DataType="System.String",IsForeignColumn=false,PropertyName="Key",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ProgramId",new SearchColumn(){Name="ProgramId",Title="ProgramId",SelectClause="ProgramId",WhereClause="AllRecords.ProgramId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ProgramId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EpisodeId",new SearchColumn(){Name="EpisodeId",Title="EpisodeId",SelectClause="EpisodeId",WhereClause="AllRecords.EpisodeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="EpisodeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetSuggestedFlowSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetSuggestedFlowBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetSuggestedFlowAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetSuggestedFlowSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[SuggestedFlow].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[SuggestedFlow].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual SuggestedFlow GetSuggestedFlow(System.Int32 SuggestedFlowId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSuggestedFlowSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SuggestedFlow] with (nolock)  where SuggestedFlowId=@SuggestedFlowId ";
			SqlParameter parameter=new SqlParameter("@SuggestedFlowId",SuggestedFlowId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return SuggestedFlowFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<SuggestedFlow> GetSuggestedFlowByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSuggestedFlowSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [SuggestedFlow] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SuggestedFlow>(ds,SuggestedFlowFromDataRow);
		}

		public virtual List<SuggestedFlow> GetAllSuggestedFlow(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetSuggestedFlowSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [SuggestedFlow] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SuggestedFlow>(ds, SuggestedFlowFromDataRow);
		}

		public virtual List<SuggestedFlow> GetPagedSuggestedFlow(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetSuggestedFlowCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [SuggestedFlowId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([SuggestedFlowId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [SuggestedFlowId] ";
            tempsql += " FROM [SuggestedFlow] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("SuggestedFlowId"))
					tempsql += " , (AllRecords.[SuggestedFlowId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[SuggestedFlowId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetSuggestedFlowSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [SuggestedFlow] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [SuggestedFlow].[SuggestedFlowId] = PageIndex.[SuggestedFlowId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<SuggestedFlow>(ds, SuggestedFlowFromDataRow);
			}else{ return null;}
		}

		private int GetSuggestedFlowCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM SuggestedFlow as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM SuggestedFlow as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(SuggestedFlow))]
		public virtual SuggestedFlow InsertSuggestedFlow(SuggestedFlow entity)
		{

			SuggestedFlow other=new SuggestedFlow();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into SuggestedFlow ( [Key]
				,[ProgramId]
				,[EpisodeId]
				,[CreationDate]
				,[LastUpdatedDate]
				,[IsActive] )
				Values
				( @Key
				, @ProgramId
				, @EpisodeId
				, @CreationDate
				, @LastUpdatedDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetSuggestedFlow(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(SuggestedFlow))]
		public virtual SuggestedFlow UpdateSuggestedFlow(SuggestedFlow entity)
		{

			if (entity.IsTransient()) return entity;
			SuggestedFlow other = GetSuggestedFlow(entity.SuggestedFlowId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update SuggestedFlow set  [Key]=@Key
							, [ProgramId]=@ProgramId
							, [EpisodeId]=@EpisodeId
							, [CreationDate]=@CreationDate
							, [LastUpdatedDate]=@LastUpdatedDate
							, [IsActive]=@IsActive 
							 where SuggestedFlowId=@SuggestedFlowId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Key",entity.Key ?? (object)DBNull.Value)
					, new SqlParameter("@ProgramId",entity.ProgramId ?? (object)DBNull.Value)
					, new SqlParameter("@EpisodeId",entity.EpisodeId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@SuggestedFlowId",entity.SuggestedFlowId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetSuggestedFlow(entity.SuggestedFlowId);
		}

		public virtual bool DeleteSuggestedFlow(System.Int32 SuggestedFlowId)
		{

			string sql="delete from SuggestedFlow where SuggestedFlowId=@SuggestedFlowId";
			SqlParameter parameter=new SqlParameter("@SuggestedFlowId",SuggestedFlowId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(SuggestedFlow))]
		public virtual SuggestedFlow DeleteSuggestedFlow(SuggestedFlow entity)
		{
			this.DeleteSuggestedFlow(entity.SuggestedFlowId);
			return entity;
		}


		public virtual SuggestedFlow SuggestedFlowFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			SuggestedFlow entity=new SuggestedFlow();
			if (dr.Table.Columns.Contains("SuggestedFlowId"))
			{
			entity.SuggestedFlowId = (System.Int32)dr["SuggestedFlowId"];
			}
			if (dr.Table.Columns.Contains("Key"))
			{
			entity.Key = dr["Key"].ToString();
			}
			if (dr.Table.Columns.Contains("ProgramId"))
			{
			entity.ProgramId = dr["ProgramId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ProgramId"];
			}
			if (dr.Table.Columns.Contains("EpisodeId"))
			{
			entity.EpisodeId = dr["EpisodeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["EpisodeId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = dr["LastUpdatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdatedDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
