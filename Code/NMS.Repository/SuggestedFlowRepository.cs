﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
    public partial class SuggestedFlowRepository : SuggestedFlowRepositoryBase, ISuggestedFlowRepository
    {
        public List<SuggestedFlow> GetSuggestedFlowByKey(string Key)
        {
            string sql = GetSuggestedFlowSelectClause();
            sql += " from [SuggestedFlow] with (nolock)  where [Key] = @Key; ";

            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@Key", Key) };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SuggestedFlow>(ds, SuggestedFlowFromDataRow);
        }

        public List<SuggestedFlow> GetSuggestedFlowsLikeKey(string Key, int ProgramId)
        {
            string sql = GetSuggestedFlowSelectClause();
            sql += string.Format(" from [SuggestedFlow] with (nolock)  where [Key] LIKE '{0}%' and ProgramId = @ProgramId ", Key);

            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@ProgramId", ProgramId) };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<SuggestedFlow>(ds, SuggestedFlowFromDataRow);
        }
    }
}
