﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TagRepositoryBase : Repository, ITagRepositoryBase 
	{
        
        public TagRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TagId",new SearchColumn(){Name="TagId",Title="TagId",SelectClause="TagId",WhereClause="AllRecords.TagId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Rank",new SearchColumn(){Name="Rank",Title="Rank",SelectClause="Rank",WhereClause="AllRecords.Rank",DataType="System.Double",IsForeignColumn=false,PropertyName="Rank",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Tag",new SearchColumn(){Name="Tag",Title="Tag",SelectClause="Tag",WhereClause="AllRecords.Tag",DataType="System.String",IsForeignColumn=false,PropertyName="Tag",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Guid",new SearchColumn(){Name="Guid",Title="Guid",SelectClause="Guid",WhereClause="AllRecords.Guid",DataType="System.String",IsForeignColumn=false,PropertyName="Guid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTagSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTagBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTagAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTagSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Tag].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Tag].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Tag GetTag(System.Int32 TagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Tag] with (nolock)  where TagId=@TagId ";
			SqlParameter parameter=new SqlParameter("@TagId",TagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TagFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Tag> GetTagByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Tag] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Tag>(ds,TagFromDataRow);
		}

		public virtual List<Tag> GetAllTag(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Tag] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Tag>(ds, TagFromDataRow);
		}

		public virtual List<Tag> GetPagedTag(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTagCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TagId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TagId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TagId] ";
            tempsql += " FROM [Tag] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TagId"))
					tempsql += " , (AllRecords.[TagId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TagId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTagSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Tag] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Tag].[TagId] = PageIndex.[TagId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Tag>(ds, TagFromDataRow);
			}else{ return null;}
		}

		private int GetTagCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Tag as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Tag as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Tag))]
		public virtual Tag InsertTag(Tag entity)
		{

			Tag other=new Tag();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Tag ( [Rank]
				,[Tag]
				,[Guid]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Rank
				, @Tag
				, @Guid
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Rank",entity.Rank)
					, new SqlParameter("@Tag",entity.Tag)
					, new SqlParameter("@Guid",entity.Guid ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTag(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Tag))]
		public virtual Tag UpdateTag(Tag entity)
		{

			if (entity.IsTransient()) return entity;
			Tag other = GetTag(entity.TagId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Tag set  [Rank]=@Rank
							, [Tag]=@Tag
							, [Guid]=@Guid
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where TagId=@TagId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Rank",entity.Rank)
					, new SqlParameter("@Tag",entity.Tag)
					, new SqlParameter("@Guid",entity.Guid ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@TagId",entity.TagId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTag(entity.TagId);
		}

		public virtual bool DeleteTag(System.Int32 TagId)
		{

			string sql="delete from Tag where TagId=@TagId";
			SqlParameter parameter=new SqlParameter("@TagId",TagId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Tag))]
		public virtual Tag DeleteTag(Tag entity)
		{
			this.DeleteTag(entity.TagId);
			return entity;
		}


        public virtual Tag GetTagByGuid(string Guid, string SelectClause = null)
        {

            string sql = string.IsNullOrEmpty(SelectClause) ? GetTagSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [Tag] with (nolock)  where Guid=@Guid ";
            SqlParameter parameter = new SqlParameter("@Guid", Guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return TagFromDataRow(ds.Tables[0].Rows[0]);
        }


        public virtual Tag TagFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Tag entity=new Tag();
			if (dr.Table.Columns.Contains("TagId"))
			{
			entity.TagId = (System.Int32)dr["TagId"];
			}
			if (dr.Table.Columns.Contains("Rank"))
			{
			entity.Rank = (System.Double)dr["Rank"];
			}
			if (dr.Table.Columns.Contains("Tag"))
			{
			entity.Tag = dr["Tag"].ToString();
			}
			if (dr.Table.Columns.Contains("Guid"))
			{
			entity.Guid = dr["Guid"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
