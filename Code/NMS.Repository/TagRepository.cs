﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class TagRepository : TagRepositoryBase, ITagRepository
    {
         public TagRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

         public TagRepository()
        {            
        }
        public List<Tag> GetTagByTerm(string term)
        {
            string sql = GetTagSelectClause();
            sql += " from Tag with (nolock)  where Tag like '%'+@Name+'%' and ISNULL(IsActive,0) = 1";
            SqlParameter parameter = new SqlParameter("@Name", term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Tag>(ds, TagFromDataRow);
        }
        
        public virtual Tag GetByGuid(string Guid, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetTagSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += " from Tag with (nolock) where Guid = @Guid order by 1 desc";
            SqlParameter parameter = new SqlParameter("@Guid", Guid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TagFromDataRow(ds.Tables[0].Rows[0]);
        }



        public virtual bool InsertTagNoReturn(Tag entity)
        {                        
            string sql = @"Insert into Tag ( [Rank]
				,[Tag]
				,[Guid]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Rank
				, @Tag
				, @Guid
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Rank",entity.Rank)
					, new SqlParameter("@Tag",entity.Tag)
					, new SqlParameter("@Guid",entity.Guid ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value)
            {
                return false;            
                throw new DataException("Identity column was null as a result of the insert operation.");

            }
            return true;            
        }

        public virtual bool AlreadyExist(string tag)
        {
            string sql = @"select count(1) from [tag] were [tag] = @tag;";
            SqlParameter[] parameterArray = new SqlParameter[] { new SqlParameter("@tag", tag) };
            return (int)SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public List<Tag> GetTagByTermExact(string term)
        {
            string sql = "select * from Tag with (nolock)  where Tag like @Name";
            SqlParameter parameter = new SqlParameter("@Name", term);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Tag>(ds, TagFromDataRow);
        }

    }


}
