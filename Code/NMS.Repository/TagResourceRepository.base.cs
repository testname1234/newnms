﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TagResourceRepositoryBase : Repository, ITagResourceRepositoryBase 
	{
        
        public TagResourceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TagResourceid",new SearchColumn(){Name="TagResourceid",Title="TagResourceid",SelectClause="TagResourceid",WhereClause="AllRecords.TagResourceid",DataType="System.Int32",IsForeignColumn=false,PropertyName="TagResourceid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TagId",new SearchColumn(){Name="TagId",Title="TagId",SelectClause="TagId",WhereClause="AllRecords.TagId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TagId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ResourceId",new SearchColumn(){Name="ResourceId",Title="ResourceId",SelectClause="ResourceId",WhereClause="AllRecords.ResourceId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ResourceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("isactive",new SearchColumn(){Name="isactive",Title="isactive",SelectClause="isactive",WhereClause="AllRecords.isactive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="Isactive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTagResourceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTagResourceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTagResourceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTagResourceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TagResource].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TagResource].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<TagResource> GetTagResourceByTagId(System.Int32? TagId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TagResource] with (nolock)  where TagId=@TagId  ";
			SqlParameter parameter=new SqlParameter("@TagId",TagId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TagResource>(ds,TagResourceFromDataRow);
		}

		public virtual List<TagResource> GetTagResourceByResourceId(System.Int32? ResourceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TagResource] with (nolock)  where ResourceId=@ResourceId  ";
			SqlParameter parameter=new SqlParameter("@ResourceId",ResourceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TagResource>(ds,TagResourceFromDataRow);
		}

		public virtual TagResource GetTagResource(System.Int32 TagResourceid,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TagResource] with (nolock)  where TagResourceid=@TagResourceid ";
			SqlParameter parameter=new SqlParameter("@TagResourceid",TagResourceid);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TagResourceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TagResource> GetTagResourceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TagResource] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TagResource>(ds,TagResourceFromDataRow);
		}

		public virtual List<TagResource> GetAllTagResource(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTagResourceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TagResource] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TagResource>(ds, TagResourceFromDataRow);
		}

		public virtual List<TagResource> GetPagedTagResource(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTagResourceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TagResourceid] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TagResourceid])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TagResourceid] ";
            tempsql += " FROM [TagResource] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TagResourceid"))
					tempsql += " , (AllRecords.[TagResourceid])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TagResourceid])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTagResourceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TagResource] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TagResource].[TagResourceid] = PageIndex.[TagResourceid] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TagResource>(ds, TagResourceFromDataRow);
			}else{ return null;}
		}

		private int GetTagResourceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TagResource as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TagResource as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TagResource))]
		public virtual TagResource InsertTagResource(TagResource entity)
		{

			TagResource other=new TagResource();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TagResource ( [TagId]
				,[ResourceId]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive] )
				Values
				( @TagId
				, @ResourceId
				, @CreationDate
				, @LastUpdateDate
				, @isactive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TagId",entity.TagId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceId",entity.ResourceId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTagResource(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TagResource))]
		public virtual TagResource UpdateTagResource(TagResource entity)
		{

			if (entity.IsTransient()) return entity;
			TagResource other = GetTagResource(entity.TagResourceid);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TagResource set  [TagId]=@TagId
							, [ResourceId]=@ResourceId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [isactive]=@isactive 
							 where TagResourceid=@TagResourceid";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TagId",entity.TagId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceId",entity.ResourceId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)
					, new SqlParameter("@TagResourceid",entity.TagResourceid)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTagResource(entity.TagResourceid);
		}

		public virtual bool DeleteTagResource(System.Int32 TagResourceid)
		{

			string sql="delete from TagResource where TagResourceid=@TagResourceid";
			SqlParameter parameter=new SqlParameter("@TagResourceid",TagResourceid);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TagResource))]
		public virtual TagResource DeleteTagResource(TagResource entity)
		{
			this.DeleteTagResource(entity.TagResourceid);
			return entity;
		}


		public virtual TagResource TagResourceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TagResource entity=new TagResource();
			if (dr.Table.Columns.Contains("TagResourceid"))
			{
			entity.TagResourceid = (System.Int32)dr["TagResourceid"];
			}
			if (dr.Table.Columns.Contains("TagId"))
			{
			entity.TagId = dr["TagId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TagId"];
			}
			if (dr.Table.Columns.Contains("ResourceId"))
			{
			entity.ResourceId = dr["ResourceId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ResourceId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("isactive"))
			{
			entity.Isactive = dr["isactive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["isactive"];
			}
			return entity;
		}

	}
	
	
}
