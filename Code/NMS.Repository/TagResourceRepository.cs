﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class TagResourceRepository : TagResourceRepositoryBase, ITagResourceRepository
    {
         public TagResourceRepository(IConnectionString iConnectionString)
        {
            this.ConnectionString = iConnectionString.ConnectionStringVar;
        }

         public TagResourceRepository()
        {            
        }
        public virtual bool InsertTagResourceNoReturn(TagResource entity)
        {
            string sql = @"Insert into TagResource ( [TagId]
				,[ResourceId]
				,[CreationDate]
				,[LastUpdateDate]
				,[isactive] )
				Values
				( @TagId
				, @ResourceId
				, @CreationDate
				, @LastUpdateDate
				, @isactive );
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@TagId",entity.TagId ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceId",entity.ResourceId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@isactive",entity.Isactive ?? (object)DBNull.Value)};
            var identity = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (identity == DBNull.Value)
            {
                return false;
                throw new DataException("Identity column was null as a result of the insert operation.");
            }
            return true;

        }
    }


}
