﻿using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
    public partial class TeamRepository : TeamRepositoryBase, ITeamRepository
    {
        public bool GetTeamByUserWorkProgramId(int userId, int workRoleId, int? programId)
        {
            string sql = GetTeamSelectClause();
            sql += " from [Team] with (nolock)  where UserId=@UserId And WorkRoleId=@WorkRoleId And ProgramId=@ProgramId ";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@UserId",userId)
					, new SqlParameter("@WorkRoleId",workRoleId)
					, new SqlParameter("@ProgramId",programId)};
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return false;
            else return true;
        }

        public List<Team> GetTeamByProgramId(int? programId)
        {
            string sql = GetTeamSelectClause();
            sql += " from [Team] with (nolock)  where ProgramId=@ProgramId";
            SqlParameter[] parameterArray = new SqlParameter[] { new SqlParameter("@ProgramId", programId) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Team>(ds, TeamFromDataRow);
        }

        public bool DeleteFromTeam(int userId, int workRoleId, int? programId)
        {
            string sql = "delete from [Team] where UserId=@UserId And WorkRoleId=@WorkRoleId And ProgramId=@ProgramId ";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@UserId",userId)
					, new SqlParameter("@WorkRoleId",workRoleId)
					, new SqlParameter("@ProgramId",programId)};
            var identity = SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);
            return (Convert.ToInt32(identity)) == 1 ? true : false;
        }

        public List<Team> GetTeamByUserId(int userId)
        {
            string sql = GetTeamSelectClause();
            sql += " from [Team] with (nolock)  where ProgramId in (select ProgramId from [Team] where [UserId] = @UserId);";

            SqlParameter[] parameterArray = new SqlParameter[] { 
                new SqlParameter("@UserId", userId) 
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Team>(ds, TeamFromDataRow);
        }
    }
}