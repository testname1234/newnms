﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TeamRoleRepositoryBase : Repository, ITeamRoleRepositoryBase 
	{
        
        public TeamRoleRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TeamRoleId",new SearchColumn(){Name="TeamRoleId",Title="TeamRoleId",SelectClause="TeamRoleId",WhereClause="AllRecords.TeamRoleId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TeamRoleId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTeamRoleSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTeamRoleBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTeamRoleAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTeamRoleSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TeamRole].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TeamRole].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TeamRole GetTeamRole(System.Int32 TeamRoleId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTeamRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TeamRole] with (nolock)  where TeamRoleId=@TeamRoleId ";
			SqlParameter parameter=new SqlParameter("@TeamRoleId",TeamRoleId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TeamRoleFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TeamRole> GetTeamRoleByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTeamRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TeamRole] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TeamRole>(ds,TeamRoleFromDataRow);
		}

		public virtual List<TeamRole> GetAllTeamRole(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTeamRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TeamRole] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TeamRole>(ds, TeamRoleFromDataRow);
		}

		public virtual List<TeamRole> GetPagedTeamRole(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTeamRoleCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TeamRoleId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TeamRoleId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TeamRoleId] ";
            tempsql += " FROM [TeamRole] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TeamRoleId"))
					tempsql += " , (AllRecords.[TeamRoleId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TeamRoleId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTeamRoleSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TeamRole] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TeamRole].[TeamRoleId] = PageIndex.[TeamRoleId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TeamRole>(ds, TeamRoleFromDataRow);
			}else{ return null;}
		}

		private int GetTeamRoleCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TeamRole as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TeamRole as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TeamRole))]
		public virtual TeamRole InsertTeamRole(TeamRole entity)
		{

			TeamRole other=new TeamRole();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TeamRole ( [Name] )
				Values
				( @Name );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTeamRole(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TeamRole))]
		public virtual TeamRole UpdateTeamRole(TeamRole entity)
		{

			if (entity.IsTransient()) return entity;
			TeamRole other = GetTeamRole(entity.TeamRoleId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TeamRole set  [Name]=@Name 
							 where TeamRoleId=@TeamRoleId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@TeamRoleId",entity.TeamRoleId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTeamRole(entity.TeamRoleId);
		}

		public virtual bool DeleteTeamRole(System.Int32 TeamRoleId)
		{

			string sql="delete from TeamRole where TeamRoleId=@TeamRoleId";
			SqlParameter parameter=new SqlParameter("@TeamRoleId",TeamRoleId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TeamRole))]
		public virtual TeamRole DeleteTeamRole(TeamRole entity)
		{
			this.DeleteTeamRole(entity.TeamRoleId);
			return entity;
		}


		public virtual TeamRole TeamRoleFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TeamRole entity=new TeamRole();
			if (dr.Table.Columns.Contains("TeamRoleId"))
			{
			entity.TeamRoleId = (System.Int32)dr["TeamRoleId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			return entity;
		}

	}
	
	
}
