﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TemplateScreenElementRepositoryBase : Repository, ITemplateScreenElementRepositoryBase 
	{
        
        public TemplateScreenElementRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TemplateScreenElementId",new SearchColumn(){Name="TemplateScreenElementId",Title="TemplateScreenElementId",SelectClause="TemplateScreenElementId",WhereClause="AllRecords.TemplateScreenElementId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TemplateScreenElementId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScreenElementId",new SearchColumn(){Name="ScreenElementId",Title="ScreenElementId",SelectClause="ScreenElementId",WhereClause="AllRecords.ScreenElementId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ScreenElementId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Top",new SearchColumn(){Name="Top",Title="Top",SelectClause="Top",WhereClause="AllRecords.Top",DataType="System.Double",IsForeignColumn=false,PropertyName="Top",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Bottom",new SearchColumn(){Name="Bottom",Title="Bottom",SelectClause="Bottom",WhereClause="AllRecords.Bottom",DataType="System.Double",IsForeignColumn=false,PropertyName="Bottom",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Left",new SearchColumn(){Name="Left",Title="Left",SelectClause="Left",WhereClause="AllRecords.Left",DataType="System.Double",IsForeignColumn=false,PropertyName="Left",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Right",new SearchColumn(){Name="Right",Title="Right",SelectClause="Right",WhereClause="AllRecords.Right",DataType="System.Double",IsForeignColumn=false,PropertyName="Right",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ZIndex",new SearchColumn(){Name="ZIndex",Title="ZIndex",SelectClause="ZIndex",WhereClause="AllRecords.ZIndex",DataType="System.Double",IsForeignColumn=false,PropertyName="Zindex",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ScreenTemplateId",new SearchColumn(){Name="ScreenTemplateId",Title="ScreenTemplateId",SelectClause="ScreenTemplateId",WhereClause="AllRecords.ScreenTemplateId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ScreenTemplateId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("flashtemplatewindowid",new SearchColumn(){Name="flashtemplatewindowid",Title="flashtemplatewindowid",SelectClause="flashtemplatewindowid",WhereClause="AllRecords.flashtemplatewindowid",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Flashtemplatewindowid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTemplateScreenElementSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTemplateScreenElementBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTemplateScreenElementAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTemplateScreenElementSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TemplateScreenElement].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TemplateScreenElement].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<TemplateScreenElement> GetTemplateScreenElementByScreenElementId(System.Int32 ScreenElementId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTemplateScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TemplateScreenElement] with (nolock)  where ScreenElementId=@ScreenElementId  ";
			SqlParameter parameter=new SqlParameter("@ScreenElementId",ScreenElementId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TemplateScreenElement>(ds,TemplateScreenElementFromDataRow);
		}

		public virtual List<TemplateScreenElement> GetTemplateScreenElementByScreenTemplateId(System.Int32? ScreenTemplateId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTemplateScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TemplateScreenElement] with (nolock)  where ScreenTemplateId=@ScreenTemplateId  ";
			SqlParameter parameter=new SqlParameter("@ScreenTemplateId",ScreenTemplateId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TemplateScreenElement>(ds,TemplateScreenElementFromDataRow);
		}

		public virtual TemplateScreenElement GetTemplateScreenElement(System.Int32 TemplateScreenElementId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTemplateScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TemplateScreenElement] with (nolock)  where TemplateScreenElementId=@TemplateScreenElementId ";
			SqlParameter parameter=new SqlParameter("@TemplateScreenElementId",TemplateScreenElementId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TemplateScreenElementFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TemplateScreenElement> GetTemplateScreenElementByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTemplateScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TemplateScreenElement] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TemplateScreenElement>(ds,TemplateScreenElementFromDataRow);
		}

		public virtual List<TemplateScreenElement> GetAllTemplateScreenElement(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTemplateScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TemplateScreenElement] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TemplateScreenElement>(ds, TemplateScreenElementFromDataRow);
		}

		public virtual List<TemplateScreenElement> GetPagedTemplateScreenElement(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTemplateScreenElementCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TemplateScreenElementId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TemplateScreenElementId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TemplateScreenElementId] ";
            tempsql += " FROM [TemplateScreenElement] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TemplateScreenElementId"))
					tempsql += " , (AllRecords.[TemplateScreenElementId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TemplateScreenElementId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTemplateScreenElementSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TemplateScreenElement] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TemplateScreenElement].[TemplateScreenElementId] = PageIndex.[TemplateScreenElementId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TemplateScreenElement>(ds, TemplateScreenElementFromDataRow);
			}else{ return null;}
		}

		private int GetTemplateScreenElementCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TemplateScreenElement as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TemplateScreenElement as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TemplateScreenElement))]
		public virtual TemplateScreenElement InsertTemplateScreenElement(TemplateScreenElement entity)
		{

			TemplateScreenElement other=new TemplateScreenElement();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TemplateScreenElement ( [ScreenElementId]
				,[Top]
				,[Bottom]
				,[Left]
				,[Right]
				,[ZIndex]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[ScreenTemplateId]
				,[flashtemplatewindowid] )
				Values
				( @ScreenElementId
				, @Top
				, @Bottom
				, @Left
				, @Right
				, @ZIndex
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @ScreenTemplateId
				, @flashtemplatewindowid );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenElementId",entity.ScreenElementId)
					, new SqlParameter("@Top",entity.Top)
					, new SqlParameter("@Bottom",entity.Bottom)
					, new SqlParameter("@Left",entity.Left)
					, new SqlParameter("@Right",entity.Right)
					, new SqlParameter("@ZIndex",entity.Zindex)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@flashtemplatewindowid",entity.Flashtemplatewindowid ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTemplateScreenElement(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TemplateScreenElement))]
		public virtual TemplateScreenElement UpdateTemplateScreenElement(TemplateScreenElement entity)
		{

			if (entity.IsTransient()) return entity;
			TemplateScreenElement other = GetTemplateScreenElement(entity.TemplateScreenElementId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TemplateScreenElement set  [ScreenElementId]=@ScreenElementId
							, [Top]=@Top
							, [Bottom]=@Bottom
							, [Left]=@Left
							, [Right]=@Right
							, [ZIndex]=@ZIndex
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [ScreenTemplateId]=@ScreenTemplateId
							, [flashtemplatewindowid]=@flashtemplatewindowid 
							 where TemplateScreenElementId=@TemplateScreenElementId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ScreenElementId",entity.ScreenElementId)
					, new SqlParameter("@Top",entity.Top)
					, new SqlParameter("@Bottom",entity.Bottom)
					, new SqlParameter("@Left",entity.Left)
					, new SqlParameter("@Right",entity.Right)
					, new SqlParameter("@ZIndex",entity.Zindex)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ScreenTemplateId",entity.ScreenTemplateId ?? (object)DBNull.Value)
					, new SqlParameter("@flashtemplatewindowid",entity.Flashtemplatewindowid ?? (object)DBNull.Value)
					, new SqlParameter("@TemplateScreenElementId",entity.TemplateScreenElementId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTemplateScreenElement(entity.TemplateScreenElementId);
		}

		public virtual bool DeleteTemplateScreenElement(System.Int32 TemplateScreenElementId)
		{

			string sql="delete from TemplateScreenElement where TemplateScreenElementId=@TemplateScreenElementId";
			SqlParameter parameter=new SqlParameter("@TemplateScreenElementId",TemplateScreenElementId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TemplateScreenElement))]
		public virtual TemplateScreenElement DeleteTemplateScreenElement(TemplateScreenElement entity)
		{
			this.DeleteTemplateScreenElement(entity.TemplateScreenElementId);
			return entity;
		}


		public virtual TemplateScreenElement TemplateScreenElementFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TemplateScreenElement entity=new TemplateScreenElement();
			if (dr.Table.Columns.Contains("TemplateScreenElementId"))
			{
			entity.TemplateScreenElementId = (System.Int32)dr["TemplateScreenElementId"];
			}
			if (dr.Table.Columns.Contains("ScreenElementId"))
			{
			entity.ScreenElementId = (System.Int32)dr["ScreenElementId"];
			}
			if (dr.Table.Columns.Contains("Top"))
			{
			entity.Top = (System.Double)dr["Top"];
			}
			if (dr.Table.Columns.Contains("Bottom"))
			{
			entity.Bottom = (System.Double)dr["Bottom"];
			}
			if (dr.Table.Columns.Contains("Left"))
			{
			entity.Left = (System.Double)dr["Left"];
			}
			if (dr.Table.Columns.Contains("Right"))
			{
			entity.Right = (System.Double)dr["Right"];
			}
			if (dr.Table.Columns.Contains("ZIndex"))
			{
			entity.Zindex = (System.Double)dr["ZIndex"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("ScreenTemplateId"))
			{
			entity.ScreenTemplateId = dr["ScreenTemplateId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ScreenTemplateId"];
			}
			if (dr.Table.Columns.Contains("flashtemplatewindowid"))
			{
			entity.Flashtemplatewindowid = dr["flashtemplatewindowid"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["flashtemplatewindowid"];
			}
			return entity;
		}

	}
	
	
}
