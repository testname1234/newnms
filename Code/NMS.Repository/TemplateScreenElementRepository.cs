﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class TemplateScreenElementRepository: TemplateScreenElementRepositoryBase, ITemplateScreenElementRepository
	{
        public List<TemplateScreenElement> GetTemplateScreenElementsByProgramId(int programId)
        {
            string sql = "select te.* ";
            sql += " from screentemplate st inner join  templatescreenelement te on st.screentemplateid=te.screentemplateid where st.ProgramId=@ProgramId";
            SqlParameter parameter1 = new SqlParameter("@ProgramId", programId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TemplateScreenElement>(ds, TemplateScreenElementFromDataRow);
        }

        public TemplateScreenElement GetByScreenTemplateIdAndflashtemplatewindowid(int ScreenTemplateId, int flashtemplatewindowid, string SelectClause = null)       
		{
			string sql=string.IsNullOrEmpty(SelectClause)?GetTemplateScreenElementSelectClause():(string.Format("Select {0} ",SelectClause));
            sql += "from [TemplateScreenElement] with (nolock)  where ScreenTemplateId=@ScreenTemplateId and flashtemplatewindowid=@flashtemplatewindowid ";
            SqlParameter parameter1 = new SqlParameter("@ScreenTemplateId", ScreenTemplateId);
            SqlParameter parameter2 = new SqlParameter("@flashtemplatewindowid", flashtemplatewindowid);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter1, parameter2 });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TemplateScreenElementFromDataRow(ds.Tables[0].Rows[0]);
		}


        public void DeleteTemplateScreenElementByScreenTemplateID(int screenTemplateId)
        {
            string sql = "delete from [TemplateScreenElement] where ScreenTemplateId=@ScreenTemplateId ";
            SqlParameter parameter = new SqlParameter("@ScreenTemplateId", screenTemplateId);
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
        }
    }
	
	
}
