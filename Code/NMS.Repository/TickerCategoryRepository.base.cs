﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TickerCategoryRepositoryBase : Repository, ITickerCategoryRepositoryBase 
	{
        
        public TickerCategoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerCategoryId",new SearchColumn(){Name="TickerCategoryId",Title="TickerCategoryId",SelectClause="TickerCategoryId",WhereClause="AllRecords.TickerCategoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerCategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceNumber",new SearchColumn(){Name="SequenceNumber",Title="SequenceNumber",SelectClause="SequenceNumber",WhereClause="AllRecords.SequenceNumber",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SequenceNumber",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentTickerCategoryId",new SearchColumn(){Name="ParentTickerCategoryId",Title="ParentTickerCategoryId",SelectClause="ParentTickerCategoryId",WhereClause="AllRecords.ParentTickerCategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentTickerCategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerSize",new SearchColumn(){Name="TickerSize",Title="TickerSize",SelectClause="TickerSize",WhereClause="AllRecords.TickerSize",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerSize",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TicketCategoryTypeId",new SearchColumn(){Name="TicketCategoryTypeId",Title="TicketCategoryTypeId",SelectClause="TicketCategoryTypeId",WhereClause="AllRecords.TicketCategoryTypeId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TicketCategoryTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EntityIds",new SearchColumn(){Name="EntityIds",Title="EntityIds",SelectClause="EntityIds",WhereClause="AllRecords.EntityIds",DataType="System.String",IsForeignColumn=false,PropertyName="EntityIds",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TagIds",new SearchColumn(){Name="TagIds",Title="TagIds",SelectClause="TagIds",WhereClause="AllRecords.TagIds",DataType="System.String",IsForeignColumn=false,PropertyName="TagIds",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TabName",new SearchColumn(){Name="TabName",Title="TabName",SelectClause="TabName",WhereClause="AllRecords.TabName",DataType="System.String",IsForeignColumn=false,PropertyName="TabName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("HourPolicy",new SearchColumn(){Name="HourPolicy",Title="HourPolicy",SelectClause="HourPolicy",WhereClause="AllRecords.HourPolicy",DataType="System.Int32?",IsForeignColumn=false,PropertyName="HourPolicy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewTickerPolicy",new SearchColumn(){Name="NewTickerPolicy",Title="NewTickerPolicy",SelectClause="NewTickerPolicy",WhereClause="AllRecords.NewTickerPolicy",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewTickerPolicy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MCRTickerCategoryId",new SearchColumn(){Name="MCRTickerCategoryId",Title="MCRTickerCategoryId",SelectClause="MCRTickerCategoryId",WhereClause="AllRecords.MCRTickerCategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="McrTickerCategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsRepeat",new SearchColumn(){Name="IsRepeat",Title="IsRepeat",SelectClause="IsRepeat",WhereClause="AllRecords.IsRepeat",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsRepeat",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Priority",new SearchColumn(){Name="Priority",Title="Priority",SelectClause="Priority",WhereClause="AllRecords.Priority",DataType="System.Int32?",IsForeignColumn=false,PropertyName="Priority",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerCategorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerCategoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerCategoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerCategorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerCategory].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerCategory].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TickerCategory GetTickerCategory(System.Int32 TickerCategoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerCategory] with (nolock)  where TickerCategoryId=@TickerCategoryId ";
			SqlParameter parameter=new SqlParameter("@TickerCategoryId",TickerCategoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerCategoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerCategory> GetTickerCategoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerCategory] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerCategory>(ds,TickerCategoryFromDataRow);
		}

		public virtual List<TickerCategory> GetAllTickerCategory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerCategorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerCategory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerCategory>(ds, TickerCategoryFromDataRow);
		}

		public virtual List<TickerCategory> GetPagedTickerCategory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerCategoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerCategoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerCategoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerCategoryId] ";
            tempsql += " FROM [TickerCategory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerCategoryId"))
					tempsql += " , (AllRecords.[TickerCategoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerCategoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerCategorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerCategory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerCategory].[TickerCategoryId] = PageIndex.[TickerCategoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerCategory>(ds, TickerCategoryFromDataRow);
			}else{ return null;}
		}

		private int GetTickerCategoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerCategory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerCategory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerCategory))]
		public virtual TickerCategory InsertTickerCategory(TickerCategory entity)
		{

			TickerCategory other=new TickerCategory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerCategory ( [CategoryId]
				,[Name]
				,[SequenceNumber]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[ParentTickerCategoryId]
				,[LanguageCode]
				,[TickerSize]
				,[TicketCategoryTypeId]
				,[EntityIds]
				,[TagIds]
				,[LocationId]
				,[TabName]
				,[HourPolicy]
				,[NewTickerPolicy]
				,[MCRTickerCategoryId]
				,[IsRepeat]
				,[Priority] )
				Values
				( @CategoryId
				, @Name
				, @SequenceNumber
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @ParentTickerCategoryId
				, @LanguageCode
				, @TickerSize
				, @TicketCategoryTypeId
				, @EntityIds
				, @TagIds
				, @LocationId
				, @TabName
				, @HourPolicy
				, @NewTickerPolicy
				, @MCRTickerCategoryId
				, @IsRepeat
				, @Priority );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@ParentTickerCategoryId",entity.ParentTickerCategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@TickerSize",entity.TickerSize)
					, new SqlParameter("@TicketCategoryTypeId",entity.TicketCategoryTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@EntityIds",entity.EntityIds ?? (object)DBNull.Value)
					, new SqlParameter("@TagIds",entity.TagIds ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@TabName",entity.TabName ?? (object)DBNull.Value)
					, new SqlParameter("@HourPolicy",entity.HourPolicy ?? (object)DBNull.Value)
					, new SqlParameter("@NewTickerPolicy",entity.NewTickerPolicy ?? (object)DBNull.Value)
					, new SqlParameter("@MCRTickerCategoryId",entity.McrTickerCategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRepeat",entity.IsRepeat ?? (object)DBNull.Value)
					, new SqlParameter("@Priority",entity.Priority ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerCategory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerCategory))]
		public virtual TickerCategory UpdateTickerCategory(TickerCategory entity)
		{

			if (entity.IsTransient()) return entity;
			TickerCategory other = GetTickerCategory(entity.TickerCategoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerCategory set  [CategoryId]=@CategoryId
							, [Name]=@Name
							, [SequenceNumber]=@SequenceNumber
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [ParentTickerCategoryId]=@ParentTickerCategoryId
							, [LanguageCode]=@LanguageCode
							, [TickerSize]=@TickerSize
							, [TicketCategoryTypeId]=@TicketCategoryTypeId
							, [EntityIds]=@EntityIds
							, [TagIds]=@TagIds
							, [LocationId]=@LocationId
							, [TabName]=@TabName
							, [HourPolicy]=@HourPolicy
							, [NewTickerPolicy]=@NewTickerPolicy
							, [MCRTickerCategoryId]=@MCRTickerCategoryId
							, [IsRepeat]=@IsRepeat
							, [Priority]=@Priority 
							 where TickerCategoryId=@TickerCategoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceNumber",entity.SequenceNumber ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@ParentTickerCategoryId",entity.ParentTickerCategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@TickerSize",entity.TickerSize)
					, new SqlParameter("@TicketCategoryTypeId",entity.TicketCategoryTypeId ?? (object)DBNull.Value)
					, new SqlParameter("@EntityIds",entity.EntityIds ?? (object)DBNull.Value)
					, new SqlParameter("@TagIds",entity.TagIds ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@TabName",entity.TabName ?? (object)DBNull.Value)
					, new SqlParameter("@HourPolicy",entity.HourPolicy ?? (object)DBNull.Value)
					, new SqlParameter("@NewTickerPolicy",entity.NewTickerPolicy ?? (object)DBNull.Value)
					, new SqlParameter("@MCRTickerCategoryId",entity.McrTickerCategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@IsRepeat",entity.IsRepeat ?? (object)DBNull.Value)
					, new SqlParameter("@Priority",entity.Priority ?? (object)DBNull.Value)
					, new SqlParameter("@TickerCategoryId",entity.TickerCategoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerCategory(entity.TickerCategoryId);
		}

		public virtual bool DeleteTickerCategory(System.Int32 TickerCategoryId)
		{

			string sql="delete from TickerCategory where TickerCategoryId=@TickerCategoryId";
			SqlParameter parameter=new SqlParameter("@TickerCategoryId",TickerCategoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerCategory))]
		public virtual TickerCategory DeleteTickerCategory(TickerCategory entity)
		{
			this.DeleteTickerCategory(entity.TickerCategoryId);
			return entity;
		}


		public virtual TickerCategory TickerCategoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerCategory entity=new TickerCategory();
			if (dr.Table.Columns.Contains("TickerCategoryId"))
			{
			entity.TickerCategoryId = (System.Int32)dr["TickerCategoryId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("SequenceNumber"))
			{
			entity.SequenceNumber = dr["SequenceNumber"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SequenceNumber"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("ParentTickerCategoryId"))
			{
			entity.ParentTickerCategoryId = dr["ParentTickerCategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentTickerCategoryId"];
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LanguageCode"];
			}
			if (dr.Table.Columns.Contains("TickerSize"))
			{
			entity.TickerSize = (System.Int32)dr["TickerSize"];
			}
			if (dr.Table.Columns.Contains("TicketCategoryTypeId"))
			{
			entity.TicketCategoryTypeId = dr["TicketCategoryTypeId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TicketCategoryTypeId"];
			}
			if (dr.Table.Columns.Contains("EntityIds"))
			{
			entity.EntityIds = dr["EntityIds"].ToString();
			}
			if (dr.Table.Columns.Contains("TagIds"))
			{
			entity.TagIds = dr["TagIds"].ToString();
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("TabName"))
			{
			entity.TabName = dr["TabName"].ToString();
			}
			if (dr.Table.Columns.Contains("HourPolicy"))
			{
			entity.HourPolicy = dr["HourPolicy"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["HourPolicy"];
			}
			if (dr.Table.Columns.Contains("NewTickerPolicy"))
			{
			entity.NewTickerPolicy = dr["NewTickerPolicy"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewTickerPolicy"];
			}
			if (dr.Table.Columns.Contains("MCRTickerCategoryId"))
			{
			entity.McrTickerCategoryId = dr["MCRTickerCategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["MCRTickerCategoryId"];
			}
			if (dr.Table.Columns.Contains("IsRepeat"))
			{
			entity.IsRepeat = dr["IsRepeat"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsRepeat"];
			}
			if (dr.Table.Columns.Contains("Priority"))
			{
			entity.Priority = dr["Priority"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["Priority"];
			}
			return entity;
		}

	}
	
	
}
