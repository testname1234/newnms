﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace NMS.Repository
{

    public partial class TickerCategoryRepository : TickerCategoryRepositoryBase, ITickerCategoryRepository
    {
        public bool InsertTickerCategoryIfNotExist(int categoryId)
        {
            string sql = @"insert into [TickerCategory] (CategoryId, Name, CreationDate, LastUpdateDate, IsActive)
                           select CategoryId, Category, @CurrentDate, @CurrentDate, 1
                           from 
                           	    Category 
                           where
                                CategoryId = @CategoryId and
                                ParentId is null and
                                not exists (select * from TickerCategory where CategoryId = @CategoryId);
                        
                           update [TickerCategory] set SequenceNumber = (select ISNULL(MAX(SequenceNumber), 0) + 1 from [TickerCategory]), IsActive = 1 where CategoryId = @CategoryId;";

            SqlParameter[] parameterArray = new SqlParameter[]{
                new SqlParameter("@CategoryId", categoryId),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public List<TickerCategory> GetTickerCategory(DateTime lastUpdateDate)
        {
            string sql = GetTickerCategorySelectClause();
            sql += " from [TickerCategory] with (nolock)  where LastUpdateDate > @Date";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Date", lastUpdateDate)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerCategory>(ds, TickerCategoryFromDataRow);
        }
        public bool UpdateTickerCategorySequence(TickerCategory category)
        {
            string sql = @"update [TickerCategory] set SequenceNumber = @SequenceNumber where CategoryId = @CategoryId;";

            SqlParameter[] parameterArray = new SqlParameter[]{
                new SqlParameter("@CategoryId", category.CategoryId),
                new SqlParameter("@SequenceNumber", category.SequenceNumber)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public List<TickerCategory> GetTickerCategoryWithSubCategories(int tickerCategoryId)
        {
            string sql = GetTickerCategorySelectClause();
            sql += " from [TickerCategory] with (nolock)  where (TickerCategoryId = @TickerCategoryId or ParentTickerCategoryId = @TickerCategoryId) and IsActive = 1";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerCategoryId", tickerCategoryId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerCategory>(ds, TickerCategoryFromDataRow);
        }
        public List<TickerCategory> GetTickerCategoryWithSubCategories(int[] tickerCategoryIds)
        {
            var categories = string.Join(",", tickerCategoryIds);
            string sql = GetTickerCategorySelectClause();
            sql += $@" from [TickerCategory] with (nolock)  
                    where (TickerCategoryId in ({categories}) or ParentTickerCategoryId in ({categories})) and IsActive = 1";

            SqlParameter[] parameters = new SqlParameter[] {
              //  new SqlParameter("@lastUpdateDate", lastUpdateDate == DateTime.MinValue ? SqlDateTime.MinValue : lastUpdateDate)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerCategory>(ds, TickerCategoryFromDataRow);
        }

        public List<TickerCategory> GetAllCategoriesHavingTickers()
        {
            var sql = string.Empty;
            sql = @"SELECT *
                        FROM TickerCategory
                        WHERE TickerCategory.IsActive = 1 AND (
                            SELECT COUNT(*)
                            FROM Ticker
                            WHERE Ticker.CategoryId = TickerCategory.TickerCategoryId AND Ticker.IsActive = 1 and StatusId = 3
                        ) > 0";

            SqlParameter[] parameters = new SqlParameter[] {
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerCategory>(ds, TickerCategoryFromDataRow);
        }

        public List<TickerCategory> GetAllTickerCategories()
        {
            var sql = string.Empty;
            sql = @"select * from tickerCategory where IsActive =1 and TicketCategoryTypeId is not null";

            SqlParameter[] parameters = new SqlParameter[] {
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerCategory>(ds, TickerCategoryFromDataRow);
        }

        public bool InsertTickerCategory(Ticker input)
        {
            string sql = @"insert into [TickerCategory] (CategoryId, Name, CreationDate, LastUpdateDate, IsActive)
                           select CategoryId, Category, @CurrentDate, @CurrentDate, 1
                           from 
                           	    Category 
                           where
                                CategoryId = @CategoryId and
                                ParentId is null and
                                not exists (select * from TickerCategory where CategoryId = @CategoryId);
                        
                           update [TickerCategory] set SequenceNumber = (select ISNULL(MAX(SequenceNumber), 0) + 1 from [TickerCategory]), IsActive = 1 where CategoryId = @CategoryId;";

            SqlParameter[] parameterArray = new SqlParameter[]{
                new SqlParameter("@CategoryId", input.CategoryId),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public int GetMaxSequenceId()
        {
            var sql = @"select isnull(max(isnull(SequenceNumber, 0)), 0)+1 from TickerCategory";
            SqlParameter[] parameters = new SqlParameter[] {
            };
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameters));
        }

    }


}
