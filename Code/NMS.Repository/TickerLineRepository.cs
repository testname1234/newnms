﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Enums;

namespace NMS.Repository
{

    public partial class TickerLineRepository : TickerLineRepositoryBase, ITickerLineRepository
    {
        public TickerLine UpdateTickerLineStatus(int tickerLineId, TickerStatuses status, int? tickerTypeId)
        {
            string sql = string.Empty;

            if (tickerTypeId.HasValue)
            {
                if (tickerTypeId.Value == (int)TickerTypes.Breaking)
                {
                    sql = @"Update [TickerLine] set 
                                [BreakingStatusId] = @TickerStatusId,
                                [LastUpdatedDate] = @Date
                            where [TickerLineId] = @TickerLineId; ";
                }
                else if (tickerTypeId.Value == (int)TickerTypes.Category)
                {
                    sql = @"Update [TickerLine] set 
                                [CategoryStatusId] = @TickerStatusId,
                                [LastUpdatedDate] = @Date
                            where [TickerLineId] = @TickerLineId; ";
                }
                else if (tickerTypeId.Value == (int)TickerTypes.Latest)
                {
                    sql = @"Update [TickerLine] set 
                                [LatestStatusId] = @TickerStatusId,
                                [LastUpdatedDate] = @Date
                            where [TickerLineId] = @TickerLineId; ";
                }
            }
            else
            {
                sql = "Update [TickerLine] set ";
                sql += "[LastUpdatedDate] = @Date where [TickerLineId] = @TickerLineId; ";
            }

            sql += @" Update [TickerLine] set
                        [TickerStatusId] = @TickerStatusId,
                        [LastUpdatedDate] = @Date
                     where [TickerLineId] = @TickerLineId; select * from TickerLine Where TickerLineId =@TickerLineId";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@TickerStatusId", (int)status),
                new SqlParameter("@TickerLineId", tickerLineId),
                new SqlParameter("@Date", DateTime.UtcNow)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TickerLineFromDataRow(ds.Tables[0].Rows[0]);
        }

        public bool DeleteMcrTickerLineData(int tickerLineId, int tickerTypeId)
        {
            string sql = string.Empty;

            if (tickerTypeId == (int)TickerTypes.Breaking)
            {
                sql += @"delete from MCRBreakingTickerRundown where TickerLineId = @TickerLineId";
            }
            else if (tickerTypeId == (int)TickerTypes.Latest)
            {
                sql += @"delete from MCRLatestTickerRundown where TickerLineId = @TickerLineId";
            }
            else if (tickerTypeId == (int)TickerTypes.Category)
            {
                sql += @"delete from MCRCategoryTickerRundown where TickerLineId = @TickerLineId";
            }

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerLineId",tickerLineId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool UpdateTickerLineStatus(int tickerLineId, TickerStatuses statusId)
        {
            string sql = @"Update [TickerLine] set
                            [TickerStatusId] = @TickerStatusId
                        where [TickerLineId] = @TickerLineId;";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@TickerStatusId", (int)statusId),
                new SqlParameter("@TickerLineId", tickerLineId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public bool UpdateTickerLine(int tickerLineId, string text)
        {
            string sql = @"Update [TickerLine] set
                            [Text] = @Text,
                            [LastUpdatedDate] = @Date
                        where [TickerLineId] = @TickerLineId;

                        Update [Ticker] set
                            [LastUpdatedDate] = @Date
                        where [TickerId] = (select top 1 [TickerId] from [TickerLine] where [TickerLineId] = @TickerLineId);";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@Text", text),
                new SqlParameter("@Date", DateTime.UtcNow),
                new SqlParameter("@TickerLineId", tickerLineId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }
        public bool SubmitTickersToMcrCategory(int tickerLineId, int categoryId, int count,int tickerId)
        {
            //+ tic.SequenceId
            string sql = @"insert into [MCRCategoryTickerRundown]  (TickerId, TickerLineId, [Text], CategoryId, LanguageCode, CreationDate, SequenceNumber, CategoryName)
                            select 
                                 ticline.TickerId,
                                 ticline.TickerLineId,
                                 ticline.[Text],
                                 tic.CategoryId,
                                 ticline.LanguageCode,
                                 @CurrentDate, 
                                 @SeqId,
                                 (select Name from [TickerCategory] where CategoryId = tic.CategoryId) as CategoryName
                                 from TickerLine ticline
                                 inner join Ticker tic on tic.TickerId =  ticline.TickerId
                                 where ticline.TickerLineId =  @TickerLineId
                                 order by ticline.[SequenceId] asc
                             update TickerLine set CategoryStatusId = @OnAirStatus, LastUpdatedDate = @CurrentDate where TickerLineId = @TickerLineId; update Ticker set LastUpdatedDate = @CurrentDate where TickerId = @TickerId";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@TickerLineId", tickerLineId),
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@CategoryId", categoryId),
                new SqlParameter("@SeqId", count),
                new SqlParameter("@CurrentDate", DateTime.UtcNow),
                new SqlParameter("@OnAirStatus", (int)TickerStatuses.OnAir),
                new SqlParameter("@TickerId",tickerId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public bool SubmitTickersToMcrBreaking(int tickerLineId, int count,int tickerId)
        {
            string sql = @"insert into [MCRBreakingTickerRundown] 
                                (TickerId, TickerLineId, [Text], LanguageCode, CreationDate, SequenceNumber)
                           select 
                                b.TickerId, b.TickerLineId, b.[Text], b.LanguageCode, @CurrentDate, @SeqId
                           from [TickerLine] b
                           where b.[TickerLineId] = @TickerLineId
                            
                           update TickerLine set BreakingStatusId = @OnAirStatus, LastUpdatedDate = @CurrentDate where TickerLineId = @TickerLineId;   update Ticker set LastUpdatedDate = @CurrentDate where TickerId = @TickerId";

            SqlParameter[] parameterArray = new SqlParameter[] {	
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@TickerLineId", tickerLineId),
                new SqlParameter("@SeqId", count),
                new SqlParameter("@CurrentDate", DateTime.UtcNow),
                new SqlParameter("@OnAirStatus", (int)TickerStatuses.OnAir)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public bool SubmitTickersToMcrLatest(int tickerLineId, int count, int tickerId)
        {
            string sql = @"insert into [MCRLatestTickerRundown] 
                                (TickerId, TickerLineId, [Text], LanguageCode, CreationDate, SequenceNumber)
                           select 
                                b.TickerId, b.TickerLineId, b.[Text], b.LanguageCode, @CurrentDate, @SeqId
                           from [TickerLine] b
                           where 
                                b.[TickerLineId] = @TickerLineId
                            
                           update TickerLine set LatestStatusId = @OnAirStatus, LastUpdatedDate = @CurrentDate where TickerLineId = @TickerLineId; update Ticker set LastUpdatedDate = @CurrentDate where TickerId = @TickerId";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@TickerLineId", tickerLineId),
                new SqlParameter("@SeqId", count),
                new SqlParameter("@CurrentDate", DateTime.UtcNow),
                new SqlParameter("@OnAirStatus", (int)TickerStatuses.OnAir)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public List<TickerLine> GetTickerLineByStatusAndLastUpdateDate(TickerStatuses tickerStatus, DateTime? lastUpdateDate)
        {
             //ticLine.[LastUpdatedDate] > ISNULL(@LastUpdateDate, ticLine.[LastUpdatedDate]) and
            string sql = string.Format(@"
		                                select ticLine.*  
		                                from 
		                                [TickerLine] ticLine  
		                                inner join
		                                Ticker tic on tic.TickerId = ticLine.TickerId
		                                left join	 
		                                [TickerCategory] ticcategory on tic.CategoryId = ticcategory.CategoryId
		                                where 
		                                (ticLine.[BreakingStatusId] = @TickerStatusId OR 
		                                ticLine.[CategoryStatusId] = @TickerStatusId OR 
		                                ticLine.[LatestStatusId] = @TickerStatusId) and
		                               
		                                ticLine.[IsActive] = 1
    	                                order by ticLine.[TickerLineId] desc");

            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@TickerStatusId", (int)tickerStatus)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerLine>(ds, TickerLineFromDataRow);
        }

        public Dictionary<string, int> GetSequenceNumbers()
        {
            Dictionary<string, int> sequenceNumber = new Dictionary<string, int>();
            string sql = string.Format(@"select max(BreakingSequenceId) as bSeq,max(LatestSequenceId) as lSeq,max(CategorySequenceId) as cSeq from tickerline (nolock)");

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                sequenceNumber["bSeq"] = ds.Tables[0].Rows[0][0] == DBNull.Value ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                sequenceNumber["lSeq"] = ds.Tables[0].Rows[0][1] == DBNull.Value ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0][1]);
                sequenceNumber["cSeq"] = ds.Tables[0].Rows[0][2] == DBNull.Value ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0][2]);
            }
            return sequenceNumber;
        }

        public TickerLine UpdateTickerLineSequence(TickerLine TickerLine, int Type)
        {

            string sql = string.Empty;
            SqlParameter[] parameterArray = new SqlParameter[2];

            if (Type == (int)TickerTypes.Breaking)
            {
                sql += @"update [TickerLine] set BreakingSequenceId = @SequenceNumber where TickerLineId = @TickerLineId; ";
                parameterArray = new SqlParameter[]{
                new SqlParameter("@TickerLineId",TickerLine.TickerLineId),
                new SqlParameter("@SequenceNumber", TickerLine.BreakingSequenceId)
               };
            }
            else if (Type == (int)TickerTypes.Latest)
            {
                sql += @"update [TickerLine] set LatestSequenceId = @SequenceNumber where TickerLineId = @TickerLineId; ";
                parameterArray = new SqlParameter[]{
                new SqlParameter("@TickerLineId",TickerLine.TickerLineId),
                new SqlParameter("@SequenceNumber", TickerLine.LatestSequenceId)
               };
            }
            else if (Type == (int)TickerTypes.Category)
            {
                sql += @"update [TickerLine] set CategorySequenceId = @SequenceNumber where TickerLineId = @TickerLineId; ";
                parameterArray = new SqlParameter[]{
                new SqlParameter("@TickerLineId",TickerLine.TickerLineId),
                new SqlParameter("@SequenceNumber", TickerLine.CategorySequenceId)
               };
            }

            sql += "select * from [TickerLine] where TickerLineId  = @TickerLineId";

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TickerLineFromDataRow(ds.Tables[0].Rows[0]);
        }

        public TickerLine UpdateTickerLineNameSeverityFrequency(TickerLine tickerLine)
        {
            string sql = @"Update [TickerLine] set
                            [Text] = @Text,
                            [Severity] = @Severity,
                            [Frequency] = @Frequency,
                            [LastUpdatedDate] = @Date
                        where [TickerLineId] = @TickerLineId;

                        select * from TickerLine Where TickerLineId =@TickerLineId";

            SqlParameter[] parameterArray = new SqlParameter[] {	 
                new SqlParameter("@Text",tickerLine.Text),
                new SqlParameter("@Severity",tickerLine.Severity),
                new SqlParameter("@Frequency",tickerLine.Frequency),
                new SqlParameter("@Date", DateTime.UtcNow),
                new SqlParameter("@TickerLineId", tickerLine.TickerLineId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TickerLineFromDataRow(ds.Tables[0].Rows[0]);
        }



        public bool FlushMcrCategoryTickerData()
        {
            string sql = @"update TickerLine set CategoryStatusId = @OnAired, LastUpdatedDate = @CurrentDate
                           where TickerLineId in (select distinct TickerLineId from MCRCategoryTickerRundown);
                           delete from MCRCategoryTickerRundown";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@OnAired", TickerStatuses.OnAired),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool FlushMcrBreakingTickerData()
        {
            string sql = @"update TickerLine set BreakingStatusId = @OnAired, LastUpdatedDate = @CurrentDate
                           where TickerLineId in (select distinct TickerLineId from MCRBreakingTickerRundown);
                           delete from MCRBreakingTickerRundown";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@OnAired", TickerStatuses.OnAired),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool FlushMcrLatestTickerData()
        {
            string sql = @"update TickerLine set LatestStatusId = @OnAired, LastUpdatedDate = @CurrentDate
                           where TickerLineId in (select distinct TickerLineId from MCRLatestTickerRundown);
                           delete from MCRLatestTickerRundown";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@OnAired", TickerStatuses.OnAired),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }


    }


}
