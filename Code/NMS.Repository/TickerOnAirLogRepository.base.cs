﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TickerOnAirLogRepositoryBase : Repository, ITickerOnAirLogRepositoryBase 
	{
        
        public TickerOnAirLogRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerOnAirLogId",new SearchColumn(){Name="TickerOnAirLogId",Title="TickerOnAirLogId",SelectClause="TickerOnAirLogId",WhereClause="AllRecords.TickerOnAirLogId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerOnAirLogId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.String",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerOnAirLogSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerOnAirLogBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerOnAirLogAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerOnAirLogSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerOnAirLog].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerOnAirLog].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TickerOnAirLog GetTickerOnAirLog(System.Int32 TickerOnAirLogId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerOnAirLogSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerOnAirLog] with (nolock)  where TickerOnAirLogId=@TickerOnAirLogId ";
			SqlParameter parameter=new SqlParameter("@TickerOnAirLogId",TickerOnAirLogId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerOnAirLogFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerOnAirLog> GetTickerOnAirLogByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerOnAirLogSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerOnAirLog] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerOnAirLog>(ds,TickerOnAirLogFromDataRow);
		}

		public virtual List<TickerOnAirLog> GetAllTickerOnAirLog(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerOnAirLogSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerOnAirLog] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerOnAirLog>(ds, TickerOnAirLogFromDataRow);
		}

		public virtual List<TickerOnAirLog> GetPagedTickerOnAirLog(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerOnAirLogCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerOnAirLogId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerOnAirLogId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerOnAirLogId] ";
            tempsql += " FROM [TickerOnAirLog] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerOnAirLogId"))
					tempsql += " , (AllRecords.[TickerOnAirLogId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerOnAirLogId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerOnAirLogSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerOnAirLog] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerOnAirLog].[TickerOnAirLogId] = PageIndex.[TickerOnAirLogId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerOnAirLog>(ds, TickerOnAirLogFromDataRow);
			}else{ return null;}
		}

		private int GetTickerOnAirLogCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerOnAirLog as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerOnAirLog as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerOnAirLog))]
		public virtual TickerOnAirLog InsertTickerOnAirLog(TickerOnAirLog entity)
		{

			TickerOnAirLog other=new TickerOnAirLog();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerOnAirLog ( [TickerId]
				,[CreationDate]
				,[UserId] )
				Values
				( @TickerId
				, @CreationDate
				, @UserId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerOnAirLog(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerOnAirLog))]
		public virtual TickerOnAirLog UpdateTickerOnAirLog(TickerOnAirLog entity)
		{

			if (entity.IsTransient()) return entity;
			TickerOnAirLog other = GetTickerOnAirLog(entity.TickerOnAirLogId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerOnAirLog set  [TickerId]=@TickerId
							, [CreationDate]=@CreationDate
							, [UserId]=@UserId 
							 where TickerOnAirLogId=@TickerOnAirLogId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@TickerOnAirLogId",entity.TickerOnAirLogId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerOnAirLog(entity.TickerOnAirLogId);
		}

		public virtual bool DeleteTickerOnAirLog(System.Int32 TickerOnAirLogId)
		{

			string sql="delete from TickerOnAirLog where TickerOnAirLogId=@TickerOnAirLogId";
			SqlParameter parameter=new SqlParameter("@TickerOnAirLogId",TickerOnAirLogId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerOnAirLog))]
		public virtual TickerOnAirLog DeleteTickerOnAirLog(TickerOnAirLog entity)
		{
			this.DeleteTickerOnAirLog(entity.TickerOnAirLogId);
			return entity;
		}


		public virtual TickerOnAirLog TickerOnAirLogFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerOnAirLog entity=new TickerOnAirLog();
			if (dr.Table.Columns.Contains("TickerOnAirLogId"))
			{
			entity.TickerOnAirLogId = (System.Int32)dr["TickerOnAirLogId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = dr["TickerId"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = dr["UserId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserId"];
			}
			return entity;
		}

	}
	
	
}
