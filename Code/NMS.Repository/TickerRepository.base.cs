﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TickerRepositoryBase : Repository, ITickerRepositoryBase 
	{
        
        public TickerRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsGuid",new SearchColumn(){Name="NewsGuid",Title="NewsGuid",SelectClause="NewsGuid",WhereClause="AllRecords.NewsGuid",DataType="System.String",IsForeignColumn=false,PropertyName="NewsGuid",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OnAiredTime",new SearchColumn(){Name="OnAiredTime",Title="OnAiredTime",SelectClause="OnAiredTime",WhereClause="AllRecords.OnAiredTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="OnAiredTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceId",new SearchColumn(){Name="SequenceId",Title="SequenceId",SelectClause="SequenceId",WhereClause="AllRecords.SequenceId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SequenceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedBy",new SearchColumn(){Name="CreatedBy",Title="CreatedBy",SelectClause="CreatedBy",WhereClause="AllRecords.CreatedBy",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CreatedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerGroupName",new SearchColumn(){Name="TickerGroupName",Title="TickerGroupName",SelectClause="TickerGroupName",WhereClause="AllRecords.TickerGroupName",DataType="System.String",IsForeignColumn=false,PropertyName="TickerGroupName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("OnAirRefId",new SearchColumn(){Name="OnAirRefId",Title="OnAirRefId",SelectClause="OnAirRefId",WhereClause="AllRecords.OnAirRefId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="OnAirRefId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("NewsFileId",new SearchColumn(){Name="NewsFileId",Title="NewsFileId",SelectClause="NewsFileId",WhereClause="AllRecords.NewsFileId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="NewsFileId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsApproved",new SearchColumn(){Name="IsApproved",Title="IsApproved",SelectClause="IsApproved",WhereClause="AllRecords.IsApproved",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsApproved",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ViewCount",new SearchColumn(){Name="ViewCount",Title="ViewCount",SelectClause="ViewCount",WhereClause="AllRecords.ViewCount",DataType="System.Int32",IsForeignColumn=false,PropertyName="ViewCount",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MCRTickerId",new SearchColumn(){Name="MCRTickerId",Title="MCRTickerId",SelectClause="MCRTickerId",WhereClause="AllRecords.MCRTickerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="McrTickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsObsolete",new SearchColumn(){Name="IsObsolete",Title="IsObsolete",SelectClause="IsObsolete",WhereClause="AllRecords.IsObsolete",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsObsolete",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Ticker].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Ticker].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Ticker GetTicker(System.Int32 TickerId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Ticker] with (nolock)  where TickerId=@TickerId ";
			SqlParameter parameter=new SqlParameter("@TickerId",TickerId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Ticker> GetTickerByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Ticker] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Ticker>(ds,TickerFromDataRow);
		}

		public virtual List<Ticker> GetAllTicker(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Ticker] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
		}

		public virtual List<Ticker> GetPagedTicker(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerId] ";
            tempsql += " FROM [Ticker] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerId"))
					tempsql += " , (AllRecords.[TickerId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Ticker] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Ticker].[TickerId] = PageIndex.[TickerId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
			}else{ return null;}
		}

		private int GetTickerCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Ticker as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Ticker as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Ticker))]
		public virtual Ticker InsertTicker(Ticker entity)
		{

			Ticker other=new Ticker();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Ticker ( [CategoryId]
				,[NewsGuid]
				,[UserId]
				,[OnAiredTime]
				,[SequenceId]
				,[CreationDate]
				,[LastUpdatedDate]
				,[IsActive]
				,[CreatedBy]
				,[TickerGroupName]
				,[LocationId]
				,[OnAirRefId]
				,[NewsFileId]
				,[IsApproved]
				,[ViewCount]
				,[Text]
				,[StatusId]
				,[MCRTickerId]
				,[IsObsolete] )
				Values
				( @CategoryId
				, @NewsGuid
				, @UserId
				, @OnAiredTime
				, @SequenceId
				, @CreationDate
				, @LastUpdatedDate
				, @IsActive
				, @CreatedBy
				, @TickerGroupName
				, @LocationId
				, @OnAirRefId
				, @NewsFileId
				, @IsApproved
				, @ViewCount
				, @Text
				, @StatusId
				, @MCRTickerId
				, @IsObsolete );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@OnAiredTime",entity.OnAiredTime ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceId",entity.SequenceId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy ?? (object)DBNull.Value)
					, new SqlParameter("@TickerGroupName",entity.TickerGroupName ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirRefId",entity.OnAirRefId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@IsApproved",entity.IsApproved)
					, new SqlParameter("@ViewCount",entity.ViewCount)
					, new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@MCRTickerId",entity.McrTickerId ?? (object)DBNull.Value)
					, new SqlParameter("@IsObsolete",entity.IsObsolete ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTicker(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Ticker))]
		public virtual Ticker UpdateTicker(Ticker entity)
		{

			if (entity.IsTransient()) return entity;
			Ticker other = GetTicker(entity.TickerId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Ticker set  [CategoryId]=@CategoryId
							, [NewsGuid]=@NewsGuid
							, [UserId]=@UserId
							, [OnAiredTime]=@OnAiredTime
							, [SequenceId]=@SequenceId
							, [CreationDate]=@CreationDate
							, [LastUpdatedDate]=@LastUpdatedDate
							, [IsActive]=@IsActive
							, [TickerGroupName]=@TickerGroupName
							, [LocationId]=@LocationId
							, [OnAirRefId]=@OnAirRefId
							, [NewsFileId]=@NewsFileId
							, [IsApproved]=@IsApproved
							, [ViewCount]=@ViewCount
							, [Text]=@Text
							, [StatusId]=@StatusId
							, [MCRTickerId]=@MCRTickerId
							, [IsObsolete]=@IsObsolete 
							 where TickerId=@TickerId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsGuid",entity.NewsGuid ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@OnAiredTime",entity.OnAiredTime ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceId",entity.SequenceId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy ?? (object)DBNull.Value)
					, new SqlParameter("@TickerGroupName",entity.TickerGroupName ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@OnAirRefId",entity.OnAirRefId ?? (object)DBNull.Value)
					, new SqlParameter("@NewsFileId",entity.NewsFileId ?? (object)DBNull.Value)
					, new SqlParameter("@IsApproved",entity.IsApproved)
					, new SqlParameter("@ViewCount",entity.ViewCount)
					, new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@MCRTickerId",entity.McrTickerId ?? (object)DBNull.Value)
					, new SqlParameter("@IsObsolete",entity.IsObsolete ?? (object)DBNull.Value)
					, new SqlParameter("@TickerId",entity.TickerId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTicker(entity.TickerId);
		}

		public virtual bool DeleteTicker(System.Int32 TickerId)
		{

			string sql="delete from Ticker where TickerId=@TickerId";
			SqlParameter parameter=new SqlParameter("@TickerId",TickerId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Ticker))]
		public virtual Ticker DeleteTicker(Ticker entity)
		{
			this.DeleteTicker(entity.TickerId);
			return entity;
		}


		public virtual Ticker TickerFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Ticker entity=new Ticker();
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = (System.Int32)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("NewsGuid"))
			{
			entity.NewsGuid = dr["NewsGuid"].ToString();
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = dr["UserId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("OnAiredTime"))
			{
			entity.OnAiredTime = dr["OnAiredTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["OnAiredTime"];
			}
			if (dr.Table.Columns.Contains("SequenceId"))
			{
			entity.SequenceId = dr["SequenceId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SequenceId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = dr["LastUpdatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdatedDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreatedBy"))
			{
			entity.CreatedBy = dr["CreatedBy"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CreatedBy"];
			}
			if (dr.Table.Columns.Contains("TickerGroupName"))
			{
			entity.TickerGroupName = dr["TickerGroupName"].ToString();
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			if (dr.Table.Columns.Contains("OnAirRefId"))
			{
			entity.OnAirRefId = dr["OnAirRefId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["OnAirRefId"];
			}
			if (dr.Table.Columns.Contains("NewsFileId"))
			{
			entity.NewsFileId = dr["NewsFileId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["NewsFileId"];
			}
			if (dr.Table.Columns.Contains("IsApproved"))
			{
			entity.IsApproved = (System.Boolean)dr["IsApproved"];
			}
			if (dr.Table.Columns.Contains("ViewCount"))
			{
			entity.ViewCount = (System.Int32)dr["ViewCount"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("MCRTickerId"))
			{
			entity.McrTickerId = dr["MCRTickerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["MCRTickerId"];
			}
			if (dr.Table.Columns.Contains("IsObsolete"))
			{
			entity.IsObsolete = dr["IsObsolete"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsObsolete"];
			}
			return entity;
		}

	}
	
	
}
