﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Enums;
using System.Linq;
using System.Data.SqlTypes;

namespace NMS.Repository
{

    public partial class TickerRepository : TickerRepositoryBase, ITickerRepository
    {
        public List<Ticker> GetTickers(string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null)
        {
            string sql = string.Format(@"select distinct a.* 
                                        from 
                                        	[Ticker] a inner join [TickerLine] b on a.[TickerId] = b.[TickerId]
                                        where 
                                            a.[UserId] = ISNULL(@UserId, a.[UserId]) and
	                                        b.[Text] like '%{0}%' and (convert(date, a.[LastUpdatedDate]) between convert(date, @FromDate) and convert(date, @ToDate)) 
                                        order by a.[TickerId] desc OFFSET {1} ROWS FETCH NEXT {2} ROWS ONLY", searchTerm, pageIndex.ToString(), pageCount.ToString());
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@UserId", userId ?? (object)DBNull.Value),
                new SqlParameter("@SearchTerm", string.IsNullOrEmpty(searchTerm) ? (object)DBNull.Value : searchTerm),
                new SqlParameter("@FromDate", fromDate),
                new SqlParameter("@ToDate", toDate)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }

        public List<Ticker> GetTickers(int? userId, DateTime? lastUpdateDate)
        {
            //sql += " or Tickerid in (select tickerline.tickerid from tickerline where breakingstatusid=7 or categorystatusid=7 or lateststatusid=7)";
            string sql = string.Format(@"select * 
                                         from 
                                         	[Ticker] where [UserId] = ISNULL(@UserId, [UserId]) and ");


            int count = 2;

            if (lastUpdateDate == null)
                count = 1;


            SqlParameter[] parameters = new SqlParameter[count];
            SqlParameter p1 = new SqlParameter("@UserId", userId ?? (object)DBNull.Value);
            parameters[0] = p1;

            if (lastUpdateDate == null)
            {
                sql += "CreationDate >= DATEADD(day, -1, GETDATE()) or Tickerid in (select tickerline.tickerid from tickerline where breakingstatusid=7 or categorystatusid=7 or lateststatusid=7)";
            }
            else if (lastUpdateDate.HasValue)
            {
                sql += "[LastUpdatedDate]  > @LastUpdateDate";
                SqlParameter p2 = new SqlParameter("@LastUpdateDate", lastUpdateDate);
                parameters[1] = p2;
            }
            sql += " order by [LastUpdatedDate],TickerId desc";

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }

        public List<Ticker> GetTickers(TickerStatuses tickerStatus, DateTime? lastUpdateDate)
        {
            string sql = string.Format(@"select a.* 
                                         from 
                                         	[Ticker] a inner join [TickerCategory] b on a.CategoryId = b.CategoryId
                                         where 
                                             (a.[BreakingStatusId] = @TickerStatusId OR 
                                             a.[CategoryStatusId] = @TickerStatusId OR 
                                             a.[LatestStatusId] = @TickerStatusId) and
        	                                 a.[LastUpdatedDate] > ISNULL(@LastUpdateDate, a.[LastUpdatedDate]) and
                                             b.[IsActive] = 1
                                         order by a.[TickerId] desc");

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerStatusId", (int)tickerStatus),
                new SqlParameter("@LastUpdateDate", lastUpdateDate ?? (object)DBNull.Value)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }

        public List<Ticker> GetTickersByTickerOnAirLog(DateTime? lastUpdateDate = null)
        {
            string sql = string.Format(@"SELECT a.* 
                                        FROM [Ticker] a INNER JOIN [MCRCategoryTickerRundown] b on a.[TickerId] = b.[TickerId]
                                        WHERE b.[CreationDate] > ISNULL(@Date, b.[CreationDate])
                                        ORDER BY [TickerId] DESC");

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Date", lastUpdateDate ?? (object)DBNull.Value)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }

        public bool FlushMcrCategoryTickerData()
        {
            string sql = @"update Ticker set CategoryStatusId = @OnAired, LastUpdatedDate = @CurrentDate
                           where TickerId in (select distinct TickerId from MCRCategoryTickerRundown);
                           delete from MCRCategoryTickerRundown";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@OnAired", TickerStatuses.OnAired),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool FlushMcrBreakingTickerData()
        {
            string sql = @"update Ticker set BreakingStatusId = @OnAired, LastUpdatedDate = @CurrentDate
                           where TickerId in (select distinct TickerId from MCRBreakingTickerRundown);
                           delete from MCRBreakingTickerRundown";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@OnAired", TickerStatuses.OnAired),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool FlushMcrLatestTickerData()
        {
            string sql = @"update Ticker set LatestStatusId = @OnAired, LastUpdatedDate = @CurrentDate
                           where TickerId in (select distinct TickerId from MCRLatestTickerRundown);
                           delete from MCRLatestTickerRundown";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@OnAired", TickerStatuses.OnAired),
                new SqlParameter("@CurrentDate", DateTime.UtcNow)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool SubmitTickersToMcrCategory(int tickerId, int categoryId, int count)
        {
            string sql = @"insert into [MCRCategoryTickerRundown] 
                                (TickerId, TickerLineId, [Text], CategoryId, LanguageCode, CreationDate, SequenceNumber, CategoryName)
                           select 
                                b.TickerId, b.TickerLineId, b.[Text], a.CategoryId, b.LanguageCode, @CurrentDate, @SeqId + b.SequenceId,
                                (select Name from [TickerCategory] where CategoryId = a.CategoryId) as CategoryName
                           from 
                                [Ticker] a inner join [TickerLine] b on a.[TickerId] = b.[TickerId]
                           where 
                                a.[TickerId] = @TickerId
                           order by b.[SequenceId] asc
                            
                           update Ticker set CategoryStatusId = @OnAirStatus, LastUpdatedDate = @CurrentDate where TickerId = @TickerId;";

            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@CategoryId", categoryId),
                new SqlParameter("@SeqId", count),
                new SqlParameter("@CurrentDate", DateTime.UtcNow),
                new SqlParameter("@OnAirStatus", (int)TickerStatuses.OnAir)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public bool SubmitTickersToMcrBreaking(int tickerId, int count)
        {
            string sql = @"insert into [MCRBreakingTickerRundown] 
                                (TickerId, TickerLineId, [Text], LanguageCode, CreationDate, SequenceNumber)
                           select 
                                b.TickerId, b.TickerLineId, b.[Text], b.LanguageCode, @CurrentDate, @SeqId + b.SequenceId
                           from 
                                [Ticker] a inner join [TickerLine] b on a.[TickerId] = b.[TickerId]
                           where 
                                a.[TickerId] = @TickerId
                           order by b.[SequenceId] asc
                            
                           update Ticker set BreakingStatusId = @OnAirStatus, LastUpdatedDate = @CurrentDate where TickerId = @TickerId;";

            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@SeqId", count),
                new SqlParameter("@CurrentDate", DateTime.UtcNow),
                new SqlParameter("@OnAirStatus", (int)TickerStatuses.OnAir)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public bool SubmitTickersToMcrLatest(int tickerId, int count)
        {
            string sql = @"insert into [MCRLatestTickerRundown] 
                                (TickerId, TickerLineId, [Text], LanguageCode, CreationDate, SequenceNumber)
                           select 
                                b.TickerId, b.TickerLineId, b.[Text], b.LanguageCode, @CurrentDate, @SeqId + b.SequenceId
                           from 
                                [Ticker] a inner join [TickerLine] b on a.[TickerId] = b.[TickerId]
                           where 
                                a.[TickerId] = @TickerId
                           order by b.[SequenceId] asc
                            
                           update Ticker set LatestStatusId = @OnAirStatus, LastUpdatedDate = @CurrentDate where TickerId = @TickerId;";

            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@SeqId", count),
                new SqlParameter("@CurrentDate", DateTime.UtcNow),
                new SqlParameter("@OnAirStatus", (int)TickerStatuses.OnAir)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public Ticker UpdateTickerStatus(Ticker ticker, TickerStatuses status, int? tickerTypeId)
        {
            string sql = string.Empty;

            if (tickerTypeId.HasValue)
            {
                if (tickerTypeId.Value == (int)TickerTypes.Breaking)
                {
                    sql = @"Update [Ticker] set 
                                [BreakingStatusId] = @TickerStatusId,
                                [LastUpdatedDate] = @Date
                            where [TickerId] = @TickerId; ";
                }
                else if (tickerTypeId.Value == (int)TickerTypes.Category)
                {
                    sql = @"Update [Ticker] set 
                                [CategoryStatusId] = @TickerStatusId,
                                [LastUpdatedDate] = @Date
                            where [TickerId] = @TickerId; ";
                }
                else if (tickerTypeId.Value == (int)TickerTypes.Latest)
                {
                    sql = @"Update [Ticker] set 
                                [LatestStatusId] = @TickerStatusId,
                                [LastUpdatedDate] = @Date
                            where [TickerId] = @TickerId; ";
                }
            }
            else
            {
                sql = "Update [Ticker] set ";

                //if (ticker.LatestStatusId != (int)TickerStatuses.OnAir && ticker.LatestStatusId != (int)TickerStatuses.Locked)
                //    sql += "[LatestStatusId] = @TickerStatusId,";
                //if (ticker.CategoryStatusId != (int)TickerStatuses.OnAir && ticker.CategoryStatusId != (int)TickerStatuses.Locked)
                //    sql += "[CategoryStatusId] = @TickerStatusId,";
                sql += "[LastUpdatedDate] = @Date where [TickerId] = @TickerId; ";
            }

            sql += @" Update [TickerLine] set
                        [TickerStatusId] = @TickerStatusId,
                        [LastUpdatedDate] = @Date
                     where [TickerId] = @TickerId; select * from Ticker Where TickerId =@TickerId";

            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@TickerStatusId", (int)status),
                new SqlParameter("@TickerId", ticker.TickerId),
                new SqlParameter("@Date", DateTime.UtcNow)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameterArray);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TickerFromDataRow(ds.Tables[0].Rows[0]);
        }

        public bool UpdateTicker(int tickerId, int severity, int repeatCount, int frequency)
        {

            string sql = @"Update [Ticker] set";

            if (severity == (int)TickerSeverity.High)
                sql += @"[BreakingStatusId] = @BreakingStatusId,";


            sql += @"[Severity] = @Severity,
                     [RepeatCount] = @RepeatCount,
                     [Frequency] =  @Frequency,
                     [LastUpdatedDate] = @Date
                     where [TickerId] = @TickerId ";

            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@Severity", severity),
                new SqlParameter("@RepeatCount", repeatCount),
                new SqlParameter("@Frequency", frequency),
                new SqlParameter("@BreakingStatusId", (int)TickerStatuses.Approved),
                new SqlParameter("@Date", DateTime.UtcNow)
            };


            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }

        public bool UpdateTickerLastUpdateDate(DateTime? date, int? tickerId)
        {
            string sql = @"Update [Ticker] set 
                            [LastUpdatedDate] = @Date
                        where [TickerId] = @TickerId;";

            SqlParameter[] parameterArray = new SqlParameter[] {
                new SqlParameter("@Date", date),
                new SqlParameter("@TickerId", tickerId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }


        public List<Ticker> GetTickerByLastUpdateDate(DateTime lastUpdateDate)
        {
            string sql = string.Format(@"select a.* 
                                         from 
                                         	[Ticker] a inner join [TickerCategory] b on a.CategoryId = b.CategoryId
                                         where 
        	                                 a.[LastUpdatedDate] > ISNULL(@LastUpdateDate, a.[LastUpdatedDate]) and
                                             b.[IsActive] = 1
                                         order by a.[TickerId] desc");

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@LastUpdateDate", lastUpdateDate)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }


        public List<Ticker> GetFilteredTickers(List<TickerStatus> discardedStatus)
        {
            List<int> list = discardedStatus.Select(i => i.TickerStatusId).ToList();
            string statusIds = string.Join<int>(",", list);

            string sql = string.Format(@"select a.* 
                                         from 
                                         	[Ticker] a inner join [TickerCategory] b on a.CategoryId = b.CategoryId
                                         where 
                                          	a.[TickerStatusId] not in ({0}) and
                                            b.[IsActive] = 1 order by a.[TickerId] desc", statusIds);

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }


        public bool DeleteMcrTickerData(int tickerId, int tickerTypeId)
        {
            string sql = string.Empty;

            if (tickerTypeId == (int)TickerTypes.Breaking)
            {
                sql += @"delete from MCRBreakingTickerRundown";
            }
            else if (tickerTypeId == (int)TickerTypes.Latest)
            {
                sql += @"delete from MCRLatestTickerRundown";
            }
            else if (tickerTypeId == (int)TickerTypes.Category)
            {
                sql += @"delete from MCRCategoryTickerRundown";
            }

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId",tickerId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public Ticker GetTickerLastRecord()
        {

            Ticker ticker = new Ticker();
            string sql = string.Format(@"select Top 1 a.* 
                                         from 
                                         	[Ticker] a inner join [TickerCategory] b on a.CategoryId = b.CategoryId
                                         where 
                                             b.[IsActive] = 1
                                         order by a.[TickerId] desc");
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TickerFromDataRow(ds.Tables[0].Rows[0]);
        }

        public bool UpdateMcrTickerData(int TickerId, int TickerLineId, int TickerTypeId, bool isFromOnAir)
        {
            string sql = string.Empty;

            if (TickerTypeId == (int)TickerTypes.Breaking && !isFromOnAir)
            {
                sql += @"update MCRBreakingTickerRundown 
                        set [Text] = (select [Text] from TickerLine where TickerLineId = @TickerLineId)
		                where TickerLineId = @TickerLineId and TickerId = @TickerId";
            }
            else if (TickerTypeId == (int)TickerTypes.Latest && !isFromOnAir)
            {

                sql += @"update MCRLatestTickerRundown 
                        set [Text] = (select [Text] from TickerLine where TickerLineId = @TickerLineId)
		                where TickerLineId = @TickerLineId and TickerId = @TickerId";
            }
            else if (TickerTypeId == (int)TickerTypes.Category && !isFromOnAir)
            {
                sql += @"update MCRCategoryTickerRundown 
                        set [Text] = (select [Text] from TickerLine where TickerLineId = @TickerLineId)
		                where TickerLineId = @TickerLineId and TickerId = @TickerId";
            }
            else if (isFromOnAir)
            {
                sql += @"update MCRBreakingTickerRundown 
                        set [Text] = (select [Text] from TickerLine where TickerLineId = @TickerLineId)
		                where TickerLineId = @TickerLineId and TickerId = @TickerId ";

                sql += @"update MCRLatestTickerRundown 
                        set [Text] = (select [Text] from TickerLine where TickerLineId = @TickerLineId)
		                where TickerLineId = @TickerLineId and TickerId = @TickerId ";

                sql += @"update MCRCategoryTickerRundown 
                        set [Text] = (select [Text] from TickerLine where TickerLineId = @TickerLineId)
		                where TickerLineId = @TickerLineId and TickerId = @TickerId";
            }
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId",TickerId),
                new SqlParameter("@TickerLineId",TickerLineId)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }
        public bool UpdateTickerSequence(Ticker Ticker, int Type)
        {

            string sql = string.Empty;
            SqlParameter[] parameterArray = new SqlParameter[2];

            if (Type == (int)TickerTypes.Breaking)
            {
                sql += @"update [Ticker] set BreakingSequenceId = @SequenceNumber where TickerId = @TickerId;";
                parameterArray = new SqlParameter[]{
                new SqlParameter("@TickerId",Ticker.TickerId)
                //new SqlParameter("@SequenceNumber", Ticker.BreakingSequenceId)
               };
            }
            else if (Type == (int)TickerTypes.Latest)
            {
                sql += @"update [Ticker] set LatestSequenceId = @SequenceNumber where TickerId = @TickerId;";
                parameterArray = new SqlParameter[]{
                new SqlParameter("@TickerId",Ticker.TickerId)
                //new SqlParameter("@SequenceNumber", Ticker.LatestSequenceId)
               };
            }
            else if (Type == (int)TickerTypes.Category)
            {
                sql += @"update [Ticker] set CategorySequenceId = @SequenceNumber where TickerId = @TickerId;";
                parameterArray = new SqlParameter[]{
                new SqlParameter("@TickerId",Ticker.TickerId)
                //new SqlParameter("@SequenceNumber", Ticker.CategorySequenceId)
               };
            }

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }


        public Ticker GetTickerByNewsGuid(string newsguid)
        {
            string sql = string.Format(@"select * from [Ticker] where [NewsGuid] = @NewsGuid");
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@NewsGuid", newsguid)
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TickerFromDataRow(ds.Tables[0].Rows[0]);
        }


        public List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds)
        {
            string sql = GetTickerSelectClause();
            sql += " from [Ticker] with (nolock)  where CategoryId in (" + string.Join(",", tickerCategoryIds) + ") and IsActive = 1";

            SqlParameter[] parameters = new SqlParameter[] {
               // new SqlParameter("@Date", lastUpdateDate ?? (object)DBNull.Value)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }
        public List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds, DateTime lastUpdateDate)
        {
            string sql = GetTickerSelectClause();
            sql += " from [Ticker] with (nolock)  where CategoryId in (" + string.Join(",", tickerCategoryIds) + ") and lastupdateddate >= @lastUpdateDate";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@lastUpdateDate", lastUpdateDate == DateTime.MinValue? SqlDateTime.MinValue : lastUpdateDate)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }

        public bool SortTicker(int tickerId, int tickerCategoryId, int sequenceId, SortDirection direction)
        {
            var sql = string.Empty;
            if (direction == SortDirection.Up)
            {
                sql = @"
                        update ticker set sequenceid = @SequenceId, LastUpdatedDate=@UtcTime where tickerid = @TickerId and categoryId = @TickerCateogryId
                        update ticker set sequenceid =  sequenceid -1, LastUpdatedDate=@UtcTime where tickerid <> @TickerId and sequenceid <= @SequenceId and categoryId = @TickerCateogryId
                        ";
            }
            else if (direction == SortDirection.Down)
            {
                sql = @"
                        update ticker set sequenceid = @SequenceId, LastUpdatedDate=@UtcTime where tickerid = @TickerId and categoryId = @TickerCateogryId
                        update ticker set sequenceid = sequenceid - 1, LastUpdatedDate=@UtcTime where sequenceid = @SequenceId and tickerid <> @TickerId and categoryId = @TickerCateogryId
                        update ticker set sequenceid =  sequenceid +1, LastUpdatedDate=@UtcTime where tickerid <> @TickerId and sequenceid >= @SequenceId and categoryId = @TickerCateogryId
                        ";
            }
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@SequenceId", sequenceId),
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@TickerCateogryId", tickerCategoryId),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public int GetMaxSequenceId()
        {
            var sql = @"select isnull(max(isnull(sequenceid, 0)), 0)+1 from ticker";
            SqlParameter[] parameters = new SqlParameter[] {
            };
            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameters));
        }

        public bool UpdateCategorySequenceIdByCreationDate(int categoryId, DateTime date, int sequenceId)
        {
            var sql = @"update t set SequenceId = @SequenceId + rowId, LastUpdatedDate = @UtcTime
				from ticker t with (nolock)
				join (select tickerid, ROW_NUMBER() over (order by sequenceid asc) rowId from ticker ts with (nolock)
					where ts.CreationDate > @CreationDate
				and ts.CategoryId = @CategoryId
				) tbl on t.TickerId = tbl.TickerId
				where t.CreationDate > @CreationDate
				and t.CategoryId = @CategoryId";
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@CreationDate",date),
                new SqlParameter("@UtcTime", DateTime.UtcNow),
                new SqlParameter("@CategoryId", categoryId),
                new SqlParameter("@SequenceId", sequenceId)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }
        public bool UpdateTickerbById(int tickerId, string text, int statusId)
        {
            var sql = string.Empty;

            sql = @"update ticker set Text = @TickerText, StatusId = @StatusId, OnAiredTime = @UtcTime, LastUpdatedDate=@UtcTime  where tickerid = @TickerId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@TickerText", text),
                new SqlParameter("@StatusId", statusId),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }
        public bool MoveTickerByCategoryId(int tickerId, int tickerCategoryId)
        {
            var sql = string.Empty;

            sql = @"update ticker set CategoryId = @TickerCategoryId, LastUpdatedDate=@UtcTime  where tickerid = @TickerId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@TickerCategoryId", tickerCategoryId),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public List<int> GetNotExistingNewsFileIds(List<int> newsFileIds, int tickerCategoryId)
        {
            var sql = string.Empty;

            sql = @"select NewsFileId from ticker where isactive = 1 and categoryId = @TickerCategoryId and newsfileid in (" + string.Join(",", newsFileIds) + ")";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerCategoryId", tickerCategoryId)
            };
            var existingNewsFileIds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters).Tables[0].AsEnumerable()
                .Select(x => Convert.ToInt32(x["NewsFileId"])).ToList();

            return newsFileIds.Where(x => !existingNewsFileIds.Contains(x)).ToList();
        }

        public bool UpdateMCRTickerId(int tickerId, int mcrTickerId)
        {

            var sql = @"update ticker set MCRTickerID = @MCRTickerID, LastUpdatedDate=@UtcTime  where tickerid = @TickerId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@MCRTickerID", mcrTickerId),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool MoveNewsTickerToTop(int newsFileId, int tickerCategoryId)
        {
            var sql = @"declare @tickerId int
select top 1 @tickerId = TickerId from ticker where newsfileid = @NewsFileId and categoryid = @TickerCategoryId and IsActive = 1
order by LastUpdatedDate

update ticker
set SequenceId = @SequenceId ,
LastUpdatedDate=@UtcTime
where tickerid = @tickerId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@NewsFileId", newsFileId),
                new SqlParameter("@TickerCategoryId", tickerCategoryId),
                new SqlParameter("@SequenceId", GetMaxSequenceId()),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public List<Ticker> GetTickersForMCR(int categoryId, int size, bool sortBySequence, bool? isApproved)
        {
            var sortBy = sortBySequence ? "SequenceId" : "CreationDate";


            var sql = $@"select top(@Size) * from [Ticker] with (nolock) 
                where CategoryId = @CategoryId and (IsObsolete is null or IsObsolete = 0) and isactive = 1  {(isApproved.HasValue ? " and isnull(IsApproved, 0) = @IsApproved" : "")}
                order by {sortBy} desc";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@CategoryId",categoryId),
                new SqlParameter("@Size",size),
                new SqlParameter("@IsApproved", isApproved.HasValue ? isApproved.Value : false)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<Ticker>();
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }

        public bool MarkTickersDeleteByHour(int hour, int tickerCategoryId)
        {
            var sql = @"update ticker
set IsObsolete = 1 ,
LastUpdatedDate=@UtcTime
where
CreationDate < DATEADD(hour, @Hour * -1, GETUTCDATE())

and CategoryId = @TickerCategoryId
";
            //and CategoryId not in (select TickerCategoryId from TickerCategory where TicketCategoryTypeId = 1)

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Hour", hour),
                new SqlParameter("@TickerCategoryId",tickerCategoryId ),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool UpdateTickerSequenceByNewsFileId(int newsFileId, int tickerCategoryId, int sequenceId)
        {
            var sql = @"declare @tickerId int
select top 1 @tickerId = TickerId from ticker where newsfileid = @NewsFileId and categoryid = @TickerCategoryId and IsActive = 1
order by LastUpdatedDate

update ticker
set SequenceId = @SequenceId ,
LastUpdatedDate=@UtcTime
where tickerid = @tickerId";
            //and CategoryId not in (select TickerCategoryId from TickerCategory where TicketCategoryTypeId = 1)

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@NewsFileId", newsFileId),
                new SqlParameter("@TickerCategoryId",tickerCategoryId ),
                new SqlParameter("@SequenceId",sequenceId ),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool MarkTickersUnapproved(int tickerCategoryId)
        {
            var sql = @"update ticker set IsApproved = 0, LastUpdatedDate=@UtcTime where categoryid = @TickerCategoryId
";
            //and CategoryId not in (select TickerCategoryId from TickerCategory where TicketCategoryTypeId = 1)

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerCategoryId",tickerCategoryId ),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public bool MarkTickerValid(int tickerId)
        {
            var sql = @"update ticker set IsObsolete = 0, LastUpdatedDate=@UtcTime  where tickerid = @TickerId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }


        public bool ApproveTickers(List<int> idsToApprove)
        {
            var sql = @"update ticker set IsApproved = 1, LastUpdatedDate=@UtcTime where tickerid in (" + string.Join(",", idsToApprove) + ")";

            SqlParameter[] parameters = new SqlParameter[] {
                 new SqlParameter("@UtcTime", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }

        public List<Ticker> GetByIds(List<int> tickerIds)
        {
            if(tickerIds.Count == 0)
            {
                return new List<Ticker>();
            }
            string sql = GetTickerSelectClause();
            sql += " from [Ticker] with (nolock)  where TickerId in (" + string.Join(",", tickerIds) + ")";

            SqlParameter[] parameters = new SqlParameter[] {
               // new SqlParameter("@Date", lastUpdateDate ?? (object)DBNull.Value)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<Ticker>();
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }
    }


}
