﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TickerStatusRepositoryBase : Repository, ITickerStatusRepositoryBase 
	{
        
        public TickerStatusRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerStatusId",new SearchColumn(){Name="TickerStatusId",Title="TickerStatusId",SelectClause="TickerStatusId",WhereClause="AllRecords.TickerStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerStatusName",new SearchColumn(){Name="TickerStatusName",Title="TickerStatusName",SelectClause="TickerStatusName",WhereClause="AllRecords.TickerStatusName",DataType="System.String",IsForeignColumn=false,PropertyName="TickerStatusName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerStatusSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerStatusBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerStatusAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerStatusSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerStatus].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerStatus].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TickerStatus GetTickerStatus(System.Int32 TickerStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerStatus] with (nolock)  where TickerStatusId=@TickerStatusId ";
			SqlParameter parameter=new SqlParameter("@TickerStatusId",TickerStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerStatusFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerStatus> GetTickerStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerStatus] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerStatus>(ds,TickerStatusFromDataRow);
		}

		public virtual List<TickerStatus> GetAllTickerStatus(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerStatus] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerStatus>(ds, TickerStatusFromDataRow);
		}

		public virtual List<TickerStatus> GetPagedTickerStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerStatusCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerStatusId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerStatusId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerStatusId] ";
            tempsql += " FROM [TickerStatus] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerStatusId"))
					tempsql += " , (AllRecords.[TickerStatusId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerStatusId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerStatusSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerStatus] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerStatus].[TickerStatusId] = PageIndex.[TickerStatusId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerStatus>(ds, TickerStatusFromDataRow);
			}else{ return null;}
		}

		private int GetTickerStatusCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerStatus as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerStatus as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerStatus))]
		public virtual TickerStatus InsertTickerStatus(TickerStatus entity)
		{

			TickerStatus other=new TickerStatus();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerStatus ( [TickerStatusName] )
				Values
				( @TickerStatusName );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerStatusName",entity.TickerStatusName ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerStatus(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerStatus))]
		public virtual TickerStatus UpdateTickerStatus(TickerStatus entity)
		{

			if (entity.IsTransient()) return entity;
			TickerStatus other = GetTickerStatus(entity.TickerStatusId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerStatus set  [TickerStatusName]=@TickerStatusName 
							 where TickerStatusId=@TickerStatusId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerStatusName",entity.TickerStatusName ?? (object)DBNull.Value)
					, new SqlParameter("@TickerStatusId",entity.TickerStatusId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerStatus(entity.TickerStatusId);
		}

		public virtual bool DeleteTickerStatus(System.Int32 TickerStatusId)
		{

			string sql="delete from TickerStatus where TickerStatusId=@TickerStatusId";
			SqlParameter parameter=new SqlParameter("@TickerStatusId",TickerStatusId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerStatus))]
		public virtual TickerStatus DeleteTickerStatus(TickerStatus entity)
		{
			this.DeleteTickerStatus(entity.TickerStatusId);
			return entity;
		}


		public virtual TickerStatus TickerStatusFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerStatus entity=new TickerStatus();
			if (dr.Table.Columns.Contains("TickerStatusId"))
			{
			entity.TickerStatusId = (System.Int32)dr["TickerStatusId"];
			}
			if (dr.Table.Columns.Contains("TickerStatusName"))
			{
			entity.TickerStatusName = dr["TickerStatusName"].ToString();
			}
			return entity;
		}

	}
	
	
}
