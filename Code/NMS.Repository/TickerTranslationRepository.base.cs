﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TickerTranslationRepositoryBase : Repository, ITickerTranslationRepositoryBase 
	{
        
        public TickerTranslationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerTranslationId",new SearchColumn(){Name="TickerTranslationId",Title="TickerTranslationId",SelectClause="TickerTranslationId",WhereClause="AllRecords.TickerTranslationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerTranslationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageId",new SearchColumn(){Name="LanguageId",Title="LanguageId",SelectClause="LanguageId",WhereClause="AllRecords.LanguageId",DataType="System.Int32",IsForeignColumn=false,PropertyName="LanguageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StatusId",new SearchColumn(){Name="StatusId",Title="StatusId",SelectClause="StatusId",WhereClause="AllRecords.StatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="StatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreatedBy",new SearchColumn(){Name="CreatedBy",Title="CreatedBy",SelectClause="CreatedBy",WhereClause="AllRecords.CreatedBy",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CreatedBy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerTranslationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerTranslationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerTranslationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerTranslationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerTranslation].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerTranslation].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TickerTranslation GetTickerTranslation(System.Int32 TickerTranslationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerTranslationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerTranslation] with (nolock)  where TickerTranslationId=@TickerTranslationId ";
			SqlParameter parameter=new SqlParameter("@TickerTranslationId",TickerTranslationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerTranslationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerTranslation> GetTickerTranslationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerTranslationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerTranslation] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerTranslation>(ds,TickerTranslationFromDataRow);
		}

		public virtual List<TickerTranslation> GetAllTickerTranslation(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerTranslationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerTranslation] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerTranslation>(ds, TickerTranslationFromDataRow);
		}

		public virtual List<TickerTranslation> GetPagedTickerTranslation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerTranslationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerTranslationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerTranslationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerTranslationId] ";
            tempsql += " FROM [TickerTranslation] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerTranslationId"))
					tempsql += " , (AllRecords.[TickerTranslationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerTranslationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerTranslationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerTranslation] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerTranslation].[TickerTranslationId] = PageIndex.[TickerTranslationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerTranslation>(ds, TickerTranslationFromDataRow);
			}else{ return null;}
		}

		private int GetTickerTranslationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerTranslation as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerTranslation as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerTranslation))]
		public virtual TickerTranslation InsertTickerTranslation(TickerTranslation entity)
		{

			TickerTranslation other=new TickerTranslation();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerTranslation ( [TickerId]
				,[Text]
				,[LanguageId]
				,[StatusId]
				,[LastUpdatedDate]
				,[IsActive]
				,[CreatedBy]
				,[CreationDate] )
				Values
				( @TickerId
				, @Text
				, @LanguageId
				, @StatusId
				, @LastUpdatedDate
				, @IsActive
				, @CreatedBy
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId)
					, new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageId",entity.LanguageId)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerTranslation(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerTranslation))]
		public virtual TickerTranslation UpdateTickerTranslation(TickerTranslation entity)
		{

			if (entity.IsTransient()) return entity;
			TickerTranslation other = GetTickerTranslation(entity.TickerTranslationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerTranslation set  [TickerId]=@TickerId
							, [Text]=@Text
							, [LanguageId]=@LanguageId
							, [StatusId]=@StatusId
							, [LastUpdatedDate]=@LastUpdatedDate
							, [IsActive]=@IsActive
							, [CreationDate]=@CreationDate 
							 where TickerTranslationId=@TickerTranslationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerId",entity.TickerId)
					, new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageId",entity.LanguageId)
					, new SqlParameter("@StatusId",entity.StatusId)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@CreatedBy",entity.CreatedBy ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@TickerTranslationId",entity.TickerTranslationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerTranslation(entity.TickerTranslationId);
		}

		public virtual bool DeleteTickerTranslation(System.Int32 TickerTranslationId)
		{

			string sql="delete from TickerTranslation where TickerTranslationId=@TickerTranslationId";
			SqlParameter parameter=new SqlParameter("@TickerTranslationId",TickerTranslationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerTranslation))]
		public virtual TickerTranslation DeleteTickerTranslation(TickerTranslation entity)
		{
			this.DeleteTickerTranslation(entity.TickerTranslationId);
			return entity;
		}


		public virtual TickerTranslation TickerTranslationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerTranslation entity=new TickerTranslation();
			if (dr.Table.Columns.Contains("TickerTranslationId"))
			{
			entity.TickerTranslationId = (System.Int32)dr["TickerTranslationId"];
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = (System.Int32)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("LanguageId"))
			{
			entity.LanguageId = (System.Int32)dr["LanguageId"];
			}
			if (dr.Table.Columns.Contains("StatusId"))
			{
			entity.StatusId = (System.Int32)dr["StatusId"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = dr["LastUpdatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdatedDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreatedBy"))
			{
			entity.CreatedBy = dr["CreatedBy"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CreatedBy"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
