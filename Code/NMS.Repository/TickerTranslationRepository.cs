﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Enums;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{

    public partial class TickerTranslationRepository : TickerTranslationRepositoryBase, ITickerTranslationRepository
    {
        public List<TickerTranslation> GetTickerTranslations(List<int> tickerIds, LanguageCode languageCode)
        {
            if (tickerIds.Count == 0) { return new List<TickerTranslation>(); }
            string sql = GetTickerTranslationSelectClause();
            sql += " from [TickerTranslation] with (nolock)  where TickerId in (" + string.Join(",", tickerIds) + ") and LanguageId = @LanguageId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@LanguageId", (int)languageCode)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return new List<TickerTranslation>();
            return CollectionFromDataSet<TickerTranslation>(ds, TickerTranslationFromDataRow);
        }

        public TickerTranslation GetTranslation(int tickerId, LanguageCode languageCode)
        {
            string sql = GetTickerTranslationSelectClause();
            sql += " from [TickerTranslation] with (nolock)  where TickerId = @TickerId and LanguageId = @LanguageId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@LanguageId", (int)languageCode),
                new SqlParameter("@TickerId", tickerId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return TickerTranslationFromDataRow(ds.Tables[0].Rows[0]);
        }

        public bool UpdateTranslation(int tickerId, string text, int statusId, LanguageCode languageCode, int userId)
        {

            var sql = string.Empty;

            sql = @"
if exists (select 1 from TickerTranslation where tickerid = @TickerId and LanguageId = @LanguageId)
begin
update TickerTranslation set Text = @TickerText, StatusId = @StatusId, LastUpdatedDate=@UtcTime  where tickerid = @TickerId and LanguageId = @LanguageId
end
else 
begin 
insert into TickerTranslation( TickerId, [Text], LanguageId, StatusId, CreationDate, IsActive, CreatedBy)
values (@TickerId, @TickerText, @LanguageId, @StatusId,@UtcTime, 1, @UserId )
end
";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
                new SqlParameter("@TickerText", text),
                new SqlParameter("@StatusId", statusId),
                new SqlParameter("@UtcTime", DateTime.UtcNow),
                new SqlParameter("@LanguageId", (int)languageCode),
                new SqlParameter("@UserId", userId)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }
    }


}
