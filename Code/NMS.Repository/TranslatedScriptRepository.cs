﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Repository
{
		
	public partial class TranslatedScriptRepository: TranslatedScriptRepositoryBase, ITranslatedScriptRepository
	{
        public virtual List<TranslatedScript> GetTranslatedScriptByLanguageId(int languageId)
        {

            string sql = "select * from [TranslatedScript] with (nolock)  where LanguageCode = @LanguageCode";
            SqlParameter parameter = new SqlParameter("@LanguageCode", languageId);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TranslatedScript>(ds, TranslatedScriptFromDataRow);
        }
    }
	
	
}
