﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class TwitterAccountRepositoryBase : Repository, ITwitterAccountRepositoryBase 
	{
        
        public TwitterAccountRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TwitterAccountId",new SearchColumn(){Name="TwitterAccountId",Title="TwitterAccountId",SelectClause="TwitterAccountId",WhereClause="AllRecords.TwitterAccountId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TwitterAccountId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserName",new SearchColumn(){Name="UserName",Title="UserName",SelectClause="UserName",WhereClause="AllRecords.UserName",DataType="System.String",IsForeignColumn=false,PropertyName="UserName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CelebrityId",new SearchColumn(){Name="CelebrityId",Title="CelebrityId",SelectClause="CelebrityId",WhereClause="AllRecords.CelebrityId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CelebrityId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Url",new SearchColumn(){Name="Url",Title="Url",SelectClause="Url",WhereClause="AllRecords.Url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsFollowed",new SearchColumn(){Name="IsFollowed",Title="IsFollowed",SelectClause="IsFollowed",WhereClause="AllRecords.IsFollowed",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsFollowed",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CategoryId",new SearchColumn(){Name="CategoryId",Title="CategoryId",SelectClause="CategoryId",WhereClause="AllRecords.CategoryId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="CategoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LocationId",new SearchColumn(){Name="LocationId",Title="LocationId",SelectClause="LocationId",WhereClause="AllRecords.LocationId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="LocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTwitterAccountSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTwitterAccountBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTwitterAccountAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTwitterAccountSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TwitterAccount].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TwitterAccount].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<TwitterAccount> GetTwitterAccountByCelebrityId(System.Int32? CelebrityId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTwitterAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TwitterAccount] with (nolock)  where CelebrityId=@CelebrityId  ";
			SqlParameter parameter=new SqlParameter("@CelebrityId",CelebrityId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TwitterAccount>(ds,TwitterAccountFromDataRow);
		}

		public virtual TwitterAccount GetTwitterAccount(System.Int32 TwitterAccountId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTwitterAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TwitterAccount] with (nolock)  where TwitterAccountId=@TwitterAccountId ";
			SqlParameter parameter=new SqlParameter("@TwitterAccountId",TwitterAccountId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TwitterAccountFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TwitterAccount> GetTwitterAccountByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTwitterAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TwitterAccount] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TwitterAccount>(ds,TwitterAccountFromDataRow);
		}

		public virtual List<TwitterAccount> GetAllTwitterAccount(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTwitterAccountSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TwitterAccount] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TwitterAccount>(ds, TwitterAccountFromDataRow);
		}

		public virtual List<TwitterAccount> GetPagedTwitterAccount(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTwitterAccountCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TwitterAccountId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TwitterAccountId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TwitterAccountId] ";
            tempsql += " FROM [TwitterAccount] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TwitterAccountId"))
					tempsql += " , (AllRecords.[TwitterAccountId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TwitterAccountId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTwitterAccountSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TwitterAccount] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TwitterAccount].[TwitterAccountId] = PageIndex.[TwitterAccountId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TwitterAccount>(ds, TwitterAccountFromDataRow);
			}else{ return null;}
		}

		private int GetTwitterAccountCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TwitterAccount as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TwitterAccount as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TwitterAccount))]
		public virtual TwitterAccount InsertTwitterAccount(TwitterAccount entity)
		{

			TwitterAccount other=new TwitterAccount();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TwitterAccount ( [UserName]
				,[CelebrityId]
				,[Url]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsFollowed]
				,[CategoryId]
				,[LocationId] )
				Values
				( @UserName
				, @CelebrityId
				, @Url
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsFollowed
				, @CategoryId
				, @LocationId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserName",entity.UserName)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsFollowed",entity.IsFollowed ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTwitterAccount(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TwitterAccount))]
		public virtual TwitterAccount UpdateTwitterAccount(TwitterAccount entity)
		{

			if (entity.IsTransient()) return entity;
			TwitterAccount other = GetTwitterAccount(entity.TwitterAccountId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TwitterAccount set  [UserName]=@UserName
							, [CelebrityId]=@CelebrityId
							, [Url]=@Url
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsFollowed]=@IsFollowed
							, [CategoryId]=@CategoryId
							, [LocationId]=@LocationId 
							 where TwitterAccountId=@TwitterAccountId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserName",entity.UserName)
					, new SqlParameter("@CelebrityId",entity.CelebrityId ?? (object)DBNull.Value)
					, new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@IsFollowed",entity.IsFollowed ?? (object)DBNull.Value)
					, new SqlParameter("@CategoryId",entity.CategoryId ?? (object)DBNull.Value)
					, new SqlParameter("@LocationId",entity.LocationId ?? (object)DBNull.Value)
					, new SqlParameter("@TwitterAccountId",entity.TwitterAccountId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTwitterAccount(entity.TwitterAccountId);
		}

		public virtual bool DeleteTwitterAccount(System.Int32 TwitterAccountId)
		{

			string sql="delete from TwitterAccount where TwitterAccountId=@TwitterAccountId";
			SqlParameter parameter=new SqlParameter("@TwitterAccountId",TwitterAccountId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TwitterAccount))]
		public virtual TwitterAccount DeleteTwitterAccount(TwitterAccount entity)
		{
			this.DeleteTwitterAccount(entity.TwitterAccountId);
			return entity;
		}


		public virtual TwitterAccount TwitterAccountFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TwitterAccount entity=new TwitterAccount();
			if (dr.Table.Columns.Contains("TwitterAccountId"))
			{
			entity.TwitterAccountId = (System.Int32)dr["TwitterAccountId"];
			}
			if (dr.Table.Columns.Contains("UserName"))
			{
			entity.UserName = dr["UserName"].ToString();
			}
			if (dr.Table.Columns.Contains("CelebrityId"))
			{
			entity.CelebrityId = dr["CelebrityId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CelebrityId"];
			}
			if (dr.Table.Columns.Contains("Url"))
			{
			entity.Url = dr["Url"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("IsFollowed"))
			{
			entity.IsFollowed = dr["IsFollowed"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsFollowed"];
			}
			if (dr.Table.Columns.Contains("CategoryId"))
			{
			entity.CategoryId = dr["CategoryId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["CategoryId"];
			}
			if (dr.Table.Columns.Contains("LocationId"))
			{
			entity.LocationId = dr["LocationId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["LocationId"];
			}
			return entity;
		}

	}
	
	
}
