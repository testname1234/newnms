﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;

namespace NMS.Repository
{
		
	public partial class TwitterAccountRepository: TwitterAccountRepositoryBase, ITwitterAccountRepository
	{

        public virtual List<TwitterAccount> GetByIsFollowed(bool isFollowed, string SelectClause = null)
        {
            string sql = string.IsNullOrEmpty(SelectClause) ? GetTwitterAccountSelectClause() : (string.Format("Select {0} ", SelectClause));
            sql += "from [TwitterAccount] with (nolock)  where isFollowed=@isFollowed and isactive = 1 ";
            SqlParameter parameter = new SqlParameter("@isFollowed", isFollowed);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TwitterAccount>(ds, TwitterAccountFromDataRow);
        }

        public virtual void UpdateTwitterAccount(int TwitterAccoutnId, bool Isfollowed)
        {
            string sql = @"Update TwitterAccount set  [IsFollowed]=@IsFollowed 
							 where TwitterAccountId=@TwitterAccountId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					  new SqlParameter("@IsFollowed",Isfollowed)
					, new SqlParameter("@TwitterAccountId",TwitterAccoutnId)};
            SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray);            
        }
		
	}
	
	
}
