﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using NMS.Core.Enums;

namespace NMS.Repository
{	
	public partial class UserRepository: UserRepositoryBase, IUserRepository
	{
        public User Login(string login, string password)
        {
            string sql = GetUserSelectClause();
            sql += " from [User] (nolock) where Login=@Login and Password=@Password ";
            SqlParameter parameter = new SqlParameter("@Login", login);
            SqlParameter parameter1 = new SqlParameter("@Password", password);
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, new SqlParameter[] { parameter, parameter1 });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return UserFromDataRow(ds.Tables[0].Rows[0]);
        }

        public List<User> GetUsers(TeamRoles teamRole)
        {
            string sql = "select a.* from [User] a (nolock) inner join [UserRole] b (nolock) on a.UserId = b.UserId where a.RoleId = @TeamRoleId";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@TeamRoleId", (int)teamRole) };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<User>(ds, UserFromDataRow);
        }
    }
}
