﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class UserRoleRepositoryBase : Repository, IUserRoleRepositoryBase 
	{
        
        public UserRoleRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("UserRoleId",new SearchColumn(){Name="UserRoleId",Title="UserRoleId",SelectClause="UserRoleId",WhereClause="AllRecords.UserRoleId",DataType="System.Int32",IsForeignColumn=false,PropertyName="UserRoleId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("RoleId",new SearchColumn(){Name="RoleId",Title="RoleId",SelectClause="RoleId",WhereClause="AllRecords.RoleId",DataType="System.Int32",IsForeignColumn=false,PropertyName="RoleId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetUserRoleSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetUserRoleBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetUserRoleAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetUserRoleSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "UserRole."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",UserRole."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<UserRole> GetUserRoleByUserId(System.Int32 UserId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetUserRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from UserRole with (nolock)  where UserId=@UserId  ";
			SqlParameter parameter=new SqlParameter("@UserId",UserId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserRole>(ds,UserRoleFromDataRow);
		}

		public virtual List<UserRole> GetUserRoleByRoleId(System.Int32 RoleId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetUserRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from UserRole with (nolock)  where RoleId=@RoleId  ";
			SqlParameter parameter=new SqlParameter("@RoleId",RoleId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserRole>(ds,UserRoleFromDataRow);
		}

		public virtual UserRole GetUserRole(System.Int32 UserRoleId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetUserRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from UserRole with (nolock)  where UserRoleId=@UserRoleId ";
			SqlParameter parameter=new SqlParameter("@UserRoleId",UserRoleId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return UserRoleFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<UserRole> GetUserRoleByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetUserRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from UserRole with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserRole>(ds,UserRoleFromDataRow);
		}

		public virtual List<UserRole> GetAllUserRole(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetUserRoleSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from UserRole with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserRole>(ds, UserRoleFromDataRow);
		}

		public virtual List<UserRole> GetPagedUserRole(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetUserRoleCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [UserRoleId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([UserRoleId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [UserRoleId] ";
            tempsql += " FROM [UserRole] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("UserRoleId"))
					tempsql += " , (AllRecords.[UserRoleId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[UserRoleId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetUserRoleSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [UserRole] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [UserRole].[UserRoleId] = PageIndex.[UserRoleId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<UserRole>(ds, UserRoleFromDataRow);
			}else{ return null;}
		}

		private int GetUserRoleCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM UserRole as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM UserRole as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(UserRole))]
		public virtual UserRole InsertUserRole(UserRole entity)
		{

			UserRole other=new UserRole();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into UserRole ( [UserId]
				,[RoleId]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @UserId
				, @RoleId
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@RoleId",entity.RoleId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetUserRole(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(UserRole))]
		public virtual UserRole UpdateUserRole(UserRole entity)
		{

			if (entity.IsTransient()) return entity;
			UserRole other = GetUserRole(entity.UserRoleId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update UserRole set  [UserId]=@UserId
							, [RoleId]=@RoleId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where UserRoleId=@UserRoleId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserId",entity.UserId)
					, new SqlParameter("@RoleId",entity.RoleId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@UserRoleId",entity.UserRoleId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetUserRole(entity.UserRoleId);
		}

		public virtual bool DeleteUserRole(System.Int32 UserRoleId)
		{

			string sql="delete from UserRole with (nolock) where UserRoleId=@UserRoleId";
			SqlParameter parameter=new SqlParameter("@UserRoleId",UserRoleId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(UserRole))]
		public virtual UserRole DeleteUserRole(UserRole entity)
		{
			this.DeleteUserRole(entity.UserRoleId);
			return entity;
		}


		public virtual UserRole UserRoleFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			UserRole entity=new UserRole();
			if (dr.Table.Columns.Contains("UserRoleId"))
			{
			entity.UserRoleId = (System.Int32)dr["UserRoleId"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = (System.Int32)dr["UserId"];
			}
			if (dr.Table.Columns.Contains("RoleId"))
			{
			entity.RoleId = (System.Int32)dr["RoleId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			return entity;
		}

	}
	
	
}
