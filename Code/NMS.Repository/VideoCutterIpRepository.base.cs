﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class VideoCutterIpRepositoryBase : Repository, IVideoCutterIpRepositoryBase 
	{
        
        public VideoCutterIpRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("VideoCutterIpId",new SearchColumn(){Name="VideoCutterIpId",Title="VideoCutterIpId",SelectClause="VideoCutterIpId",WhereClause="AllRecords.VideoCutterIpId",DataType="System.Int32",IsForeignColumn=false,PropertyName="VideoCutterIpId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserIP",new SearchColumn(){Name="UserIP",Title="UserIP",SelectClause="UserIP",WhereClause="AllRecords.UserIP",DataType="System.String",IsForeignColumn=false,PropertyName="UserIp",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Host",new SearchColumn(){Name="Host",Title="Host",SelectClause="Host",WhereClause="AllRecords.Host",DataType="System.String",IsForeignColumn=false,PropertyName="Host",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoCutterVersion",new SearchColumn(){Name="VideoCutterVersion",Title="VideoCutterVersion",SelectClause="VideoCutterVersion",WhereClause="AllRecords.VideoCutterVersion",DataType="System.String",IsForeignColumn=false,PropertyName="VideoCutterVersion",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetVideoCutterIpSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetVideoCutterIpBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetVideoCutterIpAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetVideoCutterIpSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[VideoCutterIp].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[VideoCutterIp].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual VideoCutterIp GetVideoCutterIp(System.Int32 VideoCutterIpId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoCutterIpSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [VideoCutterIp] with (nolock)  where VideoCutterIpId=@VideoCutterIpId ";
			SqlParameter parameter=new SqlParameter("@VideoCutterIpId",VideoCutterIpId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return VideoCutterIpFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<VideoCutterIp> GetVideoCutterIpByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoCutterIpSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [VideoCutterIp] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoCutterIp>(ds,VideoCutterIpFromDataRow);
		}

		public virtual List<VideoCutterIp> GetAllVideoCutterIp(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoCutterIpSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [VideoCutterIp] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoCutterIp>(ds, VideoCutterIpFromDataRow);
		}

		public virtual List<VideoCutterIp> GetPagedVideoCutterIp(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetVideoCutterIpCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [VideoCutterIpId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([VideoCutterIpId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [VideoCutterIpId] ";
            tempsql += " FROM [VideoCutterIp] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("VideoCutterIpId"))
					tempsql += " , (AllRecords.[VideoCutterIpId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[VideoCutterIpId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetVideoCutterIpSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [VideoCutterIp] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [VideoCutterIp].[VideoCutterIpId] = PageIndex.[VideoCutterIpId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoCutterIp>(ds, VideoCutterIpFromDataRow);
			}else{ return null;}
		}

		private int GetVideoCutterIpCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM VideoCutterIp as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM VideoCutterIp as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(VideoCutterIp))]
		public virtual VideoCutterIp InsertVideoCutterIp(VideoCutterIp entity)
		{

			VideoCutterIp other=new VideoCutterIp();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into VideoCutterIp ( [UserIP]
				,[Host]
				,[VideoCutterVersion]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @UserIP
				, @Host
				, @VideoCutterVersion
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserIP",entity.UserIp)
					, new SqlParameter("@Host",entity.Host ?? (object)DBNull.Value)
					, new SqlParameter("@VideoCutterVersion",entity.VideoCutterVersion ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetVideoCutterIp(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(VideoCutterIp))]
		public virtual VideoCutterIp UpdateVideoCutterIp(VideoCutterIp entity)
		{

			if (entity.IsTransient()) return entity;
			VideoCutterIp other = GetVideoCutterIp(entity.VideoCutterIpId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update VideoCutterIp set  [UserIP]=@UserIP
							, [Host]=@Host
							, [VideoCutterVersion]=@VideoCutterVersion
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where VideoCutterIpId=@VideoCutterIpId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@UserIP",entity.UserIp)
					, new SqlParameter("@Host",entity.Host ?? (object)DBNull.Value)
					, new SqlParameter("@VideoCutterVersion",entity.VideoCutterVersion ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@VideoCutterIpId",entity.VideoCutterIpId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetVideoCutterIp(entity.VideoCutterIpId);
		}

		public virtual bool DeleteVideoCutterIp(System.Int32 VideoCutterIpId)
		{

			string sql="delete from VideoCutterIp where VideoCutterIpId=@VideoCutterIpId";
			SqlParameter parameter=new SqlParameter("@VideoCutterIpId",VideoCutterIpId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(VideoCutterIp))]
		public virtual VideoCutterIp DeleteVideoCutterIp(VideoCutterIp entity)
		{
			this.DeleteVideoCutterIp(entity.VideoCutterIpId);
			return entity;
		}


		public virtual VideoCutterIp VideoCutterIpFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			VideoCutterIp entity=new VideoCutterIp();
			if (dr.Table.Columns.Contains("VideoCutterIpId"))
			{
			entity.VideoCutterIpId = (System.Int32)dr["VideoCutterIpId"];
			}
			if (dr.Table.Columns.Contains("UserIP"))
			{
			entity.UserIp = dr["UserIP"].ToString();
			}
			if (dr.Table.Columns.Contains("Host"))
			{
			entity.Host = dr["Host"].ToString();
			}
			if (dr.Table.Columns.Contains("VideoCutterVersion"))
			{
			entity.VideoCutterVersion = dr["VideoCutterVersion"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
