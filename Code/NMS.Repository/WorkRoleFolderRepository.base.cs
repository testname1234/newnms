﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.Extensions;

namespace NMS.Repository
{
		
	public abstract partial class WorkRoleFolderRepositoryBase : Repository, IWorkRoleFolderRepositoryBase 
	{
        
        public WorkRoleFolderRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("WorkRoleFolderId",new SearchColumn(){Name="WorkRoleFolderId",Title="WorkRoleFolderId",SelectClause="WorkRoleFolderId",WhereClause="AllRecords.WorkRoleFolderId",DataType="System.Int32",IsForeignColumn=false,PropertyName="WorkRoleFolderId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("WorkRoleId",new SearchColumn(){Name="WorkRoleId",Title="WorkRoleId",SelectClause="WorkRoleId",WhereClause="AllRecords.WorkRoleId",DataType="System.Int32",IsForeignColumn=false,PropertyName="WorkRoleId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FolderId",new SearchColumn(){Name="FolderId",Title="FolderId",SelectClause="FolderId",WhereClause="AllRecords.FolderId",DataType="System.Int32",IsForeignColumn=false,PropertyName="FolderId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowRead",new SearchColumn(){Name="AllowRead",Title="AllowRead",SelectClause="AllowRead",WhereClause="AllRecords.AllowRead",DataType="System.Boolean",IsForeignColumn=false,PropertyName="AllowRead",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowModify",new SearchColumn(){Name="AllowModify",Title="AllowModify",SelectClause="AllowModify",WhereClause="AllRecords.AllowModify",DataType="System.Boolean",IsForeignColumn=false,PropertyName="AllowModify",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowCreate",new SearchColumn(){Name="AllowCreate",Title="AllowCreate",SelectClause="AllowCreate",WhereClause="AllRecords.AllowCreate",DataType="System.Boolean",IsForeignColumn=false,PropertyName="AllowCreate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowCopy",new SearchColumn(){Name="AllowCopy",Title="AllowCopy",SelectClause="AllowCopy",WhereClause="AllRecords.AllowCopy",DataType="System.Boolean",IsForeignColumn=false,PropertyName="AllowCopy",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowMove",new SearchColumn(){Name="AllowMove",Title="AllowMove",SelectClause="AllowMove",WhereClause="AllRecords.AllowMove",DataType="System.Boolean",IsForeignColumn=false,PropertyName="AllowMove",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("AllowDelete",new SearchColumn(){Name="AllowDelete",Title="AllowDelete",SelectClause="AllowDelete",WhereClause="AllRecords.AllowDelete",DataType="System.Boolean",IsForeignColumn=false,PropertyName="AllowDelete",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("MaxColor",new SearchColumn(){Name="MaxColor",Title="MaxColor",SelectClause="MaxColor",WhereClause="AllRecords.MaxColor",DataType="System.Int32",IsForeignColumn=false,PropertyName="MaxColor",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetWorkRoleFolderSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetWorkRoleFolderBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetWorkRoleFolderAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetWorkRoleFolderSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[WorkRoleFolder].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[WorkRoleFolder].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<WorkRoleFolder> GetWorkRoleFolderByFolderId(System.Int32 FolderId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetWorkRoleFolderSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [WorkRoleFolder] with (nolock)  where FolderId=@FolderId  ";
			SqlParameter parameter=new SqlParameter("@FolderId",FolderId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<WorkRoleFolder>(ds,WorkRoleFolderFromDataRow);
		}

		public virtual WorkRoleFolder GetWorkRoleFolder(System.Int32 WorkRoleFolderId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetWorkRoleFolderSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [WorkRoleFolder] with (nolock)  where WorkRoleFolderId=@WorkRoleFolderId ";
			SqlParameter parameter=new SqlParameter("@WorkRoleFolderId",WorkRoleFolderId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return WorkRoleFolderFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<WorkRoleFolder> GetWorkRoleFolderByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetWorkRoleFolderSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [WorkRoleFolder] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<WorkRoleFolder>(ds,WorkRoleFolderFromDataRow);
		}

		public virtual List<WorkRoleFolder> GetAllWorkRoleFolder(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetWorkRoleFolderSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [WorkRoleFolder] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<WorkRoleFolder>(ds, WorkRoleFolderFromDataRow);
		}

		public virtual List<WorkRoleFolder> GetPagedWorkRoleFolder(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetWorkRoleFolderCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [WorkRoleFolderId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([WorkRoleFolderId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [WorkRoleFolderId] ";
            tempsql += " FROM [WorkRoleFolder] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("WorkRoleFolderId"))
					tempsql += " , (AllRecords.[WorkRoleFolderId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[WorkRoleFolderId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetWorkRoleFolderSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [WorkRoleFolder] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [WorkRoleFolder].[WorkRoleFolderId] = PageIndex.[WorkRoleFolderId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<WorkRoleFolder>(ds, WorkRoleFolderFromDataRow);
			}else{ return null;}
		}

		private int GetWorkRoleFolderCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM WorkRoleFolder as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM WorkRoleFolder as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(WorkRoleFolder))]
		public virtual WorkRoleFolder InsertWorkRoleFolder(WorkRoleFolder entity)
		{

			WorkRoleFolder other=new WorkRoleFolder();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into WorkRoleFolder ( [WorkRoleId]
				,[FolderId]
				,[AllowRead]
				,[AllowModify]
				,[AllowCreate]
				,[AllowCopy]
				,[AllowMove]
				,[AllowDelete]
				,[CreationDate]
				,[LastUpdateDate]
				,[MaxColor] )
				Values
				( @WorkRoleId
				, @FolderId
				, @AllowRead
				, @AllowModify
				, @AllowCreate
				, @AllowCopy
				, @AllowMove
				, @AllowDelete
				, @CreationDate
				, @LastUpdateDate
				, @MaxColor );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@WorkRoleId",entity.WorkRoleId)
					, new SqlParameter("@FolderId",entity.FolderId)
					, new SqlParameter("@AllowRead",entity.AllowRead)
					, new SqlParameter("@AllowModify",entity.AllowModify)
					, new SqlParameter("@AllowCreate",entity.AllowCreate)
					, new SqlParameter("@AllowCopy",entity.AllowCopy)
					, new SqlParameter("@AllowMove",entity.AllowMove)
					, new SqlParameter("@AllowDelete",entity.AllowDelete)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@MaxColor",entity.MaxColor)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetWorkRoleFolder(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(WorkRoleFolder))]
		public virtual WorkRoleFolder UpdateWorkRoleFolder(WorkRoleFolder entity)
		{

			if (entity.IsTransient()) return entity;
			WorkRoleFolder other = GetWorkRoleFolder(entity.WorkRoleFolderId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update WorkRoleFolder set  [WorkRoleId]=@WorkRoleId
							, [FolderId]=@FolderId
							, [AllowRead]=@AllowRead
							, [AllowModify]=@AllowModify
							, [AllowCreate]=@AllowCreate
							, [AllowCopy]=@AllowCopy
							, [AllowMove]=@AllowMove
							, [AllowDelete]=@AllowDelete
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [MaxColor]=@MaxColor 
							 where WorkRoleFolderId=@WorkRoleFolderId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@WorkRoleId",entity.WorkRoleId)
					, new SqlParameter("@FolderId",entity.FolderId)
					, new SqlParameter("@AllowRead",entity.AllowRead)
					, new SqlParameter("@AllowModify",entity.AllowModify)
					, new SqlParameter("@AllowCreate",entity.AllowCreate)
					, new SqlParameter("@AllowCopy",entity.AllowCopy)
					, new SqlParameter("@AllowMove",entity.AllowMove)
					, new SqlParameter("@AllowDelete",entity.AllowDelete)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@MaxColor",entity.MaxColor)
					, new SqlParameter("@WorkRoleFolderId",entity.WorkRoleFolderId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetWorkRoleFolder(entity.WorkRoleFolderId);
		}

		public virtual bool DeleteWorkRoleFolder(System.Int32 WorkRoleFolderId)
		{

			string sql="delete from WorkRoleFolder where WorkRoleFolderId=@WorkRoleFolderId";
			SqlParameter parameter=new SqlParameter("@WorkRoleFolderId",WorkRoleFolderId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(WorkRoleFolder))]
		public virtual WorkRoleFolder DeleteWorkRoleFolder(WorkRoleFolder entity)
		{
			this.DeleteWorkRoleFolder(entity.WorkRoleFolderId);
			return entity;
		}


		public virtual WorkRoleFolder WorkRoleFolderFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			WorkRoleFolder entity=new WorkRoleFolder();
			if (dr.Table.Columns.Contains("WorkRoleFolderId"))
			{
			entity.WorkRoleFolderId = (System.Int32)dr["WorkRoleFolderId"];
			}
			if (dr.Table.Columns.Contains("WorkRoleId"))
			{
			entity.WorkRoleId = (System.Int32)dr["WorkRoleId"];
			}
			if (dr.Table.Columns.Contains("FolderId"))
			{
			entity.FolderId = (System.Int32)dr["FolderId"];
			}
			if (dr.Table.Columns.Contains("AllowRead"))
			{
			entity.AllowRead = (System.Boolean)dr["AllowRead"];
			}
			if (dr.Table.Columns.Contains("AllowModify"))
			{
			entity.AllowModify = (System.Boolean)dr["AllowModify"];
			}
			if (dr.Table.Columns.Contains("AllowCreate"))
			{
			entity.AllowCreate = (System.Boolean)dr["AllowCreate"];
			}
			if (dr.Table.Columns.Contains("AllowCopy"))
			{
			entity.AllowCopy = (System.Boolean)dr["AllowCopy"];
			}
			if (dr.Table.Columns.Contains("AllowMove"))
			{
			entity.AllowMove = (System.Boolean)dr["AllowMove"];
			}
			if (dr.Table.Columns.Contains("AllowDelete"))
			{
			entity.AllowDelete = (System.Boolean)dr["AllowDelete"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("MaxColor"))
			{
			entity.MaxColor = (System.Int32)dr["MaxColor"];
			}
			return entity;
		}

	}
	
	
}
