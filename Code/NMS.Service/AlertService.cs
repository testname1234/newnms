﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Alert;
using NMS.Core;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class AlertService : IAlertService 
	{
		private IAlertRepository _iAlertRepository;
        
		public AlertService(IAlertRepository iAlertRepository)
		{
			this._iAlertRepository = iAlertRepository;
		}
        
        public Dictionary<string, string> GetAlertBasicSearchColumns()
        {
            
            return this._iAlertRepository.GetAlertBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetAlertAdvanceSearchColumns()
        {
            
            return this._iAlertRepository.GetAlertAdvanceSearchColumns();
           
        }
        

		public Alert GetAlert(System.Int32 AlertId)
		{
			return _iAlertRepository.GetAlert(AlertId);
		}

		public Alert UpdateAlert(Alert entity)
		{
			return _iAlertRepository.UpdateAlert(entity);
		}

		public bool DeleteAlert(System.Int32 AlertId)
		{
			return _iAlertRepository.DeleteAlert(AlertId);
		}

		public List<Alert> GetAllAlert()
		{
			return _iAlertRepository.GetAllAlert();
		}

        public List<Alert> GetLatestAlerts(DateTime? lastUpdateDate = null, int rowCount = 10)
        {
            return _iAlertRepository.GetLatestAlerts(lastUpdateDate);
        }

		public Alert InsertAlert(Alert entity)
		{
			 return _iAlertRepository.InsertAlert(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 alertid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out alertid))
            {
				Alert alert = _iAlertRepository.GetAlert(alertid);
                if(alert!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(alert);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Alert> alertlist = _iAlertRepository.GetAllAlert();
            if (alertlist != null && alertlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(alertlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Alert alert = new Alert();
                PostOutput output = new PostOutput();
                alert.CopyFrom(Input);
                alert = _iAlertRepository.InsertAlert(alert);
                output.CopyFrom(alert);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Alert alertinput = new Alert();
                Alert alertoutput = new Alert();
                PutOutput output = new PutOutput();
                alertinput.CopyFrom(Input);
                Alert alert = _iAlertRepository.GetAlert(alertinput.AlertId);
                if (alert!=null)
                {
                    alertoutput = _iAlertRepository.UpdateAlert(alertinput);
                    if(alertoutput!=null)
                    {
                        output.CopyFrom(alertoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 alertid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out alertid))
            {
				 bool IsDeleted = _iAlertRepository.DeleteAlert(alertid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
