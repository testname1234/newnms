﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.AssignmentResource;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class AssignmentResourceService : IAssignmentResourceService 
	{
		private IAssignmentResourceRepository _iAssignmentResourceRepository;
        
		public AssignmentResourceService(IAssignmentResourceRepository iAssignmentResourceRepository)
		{
			this._iAssignmentResourceRepository = iAssignmentResourceRepository;
		}
        
        public Dictionary<string, string> GetAssignmentResourceBasicSearchColumns()
        {
            
            return this._iAssignmentResourceRepository.GetAssignmentResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetAssignmentResourceAdvanceSearchColumns()
        {
            
            return this._iAssignmentResourceRepository.GetAssignmentResourceAdvanceSearchColumns();
           
        }
        

		public AssignmentResource GetAssignmentResource(System.Int32 AssignmentResourceId)
		{
			return _iAssignmentResourceRepository.GetAssignmentResource(AssignmentResourceId);
		}

		public AssignmentResource UpdateAssignmentResource(AssignmentResource entity)
		{
			return _iAssignmentResourceRepository.UpdateAssignmentResource(entity);
		}

		public bool DeleteAssignmentResource(System.Int32 AssignmentResourceId)
		{
			return _iAssignmentResourceRepository.DeleteAssignmentResource(AssignmentResourceId);
		}

		public List<AssignmentResource> GetAllAssignmentResource()
		{
			return _iAssignmentResourceRepository.GetAllAssignmentResource();
		}

		public AssignmentResource InsertAssignmentResource(AssignmentResource entity)
		{
			 return _iAssignmentResourceRepository.InsertAssignmentResource(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 assignmentresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out assignmentresourceid))
            {
				AssignmentResource assignmentresource = _iAssignmentResourceRepository.GetAssignmentResource(assignmentresourceid);
                if(assignmentresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(assignmentresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<AssignmentResource> assignmentresourcelist = _iAssignmentResourceRepository.GetAllAssignmentResource();
            if (assignmentresourcelist != null && assignmentresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(assignmentresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                AssignmentResource assignmentresource = new AssignmentResource();
                PostOutput output = new PostOutput();
                assignmentresource.CopyFrom(Input);
                assignmentresource = _iAssignmentResourceRepository.InsertAssignmentResource(assignmentresource);
                output.CopyFrom(assignmentresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                AssignmentResource assignmentresourceinput = new AssignmentResource();
                AssignmentResource assignmentresourceoutput = new AssignmentResource();
                PutOutput output = new PutOutput();
                assignmentresourceinput.CopyFrom(Input);
                AssignmentResource assignmentresource = _iAssignmentResourceRepository.GetAssignmentResource(assignmentresourceinput.AssignmentResourceId);
                if (assignmentresource!=null)
                {
                    assignmentresourceoutput = _iAssignmentResourceRepository.UpdateAssignmentResource(assignmentresourceinput);
                    if(assignmentresourceoutput!=null)
                    {
                        output.CopyFrom(assignmentresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 assignmentresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out assignmentresourceid))
            {
				 bool IsDeleted = _iAssignmentResourceRepository.DeleteAssignmentResource(assignmentresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
