﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Assignment;
using Validation;
using System.Linq;

namespace NMS.Service
{

    public class AssignmentService : IAssignmentService
    {
        private IAssignmentRepository _iAssignmentRepository;

        public AssignmentService(IAssignmentRepository iAssignmentRepository)
        {
            this._iAssignmentRepository = iAssignmentRepository;
        }

        public Dictionary<string, string> GetAssignmentBasicSearchColumns()
        {

            return this._iAssignmentRepository.GetAssignmentBasicSearchColumns();

        }

        public List<SearchColumn> GetAssignmentAdvanceSearchColumns()
        {

            return this._iAssignmentRepository.GetAssignmentAdvanceSearchColumns();

        }


        public Assignment GetAssignment(System.Int32 AssignmentId)
        {
            return _iAssignmentRepository.GetAssignment(AssignmentId);
        }

        public Assignment UpdateAssignment(Assignment entity)
        {
            return _iAssignmentRepository.UpdateAssignment(entity);
        }

        public bool DeleteAssignment(System.Int32 AssignmentId)
        {
            return _iAssignmentRepository.DeleteAssignment(AssignmentId);
        }

        public List<Assignment> GetAllAssignment()
        {
            return _iAssignmentRepository.GetAllAssignment();
        }

        public Assignment InsertAssignment(Assignment entity)
        {
            return _iAssignmentRepository.InsertAssignment(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 assignmentid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out assignmentid))
            {
                Assignment assignment = _iAssignmentRepository.GetAssignment(assignmentid);
                if (assignment != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(assignment);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Assignment> assignmentlist = _iAssignmentRepository.GetAllAssignment();
            if (assignmentlist != null && assignmentlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(assignmentlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Assignment assignment = new Assignment();
                PostOutput output = new PostOutput();
                assignment.CopyFrom(Input);
                assignment = _iAssignmentRepository.InsertAssignment(assignment);
                output.CopyFrom(assignment);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Assignment assignmentinput = new Assignment();
                Assignment assignmentoutput = new Assignment();
                PutOutput output = new PutOutput();
                assignmentinput.CopyFrom(Input);
                Assignment assignment = _iAssignmentRepository.GetAssignment(assignmentinput.AssignmentId);
                if (assignment != null)
                {
                    assignmentoutput = _iAssignmentRepository.UpdateAssignment(assignmentinput);
                    if (assignmentoutput != null)
                    {
                        output.CopyFrom(assignmentoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 assignmentid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out assignmentid))
            {
                bool IsDeleted = _iAssignmentRepository.DeleteAssignment(assignmentid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public Assignment GetAssignmentByKeyValue(string Key, string Value)
        {
            return _iAssignmentRepository.GetAssignmentByKeyValue(Key, Value, Operands.Equal)[0];
        }
    }


}
