﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.IService;


namespace NMS.Service
{
    public class BunchCountService : IBunchCountService
    {
        private IMBunchNewsRepository _iMBunchNewsRepository;
        private IScrapMaxDatesService _iScrapMaxDatesService;
        private IMBunchRepository _iMBunchRepository;
        public string BunchCountScrapperName = "BunchNewsCount";

        public BunchCountService(IMBunchNewsRepository iMBunchNewsRepository, IScrapMaxDatesService iScrapMaxDatesService, IMBunchRepository iMBunchRepository)
        {

            this._iMBunchNewsRepository = iMBunchNewsRepository;
            this._iScrapMaxDatesService = iScrapMaxDatesService;
            this._iMBunchRepository = iMBunchRepository;
        }


        public void PollBunchCount()
        {
            DateTime LastUpdatedDate = GetBunchLastUpdatedDate();
            
            List<MBunchCount> lstBunchIDCount = _iMBunchNewsRepository.GetBunchNewsGroupByBunchId(LastUpdatedDate);

            //now update that in mongo DB
            DateTime MaxUpdatedDateFromResult = lstBunchIDCount.OrderByDescending(x => x.LastUpdateDate).Take(1).ToList<MBunchCount>()[0].LastUpdateDate;
            

            //loop through each object and update

            foreach (MBunchCount item in lstBunchIDCount)
            {
             //   UpdateBunchNewsCount
             //   _iMBunchRepository.UpdateBunchNewsCount(item.BunchId, item.Count);
            }


            //update the maximum updated date in DB
            ScrapMaxDates entity = new ScrapMaxDates { SourceName = BunchCountScrapperName, MaxUpdateDate = MaxUpdatedDateFromResult };
            _iScrapMaxDatesService.UpdateScrapMaxDates(entity);

        }


        public DateTime GetBunchLastUpdatedDate()
        {
            DateTime myDateTime;
            ScrapMaxDates objScrapDate = _iScrapMaxDatesService.GetScrapMaxDatesBySource(BunchCountScrapperName);

            if (objScrapDate != null)
                myDateTime = objScrapDate.MaxUpdateDate;

            else
                myDateTime = DateTime.UtcNow.AddYears(-2);

            return myDateTime;
        }


    }
}
