﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchFilterNews;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class BunchFilterNewsService : IBunchFilterNewsService 
	{
		private IBunchFilterNewsRepository _iBunchFilterNewsRepository;
        
		public BunchFilterNewsService(IBunchFilterNewsRepository iBunchFilterNewsRepository)
		{
			this._iBunchFilterNewsRepository = iBunchFilterNewsRepository;
		}
        
        public Dictionary<string, string> GetBunchFilterNewsBasicSearchColumns()
        {
            
            return this._iBunchFilterNewsRepository.GetBunchFilterNewsBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetBunchFilterNewsAdvanceSearchColumns()
        {
            
            return this._iBunchFilterNewsRepository.GetBunchFilterNewsAdvanceSearchColumns();
           
        }
        



		public BunchFilterNews GetBunchFilterNews(System.Int32 BunchFilterNewsId)
		{
			return _iBunchFilterNewsRepository.GetBunchFilterNews(BunchFilterNewsId);
		}

		public BunchFilterNews UpdateBunchFilterNews(BunchFilterNews entity)
		{
			return _iBunchFilterNewsRepository.UpdateBunchFilterNews(entity);
		}

		public bool DeleteBunchFilterNews(System.Int32 BunchFilterNewsId)
		{
			return _iBunchFilterNewsRepository.DeleteBunchFilterNews(BunchFilterNewsId);
		}

		public List<BunchFilterNews> GetAllBunchFilterNews()
		{
			return _iBunchFilterNewsRepository.GetAllBunchFilterNews();
		}

		public BunchFilterNews InsertBunchFilterNews(BunchFilterNews entity)
		{
			 return _iBunchFilterNewsRepository.InsertBunchFilterNews(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bunchfilternewsid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchfilternewsid))
            {
				BunchFilterNews bunchfilternews = _iBunchFilterNewsRepository.GetBunchFilterNews(bunchfilternewsid);
                if(bunchfilternews!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bunchfilternews);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<BunchFilterNews> bunchfilternewslist = _iBunchFilterNewsRepository.GetAllBunchFilterNews();
            if (bunchfilternewslist != null && bunchfilternewslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bunchfilternewslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                BunchFilterNews bunchfilternews = new BunchFilterNews();
                PostOutput output = new PostOutput();
                bunchfilternews.CopyFrom(Input);
                bunchfilternews = _iBunchFilterNewsRepository.InsertBunchFilterNews(bunchfilternews);
                output.CopyFrom(bunchfilternews);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BunchFilterNews bunchfilternewsinput = new BunchFilterNews();
                BunchFilterNews bunchfilternewsoutput = new BunchFilterNews();
                PutOutput output = new PutOutput();
                bunchfilternewsinput.CopyFrom(Input);
                BunchFilterNews bunchfilternews = _iBunchFilterNewsRepository.GetBunchFilterNews(bunchfilternewsinput.BunchFilterNewsId);
                if (bunchfilternews!=null)
                {
                    bunchfilternewsoutput = _iBunchFilterNewsRepository.UpdateBunchFilterNews(bunchfilternewsinput);
                    if(bunchfilternewsoutput!=null)
                    {
                        output.CopyFrom(bunchfilternewsoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bunchfilternewsid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchfilternewsid))
            {
				 bool IsDeleted = _iBunchFilterNewsRepository.DeleteBunchFilterNews(bunchfilternewsid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
