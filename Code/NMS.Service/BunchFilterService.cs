﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchFilter;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities.Mongo;
using NMS.MongoRepository;
using SolrManager;
using SolrManager.InputEntities;
using SolrNet;
using System.Configuration;

namespace NMS.Service
{

    public class BunchFilterService : IBunchFilterService
    {
        private IBunchFilterRepository _iBunchFilterRepository;
        private IMBunchFilterRepository _iMBunchFilterRepository;

        public BunchFilterService(IBunchFilterRepository iBunchFilterRepository, IMBunchFilterRepository iMBunchFilterRepository)
        {
            this._iBunchFilterRepository = iBunchFilterRepository;
            this._iMBunchFilterRepository = iMBunchFilterRepository;
        }

        public Dictionary<string, string> GetBunchFilterBasicSearchColumns()
        {

            return this._iBunchFilterRepository.GetBunchFilterBasicSearchColumns();

        }

        public List<SearchColumn> GetBunchFilterAdvanceSearchColumns()
        {

            return this._iBunchFilterRepository.GetBunchFilterAdvanceSearchColumns();

        }

        public virtual void InserUpdateIfNotExist(BunchFilter bunchFilter)
        {
            BunchFilter bunchFilterNew = _iBunchFilterRepository.GetByFilterIdAndBunchId(bunchFilter.FilterId, bunchFilter.BunchId);
            if (bunchFilterNew == null)
            {
                _iBunchFilterRepository.InsertBunchFilterNoReturn(bunchFilter);
            }
            else
            {
                _iBunchFilterRepository.UpdateBunchFilterNoReturn(bunchFilter);
            }
        }

        public virtual List<BunchFilter> GetBunchFilterByFilterId(System.Int32 FilterId)
        {
            return _iBunchFilterRepository.GetBunchFilterByFilterId(FilterId);
        }

        public virtual List<BunchFilter> GetBunchFilterByBunchId(System.Int32 BunchId)
        {
            return _iBunchFilterRepository.GetBunchFilterByBunchId(BunchId);
        }

        public virtual BunchFilter GetByFilterIdAndBunchId(System.Int32 FilterId, System.Int32 BunchId)
        {
            return _iBunchFilterRepository.GetByFilterIdAndBunchId(FilterId, BunchId);
        }


        public BunchFilter GetBunchFilter(System.Int32 BunchFilterId)
        {
            return _iBunchFilterRepository.GetBunchFilter(BunchFilterId);
        }

        public BunchFilter UpdateBunchFilter(BunchFilter entity)
        {
            return _iBunchFilterRepository.UpdateBunchFilter(entity);
        }

        public bool DeleteBunchFilter(System.Int32 BunchFilterId)
        {
            return _iBunchFilterRepository.DeleteBunchFilter(BunchFilterId);
        }

        public List<BunchFilter> GetAllBunchFilter()
        {
            return _iBunchFilterRepository.GetAllBunchFilter();
        }

        public BunchFilter InsertBunchFilter(BunchFilter entity)
        {
            return _iBunchFilterRepository.InsertBunchFilter(entity);
        }
        public bool InsertBunchFilter(MBunchFilter entity)
        {
            return _iMBunchFilterRepository.InsertBunchFilter(entity);
        }

        public bool CheckIfBunchFilterExists(MBunchFilter entity)
        {
            return _iMBunchFilterRepository.CheckIfBunchFilterExists(entity);
        }

        public bool DeleteByBunchAndFilterId(MBunchFilter entity)
        {
            return _iMBunchFilterRepository.DeleteByBunchAndFilterId(entity);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bunchfilterid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out bunchfilterid))
            {
                BunchFilter bunchfilter = _iBunchFilterRepository.GetBunchFilter(bunchfilterid);
                if (bunchfilter != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bunchfilter);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<BunchFilter> bunchfilterlist = _iBunchFilterRepository.GetAllBunchFilter();
            if (bunchfilterlist != null && bunchfilterlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bunchfilterlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BunchFilter bunchfilter = new BunchFilter();
                PostOutput output = new PostOutput();
                bunchfilter.CopyFrom(Input);
                bunchfilter = _iBunchFilterRepository.InsertBunchFilter(bunchfilter);
                output.CopyFrom(bunchfilter);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BunchFilter bunchfilterinput = new BunchFilter();
                BunchFilter bunchfilteroutput = new BunchFilter();
                PutOutput output = new PutOutput();
                bunchfilterinput.CopyFrom(Input);
                BunchFilter bunchfilter = _iBunchFilterRepository.GetBunchFilter(bunchfilterinput.BunchFilterId);
                if (bunchfilter != null)
                {
                    bunchfilteroutput = _iBunchFilterRepository.UpdateBunchFilter(bunchfilterinput);
                    if (bunchfilteroutput != null)
                    {
                        output.CopyFrom(bunchfilteroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bunchfilterid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out bunchfilterid))
            {
                bool IsDeleted = _iBunchFilterRepository.DeleteBunchFilter(bunchfilterid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
    }


}
