﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchNews;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;

namespace NMS.Service
{
		
	public class BunchNewsService : IBunchNewsService 
	{
		private IBunchNewsRepository _iBunchNewsRepository;
        private IMBunchNewsRepository _iMBunchNewsRepository;
        
		public BunchNewsService(IBunchNewsRepository iBunchNewsRepository,IMBunchNewsRepository iMBunchNewsRepository)
		{
			this._iBunchNewsRepository = iBunchNewsRepository;
            this._iMBunchNewsRepository = iMBunchNewsRepository;
		}
        
        public Dictionary<string, string> GetBunchNewsBasicSearchColumns()
        {
            
            return this._iBunchNewsRepository.GetBunchNewsBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetBunchNewsAdvanceSearchColumns()
        {
            
            return this._iBunchNewsRepository.GetBunchNewsAdvanceSearchColumns();
           
        }
        

		public virtual List<BunchNews> GetBunchNewsByBunchId(System.Int32? BunchId)
		{
			return _iBunchNewsRepository.GetBunchNewsByBunchId(BunchId);
		}

		public virtual List<BunchNews> GetBunchNewsByNewsId(System.Int32? NewsId)
		{
			return _iBunchNewsRepository.GetBunchNewsByNewsId(NewsId);
		}

		public BunchNews GetBunchNews(System.Int32 BunchNewsId)
		{
			return _iBunchNewsRepository.GetBunchNews(BunchNewsId);
		}

		public BunchNews UpdateBunchNews(BunchNews entity)
		{
			return _iBunchNewsRepository.UpdateBunchNews(entity);
		}

		public bool DeleteBunchNews(System.Int32 BunchNewsId)
		{
			return _iBunchNewsRepository.DeleteBunchNews(BunchNewsId);
		}

		public List<BunchNews> GetAllBunchNews()
		{
			return _iBunchNewsRepository.GetAllBunchNews();
		}

		public BunchNews InsertBunchNews(BunchNews entity)
		{
			 return _iBunchNewsRepository.InsertBunchNews(entity);
		}


        public bool InsertBunchNews(MBunchNews entity)
        {
            return _iMBunchNewsRepository.InsertBunchNews(entity);
        }

        public bool DeleteByBunchIdAndNewsId(string newsId,string bunchId)
        {
            return _iMBunchNewsRepository.DeleteByBunchIdAndNewsId(newsId,bunchId);
        }

        public List<MBunchNews> GetByBunchIdAndNewsId(string newsId, string bunchId)
        {
            return _iMBunchNewsRepository.GetByBunchIdAndNewsId(newsId, bunchId);
        }
        


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bunchnewsid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchnewsid))
            {
				BunchNews bunchnews = _iBunchNewsRepository.GetBunchNews(bunchnewsid);
                if(bunchnews!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bunchnews);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<BunchNews> bunchnewslist = _iBunchNewsRepository.GetAllBunchNews();
            if (bunchnewslist != null && bunchnewslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bunchnewslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                BunchNews bunchnews = new BunchNews();
                PostOutput output = new PostOutput();
                bunchnews.CopyFrom(Input);
                bunchnews = _iBunchNewsRepository.InsertBunchNews(bunchnews);
                output.CopyFrom(bunchnews);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BunchNews bunchnewsinput = new BunchNews();
                BunchNews bunchnewsoutput = new BunchNews();
                PutOutput output = new PutOutput();
                bunchnewsinput.CopyFrom(Input);
                BunchNews bunchnews = _iBunchNewsRepository.GetBunchNews(bunchnewsinput.BunchNewsId);
                if (bunchnews!=null)
                {
                    bunchnewsoutput = _iBunchNewsRepository.UpdateBunchNews(bunchnewsinput);
                    if(bunchnewsoutput!=null)
                    {
                        output.CopyFrom(bunchnewsoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bunchnewsid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchnewsid))
            {
				 bool IsDeleted = _iBunchNewsRepository.DeleteBunchNews(bunchnewsid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
