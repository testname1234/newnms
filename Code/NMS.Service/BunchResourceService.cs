﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchResource;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class BunchResourceService : IBunchResourceService 
	{
		private IBunchResourceRepository _iBunchResourceRepository;
        
		public BunchResourceService(IBunchResourceRepository iBunchResourceRepository)
		{
			this._iBunchResourceRepository = iBunchResourceRepository;
		}
        
        public Dictionary<string, string> GetBunchResourceBasicSearchColumns()
        {
            
            return this._iBunchResourceRepository.GetBunchResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetBunchResourceAdvanceSearchColumns()
        {
            
            return this._iBunchResourceRepository.GetBunchResourceAdvanceSearchColumns();
           
        }
        

		public virtual List<BunchResource> GetBunchResourceByBunchId(System.Int32 BunchId)
		{
			return _iBunchResourceRepository.GetBunchResourceByBunchId(BunchId);
		}

		public virtual List<BunchResource> GetBunchResourceByResourceId(System.Int32 ResourceId)
		{
			return _iBunchResourceRepository.GetBunchResourceByResourceId(ResourceId);
		}

		public BunchResource GetBunchResource(System.Int32 BunchResourceId)
		{
			return _iBunchResourceRepository.GetBunchResource(BunchResourceId);
		}

		public BunchResource UpdateBunchResource(BunchResource entity)
		{
			return _iBunchResourceRepository.UpdateBunchResource(entity);
		}

		public bool DeleteBunchResource(System.Int32 BunchResourceId)
		{
			return _iBunchResourceRepository.DeleteBunchResource(BunchResourceId);
		}

		public List<BunchResource> GetAllBunchResource()
		{
			return _iBunchResourceRepository.GetAllBunchResource();
		}

		public BunchResource InsertBunchResource(BunchResource entity)
		{
			 return _iBunchResourceRepository.InsertBunchResource(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bunchresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchresourceid))
            {
				BunchResource bunchresource = _iBunchResourceRepository.GetBunchResource(bunchresourceid);
                if(bunchresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bunchresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<BunchResource> bunchresourcelist = _iBunchResourceRepository.GetAllBunchResource();
            if (bunchresourcelist != null && bunchresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bunchresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                BunchResource bunchresource = new BunchResource();
                PostOutput output = new PostOutput();
                bunchresource.CopyFrom(Input);
                bunchresource = _iBunchResourceRepository.InsertBunchResource(bunchresource);
                output.CopyFrom(bunchresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BunchResource bunchresourceinput = new BunchResource();
                BunchResource bunchresourceoutput = new BunchResource();
                PutOutput output = new PutOutput();
                bunchresourceinput.CopyFrom(Input);
                BunchResource bunchresource = _iBunchResourceRepository.GetBunchResource(bunchresourceinput.BunchResourceId);
                if (bunchresource!=null)
                {
                    bunchresourceoutput = _iBunchResourceRepository.UpdateBunchResource(bunchresourceinput);
                    if(bunchresourceoutput!=null)
                    {
                        output.CopyFrom(bunchresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bunchresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchresourceid))
            {
				 bool IsDeleted = _iBunchResourceRepository.DeleteBunchResource(bunchresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
