﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Bunch;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Entities.Mongo;
using NMS.Core.DataInterfaces.Mongo;

namespace NMS.Service
{

    public class BunchService : IBunchService
    {
        private IBunchRepository _iBunchRepository;
        private IMBunchRepository _iMBunchRepository;


        public BunchService(IBunchRepository iBunchRepository, IMBunchRepository iMBunchRepository)
        {
            this._iBunchRepository = iBunchRepository;
            this._iMBunchRepository = iMBunchRepository;
        }

        public Dictionary<string, string> GetBunchBasicSearchColumns()
        {

            return this._iBunchRepository.GetBunchBasicSearchColumns();

        }

        public List<SearchColumn> GetBunchAdvanceSearchColumns()
        {

            return this._iBunchRepository.GetBunchAdvanceSearchColumns();

        }


        public Bunch GetBunch(System.Int32 BunchId)
        {
            return _iBunchRepository.GetBunch(BunchId);
        }

        public Bunch GetBunchByGuid(System.String BunchGuid)
        {
            return _iBunchRepository.GetBunchByGuid(BunchGuid);
        }

        public Bunch UpdateBunch(Bunch entity)
        {
            return _iBunchRepository.UpdateBunch(entity);
        }

        public void UpdateBunchNoReturn(Bunch entity)
        {
            _iBunchRepository.UpdateBunchNoReturn(entity);
        }

        public bool DeleteBunch(System.Int32 BunchId)
        {
            return _iBunchRepository.DeleteBunch(BunchId);
        }

        public List<Bunch> GetAllBunch()
        {
            return _iBunchRepository.GetAllBunch();
        }

        public Bunch InsertBunch(Bunch entity)
        {
            return _iBunchRepository.InsertBunch(entity);
        }

        public void InsertBunchWIthPK(Bunch entity)
        {
            _iBunchRepository.InsertBunchWIthPK(entity);
        }

        public MBunch InsertBunch(MBunch entity)
        {
            return _iMBunchRepository.InsertBunch(entity);
        }
        public MBunch GetByGuid(string Guid)
        {
            return _iMBunchRepository.GetByGuid(Guid);
        }
        public bool DeleteByBunchId(string Guid)
        {
            return _iMBunchRepository.DeleteByBunchId(Guid);
        }



        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bunchid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out bunchid))
            {
                Bunch bunch = _iBunchRepository.GetBunch(bunchid);
                if (bunch != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bunch);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Bunch> bunchlist = _iBunchRepository.GetAllBunch();
            if (bunchlist != null && bunchlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bunchlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Bunch bunch = new Bunch();
                PostOutput output = new PostOutput();
                bunch.CopyFrom(Input);
                bunch = _iBunchRepository.InsertBunch(bunch);
                output.CopyFrom(bunch);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Bunch bunchinput = new Bunch();
                Bunch bunchoutput = new Bunch();
                PutOutput output = new PutOutput();
                bunchinput.CopyFrom(Input);
                Bunch bunch = _iBunchRepository.GetBunch(bunchinput.BunchId);
                if (bunch != null)
                {
                    bunchoutput = _iBunchRepository.UpdateBunch(bunchinput);
                    if (bunchoutput != null)
                    {
                        output.CopyFrom(bunchoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bunchid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out bunchid))
            {
                bool IsDeleted = _iBunchRepository.DeleteBunch(bunchid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
    }


}
