﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.BunchTag;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Entities.Mongo;
using NMS.Core.DataInterfaces.Mongo;

namespace NMS.Service
{
		
	public class BunchTagService : IBunchTagService 
	{
		private IBunchTagRepository _iBunchTagRepository;
        private IMBunchTagRepository _iMBunchTagRepository;
        
		public BunchTagService(IBunchTagRepository iBunchTagRepository,IMBunchTagRepository iMBunchTagRepository)
		{
			this._iBunchTagRepository = iBunchTagRepository;
            this._iMBunchTagRepository = iMBunchTagRepository;

		}
        
        public Dictionary<string, string> GetBunchTagBasicSearchColumns()
        {
            
            return this._iBunchTagRepository.GetBunchTagBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetBunchTagAdvanceSearchColumns()
        {
            
            return this._iBunchTagRepository.GetBunchTagAdvanceSearchColumns();
           
        }
        

		public virtual List<BunchTag> GetBunchTagByBunchId(System.Int32 BunchId)
		{
			return _iBunchTagRepository.GetBunchTagByBunchId(BunchId);
		}

		public virtual List<BunchTag> GetBunchTagByTagId(System.Int32 TagId)
		{
			return _iBunchTagRepository.GetBunchTagByTagId(TagId);
		}

        public virtual BunchTag InsertIfNotExists(BunchTag bunchTag)
        {
            BunchTag bunchTagNew = _iBunchTagRepository.GetByBunchIdAndTagId(bunchTag.BunchId, bunchTag.TagId);
            if (bunchTagNew == null)
            {
                bunchTagNew = _iBunchTagRepository.InsertBunchTag(bunchTag);
            }
            else
            {
                bunchTagNew = _iBunchTagRepository.UpdateBunchTag(bunchTag);
            }
            return bunchTagNew;
        }

        public virtual bool InsertIfNotExistsNoReturn(BunchTag bunchTag)
        {
            //BunchTag bunchTagNew = _iBunchTagRepository.GetByBunchIdAndTagId(bunchTag.BunchId, bunchTag.TagId);
            //if (bunchTagNew == null)
            //{
                return _iBunchTagRepository.InsertBunchTagNoReturn(bunchTag);
            //}
            //else
            //{
            //    bunchTagNew = _iBunchTagRepository.UpdateBunchTag(bunchTag);
            //}            
        }

        public virtual BunchTag GetByBunchIdAndTagId(System.Int32 BunchId, System.Int32 TagId)
        {
            return _iBunchTagRepository.GetByBunchIdAndTagId(BunchId, TagId);
        }

		public BunchTag GetBunchTag(System.Int32 BunchTagId)
		{
			return _iBunchTagRepository.GetBunchTag(BunchTagId);
		}

		public BunchTag UpdateBunchTag(BunchTag entity)
		{
			return _iBunchTagRepository.UpdateBunchTag(entity);
		}

		public bool DeleteBunchTag(System.Int32 BunchTagId)
		{
			return _iBunchTagRepository.DeleteBunchTag(BunchTagId);
		}

		public List<BunchTag> GetAllBunchTag()
		{
			return _iBunchTagRepository.GetAllBunchTag();
		}

		public BunchTag InsertBunchTag(BunchTag entity)
		{
			 return _iBunchTagRepository.InsertBunchTag(entity);
		}

        public bool InsertBunchTag(MBunchTag entity)
        {
            return _iMBunchTagRepository.InsertBunchTag(entity);
        }

        public bool CheckIfBunchTagExists(MBunchTag mbunchTag)
        {
            return _iMBunchTagRepository.CheckIfBunchTagExists(mbunchTag);
        }

        public bool DeleteByTagId(string tagId)
        {
            return _iMBunchTagRepository.DeleteByTagId(tagId);
        }
        

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 bunchtagid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchtagid))
            {
				BunchTag bunchtag = _iBunchTagRepository.GetBunchTag(bunchtagid);
                if(bunchtag!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(bunchtag);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<BunchTag> bunchtaglist = _iBunchTagRepository.GetAllBunchTag();
            if (bunchtaglist != null && bunchtaglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(bunchtaglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                BunchTag bunchtag = new BunchTag();
                PostOutput output = new PostOutput();
                bunchtag.CopyFrom(Input);
                bunchtag = _iBunchTagRepository.InsertBunchTag(bunchtag);
                output.CopyFrom(bunchtag);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                BunchTag bunchtaginput = new BunchTag();
                BunchTag bunchtagoutput = new BunchTag();
                PutOutput output = new PutOutput();
                bunchtaginput.CopyFrom(Input);
                BunchTag bunchtag = _iBunchTagRepository.GetBunchTag(bunchtaginput.BunchTagId);
                if (bunchtag!=null)
                {
                    bunchtagoutput = _iBunchTagRepository.UpdateBunchTag(bunchtaginput);
                    if(bunchtagoutput!=null)
                    {
                        output.CopyFrom(bunchtagoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 bunchtagid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out bunchtagid))
            {
				 bool IsDeleted = _iBunchTagRepository.DeleteBunchTag(bunchtagid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
