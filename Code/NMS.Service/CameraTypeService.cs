﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CameraType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class CameraTypeService : ICameraTypeService 
	{
		private ICameraTypeRepository _iCameraTypeRepository;
        
		public CameraTypeService(ICameraTypeRepository iCameraTypeRepository)
		{
			this._iCameraTypeRepository = iCameraTypeRepository;
		}
        
        public Dictionary<string, string> GetCameraTypeBasicSearchColumns()
        {
            
            return this._iCameraTypeRepository.GetCameraTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCameraTypeAdvanceSearchColumns()
        {
            
            return this._iCameraTypeRepository.GetCameraTypeAdvanceSearchColumns();
           
        }
        

		public CameraType GetCameraType(System.Int32 CameraTypeId)
		{
			return _iCameraTypeRepository.GetCameraType(CameraTypeId);
		}

		public CameraType UpdateCameraType(CameraType entity)
		{
			return _iCameraTypeRepository.UpdateCameraType(entity);
		}

		public bool DeleteCameraType(System.Int32 CameraTypeId)
		{
			return _iCameraTypeRepository.DeleteCameraType(CameraTypeId);
		}

		public List<CameraType> GetAllCameraType()
		{
			return _iCameraTypeRepository.GetAllCameraType();
		}

		public CameraType InsertCameraType(CameraType entity)
		{
			 return _iCameraTypeRepository.InsertCameraType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 cameratypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out cameratypeid))
            {
				CameraType cameratype = _iCameraTypeRepository.GetCameraType(cameratypeid);
                if(cameratype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(cameratype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CameraType> cameratypelist = _iCameraTypeRepository.GetAllCameraType();
            if (cameratypelist != null && cameratypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(cameratypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CameraType cameratype = new CameraType();
                PostOutput output = new PostOutput();
                cameratype.CopyFrom(Input);
                cameratype = _iCameraTypeRepository.InsertCameraType(cameratype);
                output.CopyFrom(cameratype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CameraType cameratypeinput = new CameraType();
                CameraType cameratypeoutput = new CameraType();
                PutOutput output = new PutOutput();
                cameratypeinput.CopyFrom(Input);
                CameraType cameratype = _iCameraTypeRepository.GetCameraType(cameratypeinput.CameraTypeId);
                if (cameratype!=null)
                {
                    cameratypeoutput = _iCameraTypeRepository.UpdateCameraType(cameratypeinput);
                    if(cameratypeoutput!=null)
                    {
                        output.CopyFrom(cameratypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 cameratypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out cameratypeid))
            {
				 bool IsDeleted = _iCameraTypeRepository.DeleteCameraType(cameratypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
