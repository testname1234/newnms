﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperFlashTemplateWindow;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class CasperFlashTemplateWindowService : ICasperFlashTemplateWindowService 
	{
		private ICasperFlashTemplateWindowRepository _iCasperFlashTemplateWindowRepository;
        
		public CasperFlashTemplateWindowService(ICasperFlashTemplateWindowRepository iCasperFlashTemplateWindowRepository)
		{
			this._iCasperFlashTemplateWindowRepository = iCasperFlashTemplateWindowRepository;
		}
        
        public Dictionary<string, string> GetCasperFlashTemplateWindowBasicSearchColumns()
        {
            
            return this._iCasperFlashTemplateWindowRepository.GetCasperFlashTemplateWindowBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCasperFlashTemplateWindowAdvanceSearchColumns()
        {
            
            return this._iCasperFlashTemplateWindowRepository.GetCasperFlashTemplateWindowAdvanceSearchColumns();
           
        }
        

		public CasperFlashTemplateWindow GetCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId)
		{
			return _iCasperFlashTemplateWindowRepository.GetCasperFlashTemplateWindow(CasperFlashTemplateWindowId);
		}

		public CasperFlashTemplateWindow UpdateCasperFlashTemplateWindow(CasperFlashTemplateWindow entity)
		{
			return _iCasperFlashTemplateWindowRepository.UpdateCasperFlashTemplateWindow(entity);
		}

		public bool DeleteCasperFlashTemplateWindow(System.Int32 CasperFlashTemplateWindowId)
		{
			return _iCasperFlashTemplateWindowRepository.DeleteCasperFlashTemplateWindow(CasperFlashTemplateWindowId);
		}

		public List<CasperFlashTemplateWindow> GetAllCasperFlashTemplateWindow()
		{
			return _iCasperFlashTemplateWindowRepository.GetAllCasperFlashTemplateWindow();
		}

		public CasperFlashTemplateWindow InsertCasperFlashTemplateWindow(CasperFlashTemplateWindow entity)
		{
			 return _iCasperFlashTemplateWindowRepository.InsertCasperFlashTemplateWindow(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 casperflashtemplatewindowid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out casperflashtemplatewindowid))
            {
				CasperFlashTemplateWindow casperflashtemplatewindow = _iCasperFlashTemplateWindowRepository.GetCasperFlashTemplateWindow(casperflashtemplatewindowid);
                if(casperflashtemplatewindow!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(casperflashtemplatewindow);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CasperFlashTemplateWindow> casperflashtemplatewindowlist = _iCasperFlashTemplateWindowRepository.GetAllCasperFlashTemplateWindow();
            if (casperflashtemplatewindowlist != null && casperflashtemplatewindowlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(casperflashtemplatewindowlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CasperFlashTemplateWindow casperflashtemplatewindow = new CasperFlashTemplateWindow();
                PostOutput output = new PostOutput();
                casperflashtemplatewindow.CopyFrom(Input);
                casperflashtemplatewindow = _iCasperFlashTemplateWindowRepository.InsertCasperFlashTemplateWindow(casperflashtemplatewindow);
                output.CopyFrom(casperflashtemplatewindow);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CasperFlashTemplateWindow casperflashtemplatewindowinput = new CasperFlashTemplateWindow();
                CasperFlashTemplateWindow casperflashtemplatewindowoutput = new CasperFlashTemplateWindow();
                PutOutput output = new PutOutput();
                casperflashtemplatewindowinput.CopyFrom(Input);
                CasperFlashTemplateWindow casperflashtemplatewindow = _iCasperFlashTemplateWindowRepository.GetCasperFlashTemplateWindow(casperflashtemplatewindowinput.CasperFlashTemplateWindowId);
                if (casperflashtemplatewindow!=null)
                {
                    casperflashtemplatewindowoutput = _iCasperFlashTemplateWindowRepository.UpdateCasperFlashTemplateWindow(casperflashtemplatewindowinput);
                    if(casperflashtemplatewindowoutput!=null)
                    {
                        output.CopyFrom(casperflashtemplatewindowoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 casperflashtemplatewindowid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out casperflashtemplatewindowid))
            {
				 bool IsDeleted = _iCasperFlashTemplateWindowRepository.DeleteCasperFlashTemplateWindow(casperflashtemplatewindowid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
