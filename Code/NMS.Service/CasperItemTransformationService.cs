﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core;
using NMS.Core.DataTransfer.CasperItemTransformation;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class CasperItemTransformationService : ICasperItemTransformationService 
	{
		private ICasperItemTransformationRepository _iCasperItemTransformationRepository;
        
		public CasperItemTransformationService(ICasperItemTransformationRepository iCasperItemTransformationRepository)
		{
			this._iCasperItemTransformationRepository = iCasperItemTransformationRepository;
		}
        
        public Dictionary<string, string> GetCasperItemTransformationBasicSearchColumns()
        {
            
            return this._iCasperItemTransformationRepository.GetCasperItemTransformationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCasperItemTransformationAdvanceSearchColumns()
        {
            
            return this._iCasperItemTransformationRepository.GetCasperItemTransformationAdvanceSearchColumns();
           
        }
        

		public CasperItemTransformation GetCasperItemTransformation(System.Int32 CasperItemTransformationId)
		{
			return _iCasperItemTransformationRepository.GetCasperItemTransformation(CasperItemTransformationId);
		}

		public CasperItemTransformation UpdateCasperItemTransformation(CasperItemTransformation entity)
		{
			return _iCasperItemTransformationRepository.UpdateCasperItemTransformation(entity);
		}

		public bool DeleteCasperItemTransformation(System.Int32 CasperItemTransformationId)
		{
			return _iCasperItemTransformationRepository.DeleteCasperItemTransformation(CasperItemTransformationId);
		}

		public List<CasperItemTransformation> GetAllCasperItemTransformation()
		{
			return _iCasperItemTransformationRepository.GetAllCasperItemTransformation();
		}

		public CasperItemTransformation InsertCasperItemTransformation(CasperItemTransformation entity)
		{
			 return _iCasperItemTransformationRepository.InsertCasperItemTransformation(entity);
		}

        public CasperItemTransformation GetCasperItemTransformationByCasperTemplateItemId(System.Int32 casperTemplateItemId)
		{
            return _iCasperItemTransformationRepository.GetCasperItemTransformationByCasperTemplateItemId(casperTemplateItemId);
		}
        

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 casperitemtransformationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out casperitemtransformationid))
            {
				CasperItemTransformation casperitemtransformation = _iCasperItemTransformationRepository.GetCasperItemTransformation(casperitemtransformationid);
                if(casperitemtransformation!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(casperitemtransformation);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CasperItemTransformation> casperitemtransformationlist = _iCasperItemTransformationRepository.GetAllCasperItemTransformation();
            if (casperitemtransformationlist != null && casperitemtransformationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(casperitemtransformationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CasperItemTransformation casperitemtransformation = new CasperItemTransformation();
                PostOutput output = new PostOutput();
                casperitemtransformation.CopyFrom(Input);
                casperitemtransformation = _iCasperItemTransformationRepository.InsertCasperItemTransformation(casperitemtransformation);
                output.CopyFrom(casperitemtransformation);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CasperItemTransformation casperitemtransformationinput = new CasperItemTransformation();
                CasperItemTransformation casperitemtransformationoutput = new CasperItemTransformation();
                PutOutput output = new PutOutput();
                casperitemtransformationinput.CopyFrom(Input);
                CasperItemTransformation casperitemtransformation = _iCasperItemTransformationRepository.GetCasperItemTransformation(casperitemtransformationinput.CasperItemTransformationId);
                if (casperitemtransformation!=null)
                {
                    casperitemtransformationoutput = _iCasperItemTransformationRepository.UpdateCasperItemTransformation(casperitemtransformationinput);
                    if(casperitemtransformationoutput!=null)
                    {
                        output.CopyFrom(casperitemtransformationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 casperitemtransformationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out casperitemtransformationid))
            {
				 bool IsDeleted = _iCasperItemTransformationRepository.DeleteCasperItemTransformation(casperitemtransformationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
