﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core;
using NMS.Core.DataTransfer.CasperTemplateItem;
using Validation;
using System.Linq;
using NMS.Service;

namespace NMS.Service
{
		
	public class CasperTemplateItemService : ICasperTemplateItemService 
	{
		private ICasperTemplateItemRepository _iCasperTemplateItemRepository;
        
		public CasperTemplateItemService(ICasperTemplateItemRepository iCasperTemplateItemRepository)
		{
			this._iCasperTemplateItemRepository = iCasperTemplateItemRepository;
		}
        
        public Dictionary<string, string> GetCasperTemplateItemBasicSearchColumns()
        {
            
            return this._iCasperTemplateItemRepository.GetCasperTemplateItemBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCasperTemplateItemAdvanceSearchColumns()
        {
            
            return this._iCasperTemplateItemRepository.GetCasperTemplateItemAdvanceSearchColumns();
           
        }
        

		public CasperTemplateItem GetCasperTemplateItem(System.Int32 CasperTemplatItemId)
		{
			return _iCasperTemplateItemRepository.GetCasperTemplateItem(CasperTemplatItemId);
		}

        public List<CasperTemplateItem> GetCasperTemplateItemByCasperTemplateId(System.Int32 CasperTemplatId)
		{
            return _iCasperTemplateItemRepository.GetCasperTemplateItemByCasperTemplateId(CasperTemplatId);
		}

		public CasperTemplateItem UpdateCasperTemplateItem(CasperTemplateItem entity)
		{
			return _iCasperTemplateItemRepository.UpdateCasperTemplateItem(entity);
		}

		public bool DeleteCasperTemplateItem(System.Int32 CasperTemplatItemId)
		{
			return _iCasperTemplateItemRepository.DeleteCasperTemplateItem(CasperTemplatItemId);
		}

		public List<CasperTemplateItem> GetAllCasperTemplateItem()
		{
			return _iCasperTemplateItemRepository.GetAllCasperTemplateItem();
		}

		public CasperTemplateItem InsertCasperTemplateItem(CasperTemplateItem entity)
		{
			 return _iCasperTemplateItemRepository.InsertCasperTemplateItem(entity);
		}

        public CasperTemplateItem GetCasperTemplateItemByCasperTemplateIdAndType(System.Int32 casperTemplateId, string type)
		{
            return _iCasperTemplateItemRepository.GetCasperTemplateItemByCasperTemplateIdAndType(casperTemplateId, type);
		}
        public CasperTemplateItem GetCasperTemplateItemByitemType(string type)
		{
            return _iCasperTemplateItemRepository.GetCasperTemplateItemByitemType(type);
		}

        
        
        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 caspertemplatitemid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatitemid))
            {
				CasperTemplateItem caspertemplateitem = _iCasperTemplateItemRepository.GetCasperTemplateItem(caspertemplatitemid);
                if(caspertemplateitem!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(caspertemplateitem);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CasperTemplateItem> caspertemplateitemlist = _iCasperTemplateItemRepository.GetAllCasperTemplateItem();
            if (caspertemplateitemlist != null && caspertemplateitemlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(caspertemplateitemlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CasperTemplateItem caspertemplateitem = new CasperTemplateItem();
                PostOutput output = new PostOutput();
                caspertemplateitem.CopyFrom(Input);
                caspertemplateitem = _iCasperTemplateItemRepository.InsertCasperTemplateItem(caspertemplateitem);
                output.CopyFrom(caspertemplateitem);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CasperTemplateItem caspertemplateiteminput = new CasperTemplateItem();
                CasperTemplateItem caspertemplateitemoutput = new CasperTemplateItem();
                PutOutput output = new PutOutput();
                caspertemplateiteminput.CopyFrom(Input);
                CasperTemplateItem caspertemplateitem = _iCasperTemplateItemRepository.GetCasperTemplateItem(caspertemplateiteminput.CasperTemplatItemId);
                if (caspertemplateitem!=null)
                {
                    caspertemplateitemoutput = _iCasperTemplateItemRepository.UpdateCasperTemplateItem(caspertemplateiteminput);
                    if(caspertemplateitemoutput!=null)
                    {
                        output.CopyFrom(caspertemplateitemoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 caspertemplatitemid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatitemid))
            {
				 bool IsDeleted = _iCasperTemplateItemRepository.DeleteCasperTemplateItem(caspertemplatitemid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
