﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplateKey;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class CasperTemplateKeyService : ICasperTemplateKeyService 
	{
		private ICasperTemplateKeyRepository _iCasperTemplateKeyRepository;
        
		public CasperTemplateKeyService(ICasperTemplateKeyRepository iCasperTemplateKeyRepository)
		{
			this._iCasperTemplateKeyRepository = iCasperTemplateKeyRepository;
		}
        
        public Dictionary<string, string> GetCasperTemplateKeyBasicSearchColumns()
        {
            
            return this._iCasperTemplateKeyRepository.GetCasperTemplateKeyBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCasperTemplateKeyAdvanceSearchColumns()
        {
            
            return this._iCasperTemplateKeyRepository.GetCasperTemplateKeyAdvanceSearchColumns();
           
        }
        

		public CasperTemplateKey GetCasperTemplateKey(System.Int32 CasperTemplateKeyId)
		{
			return _iCasperTemplateKeyRepository.GetCasperTemplateKey(CasperTemplateKeyId);
		}

		public CasperTemplateKey UpdateCasperTemplateKey(CasperTemplateKey entity)
		{
			return _iCasperTemplateKeyRepository.UpdateCasperTemplateKey(entity);
		}

		public bool DeleteCasperTemplateKey(System.Int32 CasperTemplateKeyId)
		{
			return _iCasperTemplateKeyRepository.DeleteCasperTemplateKey(CasperTemplateKeyId);
		}

		public List<CasperTemplateKey> GetAllCasperTemplateKey()
		{
			return _iCasperTemplateKeyRepository.GetAllCasperTemplateKey();
		}

		public CasperTemplateKey InsertCasperTemplateKey(CasperTemplateKey entity)
		{
			 return _iCasperTemplateKeyRepository.InsertCasperTemplateKey(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 caspertemplatekeyid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatekeyid))
            {
				CasperTemplateKey caspertemplatekey = _iCasperTemplateKeyRepository.GetCasperTemplateKey(caspertemplatekeyid);
                if(caspertemplatekey!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(caspertemplatekey);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CasperTemplateKey> caspertemplatekeylist = _iCasperTemplateKeyRepository.GetAllCasperTemplateKey();
            if (caspertemplatekeylist != null && caspertemplatekeylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(caspertemplatekeylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CasperTemplateKey caspertemplatekey = new CasperTemplateKey();
                PostOutput output = new PostOutput();
                caspertemplatekey.CopyFrom(Input);
                caspertemplatekey = _iCasperTemplateKeyRepository.InsertCasperTemplateKey(caspertemplatekey);
                output.CopyFrom(caspertemplatekey);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CasperTemplateKey caspertemplatekeyinput = new CasperTemplateKey();
                CasperTemplateKey caspertemplatekeyoutput = new CasperTemplateKey();
                PutOutput output = new PutOutput();
                caspertemplatekeyinput.CopyFrom(Input);
                CasperTemplateKey caspertemplatekey = _iCasperTemplateKeyRepository.GetCasperTemplateKey(caspertemplatekeyinput.CasperTemplateKeyId);
                if (caspertemplatekey!=null)
                {
                    caspertemplatekeyoutput = _iCasperTemplateKeyRepository.UpdateCasperTemplateKey(caspertemplatekeyinput);
                    if(caspertemplatekeyoutput!=null)
                    {
                        output.CopyFrom(caspertemplatekeyoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 caspertemplatekeyid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatekeyid))
            {
				 bool IsDeleted = _iCasperTemplateKeyRepository.DeleteCasperTemplateKey(caspertemplatekeyid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
