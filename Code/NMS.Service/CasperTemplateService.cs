﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core;
using NMS.Core.DataTransfer.CasperTemplate;
using Validation;
using System.Linq;
using NMS.Service;

namespace NMS.Service
{
		
	public class CasperTemplateService : ICasperTemplateService 
	{
		private ICasperTemplateRepository _iCasperTemplateRepository;
        
		public CasperTemplateService(ICasperTemplateRepository iCasperTemplateRepository)
		{
			this._iCasperTemplateRepository = iCasperTemplateRepository;
		}
        
        public Dictionary<string, string> GetCasperTemplateBasicSearchColumns()
        {
            
            return this._iCasperTemplateRepository.GetCasperTemplateBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCasperTemplateAdvanceSearchColumns()
        {
            
            return this._iCasperTemplateRepository.GetCasperTemplateAdvanceSearchColumns();
           
        }
        

		public CasperTemplate GetCasperTemplate(System.Int32 CasperTemlateId)
		{
			return _iCasperTemplateRepository.GetCasperTemplate(CasperTemlateId);
		}

		public CasperTemplate UpdateCasperTemplate(CasperTemplate entity)
		{
			return _iCasperTemplateRepository.UpdateCasperTemplate(entity);
		}

		public bool DeleteCasperTemplate(System.Int32 CasperTemlateId)
		{
			return _iCasperTemplateRepository.DeleteCasperTemplate(CasperTemlateId);
		}

		public List<CasperTemplate> GetAllCasperTemplate()
		{
			return _iCasperTemplateRepository.GetAllCasperTemplate();
		}

		public CasperTemplate InsertCasperTemplate(CasperTemplate entity)
		{
			 return _iCasperTemplateRepository.InsertCasperTemplate(entity);
		}

        public CasperTemplate GetCasperTemplateByTemplateName(string Template)
		{
            return _iCasperTemplateRepository.GetCasperTemplateByTemplateName(Template);
		}

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 caspertemlateid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemlateid))
            {
				CasperTemplate caspertemplate = _iCasperTemplateRepository.GetCasperTemplate(caspertemlateid);
                if(caspertemplate!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(caspertemplate);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CasperTemplate> caspertemplatelist = _iCasperTemplateRepository.GetAllCasperTemplate();
            if (caspertemplatelist != null && caspertemplatelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(caspertemplatelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CasperTemplate caspertemplate = new CasperTemplate();
                PostOutput output = new PostOutput();
                caspertemplate.CopyFrom(Input);
                caspertemplate = _iCasperTemplateRepository.InsertCasperTemplate(caspertemplate);
                output.CopyFrom(caspertemplate);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CasperTemplate caspertemplateinput = new CasperTemplate();
                CasperTemplate caspertemplateoutput = new CasperTemplate();
                PutOutput output = new PutOutput();
                caspertemplateinput.CopyFrom(Input);
                CasperTemplate caspertemplate = _iCasperTemplateRepository.GetCasperTemplate(caspertemplateinput.CasperTemlateId);
                if (caspertemplate!=null)
                {
                    caspertemplateoutput = _iCasperTemplateRepository.UpdateCasperTemplate(caspertemplateinput);
                    if(caspertemplateoutput!=null)
                    {
                        output.CopyFrom(caspertemplateoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 caspertemlateid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemlateid))
            {
				 bool IsDeleted = _iCasperTemplateRepository.DeleteCasperTemplate(caspertemlateid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
