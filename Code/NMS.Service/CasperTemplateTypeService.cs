﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplateType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class CasperTemplateTypeService : ICasperTemplateTypeService 
	{
		private ICasperTemplateTypeRepository _iCasperTemplateTypeRepository;
        
		public CasperTemplateTypeService(ICasperTemplateTypeRepository iCasperTemplateTypeRepository)
		{
			this._iCasperTemplateTypeRepository = iCasperTemplateTypeRepository;
		}
        
        public Dictionary<string, string> GetCasperTemplateTypeBasicSearchColumns()
        {
            
            return this._iCasperTemplateTypeRepository.GetCasperTemplateTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCasperTemplateTypeAdvanceSearchColumns()
        {
            
            return this._iCasperTemplateTypeRepository.GetCasperTemplateTypeAdvanceSearchColumns();
           
        }
        

		public CasperTemplateType GetCasperTemplateType(System.Int32 CasperTemplateTypeId)
		{
			return _iCasperTemplateTypeRepository.GetCasperTemplateType(CasperTemplateTypeId);
		}

		public CasperTemplateType UpdateCasperTemplateType(CasperTemplateType entity)
		{
			return _iCasperTemplateTypeRepository.UpdateCasperTemplateType(entity);
		}

		public bool DeleteCasperTemplateType(System.Int32 CasperTemplateTypeId)
		{
			return _iCasperTemplateTypeRepository.DeleteCasperTemplateType(CasperTemplateTypeId);
		}

		public List<CasperTemplateType> GetAllCasperTemplateType()
		{
			return _iCasperTemplateTypeRepository.GetAllCasperTemplateType();
		}

		public CasperTemplateType InsertCasperTemplateType(CasperTemplateType entity)
		{
			 return _iCasperTemplateTypeRepository.InsertCasperTemplateType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 caspertemplatetypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatetypeid))
            {
				CasperTemplateType caspertemplatetype = _iCasperTemplateTypeRepository.GetCasperTemplateType(caspertemplatetypeid);
                if(caspertemplatetype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(caspertemplatetype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CasperTemplateType> caspertemplatetypelist = _iCasperTemplateTypeRepository.GetAllCasperTemplateType();
            if (caspertemplatetypelist != null && caspertemplatetypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(caspertemplatetypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CasperTemplateType caspertemplatetype = new CasperTemplateType();
                PostOutput output = new PostOutput();
                caspertemplatetype.CopyFrom(Input);
                caspertemplatetype = _iCasperTemplateTypeRepository.InsertCasperTemplateType(caspertemplatetype);
                output.CopyFrom(caspertemplatetype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CasperTemplateType caspertemplatetypeinput = new CasperTemplateType();
                CasperTemplateType caspertemplatetypeoutput = new CasperTemplateType();
                PutOutput output = new PutOutput();
                caspertemplatetypeinput.CopyFrom(Input);
                CasperTemplateType caspertemplatetype = _iCasperTemplateTypeRepository.GetCasperTemplateType(caspertemplatetypeinput.CasperTemplateTypeId);
                if (caspertemplatetype!=null)
                {
                    caspertemplatetypeoutput = _iCasperTemplateTypeRepository.UpdateCasperTemplateType(caspertemplatetypeinput);
                    if(caspertemplatetypeoutput!=null)
                    {
                        output.CopyFrom(caspertemplatetypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 caspertemplatetypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatetypeid))
            {
				 bool IsDeleted = _iCasperTemplateTypeRepository.DeleteCasperTemplateType(caspertemplatetypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
