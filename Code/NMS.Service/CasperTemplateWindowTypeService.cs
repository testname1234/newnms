﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CasperTemplateWindowType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class CasperTemplateWindowTypeService : ICasperTemplateWindowTypeService 
	{
		private ICasperTemplateWindowTypeRepository _iCasperTemplateWindowTypeRepository;
        
		public CasperTemplateWindowTypeService(ICasperTemplateWindowTypeRepository iCasperTemplateWindowTypeRepository)
		{
			this._iCasperTemplateWindowTypeRepository = iCasperTemplateWindowTypeRepository;
		}
        
        public Dictionary<string, string> GetCasperTemplateWindowTypeBasicSearchColumns()
        {
            
            return this._iCasperTemplateWindowTypeRepository.GetCasperTemplateWindowTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCasperTemplateWindowTypeAdvanceSearchColumns()
        {
            
            return this._iCasperTemplateWindowTypeRepository.GetCasperTemplateWindowTypeAdvanceSearchColumns();
           
        }
        

		public CasperTemplateWindowType GetCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId)
		{
			return _iCasperTemplateWindowTypeRepository.GetCasperTemplateWindowType(CasperTemplateWindowTypeId);
		}

		public CasperTemplateWindowType UpdateCasperTemplateWindowType(CasperTemplateWindowType entity)
		{
			return _iCasperTemplateWindowTypeRepository.UpdateCasperTemplateWindowType(entity);
		}

		public bool DeleteCasperTemplateWindowType(System.Int32 CasperTemplateWindowTypeId)
		{
			return _iCasperTemplateWindowTypeRepository.DeleteCasperTemplateWindowType(CasperTemplateWindowTypeId);
		}

		public List<CasperTemplateWindowType> GetAllCasperTemplateWindowType()
		{
			return _iCasperTemplateWindowTypeRepository.GetAllCasperTemplateWindowType();
		}

		public CasperTemplateWindowType InsertCasperTemplateWindowType(CasperTemplateWindowType entity)
		{
			 return _iCasperTemplateWindowTypeRepository.InsertCasperTemplateWindowType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 caspertemplatewindowtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatewindowtypeid))
            {
				CasperTemplateWindowType caspertemplatewindowtype = _iCasperTemplateWindowTypeRepository.GetCasperTemplateWindowType(caspertemplatewindowtypeid);
                if(caspertemplatewindowtype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(caspertemplatewindowtype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CasperTemplateWindowType> caspertemplatewindowtypelist = _iCasperTemplateWindowTypeRepository.GetAllCasperTemplateWindowType();
            if (caspertemplatewindowtypelist != null && caspertemplatewindowtypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(caspertemplatewindowtypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CasperTemplateWindowType caspertemplatewindowtype = new CasperTemplateWindowType();
                PostOutput output = new PostOutput();
                caspertemplatewindowtype.CopyFrom(Input);
                caspertemplatewindowtype = _iCasperTemplateWindowTypeRepository.InsertCasperTemplateWindowType(caspertemplatewindowtype);
                output.CopyFrom(caspertemplatewindowtype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CasperTemplateWindowType caspertemplatewindowtypeinput = new CasperTemplateWindowType();
                CasperTemplateWindowType caspertemplatewindowtypeoutput = new CasperTemplateWindowType();
                PutOutput output = new PutOutput();
                caspertemplatewindowtypeinput.CopyFrom(Input);
                CasperTemplateWindowType caspertemplatewindowtype = _iCasperTemplateWindowTypeRepository.GetCasperTemplateWindowType(caspertemplatewindowtypeinput.CasperTemplateWindowTypeId);
                if (caspertemplatewindowtype!=null)
                {
                    caspertemplatewindowtypeoutput = _iCasperTemplateWindowTypeRepository.UpdateCasperTemplateWindowType(caspertemplatewindowtypeinput);
                    if(caspertemplatewindowtypeoutput!=null)
                    {
                        output.CopyFrom(caspertemplatewindowtypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 caspertemplatewindowtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspertemplatewindowtypeid))
            {
				 bool IsDeleted = _iCasperTemplateWindowTypeRepository.DeleteCasperTemplateWindowType(caspertemplatewindowtypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
