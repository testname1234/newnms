﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CategoryAlias;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class CategoryAliasService : ICategoryAliasService 
	{
		private ICategoryAliasRepository _iCategoryAliasRepository;
        
		public CategoryAliasService(ICategoryAliasRepository iCategoryAliasRepository)
		{
			this._iCategoryAliasRepository = iCategoryAliasRepository;
		}
        
        public Dictionary<string, string> GetCategoryAliasBasicSearchColumns()
        {
            
            return this._iCategoryAliasRepository.GetCategoryAliasBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCategoryAliasAdvanceSearchColumns()
        {
            
            return this._iCategoryAliasRepository.GetCategoryAliasAdvanceSearchColumns();
           
        }
        

		public virtual List<CategoryAlias> GetCategoryAliasByCategoryId(System.Int32 CategoryId)
		{
			return _iCategoryAliasRepository.GetCategoryAliasByCategoryId(CategoryId);
		}

		public CategoryAlias GetCategoryAlias(System.Int32 CategoryAliasId)
		{
			return _iCategoryAliasRepository.GetCategoryAlias(CategoryAliasId);
		}

		public CategoryAlias UpdateCategoryAlias(CategoryAlias entity)
		{
			return _iCategoryAliasRepository.UpdateCategoryAlias(entity);
		}

		public bool DeleteCategoryAlias(System.Int32 CategoryAliasId)
		{
			return _iCategoryAliasRepository.DeleteCategoryAlias(CategoryAliasId);
		}

		public List<CategoryAlias> GetAllCategoryAlias()
		{
			return _iCategoryAliasRepository.GetAllCategoryAlias();
		}


        public CategoryAlias InsertCategoryAliasIfNotExists(CategoryAlias entity)
        {
            if (!_iCategoryAliasRepository.Exist(entity))
                return _iCategoryAliasRepository.InsertCategoryAlias(entity);
            else
                return _iCategoryAliasRepository.GetByAlias(entity.Alias);
        }

		public CategoryAlias InsertCategoryAlias(CategoryAlias entity)
		{
            
			 return _iCategoryAliasRepository.InsertCategoryAlias(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 categoryaliasid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out categoryaliasid))
            {
				CategoryAlias categoryalias = _iCategoryAliasRepository.GetCategoryAlias(categoryaliasid);
                if(categoryalias!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(categoryalias);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CategoryAlias> categoryaliaslist = _iCategoryAliasRepository.GetAllCategoryAlias();
            if (categoryaliaslist != null && categoryaliaslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(categoryaliaslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CategoryAlias categoryalias = new CategoryAlias();
                PostOutput output = new PostOutput();
                categoryalias.CopyFrom(Input);
                categoryalias = _iCategoryAliasRepository.InsertCategoryAlias(categoryalias);
                output.CopyFrom(categoryalias);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CategoryAlias categoryaliasinput = new CategoryAlias();
                CategoryAlias categoryaliasoutput = new CategoryAlias();
                PutOutput output = new PutOutput();
                categoryaliasinput.CopyFrom(Input);
                CategoryAlias categoryalias = _iCategoryAliasRepository.GetCategoryAlias(categoryaliasinput.CategoryAliasId);
                if (categoryalias!=null)
                {
                    categoryaliasoutput = _iCategoryAliasRepository.UpdateCategoryAlias(categoryaliasinput);
                    if(categoryaliasoutput!=null)
                    {
                        output.CopyFrom(categoryaliasoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 categoryaliasid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out categoryaliasid))
            {
				 bool IsDeleted = _iCategoryAliasRepository.DeleteCategoryAlias(categoryaliasid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
