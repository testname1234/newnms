﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Category;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class CategoryService : ICategoryService
    {
        private ICategoryRepository _iCategoryRepository;

        public CategoryService(ICategoryRepository iCategoryRepository)
        {
            this._iCategoryRepository = iCategoryRepository;
        }

        public Dictionary<string, string> GetCategoryBasicSearchColumns()
        {

            return this._iCategoryRepository.GetCategoryBasicSearchColumns();

        }

        public List<SearchColumn> GetCategoryAdvanceSearchColumns()
        {

            return this._iCategoryRepository.GetCategoryAdvanceSearchColumns();

        }


        public Category GetCategory(System.Int32 CategoryId)
        {
            return _iCategoryRepository.GetCategory(CategoryId);
        }

        public Category UpdateCategory(Category entity)
        {
            return _iCategoryRepository.UpdateCategory(entity);
        }

        public bool DeleteCategory(System.Int32 CategoryId)
        {
            return _iCategoryRepository.DeleteCategory(CategoryId);
        }

        public List<Category> GetAllCategory()
        {
            return _iCategoryRepository.GetAllCategory();
        }


        public Category InsertCategoryIfNotExists(Category entity)
        {
            return _iCategoryRepository.InsertCategoryIfNotExists(entity);
        }

        public Category InsertCategory(Category entity)
        {
            return _iCategoryRepository.InsertCategory(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 categoryid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out categoryid))
            {
                Category category = _iCategoryRepository.GetCategory(categoryid);
                if (category != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(category);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Category> categorylist = _iCategoryRepository.GetAllCategory();
            if (categorylist != null && categorylist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(categorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Category category = new Category();
                PostOutput output = new PostOutput();
                category.CopyFrom(Input);
                category = _iCategoryRepository.InsertCategory(category);
                output.CopyFrom(category);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Category categoryinput = new Category();
                Category categoryoutput = new Category();
                PutOutput output = new PutOutput();
                categoryinput.CopyFrom(Input);
                Category category = _iCategoryRepository.GetCategory(categoryinput.CategoryId);
                if (category != null)
                {
                    categoryoutput = _iCategoryRepository.UpdateCategory(categoryinput);
                    if (categoryoutput != null)
                    {
                        output.CopyFrom(categoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 categoryid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out categoryid))
            {
                bool IsDeleted = _iCategoryRepository.DeleteCategory(categoryid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public List<Category> GetCategoryByTerm(string term)
        {
            return _iCategoryRepository.GetCategoryByTerm(term);
        }

        public List<Category> GetByDate(DateTime LastUpdatedDate)
        {
            return _iCategoryRepository.GetByDate(LastUpdatedDate);
        }

        public Category GetCategoryByName(string term)
        {
            return _iCategoryRepository.GetCategoryByName(term);
        }

      
        public CategoryAlias GetByAlias(string alias)
        {
            return _iCategoryRepository.GetByAlias(alias);
        }
        public Category GetCategoryCreateIfNotExist(Category category)
        {
            return _iCategoryRepository.GetCategoryCreateIfNotExist(category);
        }
    }
	
	
}
