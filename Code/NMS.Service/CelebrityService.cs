﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Celebrity;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Models;
using NMS.Core.Enums;

namespace NMS.Service
{

    public class CelebrityService : ICelebrityService
    {
        private ICelebrityRepository _iCelebrityRepository;
        private ICelebrityStatisticRepository _iCelebrityStatisticRepository;

        public CelebrityService(ICelebrityRepository iCelebrityRepository, ICelebrityStatisticRepository iCelebrityStatisticRepository)
        {
            this._iCelebrityRepository = iCelebrityRepository;
            this._iCelebrityStatisticRepository = iCelebrityStatisticRepository;
        }

        public Dictionary<string, string> GetCelebrityBasicSearchColumns()
        {

            return this._iCelebrityRepository.GetCelebrityBasicSearchColumns();

        }

        public List<SearchColumn> GetCelebrityAdvanceSearchColumns()
        {

            return this._iCelebrityRepository.GetCelebrityAdvanceSearchColumns();

        }


        public virtual List<Celebrity> GetCelebrityByCategoryId(System.Int32 CategoryId)
        {
            return _iCelebrityRepository.GetCelebrityByCategoryId(CategoryId);
        }

        public Celebrity GetCelebrity(System.Int32 CelebrityId)
        {
            return _iCelebrityRepository.GetCelebrity(CelebrityId);
        }

        public Celebrity UpdateCelebrity(Celebrity entity)
        {
            return _iCelebrityRepository.UpdateCelebrity(entity);
        }

        public bool DeleteCelebrity(System.Int32 CelebrityId)
        {
            return _iCelebrityRepository.DeleteCelebrity(CelebrityId);
        }

        public List<Celebrity> GetAllCelebrity()
        {
            return _iCelebrityRepository.GetAllCelebrity();
        }

        public Celebrity InsertCelebrity(Celebrity entity)
        {
            return _iCelebrityRepository.InsertCelebrity(entity);
        }

        public Celebrity GetBySocialMediaAccountUserNameNoCeleb(string userName)
        {
            return _iCelebrityRepository.GetBySocialMediaAccountUserNameNoCeleb(userName);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 celebrityid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out celebrityid))
            {
                Celebrity celebrity = _iCelebrityRepository.GetCelebrity(celebrityid);
                if (celebrity != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(celebrity);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Celebrity> celebritylist = _iCelebrityRepository.GetAllCelebrity();
            if (celebritylist != null && celebritylist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(celebritylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Celebrity celebrity = new Celebrity();
                PostOutput output = new PostOutput();
                celebrity.CopyFrom(Input);
                celebrity = _iCelebrityRepository.InsertCelebrity(celebrity);
                output.CopyFrom(celebrity);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Celebrity celebrityinput = new Celebrity();
                Celebrity celebrityoutput = new Celebrity();
                PutOutput output = new PutOutput();
                celebrityinput.CopyFrom(Input);
                Celebrity celebrity = _iCelebrityRepository.GetCelebrity(celebrityinput.CelebrityId);
                if (celebrity != null)
                {
                    celebrityoutput = _iCelebrityRepository.UpdateCelebrity(celebrityinput);
                    if (celebrityoutput != null)
                    {
                        output.CopyFrom(celebrityoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 celebrityid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out celebrityid))
            {
                bool IsDeleted = _iCelebrityRepository.DeleteCelebrity(celebrityid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public Celebrity GetBySocialMediaAccountUserName(string userName, int socialMediaType)
        {
            return _iCelebrityRepository.GetBySocialMediaAccountUserName(userName, socialMediaType);
        }


        public List<Celebrity> GetCelebrityByTerm(string term)
        {
            return _iCelebrityRepository.GetCelebrityByTerm(term);
        }

        public bool UpdateCelebrityStatistics(NewsStatisticsInput newsStatisticsParam, int celebrityId, NewsStatisticType type)
        {
            Celebrity celebrity = GetCelebrity(celebrityId);
            if (celebrity != null) 
            {
                if (type == NewsStatisticType.AddedToRundown)
                {
                    if (celebrity.AddedToRundown == null)
                        celebrity.AddedToRundown = 0;

                    celebrity.AddedToRundown++;
                }
                else if (type == NewsStatisticType.OnAired)
                {
                    if (celebrity.OnAired == null)
                        celebrity.OnAired = 0;

                    celebrity.OnAired++;
                }
                else if (type == NewsStatisticType.ExecutedOnOtherChannels)
                {
                    if (celebrity.ExecutedOnOtherChannels == null)
                        celebrity.ExecutedOnOtherChannels = 0;

                    celebrity.ExecutedOnOtherChannels++;
                }

                _iCelebrityRepository.UpdateCelebrityStatistics(celebrity);

                CelebrityStatistic statistic = new CelebrityStatistic();
                statistic.CelebrityId = celebrity.CelebrityId;
                statistic.ChannelId = newsStatisticsParam.ChannelId;
                statistic.ChannelName = newsStatisticsParam.ChannelName;
                statistic.ProgramId = newsStatisticsParam.ProgramId;
                statistic.ProgramName = newsStatisticsParam.ProgramName;
                statistic.EpisodeId = newsStatisticsParam.EpisodeId;
                statistic.EpisodeTime = newsStatisticsParam.EpisodeStartTime;
                statistic.StatisticType = (int)type;
                statistic.CreationDate = DateTime.UtcNow;
                statistic.LastUpdatedDate = DateTime.UtcNow;
                statistic.IsActive = true;

                _iCelebrityStatisticRepository.InsertCelebrityStatistic(statistic);

                return true;
            }

            return false;
        }
    }
}
