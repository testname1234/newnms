﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CelebrityStatistic;
using Validation;
using System.Linq;
using NMS.Core.Models;
using NMS.Core.Enums;

namespace NMS.Service
{
		
	public class CelebrityStatisticService : ICelebrityStatisticService 
	{
		private ICelebrityStatisticRepository _iCelebrityStatisticRepository;
        private ICelebrityService _iCelebrityService;
        private ISlotTemplateScreenElementService _iSlotTemplateScreenElementService;

        public CelebrityStatisticService(ICelebrityStatisticRepository iCelebrityStatisticRepository, ICelebrityService iCelebrityService, ISlotTemplateScreenElementService iSlotTemplateScreenElementService)
		{
			this._iCelebrityStatisticRepository = iCelebrityStatisticRepository;
            this._iCelebrityService = iCelebrityService;
            this._iSlotTemplateScreenElementService = iSlotTemplateScreenElementService;
		}
        
        public Dictionary<string, string> GetCelebrityStatisticBasicSearchColumns()
        {
            
            return this._iCelebrityStatisticRepository.GetCelebrityStatisticBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCelebrityStatisticAdvanceSearchColumns()
        {
            
            return this._iCelebrityStatisticRepository.GetCelebrityStatisticAdvanceSearchColumns();
           
        }
        

		public virtual List<CelebrityStatistic> GetCelebrityStatisticByCelebrityId(System.Int32? CelebrityId)
		{
			return _iCelebrityStatisticRepository.GetCelebrityStatisticByCelebrityId(CelebrityId);
		}

		public CelebrityStatistic GetCelebrityStatistic(System.Int32 CelebrityStatisticId)
		{
			return _iCelebrityStatisticRepository.GetCelebrityStatistic(CelebrityStatisticId);
		}

		public CelebrityStatistic UpdateCelebrityStatistic(CelebrityStatistic entity)
		{
			return _iCelebrityStatisticRepository.UpdateCelebrityStatistic(entity);
		}

		public bool DeleteCelebrityStatistic(System.Int32 CelebrityStatisticId)
		{
			return _iCelebrityStatisticRepository.DeleteCelebrityStatistic(CelebrityStatisticId);
		}

		public List<CelebrityStatistic> GetAllCelebrityStatistic()
		{
			return _iCelebrityStatisticRepository.GetAllCelebrityStatistic();
		}

		public CelebrityStatistic InsertCelebrityStatistic(CelebrityStatistic entity)
		{
			 return _iCelebrityStatisticRepository.InsertCelebrityStatistic(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 celebritystatisticid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out celebritystatisticid))
            {
				CelebrityStatistic celebritystatistic = _iCelebrityStatisticRepository.GetCelebrityStatistic(celebritystatisticid);
                if(celebritystatistic!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(celebritystatistic);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CelebrityStatistic> celebritystatisticlist = _iCelebrityStatisticRepository.GetAllCelebrityStatistic();
            if (celebritystatisticlist != null && celebritystatisticlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(celebritystatisticlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CelebrityStatistic celebritystatistic = new CelebrityStatistic();
                PostOutput output = new PostOutput();
                celebritystatistic.CopyFrom(Input);
                celebritystatistic = _iCelebrityStatisticRepository.InsertCelebrityStatistic(celebritystatistic);
                output.CopyFrom(celebritystatistic);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CelebrityStatistic celebritystatisticinput = new CelebrityStatistic();
                CelebrityStatistic celebritystatisticoutput = new CelebrityStatistic();
                PutOutput output = new PutOutput();
                celebritystatisticinput.CopyFrom(Input);
                CelebrityStatistic celebritystatistic = _iCelebrityStatisticRepository.GetCelebrityStatistic(celebritystatisticinput.CelebrityStatisticId);
                if (celebritystatistic!=null)
                {
                    celebritystatisticoutput = _iCelebrityStatisticRepository.UpdateCelebrityStatistic(celebritystatisticinput);
                    if(celebritystatisticoutput!=null)
                    {
                        output.CopyFrom(celebritystatisticoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 celebritystatisticid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out celebritystatisticid))
            {
				 bool IsDeleted = _iCelebrityStatisticRepository.DeleteCelebrityStatistic(celebritystatisticid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public bool DeleteCelebrityStatisticByEpisodeId(System.Int32 episodeId)
         {
             return _iCelebrityStatisticRepository.DeleteCelebrityStatisticByEpisodeId(episodeId);
         }

         public bool UpdateCelebrityStatistic(System.Int32 celebrityId)
         {
             return _iCelebrityStatisticRepository.UpdateCelebrityStatistic(celebrityId);
         }

         public void RefreshCelebrityStatistics(NewsStatisticsInput NewsStatisticsParam)
         {
             List<SlotTemplateScreenElement> slotTemplateScreenElements = _iSlotTemplateScreenElementService.GetSlotTemplateScreenElementByEpisodeId(NewsStatisticsParam.EpisodeId);

             if (slotTemplateScreenElements != null && slotTemplateScreenElements.Count > 0)
             {
                 List<int> celebrityIds = new List<int>();
                 foreach (SlotTemplateScreenElement element in slotTemplateScreenElements)
                 {
                     if (element.ScreenElementId == 8 && element.CelebrityId.HasValue && !celebrityIds.Contains(element.CelebrityId.Value))
                     {
                         celebrityIds.Add(element.CelebrityId.Value);
                     }
                 }

                 if (celebrityIds.Count > 0)
                 {
                     DeleteCelebrityStatisticByEpisodeId(NewsStatisticsParam.EpisodeId);

                     for (int i = 0; i < celebrityIds.Count; i++)
                     {
                         _iCelebrityService.UpdateCelebrityStatistics(NewsStatisticsParam, celebrityIds[i], NewsStatisticType.AddedToRundown);
                         UpdateCelebrityStatistic(celebrityIds[i]);
                     }
                 }
             }
             else
             {
                 DeleteCelebrityStatisticByEpisodeId(NewsStatisticsParam.EpisodeId);
             }
         }
	}
	
	
}
