﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CelebrityVoiceArtist;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class CelebrityVoiceArtistService : ICelebrityVoiceArtistService 
	{
		private ICelebrityVoiceArtistRepository _iCelebrityVoiceArtistRepository;
        
		public CelebrityVoiceArtistService(ICelebrityVoiceArtistRepository iCelebrityVoiceArtistRepository)
		{
			this._iCelebrityVoiceArtistRepository = iCelebrityVoiceArtistRepository;
		}
        
        public Dictionary<string, string> GetCelebrityVoiceArtistBasicSearchColumns()
        {
            
            return this._iCelebrityVoiceArtistRepository.GetCelebrityVoiceArtistBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCelebrityVoiceArtistAdvanceSearchColumns()
        {
            
            return this._iCelebrityVoiceArtistRepository.GetCelebrityVoiceArtistAdvanceSearchColumns();
           
        }
        

		public virtual List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByCelebrityId(System.Int32? CelebrityId)
		{
			return _iCelebrityVoiceArtistRepository.GetCelebrityVoiceArtistByCelebrityId(CelebrityId);
		}

		public virtual List<CelebrityVoiceArtist> GetCelebrityVoiceArtistByVoiceArtistId(System.Int32? VoiceArtistId)
		{
			return _iCelebrityVoiceArtistRepository.GetCelebrityVoiceArtistByVoiceArtistId(VoiceArtistId);
		}

		public CelebrityVoiceArtist GetCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId)
		{
			return _iCelebrityVoiceArtistRepository.GetCelebrityVoiceArtist(CelebrityVoiceArtistId);
		}

		public CelebrityVoiceArtist UpdateCelebrityVoiceArtist(CelebrityVoiceArtist entity)
		{
			return _iCelebrityVoiceArtistRepository.UpdateCelebrityVoiceArtist(entity);
		}

		public bool DeleteCelebrityVoiceArtist(System.Int32 CelebrityVoiceArtistId)
		{
			return _iCelebrityVoiceArtistRepository.DeleteCelebrityVoiceArtist(CelebrityVoiceArtistId);
		}

		public List<CelebrityVoiceArtist> GetAllCelebrityVoiceArtist()
		{
			return _iCelebrityVoiceArtistRepository.GetAllCelebrityVoiceArtist();
		}

		public CelebrityVoiceArtist InsertCelebrityVoiceArtist(CelebrityVoiceArtist entity)
		{
			 return _iCelebrityVoiceArtistRepository.InsertCelebrityVoiceArtist(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 celebrityvoiceartistid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out celebrityvoiceartistid))
            {
				CelebrityVoiceArtist celebrityvoiceartist = _iCelebrityVoiceArtistRepository.GetCelebrityVoiceArtist(celebrityvoiceartistid);
                if(celebrityvoiceartist!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(celebrityvoiceartist);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CelebrityVoiceArtist> celebrityvoiceartistlist = _iCelebrityVoiceArtistRepository.GetAllCelebrityVoiceArtist();
            if (celebrityvoiceartistlist != null && celebrityvoiceartistlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(celebrityvoiceartistlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CelebrityVoiceArtist celebrityvoiceartist = new CelebrityVoiceArtist();
                PostOutput output = new PostOutput();
                celebrityvoiceartist.CopyFrom(Input);
                celebrityvoiceartist = _iCelebrityVoiceArtistRepository.InsertCelebrityVoiceArtist(celebrityvoiceartist);
                output.CopyFrom(celebrityvoiceartist);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CelebrityVoiceArtist celebrityvoiceartistinput = new CelebrityVoiceArtist();
                CelebrityVoiceArtist celebrityvoiceartistoutput = new CelebrityVoiceArtist();
                PutOutput output = new PutOutput();
                celebrityvoiceartistinput.CopyFrom(Input);
                CelebrityVoiceArtist celebrityvoiceartist = _iCelebrityVoiceArtistRepository.GetCelebrityVoiceArtist(celebrityvoiceartistinput.CelebrityVoiceArtistId);
                if (celebrityvoiceartist!=null)
                {
                    celebrityvoiceartistoutput = _iCelebrityVoiceArtistRepository.UpdateCelebrityVoiceArtist(celebrityvoiceartistinput);
                    if(celebrityvoiceartistoutput!=null)
                    {
                        output.CopyFrom(celebrityvoiceartistoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 celebrityvoiceartistid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out celebrityvoiceartistid))
            {
				 bool IsDeleted = _iCelebrityVoiceArtistRepository.DeleteCelebrityVoiceArtist(celebrityvoiceartistid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
