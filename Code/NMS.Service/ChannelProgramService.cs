﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChannelProgram;
using Validation;
using System.Linq;
using NMS.Core;
namespace NMS.Service
{
		
	public class ChannelProgramService : IChannelProgramService 
	{
		private IChannelProgramRepository _iChannelProgramRepository;
        
		public ChannelProgramService(IChannelProgramRepository iChannelProgramRepository)
		{
			this._iChannelProgramRepository = iChannelProgramRepository;
		}
        
        public Dictionary<string, string> GetChannelProgramBasicSearchColumns()
        {
            
            return this._iChannelProgramRepository.GetChannelProgramBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetChannelProgramAdvanceSearchColumns()
        {
            
            return this._iChannelProgramRepository.GetChannelProgramAdvanceSearchColumns();
           
        }
        

		public virtual List<ChannelProgram> GetChannelProgramByChannelId(System.Int32 ChannelId)
		{
			return _iChannelProgramRepository.GetChannelProgramByChannelId(ChannelId);
		}

		public virtual List<ChannelProgram> GetChannelProgramByProgramId(System.Int32 ProgramId)
		{
			return _iChannelProgramRepository.GetChannelProgramByProgramId(ProgramId);
		}

		public ChannelProgram GetChannelProgram(System.Int32 ChannelProgramId)
		{
			return _iChannelProgramRepository.GetChannelProgram(ChannelProgramId);
		}

		public ChannelProgram UpdateChannelProgram(ChannelProgram entity)
		{
			return _iChannelProgramRepository.UpdateChannelProgram(entity);
		}

		public bool DeleteChannelProgram(System.Int32 ChannelProgramId)
		{
			return _iChannelProgramRepository.DeleteChannelProgram(ChannelProgramId);
		}

		public List<ChannelProgram> GetAllChannelProgram()
		{
			return _iChannelProgramRepository.GetAllChannelProgram();
		}

		public ChannelProgram InsertChannelProgram(ChannelProgram entity)
		{
			 return _iChannelProgramRepository.InsertChannelProgram(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 channelprogramid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channelprogramid))
            {
				ChannelProgram channelprogram = _iChannelProgramRepository.GetChannelProgram(channelprogramid);
                if(channelprogram!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(channelprogram);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ChannelProgram> channelprogramlist = _iChannelProgramRepository.GetAllChannelProgram();
            if (channelprogramlist != null && channelprogramlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(channelprogramlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ChannelProgram channelprogram = new ChannelProgram();
                PostOutput output = new PostOutput();
                channelprogram.CopyFrom(Input);
                channelprogram = _iChannelProgramRepository.InsertChannelProgram(channelprogram);
                output.CopyFrom(channelprogram);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ChannelProgram channelprograminput = new ChannelProgram();
                ChannelProgram channelprogramoutput = new ChannelProgram();
                PutOutput output = new PutOutput();
                channelprograminput.CopyFrom(Input);
                ChannelProgram channelprogram = _iChannelProgramRepository.GetChannelProgram(channelprograminput.ChannelProgramId);
                if (channelprogram!=null)
                {
                    channelprogramoutput = _iChannelProgramRepository.UpdateChannelProgram(channelprograminput);
                    if(channelprogramoutput!=null)
                    {
                        output.CopyFrom(channelprogramoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 channelprogramid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channelprogramid))
            {
				 bool IsDeleted = _iChannelProgramRepository.DeleteChannelProgram(channelprogramid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
