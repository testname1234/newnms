﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Channel;
using Validation;
using System.Linq;
using NMS.Core;
using System.IO;
using System.Globalization;
using NMS.Core.Enums;
using MMS.Integration.VMS;

namespace NMS.Service
{

    public class ChannelService : IChannelService
    {
        private IChannelRepository _iChannelRepository;
        private IReelRepository _iReelRepository;
        private IChannelVideoRepository _iChannelVideoRepository;

        public ChannelService(IChannelRepository iChannelRepository, IReelRepository iReelRepository, IChannelVideoRepository iChannelVideoRepository)
        {
            this._iChannelRepository = iChannelRepository;
            this._iReelRepository = iReelRepository;
            this._iChannelVideoRepository = iChannelVideoRepository;
        }

        public Dictionary<string, string> GetChannelBasicSearchColumns()
        {

            return this._iChannelRepository.GetChannelBasicSearchColumns();

        }

        public List<SearchColumn> GetChannelAdvanceSearchColumns()
        {

            return this._iChannelRepository.GetChannelAdvanceSearchColumns();

        }


        public Channel GetChannel(System.Int32 ChannelId)
        {
            return _iChannelRepository.GetChannel(ChannelId);
        }

        public Channel UpdateChannel(Channel entity)
        {
            return _iChannelRepository.UpdateChannel(entity);
        }

        public bool DeleteChannel(System.Int32 ChannelId)
        {
            return _iChannelRepository.DeleteChannel(ChannelId);
        }

        public List<Channel> GetAllChannel()
        {
            return _iChannelRepository.GetAllChannel();
        }

        public Channel InsertChannel(Channel entity)
        {
            return _iChannelRepository.InsertChannel(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 channelid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out channelid))
            {
                Channel channel = _iChannelRepository.GetChannel(channelid);
                if (channel != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(channel);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Channel> channellist = _iChannelRepository.GetAllChannel();
            if (channellist != null && channellist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(channellist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Channel channel = new Channel();
                PostOutput output = new PostOutput();
                channel.CopyFrom(Input);
                channel = _iChannelRepository.InsertChannel(channel);
                output.CopyFrom(channel);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Channel channelinput = new Channel();
                Channel channeloutput = new Channel();
                PutOutput output = new PutOutput();
                channelinput.CopyFrom(Input);
                Channel channel = _iChannelRepository.GetChannel(channelinput.ChannelId);
                if (channel != null)
                {
                    channeloutput = _iChannelRepository.UpdateChannel(channelinput);
                    if (channeloutput != null)
                    {
                        output.CopyFrom(channeloutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 channelid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out channelid))
            {
                bool IsDeleted = _iChannelRepository.DeleteChannel(channelid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public List<Channel> GetChannelsByOtherChannelBit(bool flag)
        {
            return _iChannelRepository.GetChannelsByOtherChannelBit(flag);
        }

        public List<Channel> GetChannelsByUserId(int UserId)
        {
            return _iChannelRepository.GetChannelsByUserId(UserId);
        }

        public int InsertNewReels(int channelId, int programId)
        {
            //return 1;
            Channel channel = _iChannelRepository.GetChannel(channelId);
            //string[] files = Directory.GetFiles(channel.ImportVideosPath, "*.ts");
            int counter = 0;
            //string[] directories = Directory.GetDirectories(channel.ImportVideosPath);

          
                //string[] files = Directory.GetFiles(dir, "*.mp4");
                
                //List<Reel> reels = new List<Reel>();
                //List<ChannelVideo> channelVideos = new List<ChannelVideo>();
                //foreach (var file in files)
                //{
                //    try
                //    {
                //        DateTime startTime = DateTime.ParseExact(file.Substring(file.LastIndexOf('\\') + 1).Replace(".mp4", "").Split('$')[1], "yyyy-MM-dd H-mm-ss", CultureInfo.InvariantCulture);
                //        if (!_iChannelVideoRepository.ChannelVideoExist(channelId, startTime))
                //        {
                //            ChannelVideo cVideo = new ChannelVideo();
                //            cVideo.ChannelId = channelId;
                //            cVideo.From = startTime;
                //            cVideo.PhysicalPath = file;
                //            cVideo.Url = "http:" + file.Replace("\\", "/");
                //            cVideo.IsProcessed = false;
                //            cVideo.ProgramId = programId;
                //            cVideo.CreationDate = DateTime.UtcNow;
                //            cVideo.LastUpdateDate = cVideo.CreationDate;
                //            cVideo.IsActive = true;
                //            channelVideos.Add(cVideo);
                //        }
                //    }
                //    catch (Exception exp)
                //    {
                //        Console.WriteLine(file);
                //        Console.WriteLine(exp.Message);
                //        Console.WriteLine(exp.StackTrace);
                //        throw;
                //    }
                //}

                VMSAPI vmApi = new VMSAPI();
                List<ChannelVideo> channelVideos = new List<ChannelVideo>();
                ChannelVideo channelVideo = _iChannelVideoRepository.GetLastChannelVideo(channel.ChannelId);

                var importedVideos = vmApi.GetReelsAfterThisDate(channelVideo.To.Value, channel.ChannelId);

                if (importedVideos.Data != null && importedVideos.Data.Count > 0)
                {
                    foreach (var chanVideo in importedVideos.Data)
                    {
                        ChannelVideo cVideo = new ChannelVideo();
                        cVideo.ChannelId = channel.ChannelId;
                        cVideo.From = chanVideo.StartTime;
                        cVideo.To = chanVideo.EndTime;
                        cVideo.PhysicalPath = chanVideo.Path;
                        cVideo.Url = "http:" + chanVideo.Path.Replace("\\", "/");
                        cVideo.IsProcessed = false;
                        cVideo.ProgramId = programId;
                        cVideo.CreationDate = DateTime.UtcNow;
                        cVideo.LastUpdateDate = cVideo.CreationDate;
                        cVideo.IsActive = true;
                        channelVideos.Add(cVideo);
                    }
                }

                channelVideos = channelVideos.OrderBy(x => x.From).Take(channelVideos.Count).ToList();
                foreach (var cVideo in channelVideos)
                {
                    //FileInfo info = new FileInfo(cVideo.PhysicalPath);
                    //if (info.Length > 0)
                    //{

                    //    int duration = (int)Math.Floor(FFMPEGLib.FFMPEG.GetDuration(cVideo.PhysicalPath));

                        ChannelVideo alreadyExisit = _iChannelVideoRepository.GetChannelVideoByStartEndTime(cVideo.From.Value, cVideo.To.Value, cVideo.ChannelId.Value);
                        if (alreadyExisit == null)
                        {
                            counter++;
                            _iChannelVideoRepository.InsertChannelVideo(cVideo);
                        }
                    //}
                }
            return counter;
        }
    }


}
