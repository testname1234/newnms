﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChannelVideo;
using Validation;
using System.Linq;
using NMS.Core;
using System.IO;
using FFMPEGLib;

namespace NMS.Service
{
		
	public class ChannelVideoService : IChannelVideoService 
	{
		private IChannelVideoRepository _iChannelVideoRepository;
        
		public ChannelVideoService(IChannelVideoRepository iChannelVideoRepository)
		{
			this._iChannelVideoRepository = iChannelVideoRepository;
		}
        
        public Dictionary<string, string> GetChannelVideoBasicSearchColumns()
        {
            
            return this._iChannelVideoRepository.GetChannelVideoBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetChannelVideoAdvanceSearchColumns()
        {
            
            return this._iChannelVideoRepository.GetChannelVideoAdvanceSearchColumns();
           
        }
        

		public ChannelVideo GetChannelVideo(System.Int32 ChannelVideoId)
		{
			return _iChannelVideoRepository.GetChannelVideo(ChannelVideoId);
		}

       

		public ChannelVideo UpdateChannelVideo(ChannelVideo entity)
		{
			return _iChannelVideoRepository.UpdateChannelVideo(entity);
		}

		public bool DeleteChannelVideo(System.Int32 ChannelVideoId)
		{
			return _iChannelVideoRepository.DeleteChannelVideo(ChannelVideoId);
		}

		public List<ChannelVideo> GetAllChannelVideo()
		{
			return _iChannelVideoRepository.GetAllChannelVideo();
		}

		public ChannelVideo InsertChannelVideo(ChannelVideo entity)
		{
			 return _iChannelVideoRepository.InsertChannelVideo(entity);
		}

        public ChannelVideo GetLastChannelVideo(int ChannelId)
        {
            return _iChannelVideoRepository.GetLastChannelVideo(ChannelId);
        }

        public List<ChannelVideo> GetDistinctPrograms()
        {
            return _iChannelVideoRepository.GetDistinctPrograms();
        }

        public double GetDuration(string filename)
        {
            double ret = 0;
            try
            {
                ret = FFMPEGLib.FFMPEG.GetDuration(filename);
                //var player = new WindowsMediaPlayer();
                //var clip = player.newMedia(filename);
                //ret = TimeSpan.FromSeconds(clip.duration).TotalSeconds;
                return ret;
            }
            catch (Exception ex)
            {
                var Errors = new string[1];
                Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
                return ret;
            }
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 channelvideoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channelvideoid))
            {
				ChannelVideo channelvideo = _iChannelVideoRepository.GetChannelVideo(channelvideoid);
                if(channelvideo!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(channelvideo);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ChannelVideo> channelvideolist = _iChannelVideoRepository.GetAllChannelVideo();
            if (channelvideolist != null && channelvideolist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(channelvideolist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ChannelVideo channelvideo = new ChannelVideo();
                PostOutput output = new PostOutput();
                channelvideo.CopyFrom(Input);
                channelvideo = _iChannelVideoRepository.InsertChannelVideo(channelvideo);
                output.CopyFrom(channelvideo);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ChannelVideo channelvideoinput = new ChannelVideo();
                ChannelVideo channelvideooutput = new ChannelVideo();
                PutOutput output = new PutOutput();
                channelvideoinput.CopyFrom(Input);
                ChannelVideo channelvideo = _iChannelVideoRepository.GetChannelVideo(channelvideoinput.ChannelVideoId);
                if (channelvideo!=null)
                {
                    channelvideooutput = _iChannelVideoRepository.UpdateChannelVideo(channelvideoinput);
                    if(channelvideooutput!=null)
                    {
                        output.CopyFrom(channelvideooutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 channelvideoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channelvideoid))
            {
				 bool IsDeleted = _iChannelVideoRepository.DeleteChannelVideo(channelvideoid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<ChannelVideo> GetAllChannelVideosBetweenDates(DateTime dtFrom, DateTime dtTo)
         {
             return _iChannelVideoRepository.GetAllChannelVideosBetweenDates(dtFrom, dtTo);
         }

        public List<ChannelVideo> GetAllChannelLiveTCS(DateTime dtFrom, DateTime dtTo)
        {
            return CacheManager.ChannelVideosStoreByDateTime(dtFrom);
        }


        public void MarkIsProcessed(int id)
         {
             _iChannelVideoRepository.MarkIsProcessed(id);
         }
    }
	
	
}
