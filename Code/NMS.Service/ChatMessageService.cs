﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChatMessage;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ChatMessageService : IChatMessageService 
	{
		private IChatMessageRepository _iChatMessageRepository;
        
		public ChatMessageService(IChatMessageRepository iChatMessageRepository)
		{
			this._iChatMessageRepository = iChatMessageRepository;
		}
        
        public Dictionary<string, string> GetChatMessageBasicSearchColumns()
        {
            
            return this._iChatMessageRepository.GetChatMessageBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetChatMessageAdvanceSearchColumns()
        {
            
            return this._iChatMessageRepository.GetChatMessageAdvanceSearchColumns();
           
        }
        

		public ChatMessage GetChatMessage(System.Int32 MessageId)
		{
			return _iChatMessageRepository.GetChatMessage(MessageId);
		}

		public ChatMessage UpdateChatMessage(ChatMessage entity)
		{
			return _iChatMessageRepository.UpdateChatMessage(entity);
		}

		public bool DeleteChatMessage(System.Int32 MessageId)
		{
			return _iChatMessageRepository.DeleteChatMessage(MessageId);
		}

		public List<ChatMessage> GetAllChatMessage()
		{
			return _iChatMessageRepository.GetAllChatMessage();
		}

		public ChatMessage InsertChatMessage(ChatMessage entity)
		{
			 return _iChatMessageRepository.InsertChatMessage(entity);
		}

        public void InsertChatMessage(int from, int to, string message, DateTime dateTime, bool isRecieved, bool? isSentToGroup=null)
        {
            ChatMessage chat = new ChatMessage();
            chat.From = from;
            chat.To = to;
            chat.Message = message;
            chat.CreationDate = dateTime;
            chat.IsRecieved = isRecieved;
            chat.IsSentToGroup = isSentToGroup;
            InsertChatMessage(chat);
        }

        public List<ChatMessage> GetChatMessagesForUser(int userId)
        {
            DateTime dt = DateTime.Now.AddHours(-24);
            return _iChatMessageRepository.GetChatMessagesForUser(userId, dt);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 messageid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out messageid))
            {
				ChatMessage chatmessage = _iChatMessageRepository.GetChatMessage(messageid);
                if(chatmessage!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(chatmessage);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ChatMessage> chatmessagelist = _iChatMessageRepository.GetAllChatMessage();
            if (chatmessagelist != null && chatmessagelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(chatmessagelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ChatMessage chatmessage = new ChatMessage();
                PostOutput output = new PostOutput();
                chatmessage.CopyFrom(Input);
                chatmessage = _iChatMessageRepository.InsertChatMessage(chatmessage);
                output.CopyFrom(chatmessage);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ChatMessage chatmessageinput = new ChatMessage();
                ChatMessage chatmessageoutput = new ChatMessage();
                PutOutput output = new PutOutput();
                chatmessageinput.CopyFrom(Input);
                ChatMessage chatmessage = _iChatMessageRepository.GetChatMessage(chatmessageinput.MessageId);
                if (chatmessage!=null)
                {
                    chatmessageoutput = _iChatMessageRepository.UpdateChatMessage(chatmessageinput);
                    if(chatmessageoutput!=null)
                    {
                        output.CopyFrom(chatmessageoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 messageid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out messageid))
            {
				 bool IsDeleted = _iChatMessageRepository.DeleteChatMessage(messageid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public List<ChatMessage> GetAllGroupMessages()
         {
             DateTime dt = DateTime.Now.AddHours(-24);
             return _iChatMessageRepository.GetAllGroupMessages(dt);
         }
	}
	
	
}
