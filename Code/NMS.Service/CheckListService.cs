﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.CheckList;
using Validation;
using System.Linq;

namespace NMS.Core.Service
{
		
	public class CheckListService : ICheckListService 
	{
		private ICheckListRepository _iCheckListRepository;

        public List<CheckList> GetNameErrorMessageCheckList()
        {
            return this._iCheckListRepository.GetNameErrorMessageCheckList();
        }

		public CheckListService(ICheckListRepository iCheckListRepository)
		{
			this._iCheckListRepository = iCheckListRepository;
		}
        
        public Dictionary<string, string> GetCheckListBasicSearchColumns()
        {
            
            return this._iCheckListRepository.GetCheckListBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetCheckListAdvanceSearchColumns()
        {
            
            return this._iCheckListRepository.GetCheckListAdvanceSearchColumns();
           
        }
        

		public CheckList GetCheckList(System.Int32 ChecklistId)
		{
			return _iCheckListRepository.GetCheckList(ChecklistId);
		}

        public void UpdatePriority(string Name, bool Priority)
        {
            _iCheckListRepository.UpdatePriority(Name, Priority);
        }

        public List<CheckList> GetPriority()
        {
            return _iCheckListRepository.GetPriority();
        }

        public int Selection(string Name)
        {
            return _iCheckListRepository.Selection(Name);
        }

        public CheckList InsertResult(CheckList entity)
        {
            return _iCheckListRepository.InsertResult(entity);
        }

        public CheckList UpdateResult(CheckList entity)
        {
            return _iCheckListRepository.UpdateResult(entity);
        }

		public CheckList UpdateCheckList(CheckList entity)
		{
			return _iCheckListRepository.UpdateCheckList(entity);
		}

		public bool DeleteCheckList(System.Int32 ChecklistId)
		{
			return _iCheckListRepository.DeleteCheckList(ChecklistId);
		}

		public List<CheckList> GetAllCheckList()
		{
			return _iCheckListRepository.GetAllCheckList();
		}

		public CheckList InsertCheckList(CheckList entity)
		{
			 return _iCheckListRepository.InsertCheckList(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 checklistid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out checklistid))
            {
				CheckList checklist = _iCheckListRepository.GetCheckList(checklistid);
                if(checklist!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(checklist);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<CheckList> checklistlist = _iCheckListRepository.GetAllCheckList();
            if (checklistlist != null && checklistlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(checklistlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                CheckList checklist = new CheckList();
                PostOutput output = new PostOutput();
                checklist.CopyFrom(Input);
                checklist = _iCheckListRepository.InsertCheckList(checklist);
                output.CopyFrom(checklist);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                CheckList checklistinput = new CheckList();
                CheckList checklistoutput = new CheckList();
                PutOutput output = new PutOutput();
                checklistinput.CopyFrom(Input);
                CheckList checklist = _iCheckListRepository.GetCheckList(checklistinput.ChecklistId);
                if (checklist!=null)
                {
                    checklistoutput = _iCheckListRepository.UpdateCheckList(checklistinput);
                    if(checklistoutput!=null)
                    {
                        output.CopyFrom(checklistoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 checklistid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out checklistid))
            {
				 bool IsDeleted = _iCheckListRepository.DeleteCheckList(checklistid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
