﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Comment;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Entities.Mongo;
using NMS.Core.DataInterfaces.Mongo;

namespace NMS.Service
{

    public class CommentService : ICommentService
    {
        private ICommentRepository _iCommentRepository;
        private IMCommentRepository _iMCommentRepository;

        public CommentService(ICommentRepository iCommentRepository, IMCommentRepository iMCommentRepository)
        {
            this._iCommentRepository = iCommentRepository;
            this._iMCommentRepository = iMCommentRepository;
        }

        public Dictionary<string, string> GetCommentBasicSearchColumns()
        {

            return this._iCommentRepository.GetCommentBasicSearchColumns();

        }

        public List<SearchColumn> GetCommentAdvanceSearchColumns()
        {

            return this._iCommentRepository.GetCommentAdvanceSearchColumns();

        }



        public virtual Comment InsertIfNotExists(Comment comment)
        {
            Comment commentNew = _iCommentRepository.GetByGuid(comment.Guid);
            if (commentNew == null)
            {
                commentNew = _iCommentRepository.InsertComment(comment);
            }
            else
            {
                commentNew = _iCommentRepository.UpdateComment(comment);
            }
            return commentNew;
        }


        public virtual Comment GetByGuid(System.String Guid)
        {
            return _iCommentRepository.GetByGuid(Guid);
        }

        public virtual Comment GetByNewsGuid(System.String NewsGuid)
        {
            return _iCommentRepository.GetByNewsGuid(NewsGuid);
        }

        public Comment GetComment(System.Int32 Commentid)
        {
            return _iCommentRepository.GetComment(Commentid);
        }

        public Comment UpdateComment(Comment entity)
        {
            return _iCommentRepository.UpdateComment(entity);
        }

        public bool DeleteComment(System.Int32 Commentid)
        {
            return _iCommentRepository.DeleteComment(Commentid);
        }

        public List<Comment> GetAllComment()
        {
            return _iCommentRepository.GetAllComment();
        }

        public Comment InsertComment(Comment entity)
        {
            return _iCommentRepository.InsertComment(entity);
        }

        public bool InsertComment(MComment entity)
        {
            return _iMCommentRepository.InsertComment(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 commentid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out commentid))
            {
                Comment comment = _iCommentRepository.GetComment(commentid);
                if (comment != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(comment);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Comment> commentlist = _iCommentRepository.GetAllComment();
            if (commentlist != null && commentlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(commentlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Comment comment = new Comment();
                PostOutput output = new PostOutput();
                comment.CopyFrom(Input);
                comment = _iCommentRepository.InsertComment(comment);
                output.CopyFrom(comment);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }


        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 commentid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out commentid))
            {
                bool IsDeleted = _iCommentRepository.DeleteComment(commentid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
    }


}
