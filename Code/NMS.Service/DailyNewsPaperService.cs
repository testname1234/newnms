﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.DailyNewsPaper;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class DailyNewsPaperService : IDailyNewsPaperService 
	{
		private IDailyNewsPaperRepository _iDailyNewsPaperRepository;
        
		public DailyNewsPaperService(IDailyNewsPaperRepository iDailyNewsPaperRepository)
		{
			this._iDailyNewsPaperRepository = iDailyNewsPaperRepository;
		}
        
        public Dictionary<string, string> GetDailyNewsPaperBasicSearchColumns()
        {
            
            return this._iDailyNewsPaperRepository.GetDailyNewsPaperBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetDailyNewsPaperAdvanceSearchColumns()
        {
            
            return this._iDailyNewsPaperRepository.GetDailyNewsPaperAdvanceSearchColumns();
           
        }
        

		public virtual List<DailyNewsPaper> GetDailyNewsPaperByNewsPaperId(System.Int32 NewsPaperId)
		{
			return _iDailyNewsPaperRepository.GetDailyNewsPaperByNewsPaperId(NewsPaperId);
		}

		public DailyNewsPaper GetDailyNewsPaper(System.Int32 DailyNewsPaperId)
		{
			return _iDailyNewsPaperRepository.GetDailyNewsPaper(DailyNewsPaperId);
		}

		public DailyNewsPaper UpdateDailyNewsPaper(DailyNewsPaper entity)
		{
			return _iDailyNewsPaperRepository.UpdateDailyNewsPaper(entity);
		}

		public bool DeleteDailyNewsPaper(System.Int32 DailyNewsPaperId)
		{
			return _iDailyNewsPaperRepository.DeleteDailyNewsPaper(DailyNewsPaperId);
		}

		public List<DailyNewsPaper> GetAllDailyNewsPaper()
		{
			return _iDailyNewsPaperRepository.GetAllDailyNewsPaper();
		}

		public DailyNewsPaper InsertDailyNewsPaper(DailyNewsPaper entity)
		{
			 return _iDailyNewsPaperRepository.InsertDailyNewsPaper(entity);
		}

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 dailynewspaperid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out dailynewspaperid))
            {
				DailyNewsPaper dailynewspaper = _iDailyNewsPaperRepository.GetDailyNewsPaper(dailynewspaperid);
                if(dailynewspaper!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(dailynewspaper);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<DailyNewsPaper> dailynewspaperlist = _iDailyNewsPaperRepository.GetAllDailyNewsPaper();
            if (dailynewspaperlist != null && dailynewspaperlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(dailynewspaperlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                DailyNewsPaper dailynewspaper = new DailyNewsPaper();
                PostOutput output = new PostOutput();
                dailynewspaper.CopyFrom(Input);
                dailynewspaper = _iDailyNewsPaperRepository.InsertDailyNewsPaper(dailynewspaper);
                output.CopyFrom(dailynewspaper);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                DailyNewsPaper dailynewspaperinput = new DailyNewsPaper();
                DailyNewsPaper dailynewspaperoutput = new DailyNewsPaper();
                PutOutput output = new PutOutput();
                dailynewspaperinput.CopyFrom(Input);
                DailyNewsPaper dailynewspaper = _iDailyNewsPaperRepository.GetDailyNewsPaper(dailynewspaperinput.DailyNewsPaperId);
                if (dailynewspaper!=null)
                {
                    dailynewspaperoutput = _iDailyNewsPaperRepository.UpdateDailyNewsPaper(dailynewspaperinput);
                    if(dailynewspaperoutput!=null)
                    {
                        output.CopyFrom(dailynewspaperoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 dailynewspaperid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out dailynewspaperid))
            {
				 bool IsDeleted = _iDailyNewsPaperRepository.DeleteDailyNewsPaper(dailynewspaperid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<DailyNewsPaper> GetDailyNewsPaperByDate(DateTime fromDate, DateTime toDate)
         {
             return _iDailyNewsPaperRepository.GetDailyNewsPaperByDate(fromDate, toDate);
         }

         public List<DailyNewsPaper> GeByDateAndNewsPaperId(DateTime fromDate, DateTime toDate, System.Int32 NewsPaperId)
         {
             return _iDailyNewsPaperRepository.GeByDateAndNewsPaperId(fromDate, toDate, NewsPaperId);
         }
    }
	
	
}
