﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.DescrepencyNewsFile;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class DescrepencyNewsFileService : IDescrepencyNewsFileService 
	{
		private IDescrepencyNewsFileRepository _iDescrepencyNewsFileRepository;
        
		public DescrepencyNewsFileService(IDescrepencyNewsFileRepository iDescrepencyNewsFileRepository)
		{
			this._iDescrepencyNewsFileRepository = iDescrepencyNewsFileRepository;
		}
        
        public Dictionary<string, string> GetDescrepencyNewsFileBasicSearchColumns()
        {
            
            return this._iDescrepencyNewsFileRepository.GetDescrepencyNewsFileBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetDescrepencyNewsFileAdvanceSearchColumns()
        {
            
            return this._iDescrepencyNewsFileRepository.GetDescrepencyNewsFileAdvanceSearchColumns();
           
        }
        

		public DescrepencyNewsFile GetDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId)
		{
			return _iDescrepencyNewsFileRepository.GetDescrepencyNewsFile(DescrepencyNewsFileId);
		}

		public DescrepencyNewsFile UpdateDescrepencyNewsFile(DescrepencyNewsFile entity)
		{
			return _iDescrepencyNewsFileRepository.UpdateDescrepencyNewsFile(entity);
		}

		public bool DeleteDescrepencyNewsFile(System.Int32 DescrepencyNewsFileId)
		{
			return _iDescrepencyNewsFileRepository.DeleteDescrepencyNewsFile(DescrepencyNewsFileId);
		}

		public List<DescrepencyNewsFile> GetAllDescrepencyNewsFile()
		{
			return _iDescrepencyNewsFileRepository.GetAllDescrepencyNewsFile();
		}

		public DescrepencyNewsFile InsertDescrepencyNewsFile(DescrepencyNewsFile entity)
		{
			 return _iDescrepencyNewsFileRepository.InsertDescrepencyNewsFile(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 descrepencynewsfileid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out descrepencynewsfileid))
            {
				DescrepencyNewsFile descrepencynewsfile = _iDescrepencyNewsFileRepository.GetDescrepencyNewsFile(descrepencynewsfileid);
                if(descrepencynewsfile!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(descrepencynewsfile);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<DescrepencyNewsFile> descrepencynewsfilelist = _iDescrepencyNewsFileRepository.GetAllDescrepencyNewsFile();
            if (descrepencynewsfilelist != null && descrepencynewsfilelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(descrepencynewsfilelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            //IList<string> errors = Validator.Validate(Input);
            try
            {
                DescrepencyNewsFile descrepencynewsfile = new DescrepencyNewsFile();
                PostOutput output = new PostOutput();
                descrepencynewsfile.CopyFrom(Input);
                descrepencynewsfile = _iDescrepencyNewsFileRepository.InsertDescrepencyNewsFile(descrepencynewsfile);
                output.CopyFrom(descrepencynewsfile);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            catch (Exception ex)
            {
                transer.IsSuccess = false;
                transer.Errors[0] = ex.Message;   //errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                DescrepencyNewsFile descrepencynewsfileinput = new DescrepencyNewsFile();
                DescrepencyNewsFile descrepencynewsfileoutput = new DescrepencyNewsFile();
                PutOutput output = new PutOutput();
                descrepencynewsfileinput.CopyFrom(Input);
                DescrepencyNewsFile descrepencynewsfile = _iDescrepencyNewsFileRepository.GetDescrepencyNewsFile(descrepencynewsfileinput.DescrepencyNewsFileId);
                if (descrepencynewsfile!=null)
                {
                    descrepencynewsfileoutput = _iDescrepencyNewsFileRepository.UpdateDescrepencyNewsFile(descrepencynewsfileinput);
                    if(descrepencynewsfileoutput!=null)
                    {
                        output.CopyFrom(descrepencynewsfileoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 descrepencynewsfileid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out descrepencynewsfileid))
            {
				 bool IsDeleted = _iDescrepencyNewsFileRepository.DeleteDescrepencyNewsFile(descrepencynewsfileid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public bool UpdateNewsFileId(int Id,int NewsFileId)
        {
           return  _iDescrepencyNewsFileRepository.UpdateNewsFileId(Id, NewsFileId);
        }
        public bool UpdateDescrepencyCategory(string Id, int CategoryId)
        {
            return _iDescrepencyNewsFileRepository.UpdateDescrepencyCategory(Id, CategoryId);
        }

        public List<int> GetDescrepencyNewsFileIdByDescrepencyValue(string DNewsFileId)
        {
            return _iDescrepencyNewsFileRepository.GetDescrepencyNewsFileIdByDescrepencyValue(DNewsFileId);
        }
        public bool UpdateDescrepencyValueByDescrepencyNewsFileId(string NewsFileId, string DescrepencyValue)
        {
            return _iDescrepencyNewsFileRepository.UpdateDescrepencyValueByDescrepencyNewsFileId(NewsFileId, DescrepencyValue);
        }
        
    }
	
	
}
