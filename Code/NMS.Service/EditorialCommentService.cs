﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EditorialComment;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Helper.Notification;
using System.Configuration;
using NMS.Core.Enums;

namespace NMS.Service
{

    public class EditorialCommentService : IEditorialCommentService
    {
        string host = ConfigurationManager.AppSettings["rabbitmqhost"];
        private IEditorialCommentRepository _iEditorialCommentRepository;
        private INewsFileRepository _iNewsFileRepository;
        private ISegmentRepository _iSegmentRepository;
        private ISlotRepository _iSlotRepository;
        private IEpisodeRepository _iEpisodeRepository;

        public EditorialCommentService(IEditorialCommentRepository iEditorialCommentRepository, INewsFileRepository iNewsFileRepository, ISegmentRepository iSegmentRepository, ISlotRepository iSlotRepository, IEpisodeRepository iEpisodeRepository)
        {
            this._iEditorialCommentRepository = iEditorialCommentRepository;
            this._iNewsFileRepository = iNewsFileRepository;
            this._iSegmentRepository = iSegmentRepository;
            this._iSlotRepository = iSlotRepository;
            this._iEpisodeRepository = iEpisodeRepository;
        }

        public Dictionary<string, string> GetEditorialCommentBasicSearchColumns()
        {

            return this._iEditorialCommentRepository.GetEditorialCommentBasicSearchColumns();

        }

        public List<SearchColumn> GetEditorialCommentAdvanceSearchColumns()
        {

            return this._iEditorialCommentRepository.GetEditorialCommentAdvanceSearchColumns();

        }


        public EditorialComment GetEditorialComment(System.Int32 EditorialCommentId)
        {
            return _iEditorialCommentRepository.GetEditorialComment(EditorialCommentId);
        }

        public EditorialComment UpdateEditorialComment(EditorialComment entity)
        {
            return _iEditorialCommentRepository.UpdateEditorialComment(entity);
        }

        public bool DeleteEditorialComment(System.Int32 EditorialCommentId)
        {
            return _iEditorialCommentRepository.DeleteEditorialComment(EditorialCommentId);
        }

        public List<EditorialComment> GetAllEditorialComment()
        {
            return _iEditorialCommentRepository.GetAllEditorialComment();
        }

        public EditorialComment InsertEditorialComment(EditorialComment entity)
        {
            return _iEditorialCommentRepository.InsertEditorialComment(entity);
        }


        public List<EditorialComment> GetComment(NMS.Core.DataTransfer.EditorialComment.PostInput input)
        {
            List<EditorialComment> comments = new List<EditorialComment>();
            if (input != null)
            {
                comments = _iEditorialCommentRepository.GetEditorialCommentByNewsFileId(Convert.ToInt32(input.NewsFileId));
            }
            return comments;
        }

        public List<EditorialComment> GetCommentByNewsFileIdAndLanguageCode(int newsfileId)
        {
            List<EditorialComment> comments = new List<EditorialComment>();
            if (newsfileId != 0)
            {
                comments = _iEditorialCommentRepository.GetEditorialCommentByNewsFileId(newsfileId);
            }
            return comments;
        }


        public List<EditorialComment> GetCommentsBySlotIdAndLastUpdateDate(DateTime lastupdatedate, int slotId)
        {
            List<EditorialComment> comments = new List<EditorialComment>();
            if (slotId != 0)
            {
                comments = _iEditorialCommentRepository.GetCommentsBySlotIdAndLastUpdateDate(lastupdatedate, slotId);
            }
            return comments;
        }


        public EditorialComment AddComment(NMS.Core.DataTransfer.EditorialComment.PostInput input)
        {
            DateTime dtnow = DateTime.UtcNow;
            EditorialComment comment = new EditorialComment();
            if (input != null)
            {
                comment.CreationDate = dtnow;
                comment.LastUpdateDate = dtnow;
                if (input.SlotId != null && input.SlotId != 0)
                    comment.SlotId = input.SlotId;
                if (input.TickerId != null && input.TickerId != 0)
                {
                    comment.TickerId = input.TickerId;
                    if (input.UserRoleId.HasValue && input.UserRoleId.Value == (int)WorkRole.EnglishTickerEditorial)
                    {
                        comment.LanguageCode = (int)LanguageCode.English;
                    }
                    else
                    {
                        comment.LanguageCode = (int)LanguageCode.Urdu;
                    }
                }

                if (!string.IsNullOrEmpty(input.EditorId)) { comment.EditorId = Convert.ToInt32(input.EditorId); }
                else { comment.EditorId = 0; }

                comment.IsActive = true;
                comment.Comment = input.Comment;
                comment.NewsFileId = Convert.ToInt32(input.NewsFileId);

                comment = _iEditorialCommentRepository.InsertEditorialComment(comment);

                try
                {
                    if (comment.SlotId.HasValue)
                    {
                        var commentcount = _iEditorialCommentRepository.GetEditorialCommentBySlotId(comment.SlotId.Value);

                        if (commentcount != null && commentcount.Count == 1)
                        {
                            RabbitMQNotification notif = new RabbitMQNotification(host);
                            var segment = _iSegmentRepository.GetSegment(_iSlotRepository.GetSlot(comment.SlotId.Value).SegmentId);

                            if (segment != null)
                            {
                                var ep = _iEpisodeRepository.GetEpisode(segment.EpisodeId);
                                InsertUpdateSlotNotification obj = new InsertUpdateSlotNotification();
                                obj.SlotId = comment.SlotId.Value;
                                obj.SegmentId = segment.SegmentId;
                                obj.EpisodeId = segment.EpisodeId;
                                obj.ProgramId = ep.ProgramId;

                                notif.PublishMessage(new RabbitWrapper<InsertUpdateSlotNotification>
                                {
                                    Data = obj,
                                    EventName = RabbitRoutingKey.ProgramEditorial.Events.Slot_FirstComment
                                },
                                    RabbitChannels.Program, RabbitRoutingKey.ProgramEditorial.Routes.Slot);

                            }


                        }
                    }



                    // var episode = episodeService.GetEpisodeBySlotId(returnSlots.FirstOrDefault().SlotId);

                }
                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                }

                if (comment != null)
                {
                    var newsfile = _iNewsFileRepository.GetNewsFile(comment.NewsFileId);
                    if (newsfile != null)
                    {
                        int CommentCount = newsfile.EcCount;
                        newsfile.EcCount = CommentCount + 1;
                        newsfile.LastUpdateDate = dtnow;
                        _iNewsFileRepository.UpdateNewsFile(newsfile);
                    }
                }
            }
            return comment;
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 editorialcommentid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out editorialcommentid))
            {
                EditorialComment editorialcomment = _iEditorialCommentRepository.GetEditorialComment(editorialcommentid);
                if (editorialcomment != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(editorialcomment);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<EditorialComment> editorialcommentlist = _iEditorialCommentRepository.GetAllEditorialComment();
            if (editorialcommentlist != null && editorialcommentlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(editorialcommentlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                EditorialComment editorialcomment = new EditorialComment();
                PostOutput output = new PostOutput();
                editorialcomment.CopyFrom(Input);
                editorialcomment = _iEditorialCommentRepository.InsertEditorialComment(editorialcomment);
                output.CopyFrom(editorialcomment);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                EditorialComment editorialcommentinput = new EditorialComment();
                EditorialComment editorialcommentoutput = new EditorialComment();
                PutOutput output = new PutOutput();
                editorialcommentinput.CopyFrom(Input);
                EditorialComment editorialcomment = _iEditorialCommentRepository.GetEditorialComment(editorialcommentinput.EditorialCommentId);
                if (editorialcomment != null)
                {
                    editorialcommentoutput = _iEditorialCommentRepository.UpdateEditorialComment(editorialcommentinput);
                    if (editorialcommentoutput != null)
                    {
                        output.CopyFrom(editorialcommentoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 editorialcommentid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out editorialcommentid))
            {
                bool IsDeleted = _iEditorialCommentRepository.DeleteEditorialComment(editorialcommentid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<EditorialComment> GetCommentByTickerId(int tickerId, int userRoleId)
        {
            List<EditorialComment> comments = new List<EditorialComment>();
            if (tickerId > 0)
            {
                var languageId = (int)LanguageCode.Urdu;
                if(userRoleId == (int)WorkRole.EnglishTickerEditorial || userRoleId == (int)WorkRole.EnglishTickerWriter)
                {
                    languageId = (int)LanguageCode.English;
                }

                comments = _iEditorialCommentRepository.GetEditorialCommentByTickerId(tickerId, languageId);
            }
            return comments;
        }
    }


}
