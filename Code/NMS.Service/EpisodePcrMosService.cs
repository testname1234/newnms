﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using Validation;
using System.Linq;

namespace NMS.Core.Service
{
		
	public class EpisodePcrMosService : IEpisodePcrMosService 
	{
		private IEpisodePcrMosRepository _iEpisodePcrMosRepository;
        
		public EpisodePcrMosService(IEpisodePcrMosRepository iEpisodePcrMosRepository)
		{
			this._iEpisodePcrMosRepository = iEpisodePcrMosRepository;
		}
        
        public Dictionary<string, string> GetEpisodePcrMosBasicSearchColumns()
        {
            
            return this._iEpisodePcrMosRepository.GetEpisodePcrMosBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetEpisodePcrMosAdvanceSearchColumns()
        {
            
            return this._iEpisodePcrMosRepository.GetEpisodePcrMosAdvanceSearchColumns();
           
        }
        

		public EpisodePcrMos GetEpisodePcrMos(System.Int32 EpisodePcrMosId)
		{
			return _iEpisodePcrMosRepository.GetEpisodePcrMos(EpisodePcrMosId);
		}

		public EpisodePcrMos UpdateEpisodePcrMos(EpisodePcrMos entity)
		{
			return _iEpisodePcrMosRepository.UpdateEpisodePcrMos(entity);
		}

		public bool DeleteEpisodePcrMos(System.Int32 EpisodePcrMosId)
		{
			return _iEpisodePcrMosRepository.DeleteEpisodePcrMos(EpisodePcrMosId);
		}

		public List<EpisodePcrMos> GetAllEpisodePcrMos()
		{
			return _iEpisodePcrMosRepository.GetAllEpisodePcrMos();
		}

		public EpisodePcrMos InsertEpisodePcrMos(EpisodePcrMos entity)
		{
			 return _iEpisodePcrMosRepository.InsertEpisodePcrMos(entity);
		}

	}
	
	
}
