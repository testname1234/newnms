﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Episode;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Enums;

namespace NMS.Service
{
		
	public class EpisodeService : IEpisodeService 
	{
		private IEpisodeRepository _iEpisodeRepository;
        
		public EpisodeService(IEpisodeRepository iEpisodeRepository)
		{
			this._iEpisodeRepository = iEpisodeRepository;
		}
        
        public Dictionary<string, string> GetEpisodeBasicSearchColumns()
        {
            
            return this._iEpisodeRepository.GetEpisodeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetEpisodeAdvanceSearchColumns()
        {
            
            return this._iEpisodeRepository.GetEpisodeAdvanceSearchColumns();
           
        }
        

		public virtual List<Episode> GetEpisodeByProgramId(System.Int32 ProgramId)
		{
			return _iEpisodeRepository.GetEpisodeByProgramId(ProgramId);
		}

		public Episode GetEpisode(System.Int32 EpisodeId)
		{
			return _iEpisodeRepository.GetEpisode(EpisodeId);
		}

        public Episode GetEpisodeBySegmentId(System.Int32 SegmentId)
        {
            return _iEpisodeRepository.GetEpisodeBySegmentId(SegmentId);
        }

        
        public Episode GetEpisodeBySlotId(System.Int32 SlotId)
        {
            return _iEpisodeRepository.GetEpisodeBySlotId(SlotId);
        }

		public Episode UpdateEpisode(Episode entity)
		{
			return _iEpisodeRepository.UpdateEpisode(entity);
		}

		public bool DeleteEpisode(System.Int32 EpisodeId)
		{
			return _iEpisodeRepository.DeleteEpisode(EpisodeId);
		}

		public List<Episode> GetAllEpisode()
		{
			return _iEpisodeRepository.GetAllEpisode();
		}

		public Episode InsertEpisode(Episode entity)
		{
			 return _iEpisodeRepository.InsertEpisode(entity);
		}

        public List<Episode> GetEpisodesByProgramType(ProgramTypes programType, string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null)
        {
            return _iEpisodeRepository.GetEpisodesByProgramType(programType, searchTerm, fromDate, toDate, pageCount, pageIndex, userId);
        }

        public List<int> GetProgramSetMappingID(int EpisodeId)
        {
            return _iEpisodeRepository.GetProgramSetMappingID(EpisodeId);
        }

        public List<NMS.Core.DataTransfer.Episode.GetOutput> GetEpisodeDetailByProgram(List<int> lstprogramid, DateTime from, DateTime to)
        {
            return _iEpisodeRepository.GetEpisodeDetailByProgram(lstprogramid, from,to);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 episodeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out episodeid))
            {
				Episode episode = _iEpisodeRepository.GetEpisode(episodeid);
                if(episode!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(episode);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Episode> episodelist = _iEpisodeRepository.GetAllEpisode();
            if (episodelist != null && episodelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(episodelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Episode episode = new Episode();
                PostOutput output = new PostOutput();
                episode.CopyFrom(Input);
                episode = _iEpisodeRepository.InsertEpisode(episode);
                output.CopyFrom(episode);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Episode episodeinput = new Episode();
                Episode episodeoutput = new Episode();
                PutOutput output = new PutOutput();
                episodeinput.CopyFrom(Input);
                Episode episode = _iEpisodeRepository.GetEpisode(episodeinput.EpisodeId);
                if (episode!=null)
                {
                    episodeoutput = _iEpisodeRepository.UpdateEpisode(episodeinput);
                    if(episodeoutput!=null)
                    {
                        output.CopyFrom(episodeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 episodeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out episodeid))
            {
				 bool IsDeleted = _iEpisodeRepository.DeleteEpisode(episodeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
