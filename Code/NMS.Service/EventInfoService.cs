﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventInfo;
using NMS.Core;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class EventInfoService : IEventInfoService 
	{
		private IEventInfoRepository _iEventInfoRepository;
        
		public EventInfoService(IEventInfoRepository iEventInfoRepository)
		{
			this._iEventInfoRepository = iEventInfoRepository;
		}
        
        public Dictionary<string, string> GetEventInfoBasicSearchColumns()
        {
            
            return this._iEventInfoRepository.GetEventInfoBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetEventInfoAdvanceSearchColumns()
        {
            
            return this._iEventInfoRepository.GetEventInfoAdvanceSearchColumns();
           
        }
        

		public EventInfo GetEventInfo(System.Int32 EventInfoId)
		{
			return _iEventInfoRepository.GetEventInfo(EventInfoId);
		}

        public List<EventInfo> GetUpcommingEvents()
        {
            return _iEventInfoRepository.GetUpcommingEvents();
        }

		public EventInfo UpdateEventInfo(EventInfo entity)
		{
			return _iEventInfoRepository.UpdateEventInfo(entity);
		}

		public bool DeleteEventInfo(System.Int32 EventInfoId)
		{
			return _iEventInfoRepository.DeleteEventInfo(EventInfoId);
		}

		public List<EventInfo> GetAllEventInfo()
		{
			return _iEventInfoRepository.GetAllEventInfo();
		}

		public EventInfo InsertEventInfo(EventInfo entity)
		{
			 return _iEventInfoRepository.InsertEventInfo(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 eventinfoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventinfoid))
            {
				EventInfo eventinfo = _iEventInfoRepository.GetEventInfo(eventinfoid);
                if(eventinfo!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(eventinfo);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<EventInfo> eventinfolist = _iEventInfoRepository.GetAllEventInfo();
            if (eventinfolist != null && eventinfolist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(eventinfolist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                EventInfo eventinfo = new EventInfo();
                PostOutput output = new PostOutput();
                eventinfo.CopyFrom(Input);
                eventinfo = _iEventInfoRepository.InsertEventInfo(eventinfo);
                output.CopyFrom(eventinfo);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                EventInfo eventinfoinput = new EventInfo();
                EventInfo eventinfooutput = new EventInfo();
                PutOutput output = new PutOutput();
                eventinfoinput.CopyFrom(Input);
                EventInfo eventinfo = _iEventInfoRepository.GetEventInfo(eventinfoinput.EventInfoId);
                if (eventinfo!=null)
                {
                    eventinfooutput = _iEventInfoRepository.UpdateEventInfo(eventinfoinput);
                    if(eventinfooutput!=null)
                    {
                        output.CopyFrom(eventinfooutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 eventinfoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventinfoid))
            {
				 bool IsDeleted = _iEventInfoRepository.DeleteEventInfo(eventinfoid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
