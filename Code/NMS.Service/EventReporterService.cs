﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventReporter;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class EventReporterService : IEventReporterService 
	{
		private IEventReporterRepository _iEventReporterRepository;
        
		public EventReporterService(IEventReporterRepository iEventReporterRepository)
		{
			this._iEventReporterRepository = iEventReporterRepository;
		}
        
        public Dictionary<string, string> GetEventReporterBasicSearchColumns()
        {
            
            return this._iEventReporterRepository.GetEventReporterBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetEventReporterAdvanceSearchColumns()
        {
            
            return this._iEventReporterRepository.GetEventReporterAdvanceSearchColumns();
           
        }
        

		public virtual List<EventReporter> GetEventReporterByNewsFileId(System.Int32? NewsFileId)
		{
			return _iEventReporterRepository.GetEventReporterByNewsFileId(NewsFileId);
		}

		public EventReporter GetEventReporter(System.Int32 EventReporterId)
		{
			return _iEventReporterRepository.GetEventReporter(EventReporterId);
		}

		public EventReporter UpdateEventReporter(EventReporter entity)
		{
			return _iEventReporterRepository.UpdateEventReporter(entity);
		}

		public bool DeleteEventReporter(System.Int32 EventReporterId)
		{
			return _iEventReporterRepository.DeleteEventReporter(EventReporterId);
		}

		public List<EventReporter> GetAllEventReporter()
		{
			return _iEventReporterRepository.GetAllEventReporter();
		}

		public EventReporter InsertEventReporter(EventReporter entity)
		{
			 return _iEventReporterRepository.InsertEventReporter(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 eventreporterid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventreporterid))
            {
				EventReporter eventreporter = _iEventReporterRepository.GetEventReporter(eventreporterid);
                if(eventreporter!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(eventreporter);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<EventReporter> eventreporterlist = _iEventReporterRepository.GetAllEventReporter();
            if (eventreporterlist != null && eventreporterlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(eventreporterlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                EventReporter eventreporter = new EventReporter();
                PostOutput output = new PostOutput();
                eventreporter.CopyFrom(Input);
                eventreporter = _iEventReporterRepository.InsertEventReporter(eventreporter);
                output.CopyFrom(eventreporter);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                EventReporter eventreporterinput = new EventReporter();
                EventReporter eventreporteroutput = new EventReporter();
                PutOutput output = new PutOutput();
                eventreporterinput.CopyFrom(Input);
                EventReporter eventreporter = _iEventReporterRepository.GetEventReporter(eventreporterinput.EventReporterId);
                if (eventreporter!=null)
                {
                    eventreporteroutput = _iEventReporterRepository.UpdateEventReporter(eventreporterinput);
                    if(eventreporteroutput!=null)
                    {
                        output.CopyFrom(eventreporteroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 eventreporterid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventreporterid))
            {
				 bool IsDeleted = _iEventReporterRepository.DeleteEventReporter(eventreporterid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
