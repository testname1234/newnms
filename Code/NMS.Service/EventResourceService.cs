﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventResource;
using Validation;
using System.Linq;
using NMS.Core;


namespace NMS.Service
{
		
	public class EventResourceService : IEventResourceService 
	{
		private IEventResourceRepository _iEventResourceRepository;
        
		public EventResourceService(IEventResourceRepository iEventResourceRepository)
		{
			this._iEventResourceRepository = iEventResourceRepository;
		}
        
        public Dictionary<string, string> GetEventResourceBasicSearchColumns()
        {
            
            return this._iEventResourceRepository.GetEventResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetEventResourceAdvanceSearchColumns()
        {
            
            return this._iEventResourceRepository.GetEventResourceAdvanceSearchColumns();
           
        }
        

		public virtual List<EventResource> GetEventResourceByNewsFileId(System.Int32? NewsFileId)
		{
			return _iEventResourceRepository.GetEventResourceByNewsFileId(NewsFileId);
		}

		public EventResource GetEventResource(System.Int32 EventResourceId)
		{
			return _iEventResourceRepository.GetEventResource(EventResourceId);
		}

		public EventResource UpdateEventResource(EventResource entity)
		{
			return _iEventResourceRepository.UpdateEventResource(entity);
		}

		public bool DeleteEventResource(System.Int32 EventResourceId)
		{
			return _iEventResourceRepository.DeleteEventResource(EventResourceId);
		}

		public List<EventResource> GetAllEventResource()
		{
			return _iEventResourceRepository.GetAllEventResource();
		}

		public EventResource InsertEventResource(EventResource entity)
		{
			 return _iEventResourceRepository.InsertEventResource(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 eventresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventresourceid))
            {
				EventResource eventresource = _iEventResourceRepository.GetEventResource(eventresourceid);
                if(eventresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(eventresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<EventResource> eventresourcelist = _iEventResourceRepository.GetAllEventResource();
            if (eventresourcelist != null && eventresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(eventresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                EventResource eventresource = new EventResource();
                PostOutput output = new PostOutput();
                eventresource.CopyFrom(Input);
                eventresource = _iEventResourceRepository.InsertEventResource(eventresource);
                output.CopyFrom(eventresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                EventResource eventresourceinput = new EventResource();
                EventResource eventresourceoutput = new EventResource();
                PutOutput output = new PutOutput();
                eventresourceinput.CopyFrom(Input);
                EventResource eventresource = _iEventResourceRepository.GetEventResource(eventresourceinput.EventResourceId);
                if (eventresource!=null)
                {
                    eventresourceoutput = _iEventResourceRepository.UpdateEventResource(eventresourceinput);
                    if(eventresourceoutput!=null)
                    {
                        output.CopyFrom(eventresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 eventresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventresourceid))
            {
				 bool IsDeleted = _iEventResourceRepository.DeleteEventResource(eventresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
