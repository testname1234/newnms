﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.EventType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class EventTypeService : IEventTypeService 
	{
		private IEventTypeRepository _iEventTypeRepository;
        
		public EventTypeService(IEventTypeRepository iEventTypeRepository)
		{
			this._iEventTypeRepository = iEventTypeRepository;
		}
        
        public Dictionary<string, string> GetEventTypeBasicSearchColumns()
        {
            
            return this._iEventTypeRepository.GetEventTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetEventTypeAdvanceSearchColumns()
        {
            
            return this._iEventTypeRepository.GetEventTypeAdvanceSearchColumns();
           
        }
        

		public EventType GetEventType(System.Int32 EventTypeId)
		{
			return _iEventTypeRepository.GetEventType(EventTypeId);
		}

		public EventType UpdateEventType(EventType entity)
		{
			return _iEventTypeRepository.UpdateEventType(entity);
		}

		public bool DeleteEventType(System.Int32 EventTypeId)
		{
			return _iEventTypeRepository.DeleteEventType(EventTypeId);
		}

		public List<EventType> GetAllEventType()
		{
			return _iEventTypeRepository.GetAllEventType();
		}

		public EventType InsertEventType(EventType entity)
		{
			 return _iEventTypeRepository.InsertEventType(entity);
		}

        public List<EventType> GetEventTypeByTerm(string term)
        {
            return _iEventTypeRepository.GetEventTypeByTerm(term);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 eventtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventtypeid))
            {
				EventType eventtype = _iEventTypeRepository.GetEventType(eventtypeid);
                if(eventtype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(eventtype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<EventType> eventtypelist = _iEventTypeRepository.GetAllEventType();
            if (eventtypelist != null && eventtypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(eventtypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                EventType eventtype = new EventType();
                PostOutput output = new PostOutput();
                eventtype.CopyFrom(Input);
                eventtype = _iEventTypeRepository.InsertEventType(eventtype);
                output.CopyFrom(eventtype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                EventType eventtypeinput = new EventType();
                EventType eventtypeoutput = new EventType();
                PutOutput output = new PutOutput();
                eventtypeinput.CopyFrom(Input);
                EventType eventtype = _iEventTypeRepository.GetEventType(eventtypeinput.EventTypeId);
                if (eventtype!=null)
                {
                    eventtypeoutput = _iEventTypeRepository.UpdateEventType(eventtypeinput);
                    if(eventtypeoutput!=null)
                    {
                        output.CopyFrom(eventtypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 eventtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out eventtypeid))
            {
				 bool IsDeleted = _iEventTypeRepository.DeleteEventType(eventtypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
