﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FacebookAccount;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class FacebookAccountService : IFacebookAccountService 
	{
		private IFacebookAccountRepository _iFacebookAccountRepository;
        
		public FacebookAccountService(IFacebookAccountRepository iFacebookAccountRepository)
		{
			this._iFacebookAccountRepository = iFacebookAccountRepository;
		}
        
        public Dictionary<string, string> GetFacebookAccountBasicSearchColumns()
        {
            
            return this._iFacebookAccountRepository.GetFacebookAccountBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFacebookAccountAdvanceSearchColumns()
        {
            
            return this._iFacebookAccountRepository.GetFacebookAccountAdvanceSearchColumns();
           
        }
        

		public virtual List<FacebookAccount> GetFacebookAccountByCelebrityId(System.Int32? CelebrityId)
		{
			return _iFacebookAccountRepository.GetFacebookAccountByCelebrityId(CelebrityId);
		}

		public FacebookAccount GetFacebookAccount(System.Int32 FacebookAccountId)
		{
			return _iFacebookAccountRepository.GetFacebookAccount(FacebookAccountId);
		}

		public FacebookAccount UpdateFacebookAccount(FacebookAccount entity)
		{
			return _iFacebookAccountRepository.UpdateFacebookAccount(entity);
		}

		public bool DeleteFacebookAccount(System.Int32 FacebookAccountId)
		{
			return _iFacebookAccountRepository.DeleteFacebookAccount(FacebookAccountId);
		}

		public List<FacebookAccount> GetAllFacebookAccount()
		{
			return _iFacebookAccountRepository.GetAllFacebookAccount();
		}

		public FacebookAccount InsertFacebookAccount(FacebookAccount entity)
		{
			 return _iFacebookAccountRepository.InsertFacebookAccount(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 facebookaccountid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out facebookaccountid))
            {
				FacebookAccount facebookaccount = _iFacebookAccountRepository.GetFacebookAccount(facebookaccountid);
                if(facebookaccount!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(facebookaccount);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FacebookAccount> facebookaccountlist = _iFacebookAccountRepository.GetAllFacebookAccount();
            if (facebookaccountlist != null && facebookaccountlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(facebookaccountlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FacebookAccount facebookaccount = new FacebookAccount();
                PostOutput output = new PostOutput();
                facebookaccount.CopyFrom(Input);
                facebookaccount = _iFacebookAccountRepository.InsertFacebookAccount(facebookaccount);
                output.CopyFrom(facebookaccount);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FacebookAccount facebookaccountinput = new FacebookAccount();
                FacebookAccount facebookaccountoutput = new FacebookAccount();
                PutOutput output = new PutOutput();
                facebookaccountinput.CopyFrom(Input);
                FacebookAccount facebookaccount = _iFacebookAccountRepository.GetFacebookAccount(facebookaccountinput.FacebookAccountId);
                if (facebookaccount!=null)
                {
                    facebookaccountoutput = _iFacebookAccountRepository.UpdateFacebookAccount(facebookaccountinput);
                    if(facebookaccountoutput!=null)
                    {
                        output.CopyFrom(facebookaccountoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 facebookaccountid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out facebookaccountid))
            {
				 bool IsDeleted = _iFacebookAccountRepository.DeleteFacebookAccount(facebookaccountid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
