﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FacebookCredential;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class FacebookCredentialService : IFacebookCredentialService
    {
        private IFacebookCredentialRepository _iFacebookCredentialRepository;

        public FacebookCredentialService(IFacebookCredentialRepository iFacebookCredentialRepository)
        {
            this._iFacebookCredentialRepository = iFacebookCredentialRepository;
        }

        public Dictionary<string, string> GetFacebookCredentialBasicSearchColumns()
        {

            return this._iFacebookCredentialRepository.GetFacebookCredentialBasicSearchColumns();

        }

        public List<SearchColumn> GetFacebookCredentialAdvanceSearchColumns()
        {

            return this._iFacebookCredentialRepository.GetFacebookCredentialAdvanceSearchColumns();

        }


        public FacebookCredential GetFacebookCredential(System.Int32 FacebookCredentialId)
        {
            return _iFacebookCredentialRepository.GetFacebookCredential(FacebookCredentialId);
        }

        public FacebookCredential UpdateFacebookCredential(FacebookCredential entity)
        {
            return _iFacebookCredentialRepository.UpdateFacebookCredential(entity);
        }

        public bool DeleteFacebookCredential(System.Int32 FacebookCredentialId)
        {
            return _iFacebookCredentialRepository.DeleteFacebookCredential(FacebookCredentialId);
        }

        public List<FacebookCredential> GetAllFacebookCredential()
        {
            return _iFacebookCredentialRepository.GetAllFacebookCredential();
        }

        public FacebookCredential InsertFacebookCredential(FacebookCredential entity)
        {
            return _iFacebookCredentialRepository.InsertFacebookCredential(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 facebookcredentialid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out facebookcredentialid))
            {
                FacebookCredential facebookcredential = _iFacebookCredentialRepository.GetFacebookCredential(facebookcredentialid);
                if (facebookcredential != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(facebookcredential);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FacebookCredential> facebookcredentiallist = _iFacebookCredentialRepository.GetAllFacebookCredential();
            if (facebookcredentiallist != null && facebookcredentiallist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(facebookcredentiallist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FacebookCredential facebookcredential = new FacebookCredential();
                PostOutput output = new PostOutput();
                facebookcredential.CopyFrom(Input);
                facebookcredential = _iFacebookCredentialRepository.InsertFacebookCredential(facebookcredential);
                output.CopyFrom(facebookcredential);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FacebookCredential facebookcredentialinput = new FacebookCredential();
                FacebookCredential facebookcredentialoutput = new FacebookCredential();
                PutOutput output = new PutOutput();
                facebookcredentialinput.CopyFrom(Input);
                FacebookCredential facebookcredential = _iFacebookCredentialRepository.GetFacebookCredential(facebookcredentialinput.FacebookCredentialId);
                if (facebookcredential != null)
                {
                    facebookcredentialoutput = _iFacebookCredentialRepository.UpdateFacebookCredential(facebookcredentialinput);
                    if (facebookcredentialoutput != null)
                    {
                        output.CopyFrom(facebookcredentialoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 facebookcredentialid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out facebookcredentialid))
            {
                bool IsDeleted = _iFacebookCredentialRepository.DeleteFacebookCredential(facebookcredentialid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public void UdpateAccessToken(string AccessToken, DateTime LastUpdateDate, int CredentialID)
        {
            _iFacebookCredentialRepository.UdpateAccessToken(AccessToken, LastUpdateDate, CredentialID);
        }

        public string GetAccessToken()
        {
            return _iFacebookCredentialRepository.GetAccessToken();
        }
    }


}
