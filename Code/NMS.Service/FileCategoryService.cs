﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileCategory;
using NMS.Core;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class FileCategoryService : IFileCategoryService 
	{
		private IFileCategoryRepository _iFileCategoryRepository;
        
		public FileCategoryService(IFileCategoryRepository iFileCategoryRepository)
		{
			this._iFileCategoryRepository = iFileCategoryRepository;
		}
        
        public Dictionary<string, string> GetFileCategoryBasicSearchColumns()
        {
            
            return this._iFileCategoryRepository.GetFileCategoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFileCategoryAdvanceSearchColumns()
        {
            
            return this._iFileCategoryRepository.GetFileCategoryAdvanceSearchColumns();
           
        }
        

		public virtual List<FileCategory> GetFileCategoryByNewsFileId(System.Int32 NewsFileId)
		{
			return _iFileCategoryRepository.GetFileCategoryByNewsFileId(NewsFileId);
		}

		public virtual List<FileCategory> GetFileCategoryByCategoryId(System.Int32 CategoryId)
		{
			return _iFileCategoryRepository.GetFileCategoryByCategoryId(CategoryId);
		}

		public FileCategory GetFileCategory(System.Int32 FileCategoryId)
		{
			return _iFileCategoryRepository.GetFileCategory(FileCategoryId);
		}

		public FileCategory UpdateFileCategory(FileCategory entity)
		{
			return _iFileCategoryRepository.UpdateFileCategory(entity);
		}

		public bool DeleteFileCategory(System.Int32 FileCategoryId)
		{
			return _iFileCategoryRepository.DeleteFileCategory(FileCategoryId);
		}

		public List<FileCategory> GetAllFileCategory()
		{
			return _iFileCategoryRepository.GetAllFileCategory();
		}

		public FileCategory InsertFileCategory(FileCategory entity)
		{
			 return _iFileCategoryRepository.InsertFileCategory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filecategoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filecategoryid))
            {
				FileCategory filecategory = _iFileCategoryRepository.GetFileCategory(filecategoryid);
                if(filecategory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filecategory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FileCategory> filecategorylist = _iFileCategoryRepository.GetAllFileCategory();
            if (filecategorylist != null && filecategorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(filecategorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FileCategory filecategory = new FileCategory();
                PostOutput output = new PostOutput();
                filecategory.CopyFrom(Input);
                filecategory = _iFileCategoryRepository.InsertFileCategory(filecategory);
                output.CopyFrom(filecategory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FileCategory filecategoryinput = new FileCategory();
                FileCategory filecategoryoutput = new FileCategory();
                PutOutput output = new PutOutput();
                filecategoryinput.CopyFrom(Input);
                FileCategory filecategory = _iFileCategoryRepository.GetFileCategory(filecategoryinput.FileCategoryId);
                if (filecategory!=null)
                {
                    filecategoryoutput = _iFileCategoryRepository.UpdateFileCategory(filecategoryinput);
                    if(filecategoryoutput!=null)
                    {
                        output.CopyFrom(filecategoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filecategoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filecategoryid))
            {
				 bool IsDeleted = _iFileCategoryRepository.DeleteFileCategory(filecategoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public bool DeleteFileCategoryByNewsFileId(int newsFileId)
        {
            return _iFileCategoryRepository.DeleteFileCategoryByNewsFileId(newsFileId);
        }

    }
	
	




}
