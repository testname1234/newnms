﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileDetail;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class FileDetailService : IFileDetailService 
	{
		private IFileDetailRepository _iFileDetailRepository;
        
		public FileDetailService(IFileDetailRepository iFileDetailRepository)
		{
			this._iFileDetailRepository = iFileDetailRepository;
		}
        
        public Dictionary<string, string> GetFileDetailBasicSearchColumns()
        {
            
            return this._iFileDetailRepository.GetFileDetailBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFileDetailAdvanceSearchColumns()
        {
            
            return this._iFileDetailRepository.GetFileDetailAdvanceSearchColumns();
           
        }
        

		public virtual List<FileDetail> GetFileDetailByNewsFileId(System.Int32 NewsFileId)
		{
			return _iFileDetailRepository.GetFileDetailByNewsFileId(NewsFileId);
		}

		public FileDetail GetFileDetail(System.Int32 FileDetailId)
		{
			return _iFileDetailRepository.GetFileDetail(FileDetailId);
		}

		public FileDetail UpdateFileDetail(FileDetail entity)
		{
			return _iFileDetailRepository.UpdateFileDetail(entity);
		}

		public bool DeleteFileDetail(System.Int32 FileDetailId)
		{
			return _iFileDetailRepository.DeleteFileDetail(FileDetailId);
		}

		public List<FileDetail> GetAllFileDetail()
		{
			return _iFileDetailRepository.GetAllFileDetail();
		}

		public FileDetail InsertFileDetail(FileDetail entity)
		{
			 return _iFileDetailRepository.InsertFileDetail(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filedetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filedetailid))
            {
				FileDetail filedetail = _iFileDetailRepository.GetFileDetail(filedetailid);
                if(filedetail!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filedetail);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FileDetail> filedetaillist = _iFileDetailRepository.GetAllFileDetail();
            if (filedetaillist != null && filedetaillist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(filedetaillist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FileDetail filedetail = new FileDetail();
                PostOutput output = new PostOutput();
                filedetail.CopyFrom(Input);
                filedetail = _iFileDetailRepository.InsertFileDetail(filedetail);
                output.CopyFrom(filedetail);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FileDetail filedetailinput = new FileDetail();
                FileDetail filedetailoutput = new FileDetail();
                PutOutput output = new PutOutput();
                filedetailinput.CopyFrom(Input);
                FileDetail filedetail = _iFileDetailRepository.GetFileDetail(filedetailinput.FileDetailId);
                if (filedetail!=null)
                {
                    filedetailoutput = _iFileDetailRepository.UpdateFileDetail(filedetailinput);
                    if(filedetailoutput!=null)
                    {
                        output.CopyFrom(filedetailoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filedetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filedetailid))
            {
				 bool IsDeleted = _iFileDetailRepository.DeleteFileDetail(filedetailid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
