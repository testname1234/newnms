﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileFolderHistory;
using Validation;
using System.Linq;
using NMS.Core;


namespace NMS.Service
{
		
	public class FileFolderHistoryService : IFileFolderHistoryService 
	{
		private IFileFolderHistoryRepository _iFileFolderHistoryRepository;
        
		public FileFolderHistoryService(IFileFolderHistoryRepository iFileFolderHistoryRepository)
		{
			this._iFileFolderHistoryRepository = iFileFolderHistoryRepository;
		}
        
        public Dictionary<string, string> GetFileFolderHistoryBasicSearchColumns()
        {
            
            return this._iFileFolderHistoryRepository.GetFileFolderHistoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFileFolderHistoryAdvanceSearchColumns()
        {
            
            return this._iFileFolderHistoryRepository.GetFileFolderHistoryAdvanceSearchColumns();
           
        }
        

		public virtual List<FileFolderHistory> GetFileFolderHistoryByNewsFileId(System.Int32 NewsFileId)
		{
			return _iFileFolderHistoryRepository.GetFileFolderHistoryByNewsFileId(NewsFileId);
		}

		public virtual List<FileFolderHistory> GetFileFolderHistoryByFolderId(System.Int32 FolderId)
		{
			return _iFileFolderHistoryRepository.GetFileFolderHistoryByFolderId(FolderId);
		}

		public FileFolderHistory GetFileFolderHistory(System.Int32 FileFolderHistoryId)
		{
			return _iFileFolderHistoryRepository.GetFileFolderHistory(FileFolderHistoryId);
		}

		public FileFolderHistory UpdateFileFolderHistory(FileFolderHistory entity)
		{
			return _iFileFolderHistoryRepository.UpdateFileFolderHistory(entity);
		}

		public bool DeleteFileFolderHistory(System.Int32 FileFolderHistoryId)
		{
			return _iFileFolderHistoryRepository.DeleteFileFolderHistory(FileFolderHistoryId);
		}

		public List<FileFolderHistory> GetAllFileFolderHistory()
		{
			return _iFileFolderHistoryRepository.GetAllFileFolderHistory();
		}

		public FileFolderHistory InsertFileFolderHistory(FileFolderHistory entity)
		{
			 return _iFileFolderHistoryRepository.InsertFileFolderHistory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filefolderhistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filefolderhistoryid))
            {
				FileFolderHistory filefolderhistory = _iFileFolderHistoryRepository.GetFileFolderHistory(filefolderhistoryid);
                if(filefolderhistory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filefolderhistory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FileFolderHistory> filefolderhistorylist = _iFileFolderHistoryRepository.GetAllFileFolderHistory();
            if (filefolderhistorylist != null && filefolderhistorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(filefolderhistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FileFolderHistory filefolderhistory = new FileFolderHistory();
                PostOutput output = new PostOutput();
                filefolderhistory.CopyFrom(Input);
                filefolderhistory = _iFileFolderHistoryRepository.InsertFileFolderHistory(filefolderhistory);
                output.CopyFrom(filefolderhistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FileFolderHistory filefolderhistoryinput = new FileFolderHistory();
                FileFolderHistory filefolderhistoryoutput = new FileFolderHistory();
                PutOutput output = new PutOutput();
                filefolderhistoryinput.CopyFrom(Input);
                FileFolderHistory filefolderhistory = _iFileFolderHistoryRepository.GetFileFolderHistory(filefolderhistoryinput.FileFolderHistoryId);
                if (filefolderhistory!=null)
                {
                    filefolderhistoryoutput = _iFileFolderHistoryRepository.UpdateFileFolderHistory(filefolderhistoryinput);
                    if(filefolderhistoryoutput!=null)
                    {
                        output.CopyFrom(filefolderhistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filefolderhistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filefolderhistoryid))
            {
				 bool IsDeleted = _iFileFolderHistoryRepository.DeleteFileFolderHistory(filefolderhistoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
