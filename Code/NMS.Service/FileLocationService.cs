﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileLocation;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class FileLocationService : IFileLocationService 
	{
		private IFileLocationRepository _iFileLocationRepository;
        
		public FileLocationService(IFileLocationRepository iFileLocationRepository)
		{
			this._iFileLocationRepository = iFileLocationRepository;
		}
        
        public Dictionary<string, string> GetFileLocationBasicSearchColumns()
        {
            
            return this._iFileLocationRepository.GetFileLocationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFileLocationAdvanceSearchColumns()
        {
            
            return this._iFileLocationRepository.GetFileLocationAdvanceSearchColumns();
           
        }
        

		public virtual List<FileLocation> GetFileLocationByNewsFileId(System.Int32 NewsFileId)
		{
			return _iFileLocationRepository.GetFileLocationByNewsFileId(NewsFileId);
		}

		public virtual List<FileLocation> GetFileLocationByLocationId(System.Int32 LocationId)
		{
			return _iFileLocationRepository.GetFileLocationByLocationId(LocationId);
		}

		public FileLocation GetFileLocation(System.Int32 FileLocationId)
		{
			return _iFileLocationRepository.GetFileLocation(FileLocationId);
		}

		public FileLocation UpdateFileLocation(FileLocation entity)
		{
			return _iFileLocationRepository.UpdateFileLocation(entity);
		}

		public bool DeleteFileLocation(System.Int32 FileLocationId)
		{
			return _iFileLocationRepository.DeleteFileLocation(FileLocationId);
		}

		public List<FileLocation> GetAllFileLocation()
		{
			return _iFileLocationRepository.GetAllFileLocation();
		}

		public FileLocation InsertFileLocation(FileLocation entity)
		{
			 return _iFileLocationRepository.InsertFileLocation(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filelocationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filelocationid))
            {
				FileLocation filelocation = _iFileLocationRepository.GetFileLocation(filelocationid);
                if(filelocation!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filelocation);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FileLocation> filelocationlist = _iFileLocationRepository.GetAllFileLocation();
            if (filelocationlist != null && filelocationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(filelocationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FileLocation filelocation = new FileLocation();
                PostOutput output = new PostOutput();
                filelocation.CopyFrom(Input);
                filelocation = _iFileLocationRepository.InsertFileLocation(filelocation);
                output.CopyFrom(filelocation);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FileLocation filelocationinput = new FileLocation();
                FileLocation filelocationoutput = new FileLocation();
                PutOutput output = new PutOutput();
                filelocationinput.CopyFrom(Input);
                FileLocation filelocation = _iFileLocationRepository.GetFileLocation(filelocationinput.FileLocationId);
                if (filelocation!=null)
                {
                    filelocationoutput = _iFileLocationRepository.UpdateFileLocation(filelocationinput);
                    if(filelocationoutput!=null)
                    {
                        output.CopyFrom(filelocationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filelocationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filelocationid))
            {
				 bool IsDeleted = _iFileLocationRepository.DeleteFileLocation(filelocationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public bool DeleteFileLocationByNewsFileId(int newsFileId)
        {
            return _iFileLocationRepository.DeleteFileLocationByNewsFileId(newsFileId);
        }
    }
	
	
}
