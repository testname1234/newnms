﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileResource;
using Validation;
using System.Linq;
using NMS.Core;
using System.Net;
using NMS.Core.Helper;

namespace NMS.Service
{
		
	public class FileResourceService : IFileResourceService 
	{
		private IFileResourceRepository _iFileResourceRepository;
        
		public FileResourceService(IFileResourceRepository iFileResourceRepository)
		{
			this._iFileResourceRepository = iFileResourceRepository;
		}
        
        public Dictionary<string, string> GetFileResourceBasicSearchColumns()
        {
            
            return this._iFileResourceRepository.GetFileResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFileResourceAdvanceSearchColumns()
        {
            
            return this._iFileResourceRepository.GetFileResourceAdvanceSearchColumns();
           
        }
        

		public virtual List<FileResource> GetFileResourceByNewsFileId(System.Int32 NewsFileId)
		{
			return _iFileResourceRepository.GetFileResourceByNewsFileId(NewsFileId);
		}

		public FileResource GetFileResource(System.Int32 FileResourceId)
		{
			return _iFileResourceRepository.GetFileResource(FileResourceId);
		}

		public FileResource UpdateFileResource(FileResource entity)
		{
			return _iFileResourceRepository.UpdateFileResource(entity);
		}

		public bool DeleteFileResource(System.Int32 FileResourceId)
		{
			return _iFileResourceRepository.DeleteFileResource(FileResourceId);
		}

		public List<FileResource> GetAllFileResource()
		{
			return _iFileResourceRepository.GetAllFileResource();
		}

		public FileResource InsertFileResource(FileResource entity)
		{
			 return _iFileResourceRepository.InsertFileResource(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 fileresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out fileresourceid))
            {
				FileResource fileresource = _iFileResourceRepository.GetFileResource(fileresourceid);
                if(fileresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(fileresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FileResource> fileresourcelist = _iFileResourceRepository.GetAllFileResource();
            if (fileresourcelist != null && fileresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(fileresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FileResource fileresource = new FileResource();
                PostOutput output = new PostOutput();
                fileresource.CopyFrom(Input);
                fileresource = _iFileResourceRepository.InsertFileResource(fileresource);
                output.CopyFrom(fileresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FileResource fileresourceinput = new FileResource();
                FileResource fileresourceoutput = new FileResource();
                PutOutput output = new PutOutput();
                fileresourceinput.CopyFrom(Input);
                FileResource fileresource = _iFileResourceRepository.GetFileResource(fileresourceinput.FileResourceId);
                if (fileresource!=null)
                {
                    fileresourceoutput = _iFileResourceRepository.UpdateFileResource(fileresourceinput);
                    if(fileresourceoutput!=null)
                    {
                        output.CopyFrom(fileresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 fileresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out fileresourceid))
            {
				 bool IsDeleted = _iFileResourceRepository.DeleteFileResource(fileresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        
    }
	
	
}
