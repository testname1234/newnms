﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileStatusHistory;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class FileStatusHistoryService : IFileStatusHistoryService 
	{
		private IFileStatusHistoryRepository _iFileStatusHistoryRepository;
        
		public FileStatusHistoryService(IFileStatusHistoryRepository iFileStatusHistoryRepository)
		{
			this._iFileStatusHistoryRepository = iFileStatusHistoryRepository;
		}
        
        public Dictionary<string, string> GetFileStatusHistoryBasicSearchColumns()
        {
            
            return this._iFileStatusHistoryRepository.GetFileStatusHistoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFileStatusHistoryAdvanceSearchColumns()
        {
            
            return this._iFileStatusHistoryRepository.GetFileStatusHistoryAdvanceSearchColumns();
           
        }
        

		public virtual List<FileStatusHistory> GetFileStatusHistoryByNewsFileId(System.Int32 NewsFileId)
		{
			return _iFileStatusHistoryRepository.GetFileStatusHistoryByNewsFileId(NewsFileId);
		}

		public FileStatusHistory GetFileStatusHistory(System.Int32 FileStatusHistoryId)
		{
			return _iFileStatusHistoryRepository.GetFileStatusHistory(FileStatusHistoryId);
		}

		public FileStatusHistory UpdateFileStatusHistory(FileStatusHistory entity)
		{
			return _iFileStatusHistoryRepository.UpdateFileStatusHistory(entity);
		}

		public bool DeleteFileStatusHistory(System.Int32 FileStatusHistoryId)
		{
			return _iFileStatusHistoryRepository.DeleteFileStatusHistory(FileStatusHistoryId);
		}

		public List<FileStatusHistory> GetAllFileStatusHistory()
		{
			return _iFileStatusHistoryRepository.GetAllFileStatusHistory();
		}

		public FileStatusHistory InsertFileStatusHistory(FileStatusHistory entity)
		{
			 return _iFileStatusHistoryRepository.InsertFileStatusHistory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filestatushistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filestatushistoryid))
            {
				FileStatusHistory filestatushistory = _iFileStatusHistoryRepository.GetFileStatusHistory(filestatushistoryid);
                if(filestatushistory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filestatushistory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FileStatusHistory> filestatushistorylist = _iFileStatusHistoryRepository.GetAllFileStatusHistory();
            if (filestatushistorylist != null && filestatushistorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(filestatushistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FileStatusHistory filestatushistory = new FileStatusHistory();
                PostOutput output = new PostOutput();
                filestatushistory.CopyFrom(Input);
                filestatushistory = _iFileStatusHistoryRepository.InsertFileStatusHistory(filestatushistory);
                output.CopyFrom(filestatushistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FileStatusHistory filestatushistoryinput = new FileStatusHistory();
                FileStatusHistory filestatushistoryoutput = new FileStatusHistory();
                PutOutput output = new PutOutput();
                filestatushistoryinput.CopyFrom(Input);
                FileStatusHistory filestatushistory = _iFileStatusHistoryRepository.GetFileStatusHistory(filestatushistoryinput.FileStatusHistoryId);
                if (filestatushistory!=null)
                {
                    filestatushistoryoutput = _iFileStatusHistoryRepository.UpdateFileStatusHistory(filestatushistoryinput);
                    if(filestatushistoryoutput!=null)
                    {
                        output.CopyFrom(filestatushistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filestatushistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filestatushistoryid))
            {
				 bool IsDeleted = _iFileStatusHistoryRepository.DeleteFileStatusHistory(filestatushistoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
