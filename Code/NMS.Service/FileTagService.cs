﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FileTag;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class FileTagService : IFileTagService 
	{
		private IFileTagRepository _iFileTagRepository;
        
		public FileTagService(IFileTagRepository iFileTagRepository)
		{
			this._iFileTagRepository = iFileTagRepository;
		}
        
        public Dictionary<string, string> GetFileTagBasicSearchColumns()
        {
            
            return this._iFileTagRepository.GetFileTagBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFileTagAdvanceSearchColumns()
        {
            
            return this._iFileTagRepository.GetFileTagAdvanceSearchColumns();
           
        }
        

		public virtual List<FileTag> GetFileTagByNewsFileId(System.Int32 NewsFileId)
		{
			return _iFileTagRepository.GetFileTagByNewsFileId(NewsFileId);
		}

		public virtual List<FileTag> GetFileTagByTagId(System.Int32 TagId)
		{
			return _iFileTagRepository.GetFileTagByTagId(TagId);
		}

		public FileTag GetFileTag(System.Int32 FileTagId)
		{
			return _iFileTagRepository.GetFileTag(FileTagId);
		}

		public FileTag UpdateFileTag(FileTag entity)
		{
			return _iFileTagRepository.UpdateFileTag(entity);
		}

		public bool DeleteFileTag(System.Int32 FileTagId)
		{
			return _iFileTagRepository.DeleteFileTag(FileTagId);
		}

		public List<FileTag> GetAllFileTag()
		{
			return _iFileTagRepository.GetAllFileTag();
		}

		public FileTag InsertFileTag(FileTag entity)
		{
			 return _iFileTagRepository.InsertFileTag(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filetagid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filetagid))
            {
				FileTag filetag = _iFileTagRepository.GetFileTag(filetagid);
                if(filetag!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filetag);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FileTag> filetaglist = _iFileTagRepository.GetAllFileTag();
            if (filetaglist != null && filetaglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(filetaglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FileTag filetag = new FileTag();
                PostOutput output = new PostOutput();
                filetag.CopyFrom(Input);
                filetag = _iFileTagRepository.InsertFileTag(filetag);
                output.CopyFrom(filetag);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FileTag filetaginput = new FileTag();
                FileTag filetagoutput = new FileTag();
                PutOutput output = new PutOutput();
                filetaginput.CopyFrom(Input);
                FileTag filetag = _iFileTagRepository.GetFileTag(filetaginput.FileTagId);
                if (filetag!=null)
                {
                    filetagoutput = _iFileTagRepository.UpdateFileTag(filetaginput);
                    if(filetagoutput!=null)
                    {
                        output.CopyFrom(filetagoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filetagid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filetagid))
            {
				 bool IsDeleted = _iFileTagRepository.DeleteFileTag(filetagid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
