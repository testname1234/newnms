﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Filter;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Enums;

namespace NMS.Service
{

    public class FilterService : IFilterService
    {
        private IFilterRepository _iFilterRepository;

        public FilterService(IFilterRepository iFilterRepository)
        {
            this._iFilterRepository = iFilterRepository;
        }

        public Dictionary<string, string> GetFilterBasicSearchColumns()
        {

            return this._iFilterRepository.GetFilterBasicSearchColumns();

        }

        public List<SearchColumn> GetFilterAdvanceSearchColumns()
        {

            return this._iFilterRepository.GetFilterAdvanceSearchColumns();

        }


        public virtual List<Filter> GetFilterByFilterTypeId(System.Int32 FilterTypeId)
        {
            return _iFilterRepository.GetFilterByFilterTypeId(FilterTypeId);
        }

        public Filter GetFilter(System.Int32 FilterId)
        {
            return _iFilterRepository.GetFilter(FilterId);
        }

        public Filter UpdateFilter(Filter entity)
        {
            return _iFilterRepository.UpdateFilter(entity);
        }

        public bool DeleteFilter(System.Int32 FilterId)
        {
            return _iFilterRepository.DeleteFilter(FilterId);
        }

        public List<Filter> GetAllFilter()
        {
            return _iFilterRepository.GetAllFilter();
        }

        public List<GetOutput> GetAllFilterInHierarchy()
        {
            List<GetOutput> output = new List<GetOutput>();

            List<Filter> filters = _iFilterRepository.GetAllFilter();

            List<GetOutput> lst = new List<GetOutput>();
            foreach (var filter in filters)
            {
                lst.Add(new GetOutput());
                lst.Last().CopyFrom(filter);
                lst.Last().FilterId = filter.FilterId;
            }
            foreach (var item in lst.Where(x => !x.ParentId.HasValue))
            {
                FillChildren(item, lst);
                output.Add(item);
            }
            output.Add(new GetOutput() { FilterTypeId = (int)FilterTypes.Category, Name = "Category" });
            output.Last().Children = output.Where(x => x.FilterTypeId == (int)FilterTypes.Category && x.Name != "Category").ToList();
            output.RemoveAll(x => x.FilterTypeId == (int)FilterTypes.Category && x.Name != "Category");
            return output;
        }

        private void FillChildren(GetOutput output, List<GetOutput> filters)
        {
            output.Children = filters.Where(x => x.ParentId == output.FilterId).ToList();
            foreach (var item in output.Children)
            {
                FillChildren(item, filters);
            }
        }

        public Filter InsertFilter(Filter entity)
        {
            return _iFilterRepository.InsertFilter(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filterid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out filterid))
            {
                Filter filter = _iFilterRepository.GetFilter(filterid);
                if (filter != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filter);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Filter> filterlist = _iFilterRepository.GetAllFilter().OrderBy(x => x.ParentId).ToList();

            filterlist.RemoveAll(x => !x.IsApproved.HasValue || !x.IsApproved.Value);

            //Bringing Others to last of list 
            var index = filterlist.FindIndex(x => x.FilterId == 125);
            var item = filterlist[index];
            filterlist[index] = filterlist[filterlist.Count - 1];
            filterlist[filterlist.Count - 1] = item;

            if (filterlist != null && filterlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                filterlist = filterlist.Where(x => x.IsApproved.HasValue && x.IsApproved.Value).ToList();
                outputlist.CopyFrom(filterlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Filter filter = new Filter();
                PostOutput output = new PostOutput();
                filter.CopyFrom(Input);
                filter = _iFilterRepository.InsertFilter(filter);
                output.CopyFrom(filter);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Filter filterinput = new Filter();
                Filter filteroutput = new Filter();
                PutOutput output = new PutOutput();
                filterinput.CopyFrom(Input);
                Filter filter = _iFilterRepository.GetFilter(filterinput.FilterId);
                if (filter != null)
                {
                    filteroutput = _iFilterRepository.UpdateFilter(filterinput);
                    if (filteroutput != null)
                    {
                        output.CopyFrom(filteroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filterid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out filterid))
            {
                bool IsDeleted = _iFilterRepository.DeleteFilter(filterid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<Filter> GetFilterByLastUpdateDate(DateTime filterLastUpdateDate)
        {
            return _iFilterRepository.GetFilterByLastUpdateDate(filterLastUpdateDate);
        }


        public Filter GetFilterByNameAndFilterType(FilterTypes filterType, string name)
        {
            return _iFilterRepository.GetFilterByNameAndFilterType(filterType,name);
        }


        public Filter GetFilterCreateIfNotExist(Filter filter)
        { 
             return _iFilterRepository.GetFilterCreateIfNotExist(filter);
        }
        public List<Filter> GetAllParentFilters()
        {
            return _iFilterRepository.GetAllParentFilters();
        }
        
}


}
