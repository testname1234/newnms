﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.FilterType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class FilterTypeService : IFilterTypeService 
	{
		private IFilterTypeRepository _iFilterTypeRepository;
        
		public FilterTypeService(IFilterTypeRepository iFilterTypeRepository)
		{
			this._iFilterTypeRepository = iFilterTypeRepository;
		}
        
        public Dictionary<string, string> GetFilterTypeBasicSearchColumns()
        {
            
            return this._iFilterTypeRepository.GetFilterTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFilterTypeAdvanceSearchColumns()
        {
            
            return this._iFilterTypeRepository.GetFilterTypeAdvanceSearchColumns();
           
        }
        

		public FilterType GetFilterType(System.Int32 FilterTypeId)
		{
			return _iFilterTypeRepository.GetFilterType(FilterTypeId);
		}

		public FilterType UpdateFilterType(FilterType entity)
		{
			return _iFilterTypeRepository.UpdateFilterType(entity);
		}

		public bool DeleteFilterType(System.Int32 FilterTypeId)
		{
			return _iFilterTypeRepository.DeleteFilterType(FilterTypeId);
		}

		public List<FilterType> GetAllFilterType()
		{
			return _iFilterTypeRepository.GetAllFilterType();
		}

		public FilterType InsertFilterType(FilterType entity)
		{
			 return _iFilterTypeRepository.InsertFilterType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 filtertypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filtertypeid))
            {
				FilterType filtertype = _iFilterTypeRepository.GetFilterType(filtertypeid);
                if(filtertype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(filtertype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<FilterType> filtertypelist = _iFilterTypeRepository.GetAllFilterType();
            if (filtertypelist != null && filtertypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(filtertypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                FilterType filtertype = new FilterType();
                PostOutput output = new PostOutput();
                filtertype.CopyFrom(Input);
                filtertype = _iFilterTypeRepository.InsertFilterType(filtertype);
                output.CopyFrom(filtertype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                FilterType filtertypeinput = new FilterType();
                FilterType filtertypeoutput = new FilterType();
                PutOutput output = new PutOutput();
                filtertypeinput.CopyFrom(Input);
                FilterType filtertype = _iFilterTypeRepository.GetFilterType(filtertypeinput.FilterTypeId);
                if (filtertype!=null)
                {
                    filtertypeoutput = _iFilterTypeRepository.UpdateFilterType(filtertypeinput);
                    if(filtertypeoutput!=null)
                    {
                        output.CopyFrom(filtertypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 filtertypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out filtertypeid))
            {
				 bool IsDeleted = _iFilterTypeRepository.DeleteFilterType(filtertypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
