﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Folder;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class FolderService : IFolderService 
	{
		private IFolderRepository _iFolderRepository;
        private IEpisodeRepository _iEpisodeRepository;
        
		public FolderService(IFolderRepository iFolderRepository, IEpisodeRepository iEpisodeRepository)
		{
			this._iFolderRepository = iFolderRepository;
            this._iEpisodeRepository = iEpisodeRepository;
		}
        
        public Dictionary<string, string> GetFolderBasicSearchColumns()
        {
            
            return this._iFolderRepository.GetFolderBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetFolderAdvanceSearchColumns()
        {
            
            return this._iFolderRepository.GetFolderAdvanceSearchColumns();
           
        }
        

		public Folder GetFolder(System.Int32 FolderId)
		{
			return _iFolderRepository.GetFolder(FolderId);
		}

		public Folder UpdateFolder(Folder entity)
		{
			return _iFolderRepository.UpdateFolder(entity);
		}

		public bool DeleteFolder(System.Int32 FolderId)
		{
			return _iFolderRepository.DeleteFolder(FolderId);
		}

		public List<Folder> GetAllFolder()
		{
			return _iFolderRepository.GetAllFolder();
		}

		public Folder InsertFolder(Folder entity)
		{
			 return _iFolderRepository.InsertFolder(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 folderid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out folderid))
            {
				Folder folder = _iFolderRepository.GetFolder(folderid);
                if(folder!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(folder);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Folder> folderlist = _iFolderRepository.GetAllFolder();
            if (folderlist != null && folderlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(folderlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Folder folder = new Folder();
                PostOutput output = new PostOutput();
                folder.CopyFrom(Input);
                folder = _iFolderRepository.InsertFolder(folder);
                output.CopyFrom(folder);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Folder folderinput = new Folder();
                Folder folderoutput = new Folder();
                PutOutput output = new PutOutput();
                folderinput.CopyFrom(Input);
                Folder folder = _iFolderRepository.GetFolder(folderinput.FolderId);
                if (folder!=null)
                {
                    folderoutput = _iFolderRepository.UpdateFolder(folderinput);
                    if(folderoutput!=null)
                    {
                        output.CopyFrom(folderoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 folderid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out folderid))
            {
				 bool IsDeleted = _iFolderRepository.DeleteFolder(folderid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public Folder CreateRundownFolder(PostInput input)
         {
             Folder folder = new Folder();
             folder.CopyFrom(input);
             folder.IsRundown = true;
             folder.CreationDate = DateTime.UtcNow;
             if (!_iFolderRepository.EpisodeFolderExist(folder.EpisodeId.Value))
             {
                 Episode episode = _iEpisodeRepository.GetEpisodeWithProgramName(folder.EpisodeId.Value);
                 folder.Name = episode.ProgramName.Replace(" ", "") + "-" + episode.From.ToString("ddMMMyyyy-hh:mm");
                 folder.ProgramId = episode.ProgramId;
                 folder.StartTime = episode.From;
                 folder.EndTime = episode.To;
                 return _iFolderRepository.InsertFolder(folder);
             }
             else return _iFolderRepository.GetByEpisodeId(folder.EpisodeId.Value).First();
         }
    }
	
	
}
