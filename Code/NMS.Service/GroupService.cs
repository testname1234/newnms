﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Group;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class GroupService : IGroupService 
	{
		private IGroupRepository _iGroupRepository;
        
		public GroupService(IGroupRepository iGroupRepository)
		{
			this._iGroupRepository = iGroupRepository;
		}
        
        public Dictionary<string, string> GetGroupBasicSearchColumns()
        {
            
            return this._iGroupRepository.GetGroupBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetGroupAdvanceSearchColumns()
        {
            
            return this._iGroupRepository.GetGroupAdvanceSearchColumns();
           
        }
        

		public Group GetGroup(System.Int32 GroupId)
		{
			return _iGroupRepository.GetGroup(GroupId);
		}

		public Group UpdateGroup(Group entity)
		{
			return _iGroupRepository.UpdateGroup(entity);
		}

		public bool DeleteGroup(System.Int32 GroupId)
		{
			return _iGroupRepository.DeleteGroup(GroupId);
		}

		public List<Group> GetAllGroup()
		{
			return _iGroupRepository.GetAllGroup();
		}

		public Group InsertGroup(Group entity)
		{
			 return _iGroupRepository.InsertGroup(entity);
		}


        public bool DeleteGroupWithGroupUsers(int groupId)
        {
            IGroupUserRepository groupUserRep = IoC.Resolve<IGroupUserRepository>("GroupUserRepository");
            groupUserRep.DeleteGroupUserByGroupId(groupId);
            return _iGroupRepository.DeleteGroup(groupId);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 groupid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out groupid))
            {
				Group group = _iGroupRepository.GetGroup(groupid);
                if(group!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(group);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Group> grouplist = _iGroupRepository.GetAllGroup();
            if (grouplist != null && grouplist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(grouplist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Group group = new Group();
                PostOutput output = new PostOutput();
                group.CopyFrom(Input);
                group = _iGroupRepository.InsertGroup(group);
                output.CopyFrom(group);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Group groupinput = new Group();
                Group groupoutput = new Group();
                PutOutput output = new PutOutput();
                groupinput.CopyFrom(Input);
                Group group = _iGroupRepository.GetGroup(groupinput.GroupId);
                if (group!=null)
                {
                    groupoutput = _iGroupRepository.UpdateGroup(groupinput);
                    if(groupoutput!=null)
                    {
                        output.CopyFrom(groupoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 groupid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out groupid))
            {
				 bool IsDeleted = _iGroupRepository.DeleteGroup(groupid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public bool AddNewGroupWithUserIds(string GroupName, List<int> Users)
         {
             bool res = false;
             Group grp = new Group();
             grp.GroupName = GroupName;
             grp = InsertGroup(grp);
             if (grp != null)
             {
                 IGroupUserRepository groupUserRep = IoC.Resolve<IGroupUserRepository>("GroupUserRepository");
                 for (int i = 0; i < Users.Count; i++)
                 {
                     GroupUser gu = new GroupUser();
                     gu.UserId = Users[i];
                     gu.GroupId = grp.GroupId;
                     res = groupUserRep.InsertGroupUser(gu) != null ? true : false;
                 }
             }
             return res;
         }

         public bool EditGroupWithUserIds(int GroupId, List<int> Users, List<int> oldUsersIds)
         {
             bool res = false;
             Group grp = new Group();
             grp.GroupId = GroupId;
             grp = GetGroup(grp.GroupId);
             if (grp != null)
             {
                 IGroupUserRepository groupUserRep = IoC.Resolve<IGroupUserRepository>("GroupUserRepository");
                 if (Users != null && Users.Count > 0)
                 {
                     if (oldUsersIds != null && oldUsersIds.Count > 0)
                     {
                         for (int x = 0; x < oldUsersIds.Count; x++)
                         {
                             if (Users.Contains(oldUsersIds[x]))
                             {
                             }
                             else
                             {
                                 groupUserRep.RemoveUserFromGroup(oldUsersIds[x], GroupId);
                             }
                         }
                     }
                     for (int i = 0; i < Users.Count; i++)
                     {
                         if (oldUsersIds.Contains(Users[i]))
                         {
                         }
                         else
                         {
                             GroupUser gu = new GroupUser();
                             gu.UserId = Users[i];
                             gu.GroupId = grp.GroupId;
                             res = groupUserRep.InsertGroupUser(gu) != null ? true : false;
                         }
                     }
                 }
             }
             return true;
         }

        

        public List<Group> GetAllGroupWithUserIds()
        {
            List<Group> groups = GetAllGroup();
            if (groups != null)
            {
                IGroupUserRepository groupUserRep = IoC.Resolve<IGroupUserRepository>("GroupUserRepository");
                List<GroupUser> gUsers = groupUserRep.GetAllGroupUser();
                foreach (Group gp in groups)
                {
                    List<GroupUser> userIds = gUsers.Where(x => x.GroupId == gp.GroupId).ToList();
                    if (userIds != null)
                        gp.UserIds = userIds.Select(j => j.UserId.Value).ToList();
                }
            }
            return groups;
        }
   
	}
	
	
}
