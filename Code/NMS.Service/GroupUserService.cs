﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.GroupUser;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class GroupUserService : IGroupUserService 
	{
		private IGroupUserRepository _iGroupUserRepository;
        
		public GroupUserService(IGroupUserRepository iGroupUserRepository)
		{
			this._iGroupUserRepository = iGroupUserRepository;
		}
        
        public Dictionary<string, string> GetGroupUserBasicSearchColumns()
        {
            
            return this._iGroupUserRepository.GetGroupUserBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetGroupUserAdvanceSearchColumns()
        {
            
            return this._iGroupUserRepository.GetGroupUserAdvanceSearchColumns();
           
        }
        

		public GroupUser GetGroupUser(System.Int32 GroupUserId)
		{
			return _iGroupUserRepository.GetGroupUser(GroupUserId);
		}

		public GroupUser UpdateGroupUser(GroupUser entity)
		{
			return _iGroupUserRepository.UpdateGroupUser(entity);
		}

		public bool DeleteGroupUser(System.Int32 GroupUserId)
		{
			return _iGroupUserRepository.DeleteGroupUser(GroupUserId);
		}

		public List<GroupUser> GetAllGroupUser()
		{
			return _iGroupUserRepository.GetAllGroupUser();
		}

		public GroupUser InsertGroupUser(GroupUser entity)
		{
			 return _iGroupUserRepository.InsertGroupUser(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 groupuserid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out groupuserid))
            {
				GroupUser groupuser = _iGroupUserRepository.GetGroupUser(groupuserid);
                if(groupuser!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(groupuser);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<GroupUser> groupuserlist = _iGroupUserRepository.GetAllGroupUser();
            if (groupuserlist != null && groupuserlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(groupuserlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                GroupUser groupuser = new GroupUser();
                PostOutput output = new PostOutput();
                groupuser.CopyFrom(Input);
                groupuser = _iGroupUserRepository.InsertGroupUser(groupuser);
                output.CopyFrom(groupuser);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                GroupUser groupuserinput = new GroupUser();
                GroupUser groupuseroutput = new GroupUser();
                PutOutput output = new PutOutput();
                groupuserinput.CopyFrom(Input);
                GroupUser groupuser = _iGroupUserRepository.GetGroupUser(groupuserinput.GroupUserId);
                if (groupuser!=null)
                {
                    groupuseroutput = _iGroupUserRepository.UpdateGroupUser(groupuserinput);
                    if(groupuseroutput!=null)
                    {
                        output.CopyFrom(groupuseroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 groupuserid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out groupuserid))
            {
				 bool IsDeleted = _iGroupUserRepository.DeleteGroupUser(groupuserid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
