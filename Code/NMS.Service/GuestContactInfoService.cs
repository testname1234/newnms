﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.GuestContactInfo;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class GuestContactInfoService : IGuestContactInfoService 
	{
		private IGuestContactInfoRepository _iGuestContactInfoRepository;
        
		public GuestContactInfoService(IGuestContactInfoRepository iGuestContactInfoRepository)
		{
			this._iGuestContactInfoRepository = iGuestContactInfoRepository;
		}
        
        public Dictionary<string, string> GetGuestContactInfoBasicSearchColumns()
        {
            
            return this._iGuestContactInfoRepository.GetGuestContactInfoBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetGuestContactInfoAdvanceSearchColumns()
        {
            
            return this._iGuestContactInfoRepository.GetGuestContactInfoAdvanceSearchColumns();
           
        }
        

		public virtual List<GuestContactInfo> GetGuestContactInfoByCelebrityId(System.Int32? CelebrityId)
		{
			return _iGuestContactInfoRepository.GetGuestContactInfoByCelebrityId(CelebrityId);
		}

		public GuestContactInfo GetGuestContactInfo(System.Int32 GuestContactInfoId)
		{
			return _iGuestContactInfoRepository.GetGuestContactInfo(GuestContactInfoId);
		}

		public GuestContactInfo UpdateGuestContactInfo(GuestContactInfo entity)
		{
			return _iGuestContactInfoRepository.UpdateGuestContactInfo(entity);
		}

		public bool DeleteGuestContactInfo(System.Int32 GuestContactInfoId)
		{
			return _iGuestContactInfoRepository.DeleteGuestContactInfo(GuestContactInfoId);
		}

		public List<GuestContactInfo> GetAllGuestContactInfo()
		{
			return _iGuestContactInfoRepository.GetAllGuestContactInfo();
		}

		public GuestContactInfo InsertGuestContactInfo(GuestContactInfo entity)
		{
			 return _iGuestContactInfoRepository.InsertGuestContactInfo(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 guestcontactinfoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out guestcontactinfoid))
            {
				GuestContactInfo guestcontactinfo = _iGuestContactInfoRepository.GetGuestContactInfo(guestcontactinfoid);
                if(guestcontactinfo!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(guestcontactinfo);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<GuestContactInfo> guestcontactinfolist = _iGuestContactInfoRepository.GetAllGuestContactInfo();
            if (guestcontactinfolist != null && guestcontactinfolist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(guestcontactinfolist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                GuestContactInfo guestcontactinfo = new GuestContactInfo();
                PostOutput output = new PostOutput();
                guestcontactinfo.CopyFrom(Input);
                guestcontactinfo = _iGuestContactInfoRepository.InsertGuestContactInfo(guestcontactinfo);
                output.CopyFrom(guestcontactinfo);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                GuestContactInfo guestcontactinfoinput = new GuestContactInfo();
                GuestContactInfo guestcontactinfooutput = new GuestContactInfo();
                PutOutput output = new PutOutput();
                guestcontactinfoinput.CopyFrom(Input);
                GuestContactInfo guestcontactinfo = _iGuestContactInfoRepository.GetGuestContactInfo(guestcontactinfoinput.GuestContactInfoId);
                if (guestcontactinfo!=null)
                {
                    guestcontactinfooutput = _iGuestContactInfoRepository.UpdateGuestContactInfo(guestcontactinfoinput);
                    if(guestcontactinfooutput!=null)
                    {
                        output.CopyFrom(guestcontactinfooutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 guestcontactinfoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out guestcontactinfoid))
            {
				 bool IsDeleted = _iGuestContactInfoRepository.DeleteGuestContactInfo(guestcontactinfoid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
