﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Guest;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class GuestService : IGuestService 
	{
		private IGuestRepository _iGuestRepository;
        
		public GuestService(IGuestRepository iGuestRepository)
		{
			this._iGuestRepository = iGuestRepository;
		}
        
        public Dictionary<string, string> GetGuestBasicSearchColumns()
        {
            
            return this._iGuestRepository.GetGuestBasicSearchColumns();
           
        }

       

        public List<SearchColumn> GetGuestAdvanceSearchColumns()
        {
            
            return this._iGuestRepository.GetGuestAdvanceSearchColumns();
           
        }
        

		public Guest GetGuest(System.Int32 GuestId)
		{
			return _iGuestRepository.GetGuest(GuestId);
		}

		public Guest UpdateGuest(Guest entity)
		{
			return _iGuestRepository.UpdateGuest(entity);
		}

		public bool DeleteGuest(System.Int32 GuestId)
		{
			return _iGuestRepository.DeleteGuest(GuestId);
		}

		public List<Guest> GetAllGuest()
		{
			return _iGuestRepository.GetAllGuest();
		}

		public Guest InsertGuest(Guest entity)
		{
			 return _iGuestRepository.InsertGuest(entity);
		}

        public List<Guest> GetAllGuestBySlotId(int slotId)
        {
            return _iGuestRepository.GetAllGuestBySlotId(slotId);
        }

        public bool DeleteAllGuestBySlotId(System.Int32 SlotId)
        {
            return _iGuestRepository.DeleteAllGuestBySlotId(SlotId);
        }


        public List<Guest> GetGuestEpisodeReport()
        {
            var report = _iGuestRepository.GetGuestEpisodeReport();
            if (report != null && report.Count > 0)
            {

                for (int i = 0; i <= report.Count - 1; i++)
                {
                    if (report[i].guestduration > 0)
                    {
                        report[i].guestEndTime = report[i].guestStartTime;
                        report[i].guestEndTime = report[i].guestStartTime.AddSeconds(report[i].guestduration);
                    }
                }
            }

            return report;
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 guestid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out guestid))
            {
				Guest guest = _iGuestRepository.GetGuest(guestid);
                if(guest!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(guest);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Guest> guestlist = _iGuestRepository.GetAllGuest();
            if (guestlist != null && guestlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(guestlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Guest guest = new Guest();
                PostOutput output = new PostOutput();
                guest.CopyFrom(Input);
                guest.CreationDate = DateTime.UtcNow;
                guest.LastUpdateDate = DateTime.UtcNow;
                guest = _iGuestRepository.InsertGuest(guest);
                output.CopyFrom(guest);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Guest guestinput = new Guest();
                Guest guestoutput = new Guest();
                PutOutput output = new PutOutput();
                guestinput.CopyFrom(Input);
                Guest guest = _iGuestRepository.GetGuest(guestinput.GuestId);
                if (guest!=null)
                {
                    guestoutput = _iGuestRepository.UpdateGuest(guestinput);
                    if(guestoutput!=null)
                    {
                        output.CopyFrom(guestoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 guestid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out guestid))
            {
				 bool IsDeleted = _iGuestRepository.DeleteGuest(guestid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
