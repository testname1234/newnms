﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.LocationAlias;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class LocationAliasService : ILocationAliasService 
	{
		private ILocationAliasRepository _iLocationAliasRepository;
        
		public LocationAliasService(ILocationAliasRepository iLocationAliasRepository)
		{
			this._iLocationAliasRepository = iLocationAliasRepository;
		}
        
        public Dictionary<string, string> GetLocationAliasBasicSearchColumns()
        {
            
            return this._iLocationAliasRepository.GetLocationAliasBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetLocationAliasAdvanceSearchColumns()
        {
            
            return this._iLocationAliasRepository.GetLocationAliasAdvanceSearchColumns();
           
        }
        

		public virtual List<LocationAlias> GetLocationAliasByLocationId(System.Int32 LocationId)
		{
			return _iLocationAliasRepository.GetLocationAliasByLocationId(LocationId);
		}

		public LocationAlias GetLocationAlias(System.Int32 LocationAliasId)
		{
			return _iLocationAliasRepository.GetLocationAlias(LocationAliasId);
		}

		public LocationAlias UpdateLocationAlias(LocationAlias entity)
		{
			return _iLocationAliasRepository.UpdateLocationAlias(entity);
		}

		public bool DeleteLocationAlias(System.Int32 LocationAliasId)
		{
			return _iLocationAliasRepository.DeleteLocationAlias(LocationAliasId);
		}

		public List<LocationAlias> GetAllLocationAlias()
		{
			return _iLocationAliasRepository.GetAllLocationAlias();
		}



        public LocationAlias InsertLocationAliasIfNotExists(LocationAlias entity)
        {
            if (!_iLocationAliasRepository.Exists(entity))
                return _iLocationAliasRepository.InsertLocationAlias(entity);
            else
                return _iLocationAliasRepository.GetByAlias(entity.Alias);
        }

		public LocationAlias InsertLocationAlias(LocationAlias entity)
		{
			 return _iLocationAliasRepository.InsertLocationAlias(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 locationaliasid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out locationaliasid))
            {
				LocationAlias locationalias = _iLocationAliasRepository.GetLocationAlias(locationaliasid);
                if(locationalias!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(locationalias);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<LocationAlias> locationaliaslist = _iLocationAliasRepository.GetAllLocationAlias();
            if (locationaliaslist != null && locationaliaslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(locationaliaslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                LocationAlias locationalias = new LocationAlias();
                PostOutput output = new PostOutput();
                locationalias.CopyFrom(Input);
                locationalias = _iLocationAliasRepository.InsertLocationAlias(locationalias);
                output.CopyFrom(locationalias);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                LocationAlias locationaliasinput = new LocationAlias();
                LocationAlias locationaliasoutput = new LocationAlias();
                PutOutput output = new PutOutput();
                locationaliasinput.CopyFrom(Input);
                LocationAlias locationalias = _iLocationAliasRepository.GetLocationAlias(locationaliasinput.LocationAliasId);
                if (locationalias!=null)
                {
                    locationaliasoutput = _iLocationAliasRepository.UpdateLocationAlias(locationaliasinput);
                    if(locationaliasoutput!=null)
                    {
                        output.CopyFrom(locationaliasoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 locationaliasid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out locationaliasid))
            {
				 bool IsDeleted = _iLocationAliasRepository.DeleteLocationAlias(locationaliasid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
