﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Location;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class LocationService : ILocationService 
	{
		private ILocationRepository _iLocationRepository;
        
		public LocationService(ILocationRepository iLocationRepository)
		{
			this._iLocationRepository = iLocationRepository;
		}
        
        public Dictionary<string, string> GetLocationBasicSearchColumns()
        {
            
            return this._iLocationRepository.GetLocationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetLocationAdvanceSearchColumns()
        {
            
            return this._iLocationRepository.GetLocationAdvanceSearchColumns();
           
        }
        

		public Location GetLocation(System.Int32 LocationId)
		{
			return _iLocationRepository.GetLocation(LocationId);
		}

		public Location UpdateLocation(Location entity)
		{
			return _iLocationRepository.UpdateLocation(entity);
		}

		public bool DeleteLocation(System.Int32 LocationId)
		{
			return _iLocationRepository.DeleteLocation(LocationId);
		}

		public List<Location> GetAllLocation()
		{
			return _iLocationRepository.GetAllLocation();
		}

		public Location InsertLocation(Location entity)
		{
			 return _iLocationRepository.InsertLocation(entity);
		}

        public Location InsertLocationIfNotExists(Location entity)
        {
            return _iLocationRepository.InsertLocationIfNotExists(entity);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 locationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out locationid))
            {
				Location location = _iLocationRepository.GetLocation(locationid);
                if(location!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(location);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Location> locationlist = _iLocationRepository.GetAllLocation();
            if (locationlist != null && locationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(locationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Location location = new Location();
                PostOutput output = new PostOutput();
                location.CopyFrom(Input);
                location = _iLocationRepository.InsertLocation(location);
                output.CopyFrom(location);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Location locationinput = new Location();
                Location locationoutput = new Location();
                PutOutput output = new PutOutput();
                locationinput.CopyFrom(Input);
                Location location = _iLocationRepository.GetLocation(locationinput.LocationId);
                if (location!=null)
                {
                    locationoutput = _iLocationRepository.UpdateLocation(locationinput);
                    if(locationoutput!=null)
                    {
                        output.CopyFrom(locationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 locationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out locationid))
            {
				 bool IsDeleted = _iLocationRepository.DeleteLocation(locationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<Location> GetLocationByTerm(string term)
         {
             return _iLocationRepository.GetLocationByTerm(term);
         }

         public List<Location> GetByDate(DateTime LastUpdateDate)
         {
             return _iLocationRepository.GetByDate(LastUpdateDate);
         }

        public Location GetLocationByName(string name)
        {
            return _iLocationRepository.GetLocationByName(name);
        }

        public LocationAlias GetByAlias(string alias)
        {
            return _iLocationRepository.GetByAlias(alias);
        }

        public Location GetLocationCreateIfNotExist(Location location)
        {
            return _iLocationRepository.GetLocationCreateIfNotExist(location);
        }
    }
	
	
}
