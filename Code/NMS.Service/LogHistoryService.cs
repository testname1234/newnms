﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.LogHistory;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class LogHistoryService : ILogHistoryService 
	{
		private ILogHistoryRepository _iLogHistoryRepository;
        
		public LogHistoryService(ILogHistoryRepository iLogHistoryRepository)
		{
			this._iLogHistoryRepository = iLogHistoryRepository;
		}
        
        public Dictionary<string, string> GetLogHistoryBasicSearchColumns()
        {
            
            return this._iLogHistoryRepository.GetLogHistoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetLogHistoryAdvanceSearchColumns()
        {
            
            return this._iLogHistoryRepository.GetLogHistoryAdvanceSearchColumns();
           
        }
        

		public LogHistory GetLogHistory(System.Int32 LogHistoryId)
		{
			return _iLogHistoryRepository.GetLogHistory(LogHistoryId);
		}

		public LogHistory UpdateLogHistory(LogHistory entity)
		{
			return _iLogHistoryRepository.UpdateLogHistory(entity);
		}

		public bool DeleteLogHistory(System.Int32 LogHistoryId)
		{
			return _iLogHistoryRepository.DeleteLogHistory(LogHistoryId);
		}

		public List<LogHistory> GetAllLogHistory()
		{
			return _iLogHistoryRepository.GetAllLogHistory();
		}

		public LogHistory InsertLogHistory(LogHistory entity)
		{
			 return _iLogHistoryRepository.InsertLogHistory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 loghistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out loghistoryid))
            {
				LogHistory loghistory = _iLogHistoryRepository.GetLogHistory(loghistoryid);
                if(loghistory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(loghistory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<LogHistory> loghistorylist = _iLogHistoryRepository.GetAllLogHistory();
            if (loghistorylist != null && loghistorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(loghistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                LogHistory loghistory = new LogHistory();
                PostOutput output = new PostOutput();
                loghistory.CopyFrom(Input);
                loghistory = _iLogHistoryRepository.InsertLogHistory(loghistory);
                output.CopyFrom(loghistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                LogHistory loghistoryinput = new LogHistory();
                LogHistory loghistoryoutput = new LogHistory();
                PutOutput output = new PutOutput();
                loghistoryinput.CopyFrom(Input);
                LogHistory loghistory = _iLogHistoryRepository.GetLogHistory(loghistoryinput.LogHistoryId);
                if (loghistory!=null)
                {
                    loghistoryoutput = _iLogHistoryRepository.UpdateLogHistory(loghistoryinput);
                    if(loghistoryoutput!=null)
                    {
                        output.CopyFrom(loghistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 loghistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out loghistoryid))
            {
				 bool IsDeleted = _iLogHistoryRepository.DeleteLogHistory(loghistoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
