﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrBreakingTickerRundown;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrBreakingTickerRundownService : IMcrBreakingTickerRundownService 
	{
		private IMcrBreakingTickerRundownRepository _iMcrBreakingTickerRundownRepository;
        
		public McrBreakingTickerRundownService(IMcrBreakingTickerRundownRepository iMcrBreakingTickerRundownRepository)
		{
			this._iMcrBreakingTickerRundownRepository = iMcrBreakingTickerRundownRepository;
		}
        
        public Dictionary<string, string> GetMcrBreakingTickerRundownBasicSearchColumns()
        {
            
            return this._iMcrBreakingTickerRundownRepository.GetMcrBreakingTickerRundownBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrBreakingTickerRundownAdvanceSearchColumns()
        {
            
            return this._iMcrBreakingTickerRundownRepository.GetMcrBreakingTickerRundownAdvanceSearchColumns();
           
        }
        

		public McrBreakingTickerRundown GetMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId)
		{
			return _iMcrBreakingTickerRundownRepository.GetMcrBreakingTickerRundown(McrBreakingTickerRundownId);
		}

		public McrBreakingTickerRundown UpdateMcrBreakingTickerRundown(McrBreakingTickerRundown entity)
		{
			return _iMcrBreakingTickerRundownRepository.UpdateMcrBreakingTickerRundown(entity);
		}

		public bool DeleteMcrBreakingTickerRundown(System.Int32 McrBreakingTickerRundownId)
		{
			return _iMcrBreakingTickerRundownRepository.DeleteMcrBreakingTickerRundown(McrBreakingTickerRundownId);
		}

		public List<McrBreakingTickerRundown> GetAllMcrBreakingTickerRundown()
		{
			return _iMcrBreakingTickerRundownRepository.GetAllMcrBreakingTickerRundown();
		}

		public McrBreakingTickerRundown InsertMcrBreakingTickerRundown(McrBreakingTickerRundown entity)
		{
			 return _iMcrBreakingTickerRundownRepository.InsertMcrBreakingTickerRundown(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrbreakingtickerrundownid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrbreakingtickerrundownid))
            {
				McrBreakingTickerRundown mcrbreakingtickerrundown = _iMcrBreakingTickerRundownRepository.GetMcrBreakingTickerRundown(mcrbreakingtickerrundownid);
                if(mcrbreakingtickerrundown!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrbreakingtickerrundown);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrBreakingTickerRundown> mcrbreakingtickerrundownlist = _iMcrBreakingTickerRundownRepository.GetAllMcrBreakingTickerRundown();
            if (mcrbreakingtickerrundownlist != null && mcrbreakingtickerrundownlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrbreakingtickerrundownlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrBreakingTickerRundown mcrbreakingtickerrundown = new McrBreakingTickerRundown();
                PostOutput output = new PostOutput();
                mcrbreakingtickerrundown.CopyFrom(Input);
                mcrbreakingtickerrundown = _iMcrBreakingTickerRundownRepository.InsertMcrBreakingTickerRundown(mcrbreakingtickerrundown);
                output.CopyFrom(mcrbreakingtickerrundown);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrBreakingTickerRundown mcrbreakingtickerrundowninput = new McrBreakingTickerRundown();
                McrBreakingTickerRundown mcrbreakingtickerrundownoutput = new McrBreakingTickerRundown();
                PutOutput output = new PutOutput();
                mcrbreakingtickerrundowninput.CopyFrom(Input);
                McrBreakingTickerRundown mcrbreakingtickerrundown = _iMcrBreakingTickerRundownRepository.GetMcrBreakingTickerRundown(mcrbreakingtickerrundowninput.McrBreakingTickerRundownId);
                if (mcrbreakingtickerrundown!=null)
                {
                    mcrbreakingtickerrundownoutput = _iMcrBreakingTickerRundownRepository.UpdateMcrBreakingTickerRundown(mcrbreakingtickerrundowninput);
                    if(mcrbreakingtickerrundownoutput!=null)
                    {
                        output.CopyFrom(mcrbreakingtickerrundownoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrbreakingtickerrundownid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrbreakingtickerrundownid))
            {
				 bool IsDeleted = _iMcrBreakingTickerRundownRepository.DeleteMcrBreakingTickerRundown(mcrbreakingtickerrundownid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
