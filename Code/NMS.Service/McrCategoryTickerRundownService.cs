﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrCategoryTickerRundown;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrCategoryTickerRundownService : IMcrCategoryTickerRundownService 
	{
		private IMcrCategoryTickerRundownRepository _iMcrCategoryTickerRundownRepository;
        
		public McrCategoryTickerRundownService(IMcrCategoryTickerRundownRepository iMcrCategoryTickerRundownRepository)
		{
			this._iMcrCategoryTickerRundownRepository = iMcrCategoryTickerRundownRepository;
		}
        
        public Dictionary<string, string> GetMcrCategoryTickerRundownBasicSearchColumns()
        {
            
            return this._iMcrCategoryTickerRundownRepository.GetMcrCategoryTickerRundownBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrCategoryTickerRundownAdvanceSearchColumns()
        {
            
            return this._iMcrCategoryTickerRundownRepository.GetMcrCategoryTickerRundownAdvanceSearchColumns();
           
        }
        

		public McrCategoryTickerRundown GetMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId)
		{
			return _iMcrCategoryTickerRundownRepository.GetMcrCategoryTickerRundown(McrCategoryTickerRundownId);
		}

		public McrCategoryTickerRundown UpdateMcrCategoryTickerRundown(McrCategoryTickerRundown entity)
		{
			return _iMcrCategoryTickerRundownRepository.UpdateMcrCategoryTickerRundown(entity);
		}

		public bool DeleteMcrCategoryTickerRundown(System.Int32 McrCategoryTickerRundownId)
		{
			return _iMcrCategoryTickerRundownRepository.DeleteMcrCategoryTickerRundown(McrCategoryTickerRundownId);
		}

		public List<McrCategoryTickerRundown> GetAllMcrCategoryTickerRundown()
		{
			return _iMcrCategoryTickerRundownRepository.GetAllMcrCategoryTickerRundown();
		}

		public McrCategoryTickerRundown InsertMcrCategoryTickerRundown(McrCategoryTickerRundown entity)
		{
			 return _iMcrCategoryTickerRundownRepository.InsertMcrCategoryTickerRundown(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrcategorytickerrundownid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrcategorytickerrundownid))
            {
				McrCategoryTickerRundown mcrcategorytickerrundown = _iMcrCategoryTickerRundownRepository.GetMcrCategoryTickerRundown(mcrcategorytickerrundownid);
                if(mcrcategorytickerrundown!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrcategorytickerrundown);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrCategoryTickerRundown> mcrcategorytickerrundownlist = _iMcrCategoryTickerRundownRepository.GetAllMcrCategoryTickerRundown();
            if (mcrcategorytickerrundownlist != null && mcrcategorytickerrundownlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrcategorytickerrundownlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrCategoryTickerRundown mcrcategorytickerrundown = new McrCategoryTickerRundown();
                PostOutput output = new PostOutput();
                mcrcategorytickerrundown.CopyFrom(Input);
                mcrcategorytickerrundown = _iMcrCategoryTickerRundownRepository.InsertMcrCategoryTickerRundown(mcrcategorytickerrundown);
                output.CopyFrom(mcrcategorytickerrundown);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrCategoryTickerRundown mcrcategorytickerrundowninput = new McrCategoryTickerRundown();
                McrCategoryTickerRundown mcrcategorytickerrundownoutput = new McrCategoryTickerRundown();
                PutOutput output = new PutOutput();
                mcrcategorytickerrundowninput.CopyFrom(Input);
                McrCategoryTickerRundown mcrcategorytickerrundown = _iMcrCategoryTickerRundownRepository.GetMcrCategoryTickerRundown(mcrcategorytickerrundowninput.McrCategoryTickerRundownId);
                if (mcrcategorytickerrundown!=null)
                {
                    mcrcategorytickerrundownoutput = _iMcrCategoryTickerRundownRepository.UpdateMcrCategoryTickerRundown(mcrcategorytickerrundowninput);
                    if(mcrcategorytickerrundownoutput!=null)
                    {
                        output.CopyFrom(mcrcategorytickerrundownoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrcategorytickerrundownid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrcategorytickerrundownid))
            {
				 bool IsDeleted = _iMcrCategoryTickerRundownRepository.DeleteMcrCategoryTickerRundown(mcrcategorytickerrundownid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
