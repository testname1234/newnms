﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrLatestTickerRundown;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrLatestTickerRundownService : IMcrLatestTickerRundownService 
	{
		private IMcrLatestTickerRundownRepository _iMcrLatestTickerRundownRepository;
        
		public McrLatestTickerRundownService(IMcrLatestTickerRundownRepository iMcrLatestTickerRundownRepository)
		{
			this._iMcrLatestTickerRundownRepository = iMcrLatestTickerRundownRepository;
		}
        
        public Dictionary<string, string> GetMcrLatestTickerRundownBasicSearchColumns()
        {
            
            return this._iMcrLatestTickerRundownRepository.GetMcrLatestTickerRundownBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrLatestTickerRundownAdvanceSearchColumns()
        {
            
            return this._iMcrLatestTickerRundownRepository.GetMcrLatestTickerRundownAdvanceSearchColumns();
           
        }
        

		public McrLatestTickerRundown GetMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId)
		{
			return _iMcrLatestTickerRundownRepository.GetMcrLatestTickerRundown(McrLatestTickerRundownId);
		}

		public McrLatestTickerRundown UpdateMcrLatestTickerRundown(McrLatestTickerRundown entity)
		{
			return _iMcrLatestTickerRundownRepository.UpdateMcrLatestTickerRundown(entity);
		}

		public bool DeleteMcrLatestTickerRundown(System.Int32 McrLatestTickerRundownId)
		{
			return _iMcrLatestTickerRundownRepository.DeleteMcrLatestTickerRundown(McrLatestTickerRundownId);
		}

		public List<McrLatestTickerRundown> GetAllMcrLatestTickerRundown()
		{
			return _iMcrLatestTickerRundownRepository.GetAllMcrLatestTickerRundown();
		}

		public McrLatestTickerRundown InsertMcrLatestTickerRundown(McrLatestTickerRundown entity)
		{
			 return _iMcrLatestTickerRundownRepository.InsertMcrLatestTickerRundown(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrlatesttickerrundownid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrlatesttickerrundownid))
            {
				McrLatestTickerRundown mcrlatesttickerrundown = _iMcrLatestTickerRundownRepository.GetMcrLatestTickerRundown(mcrlatesttickerrundownid);
                if(mcrlatesttickerrundown!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrlatesttickerrundown);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrLatestTickerRundown> mcrlatesttickerrundownlist = _iMcrLatestTickerRundownRepository.GetAllMcrLatestTickerRundown();
            if (mcrlatesttickerrundownlist != null && mcrlatesttickerrundownlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrlatesttickerrundownlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrLatestTickerRundown mcrlatesttickerrundown = new McrLatestTickerRundown();
                PostOutput output = new PostOutput();
                mcrlatesttickerrundown.CopyFrom(Input);
                mcrlatesttickerrundown = _iMcrLatestTickerRundownRepository.InsertMcrLatestTickerRundown(mcrlatesttickerrundown);
                output.CopyFrom(mcrlatesttickerrundown);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrLatestTickerRundown mcrlatesttickerrundowninput = new McrLatestTickerRundown();
                McrLatestTickerRundown mcrlatesttickerrundownoutput = new McrLatestTickerRundown();
                PutOutput output = new PutOutput();
                mcrlatesttickerrundowninput.CopyFrom(Input);
                McrLatestTickerRundown mcrlatesttickerrundown = _iMcrLatestTickerRundownRepository.GetMcrLatestTickerRundown(mcrlatesttickerrundowninput.McrLatestTickerRundownId);
                if (mcrlatesttickerrundown!=null)
                {
                    mcrlatesttickerrundownoutput = _iMcrLatestTickerRundownRepository.UpdateMcrLatestTickerRundown(mcrlatesttickerrundowninput);
                    if(mcrlatesttickerrundownoutput!=null)
                    {
                        output.CopyFrom(mcrlatesttickerrundownoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrlatesttickerrundownid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrlatesttickerrundownid))
            {
				 bool IsDeleted = _iMcrLatestTickerRundownRepository.DeleteMcrLatestTickerRundown(mcrlatesttickerrundownid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
