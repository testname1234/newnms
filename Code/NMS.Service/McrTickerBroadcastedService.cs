﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerBroadcasted;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrTickerBroadcastedService : IMcrTickerBroadcastedService 
	{
		private IMcrTickerBroadcastedRepository _iMcrTickerBroadcastedRepository;
        
		public McrTickerBroadcastedService(IMcrTickerBroadcastedRepository iMcrTickerBroadcastedRepository)
		{
			this._iMcrTickerBroadcastedRepository = iMcrTickerBroadcastedRepository;
		}
        
        public Dictionary<string, string> GetMcrTickerBroadcastedBasicSearchColumns()
        {
            
            return this._iMcrTickerBroadcastedRepository.GetMcrTickerBroadcastedBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrTickerBroadcastedAdvanceSearchColumns()
        {
            
            return this._iMcrTickerBroadcastedRepository.GetMcrTickerBroadcastedAdvanceSearchColumns();
           
        }
        

		public virtual List<McrTickerBroadcasted> GetMcrTickerBroadcastedByTickerCategoryId(System.Int32 TickerCategoryId)
		{
			return _iMcrTickerBroadcastedRepository.GetMcrTickerBroadcastedByTickerCategoryId(TickerCategoryId);
		}

		public McrTickerBroadcasted GetMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId)
		{
			return _iMcrTickerBroadcastedRepository.GetMcrTickerBroadcasted(McrTickerBroadcastedId);
		}

		public McrTickerBroadcasted UpdateMcrTickerBroadcasted(McrTickerBroadcasted entity)
		{
			return _iMcrTickerBroadcastedRepository.UpdateMcrTickerBroadcasted(entity);
		}

		public bool DeleteMcrTickerBroadcasted(System.Int32 McrTickerBroadcastedId)
		{
			return _iMcrTickerBroadcastedRepository.DeleteMcrTickerBroadcasted(McrTickerBroadcastedId);
		}

		public List<McrTickerBroadcasted> GetAllMcrTickerBroadcasted()
		{
			return _iMcrTickerBroadcastedRepository.GetAllMcrTickerBroadcasted();
		}

		public McrTickerBroadcasted InsertMcrTickerBroadcasted(McrTickerBroadcasted entity)
		{
			 return _iMcrTickerBroadcastedRepository.InsertMcrTickerBroadcasted(entity);
		}

        public List<McrTickerBroadcasted> GetMcrTickerBroadcastedByDate(DateTime From, DateTime To)
        {
            return _iMcrTickerBroadcastedRepository.GetMcrTickerBroadcastedByDate(From, To);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrtickerbroadcastedid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerbroadcastedid))
            {
				McrTickerBroadcasted mcrtickerbroadcasted = _iMcrTickerBroadcastedRepository.GetMcrTickerBroadcasted(mcrtickerbroadcastedid);
                if(mcrtickerbroadcasted!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrtickerbroadcasted);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrTickerBroadcasted> mcrtickerbroadcastedlist = _iMcrTickerBroadcastedRepository.GetAllMcrTickerBroadcasted();
            if (mcrtickerbroadcastedlist != null && mcrtickerbroadcastedlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrtickerbroadcastedlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrTickerBroadcasted mcrtickerbroadcasted = new McrTickerBroadcasted();
                PostOutput output = new PostOutput();
                mcrtickerbroadcasted.CopyFrom(Input);
                mcrtickerbroadcasted = _iMcrTickerBroadcastedRepository.InsertMcrTickerBroadcasted(mcrtickerbroadcasted);
                output.CopyFrom(mcrtickerbroadcasted);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrTickerBroadcasted mcrtickerbroadcastedinput = new McrTickerBroadcasted();
                McrTickerBroadcasted mcrtickerbroadcastedoutput = new McrTickerBroadcasted();
                PutOutput output = new PutOutput();
                mcrtickerbroadcastedinput.CopyFrom(Input);
                McrTickerBroadcasted mcrtickerbroadcasted = _iMcrTickerBroadcastedRepository.GetMcrTickerBroadcasted(mcrtickerbroadcastedinput.McrTickerBroadcastedId);
                if (mcrtickerbroadcasted!=null)
                {
                    mcrtickerbroadcastedoutput = _iMcrTickerBroadcastedRepository.UpdateMcrTickerBroadcasted(mcrtickerbroadcastedinput);
                    if(mcrtickerbroadcastedoutput!=null)
                    {
                        output.CopyFrom(mcrtickerbroadcastedoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrtickerbroadcastedid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerbroadcastedid))
            {
				 bool IsDeleted = _iMcrTickerBroadcastedRepository.DeleteMcrTickerBroadcasted(mcrtickerbroadcastedid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
