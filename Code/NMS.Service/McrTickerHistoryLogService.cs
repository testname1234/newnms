﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerHistoryLog;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrTickerHistoryLogService : IMcrTickerHistoryLogService 
	{
		private IMcrTickerHistoryLogRepository _iMcrTickerHistoryLogRepository;
        
		public McrTickerHistoryLogService(IMcrTickerHistoryLogRepository iMcrTickerHistoryLogRepository)
		{
			this._iMcrTickerHistoryLogRepository = iMcrTickerHistoryLogRepository;
		}
        
        public Dictionary<string, string> GetMcrTickerHistoryLogBasicSearchColumns()
        {
            
            return this._iMcrTickerHistoryLogRepository.GetMcrTickerHistoryLogBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrTickerHistoryLogAdvanceSearchColumns()
        {
            
            return this._iMcrTickerHistoryLogRepository.GetMcrTickerHistoryLogAdvanceSearchColumns();
           
        }
        

		public McrTickerHistoryLog GetMcrTickerHistoryLog(System.Int32 McrTickerHistoryLogId)
		{
			return _iMcrTickerHistoryLogRepository.GetMcrTickerHistoryLog(McrTickerHistoryLogId);
		}

		public McrTickerHistoryLog UpdateMcrTickerHistoryLog(McrTickerHistoryLog entity)
		{
			return _iMcrTickerHistoryLogRepository.UpdateMcrTickerHistoryLog(entity);
		}

		public bool DeleteMcrTickerHistoryLog(System.Int32 McrTickerHistoryLogId)
		{
			return _iMcrTickerHistoryLogRepository.DeleteMcrTickerHistoryLog(McrTickerHistoryLogId);
		}

		public List<McrTickerHistoryLog> GetAllMcrTickerHistoryLog()
		{
			return _iMcrTickerHistoryLogRepository.GetAllMcrTickerHistoryLog();
		}

		public McrTickerHistoryLog InsertMcrTickerHistoryLog(McrTickerHistoryLog entity)
		{
			 return _iMcrTickerHistoryLogRepository.InsertMcrTickerHistoryLog(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrtickerhistorylogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerhistorylogid))
            {
				McrTickerHistoryLog mcrtickerhistorylog = _iMcrTickerHistoryLogRepository.GetMcrTickerHistoryLog(mcrtickerhistorylogid);
                if(mcrtickerhistorylog!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrtickerhistorylog);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrTickerHistoryLog> mcrtickerhistoryloglist = _iMcrTickerHistoryLogRepository.GetAllMcrTickerHistoryLog();
            if (mcrtickerhistoryloglist != null && mcrtickerhistoryloglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrtickerhistoryloglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrTickerHistoryLog mcrtickerhistorylog = new McrTickerHistoryLog();
                PostOutput output = new PostOutput();
                mcrtickerhistorylog.CopyFrom(Input);
                mcrtickerhistorylog = _iMcrTickerHistoryLogRepository.InsertMcrTickerHistoryLog(mcrtickerhistorylog);
                output.CopyFrom(mcrtickerhistorylog);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrTickerHistoryLog mcrtickerhistoryloginput = new McrTickerHistoryLog();
                McrTickerHistoryLog mcrtickerhistorylogoutput = new McrTickerHistoryLog();
                PutOutput output = new PutOutput();
                mcrtickerhistoryloginput.CopyFrom(Input);
                McrTickerHistoryLog mcrtickerhistorylog = _iMcrTickerHistoryLogRepository.GetMcrTickerHistoryLog(mcrtickerhistoryloginput.McrTickerHistoryLogId);
                if (mcrtickerhistorylog!=null)
                {
                    mcrtickerhistorylogoutput = _iMcrTickerHistoryLogRepository.UpdateMcrTickerHistoryLog(mcrtickerhistoryloginput);
                    if(mcrtickerhistorylogoutput!=null)
                    {
                        output.CopyFrom(mcrtickerhistorylogoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrtickerhistorylogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerhistorylogid))
            {
				 bool IsDeleted = _iMcrTickerHistoryLogRepository.DeleteMcrTickerHistoryLog(mcrtickerhistorylogid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
