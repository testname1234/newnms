﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerHistory;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrTickerHistoryService : IMcrTickerHistoryService 
	{
		private IMcrTickerHistoryRepository _iMcrTickerHistoryRepository;
        
		public McrTickerHistoryService(IMcrTickerHistoryRepository iMcrTickerHistoryRepository)
		{
			this._iMcrTickerHistoryRepository = iMcrTickerHistoryRepository;
		}
        
        public Dictionary<string, string> GetMcrTickerHistoryBasicSearchColumns()
        {
            
            return this._iMcrTickerHistoryRepository.GetMcrTickerHistoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrTickerHistoryAdvanceSearchColumns()
        {
            
            return this._iMcrTickerHistoryRepository.GetMcrTickerHistoryAdvanceSearchColumns();
           
        }
        

		public virtual List<McrTickerHistory> GetMcrTickerHistoryByTickerId(System.Int32? TickerId)
		{
			return _iMcrTickerHistoryRepository.GetMcrTickerHistoryByTickerId(TickerId);
		}

		public virtual List<McrTickerHistory> GetMcrTickerHistoryByTickerCategoryId(System.Int32? TickerCategoryId)
		{
			return _iMcrTickerHistoryRepository.GetMcrTickerHistoryByTickerCategoryId(TickerCategoryId);
		}

		public McrTickerHistory GetMcrTickerHistory(System.Int32 McrTickerHistoryId)
		{
			return _iMcrTickerHistoryRepository.GetMcrTickerHistory(McrTickerHistoryId);
		}

		public McrTickerHistory UpdateMcrTickerHistory(McrTickerHistory entity)
		{
			return _iMcrTickerHistoryRepository.UpdateMcrTickerHistory(entity);
		}

		public bool DeleteMcrTickerHistory(System.Int32 McrTickerHistoryId)
		{
			return _iMcrTickerHistoryRepository.DeleteMcrTickerHistory(McrTickerHistoryId);
		}

		public List<McrTickerHistory> GetAllMcrTickerHistory()
		{
			return _iMcrTickerHistoryRepository.GetAllMcrTickerHistory();
		}

		public McrTickerHistory InsertMcrTickerHistory(McrTickerHistory entity)
		{
			 return _iMcrTickerHistoryRepository.InsertMcrTickerHistory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrtickerhistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerhistoryid))
            {
				McrTickerHistory mcrtickerhistory = _iMcrTickerHistoryRepository.GetMcrTickerHistory(mcrtickerhistoryid);
                if(mcrtickerhistory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrtickerhistory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrTickerHistory> mcrtickerhistorylist = _iMcrTickerHistoryRepository.GetAllMcrTickerHistory();
            if (mcrtickerhistorylist != null && mcrtickerhistorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrtickerhistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrTickerHistory mcrtickerhistory = new McrTickerHistory();
                PostOutput output = new PostOutput();
                mcrtickerhistory.CopyFrom(Input);
                mcrtickerhistory = _iMcrTickerHistoryRepository.InsertMcrTickerHistory(mcrtickerhistory);
                output.CopyFrom(mcrtickerhistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrTickerHistory mcrtickerhistoryinput = new McrTickerHistory();
                McrTickerHistory mcrtickerhistoryoutput = new McrTickerHistory();
                PutOutput output = new PutOutput();
                mcrtickerhistoryinput.CopyFrom(Input);
                McrTickerHistory mcrtickerhistory = _iMcrTickerHistoryRepository.GetMcrTickerHistory(mcrtickerhistoryinput.McrTickerHistoryId);
                if (mcrtickerhistory!=null)
                {
                    mcrtickerhistoryoutput = _iMcrTickerHistoryRepository.UpdateMcrTickerHistory(mcrtickerhistoryinput);
                    if(mcrtickerhistoryoutput!=null)
                    {
                        output.CopyFrom(mcrtickerhistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public List<McrTickerHistory> GetLastBroadcastedTickers(int tickerCategoryId, int size, List<int> tickerIds)
        {
            return _iMcrTickerHistoryRepository.GetLastBroadcastedTickers(tickerCategoryId, size, tickerIds);
        }

        public List<McrTickerHistory> GetMcrTickerHistoryByBroadcastedId(int Id)
        {
            return _iMcrTickerHistoryRepository.GetMcrTickerHistoryByBroadcastedId(Id);
        }
         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrtickerhistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerhistoryid))
            {
				 bool IsDeleted = _iMcrTickerHistoryRepository.DeleteMcrTickerHistory(mcrtickerhistoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
