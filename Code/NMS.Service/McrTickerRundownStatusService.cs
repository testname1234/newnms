﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerRundownStatus;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrTickerRundownStatusService : IMcrTickerRundownStatusService 
	{
		private IMcrTickerRundownStatusRepository _iMcrTickerRundownStatusRepository;
        
		public McrTickerRundownStatusService(IMcrTickerRundownStatusRepository iMcrTickerRundownStatusRepository)
		{
			this._iMcrTickerRundownStatusRepository = iMcrTickerRundownStatusRepository;
		}
        
        public Dictionary<string, string> GetMcrTickerRundownStatusBasicSearchColumns()
        {
            
            return this._iMcrTickerRundownStatusRepository.GetMcrTickerRundownStatusBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrTickerRundownStatusAdvanceSearchColumns()
        {
            
            return this._iMcrTickerRundownStatusRepository.GetMcrTickerRundownStatusAdvanceSearchColumns();
           
        }
        

		public virtual List<McrTickerRundownStatus> GetMcrTickerRundownStatusByStatusId(System.Int32 StatusId)
		{
			return _iMcrTickerRundownStatusRepository.GetMcrTickerRundownStatusByStatusId(StatusId);
		}

		public McrTickerRundownStatus GetMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId)
		{
			return _iMcrTickerRundownStatusRepository.GetMcrTickerRundownStatus(McrTickerRundownStatusId);
		}

		public McrTickerRundownStatus UpdateMcrTickerRundownStatus(McrTickerRundownStatus entity)
		{
			return _iMcrTickerRundownStatusRepository.UpdateMcrTickerRundownStatus(entity);
		}

		public bool DeleteMcrTickerRundownStatus(System.Int32 McrTickerRundownStatusId)
		{
			return _iMcrTickerRundownStatusRepository.DeleteMcrTickerRundownStatus(McrTickerRundownStatusId);
		}

		public List<McrTickerRundownStatus> GetAllMcrTickerRundownStatus()
		{
			return _iMcrTickerRundownStatusRepository.GetAllMcrTickerRundownStatus();
		}

		public McrTickerRundownStatus InsertMcrTickerRundownStatus(McrTickerRundownStatus entity)
		{
			 return _iMcrTickerRundownStatusRepository.InsertMcrTickerRundownStatus(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrtickerrundownstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerrundownstatusid))
            {
				McrTickerRundownStatus mcrtickerrundownstatus = _iMcrTickerRundownStatusRepository.GetMcrTickerRundownStatus(mcrtickerrundownstatusid);
                if(mcrtickerrundownstatus!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrtickerrundownstatus);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrTickerRundownStatus> mcrtickerrundownstatuslist = _iMcrTickerRundownStatusRepository.GetAllMcrTickerRundownStatus();
            if (mcrtickerrundownstatuslist != null && mcrtickerrundownstatuslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrtickerrundownstatuslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrTickerRundownStatus mcrtickerrundownstatus = new McrTickerRundownStatus();
                PostOutput output = new PostOutput();
                mcrtickerrundownstatus.CopyFrom(Input);
                mcrtickerrundownstatus = _iMcrTickerRundownStatusRepository.InsertMcrTickerRundownStatus(mcrtickerrundownstatus);
                output.CopyFrom(mcrtickerrundownstatus);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrTickerRundownStatus mcrtickerrundownstatusinput = new McrTickerRundownStatus();
                McrTickerRundownStatus mcrtickerrundownstatusoutput = new McrTickerRundownStatus();
                PutOutput output = new PutOutput();
                mcrtickerrundownstatusinput.CopyFrom(Input);
                McrTickerRundownStatus mcrtickerrundownstatus = _iMcrTickerRundownStatusRepository.GetMcrTickerRundownStatus(mcrtickerrundownstatusinput.McrTickerRundownStatusId);
                if (mcrtickerrundownstatus!=null)
                {
                    mcrtickerrundownstatusoutput = _iMcrTickerRundownStatusRepository.UpdateMcrTickerRundownStatus(mcrtickerrundownstatusinput);
                    if(mcrtickerrundownstatusoutput!=null)
                    {
                        output.CopyFrom(mcrtickerrundownstatusoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrtickerrundownstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerrundownstatusid))
            {
				 bool IsDeleted = _iMcrTickerRundownStatusRepository.DeleteMcrTickerRundownStatus(mcrtickerrundownstatusid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
