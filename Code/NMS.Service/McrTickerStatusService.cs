﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.McrTickerStatus;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class McrTickerStatusService : IMcrTickerStatusService 
	{
		private IMcrTickerStatusRepository _iMcrTickerStatusRepository;
        
		public McrTickerStatusService(IMcrTickerStatusRepository iMcrTickerStatusRepository)
		{
			this._iMcrTickerStatusRepository = iMcrTickerStatusRepository;
		}
        
        public Dictionary<string, string> GetMcrTickerStatusBasicSearchColumns()
        {
            
            return this._iMcrTickerStatusRepository.GetMcrTickerStatusBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMcrTickerStatusAdvanceSearchColumns()
        {
            
            return this._iMcrTickerStatusRepository.GetMcrTickerStatusAdvanceSearchColumns();
           
        }
        

		public McrTickerStatus GetMcrTickerStatus(System.Int32 McrTickerStatusId)
		{
			return _iMcrTickerStatusRepository.GetMcrTickerStatus(McrTickerStatusId);
		}

		public McrTickerStatus UpdateMcrTickerStatus(McrTickerStatus entity)
		{
			return _iMcrTickerStatusRepository.UpdateMcrTickerStatus(entity);
		}

		public bool DeleteMcrTickerStatus(System.Int32 McrTickerStatusId)
		{
			return _iMcrTickerStatusRepository.DeleteMcrTickerStatus(McrTickerStatusId);
		}

		public List<McrTickerStatus> GetAllMcrTickerStatus()
		{
			return _iMcrTickerStatusRepository.GetAllMcrTickerStatus();
		}

		public McrTickerStatus InsertMcrTickerStatus(McrTickerStatus entity)
		{
			 return _iMcrTickerStatusRepository.InsertMcrTickerStatus(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mcrtickerstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerstatusid))
            {
				McrTickerStatus mcrtickerstatus = _iMcrTickerStatusRepository.GetMcrTickerStatus(mcrtickerstatusid);
                if(mcrtickerstatus!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mcrtickerstatus);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<McrTickerStatus> mcrtickerstatuslist = _iMcrTickerStatusRepository.GetAllMcrTickerStatus();
            if (mcrtickerstatuslist != null && mcrtickerstatuslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mcrtickerstatuslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                McrTickerStatus mcrtickerstatus = new McrTickerStatus();
                PostOutput output = new PostOutput();
                mcrtickerstatus.CopyFrom(Input);
                mcrtickerstatus = _iMcrTickerStatusRepository.InsertMcrTickerStatus(mcrtickerstatus);
                output.CopyFrom(mcrtickerstatus);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                McrTickerStatus mcrtickerstatusinput = new McrTickerStatus();
                McrTickerStatus mcrtickerstatusoutput = new McrTickerStatus();
                PutOutput output = new PutOutput();
                mcrtickerstatusinput.CopyFrom(Input);
                McrTickerStatus mcrtickerstatus = _iMcrTickerStatusRepository.GetMcrTickerStatus(mcrtickerstatusinput.McrTickerStatusId);
                if (mcrtickerstatus!=null)
                {
                    mcrtickerstatusoutput = _iMcrTickerStatusRepository.UpdateMcrTickerStatus(mcrtickerstatusinput);
                    if(mcrtickerstatusoutput!=null)
                    {
                        output.CopyFrom(mcrtickerstatusoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mcrtickerstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mcrtickerstatusid))
            {
				 bool IsDeleted = _iMcrTickerStatusRepository.DeleteMcrTickerStatus(mcrtickerstatusid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
