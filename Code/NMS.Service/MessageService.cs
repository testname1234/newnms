﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Message;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class MessageService : IMessageService
    {
        private IMessageRepository _iMessageRepository;

        public MessageService(IMessageRepository iMessageRepository)
        {
            this._iMessageRepository = iMessageRepository;
        }

        public Dictionary<string, string> GetMessageBasicSearchColumns()
        {

            return this._iMessageRepository.GetMessageBasicSearchColumns();

        }

        public List<SearchColumn> GetMessageAdvanceSearchColumns()
        {

            return this._iMessageRepository.GetMessageAdvanceSearchColumns();

        }


        public Message GetMessage(System.Int32 MessageId)
        {
            return _iMessageRepository.GetMessage(MessageId);
        }

        public Message UpdateMessage(Message entity)
        {
            return _iMessageRepository.UpdateMessage(entity);
        }

        public bool DeleteMessage(System.Int32 MessageId)
        {
            return _iMessageRepository.DeleteMessage(MessageId);
        }

        public List<Message> GetAllMessage()
        {
            return _iMessageRepository.GetAllMessage();
        }

        public Message InsertMessage(Message entity)
        {
            return _iMessageRepository.InsertMessage(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 messageid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out messageid))
            {
                Message message = _iMessageRepository.GetMessage(messageid);
                if (message != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(message);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Message> messagelist = _iMessageRepository.GetAllMessage();
            if (messagelist != null && messagelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(messagelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Message message = new Message();
                PostOutput output = new PostOutput();
                message.CopyFrom(Input);
                message = _iMessageRepository.InsertMessage(message);
                output.CopyFrom(message);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Message messageinput = new Message();
                Message messageoutput = new Message();
                PutOutput output = new PutOutput();
                messageinput.CopyFrom(Input);
                Message message = _iMessageRepository.GetMessage(messageinput.MessageId);
                if (message != null)
                {
                    messageoutput = _iMessageRepository.UpdateMessage(messageinput);
                    if (messageoutput != null)
                    {
                        output.CopyFrom(messageoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 messageid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out messageid))
            {
                bool IsDeleted = _iMessageRepository.DeleteMessage(messageid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }



        private List<Message> ConcatenateMessageList(List<Message> ToMessages, List<Message> FromMessages)
        {

            if (ToMessages != null && FromMessages != null)
            {
                List<Message> lstAllMessages =
                   (
                      (from to in ToMessages
                       select new Message
                       {
                           MessageId = to.MessageId,
                           To = to.To,
                           From = to.From,
                           IsRecieved = to.IsRecieved,
                           Message = to.Message
                       })
                      .Concat(from frm in FromMessages
                              select new Message
                              {
                                  MessageId = frm.MessageId,
                                  To = frm.To,
                                  From = frm.From,
                                  IsRecieved = frm.IsRecieved,
                                  Message = frm.Message
                              })
                   ).ToList<Message>();

                return lstAllMessages.OrderBy(x => x.MessageId).ToList<Message>();
            }

            else if (ToMessages != null)
            {
                List<Message> lstAllMessages =
                   (from to in ToMessages
                    select new Message
                    {
                        MessageId = to.MessageId,
                        To = to.To,
                        From = to.From,
                        IsRecieved = to.IsRecieved,
                        Message = to.Message
                    }).ToList<Message>();

                return lstAllMessages.OrderBy(x => x.MessageId).ToList<Message>();
            }
            else if (FromMessages != null)
            {
                List<Message> lstAllMessages =
                   (from to in FromMessages
                    select new Message
                    {
                        MessageId = to.MessageId,
                        To = to.To,
                        From = to.From,
                        IsRecieved = to.IsRecieved,
                        Message = to.Message
                    }).ToList<Message>();

                return lstAllMessages.OrderBy(x => x.MessageId).ToList<Message>();
            }
            else

                return null;
        }

        public List<Message> GetAllUserMessages(int UserId)
        {
            List<Message> lstAllMessages = _iMessageRepository.GetAllUserMessages(UserId, DateTime.UtcNow.AddHours(-24));

            if (lstAllMessages != null)
            {
                return lstAllMessages.OrderBy(x => x.MessageId).ToList<Message>();
            }
            return null;
        }

        public List<Message> GetAllUserPendingMessages(int UserId, List<int> MessageId)
        {

            //first update the messageid provided and set isRecieved = 1
            if(MessageId != null)
                _iMessageRepository.UpdateMessagesRecievedStatus(MessageId);

            //first get all the messages of To
            List<Message> lstAllMessages = _iMessageRepository.GetAllUserPendingMessages(UserId);

            if (lstAllMessages != null)
            {
                return lstAllMessages.OrderBy(x => x.MessageId).ToList<Message>();
            }
            return null;
        }

        public List<Message> GetAllUserPendingMessagesBySlotScreenTemplateId(int SlotScreenTemplateId)
        {
            List<Message> lstAllMessages = _iMessageRepository.GetAllUserPendingMessagesBySlotScreenTemplateId(SlotScreenTemplateId);

            if (lstAllMessages != null)
            {
                return lstAllMessages.OrderBy(x => x.MessageId).ToList<Message>();
            }
            return null;
        }

        public List<Message> GetMessages(int SlotScreenTemplateId, DateTime lastUpdateDate)
        {
            return _iMessageRepository.GetMessages(SlotScreenTemplateId, lastUpdateDate);
        }
    }
	
}
