﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MigrationBunchTemp;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class MigrationBunchTempService : IMigrationBunchTempService 
	{
		private IMigrationBunchTempRepository _iMigrationBunchTempRepository;
        
		public MigrationBunchTempService(IMigrationBunchTempRepository iMigrationBunchTempRepository)
		{
			this._iMigrationBunchTempRepository = iMigrationBunchTempRepository;
		}
        
        public Dictionary<string, string> GetMigrationBunchTempBasicSearchColumns()
        {
            
            return this._iMigrationBunchTempRepository.GetMigrationBunchTempBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMigrationBunchTempAdvanceSearchColumns()
        {
            
            return this._iMigrationBunchTempRepository.GetMigrationBunchTempAdvanceSearchColumns();
           
        }
        

		public MigrationBunchTemp GetMigrationBunchTemp(System.Int32 MigrationBunchTempId)
		{
			return _iMigrationBunchTempRepository.GetMigrationBunchTemp(MigrationBunchTempId);
		}

		public MigrationBunchTemp UpdateMigrationBunchTemp(MigrationBunchTemp entity)
		{
			return _iMigrationBunchTempRepository.UpdateMigrationBunchTemp(entity);
		}

		public bool DeleteMigrationBunchTemp(System.Int32 MigrationBunchTempId)
		{
			return _iMigrationBunchTempRepository.DeleteMigrationBunchTemp(MigrationBunchTempId);
		}

		public List<MigrationBunchTemp> GetAllMigrationBunchTemp()
		{
			return _iMigrationBunchTempRepository.GetAllMigrationBunchTemp();
		}

		public MigrationBunchTemp InsertMigrationBunchTemp(MigrationBunchTemp entity)
		{
			 return _iMigrationBunchTempRepository.InsertMigrationBunchTemp(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 migrationbunchtempid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out migrationbunchtempid))
            {
				MigrationBunchTemp migrationbunchtemp = _iMigrationBunchTempRepository.GetMigrationBunchTemp(migrationbunchtempid);
                if(migrationbunchtemp!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(migrationbunchtemp);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<MigrationBunchTemp> migrationbunchtemplist = _iMigrationBunchTempRepository.GetAllMigrationBunchTemp();
            if (migrationbunchtemplist != null && migrationbunchtemplist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(migrationbunchtemplist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                MigrationBunchTemp migrationbunchtemp = new MigrationBunchTemp();
                PostOutput output = new PostOutput();
                migrationbunchtemp.CopyFrom(Input);
                migrationbunchtemp = _iMigrationBunchTempRepository.InsertMigrationBunchTemp(migrationbunchtemp);
                output.CopyFrom(migrationbunchtemp);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                MigrationBunchTemp migrationbunchtempinput = new MigrationBunchTemp();
                MigrationBunchTemp migrationbunchtempoutput = new MigrationBunchTemp();
                PutOutput output = new PutOutput();
                migrationbunchtempinput.CopyFrom(Input);
                MigrationBunchTemp migrationbunchtemp = _iMigrationBunchTempRepository.GetMigrationBunchTemp(migrationbunchtempinput.MigrationBunchTempId);
                if (migrationbunchtemp!=null)
                {
                    migrationbunchtempoutput = _iMigrationBunchTempRepository.UpdateMigrationBunchTemp(migrationbunchtempinput);
                    if(migrationbunchtempoutput!=null)
                    {
                        output.CopyFrom(migrationbunchtempoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 migrationbunchtempid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out migrationbunchtempid))
            {
				 bool IsDeleted = _iMigrationBunchTempRepository.DeleteMigrationBunchTemp(migrationbunchtempid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
