﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Resource;
using Validation;
using System.Linq;
using NMS.Core;
using System.Net;
using NMS.Core.Helper;
using System.IO;
using Newtonsoft.Json;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Models;
using System.Drawing;
using System.Collections.Specialized;
using NMS.Core.Entities.Mongo;
using NMS.MongoRepository;
using NMS.Repository;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Data;
using System.Transactions;

namespace NMS.Service
{

    public class MigrationService
    {
        #region Sql initialization

        IResourceService _iResourceService = IoC.Resolve<IResourceService>("ResourceService");
        IBunchFilterService _iBunchFilterService = IoC.Resolve<IBunchFilterService>("BunchFilterService");
        IBunchFilterNewsService _iBunchFilterNewsService = IoC.Resolve<IBunchFilterNewsService>("BunchFilterNewsService");
        IBunchService _iBunchService = IoC.Resolve<IBunchService>("BunchService");
        INewsService _iNewsService = IoC.Resolve<INewsService>("NewsService");
        IBunchResourceService _iBunchResourceService = IoC.Resolve<IBunchResourceService>("BunchResourceService");
        ICommentService _iCommentService = IoC.Resolve<ICommentService>("CommentService");
        ITagService _iTagService = IoC.Resolve<ITagService>("TagService");
        ITagResourceService _iTagResourceService = IoC.Resolve<ITagResourceService>("TagResourceService");
        INewsResourceEditService _iNewsResourceEditService = IoC.Resolve<INewsResourceEditService>("NewsResourceEditService");
        IResourceEditclipinfoService _iResourceEditclipinfoService = IoC.Resolve<IResourceEditclipinfoService>("ResourceEditclipinfoService");
        IBunchTagService _iBunchTagService = IoC.Resolve<IBunchTagService>("BunchTagService");
        INewsFilterService _iNewsFilterService = IoC.Resolve<INewsFilterService>("NewsFilterService");
        INewsCategoryService _iNewsCategoryService = IoC.Resolve<INewsCategoryService>("NewsCategoryService");
        INewsLocationService _iNewsLocationService = IoC.Resolve<INewsLocationService>("NewsLocationService");
        INewsTagService _iNewsTagService = IoC.Resolve<INewsTagService>("NewsTagService");
        INewsResourceService _iNewsResourceService = IoC.Resolve<INewsResourceService>("NewsResourceService");
        ILocationService _iLocationService = IoC.Resolve<ILocationService>("LocationService");
        IBunchNewsService _iBunchNewsService = IoC.Resolve<IBunchNewsService>("BunchNewsService");
        ICategoryService _iCategoryService = IoC.Resolve<ICategoryService>("CategoryService");

        #endregion

        #region mongo repositories

        MResourceRepository mResourceRepository = new MResourceRepository();
        MBunchRepository mBunchRepository = new MBunchRepository();
        MBunchFilterRepository mBunchFilterRepository = new MBunchFilterRepository();
        MCommentRepository mCommentReporsitory = new MCommentRepository();
        MTagRepository mTagRepository = new MTagRepository();
        MBunchTagRepository mBunchTagRepository = new MBunchTagRepository();
        MTagResourceRepository mTagResourceRepository = new MTagResourceRepository();
        MNewsRepository mNewsRepository = new MNewsRepository();
        MNewsFilterRepository mNewsFilterRepository = new MNewsFilterRepository();
        MBunchResourceRepository mBunchResourceRepository = new MBunchResourceRepository();

        #endregion

        #region sql repositories
        BunchRepository BunchRepository = new BunchRepository();
        BunchResource BunchResources = new BunchResource();
        ResourceRepository resourceRepository = new ResourceRepository();
        #endregion

        MMigrationRepository mongoMigrationRepository = new MMigrationRepository();
        MigrationRepository sqlMigrationRepository = new MigrationRepository();

        public MigrationService()
        {

        }

        #region Mongo to Sql Migration Work

        DateTime MongoToSqlDate = new DateTime();

        public int DeleteCollectionDate(DateTime RemoveDate)
        {
            try
            {
                MBunchNewsRepository mBunchNewsRepository = new MBunchNewsRepository();

                MigrationBunchTempRepository migrationTempRepo = new MigrationBunchTempRepository();
                List<MigrationBunchTemp> MigrationBunchTempList = migrationTempRepo.GetAllMigrationBunchTemp();

                if (MigrationBunchTempList != null && MigrationBunchTempList.Count > 0)
                {
                    List<string> BunchList = MigrationBunchTempList.Select(x => x.BunchGuid).ToList();

                    #region bunch Dependent Collections

                    List<MNews> tmpNewsAll = mNewsRepository.GetByBunchIDsForMigration(BunchList).ToList();
                    List<string> NewsToDelete = tmpNewsAll.Select(x => x._id).ToList();

                    mBunchRepository.DeleteOlderData(BunchList);
                    mBunchNewsRepository.DeleteOlderData(BunchList);
                    mBunchFilterRepository.DeleteOlderData(BunchList);
                    mBunchTagRepository.DeleteOlderData(BunchList);
                    mBunchResourceRepository.DeleteOlderData(BunchList);
                    mNewsRepository.DeleteOlderData(BunchList);
                    mNewsFilterRepository.DeleteOlderData(NewsToDelete);

                    #endregion

                    #region bunch InDependent Collections
                    mResourceRepository.DeleteOlderData(RemoveDate);
                    //Obselecte collections
                    //mTagRepository.DeleteOlderData(dt);
                    //mTagResourceRepository.DeleteOlderData(dt);
                    #endregion

                    migrationTempRepo.DeleteAll();

                    return NewsToDelete.Count();
                }
            }
            catch (Exception ex)
            {
                throw;
                return 0;
            }
            return 0;
        }


        public bool DeleteQanews()
        {
            MBunchRepository br = new MBunchRepository();
            MNewsRepository newsrep = new MNewsRepository();
            List<MBunch> bunchs = br.GetAllBunches();
            List<MNews> newses = newsrep.GetAllBunches();
            List<string> delenews = newses.Where(x => !bunchs.Any(y => y._id == x.BunchGuid)).Select(x => x._id).ToList();
            newsrep.DeleteByNewsId(delenews);


            return true;
        }


        public int CompleteMongotoSql(DateTime RemovalDate)
        {
            // MongoToSqlDate = DateTime.SpecifyKind(sqlMigrationRepository.GetUpdatedDate("LastUpdateDate", "news"), DateTimeKind.Utc).AddMilliseconds(2);
            try
            {
                List<string> BuncheList = new List<string>();

                #region News Migration Work
                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                options.Timeout = new TimeSpan(0, 60, 0);
                //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                //{     
                Console.WriteLine("Archival Process Started : " + DateTime.Now);
                if (!SyncNews(RemovalDate, out BuncheList))
                    throw new Exception("Error in Migrating News");

                if (BuncheList != null && BuncheList.Count > 0)
                {
                    Console.WriteLine("News Completed: " + DateTime.Now + ", Total Bunches to Process Count: " + BuncheList.Count);
                    if (!SynchBunch(BuncheList))
                        throw new Exception("Error in Migrating Bunch");
                    Console.WriteLine("Bunch BuncheList: " + DateTime.Now);
                    if (!SynchBunchFilter(BuncheList))
                        throw new Exception("Error in Migrating Bunch Filter");
                    Console.WriteLine("BunchFilter Completed: " + DateTime.Now);
                    if (!SynchBunchNews(BuncheList))
                        throw new Exception("Error in Migrating Bunch News");
                    Console.WriteLine("BunchNews Completed: " + DateTime.Now);
                    if (!SyncResources(RemovalDate))
                        throw new Exception("Error in Migrating Resources");
                    Console.WriteLine("Resources Completed: " + DateTime.Now);
                    if (!SynchComments(RemovalDate))
                        throw new Exception("Error in Migrating Comments");
                    Console.WriteLine("Comments Completed: " + DateTime.Now);

                    Console.WriteLine("Archival Process Completed : " + DateTime.Now);
                }

                if (!AddMigratedBunch(BuncheList))
                    throw new Exception("Error in Adding Migrated Data For Deletion from Mongo");

                Console.WriteLine("Deletion Process Started: " + DateTime.Now);
                //  scope.Complete();
                // }
                #endregion

                #region Delete Mongo Data
                int MigratedNewsCount = DeleteCollectionDate(RemovalDate);
                Console.WriteLine("Deletion Process Completed: " + DateTime.Now);
                #endregion

                return MigratedNewsCount;
            }
            catch (Exception ex)
            {
                throw;
                return 0;
            }
        }

        public bool SyncResources(DateTime RemovalDate)
        {
            string SQLEntityName = "Resource";
            try
            {
                //Get Resource Date          
                List<MResource> mEntities = null;
                List<Resource> SqlEntities = new List<Resource>();


                mEntities = mResourceRepository.GetResourcesByDate(RemovalDate);   //raw date to get  all                
                if (mEntities != null && mEntities.Count > 0)
                {
                    //List<Resource> resources = _iResourceService.GetAllResource();
                    // resources.Where(x => !mEntities.Select(y => y.Guid).Contains(x.Guid)).ToList();
                    //if (resources != null)
                    //  mEntities = mEntities.Where(x => !resources.Any(y => y.Guid == x.Guid)).ToList();

                    mEntities = mEntities.Where(x => !String.IsNullOrEmpty(x.Guid) && x.ResourceTypeId != 0).ToList();
                    foreach (MResource mresource in mEntities)
                    {
                        Resource resource = new Resource();
                        resource.CopyFrom(mresource);
                        resource.LastUpdateDate = resource.LastUpdateDate.ToUniversalTime();
                        SqlEntities.Add(resource);
                        //resourceRepository.InsertResourceNoReturn(resource);

                    }
                    sqlMigrationRepository.BulkInsert<Resource>(SqlEntities, SQLEntityName, ignoreColumnList);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
        }

        public bool SynchBunch(List<string> BunchList)
        {
            string SQLEntityName = "bunch";
            try
            {
                //Get Resource Date                            
                // List<MBunch> mEntities = GetCollection<MBunch>("LastUpdateDate", "Bunch", "LastUpdateDate", "Bunch");

                //List<Bunch> Entities = _iBunchService.GetAllBunch(RemovalDate);
                //List<MBunch> mEntities = GetAllCollection<MBunch>("Bunch");

                //if (Entities != null && Entities.Count > 0)
                //{
                //    //var itemIds = Entities.Select(x => x.Guid).ToArray();
                //    //mEntities = mEntities.Where(x => !itemIds.Contains(x._id)).ToList();                                        
                //    foreach (Bunch bunch in Entities)
                //    {
                //        mEntities.Remove(mEntities.FirstOrDefault(s => s._id == bunch.Guid));
                //    }
                //    // mEntities = mEntities.Where(x => !Entities.Any(y => y.Guid == x._id)).ToList();
                //    // mEntities = mEntities.Where(x => !Entities.Any(y => y.Guid == x._id)).ToList();
                //}

                List<Bunch> SqlEntities = new List<Bunch>();
                List<MBunch> mEntities = mBunchRepository.GetByIDs(BunchList);

                if (mEntities != null && mEntities.Count > 0)
                {
                    foreach (MBunch mBunch in mEntities)
                    {
                        Bunch bunch = new Bunch();
                        if (mBunch.CreationDate.Year < 2000)
                        {
                            mBunch.CreationDate = DateTime.UtcNow;
                            mBunch.LastUpdateDate = DateTime.UtcNow;
                        }
                        bunch.CopyFrom(mBunch);
                        bunch.Guid = mBunch._id;
                        SqlEntities.Add(bunch);
                    }
                }
                sqlMigrationRepository.BulkInsert<Bunch>(SqlEntities, SQLEntityName, ignoreColumnList);
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
        }

        public bool SynchBunchFilter(List<string> BunchList)
        {
            string SQLEntityName = "BunchFilterNews";
            try
            {

                MBunchFilterRepository mBunchFilterRepository = new MBunchFilterRepository();
                List<BunchFilterNews> SqlEntities = new List<BunchFilterNews>();
                List<MBunchFilter> mEntities = mBunchFilterRepository.GteByBunchIds(BunchList);
                //Get Resource Date                            
                // List<MBunchFilter> mEntities = GetCollection<MBunchFilter>("LastUpdateDate", "BunchFilter", "LastUpdateDate", "BunchFilter", MongoToSqlDate);                
                //List<BunchFilterNews> Entities = _iBunchFilterNewsService.GetAllBunchFilterNews();
                //foreach (BunchFilterNews bunchfilter in Entities)
                //{
                //    mEntities.Remove(mEntities.FirstOrDefault(s => s.BunchId == bunchfilter.BunchGuid && s.FilterId == bunchfilter.FilterId);
                //}

                if (mEntities != null && mEntities.Count > 0)
                {
                    int filterId = 0;
                    string BunchGuid = "";
                    foreach (MBunchFilter mBunchFilter in mEntities)
                    {
                        filterId = mBunchFilter.FilterId;
                        BunchGuid = mBunchFilter.BunchId;

                        foreach (MNews news in mBunchFilter.NewsList)
                        {
                            // if (Entities.Where(x => x.BunchGuid == BunchGuid && x.FilterId == filterId && x.Newsguid == news._id) == null)
                            // {
                            BunchFilterNews bunchFilterNews = new BunchFilterNews();
                            bunchFilterNews.CopyFrom(mBunchFilter);
                            bunchFilterNews.Newsguid = news._id;
                            bunchFilterNews.BunchGuid = BunchGuid;
                            if (bunchFilterNews.CreationDate.Value.Year <= 2000 && bunchFilterNews.LastUpdateDate.Value.Year > 2000)
                            {
                                bunchFilterNews.CreationDate = bunchFilterNews.LastUpdateDate;
                            }
                            if (bunchFilterNews.CreationDate.Value.Year > 2000 && bunchFilterNews.LastUpdateDate.Value.Year <= 2000)
                            {
                                bunchFilterNews.LastUpdateDate = bunchFilterNews.CreationDate;
                            }
                            if (bunchFilterNews.CreationDate.Value.Year <= 2000 && bunchFilterNews.LastUpdateDate.Value.Year <= 2000)
                            {
                                bunchFilterNews.CreationDate = BunchRepository.GetBunchByGuid(BunchGuid).CreationDate;
                                bunchFilterNews.LastUpdateDate = BunchRepository.GetBunchByGuid(BunchGuid).CreationDate;
                            }
                            SqlEntities.Add(bunchFilterNews);
                            //  }
                        }
                    }
                    sqlMigrationRepository.BulkInsert<BunchFilterNews>(SqlEntities, SQLEntityName, ignoreColumnList);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
        }

        public bool SynchBunchNews(List<string> Bunches)
        {
            string SQLEntityName = "BunchNews";
            MBunchNewsRepository mBunchNewsRepository = new MBunchNewsRepository();
            try
            {
                //Get BunchNews Date                            
                //List<MBunchNews> mEntities = GetCollection<MBunchNews>("CreationDate", "BunchNews", "CreationDate", "BunchNews", MongoToSqlDate);
                //List<MBunchNews> mEntities = GetAllCollection<MBunchNews>("BunchNews");
                //List<BunchNews> Entities = _iBunchNewsService.GetAllBunchNews();

                List<BunchNews> SqlEntities = new List<BunchNews>();
                List<MBunchNews> mEntities = mBunchNewsRepository.GetByBunchIds(Bunches);
                if (mEntities != null && mEntities.Count > 0)
                {
                    foreach (MBunchNews mBunchNews in mEntities)
                    {
                        BunchNews bunchNews = new BunchNews();
                        bunchNews.BunchGuid = mBunchNews.BunchId;
                        bunchNews.NewsGuid = mBunchNews.NewsId;
                        bunchNews.CreationDate = mBunchNews.CreationDate;
                        SqlEntities.Add(bunchNews);
                    }
                    sqlMigrationRepository.BulkInsert<BunchNews>(SqlEntities, SQLEntityName, ignoreColumnList);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
        }

        public bool SynchComments(DateTime RemoveDate)
        {
            string SQLEntityName = "Comment";
            try
            {
                //Get Resource Date                            
                //List<MComment> mEntities = GetCollection<MComment>("LastUpdateDate", "Comment", "LastUpdateDate", "Comment", MongoToSqlDate);
                List<Comment> SqlEntities = new List<Comment>();
                List<MComment> mEntities = mCommentReporsitory.GetCommentsByLastUpdateForMigration(RemoveDate);
                //List<Comment> Entities = _iCommentService.GetAllComment();

                if (mEntities != null && mEntities.Count > 0)
                {
                    foreach (MComment mEntity in mEntities)
                    {
                        Comment sqlEntity = new Comment();
                        sqlEntity.CopyFrom(mEntity);
                        sqlEntity.NewsGuid = mEntity.NewsId;
                        sqlEntity.Guid = mEntity._id;
                        //_iCommentService.InsertIfNotExists(sqlEntity);
                        SqlEntities.Add(sqlEntity);
                    }
                    sqlMigrationRepository.BulkInsert<Comment>(SqlEntities, SQLEntityName, ignoreColumnList);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
        }

        public bool AddMigratedBunch(List<string> Bunches)
        {
            string SQLEntityName = "MigrationBunchTemp";
            try
            {
                DateTime dtnow = DateTime.UtcNow;
                List<MigrationBunchTemp> MigrationBunchTempList = new List<MigrationBunchTemp>();
                foreach (string bunchStr in Bunches)
                {
                    MigrationBunchTemp migrationBunchTemp = new MigrationBunchTemp();
                    migrationBunchTemp.BunchGuid = bunchStr;
                    migrationBunchTemp.CreatedDate = dtnow;
                    MigrationBunchTempList.Add(migrationBunchTemp);
                }
                sqlMigrationRepository.BulkInsert<MigrationBunchTemp>(MigrationBunchTempList, SQLEntityName, ignoreColumnList);
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
        }


        public bool SyncNews(DateTime RemoveDate, out List<string> BunchList)
        {
            string SQLEntityName = "News";
            NewsRepository newsrepo = new NewsRepository();
            BunchList = null;
            try
            {

                //Get Resource Date                            
                //List<MNews> mEntities = GetEntitiesByDateAndIsCompleted<MNews>("LastUpdateDate", "IsCompleted", "News", "LastUpdateDate", "IsCompleted", "News", MongoToSqlDate);                
                //List<MNews> mEntities = GetAllCollection<MNews>("News");
                // List<News> SqlEntities = newsrepo.GetAllNews();
                //List<MNews> mEntities = mNewsRepository.GetByDateForMigration(RemoveDate);

                //Ooper ki cheez                                
                List<MNews> tmpBunches = mNewsRepository.GetAllBunchesByDate(RemoveDate);
                if (tmpBunches != null && tmpBunches.Count > 0)
                {
                    List<string> bunchesStrList = tmpBunches.Select(x => x.BunchGuid).Distinct().ToList();
                    List<MNews> tmpNews = mNewsRepository.GetByBunchIDsForMigrationByDate(bunchesStrList, RemoveDate).ToList();
                    if (tmpNews != null && tmpNews.Count > 0)
                    {
                        tmpNews.Select(x => bunchesStrList.Remove(x.BunchGuid)).ToList();
                    }
                    BunchList = bunchesStrList;

                    List<MNews> mEntities = mNewsRepository.GetByBunchIDs(bunchesStrList).OrderByDescending(x => x.LastUpdateDate).ToList();
                    List<News> SqlEntities = new List<News>();
                    if (mEntities != null && mEntities.Count > 0)
                    {
                        List<NewsFilter> newsfilters = new List<NewsFilter>();
                        List<NewsCategory> newsCategors = new List<NewsCategory>();
                        List<NewsLocation> newsLocations = new List<NewsLocation>();
                        List<NewsTag> newsTags = new List<NewsTag>();
                        List<NewsResource> newsResources = new List<NewsResource>();
                        List<NewsStatistics> newsStatisticss = new List<NewsStatistics>();
                        News sqlEntity = new News();

                        foreach (MNews mEntity in mEntities)
                        {
                            sqlEntity = new News();
                            //sqlEntity  = SqlEntities.Where(x=>x.Guid == mEntity._id).FirstOrDefault();
                            //if (sqlEntity != null)
                            //{
                            sqlEntity.Guid = mEntity._id;
                            sqlEntity.BunchGuid = mEntity.BunchGuid;
                            sqlEntity.ParentNewsGuid = mEntity.ParentNewsId;
                            sqlEntity.ReferenceNewsGuid = mEntity.ReferenceNewsId;
                            sqlEntity.ThumbnailId = mEntity.ThumbnailUrl;
                            sqlEntity.IsVerified = mEntity.IsVerified;
                            sqlEntity.AddedToRundownCount = mEntity.AddedToRundownCount;
                            sqlEntity.OnAirCount = mEntity.OnAirCount;
                            sqlEntity.OtherChannelExecutionCount = mEntity.OtherChannelExecutionCount;
                            sqlEntity.NewsTickerCount = mEntity.NewsTickerCount;
                            sqlEntity.IsCompleted = true;
                            sqlEntity.MigrationStatus = 1;
                            sqlEntity.IsArchival = true;
                            SqlEntities.Add(sqlEntity);
                            //newsrepo.UpdateNewsForMigration(sqlEntity);

                            #region News Lists

                            #region Check News Filters
                            //List<NewsFilter> newsFiltersToDelete = _iNewsFilterService.GetNewsFilterByNewsId(sqlEntity.NewsId);
                            //if (newsFiltersToDelete != null && newsFiltersToDelete.Count > 0)
                            //{
                            //    foreach (NewsFilter nf in newsFiltersToDelete)
                            //    {
                            //        _iNewsFilterService.DeleteNewsFilter(nf.NewsId);
                            //    }
                            //}

                            if (mEntity.Filters != null && mEntity.Filters.Count > 0)
                            {
                                foreach (MNewsFilter mNF in mEntity.Filters)
                                {
                                    NewsFilter sqlNewsFiltrer = new NewsFilter();
                                    if (mNF.LastUpdateDate.Year <= 2000)
                                    {
                                        sqlNewsFiltrer.LastUpdateDate = DateTime.UtcNow;
                                        sqlNewsFiltrer.CreationDate = DateTime.UtcNow;
                                    }
                                    else
                                    {
                                        sqlNewsFiltrer.CreationDate = mNF.CreationDate;
                                        sqlNewsFiltrer.LastUpdateDate = mNF.LastUpdateDate;
                                    }
                                    sqlNewsFiltrer.NewsGuid = mNF.NewsId;
                                    sqlNewsFiltrer.FilterId = mNF.FilterId;
                                    sqlNewsFiltrer.ReporterId = mNF.ReporterId;
                                    newsfilters.Add(sqlNewsFiltrer);
                                }
                            }

                            #endregion

                            #region Check News Categories
                            //List<NewsCategory> newsCategoryToDelete = _iNewsCategoryService.GetNewsCategoryByNewsId(sqlEntity.NewsId);
                            //if (newsCategoryToDelete != null && newsCategoryToDelete.Count > 0)
                            //{
                            //    foreach (NewsCategory nC in newsCategoryToDelete)
                            //    {
                            //        _iNewsCategoryService.DeleteNewsCategory(nC.NewsCategoryId);
                            //    }
                            //}

                            if (mEntity.Categories != null && mEntity.Categories.Count > 0)
                            {
                                foreach (Category mC in mEntity.Categories)
                                {
                                    NewsCategory sqlCategory = new NewsCategory();
                                    sqlCategory.CategoryId = mC.CategoryId;
                                    if (mC.LastUpdateDate.Year <= 2000)
                                    {
                                        sqlCategory.LastUpdateDate = DateTime.UtcNow;
                                        sqlCategory.CreationDate = DateTime.UtcNow;
                                    }
                                    else
                                    {
                                        sqlCategory.CreationDate = mC.CreationDate;
                                        sqlCategory.LastUpdateDate = mC.LastUpdateDate;
                                    }
                                    sqlCategory.NewsGuid = mEntity._id;
                                    newsCategors.Add(sqlCategory);
                                }
                            }

                            #endregion

                            #region Check News Location
                            //List<NewsLocation> newsLocationToDelete = _iNewsLocationService.GetNewsLocationByNewsId(sqlEntity.NewsId);
                            //if (newsLocationToDelete != null && newsLocationToDelete.Count > 0)
                            //{
                            //    foreach (NewsLocation nL in newsLocationToDelete)
                            //    {
                            //        _iNewsLocationService.DeleteNewsLocation(nL.NewsLocationid);
                            //    }
                            //}

                            if (mEntity.Locations != null && mEntity.Locations.Count > 0)
                            {
                                SQLEntityName = "NewsLocation";
                                foreach (Location mL in mEntity.Locations)
                                {
                                    NewsLocation sqlNewsLocation = new NewsLocation();
                                    sqlNewsLocation.LocationId = mL.LocationId;
                                    if (mL.LastUpdateDate.Year <= 2000)
                                    {
                                        sqlNewsLocation.LastUpdateDate = DateTime.UtcNow;
                                        sqlNewsLocation.CreationDate = DateTime.UtcNow;
                                    }
                                    else
                                    {
                                        sqlNewsLocation.CreationDate = mL.CreationDate;
                                        sqlNewsLocation.LastUpdateDate = mL.LastUpdateDate;
                                    }
                                    sqlNewsLocation.NewsGuid = mEntity._id;
                                    newsLocations.Add(sqlNewsLocation);
                                    //_iNewsLocationService.InsertNewsLocation(sqlNewsLocation);
                                }

                            }

                            #endregion

                            #region Check News Tags
                            //List<NewsTag> newsTagToDelete = _iNewsTagService.GetNewsTagByNewsId(sqlEntity.NewsId);
                            //if (newsTagToDelete != null && newsTagToDelete.Count > 0)
                            //{
                            //    foreach (NewsTag nT in newsTagToDelete)
                            //    {
                            //        _iNewsTagService.DeleteNewsTag(nT.NewsTagId);
                            //    }
                            //}

                            if (mEntity.Tags != null && mEntity.Tags.Count > 0)
                            {
                                SQLEntityName = "NewsTag";
                                foreach (MTag mT in mEntity.Tags)
                                {
                                    NewsTag sqlNewsTag = new NewsTag();
                                    sqlNewsTag.Rank = mT.Rank;
                                    if (mT.LastUpdateDate.Year <= 2000)
                                    {
                                        sqlNewsTag.LastUpdateDate = DateTime.UtcNow;
                                        sqlNewsTag.CreationDate = DateTime.UtcNow;
                                    }
                                    else
                                    {
                                        sqlNewsTag.CreationDate = mT.CreationDate;
                                        sqlNewsTag.LastUpdateDate = mT.LastUpdateDate;
                                    }
                                    sqlNewsTag.NewsGuid = mEntity._id;
                                    sqlNewsTag.TagGuid = mT._id;
                                    sqlNewsTag.Tag = mT.Tag;
                                    newsTags.Add(sqlNewsTag);
                                    //_iNewsTagService.InsertNewsTag(sqlNewsTag);                                    
                                }

                            }

                            #endregion

                            #region Check News Resources

                            //List<NewsResource> newsResourceToDelete = _iNewsResourceService.GetNewsResourceByNewsId(sqlEntity.NewsId);
                            //if (newsResourceToDelete != null && newsResourceToDelete.Count > 0)
                            //{
                            //    foreach (NewsResource nR in newsResourceToDelete)
                            //    {
                            //        _iNewsResourceService.DeleteNewsResource(nR.NewsResourceId);
                            //    }
                            //}
                            if (mEntity.Resources != null && mEntity.Resources.Count > 0)
                            {
                                SQLEntityName = "NewsResource";
                                foreach (MResource nR in mEntity.Resources)
                                {
                                    NewsResource sqlNewsResource = new NewsResource();
                                    //sqlNewsResource.CopyFrom(nR);
                                    sqlNewsResource.NewsGuid = mEntity._id;
                                    sqlNewsResource.ResourceGuid = nR.Guid;
                                    if (nR.LastUpdateDate.Year <= 2000)
                                    {
                                        sqlNewsResource.LastUpdateDate = DateTime.UtcNow;
                                        sqlNewsResource.CreationDate = DateTime.UtcNow;
                                    }
                                    else
                                    {
                                        sqlNewsResource.CreationDate = nR.CreationDate;
                                        sqlNewsResource.LastUpdateDate = nR.LastUpdateDate;
                                    }
                                    //_iNewsResourceService.InsertNewsResource(sqlNewsResource);
                                    newsResources.Add(sqlNewsResource);
                                }

                            }

                            #endregion

                            #region Resource Edits

                            //List<NewsResourceEdit> newsResourceEditDelete = _iNewsResourceEditService.GetNewsResourceEditByNewsId(sqlEntity.NewsId);

                            //if (newsResourceEditDelete != null && newsResourceEditDelete.Count > 0)
                            //{
                            //    foreach (NewsResourceEdit newsResourceEdit in newsResourceEditDelete)
                            //    {
                            //        _iNewsResourceEditService.DeleteNewsResourceEdit(newsResourceEdit.NewsResourceEditId);
                            //        _iResourceEditclipinfoService.DeleteByNewsResourceEditId(newsResourceEdit.NewsResourceEditId);
                            //    }
                            //}

                            if (mEntity.ResourceEdit != null)
                            {
                                NewsResourceEdit nResourceEdit = new NewsResourceEdit();
                                nResourceEdit.CopyFrom(mEntity.ResourceEdit);
                                nResourceEdit.NewsGuid = mEntity._id;
                                //tagresource.TagId = tag.TagId;
                                NewsResourceEdit nResourceEditNew = _iNewsResourceEditService.InsertNewsResourceEdit(nResourceEdit);
                                if (mEntity.ResourceEdit.FromTos != null && mEntity.ResourceEdit.FromTos.Count > 0)
                                {
                                    foreach (FromTo fromto in mEntity.ResourceEdit.FromTos)
                                    {
                                        ResourceEditclipinfo resourceEditclipinfo = new ResourceEditclipinfo();
                                        resourceEditclipinfo.CopyFrom(fromto);
                                        resourceEditclipinfo.NewsResourceEditId = nResourceEditNew.NewsResourceEditId;
                                        _iResourceEditclipinfoService.InsertResourceEditclipinfo(resourceEditclipinfo);
                                    }
                                }
                            }
                            #endregion

                            #region News Statistics

                            if (mEntity.NewsStatistics != null && mEntity.NewsStatistics.Count > 0)
                            {
                                SQLEntityName = "NewsStatistics";
                                foreach (MNewsStatistics ns in mEntity.NewsStatistics)
                                {
                                    NewsStatistics newsStatistics = new NewsStatistics();
                                    newsStatistics.CopyFrom(ns);
                                    newsStatistics.NewsGuid = mEntity._id;
                                    newsStatisticss.Add(newsStatistics);
                                }
                            }

                            #endregion

                            #endregion
                        }
                        sqlMigrationRepository.BulkUpdateNews<News>(SqlEntities, "News", ignoreColumnList);
                        sqlMigrationRepository.BulkInsert<NewsFilter>(newsfilters, "NewsFilter", ignoreColumnList);
                        sqlMigrationRepository.BulkInsert<NewsCategory>(newsCategors, "NewsCategory", ignoreColumnList);
                        sqlMigrationRepository.BulkInsert<NewsLocation>(newsLocations, "NewsLocation", ignoreColumnList);
                        sqlMigrationRepository.BulkInsert<NewsTag>(newsTags, "NewsTag", ignoreColumnList);
                        sqlMigrationRepository.BulkInsert<NewsResource>(newsResources, "NewsResource", ignoreColumnList);
                        sqlMigrationRepository.BulkInsert<NewsStatistics>(newsStatisticss, "NewsStatistics", ignoreColumnList);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
        }


        public int RemoveGarbage()
        {
            DateTime RemoveDate = DateTime.Now.AddDays(-100);
            List<MNews> tmpBunches = mNewsRepository.GetAllBunchesByDate(RemoveDate);
            if (tmpBunches != null && tmpBunches.Count > 0)
            {
                List<string> bunchesStrList = tmpBunches.Select(x => x.BunchGuid).ToList();
                List<MNews> tmpNews = mNewsRepository.GetByBunchIDsForMigration(bunchesStrList).Where(x => x.LastUpdateDate >= RemoveDate).ToList();
                if (tmpNews != null && tmpNews.Count > 0)
                {
                    tmpNews.Select(x => bunchesStrList.Remove(x.BunchGuid)).ToList();
                }
                bunchesStrList = bunchesStrList;

            }
            return 0;

        }



        public bool SynchBunchFilterNews()
        {
            string SQLEntityName = "BunchFilterNews";
            try
            {
                //Get Resource Date                            
                List<MBunchFilter> mEntities = GetCollection<MBunchFilter>("LastUpdateDate", "BunchFilterNews", "LastUpdateDate", "BunchFilter", MongoToSqlDate);
                if (mEntities != null && mEntities.Count > 0)
                {

                    //Sql Place BunchGuid in all Bunch Tables
                    //Or Join 

                    foreach (MBunchFilter mBunchFilter in mEntities)
                    {
                        Bunch bunch = _iBunchService.GetBunchByGuid(mBunchFilter.BunchId);
                        //List<BunchFilterNews> bunchFilterNewsDelete = _iBunchFilterNewsService.GetByBunchIdAndFilterId(bunch.BunchId, mBunchFilter.FilterId);
                        if (bunch != null)
                        {
                            BunchFilter bunchFilter = _iBunchFilterService.GetByFilterIdAndBunchId(mBunchFilter.FilterId, bunch.BunchId);
                            //List<BunchFilterNews> bunchFilterNewsDelete = _iBunchFilterNewsService.GetBunchFilterNewsByBunchFilterId(bunchFilter.BunchFilterId);
                            List<BunchFilterNews> bunchFilterNewsDelete = _iBunchFilterNewsService.GetAllBunchFilterNews();

                            if (bunchFilterNewsDelete != null && bunchFilterNewsDelete.Count > 0)
                            {
                                foreach (BunchFilterNews bfn in bunchFilterNewsDelete)
                                {
                                    _iBunchFilterNewsService.DeleteBunchFilterNews(bfn.BunchFilterNewsId);
                                }
                            }

                            BunchFilterNews bunchFilterNews = new BunchFilterNews();
                            bunchFilterNews.CopyFrom(mBunchFilter);

                            if (mBunchFilter.NewsList != null && mBunchFilter.NewsList.Count > 0)
                            {
                                foreach (MNews mnews in mBunchFilter.NewsList)
                                {
                                    News news = _iNewsService.GetByNewsGuid(mnews._id);
                                    // bunchFilterNews.NewsId = news.NewsId;
                                    // bunchFilterNews.BunchFilterId = bunchFilter.BunchFilterId;
                                    //_iBunchFilterNewsService.InsertBunchFilterNews(bunchFilterNews);
                                    sqlMigrationRepository.InsertEntityNoReturn<BunchFilterNews>(bunchFilterNews, "bunchFilterNews"); //using dynamic method for insert only
                                }
                            }
                        }
                        //_iBunchFilterNewsService.InserUpdateIfNotExist(bunchFilter);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteError("Mongo TO Sql Migration: " + SQLEntityName + " Migration", ex);
                return false;
            }
        }

        public bool SynchTags()
        {
            string SQLEntityName = "Tags";
            try
            {
                //Get Resource Date                            
                List<MTag> mEntities = GetCollection<MTag>("LastUpdateDate", "Tag", "LastUpdateDate", "Tag", MongoToSqlDate);
                if (mEntities != null && mEntities.Count > 0)
                {
                    foreach (MTag mEntity in mEntities)
                    {
                        Tag sqlEntity = new Tag();
                        sqlEntity.CopyFrom(mEntity);
                        sqlEntity.Guid = mEntity._id;
                        _iTagService.InsertTagNoReturn(sqlEntity);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteError("Mongo TO Sql Migration: " + SQLEntityName + " Migration", ex);
                return false;
            }
        }

        public bool SynchBunchTags()
        {
            string SQLEntityName = "BunchTags";
            try
            {
                //Get Resource Date                            
                List<MBunchTag> mEntities = GetCollection<MBunchTag>("LastUpdateDate", "BunchTag", "LastUpdateDate", "BunchTag", MongoToSqlDate);
                if (mEntities != null && mEntities.Count > 0)
                {
                    foreach (MBunchTag mEntity in mEntities)
                    {
                        Tag tag = _iTagService.GetByGuid(mEntity.TagId);
                        Bunch bunch = _iBunchService.GetBunchByGuid(mEntity.BunchId);
                        BunchTag sqlEntity = new BunchTag();
                        sqlEntity.CopyFrom(mEntity);
                        sqlEntity.TagId = tag.TagId;
                        sqlEntity.BunchId = bunch.BunchId;
                        _iBunchTagService.InsertIfNotExistsNoReturn(sqlEntity);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteError("Mongo TO Sql Migration: " + SQLEntityName + " Migration", ex);
                return false;
            }
        }

        public bool SynchTagResources()
        {
            string SQLEntityName = "TagResources";
            try
            {
                //Get Resource Date                            
                List<MTagResource> mEntities = GetCollection<MTagResource>("LastUpdateDate", "TagResource", "LastUpdateDate", "TagResource", MongoToSqlDate, 100000);
                if (mEntities != null && mEntities.Count > 0)
                {
                    List<TagResource> tagResources = new List<TagResource>();
                    List<Resource> resources = _iResourceService.GetAllResource();
                    List<Tag> tags = _iTagService.GetAllTag();
                    foreach (MTagResource mEntity in mEntities)
                    {
                        Tag tag = tags.Where(x => x.Guid == mEntity.TagId).FirstOrDefault();
                        List<TagResource> tagResourceNewsDelete = _iTagResourceService.GetTagResourceByTagId(tag.TagId);
                        if (tagResourceNewsDelete != null && tagResourceNewsDelete.Count > 0)
                        {
                            foreach (TagResource tresource in tagResourceNewsDelete)
                            {
                                _iTagResourceService.DeleteTagResource(tresource.TagResourceid);
                            }
                        }
                        TagResource tagresource = new TagResource();
                        tagresource.CopyFrom(mEntity);
                        //Get All Resources to save the hits

                        if (mEntity.Resources != null && mEntity.Resources.Count > 0)
                        {
                            foreach (MResource mResource in mEntity.Resources)
                            {
                                Resource resource = resources.Where(x => x.Guid == mResource.Guid).FirstOrDefault();
                                //Resource resource = _iResourceService.GetResourceByGuid(mResource.Guid);
                                tagresource.ResourceId = resource.ResourceId;
                                tagresource.TagId = tag.TagId;
                                tagResources.Add(tagresource);
                                //_iTagResourceService.InsertTagResourceNoReturn(tagresource);                                
                                //  sqlMigrationRepository.BulkInsert<TagResource>(tagResources, SQLEntityName, ignoreColumnList);
                            }
                        }
                    }
                    sqlMigrationRepository.BulkInsert<TagResource>(tagResources, SQLEntityName, ignoreColumnList);
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteError("Mongo TO Sql Migration: " + SQLEntityName + " Migration", ex);
                return false;
            }
        }

        public bool SyncNewsFilters()
        {
            string SQLEntityName = "NewsFilters";
            try
            {
                //Get Resource Date                            
                List<MNewsFilter> mEntities = GetCollection<MNewsFilter>("CreationDate", "NewsFilter", "CreationDate", "NewsFilter", MongoToSqlDate);
                if (mEntities != null && mEntities.Count > 0)
                {
                    foreach (MNewsFilter mEntity in mEntities)
                    {
                        News news = _iNewsService.GetByNewsGuid(mEntity.NewsId);
                        if (news != null)
                        {
                            NewsFilter sqlEntity = new NewsFilter();
                            sqlEntity.CopyFrom(mEntity);
                            sqlEntity.NewsId = news.NewsId;
                            if (sqlEntity.LastUpdateDate == null || Convert.ToDateTime(sqlEntity.LastUpdateDate).Year <= 2000)
                                sqlEntity.LastUpdateDate = sqlEntity.CreationDate;

                            if (_iNewsFilterService.GetByFilterIdAndNewsId(sqlEntity.FilterId, sqlEntity.NewsId) == null)
                            {
                                _iNewsFilterService.InsertNewsFilter(sqlEntity);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteError("Mongo TO Sql Migration: " + SQLEntityName + " Migration", ex);
                return false;
            }
        }

        public bool SynchBunchResource()
        {
            string SQLEntityName = "BunchResource";
            try
            {
                //Get Resource Date                            
                List<MBunch> mEntities = GetCollection<MBunch>("LastUpdateDate", "BunchResource", "LastUpdateDate", "Bunch", MongoToSqlDate);
                if (mEntities != null && mEntities.Count > 0)
                {
                    foreach (MBunch mBunch in mEntities)
                    {
                        Bunch bunch = _iBunchService.GetBunchByGuid(mBunch._id);
                        List<BunchResource> bunchResourceToDelete = _iBunchResourceService.GetBunchResourceByBunchId(bunch.BunchId);

                        if (bunchResourceToDelete != null && bunchResourceToDelete.Count > 0)
                        {
                            foreach (BunchResource bfn in bunchResourceToDelete)
                            {
                                _iBunchFilterService.DeleteBunchFilter(bfn.BunchResourceId);
                            }
                        }

                        BunchResource bunchResource = new BunchResource();
                        bunchResource.CopyFrom(mBunch);

                        if (mBunch.Resources != null && mBunch.Resources.Count > 0)
                        {
                            foreach (MResource mResource in mBunch.Resources)
                            {
                                Resource resource = _iResourceService.GetResourceByGuid(mResource.Guid);
                                if (resource != null)
                                {
                                    bunchResource.ResourceId = resource.ResourceId;
                                    bunchResource.BunchId = bunch.BunchId;
                                    sqlMigrationRepository.InsertEntityNoReturn<BunchResource>(bunchResource, "BunchResource"); //using dynamic method for insert only
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteError("Mongo TO Sql Migration: " + SQLEntityName + " Migration", ex);
                return false;
            }
        }

        #endregion

        #region Sql to Mongo Migration

        public bool CompleteSqltoMongo()
        {
            try
            {
                if (MongoSynchResources() == false)
                    return false;
                if (MongoSynchBunch() == false)
                    return false;
                if (MongoSynchBunchFilters() == false)
                    return false;
                if (MongoSynchBunchNews() == false)
                    return false;
                if (MongoSynchComments() == false)
                    return false;
                if (MongoSynchTags() == false)
                    return false;
                if (MongoSynchBunchTags() == false)
                    return false;
                if (MongoSynchTagResources() == false)
                    return false;
                if (MongoSyncNewsFilters() == false)
                    return false;
                if (MongoSyncNews() == false)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool MongoSynchResources()
        {
            try
            {
                List<Resource> sqlEntities = _iResourceService.GetAllResource();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    foreach (Resource resource in sqlEntities)
                    {
                        MResource mr = new MResource();
                        mr.CopyFrom(_iResourceService.GetByGuid(resource.Guid));
                        if (mr.Guid != null)
                        {
                            _iResourceService.DeleteByResourceGuid(resource.Guid);
                        }
                        MResource mResource = new MResource();
                        mResource.CopyFrom(resource);
                        _iResourceService.InsertResource(mResource);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return true;
        }

        public bool MongoSynchBunch()
        {
            try
            {

                List<Bunch> sqlEntities = _iBunchService.GetAllBunch();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {

                    foreach (Bunch bunch in sqlEntities)
                    {
                        MBunch mB = _iBunchService.GetByGuid(bunch.Guid);
                        if (mB != null)
                        {
                            _iBunchService.DeleteByBunchId(bunch.Guid);
                        }

                        MBunch mBunch = new MBunch();
                        mBunch.CopyFrom(bunch);
                        mBunch._id = new BaseEntity { _id = bunch.Guid }._id;

                        List<BunchResource> bunchResources = _iBunchResourceService.GetBunchResourceByBunchId(bunch.BunchId);
                        if (bunchResources != null && bunchResources.Count > 0)
                        {
                            mBunch.Resources = new List<MResource>();
                            foreach (BunchResource bresource in bunchResources)
                            {
                                MResource mR = new MResource();
                                mR.CopyFrom(_iResourceService.GetResource(bresource.ResourceId));
                                mBunch.Resources.Add(mR);
                            }
                            _iBunchService.InsertBunch(mBunch);
                        }
                        else
                        {
                            _iBunchService.InsertBunch(mBunch);
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return true;
        }

        public bool MongoSynchBunchFilters()
        {
            try
            {

                List<BunchFilter> sqlEntities = _iBunchFilterService.GetAllBunchFilter();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {

                    foreach (BunchFilter bunchFilter in sqlEntities)
                    {
                        MBunchFilter mBunchFilter = new MBunchFilter();
                        mBunchFilter.CopyFrom(bunchFilter);

                        Bunch b = new Bunch();
                        b.CopyFrom(_iBunchService.GetBunch(bunchFilter.BunchId));
                        mBunchFilter.BunchId = b.Guid;

                        if (_iBunchFilterService.CheckIfBunchFilterExists(mBunchFilter) == true)
                        {
                            _iBunchFilterService.DeleteByBunchAndFilterId(mBunchFilter);
                        }

                        List<BunchFilterNews> bunchFilterNews = _iBunchFilterNewsService.GetAllBunchFilterNews();
                        mBunchFilter.NewsList = new List<MNews>();
                        if (bunchFilterNews != null && bunchFilterNews.Count > 0)
                        {
                            foreach (BunchFilterNews bFn in bunchFilterNews)
                            {
                                MNews mN = new MNews();
                                // mN.CopyFrom(_iNewsService.GetNews(Convert.ToInt32(bFn.NewsId)));
                                mBunchFilter.NewsList.Add(mN);
                            }
                            _iBunchFilterService.InsertBunchFilter(mBunchFilter);
                        }
                        else
                        {
                            _iBunchFilterService.InsertBunchFilter(mBunchFilter);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return true;
        }

        public bool MongoSynchBunchNews()
        {

            try
            {
                List<BunchNews> sqlEntities = _iBunchNewsService.GetAllBunchNews();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {

                    foreach (BunchNews bunchNews in sqlEntities)
                    {
                        MBunchNews mBN = new MBunchNews();
                        mBN.CopyFrom(bunchNews);

                        Bunch b = new Bunch();
                        b.CopyFrom(_iBunchService.GetBunch(Convert.ToInt32(bunchNews.BunchId)));
                        mBN.BunchId = b.Guid;

                        News n = new News();
                        n.CopyFrom(_iNewsService.GetNews(Convert.ToInt32(bunchNews.NewsId)));
                        mBN.NewsId = n.Guid;
                        mBN.LastUpdateDate = n.LastUpdateDate;

                        List<MBunchNews> mbNews = _iBunchNewsService.GetByBunchIdAndNewsId(n.Guid, b.Guid);
                        if (mbNews != null & mbNews.Count > 0)
                        {
                            _iBunchNewsService.DeleteByBunchIdAndNewsId(n.Guid, b.Guid);
                        }
                        _iBunchNewsService.InsertBunchNews(mBN);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return true;
        }

        public bool MongoSynchComments()
        {
            try
            {
                List<Comment> sqlEntities = _iCommentService.GetAllComment();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    foreach (Comment comments in sqlEntities)
                    {
                        MComment mComments = new MComment();
                        mComments._id = new BaseEntity { _id = comments.Guid }._id;
                        mComments.CopyFrom(comments);
                        mComments.NewsId = new BaseEntity { _id = comments.NewsGuid }._id;

                        News n = new News();
                        n.CopyFrom(_iNewsService.GetByNewsGuid(comments.NewsGuid));
                        mComments.ReporterId.CopyFrom(n.ReporterId);

                        _iCommentService.InsertComment(mComments);

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return true;
        }

        public bool MongoSynchTags()
        {

            try
            {
                List<Tag> sqlEntities = _iTagService.GetAllTag();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    foreach (Tag tags in sqlEntities)
                    {
                        MTag mTags = new MTag();
                        if (_iTagService.CheckIfTagExists(tags.Tag) == true)
                        {
                            _iTagService.DeleteByTagGuid(tags.Guid);
                        }
                        mTags.CopyFrom(tags);
                        mTags._id = new BaseEntity { _id = tags.Guid }._id;
                        _iTagService.InsertTag(mTags);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return true;
        }

        public bool MongoSynchBunchTags()
        {

            try
            {
                List<BunchTag> sqlEntities = _iBunchTagService.GetAllBunchTag();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    foreach (BunchTag bunchTags in sqlEntities)
                    {
                        MBunchTag mBunchTags = new MBunchTag();
                        mBunchTags.CopyFrom(bunchTags);

                        Bunch b = new Bunch();
                        b.CopyFrom(_iBunchService.GetBunch(bunchTags.BunchId));
                        mBunchTags.BunchId = b.Guid;

                        Tag tg = new Tag();
                        tg.CopyFrom(_iTagService.GetTag(bunchTags.TagId));
                        mBunchTags.TagId = tg.Guid;

                        if (_iBunchTagService.CheckIfBunchTagExists(mBunchTags) == true) ;
                        {
                            _iBunchTagService.DeleteByTagId(tg.Guid);
                        }
                        _iBunchTagService.InsertBunchTag(mBunchTags);

                    }
                }
            }
            catch (Exception ex)
            {
            }

            return true;
        }

        public bool MongoSynchTagResources()
        {
            try
            {
                List<TagResource> sqlEntities = _iTagResourceService.GetAllTagResource();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    var DistinctItems = sqlEntities.GroupBy(x => x.TagId).Select(y => y.First());
                    foreach (var tagRs in DistinctItems)
                    {
                        MTagResource mTagResources = new MTagResource();
                        mTagResources.CopyFrom(tagRs);

                        Tag tag = _iTagService.GetTag(Convert.ToInt32(tagRs.TagId));
                        mTagResources.TagId = tag.Guid;

                        if (_iTagResourceService.CheckIfTagResourceExists(tag.Guid) == true)
                        {
                            _iTagResourceService.DeleteByTagGuid(tag.Guid);
                        }

                        mTagResources.Resources = new List<MResource>();

                        List<TagResource> tr = _iTagResourceService.GetTagResourceByTagId(tagRs.TagId);
                        foreach (var distinctTagresource in tr)
                        {
                            MResource r = new MResource();
                            r.CopyFrom(_iResourceService.GetResource(Convert.ToInt32(distinctTagresource.ResourceId)));
                            mTagResources.Resources.Add(r);
                        }
                        _iTagResourceService.InsertTagResource(mTagResources);
                    }
                }
            }

            catch (Exception ex)
            {
            }
            return true;
        }

        public bool MongoSyncNewsFilters()
        {
            try
            {
                List<NewsFilter> sqlEntities = _iNewsFilterService.GetAllNewsFilter();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    foreach (NewsFilter newsFilter in sqlEntities)
                    {
                        MNewsFilter mNewsFilter = new MNewsFilter();
                        mNewsFilter.CopyFrom(newsFilter);

                        News ns = new News();
                        ns.CopyFrom(_iNewsService.GetNews(newsFilter.NewsId));
                        mNewsFilter.NewsId = ns.Guid;

                        if (_iNewsFilterService.CheckIfNewsFilterExists(mNewsFilter) == true)
                        {
                            _iNewsFilterService.DeleteNewsFilterByNewsAndFilterId(mNewsFilter);
                        }

                        mNewsFilter.ReporterId = Convert.ToInt32(ns.ReporterId);
                        _iNewsFilterService.InsertNewsFilter(mNewsFilter);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return true;
        }

        public bool MongoSyncNews()
        {
            try
            {
                List<News> sqlEntities = _iNewsService.GetAllNews();
                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    foreach (News news in sqlEntities)
                    {
                        MNews mNews = new MNews();
                        mNews.CopyFrom(news);
                        mNews._id = new BaseEntity { _id = news.Guid }._id;

                        if (_iNewsService.CheckIfNewsExists(mNews) == true)
                        {
                            _iNewsService.DeleteNews(mNews);
                        }


                        #region Insert NewsFilters
                        List<NewsFilter> NewsFilters = _iNewsFilterService.GetNewsFilterByNewsId(news.NewsId);
                        if (NewsFilters != null && NewsFilters.Count > 0)
                        {
                            mNews.Filters = new List<MNewsFilter>();
                            foreach (NewsFilter nF in NewsFilters)
                            {
                                MNewsFilter mNF = new MNewsFilter();
                                News n = new News();
                                n.CopyFrom(_iNewsService.GetNews(nF.NewsId));
                                mNF.CopyFrom(nF);
                                mNF.NewsId = n.Guid;
                                mNews.Filters.Add(mNF);
                            }
                        }
                        #endregion

                        #region Insert NewsTags
                        List<NewsTag> NewsTags = _iNewsTagService.GetNewsTagByNewsId(news.NewsId);
                        if (NewsTags != null && NewsTags.Count > 0)
                        {
                            mNews.Tags = new List<MTag>();
                            foreach (NewsTag nT in NewsTags)
                            {
                                MTag mTag = new MTag();
                                Tag t = _iTagService.GetTag(nT.TagId);

                                mTag.CopyFrom(nT);
                                mTag.Tag = t.Tag;
                                mNews.Tags.Add(mTag);
                            }
                        }
                        #endregion

                        #region Insert NewsLocation
                        List<NewsLocation> nL = _iNewsLocationService.GetNewsLocationByNewsId(news.NewsId);
                        if (nL != null && nL.Count > 0)
                        {
                            mNews.Locations = new List<Location>();
                            foreach (NewsLocation nlocation in nL)
                            {
                                Location Lt = new Location();
                                Location loc = _iLocationService.GetLocation(Convert.ToInt32(nlocation.LocationId));
                                Lt.CopyFrom(nlocation);
                                Lt.Location = loc.Location;
                                mNews.Locations.Add(Lt);
                            }
                        }
                        #endregion

                        #region Insert NewsCategory
                        List<NewsCategory> NewsCategory = _iNewsCategoryService.GetNewsCategoryByNewsId(news.NewsId);
                        if (NewsCategory != null && NewsCategory.Count > 0)
                        {
                            mNews.Categories = new List<Category>();
                            foreach (NewsCategory nC in NewsCategory)
                            {
                                Category Ct = new Category();
                                Ct.CopyFrom(_iCategoryService.GetCategory(nC.CategoryId));
                                mNews.Categories.Add(_iCategoryService.GetCategory(nC.CategoryId));
                            }
                        }
                        #endregion

                        #region Insert NewsResource
                        List<NewsResource> NewsResources = _iNewsResourceService.GetNewsResourceByNewsId(news.NewsId);
                        if (NewsResources != null && NewsResources.Count > 0)
                        {
                            mNews.Resources = new List<MResource>();
                            foreach (NewsResource nR in NewsResources)
                            {
                                MResource mR = new MResource();
                                mR.CopyFrom(_iResourceService.GetResource(nR.ResourceId));
                                mNews.Resources.Add(mR);
                            }
                        }
                        #endregion

                        #region Insert NewsComments
                        Comment NewsComments = _iCommentService.GetByNewsGuid(news.Guid);
                        if (NewsComments != null)
                        {
                            mNews.Comments = new List<MComment>();
                            MComment mcom = new MComment();
                            mcom.CopyFrom(NewsComments);
                            mcom.NewsId = new BaseEntity { _id = NewsComments.NewsGuid }._id;
                            mNews.Comments.Add(mcom);

                        }
                        #endregion

                        #region Insert ResourceEdit
                        NewsResourceEdit NewsResourceEdt = new NewsResourceEdit();
                        NewsResourceEdt.CopyFrom(_iNewsResourceEditService.GetNewsResourceEditByNewsId(news.NewsId));
                        if (NewsResourceEdt != null)
                        {
                            mNews.ResourceEdit = new ResourceEdit();
                            mNews.ResourceEdit.CopyFrom(NewsResourceEdt);
                            mNews.ResourceEdit.FromTos = new List<FromTo>();

                            List<FromTo> fromToList = new List<FromTo>();
                            fromToList.CopyFrom(_iResourceEditclipinfoService.GetResourceEditclipinfoByNewsResourceEditId(NewsResourceEdt.NewsResourceEditId));
                            foreach (FromTo frmTo in fromToList)
                            {
                                mNews.ResourceEdit.FromTos.Add(frmTo);
                            }
                        }
                        #endregion

                        _iNewsService.InsertNews(mNews);
                    }
                }
            }

            catch (Exception ex)
            {

            }
            return true;
        }

        #endregion

        #region sql to sql initialization

        //IResourceService iArchivalResourceService = IoC.Resolve<IResourceService>("ArchivalResourceService");
        //IBunchService iArchivalBunchService = IoC.Resolve<IBunchService>("ArchivalBunchService");
        //IBunchResourceService iArchivalBunchResourceService = IoC.Resolve<IBunchResourceService>("ArchivalBunchResourceService");
        //IBunchFilterService iArchivalBunchFilterService = IoC.Resolve<IBunchFilterService>("ArchivalBunchFilterService");
        //IBunchFilterNewsService iArchivalBunchFilterNewsService = IoC.Resolve<IBunchFilterNewsService>("ArchivalBunchFilterNewsService");
        //IBunchNewsService iArchivalBunchNewsService = IoC.Resolve<IBunchNewsService>("ArchivalBunchNewsService");
        //ICommentService iArchivalCommentService = IoC.Resolve<ICommentService>("ArchivalCommentService");
        //ITagService iArchivalTagService = IoC.Resolve<ITagService>("ArchivalTagService");
        //IBunchTagService iArchivalBunchTagService = IoC.Resolve<IBunchTagService>("ArchivalBunchTagService");
        //ITagResourceService iArchivalTagResourceService = IoC.Resolve<ITagResourceService>("ArchivalTagResourceService");
        //INewsFilterService iArchivalNewsFilterService = IoC.Resolve<INewsFilterService>("ArchivalNewsFilterService");
        //INewsService iArchivalNewsService = IoC.Resolve<INewsService>("ArchivalNewsService");


        IResourceService iArchivalResourceService = null;
        IBunchService iArchivalBunchService = null;
        IBunchResourceService iArchivalBunchResourceService = null;
        IBunchFilterService iArchivalBunchFilterService = null;
        IBunchFilterNewsService iArchivalBunchFilterNewsService = null;
        IBunchNewsService iArchivalBunchNewsService = null;
        ICommentService iArchivalCommentService = null;
        ITagService iArchivalTagService = null;
        IBunchTagService iArchivalBunchTagService = null;
        ITagResourceService iArchivalTagResourceService = null;
        INewsFilterService iArchivalNewsFilterService = null;
        INewsService iArchivalNewsService = null;


        #endregion

        #region Sql to Sql Migration

        public bool CompleteSqltoSql(SqlConnection connArchival, SqlConnection connSource)
        {
            try
            {
                //List<object, string> EntityList = new List<object, string>();        

                #region Table List
                List<KeyValuePair<string, object>> lsts = new List<KeyValuePair<string, object>>();

                lsts.Add(new KeyValuePair<string, object>("FilterType", new FilterType()));
                lsts.Add(new KeyValuePair<string, object>("ResourceType", new ResourceType()));
                lsts.Add(new KeyValuePair<string, object>("NewsType", new NewsType()));
                lsts.Add(new KeyValuePair<string, object>("CameraType", new CameraType()));
                lsts.Add(new KeyValuePair<string, object>("SlotType", new SlotType()));
                lsts.Add(new KeyValuePair<string, object>("recurringtype", new RecurringType()));

                lsts.Add(new KeyValuePair<string, object>("Filter", new Filter()));
                lsts.Add(new KeyValuePair<string, object>("Location", new Location()));
                lsts.Add(new KeyValuePair<string, object>("LocationAlias", new LocationAlias()));
                lsts.Add(new KeyValuePair<string, object>("Category", new Category()));
                lsts.Add(new KeyValuePair<string, object>("CategoryAlias", new CategoryAlias()));

                lsts.Add(new KeyValuePair<string, object>("Bunch", new Bunch()));
                lsts.Add(new KeyValuePair<string, object>("News", new News()));
                lsts.Add(new KeyValuePair<string, object>("Resource", new Resource()));
                lsts.Add(new KeyValuePair<string, object>("Tag", new Tag()));

                lsts.Add(new KeyValuePair<string, object>("BunchFilter", new BunchFilter()));
                lsts.Add(new KeyValuePair<string, object>("BunchFilterNews", new BunchFilterNews()));
                lsts.Add(new KeyValuePair<string, object>("BunchNews", new BunchNews()));
                lsts.Add(new KeyValuePair<string, object>("BunchResource", new BunchResource()));
                lsts.Add(new KeyValuePair<string, object>("BunchTag", new BunchTag()));
                lsts.Add(new KeyValuePair<string, object>("NewsTag", new NewsTag()));
                lsts.Add(new KeyValuePair<string, object>("TagResource", new TagResource()));

                lsts.Add(new KeyValuePair<string, object>("Celebrity", new Celebrity()));
                lsts.Add(new KeyValuePair<string, object>("Channel", new Channel()));
                lsts.Add(new KeyValuePair<string, object>("ChannelVideo", new ChannelVideo()));
                lsts.Add(new KeyValuePair<string, object>("Comment", new Comment()));
                lsts.Add(new KeyValuePair<string, object>("Program", new Program()));
                lsts.Add(new KeyValuePair<string, object>("Episode", new Episode()));

                lsts.Add(new KeyValuePair<string, object>("NewsCategory", new NewsCategory()));
                lsts.Add(new KeyValuePair<string, object>("NewsFilter", new NewsFilter()));
                lsts.Add(new KeyValuePair<string, object>("NewsLocation", new NewsLocation()));
                lsts.Add(new KeyValuePair<string, object>("NewsPaper", new NewsPaper()));
                lsts.Add(new KeyValuePair<string, object>("DailyNewsPaper", new DailyNewsPaper()));
                lsts.Add(new KeyValuePair<string, object>("NewsPaperPage", new NewsPaperPage()));
                lsts.Add(new KeyValuePair<string, object>("NewsPaperPagesPart", new NewsPaperPagesPart()));
                lsts.Add(new KeyValuePair<string, object>("NewsPaperPagesPartsDetail", new NewsPaperPagesPartsDetail()));
                lsts.Add(new KeyValuePair<string, object>("NewsResource", new NewsResource()));

                lsts.Add(new KeyValuePair<string, object>("ProgramSchedule", new ProgramSchedule()));
                lsts.Add(new KeyValuePair<string, object>("ProgramScreenElement", new ProgramScreenElement()));
                lsts.Add(new KeyValuePair<string, object>("RadioStation", new RadioStation()));
                lsts.Add(new KeyValuePair<string, object>("RadioStream", new RadioStream()));

                lsts.Add(new KeyValuePair<string, object>("Role", new Role()));
                lsts.Add(new KeyValuePair<string, object>("ScrapMaxDates", new ScrapMaxDates()));
                lsts.Add(new KeyValuePair<string, object>("ScreenElement", new ScreenElement()));
                lsts.Add(new KeyValuePair<string, object>("ScreenTemplate", new ScreenTemplate()));
                lsts.Add(new KeyValuePair<string, object>("SegmentType", new SegmentType()));
                lsts.Add(new KeyValuePair<string, object>("Segment", new Segment()));

                lsts.Add(new KeyValuePair<string, object>("SocialMedia", new SocialMedia()));

                lsts.Add(new KeyValuePair<string, object>("TeamRole", new TeamRole()));
                lsts.Add(new KeyValuePair<string, object>("Team", new Team()));
                lsts.Add(new KeyValuePair<string, object>("User", new User()));

                lsts.Add(new KeyValuePair<string, object>("TemplateScreenElement", new TemplateScreenElement()));
                lsts.Add(new KeyValuePair<string, object>("TwitterAccount", new TwitterAccount()));

                lsts.Add(new KeyValuePair<string, object>("UserRole", new UserRole()));
                lsts.Add(new KeyValuePair<string, object>("Website", new Website()));
                lsts.Add(new KeyValuePair<string, object>("Wire", new Wire()));

                #endregion

                string Updateddate = "LastUpdateDate";
                List<string> UpdateColumnList = new List<string>();

                UpdateColumnList.Add("bunchnews");
                UpdateColumnList.Add("cameratype");

                foreach (KeyValuePair<string, object> kv in lsts)
                {
                    if (UpdateColumnList.IndexOf(kv.Key.ToString().ToLower()) == -1)
                        Updateddate = "LastUpdateDate";
                    else
                        Updateddate = "CreationDate";

                    if (kv.Key.ToString().ToLower() == "cameratype" || kv.Key.ToString().ToLower() == "resourcetype"
                        || kv.Key.ToString().ToLower() == "newstype" || kv.Key.ToString().ToLower() == "slottype"
                        || kv.Key.ToString().ToLower() == "role" || kv.Key.ToString().ToLower() == "scrapmaxdates"
                        || kv.Key.ToString().ToLower() == "segmenttype" || kv.Key.ToString().ToLower() == "lastupdatedate"
                        || kv.Key.ToString().ToLower() == "teamrole")

                        Updateddate = "";

                    MethodInfo method = this.GetType().GetMethod("SqlToSqlSynch");
                    MethodInfo generic = method.MakeGenericMethod(kv.Value.GetType());
                    generic.Invoke(this, new object[] { kv.Key, Updateddate, connArchival, connSource });

                    //Type typeArgument = kv.Value.GetType();
                    ////Type genericClass = typeof(List<>);                    
                    ////Type constructedClass = genericClass.MakeGenericType(typeArgument);
                    ////var list = Activator.CreateInstance(constructedClass);       

                    //SqlToSqlSynch<T>(kv.Key, "LastUpdateDate");
                    //if (SqlToSqlSynch<kv.Value.GetType()>("Bunch", "LastUpdateDate") == false)                 
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        DateTime DtMaxDate = DateTime.Now;
        List<string> ignoreColumnList = new List<string>() { "CreationDateStr", "LastUpdateDateStr", "IsUpdated", 
            "PublishTimeStr", "UpdateTimeStr", "FromStr", "ToStr", "DateStr","Segments" ,"MaxUpdateDateStr","CreatonDateStr","TimeStr","CreatedDate","CreatedDateStr"};

        public bool SqlToSqlSynch<T>(string EntityName, string UpdatedDate, SqlConnection connArchival, SqlConnection connSource)
        {


            if (EntityName == "TemplateScreenElement")
            {
                ignoreColumnList.Add("ProgramScreenElementId");
                ignoreColumnList.Add("ImageGuid");
            }

            if (EntityName == "Segment")
                ignoreColumnList.Add("Slots");
            if (EntityName == "Program")
                ignoreColumnList.Add("Schedule");
            if (EntityName == "ScreenTemplate")
                ignoreColumnList.Add("Elements");

            if (EntityName == "Celebrity")
            {
                ignoreColumnList.Add("Location");
                ignoreColumnList.Add("Category");
            }


            try
            {
                List<T> sqlEntities = null;

                if (UpdatedDate == "")
                    sqlEntities = sqlMigrationRepository.GetAll<T>(EntityName);
                else
                {
                    DateTime LastDate = sqlMigrationRepository.GetUpdatedDate(UpdatedDate, EntityName, connArchival);
                    if (EntityName == "Bunch" && LastDate.Year < 2014)
                    {
                        DtMaxDate = sqlMigrationRepository.GetUpdatedDate(UpdatedDate, EntityName, connSource);
                    }
                    else if (EntityName == "Bunch")
                        DtMaxDate = LastDate;
                    sqlEntities = sqlMigrationRepository.GetByDateWithParam<T>(UpdatedDate, LastDate, DtMaxDate, EntityName, connSource);
                }

                if (sqlEntities != null && sqlEntities.Count > 0)
                {
                    foreach (T entity in sqlEntities)
                    {
                        if (sqlMigrationRepository.GetEntity<T>(entity, EntityName, connArchival) != null)
                            sqlMigrationRepository.UpdateEntity<T>(entity, EntityName, connArchival, ignoreColumnList);
                        else
                            sqlMigrationRepository.InsertEntity<T>(entity, EntityName, connArchival, ignoreColumnList);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteError("SQL TO Sql Migration: " + EntityName + " Migration", ex);
            }
            return true;
        }

        #endregion

        #region Helping Methods

        public List<T> GetEntitiesByDate<T>(DateTime dt, string columnName, string collectionName)
        {
            return mongoMigrationRepository.GetEntitiesByDate<T>(dt, columnName, collectionName);
        }

        private List<T> GetCollection<T>(string SqlColumnName, string SqlCollectionName, string MongoColumnName, string MongoCollectionName, bool IsFirstTime = false)
        {
            //Addded 2 miliseconds for mongo synchronization issues
            DateTime LastDate = new DateTime();
            if (!IsFirstTime)
            {
                LastDate = DateTime.SpecifyKind(sqlMigrationRepository.GetUpdatedDate(SqlColumnName, SqlCollectionName), DateTimeKind.Utc).AddMilliseconds(2);
            }
            else
                LastDate = new DateTime(2000, 1, 1);
            List<T> mEntities = mongoMigrationRepository.GetEntitiesByDate<T>(LastDate, MongoColumnName, MongoCollectionName);
            return mEntities;
        }

        private List<T> GetCollection<T>(string SqlColumnName, string SqlCollectionName, string MongoColumnName, string MongoCollectionName, DateTime MaxDate, int? Limit = null)
        {
            //Addded 2 miliseconds for mongo synchronization issues
            DateTime LastDate = new DateTime();
            LastDate = DateTime.SpecifyKind(sqlMigrationRepository.GetUpdatedDate(SqlColumnName, SqlCollectionName), DateTimeKind.Utc).AddMilliseconds(2);
            List<T> mEntities = mongoMigrationRepository.GetEntitiesByDate<T>(LastDate, MaxDate, MongoColumnName, MongoCollectionName, Limit);
            return mEntities;
        }

        private List<T> GetEntitiesByDateAndIsCompleted<T>(string SqlColumnName, string SqlColumnName1, string SqlCollectionName, string MongoColumnName, string MongoColumnName1, string MongoCollectionName, DateTime MaxDate, bool IsFirstTime = false)
        {
            //Addded 2 miliseconds for mongo synchronization issues
            DateTime LastDate = new DateTime();
            //if (!IsFirstTime)
            //{
            LastDate = DateTime.SpecifyKind(sqlMigrationRepository.GetUpdatedDate(SqlColumnName, SqlCollectionName), DateTimeKind.Utc).AddMilliseconds(2);
            //}
            //else
            //LastDate = new DateTime(2000, 1, 1);
            bool IsCompleted = false;
            List<T> mEntities = mongoMigrationRepository.GetEntitiesByDateAndIsCompleted<T>(LastDate, MaxDate, MongoColumnName, IsCompleted, MongoColumnName1, MongoCollectionName);
            return mEntities;
        }

        private List<T> GetAllCollection<T>(string MongoCollectionName)
        {
            List<T> mEntities = mongoMigrationRepository.GetAllEntities<T>(MongoCollectionName);
            return mEntities;
        }

        private List<T> GetCollectionBydate<T>(DateTime dt, string SqlCollectionName)
        {
            List<T> Entities = new List<T>();
            Entities.CopyFrom(sqlMigrationRepository.GetByDate<T>(dt, SqlCollectionName));
            return Entities;
        }



        #endregion

        #region loging
        private static void WriteError(string MigrationType, Exception ex)
        {
            try
            {
                string path = "C://MigrationLog//Error//" + DateTime.Today.ToString("dd-mm-yy") + ".txt";
                if (!Directory.Exists("C://MigrationLog//Error"))
                {
                    Directory.CreateDirectory("C://MigrationLog//Error");
                }

                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                }
                using (StreamWriter w = File.AppendText(path))
                {
                    w.WriteLine("\r\nLog Entry : ");
                    w.WriteLine("{0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    string err = "Error in: " + MigrationType +
                                  ". Error Message:" + ex.ToString();
                    w.WriteLine(err);
                    w.WriteLine("__________________________");
                    w.Flush();
                    w.Close();
                }
            }
            catch (Exception ex1)
            {
                // WriteLogError(ex.Message);
            }
        }
        #endregion



        #region new Migration Code for Archival





        #endregion
    }


}
