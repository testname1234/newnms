﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.IService;
using NMS.TextAnalysis.SemanticLibrary;

namespace NMS.Service
{
    public class MNewsService:IMNewsService
    {
        IMNewsRepository _mongoNewsRepository;
        IMTagRepository _mongoTagRepository;
        INewsRepository _sqlNewsRepository;
        public MNewsService(IMNewsRepository mongoNewsRepository,IMTagRepository mongoTagRepository, INewsRepository sqlNewsRepository)
        {
            this._mongoNewsRepository = mongoNewsRepository;
            this._sqlNewsRepository = sqlNewsRepository;
            this._mongoTagRepository = mongoTagRepository;
        }

        public void InsertNews(MParsedNews parsedNews)
        {
            KeywordAnalyzer keywordAnalyzer = new KeywordAnalyzer();
            var keyWordObj=keywordAnalyzer.Analyze(parsedNews.Description);
            MNews news = new MNews();
            news.Title = parsedNews.Title;
            news.PublishTime = parsedNews.PublishTime;
            news.LastUpdateDate = parsedNews.UpdateTime;
            news.CreationDate = DateTime.UtcNow;
            news.Description = parsedNews.Description;
            news.Author = parsedNews.Author;
            news.LanguageCode = "eng";
            news.Tags = new List<MTag>();
            foreach (var item in keyWordObj.Keywords)
            {
                MTag tag = new MTag();
                tag.Tag = item.Word;
                tag.Rank = Convert.ToDouble(item.Rank);
                news.Tags.Add(tag);
                _mongoTagRepository.InsertTag(tag);
            }
            _mongoNewsRepository.InsertNews(news);
        }
    }
}
