﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosActiveEpisode;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class MosActiveEpisodeService : IMosActiveEpisodeService 
	{
		private IMosActiveEpisodeRepository _iMosActiveEpisodeRepository;
        
		public MosActiveEpisodeService(IMosActiveEpisodeRepository iMosActiveEpisodeRepository)
		{
			this._iMosActiveEpisodeRepository = iMosActiveEpisodeRepository;
		}

        public void UpdateStatus(int StatusCode, int MosActiveEpisodeId, string PCRTelePropterJson, string PCRPlayoutJson)
        {
           //_iMosActiveEpisodeRepository.UpdateStatus(StatusCode, MosActiveEpisodeId);
            _iMosActiveEpisodeRepository.UpdateStatus(StatusCode, MosActiveEpisodeId, PCRTelePropterJson, PCRPlayoutJson);            
        }

        //UpdateStatus(int StatusCode, int MosActiveEpisodeId)
        
        public Dictionary<string, string> GetMosActiveEpisodeBasicSearchColumns()
        {
            
            return this._iMosActiveEpisodeRepository.GetMosActiveEpisodeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMosActiveEpisodeAdvanceSearchColumns()
        {
            
            return this._iMosActiveEpisodeRepository.GetMosActiveEpisodeAdvanceSearchColumns();
           
        }

        public virtual MosActiveEpisode InsertUpdateIfExist(MosActiveEpisode mosActiveEpisode)
        {
            MosActiveEpisode mosActiveEpisodeNew = new MosActiveEpisode();
            List<MosActiveEpisode> mosActiveEpisodes = _iMosActiveEpisodeRepository.GetMosActiveEpisodeByEpisodeId(mosActiveEpisode.EpisodeId);
            DateTime dtNow = DateTime.UtcNow;
            if (mosActiveEpisodes != null && mosActiveEpisodes.Count > 0)
            {                
                mosActiveEpisode.LastUpdateDate = dtNow;
                mosActiveEpisode = mosActiveEpisodes[0];
                mosActiveEpisode.StatusCode = 2;
                mosActiveEpisodeNew = _iMosActiveEpisodeRepository.UpdateMosActiveEpisode(mosActiveEpisode);
            }
            else
            {
                mosActiveEpisode.CreationDate = dtNow;
                mosActiveEpisode.LastUpdateDate = dtNow;
                mosActiveEpisodeNew = _iMosActiveEpisodeRepository.InsertMosActiveEpisode(mosActiveEpisode);
            }
            return mosActiveEpisodeNew;
        }

        public virtual MosActiveEpisode InsertIfNotExist(MosActiveEpisode mosActiveEpisode)
        {
            MosActiveEpisode mosActiveEpisodeNew = new MosActiveEpisode();
            List<MosActiveEpisode> mosActiveEpisodes = _iMosActiveEpisodeRepository.GetMosActiveEpisodeByEpisodeId(mosActiveEpisode.EpisodeId);
            DateTime dtNow = DateTime.UtcNow;
            if (mosActiveEpisodes != null && mosActiveEpisodes.Count > 0)
            {
                //mosActiveEpisode.LastUpdateDate = dtNow;
                //mosActiveEpisode = mosActiveEpisodes[0];
                //mosActiveEpisode.StatusCode = 2;
                //mosActiveEpisodeNew = _iMosActiveEpisodeRepository.UpdateMosActiveEpisode(mosActiveEpisode);
                return null;
            }
            else
            {
                mosActiveEpisode.CreationDate = dtNow;
                mosActiveEpisode.LastUpdateDate = dtNow;
                mosActiveEpisodeNew = _iMosActiveEpisodeRepository.InsertMosActiveEpisode(mosActiveEpisode);
            }
            return mosActiveEpisodeNew;
        }

        public virtual List<MosActiveEpisode> GetMosActiveEpisodeByStatus(System.Int32? StatusCode)
        {
            return _iMosActiveEpisodeRepository.GetMosActiveEpisodeByStatus(StatusCode);
        }

		public virtual List<MosActiveEpisode> GetMosActiveEpisodeByEpisodeId(System.Int32? EpisodeId)
		{
			return _iMosActiveEpisodeRepository.GetMosActiveEpisodeByEpisodeId(EpisodeId);
		}

		public MosActiveEpisode GetMosActiveEpisode(System.Int32 MosActiveEpisodeId)
		{
			return _iMosActiveEpisodeRepository.GetMosActiveEpisode(MosActiveEpisodeId);
		}

		public MosActiveEpisode UpdateMosActiveEpisode(MosActiveEpisode entity)
		{
			return _iMosActiveEpisodeRepository.UpdateMosActiveEpisode(entity);
		}

		public bool DeleteMosActiveEpisode(System.Int32 MosActiveEpisodeId)
		{
			return _iMosActiveEpisodeRepository.DeleteMosActiveEpisode(MosActiveEpisodeId);
		}

		public List<MosActiveEpisode> GetAllMosActiveEpisode()
		{
			return _iMosActiveEpisodeRepository.GetAllMosActiveEpisode();
		}

		public MosActiveEpisode InsertMosActiveEpisode(MosActiveEpisode entity)
		{
			 return _iMosActiveEpisodeRepository.InsertMosActiveEpisode(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mosactiveepisodeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mosactiveepisodeid))
            {
				MosActiveEpisode mosactiveepisode = _iMosActiveEpisodeRepository.GetMosActiveEpisode(mosactiveepisodeid);
                if(mosactiveepisode!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mosactiveepisode);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<MosActiveEpisode> mosactiveepisodelist = _iMosActiveEpisodeRepository.GetAllMosActiveEpisode();
            if (mosactiveepisodelist != null && mosactiveepisodelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mosactiveepisodelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                MosActiveEpisode mosactiveepisode = new MosActiveEpisode();
                PostOutput output = new PostOutput();
                mosactiveepisode.CopyFrom(Input);
                mosactiveepisode = _iMosActiveEpisodeRepository.InsertMosActiveEpisode(mosactiveepisode);
                output.CopyFrom(mosactiveepisode);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                MosActiveEpisode mosactiveepisodeinput = new MosActiveEpisode();
                MosActiveEpisode mosactiveepisodeoutput = new MosActiveEpisode();
                PutOutput output = new PutOutput();
                mosactiveepisodeinput.CopyFrom(Input);
                MosActiveEpisode mosactiveepisode = _iMosActiveEpisodeRepository.GetMosActiveEpisode(mosactiveepisodeinput.MosActiveEpisodeId);
                if (mosactiveepisode!=null)
                {
                    mosactiveepisodeoutput = _iMosActiveEpisodeRepository.UpdateMosActiveEpisode(mosactiveepisodeinput);
                    if(mosactiveepisodeoutput!=null)
                    {
                        output.CopyFrom(mosactiveepisodeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mosactiveepisodeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mosactiveepisodeid))
            {
				 bool IsDeleted = _iMosActiveEpisodeRepository.DeleteMosActiveEpisode(mosactiveepisodeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
