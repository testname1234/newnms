﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosActiveItem;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class MosActiveItemService : IMosActiveItemService 
	{
		private IMosActiveItemRepository _iMosActiveItemRepository;
        
		public MosActiveItemService(IMosActiveItemRepository iMosActiveItemRepository)
		{
			this._iMosActiveItemRepository = iMosActiveItemRepository;
		}
        
        public Dictionary<string, string> GetMosActiveItemBasicSearchColumns()
        {
            
            return this._iMosActiveItemRepository.GetMosActiveItemBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMosActiveItemAdvanceSearchColumns()
        {
            
            return this._iMosActiveItemRepository.GetMosActiveItemAdvanceSearchColumns();
           
        }
        

		public virtual List<MosActiveItem> GetMosActiveItemByEpisodeId(System.Int32? EpisodeId)
		{
			return _iMosActiveItemRepository.GetMosActiveItemByEpisodeId(EpisodeId);
		}

        public virtual List<MosActiveItem> GetMosActiveItemWithRunDownByEpisodeId(System.Int32? EpisodeId)
        {
            return _iMosActiveItemRepository.GetMosActiveItemWithRunDownByEpisodeId(EpisodeId);
        }

		public MosActiveItem GetMosActiveItem(System.Int32 MosActiveItemId)
		{
			return _iMosActiveItemRepository.GetMosActiveItem(MosActiveItemId);
		}

		public MosActiveItem UpdateMosActiveItem(MosActiveItem entity)
		{
			return _iMosActiveItemRepository.UpdateMosActiveItem(entity);
		}

		public bool DeleteMosActiveItem(System.Int32 MosActiveItemId)
		{
			return _iMosActiveItemRepository.DeleteMosActiveItem(MosActiveItemId);
		}

		public List<MosActiveItem> GetAllMosActiveItem()
		{
			return _iMosActiveItemRepository.GetAllMosActiveItem();
		}

		public MosActiveItem InsertMosActiveItem(MosActiveItem entity)
		{
			 return _iMosActiveItemRepository.InsertMosActiveItem(entity);
		}




        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mosactiveitemid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mosactiveitemid))
            {
				MosActiveItem mosactiveitem = _iMosActiveItemRepository.GetMosActiveItem(mosactiveitemid);
                if(mosactiveitem!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mosactiveitem);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<MosActiveItem> mosactiveitemlist = _iMosActiveItemRepository.GetAllMosActiveItem();
            if (mosactiveitemlist != null && mosactiveitemlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mosactiveitemlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                MosActiveItem mosactiveitem = new MosActiveItem();
                PostOutput output = new PostOutput();
                mosactiveitem.CopyFrom(Input);
                mosactiveitem = _iMosActiveItemRepository.InsertMosActiveItem(mosactiveitem);
                output.CopyFrom(mosactiveitem);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                MosActiveItem mosactiveiteminput = new MosActiveItem();
                MosActiveItem mosactiveitemoutput = new MosActiveItem();
                PutOutput output = new PutOutput();
                mosactiveiteminput.CopyFrom(Input);
                MosActiveItem mosactiveitem = _iMosActiveItemRepository.GetMosActiveItem(mosactiveiteminput.MosActiveItemId);
                if (mosactiveitem!=null)
                {
                    mosactiveitemoutput = _iMosActiveItemRepository.UpdateMosActiveItem(mosactiveiteminput);
                    if(mosactiveitemoutput!=null)
                    {
                        output.CopyFrom(mosactiveitemoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mosactiveitemid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mosactiveitemid))
            {
				 bool IsDeleted = _iMosActiveItemRepository.DeleteMosActiveItem(mosactiveitemid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
