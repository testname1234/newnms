﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosActiveTransformation;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class MosActiveTransformationService : IMosActiveTransformationService 
	{
		private IMosActiveTransformationRepository _iMosActiveTransformationRepository;
        
		public MosActiveTransformationService(IMosActiveTransformationRepository iMosActiveTransformationRepository)
		{
			this._iMosActiveTransformationRepository = iMosActiveTransformationRepository;
		}
        
        public Dictionary<string, string> GetMosActiveTransformationBasicSearchColumns()
        {
            
            return this._iMosActiveTransformationRepository.GetMosActiveTransformationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMosActiveTransformationAdvanceSearchColumns()
        {
            
            return this._iMosActiveTransformationRepository.GetMosActiveTransformationAdvanceSearchColumns();
           
        }
        

		public virtual List<MosActiveTransformation> GetMosActiveTransformationByMosActiveItemId(System.Int32? MosActiveItemId)
		{
			return _iMosActiveTransformationRepository.GetMosActiveTransformationByMosActiveItemId(MosActiveItemId);
		}

		public MosActiveTransformation GetMosActiveTransformation(System.Int32 MosActiveDetailId)
		{
			return _iMosActiveTransformationRepository.GetMosActiveTransformation(MosActiveDetailId);
		}

		public MosActiveTransformation UpdateMosActiveTransformation(MosActiveTransformation entity)
		{
			return _iMosActiveTransformationRepository.UpdateMosActiveTransformation(entity);
		}

		public bool DeleteMosActiveTransformation(System.Int32 MosActiveDetailId)
		{
			return _iMosActiveTransformationRepository.DeleteMosActiveTransformation(MosActiveDetailId);
		}

		public List<MosActiveTransformation> GetAllMosActiveTransformation()
		{
			return _iMosActiveTransformationRepository.GetAllMosActiveTransformation();
		}

		public MosActiveTransformation InsertMosActiveTransformation(MosActiveTransformation entity)
		{
			 return _iMosActiveTransformationRepository.InsertMosActiveTransformation(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 mosactivedetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mosactivedetailid))
            {
				MosActiveTransformation mosactivetransformation = _iMosActiveTransformationRepository.GetMosActiveTransformation(mosactivedetailid);
                if(mosactivetransformation!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mosactivetransformation);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<MosActiveTransformation> mosactivetransformationlist = _iMosActiveTransformationRepository.GetAllMosActiveTransformation();
            if (mosactivetransformationlist != null && mosactivetransformationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mosactivetransformationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                MosActiveTransformation mosactivetransformation = new MosActiveTransformation();
                PostOutput output = new PostOutput();
                mosactivetransformation.CopyFrom(Input);
                mosactivetransformation = _iMosActiveTransformationRepository.InsertMosActiveTransformation(mosactivetransformation);
                output.CopyFrom(mosactivetransformation);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                MosActiveTransformation mosactivetransformationinput = new MosActiveTransformation();
                MosActiveTransformation mosactivetransformationoutput = new MosActiveTransformation();
                PutOutput output = new PutOutput();
                mosactivetransformationinput.CopyFrom(Input);
                MosActiveTransformation mosactivetransformation = _iMosActiveTransformationRepository.GetMosActiveTransformation(mosactivetransformationinput.MosActiveDetailId);
                if (mosactivetransformation!=null)
                {
                    mosactivetransformationoutput = _iMosActiveTransformationRepository.UpdateMosActiveTransformation(mosactivetransformationinput);
                    if(mosactivetransformationoutput!=null)
                    {
                        output.CopyFrom(mosactivetransformationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 mosactivedetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out mosactivedetailid))
            {
				 bool IsDeleted = _iMosActiveTransformationRepository.DeleteMosActiveTransformation(mosactivedetailid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
