﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.MosItem;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class MosItemService : IMosItemService 
	{
		private IMosItemRepository _iMosItemRepository;
        
		public MosItemService(IMosItemRepository iMosItemRepository)
		{
			this._iMosItemRepository = iMosItemRepository;
		}
        
        public Dictionary<string, string> GetMosItemBasicSearchColumns()
        {
            
            return this._iMosItemRepository.GetMosItemBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetMosItemAdvanceSearchColumns()
        {
            
            return this._iMosItemRepository.GetMosItemAdvanceSearchColumns();
           
        }
        

		public MosItem GetMosItem(System.Int32 CasperMosItemId)
		{
			return _iMosItemRepository.GetMosItem(CasperMosItemId);
		}

		public MosItem UpdateMosItem(MosItem entity)
		{
			return _iMosItemRepository.UpdateMosItem(entity);
		}

		public bool DeleteMosItem(System.Int32 CasperMosItemId)
		{
			return _iMosItemRepository.DeleteMosItem(CasperMosItemId);
		}

		public List<MosItem> GetAllMosItem()
		{
			return _iMosItemRepository.GetAllMosItem();
		}

		public MosItem InsertMosItem(MosItem entity)
		{
			 return _iMosItemRepository.InsertMosItem(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 caspermositemid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspermositemid))
            {
				MosItem mositem = _iMosItemRepository.GetMosItem(caspermositemid);
                if(mositem!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(mositem);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<MosItem> mositemlist = _iMosItemRepository.GetAllMosItem();
            if (mositemlist != null && mositemlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(mositemlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                MosItem mositem = new MosItem();
                PostOutput output = new PostOutput();
                mositem.CopyFrom(Input);
                mositem = _iMosItemRepository.InsertMosItem(mositem);
                output.CopyFrom(mositem);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                MosItem mositeminput = new MosItem();
                MosItem mositemoutput = new MosItem();
                PutOutput output = new PutOutput();
                mositeminput.CopyFrom(Input);
                MosItem mositem = _iMosItemRepository.GetMosItem(mositeminput.CasperMosItemId);
                if (mositem!=null)
                {
                    mositemoutput = _iMosItemRepository.UpdateMosItem(mositeminput);
                    if(mositemoutput!=null)
                    {
                        output.CopyFrom(mositemoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 caspermositemid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out caspermositemid))
            {
				 bool IsDeleted = _iMosItemRepository.DeleteMosItem(caspermositemid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
