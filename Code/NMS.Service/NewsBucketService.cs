﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsBucket;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class NewsBucketService : INewsBucketService 
	{
		private INewsBucketRepository _iNewsBucketRepository;
        
		public NewsBucketService(INewsBucketRepository iNewsBucketRepository)
		{
			this._iNewsBucketRepository = iNewsBucketRepository;
		}
        
        public Dictionary<string, string> GetNewsBucketBasicSearchColumns()
        {
            
            return this._iNewsBucketRepository.GetNewsBucketBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsBucketAdvanceSearchColumns()
        {
            
            return this._iNewsBucketRepository.GetNewsBucketAdvanceSearchColumns();
           
        }
        

		public NewsBucket GetNewsBucket(System.Int32 NewsBucketId)
		{
			return _iNewsBucketRepository.GetNewsBucket(NewsBucketId);
		}

		public NewsBucket UpdateNewsBucket(NewsBucket entity)
		{
			return _iNewsBucketRepository.UpdateNewsBucket(entity);
		}

		public bool DeleteNewsBucket(System.Int32 NewsBucketId)
		{
			return _iNewsBucketRepository.DeleteNewsBucket(NewsBucketId);
		}

		public List<NewsBucket> GetAllNewsBucket()
		{
			return _iNewsBucketRepository.GetAllNewsBucket();
		}

		public NewsBucket InsertNewsBucket(NewsBucket entity)
		{
			 return _iNewsBucketRepository.InsertNewsBucket(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsbucketid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsbucketid))
            {
				NewsBucket newsbucket = _iNewsBucketRepository.GetNewsBucket(newsbucketid);
                if(newsbucket!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsbucket);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsBucket> newsbucketlist = _iNewsBucketRepository.GetAllNewsBucket();
            if (newsbucketlist != null && newsbucketlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsbucketlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsBucket newsbucket = new NewsBucket();
                PostOutput output = new PostOutput();
                newsbucket.CopyFrom(Input);
                newsbucket = _iNewsBucketRepository.InsertNewsBucket(newsbucket);
                output.CopyFrom(newsbucket);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsBucket newsbucketinput = new NewsBucket();
                NewsBucket newsbucketoutput = new NewsBucket();
                PutOutput output = new PutOutput();
                newsbucketinput.CopyFrom(Input);
                NewsBucket newsbucket = _iNewsBucketRepository.GetNewsBucket(newsbucketinput.NewsBucketId);
                if (newsbucket!=null)
                {
                    newsbucketoutput = _iNewsBucketRepository.UpdateNewsBucket(newsbucketinput);
                    if(newsbucketoutput!=null)
                    {
                        output.CopyFrom(newsbucketoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsbucketid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsbucketid))
            {
				 bool IsDeleted = _iNewsBucketRepository.DeleteNewsBucket(newsbucketid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<NewsBucket> GetNewsBucketByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
         {
             return _iNewsBucketRepository.GetNewsBucketByKeyValue(Key, Value, Operands.Equal,SelectClause);
         }
         public List<NewsBucket> GetNewsBucketByProgramIds(string CommaSepratedeIds) {

             return _iNewsBucketRepository.GetNewsBucketByProgramIds(CommaSepratedeIds);
         }


         public NewsBucket UpdateSequence(NewsBucket item)
         {
             return _iNewsBucketRepository.UpdateSequence(item);
         }
    }
	
	
}
