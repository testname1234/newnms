﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsCameraMan;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsCameraManService : INewsCameraManService 
	{
		private INewsCameraManRepository _iNewsCameraManRepository;
        
		public NewsCameraManService(INewsCameraManRepository iNewsCameraManRepository)
		{
			this._iNewsCameraManRepository = iNewsCameraManRepository;
		}
        
        public Dictionary<string, string> GetNewsCameraManBasicSearchColumns()
        {
            
            return this._iNewsCameraManRepository.GetNewsCameraManBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsCameraManAdvanceSearchColumns()
        {
            
            return this._iNewsCameraManRepository.GetNewsCameraManAdvanceSearchColumns();
           
        }
        

		public NewsCameraMan GetNewsCameraMan(System.Int32 NewsCameraManId)
		{
			return _iNewsCameraManRepository.GetNewsCameraMan(NewsCameraManId);
		}

		public NewsCameraMan UpdateNewsCameraMan(NewsCameraMan entity)
		{
			return _iNewsCameraManRepository.UpdateNewsCameraMan(entity);
		}

		public bool DeleteNewsCameraMan(System.Int32 NewsCameraManId)
		{
			return _iNewsCameraManRepository.DeleteNewsCameraMan(NewsCameraManId);
		}

		public List<NewsCameraMan> GetAllNewsCameraMan()
		{
			return _iNewsCameraManRepository.GetAllNewsCameraMan();
		}

		public NewsCameraMan InsertNewsCameraMan(NewsCameraMan entity)
		{
			 return _iNewsCameraManRepository.InsertNewsCameraMan(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newscameramanid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newscameramanid))
            {
				NewsCameraMan newscameraman = _iNewsCameraManRepository.GetNewsCameraMan(newscameramanid);
                if(newscameraman!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newscameraman);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsCameraMan> newscameramanlist = _iNewsCameraManRepository.GetAllNewsCameraMan();
            if (newscameramanlist != null && newscameramanlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newscameramanlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsCameraMan newscameraman = new NewsCameraMan();
                PostOutput output = new PostOutput();
                newscameraman.CopyFrom(Input);
                newscameraman = _iNewsCameraManRepository.InsertNewsCameraMan(newscameraman);
                output.CopyFrom(newscameraman);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsCameraMan newscameramaninput = new NewsCameraMan();
                NewsCameraMan newscameramanoutput = new NewsCameraMan();
                PutOutput output = new PutOutput();
                newscameramaninput.CopyFrom(Input);
                NewsCameraMan newscameraman = _iNewsCameraManRepository.GetNewsCameraMan(newscameramaninput.NewsCameraManId);
                if (newscameraman!=null)
                {
                    newscameramanoutput = _iNewsCameraManRepository.UpdateNewsCameraMan(newscameramaninput);
                    if(newscameramanoutput!=null)
                    {
                        output.CopyFrom(newscameramanoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newscameramanid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newscameramanid))
            {
				 bool IsDeleted = _iNewsCameraManRepository.DeleteNewsCameraMan(newscameramanid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
