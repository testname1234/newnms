﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsCategory;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsCategoryService : INewsCategoryService 
	{
		private INewsCategoryRepository _iNewsCategoryRepository;
        
		public NewsCategoryService(INewsCategoryRepository iNewsCategoryRepository)
		{
			this._iNewsCategoryRepository = iNewsCategoryRepository;
		}
        
        public Dictionary<string, string> GetNewsCategoryBasicSearchColumns()
        {
            
            return this._iNewsCategoryRepository.GetNewsCategoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsCategoryAdvanceSearchColumns()
        {
            
            return this._iNewsCategoryRepository.GetNewsCategoryAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsCategory> GetNewsCategoryByNewsId(System.Int32 NewsId)
		{
			return _iNewsCategoryRepository.GetNewsCategoryByNewsId(NewsId);
		}

		public virtual List<NewsCategory> GetNewsCategoryByCategoryId(System.Int32 CategoryId)
		{
			return _iNewsCategoryRepository.GetNewsCategoryByCategoryId(CategoryId);
		}

		public NewsCategory GetNewsCategory(System.Int32 NewsCategoryId)
		{
			return _iNewsCategoryRepository.GetNewsCategory(NewsCategoryId);
		}

		public NewsCategory UpdateNewsCategory(NewsCategory entity)
		{
			return _iNewsCategoryRepository.UpdateNewsCategory(entity);
		}

		public bool DeleteNewsCategory(System.Int32 NewsCategoryId)
		{
			return _iNewsCategoryRepository.DeleteNewsCategory(NewsCategoryId);
		}

        public bool DeleteNewsCategoryByNewsId(System.Int32 NewsId)
        {
            return _iNewsCategoryRepository.DeleteNewsCategoryByNewsId(NewsId);
        }

		public List<NewsCategory> GetAllNewsCategory()
		{
			return _iNewsCategoryRepository.GetAllNewsCategory();
		}

		public NewsCategory InsertNewsCategory(NewsCategory entity)
		{
			 return _iNewsCategoryRepository.InsertNewsCategory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newscategoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newscategoryid))
            {
				NewsCategory newscategory = _iNewsCategoryRepository.GetNewsCategory(newscategoryid);
                if(newscategory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newscategory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsCategory> newscategorylist = _iNewsCategoryRepository.GetAllNewsCategory();
            if (newscategorylist != null && newscategorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newscategorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsCategory newscategory = new NewsCategory();
                PostOutput output = new PostOutput();
                newscategory.CopyFrom(Input);
                newscategory = _iNewsCategoryRepository.InsertNewsCategory(newscategory);
                output.CopyFrom(newscategory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsCategory newscategoryinput = new NewsCategory();
                NewsCategory newscategoryoutput = new NewsCategory();
                PutOutput output = new PutOutput();
                newscategoryinput.CopyFrom(Input);
                NewsCategory newscategory = _iNewsCategoryRepository.GetNewsCategory(newscategoryinput.NewsCategoryId);
                if (newscategory!=null)
                {
                    newscategoryoutput = _iNewsCategoryRepository.UpdateNewsCategory(newscategoryinput);
                    if(newscategoryoutput!=null)
                    {
                        output.CopyFrom(newscategoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newscategoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newscategoryid))
            {
				 bool IsDeleted = _iNewsCategoryRepository.DeleteNewsCategory(newscategoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
