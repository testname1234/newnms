﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFileFilter;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsFileFilterService : INewsFileFilterService 
	{
		private INewsFileFilterRepository _iNewsFileFilterRepository;
        
		public NewsFileFilterService(INewsFileFilterRepository iNewsFileFilterRepository)
		{
			this._iNewsFileFilterRepository = iNewsFileFilterRepository;
		}
        
        public Dictionary<string, string> GetNewsFileFilterBasicSearchColumns()
        {
            
            return this._iNewsFileFilterRepository.GetNewsFileFilterBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsFileFilterAdvanceSearchColumns()
        {
            
            return this._iNewsFileFilterRepository.GetNewsFileFilterAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsFileFilter> GetNewsFileFilterByFilterId(System.Int32? FilterId)
		{
			return _iNewsFileFilterRepository.GetNewsFileFilterByFilterId(FilterId);
		}

		public virtual List<NewsFileFilter> GetNewsFileFilterByNewsFileId(System.Int32? NewsFileId)
		{
			return _iNewsFileFilterRepository.GetNewsFileFilterByNewsFileId(NewsFileId);
		}

		public NewsFileFilter GetNewsFileFilter(System.Int32 NewsFileFilterId)
		{
			return _iNewsFileFilterRepository.GetNewsFileFilter(NewsFileFilterId);
		}

		public NewsFileFilter UpdateNewsFileFilter(NewsFileFilter entity)
		{
			return _iNewsFileFilterRepository.UpdateNewsFileFilter(entity);
		}

		public bool DeleteNewsFileFilter(System.Int32 NewsFileFilterId)
		{
			return _iNewsFileFilterRepository.DeleteNewsFileFilter(NewsFileFilterId);
		}

		public List<NewsFileFilter> GetAllNewsFileFilter()
		{
			return _iNewsFileFilterRepository.GetAllNewsFileFilter();
		}

		public NewsFileFilter InsertNewsFileFilter(NewsFileFilter entity)
		{
			 return _iNewsFileFilterRepository.InsertNewsFileFilter(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsfilefilterid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfilefilterid))
            {
				NewsFileFilter newsfilefilter = _iNewsFileFilterRepository.GetNewsFileFilter(newsfilefilterid);
                if(newsfilefilter!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsfilefilter);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsFileFilter> newsfilefilterlist = _iNewsFileFilterRepository.GetAllNewsFileFilter();
            if (newsfilefilterlist != null && newsfilefilterlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsfilefilterlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsFileFilter newsfilefilter = new NewsFileFilter();
                PostOutput output = new PostOutput();
                newsfilefilter.CopyFrom(Input);
                newsfilefilter = _iNewsFileFilterRepository.InsertNewsFileFilter(newsfilefilter);
                output.CopyFrom(newsfilefilter);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsFileFilter newsfilefilterinput = new NewsFileFilter();
                NewsFileFilter newsfilefilteroutput = new NewsFileFilter();
                PutOutput output = new PutOutput();
                newsfilefilterinput.CopyFrom(Input);
                NewsFileFilter newsfilefilter = _iNewsFileFilterRepository.GetNewsFileFilter(newsfilefilterinput.NewsFileFilterId);
                if (newsfilefilter!=null)
                {
                    newsfilefilteroutput = _iNewsFileFilterRepository.UpdateNewsFileFilter(newsfilefilterinput);
                    if(newsfilefilteroutput!=null)
                    {
                        output.CopyFrom(newsfilefilteroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsfilefilterid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfilefilterid))
            {
				 bool IsDeleted = _iNewsFileFilterRepository.DeleteNewsFileFilter(newsfilefilterid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
