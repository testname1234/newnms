﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFileHistory;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsFileHistoryService : INewsFileHistoryService 
	{
		private INewsFileHistoryRepository _iNewsFileHistoryRepository;
        
		public NewsFileHistoryService(INewsFileHistoryRepository iNewsFileHistoryRepository)
		{
			this._iNewsFileHistoryRepository = iNewsFileHistoryRepository;
		}
        
        public Dictionary<string, string> GetNewsFileHistoryBasicSearchColumns()
        {
            
            return this._iNewsFileHistoryRepository.GetNewsFileHistoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsFileHistoryAdvanceSearchColumns()
        {
            
            return this._iNewsFileHistoryRepository.GetNewsFileHistoryAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsFileHistory> GetNewsFileHistoryByNewsFileId(System.Int32 NewsFileId)
		{
			return _iNewsFileHistoryRepository.GetNewsFileHistoryByNewsFileId(NewsFileId);
		}

		public NewsFileHistory GetNewsFileHistory(System.Int32 NewsFileHistoryId)
		{
			return _iNewsFileHistoryRepository.GetNewsFileHistory(NewsFileHistoryId);
		}

		public NewsFileHistory UpdateNewsFileHistory(NewsFileHistory entity)
		{
			return _iNewsFileHistoryRepository.UpdateNewsFileHistory(entity);
		}

		public bool DeleteNewsFileHistory(System.Int32 NewsFileHistoryId)
		{
			return _iNewsFileHistoryRepository.DeleteNewsFileHistory(NewsFileHistoryId);
		}

		public List<NewsFileHistory> GetAllNewsFileHistory()
		{
			return _iNewsFileHistoryRepository.GetAllNewsFileHistory();
		}

		public NewsFileHistory InsertNewsFileHistory(NewsFileHistory entity)
		{
			 return _iNewsFileHistoryRepository.InsertNewsFileHistory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsfilehistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfilehistoryid))
            {
				NewsFileHistory newsfilehistory = _iNewsFileHistoryRepository.GetNewsFileHistory(newsfilehistoryid);
                if(newsfilehistory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsfilehistory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsFileHistory> newsfilehistorylist = _iNewsFileHistoryRepository.GetAllNewsFileHistory();
            if (newsfilehistorylist != null && newsfilehistorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsfilehistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsFileHistory newsfilehistory = new NewsFileHistory();
                PostOutput output = new PostOutput();
                newsfilehistory.CopyFrom(Input);
                newsfilehistory = _iNewsFileHistoryRepository.InsertNewsFileHistory(newsfilehistory);
                output.CopyFrom(newsfilehistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsFileHistory newsfilehistoryinput = new NewsFileHistory();
                NewsFileHistory newsfilehistoryoutput = new NewsFileHistory();
                PutOutput output = new PutOutput();
                newsfilehistoryinput.CopyFrom(Input);
                NewsFileHistory newsfilehistory = _iNewsFileHistoryRepository.GetNewsFileHistory(newsfilehistoryinput.NewsFileHistoryId);
                if (newsfilehistory!=null)
                {
                    newsfilehistoryoutput = _iNewsFileHistoryRepository.UpdateNewsFileHistory(newsfilehistoryinput);
                    if(newsfilehistoryoutput!=null)
                    {
                        output.CopyFrom(newsfilehistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsfilehistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfilehistoryid))
            {
				 bool IsDeleted = _iNewsFileHistoryRepository.DeleteNewsFileHistory(newsfilehistoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
