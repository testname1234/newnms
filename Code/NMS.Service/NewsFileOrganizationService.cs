﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFileOrganization;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsFileOrganizationService : INewsFileOrganizationService 
	{
		private INewsFileOrganizationRepository _iNewsFileOrganizationRepository;
        
		public NewsFileOrganizationService(INewsFileOrganizationRepository iNewsFileOrganizationRepository)
		{
			this._iNewsFileOrganizationRepository = iNewsFileOrganizationRepository;
		}
        
        public Dictionary<string, string> GetNewsFileOrganizationBasicSearchColumns()
        {
            
            return this._iNewsFileOrganizationRepository.GetNewsFileOrganizationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsFileOrganizationAdvanceSearchColumns()
        {
            
            return this._iNewsFileOrganizationRepository.GetNewsFileOrganizationAdvanceSearchColumns();
           
        }
        

		public NewsFileOrganization GetNewsFileOrganization(System.Int32 NewsFileOrganizationId)
		{
			return _iNewsFileOrganizationRepository.GetNewsFileOrganization(NewsFileOrganizationId);
		}

		public NewsFileOrganization UpdateNewsFileOrganization(NewsFileOrganization entity)
		{
			return _iNewsFileOrganizationRepository.UpdateNewsFileOrganization(entity);
		}

		public bool DeleteNewsFileOrganization(System.Int32 NewsFileOrganizationId)
		{
			return _iNewsFileOrganizationRepository.DeleteNewsFileOrganization(NewsFileOrganizationId);
		}

		public List<NewsFileOrganization> GetAllNewsFileOrganization()
		{
			return _iNewsFileOrganizationRepository.GetAllNewsFileOrganization();
		}

		public NewsFileOrganization InsertNewsFileOrganization(NewsFileOrganization entity)
		{
			 return _iNewsFileOrganizationRepository.InsertNewsFileOrganization(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsfileorganizationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfileorganizationid))
            {
				NewsFileOrganization newsfileorganization = _iNewsFileOrganizationRepository.GetNewsFileOrganization(newsfileorganizationid);
                if(newsfileorganization!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsfileorganization);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsFileOrganization> newsfileorganizationlist = _iNewsFileOrganizationRepository.GetAllNewsFileOrganization();
            if (newsfileorganizationlist != null && newsfileorganizationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsfileorganizationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsFileOrganization newsfileorganization = new NewsFileOrganization();
                PostOutput output = new PostOutput();
                newsfileorganization.CopyFrom(Input);
                newsfileorganization = _iNewsFileOrganizationRepository.InsertNewsFileOrganization(newsfileorganization);
                output.CopyFrom(newsfileorganization);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsFileOrganization newsfileorganizationinput = new NewsFileOrganization();
                NewsFileOrganization newsfileorganizationoutput = new NewsFileOrganization();
                PutOutput output = new PutOutput();
                newsfileorganizationinput.CopyFrom(Input);
                NewsFileOrganization newsfileorganization = _iNewsFileOrganizationRepository.GetNewsFileOrganization(newsfileorganizationinput.NewsFileOrganizationId);
                if (newsfileorganization!=null)
                {
                    newsfileorganizationoutput = _iNewsFileOrganizationRepository.UpdateNewsFileOrganization(newsfileorganizationinput);
                    if(newsfileorganizationoutput!=null)
                    {
                        output.CopyFrom(newsfileorganizationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsfileorganizationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfileorganizationid))
            {
				 bool IsDeleted = _iNewsFileOrganizationRepository.DeleteNewsFileOrganization(newsfileorganizationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
