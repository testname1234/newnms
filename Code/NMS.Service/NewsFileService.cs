﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFile;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Models;
using NMS.Core.Enums;
using System.Threading;
using NMS.Repository;
using System.Text.RegularExpressions;
using NMS.TextAnalysis.SemanticLibrary;
using NMS.Core.Entities.Mongo;
using System.Text;
using NMS.Core.Helper;
using System.Net;
using System.Web.Script.Serialization;

namespace NMS.Service
{

    public class NewsFileService : INewsFileService
    {   
        private INewsFileRepository _iNewsFileRepository;
        private IFileStatusHistoryRepository _iFileStatusHistoryRepository;
        private IFileFolderHistoryRepository _iFileFolderHistoryRepository;
        private IFileDetailRepository _iFileDetailRepository;
        private IFileResourceRepository _iFileResourceRepository;
        private IProgramRepository _iProgramRepository;
        private IFileLocationRepository _iFileLocationRepository;
        private IFileCategoryRepository _iFileCategoryRepository;
        private IFileTagRepository _iFileTagRepository;
        private ITagRepository _iTagRepository;
        private IFilterRepository _iFilterRepository;
        private INewsFileFilterRepository _iNewsFileFilterRepository;
        private IChannelRepository _iChannelRepository;
        private IRadioStationRepository _iRadioStationRepository;
        private INewsPaperRepository _iNewsPaperRepository;
        private ILocationRepository _iLocationRepository;
        private ICategoryRepository _iCategoryRepository;
        private IFolderRepository _iFolderRepository;
        private IOrganizationRepository _iOrganizationRepository;
        private INewsFileOrganizationRepository _iNewsFileOrganizationRepository;
        private IEditorialCommentRepository _iEditorialCommentRepository;
        private IEventReporterRepository _iEventReporterRepository;
        private IEventResourceRepository _iEventResourceRepository;
        private INewsFileHistoryRepository _iNewsFileHistoryRepository;
        private IEventTypeRepository _iEventTypeRepository;




        public NewsFileService(INewsFileRepository iNewsFileRepository, IFileStatusHistoryRepository iFileStatusHistoryRepository, IFileFolderHistoryRepository iFileFolderHistoryRepository, IFileDetailRepository iFileDetailRepository, IFileResourceRepository iFileResourceRepository, IProgramRepository iProgramRepository, IFileLocationRepository iFileLocationRepository, IFileCategoryRepository iFileCategoryRepository, IFileTagRepository iFileTagRepository, ITagRepository iTagRepository, IFilterRepository iFilterRepository, INewsFileFilterRepository iNewsFileFilterRepository, IChannelRepository iChannelRepository, IRadioStationRepository iRadioStationRepository, INewsPaperRepository iNewsPaperRepository, ILocationRepository iLocationRepository, ICategoryRepository iCategoryRepository, IFolderRepository iFolderRepository, IOrganizationRepository iOrganizationRepository, INewsFileOrganizationRepository iNewsFileOrganizationRepository, IEditorialCommentRepository iEditorialCommentRepository, IEventReporterRepository iEventReporterRepository, IEventResourceRepository iEventResourceRepository, INewsFileHistoryRepository iNewsFileHistoryRepository, IEventTypeRepository iEventTypeRepository)
        {
            this._iNewsFileRepository = iNewsFileRepository;
            this._iFileStatusHistoryRepository = iFileStatusHistoryRepository;
            this._iFileFolderHistoryRepository = iFileFolderHistoryRepository;
            this._iFileDetailRepository = iFileDetailRepository;
            this._iFileResourceRepository = iFileResourceRepository;
            this._iProgramRepository = iProgramRepository;
            this._iFileLocationRepository = iFileLocationRepository;
            this._iFileCategoryRepository = iFileCategoryRepository;
            this._iFileTagRepository = iFileTagRepository;
            this._iTagRepository = iTagRepository;
            this._iFilterRepository = iFilterRepository;
            this._iNewsFileFilterRepository = iNewsFileFilterRepository;
            this._iChannelRepository = iChannelRepository;
            this._iRadioStationRepository = iRadioStationRepository;
            this._iNewsPaperRepository = iNewsPaperRepository;
            this._iLocationRepository = iLocationRepository;
            this._iCategoryRepository = iCategoryRepository;
            this._iFolderRepository = iFolderRepository;
            this._iOrganizationRepository = iOrganizationRepository;
            this._iNewsFileOrganizationRepository = iNewsFileOrganizationRepository;
            this._iEditorialCommentRepository = iEditorialCommentRepository;
            this._iEventReporterRepository = iEventReporterRepository;
            this._iEventResourceRepository = iEventResourceRepository;
            this._iNewsFileHistoryRepository = iNewsFileHistoryRepository;
            this._iEventTypeRepository = iEventTypeRepository;
        }

        public Dictionary<string, string> GetNewsFileBasicSearchColumns()
        {

            return this._iNewsFileRepository.GetNewsFileBasicSearchColumns();

        }

        public List<SearchColumn> GetNewsFileAdvanceSearchColumns()
        {
            return this._iNewsFileRepository.GetNewsFileAdvanceSearchColumns();
        }

        public virtual List<NewsFile> GetNewsFileByFolderId(System.Int32 FolderId)
        {
            return _iNewsFileRepository.GetNewsFileByFolderId(FolderId);
        }

        public NewsFile GetNewsFile(System.Int32 NewsFileId)
        {
            return _iNewsFileRepository.GetNewsFile(NewsFileId);
        }
        public int GetMaxSequenceNumByFolderId(System.Int32 FolderId)
        {
            return _iNewsFileRepository.GetMaxSequenceNumByFolderId(FolderId);
        }

        public bool UpdateIsLiveBitNewsFile(int programid)
        {
            return _iNewsFileRepository.UpdateIsLiveBitNewsFile(programid);
        }
        public NewsFile UpdateNewsFile(NewsFile entity)
        {
            return _iNewsFileRepository.UpdateNewsFile(entity);
        }

        public bool DeleteNewsFile(System.Int32 NewsFileId)
        {
            return _iNewsFileRepository.DeleteNewsFile(NewsFileId);
        }

        public List<NewsFile> GetAllNewsFile()
        {
            return _iNewsFileRepository.GetAllNewsFile();
        }

        public NewsFile InsertNewsFile(NewsFile entity)
        {
            return _iNewsFileRepository.InsertNewsFile(entity);
        }

        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput UpdateNewsFileStatus(PostInput input)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput result = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            NewsFile res = new NewsFile();
            FileStatusHistory fileStatusHist = new FileStatusHistory();
            int status = Convert.ToInt32(input.StatusId);
            res = GetNewsFile(Convert.ToInt32(input.NewsFileId));
            if (res != null)
            {
                res.StatusId = status;
                res.LastUpdateDate = DateTime.UtcNow;
                var done = UpdateNewsFileStatusAndLastUpdate(res);

                fileStatusHist.NewsFileId = Convert.ToInt32(input.NewsFileId);
                fileStatusHist.StatusId = status;
                fileStatusHist.CreationDate = res.LastUpdateDate;
                fileStatusHist.UserId = Convert.ToInt32(input.CreatedBy);

               _iFileStatusHistoryRepository.InsertFileStatusHistory(fileStatusHist);
            }
            result.NewsFile = new List<Core.DataTransfer.NewsFile.GetOutput>();
            Core.DataTransfer.NewsFile.GetOutput newFile = new Core.DataTransfer.NewsFile.GetOutput();
            newFile.CopyFrom(res);
            result.NewsFile.Add(newFile);

            result.FileStatusHistory = new List<Core.DataTransfer.FileStatusHistory.GetOutput>();
            Core.DataTransfer.FileStatusHistory.GetOutput fileHist = new Core.DataTransfer.FileStatusHistory.GetOutput();
            fileHist.CopyFrom(fileStatusHist);
            result.FileStatusHistory.Add(fileHist);

            return result;
        }


        public List<NewsFile> GetNewsFileForTaggReport(DateTime? lstUpdate = null)
        {
            List<NewsFile> files = new List<NewsFile>();
            if (lstUpdate != null)
            {
                files = _iNewsFileRepository.TaggReportPolling(lstUpdate.Value);
            }
            else
            {
                files = _iNewsFileRepository.GetNewsFileForTaggReport();
            }

            int Thishour = DateTime.Now.Hour;
            DateTime dt = DateTime.Now;
            if (files != null)
            {   
                for (int i = 0; i <= files.Count - 1; i++)
                {
                    if (files[i].ChildNewsStatus == false)
                    {

                        dt = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0);
                        TimeSpan t = TimeSpan.FromSeconds(files[i].ProgramInterval);

                        dt = dt.Add(t);

                        if (dt < DateTime.Now) { dt = dt.AddHours(1); }

                        files[i].ProgramTime = dt;

                        if (files[i].lstOrganization == null)
                            files[i].lstOrganization = new List<Organization>();

                        if (files[i].lstTag == null)
                            files[i].lstTag = new List<Tag>();

                        List<NewsFileOrganization> lstnewsfileorg = _iNewsFileOrganizationRepository.GetNewsFileOrganizationByNewsFileId(files[i].ParentNewsId);
                        List<FileTag> lstfiletag = _iFileTagRepository.GetFileTagByNewsFileId(files[i].ParentNewsId);

                        if (lstnewsfileorg != null)
                        {
                            for (int j = 0; j <= lstnewsfileorg.Count - 1; j++)
                            {
                                files[i].lstOrganization.Add(_iOrganizationRepository.GetOrganization(lstnewsfileorg[j].OrganizationId));
                            }
                        }
                        if (lstfiletag != null)
                        {
                            for (int k = 0; k <= lstfiletag.Count - 1; k++)
                            {
                                files[i].lstTag.Add(_iTagRepository.GetTag(lstfiletag[k].TagId));
                            }
                        }

                    }
                }
                files = files.OrderBy(x => x.ProgramTime).ToList();
            }
            return files;
        }

        public List<NewsFile> GetBroadcastedNewsFiles(DateTime from, DateTime to)
        {    
            List<NewsFile> files = _iNewsFileRepository.GetBroadcastedNewsFiles(from, to);
            return files;
        }

        public List<NewsFile> GetBroadcatedNewsFileDetail(int Id)
        {
            List<NewsFile> files = _iNewsFileRepository.GetBroadcatedNewsFileDetail(Id);
            return files;
        }


        public bool MarkNewsFilestatus(PostInput input)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput result = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            int status = Convert.ToInt32(input.StatusId);
            int Id = Convert.ToInt32(input.NewsFileId);
            bool fileStatus = false;

            if (input.NewsFileId != null || input.NewsFileId != "0")
            {
                fileStatus = _iNewsFileRepository.UpdateNewsFileStatusById(Id, status, DateTime.UtcNow);
            }

            return fileStatus;
        }
        public bool MarkNewsVerificationStatus(PostInput input)
        {
            bool status = Convert.ToBoolean(input.IsVerified);
            int Id = Convert.ToInt32(input.NewsFileId);
            if (input.NewsFileId != null || input.NewsFileId != "0")
            {
                status = _iNewsFileRepository.MarkNewsVerificationStatus(Id, status, DateTime.UtcNow);
            }
            return status;
        }

        private bool UpdateNewsFileStatusAndLastUpdate(NewsFile res)
        {
            return _iNewsFileRepository.UpdateNewsFileStatusAndLastUpdate(res.NewsFileId, res.StatusId, res.LastUpdateDate);
        }

        public bool MarkTitleVoiceOverStatus(NMS.Core.DataTransfer.FileDetail.PostOutput res)
        {
            return _iNewsFileRepository.MarkTitleVoiceOverStatus(res);
        }
        public List<FileDetail> GetApprovedNewsfile(int[] programIds)
        {
            return _iFileDetailRepository.GetApprovedNewsfile(programIds);
        }
        
        public bool UpdateNewsFileSequence(int Id, int sequenceId)
        {
            return _iNewsFileRepository.UpdateNewsFileSequence(Id, sequenceId); ;
        }

        public NewsFile UpdateNewsFileSequenceByNewsFileId(int Id, int sequenceId)
        {
            return _iNewsFileRepository.UpdateNewsFileSequenceByNewsFileId(Id, sequenceId);
        }

        public FileStatusHistory PublishNews(PostInput input)
        {
            //NewsFile res = new NewsFile();
            //res = GetNewsFile(Convert.ToInt32(input.NewsFileId));
            //var metas = input.Metas.Select(x => x.MetaValue).ToList();
            //if ((res.StatusId == (int)NewsFileStatus.ProductionApproved && metas.Contains((int)NewNmsRoles.CopyWriter)) || (res.StatusId == (int)NewsFileStatus.CopyWriterApproved && metas.Contains((int)NewNmsRoles.Production))) 
            //{
            //    res.StatusId = (int)NewsFileStatus.CopyWriterAndProductionApproved;
            //}
            //else if (res.StatusId == (int)NewsFileStatus.CopyWriterApprovalPending)
            //{
            //    if (metas.Contains((int)NewNmsRoles.CopyWriter))
            //    {
            //        res.StatusId = (int)NewsFileStatus.CopyWriterApproved;
            //    }
            //    else if (metas.Contains((int)NewNmsRoles.Production))
            //    {
            //        res.StatusId = (int)NewsFileStatus.ProductionApproved;
            //    }
            //}
            //else if (res.StatusId == (int)NewsFileStatus.Created && metas.Contains((int)NewNmsRoles.Reporter))
            //{
            //    res.StatusId = (int)NewsFileStatus.CopyWriterApprovalPending;
            //}
            //res.LastUpdateDate = DateTime.UtcNow;
            //if (UpdateNewsFileStatusAndLastUpdate(res))
            //{
            //    FileStatusHistory fileHist = new FileStatusHistory();
            //    fileHist.CopyFrom(res);
            //    fileHist.UserId = res.CreatedBy;
            //    fileHist = _iFileStatusHistoryRepository.InsertFileStatusHistory(fileHist);
            //    return fileHist;
            //}
            //else
            //    return null;
            return null;
        }

        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput UpdateNewsFileAction(PostInput input)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput result = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            NewsFile res = new NewsFile();
            FileFolderHistory fileFold = new FileFolderHistory();
            FileStatusHistory fileHist = new FileStatusHistory();
            FileDetail fD = new FileDetail();
            int status = Convert.ToInt32(input.StatusId);
            res = GetNewsFile(Convert.ToInt32(input.NewsFileId));
            if (res != null)
            {
                res.FolderId = Convert.ToInt32(input.FolderId);
                res.LastUpdateDate = DateTime.UtcNow;
                if (status == (int)NewsFileActionStatus.Move)
                {
                    res = UpdateNewsFile(res);
                    var fileD = _iFileDetailRepository.GetFileDetailByNewsFileId(Convert.ToInt32(input.NewsFileId));
                    if (fileD != null)
                    {
                        fD = fileD.Last();
                    }
                    else
                    {
                        fD.CopyFrom(res);
                    }
                }
                else if (status == (int)NewsFileActionStatus.Copy)
                {
                    res.NewsFileId = 0;
                    res.CreationDate = res.LastUpdateDate;
                    res.StatusId = (int)NewsFileStatus.Blue;
                    res = InsertNewsFile(res);
                    fileHist.CopyFrom(res);
                    var fileD = _iFileDetailRepository.GetFileDetailByNewsFileId(Convert.ToInt32(input.NewsFileId));
                    if (fileD != null)
                    {
                        fD = fileD.Last();
                        fD.FileDetailId = 0;
                        fD.NewsFileId = res.NewsFileId;
                        fD.CreationDate = res.CreationDate;
                        fD.LastUpdateDate = res.CreationDate;
                        fD = _iFileDetailRepository.InsertFileDetail(fD);
                    }
                    else
                    {
                        fD.CopyFrom(res);
                    }

                    fileHist.UserId = res.CreatedBy;
                    fileHist.CreationDate = res.LastUpdateDate;
                    fileHist = _iFileStatusHistoryRepository.InsertFileStatusHistory(fileHist);

                    var fileRes = _iFileResourceRepository.GetFileResourceByNewsFileId(Convert.ToInt32(input.NewsFileId));
                    result.FileResource = new List<Core.DataTransfer.FileResource.GetOutput>();
                    List<FileResource> resourcesAdded = new List<FileResource>();
                    if (fileRes != null)
                    {
                        foreach (var fr in fileRes)
                        {
                            if (fr.IsActive)
                            {
                                fr.NewsFileId = res.NewsFileId;
                                fr.FileResourceId = 0;
                                fr.CreationDate = DateTime.UtcNow;
                                fr.LastUpdateDate = DateTime.UtcNow;
                                resourcesAdded.Add(_iFileResourceRepository.InsertFileResource(fr));

                            }
                        }
                        result.FileResource.CopyFrom(resourcesAdded);
                    }
                }
                fileFold.CopyFrom(res);
                fileFold.UserId = res.CreatedBy;
                fileFold.CreationDate = res.LastUpdateDate;
                fileFold = _iFileFolderHistoryRepository.InsertFileFolderHistory(fileFold);
            }
            result.NewsFile = new List<Core.DataTransfer.NewsFile.GetOutput>();
            Core.DataTransfer.NewsFile.GetOutput newFile = new Core.DataTransfer.NewsFile.GetOutput();
            newFile.CopyFrom(res);
            result.NewsFile.Add(newFile);

            result.FileDetail = new List<Core.DataTransfer.FileDetail.GetOutput>();
            Core.DataTransfer.FileDetail.GetOutput fileDetail = new Core.DataTransfer.FileDetail.GetOutput();
            fileDetail.CopyFrom(fD);
            result.FileDetail.Add(fileDetail);

            result.FileFolderHistory = new List<Core.DataTransfer.FileFolderHistory.GetOutput>();
            Core.DataTransfer.FileFolderHistory.GetOutput FileFoldHistory = new Core.DataTransfer.FileFolderHistory.GetOutput();
            FileFoldHistory.CopyFrom(fileFold);
            result.FileFolderHistory.Add(FileFoldHistory);

            Core.DataTransfer.FileStatusHistory.GetOutput FileStatHistory = new Core.DataTransfer.FileStatusHistory.GetOutput();
            FileStatHistory.CopyFrom(fileHist);
            result.FileStatusHistory = new List<Core.DataTransfer.FileStatusHistory.GetOutput>();
            result.FileStatusHistory.Add(FileStatHistory);

            return result;
        }

        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput UpdateNewsFile(NMS.Core.DataTransfer.FileDetail.PostOutput input)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput result = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            NewsFile res = new NewsFile();
            List<FileResource> resources = new List<FileResource>();
            res = GetNewsFile(Convert.ToInt32(input.NewsFileId));
            int status = res.StatusId;
            FileDetail fD = new FileDetail();
            if (status == (int)NewsFileStatus.Red)
            {
                var fileD = _iFileDetailRepository.GetFileDetailByNewsFileId(input.NewsFileId);
                if (fileD != null)
                    fD = fileD.Last();

                if (fileD != null)
                {
                    fD.Slug = input.Slug;
                    fD.Text = input.Text;
                    fD.ReportedBy = input.ReportedBy;
                    fD.LastUpdateDate = DateTime.UtcNow;
                    fD = _iFileDetailRepository.UpdateFileDetail(fD);
                }
                else
                {
                    fD.CopyFrom(res);
                    fD.Text = input.Text;
                    fD.ReportedBy = input.ReportedBy;
                    fD.CreationDate = DateTime.UtcNow;
                    fD.LastUpdateDate = fD.CreationDate;
                    fD = _iFileDetailRepository.InsertFileDetail(fD);
                }
            }
            else
            {
                fD.CopyFrom(res);
                fD.Text = input.Text;
                fD.ReportedBy = input.ReportedBy;
                fD.CreationDate = DateTime.UtcNow;
                fD.LastUpdateDate = fD.CreationDate;
                fD = _iFileDetailRepository.InsertFileDetail(fD);
            }

            if (fD != null && input.FileResource != null)
            {
                var alreadyRes = _iFileResourceRepository.GetFileResourceByNewsFileId(input.NewsFileId);
                List<Guid> alreadyGuid = new List<Guid>();
                if (alreadyRes != null)
                    alreadyGuid = alreadyRes.Select(x => x.Guid).ToList();
                for (int x = 0; x < input.FileResource.Count; x++)
                {
                    var resource = input.FileResource[x];
                    if (!alreadyGuid.Contains(resource.Guid))
                    {
                        resource.CreationDate = DateTime.UtcNow;
                        resource.LastUpdateDate = resource.CreationDate;
                        resources.Add(_iFileResourceRepository.InsertFileResource(resource));
                    }
                }
            }
            result.FileResource = new List<NMS.Core.DataTransfer.FileResource.GetOutput>();
            result.FileResource.CopyFrom(resources);
            result.NewsFile = new List<GetOutput>();
            result.NewsFile.CopyFrom(res);
            result.FileDetail = new List<Core.DataTransfer.FileDetail.GetOutput>();
            result.FileDetail.CopyFrom(fD);
            return result;
        }

        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput InsertUpdateNewsFile(NMS.Core.DataTransfer.FileDetail.PostOutput input)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput result = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            NewsFile res = new NewsFile();
            List<FileResource> resources = new List<FileResource>();
            FileStatusHistory fileHist = new FileStatusHistory();
            FileFolderHistory fileFo = new FileFolderHistory();
            FileDetail fD = new FileDetail();

            if (input.NewsFileId == 0)
            {
                res.CopyFrom(input);
                res.CreationDate = DateTime.UtcNow;
                res.LastUpdateDate = res.CreationDate;
                res.PublishTime = res.CreationDate;
                res = _iNewsFileRepository.InsertNewsFile(res);

                fileHist.CopyFrom(res);
                fileHist.UserId = res.CreatedBy;
                fileHist.CreationDate = res.LastUpdateDate;
                fileHist = _iFileStatusHistoryRepository.InsertFileStatusHistory(fileHist);
            }
            else
            {
                res = GetNewsFile(input.NewsFileId);
                var fileD = _iFileDetailRepository.GetFileDetailByNewsFileId(input.NewsFileId);
                if (fileD != null)
                    fD = fileD.Last();

                if (res.Slug != input.Slug || fileD == null || (fileD != null && fD.Text != input.Text))
                {
                    res.Slug = input.Slug;
                    res.LastUpdateDate = DateTime.UtcNow;
                    UpdateNewsFile(res);
                }
            }

            fileFo.CopyFrom(res);
            fileFo.UserId = res.CreatedBy;
            fileFo.CreationDate = res.LastUpdateDate;
            fileFo = _iFileFolderHistoryRepository.InsertFileFolderHistory(fileFo);

            fD.CopyFrom(input);
            fD.NewsFileId = res.NewsFileId;
            fD.CreationDate = res.LastUpdateDate;
            fD.LastUpdateDate = fD.CreationDate;
            fD = _iFileDetailRepository.InsertFileDetail(fD);

            if (fD != null && input.FileResource.Count > 0)
            {
                var alreadyRes = _iFileResourceRepository.GetFileResourceByNewsFileId(input.NewsFileId);
                List<Guid> alreadyGuid = new List<Guid>();
                if (alreadyRes != null)
                    alreadyGuid = alreadyRes.Select(x => x.Guid).ToList();
                for (int x = 0; x < input.FileResource.Count; x++)
                {
                    var resource = input.FileResource[x];
                    if (!alreadyGuid.Contains(resource.Guid))
                    {
                        resource.CreationDate = DateTime.UtcNow;
                        resource.IsActive = true;
                        resource.LastUpdateDate = resource.CreationDate;
                        resources.Add(_iFileResourceRepository.InsertFileResource(resource));
                    }
                }
            }
            if (resources.Count > 0)
            {
                result.FileResource = new List<Core.DataTransfer.FileResource.GetOutput>();
                List<Core.DataTransfer.FileResource.GetOutput> resourcesOut = new List<Core.DataTransfer.FileResource.GetOutput>();
                resourcesOut.CopyFrom(resources);
                result.FileResource.AddRange(resourcesOut);
            }
            result.FileFolderHistory = new List<Core.DataTransfer.FileFolderHistory.GetOutput>();
            Core.DataTransfer.FileFolderHistory.GetOutput fileFolder = new Core.DataTransfer.FileFolderHistory.GetOutput();
            fileFolder.CopyFrom(fileFo);
            result.FileFolderHistory.Add(fileFolder);

            result.NewsFile = new List<GetOutput>();
            result.NewsFile.CopyFrom(res);
            result.FileDetail = new List<Core.DataTransfer.FileDetail.GetOutput>();
            result.FileDetail.CopyFrom(fD);

            Core.DataTransfer.FileStatusHistory.GetOutput FileStatHistory = new Core.DataTransfer.FileStatusHistory.GetOutput();
            FileStatHistory.CopyFrom(fileHist);
            result.FileStatusHistory = new List<Core.DataTransfer.FileStatusHistory.GetOutput>();
            result.FileStatusHistory.Add(FileStatHistory);

            Core.DataTransfer.FileDetail.GetOutput fileDetail = new Core.DataTransfer.FileDetail.GetOutput();
            fileDetail.CopyFrom(fD);
            result.FileDetail = new List<Core.DataTransfer.FileDetail.GetOutput>();
            result.FileDetail.Add(fileDetail);

            result.NewsFile = new List<Core.DataTransfer.NewsFile.GetOutput>();
            Core.DataTransfer.NewsFile.GetOutput newFile = new Core.DataTransfer.NewsFile.GetOutput();
            newFile.CopyFrom(res);
            result.NewsFile.Add(newFile);

            return result;
        }

        public List<NMS.Core.DataTransfer.User.GetOutput> GetAllUsers()
        {
            List<NMS.Core.DataTransfer.User.GetOutput> usersToRet = new List<Core.DataTransfer.User.GetOutput>();
            MMS.Integration.UserManagement.UserManagement userInt = new MMS.Integration.UserManagement.UserManagement();
            var userOut = userInt.UsersWithWorkRole(22);
            var users = userOut.Data;
            for (int i = 0; i < users.Count; i++)
            {
                usersToRet.Add(new NMS.Core.DataTransfer.User.GetOutput { UserId = users[i].UserId, Name = users[i].Fullname });
            }
            return usersToRet;
        }

        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput GetRundownByEpisodeId(int episodeId)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput res = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            NewsFilePollingData cData = CacheManager.NewsFilePollingData;
            var cFolders = CacheManager.Folders;

            List<Folder> runDownFolder = cFolders.Where(x => x.IsRundown == true && x.EpisodeId == episodeId).ToList();
            List<Core.DataTransfer.Folder.GetOutput> folds = new List<Core.DataTransfer.Folder.GetOutput>();
            folds.CopyFrom(runDownFolder);
            res.Folder = new List<NMS.Core.DataTransfer.Folder.GetOutput>();
            if (folds != null)
                res.Folder.AddRange(folds);


            List<int> folderIds = runDownFolder.Select(x => x.FolderId).ToList();
            List<NewsFile> runDownFiles = cData.Files.Where(x => folderIds.Contains(x.FolderId)).ToList();
            List<int> fileIds = runDownFiles.Select(x => x.NewsFileId).ToList();
            res.NewsFile = new List<GetOutput>();
            if (runDownFiles != null)
                res.NewsFile.CopyFrom(runDownFiles);


            List<FileResource> fileResources = cData.FileResources.Where(x => fileIds.Contains(x.NewsFileId)).ToList();
            res.FileResource = new List<Core.DataTransfer.FileResource.GetOutput>();
            if (fileResources != null)
                res.FileResource.CopyFrom(fileResources);


            List<FileDetail> runDownDetails = new List<FileDetail>();
            runDownDetails = cData.FileDetails.Where(x => fileIds.Contains(x.NewsFileId)).ToList();
            res.FileDetail = new List<NMS.Core.DataTransfer.FileDetail.GetOutput>();
            if (runDownDetails != null)
                res.FileDetail.CopyFrom(runDownFiles);


            return res;
        }

        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput GetRundownByEpisodeIdFromDB(int episodeId)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput res = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            FolderRepository fRepo = new FolderRepository();
            List<Folder> runDownFolder = fRepo.GetFolderByEpisodeId(episodeId);
            List<Core.DataTransfer.Folder.GetOutput> folds = new List<Core.DataTransfer.Folder.GetOutput>();
            folds.CopyFrom(runDownFolder);
            res.Folder = new List<NMS.Core.DataTransfer.Folder.GetOutput>();
            if (folds != null)
                res.Folder.AddRange(folds);

            List<int> folderIds = runDownFolder.Select(x => x.FolderId).ToList();
            NewsFileRepository newsFileRepo = new NewsFileRepository();
            List<NewsFile> runDownFiles = new List<NewsFile>();
            foreach (var fol in folderIds)
            {
                runDownFiles.AddRange(newsFileRepo.GetNewsFileByFolderId(fol));
            }
            List<int> fileIds = runDownFiles.Select(x => x.NewsFileId).ToList();
            res.NewsFile = new List<GetOutput>();
            if (runDownFiles != null)
                res.NewsFile.CopyFrom(runDownFiles);

            FileResourceRepository resourceRepo = new FileResourceRepository();
            FileDetailRepository fileDetRepo = new FileDetailRepository();

            List<FileResource> fileResources = new List<FileResource>();
            List<FileDetail> runDownDetails = new List<FileDetail>();
            foreach (var fileId in fileIds)
            {
                fileResources.AddRange(resourceRepo.GetFileResourceByNewsFileId(fileId));
                runDownDetails.AddRange(fileDetRepo.GetFileDetailByNewsFileId(fileId));
            }

            // cData.FileResources.Where(x => fileIds.Contains(x.NewsFileId)).ToList();
            res.FileResource = new List<Core.DataTransfer.FileResource.GetOutput>();
            res.FileDetail = new List<NMS.Core.DataTransfer.FileDetail.GetOutput>();
            if (fileResources != null)
                res.FileResource.CopyFrom(fileResources);
            if (runDownDetails != null)
                res.FileDetail.CopyFrom(runDownFiles);

            return res;
        }

        public bool DeleteFileResouceByGuid(string guid)
        {
            return _iFileResourceRepository.MarkFileResourceInActiveByGuid(guid);
        }

        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput GetNewsFiles(NMS.Core.DataTransfer.NewsFile.GET.NMSPollingInput input)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput res = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            NewsFilePollingData cData = CacheManager.NewsFilePollingData;
            List<int> currentUserProgIds = new List<int>();
            if (cData != null)
            {
                List<int> fileIds = new List<int>();
                var cFolders = CacheManager.Folders;
                var folderIDs = CacheManager.WorkRoleFolder.Where(x => x.WorkRoleId == input.WorkRoleId).Select(x => x.FolderId).ToList();
                var currentUserProgs = CacheManager.Teams.Where(x => x.UserId == input.UserId).ToList();
                currentUserProgIds = currentUserProgs.Select(y => y.ProgramId.Value).ToList();
                List<Folder> pFolders = new List<Folder>();
                if (currentUserProgIds.Count > 0)
                    pFolders = pFolders = cFolders.Where(x => x.ProgramId.HasValue && currentUserProgIds.Contains(x.ProgramId.Value)).ToList();

                if (pFolders != null)
                {
                    res.Folder = new List<NMS.Core.DataTransfer.Folder.GetOutput>();
                    res.WorkRoleFolder = new List<NMS.Core.DataTransfer.WorkRoleFolder.GetOutput>();
                    res.Folder.CopyFrom(cFolders.Where(x => folderIDs.Contains(x.FolderId)).ToList());
                    List<int> additionalFolderIds = pFolders.Select(x => x.FolderId).ToList();
                    folderIDs.AddRange(additionalFolderIds);
                    List<Core.DataTransfer.Folder.GetOutput> folds = new List<Core.DataTransfer.Folder.GetOutput>();
                    folds.CopyFrom(pFolders);
                    res.Folder.AddRange(folds);
                }

                if (input.FileDetailLastUpdate == DateTime.MinValue && input.FileFolderHistoryLastUpdate == DateTime.MinValue && input.FileResourceLastUpdate == DateTime.MinValue && input.FileStatusHistoryLastUpdate == DateTime.MinValue && input.NewsFileLastUpdate == DateTime.MinValue)
                {
                    res.Users = GetAllUsers();
                    res.Programs = GetAllProgramsByUserId(input.UserId);
                    res.Programs.Select(x => x.ProgramId).ToList();
                }
                if (cData.Files != null)
                {
                    var query = cData.Files.Where(x => folderIDs.Contains(x.FolderId));
                    if (input.NewsFileLastUpdate != DateTime.MinValue)
                    {
                        query = query.Where(x => x.CreationDate > input.NewsFileLastUpdate);
                        res.Folder = new List<NMS.Core.DataTransfer.Folder.GetOutput>();
                    }
                    else
                    {
                        res.WorkRoleFolder.CopyFrom(CacheManager.WorkRoleFolder.Where(x => x.WorkRoleId == input.WorkRoleId).ToList());
                    }
                    res.NewsFile = new List<GetOutput>();
                    res.NewsFile.CopyFrom(query.ToList());
                    res.NewsFile.CopyFrom(cData.Files.ToList());
                    res.NewsFile.RemoveAll(x => x.FolderId == 12 && x.CreatedBy != input.UserId);
                    fileIds = cData.Files.Where(x => folderIDs.Contains(x.FolderId)).Select(x => x.NewsFileId).ToList();
                }

                if (cData.FileDetails != null)
                {
                    var query = cData.FileDetails.Where(x => fileIds.Contains(x.NewsFileId));
                    if (input.FileDetailLastUpdate != DateTime.MinValue)
                        query = query.Where(x => x.CreationDate > input.FileDetailLastUpdate);
                    res.FileDetail = new List<NMS.Core.DataTransfer.FileDetail.GetOutput>();
                    if (query.ToList().Count > 0)
                        res.FileDetail.CopyFrom(query.ToList());
                    res.FileDetail = new List<NMS.Core.DataTransfer.FileDetail.GetOutput>();
                    res.FileDetail.CopyFrom(cData.FileDetails.ToList());
                }
                if (cData.FileResources != null)
                {
                    var query = cData.FileResources.Where(x => fileIds.Contains(x.NewsFileId));
                    if (input.FileResourceLastUpdate != DateTime.MinValue)
                        query = query.Where(x => x.CreationDate > input.FileResourceLastUpdate);
                    res.FileResource = new List<NMS.Core.DataTransfer.FileResource.GetOutput>();
                    if (query.ToList().Count > 0)
                        res.FileResource.CopyFrom(query.ToList());
                    res.FileResource = new List<NMS.Core.DataTransfer.FileResource.GetOutput>();
                    if (query.ToList().Count > 0)
                        res.FileResource.CopyFrom(query.ToList());

                }
                if (cData.FileStatusHistory != null)
                {
                    var query = cData.FileStatusHistory.Where(x => fileIds.Contains(x.NewsFileId));
                    if (input.FileStatusHistoryLastUpdate != DateTime.MinValue)
                        query = query.Where(x => x.CreationDate > input.FileStatusHistoryLastUpdate);
                    res.FileStatusHistory = new List<NMS.Core.DataTransfer.FileStatusHistory.GetOutput>();
                    if (query.ToList().Count > 0)
                        res.FileStatusHistory.CopyFrom(query.ToList());
                    res.FileStatusHistory = new List<NMS.Core.DataTransfer.FileStatusHistory.GetOutput>();
                    if (query.ToList().Count > 0)
                        res.FileStatusHistory.CopyFrom(query.ToList());

                }
                if (cData.FileFolderHistory != null)
                {
                    var query = cData.FileFolderHistory.ToList();
                    if (input.FileFolderHistoryLastUpdate != DateTime.MinValue)
                        query = query.Where(x => x.CreationDate > input.FileFolderHistoryLastUpdate).ToList();
                    res.FileFolderHistory = new List<NMS.Core.DataTransfer.FileFolderHistory.GetOutput>();
                    if (query.Count > 0)
                        res.FileFolderHistory.CopyFrom(query);
                }

            }

            return res;
        }

        private List<Core.DataTransfer.Program.GetOutput> GetAllProgramsByUserId(int UserId)
        {
            List<Core.DataTransfer.Program.GetOutput> ret = new List<Core.DataTransfer.Program.GetOutput>();

            var progs = _iProgramRepository.GetProgramByUserTeam(UserId);
            ret.CopyFrom(progs);
            return ret;
        }
        public NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput GetAllNewsFilesData(NMS.Core.DataTransfer.NewsFile.GET.NMSPollingInput input)
        {
            NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput res = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            NewsFilePollingData cData = new NewsFilePollingData(); // CacheManager.NewsFilePollingData;
            cData.Files = _iNewsFileRepository.GetAllNewsFile();

            if (cData.Files.Count > 0)
            {
                //cData.Files = cData.Files.Where(x => x.NewsStatus != 0).ToList().OrderBy(x => x.NewsStatus).ToList(); ;

            }

            List <int> currentUserProgIds = new List<int>();
            if (cData != null)
            {
                //List<int> fileIds = new List<int>();
                //var cFolders = CacheManager.Folders;
                //var folderIDs = CacheManager.WorkRoleFolder.Where(x => x.WorkRoleId == input.WorkRoleId).Select(x => x.FolderId).ToList();
                //var currentUserProgs = CacheManager.Teams.Where(x => x.UserId == input.UserId).ToList();
                //currentUserProgIds = currentUserProgs.Select(y => y.ProgramId.Value).ToList();
                //List<Folder> pFolders = new List<Folder>();
                //if (currentUserProgIds.Count > 0)
                //    pFolders = pFolders = cFolders.Where(x => x.ProgramId.HasValue && currentUserProgIds.Contains(x.ProgramId.Value)).ToList();

                //if (pFolders != null)
                //{
                //    res.Folder = new List<NMS.Core.DataTransfer.Folder.GetOutput>();
                //    res.WorkRoleFolder = new List<NMS.Core.DataTransfer.WorkRoleFolder.GetOutput>();
                //    res.Folder.CopyFrom(cFolders.Where(x => folderIDs.Contains(x.FolderId)).ToList());
                //    List<int> additionalFolderIds = pFolders.Select(x => x.FolderId).ToList();
                //    folderIDs.AddRange(additionalFolderIds);
                //    List<Core.DataTransfer.Folder.GetOutput> folds = new List<Core.DataTransfer.Folder.GetOutput>();
                //    folds.CopyFrom(pFolders);
                //    res.Folder.AddRange(folds);
                //}

                //if (input.FileDetailLastUpdate == DateTime.MinValue && input.FileFolderHistoryLastUpdate == DateTime.MinValue && input.FileResourceLastUpdate == DateTime.MinValue && input.FileStatusHistoryLastUpdate == DateTime.MinValue && input.NewsFileLastUpdate == DateTime.MinValue)
                //{
                //    res.Users = GetAllUsers();
                //    res.Programs = GetAllProgramsByUserId(input.UserId);
                //    //res.Programs.Select(x=>x.ProgramId).ToList();
                //}
                if (cData.Files != null)
                {
                    //var query = cData.Files.Where(x => folderIDs.Contains(x.FolderId));
                    //if (input.NewsFileLastUpdate != DateTime.MinValue)
                    //{
                    //    query = query.Where(x => x.CreationDate > input.NewsFileLastUpdate);
                    //    res.Folder = new List<NMS.Core.DataTransfer.Folder.GetOutput>();
                    //}
                    //else
                    //{
                    //    res.WorkRoleFolder.CopyFrom(CacheManager.WorkRoleFolder.Where(x => x.WorkRoleId == input.WorkRoleId).ToList());
                    //}
                    res.NewsFile = new List<GetOutput>();
                    //res.NewsFile.CopyFrom(query.ToList());
                    res.NewsFile.CopyFrom(cData.Files.ToList());
                    //res.NewsFile.RemoveAll(x => x.FolderId == 12 && x.CreatedBy != input.UserId);
                    //fileIds = cData.Files.Where(x => folderIDs.Contains(x.FolderId)).Select(x => x.NewsFileId).ToList();
                }

                if (cData.FileDetails != null)
                {
                    //var query = cData.FileDetails.Where(x => fileIds.Contains(x.NewsFileId));
                    //if (input.FileDetailLastUpdate != DateTime.MinValue)
                    //    query = query.Where(x => x.CreationDate > input.FileDetailLastUpdate);
                    //res.FileDetail = new List<NMS.Core.DataTransfer.FileDetail.GetOutput>();
                    //if (query.ToList().Count > 0)
                    //res.FileDetail.CopyFrom(query.ToList());
                    res.FileDetail = new List<NMS.Core.DataTransfer.FileDetail.GetOutput>();
                    res.FileDetail.CopyFrom(cData.FileDetails.ToList());
                }
                if (cData.FileResources != null)
                {
                    //var query = cData.FileResources.Where(x => fileIds.Contains(x.NewsFileId));
                    //if (input.FileResourceLastUpdate != DateTime.MinValue)
                    //    query = query.Where(x => x.CreationDate > input.FileResourceLastUpdate);
                    //res.FileResource = new List<NMS.Core.DataTransfer.FileResource.GetOutput>();
                    //if (query.ToList().Count > 0)
                    //    res.FileResource.CopyFrom(query.ToList());
                    res.FileResource = new List<NMS.Core.DataTransfer.FileResource.GetOutput>();
                    //if (query.ToList().Count > 0)
                    //    res.FileResource.CopyFrom(query.ToList());

                }
                if (cData.FileStatusHistory != null)
                {
                    //var query = cData.FileStatusHistory.Where(x => fileIds.Contains(x.NewsFileId));
                    //if (input.FileStatusHistoryLastUpdate != DateTime.MinValue)
                    //    query = query.Where(x => x.CreationDate > input.FileStatusHistoryLastUpdate);
                    //res.FileStatusHistory = new List<NMS.Core.DataTransfer.FileStatusHistory.GetOutput>();
                    //if (query.ToList().Count > 0)
                    //    res.FileStatusHistory.CopyFrom(query.ToList());
                    res.FileStatusHistory = new List<NMS.Core.DataTransfer.FileStatusHistory.GetOutput>();
                    //if (query.ToList().Count > 0)
                    //    res.FileStatusHistory.CopyFrom(query.ToList());

                }
                if (cData.FileFolderHistory != null)
                {
                    //var query = cData.FileFolderHistory.ToList();
                    //if (input.FileFolderHistoryLastUpdate != DateTime.MinValue)
                    //    query = query.Where(x => x.CreationDate > input.FileFolderHistoryLastUpdate).ToList();
                    //res.FileFolderHistory = new List<NMS.Core.DataTransfer.FileFolderHistory.GetOutput>();
                    //if (query.Count > 0)
                    //    res.FileFolderHistory.CopyFrom(query);
                    //res.FileFolderHistory = new List<NMS.Core.DataTransfer.FileFolderHistory.GetOutput>();
                    //res.FileFolderHistory.CopyFrom(cData.FileFolderHistory.ToList());
                }

            }

            return res;
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsfileid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out newsfileid))
            {
                NewsFile newsfile = _iNewsFileRepository.GetNewsFile(newsfileid);
                if (newsfile != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsfile);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsFile> newsfilelist = _iNewsFileRepository.GetAllNewsFile();
            if (newsfilelist != null && newsfilelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsfilelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            NewsFile file = new NewsFile();
            file.CopyFrom(Input);
            file.CreationDate = DateTime.UtcNow;
            file.LastUpdateDate = file.CreationDate;
            file.PublishTime = file.CreationDate;

            file = _iNewsFileRepository.InsertNewsFile(file);

            FileStatusHistory fileHist = new FileStatusHistory();
            fileHist.CopyFrom(file);
            fileHist.UserId = file.CreatedBy;

            FileFolderHistory fileFold = new FileFolderHistory();
            fileFold.CopyFrom(file);
            fileFold.UserId = file.CreatedBy;


            if (file != null)
            {
                _iFileStatusHistoryRepository.InsertFileStatusHistory(fileHist);
                _iFileFolderHistoryRepository.InsertFileFolderHistory(fileFold);
            }

            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            transer.Data = new PostOutput();
            transer.Data.CopyFrom(file);
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsFile newsfileinput = new NewsFile();
                NewsFile newsfileoutput = new NewsFile();
                PutOutput output = new PutOutput();
                newsfileinput.CopyFrom(Input);
                NewsFile newsfile = _iNewsFileRepository.GetNewsFile(newsfileinput.NewsFileId);
                if (newsfile != null)
                {
                    newsfileoutput = _iNewsFileRepository.UpdateNewsFile(newsfileinput);
                    if (newsfileoutput != null)
                    {
                        output.CopyFrom(newsfileoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsfileid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out newsfileid))
            {
                bool IsDeleted = _iNewsFileRepository.DeleteNewsFile(newsfileid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public NewsFile InsertNews(NMS.Core.DataTransfer.News.PostInput postInput)
        {
            StringBuilder sb = new StringBuilder();
            DateTime dtNow = DateTime.UtcNow;
            NewsFile newsFile = new NewsFile();
            NewsFileFilter newsfilefilter = new NewsFileFilter();
            bool isSameNews = false;


            //// for resource edits /////////////
            if (postInput.ResourceEdit != null && postInput.ResourceEdit.Id > 0)
            {
                //if(postInput.ProgramId == 0 && postInput.ResourceEdit.ProgramId > 0)
                //{
                //    postInput.ProgramId = postInput.ResourceEdit.ProgramId;
                //}

                INewsService nService = IoC.Resolve<INewsService>("NewsService");
                if (postInput.Resources == null)
                    postInput.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                Core.Models.ResourceEdit resEdid = new ResourceEdit(); //postInput.ResourceEdit resEdid
                postInput.Resources.Add(nService.GenerateResource(postInput.ResourceEdit));
                postInput.ResourceEdit = postInput.ResourceEdit;

                newsFile.ResourceGuid = postInput.ResourceEdit.ResourceGuid.ToString();

            }

            //////////////////////////////////////

            if (postInput.NewsFileId > 0)
            {
                newsFile = _iNewsFileRepository.GetNewsFile(postInput.NewsFileId);
                if (newsFile != null) { isSameNews = true; }
            }
            else
            {
                newsFile.CreatedBy = postInput.ReporterId;
                newsFile.CreationDate = dtNow;
                newsFile.LastUpdateDate = newsFile.CreationDate;
            }

            newsFile.Title = postInput.Title;

            if (postInput.NewsDate.Year > 1)
                newsFile.PublishTime = postInput.NewsDate;
            else
                newsFile.PublishTime = DateTime.UtcNow;

            newsFile.LastUpdateDate = dtNow;
            newsFile.LanguageCode = postInput.LanguageCode.ToLower();
            newsFile.Highlights = postInput.Highlight;
            newsFile.Source = postInput.Source;
            newsFile.SourceNewsUrl = postInput.Url;
            newsFile.SourceTypeId = postInput.FilterTypeId;
            if (postInput.IsTagged.HasValue)
            {
                newsFile.IsTagged = postInput.IsTagged.Value;
            }
            else
            {
                newsFile.IsTagged = false;
            }


            if (postInput.TaggedBy.HasValue)
                newsFile.TaggedBy = postInput.TaggedBy.Value;

            if (postInput.InsertedBy.HasValue)
                newsFile.InsertedBy = postInput.InsertedBy.Value;

            if (postInput.BureauId != null || postInput.BureauId > 0)
                newsFile.ReporterBureauId = postInput.BureauId;

            if (postInput.IsLive)
                newsFile.IsLive = true;
            
            if (!string.IsNullOrEmpty(postInput.EventType))
            {
                EventType eventtype = _iEventTypeRepository.GetEventTypeByExactTerm(postInput.EventType.ToLower().Trim());
                if (eventtype == null)
                {
                    EventType type = new EventType();
                    type.CreationDate = DateTime.UtcNow;
                    type.LastUpdateDate = DateTime.UtcNow;
                    type.IsActive = true;
                    type.IsApproved = false;
                    type.Name = postInput.EventType.ToLower().Trim();

                    var newevent = _iEventTypeRepository.InsertEventType(type);
                    newsFile.EventType = newevent.EventTypeId;
                }
                else
                {
                    newsFile.EventType = eventtype.EventTypeId;
                }
            }
                
            if (postInput.Coverage.HasValue)
                newsFile.Coverage = postInput.Coverage.Value;
            
            if (postInput.FilterTypeId != (int)FilterTypes.FieldReporter && postInput.FilterTypeId != (int)FilterTypes.Channel && postInput.FilterTypeId != (int)FilterTypes.Event)
            {
                if (newsFile.LanguageCode.ToLower() != "en")
                {
                    Translator NewsTranslater = new Translator();

                    if (!String.IsNullOrEmpty(postInput.Slug))
                        newsFile.Slug = NewsTranslater.Translate(postInput.Slug, newsFile.LanguageCode, "en");
                }
                else
                {
                    newsFile.Slug = postInput.Slug;
                }
            }
            else
            {
                newsFile.Slug = postInput.Slug;
            }
            if (string.IsNullOrEmpty(newsFile.Slug) && !string.IsNullOrEmpty(newsFile.Title))
                newsFile.Slug = newsFile.Title;


            if (newsFile.LanguageCode.ToLower() != "en")
            {
                if (newsFile.Slug != newsFile.Title)
                {
                    sb.Append(newsFile.Slug);
                    sb.Append(" ");
                }
            }
            else
            {
                sb.Append(newsFile.Slug);
                sb.Append(" ");
            }
            
            
            if (postInput.Resources != null && postInput.Resources.Count > 0)
                newsFile.ResourceGuid = postInput.Resources.First().Guid;

            if (postInput.ProgramId == 0)
            {
                newsFile.FolderId = 12;
            }
            else
            {
                var folder = _iFolderRepository.GetFolderByProgramId(postInput.ProgramId);
                if (folder != null)
                {
                    newsFile.FolderId = folder.First().FolderId;
                }
            }
            newsFile.ProgramId = postInput.ProgramId > 0 ? postInput.ProgramId : newsFile.ProgramId;
            newsFile.AssignedTo = postInput.AssignedTo > 0 ? postInput.AssignedTo : newsFile.AssignedTo;

            if (postInput.CategoryIds != null && postInput.CategoryIds.Count > 0)
            {
                newsFile.CategoryId = postInput.CategoryIds.First();
            }

            if (postInput.LocationIds != null && postInput.LocationIds.Count > 0)
            {
                newsFile.LocationId = postInput.LocationIds.First();
            }

            if (newsFile.LocationId.HasValue)
            {
                if (postInput.FilterTypeId == (int)FilterTypes.FieldReporter)
                {
                    var loc = _iLocationRepository.GetLocation(newsFile.LocationId.Value);
                    newsFile.Source = loc.Location;
                }
                else if (postInput.FilterTypeId == (int)FilterTypes.Website)
                {
                    newsFile.LocationId = newsFile.LocationId;
                }
            }

            if (postInput.FilterTypeId == (int)FilterTypes.FieldReporter)
            {
                newsFile.IsVerified = true;
            }

            if (postInput.FilterTypeId == (int)FilterTypes.Channel)
            {
                newsFile.IsVerified = true;
                if (postInput.ChannelId > 0)
                {
                    var channel = _iChannelRepository.GetChannel(postInput.ChannelId);
                    if (channel != null)
                    {
                        postInput.Source = channel.Name;
                        newsFile.Source = postInput.Source;
                    }
                }
            }


            if (postInput.NewsFileId > 0)
                newsFile = _iNewsFileRepository.UpdateNewsFile(newsFile);

            else newsFile = _iNewsFileRepository.InsertNewsFile(newsFile);

            #region File Detail

            FileDetail detail = new FileDetail();
            if (postInput.NewsFileId > 0)
            {
                List<FileDetail> lstdetail = _iFileDetailRepository.GetFileDetailByNewsFileId(postInput.NewsFileId);

                if (lstdetail != null && lstdetail.Count > 0)
                    detail = lstdetail.FirstOrDefault();
            }
            else
            {
                detail.CreationDate = dtNow;
            }
            
            
            detail.NewsFileId = newsFile.NewsFileId;
            detail.CreatedBy = postInput.ReporterId;
            detail.LastUpdateDate = dtNow;
            detail.ReportedBy = postInput.ReporterId;
            detail.Slug = newsFile.Slug;
            detail.Text = postInput.Description == null ? string.Empty : postInput.Description;
            postInput.NewsPaperdescription = postInput.NewsPaperdescription == null ? string.Empty : postInput.NewsPaperdescription;


            #region Text Translation incase not English

            detail.DescriptionText = Regex.Replace(detail.Text, "<.*?>", "", RegexOptions.IgnoreCase).Trim();
            detail.Text = Regex.Replace(detail.Text, "<?.img.*?>", "", RegexOptions.IgnoreCase).Trim();
            //Newspaper description needed to be sorted out
            postInput.NewsPaperdescription = Regex.Replace(postInput.NewsPaperdescription, "<?.img.*?>", "", RegexOptions.IgnoreCase).Trim();
            if (newsFile.LanguageCode != "en")
            {
                detail.DescriptionText = TranslateNews(detail, newsFile.LanguageCode, postInput.NewsPaperdescription);
            }
            else
                detail.Text = String.IsNullOrEmpty(detail.Text) ? postInput.NewsPaperdescription : detail.Text;


            //description text add in search
            sb.Append(detail.DescriptionText);
            sb.Append(" ");


            #endregion Text Translation incase not English

            if (postInput.NewsFileId > 0)
                _iFileDetailRepository.UpdateFileDetail(detail);

            else
            _iFileDetailRepository.InsertFileDetail(detail);

            #endregion

            #region Categories

            if (postInput.CategoryIds != null && postInput.CategoryIds.Count > 0)
            {
                postInput.CategoryIds = postInput.CategoryIds.Distinct().ToList();
                if (postInput.NewsFileId > 0)
                    _iFileCategoryRepository.DeleteFileCategoryByNewsFileId(newsFile.NewsFileId);
                foreach (var item in postInput.CategoryIds)
                {
                    FileCategory category = new FileCategory();
                    category.CategoryId = item;
                    category.CreationDate = dtNow;
                    category.NewsFileId = newsFile.NewsFileId;
                    _iFileCategoryRepository.InsertFileCategory(category);
                }
            }
            #endregion Categories

            #region Location

            if (postInput.LocationIds != null && postInput.LocationIds.Count > 0)
            {
                postInput.LocationIds = postInput.LocationIds.Distinct().ToList();
                if (postInput.NewsFileId > 0)
                    _iFileLocationRepository.DeleteFileLocationByNewsFileId(newsFile.NewsFileId);
                foreach (int locationid in postInput.LocationIds)
                {
                    FileLocation location = new FileLocation();
                    location.LocationId = locationid;
                    location.CreationDate = dtNow;
                    location.NewsFileId = newsFile.NewsFileId;
                    _iFileLocationRepository.InsertFileLocation(location);
                }
            }
            #endregion Location

            #region searchabletext location & category
            //add category in search
            if (newsFile.CategoryId.HasValue)
            {
                Category cat = _iCategoryRepository.GetCategory(newsFile.CategoryId.Value);
                if (cat != null)
                {
                    sb.Append(cat.Category);
                    sb.Append(" ");
                }
            }

            //add location
            if (newsFile.LocationId.HasValue)
            {
                Location loc = _iLocationRepository.GetLocation(newsFile.LocationId.Value);
                if (loc != null)
                {
                    sb.Append(loc.Location);
                    sb.Append(" ");
                }
            }

            #endregion
            
            #region Tags

            if (!string.IsNullOrEmpty(detail.DescriptionText))
            {
                int tagmaxlength = 50;
                //Work for Related Resources if no resource
                KeywordAnalyzer keywordAnalyzer = new KeywordAnalyzer();
                var keyWordObj = keywordAnalyzer.Analyze(detail.DescriptionText);

                Tag tag = new Tag();
                foreach (var item in keyWordObj.Keywords)
                {
                    var taglst = _iTagRepository.GetTagByTermExact(item.Word.ToLower());

                    if (taglst == null)
                    {
                        if (item.Word.Length <= tagmaxlength)
                        {
                            tag.Tag = item.Word.ToLower();
                            tag.Rank = Convert.ToDouble(item.Rank);
                            tag.CreationDate = dtNow;
                            tag.LastUpdateDate = tag.CreationDate;
                            tag.Guid = Guid.NewGuid().ToString();
                            tag.IsActive = true;

                            NMS.Core.DataTransfer.Tag.PostInput taginp = new Core.DataTransfer.Tag.PostInput();
                            taginp.CopyFrom(tag);

                            if (postInput.Tags == null)
                                postInput.Tags = new List<Core.DataTransfer.Tag.PostInput>();

                            postInput.Tags.Add(taginp);
                            // tag = _iMTagRepository.GetTagCreateIfNotExistOptimized(tag);  //No need of tags
                            // mongoNews.Tags.Add(tag);
                        }
                    }
                }
            }

            if (postInput.Tags != null)
            {
               
                if (postInput.NewsFileId > 0)
                    _iFileTagRepository.DeleteFileTagByNewsFileId(newsFile.NewsFileId);
                postInput.Tags = postInput.Tags.Distinct().ToList();
                foreach (var item in postInput.Tags)
                {   
                        FileTag tag = new FileTag();
                        tag.CreationDate = dtNow;
                        tag.NewsFileId = newsFile.NewsFileId;
                        tag.LastUpdateDate = tag.CreationDate;
                        tag.Rank = Convert.ToDouble(item.Rank);
                        var lst = _iTagRepository.GetTagByTermExact(item.Tag.ToLower().Trim());
                        if (lst == null || lst.Count == 0)
                        {
                            Tag _tag = new Tag();
                            _tag.CreationDate = dtNow;
                            _tag.Guid = Guid.NewGuid().ToString();
                            _tag.IsActive = true;
                            _tag.Rank = Convert.ToDouble(item.Rank);
                            _tag.LastUpdateDate = _tag.CreationDate;
                            _tag.Tag = item.Tag.ToLower().Trim();
                            _tag = _iTagRepository.InsertTag(_tag);
                            sb.Append(_tag.Tag.Trim());
                            sb.Append(" ");
                            tag.TagId = _tag.TagId;
                        }
                        else
                        {
                            tag.TagId = lst[0].TagId;
                            sb.Append(lst[0].Tag.Trim());
                            sb.Append(" ");
                        }

                        _iFileTagRepository.InsertFileTag(tag);
                    
                }
            }

            if (sb.Length > 0)
            {
                newsFile.SearchableText = sb.ToString();
                newsFile = _iNewsFileRepository.UpdateNewsFile(newsFile);
            }
            #endregion Tags
            
            #region ResourceEdit

            if (postInput.ResourceEdit != null)
            {

                NewsResourceEdit resourceEdit = new NewsResourceEdit();
                resourceEdit.ResourceGuid = postInput.ResourceEdit.ResourceGuid;

                if (newsFile != null)
                    resourceEdit.NewsId = newsFile.NewsFileId;

                resourceEdit.ResourceTypeId = postInput.ResourceEdit.ResourceTypeId;
                resourceEdit.FileName = postInput.ResourceEdit.FileName;
                resourceEdit.Top = postInput.ResourceEdit.Top;
                resourceEdit.Left = postInput.ResourceEdit.FromTos[0].From;
                resourceEdit.Bottom = postInput.ResourceEdit.Bottom;
                resourceEdit.Right = postInput.ResourceEdit.FromTos[0].To;
                resourceEdit.Url = postInput.ResourceEdit.Url;
                resourceEdit.ChannelVideoId = postInput.ResourceEdit.Id;
                resourceEdit.CreationDate = DateTime.UtcNow;
                resourceEdit.LastUpdateDate = DateTime.UtcNow;
                resourceEdit.Isactive = true;
                INewsResourceEditRepository inewsResourceEditRepository = IoC.Resolve<INewsResourceEditRepository>("NewsResourceEditRepository");
                inewsResourceEditRepository.InsertNewsResourceEdit(resourceEdit);
            }

            #endregion
            
            #region Resources

            if (postInput.Resources != null)
            {
                if (postInput.NewsFileId > 0)
                    _iFileResourceRepository.DeleteFileResourceByNewsFileId(newsFile.NewsFileId);
                foreach (var item in postInput.Resources)
                {
                    
                    FileResource resource = new FileResource();
                    resource.Guid = new Guid(item.Guid);
                    resource.CreationDate = dtNow;
                    resource.LastUpdateDate = dtNow;
                    resource.IsActive = true;
                    resource.NewsFileId = newsFile.NewsFileId;
                    resource.ResourceTypeId = Convert.ToInt32(item.ResourceTypeId);
                    _iFileResourceRepository.InsertFileResource(resource);
                }
            }

            #endregion Resources
            
            #region AddResourceMeta


            if (postInput.Resources != null && postInput.Resources.Count > 0)
            {
                if(newsFile.Resources == null)
                {
                    List<FileResource> lstresources = _iFileResourceRepository.GetFileResourceByNewsFileId(newsFile.NewsFileId);
                    newsFile.Resources = new List<FileResource>();
                    newsFile.Resources.CopyFrom(lstresources);
                }

               
                if (newsFile.lstCategory == null)
                {
                    Category cat = new Category();

                    if (newsFile.CategoryId.HasValue)
                        cat = _iCategoryRepository.GetCategory(newsFile.CategoryId.Value);
                    if (cat != null)
                    {
                        newsFile.lstCategory = new List<Category>();
                        newsFile.lstCategory.Add(cat);
                    }
                }

                if (newsFile.lstLocation == null)
                {
                    Location loc = new Location();

                    if (newsFile.LocationId.HasValue)
                        loc = _iLocationRepository.GetLocation(newsFile.LocationId.Value);
                    if (loc != null)
                    {
                        newsFile.lstLocation = new List<Location>();
                        newsFile.lstLocation.Add(loc);
                    }
                }

                if (newsFile.lstTag == null)
                {   
                    List<FileTag> filetaglst = new List<FileTag>();
                    filetaglst = _iFileTagRepository.GetFileTagByNewsFileId(newsFile.NewsFileId);

                    newsFile.lstTag = new List<Tag>();

                    if (filetaglst != null)
                    {
                        for (int i = 0; i <= filetaglst.Count - 1; i++)
                        {
                            Tag tags = _iTagRepository.GetTag(filetaglst[i].TagId);
                            newsFile.lstTag.Add(tags);
                        }
                    }
                }

                string result = AddResourceMeta(newsFile);
            }

            #endregion

            #region Filter
            List<NewsFileFilter> lstNewsFileFilter = new List<NewsFileFilter>();
            if (postInput.ProgramId == 0 && postInput.FilterTypeId != (int)FilterTypes.SocialMedia && postInput.FilterTypeId != (int)FilterTypes.Event)
            {
                var allNewsFilter = _iFilterRepository.GetFilterByFilterTypeId((int)FilterTypes.AllNews).FirstOrDefault();
                lstNewsFileFilter.Add(getNewsFilter(allNewsFilter, newsFile.NewsFileId, dtNow));

                //var TopRatedFilter = _iFilterRepository.GetFilter((int)NewsFilters.TopRated);
                //lstNewsFileFilter.Add(getNewsFilter(TopRatedFilter, newsFile.NewsFileId, dtNow));
            }
            else
            {
                Program pg = _iProgramRepository.GetProgram(postInput.ProgramId);
                if (pg != null)
                {
                    Filter filter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.Program, pg.Name);
                    if (filter == null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = pg.Name;
                        filter.FilterTypeId = (int)FilterTypes.Program;
                        filter.Value = postInput.ProgramId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filter.FilterTypeId).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);

                        lstNewsFileFilter.Add(getNewsFilter(filter, newsFile.NewsFileId, dtNow));


                    }
                    else
                    {
                        lstNewsFileFilter.Add(getNewsFilter(filter, newsFile.NewsFileId, dtNow));
                    }
                }
            }
            if (postInput.MarkNewsVerified == null)
            {
                var notVerifiedFilter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsFilter, "NotVerified");
                lstNewsFileFilter.Add(getNewsFilter(notVerifiedFilter, newsFile.NewsFileId, dtNow));
            }

            else if (!isSameNews)
            {
                var notVerifiedFilter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsFilter, postInput.MarkNewsVerified.IsVerified ? "Verified" : "Rejected");
                lstNewsFileFilter.Add(getNewsFilter(notVerifiedFilter, newsFile.NewsFileId, dtNow));
            }


            if (postInput.FilterTypeId == (int)FilterTypes.Channel)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, postInput.ChannelId, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Radio)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, postInput.RadioStationId, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.NewsPaper)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, postInput.NewsPaperId, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Program)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, postInput.ProgramId, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.SocialMedia)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, 0, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.FieldReporter)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, postInput.BureauId.Value, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Website)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, 0, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Wire)
            {
                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, 0, postInput.Source, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Event)
            {
                if (string.IsNullOrEmpty(postInput.Source))
                    postInput.Source = FilterTypes.Event.ToString();

                AddSourceFilter(newsFile, lstNewsFileFilter, postInput.FilterTypeId, 0, postInput.Source, dtNow);
            }


            if (postInput.CategoryIds != null && postInput.CategoryIds.Count > 0)
            {
                for (int i = 0; i <= postInput.CategoryIds.Count - 1; i++)
                {
                    var category = _iCategoryRepository.GetCategory(postInput.CategoryIds[i]);
                    if (category != null)
                    {
                        Filter catfilter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.Category, category.Category);

                        if (catfilter == null)
                        {
                            List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                            catfilter = new Filter();
                            catfilter.Name = category.Category.ToLower();
                            catfilter.FilterTypeId = (int)FilterTypes.Category;
                            catfilter.Value = category.CategoryId;
                            catfilter.CreationDate = dtNow;
                            catfilter.LastUpdateDate = catfilter.CreationDate;
                            var pFilter = parentFilters.Where(x => x.Name == "Others").FirstOrDefault();
                            if (pFilter != null)
                            {
                                catfilter.ParentId = pFilter.FilterId;
                                lstNewsFileFilter.Add(getNewsFilter(pFilter, newsFile.NewsFileId, dtNow));
                            }
                            catfilter.IsActive = true;
                            catfilter.IsApproved = false;
                            catfilter = _iFilterRepository.InsertFilter(catfilter);
                            
                        }

                        lstNewsFileFilter.Add(getNewsFilter(catfilter, newsFile.NewsFileId, dtNow));

                    }

                }
            }
            

            #region Ticker Filters Marking
            //int CheckTelevisionNews = String.IsNullOrEmpty(postInput.Description) ? 0 : 1;
            //int CheckNewsPaperNews = String.IsNullOrEmpty(postInput.NewsPaperdescription) ? 0 : 1;
            //int CheckTickerNews = (postInput.Tickers != null && postInput.Tickers.Count > 0) ? 1 : 0;


            //var NewsTelevisionFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.ReportedNews, "Television News");
            //var NewsNewsPaperFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.ReportedNews, "NewsPaper News");
            //var NewsTickerFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.ReportedNews, "Ticker News");

            //if (CheckTelevisionNews > 0 && postInput.FilterTypeId != (int)FilterTypes.SocialMedia)
            //    lstNewsFileFilter.Add(getNewsFilter(NewsTelevisionFitler, newsFile.NewsFileId, dtNow));
            //if (CheckNewsPaperNews > 0)
            //    lstNewsFileFilter.Add(getNewsFilter(NewsNewsPaperFitler, newsFile.NewsFileId, dtNow));
            //if (CheckTickerNews > 0)
            //    lstNewsFileFilter.Add(getNewsFilter(NewsTickerFitler, newsFile.NewsFileId, dtNow));

            #endregion

            #region Insert NewsFileFilter

            if (lstNewsFileFilter != null && lstNewsFileFilter.Count > 0)
            {
                if(newsFile.NewsFileId > 0)
                _iNewsFileFilterRepository.DeleteNewsFileFilterByNewsFileId(newsFile.NewsFileId);
                
                for (int i = 0; i <= lstNewsFileFilter.Count - 1; i++)
                {
                    _iNewsFileFilterRepository.InsertNewsFileFilter(lstNewsFileFilter[i]);
                }
            }

            #endregion

            #endregion
            
            #region Organization

            if (postInput.Organizations != null)
            {
                if (newsFile.NewsFileId > 0)
                    _iNewsFileOrganizationRepository.DeleteNewsFileOrganizationByNewsFileId(newsFile.NewsFileId);

                for (int j = 0; j < postInput.Organizations.Count; j++)
                {

                    Organization org = _iOrganizationRepository.GetOrganizationByName(postInput.Organizations[j]);
                    if (org == null)
                    {
                        org = new Organization();
                        org.CreationDate = DateTime.UtcNow;
                        org.LastUpdateDate = DateTime.UtcNow;
                        org.IsActive = true;
                        org.IsApproved = false;
                        org.Name = postInput.Organizations[j];

                        org = _iOrganizationRepository.InsertOrganization(org);
                    }

                    NewsFileOrganization orgfile = new NewsFileOrganization();
                    orgfile.CreationDate = newsFile.LastUpdateDate;
                    orgfile.LastUpdateDate = newsFile.LastUpdateDate;
                    orgfile.NewsFileId = newsFile.NewsFileId;
                    orgfile.OrganizationId = org.OrganizationId;
                    orgfile.IsActive = true;

                    orgfile = _iNewsFileOrganizationRepository.InsertNewsFileOrganization(orgfile);
                }
            }
            #endregion
            
            #region Add Event Reporter Mapping

            if (postInput.FilterTypeId == (int)FilterTypes.Event)
            {
                if(postInput.EventReporterId != null && postInput.EventReporterId.Count > 0)
                {
                    if(newsFile.NewsFileId > 0)
                     _iEventReporterRepository.DeleteEventReporterByNewsFileId(newsFile.NewsFileId);

                    EventReporter reporter = new EventReporter();
                    for (int i = 0; i <= postInput.EventReporterId.Count - 1; i++)
                    {
                        reporter = new EventReporter();
                        reporter.CreationDate = dtNow;
                        reporter.LastUpdateDate = dtNow;
                        reporter.IsActive = true;
                        reporter.NewsFileId = newsFile.NewsFileId;
                        reporter.ReporterId = postInput.EventReporterId[i];

                        _iEventReporterRepository.InsertEventReporter(reporter);
                    }
                }   
            }
            #endregion

            #region Add Event Resource Mapping

            if (postInput.FilterTypeId == (int)FilterTypes.Event)
            {
                if (postInput.EventResourceId != null && postInput.EventResourceId.Count > 0)
                {
                    if(newsFile.NewsFileId > 0)
                    _iEventResourceRepository.DeleteEventResourceByNewsFileId(newsFile.NewsFileId);

                    EventResource resource = new EventResource();
                    for (int i = 0; i <= postInput.EventResourceId.Count - 1; i++)
                    {
                        resource = new EventResource();
                        resource.CreationDate = dtNow;
                        resource.LastUpdateDate = dtNow;
                        resource.IsActive = true;
                        resource.NewsFileId = newsFile.NewsFileId;
                        resource.EventResourceType = postInput.EventResourceId[i];

                        _iEventResourceRepository.InsertEventResource(resource);
                    }
                }
            }
            #endregion
            
            return newsFile;
        }

        public NewsFileHistory InsertNewsHistory(NMS.Core.DataTransfer.News.PostInput input)
        {
            NewsFileHistory filehistory = new NewsFileHistory();
            var newsfile = GetNews(input.NewsFileId.ToString());

            if (newsfile != null)
            {
                string json = new JavaScriptSerializer().Serialize(newsfile);
                if (!string.IsNullOrEmpty(json))
                {
                    NewsFileHistory history = new NewsFileHistory();
                    history.CreationDate = DateTime.UtcNow;
                    history.NewsFileId = input.NewsFileId;
                    history.NewsFileJson = json;

                    filehistory = _iNewsFileHistoryRepository.InsertNewsFileHistory(history);
                }
            }
            return filehistory;
        }

        public NewsFile UpdateNewsFileWithHistory(NMS.Core.DataTransfer.News.PostInput postInput)
        {
            NewsFile newsfile = new NewsFile();

            if (postInput != null)
            {
                newsfile = InsertNews(postInput);
            }
            return newsfile;
        }

        

        private NewsFileFilter getNewsFilter(Filter filter, int newsId, DateTime dtNow)
        {
            NewsFileFilter newsFilter = new NewsFileFilter();
            newsFilter.CreationDate = dtNow;
            newsFilter.LastUpdateDate = dtNow;
            newsFilter.NewsFileId = newsId;
            newsFilter.FilterId = filter.FilterId;
            newsFilter.IsActive = true;
            return newsFilter;
        }

        private void AddSourceFilter(NewsFile newsFile, List<NewsFileFilter> lstnewsfilefilter, int filterTypeId, int id, string name, DateTime dtNow)
        {
            Filter filter = (id>0)?_iFilterRepository.GetFilterByFilterTypeAndValue(filterTypeId, id): _iFilterRepository.GetFilterByNameAndFilterType((FilterTypes)filterTypeId, name);
            if (filter == null)
            {
                if (filterTypeId == (int)FilterTypes.Channel)
                {
                    var channel = _iChannelRepository.GetChannel(id);
                    if (channel != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = channel.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = channel.ChannelId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId && x.ParentId == null).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else if (filterTypeId == (int)FilterTypes.Radio)
                {
                    var radio = _iRadioStationRepository.GetRadioStation(id);
                    if (radio != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = radio.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = radio.RadioStationId;

                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId && x.ParentId == null).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else if (filterTypeId == (int)FilterTypes.NewsPaper)
                {
                    var newsPaper = _iNewsPaperRepository.GetNewsPaper(id);
                    if (newsPaper != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = newsPaper.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = newsPaper.NewsPaperId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId && x.ParentId == null).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else if (filterTypeId == (int)FilterTypes.Program)
                {
                    var program = _iProgramRepository.GetProgram(id);
                    if (program != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = program.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = program.ProgramId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId && x.ParentId == null).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else if (filterTypeId == (int)FilterTypes.Website || filterTypeId == (int)FilterTypes.Wire || filterTypeId == (int)FilterTypes.SocialMedia)
                {
                    List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                    filter = new Filter();
                    filter.Name = name;
                    filter.FilterTypeId = filterTypeId;
                    filter.CreationDate = dtNow;
                    filter.LastUpdateDate = filter.CreationDate;
                    var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId && x.ParentId == null).FirstOrDefault();
                    if (pFilter != null)
                        filter.ParentId = pFilter.FilterId;
                    filter.IsActive = true;
                    filter.IsApproved = true;
                    filter = _iFilterRepository.InsertFilter(filter);
                }
                else if (filterTypeId == (int)FilterTypes.FieldReporter)
                {
                    var location = _iLocationRepository.GetLocation(id);
                    List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                    filter = new Filter();
                    filter.Name = location.Location;
                    filter.FilterTypeId = filterTypeId;
                    filter.Value = location.LocationId;
                    filter.CreationDate = dtNow;
                    filter.LastUpdateDate = filter.CreationDate;
                    var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId && x.ParentId == null).FirstOrDefault();
                    if (pFilter != null)
                        filter.ParentId = pFilter.FilterId;
                    filter.IsActive = true;
                    filter.IsApproved = true;
                    filter = _iFilterRepository.InsertFilter(filter);
                }
                else if (filterTypeId == (int)FilterTypes.Event)
                {
                    List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                    filter = new Filter();
                    filter.Name = name;
                    filter.FilterTypeId = filterTypeId;
                    filter.CreationDate = dtNow;
                    filter.LastUpdateDate = filter.CreationDate;
                    var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId && x.ParentId == null).FirstOrDefault();
                    if (pFilter != null)
                        filter.ParentId = pFilter.FilterId;
                    filter.IsActive = true;
                    filter.IsApproved = true;
                    filter = _iFilterRepository.InsertFilter(filter);
                }
                else
                {
                    var location = _iLocationRepository.GetLocation(id);
                    if (location != null)
                    {
                        filter = InsertLocationFilters(location, dtNow, filterTypeId);
                    }
                }
            }
            newsFile.SourceTypeId = filter.FilterTypeId;
            newsFile.SourceFilterId = filter.FilterId;
            newsFile.Source = filter.Name;
            lstnewsfilefilter.Add(getNewsFilter(filter, newsFile.NewsFileId, dtNow));
            if (filter.ParentId.HasValue)
            {
                var parentFilter = _iFilterRepository.GetFilter(filter.ParentId.Value);
                if (parentFilter != null)
                {
                    lstnewsfilefilter.Add(getNewsFilter(parentFilter, newsFile.NewsFileId, dtNow));
                }
            }
        }

        private MNews TranslateNews(MNews mongoNews, string NewsPaperDesc = "") //translate
        {
            Translator NewsTranslater = new Translator();
            mongoNews.TranslatedTitle = NewsTranslater.Translate(mongoNews.Title, mongoNews.LanguageCode, "en");
            if (!String.IsNullOrEmpty(mongoNews.Slug))
                mongoNews.TranslatedSlug = NewsTranslater.Translate(mongoNews.Slug, mongoNews.LanguageCode, "en");
            if (!String.IsNullOrEmpty(mongoNews.Description))
                mongoNews.TranslatedDescription = NewsTranslater.Translate(mongoNews.Description, mongoNews.LanguageCode, "en");
            else
                mongoNews.TranslatedDescription = NewsTranslater.Translate(NewsPaperDesc, mongoNews.LanguageCode, "en");
            mongoNews.DescriptionText = Regex.Replace(mongoNews.TranslatedDescription, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase);
            return mongoNews;
        }

        private string TranslateNews(FileDetail detail, string LanguageCode, string NewsPaperDesc = "") //translate
        {
            Translator NewsTranslater = new Translator();
            if (!String.IsNullOrEmpty(detail.Text))
                detail.DescriptionText = NewsTranslater.Translate(detail.Text, LanguageCode, "en");
            return detail.DescriptionText;
        }

        private Filter InsertLocationFilters(Location location, DateTime dtNow, int filterTypeId)
        {
            List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
            Filter filter = new Filter();
            filter.Name = location.Location;


            filter.FilterTypeId = filterTypeId;
            filter.Value = location.LocationId;
            filter.CreationDate = dtNow;
            filter.LastUpdateDate = filter.CreationDate;
            var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId).FirstOrDefault();
            if (pFilter != null)
                filter.ParentId = pFilter.FilterId;
            filter.IsActive = true;
            filter.IsApproved = true;
            filter = _iFilterRepository.InsertFilter(filter);
            if (location.ParentId.HasValue)
            {
                var parentLocation = _iLocationRepository.GetLocation(location.ParentId.Value);
                if (parentLocation != null)
                {
                    Filter parentFilter = new Filter();
                    parentFilter.Name = location.Location;
                    parentFilter.FilterTypeId = filterTypeId;
                    parentFilter.Value = location.LocationId;
                    parentFilter.CreationDate = dtNow;
                    if (pFilter != null)
                        parentFilter.ParentId = pFilter.FilterId;
                    parentFilter.LastUpdateDate = parentFilter.CreationDate;
                    parentFilter.IsActive = true;
                    parentFilter.IsApproved = true;
                    parentFilter = _iFilterRepository.InsertFilter(parentFilter);
                }
            }
            return filter;
        }

        public List<MNews> GetNewsByReporterId(int rId, int pageCount, int startIndex)
        {
            DateTime dt = DateTime.Now.AddDays(-3);
            var newsFiles = _iNewsFileRepository.GetReporterNewsFile(rId, dt, startIndex);
            List<MNews> newsList = new List<MNews>();
            if(newsFiles != null)
            {
                foreach (var file in newsFiles)
                {
                    newsList.Add(ConvertNewsFileToMNews(file));
                }
            }
            return newsList;
        }

        public List<MNews> MyNewsPolling(PollingObject obj)
        {
            List<NewsFile> newsFiles = new List<NewsFile>();

            if (obj.LastUpdateDate.Year > 1)
                newsFiles = _iNewsFileRepository.GetNewsFileAfterDateTime(obj.LastUpdateDate, obj.AssignedTo);

            else if (!string.IsNullOrEmpty(obj.From) && !string.IsNullOrEmpty(obj.To))
            {
                if (string.IsNullOrEmpty(obj.SearchText))
                    newsFiles = _iNewsFileRepository.GetNewsFileWithFilterFromTo(obj.From, obj.To, obj.AssignedTo);

                else
                {

                    // search in filedetails description
                    var fileDetails = _iFileDetailRepository.GetFileDetailWithFilterFromToAndSearch(obj.From, obj.To, obj.SearchText);
                    // search in newsfile slug
                    var newFiles = _iNewsFileRepository.GetNewsFileWithFilterFromToAndSearch(obj.From, obj.To, obj.SearchText, obj.AssignedTo);

                    if (newFiles != null)
                    {
                        newsFiles.AddRange(newFiles);
                    }

                    if(fileDetails != null)
                    {
                        foreach(var fd in fileDetails)
                        {
                            var alreadyExist = newsFiles.Where(x => x.NewsFileId == fd.NewsFileId).FirstOrDefault();
                            if (alreadyExist == null)
                            {
                                var newsFromFileDetail = _iNewsFileRepository.GetNewsFile(fd.NewsFileId);
                                if (newsFromFileDetail != null && ((newsFromFileDetail.AssignedTo.HasValue && newsFromFileDetail.AssignedTo.Value == obj.AssignedTo) || newsFromFileDetail.CreatedBy == obj.AssignedTo))
                                {
                                    newsFiles.Add(newsFromFileDetail);
                                }
                            }
                        }
                    }
                    
                }
                    
            }
            
            
            List<MNews> newsList = new List<MNews>();
            if (newsFiles != null && newsFiles.Count > 0)
            {
                foreach (var file in newsFiles)
                {
                    newsList.Add(ConvertNewsFileToMNews(file));
                }
            }
            return newsList;
        }

        public MNews ConvertNewsFileToMNews(NewsFile file)
        {
            MNews news = new MNews();
            FileDetail fileDetail = new FileDetail();

            var fDetail = _iFileDetailRepository.GetFileDetailByNewsFileId(file.NewsFileId);
            if(fDetail!= null)
            {
                fileDetail= fDetail.LastOrDefault();
            }

            INewsResourceEditRepository _iresourceedit = IoC.Resolve<INewsResourceEditRepository>("NewsResourceEditRepository");
            var fileResources = _iFileResourceRepository.GetFileResourceByNewsFileId(file.NewsFileId);
            var fileCategories = _iFileCategoryRepository.GetFileCategoryByNewsFileId(file.NewsFileId);
            var fileResourceEdit = _iresourceedit.GetNewsResourceEditByNewsId(file.NewsFileId);
            //var fileLocations = _iFileLocationRepository.GetFileLocationByNewsFileId(file.NewsFileId);
            news.Filters = new List<Core.Entities.Mongo.MNewsFilter>();
            news.BunchGuid = Guid.NewGuid().ToString();
            news.Highlight = file.Highlights;
            news.CreationDate = file.CreationDate;
            news.Description = fileDetail != null ? fileDetail.Text : string.Empty;
            news.LanguageCode = file.LanguageCode;
            news.NewsTypeId = 1;
            news.PublishTime = file.PublishTime;
            news.ReporterId = fileDetail != null ? fileDetail.CreatedBy : file.CreatedBy;
            news.ShortDescription = news.Description;
            news.Slug = fileDetail != null ? fileDetail.Slug : file.Slug;
            news.SlugId = file.SlugId.HasValue ? file.SlugId.Value : 0;
            news.Title = file.Title;
            news.LastUpdateDate = file.LastUpdateDate;
            news.Source = file.Source;
            news._id = file.NewsFileId.ToString();

            news.Categories = new List<Category>();
            if (fileCategories != null)
            {
                foreach (var filecategory in fileCategories)
                {
                    ICategoryService iCategoryService = IoC.Resolve<ICategoryService>("CategoryService");
                    var category = iCategoryService.GetCategory(filecategory.CategoryId);
                    news.Categories.Add(category);
                }
            }
            news.Resources = new List<MResource>();
            if (fileResources != null)
            {
                foreach (var fileResource in fileResources)
                {
                    MResource resource = new MResource();
                    resource.Guid = fileResource.Guid.ToString();
                    resource.ResourceTypeId = fileResource.ResourceTypeId;
                    news.Resources.Add(resource);
                }
            }
            news.ResourceEdit = new ResourceEdit();
            if(fileResourceEdit != null)
            {
                foreach (var fileResEdit in fileResourceEdit)
                {
                    NMS.Core.Models.ResourceEdit resourceEdit = new NMS.Core.Models.ResourceEdit();
                    news.ResourceEdit.ResourceGuid = fileResEdit.ResourceGuid.Value;
                    news.ResourceEdit.Id = fileResEdit.NewsResourceEditId;
                    news.ResourceEdit.Url = fileResEdit.Url;
                    FromTo fTo = new FromTo();
                    fTo.From = fileResEdit.Left.Value;
                    fTo.To = fileResEdit.Right.Value;
                    news.ResourceEdit.FromTos = new List<FromTo>();
                    news.ResourceEdit.FromTos.Add(fTo);
                }
            }

            
            return news;
        }

        public MNews GetNews(string id)
        {
            var file = _iNewsFileRepository.GetNewsFile(Convert.ToInt32(id));

            if(file != null)
            {

                List<MNews> newsList = new List<MNews>();
                MNews news = new MNews();
                INewsResourceEditRepository _iresourceedit = IoC.Resolve<INewsResourceEditRepository>("NewsResourceEditRepository");
                var fileDetail = _iFileDetailRepository.GetFileDetailByNewsFileId(file.NewsFileId);
                var fileResources = _iFileResourceRepository.GetFileResourceByNewsFileId(file.NewsFileId);
                var fileResourceEdit = _iresourceedit.GetNewsResourceEditByNewsId(file.NewsFileId);
                var fileCategories = _iFileCategoryRepository.GetFileCategoryByNewsFileId(file.NewsFileId);
                var fileLocations = _iFileLocationRepository.GetFileLocationByNewsFileId(file.NewsFileId);
                var fileTags = _iFileTagRepository.GetFileTagByNewsFileId(file.NewsFileId);
                var fileorganization = _iNewsFileOrganizationRepository.GetNewsFileOrganizationByNewsFileId(file.NewsFileId);
                var editorialcomments = _iEditorialCommentRepository.GetEditorialCommentByNewsFileId(file.NewsFileId);
                var eventreporter = _iEventReporterRepository.GetEventReporterByNewsFileId(file.NewsFileId);
                var eventresource = _iEventResourceRepository.GetEventResourceByNewsFileId(file.NewsFileId);


                FileDetail detail = new FileDetail();

                if (fileDetail != null)
                    detail = fileDetail.Last();

                news.Filters = new List<Core.Entities.Mongo.MNewsFilter>();
                news.BunchGuid = Guid.NewGuid().ToString();
                news.CreationDate = file.CreationDate;
                news.Description = detail.Text;
                news.LanguageCode = file.LanguageCode;
                news.NewsTypeId = 1;
                news.PublishTime = file.PublishTime;
                news.IsVerified = file.IsVerified;
                news.NewsStatus = file.NewsStatus;
                news.ReporterId = detail.CreatedBy;
                news.ShortDescription = news.Description;
                news.Highlight = file.Highlights;
                news.Slug = file.Slug;
                news.SlugId = file.SlugId.HasValue ? file.SlugId.Value : 0;
                news.Title = file.Title;
                news._id = file.NewsFileId.ToString();
                news.ProgramId = file.ProgramId;
                news.AssignedTo = file.AssignedTo;
                news.Source = file.Source;
                news.ResourceGuid = file.ResourceGuid;
                news.Coverage = file.Coverage;
                news.EventType = file.EventType;

                if (file.SourceTypeId.HasValue)
                    news.SourceTypeId = file.SourceTypeId.Value;

                if (file.TaggedBy.HasValue)
                    news.TaggedBy = file.TaggedBy;

                if (file.InsertedBy.HasValue)
                    news.InsertedBy = file.InsertedBy;
                
                if (news.EventType.HasValue)
                {
                    EventType Etype = _iEventTypeRepository.GetEventType(news.EventType.Value);
                    if (Etype != null) { news.EventTypeName = Etype.Name; }
                }

                if (file.ReporterBureauId.HasValue)
                {
                    news.ReporterBureauId = file.ReporterBureauId.Value;

                    var loc = _iLocationRepository.GetLocation(file.ReporterBureauId.Value);
                    if(loc != null)
                    {
                        news.BureauLocation = loc.Location;
                    }
                }
                
                news.Organization = new List<Organization>();
                if (fileorganization != null)
                {
                    foreach (var org in fileorganization)
                    {
                        IOrganizationService iOrganizationService = IoC.Resolve<IOrganizationService>("OrganizationService");
                        var organization = iOrganizationService.GetOrganization(org.OrganizationId);
                        news.Organization.Add(organization);
                    }
                }

                news.Categories = new List<Category>();
                if (fileCategories != null)
                {
                    foreach (var filecategory in fileCategories)
                    {
                        ICategoryService iCategoryService = IoC.Resolve<ICategoryService>("CategoryService");
                        var category = iCategoryService.GetCategory(filecategory.CategoryId);
                        news.Categories.Add(category);
                    }
                }
                news.Locations = new List<Location>();
                if (fileLocations != null)
                {
                    foreach (var filelocation in fileLocations)
                    {
                        ILocationService iLocationService = IoC.Resolve<ILocationService>("LocationService");
                        var location = iLocationService.GetLocation(filelocation.LocationId);
                        news.Locations.Add(location);
                    }
                }
                news.Resources = new List<MResource>();
                if (fileResources != null)
                {
                    foreach (var fileResource in fileResources)
                    {
                        MResource resource = new MResource();
                        resource.Guid = fileResource.Guid.ToString();
                        resource.ResourceTypeId = fileResource.ResourceTypeId;
                        news.Resources.Add(resource);
                    }
                }
                news.Tags = new List<Core.Entities.Mongo.MTag>();
                if (fileTags != null)
                {
                    foreach (var fileTag in fileTags)
                    {
                        var newstag = _iTagRepository.GetTag(fileTag.TagId);
                        if (newstag != null && fileTag.Rank == 0)
                        {
                            MTag cTag = new MTag();
                            cTag.Tag = newstag.Tag;
                            cTag.Rank = newstag.Rank;
                            news.Tags.Add(cTag);
                        }
                    }
                }
                news.ResourceEdit = new ResourceEdit();
                if (fileResourceEdit != null)
                {
                    foreach (var fileResEdit in fileResourceEdit)
                    {
                        NMS.Core.Models.ResourceEdit resourceEdit = new NMS.Core.Models.ResourceEdit();
                        news.ResourceEdit.ResourceGuid = fileResEdit.ResourceGuid.Value;
                        news.ResourceEdit.Id = fileResEdit.NewsResourceEditId;
                        news.ResourceEdit.Url = fileResEdit.Url;
                        FromTo fTo = new FromTo();
                        fTo.From = fileResEdit.Left.Value;
                        fTo.To = fileResEdit.Right.Value;
                        news.ResourceEdit.FromTos = new List<FromTo>();
                        news.ResourceEdit.FromTos.Add(fTo);
                    }
                }

                news.EditorialComments = new List<EditorialComment>();
                {
                    if (editorialcomments != null)
                    {
                        news.EditorialComments = editorialcomments;
                    }
                }

                news.EventReporter = new List<EventReporter>();
                {
                    if(eventreporter != null)
                    {
                        news.EventReporter = eventreporter;
                    }
                }

                news.EventResource = new List<EventResource>();
                {
                    if (eventresource != null)
                    {
                        news.EventResource = eventresource;
                    }
                }

                return news;
            }

            return null;
            
        }

        //public List<NewsFile> GetNewsFileForProducer(List<Filter> filters, DateTime newsLastUpdateDate, bool includeProgramRelatedNews, string term, string PageOffset, string PageNumber)
        //{
        //    var categories = _iCategoryRepository.GetAllCategory();
        //    List<NewsFile> news = _iNewsFileRepository.GetNewsFileForProducer(filters, newsLastUpdateDate, includeProgramRelatedNews, term, PageOffset, PageNumber);
        //    if (news != null && categories != null)
        //    {
        //        foreach (var n in news)
        //        {
        //            if (n.CategoryId.HasValue)
        //                n.Category = categories.First(x => x.CategoryId == n.CategoryId.Value).Category;
        //        }
        //    }
        //    return news;
        //}

        public List<NewsFile> GetNewsFileWithFilterFromTo(List<Filter> filters, DateTime From, int[] folderids, string term, int PageOffset, int PageSize, DateTime? To)
        {
            var categories = _iCategoryRepository.GetAllCategory();
            DateTime dtNow = DateTime.UtcNow;
            List<NewsFile> news = _iNewsFileRepository.GetNewsFileWithFilterFromTo(filters, From, folderids, term, PageOffset, PageSize, To);
            var lstnewsfile = news.Where(x => x.ParentId == null && x.ProgramId != null && (x.SourceTypeId == (int)FilterTypes.FieldReporter || x.SourceTypeId == (int)FilterTypes.Channel)).ToList();
            System.Diagnostics.Debug.WriteLine("GetNewsFileWithFilterFromTo=>" + DateTime.UtcNow.Subtract(dtNow).TotalMilliseconds);
            if(lstnewsfile != null && lstnewsfile.Count > 0)
            {
                for(int i = 0; i <= lstnewsfile.Count - 1; i++)
                {
                    news.Remove(lstnewsfile[i]);
                }
            }

            //if (news != null && categories != null)
            //{
            //    foreach (var n in news)
            //    {
            //        if (n.CategoryId.HasValue)
            //            n.Category = categories.First(x => x.CategoryId == n.CategoryId.Value).Category;
            //    }
            //}
            dtNow = DateTime.UtcNow;
            //if (news != null)
            //{
            //    foreach(var n in news)
            //    {
            //        if(n.ReporterBureauId.HasValue)
            //        {
            //            var loc = _iLocationRepository.GetLocation(n.ReporterBureauId.Value);
            //            if(loc!= null)
            //            {
            //                n.BureauLocation = loc.Location;
            //            }

            //        }
            //    }
            //}
            System.Diagnostics.Debug.WriteLine("GetLocation=>" + DateTime.UtcNow.Subtract(dtNow).TotalMilliseconds);
            return news;
        }

        public List<NewsFile> GetSocialMediaNewsFileWithFilterFromTo(List<Filter> filters, DateTime from, bool includeProgramRelatedNews, string term, string PageOffset, string PageSize, DateTime? to, string SocialMediaFilterId)
        {
            var categories = _iCategoryRepository.GetAllCategory();
            List<NewsFile> news = _iNewsFileRepository.GetSocialMediaNewsFileWithFilterFromTo(filters, from, includeProgramRelatedNews, term, PageOffset, PageSize, to, SocialMediaFilterId);
            if (news != null && categories != null)
            {
                foreach (var n in news)
                {
                    if (n.CategoryId.HasValue)
                        n.Category = categories.First(x => x.CategoryId == n.CategoryId.Value).Category;
                }
            }
            return news;
        }
        
        public bool CheckIfNewsExistsSQL(string title, string source)
        {
            return _iNewsFileRepository.CheckIfNewsExistsSQL(title, source);
            //return _iMNewsRepository.CheckIfNewsExists(entity);
        }

        public List<NewsFile> GetNewsFileByProgramIdwithstatus(int ProgramId, string SelectClause = null)
        {
            return _iNewsFileRepository.GetNewsFileByProgramIdwithstatus(ProgramId, SelectClause);
        }

        public List<NewsFile> GetNewsFilesByFolderId(int FolderId, int StoryCount)
        {
            List<NewsFile>  NewsFiles= _iNewsFileRepository.GetNewsFilesByFolderId(FolderId, StoryCount);
            List<FileDetail> NewsFilesDetails = new List<FileDetail>();
            if (NewsFiles !=null)
            {
                NewsFilesDetails =_iFileDetailRepository.getNewsFileDetailsByNewsFileIds(FolderId,StoryCount);
                for (int i = 0; i < NewsFiles.Count; i++)
                {
                    NewsFiles[i].FileDetails = new List<FileDetail>();
                    NewsFiles[i].FileDetails.Add(NewsFilesDetails.Where(x => x.NewsFileId == NewsFiles[i].NewsFileId).OrderByDescending(x => x.LastUpdateDate).FirstOrDefault());//.FirstOrDefault();
                }
            }
            
            return NewsFiles;
        }
        
        public NewsFile OrganizationTagsUpdate(NMS.Core.DataTransfer.News.PostInput input)
        {
            DateTime dtNow = DateTime.UtcNow;
            if (input != null)
            {
                if (input.NewsFileId > 0)
                {
                    NewsFile file = _iNewsFileRepository.GetNewsFile(input.NewsFileId);

                    if (file != null)
                    {
                        #region Category & Location

                        if (input.CategoryIds != null && input.CategoryIds.Count > 0)
                        {
                            file.CategoryId = input.CategoryIds.First();

                            #region FileCategories
   
                                if (file.NewsFileId > 0)
                                    _iFileCategoryRepository.DeleteFileCategoryByNewsFileId(file.NewsFileId);


                            if (file.CategoryId.HasValue)
                            {
                                FileCategory category = new FileCategory();
                                category.CategoryId = file.CategoryId.Value;
                                category.CreationDate = dtNow;
                                category.NewsFileId = file.NewsFileId;
                                _iFileCategoryRepository.InsertFileCategory(category);
                            }
                            
                            #endregion Categories
                            
                        }

                        if (input.LocationIds != null && input.LocationIds.Count > 0)
                        {
                            file.LocationId = input.LocationIds.First();

                            #region FileLocation
                                
                                if (file.NewsFileId > 0)
                                    _iFileLocationRepository.DeleteFileLocationByNewsFileId(file.NewsFileId);

                            if (file.LocationId.HasValue)
                            {
                                FileLocation location = new FileLocation();
                                location.LocationId = file.LocationId.Value;
                                location.CreationDate = dtNow;
                                location.NewsFileId = file.NewsFileId;
                                _iFileLocationRepository.InsertFileLocation(location);
                            }


                            #endregion FileLocation

                        }

                        if (file.LocationId.HasValue)
                        {
                            if (file.SourceTypeId.HasValue)
                            {
                                if (file.SourceTypeId.Value == (int)FilterTypes.FieldReporter)
                                {
                                    var loc = _iLocationRepository.GetLocation(file.LocationId.Value);
                                    file.Source = loc.Location;
                                }
                            }

                        }

                        #endregion

                        file.LastUpdateDate = DateTime.UtcNow;
                        file.PublishTime = input.NewsDate;
                        file.Slug = input.Slug;
                        file.IsTagged = true;
                        file = _iNewsFileRepository.UpdateNewsFile(file);
                    }

                    #region Tag
                    if (input.Tags != null)
                    {

                        _iFileTagRepository.DeleteFileTagByNewsFileIdRankAboveZero(file.NewsFileId);
                        input.Tags = input.Tags.Distinct().ToList();
                        foreach (var item in input.Tags)
                        {
                            FileTag tag = new FileTag();
                            tag.CreationDate = file.LastUpdateDate;
                            tag.NewsFileId = file.NewsFileId;
                            tag.LastUpdateDate = tag.CreationDate;
                            var lst = _iTagRepository.GetTagByTermExact(item.Tag);
                            if (lst == null || lst.Count == 0)
                            {
                                Tag _tag = new Tag();
                                _tag.CreationDate = file.LastUpdateDate;
                                _tag.Guid = Guid.NewGuid().ToString();
                                _tag.IsActive = true;
                                _tag.Rank = Convert.ToDouble(item.Rank);
                                _tag.LastUpdateDate = _tag.CreationDate;
                                _tag.Tag = item.Tag;
                                _tag = _iTagRepository.InsertTag(_tag);

                                tag.TagId = _tag.TagId;
                            }
                            else
                            {
                                tag.TagId = lst[0].TagId;
                            }

                            _iFileTagRepository.InsertFileTag(tag);
                        }

                    }
                    #endregion

                    #region Organization

                    if(input.Organizations != null)
                    {   
                        if (file.NewsFileId > 0)
                            _iNewsFileOrganizationRepository.DeleteNewsFileOrganizationByNewsFileId(file.NewsFileId);
                        
                        for (int j = 0; j < input.Organizations.Count; j++)
                        {

                            Organization org = _iOrganizationRepository.GetOrganizationByName(input.Organizations[j]);
                            if (org == null)
                            {
                                org = new Organization();
                                org.CreationDate = DateTime.UtcNow;
                                org.LastUpdateDate = DateTime.UtcNow;
                                org.IsActive = true;
                                org.IsApproved = false;
                                org.Name = input.Organizations[j];

                                org = _iOrganizationRepository.InsertOrganization(org);
                            }
                            
                                NewsFileOrganization orgfile = new NewsFileOrganization();
                                orgfile.CreationDate = file.LastUpdateDate;
                                orgfile.LastUpdateDate = file.LastUpdateDate;
                                orgfile.NewsFileId = file.NewsFileId;
                                orgfile.OrganizationId = org.OrganizationId;
                                orgfile.IsActive = true;
                            
                                orgfile = _iNewsFileOrganizationRepository.InsertNewsFileOrganization(orgfile);
                        }
                    }
                    #endregion
                    
                    #region Filter
                    NewsFileFilter filter = null;

                    var categoryfilter = _iNewsFileFilterRepository.GetNewsFileFilterOfNewsFileByFilterType(file.NewsFileId, (int)FilterTypes.Category);
                    if(categoryfilter != null)
                    {
                        _iNewsFileFilterRepository.DeleteNewsFileFilter(categoryfilter.NewsFileFilterId);
                    }

                    if (input.CategoryIds != null && input.CategoryIds.Count > 0)
                    {
                        for (int i = 0; i <= input.CategoryIds.Count - 1; i++)
                        {
                            var category = _iCategoryRepository.GetCategory(input.CategoryIds[i]);
                            if (category != null)
                            {
                                Filter catfilter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.Category, category.Category);

                                if (catfilter == null)
                                {
                                    List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                                    catfilter = new Filter();
                                    catfilter.Name = category.Category;
                                    catfilter.FilterTypeId = (int)FilterTypes.Category;
                                    catfilter.Value = category.CategoryId;
                                    catfilter.CreationDate = DateTime.UtcNow;
                                    catfilter.LastUpdateDate = catfilter.CreationDate;
                                    var pFilter = parentFilters.Where(x => x.Name == "Others").FirstOrDefault();
                                    if (pFilter != null)
                                    {
                                        catfilter.ParentId = pFilter.FilterId;
                                        filter = getNewsFilter(pFilter, file.NewsFileId, DateTime.UtcNow);
                                    }
                                    catfilter.IsActive = true;
                                    catfilter.IsApproved = false;
                                    catfilter = _iFilterRepository.InsertFilter(catfilter);

                                }
                                filter = getNewsFilter(catfilter, file.NewsFileId, DateTime.UtcNow);

                                if(filter!= null)
                                _iNewsFileFilterRepository.InsertNewsFileFilter(filter);
                            }

                        }
                    }
                    #endregion

                    return file;
                }

            }
            return null;
        }


        public NewsFile UpdateNewsFileCategoryLocationById(int NewsFileId,int CategoryId,int type)
        {
            if (type == (int)DescrepencyType.Category)
            {
                return _iNewsFileRepository.UpdateNewsFileCategoryById(NewsFileId, CategoryId);
            }
            else if (type == (int)DescrepencyType.Location)
            {
                return _iNewsFileRepository.UpdateNewsFileLocationById(NewsFileId, CategoryId);
            }
            else { return new NewsFile(); }
        }

        public bool UpdateProgramCountAndHistory(int programId)
        {
           return _iNewsFileRepository.UpdateProgramCountAndHistory(programId);
        }

        public string AddResourceMeta(NewsFile mongoNews)
        {
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = null;

            #region add Resource Mata
            try
            {
                foreach (FileResource mResource in mongoNews.Resources)
                {
                    MS.Core.DataTransfer.Resource.PostInput resourceMetaInput = new MS.Core.DataTransfer.Resource.PostInput();
                    var Category = (mongoNews.lstCategory == null || mongoNews.lstCategory.Count == 0) ? null : mongoNews.lstCategory.Where(x => x.ParentId == null).FirstOrDefault();
                    var Location = (mongoNews.lstLocation == null || mongoNews.lstLocation.Count == 0) ? null : mongoNews.lstLocation.Where(x => x.ParentId == null).FirstOrDefault();
                    //string Location = mongoNews.Locations.Where(x => x.ParentId == null).FirstOrDefault().Location;
                    string caption = mongoNews.LanguageCode == "en" ? mongoNews.Title : mongoNews.Slug;
                    resourceMetaInput.CopyFrom(mResource);
                    resourceMetaInput.Location = Location != null ? Location.Location : "";
                    resourceMetaInput.Category = Category != null ? Category.Category : "";
                    resourceMetaInput.Caption = caption;
                    resourceMetaInput.ResourceMeta = new List<KeyValuePair<string, string>>();
                    if (mongoNews.lstTag != null && mongoNews.lstTag.Count > 0)
                    {
                        mongoNews.lstTag = mongoNews.lstTag.OrderByDescending(x => x.Rank).Take(5).ToList();
                        foreach (NMS.Core.Entities.Tag mTag in mongoNews.lstTag)
                        {
                            resourceMetaInput.ResourceMeta.Add(new KeyValuePair<string, string>("Tag", mTag.Tag));
                        }
                    }
                    response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/AddResourceMeta", resourceMetaInput, null);
                    string status = string.Empty;
                    if (response.StatusCode == HttpStatusCode.OK)
                        status = "Success";
                    if (response != null) response.Close();

                }
                return "Success";
            }
            catch (Exception ex)
            {
                if (response != null) response.Close();
                throw ex;
            }
            #endregion

        }


    }

}
