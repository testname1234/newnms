﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsFilter;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities.Mongo;

namespace NMS.Service
{
		
	public class NewsFilterService : INewsFilterService 
	{
		private INewsFilterRepository _iNewsFilterRepository;
        private IMNewsFilterRepository _iMNewsFilterRepository;
        
		public NewsFilterService(INewsFilterRepository iNewsFilterRepository,IMNewsFilterRepository iMNewsFilterRepository)
		{
			this._iNewsFilterRepository = iNewsFilterRepository;
            this._iMNewsFilterRepository = iMNewsFilterRepository;
		}
        
        public Dictionary<string, string> GetNewsFilterBasicSearchColumns()
        {
            
            return this._iNewsFilterRepository.GetNewsFilterBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsFilterAdvanceSearchColumns()
        {
            
            return this._iNewsFilterRepository.GetNewsFilterAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsFilter> GetNewsFilterByFilterId(System.Int32 FilterId)
		{
			return _iNewsFilterRepository.GetNewsFilterByFilterId(FilterId);
		}

		public virtual List<NewsFilter> GetNewsFilterByNewsId(System.Int32 NewsId)
		{
			return _iNewsFilterRepository.GetNewsFilterByNewsId(NewsId);
		}

        public virtual NewsFilter GetByFilterIdAndNewsId(System.Int32 FilterId, System.Int32 NewsId)
        {
            return _iNewsFilterRepository.GetByFilterIdAndNewsId(FilterId, NewsId);
        }

		public NewsFilter GetNewsFilter(System.Int32 NewsFilterId)
		{
			return _iNewsFilterRepository.GetNewsFilter(NewsFilterId);
		}

		public NewsFilter UpdateNewsFilter(NewsFilter entity)
		{
			return _iNewsFilterRepository.UpdateNewsFilter(entity);
		}

		public bool DeleteNewsFilter(System.Int32 NewsFilterId)
		{
			return _iNewsFilterRepository.DeleteNewsFilter(NewsFilterId);
		}

        public bool DeleteNewsFilterByNewsId(System.Int32 NewsId)
        {
            return _iNewsFilterRepository.DeleteNewsFilterByNewsId(NewsId);
        }        

		public List<NewsFilter> GetAllNewsFilter()
		{
			return _iNewsFilterRepository.GetAllNewsFilter();
		}

		public NewsFilter InsertNewsFilter(NewsFilter entity)
		{
			 return _iNewsFilterRepository.InsertNewsFilter(entity);
		}

        public bool InsertNewsFilter(MNewsFilter entity)
        {
            return _iMNewsFilterRepository.InsertNewsFilter(entity);
        }

        public bool CheckIfNewsFilterExists(MNewsFilter entity)
        {
            return _iMNewsFilterRepository.CheckIfNewsFilterExists(entity);
        }

        public bool DeleteNewsFilterByNewsAndFilterId(MNewsFilter entity)
        {
            return _iMNewsFilterRepository.DeleteNewsFilterByNewsAndFilterId(entity);
        }
        

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsfilterid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfilterid))
            {
				NewsFilter newsfilter = _iNewsFilterRepository.GetNewsFilter(newsfilterid);
                if(newsfilter!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsfilter);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsFilter> newsfilterlist = _iNewsFilterRepository.GetAllNewsFilter();
            if (newsfilterlist != null && newsfilterlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsfilterlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsFilter newsfilter = new NewsFilter();
                PostOutput output = new PostOutput();
                newsfilter.CopyFrom(Input);
                newsfilter = _iNewsFilterRepository.InsertNewsFilter(newsfilter);
                output.CopyFrom(newsfilter);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsFilter newsfilterinput = new NewsFilter();
                NewsFilter newsfilteroutput = new NewsFilter();
                PutOutput output = new PutOutput();
                newsfilterinput.CopyFrom(Input);
                NewsFilter newsfilter = _iNewsFilterRepository.GetNewsFilter(newsfilterinput.NewsFilterId);
                if (newsfilter!=null)
                {
                    newsfilteroutput = _iNewsFilterRepository.UpdateNewsFilter(newsfilterinput);
                    if(newsfilteroutput!=null)
                    {
                        output.CopyFrom(newsfilteroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsfilterid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsfilterid))
            {
				 bool IsDeleted = _iNewsFilterRepository.DeleteNewsFilter(newsfilterid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
