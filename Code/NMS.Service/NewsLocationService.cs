﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsLocation;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsLocationService : INewsLocationService 
	{
		private INewsLocationRepository _iNewsLocationRepository;
        
		public NewsLocationService(INewsLocationRepository iNewsLocationRepository)
		{
			this._iNewsLocationRepository = iNewsLocationRepository;
		}
        
        public Dictionary<string, string> GetNewsLocationBasicSearchColumns()
        {
            
            return this._iNewsLocationRepository.GetNewsLocationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsLocationAdvanceSearchColumns()
        {
            
            return this._iNewsLocationRepository.GetNewsLocationAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsLocation> GetNewsLocationByNewsId(System.Int32? NewsId)
		{
			return _iNewsLocationRepository.GetNewsLocationByNewsId(NewsId);
		}

		public virtual List<NewsLocation> GetNewsLocationByLocationId(System.Int32? LocationId)
		{
			return _iNewsLocationRepository.GetNewsLocationByLocationId(LocationId);
		}

		public NewsLocation GetNewsLocation(System.Int32 NewsLocationid)
		{
			return _iNewsLocationRepository.GetNewsLocation(NewsLocationid);
		}

		public NewsLocation UpdateNewsLocation(NewsLocation entity)
		{
			return _iNewsLocationRepository.UpdateNewsLocation(entity);
		}

		public bool DeleteNewsLocation(System.Int32 NewsLocationid)
		{
			return _iNewsLocationRepository.DeleteNewsLocation(NewsLocationid);
		}

        public bool DeleteNewsLocationByNewsId(System.Int32 Newsid)
        {
            return _iNewsLocationRepository.DeleteNewsLocationByNewsId(Newsid);
        }

		public List<NewsLocation> GetAllNewsLocation()
		{
			return _iNewsLocationRepository.GetAllNewsLocation();
		}

		public NewsLocation InsertNewsLocation(NewsLocation entity)
		{
			 return _iNewsLocationRepository.InsertNewsLocation(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newslocationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newslocationid))
            {
				NewsLocation newslocation = _iNewsLocationRepository.GetNewsLocation(newslocationid);
                if(newslocation!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newslocation);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsLocation> newslocationlist = _iNewsLocationRepository.GetAllNewsLocation();
            if (newslocationlist != null && newslocationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newslocationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsLocation newslocation = new NewsLocation();
                PostOutput output = new PostOutput();
                newslocation.CopyFrom(Input);
                newslocation = _iNewsLocationRepository.InsertNewsLocation(newslocation);
                output.CopyFrom(newslocation);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsLocation newslocationinput = new NewsLocation();
                NewsLocation newslocationoutput = new NewsLocation();
                PutOutput output = new PutOutput();
                newslocationinput.CopyFrom(Input);
                NewsLocation newslocation = _iNewsLocationRepository.GetNewsLocation(newslocationinput.NewsLocationid);
                if (newslocation!=null)
                {
                    newslocationoutput = _iNewsLocationRepository.UpdateNewsLocation(newslocationinput);
                    if(newslocationoutput!=null)
                    {
                        output.CopyFrom(newslocationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newslocationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newslocationid))
            {
				 bool IsDeleted = _iNewsLocationRepository.DeleteNewsLocation(newslocationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
