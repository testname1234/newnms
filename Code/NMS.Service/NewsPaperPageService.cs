﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaperPage;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsPaperPageService : INewsPaperPageService 
	{
		private INewsPaperPageRepository _iNewsPaperPageRepository;
        
		public NewsPaperPageService(INewsPaperPageRepository iNewsPaperPageRepository)
		{
			this._iNewsPaperPageRepository = iNewsPaperPageRepository;
		}
        
        public Dictionary<string, string> GetNewsPaperPageBasicSearchColumns()
        {
            
            return this._iNewsPaperPageRepository.GetNewsPaperPageBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsPaperPageAdvanceSearchColumns()
        {
            
            return this._iNewsPaperPageRepository.GetNewsPaperPageAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsPaperPage> GetNewsPaperPageByDailyNewsPaperId(System.Int32 DailyNewsPaperId)
		{
			return _iNewsPaperPageRepository.GetNewsPaperPageByDailyNewsPaperId(DailyNewsPaperId);
		}

		public NewsPaperPage GetNewsPaperPage(System.Int32 NewsPaperPageId)
		{
			return _iNewsPaperPageRepository.GetNewsPaperPage(NewsPaperPageId);
		}

		public NewsPaperPage UpdateNewsPaperPage(NewsPaperPage entity)
		{
			return _iNewsPaperPageRepository.UpdateNewsPaperPage(entity);
		}

		public bool DeleteNewsPaperPage(System.Int32 NewsPaperPageId)
		{
			return _iNewsPaperPageRepository.DeleteNewsPaperPage(NewsPaperPageId);
		}

		public List<NewsPaperPage> GetAllNewsPaperPage()
		{
			return _iNewsPaperPageRepository.GetAllNewsPaperPage();
		}

		public NewsPaperPage InsertNewsPaperPage(NewsPaperPage entity)
		{
			 return _iNewsPaperPageRepository.InsertNewsPaperPage(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newspaperpageid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperpageid))
            {
				NewsPaperPage newspaperpage = _iNewsPaperPageRepository.GetNewsPaperPage(newspaperpageid);
                if(newspaperpage!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newspaperpage);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsPaperPage> newspaperpagelist = _iNewsPaperPageRepository.GetAllNewsPaperPage();
            if (newspaperpagelist != null && newspaperpagelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newspaperpagelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsPaperPage newspaperpage = new NewsPaperPage();
                PostOutput output = new PostOutput();
                newspaperpage.CopyFrom(Input);
                newspaperpage = _iNewsPaperPageRepository.InsertNewsPaperPage(newspaperpage);
                output.CopyFrom(newspaperpage);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsPaperPage newspaperpageinput = new NewsPaperPage();
                NewsPaperPage newspaperpageoutput = new NewsPaperPage();
                PutOutput output = new PutOutput();
                newspaperpageinput.CopyFrom(Input);
                NewsPaperPage newspaperpage = _iNewsPaperPageRepository.GetNewsPaperPage(newspaperpageinput.NewsPaperPageId);
                if (newspaperpage!=null)
                {
                    newspaperpageoutput = _iNewsPaperPageRepository.UpdateNewsPaperPage(newspaperpageinput);
                    if(newspaperpageoutput!=null)
                    {
                        output.CopyFrom(newspaperpageoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newspaperpageid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperpageid))
            {
				 bool IsDeleted = _iNewsPaperPageRepository.DeleteNewsPaperPage(newspaperpageid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<NewsPaperPage> GetNewsPaperPagesByDailyNewsPaperDate(DateTime fromDate, DateTime toDate)
         {
             return _iNewsPaperPageRepository.GetNewsPaperPagesByDailyNewsPaperDate(fromDate, toDate);
         }


         public void MarkIsProcessed(int id)
         {
             _iNewsPaperPageRepository.MarkIsProcessed(id);
         }
    }
	
	
}
