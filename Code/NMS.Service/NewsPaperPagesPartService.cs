﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaperPagesPart;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsPaperPagesPartService : INewsPaperPagesPartService 
	{
		private INewsPaperPagesPartRepository _iNewsPaperPagesPartRepository;
        
		public NewsPaperPagesPartService(INewsPaperPagesPartRepository iNewsPaperPagesPartRepository)
		{
			this._iNewsPaperPagesPartRepository = iNewsPaperPagesPartRepository;
		}
        
        public Dictionary<string, string> GetNewsPaperPagesPartBasicSearchColumns()
        {
            
            return this._iNewsPaperPagesPartRepository.GetNewsPaperPagesPartBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsPaperPagesPartAdvanceSearchColumns()
        {
            
            return this._iNewsPaperPagesPartRepository.GetNewsPaperPagesPartAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsPaperPagesPart> GetNewsPaperPagesPartByNewsPaperPageId(System.Int32? NewsPaperPageId)
		{
			return _iNewsPaperPagesPartRepository.GetNewsPaperPagesPartByNewsPaperPageId(NewsPaperPageId);
		}

		public NewsPaperPagesPart GetNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId)
		{
			return _iNewsPaperPagesPartRepository.GetNewsPaperPagesPart(NewsPaperPagesPartId);
		}

		public NewsPaperPagesPart UpdateNewsPaperPagesPart(NewsPaperPagesPart entity)
		{
			return _iNewsPaperPagesPartRepository.UpdateNewsPaperPagesPart(entity);
		}

		public bool DeleteNewsPaperPagesPart(System.Int32 NewsPaperPagesPartId)
		{
			return _iNewsPaperPagesPartRepository.DeleteNewsPaperPagesPart(NewsPaperPagesPartId);
		}

		public List<NewsPaperPagesPart> GetAllNewsPaperPagesPart()
		{
			return _iNewsPaperPagesPartRepository.GetAllNewsPaperPagesPart();
		}

		public NewsPaperPagesPart InsertNewsPaperPagesPart(NewsPaperPagesPart entity)
		{
			 return _iNewsPaperPagesPartRepository.InsertNewsPaperPagesPart(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newspaperpagespartid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperpagespartid))
            {
				NewsPaperPagesPart newspaperpagespart = _iNewsPaperPagesPartRepository.GetNewsPaperPagesPart(newspaperpagespartid);
                if(newspaperpagespart!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newspaperpagespart);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsPaperPagesPart> newspaperpagespartlist = _iNewsPaperPagesPartRepository.GetAllNewsPaperPagesPart();
            if (newspaperpagespartlist != null && newspaperpagespartlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newspaperpagespartlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsPaperPagesPart newspaperpagespart = new NewsPaperPagesPart();
                PostOutput output = new PostOutput();
                newspaperpagespart.CopyFrom(Input);
                newspaperpagespart = _iNewsPaperPagesPartRepository.InsertNewsPaperPagesPart(newspaperpagespart);
                output.CopyFrom(newspaperpagespart);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsPaperPagesPart newspaperpagespartinput = new NewsPaperPagesPart();
                NewsPaperPagesPart newspaperpagespartoutput = new NewsPaperPagesPart();
                PutOutput output = new PutOutput();
                newspaperpagespartinput.CopyFrom(Input);
                NewsPaperPagesPart newspaperpagespart = _iNewsPaperPagesPartRepository.GetNewsPaperPagesPart(newspaperpagespartinput.NewsPaperPagesPartId);
                if (newspaperpagespart!=null)
                {
                    newspaperpagespartoutput = _iNewsPaperPagesPartRepository.UpdateNewsPaperPagesPart(newspaperpagespartinput);
                    if(newspaperpagespartoutput!=null)
                    {
                        output.CopyFrom(newspaperpagespartoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newspaperpagespartid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperpagespartid))
            {
				 bool IsDeleted = _iNewsPaperPagesPartRepository.DeleteNewsPaperPagesPart(newspaperpagespartid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public List<NewsPaperPagesPart> GetNewsPaperPagesPartsByDailyNewsPaperDate(DateTime fromDate, DateTime toDate)
         {
             return _iNewsPaperPagesPartRepository.GetNewsPaperPagesPartsByDailyNewsPaperDate(fromDate, toDate);
         }
	}
	
	
}
