﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaperPagesPartsDetail;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsPaperPagesPartsDetailService : INewsPaperPagesPartsDetailService 
	{
		private INewsPaperPagesPartsDetailRepository _iNewsPaperPagesPartsDetailRepository;
        
		public NewsPaperPagesPartsDetailService(INewsPaperPagesPartsDetailRepository iNewsPaperPagesPartsDetailRepository)
		{
			this._iNewsPaperPagesPartsDetailRepository = iNewsPaperPagesPartsDetailRepository;
		}
        
        public Dictionary<string, string> GetNewsPaperPagesPartsDetailBasicSearchColumns()
        {
            
            return this._iNewsPaperPagesPartsDetailRepository.GetNewsPaperPagesPartsDetailBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsPaperPagesPartsDetailAdvanceSearchColumns()
        {
            
            return this._iNewsPaperPagesPartsDetailRepository.GetNewsPaperPagesPartsDetailAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsPaperPagesPartsDetail> GetNewsPaperPagesPartsDetailByNewsPaperPagesPartId(System.Int32? NewsPaperPagesPartId)
		{
			return _iNewsPaperPagesPartsDetailRepository.GetNewsPaperPagesPartsDetailByNewsPaperPagesPartId(NewsPaperPagesPartId);
		}

		public NewsPaperPagesPartsDetail GetNewsPaperPagesPartsDetail(System.Int32 NewsPaperPagesPartsDetailId)
		{
			return _iNewsPaperPagesPartsDetailRepository.GetNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetailId);
		}

		public NewsPaperPagesPartsDetail UpdateNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetail entity)
		{
			return _iNewsPaperPagesPartsDetailRepository.UpdateNewsPaperPagesPartsDetail(entity);
		}

		public bool DeleteNewsPaperPagesPartsDetail(System.Int32 NewsPaperPagesPartsDetailId)
		{
			return _iNewsPaperPagesPartsDetailRepository.DeleteNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetailId);
		}

		public List<NewsPaperPagesPartsDetail> GetAllNewsPaperPagesPartsDetail()
		{
			return _iNewsPaperPagesPartsDetailRepository.GetAllNewsPaperPagesPartsDetail();
		}

		public NewsPaperPagesPartsDetail InsertNewsPaperPagesPartsDetail(NewsPaperPagesPartsDetail entity)
		{
			 return _iNewsPaperPagesPartsDetailRepository.InsertNewsPaperPagesPartsDetail(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newspaperpagespartsdetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperpagespartsdetailid))
            {
				NewsPaperPagesPartsDetail newspaperpagespartsdetail = _iNewsPaperPagesPartsDetailRepository.GetNewsPaperPagesPartsDetail(newspaperpagespartsdetailid);
                if(newspaperpagespartsdetail!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newspaperpagespartsdetail);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsPaperPagesPartsDetail> newspaperpagespartsdetaillist = _iNewsPaperPagesPartsDetailRepository.GetAllNewsPaperPagesPartsDetail();
            if (newspaperpagespartsdetaillist != null && newspaperpagespartsdetaillist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newspaperpagespartsdetaillist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsPaperPagesPartsDetail newspaperpagespartsdetail = new NewsPaperPagesPartsDetail();
                PostOutput output = new PostOutput();
                newspaperpagespartsdetail.CopyFrom(Input);
                newspaperpagespartsdetail = _iNewsPaperPagesPartsDetailRepository.InsertNewsPaperPagesPartsDetail(newspaperpagespartsdetail);
                output.CopyFrom(newspaperpagespartsdetail);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsPaperPagesPartsDetail newspaperpagespartsdetailinput = new NewsPaperPagesPartsDetail();
                NewsPaperPagesPartsDetail newspaperpagespartsdetailoutput = new NewsPaperPagesPartsDetail();
                PutOutput output = new PutOutput();
                newspaperpagespartsdetailinput.CopyFrom(Input);
                NewsPaperPagesPartsDetail newspaperpagespartsdetail = _iNewsPaperPagesPartsDetailRepository.GetNewsPaperPagesPartsDetail(newspaperpagespartsdetailinput.NewsPaperPagesPartsDetailId);
                if (newspaperpagespartsdetail!=null)
                {
                    newspaperpagespartsdetailoutput = _iNewsPaperPagesPartsDetailRepository.UpdateNewsPaperPagesPartsDetail(newspaperpagespartsdetailinput);
                    if(newspaperpagespartsdetailoutput!=null)
                    {
                        output.CopyFrom(newspaperpagespartsdetailoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newspaperpagespartsdetailid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperpagespartsdetailid))
            {
				 bool IsDeleted = _iNewsPaperPagesPartsDetailRepository.DeleteNewsPaperPagesPartsDetail(newspaperpagespartsdetailid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public List<NewsPaperPagesPartsDetail> GetNewsPaperPagesPartsDetailByDailyNewsPaperDate(DateTime fromDate, DateTime toDate)
         {
             return _iNewsPaperPagesPartsDetailRepository.GetNewsPaperPagesPartsDetailByDailyNewsPaperDate(fromDate, toDate);
         }
	}
	
	
}
