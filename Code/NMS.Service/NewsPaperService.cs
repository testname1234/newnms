﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsPaper;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsPaperService : INewsPaperService 
	{
		private INewsPaperRepository _iNewsPaperRepository;
        
		public NewsPaperService(INewsPaperRepository iNewsPaperRepository)
		{
			this._iNewsPaperRepository = iNewsPaperRepository;
		}
        
        public Dictionary<string, string> GetNewsPaperBasicSearchColumns()
        {
            
            return this._iNewsPaperRepository.GetNewsPaperBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsPaperAdvanceSearchColumns()
        {
            
            return this._iNewsPaperRepository.GetNewsPaperAdvanceSearchColumns();
           
        }
        

		public NewsPaper GetNewsPaper(System.Int32 NewsPaperId)
		{
			return _iNewsPaperRepository.GetNewsPaper(NewsPaperId);
		}

		public NewsPaper UpdateNewsPaper(NewsPaper entity)
		{
			return _iNewsPaperRepository.UpdateNewsPaper(entity);
		}

		public bool DeleteNewsPaper(System.Int32 NewsPaperId)
		{
			return _iNewsPaperRepository.DeleteNewsPaper(NewsPaperId);
		}

		public List<NewsPaper> GetAllNewsPaper()
		{
			return _iNewsPaperRepository.GetAllNewsPaper();
		}

		public NewsPaper InsertNewsPaper(NewsPaper entity)
		{
			 return _iNewsPaperRepository.InsertNewsPaper(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newspaperid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperid))
            {
				NewsPaper newspaper = _iNewsPaperRepository.GetNewsPaper(newspaperid);
                if(newspaper!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newspaper);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsPaper> newspaperlist = _iNewsPaperRepository.GetAllNewsPaper();
            if (newspaperlist != null && newspaperlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newspaperlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsPaper newspaper = new NewsPaper();
                PostOutput output = new PostOutput();
                newspaper.CopyFrom(Input);
                newspaper = _iNewsPaperRepository.InsertNewsPaper(newspaper);
                output.CopyFrom(newspaper);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsPaper newspaperinput = new NewsPaper();
                NewsPaper newspaperoutput = new NewsPaper();
                PutOutput output = new PutOutput();
                newspaperinput.CopyFrom(Input);
                NewsPaper newspaper = _iNewsPaperRepository.GetNewsPaper(newspaperinput.NewsPaperId);
                if (newspaper!=null)
                {
                    newspaperoutput = _iNewsPaperRepository.UpdateNewsPaper(newspaperinput);
                    if(newspaperoutput!=null)
                    {
                        output.CopyFrom(newspaperoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newspaperid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newspaperid))
            {
				 bool IsDeleted = _iNewsPaperRepository.DeleteNewsPaper(newspaperid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
