﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsResourceEdit;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsResourceEditService : INewsResourceEditService 
	{
		private INewsResourceEditRepository _iNewsResourceEditRepository;
        
		public NewsResourceEditService(INewsResourceEditRepository iNewsResourceEditRepository)
		{
			this._iNewsResourceEditRepository = iNewsResourceEditRepository;
		}
        
        public Dictionary<string, string> GetNewsResourceEditBasicSearchColumns()
        {
            
            return this._iNewsResourceEditRepository.GetNewsResourceEditBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsResourceEditAdvanceSearchColumns()
        {
            
            return this._iNewsResourceEditRepository.GetNewsResourceEditAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsResourceEdit> GetNewsResourceEditByNewsId(System.Int32? NewsId)
		{
			return _iNewsResourceEditRepository.GetNewsResourceEditByNewsId(NewsId);
		}

		public NewsResourceEdit GetNewsResourceEdit(System.Int32 NewsResourceEditId)
		{
			return _iNewsResourceEditRepository.GetNewsResourceEdit(NewsResourceEditId);
		}

		public NewsResourceEdit UpdateNewsResourceEdit(NewsResourceEdit entity)
		{
			return _iNewsResourceEditRepository.UpdateNewsResourceEdit(entity);
		}

		public bool DeleteNewsResourceEdit(System.Int32 NewsResourceEditId)
		{
			return _iNewsResourceEditRepository.DeleteNewsResourceEdit(NewsResourceEditId);
		}

		public List<NewsResourceEdit> GetAllNewsResourceEdit()
		{
			return _iNewsResourceEditRepository.GetAllNewsResourceEdit();
		}

		public NewsResourceEdit InsertNewsResourceEdit(NewsResourceEdit entity)
		{
			 return _iNewsResourceEditRepository.InsertNewsResourceEdit(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsresourceeditid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsresourceeditid))
            {
				NewsResourceEdit newsresourceedit = _iNewsResourceEditRepository.GetNewsResourceEdit(newsresourceeditid);
                if(newsresourceedit!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsresourceedit);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }  
        
        public List<NewsResourceEdit> GetActiveNewsResourceEdit()
        {
            List<NewsResourceEdit> ret = new List<NewsResourceEdit>();
            var activeRes = _iNewsResourceEditRepository.GetActiveNewsResourceEdit();
            if(activeRes != null)
            {
                ret.AddRange(activeRes);
            }
            return ret;
        }

        public void SetInactive(int id)
        {
            _iNewsResourceEditRepository.SetInactive(id);
        }

         
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsResourceEdit> newsresourceeditlist = _iNewsResourceEditRepository.GetAllNewsResourceEdit();
            if (newsresourceeditlist != null && newsresourceeditlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsresourceeditlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsResourceEdit newsresourceedit = new NewsResourceEdit();
                PostOutput output = new PostOutput();
                newsresourceedit.CopyFrom(Input);
                newsresourceedit = _iNewsResourceEditRepository.InsertNewsResourceEdit(newsresourceedit);
                output.CopyFrom(newsresourceedit);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsResourceEdit newsresourceeditinput = new NewsResourceEdit();
                NewsResourceEdit newsresourceeditoutput = new NewsResourceEdit();
                PutOutput output = new PutOutput();
                newsresourceeditinput.CopyFrom(Input);
                NewsResourceEdit newsresourceedit = _iNewsResourceEditRepository.GetNewsResourceEdit(newsresourceeditinput.NewsResourceEditId);
                if (newsresourceedit!=null)
                {
                    newsresourceeditoutput = _iNewsResourceEditRepository.UpdateNewsResourceEdit(newsresourceeditinput);
                    if(newsresourceeditoutput!=null)
                    {
                        output.CopyFrom(newsresourceeditoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsresourceeditid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsresourceeditid))
            {
				 bool IsDeleted = _iNewsResourceEditRepository.DeleteNewsResourceEdit(newsresourceeditid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
