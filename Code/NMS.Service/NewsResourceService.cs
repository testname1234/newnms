﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsResource;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsResourceService : INewsResourceService 
	{
		private INewsResourceRepository _iNewsResourceRepository;
        
		public NewsResourceService(INewsResourceRepository iNewsResourceRepository)
		{
			this._iNewsResourceRepository = iNewsResourceRepository;
		}
        
        public Dictionary<string, string> GetNewsResourceBasicSearchColumns()
        {
            
            return this._iNewsResourceRepository.GetNewsResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsResourceAdvanceSearchColumns()
        {
            
            return this._iNewsResourceRepository.GetNewsResourceAdvanceSearchColumns();
           
        }
        

		public NewsResource GetNewsResource(System.Int32 NewsResourceId)
		{
			return _iNewsResourceRepository.GetNewsResource(NewsResourceId);
		}

        public List<NewsResource> GetNewsResourceByNewsId(System.Int32 NewsId)
        {
            return _iNewsResourceRepository.GetNewsResourceByNewsId(NewsId);
        }

		public NewsResource UpdateNewsResource(NewsResource entity)
		{
			return _iNewsResourceRepository.UpdateNewsResource(entity);
		}

		public bool DeleteNewsResource(System.Int32 NewsResourceId)
		{
			return _iNewsResourceRepository.DeleteNewsResource(NewsResourceId);
		}

        public bool DeleteNewsResourceByNewsId(System.Int32 NewsId)
        {
            return _iNewsResourceRepository.DeleteNewsResourceByNewsId(NewsId);
        }

		public List<NewsResource> GetAllNewsResource()
		{
			return _iNewsResourceRepository.GetAllNewsResource();
		}

		public NewsResource InsertNewsResource(NewsResource entity)
		{
			 return _iNewsResourceRepository.InsertNewsResource(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsresourceid))
            {
				NewsResource newsresource = _iNewsResourceRepository.GetNewsResource(newsresourceid);
                if(newsresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsResource> newsresourcelist = _iNewsResourceRepository.GetAllNewsResource();
            if (newsresourcelist != null && newsresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsResource newsresource = new NewsResource();
                PostOutput output = new PostOutput();
                newsresource.CopyFrom(Input);
                newsresource = _iNewsResourceRepository.InsertNewsResource(newsresource);
                output.CopyFrom(newsresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsResource newsresourceinput = new NewsResource();
                NewsResource newsresourceoutput = new NewsResource();
                PutOutput output = new PutOutput();
                newsresourceinput.CopyFrom(Input);
                NewsResource newsresource = _iNewsResourceRepository.GetNewsResource(newsresourceinput.NewsResourceId);
                if (newsresource!=null)
                {
                    newsresourceoutput = _iNewsResourceRepository.UpdateNewsResource(newsresourceinput);
                    if(newsresourceoutput!=null)
                    {
                        output.CopyFrom(newsresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsresourceid))
            {
				 bool IsDeleted = _iNewsResourceRepository.DeleteNewsResource(newsresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
