﻿     using Newtonsoft.Json;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.News;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.TextAnalysis.SemanticLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using MS.MediaIntegration.API;
using SolrManager;
using SolrManager.InputEntities;
using NMS.Repository;


namespace NMS.Service
{
    public class NewsService : INewsService
    {
        private INewsRepository _iNewsRepository;
        private IMNewsRepository _iMNewsRepository;
        private IMLocationRepository _iMLocationRepository;
        private ILocationRepository _iLocationRepository;
        private IMTagRepository _iMTagRepository;
        private ICategoryRepository _iCategoryRepository;
        private IFilterRepository _iFilterRepository;
        private IMBunchRepository _iMBunchRepository;
        private IBunchRepository _iBunchRepository;
        private IMBunchFilterRepository _iMBunchFilterRepository;
        private IMBunchTagRepository _iMBunchTagRepository;
        private IMBunchNewsRepository _iMBunchNewsRepository;
        private IMResourceRepository _iMResourceRepository;
        private IMNewsFilterRepository _iMNewsFilterRepository;
        private IMFilterCountRepository _iMFilterCountRepository;
        private IBunchNewsRepository _iBunchNewsRepository;
        private IMScrapMaxDatesRepository _iMScrapMaxDatesRepository;
        private IMTagResourceRepository _iMTagResourceRepository;
        private IMCommentRepository _iMCommentRepository;
        private IChannelVideoRepository _iChannelVideoRepository;
        private IResourceService _iResourceService;
        private IMDescrepencyNewsRepository _iMDescrepencyNewsRepository;
        private IChannelRepository _iChannelRepository;
        private IScrapMaxDatesService _iScrapMaxDatesService;
        private IRadioStreamRepository _iRadioStreamRepository;
        private IRadioStationRepository _iRadioStationRepository;
        private INewsPaperPageRepository _iNewsPaperPageRepository;
        private INewsPaperRepository _iNewsPaperRepository;
        private INewsCacheRepository _iNewsCacheRepository;
        private IProgramRepository _iProgramRepository;
        private INewsCameraManRepository _iNewsCameraManRepository;
        private ITickerRepository _iTickerRepository;
        private ITickerLineRepository _iTickerLineRepository;
        private INewsFileRepository _iNewsFileRepository;
        private IDescrepencyNewsFileRepository _iDescrepencyNewsFileRepository;


        public NewsService(INewsRepository iNewsRepository, IMNewsRepository iMNewsRepository, ILocationRepository iLocationRepository,
            IMLocationRepository iMLocationRepository, IMTagRepository iMTagRepository, ICategoryRepository iCategoryRepository, IFilterRepository iFilterRepository,
            IMBunchRepository iMBunchRepository, IMResourceRepository iMResourceRepository, IMBunchFilterRepository iMBunchFilterRepository,
            IMBunchTagRepository iMBunchTagRepository, IMBunchNewsRepository iMBunchNewsRepository, IBunchRepository iBunchRepository,
            IMNewsFilterRepository iMNewsFilterRepository, IMFilterCountRepository iMFilterCountRepository, IBunchNewsRepository iBunchNewsRepository,
            IMScrapMaxDatesRepository iMScrapMaxDatesRepository, IMTagResourceRepository iMTagResourceRepository, IMCommentRepository iMCommentRepository,
            IChannelVideoRepository iChannelVideoRepository, IResourceService iResourceService, IMDescrepencyNewsRepository iMDescrepencyNewsRepository,
            IScrapMaxDatesService iScrapMaxDatesService, IChannelRepository iChannelRepository, IRadioStreamRepository iRadioStreamRepository,
            INewsPaperPageRepository iNewsPaperPageRepository, IRadioStationRepository iRadioStationRepository, INewsPaperRepository iNewsPaperRepository,
            INewsCacheRepository iNewsCacheRepository, IProgramRepository iProgramRepository, INewsCameraManRepository iNewsCameraManRepository,
            ITickerRepository iTickerRepository, ITickerLineRepository iTickerLineRepository, INewsFileRepository iNewsFileRepository
            , IDescrepencyNewsFileRepository iDescrepencyNewsFileRepository
            )
        {
            this._iNewsRepository = iNewsRepository;
            this._iMNewsRepository = iMNewsRepository;
            this._iLocationRepository = iLocationRepository;
            this._iMLocationRepository = iMLocationRepository;
            this._iMTagRepository = iMTagRepository;
            this._iCategoryRepository = iCategoryRepository;
            this._iFilterRepository = iFilterRepository;
            this._iMBunchRepository = iMBunchRepository;
            this._iMResourceRepository = iMResourceRepository;
            this._iMBunchFilterRepository = iMBunchFilterRepository;
            this._iMBunchTagRepository = iMBunchTagRepository;
            this._iMBunchNewsRepository = iMBunchNewsRepository;
            this._iBunchRepository = iBunchRepository;
            this._iMNewsFilterRepository = iMNewsFilterRepository;
            this._iMFilterCountRepository = iMFilterCountRepository;
            this._iBunchNewsRepository = iBunchNewsRepository;
            this._iMScrapMaxDatesRepository = iMScrapMaxDatesRepository;
            this._iMTagResourceRepository = iMTagResourceRepository;
            this._iMCommentRepository = iMCommentRepository;
            this._iChannelVideoRepository = iChannelVideoRepository;
            this._iResourceService = iResourceService;
            this._iMDescrepencyNewsRepository = iMDescrepencyNewsRepository;
            this._iScrapMaxDatesService = iScrapMaxDatesService;
            this._iChannelRepository = iChannelRepository;
            this._iRadioStreamRepository = iRadioStreamRepository;
            this._iNewsPaperPageRepository = iNewsPaperPageRepository;
            this._iRadioStationRepository = iRadioStationRepository;
            this._iNewsPaperRepository = iNewsPaperRepository;
            this._iNewsCacheRepository = iNewsCacheRepository;
            this._iProgramRepository = iProgramRepository;
            this._iNewsCameraManRepository = iNewsCameraManRepository;
            this._iTickerRepository = iTickerRepository;
            this._iTickerLineRepository = iTickerLineRepository;
            this._iNewsFileRepository = iNewsFileRepository;
            this._iDescrepencyNewsFileRepository = iDescrepencyNewsFileRepository;
        }

        public Dictionary<string, string> GetNewsBasicSearchColumns()
        {
            return this._iNewsRepository.GetNewsBasicSearchColumns();
        }

        public List<SearchColumn> GetNewsAdvanceSearchColumns()
        {
            return this._iNewsRepository.GetNewsAdvanceSearchColumns();
        }

        public virtual List<News> GetNewsByLocationId(System.Int32? LocationId)
        {
            return _iNewsRepository.GetNewsByLocationId(LocationId);
        }

        public virtual List<News> GetNewsByNewsTypeId(System.Int32 NewsTypeId)
        {
            return _iNewsRepository.GetNewsByNewsTypeId(NewsTypeId);
        }

        public News GetNews(System.Int32 NewsId)
        {
            return _iNewsRepository.GetNews(NewsId);
        }

        public News UpdateNews(News entity)
        {
            return _iNewsRepository.UpdateNews(entity);
        }

        public List<MNews> GetByIDs(List<string> NewsIds)
        {
            return _iMNewsRepository.GetByIDs(NewsIds);
        }

        public bool DeleteNews(System.Int32 NewsId)
        {
            return _iNewsRepository.DeleteNews(NewsId);
        }

        public List<News> GetAllNews()
        {
            return _iNewsRepository.GetAllNews();
        }

        public DateTime GetLastNewsDate()
        {
            return _iNewsRepository.GetLastNewsDate();
        }


        public News InsertUpdateNewsIfExist(News entity)
        {
            News entityNew = _iNewsRepository.GetNews(entity.NewsId);
            if (entityNew == null)
            {
                entityNew = _iNewsRepository.InsertNews(entity);
            }
            else
            {
                entityNew = _iNewsRepository.UpdateNews(entity);
            }
            return entityNew;
        }

        public News InsertNews(News entity)
        {
            return _iNewsRepository.InsertNews(entity);
        }

        public bool InsertNews(MNews entity)
        {
            return _iMNewsRepository.InsertNews(entity);
        }

        public bool CheckIfNewsExists(MNews entity)
        {
            return _iMNewsRepository.CheckIfNewsExists(entity);
        }

        public bool CheckIfNewsExistsSQL(MNews entity)
        {
            return _iNewsRepository.CheckIfNewsExistsSQL(entity.Title, entity.Source);
            //return _iMNewsRepository.CheckIfNewsExists(entity);
        }

        public bool DeleteNews(MNews entity)
        {
            return _iMNewsRepository.DeleteNews(entity);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out newsid))
            {
                News news = _iNewsRepository.GetNews(newsid);
                if (news != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(news);
                    tranfer.Data = output;
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<News> newslist = _iNewsRepository.GetAllNews();
            if (newslist != null && newslist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newslist);
                tranfer.Data = outputlist;
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out newsid))
            {
                bool IsDeleted = _iNewsRepository.DeleteNews(newsid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        private Filter InsertLocationFilters(Location location, DateTime dtNow, int filterTypeId)
        {
            List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
            Filter filter = new Filter();
            filter.Name = location.Location;
            filter.FilterTypeId = filterTypeId;
            filter.Value = location.LocationId;
            filter.CreationDate = dtNow;
            filter.LastUpdateDate = filter.CreationDate;
            var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId).FirstOrDefault();
            if (pFilter != null)
                filter.ParentId = pFilter.FilterId;
            filter.IsActive = true;
            filter.IsApproved = true;
            filter = _iFilterRepository.InsertFilter(filter);
            if (location.ParentId.HasValue)
            {
                var parentLocation = _iLocationRepository.GetLocation(location.ParentId.Value);
                if (parentLocation != null)
                {
                    Filter parentFilter = new Filter();
                    parentFilter.Name = location.Location;
                    parentFilter.FilterTypeId = filterTypeId;
                    parentFilter.Value = location.LocationId;
                    parentFilter.CreationDate = dtNow;
                    if (pFilter != null)
                        parentFilter.ParentId = pFilter.FilterId;
                    parentFilter.LastUpdateDate = parentFilter.CreationDate;
                    parentFilter.IsActive = true;
                    parentFilter.IsApproved = true;
                    parentFilter = _iFilterRepository.InsertFilter(parentFilter);
                }
            }
            return filter;
        }

        private void AddSourceFilter(MNews mongoNews, int filterTypeId, int id, DateTime dtNow)
        {
            Filter filter = _iFilterRepository.GetFilterByFilterTypeAndValue(filterTypeId, id);
            if (filter == null)
            {
                if (filterTypeId == (int)FilterTypes.Channel)
                {
                    var channel = _iChannelRepository.GetChannel(id);
                    if (channel != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = channel.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = channel.ChannelId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else if (filterTypeId == (int)FilterTypes.Radio)
                {
                    var radio = _iRadioStationRepository.GetRadioStation(id);
                    if (radio != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = radio.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = radio.RadioStationId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else if (filterTypeId == (int)FilterTypes.NewsPaper)
                {
                    var newsPaper = _iNewsPaperRepository.GetNewsPaper(id);
                    if (newsPaper != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = newsPaper.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = newsPaper.NewsPaperId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else if (filterTypeId == (int)FilterTypes.Program)
                {
                    var program = _iProgramRepository.GetProgram(id);
                    if (program != null)
                    {
                        List<Filter> parentFilters = _iFilterRepository.GetAllParentFilters();
                        filter = new Filter();
                        filter.Name = program.Name;
                        filter.FilterTypeId = filterTypeId;
                        filter.Value = program.ProgramId;
                        filter.CreationDate = dtNow;
                        filter.LastUpdateDate = filter.CreationDate;
                        var pFilter = parentFilters.Where(x => x.FilterTypeId == filterTypeId).FirstOrDefault();
                        if (pFilter != null)
                            filter.ParentId = pFilter.FilterId;
                        filter.IsActive = true;
                        filter.IsApproved = true;
                        filter = _iFilterRepository.InsertFilter(filter);
                    }
                }
                else
                {
                    var location = _iLocationRepository.GetLocation(id);
                    if (location != null)
                    {
                        filter = InsertLocationFilters(location, dtNow, filterTypeId);
                    }
                }
            }
            mongoNews.SourceTypeId = filter.FilterTypeId;
            mongoNews.SourceFilterId = filter.FilterId;
            mongoNews.Source = filter.Name;
            mongoNews.Filters.Add(getNewsFilter(filter, mongoNews._id, mongoNews.ReporterId, dtNow));
            if (filter.ParentId.HasValue)
            {
                var parentFilter = _iFilterRepository.GetFilter(filter.ParentId.Value);
                if (parentFilter != null)
                {
                    mongoNews.Filters.Add(getNewsFilter(parentFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
                }
            }
        }

        private bool IsNewsUpdated(MNews source, PostInput copy)
        {
            if (source.Title.ToLower().Trim() != copy.Title.ToLower().Trim())
                return true;
            if (source.Description.ToLower().Trim() != copy.Description.ToLower().Trim())
                return true;
            //if (source.SourceTypeId != copy.FilterTypeId)
            //    return true;
            if (source.Tags == null || copy.Tags == null)
            {
                if ((source.Tags == null || source.Tags.Count == 0) && (copy.Tags == null || copy.Tags.Count == 0))
                { }
                else return true;
            }
            else if (source.Tags.Count() != copy.Tags.Count() || source.Tags.Where(x => copy.Tags.Where(y => y.Tag == x.Tag).Count() == 0).Count() > 0)
                return true;
            //if (source.Resources == null || copy.Resources == null)
            //{
            //    if ((source.Resources == null || source.Resources.Count == 0) && (copy.Resources == null || copy.Resources.Count == 0))
            //    { }
            //    else return true;
            //}
            //else if (source.Resources.Count() != copy.Resources.Count() || source.Resources.Where(x => copy.Resources.Where(y => y.Guid == x.Guid).Count() == 0).Count() > 0)
            //    return true;
            if (source.Categories.Count() != copy.CategoryIds.Count() || source.Categories.Where(x => copy.CategoryIds.Where(y => y == x.CategoryId).Count() == 0).Count() > 0)
                return true;
            if (source.Locations == null || copy.LocationIds == null)
            {
                if ((source.Locations == null || source.Locations.Count == 0) && (copy.LocationIds == null || copy.LocationIds.Count == 0))
                { }
                else return true;
            }
            else if (source.Locations.Count() != copy.LocationIds.Count() || source.Locations.Where(x => copy.LocationIds.Where(y => y == x.LocationId).Count() == 0).Count() > 0)
                return true;
            return false;
        }

        public MNews InsertReportedNews(PostInput postInput) 
        {
            DateTime dtstart = DateTime.UtcNow;
            MBunch mongoBunch = new MBunch();
            MNews prevNews = new MNews();
            MNews mongoNews = new MNews();
            DateTime dtNow = DateTime.UtcNow;
            InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;

            mongoNews.Title = postInput.Title;
            mongoNews.ParentNewsId = postInput.ParentNewsId;
            mongoNews.PublishTime = postInput.NewsDate;

            #region Same News Checking

            bool byPassCheckIfNewsExistCheck = false;
            bool isSameNews = false;
            if (!string.IsNullOrEmpty(postInput.LinkedNewsId) || !string.IsNullOrEmpty(postInput.ReferenceNewsId))
            {
                prevNews = _iMNewsRepository.GetNewsByID(string.IsNullOrEmpty(postInput.LinkedNewsId) ? postInput.ReferenceNewsId : postInput.LinkedNewsId);
                if (prevNews != null)
                {
                    if (IsNewsUpdated(prevNews, postInput))
                    {
                        if (postInput.MarkNewsVerified != null)
                        {
                            var newsId = postInput.MarkNewsVerified.NewsIds.Where(x => x == prevNews._id).FirstOrDefault();
                            if (newsId != null)
                            {
                                postInput.MarkNewsVerified.NewsIds.Remove(newsId);
                                //postInput.MarkNewsVerified.NewsIds.Add(mongoNews._id);
                            }
                        }
                    }
                    else isSameNews = true;
                }
                byPassCheckIfNewsExistCheck = true;
            }

            #endregion Same News Checking

            #region Filters

            mongoNews.Filters = new List<MNewsFilter>();
            if (postInput.ProgramId == 0)
            {
                var allNewsFilter = _iFilterRepository.GetFilterByFilterTypeId((int)FilterTypes.AllNews).FirstOrDefault();
                mongoNews.Filters.Add(getNewsFilter(allNewsFilter, mongoNews._id, mongoNews.ReporterId, dtNow));

                var TopRatedFilter = _iFilterRepository.GetFilter((int)NewsFilters.TopRated);
                mongoNews.Filters.Add(getNewsFilter(TopRatedFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
            }

            if (postInput.MarkNewsVerified == null)
            {
                var notVerifiedFilter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsFilter, "NotVerified");
                mongoNews.Filters.Add(getNewsFilter(notVerifiedFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
            }
            else if (!isSameNews)
            {
                //postInput.MarkNewsVerified.NewsIds.RemoveAll(x => x == mongoNews._id);
                var notVerifiedFilter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsFilter, postInput.MarkNewsVerified.IsVerified ? "Verified" : "Rejected");
                mongoNews.Filters.Add(getNewsFilter(notVerifiedFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
            }

            if (postInput.FilterTypeId == (int)FilterTypes.FieldReporter || postInput.FilterTypeId == (int)FilterTypes.PublicReporter)
            {
                foreach (int id in postInput.LocationIds)
                {
                    AddSourceFilter(mongoNews, postInput.FilterTypeId, id, dtNow);
                    if (postInput.NewsTypeId == (int)NewsTypes.Package)
                        AddSourceFilter(mongoNews, (int)FilterTypes.Package, id, dtNow);
                }
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Channel)
            {
                AddSourceFilter(mongoNews, postInput.FilterTypeId, postInput.ChannelId, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Radio)
            {
                AddSourceFilter(mongoNews, postInput.FilterTypeId, postInput.RadioStationId, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.NewsPaper)
            {
                AddSourceFilter(mongoNews, postInput.FilterTypeId, postInput.NewsPaperId, dtNow);
            }
            else if (postInput.FilterTypeId == (int)FilterTypes.Program)
            {
                AddSourceFilter(mongoNews, postInput.FilterTypeId, postInput.ProgramId, dtNow);
            }

            #region Ticker Filters Marking
            int CheckTelevisionNews = String.IsNullOrEmpty(postInput.Description) ? 0 : 1;
            int CheckNewsPaperNews = String.IsNullOrEmpty(postInput.NewsPaperdescription) ? 0 : 1;
            int CheckTickerNews = (postInput.Tickers != null && postInput.Tickers.Count > 0) ? 1 : 0;


            var NewsTelevisionFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.ReportedNews, "Television News");
            var NewsNewsPaperFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.ReportedNews, "NewsPaper News");
            var NewsTickerFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.ReportedNews, "Ticker News");

            if (CheckTelevisionNews > 0)
                mongoNews.Filters.Add(getNewsFilter(NewsTelevisionFitler, mongoNews._id, mongoNews.ReporterId, dtNow));
            if (CheckNewsPaperNews > 0)
                mongoNews.Filters.Add(getNewsFilter(NewsNewsPaperFitler, mongoNews._id, mongoNews.ReporterId, dtNow));
            if (CheckTickerNews > 0)
                mongoNews.Filters.Add(getNewsFilter(NewsTickerFitler, mongoNews._id, mongoNews.ReporterId, dtNow));
            #endregion

            #endregion Filters

            if (!isSameNews && (byPassCheckIfNewsExistCheck || !_iMNewsRepository.CheckIfNewsExists(mongoNews)))// on the basis of title,publishtime,sourcetype,sourcefilter
            {
                if (postInput.ResourceEdit != null && postInput.ResourceEdit.Id > 0)
                {
                    if (postInput.Resources == null)
                        postInput.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                    postInput.Resources.Add(GenerateResource(postInput.ResourceEdit));
                    mongoNews.ResourceEdit = postInput.ResourceEdit;
                    mongoNews.HasResourceEdit = true;
                }

                if (!string.IsNullOrEmpty(postInput.LinkedNewsId))
                {
                    prevNews.LinkNewsId = mongoNews._id;
                    prevNews.LastUpdateDate = dtNow;
                    foreach (var filter in prevNews.Filters)
                        _iMBunchFilterRepository.RemoveNewsFromBunchFilter(filter.FilterId, prevNews.BunchGuid, prevNews._id);
                    _iMNewsRepository.UpdateNews(prevNews);
                    mongoBunch._id = prevNews.BunchGuid;
                }

                mongoNews.ReferenceNewsId = postInput.ReferenceNewsId;
                mongoNews.CreationDate = dtNow;
                mongoNews.LastUpdateDate = mongoNews.CreationDate;
                mongoNews.Description = postInput.Description;
                mongoNews.ReporterId = postInput.ReporterId;
                mongoNews.LanguageCode = postInput.LanguageCode.ToLower();
                mongoNews.NewsTypeId = postInput.NewsTypeId;
                mongoNews.ThumbnailUrl = postInput.ThumbnailUrl;
                mongoNews.NewsCameraMen = postInput.NewsCameraMen;
                mongoNews.Slug = postInput.Slug;


                #region BunchId

                if (string.IsNullOrEmpty(postInput.ParentNewsId))
                    mongoNews.BunchGuid = mongoBunch._id;
                else
                {
                    var parentNews = _iMNewsRepository.GetNewsByID(postInput.ParentNewsId);
                    if (parentNews.Updates == null)
                        parentNews.Updates = new List<MNews>();
                    parentNews.Updates.Add(mongoNews.GetAsUpdateNews());
                    _iMNewsRepository.UpdateNews(parentNews);
                    mongoBunch._id = parentNews.BunchGuid;
                }

                #endregion BunchId

                #region Categories

                mongoNews.Categories = new List<Category>();
                foreach (var item in postInput.CategoryIds)
                {
                    mongoNews.Categories.Add(_iCategoryRepository.GetCategory(item));
                }

                #endregion Categories

                #region Location

                mongoNews.Locations = new List<Location>();
                foreach (int locationid in postInput.LocationIds)
                {
                    mongoNews.Locations.Add(_iLocationRepository.GetLocation(locationid));
                }

                #endregion Location

                #region Text Translation incase not English

                //mongoNews.DescriptionText = Regex.Replace(mongoNews.Description, "<.*?>", "", RegexOptions.IgnoreCase).Trim();
                //mongoNews.Description = Regex.Replace(mongoNews.Description, "<?.img.*?>", "", RegexOptions.IgnoreCase).Trim();
                //postInput.NewsPaperdescription = Regex.Replace(postInput.NewsPaperdescription, "<?.img.*?>", "", RegexOptions.IgnoreCase).Trim();
                //if (mongoNews.LanguageCode != "en")
                //{
                //    mongoNews = TranslateNews(mongoNews, postInput.NewsPaperdescription);
                //}
                //else
                //    mongoNews.Description = String.IsNullOrEmpty(mongoNews.Description) ? postInput.NewsPaperdescription : mongoNews.Description;


                #endregion Text Translation incase not English

                #region Tags

                mongoNews.Tags = new List<MTag>();
                if (!string.IsNullOrEmpty(mongoNews.DescriptionText))
                {
                    //Work for Related Resources if no resource
                    KeywordAnalyzer keywordAnalyzer = new KeywordAnalyzer();
                    var keyWordObj = keywordAnalyzer.Analyze(mongoNews.DescriptionText);
                    foreach (var item in keyWordObj.Keywords)
                    {
                        MTag tag = new MTag();
                        tag.Tag = item.Word.ToLower();
                        tag.Rank = Convert.ToDouble(item.Rank);
                        tag.CreationDate = dtNow;
                        tag.LastUpdateDate = tag.CreationDate;
                        // tag = _iMTagRepository.GetTagCreateIfNotExistOptimized(tag);  //No need of tags
                        mongoNews.Tags.Add(tag);
                    }
                }

                if (postInput.Tags != null)
                {
                    foreach (var item in postInput.Tags)
                    {
                        if (mongoNews.Tags.Where(x => x.Tag == item.Tag).Count() == 0)
                        {
                            MTag tag = new MTag();
                            tag.CopyFrom(item);
                            tag.Tag = tag.Tag.ToLower();
                            tag.CreationDate = dtNow;
                            tag.LastUpdateDate = tag.CreationDate;
                            //tag = _iMTagRepository.GetTagCreateIfNotExistOptimized(tag); //No need of tags
                            mongoNews.Tags.Add(tag);
                        }
                    }
                }

                #endregion Tags

                #region Category Filters

                foreach (var category in mongoNews.Categories)
                {
                    var catFilter = _iFilterRepository.GetFilterByFilterTypeAndValue((int)FilterTypes.Category, category.CategoryId);
                    if (catFilter == null)
                    {
                        catFilter = new Filter();
                        catFilter.Name = category.Category;
                        catFilter.Value = category.CategoryId;
                        catFilter.FilterTypeId = (int)FilterTypes.Category;
                        catFilter.CreationDate = dtNow;
                        catFilter.LastUpdateDate = catFilter.CreationDate;
                        catFilter.IsActive = true;
                        catFilter.IsApproved = false;
                        catFilter = _iFilterRepository.InsertFilter(catFilter);
                    }
                    mongoNews.Filters.Add(getNewsFilter(catFilter, mongoNews._id, mongoNews.ReporterId, dtNow));

                    if (catFilter.ParentId.HasValue)
                    {
                        var parentFilter = _iFilterRepository.GetFilter(catFilter.ParentId.Value);
                        if (parentFilter != null)
                        {
                            mongoNews.Filters.Add(getNewsFilter(parentFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
                        }
                    }
                }

                #endregion Category Filters

                #region Resources

                mongoNews.Resources = new List<MResource>();

                #region scrap resources from Body

                //MatchCollection collection = null;
                MNews mongoNewstmp = new MNews();
                //collection = Regex.Matches(postInput.Description.ToString(), "src=(\"|')((?!><).)*?(jpg|gif|png|mp4|flv|jpeg)", RegexOptions.IgnoreCase);
                //if (collection.Count > 0)
                //{
                //    mongoNewstmp.Resources = new List<MResource>();
                //    foreach (var match in collection)
                //    {
                //        string urlPrefix = "";
                //        if (match.ToString().Contains("www.") || match.ToString().Contains("http"))
                //        {
                //            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
                //            MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                //            resource.ResourceStatusId = ((int)MS.Core.Enums.ResourceStatuses.DownloadPending).ToString();
                //            resource.Source = urlPrefix + match.ToString().Replace("src=\"", "").Replace("\"", "").Replace("src='", "").Replace("'", "");
                //            if (!Regex.Match(resource.Source, ".*(mp4|flv)", RegexOptions.IgnoreCase).Success)
                //            // (resource.Source.Substring(resource.Source.LastIndexOf('.') + 1) != "mp4")
                //            {
                //                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                //            }
                //            else
                //            {
                //                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                //            }
                //            resource.IsFromExternalSource = true;

                //            _iResourceService.MediaPostResource(resource, "NMS User");

                //            MSApi MediaApi = new MSApi();
                //            string dir1 = "/" + "NMS User";
                //            string dir2 = "/" + "NMS User" + "/" + DateTime.UtcNow.Year.ToString();
                //            string dir3 = "/" + "NMS User" + "/" + DateTime.UtcNow.Year.ToString() + "/" + DateTime.UtcNow.Month.ToString("00");
                //            List<string> lstDir = new List<string>();
                //            lstDir.Add(dir1);
                //            lstDir.Add(dir2);
                //            lstDir.Add(dir3);

                //            foreach (string d in lstDir)
                //            {
                //                var Result = MediaApi.CreateSubBucket(((int)NMSBucket.NMSBucket), NMS.Core.AppSettings.MediaServerApiKey, d);
                //            }
                //            resource.FilePath = dir3 + "/" + Path.GetFileName(resource.Source);
                //            resource.BucketId = ((int)NMSBucket.NMSBucket);
                //            resource.ApiKey = NMS.Core.AppSettings.MediaServerApiKey;
                //            MResource res = new MResource();
                //            res.CopyFrom(MediaApi.PostResource(resource));
                //            mongoNewstmp.Resources.Add(res);
                //        }
                //    }
                //}


                #endregion scrap resources from Body

                //Code if no resource found for a news
                //if ((postInput.Resources == null || postInput.Resources.Count <= 0) && (mongoNewstmp.Resources == null || mongoNewstmp.Resources.Count <= 0) && mongoNews.Tags != null && mongoNews.Tags.Count > 0)
                //{
                //    string TagsString = "";
                //    foreach (MTag tag in mongoNews.Tags.Where(x => x.Tag.Trim().Count(y => y == ' ') <= 1).OrderByDescending(x => x.Rank).Take(3))
                //    {
                //        TagsString += "," + tag.Tag;
                //    }
                //    List<MResource> mResources = _iResourceService.GetResourceByMeta(TagsString);
                //    mongoNewstmp.Resources = mResources;
                //    mongoNews.HasNoResource = true;
                //}

                if (postInput.Resources != null || mongoNewstmp.Resources != null)
                {
                    if (postInput.Resources != null)
                    {
                        foreach (var item in postInput.Resources)
                        {
                            MResource resource = new MResource();
                            resource.CopyFrom(item);
                            AddResources(mongoBunch, mongoNews, resource, dtNow);
                        }
                        if (mongoNews.Resources.Count() > 0)
                            mongoNews.ThumbnailUrl = mongoNews.Resources.First().Guid;
                    }
                    if (mongoNewstmp.Resources != null)
                    {
                        foreach (var item in mongoNewstmp.Resources)
                        {
                            AddResources(mongoBunch, mongoNews, item, dtNow);
                        }
                        if (mongoNews.Resources.Count() > 0)
                            mongoNews.ThumbnailUrl = mongoNews.Resources.First().Guid;
                    }

                    if (mongoNews.Resources != null && mongoNews.Resources.Count > 0)
                    {
                        int CheckImagesResource = mongoNews.Resources.Count(x => x.ResourceTypeId == Convert.ToInt32(ResourceTypes.Image));
                        int CheckvideoResource = mongoNews.Resources.Count(x => x.ResourceTypeId == Convert.ToInt32(ResourceTypes.Video));


                        var NewsMediaImagesFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsMedia, "Images");
                        var NewsMediaVideosFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsMedia, "Videos");

                        if (CheckImagesResource > 0)
                            mongoNews.Filters.Add(getNewsFilter(NewsMediaImagesFitler, mongoNews._id, mongoNews.ReporterId, dtNow));
                        if (CheckvideoResource > 0)
                            mongoNews.Filters.Add(getNewsFilter(NewsMediaVideosFitler, mongoNews._id, mongoNews.ReporterId, dtNow));
                    }

                }

                #endregion Resources

                #region tag resource

                //if (mongoNews.Tags != null && mongoNews.Tags.Count > 0)
                //{
                //    foreach (MTag tag in mongoNews.Tags)
                //    {
                //        MTagResource tagResource = new MTagResource();
                //        tagResource.TagId = tag._id;
                //        tagResource.Resources = mongoNews.Resources;
                //        tagResource.CreationDate = dtNow;
                //        tagResource.LastUpdateDate = tagResource.CreationDate;
                //        _iMTagResourceRepository.CreateIfNotExist(tagResource);
                //    }
                //}

                #endregion tag resource

                #region Comments

                if (postInput.Comments != null)
                {
                    mongoNews.Comments = new List<MComment>();
                    foreach (var comment in postInput.Comments)
                    {
                        MComment mComment = new MComment();
                        mComment.NewsId = mongoNews._id;
                        mComment.UserId = postInput.ReporterId;
                        mComment.CopyFrom(comment);
                        mComment.CreationDate = dtNow;
                        mComment.LastUpdateDate = mComment.CreationDate;
                        _iMCommentRepository.InsertComment(mComment);
                        mongoNews.Comments.Add(mComment);
                    }
                }

                #endregion Comments

                #region AddResourceMeta

                // mongoNews.Tags = mongoNews.Tags.OrderByDescending(x => x.Rank).Take(5).ToList;
                _iResourceService.AddResourceMeta(mongoNews);

                #endregion AddResourceMeta

                DateTime dtmiddle = DateTime.UtcNow;
                int totalMIddleseconds = dtmiddle.Subtract(dtstart).Seconds;

                //INsert cameraman
                //sql entity newscameraman
                //newsguid pk get from mongonews _id
                for (int i = 0; i < postInput.NewsCameraMen.Count; i++)
                {
                    NewsCameraMan cam = new NewsCameraMan();
                    cam.UserId = postInput.NewsCameraMen[i];
                    cam.NewsGuid = mongoNews._id;
                    cam.CreationDate = DateTime.UtcNow;
                    cam.LastUpdateDate = cam.CreationDate;
                    cam.IsActive = true;
                    _iNewsCameraManRepository.InsertNewsCameraMan(cam);
                }

                if (postInput.Tickers != null)
                {
                    foreach (var ticker in postInput.Tickers)
                    {
                        ITickerService tickerService = IoC.Resolve<ITickerService>("TickerService");
                        ticker.TickerGroupName = postInput.Slug;
                        ticker.NewsGuid = mongoNews._id;
                        tickerService.InsertTicker(ticker, 0, dtNow);
                    }
                }

                InsertNews(mongoNews, mongoBunch, dtNow, "", postInput);
                DateTime dtlast = DateTime.UtcNow;
                int totalLastseconds = dtlast.Subtract(dtmiddle).Seconds;

                if (postInput.MarkNewsVerified != null && postInput.MarkNewsVerified.NewsIds.Count > 0)
                    MarkVerifyNews(postInput.MarkNewsVerified.NewsIds, postInput.MarkNewsVerified.bunchId, postInput.MarkNewsVerified.IsVerified);
                return mongoNews;
            }
            if (!string.IsNullOrEmpty(postInput.LinkedNewsId) || !string.IsNullOrEmpty(postInput.ReferenceNewsId))
            {
                UpdatePreviousNews(prevNews, postInput, dtNow, mongoNews.Filters);
            }
            if (postInput.MarkNewsVerified != null && postInput.MarkNewsVerified.NewsIds.Count > 0)
                MarkVerifyNews(postInput.MarkNewsVerified.NewsIds, postInput.MarkNewsVerified.bunchId, postInput.MarkNewsVerified.IsVerified);
            return null;
        }

        public MNews EditNews(PostInput postInput)
        {
            DateTime dtstart = DateTime.UtcNow;
            MBunch mongoBunch = new MBunch();
            MNews mongoNews = _iMNewsRepository.GetNewsByID(postInput.Guid);
            News HistoryNews = _iNewsRepository.GetByNewsGuid(postInput.Guid);
            DateTime dtNow = DateTime.UtcNow;

            #region Setting Properties
            mongoNews._id = postInput.Guid;
            mongoNews.Title = postInput.Title;
            mongoNews.PublishTime = postInput.NewsDate;
            mongoNews.LastUpdateDate = dtNow;
            mongoNews.Description = postInput.Description;
            mongoNews.ReporterId = postInput.ReporterId;
            mongoNews.LanguageCode = postInput.LanguageCode.ToLower();
            mongoNews.ThumbnailUrl = postInput.ThumbnailUrl;
            mongoNews.NewsCameraMen = postInput.NewsCameraMen;
            mongoNews.Slug = postInput.Slug;
            #endregion
            #region Text Translation incase not English

            mongoNews.DescriptionText = Regex.Replace(mongoNews.Description, "<.*?>", "", RegexOptions.IgnoreCase).Trim();
            mongoNews.Description = Regex.Replace(mongoNews.Description, "<?.img.*?>", "", RegexOptions.IgnoreCase).Trim();
            postInput.NewsPaperdescription = Regex.Replace(postInput.NewsPaperdescription, "<?.img.*?>", "", RegexOptions.IgnoreCase).Trim();
            if (mongoNews.LanguageCode != "en")
            {
                mongoNews = TranslateNews(mongoNews, postInput.NewsPaperdescription);
            }
            else
                mongoNews.Description = String.IsNullOrEmpty(mongoNews.Description) ? postInput.NewsPaperdescription : mongoNews.Description;


            #endregion Text Translation incase not English
            #region Tags

            mongoNews.Tags = new List<MTag>();
            if (!string.IsNullOrEmpty(mongoNews.DescriptionText))
            {
                //Work for Related Resources if no resource
                KeywordAnalyzer keywordAnalyzer = new KeywordAnalyzer();
                var keyWordObj = keywordAnalyzer.Analyze(mongoNews.DescriptionText);
                foreach (var item in keyWordObj.Keywords)
                {
                    MTag tag = new MTag();
                    tag.Tag = item.Word.ToLower();
                    tag.Rank = Convert.ToDouble(item.Rank);
                    tag.CreationDate = dtNow;
                    tag.LastUpdateDate = tag.CreationDate;
                    // tag = _iMTagRepository.GetTagCreateIfNotExistOptimized(tag);  //No need of tags
                    mongoNews.Tags.Add(tag);
                }
            }

            if (postInput.Tags != null)
            {
                foreach (var item in postInput.Tags)
                {
                    if (mongoNews.Tags.Where(x => x.Tag == item.Tag).Count() == 0)
                    {
                        MTag tag = new MTag();
                        tag.CopyFrom(item);
                        tag.Tag = tag.Tag.ToLower();
                        tag.CreationDate = dtNow;
                        tag.LastUpdateDate = tag.CreationDate;
                        mongoNews.Tags.Add(tag);
                    }
                }
            }

            #endregion Tags        
            #region Slug Creation
            SlugService slugService = IoC.Resolve<SlugService>("SlugService");
            Slug slug = new Slug();
            slug.Slug = mongoNews.Slug;
            slug = slugService.InsertIfNotExists(slug);
            #endregion
            #region setting variables
            mongoNews.SlugId = slug.SlugId;
            News news = new News();
            news.CopyFrom(mongoNews);
            news.ThumbnailId = mongoNews.ThumbnailUrl;
            news.Guid = mongoNews._id;
            if (mongoNews.Locations != null && mongoNews.Locations.Count > 0)
                news.LocationId = mongoNews.Locations.Last().LocationId;
            news.NewsPaperdescription = postInput.NewsPaperdescription;
            news.Suggestions = postInput.Suggestions;
            #endregion

            _iMNewsRepository.DeleteByNewsId(new List<string>() { mongoNews._id });
            _iMNewsRepository.InsertNews(mongoNews);
            _iNewsRepository.UpdateCompleteNews(news);

            NewsUpdateHistoryRepository newsUpdateHistoryRepository = new NewsUpdateHistoryRepository();
            NewsUpdateHistory newsUpdateHistory = new NewsUpdateHistory();
            newsUpdateHistory.CopyFrom(HistoryNews);
            newsUpdateHistory.NewsGuid = HistoryNews.Guid;
            newsUpdateHistoryRepository.InsertNewsUpdateHistory(newsUpdateHistory);

            return mongoNews;
        }

        public void UpdatePreviousNews(MNews mongoNews, PostInput postInput, DateTime dtNow, List<MNewsFilter> filters)
        {
            MBunch mongoBunch = new MBunch();
            mongoBunch._id = mongoNews.BunchGuid;
            if (postInput.ResourceEdit != null)
            {
                if (postInput.Resources == null)
                    postInput.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                postInput.Resources.Add(GenerateResource(postInput.ResourceEdit));
                mongoNews.ResourceEdit = postInput.ResourceEdit;
                mongoNews.HasResourceEdit = true;
            }

            if (postInput.Resources != null)
            {
                if (mongoNews.ResourceEdit == null)
                    mongoNews.Resources = new List<MResource>();
                foreach (var item in postInput.Resources)
                {
                    if (!mongoNews.Resources.Any(x => x.Guid == item.Guid))
                    {
                        MResource resource = new MResource();
                        resource.CopyFrom(item);
                        AddResources(mongoBunch, mongoNews, resource, dtNow);
                        //if (mongoBunch.Resources == null)
                        //    mongoBunch.Resources = new List<MResource>();
                        //if (!mongoBunch.Resources.Any(x => x.Guid == resource.Guid))
                        //    mongoBunch.Resources.Add(resource);
                    }
                }
                if (mongoNews.Resources.Count() > 0)
                    mongoNews.ThumbnailUrl = mongoNews.Resources.First().Guid;

                List<int> filterIDs = new List<int>();
                if (postInput.FilterTypeId == (int)FilterTypes.Channel)
                {
                    foreach (var filter in filters)
                    {
                        if (!mongoNews.Filters.Any(x => x.FilterId == filter.FilterId))
                        {
                            mongoNews.Filters.Add(filter);
                            filterIDs.Add(filter.FilterId);
                        }
                    }
                }

                #region AddResourceMeta

                _iResourceService.AddResourceMeta(mongoNews);

                #endregion AddResourceMeta

                _iMNewsRepository.UpdateResourcesAndFilters(mongoNews);
                mongoNews.Filters.RemoveAll(x => !filterIDs.Contains(x.FilterId));
                InsertBunchFilters(mongoBunch._id, mongoNews, true);
            }
        }

        public InsertNewsStatus InsertRawNews(RawNews parsedNews)
        {
            MBunch mongoBunch = new MBunch();
            MNews mongoNews = new MNews();
            DateTime dtNow = DateTime.UtcNow;
            InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;

            mongoNews.Title = parsedNews.Title;
            mongoNews.PublishTime = parsedNews.PublishTime;

            if (dtNow.Subtract(parsedNews.PublishTime).TotalHours > 47)
                dtNow = parsedNews.PublishTime;

            #region Filters

            mongoNews.Filters = new List<MNewsFilter>();
            var NewsMediaImagesFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsMedia, "Images");
            var NewsMediaVideosFitler = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsMedia, "Videos");

            var allNewsFilter = _iFilterRepository.GetFilterByFilterTypeId((int)FilterTypes.AllNews).FirstOrDefault();
            var notVerifiedFilter = _iFilterRepository.GetFilterByNameAndFilterType(FilterTypes.NewsFilter, "NotVerified");
            mongoNews.Filters.Add(getNewsFilter(allNewsFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
            mongoNews.Filters.Add(getNewsFilter(notVerifiedFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
            if (!string.IsNullOrEmpty(parsedNews.Source))
            {
                Filter filter = new Filter();
                filter.Name = parsedNews.Source;
                filter.FilterTypeId = (int)parsedNews.SourceType;
                filter.CreationDate = dtNow;
                filter.LastUpdateDate = filter.CreationDate;
                filter.IsActive = true;
                filter.IsApproved = false;
                filter = _iFilterRepository.GetFilterCreateIfNotExist(filter);
                mongoNews.SourceTypeId = filter.FilterTypeId;
                mongoNews.SourceFilterId = filter.FilterId;
                mongoNews.Source = filter.Name;
                mongoNews.Filters.Add(getNewsFilter(filter, mongoNews._id, mongoNews.ReporterId, dtNow));
                if (filter.ParentId.HasValue)
                {
                    var parentFilter = _iFilterRepository.GetFilter(filter.ParentId.Value);
                    if (parentFilter != null)
                    {
                        mongoNews.Filters.Add(getNewsFilter(parentFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
                    }
                }
            }

            #endregion Filters

            CheckNewsDescrepencyMarking(parsedNews, dtNow, out newsStatus);

            if (newsStatus == InsertNewsStatus.DiscrepancyCategory)
            {
                parsedNews.Categories = new List<string>();
                parsedNews.Categories.Add("Misc");
                parsedNews.Location = "Misc";
            }
            else if (newsStatus == InsertNewsStatus.DiscrepancyLocation)
            {
                parsedNews.Location = "Misc";
            }

            mongoNews.CreationDate = dtNow;
            mongoNews.LastUpdateDate = mongoNews.CreationDate;
            mongoNews.PublishTime = mongoNews.CreationDate;
            mongoNews.Description = parsedNews.Description;
            mongoNews.TranslatedTitle = parsedNews.TranslatedTitle;
            mongoNews.TranslatedDescription = parsedNews.TranslatedDescription;
            mongoNews.DescriptionText = parsedNews.DescriptionText;
            mongoNews.LanguageCode = parsedNews.LanguageCode;
            mongoNews.Author = parsedNews.Author;
            mongoNews.NewsTypeId = (int)parsedNews.NewsType;
            mongoNews.Slug = parsedNews.Slug;

            #region BunchId
            mongoNews.BunchGuid = mongoBunch._id;
            #endregion BunchId

            #region Categories

            mongoNews.Categories = new List<Category>();
            foreach (var item in parsedNews.Categories)
            {
                Category category = new Category();
                category.Category = item;
                category.CreationDate = dtNow;
                category.LastUpdateDate = category.CreationDate;
                category.IsActive = true;
                category.IsApproved = false;
                category = _iCategoryRepository.GetCategoryCreateIfNotExist(category);
                mongoNews.Categories.Add(category);
            }

            #endregion Categories

            #region Location

            if (!string.IsNullOrEmpty(parsedNews.Location))
            {
                Location loc = new Location();
                loc.CreationDate = dtNow;
                loc.LastUpdateDate = loc.CreationDate;
                loc.IsActive = true;
                loc.Location = parsedNews.Location;
                loc = _iLocationRepository.GetLocationCreateIfNotExist(loc);
                mongoNews.Locations = new List<Location>() { loc };
            }

            #endregion Location


            #region Resources

            MNews mongoNewstmp = new MNews();
            mongoNews.Resources = new List<MResource>();
            if (parsedNews.ImageGuids != null)
            {
                int CheckImagesResource = parsedNews.ImageGuids.Count(x => x.ResourceTypeId == Convert.ToInt32(ResourceTypes.Image));
                int CheckvideoResource = parsedNews.ImageGuids.Count(x => x.ResourceTypeId == Convert.ToInt32(ResourceTypes.Video));

                if (CheckImagesResource > 0)
                    mongoNews.Filters.Add(getNewsFilter(NewsMediaImagesFitler, mongoNews._id, mongoNews.ReporterId, dtNow));
                if (CheckvideoResource > 0)
                    mongoNews.Filters.Add(getNewsFilter(NewsMediaVideosFitler, mongoNews._id, mongoNews.ReporterId, dtNow));

                foreach (var item in parsedNews.ImageGuids)
                {
                    MResource resource = new MResource();
                    resource.CopyFrom(item);
                    AddResources(mongoBunch, mongoNews, resource, dtNow);
                }
                if (mongoNews.Resources.Count() > 0)
                    mongoNews.ThumbnailUrl = mongoNews.Resources.First().Guid;
            }
            #endregion

            #region NewsInsertion
            mongoBunch.CreationDate = dtNow;
            mongoBunch.LastUpdateDate = mongoBunch.CreationDate;

            News news = new News();
            news.CopyFrom(mongoNews);
            news.SourceNewsUrl = parsedNews.Url;
            news.ThumbnailId = mongoNews.ThumbnailUrl;
            news.IsIndexed = true;
            news.Guid = mongoNews._id;
            if (mongoNews.Locations != null && mongoNews.Locations.Count > 0)
                news.LocationId = mongoNews.Locations.Last().LocationId;
            _iNewsRepository.InsertNewsNoReturn(news);
            #endregion

            #region Tags

            mongoNews.Tags = new List<MTag>();
            if (!string.IsNullOrEmpty(mongoNews.DescriptionText))
            {
                //Work for Related Resources if no resource
                KeywordAnalyzer keywordAnalyzer = new KeywordAnalyzer();
                var keyWordObj = keywordAnalyzer.Analyze(mongoNews.DescriptionText);
                foreach (var item in keyWordObj.Keywords)
                {
                    MTag tag = new MTag();
                    tag.Tag = item.Word.ToLower();
                    tag.Rank = Convert.ToDouble(item.Rank);
                    tag.CreationDate = dtNow;
                    tag.LastUpdateDate = tag.CreationDate;
                    // tag = _iMTagRepository.GetTagCreateIfNotExistOptimized(tag);
                    mongoNews.Tags.Add(tag);
                }
            }

            #endregion Tags

            #region Category Filters

            foreach (var category in mongoNews.Categories)
            {
                Filter catFilter = new Filter();
                catFilter.Name = category.Category;
                catFilter.Value = category.CategoryId;
                catFilter.FilterTypeId = (int)FilterTypes.Category;
                catFilter.CreationDate = dtNow;
                catFilter.LastUpdateDate = catFilter.CreationDate;
                catFilter.IsActive = true;
                catFilter.IsApproved = false;
                if (catFilter.Name == "Others")
                {
                    catFilter.FilterId = 398;
                    catFilter = _iFilterRepository.GetFilterCreateIfNotExistByValue(catFilter);
                }
                else
                    catFilter = _iFilterRepository.GetFilterCreateIfNotExist(catFilter);
                    mongoNews.Filters.Add(getNewsFilter(catFilter, mongoNews._id, mongoNews.ReporterId, dtNow));

                if (catFilter.ParentId.HasValue)
                {
                    var parentFilter = _iFilterRepository.GetFilter(catFilter.ParentId.Value);
                    if (parentFilter != null)
                    {
                        mongoNews.Filters.Add(getNewsFilter(parentFilter, mongoNews._id, mongoNews.ReporterId, dtNow));
                    }
                }
            }

            #endregion Category Filters

            //insert in mongo
            _iMNewsRepository.InsertNews(mongoNews);

            #region AddResourceMeta
            _iResourceService.AddResourceMeta(mongoNews);
            #endregion AddResourceMeta

            int NewsCount = 1;

            #region Sql Related Bunch Creation
            System.Threading.Thread.Sleep(2000);
            List<string> Bunches = _iNewsRepository.GetRelatedNewsV2Load(news.Guid, news.BunchGuid);

            if (Bunches != null && Bunches.Count > 0)
            {
                try
                {
                    Bunches = Bunches.Distinct().ToList();
                    string TopBunch = TopBunch = Bunches.FirstOrDefault();
                    if (Bunches.Count > 1)
                    {
                        int newscount = 0;
                        List<MNews> lst = _iMNewsRepository.GetByBunchGuidByBunchIDs(Bunches);
                        if (lst != null && lst.Count > 0)
                        {
                            foreach (string bunchstr in lst.Select(x => x.BunchGuid).Distinct())
                            {
                                if (lst.Count > newscount)
                                {
                                    newscount = lst.Count(x => x.BunchGuid == bunchstr);
                                    TopBunch = bunchstr;
                                }
                            }
                        }
                    }

                    NewsRepository newsRepo = new NewsRepository();
                    string BunchCsv = String.Join("','", Bunches);
                    BunchCsv = "'" + BunchCsv + "'";
                    List<News> SqlTmpNewsList = newsRepo.GetByBunchIds(BunchCsv);

                    #region Mongo newsrestoration
                    NewsFilterRepository SqlNewsFiltersRepo = new NewsFilterRepository();
                    NewsResourceRepository SqlNewsResourceRepo = new NewsResourceRepository();
                    NewsTagRepository SqlNewstagsRepo = new NewsTagRepository();
                    NewsStatisticsRepository SqlNewsStatisticsRepo = new NewsStatisticsRepository();
                    NewsLocationRepository SqlNewsLocationRepo = new NewsLocationRepository();
                    NewsCategoryRepository SqlNewsCategoryRepo = new NewsCategoryRepository();
                    BunchRepository SqlbunchRepository = new BunchRepository();
                    NewsResourceEditRepository SqlNewsResourceEditRepo = new NewsResourceEditRepository();
                    ResourceEditclipinfoRepository SqlResourceEditclipinfoRepo = new ResourceEditclipinfoRepository();


                    List<MNewsFilter> mNewsFilters = new List<MNewsFilter>();
                    List<MTag> mNewsTags = new List<MTag>();
                    List<MResource> MNewsResources = new List<MResource>();
                    List<Category> MCategories = new List<Category>();
                    List<Location> MLocations = new List<Location>();
                    List<MNewsStatistics> mewsStatisticsList = new List<MNewsStatistics>();
                    List<ResourceEdit> mResourceEdits = new List<ResourceEdit>();
                    List<MNews> mNews = new List<MNews>();
                    List<MBunch> mbunchs = new List<MBunch>();

                    List<News> ArchivalNews = SqlTmpNewsList.Where(x => x.IsArchival != null && Convert.ToBoolean(x.IsArchival)).ToList();
                    List<string> ArchivalBunches = ArchivalNews.Select(x => x.BunchGuid).Distinct().ToList();
                    if (ArchivalBunches != null && ArchivalBunches.Count > 0)
                    {
                        List<string> objBunch = ArchivalBunches.Where(x => x == TopBunch).ToList();
                        if (objBunch != null && objBunch.Count > 0)
                        {
                            Bunch SqlBunch = SqlbunchRepository.GetBunchByGuid(TopBunch);
                            if (SqlBunch != null)
                            {
                                MBunch mbunch = new MBunch();
                                mbunch.CopyFrom(SqlBunch);
                                mbunch._id = TopBunch;
                                mbunch.NewsCount = SqlTmpNewsList.Count + 1;
                                mbunchs.Add(mbunch);
                            }
                        }

                        foreach (News sqlNewsArchival in ArchivalNews)
                        {
                            //Call Mongo restoration method here 
                            #region get from Sql

                            SqlMigration sqlMigration = newsRepo.GetAllFromArchivalByNews(sqlNewsArchival.Guid);
                            List<NewsFilter> NewsFilters = sqlMigration.NewsFilters;
                            List<NewsTag> NewsTags = sqlMigration.NewsTags;
                            List<NewsStatistics> NewsStatisticsList = sqlMigration.NewsStatistics;
                            List<NewsResource> NewsResources = sqlMigration.NewsResources;
                            List<NewsLocation> NewsLocations = sqlMigration.NewsLocations;
                            List<NewsCategory> NewsCategories = sqlMigration.NewsCategories;
                            List<NewsResourceEdit> NewsResourceEdits = sqlMigration.newsResourceEdits;
                            List<ResourceEditclipinfo> ResourceEditclipinfos = sqlMigration.ResourceEditclipinfos;

                            #endregion

                            #region Fill mongo Collections
                            if (NewsFilters != null && NewsFilters.Count > 0)
                            {
                                foreach (NewsFilter newsfilter in NewsFilters)
                                {
                                    MNewsFilter mnewsFilter = new MNewsFilter();
                                    mnewsFilter.CopyFrom(newsfilter);
                                    mnewsFilter.NewsId = newsfilter.NewsGuid;
                                    mNewsFilters.Add(mnewsFilter);
                                }
                            }
                            if (NewsTags != null && NewsTags.Count > 0)
                            {
                                foreach (NewsTag newsTag in NewsTags)
                                {
                                    MTag mNewsTag = new MTag();
                                    mNewsTag.CopyFrom(newsTag);
                                    mNewsTags.Add(mNewsTag);
                                }
                            }
                            if (NewsResources != null && NewsResources.Count > 0)
                            {
                                foreach (NewsResource newsResource in NewsResources)
                                {
                                    MResource mResource = new MResource();
                                    mResource.CopyFrom(newsResource);
                                    MNewsResources.Add(mResource);
                                }
                            }
                            if (NewsCategories != null && NewsCategories.Count > 0)
                            {
                                foreach (NewsCategory newsCategory in NewsCategories)
                                {
                                    Category mCategory = new Category();
                                    mCategory.CopyFrom(newsCategory);
                                    MCategories.Add(mCategory);
                                }
                            }
                            if (NewsLocations != null && NewsLocations.Count > 0)
                            {
                                foreach (NewsLocation newsLocation in NewsLocations)
                                {
                                    Location mLocation = new Location();
                                    mLocation.CopyFrom(newsLocation);
                                    MLocations.Add(mLocation);
                                }
                            }
                            if (NewsStatisticsList != null && NewsStatisticsList.Count > 0)
                            {
                                foreach (NewsStatistics newsStatistics in NewsStatisticsList)
                                {
                                    MNewsStatistics mNewsStatistics = new MNewsStatistics();
                                    mNewsStatistics.CopyFrom(newsStatistics);
                                    mewsStatisticsList.Add(mNewsStatistics);
                                }
                            }

                            if (NewsResourceEdits != null && NewsResourceEdits.Count > 0)
                            {
                                foreach (NewsResourceEdit newsResourceEdit in NewsResourceEdits)
                                {
                                    ResourceEdit mResourceEdit = new ResourceEdit();
                                    mResourceEdit.CopyFrom(newsResourceEdit);
                                    if (ResourceEditclipinfos != null && ResourceEditclipinfos.Count > 0)
                                    {
                                        mResourceEdit.FromTos = new List<FromTo>();
                                        foreach (ResourceEditclipinfo resourceEditclipinfo in ResourceEditclipinfos)
                                        {
                                            FromTo fromTo = new FromTo();
                                            fromTo.CopyFrom(resourceEditclipinfo);
                                            mResourceEdit.FromTos.Add(fromTo);
                                        }
                                    }
                                    mResourceEdits.Add(mResourceEdit);
                                }
                            }

                            MNews mongoNewsInsert = new MNews();
                            mongoNewsInsert.CopyFrom(sqlNewsArchival);
                            mongoNewsInsert._id = sqlNewsArchival.Guid;
                            mongoNewsInsert.Filters = mNewsFilters;
                            mongoNewsInsert.Resources = MNewsResources;
                            mongoNewsInsert.Tags = mNewsTags;
                            mongoNewsInsert.NewsStatistics = mewsStatisticsList;
                            mongoNewsInsert.Locations = MLocations;
                            mongoNewsInsert.Categories = MCategories;
                            if (mResourceEdits != null && mResourceEdits.Count > 0)
                                mongoNewsInsert.ResourceEdit = mResourceEdits[0];
                            mNews.Add(mongoNewsInsert);
                            #endregion
                        }

                        #region Adding news to mongo in bulk
                        try
                        {
                            if (mbunchs.Count > 0)
                                _iMBunchRepository.InsertBunch(mbunchs[0]);
                            //_iMNewsFilterRepository.BulkInsert(mNewsFilters);
                            _iMNewsRepository.BulkInsert(mNews);
                        }
                        catch (Exception ex)
                        {
                            string rawids = string.Join(",", mNews.Select(x => x._id).ToList());
                            throw new Exception("Bunch Count=" + mbunchs.Count + "_NewsCount:" + mNews.Count + "_" + rawids + "_" + ex.Message + "BunchId_" + TopBunch);
                        }
                        #endregion
                    }

                    #endregion

                    Bunches.Add(news.BunchGuid); //adding the fresh news bunch 
                    List<MNews> AllNewsMongo = _iMNewsRepository.GetByBunchIDs(Bunches);
                    if (AllNewsMongo != null && AllNewsMongo.Count > 0)
                    {
                        NewsCount = AllNewsMongo.Count();
                        string NewsCsv = String.Join("','", AllNewsMongo.Select(x => x._id));
                        NewsCsv = "'" + NewsCsv + "'";
                        _iMNewsRepository.UpdateNewsBunchByNewsIds(AllNewsMongo.Select(x => x._id).ToList(), TopBunch);
                        _iNewsRepository.UpdateNewsBunchbyNewsIds(NewsCsv, TopBunch);  //Updating sql news bunch
                        _iMBunchRepository.UpdateBunchNewsCount(TopBunch, NewsCount);

                        #region Top rated marking
                        List<int> BunchSourceCount = AllNewsMongo.GroupBy(x => x.SourceFilterId).Select(y => y.Key).ToList();
                        if (BunchSourceCount.Count > 1)//Top Rated Bunch
                        {
                            foreach (MNews mnews in AllNewsMongo)
                            {
                                if (!mnews.Filters.Select(x => x.FilterId).Contains((int)NewsFilters.TopRated))
                                {
                                    MNewsFilter Newsfilter = new MNewsFilter();
                                    Newsfilter.FilterId = (int)NewsFilters.TopRated;
                                    Newsfilter.NewsId = mnews._id;
                                    Newsfilter.CreationDate = dtNow;
                                    Newsfilter.LastUpdateDate = dtNow;
                                    // _iMNewsFilterRepository.InsertNewsFilter(Newsfilter);   //Check for existing code embeded in call

                                    mnews.Filters.Add(Newsfilter);
                                    mnews.LastUpdateDate = dtNow;
                                    _iMNewsRepository.UpdateFiltersWithDate(mnews);     //update filters in news      
                                }
                            }
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    //Execute Complete News for now
                    mongoBunch.NewsCount = NewsCount;
                    _iMBunchRepository.InsertBunch(mongoBunch);
                    Bunch bunch = new Bunch();
                    bunch.CopyFrom(mongoBunch);
                    bunch.Guid = mongoBunch._id;
                    _iBunchRepository.InsertBunch(bunch);
                    throw;
                }
            }
            else
            {
                mongoBunch.NewsCount = NewsCount;
                _iMBunchRepository.InsertBunch(mongoBunch);
                Bunch bunch = new Bunch();
                bunch.CopyFrom(mongoBunch);
                bunch.Guid = mongoBunch._id;
                _iBunchRepository.InsertBunch(bunch);
            }
            #endregion
            return newsStatus;
        }

        private void AddResources(MBunch mongoBunch, MNews mongoNews, MResource resource, DateTime dtNow)
        {
            var Category = (mongoNews.Categories == null) ? null : mongoNews.Categories.Where(x => x.ParentId == null).FirstOrDefault();
            var Location = (mongoNews.Locations == null) ? null : mongoNews.Locations.Where(x => x.ParentId == null).FirstOrDefault();
            string caption = mongoNews.LanguageCode == "en" ? String.IsNullOrEmpty(resource.Caption) ? mongoNews.Title : resource.Caption : mongoNews.TranslatedTitle;
            resource.Location = Location != null ? Location.Location : null;
            resource.Category = Category != null ? Category.Category : null;
            resource.Caption = caption;
            resource.CreationDate = dtNow;
            resource.LastUpdateDate = resource.CreationDate;


            resource = _iMResourceRepository.GetResourceCreateIfNotExist(resource);
            if (mongoNews.Resources == null)
                mongoNews.Resources = new List<MResource>();
            mongoNews.Resources.Add(resource);
            if (mongoBunch.Resources == null)
                mongoBunch.Resources = new List<MResource>();
            if (!mongoBunch.Resources.Any(x => x.Guid == resource.Guid))
                mongoBunch.Resources.Add(resource);
        }


        private void InsertNews(MNews mongoNews, MBunch mongoBunch, DateTime dtNow, string sourceUrl, PostInput newsInput = null)
        {
            mongoBunch.CreationDate = dtNow;
            mongoBunch.LastUpdateDate = mongoBunch.CreationDate;

            //List<MNews> newsForSimilarMatching = _iMNewsRepository.GetNewsForSimilar(mongoNews.Categories.Select(x => x.CategoryId).ToList(), mongoNews.PublishTime);
            //NMS.SimilarNews.MatchSimilarNews similarNews = new SimilarNews.MatchSimilarNews();
            //string similarBunchId = similarNews.Match(mongoNews, newsForSimilarMatching);
            //if (!string.IsNullOrEmpty(similarBunchId))
            //{
            //    mongoNews.BunchGuid = similarBunchId;
            //    mongoBunch._id = similarBunchId;
            //}

            _iMNewsRepository.InsertNews(mongoNews);
            News news = new News();
            news.CopyFrom(mongoNews);
            news.SourceNewsUrl = sourceUrl;
            news.ThumbnailId = mongoNews.ThumbnailUrl;
            news.Guid = mongoNews._id;

            if (mongoNews.Locations != null && mongoNews.Locations.Count > 0)
                news.LocationId = mongoNews.Locations.Last().LocationId;

            if (newsInput != null)
            {
                news.NewsPaperdescription = newsInput.NewsPaperdescription;
                news.Suggestions = newsInput.Suggestions;
            }
            _iNewsRepository.InsertNews(news);

            InsertBunch(mongoBunch, mongoNews, dtNow);

            Bunch bunch = new Bunch();
            bunch.CopyFrom(mongoBunch);
            bunch.Guid = mongoBunch._id;
            _iBunchRepository.InsertBunch(bunch);
        }

        private MNews TranslateNews(MNews mongoNews, string NewsPaperDesc = "")
        {
            Translator NewsTranslater = new Translator();
            mongoNews.TranslatedTitle = NewsTranslater.Translate(mongoNews.Title, mongoNews.LanguageCode, "en");
            if (!String.IsNullOrEmpty(mongoNews.Slug))
                mongoNews.TranslatedSlug = NewsTranslater.Translate(mongoNews.Slug, mongoNews.LanguageCode, "en");
            if (!String.IsNullOrEmpty(mongoNews.Description))
                mongoNews.TranslatedDescription = NewsTranslater.Translate(mongoNews.Description, mongoNews.LanguageCode, "en");
            else
                mongoNews.TranslatedDescription = NewsTranslater.Translate(NewsPaperDesc, mongoNews.LanguageCode, "en");
            mongoNews.DescriptionText = Regex.Replace(mongoNews.TranslatedDescription, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase);
            return mongoNews;
        }

        public NMS.Core.DataTransfer.Resource.PostInput GenerateResource(Core.Models.ResourceEdit resourceEdit)
        {

            NMS.Core.DataTransfer.Resource.PostInput input = new Core.DataTransfer.Resource.PostInput();
            input.ResourceTypeId = resourceEdit.ResourceTypeId.ToString();
            if (resourceEdit.ResourceTypeId == (int)ResourceTypes.Video)
            {
                resourceEdit.GenenrateFileName();
                string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
                var res = _iResourceService.GetResourceGuidsFromMediaServer(new List<NMS.Core.DataTransfer.Resource.PostInput>() { new NMS.Core.DataTransfer.Resource.PostInput() { FileName = resourceEdit.FileName } }, _storageRoot);
                Guid guid = new Guid();
                if (res != null && res.Count > 0 && Guid.TryParse(res.First().Guid, out guid))
                {
                    _iResourceService.UpdateResourceStatus(guid, 5);
                    resourceEdit.ResourceGuid = guid;
                    //ThreadPool.QueueUserWorkItem(new WaitCallback(EditResourceVideo), new EditResourceWorkArg() { ResourceEdit = resourceEdit, Context = HttpContext.Current });
                    input.CopyFrom(res.First());
                }
            }
            else if (resourceEdit.ResourceTypeId == (int)ResourceTypes.Audio)
            {
                resourceEdit.GenenrateFileName();
                string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
                var res = _iResourceService.GetResourceGuidsFromMediaServer(new List<NMS.Core.DataTransfer.Resource.PostInput>() { new NMS.Core.DataTransfer.Resource.PostInput() { FileName = resourceEdit.FileName } }, _storageRoot);
                Guid guid = new Guid();
                if (res != null && res.Count > 0 && Guid.TryParse(res.First().Guid, out guid))
                {
                    _iResourceService.UpdateResourceStatus(guid, 5);
                    resourceEdit.ResourceGuid = guid;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(EditResourceAudio), new EditResourceWorkArg() { ResourceEdit = resourceEdit, Context = HttpContext.Current });
                    input.CopyFrom(res.First());
                }
            }
            else if (resourceEdit.ResourceTypeId == (int)ResourceTypes.Image)
            {
                resourceEdit.GenenrateFileName();
                string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
                var res = _iResourceService.GetResourceGuidsFromMediaServer(new List<NMS.Core.DataTransfer.Resource.PostInput>() { new NMS.Core.DataTransfer.Resource.PostInput() { FileName = resourceEdit.FileName } }, _storageRoot);
                Guid guid = new Guid();
                if (res != null && res.Count > 0 && Guid.TryParse(res.First().Guid, out guid))
                {
                    _iResourceService.UpdateResourceStatus(guid, 5);
                    resourceEdit.ResourceGuid = guid;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(EditResourceImage), new EditResourceWorkArg() { ResourceEdit = resourceEdit, Context = HttpContext.Current });
                    input.CopyFrom(res.First());
                }
            }
            return input;
        }

        public void EditResourceVideo(object obj)
        {
            EditResourceWorkArg arg = obj as EditResourceWorkArg;
            try
            {
                Core.Models.ResourceEdit resourceEdit = arg.ResourceEdit;
                var channelVideo = _iChannelVideoRepository.GetChannelVideo(resourceEdit.Id);

                CustomSnapshotInput input = new CustomSnapshotInput();
                input.Id = resourceEdit.ResourceGuid.ToString();
                input.ResourceTypeId = resourceEdit.ResourceTypeId;
                input.TimeSpan = resourceEdit.FromTos.First().From;
                input.FilePath = channelVideo.PhysicalPath;
                _iResourceService.GenerateCustomSnapshotOnMediaServer(input);

                string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
                string ffmpegPath = arg.Context.Server.MapPath("~/ffmpeg/");
                FFMPEGLib.FFMPEG.InitialPath = ffmpegPath;
                List<string> files = new List<string>();
                foreach (var fromTos in resourceEdit.FromTos)
                {
                    string filename = _storageRoot + Guid.NewGuid().ToString() + ".mp4";
                    FFMPEGLib.FFMPEG.ClipVideo(channelVideo.PhysicalPath, TimeSpan.FromSeconds(fromTos.From), TimeSpan.FromSeconds(fromTos.To), filename);
                    files.Add(filename);
                }

                if (files.Count > 1)
                {
                    FFMPEGLib.FFMPEG.ConcatenateVideos(files, _storageRoot + resourceEdit.ResourceGuid.ToString() + ".mp4");
                    foreach (string file in files)
                    {
                        File.Delete(file);
                    }
                }
                else
                {
                    File.Move(files[0], _storageRoot + resourceEdit.ResourceGuid.ToString() + ".mp4");
                }
                _iResourceService.UpdateResourceStatus(resourceEdit.ResourceGuid, 1);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
            }
        }

        public void EditResourceAudio(object obj)
        {
            EditResourceWorkArg arg = obj as EditResourceWorkArg;
            try
            {
                Core.Models.ResourceEdit resourceEdit = arg.ResourceEdit;
                var radionStream = _iRadioStreamRepository.GetRadioStream(resourceEdit.Id);

                CustomSnapshotInput input = new CustomSnapshotInput();
                input.Id = resourceEdit.ResourceGuid.ToString();
                input.ResourceTypeId = resourceEdit.ResourceTypeId;
                input.TimeSpan = resourceEdit.FromTos.First().From;
                input.FilePath = radionStream.PhysicalPath;
                _iResourceService.GenerateCustomSnapshotOnMediaServer(input);

                string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
                string ffmpegPath = arg.Context.Server.MapPath("~/ffmpeg/");
                FFMPEGLib.FFMPEG.InitialPath = ffmpegPath;
                List<string> files = new List<string>();
                foreach (var fromTos in resourceEdit.FromTos)
                {
                    string filename = _storageRoot + Guid.NewGuid().ToString() + ".mp3";
                    FFMPEGLib.FFMPEG.ClipVideo(radionStream.PhysicalPath, TimeSpan.FromSeconds(fromTos.From), TimeSpan.FromSeconds(fromTos.To), filename);
                    files.Add(filename);
                }

                if (files.Count > 1)
                {
                    FFMPEGLib.FFMPEG.ConcatenateVideos(files, _storageRoot + resourceEdit.ResourceGuid.ToString() + ".mp3", true);
                    foreach (string file in files)
                    {
                        File.Delete(file);
                    }
                }
                else
                {
                    File.Move(files[0], _storageRoot + resourceEdit.ResourceGuid.ToString() + ".mp3");
                }
                _iResourceService.UpdateResourceStatus(resourceEdit.ResourceGuid, 1);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
            }
        }

        public void EditResourceImage(object obj)
        {
            EditResourceWorkArg arg = obj as EditResourceWorkArg;
            try
            {
                Core.Models.ResourceEdit resourceEdit = arg.ResourceEdit;
                var newsPaperPage = _iNewsPaperPageRepository.GetNewsPaperPage(resourceEdit.Id);

                string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
                string _storageurl = ConfigurationManager.AppSettings["MediaServerUrl"];
                string filepath = _storageurl + "/getresource/" + newsPaperPage.ImageGuid;

                //Kindly See from below code
                string filename = _storageRoot + resourceEdit.ResourceGuid.ToString() + ".jpg";

                //testing path
                System.Net.WebClient client = new System.Net.WebClient();
                MemoryStream memStream = new MemoryStream(client.DownloadData(filepath));
                Image img = Image.FromStream(memStream);
                Image CroppedIMage = CropImage(img, Convert.ToInt32(resourceEdit.Bottom) - Convert.ToInt32(resourceEdit.Top), Convert.ToInt32(resourceEdit.Right) - Convert.ToInt32(resourceEdit.Left), Convert.ToInt32(resourceEdit.Top), Convert.ToInt32(resourceEdit.Left));
                CroppedIMage.Save(filename);
                _iResourceService.GenerateSnapshotOnMediaServer(resourceEdit.ResourceGuid);
                _iResourceService.UpdateResourceStatus(resourceEdit.ResourceGuid, 1);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
            }
        }

        //The crop image sub
        public static System.Drawing.Image CropImage(System.Drawing.Image Image, int Height, int Width, int StartAtX, int StartAtY)
        {
            Image outimage;
            MemoryStream mm = null;
            try
            {
                //check the image height against our desired image height
                if (Image.Height < Height)
                {
                    Height = Image.Height;
                }

                if (Image.Width < Width)
                {
                    Width = Image.Width;
                }

                //create a bitmap window for cropping
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                //bmPhoto.SetResolution(72, 72);

                //create a new graphics object from our image and set properties
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //now do the crop
                grPhoto.DrawImage(Image, new Rectangle(0, 0, Width, Height), StartAtY, StartAtX, Width, Height, GraphicsUnit.Pixel);

                // Save out to memory and get an image from it to send back out the method.
                mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                Image.Dispose();
                bmPhoto.Dispose();
                grPhoto.Dispose();
                outimage = Image.FromStream(mm);

                return outimage;
            }
            catch (Exception ex)
            {
                throw new Exception("Error cropping image, the error was: " + ex.Message);
            }
        }

        public bool CheckIfNewsExists(RawNews parsedNews, out InsertNewsStatus newsStatus)
        {
           // MNews mongoNews = new MNews();
            bool status = false;
            newsStatus = InsertNewsStatus.Inserted;
           // mongoNews.Title = parsedNews.Title;
           // mongoNews.SourceTypeId = (int)parsedNews.SourceType;
           // var filter = _iFilterRepository.GetFilterByNameAndFilterType(parsedNews.SourceType, parsedNews.Source);
            //if (filter != null)
            //{
            //    mongoNews.SourceFilterId = filter.FilterId;
            //}
            List<NewsFile> lst = _iNewsFileRepository.Getnewsfilebytitleandsource(parsedNews.Title, parsedNews.Source);

            if (lst != null)
            {
                status = true;
                if (status == true)
                    newsStatus = InsertNewsStatus.DuplicateNews;
            }
            return status;
        }

        public bool CheckIfNewsExistsSQL(RawNews parsedNews, out InsertNewsStatus newsStatus)
        {
            //MNews mongoNews = new MNews();
            bool status = false;
            newsStatus = InsertNewsStatus.Inserted;
            //mongoNews.Title = parsedNews.Title;
            //mongoNews.SourceTypeId = (int)parsedNews.SourceType;

            //if (_iFilterRepository.GetFilterByNameAndFilterType(parsedNews.SourceType, parsedNews.Source).FilterId != null)
            //{
            //    mongoNews.SourceFilterId = _iFilterRepository.GetFilterByNameAndFilterType(parsedNews.SourceType, parsedNews.Source).FilterId;
            //}
            status = _iNewsRepository.CheckIfNewsExistsSQL(parsedNews.Title, parsedNews.Source);
            if (status == true)
                newsStatus = InsertNewsStatus.DuplicateNews;
            return status;
        }

        private bool CheckNewsDescrepencyMarking(RawNews rawNews, DateTime dtNow, out InsertNewsStatus newsStatus)
        {
            newsStatus = InsertNewsStatus.Inserted;
            foreach (var category in rawNews.Categories)
            {
                var cat = _iCategoryRepository.GetByAlias(category);
                if (cat == null)
                {
                    //mdescrepencynews descrepency = new mdescrepencynews();
                    //descrepency.rawnews = rawnews;
                    //descrepency.creationdate = dtnow;
                    //descrepency.lastupdatedate = descrepency.creationdate;
                    //descrepency.descrepencytype = (int)descrepencytype.category;
                    //descrepency.descrepencyvalue = category;
                    //descrepency.title = rawnews.title;
                    //descrepency.source = rawnews.source;
                    //descrepency.isinserted = true;
                    //_imdescrepencynewsrepository.insert(descrepency);
                    //newsstatus = insertnewsstatus.discrepancycategory;
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(rawNews.Location))
            {
                var loc = _iLocationRepository.GetByAlias(rawNews.Location);
                if (loc == null)
                {
                    //MDescrepencyNews descrepency = new MDescrepencyNews();
                    //descrepency.RawNews = rawNews;
                    //descrepency.CreationDate = dtNow;
                    //descrepency.Title = rawNews.Title;
                    //descrepency.Source = rawNews.Source;
                    //descrepency.LastUpdateDate = descrepency.CreationDate;
                    //descrepency.DescrepencyType = (int)DescrepencyType.Location;
                    //descrepency.DescrepencyValue = rawNews.Location;
                    //descrepency.IsInserted = true;
                    //_iMDescrepencyNewsRepository.Insert(descrepency);
                    //newsStatus = InsertNewsStatus.DiscrepancyLocation;
                    return false;
                }
            }
            return true;
        }

        private MNewsFilter getNewsFilter(Filter filter, string newsId, int reporterId, DateTime dtNow)
        {
            MNewsFilter newsFilter = new MNewsFilter();
            newsFilter.CreationDate = dtNow;
            newsFilter.LastUpdateDate = dtNow;
            newsFilter.NewsId = newsId;
            newsFilter.FilterId = filter.FilterId;
            newsFilter.FilterTypeId = filter.FilterTypeId;
            newsFilter.ReporterId = reporterId;
            return newsFilter;
        }

        private void InsertBunch(MBunch bunch, MNews news, DateTime dtNow, bool insertFiltersInNews = true)
        {
            // InsertBunchFilters(bunch._id, news, insertFiltersInNews);
            //InsertBunchTags(bunch._id, dtNow, news);
            //// if (news.IsSearchable)
            //     InsertBunchNews(bunch._id, news);
            //if(_iMBunchRepository.GetByGuid(
            bunch.NewsCount = 1;
            _iMBunchRepository.InsertBunch(bunch);
        }

        public void InsertBunchFilters(string bunchId, MNews news, bool insertFiltersInNews = true, bool IsTopRated = false)
        {
            foreach (var filter in news.Filters)
            {
                MBunchFilter bunchFilter = new MBunchFilter();
                bunchFilter.BunchId = bunchId;
                bunchFilter.FilterId = filter.FilterId;
                bunchFilter.CreationDate = filter.CreationDate;
                bunchFilter.LastUpdateDate = filter.CreationDate;
                bunchFilter.NewsList = new List<MNews>();
                if (news.IsSearchable)
                    bunchFilter.NewsList.Add(news.GetAsUpdateNews());
                _iMBunchFilterRepository.InsertBunchFilter(bunchFilter);
                if (insertFiltersInNews)
                    _iMNewsFilterRepository.InsertNewsFilter(filter);
            }
        }

        public void InsertBunchTags(string bunchId, DateTime dtNow, MNews news)
        {
            foreach (var tag in news.Tags)
            {
                MBunchTag bunchTag = new MBunchTag();
                bunchTag.BunchId = bunchId;
                bunchTag.TagId = tag._id;
                bunchTag.CreationDate = dtNow;
                bunchTag.LastUpdateDate = bunchTag.CreationDate;
                _iMBunchTagRepository.InsertBunchTag(bunchTag);
            }
        }

        public void InsertBunchNews(string bunchId, MNews news)
        {
            MBunchNews bunchNews = new MBunchNews();
            bunchNews.BunchId = bunchId;
            bunchNews.NewsId = news._id;
            bunchNews.CreationDate = news.CreationDate;
            bunchNews.LastUpdateDate = news.CreationDate;
            _iMBunchNewsRepository.InsertBunchNews(bunchNews);
        }

        public List<MNews> GetReportedNews(int reportedId, int pageCount, int startIndex)
        {
            return _iMNewsRepository.GetReportedNews(reportedId, pageCount, startIndex);
        }

        public List<MNewsFilter> GetUpdatedNewsFilters(DateTime lastUpdateDate, int? reporterId = null, List<string> newsIds = null)
        {
            return _iMNewsFilterRepository.GetUpdatedNewsFilters(lastUpdateDate, reporterId, newsIds);
        }




        public List<MNews> GetNewsByFilterIds(List<int> filterIds, int pageCount, int startIndex, DateTime from, DateTime to, string term)
        {
            //_iNewsCacheRepository.GetNews(filterIds)
            List<MBunchFilter> newsList = _iMBunchFilterRepository.GetNewsByFilterIds(filterIds, pageCount, startIndex, from, to, term);

            List<MNews> newsDic = new List<MNews>();
            foreach (var bunchFilter in newsList)
            {
                foreach (var news in bunchFilter.NewsList)
                {
                    if (newsDic.Where(x => x.BunchGuid == news.BunchGuid).Count() < 4)
                    {
                        newsDic.Add(news);
                    }
                }
            }
            var newsLst = _iMNewsRepository.GetNewsByIDs(newsDic.Select(x => x._id).ToList());
            return newsLst;
        }

        public List<MNews> GetNewsByFilterIds(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null)
        {
            List<MBunchFilter> listNews = _iMBunchFilterRepository.GetNewsByFilterIds(filters, pageCount, startIndex, from, to, term, discaredFilters);
            if (discaredFilters == null)
                discaredFilters = new List<Filter>();
            List<MNews> newsDic = new List<MNews>();
            foreach (var bunchFilter in listNews)
            {
                foreach (var news in bunchFilter.NewsList)
                {
                    int newsCount = newsDic.Where(x => x.BunchGuid == news.BunchGuid).Count();
                    if (newsCount < 4 && IsValidByFilterNews(filters, news, listNews))
                        newsDic.Add(news);
                }
            }
            var newsLst = _iMNewsRepository.GetNewsByIDs(newsDic.Select(x => x._id).ToList());
            // newsLst = newsLst.Where(z => !z.Filters.Any(x => discaredFilters.Where(y => y.FilterId == x.FilterId).Count() > 0)).ToList();
            return newsLst;
        }

        private LoadNews GetNewsAndBunch(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false)
        {
            LoadNews loadNews = new LoadNews();
            List<MBunchFilter> listNews = _iMBunchFilterRepository.GetNewsByFilterIds(filters, pageCount, startIndex, from, to, term, discaredFilters);
            if (discaredFilters == null)
                discaredFilters = new List<Filter>();
            List<MNews> newsDic = new List<MNews>();
            List<MBunch> MBunchDic = new List<MBunch>();
            MBunchDic = _iMBunchRepository.GetByIDs(listNews.Select(x => x.BunchId).ToList());
            foreach (var bunchFilter in listNews)
            {
                foreach (var news in bunchFilter.NewsList)
                {
                    int newsCount = newsDic.Where(x => x.BunchGuid == news.BunchGuid).Count();
                    //if (newsCount < 4 && IsValidByFilterNews(filters, news, listNews))
                    if (newsCount < 4)
                        newsDic.Add(news);
                }
            }
            var newsLst = _iMNewsRepository.GetNewsByIDs(newsDic.Select(x => x._id).ToList());
            // newsLst = newsLst.Where(z => !z.Filters.Any(x => discaredFilters.Where(y => y.FilterId == x.FilterId).Count() > 0)).ToList();
            foreach (var news in newsLst)
            {
                news.PublishTime = news.PublishTime.Date.Add(news.LastUpdateDate.TimeOfDay);
            }

            loadNews.News = new List<GetOutput>();
            loadNews.News.CopyFrom(newsLst);
            loadNews.Bunchs = new List<Core.DataTransfer.Bunch.GetOutput>();
            loadNews.Bunchs.CopyFrom(MBunchDic);
            return loadNews;
        }

        public LoadNews GetNewsAndBunchOptimized(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false)
        {
            LoadNews loadNews = new LoadNews();
            loadNews.News = new List<GetOutput>();
            loadNews.Bunchs = new List<Core.DataTransfer.Bunch.GetOutput>();

            List<int> intdfilters = discaredFilters.Select(x => x.FilterId).ToList();
            List<MNews> stNews = _iMNewsRepository.GetAllBunchesByDateAndFilters(filters, pageCount, startIndex, from, to, intdfilters);
            if (discaredFilters == null)
                discaredFilters = new List<Filter>();
            List<MBunch> MBunchDic = new List<MBunch>();
            if (stNews != null && stNews.Count > 0)
            {
                MBunchDic = _iMBunchRepository.GetByIDs(stNews.Select(x => x.BunchGuid).ToList());

                if (MBunchDic.Count > 0)
                {
                    //stNews = _iMNewsRepository.GetByBunchIDsWithFilters(MBunchDic.Select(x => x._id).ToList(), filters, pageCount, startIndex, from, to, intdfilters);

                    //foreach (var news in stNews)
                    //{
                    //    int newsCount = newsDic.Where(x => x.BunchGuid == news.BunchGuid).Count();
                    //    //if (newsCount < 4 && IsValidByFilterNews(filters, news, listNews))
                    //    if (newsCount < 4)
                    //        newsDic.Add(news);
                    //}
                    //var newsLst = _iMNewsRepository.GetNewsByIDs(newsDic.Select(x => x._id).ToList());
                    // newsLst = newsLst.Where(z => !z.Filters.Any(x => discaredFilters.Where(y => y.FilterId == x.FilterId).Count() > 0)).ToList();
                    //No need now
                    //foreach (var news in stNews)
                    //{
                    //    news.PublishTime = news.PublishTime.Date.Add(news.LastUpdateDate.TimeOfDay);
                    //}
                    loadNews.News.CopyFrom(stNews);
                    loadNews.Bunchs.CopyFrom(MBunchDic);
                }
            }
            else
            {
                #region Get from Sql
                if ((DateTime.UtcNow - from).Days > 29)
                {
                    List<MNews> tmpNews = _iMNewsRepository.GetAllBunchesByDateAndFilters(filters, 1000000, 0, from, to, intdfilters);
                    int indexCount = 0;
                    if (tmpNews != null && tmpNews.Count > 0)
                        indexCount = tmpNews.Count;

                    BunchRepository brepo = new BunchRepository();
                    NewsRepository newsrepo = new NewsRepository();
                    List<Bunch> SqlBunchDic = new List<Bunch>();
                    List<News> SqlnewsDic = new List<News>();
                    NewsFilterRepository nfrepo = new NewsFilterRepository();
                    NewsLocationRepository nlrepo = new NewsLocationRepository();
                    NewsCategoryRepository ncrepo = new NewsCategoryRepository();
                    NewsResourceRepository nrrepo = new NewsResourceRepository();
                    NewsTagRepository ntrepo = new NewsTagRepository();

                    loadNews = new LoadNews();

                    string filtercsv = string.Empty;
                    string discaredFilterscsv = string.Empty;
                    if (filters != null && filters.Count > 0)
                        filtercsv = String.Join(",", filters.Select(x => x.FilterId));
                    if (discaredFilters != null && discaredFilters.Count > 0)
                        discaredFilterscsv = String.Join(",", discaredFilters.Select(x => x.FilterId));

                    SqlnewsDic = newsrepo.GetByDateAndfilters(filtercsv, pageCount, startIndex - indexCount, from, to, discaredFilterscsv);
                    if (SqlnewsDic != null && SqlnewsDic.Count > 0)
                    {
                        string bunchcsv = "";
                        string bunchcsvReplaced = "";
                        List<string> bunches = SqlnewsDic.Select(x => x.BunchGuid).Distinct().ToList();
                        if (bunches.Count > 1)
                        {
                            bunchcsv = String.Join(",", bunches);
                            bunchcsv = "'" + bunchcsv;
                            bunchcsvReplaced = bunchcsv.Replace(",", "','");
                            bunchcsvReplaced = bunchcsvReplaced + "'";
                        }
                        else if (bunches.Count == 1)
                            bunchcsvReplaced = "'" + bunches[0] + "'";

                        SqlBunchDic = brepo.GetByBunchIds(bunchcsvReplaced);
                        loadNews.News = new List<GetOutput>();
                        loadNews.News.CopyFrom(SqlnewsDic);

                        foreach (GetOutput mnews in loadNews.News)
                        {
                            List<NewsFilter> newsFilters = nfrepo.GetNewsFilterByNewsGuid(mnews._id);
                            if (newsFilters != null && newsFilters.Count > 0)
                            {
                                mnews.Filters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                                foreach (NewsFilter newsFilter in newsFilters)
                                {
                                    Core.DataTransfer.NewsFilter.GetOutput mNewsFilter = new Core.DataTransfer.NewsFilter.GetOutput();
                                    mNewsFilter.CopyFrom(newsFilters);
                                    mNewsFilter.NewsId = mnews._id;
                                    mnews.Filters.Add(mNewsFilter);
                                }
                            }
                            else
                            {
                                if (mnews.Filters == null || mnews.Filters.Count == 0)
                                {
                                    Core.DataTransfer.NewsFilter.GetOutput filter = new Core.DataTransfer.NewsFilter.GetOutput();
                                    filter.FilterId = 78;
                                    filter.NewsId = mnews._id;
                                    mnews.Filters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                                    mnews.Filters.Add(filter);
                                }
                            }

                            List<NewsLocation> newsLocation = nlrepo.GetNewsLocationByNewsGuid(mnews._id);
                            if (newsLocation != null && newsLocation.Count > 0)
                            {
                                mnews.Locations = new List<Core.DataTransfer.Location.GetOutput>();
                                mnews.Locations.CopyFrom(newsLocation);
                            }

                            List<NewsResource> newsResource = nrrepo.GetByNewsGuid(mnews._id);
                            if (newsResource != null && newsResource.Count > 0)
                            {
                                mnews.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                                mnews.Resources.CopyFrom(newsResource);
                            }

                            List<NewsTag> newsTag = ntrepo.GetByNewsGuid(mnews._id);
                            if (newsTag != null && newsTag.Count > 0)
                            {
                                mnews.Tags = new List<Core.DataTransfer.Tag.GetOutput>();
                                mnews.Tags.CopyFrom(newsTag);
                            }

                            List<NewsCategory> newsCategory = ncrepo.GetByNewsGuid(mnews._id);
                            if (newsCategory != null && newsCategory.Count > 0)
                            {
                                mnews.Categories = new List<Core.DataTransfer.Category.GetOutput>();
                                mnews.Categories.CopyFrom(newsCategory);
                            }
                            else
                            {
                                if (mnews.Categories == null || mnews.Categories.Count == 0)
                                {
                                    Core.DataTransfer.Category.GetOutput Category = new Core.DataTransfer.Category.GetOutput();
                                    Category.CategoryId = 1;
                                    mnews.Categories = new List<Core.DataTransfer.Category.GetOutput>();
                                    mnews.Categories.Add(Category);
                                }
                            }
                        }
                        loadNews.Bunchs = new List<Core.DataTransfer.Bunch.GetOutput>();
                        loadNews.Bunchs.CopyFrom(SqlBunchDic);
                    }
                }
                #endregion
            }
            return loadNews;
        }

        public LoadNews GetNewsAndBunchWithArchival(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false)
        {
            LoadNews loadNews = new LoadNews();
            if (IsArchivalSearch)
            {
                BunchFilterNewsRepository bfnrepo = new BunchFilterNewsRepository();
                string filtercsv = string.Empty;
                string discaredFilterscsv = string.Empty;
                if (filters != null && filters.Count > 0)
                    filtercsv = String.Join(",", filters.Select(x => x.FilterId));
                if (discaredFilters != null && discaredFilters.Count > 0)
                    discaredFilterscsv = String.Join(",", discaredFilters.Select(x => x.FilterId));

                List<BunchFilterNews> SqlBunchfiltersNews = bfnrepo.GetByDateAndfilters(filtercsv, pageCount, startIndex, from, to, term, discaredFilterscsv);
                string NewsCsv = "";
                string BunchCsv = "";

                if (SqlBunchfiltersNews.Count > 0)
                {
                    List<string> bunches = SqlBunchfiltersNews.Select(x => x.BunchGuid).Distinct().ToList();
                    NewsCsv = String.Join("','", SqlBunchfiltersNews.Select(x => x.Newsguid).Distinct().ToList());
                    BunchCsv = String.Join("','", bunches);
                    BunchCsv = "'" + BunchCsv + "'";
                    NewsCsv = "'" + NewsCsv + "'";

                    BunchRepository brepo = new BunchRepository();
                    NewsRepository newsrepo = new NewsRepository();
                    List<Bunch> SqlBunchDic = new List<Bunch>();
                    List<News> SqlnewsDic = new List<News>();
                    NewsFilterRepository nfrepo = new NewsFilterRepository();
                    NewsLocationRepository nlrepo = new NewsLocationRepository();
                    NewsCategoryRepository ncrepo = new NewsCategoryRepository();
                    NewsResourceRepository nrrepo = new NewsResourceRepository();
                    NewsTagRepository ntrepo = new NewsTagRepository();

                    SqlBunchDic = brepo.GetByBunchIds(BunchCsv);
                    SqlnewsDic = newsrepo.GetByNewsGuids(NewsCsv);

                    List<News> SQLSelectedNews = new List<News>();
                    foreach (string bunchguid in bunches)
                    {
                        SQLSelectedNews.AddRange(SqlnewsDic.Where(x => x.BunchGuid == bunchguid).Take(3).ToList());
                    }
                    loadNews.News = new List<GetOutput>();
                    loadNews.News.CopyFrom(SQLSelectedNews);

                    foreach (GetOutput mnews in loadNews.News)
                    {
                        List<NewsFilter> newsFilters = nfrepo.GetNewsFilterByNewsGuid(mnews._id);
                        if (newsFilters != null && newsFilters.Count > 0)
                        {
                            mnews.Filters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                            mnews.Filters.CopyFrom(newsFilters);
                        }
                        else
                        {
                            if (mnews.Filters == null || mnews.Filters.Count == 0)
                            {
                                Core.DataTransfer.NewsFilter.GetOutput filter = new Core.DataTransfer.NewsFilter.GetOutput();
                                filter.FilterId = 78;
                                mnews.Filters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                                mnews.Filters.Add(filter);
                            }
                        }

                        List<NewsLocation> newsLocation = nlrepo.GetNewsLocationByNewsGuid(mnews._id);
                        if (newsLocation != null && newsLocation.Count > 0)
                        {
                            mnews.Locations = new List<Core.DataTransfer.Location.GetOutput>();
                            mnews.Locations.CopyFrom(newsLocation);
                        }

                        List<NewsResource> newsResource = nrrepo.GetByNewsGuid(mnews._id);
                        if (newsResource != null && newsResource.Count > 0)
                        {
                            mnews.Resources = new List<Core.DataTransfer.Resource.GetOutput>();
                            mnews.Resources.CopyFrom(newsResource);
                        }

                        List<NewsTag> newsTag = ntrepo.GetByNewsGuid(mnews._id);
                        if (newsTag != null && newsTag.Count > 0)
                        {
                            mnews.Tags = new List<Core.DataTransfer.Tag.GetOutput>();
                            mnews.Tags.CopyFrom(newsTag);
                        }

                        List<NewsCategory> newsCategory = ncrepo.GetNewsCategoryByNewsGuid(mnews._id);
                        if (newsCategory != null && newsCategory.Count > 0)
                        {
                            mnews.Categories = new List<Core.DataTransfer.Category.GetOutput>();
                            mnews.Categories.CopyFrom(newsCategory);
                        }
                        else
                        {
                            if (mnews.Categories == null || mnews.Categories.Count == 0)
                            {
                                Core.DataTransfer.Category.GetOutput Category = new Core.DataTransfer.Category.GetOutput();
                                Category.CategoryId = 1;
                                mnews.Categories = new List<Core.DataTransfer.Category.GetOutput>();
                                mnews.Categories.Add(Category);
                            }
                        }
                    }
                    loadNews.Bunchs = new List<Core.DataTransfer.Bunch.GetOutput>();
                    loadNews.Bunchs.CopyFrom(SqlBunchDic);
                    if (loadNews.News.Where(x => x.Title == "Saudi Arabia invasion in Yemen to create risks for whole region: Iran").ToList().Count > 0)
                    {

                    }
                }
            }


            List<MBunchFilter> listNews = _iMBunchFilterRepository.GetNewsByFilterIds(filters, pageCount, startIndex, from, to, term, discaredFilters);
            List<MNews> newsDic = new List<MNews>();
            List<MBunch> MBunchDic = new List<MBunch>();
            MBunchDic = _iMBunchRepository.GetByIDs(listNews.Select(x => x.BunchId).ToList());
            foreach (var bunchFilter in listNews)
            {
                foreach (var news in bunchFilter.NewsList)
                {
                    int newsCount = newsDic.Where(x => x.BunchGuid == news.BunchGuid).Count();
                    //if (newsCount < 4 && IsValidByFilterNews(filters, news, listNews))
                    if (newsCount < 4)
                        newsDic.Add(news);
                }
            }
            var newsLst = _iMNewsRepository.GetNewsByIDs(newsDic.Select(x => x._id).ToList());
            // newsLst = newsLst.Where(z => !z.Filters.Any(x => discaredFilters.Where(y => y.FilterId == x.FilterId).Count() > 0)).ToList();
            foreach (var news in newsLst)
            {
                news.PublishTime = news.PublishTime.Date.Add(news.LastUpdateDate.TimeOfDay);
            }
            if (loadNews.News != null && loadNews.News.Count > 0)
            {
                if (newsLst.Where(x => x.Title == "Saudi Arabia invasion in Yemen to create risks for whole region: Iran").ToList().Count > 0)
                {

                }

                List<GetOutput> mOutputNewslist = new List<GetOutput>();
                List<Core.DataTransfer.Bunch.GetOutput> mOutputbunchlist = new List<Core.DataTransfer.Bunch.GetOutput>();
                mOutputNewslist.CopyFrom(newsLst);
                mOutputbunchlist.CopyFrom(MBunchDic);
                loadNews.News.AddRange(mOutputNewslist);
                loadNews.Bunchs.AddRange(mOutputbunchlist);
            }
            else
            {
                loadNews.News = new List<GetOutput>();
                loadNews.News.CopyFrom(newsLst);
                loadNews.Bunchs = new List<Core.DataTransfer.Bunch.GetOutput>();
                loadNews.Bunchs.CopyFrom(MBunchDic);
            }

            return loadNews;
        }

        public LoadNews GetNewsAndBunchCached(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null)
        {
            LoadNews loadNews = _iNewsCacheRepository.GetNewsAndBunch(filters, pageCount, startIndex, from, to, term, discaredFilters);
            if (loadNews == null)
            {
                loadNews = this.GetNewsAndBunchOptimized(filters, pageCount, startIndex, from, to, term, discaredFilters);
                _iNewsCacheRepository.UpdateCache(filters, pageCount, startIndex, from, to, term, discaredFilters, loadNews);
            }
            return loadNews;
        }

        public void UpdateGetNewsAndBunchCache()
        {
            var lst = _iNewsCacheRepository.GetAllKeys();
            if (lst != null && lst.Count > 0)
            {
                foreach (var item in lst)
                {
                    if (DateTime.UtcNow.Subtract(item.DateUpdated).Minutes > 2)
                    {
                        var loadNews = this.GetNewsAndBunchOptimized(item.filters, item.pageCount, item.startIndex, DateTime.UtcNow.AddMonths(-1), DateTime.UtcNow, item.term, item.discaredFilters);
                        _iNewsCacheRepository.UpdateCache(item.filters, item.pageCount, item.startIndex, item.from, item.to, item.term, item.discaredFilters, loadNews);
                    }
                }
            }
        }

        public LoadNews GetNewsAndBunchCache(List<Filter> filters, int pageCount, int startIndex, string from, string to, string term, List<Filter> discaredFilters = null, bool IsArchivalSearch = false)
        {
            SolrService<SolarNews> sService = new SolrService<SolarNews>();
            try
            {
                sService.Connect(ConfigurationManager.AppSettings["solrUrl"].ToString());
            }
            catch (Exception ex)
            {
            }

            string filterQuery = "";
            string strAnd = "";

            foreach (int[] lst in _iMNewsRepository.GetFilterGroups(filters))
            {
                filterQuery += strAnd + " filters : (" + string.Join(" ", lst) + ")";
                strAnd = "AND";
            }

            if (!IsArchivalSearch)
                filterQuery += " AND isarchival:" + IsArchivalSearch.ToString().ToLower();

            filterQuery += " AND lastupdatedate:[" + from.ToString() + " TO " + DateTime.UtcNow.AddYears(1).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "]";
            // string query = filterQuery;
            string searchTitle = string.Empty;
            string searchQuery1 = string.Empty;
            string searchDescription = string.Empty;

            filterQuery += " AND " + sService.GenerateQuery(term, new List<string>() { "tags", "slug", "title", "description" }) + "";

            //SolrManager.OutputEntities.SearchResult<SolarNews> snews = sService.SearchEDismax(term + filterQuery, pageCount, startIndex + 1, term, "slug^3 title^2 description", new List<SolrNet.SortOrder> { new SolrNet.SortOrder("score", SolrNet.Order.DESC), new SolrNet.SortOrder("publishdate", SolrNet.Order.DESC) });
            SolrManager.OutputEntities.SearchResult<SolarNews> snews = sService.QuerySolr(filterQuery, pageCount, startIndex, new List<SolrNet.SortOrder> { new SolrNet.SortOrder("lastupdatedate", SolrNet.Order.DESC), new SolrNet.SortOrder("score", SolrNet.Order.DESC) }, true);
            List<SolarNews> sResultNews = new List<SolarNews>();
            LoadNews loadNews = null;
            if (snews != null && snews.Results != null && snews.Results.Count > 0)
            {
                NewsRepository newsrepo = new NewsRepository();
                //Need to add loop and assig score
                loadNews = new LoadNews();
                sResultNews = snews.Results;
                List<string> NewsList = sResultNews.Select(x => x._id).Distinct().ToList();
                string newsCsv = "";
                string NewscsvReplaced = "";
                if (NewsList.Count > 1)
                {
                    newsCsv = String.Join(",", NewsList);
                    newsCsv = "'" + newsCsv;
                    NewscsvReplaced = newsCsv.Replace(",", "','");
                    NewscsvReplaced = NewscsvReplaced + "'";
                }
                else if (NewsList.Count == 1)
                    NewscsvReplaced = "'" + NewsList[0] + "'";

                List<string> bunches = newsrepo.GetBunchesByNewsGuids(NewscsvReplaced).Select(x => x.BunchGuid).Where(x => !string.IsNullOrEmpty(x)).ToList();

                List<MBunch> MBunchDic = new List<MBunch>();
                List<MNews> newsDic = new List<MNews>();
                if (IsArchivalSearch)
                {
                    #region Archival Code
                    BunchRepository brepo = new BunchRepository();
                    List<Bunch> SqlBunchDic = new List<Bunch>();
                    List<News> SqlnewsDic = new List<News>();
                    NewsFilterRepository nfrepo = new NewsFilterRepository();
                    NewsLocationRepository nlrepo = new NewsLocationRepository();
                    NewsCategoryRepository ncrepo = new NewsCategoryRepository();

                    string bunchcsv = "";
                    string bunchcsvReplaced = "";
                    if (bunches.Count > 1)
                    {
                        bunchcsv = String.Join(",", bunches);
                        bunchcsv = "'" + bunchcsv;
                        bunchcsvReplaced = bunchcsv.Replace(",", "','");
                        bunchcsvReplaced = bunchcsvReplaced + "'";
                    }
                    else if (bunches.Count == 1)
                        bunchcsvReplaced = "'" + bunches[0] + "'";

                    SqlBunchDic = brepo.GetByBunchIds(bunchcsvReplaced);
                    SqlnewsDic = newsrepo.GetByBunchIds(bunchcsvReplaced);

                    int i = 0;
                    foreach (SolarNews rNews in sResultNews)
                    {
                        News news = SqlnewsDic.Where(x => x.Guid.ToLower() == rNews._id.ToLower()).FirstOrDefault();
                        if (news != null)
                        {
                            news.Score = snews.TotalCount - (startIndex + i);
                        }
                        i++;
                    }
                    List<News> SQLSelectedNews = new List<News>();
                    foreach (string bunchguid in bunches)
                    {
                        SQLSelectedNews.AddRange(SqlnewsDic.Where(x => x.Score > 0 && x.BunchGuid == bunchguid).Take(3).ToList());
                    }
                    SQLSelectedNews = SQLSelectedNews.OrderByDescending(x => x.Score).ToList();
                    loadNews.News = new List<GetOutput>();
                    loadNews.News.CopyFrom(SQLSelectedNews);

                    foreach (GetOutput mnews in loadNews.News)
                    {
                        List<NewsFilter> newsFilters = nfrepo.GetNewsFilterByNewsGuid(mnews._id);
                        if (newsFilters != null && newsFilters.Count > 0)
                        {
                            mnews.Filters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                            mnews.Filters.CopyFrom(newsFilters);
                        }
                        else
                        {
                            if (mnews.Filters == null || mnews.Filters.Count == 0)
                            {
                                Core.DataTransfer.NewsFilter.GetOutput filter = new Core.DataTransfer.NewsFilter.GetOutput();
                                filter.FilterId = 78;
                                mnews.Filters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                                mnews.Filters.Add(filter);
                            }
                        }

                        List<NewsLocation> newsLocation = nlrepo.GetNewsLocationByNewsGuid(mnews._id);
                        if (newsLocation != null && newsLocation.Count > 0)
                        {
                            mnews.Locations = new List<Core.DataTransfer.Location.GetOutput>();
                            mnews.Locations.CopyFrom(newsLocation);
                        }

                        List<NewsCategory> newsCategory = ncrepo.GetNewsCategoryByNewsGuid(mnews._id);
                        if (newsCategory != null && newsCategory.Count > 0)
                        {
                            mnews.Categories = new List<Core.DataTransfer.Category.GetOutput>();
                            mnews.Categories.CopyFrom(newsCategory);
                        }
                        else
                        {
                            if (mnews.Categories == null || mnews.Categories.Count == 0)
                            {
                                Core.DataTransfer.Category.GetOutput Category = new Core.DataTransfer.Category.GetOutput();
                                Category.CategoryId = 1;
                                mnews.Categories = new List<Core.DataTransfer.Category.GetOutput>();
                                mnews.Categories.Add(Category);
                            }
                        }
                    }
                    loadNews.Bunchs = new List<Core.DataTransfer.Bunch.GetOutput>();
                    loadNews.Bunchs.CopyFrom(SqlBunchDic);
                    #endregion
                }
                else
                {
                    #region Default Code
                    MBunchDic = _iMBunchRepository.GetByIDs(bunches);
                    newsDic = _iMNewsRepository.GetByBunchIDs(bunches);

                    int i = 0;
                    foreach (SolarNews rNews in sResultNews)
                    {
                        MNews news = newsDic.Where(x => x._id.ToLower() == rNews._id.ToLower()).FirstOrDefault();
                        if (news != null)
                        {
                            news.Score = snews.TotalCount - (startIndex + i);
                            news.PublishTime = news.PublishTime.Date.Add(news.LastUpdateDate.TimeOfDay);
                        }
                        i++;
                    }
                    List<MNews> SelectedNews = new List<MNews>();
                    foreach (string bunchguid in bunches)
                    {
                        SelectedNews.AddRange(newsDic.Where(x => x.Score > 0 && x.BunchGuid == bunchguid).Take(3).ToList());
                    }



                    SelectedNews = SelectedNews.OrderByDescending(x => x.Score).ToList();
                    loadNews.News = new List<GetOutput>();
                    loadNews.News.CopyFrom(SelectedNews);
                    loadNews.Bunchs = new List<Core.DataTransfer.Bunch.GetOutput>();
                    loadNews.Bunchs.CopyFrom(MBunchDic);
                    #endregion
                }
            }
            return loadNews;
        }

        public int SeNewsCache(SolrService<SolarNews> sService, int MigrationType = 0)
        {
            NewsRepository newsrepo = new NewsRepository();
            int BunchCount = 0;
            string query = "title:(*:*)";
            DateTime lastDate = DateTime.UtcNow.AddYears(-10);
            var searchResult = sService.Sort(query, "lastupdatedate", SolrNet.Order.DESC, 1);
            if (searchResult != null && searchResult.Results.Count > 0)
            {
                lastDate = searchResult.Results.First().LastUpdateDate;
                lastDate = DateTime.SpecifyKind(lastDate, DateTimeKind.Utc);
            }
            if (MigrationType == 2) //Mongo All
            {
                #region All Mongo Start Region
                List<MNews> ListmNews = new List<MNews>();
                List<MNews> tmpNews = _iMNewsRepository.GetByUpdatedDate2000(new DateTime(2000, 1, 1));
                while (tmpNews != null && tmpNews.Count > 0)
                {
                    ListmNews.AddRange(tmpNews);
                    lastDate = ListmNews.Max(x => x.LastUpdateDate);
                    tmpNews = _iMNewsRepository.GetByUpdatedDate2000(lastDate);
                }
                if (ListmNews != null && ListmNews.Count > 0)
                {
                    BunchCount = ListmNews.Count();
                    List<SolarNews> sNewsList = new List<SolarNews>();
                    foreach (MNews news in ListmNews)
                    {
                        SolarNews sNews = new SolarNews();
                        string title = String.IsNullOrEmpty(news.TranslatedTitle) ? news.Title : news.TranslatedTitle;
                        string Description = String.IsNullOrEmpty(news.TranslatedDescription) ? news.Description : news.TranslatedDescription;
                        sNews.slug = String.IsNullOrEmpty(news.TranslatedSlug) ? news.Slug : news.TranslatedSlug;
                        sNews.title = title;
                        string tags = "";
                        if (news.Tags != null && news.Tags.Count > 0)
                            tags = string.Join(" ", news.Tags.Select(x => x.Tag).ToList());
                        if (!string.IsNullOrEmpty(Description))
                        {
                            sNews.Description = Regex.Replace(Description, "<.*?>", "", RegexOptions.IgnoreCase).Trim();
                        }
                        sNews.BunchId = news.BunchGuid;
                        sNews.LastUpdateDate = news.LastUpdateDate;
                        sNews.PublishDate = Convert.ToDateTime(news.PublishTime);
                        sNews._id = news._id;
                        sNews.IsArchival = false;
                        sNews.Tags = tags;
                        if (news.Filters != null && news.Filters.Count > 0)
                        {
                            sNews.Filters = news.Filters.Select(x => x.FilterId).ToList();
                        }
                        sNewsList.Add(sNews);
                        if (sNewsList.Count == 1000)
                        {
                            sService.AddRecords(sNewsList);
                            sNewsList.Clear();
                        }
                    }
                    if (sNewsList != null && sNewsList.Count > 0)
                    {
                        sService.AddRecords(sNewsList);
                    }
                }
                #endregion
            }
            else
            {
                #region Default mongo

                List<MNews> ListmNews = _iMNewsRepository.GetByUpdatedDate2000(lastDate);
                if (ListmNews != null && ListmNews.Count > 0)
                {
                    //List<News> ListmNews = newsrepo.GetFromDate(lastDate);
                    BunchCount = ListmNews.Count();
                    List<SolarNews> sNewsList = new List<SolarNews>();
                    foreach (MNews news in ListmNews)
                    {
                        SolarNews sNews = new SolarNews();
                        string title = String.IsNullOrEmpty(news.TranslatedTitle) ? news.Title : news.TranslatedTitle;
                        string Description = String.IsNullOrEmpty(news.TranslatedDescription) ? news.Description : news.TranslatedDescription;

                        string tags = "";
                        if (news.Tags != null && news.Tags.Count > 0)
                            tags = string.Join(" ", news.Tags.Select(x => x.Tag).ToList());

                        sNews.title = title;
                        sNews.slug = String.IsNullOrEmpty(news.TranslatedSlug) ? news.Slug : news.TranslatedSlug;
                        sNews.Description = Regex.Replace(Description, "<.*?>", "", RegexOptions.IgnoreCase).Trim();
                        sNews.BunchId = news.BunchGuid;
                        sNews.LastUpdateDate = news.LastUpdateDate;
                        sNews.PublishDate = Convert.ToDateTime(news.PublishTime);
                        sNews._id = news._id;
                        sNews.IsArchival = false;
                        sNews.Tags = tags;
                        if (news.Filters != null && news.Filters.Count > 0)
                        {
                            sNews.Filters = news.Filters.Select(x => x.FilterId).ToList();
                        }
                        sNewsList.Add(sNews);
                    }
                    if (sNewsList != null && sNewsList.Count > 0)
                    {
                        sService.AddRecords(sNewsList);
                        BunchCount += sNewsList.Count;
                    }
                }
                #endregion
            }
            #region Sql complete start
            List<News> ListNews = newsrepo.GetFromDateWithStatus();
            if (ListNews != null && ListNews.Count > 0)
            {
                BunchCount = ListNews.Count();
                List<SolarNews> sNewsSqlList = new List<SolarNews>();
                List<string> Newsids = new List<string>();
                foreach (News news in ListNews)
                {
                    SolarNews sNews = new SolarNews();
                    string title = String.IsNullOrEmpty(news.TranslatedTitle) ? news.Title : news.TranslatedTitle;
                    string Description = String.IsNullOrEmpty(news.TranslatedDescription) ? news.Description : news.TranslatedDescription;
                    bool? Isarchival = news.IsArchival;
                    Isarchival = Isarchival == null ? false : Isarchival;

                    sNews.title = title;
                    sNews.slug = String.IsNullOrEmpty(news.TranslatedSlug) ? news.Slug : news.TranslatedSlug;
                    sNews.Description = Regex.Replace(Description, "<.*?>", "", RegexOptions.IgnoreCase).Trim();
                    sNews.BunchId = news.BunchGuid;
                    sNews.LastUpdateDate = news.LastUpdateDate;
                    sNews.PublishDate = Convert.ToDateTime(news.PublishTime);
                    sNews._id = news.Guid;
                    sNews.Tags = news.tags;
                    sNews.IsArchival = Convert.ToBoolean(Isarchival);
                    if (news.filters != null && news.filters.Length > 1)
                    {
                        List<string> tmpString = news.filters.Split(',').ToList();
                        sNews.Filters = tmpString.Select(x => Convert.ToInt32(x)).ToList();
                    }
                    sNewsSqlList.Add(sNews);
                    if (sNewsSqlList.Count == 1000)
                    {
                        sService.AddRecords(sNewsSqlList);
                        string Guidcsv = string.Join(",", Newsids);
                        newsrepo.UpdateNewsMigrationStatus(Guidcsv);
                        sNewsSqlList.Clear();
                    }
                    Newsids.Add(news.NewsId.ToString());
                }
                if (sNewsSqlList != null && sNewsSqlList.Count > 0)
                {
                    sService.AddRecords(sNewsSqlList);
                    string Guidcsv = string.Join(",", Newsids);
                    newsrepo.UpdateNewsMigrationStatus(Guidcsv);
                }
            }
            #endregion
            //}
            return BunchCount;
        }

        public List<MBunch> GetBunchsByFilterIds(List<Filter> filters, int pageCount, int startIndex, DateTime from, DateTime to, string term, List<Filter> discaredFilters = null)
        {
            List<MBunchFilter> newsList = _iMBunchFilterRepository.GetNewsByFilterIds(filters, pageCount, startIndex, from, to, term, discaredFilters);
            return _iMBunchRepository.GetByIDs(newsList.Select(x => x.BunchId).ToList());
        }

        public List<MNews> GetNewsByFilterIds(List<Filter> filters, DateTime lastUpdateddate, int PageSize)
        {
            return _iMNewsRepository.GetNewsByFilterIds(filters, lastUpdateddate, PageSize);
        }

        private bool IsValidByFilterNews(List<Filter> filters, MNews news, List<MBunchFilter> newsList)
        {
            if (filters.Count == 0) return true;
            var _bunchFilter = newsList.Where(x => x.NewsList.Any(y => y._id == news._id));

            if ((filters.Any(x => x.FilterId == (int)NMS.Core.Enums.NewsFilters.AllNews) && _bunchFilter.Any(x => x.FilterId == (int)NMS.Core.Enums.NewsFilters.AllNews))
                || (filters.Any(x => x.FilterId == (int)NMS.Core.Enums.NewsFilters.AllNews) && _bunchFilter.Any(x => filters.Where(y => y.FilterTypeId == (int)FilterTypes.Program).Select(y => y.FilterId).Contains(x.FilterId))))
            {
                return true;
            }
            else
            {
                List<int> filterIds = _bunchFilter.Select(x => x.FilterId).Distinct().ToList();
                return filterIds.Count == filters.Count;
            }
            //bool flag = false;
            //if (filters.Where(x => x.FilterTypeId == (int)FilterTypes.AllNews || x.FilterTypeId == (int)FilterTypes.NewsFilter).Count() > 0)
            //{
            //    flag = (filters.Where(x => (x.FilterTypeId == (int)FilterTypes.AllNews || x.FilterTypeId == (int)FilterTypes.NewsFilter) && filterIds.Contains(x.FilterId)).Count() > 0);
            //    if (!flag)
            //        return false;
            //}
            //if (filters.Where(x => x.FilterTypeId == (int)FilterTypes.Category).Count() > 0)
            //{
            //    flag = (filters.Where(x => (x.FilterTypeId == (int)FilterTypes.Category) && filterIds.Contains(x.FilterId)).Count() > 0);
            //    if (!flag)
            //        return false;
            //}
            //if (filters.Where(x => x.FilterTypeId != (int)FilterTypes.AllNews && x.FilterTypeId != (int)FilterTypes.NewsFilter && x.FilterTypeId != (int)FilterTypes.Category).Count() > 0)
            //{
            //    flag = (filters.Where(x => (x.FilterTypeId != (int)FilterTypes.AllNews && x.FilterTypeId != (int)FilterTypes.NewsFilter && x.FilterTypeId != (int)FilterTypes.Category) && filterIds.Contains(x.FilterId)).Count() > 0);
            //    if (!flag)
            //        return false;
            //}
            return true;
        }

        public List<MNews> GetNewsByBunchId(string bunchId)
        {
            return _iMNewsRepository.GetNewsByBunchId(bunchId);
        }

        public bool UpdateThumbId(string id, string resGuid)
        {
            _iMNewsRepository.UpdateThumb(id, resGuid);
            return true;
        }

        public bool MarkVerifyNews(List<string> newsIds, string bunchId, bool isVerified)
        {
            Filter filter = isVerified ? _iFilterRepository.GetFilter((int)NewsFilters.Verified) : _iFilterRepository.GetFilter((int)NewsFilters.Rejected);
            List<MNewsFilter> newsFilters = _iMNewsRepository.MarkVerifyNews(newsIds.ToArray(), getNewsFilter(filter, "", 0, DateTime.UtcNow), isVerified);
            foreach (MNewsFilter newsFilter in newsFilters)
            {
                MNews news = new MNews();
                news._id = newsFilter.NewsId;
                news.Filters = new List<MNewsFilter>();
                news.Filters.Add(newsFilter);
                InsertBunchFilters(bunchId, news);
                _iMBunchFilterRepository.RemoveNewsFromBunchFilter((int)NewsFilters.NotVerified, bunchId, newsFilter.NewsId);
            }
            return true;
        }

        public MNews GetNewsById(string id)
        {
            return _iMNewsRepository.GetNewsByID(id);
        }

        public News GetNewsByIdSql(string id)
        {
            return _iNewsRepository.GetByNewsGuid(id);
        }

        public void UpdateFilterCount()
        {
            //DateTime lastUpdateDate = _iMFilterCountRepository.GetMaxLastUpdateDate();
            //List<MNewsFilter> newsFilters = _iMNewsFilterRepository.GetUpdatedNewsFilters(lastUpdateDate);
            //List<MFilterCount> filterCounts = newsFilters.GroupBy(x => x.FilterId).Select(x => new MFilterCount() { FilterId = x.Key, Count = x.Count(), LastUpdateDate = x.Max(y => y.CreationDate) }).ToList();
            //foreach (var filterCount in filterCounts)
            //{
            //    _iMFilterCountRepository.InsertOrUpdate(filterCount);
            //}
            List<MFilterCount> lstCacheFilterCount = new List<MFilterCount>();
            List<int> filterIds = _iFilterRepository.GetDistinctFilterIds();
            foreach (int filterid in filterIds)
            {
                DateTime lastUpdateDate = _iMFilterCountRepository.GetMaxLastUpdateDate(filterid);
                List<MNewsFilter> newsFilters = _iMNewsFilterRepository.GetUpdatedNewsFilters(lastUpdateDate, filterId: filterid);
                if (newsFilters.Count > 0)
                {
                    MFilterCount filterCount = new MFilterCount();
                    filterCount.LastUpdateDate = newsFilters.Max(x => x.CreationDate);
                    filterCount.Count = newsFilters.Count;
                    filterCount.FilterId = filterid;

                    //_iMFilterCountRepository.InsertOrUpdate(filterCount);         //no need to update this in SQL Server

                    lstCacheFilterCount.Add(filterCount);
                }
            }

            //new work for cache rellated things.
            _iMFilterCountRepository.InsertOrUpdate(lstCacheFilterCount);
        }

        public List<MFilterCount> GetFilterCountByLastUpdateDate(List<Filter> filterIds, DateTime from, DateTime to, List<int> programFilterIds)
        {
            return _iMFilterCountRepository.GetFilterCountByLastUpdateDate(filterIds, from, to, programFilterIds);
        }

        public void IndexNewsOptimized()
        {
            NewsRepository nr = new NewsRepository();
            List<News> newsList = nr.GetNonIndexedNewsOptimized();
            DateTime dtNow = DateTime.UtcNow;
            if (newsList != null && newsList.Count > 0)
            {
                foreach (News sqlNews in newsList)
                {
                    List<string> Bunches = nr.GetRelatedNewsV2(sqlNews.NewsId, sqlNews.BunchGuid);
                    if (Bunches != null && Bunches.Count > 0)
                    {
                        Bunches = Bunches.Distinct().ToList();
                        string TopBunch = TopBunch = Bunches.FirstOrDefault();
                        if (Bunches.Count > 1)
                        {
                            int newscount = 0;
                            foreach (string bunch in Bunches)
                            {
                                List<MNews> lst = _iMNewsRepository.GetNewsByBunchId(bunch);
                                if (lst != null && lst.Count > 0 && lst.Count > newscount)
                                {
                                    newscount = lst.Count;
                                    TopBunch = bunch;
                                }
                            }
                        }

                        Bunches.Add(sqlNews.BunchGuid); //adding the fresh news bunch 
                        List<MNews> ALLMNews = new List<MNews>();
                        List<MNews> lsttmpNews = _iMNewsRepository.GetNewsByBunchId(TopBunch);
                        if (lsttmpNews != null && lsttmpNews.Count > 0)
                        {
                            ALLMNews.AddRange(lsttmpNews);
                            /// ALLMNews = _iMNewsRepository.GetByBunchIDs(Bunches);
                            foreach (string relBunch in Bunches)
                            {
                                DateTime dtnow = DateTime.UtcNow;
                                if (relBunch != TopBunch)
                                {
                                    #region News
                                    //Update top bunch in every related news

                                    List<MNews> LstmongoNews = _iMNewsRepository.GetNewsByBunchId(relBunch);
                                    if (LstmongoNews != null && LstmongoNews.Count > 0)
                                    {
                                        foreach (MNews mongoNews in LstmongoNews)
                                        {
                                            _iMNewsRepository.UpdateNewsBunch(mongoNews._id, TopBunch);
                                            _iNewsRepository.UpdateNewsBunch(mongoNews._id, TopBunch);  //Updating sql news bunch

                                            mongoNews.BunchGuid = TopBunch;//required for universdal list
                                            ALLMNews.Add(mongoNews);
                                        }
                                    }

                                    #endregion
                                }
                            }

                            #region Top rated marking

                            List<int> BunchSourceCount = ALLMNews.GroupBy(x => x.SourceFilterId).Select(y => y.Key).ToList();
                            if (BunchSourceCount.Count > 1)//Top Rated Bunch
                            {
                                //code dont run on allready top rated marked news
                                //ALLMNews = ALLMNews.Where(x=>x.Filters.Any(y=>y.FilterId == (int)NewsFilters.TopRated)).ToList();
                                #region News and News filter
                                foreach (MNews mnews in ALLMNews)
                                {
                                    if (!mnews.Filters.Select(x => x.FilterId).Contains((int)NewsFilters.TopRated))
                                    {
                                        MNewsFilter Newsfilter = new MNewsFilter();
                                        Newsfilter.FilterId = (int)NewsFilters.TopRated;
                                        Newsfilter.NewsId = mnews._id;
                                        Newsfilter.CreationDate = dtNow;
                                        Newsfilter.LastUpdateDate = dtNow;
                                        mnews.Filters.Add(Newsfilter);
                                        mnews.LastUpdateDate = dtNow;
                                        mnews.BunchGuid = TopBunch;
                                        _iMNewsRepository.UpdateFiltersWithDate(mnews);     //update filters in news      
                                    }
                                    // if (_iMNewsFilterRepository.CheckIfNewsFilterExists(Newsfilter) == null) //Check to save news update call
                                    // {

                                    //  }
                                }
                                #endregion
                            }

                            #endregion

                            _iMBunchRepository.UpdateBunchNewsCount(TopBunch, ALLMNews.Count);   //not needed

                            //#region set thumbnail for bunch news without thumbnail

                            //if (ALLMNews != null && ALLMNews.Count > 0 && ALLMNews.Any(x => x.ThumbnailUrl == null))
                            //{
                            //    List<MResource> mResources = new List<MResource>();
                            //    foreach (MNews news in ALLMNews)
                            //    {
                            //        if (news.Resources != null && news.Resources.Count > 0)
                            //            mResources.AddRange(news.Resources);
                            //    }
                            //    if (mResources != null && mResources.Count > 0)
                            //    {
                            //        foreach (MNews news in ALLMNews)
                            //        {
                            //            if (news.ThumbnailUrl == null)
                            //            {
                            //                news.ThumbnailUrl = mResources[0].Guid;
                            //                _iMNewsRepository.UpdateNewsThumb(news);
                            //                sqlNews.ThumbnailId = news.ThumbnailUrl;
                            //                break;
                            //            }
                            //        }
                            //    }
                            //    //mNews.Resources = new List<MResource>();
                            //    //mNews.Resources.Add(mBunchres.Resources[0]);                                              
                            //}

                            //#endregion
                        }
                    }
                    sqlNews.IsIndexed = true;
                    nr.UpdateNewsIndex(sqlNews);
                    //List<MFilterCount> filtercount = lst.GroupBy(x => x.FilterId).Select(g => new MFilterCount { FilterId = g.Key, Count = g.Count() }).ToList();

                }
            }
        }

        public void MarkNewsTopRated()
        {
            //#region Filters
            //DateTime dtnow = DateTime.UtcNow;

            //_iBunchNewsRepository.GetAllBunchNews

            //// Get bunches with count more then 3 and have different source and top rated filter not marked.

            //// loop here

            //int bunchId = 0;
            //List<MBunchNews> mbunchs = _iMBunchNewsRepository.GetBunchNewsByBunchId(bunchId);

            //MBunchFilter bunchFilter = new MBunchFilter();
            //bunchFilter.BunchId = bunchId;
            //bunchFilter.FilterId = (int)NewsFilters.TopRated;
            //bunchFilter.CreationDate = dtnow;
            //bunchFilter.LastUpdateDate = dtnow;
            //bunchFilter.NewsList = new List<MNews>();
            //_iMBunchFilterRepository.InsertBunchFilter(bunchFilter);
            //#endregion
        }

        private void DeleteBunch(string BunchId)
        {
            try
            {
                Bunch bunch = _iBunchRepository.GetBunchByGuid(BunchId);
                _iMBunchFilterRepository.DeleteByBunchId(BunchId);
                _iMBunchNewsRepository.DeleteByBunchId(BunchId);
                _iMBunchTagRepository.DeleteByBunchId(BunchId);
                _iMBunchRepository.DeleteByBunchId(BunchId);

                //Sql Deletion
                if (bunch != null)
                {
                    _iBunchNewsRepository.DeleteByBunchId(bunch.BunchId);
                    _iBunchRepository.DeleteBunch(bunch.BunchId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + BunchId, ex);
            }

        }

        public DateTime GetMaxScrapDate(ScrapMaxDates ScrapMaxDates)
        {
            return _iScrapMaxDatesService.GetScrapDateCreateIfNotExist(ScrapMaxDates).MaxUpdateDate;
        }

        public void UpdateScrapDate(ScrapMaxDates ScrapMaxDates)
        {
            _iScrapMaxDatesService.UpdateScrapMaxDates(ScrapMaxDates);
        }

        public MComment InsertComment(MComment mComment)
        {
            mComment.CreationDate = DateTime.UtcNow;
            mComment.LastUpdateDate = mComment.CreationDate;
            if (_iMNewsRepository.UpdateComment(mComment))
            {
                _iMCommentRepository.InsertComment(mComment);
                return mComment;
            }
            return null;
        }

        public List<MNews> GetNewsWithBunch(string bunchId, List<Filter> filterIds, int pageCount, int startIndex, DateTime from, DateTime to)
        {
            return _iMNewsRepository.GetNewsByBunchId(bunchId, filterIds);
            //List<MBunchFilter> newsList = _iMBunchFilterRepository.GetByFilterIdAndBunchId(filterIds, bunchId);

            //List<MNews> newsDic = new List<MNews>();
            //foreach (var bunchFilter in newsList)
            //{
            //    foreach (var news in bunchFilter.NewsList)
            //    {
            //        if (news.LastUpdateDate > from && news.LastUpdateDate < to)
            //            newsDic.Add(news);
            //    }
            //}
            //newsDic = newsDic.OrderByDescending(x => x.LastUpdateDate).Skip(startIndex).Take(pageCount).ToList();
            //return _iMNewsRepository.GetNewsByIDs(newsDic.Select(x => x._id).ToList(), true);
        }

        public List<MComment> GetCommentsByLastUpdateDate(DateTime commentsLastUpdate, int reporterId)
        {
            return _iMCommentRepository.GetCommentsByLastUpdate(commentsLastUpdate, reporterId);
        }

        public List<MNews> GetNewsWithUpdatesAndRelatedById(string newsId, string bunchId)
        {
            List<MNews> news = _iMNewsRepository.GetNewsWithUpdatesAndRelatedById(newsId, bunchId);
            foreach (var n in news)
            {
                if (n.ResourceEdit != null)
                {
                    if (n.ResourceEdit.ResourceTypeId == (int)ResourceTypes.Audio)
                        n.ResourceEdit.Url = _iRadioStreamRepository.GetRadioStream(n.ResourceEdit.Id).Url;
                    if (n.ResourceEdit.ResourceTypeId == (int)ResourceTypes.Video)
                        n.ResourceEdit.Url = _iChannelVideoRepository.GetChannelVideo(n.ResourceEdit.Id).Url;
                    if (n.ResourceEdit.ResourceTypeId == (int)ResourceTypes.Image)
                        n.ResourceEdit.Url = _iNewsPaperPageRepository.GetNewsPaperPage(n.ResourceEdit.Id).ImageGuid.ToString();
                }
            }
            return news;
        }

        public List<MDescrepencyNews> GetAllDescrepancyNews(DescrepencyType descrepancyTypeId)
        {
            return _iMDescrepencyNewsRepository.GetAllByDescrepancyType((int)descrepancyTypeId);
        }

        public List<MDescrepencyNews> GetDescrepancyByTitleAndSource(string Title, string Source)
        {
            return _iMDescrepencyNewsRepository.GetDescrepancyByTitleAndSource(Title, Source);
        }

        public List<MNews> GetNewsByTitle(string term)
        {
            return _iMNewsRepository.GetNewsByTitle(term);
        }

        public List<MDescrepencyNews> GetAllDescrepancyNews(DescrepencyType descrepancyTypeId, string descrepancyValue)
        {
            List<DescrepencyNewsFile> DNewsFile = _iDescrepencyNewsFileRepository.GetAllByDescrepancyType((int)descrepancyTypeId, descrepancyValue);
            List<MDescrepencyNews> result = new List<MDescrepencyNews>();
            if (DNewsFile != null)
            {
                result.CopyFrom(DNewsFile);
            }
            return result;
        }

        public List<MDescrepencyNews> GetDiscrepencyBystatus(DescrepencyType descrepancyTypeId, int Status)
        {
            List<DescrepencyNewsFile> DNewsFile = _iDescrepencyNewsFileRepository.GetDiscrepencyBystatus((int)descrepancyTypeId, Status);
            List<MDescrepencyNews> result = new List<MDescrepencyNews>();
            if (DNewsFile != null)
            {
                result.CopyFrom(DNewsFile);
            }
            return result;
        }


        public bool DeleteDescrepancyNews(int DescrepancyNewsID)
        {
            _iDescrepencyNewsFileRepository.DeleteDescrepencyNewsFile(DescrepancyNewsID);
            return true;
        }

        public class EditResourceWorkArg
        {
            public HttpContext Context { get; set; }

            public ResourceEdit ResourceEdit { get; set; }
        }

        public void ProcessProducerDataPolling()
        {
            _iMFilterCountRepository.ProcessProducerDataPolling();
        }

        public List<News> GetDisitnctNewsSource()
        {
            return _iNewsRepository.GetDisitnctNewsSource();
        }

        public News GetByNewsGuid(System.String NewsGuid)
        {
            return _iNewsRepository.GetByNewsGuid(NewsGuid);
        }

        public News GetByNewsGuidAndIsComplete(System.String NewsGuid, bool isComplete, string SelectClause = null)
        {
            return _iNewsRepository.GetByNewsGuidAndIsComplete(NewsGuid, isComplete, SelectClause);
        }

        public bool UpdateNewsStatistics(NewsStatisticsInput newsStatisticsParam, string newsId, NewsStatisticType type)
        {
            MNews news = GetNewsById(newsId);
            News Sqlnews = GetNewsByIdSql(newsId);

            if (news != null)
            {
                if (Sqlnews != null && Convert.ToBoolean(Sqlnews.IsArchival))       //Operation in Sql
                {
                    NewsRepository SqlNewsRepo = new NewsRepository();
                    NewsStatisticsRepository SqlNewsStatisticsRepo = new NewsStatisticsRepository();

                    if (type == NewsStatisticType.NewsTickerCount)
                    {
                        int Count = Convert.ToInt32(Sqlnews.NewsTickerCount == null ? 0 : Sqlnews.NewsTickerCount);
                        Count++;
                        SqlNewsRepo.UpdateNewsTickerCount(Sqlnews.Guid, Count);
                    }
                    else
                    {
                        bool isAlreadyExist = false;
                        List<NewsStatistics> newsStatistics = SqlNewsStatisticsRepo.GetNewsStatisticsByKeyValue("NewsGuid", Sqlnews.Guid, Operands.Equal);
                        if (newsStatistics != null && newsStatistics.Count > 0)
                        {
                            if (newsStatistics.Count(x => x.EpisodeId == newsStatisticsParam.EpisodeId && x.Type == (int)type) > 0)
                            {
                                isAlreadyExist = true;
                            }
                        }

                        if (!isAlreadyExist)
                        {
                            if (type == NewsStatisticType.AddedToRundown)
                            {
                                Sqlnews.AddedToRundownCount++;
                            }
                            else if (type == NewsStatisticType.OnAired)
                            {
                                Sqlnews.OnAirCount++;
                            }
                            else if (type == NewsStatisticType.ExecutedOnOtherChannels)
                            {
                                Sqlnews.OtherChannelExecutionCount++;
                            }
                            _iNewsRepository.UpdateNews(Sqlnews);

                            NewsStatistics SqlnewsStats = new NewsStatistics();
                            SqlnewsStats.ProgramId = newsStatisticsParam.ProgramId;
                            SqlnewsStats.ProgramName = newsStatisticsParam.ProgramName;
                            SqlnewsStats.ChannelName = newsStatisticsParam.ChannelName;
                            SqlnewsStats.EpisodeId = newsStatisticsParam.EpisodeId;
                            SqlnewsStats.Time = newsStatisticsParam.EpisodeStartTime;
                            SqlnewsStats.Type = (int)type;
                            SqlNewsStatisticsRepo.InsertNewsStatistics(SqlnewsStats);

                            return true;
                        }
                    }
                }
                else
                {
                    if (type == NewsStatisticType.NewsTickerCount)
                    {
                        news.NewsTickerCount++;
                        _iMNewsRepository.UpdateNewsTickerCount(news);
                    }
                    else
                    {
                        bool isAlreadyExist = false;

                        if (news.NewsStatistics != null && news.NewsStatistics.Count > 0)
                        {
                            if (news.NewsStatistics.Count(x => x.EpisodeId == newsStatisticsParam.EpisodeId && x.Type == (int)type) > 0)
                            {
                                isAlreadyExist = true;
                            }
                        }

                        if (!isAlreadyExist)
                        {
                            if (type == NewsStatisticType.AddedToRundown)
                            {
                                news.AddedToRundownCount++;
                            }
                            else if (type == NewsStatisticType.OnAired)
                            {
                                news.OnAirCount++;
                            }
                            else if (type == NewsStatisticType.ExecutedOnOtherChannels)
                            {
                                news.OtherChannelExecutionCount++;
                            }

                            MNewsStatistics newsStats = new MNewsStatistics();
                            newsStats.ProgramId = newsStatisticsParam.ProgramId;
                            newsStats.ProgramName = newsStatisticsParam.ProgramName;
                            newsStats.ChannelName = newsStatisticsParam.ChannelName;
                            newsStats.EpisodeId = newsStatisticsParam.EpisodeId;
                            newsStats.Time = newsStatisticsParam.EpisodeStartTime;
                            newsStats.Type = (int)type;

                            if (news.NewsStatistics == null)
                            {
                                news.NewsStatistics = new List<MNewsStatistics>();
                            }

                            news.NewsStatistics.Add(newsStats);

                            _iMNewsRepository.UpdateNewsStatistics(news);

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool DeleteNewsStatistics(string newsId, int episodeId)
        {
            MNews news = GetNewsById(newsId);

            if (news != null && news.NewsStatistics != null && news.NewsStatistics.Count > 0)
            {
                MNewsStatistics tempObj = news.NewsStatistics.Find(x => x.EpisodeId == episodeId);
                if (tempObj != null)
                {
                    news.NewsStatistics.Remove(tempObj);
                    _iMNewsRepository.UpdateNewsStatistics(news);
                }
            }

            return true;
        }
        public void UpdateDescrepancyNewsByValue(int descrepencyType, string DescrepencyValue)
        {
            _iDescrepencyNewsFileRepository.UpdateDescrepencyNewsByValue(descrepencyType, DescrepencyValue);
        }

        public List<MDescrepencyNews> GetAllDescrepancyByStatus(int DescrepencyType)
        {
                List<DescrepencyNewsFile> DNewsFile = _iDescrepencyNewsFileRepository.GetAllDescrepencyByStatus(DescrepencyType);
                List<MDescrepencyNews> result = new List<MDescrepencyNews>();
                if (DNewsFile != null)
                {
                    result.CopyFrom(DNewsFile);
                }
            return result;
        }

        public List<TickerImage> GetTickerImages(DateTime lastUpdateDate)
        {
            return _iNewsRepository.GetTickerImages(lastUpdateDate);
        }

        public List<TickerImage> GetTickerImagesByLastUpdateDate(DateTime lastUpdateDate)
        {
            return _iNewsRepository.GetTickerImagesByLastUpdateDate(lastUpdateDate);
        }
    }
    
}