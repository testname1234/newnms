﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsSource;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsSourceService : INewsSourceService 
	{
		private INewsSourceRepository _iNewsSourceRepository;
        
		public NewsSourceService(INewsSourceRepository iNewsSourceRepository)
		{
			this._iNewsSourceRepository = iNewsSourceRepository;
		}
        
        public Dictionary<string, string> GetNewsSourceBasicSearchColumns()
        {            
            return this._iNewsSourceRepository.GetNewsSourceBasicSearchColumns();           
        }
        
        public List<SearchColumn> GetNewsSourceAdvanceSearchColumns()
        {            
            return this._iNewsSourceRepository.GetNewsSourceAdvanceSearchColumns();           
        }
        

		public NewsSource GetNewsSource(System.Int32 NewsSourceId)
		{
			return _iNewsSourceRepository.GetNewsSource(NewsSourceId);
		}

		public NewsSource UpdateNewsSource(NewsSource entity)
		{
			return _iNewsSourceRepository.UpdateNewsSource(entity);
		}

		public bool DeleteNewsSource(System.Int32 NewsSourceId)
		{
			return _iNewsSourceRepository.DeleteNewsSource(NewsSourceId);
		}

		public List<NewsSource> GetAllNewsSource()
		{
			return _iNewsSourceRepository.GetAllNewsSource();
		}

		public NewsSource InsertNewsSource(NewsSource entity)
		{
			 return _iNewsSourceRepository.InsertNewsSource(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newssourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newssourceid))
            {
				NewsSource newssource = _iNewsSourceRepository.GetNewsSource(newssourceid);
                if(newssource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newssource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsSource> newssourcelist = _iNewsSourceRepository.GetAllNewsSource();
            if (newssourcelist != null && newssourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newssourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsSource newssource = new NewsSource();
                PostOutput output = new PostOutput();
                newssource.CopyFrom(Input);
                newssource = _iNewsSourceRepository.InsertNewsSource(newssource);
                output.CopyFrom(newssource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsSource newssourceinput = new NewsSource();
                NewsSource newssourceoutput = new NewsSource();
                PutOutput output = new PutOutput();
                newssourceinput.CopyFrom(Input);
                NewsSource newssource = _iNewsSourceRepository.GetNewsSource(newssourceinput.NewsSourceId);
                if (newssource!=null)
                {
                    newssourceoutput = _iNewsSourceRepository.UpdateNewsSource(newssourceinput);
                    if(newssourceoutput!=null)
                    {
                        output.CopyFrom(newssourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newssourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newssourceid))
            {
				 bool IsDeleted = _iNewsSourceRepository.DeleteNewsSource(newssourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
