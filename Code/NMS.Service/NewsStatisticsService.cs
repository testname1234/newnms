﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsStatistics;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsStatisticsService : INewsStatisticsService 
	{
		private INewsStatisticsRepository _iNewsStatisticsRepository;
        
		public NewsStatisticsService(INewsStatisticsRepository iNewsStatisticsRepository)
		{
			this._iNewsStatisticsRepository = iNewsStatisticsRepository;
		}
        
        public Dictionary<string, string> GetNewsStatisticsBasicSearchColumns()
        {
            
            return this._iNewsStatisticsRepository.GetNewsStatisticsBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsStatisticsAdvanceSearchColumns()
        {
            
            return this._iNewsStatisticsRepository.GetNewsStatisticsAdvanceSearchColumns();
           
        }
        

		public NewsStatistics GetNewsStatistics(System.Int32 NewsStatisticsId)
		{
			return _iNewsStatisticsRepository.GetNewsStatistics(NewsStatisticsId);
		}

		public NewsStatistics UpdateNewsStatistics(NewsStatistics entity)
		{
			return _iNewsStatisticsRepository.UpdateNewsStatistics(entity);
		}

		public bool DeleteNewsStatistics(System.Int32 NewsStatisticsId)
		{
			return _iNewsStatisticsRepository.DeleteNewsStatistics(NewsStatisticsId);
		}

		public List<NewsStatistics> GetAllNewsStatistics()
		{
			return _iNewsStatisticsRepository.GetAllNewsStatistics();
		}

		public NewsStatistics InsertNewsStatistics(NewsStatistics entity)
		{
			 return _iNewsStatisticsRepository.InsertNewsStatistics(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsstatisticsid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsstatisticsid))
            {
				NewsStatistics newsstatistics = _iNewsStatisticsRepository.GetNewsStatistics(newsstatisticsid);
                if(newsstatistics!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsstatistics);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsStatistics> newsstatisticslist = _iNewsStatisticsRepository.GetAllNewsStatistics();
            if (newsstatisticslist != null && newsstatisticslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsstatisticslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsStatistics newsstatistics = new NewsStatistics();
                PostOutput output = new PostOutput();
                newsstatistics.CopyFrom(Input);
                newsstatistics = _iNewsStatisticsRepository.InsertNewsStatistics(newsstatistics);
                output.CopyFrom(newsstatistics);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsStatistics newsstatisticsinput = new NewsStatistics();
                NewsStatistics newsstatisticsoutput = new NewsStatistics();
                PutOutput output = new PutOutput();
                newsstatisticsinput.CopyFrom(Input);
                NewsStatistics newsstatistics = _iNewsStatisticsRepository.GetNewsStatistics(newsstatisticsinput.NewsStatisticsId);
                if (newsstatistics!=null)
                {
                    newsstatisticsoutput = _iNewsStatisticsRepository.UpdateNewsStatistics(newsstatisticsinput);
                    if(newsstatisticsoutput!=null)
                    {
                        output.CopyFrom(newsstatisticsoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsstatisticsid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newsstatisticsid))
            {
				 bool IsDeleted = _iNewsStatisticsRepository.DeleteNewsStatistics(newsstatisticsid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
