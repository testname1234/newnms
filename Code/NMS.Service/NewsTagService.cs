﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsTag;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsTagService : INewsTagService 
	{
		private INewsTagRepository _iNewsTagRepository;
        
		public NewsTagService(INewsTagRepository iNewsTagRepository)
		{
			this._iNewsTagRepository = iNewsTagRepository;
		}
        
        public Dictionary<string, string> GetNewsTagBasicSearchColumns()
        {
            
            return this._iNewsTagRepository.GetNewsTagBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsTagAdvanceSearchColumns()
        {
            
            return this._iNewsTagRepository.GetNewsTagAdvanceSearchColumns();
           
        }
        

		public virtual List<NewsTag> GetNewsTagByTagId(System.Int32 TagId)
		{
			return _iNewsTagRepository.GetNewsTagByTagId(TagId);
		}

		public virtual List<NewsTag> GetNewsTagByNewsId(System.Int32 NewsId)
		{
			return _iNewsTagRepository.GetNewsTagByNewsId(NewsId);
		}

		public NewsTag GetNewsTag(System.Int32 NewsTagId)
		{
			return _iNewsTagRepository.GetNewsTag(NewsTagId);
		}

		public NewsTag UpdateNewsTag(NewsTag entity)
		{
			return _iNewsTagRepository.UpdateNewsTag(entity);
		}

		public bool DeleteNewsTag(System.Int32 NewsTagId)
		{
			return _iNewsTagRepository.DeleteNewsTag(NewsTagId);
		}

        public bool DeleteNewsTagBYNewsId(System.Int32 NewsId)
        {
            return _iNewsTagRepository.DeleteNewsTagBYNewsId(NewsId);
        }

		public List<NewsTag> GetAllNewsTag()
		{
			return _iNewsTagRepository.GetAllNewsTag();
		}

		public NewsTag InsertNewsTag(NewsTag entity)
		{
			 return _iNewsTagRepository.InsertNewsTag(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newstagid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newstagid))
            {
				NewsTag newstag = _iNewsTagRepository.GetNewsTag(newstagid);
                if(newstag!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newstag);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsTag> newstaglist = _iNewsTagRepository.GetAllNewsTag();
            if (newstaglist != null && newstaglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newstaglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsTag newstag = new NewsTag();
                PostOutput output = new PostOutput();
                newstag.CopyFrom(Input);
                newstag = _iNewsTagRepository.InsertNewsTag(newstag);
                output.CopyFrom(newstag);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsTag newstaginput = new NewsTag();
                NewsTag newstagoutput = new NewsTag();
                PutOutput output = new PutOutput();
                newstaginput.CopyFrom(Input);
                NewsTag newstag = _iNewsTagRepository.GetNewsTag(newstaginput.NewsTagId);
                if (newstag!=null)
                {
                    newstagoutput = _iNewsTagRepository.UpdateNewsTag(newstaginput);
                    if(newstagoutput!=null)
                    {
                        output.CopyFrom(newstagoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newstagid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newstagid))
            {
				 bool IsDeleted = _iNewsTagRepository.DeleteNewsTag(newstagid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
