﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class NewsTypeService : INewsTypeService 
	{
		private INewsTypeRepository _iNewsTypeRepository;
        
		public NewsTypeService(INewsTypeRepository iNewsTypeRepository)
		{
			this._iNewsTypeRepository = iNewsTypeRepository;
		}
        
        public Dictionary<string, string> GetNewsTypeBasicSearchColumns()
        {
            
            return this._iNewsTypeRepository.GetNewsTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNewsTypeAdvanceSearchColumns()
        {
            
            return this._iNewsTypeRepository.GetNewsTypeAdvanceSearchColumns();
           
        }
        

		public NewsType GetNewsType(System.Int32 NewsTypeId)
		{
			return _iNewsTypeRepository.GetNewsType(NewsTypeId);
		}

		public NewsType UpdateNewsType(NewsType entity)
		{
			return _iNewsTypeRepository.UpdateNewsType(entity);
		}

		public bool DeleteNewsType(System.Int32 NewsTypeId)
		{
			return _iNewsTypeRepository.DeleteNewsType(NewsTypeId);
		}

		public List<NewsType> GetAllNewsType()
		{
			return _iNewsTypeRepository.GetAllNewsType();
		}

		public NewsType InsertNewsType(NewsType entity)
		{
			 return _iNewsTypeRepository.InsertNewsType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newstypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newstypeid))
            {
				NewsType newstype = _iNewsTypeRepository.GetNewsType(newstypeid);
                if(newstype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newstype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsType> newstypelist = _iNewsTypeRepository.GetAllNewsType();
            if (newstypelist != null && newstypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newstypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NewsType newstype = new NewsType();
                PostOutput output = new PostOutput();
                newstype.CopyFrom(Input);
                newstype = _iNewsTypeRepository.InsertNewsType(newstype);
                output.CopyFrom(newstype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsType newstypeinput = new NewsType();
                NewsType newstypeoutput = new NewsType();
                PutOutput output = new PutOutput();
                newstypeinput.CopyFrom(Input);
                NewsType newstype = _iNewsTypeRepository.GetNewsType(newstypeinput.NewsTypeId);
                if (newstype!=null)
                {
                    newstypeoutput = _iNewsTypeRepository.UpdateNewsType(newstypeinput);
                    if(newstypeoutput!=null)
                    {
                        output.CopyFrom(newstypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newstypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out newstypeid))
            {
				 bool IsDeleted = _iNewsTypeRepository.DeleteNewsType(newstypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
