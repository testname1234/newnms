﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NewsUpdateHistory;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class NewsUpdateHistoryService : INewsUpdateHistoryService
    {
        private INewsUpdateHistoryRepository _iNewsUpdateHistoryRepository;

        public NewsUpdateHistoryService(INewsUpdateHistoryRepository iNewsUpdateHistoryRepository)
        {
            this._iNewsUpdateHistoryRepository = iNewsUpdateHistoryRepository;
        }

        public Dictionary<string, string> GetNewsUpdateHistoryBasicSearchColumns()
        {

            return this._iNewsUpdateHistoryRepository.GetNewsUpdateHistoryBasicSearchColumns();

        }

        public List<SearchColumn> GetNewsUpdateHistoryAdvanceSearchColumns()
        {

            return this._iNewsUpdateHistoryRepository.GetNewsUpdateHistoryAdvanceSearchColumns();

        }


        public NewsUpdateHistory GetNewsUpdateHistory(System.Int32 NewsUpdateHistoryId)
        {
            return _iNewsUpdateHistoryRepository.GetNewsUpdateHistory(NewsUpdateHistoryId);
        }

        public List<NewsUpdateHistory> GetByNewsGuid(System.String NewsGuid)
        {
            return _iNewsUpdateHistoryRepository.GetByGuid(NewsGuid);
        }

        public NewsUpdateHistory UpdateNewsUpdateHistory(NewsUpdateHistory entity)
        {
            return _iNewsUpdateHistoryRepository.UpdateNewsUpdateHistory(entity);
        }

        public bool DeleteNewsUpdateHistory(System.Int32 NewsUpdateHistoryId)
        {
            return _iNewsUpdateHistoryRepository.DeleteNewsUpdateHistory(NewsUpdateHistoryId);
        }

        public List<NewsUpdateHistory> GetAllNewsUpdateHistory()
        {
            return _iNewsUpdateHistoryRepository.GetAllNewsUpdateHistory();
        }

        public NewsUpdateHistory InsertNewsUpdateHistory(NewsUpdateHistory entity)
        {
            return _iNewsUpdateHistoryRepository.InsertNewsUpdateHistory(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 newsupdatehistoryid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out newsupdatehistoryid))
            {
                NewsUpdateHistory newsupdatehistory = _iNewsUpdateHistoryRepository.GetNewsUpdateHistory(newsupdatehistoryid);
                if (newsupdatehistory != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(newsupdatehistory);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NewsUpdateHistory> newsupdatehistorylist = _iNewsUpdateHistoryRepository.GetAllNewsUpdateHistory();
            if (newsupdatehistorylist != null && newsupdatehistorylist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(newsupdatehistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsUpdateHistory newsupdatehistory = new NewsUpdateHistory();
                PostOutput output = new PostOutput();
                newsupdatehistory.CopyFrom(Input);
                newsupdatehistory = _iNewsUpdateHistoryRepository.InsertNewsUpdateHistory(newsupdatehistory);
                output.CopyFrom(newsupdatehistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NewsUpdateHistory newsupdatehistoryinput = new NewsUpdateHistory();
                NewsUpdateHistory newsupdatehistoryoutput = new NewsUpdateHistory();
                PutOutput output = new PutOutput();
                newsupdatehistoryinput.CopyFrom(Input);
                NewsUpdateHistory newsupdatehistory = _iNewsUpdateHistoryRepository.GetNewsUpdateHistory(newsupdatehistoryinput.NewsUpdateHistoryId);
                if (newsupdatehistory != null)
                {
                    newsupdatehistoryoutput = _iNewsUpdateHistoryRepository.UpdateNewsUpdateHistory(newsupdatehistoryinput);
                    if (newsupdatehistoryoutput != null)
                    {
                        output.CopyFrom(newsupdatehistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 newsupdatehistoryid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out newsupdatehistoryid))
            {
                bool IsDeleted = _iNewsUpdateHistoryRepository.DeleteNewsUpdateHistory(newsupdatehistoryid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
    }


}
