﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Notification;
using NMS.Core;
using Validation;
using System.Linq;
using NMS.Core.Enums;
using System.Text;

namespace NMS.Service
{

    public class NotificationService : INotificationService
    {
        private INotificationRepository _iNotificationRepository;
        private INotificationTypeService _iNotificationTypeService;

        public NotificationService(INotificationRepository iNotificationRepository, INotificationTypeService iNotificationTypeService)
        {
            this._iNotificationRepository = iNotificationRepository;
            this._iNotificationTypeService = iNotificationTypeService;
        }

        public Dictionary<string, string> GetNotificationBasicSearchColumns()
        {

            return this._iNotificationRepository.GetNotificationBasicSearchColumns();

        }

        public List<SearchColumn> GetNotificationAdvanceSearchColumns()
        {

            return this._iNotificationRepository.GetNotificationAdvanceSearchColumns();

        }


        public virtual List<Notification> GetNotificationByNotificationTypeId(System.Int32 NotificationTypeId)
        {
            return _iNotificationRepository.GetNotificationByNotificationTypeId(NotificationTypeId);
        }

        public Notification GetNotification(System.Int32 NotificationId)
        {
            return _iNotificationRepository.GetNotification(NotificationId);
        }

        public Notification UpdateNotification(Notification entity)
        {
            return _iNotificationRepository.UpdateNotification(entity);
        }

        public bool DeleteNotification(System.Int32 NotificationId)
        {
            return _iNotificationRepository.DeleteNotification(NotificationId);
        }

        public bool DeleteByJsonSlotId(System.Int32 SlotId)
        {
            return _iNotificationRepository.DeleteByJsonSlotId(SlotId);
        }

        public bool DeleteBySlotId(System.Int32 SlotId)
        {
            return _iNotificationRepository.DeleteBySlotId(SlotId);
        }

        public bool DeleteBySlotScreenTemplateId(System.Int32 Slotscrentemplateid)
        {
            return _iNotificationRepository.DeleteBySlotScreenTemplateId(Slotscrentemplateid);
        }

        public List<Notification> GetAllNotification()
        {
            return _iNotificationRepository.GetAllNotification();
        }

        public List<Notification> GetNotifications(int userId, DateTime? lastUpdateDate = null, int rowCount = 10)
        {
            return _iNotificationRepository.GetNotifications(userId, lastUpdateDate, rowCount);
        }

        public Notification InsertNotification(Notification entity)
        {
            return _iNotificationRepository.InsertNotification(entity);
        }

        public bool MarkAsRead(System.Int32 notificationId, int userId)
        {
            if (notificationId == -1)
            {
                return _iNotificationRepository.MarkAllAsRead(userId);
            }
            else
            {
                return _iNotificationRepository.MarkAsRead(notificationId);
            }
        }

        public bool Notify(int senderId, string senderName, int recipientId, NotificationTypes notificationType, Dictionary<string, string> arguments = null)
        {
            Notification entity = new Notification();

            entity.Title = GetNotificationTitle(notificationType, senderName);
            entity.Argument = GetArgument(arguments);
            entity.SenderId = senderId;
            entity.RecipientId = recipientId;
            entity.NotificationTypeId = (int)notificationType;
            entity.IsRead = false;
            entity.CreationDate = DateTime.UtcNow;
            entity.LastUpdateDate = DateTime.UtcNow;
            entity.IsActive = true;

            return InsertNotification(entity) != null;
        }

        public bool Notify(int senderId, string senderName, int recipientId, NotificationTypes notificationType,int slotId,int SlotScreenTemplateId, Dictionary<string, string> arguments = null)
        {
            Notification entity = new Notification();

            entity.Title = GetNotificationTitle(notificationType, senderName);
            entity.Argument = GetArgument(arguments);
            entity.SenderId = senderId;
            entity.Slotid = slotId;
            entity.Slotscreentemplateid = SlotScreenTemplateId;
            entity.RecipientId = recipientId;
            entity.NotificationTypeId = (int)notificationType;
            entity.IsRead = false;
            entity.CreationDate = DateTime.UtcNow;
            entity.LastUpdateDate = DateTime.UtcNow;
            entity.IsActive = true;

            return InsertNotification(entity) != null;
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 notificationid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out notificationid))
            {
                Notification notification = _iNotificationRepository.GetNotification(notificationid);
                if (notification != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(notification);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Notification> notificationlist = _iNotificationRepository.GetAllNotification();
            if (notificationlist != null && notificationlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(notificationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Notification notification = new Notification();
                PostOutput output = new PostOutput();
                notification.CopyFrom(Input);
                notification = _iNotificationRepository.InsertNotification(notification);
                output.CopyFrom(notification);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }
        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Notification notificationinput = new Notification();
                Notification notificationoutput = new Notification();
                PutOutput output = new PutOutput();
                notificationinput.CopyFrom(Input);
                Notification notification = _iNotificationRepository.GetNotification(notificationinput.NotificationId);
                if (notification != null)
                {
                    notificationoutput = _iNotificationRepository.UpdateNotification(notificationinput);
                    if (notificationoutput != null)
                    {
                        output.CopyFrom(notificationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }
        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 notificationid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out notificationid))
            {
                bool IsDeleted = _iNotificationRepository.DeleteNotification(notificationid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        private string GetNotificationTitle(NotificationTypes notificationType, string senderName)
        {
            string title = string.Empty;

            NotificationType entity = _iNotificationTypeService.GetNotificationType((int)notificationType);

            if (notificationType == NotificationTypes.TaskAssigned || notificationType == NotificationTypes.TaskSubmitted)
            {
                title = entity.Template.Replace("[VAR_NAME]", senderName);
            }

            return title;
        }

        private string GetArgument(Dictionary<string, string> args)
        {
            StringBuilder sb = new StringBuilder();
            if (args != null && args.Count > 0)
            {
                sb.Append("{");
                foreach (KeyValuePair<string, string> pair in args)
                {
                    sb.AppendFormat("\"{0}\":{1}", pair.Key, pair.Value);
                    sb.Append(",");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append("}");
            }
            return sb.ToString();
        }
    }


}
