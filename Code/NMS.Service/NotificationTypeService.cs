﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.NotificationType;
using NMS.Core;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class NotificationTypeService : INotificationTypeService 
	{
		private INotificationTypeRepository _iNotificationTypeRepository;
        
		public NotificationTypeService(INotificationTypeRepository iNotificationTypeRepository)
		{
			this._iNotificationTypeRepository = iNotificationTypeRepository;
		}
        
        public Dictionary<string, string> GetNotificationTypeBasicSearchColumns()
        {
            
            return this._iNotificationTypeRepository.GetNotificationTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetNotificationTypeAdvanceSearchColumns()
        {
            
            return this._iNotificationTypeRepository.GetNotificationTypeAdvanceSearchColumns();
           
        }
        

		public NotificationType GetNotificationType(System.Int32 NotificationTypeId)
		{
			return _iNotificationTypeRepository.GetNotificationType(NotificationTypeId);
		}

		public NotificationType UpdateNotificationType(NotificationType entity)
		{
			return _iNotificationTypeRepository.UpdateNotificationType(entity);
		}

		public bool DeleteNotificationType(System.Int32 NotificationTypeId)
		{
			return _iNotificationTypeRepository.DeleteNotificationType(NotificationTypeId);
		}

		public List<NotificationType> GetAllNotificationType()
		{
			return _iNotificationTypeRepository.GetAllNotificationType();
		}

		public NotificationType InsertNotificationType(NotificationType entity)
		{
			 return _iNotificationTypeRepository.InsertNotificationType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 notificationtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out notificationtypeid))
            {
				NotificationType notificationtype = _iNotificationTypeRepository.GetNotificationType(notificationtypeid);
                if(notificationtype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(notificationtype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<NotificationType> notificationtypelist = _iNotificationTypeRepository.GetAllNotificationType();
            if (notificationtypelist != null && notificationtypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(notificationtypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                NotificationType notificationtype = new NotificationType();
                PostOutput output = new PostOutput();
                notificationtype.CopyFrom(Input);
                notificationtype = _iNotificationTypeRepository.InsertNotificationType(notificationtype);
                output.CopyFrom(notificationtype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                NotificationType notificationtypeinput = new NotificationType();
                NotificationType notificationtypeoutput = new NotificationType();
                PutOutput output = new PutOutput();
                notificationtypeinput.CopyFrom(Input);
                NotificationType notificationtype = _iNotificationTypeRepository.GetNotificationType(notificationtypeinput.NotificationTypeId);
                if (notificationtype!=null)
                {
                    notificationtypeoutput = _iNotificationTypeRepository.UpdateNotificationType(notificationtypeinput);
                    if(notificationtypeoutput!=null)
                    {
                        output.CopyFrom(notificationtypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 notificationtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out notificationtypeid))
            {
				 bool IsDeleted = _iNotificationTypeRepository.DeleteNotificationType(notificationtypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
