﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.OnAirTickerLine;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class OnAirTickerLineService : IOnAirTickerLineService
    {
        private IOnAirTickerLineRepository _iOnAirTickerLineRepository;


        public OnAirTickerLineService(IOnAirTickerLineRepository iOnAirTickerLineRepository)
        {
            this._iOnAirTickerLineRepository = iOnAirTickerLineRepository;
        }

        public Dictionary<string, string> GetOnAirTickerLineBasicSearchColumns()
        {

            return this._iOnAirTickerLineRepository.GetOnAirTickerLineBasicSearchColumns();

        }

        public List<SearchColumn> GetOnAirTickerLineAdvanceSearchColumns()
        {

            return this._iOnAirTickerLineRepository.GetOnAirTickerLineAdvanceSearchColumns();

        }


        public virtual List<OnAirTickerLine> GetOnAirTickerLineByOnAirTickerId(System.Int32? OnAirTickerId)
        {
            return _iOnAirTickerLineRepository.GetOnAirTickerLineByTickerId(OnAirTickerId);
        }

        public OnAirTickerLine GetOnAirTickerLine(System.Int32 OnAirTickerLineId)
        {
            return _iOnAirTickerLineRepository.GetOnAirTickerLine(OnAirTickerLineId);
        }

        public OnAirTickerLine UpdateOnAirTickerLine(OnAirTickerLine entity)
        {
            return _iOnAirTickerLineRepository.UpdateOnAirTickerLine(entity);
        }

        public bool DeleteOnAirTickerLine(System.Int32 OnAirTickerLineId)
        {
            return _iOnAirTickerLineRepository.DeleteOnAirTickerLine(OnAirTickerLineId);
        }

        public List<OnAirTickerLine> GetAllOnAirTickerLine()
        {
            return _iOnAirTickerLineRepository.GetAllOnAirTickerLine();
        }

        public OnAirTickerLine InsertOnAirTickerLine(OnAirTickerLine entity, int? type)
        {
            entity.IsActive = true;
            entity.CreationDate = DateTime.UtcNow;
            entity.LastUpdatedDate = DateTime.UtcNow;
            var tempObj = _iOnAirTickerLineRepository.GetTickerLineBySeqNumAndTypeAndOnAirTickerId(entity.OperatorNumber.Value, entity.TickerId.Value, type.Value);
            bool flag = true;
            if (tempObj != null && type.HasValue)
                flag = _iOnAirTickerLineRepository.MigrateDataToOnAiredTable(tempObj, type.Value);

            if (flag)
                entity = _iOnAirTickerLineRepository.InsertOnAirTickerLine(entity);

            return entity;
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 onairtickerlineid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out onairtickerlineid))
            {
                OnAirTickerLine onairtickerline = _iOnAirTickerLineRepository.GetOnAirTickerLine(onairtickerlineid);
                if (onairtickerline != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(onairtickerline);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<OnAirTickerLine> onairtickerlinelist = _iOnAirTickerLineRepository.GetAllOnAirTickerLine();
            if (onairtickerlinelist != null && onairtickerlinelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(onairtickerlinelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                OnAirTickerLine onairtickerline = new OnAirTickerLine();
                PostOutput output = new PostOutput();
                onairtickerline.CopyFrom(Input);
                onairtickerline = _iOnAirTickerLineRepository.InsertOnAirTickerLine(onairtickerline);
                output.CopyFrom(onairtickerline);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                OnAirTickerLine onairtickerlineinput = new OnAirTickerLine();
                OnAirTickerLine onairtickerlineoutput = new OnAirTickerLine();
                PutOutput output = new PutOutput();
                onairtickerlineinput.CopyFrom(Input);
                OnAirTickerLine onairtickerline = _iOnAirTickerLineRepository.GetOnAirTickerLine(onairtickerlineinput.TickerLineId);
                if (onairtickerline != null)
                {
                    onairtickerlineoutput = _iOnAirTickerLineRepository.UpdateOnAirTickerLine(onairtickerlineinput);
                    if (onairtickerlineoutput != null)
                    {
                        output.CopyFrom(onairtickerlineoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 onairtickerlineid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out onairtickerlineid))
            {
                bool IsDeleted = _iOnAirTickerLineRepository.DeleteOnAirTickerLine(onairtickerlineid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<OnAirTickerLine> SubmitOnAirTickerLineToMcrBreaking(OnAirTickerLine OnAirTickerLine)
        {
            return _iOnAirTickerLineRepository.SubmitOnAirTickerLineToMcrBreaking(OnAirTickerLine);
        }

        public List<OnAirTickerLine> SubmitOnAirTickerLineToMcrLatest(OnAirTickerLine OnAirTickerLine)
        {
            return _iOnAirTickerLineRepository.SubmitOnAirTickerLineToMcrLatest(OnAirTickerLine);
        }
        public List<OnAirTickerLine> GetOnAirTickerLineByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
        {
            return _iOnAirTickerLineRepository.GetOnAirTickerLineByKeyValue(Key, Value, operand, SelectClause);
        }

        public bool DeleteOnAirTickerLineFromMCRBreaking(OnAirTickerLine tempObj)
        {
            return _iOnAirTickerLineRepository.DeleteOnAirTickerLineFromMCRBreaking(tempObj);
        }

        public bool DeleteOnAirTickerLineFromMCRLatest(OnAirTickerLine tempObj)
        {
            return _iOnAirTickerLineRepository.DeleteOnAirTickerLineFromMCRLatest(tempObj);
        }

        public bool DeleteOnAirTickerLineFromMCRCategory(OnAirTickerLine tempObj)
        {
            return _iOnAirTickerLineRepository.DeleteOnAirTickerLineFromMCRCategory(tempObj);
        }


        public bool MigrateGroupToOnAiredTable(OnAirTickerLine tempObj, int type)
        {
            return _iOnAirTickerLineRepository.MigrateGroupToOnAiredTable(tempObj, type);
        }


        public List<OnAirTickerLine> SubmitOnAirTickerLineToMcrCategory(OnAirTickerLine temp)
        {
            return _iOnAirTickerLineRepository.SubmitOnAirTickerLineToMcrCategory(temp);
        }


        public OnAirTickerLine UpdateOnAirTickerLineSequenceNumber(OnAirTickerLine item)
        {
            return _iOnAirTickerLineRepository.UpdateOnAirTickerLineSequenceNumber(item);
                
        }


        public List<OnAirTickerLine> GetLatestOnAirTickerLine(DateTime dateTime)
        {
            return _iOnAirTickerLineRepository.GetLatestOnAirTickerLine(dateTime);
        }
    }
}
