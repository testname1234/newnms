﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.OnAirTicker;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Enums;

namespace NMS.Service
{

    public class OnAirTickerService : IOnAirTickerService
    {
        private IOnAirTickerRepository _iOnAirTickerRepository;
        private IOnAirTickerLineRepository _iOnAirTickerLineRepository;

        public OnAirTickerService(IOnAirTickerRepository iOnAirTickerRepository, IOnAirTickerLineRepository iOnAirTickerLineRepository)
        {
            this._iOnAirTickerRepository = iOnAirTickerRepository;
            this._iOnAirTickerLineRepository = iOnAirTickerLineRepository;
        }

        public Dictionary<string, string> GetOnAirTickerBasicSearchColumns()
        {

            return this._iOnAirTickerRepository.GetOnAirTickerBasicSearchColumns();

        }

        public List<SearchColumn> GetOnAirTickerAdvanceSearchColumns()
        {

            return this._iOnAirTickerRepository.GetOnAirTickerAdvanceSearchColumns();

        }


        public OnAirTicker GetOnAirTicker(System.Int32 OnAirTickerId)
        {
            return _iOnAirTickerRepository.GetOnAirTicker(OnAirTickerId);
        }

        public OnAirTicker UpdateOnAirTicker(OnAirTicker entity)
        {
            return _iOnAirTickerRepository.UpdateOnAirTicker(entity);
        }

        public bool DeleteOnAirTicker(System.Int32 OnAirTickerId)
        {
            return _iOnAirTickerRepository.DeleteOnAirTicker(OnAirTickerId);
        }

        public List<OnAirTicker> GetAllOnAirTicker()
        {
            return _iOnAirTickerRepository.GetAllOnAirTicker();
        }

        public OnAirTicker InsertOnAirTicker(OnAirTicker entity)
        {
            entity.LastUpdatedDate = DateTime.UtcNow;
            entity.CreationDate = DateTime.UtcNow;
            entity.IsActive = true;
            entity.OnAiredTime = DateTime.UtcNow;
            Dictionary<string, int> seqNumbers = GetSequenceNumbers();
            entity.SequenceId = ++seqNumbers["onAirSeqId"];
            return _iOnAirTickerRepository.InsertOnAirTicker(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 onairtickerid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out onairtickerid))
            {
                OnAirTicker onairticker = _iOnAirTickerRepository.GetOnAirTicker(onairtickerid);
                if (onairticker != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(onairticker);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<OnAirTicker> onairtickerlist = _iOnAirTickerRepository.GetAllOnAirTicker();
            if (onairtickerlist != null && onairtickerlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(onairtickerlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                OnAirTicker onairticker = new OnAirTicker();
                PostOutput output = new PostOutput();
                onairticker.CopyFrom(Input);
                onairticker = _iOnAirTickerRepository.InsertOnAirTicker(onairticker);
                output.CopyFrom(onairticker);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                OnAirTicker onairtickerinput = new OnAirTicker();
                OnAirTicker onairtickeroutput = new OnAirTicker();
                PutOutput output = new PutOutput();
                onairtickerinput.CopyFrom(Input);
                OnAirTicker onairticker = _iOnAirTickerRepository.GetOnAirTicker(onairtickerinput.TickerId);
                if (onairticker != null)
                {
                    onairtickeroutput = _iOnAirTickerRepository.UpdateOnAirTicker(onairtickerinput);
                    if (onairtickeroutput != null)
                    {
                        output.CopyFrom(onairtickeroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 onairtickerid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out onairtickerid))
            {
                bool IsDeleted = _iOnAirTickerRepository.DeleteOnAirTicker(onairtickerid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public OnAirTicker FillOnAirTicker(OnAirTicker item)
        {
            if (item != null)
            {
                item.TickerLines = new List<OnAirTickerLine>();
                item.TickerLines = _iOnAirTickerLineRepository.GetOnAirTickerLineByKeyValue("TickerId", item.TickerId.ToString(), Operands.Equal);
            }
            if (item.TickerLines != null && item.TickerLines.Count > 0)
                item.TickerLines = item.TickerLines.OrderBy(i => i.SequenceId).ToList();

            return item;
        }
        public List<OnAirTicker> FillOnAirTicker(List<OnAirTicker> onAirTickers)
        {
            if (onAirTickers != null && onAirTickers.Count > 0)
            {
                foreach (OnAirTicker item in onAirTickers)
                {
                    item.TickerLines = new List<OnAirTickerLine>();
                    item.TickerLines = _iOnAirTickerLineRepository.GetOnAirTickerLineByKeyValue("TickerId", item.TickerId.ToString(), Operands.Equal);
                    if (item.TickerLines != null && item.TickerLines.Count > 0)
                        item.TickerLines = item.TickerLines.OrderBy(i => i.SequenceId).ToList();
                }
            }
            return onAirTickers;
        }


        public List<OnAirTicker> GetOnAirTickerByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
        {
            return _iOnAirTickerRepository.GetOnAirTickerByKeyValue(Key, Value,operand,SelectClause);
        }


        public OnAirTicker UpdateOnAirTickerSequenceNumber(OnAirTicker item)
        {
            if (item.SequenceId > 0 && item.TickerId > 0)
                return _iOnAirTickerRepository.UpdateOnAirTickerSequenceNumber(item);

            return null;
        }

        public Dictionary<string, int> GetSequenceNumbers() {
            return _iOnAirTickerRepository.GetSequenceNumbers();
        }


        public List<OnAirTicker> GetLatestOnAirTicker(DateTime dateTime)
        {
            return _iOnAirTickerRepository.GetLatestOnAirTicker(dateTime);
        }
    }


}
