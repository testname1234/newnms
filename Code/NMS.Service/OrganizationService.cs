﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Organization;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class OrganizationService : IOrganizationService 
	{
		private IOrganizationRepository _iOrganizationRepository;
        
		public OrganizationService(IOrganizationRepository iOrganizationRepository)
		{
			this._iOrganizationRepository = iOrganizationRepository;
		}
        
        public Dictionary<string, string> GetOrganizationBasicSearchColumns()
        {
            
            return this._iOrganizationRepository.GetOrganizationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetOrganizationAdvanceSearchColumns()
        {
            
            return this._iOrganizationRepository.GetOrganizationAdvanceSearchColumns();
           
        }
        

		public Organization GetOrganization(System.Int32 OrganizationId)
		{
			return _iOrganizationRepository.GetOrganization(OrganizationId);
		}

		public Organization UpdateOrganization(Organization entity)
		{
			return _iOrganizationRepository.UpdateOrganization(entity);
		}

		public bool DeleteOrganization(System.Int32 OrganizationId)
		{
			return _iOrganizationRepository.DeleteOrganization(OrganizationId);
		}

		public List<Organization> GetAllOrganization()
		{
			return _iOrganizationRepository.GetAllOrganization();
		}

		public Organization InsertOrganization(Organization entity)
		{
			 return _iOrganizationRepository.InsertOrganization(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 organizationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out organizationid))
            {
				Organization organization = _iOrganizationRepository.GetOrganization(organizationid);
                if(organization!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(organization);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Organization> organizationlist = _iOrganizationRepository.GetAllOrganization();
            if (organizationlist != null && organizationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(organizationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Organization organization = new Organization();
                PostOutput output = new PostOutput();
                organization.CopyFrom(Input);
                organization = _iOrganizationRepository.InsertOrganization(organization);
                output.CopyFrom(organization);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Organization organizationinput = new Organization();
                Organization organizationoutput = new Organization();
                PutOutput output = new PutOutput();
                organizationinput.CopyFrom(Input);
                Organization organization = _iOrganizationRepository.GetOrganization(organizationinput.OrganizationId);
                if (organization!=null)
                {
                    organizationoutput = _iOrganizationRepository.UpdateOrganization(organizationinput);
                    if(organizationoutput!=null)
                    {
                        output.CopyFrom(organizationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }


        public List<Organization> GetOrganizationByTerm(string term)
        {
            return _iOrganizationRepository.GetOrganizationByTerm(term);
        }


        public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 organizationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out organizationid))
            {
				 bool IsDeleted = _iOrganizationRepository.DeleteOrganization(organizationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
