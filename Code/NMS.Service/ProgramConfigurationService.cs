﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
//using NMS.Core.DataTransfer.ProgramConfiguration;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class ProgramConfigurationService : IProgramConfigurationService 
	{
		private IProgramConfigurationRepository _iProgramConfigurationRepository;
        
		public ProgramConfigurationService(IProgramConfigurationRepository iProgramConfigurationRepository)
		{
			this._iProgramConfigurationRepository = iProgramConfigurationRepository;
		}
        
        public Dictionary<string, string> GetProgramConfigurationBasicSearchColumns()
        {
            
            return this._iProgramConfigurationRepository.GetProgramConfigurationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetProgramConfigurationAdvanceSearchColumns()
        {
            
            return this._iProgramConfigurationRepository.GetProgramConfigurationAdvanceSearchColumns();
           
        }
        

		public virtual List<ProgramConfiguration> GetProgramConfigurationByProgramId(System.Int32 ProgramId)
		{
			return _iProgramConfigurationRepository.GetProgramConfigurationByProgramId(ProgramId);
		}

		public virtual List<ProgramConfiguration> GetProgramConfigurationBySegmentTypeId(System.Int32 SegmentTypeId)
		{
			return _iProgramConfigurationRepository.GetProgramConfigurationBySegmentTypeId(SegmentTypeId);
		}

		public ProgramConfiguration GetProgramConfiguration(System.Int32 ProgramConfigurationId)
		{
			return _iProgramConfigurationRepository.GetProgramConfiguration(ProgramConfigurationId);
		}

		public ProgramConfiguration UpdateProgramConfiguration(ProgramConfiguration entity)
		{
			return _iProgramConfigurationRepository.UpdateProgramConfiguration(entity);
		}

		public bool DeleteProgramConfiguration(System.Int32 ProgramConfigurationId)
		{
			return _iProgramConfigurationRepository.DeleteProgramConfiguration(ProgramConfigurationId);
		}

		public List<ProgramConfiguration> GetAllProgramConfiguration()
		{
			return _iProgramConfigurationRepository.GetAllProgramConfiguration();
		}

		public ProgramConfiguration InsertProgramConfiguration(ProgramConfiguration entity)
		{
            _iProgramConfigurationRepository.DeleteProgramConfigurationByProgramId(entity);
			 return _iProgramConfigurationRepository.InsertProgramConfiguration(entity);
		}


        //public DataTransfer<GetOutput> Get(string _id)
        //{
        //    DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
        //    System.Int32 programconfigurationid=0;
        //    if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programconfigurationid))
        //    {
        //        ProgramConfiguration programconfiguration = _iProgramConfigurationRepository.GetProgramConfiguration(programconfigurationid);
        //        if(programconfiguration!=null)
        //        {
        //            tranfer.IsSuccess = true;
        //            GetOutput output = new GetOutput();
        //            output.CopyFrom(programconfiguration);
        //            tranfer.Data=output ;

        //        }
        //        else
        //        {
        //            tranfer.IsSuccess = false;
        //            tranfer.Errors = new string[1];
        //            tranfer.Errors[0] = "Error: No record found.";
        //        }             
                
        //    }
        //    else
        //    {
        //        tranfer.IsSuccess = false;
        //        tranfer.Errors = new string[1];
        //        tranfer.Errors[0] = "Error: Invalid request.";
        //    }
        //    return tranfer;
        //}   
        //public DataTransfer<List<GetOutput>> GetAll()
        //{
        //    DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
        //    List<ProgramConfiguration> programconfigurationlist = _iProgramConfigurationRepository.GetAllProgramConfiguration();
        //    if (programconfigurationlist != null && programconfigurationlist.Count>0)
        //    {
        //        tranfer.IsSuccess = true;
        //        List<GetOutput> outputlist = new List<GetOutput>();
        //        outputlist.CopyFrom(programconfigurationlist);
        //        tranfer.Data = outputlist;

        //    }
        //    else
        //    {
        //        tranfer.IsSuccess = false;
        //        tranfer.Errors = new string[1];
        //        tranfer.Errors[0] = "Error: No record found.";
        //    }
        //    return tranfer;
        //}
        //public DataTransfer<PostOutput> Insert(PostInput Input)
        //{
        //   DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
        //    IList<string> errors = Validator.Validate(Input);
        //    if(errors.Count==0)
        //    {
        //        ProgramConfiguration programconfiguration = new ProgramConfiguration();
        //        PostOutput output = new PostOutput();
        //        programconfiguration.CopyFrom(Input);
        //        programconfiguration = _iProgramConfigurationRepository.InsertProgramConfiguration(programconfiguration);
        //        output.CopyFrom(programconfiguration);
        //        transer.IsSuccess = true;
        //        transer.Data = output;
        //    }
        //    else
        //    {
        //        transer.IsSuccess = false;
        //        transer.Errors = errors.ToArray<string>();
        //    }
        //    return transer;
        //}

        //public DataTransfer<PutOutput> Update(PutInput Input)
        //{
        //    DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
        //    IList<string> errors = Validator.Validate(Input);
        //    if (errors.Count == 0)
        //    {
        //        ProgramConfiguration programconfigurationinput = new ProgramConfiguration();
        //        ProgramConfiguration programconfigurationoutput = new ProgramConfiguration();
        //        PutOutput output = new PutOutput();
        //        programconfigurationinput.CopyFrom(Input);
        //        ProgramConfiguration programconfiguration = _iProgramConfigurationRepository.GetProgramConfiguration(programconfigurationinput.ProgramConfigurationId);
        //        if (programconfiguration!=null)
        //        {
        //            programconfigurationoutput = _iProgramConfigurationRepository.UpdateProgramConfiguration(programconfigurationinput);
        //            if(programconfigurationoutput!=null)
        //            {
        //                output.CopyFrom(programconfigurationoutput);
        //                transer.IsSuccess = true;
        //                transer.Data = output;
        //            }
        //            else
        //            {
        //                transer.IsSuccess = false;
        //                transer.Errors = new string[1];
        //                transer.Errors[0] = "Error: Could not update.";
        //            } 
        //        }
        //        else                
        //        {
        //            transer.IsSuccess = false;
        //            transer.Errors = new string[1];
        //            transer.Errors[0] = "Error: Record not found.";
        //        }
        //    }
        //    else
        //    {
        //        transer.IsSuccess = false;
        //        transer.Errors = errors.ToArray<string>();
        //    }
        //    return transer;
        //}

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programconfigurationid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out programconfigurationid))
            {
                bool IsDeleted = _iProgramConfigurationRepository.DeleteProgramConfiguration(programconfigurationid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
