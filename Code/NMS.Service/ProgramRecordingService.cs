﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramRecording;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ProgramRecordingService : IProgramRecordingService 
	{
		private IProgramRecordingRepository _iProgramRecordingRepository;
        
		public ProgramRecordingService(IProgramRecordingRepository iProgramRecordingRepository)
		{
			this._iProgramRecordingRepository = iProgramRecordingRepository;
		}
        
        public Dictionary<string, string> GetProgramRecordingBasicSearchColumns()
        {
            
            return this._iProgramRecordingRepository.GetProgramRecordingBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetProgramRecordingAdvanceSearchColumns()
        {
            
            return this._iProgramRecordingRepository.GetProgramRecordingAdvanceSearchColumns();
           
        }
        

		public virtual List<ProgramRecording> GetProgramRecordingByProgramId(System.Int32? ProgramId)
		{
			return _iProgramRecordingRepository.GetProgramRecordingByProgramId(ProgramId);
		}

		public ProgramRecording GetProgramRecording(System.Int32 ProgramRecordingId)
		{
			return _iProgramRecordingRepository.GetProgramRecording(ProgramRecordingId);
		}

		public ProgramRecording UpdateProgramRecording(ProgramRecording entity)
		{
			return _iProgramRecordingRepository.UpdateProgramRecording(entity);
		}

		public bool DeleteProgramRecording(System.Int32 ProgramRecordingId)
		{
			return _iProgramRecordingRepository.DeleteProgramRecording(ProgramRecordingId);
		}

		public List<ProgramRecording> GetAllProgramRecording()
		{
			return _iProgramRecordingRepository.GetAllProgramRecording();
		}

		public ProgramRecording InsertProgramRecording(ProgramRecording entity)
		{
			 return _iProgramRecordingRepository.InsertProgramRecording(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programrecordingid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programrecordingid))
            {
				ProgramRecording programrecording = _iProgramRecordingRepository.GetProgramRecording(programrecordingid);
                if(programrecording!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(programrecording);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ProgramRecording> programrecordinglist = _iProgramRecordingRepository.GetAllProgramRecording();
            if (programrecordinglist != null && programrecordinglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programrecordinglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ProgramRecording programrecording = new ProgramRecording();
                PostOutput output = new PostOutput();
                programrecording.CopyFrom(Input);
                programrecording = _iProgramRecordingRepository.InsertProgramRecording(programrecording);
                output.CopyFrom(programrecording);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramRecording programrecordinginput = new ProgramRecording();
                ProgramRecording programrecordingoutput = new ProgramRecording();
                PutOutput output = new PutOutput();
                programrecordinginput.CopyFrom(Input);
                ProgramRecording programrecording = _iProgramRecordingRepository.GetProgramRecording(programrecordinginput.ProgramRecordingId);
                if (programrecording!=null)
                {
                    programrecordingoutput = _iProgramRecordingRepository.UpdateProgramRecording(programrecordinginput);
                    if(programrecordingoutput!=null)
                    {
                        output.CopyFrom(programrecordingoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programrecordingid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programrecordingid))
            {
				 bool IsDeleted = _iProgramRecordingRepository.DeleteProgramRecording(programrecordingid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
