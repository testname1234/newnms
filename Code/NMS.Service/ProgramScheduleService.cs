﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramSchedule;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class ProgramScheduleService : IProgramScheduleService
    {
        private IProgramScheduleRepository _iProgramScheduleRepository;

        public ProgramScheduleService(IProgramScheduleRepository iProgramScheduleRepository)
        {
            this._iProgramScheduleRepository = iProgramScheduleRepository;
        }

        public Dictionary<string, string> GetProgramScheduleBasicSearchColumns()
        {

            return this._iProgramScheduleRepository.GetProgramScheduleBasicSearchColumns();

        }

        public List<SearchColumn> GetProgramScheduleAdvanceSearchColumns()
        {

            return this._iProgramScheduleRepository.GetProgramScheduleAdvanceSearchColumns();

        }


        public virtual List<ProgramSchedule> GetProgramScheduleByProgramId(System.Int32 ProgramId)
        {
            return _iProgramScheduleRepository.GetProgramScheduleByProgramId(ProgramId);
        }

        public virtual List<ProgramSchedule> GetProgramScheduleByRecurringTypeId(System.Int32 RecurringTypeId)
        {
            return _iProgramScheduleRepository.GetProgramScheduleByRecurringTypeId(RecurringTypeId);
        }

        public ProgramSchedule GetProgramSchedule(System.Int32 ProgramScheduleId)
        {
            return _iProgramScheduleRepository.GetProgramSchedule(ProgramScheduleId);
        }

        public ProgramSchedule UpdateProgramSchedule(ProgramSchedule entity)
        {
            return _iProgramScheduleRepository.UpdateProgramSchedule(entity);
        }

        public bool DeleteProgramSchedule(System.Int32 ProgramScheduleId)
        {
            return _iProgramScheduleRepository.DeleteProgramSchedule(ProgramScheduleId);
        }

        public List<ProgramSchedule> GetAllProgramSchedule()
        {
            return _iProgramScheduleRepository.GetAllProgramSchedule();
        }

        public ProgramSchedule InsertProgramSchedule(ProgramSchedule entity)
        {
            return _iProgramScheduleRepository.InsertProgramSchedule(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programscheduleid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out programscheduleid))
            {
                ProgramSchedule programschedule = _iProgramScheduleRepository.GetProgramSchedule(programscheduleid);
                if (programschedule != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(programschedule);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ProgramSchedule> programschedulelist = _iProgramScheduleRepository.GetAllProgramSchedule();
            if (programschedulelist != null && programschedulelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programschedulelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramSchedule programschedule = new ProgramSchedule();

                programschedule.CopyFrom(Input);

                bool isExist = _iProgramScheduleRepository.ifExistProgramSchedule(programschedule);
                if (!isExist)
                {
                    programschedule = _iProgramScheduleRepository.InsertProgramSchedule(programschedule);
                    transer.IsSuccess = true;
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[] { "Time Already Reaserved." };
                }
                PostOutput output = new PostOutput();
                output.CopyFrom(programschedule);
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramSchedule programscheduleinput = new ProgramSchedule();
                ProgramSchedule programscheduleoutput = new ProgramSchedule();
                PutOutput output = new PutOutput();
                programscheduleinput.CopyFrom(Input);
                ProgramSchedule programschedule = _iProgramScheduleRepository.GetProgramSchedule(programscheduleinput.ProgramScheduleId);
                if (programschedule != null)
                {
                    programscheduleoutput = _iProgramScheduleRepository.UpdateProgramSchedule(programscheduleinput);
                    if (programscheduleoutput != null)
                    {
                        output.CopyFrom(programscheduleoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programscheduleid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out programscheduleid))
            {
                bool IsDeleted = _iProgramScheduleRepository.DeleteProgramSchedule(programscheduleid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
    }


}
