﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramScreenElement;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ProgramScreenElementService : IProgramScreenElementService 
	{
		private IProgramScreenElementRepository _iProgramScreenElementRepository;
        
		public ProgramScreenElementService(IProgramScreenElementRepository iProgramScreenElementRepository)
		{
			this._iProgramScreenElementRepository = iProgramScreenElementRepository;
		}
        
        public Dictionary<string, string> GetProgramScreenElementBasicSearchColumns()
        {
            
            return this._iProgramScreenElementRepository.GetProgramScreenElementBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetProgramScreenElementAdvanceSearchColumns()
        {
            
            return this._iProgramScreenElementRepository.GetProgramScreenElementAdvanceSearchColumns();
           
        }
        

		public ProgramScreenElement GetProgramScreenElement(System.Int32 ProgramScreenElementId)
		{
			return _iProgramScreenElementRepository.GetProgramScreenElement(ProgramScreenElementId);
		}

		public ProgramScreenElement UpdateProgramScreenElement(ProgramScreenElement entity)
		{
			return _iProgramScreenElementRepository.UpdateProgramScreenElement(entity);
		}

		public bool DeleteProgramScreenElement(System.Int32 ProgramScreenElementId)
		{
			return _iProgramScreenElementRepository.DeleteProgramScreenElement(ProgramScreenElementId);
		}

		public List<ProgramScreenElement> GetAllProgramScreenElement()
		{
			return _iProgramScreenElementRepository.GetAllProgramScreenElement();
		}

		public ProgramScreenElement InsertProgramScreenElement(ProgramScreenElement entity)
		{
			 return _iProgramScreenElementRepository.InsertProgramScreenElement(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programscreenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programscreenelementid))
            {
				ProgramScreenElement programscreenelement = _iProgramScreenElementRepository.GetProgramScreenElement(programscreenelementid);
                if(programscreenelement!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(programscreenelement);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ProgramScreenElement> programscreenelementlist = _iProgramScreenElementRepository.GetAllProgramScreenElement();
            if (programscreenelementlist != null && programscreenelementlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programscreenelementlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ProgramScreenElement programscreenelement = new ProgramScreenElement();
                PostOutput output = new PostOutput();
                programscreenelement.CopyFrom(Input);
                programscreenelement = _iProgramScreenElementRepository.InsertProgramScreenElement(programscreenelement);
                output.CopyFrom(programscreenelement);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramScreenElement programscreenelementinput = new ProgramScreenElement();
                ProgramScreenElement programscreenelementoutput = new ProgramScreenElement();
                PutOutput output = new PutOutput();
                programscreenelementinput.CopyFrom(Input);
                ProgramScreenElement programscreenelement = _iProgramScreenElementRepository.GetProgramScreenElement(programscreenelementinput.ProgramScreenElementId);
                if (programscreenelement!=null)
                {
                    programscreenelementoutput = _iProgramScreenElementRepository.UpdateProgramScreenElement(programscreenelementinput);
                    if(programscreenelementoutput!=null)
                    {
                        output.CopyFrom(programscreenelementoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programscreenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programscreenelementid))
            {
				 bool IsDeleted = _iProgramScreenElementRepository.DeleteProgramScreenElement(programscreenelementid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
