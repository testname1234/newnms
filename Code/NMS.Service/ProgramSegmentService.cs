﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramSegment;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ProgramSegmentService : IProgramSegmentService 
	{
		private IProgramSegmentRepository _iProgramSegmentRepository;
        
		public ProgramSegmentService(IProgramSegmentRepository iProgramSegmentRepository)
		{
			this._iProgramSegmentRepository = iProgramSegmentRepository;
		}
        
        public Dictionary<string, string> GetProgramSegmentBasicSearchColumns()
        {
            
            return this._iProgramSegmentRepository.GetProgramSegmentBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetProgramSegmentAdvanceSearchColumns()
        {
            
            return this._iProgramSegmentRepository.GetProgramSegmentAdvanceSearchColumns();
           
        }
        

		public virtual List<ProgramSegment> GetProgramSegmentByProgramId(System.Int32 ProgramId)
		{
			return _iProgramSegmentRepository.GetProgramSegmentByProgramId(ProgramId);
		}

		public virtual List<ProgramSegment> GetProgramSegmentBySegmentTypeId(System.Int32 SegmentTypeId)
		{
			return _iProgramSegmentRepository.GetProgramSegmentBySegmentTypeId(SegmentTypeId);
		}

		public ProgramSegment GetProgramSegment(System.Int32 ProgramSegmentId)
		{
			return _iProgramSegmentRepository.GetProgramSegment(ProgramSegmentId);
		}

		public ProgramSegment UpdateProgramSegment(ProgramSegment entity)
		{
			return _iProgramSegmentRepository.UpdateProgramSegment(entity);
		}

		public bool DeleteProgramSegment(System.Int32 ProgramSegmentId)
		{
			return _iProgramSegmentRepository.DeleteProgramSegment(ProgramSegmentId);
		}

		public List<ProgramSegment> GetAllProgramSegment()
		{
			return _iProgramSegmentRepository.GetAllProgramSegment();
		}

		public ProgramSegment InsertProgramSegment(ProgramSegment entity)
		{
			 return _iProgramSegmentRepository.InsertProgramSegment(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programsegmentid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programsegmentid))
            {
				ProgramSegment programsegment = _iProgramSegmentRepository.GetProgramSegment(programsegmentid);
                if(programsegment!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(programsegment);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ProgramSegment> programsegmentlist = _iProgramSegmentRepository.GetAllProgramSegment();
            if (programsegmentlist != null && programsegmentlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programsegmentlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ProgramSegment programsegment = new ProgramSegment();
                PostOutput output = new PostOutput();
                programsegment.CopyFrom(Input);
                programsegment = _iProgramSegmentRepository.InsertProgramSegment(programsegment);
                output.CopyFrom(programsegment);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramSegment programsegmentinput = new ProgramSegment();
                ProgramSegment programsegmentoutput = new ProgramSegment();
                PutOutput output = new PutOutput();
                programsegmentinput.CopyFrom(Input);
                ProgramSegment programsegment = _iProgramSegmentRepository.GetProgramSegment(programsegmentinput.ProgramSegmentId);
                if (programsegment!=null)
                {
                    programsegmentoutput = _iProgramSegmentRepository.UpdateProgramSegment(programsegmentinput);
                    if(programsegmentoutput!=null)
                    {
                        output.CopyFrom(programsegmentoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programsegmentid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programsegmentid))
            {
				 bool IsDeleted = _iProgramSegmentRepository.DeleteProgramSegment(programsegmentid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public bool DeleteProgramSegmentByProgramId(System.Int32 ProgramId)
         {
             return _iProgramSegmentRepository.DeleteProgramSegmentByProgramId(ProgramId);
         }
	}
	
	
}
