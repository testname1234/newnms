﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Program;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Helper;
using System.Drawing;
using System.Configuration;
using NMS.Core.Enums;
using System.Net;
using FFMPEGLib;
using System.IO;
using MS.MediaIntegration.API;
using NMS.Core.Models;
using MS.Core.Enums;
using System.Threading.Tasks;
using NMS.Repository;

namespace NMS.Service
{

    public class ProgramService : IProgramService
    {
        private IProgramRepository _iProgramRepository;
        private IEpisodeRepository _iEpisodeRepository;
        private IProgramScheduleRepository _iProgramScheduleRepository;
        private IResourceService _iResourceService;
        private ISegmentRepository _iSegmentRepository;
        private ISlotRepository _iSlotRepository;
        private ISlotScreenTemplateRepository _iSlotScreenTemplateRepository;
        private ISlotTemplateScreenElementRepository _iSlotTemplateScreenElementRepository;
        private ISlotTemplateScreenElementResourceRepository _iSlotTemplateScreenElementResourceRepository;
        private ISlotScreenTemplateResourceRepository _iSlotScreenTemplateResourceRepository;
        private IMosActiveEpisodeRepository _iMosActiveEpisodeRepository;
        private ISlotScreenTemplatekeyRepository _iSlotScreenTemplatekeyRepository;
        private IProgramSegmentRepository _iProgramSegmentRepository;
        private IGuestRepository _iGuestRepository;
        private ICelebrityRepository _iCelebrityRepository;
        private IEditorialCommentRepository _iEditorialCommentRepository;
        private ISlotHeadlineRepository _iSlotHeadlineRepository;
        private IProgramAnchorRepository _iProgramAnchorRepository;
        public ProgramService(IProgramRepository iProgramRepository, IEpisodeRepository iEpisodeRepository, IProgramScheduleRepository iProgramScheduleRepository, IResourceService iResourceService, ISegmentRepository iSegmentRepository, ISlotRepository iSlotRepository, ISlotScreenTemplateRepository iSlotScreenTemplateRepository,
            ISlotTemplateScreenElementRepository iSlotTemplateScreenElementRepository, ISlotScreenTemplateResourceRepository iSlotScreenTemplateResourceRepository,
            IMosActiveEpisodeRepository iMosActiveEpisodeRepository, ISlotScreenTemplatekeyRepository iSlotScreenTemplatekeyRepository,
            IProgramSegmentRepository iProgramSegmentRepository, ISlotTemplateScreenElementResourceRepository iSlotTemplateScreenElementResourceRepository,
            IGuestRepository iGuestRepository, ICelebrityRepository iCelebrityRepository, IEditorialCommentRepository iEditorialCommentRepository,
            ISlotHeadlineRepository iSlotHeadlineRepository, IProgramAnchorRepository iProgramAnchorRepository)
        {
            this._iProgramRepository = iProgramRepository;
            this._iEpisodeRepository = iEpisodeRepository;
            this._iProgramScheduleRepository = iProgramScheduleRepository;
            this._iResourceService = iResourceService;
            this._iSegmentRepository = iSegmentRepository;
            this._iSlotRepository = iSlotRepository;
            this._iSlotScreenTemplateRepository = iSlotScreenTemplateRepository;
            this._iSlotTemplateScreenElementRepository = iSlotTemplateScreenElementRepository;
            this._iSlotScreenTemplateResourceRepository = iSlotScreenTemplateResourceRepository;
            this._iMosActiveEpisodeRepository = iMosActiveEpisodeRepository;
            this._iSlotScreenTemplatekeyRepository = iSlotScreenTemplatekeyRepository;
            this._iProgramSegmentRepository = iProgramSegmentRepository;
            this._iSlotTemplateScreenElementResourceRepository = iSlotTemplateScreenElementResourceRepository;
            this._iGuestRepository = iGuestRepository;
            this._iCelebrityRepository = iCelebrityRepository;
            this._iEditorialCommentRepository = iEditorialCommentRepository;
            this._iSlotHeadlineRepository = iSlotHeadlineRepository;
            this._iProgramAnchorRepository = iProgramAnchorRepository;
        }

        public Dictionary<string, string> GetProgramBasicSearchColumns()
        {

            return this._iProgramRepository.GetProgramBasicSearchColumns();

        }

        public List<SearchColumn> GetProgramAdvanceSearchColumns()
        {

            return this._iProgramRepository.GetProgramAdvanceSearchColumns();

        }


        public virtual List<Program> GetProgramByChannelIdAndUserId(int ChannelId, int UserId)
        {
            return _iProgramRepository.GetProgramByChannelIdAndUserId(ChannelId, UserId);
        }

        public virtual List<Program> GetProgramByChannelId(System.Int32? ChannelId)
        {
            return _iProgramRepository.GetProgramByChannelId(ChannelId);
        }

        public Program GetProgram(System.Int32 ProgramId)
        {
            return _iProgramRepository.GetProgram(ProgramId);
        }

        public Program UpdateProgram(Program entity)
        {
            return _iProgramRepository.UpdateProgram(entity);
        }

        public bool DeleteProgram(System.Int32 ProgramId)
        {
            return _iProgramRepository.DeleteProgram(ProgramId);
        }

        public List<Program> GetAllProgram()
        {
            return _iProgramRepository.GetAllProgram();
        }

        public Program InsertProgram(Program entity)
        {
            return _iProgramRepository.InsertProgram(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out programid))
            {
                Program program = _iProgramRepository.GetProgram(programid);
                if (program != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(program);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }//

        public DataTransfer<List<GetOutput>> GetAllProgramsForReporter(bool? withoutFolder)
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Program> programlist = new List<Program>();
            if (withoutFolder.HasValue && withoutFolder.Value == true)
            {
                programlist = _iProgramRepository.GetAllProgram();
                Program forLiveProgram = new Program();
                forLiveProgram.ProgramId = -1;
                forLiveProgram.Name = "Hourly Slot";
                programlist.Add(forLiveProgram);
            }
            else
            {
                programlist = _iProgramRepository.GetAllProgramsForReporter();
            }

            if (programlist != null && programlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }

        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Program> programlist = _iProgramRepository.GetAllProgram();
            if (programlist != null && programlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Program program = new Program();
                PostOutput output = new PostOutput();
                program.CopyFrom(Input);
                List<Program> programs = _iProgramRepository.GetProgramByMultipleKeyValue("Name", Input.Name, "ChannelId", Input.ChannelId, Operands.Equal);
                if (programs != null)
                {
                    foreach (Program prog in programs)
                    {
                        if (!prog.IsActive)
                            prog.IsActive = true;
                        prog.MinStoryCount = program.MinStoryCount;
                        prog.MaxStoryCount = program.MaxStoryCount;
                        prog.ProgramTypeId = program.ProgramTypeId;
                        _iProgramRepository.UpdateProgram(prog);
                        output.CopyFrom(prog);
                        transer.Data = output;
                        transer.Errors = new string[] { "Program Already Exist" };
                        transer.IsSuccess = true;
                        break;
                    }
                }
                else
                {
                    program = _iProgramRepository.InsertProgram(program);
                    output.CopyFrom(program);
                    transer.IsSuccess = true;
                    transer.Data = output;
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Program programinput = new Program();
                Program programoutput = new Program();
                PutOutput output = new PutOutput();
                programinput.CopyFrom(Input);
                Program program = _iProgramRepository.GetProgram(programinput.ProgramId);
                if (program != null)
                {
                    programoutput = _iProgramRepository.UpdateProgram(programinput);
                    if (programoutput != null)
                    {
                        output.CopyFrom(programoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out programid))
            {
                bool IsDeleted = _iProgramRepository.DeleteProgram(programid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<Episode> GetProgramEpisode(int programId, DateTime date)
        {
            List<Episode> episodes = _iEpisodeRepository.GetEpisodeByProgramIdAndDate(programId, date);
            if (episodes == null || episodes.Count == 0)
            {
                List<ProgramSchedule> schedule = _iProgramScheduleRepository.GetProgramScheduleByProgramId(programId);
                if (schedule != null && schedule.Count > 0)
                {
                    episodes = new List<Episode>();
                    foreach (var sce in schedule.Where(x => x.WeekDayId == (int)date.DayOfWeek))
                    {
                        Episode episode = new Episode();
                        episode.CreationDate = DateTime.UtcNow;
                        episode.LastUpdateDate = episode.CreationDate;
                        episode.IsActive = true;
                        episode.From = date.Date.AddTicks(sce.StartTime.Ticks);
                        episode.To = date.Date.AddTicks(sce.EndTime.Ticks);
                        episode.ProgramId = programId;
                        episodes.Add(_iEpisodeRepository.InsertEpisode(episode));

                        List<ProgramSegment> programSegments = _iProgramSegmentRepository.GetProgramSegmentByProgramId(programId);

                        foreach (ProgramSegment programSegment in programSegments)
                        {
                            if (programSegment.SegmentTypeId == 1 || programSegment.SegmentTypeId == 2)
                            {
                                Segment segment = new Segment();
                                segment.StoryCount = programSegment.StoryCount;
                                segment.CreationDate = DateTime.UtcNow;
                                segment.LastUpdateDate = segment.CreationDate;
                                segment.IsActive = true;
                                segment.Duration = programSegment.Duration;
                                segment.Name = programSegment.Name;
                                segment.SegmentTypeId = programSegment.SegmentTypeId;
                                segment.SequnceNumber = programSegment.SequenceNumber;
                                segment.EpisodeId = episodes.Last().EpisodeId;

                                _iSegmentRepository.InsertSegment(segment);
                            }
                        }
                    }
                }
            }

            if (episodes != null)
            {
                foreach (var episode in episodes)
                {
                    FillEpisode(episode);
                }
            }
            return episodes;
        }

        public Episode GetProgramEpisodeByRundownId(int runDownId)
        {
            MosActiveEpisode mosActEpisode = _iMosActiveEpisodeRepository.GetMosActiveEpisode(runDownId);

            if (mosActEpisode != null)
            {
                Episode episode = new Episode();
                episode = _iEpisodeRepository.GetEpisode(Convert.ToInt32(mosActEpisode.EpisodeId));
                if (episode != null)
                {
                    FillEpisode(episode);
                }
                return episode;
            }

            return null;


        }

        public void FillEpisode(Episode episode, DateTime? lastUpdateDate = null, bool isAssignedToNLE = false, bool isAssignedToStoryWriter = false)
        {
            episode.Segments = _iSegmentRepository.GetSegmentByEpisodeId(episode.EpisodeId);
            if (episode.Segments != null)
            {
                foreach (var segment in episode.Segments)
                {
                    segment.Slots = _iSlotRepository.GetSlotBySegmentId(segment.SegmentId);
                    if (segment.Slots != null)
                    {
                        foreach (var slot in segment.Slots)
                        {
                            FillSlot(slot, lastUpdateDate, isAssignedToNLE, isAssignedToStoryWriter);
                        }
                    }
                }
            }

            if (lastUpdateDate.HasValue)
            {
                if (episode.Segments != null)
                {
                    for (int i = 0; i < episode.Segments.Count; i++)
                    {
                        if (episode.Segments[i].Slots != null)
                        {
                            for (int j = 0; j < episode.Segments[i].Slots.Count; j++)
                            {
                                if (episode.Segments[i].Slots[j].SlotScreenTemplates == null || episode.Segments[i].Slots[j].SlotScreenTemplates.Count == 0)
                                {
                                    episode.Segments[i].Slots.RemoveAt(j);
                                    j--;
                                }
                            }
                        }


                        if (episode.Segments[i].Slots == null || episode.Segments[i].Slots.Count == 0)
                        {
                            episode.Segments.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
        }

        public void FillSlot(Slot slot, DateTime? lastUpdateDate = null, bool isAssignedToNLE = false, bool isAssignedToStoryWriter = false)
        {
            slot.SlotScreenTemplates = _iSlotScreenTemplateRepository.GetSlotScreenTemplateBySlotId(slot.SlotId);
            slot.Guest = new List<Guest>();
            slot.SlotHeadlines = new List<SlotHeadline>();
            slot.Guest = _iGuestRepository.GetAllGuestBySlotId(slot.SlotId);
            slot.EditorialComments = _iEditorialCommentRepository.GetEditorialCommentBySlotId(slot.SlotId);
            if (slot.AnchorId.HasValue && slot.AnchorId.Value > 0)
            {
                slot.AnchorName = _iCelebrityRepository.GetCelebrity(slot.AnchorId.Value)?.Name;
            }
            slot.SlotHeadlines = _iSlotHeadlineRepository.GetSlotHeadlineBySlotId(slot.SlotId);
            if (slot.SlotScreenTemplates != null)
            {
                if (lastUpdateDate.HasValue)
                    slot.SlotScreenTemplates = slot.SlotScreenTemplates.Where(x => x.LastUpdateDate > lastUpdateDate &&
                        ((isAssignedToNLE && x.IsAssignedToNle.HasValue && x.IsAssignedToNle.Value) || !isAssignedToNLE) &&
                        ((isAssignedToStoryWriter && x.IsAssignedToStoryWriter.HasValue && x.IsAssignedToStoryWriter.Value) || !isAssignedToStoryWriter)).ToList();
                foreach (var slotScreenTemplates in slot.SlotScreenTemplates)
                {
                    slotScreenTemplates.SlotTemplateScreenElements = _iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElementBySlotScreenTemplateId(slotScreenTemplates.SlotScreenTemplateId);
                    slotScreenTemplates.SlotScreenTemplateResources = _iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceBySlotScreenTemplateId(slotScreenTemplates.SlotScreenTemplateId);
                    slotScreenTemplates.SlotScreenTemplatekeys = _iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekeyBySlotScreenTemplateId(slotScreenTemplates.SlotScreenTemplateId);
                    if (slotScreenTemplates.SlotTemplateScreenElements != null)
                    {
                        foreach (var element in slotScreenTemplates.SlotTemplateScreenElements)
                        {
                            element.SlotTemplateScreenElementResources = _iSlotTemplateScreenElementResourceRepository.GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(element.SlotTemplateScreenElementId);
                        }
                    }
                }
            }

            if (slot.Guest != null)
            {
                foreach (var guest in slot.Guest)
                {
                    int celebId = Convert.ToInt32(guest.CelebrityId);
                    guest.CelebrityName = _iCelebrityRepository.GetCelebrity(celebId).Name;

                }

            }

        }

        public string GenerateScreenTemplateImage(Core.DataTransfer.ScreenTemplate.PostInput input)
        {
            MMS.ScreenTemplateGenerator.ScreenTemplate template = new MMS.ScreenTemplateGenerator.ScreenTemplate();
            template.Background = new MMS.ScreenTemplateGenerator.Background();
            template.Background.ImageUrl = AppSettings.MediaServerUrl + "/getresource/" + input.BackgroundImageUrl;
            if (input.BackgroundRepeat == "repeat")
                template.Background.RepeatX = template.Background.RepeatY = true;
            else if (input.BackgroundRepeat == "repeat-x") template.Background.RepeatX = true;
            else if (input.BackgroundRepeat == "repeat-y") template.Background.RepeatY = true;
            template.Background.Color = input.BackgroundColor;
            template.Elements = new List<MMS.ScreenTemplateGenerator.ProgramScreenElement>();
            foreach (var ele in input.Elements)
            {
                var element = new MMS.ScreenTemplateGenerator.ProgramScreenElement();
                element.ScreenElementId = Convert.ToInt32(ele.ScreenElementId);

                if (element.ScreenElementId == 22)
                {
                    element.ImageUrl = AppSettings.MediaServerUrl + "/getthumb/" + ele.ImageGuid;
                }
                else if (element.ScreenElementId != 3)
                {
                    element.ImageUrl = AppSettings.MediaServerUrl + "/getresource/" + ele.ImageGuid;
                }
                else
                {
                    element.ImageUrl = AppSettings.MediaServerUrl + "/getthumb/" + ele.ImageGuid;
                }
                element.Left = Convert.ToInt32(Convert.ToDouble(ele.Left));
                element.Height = Convert.ToInt32(Convert.ToDouble(ele.Bottom)) - Convert.ToInt32(Convert.ToDouble(ele.Top));
                element.Width = Convert.ToInt32(Convert.ToDouble(ele.Right)) - Convert.ToInt32(Convert.ToDouble(ele.Left));
                element.Zindex = Convert.ToInt32(Convert.ToDouble(ele.Zindex));
                element.Right = Convert.ToInt32(Convert.ToDouble(ele.Right));
                element.Top = Convert.ToInt32(Convert.ToDouble(ele.Top));
                element.Bottom = Convert.ToInt32(Convert.ToDouble(ele.Bottom));
                template.Elements.Add(element);
            }
            template.Width = 540;
            template.Height = 342;
            MMS.ScreenTemplateGenerator.TemplateGenerator generator = new MMS.ScreenTemplateGenerator.TemplateGenerator();
            Image img = generator.Generate(template);
            if (!System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/MediaResources"))
                System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/MediaResources");
            string name = "/MediaResources" + "/" + Guid.NewGuid().ToString() + ".jpg";
            img.Save(AppDomain.CurrentDomain.BaseDirectory + name);
            return name;
        }

        public List<Episode> GetProgramEpisodeByDate(int userId, Core.Enums.TeamRoles teamRoles, DateTime dateTime, DateTime lastUpdateDate)
        {
            List<Episode> episodes = _iEpisodeRepository.GetProgramEpisodeByDate(userId, teamRoles, dateTime, lastUpdateDate);
            if (episodes != null)
            {
                foreach (var episode in episodes)
                {
                    FillEpisode(episode, lastUpdateDate, teamRoles == TeamRoles.NLE, teamRoles == TeamRoles.StoryWriter);
                }
            }
            return episodes;
        }


        public List<Program> GetProgramByUserTeam(int userid)
        {
            return _iProgramRepository.GetProgramByUserTeam(userid);
        }

        private void CreateBucket(string bucketpath)
        {
            MSApi api = new MSApi();
            string path = "";
            bool isExist = false;
            bucketpath = bucketpath.Substring(0, bucketpath.LastIndexOf("\\"));
            string[] folders = bucketpath.Split(new char[] { '\\' });
            foreach (string item in folders)
            {
                path += "\\" + item;
                api.CreateSubBucket(Convert.ToInt32(ConfigurationManager.AppSettings["BucketId"].ToString()), ConfigurationManager.AppSettings["ApiKey"].ToString(), path);
            }

        }

        public EpisodePreviewModel GetRunDownPreviewByEpisodeId(int episodeid)
        {

            int DefaultDuration = 5;
            IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
            ISegmentService segmentservice = IoC.Resolve<ISegmentService>("SegmentService");
            ISlotService slotService = IoC.Resolve<ISlotService>("SlotService");
            ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
            ISlotTemplateScreenElementService slotTemplateScreenElementService = IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
            MS.Core.IService.IServerService serverservice = IoC.Resolve<MS.Core.IService.IServerService>("ServerService");

            EpisodePreviewModel model = new EpisodePreviewModel();
            List<FilesOrder> downloadedFilePath = new List<FilesOrder>();
            List<Segment> segments = segmentservice.GetSegmentByEpisodeId(episodeid);

            model.EpisodeId = episodeid;

            if (segments != null)
            {
                segments = segments.OrderBy(x => x.SequnceNumber).ToList();
                Guid guid = Guid.NewGuid();
                string folderPath = ConfigurationManager.AppSettings["TempContentLocation"].ToString() + guid.ToString();
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                try
                {
                    List<Slot> slots = new List<Slot>();
                    foreach (Segment segment in segments)
                    {
                        List<Slot> _slots = slotService.GetSlotBySegmentId(segment.SegmentId);

                        if (_slots != null)
                        {
                            _slots = _slots.OrderBy(x => x.SequnceNumber).ToList();

                            slots.AddRange(_slots);
                        }
                    }

                    Parallel.ForEach(slots, slot =>
                    //foreach (Slot slot in slots)
                    {
                        List<SlotScreenTemplate> slotscreentemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slot.SlotId);

                        if (slotscreentemplates != null)
                        {
                            slotscreentemplates = slotscreentemplates.Where(x => !x.ParentId.HasValue).OrderBy(x => x.SequenceNumber).ToList();
                            List<FilesOrder> slotFilePath = new List<FilesOrder>();
                            Parallel.ForEach(slotscreentemplates, slotscreentemplate =>
                            //foreach (SlotScreenTemplate slotscreentemplate in slotscreentemplates)
                            {
                                bool flag = false;
                                Dictionary<int, int> VideoDurations = new Dictionary<int, int>();
                                Guid fileguid = Guid.NewGuid();
                                try
                                {
                                    System.Net.WebRequest request = System.Net.WebRequest.Create(ConfigurationManager.AppSettings["MsApi"].ToString() + "/api/Resource/getresource/" + slotscreentemplate.ThumbGuid.ToString());
                                    System.Net.WebResponse response = request.GetResponse();
                                    System.IO.Stream responseStream = response.GetResponseStream();
                                    using (Bitmap bitmap = new Bitmap(responseStream))
                                        bitmap.Save(folderPath + "\\" + fileguid.ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                    flag = true;
                                }
                                catch { }
                                if (flag)
                                {
                                    int Duration = (slotscreentemplate.Duration.HasValue && slotscreentemplate.Duration.Value > 0) ? slotscreentemplate.Duration.Value : DefaultDuration;

                                    List<SlotTemplateScreenElement> slotTemplateScreenElements = slotTemplateScreenElementService.GetSlotTemplateScreenElementBySlotScreenTemplateId(slotscreentemplate.SlotScreenTemplateId);
                                    if (slotTemplateScreenElements != null)
                                    {
                                        slotTemplateScreenElements = slotTemplateScreenElements.Where(a => a.ScreenElementId == 3).ToList();
                                        if (slotTemplateScreenElements.Count > 0)
                                        {
                                            foreach (var slotTemplateScreenElement in slotTemplateScreenElements)
                                            {
                                                VideoDurations.Add(slotTemplateScreenElement.SlotTemplateScreenElementId, (int)Math.Floor(FFMPEGLib.FFMPEG.GetDuration(ConfigurationManager.AppSettings["MsApi"].ToString() + "/api/Resource/getresource/" + slotTemplateScreenElement.ResourceGuid.ToString())));
                                            }
                                            Duration = VideoDurations.Select(x => x.Value).Max() > Duration ? VideoDurations.Select(x => x.Value).Max() : Duration;
                                        }
                                        else
                                        {
                                            Duration = 5;
                                        }
                                    }


                                    int From = 0;
                                    string videopath = folderPath + "\\" + fileguid.ToString() + ".ts";
                                    FFMPEG.CreateVideofromThumb(folderPath + "\\" + fileguid.ToString() + ".jpg", folderPath + "\\", fileguid.ToString() + ".ts", TimeSpan.FromSeconds(Duration));
                                    lock (slotFilePath)
                                        slotFilePath.Add(new FilesOrder() { FileName = videopath, SequenceNumber = slotscreentemplate.SequenceNumber.HasValue ? slotscreentemplate.SequenceNumber.Value : 1 });

                                    if (slotTemplateScreenElements != null)
                                    {
                                        slotTemplateScreenElements = slotTemplateScreenElements.Where(a => a.ScreenElementId == 3 || a.ScreenElementId == 13 || a.ScreenElementId == 8).ToList();

                                        if (slotTemplateScreenElements != null)
                                        {
                                            foreach (SlotTemplateScreenElement slotTemplateScreenElement in slotTemplateScreenElements)
                                            {

                                                byte[] overlapdata = null;
                                                using (WebClient client = new WebClient())
                                                {
                                                    if (slotTemplateScreenElement.ResourceGuid != null)
                                                    {
                                                        overlapdata = client.DownloadData(ConfigurationManager.AppSettings["MsApi"].ToString() + "/api/Resource/getresource/" + slotTemplateScreenElement.ResourceGuid.ToString());
                                                    }
                                                }

                                                if (overlapdata.Length > 1200)
                                                {
                                                    Guid embeddedVideoGuid = Guid.NewGuid();
                                                    Guid outputGuid = Guid.NewGuid();
                                                    string path = "";

                                                    if (slotTemplateScreenElement.ScreenElementId == 3)
                                                    {
                                                        path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".ts";
                                                        File.WriteAllBytes(path, overlapdata);
                                                        FFMPEG.EmbedVideo(videopath, path, folderPath + "\\" + outputGuid + ".ts", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(VideoDurations[slotTemplateScreenElement.SlotTemplateScreenElementId]));
                                                    }
                                                    else if (slotTemplateScreenElement.ScreenElementId == 13 || slotTemplateScreenElement.ScreenElementId == 8)
                                                    {
                                                        path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".jpg";
                                                        File.WriteAllBytes(path, overlapdata);

                                                        using (System.IO.MemoryStream responseStream = new MemoryStream(overlapdata))
                                                        {
                                                            using (Bitmap bitmap = new Bitmap(responseStream))
                                                                bitmap.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        }
                                                        FFMPEG.EmbedImage(videopath, path, folderPath + "\\" + outputGuid + ".ts", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(Duration));
                                                    }
                                                    File.Delete(videopath);
                                                    File.Move(folderPath + "\\" + outputGuid + ".ts", videopath);
                                                }
                                            }
                                        }
                                    }
                                }
                            });

                            if (slotFilePath != null && slotFilePath.Count > 0)
                            {
                                Guid slotoutputGuid = Guid.NewGuid();
                                string slotoutputpath = folderPath + "\\" + slotoutputGuid + ".mp4";
                                slotFilePath = slotFilePath.OrderBy(x => x.SequenceNumber).ToList();
                                FFMPEG.ConcatenateVideosForPreviewWithEncoding(slotFilePath.Select(x => x.FileName).ToList(), slotoutputpath);
                                foreach (var path in slotFilePath)
                                {
                                    path.ParentSequenceNumber = slot.SequnceNumber;
                                }
                                lock (downloadedFilePath)
                                {
                                    downloadedFilePath.AddRange(slotFilePath);
                                }

                                HttpStatusCode code = HttpStatusCode.UseProxy;

                                byte[] slotpreviewFile;
                                using (WebClient client = new WebClient())
                                {
                                     //slotpreviewFile = client.DownloadData(slotoutputpath);
                                     slotpreviewFile = File.ReadAllBytes(slotoutputpath);
                                    if (slotpreviewFile.Length > 1200)
                                    {
                                        MSApi api = new MSApi();
                                        string bucketpath = @"SlotPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";

                                        MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

                                        resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
                                        resource.ResourceTypeId = ((int)NMS.Core.Enums.ResourceTypes.Video).ToString();
                                        resource.CreationDate = DateTime.UtcNow.ToString();
                                        resource.LastUpdateDate = resource.CreationDate;
                                        resource.IsActive = "True";
                                        resource.BucketId = (int)NMSBucket.NMSBucket;
                                        resource.Source = slotoutputGuid + ".mp4";
                                        resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
                                        resource.IsPrivate = "True";

                                        MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);

                                        try { CreateBucket(bucketpath); }
                                        catch (Exception ex) { }

                                        code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", slotpreviewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());

                                        if (code == HttpStatusCode.OK)
                                        {
                                            Slot slott = slotService.GetSlot(slot.SlotId);
                                            slott.PreviewGuid = output.Guid;
                                            slotService.UpdateSlot(slott);
                                            model.SlotPreviews.Add(slot.SlotId, output.Guid.ToString());
                                        }

                                    }

                                }

                            }

                        }
                    });


                    if (downloadedFilePath != null && downloadedFilePath.Count > 0)
                    {
                        Guid EpisodePreviewGuid = Guid.NewGuid();
                        string episodepreviewpath = folderPath + "\\" + EpisodePreviewGuid + ".mp4";
                        if (downloadedFilePath.Count > 1)
                        {
                            downloadedFilePath = downloadedFilePath.OrderBy(x => x.SequenceNumber).OrderBy(x => x.ParentSequenceNumber).ToList();
                            FFMPEG.ConcatenateVideosForPreviewWithEncoding(downloadedFilePath.Select(x => x.FileName).ToList(), episodepreviewpath);
                            HttpStatusCode code = HttpStatusCode.UseProxy;

                            byte[] previewFile;
                            using (WebClient client = new WebClient())
                            {
                                previewFile = client.DownloadData(episodepreviewpath);
                                if (previewFile.Length > 1200)
                                {
                                    MSApi api = new MSApi();
                                    string bucketpath = @"RunDownPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";

                                    MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

                                    resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
                                    resource.ResourceTypeId = ((int)NMS.Core.Enums.ResourceTypes.Video).ToString();
                                    resource.CreationDate = DateTime.UtcNow.ToString();
                                    resource.LastUpdateDate = resource.CreationDate;
                                    resource.IsActive = "True";
                                    resource.BucketId = (int)NMSBucket.NMSBucket;
                                    resource.Source = EpisodePreviewGuid + ".mp4";
                                    resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
                                    resource.IsPrivate = "True";

                                    MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);

                                    try { CreateBucket(bucketpath); }
                                    catch (Exception ex) { }

                                    code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", previewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());
                                    if (code == HttpStatusCode.OK)
                                    {
                                        Episode episode = episodeService.GetEpisode(episodeid);
                                        episode.PreviewGuid = output.Guid;
                                        episodeService.UpdateEpisode(episode);
                                        Directory.Delete(folderPath, true);
                                        model.EpisodePreview = output.Guid;
                                        return model;
                                    }
                                }

                            }

                        }
                        else
                        {
                            File.Move(downloadedFilePath[0].FileName, episodepreviewpath);
                            Episode episode = episodeService.GetEpisode(episodeid);
                            episode.PreviewGuid = new Guid(model.SlotPreviews.Select(x => x.Value).First());
                            episodeService.UpdateEpisode(episode);
                            Directory.Delete(folderPath, true);
                            model.EpisodePreview = episode.PreviewGuid;
                            return model;
                        }
                    }
                }
                catch
                {
                    if (Directory.Exists(folderPath))
                        Directory.Delete(folderPath, true);
                    throw;
                }
            }
            return null;

        }


        public List<SlotScreenTemplate> CopySlotInfo(int fromSlotID, Slot toSlot)
        {
            DateTime dt = DateTime.UtcNow;
            Slot fromSlot = _iSlotRepository.GetSlot(fromSlotID);
            FillSlot(fromSlot);

            fromSlot.SlotId = toSlot.SlotId;

            if (fromSlot.SlotScreenTemplates != null)
            {
                foreach (var slotTemplate in fromSlot.SlotScreenTemplates)
                {
                    slotTemplate.SlotId = toSlot.SlotId;
                    slotTemplate.SlotScreenTemplateId = 0;
                    slotTemplate.LastUpdateDate = slotTemplate.CreatonDate = dt;
                    var _slotTemplate = _iSlotScreenTemplateRepository.InsertSlotScreenTemplate(slotTemplate);
                    slotTemplate.SlotScreenTemplateId = _slotTemplate.SlotScreenTemplateId;

                    if (slotTemplate.SlotTemplateScreenElements != null)
                    {
                        foreach (var element in slotTemplate.SlotTemplateScreenElements)
                        {
                            element.SlotScreenTemplateId = slotTemplate.SlotScreenTemplateId;
                            element.SlotTemplateScreenElementId = 0;
                            element.LastUpdateDate = element.CreationDate = dt;
                            var _element = _iSlotTemplateScreenElementRepository.InsertSlotTemplateScreenElement(element);
                            element.SlotTemplateScreenElementId = _element.SlotTemplateScreenElementId;

                            if (element.SlotTemplateScreenElementResources != null)
                            {
                                foreach (var resource in element.SlotTemplateScreenElementResources)
                                {
                                    resource.SlotScreenTemplateId = slotTemplate.SlotScreenTemplateId;
                                    resource.SlotTemplateScreenElementId = element.SlotTemplateScreenElementId;
                                    resource.SlotTemplateScreenElementResourceId = 0;
                                    resource.LastUpdateDate = resource.CreationDate = dt;
                                    var _resource = _iSlotTemplateScreenElementResourceRepository.InsertSlotTemplateScreenElementResource(resource);
                                    resource.SlotTemplateScreenElementResourceId = _resource.SlotTemplateScreenElementResourceId;
                                }
                            }
                        }
                    }

                    if (slotTemplate.SlotScreenTemplatekeys != null)
                    {
                        foreach (var element in slotTemplate.SlotScreenTemplatekeys)
                        {
                            element.SlotScreenTemplateId = slotTemplate.SlotScreenTemplateId;
                            element.SlotScreenTemplatekeyId = 0;
                            element.Updateddate = element.CreationDate = dt;
                            var _element = _iSlotScreenTemplatekeyRepository.InsertSlotScreenTemplatekey(element);
                            element.SlotScreenTemplatekeyId = _element.SlotScreenTemplatekeyId;
                        }
                    }

                    if (slotTemplate.SlotScreenTemplateResources != null)
                    {
                        foreach (var element in slotTemplate.SlotScreenTemplateResources)
                        {
                            element.SlotScreenTemplateId = slotTemplate.SlotScreenTemplateId;
                            element.SlotScreenTemplateResourceId = 0;
                            element.CreationDate = dt;
                            var _element = _iSlotScreenTemplateResourceRepository.InsertSlotScreenTemplateResource(element);
                            element.SlotScreenTemplateResourceId = _element.SlotScreenTemplateResourceId;
                        }
                    }
                }
            }
            return fromSlot.SlotScreenTemplates;
        }

        public int GetProgramIdByBroadcastedTime(int second, int interval)
        {
            return _iProgramRepository.GetProgramIdByBroadcastedTime(second, interval);
        }

        public List<Celebrity> GetProgramAnchor(int programId)
        {
            var response = new List<Celebrity>();
            var programAnchors = _iProgramAnchorRepository.GetProgramAnchorByProgramId(programId);
            if (programAnchors != null && programAnchors.Count > 0)
            {
                response = _iCelebrityRepository.GetCelebrityByIds(programAnchors.Select(x => x.CelebrityId).ToList());
            }

            return response;
        }
    }

    class FilesOrder
    {
        public string FileName { get; set; }
        public int SequenceNumber { get; set; }
        public int ParentSequenceNumber { get; set; }
    }

}
