﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramSetMapping;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class ProgramSetMappingService : IProgramSetMappingService
    {
        private IProgramSetMappingRepository _iProgramSetMappingRepository;

        public ProgramSetMappingService(IProgramSetMappingRepository iProgramSetMappingRepository)
        {
            this._iProgramSetMappingRepository = iProgramSetMappingRepository;
        }

        public Dictionary<string, string> GetProgramSetMappingBasicSearchColumns()
        {

            return this._iProgramSetMappingRepository.GetProgramSetMappingBasicSearchColumns();

        }

        public List<SearchColumn> GetProgramSetMappingAdvanceSearchColumns()
        {

            return this._iProgramSetMappingRepository.GetProgramSetMappingAdvanceSearchColumns();

        }


        public ProgramSetMapping GetProgramSetMapping(System.Int32 ProgramSetMappingId)
        {
            return _iProgramSetMappingRepository.GetProgramSetMapping(ProgramSetMappingId);
        }

        public ProgramSetMapping UpdateProgramSetMapping(ProgramSetMapping entity)
        {
            return _iProgramSetMappingRepository.UpdateProgramSetMapping(entity);
        }

        public bool DeleteProgramSetMapping(System.Int32 ProgramSetMappingId)
        {
            return _iProgramSetMappingRepository.DeleteProgramSetMapping(ProgramSetMappingId);
        }

        public List<ProgramSetMapping> GetAllProgramSetMapping()
        {
            return _iProgramSetMappingRepository.GetAllProgramSetMapping();
        }

        public ProgramSetMapping InsertProgramSetMapping(ProgramSetMapping entity)
        {
            return _iProgramSetMappingRepository.InsertProgramSetMapping(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programsetmappingid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out programsetmappingid))
            {
                ProgramSetMapping programsetmapping = _iProgramSetMappingRepository.GetProgramSetMapping(programsetmappingid);
                if (programsetmapping != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(programsetmapping);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ProgramSetMapping> programsetmappinglist = _iProgramSetMappingRepository.GetAllProgramSetMapping();
            if (programsetmappinglist != null && programsetmappinglist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programsetmappinglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramSetMapping programsetmapping = new ProgramSetMapping();
                PostOutput output = new PostOutput();
                programsetmapping.CopyFrom(Input);
                programsetmapping = _iProgramSetMappingRepository.InsertProgramSetMapping(programsetmapping);
                output.CopyFrom(programsetmapping);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramSetMapping programsetmappinginput = new ProgramSetMapping();
                ProgramSetMapping programsetmappingoutput = new ProgramSetMapping();
                PutOutput output = new PutOutput();
                programsetmappinginput.CopyFrom(Input);
                ProgramSetMapping programsetmapping = _iProgramSetMappingRepository.GetProgramSetMapping(programsetmappinginput.ProgramSetMappingId);
                if (programsetmapping != null)
                {
                    programsetmappingoutput = _iProgramSetMappingRepository.UpdateProgramSetMapping(programsetmappinginput);
                    if (programsetmappingoutput != null)
                    {
                        output.CopyFrom(programsetmappingoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programsetmappingid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out programsetmappingid))
            {
                bool IsDeleted = _iProgramSetMappingRepository.DeleteProgramSetMapping(programsetmappingid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public void SyncProgramSetMapping()
        {
            ProgramSetMapping _setMappingEntity = new ProgramSetMapping();
            _setMappingEntity = _iProgramSetMappingRepository.GetProgramSetMappingByLastUpdateDate();
            DateTime LastUpdateDate;

            if (_setMappingEntity != null)
                LastUpdateDate = _setMappingEntity.LastUpdateDate.Value;
            else
                LastUpdateDate = DateTime.Today.AddYears(-2);

            MMS.Integration.DataTransfer<List<MMS.Integration.FPC.DataTransfer.ProgramSetMapping>> ForeignMapping = new MMS.Integration.DataTransfer<List<MMS.Integration.FPC.DataTransfer.ProgramSetMapping>>();

            MMS.Integration.FPC.FPCAPI foreignCall = new MMS.Integration.FPC.FPCAPI();
            ForeignMapping = foreignCall.GetProgramSetMappingByLastUpdateDate(LastUpdateDate);

            if (ForeignMapping.IsSuccess && ForeignMapping.Data != null)
            {
                List<ProgramSetMapping> listProgramSetMapping = new List<ProgramSetMapping>();
                listProgramSetMapping.CopyFrom(ForeignMapping.Data);
                foreach (ProgramSetMapping localMapping in listProgramSetMapping)
                {
                    foreach (var foreignMapping in ForeignMapping.Data)
                    {
                        if (localMapping.ProgramSetMappingId == foreignMapping.ProgramSetMappingId)
                        {

                            localMapping.ProgramId = foreignMapping.ProgramChannelMappingId;
                            _iProgramSetMappingRepository.InsertProgramSetMapping(localMapping);
                            break;
                        }
                    }
                }
            }


        }
    }


}
