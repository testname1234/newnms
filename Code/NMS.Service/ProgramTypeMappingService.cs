﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramTypeMapping;
using NMS.Core;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class ProgramTypeMappingService : IProgramTypeMappingService 
	{
		private IProgramTypeMappingRepository _iProgramTypeMappingRepository;
        
		public ProgramTypeMappingService(IProgramTypeMappingRepository iProgramTypeMappingRepository)
		{
			this._iProgramTypeMappingRepository = iProgramTypeMappingRepository;
		}
        
        public Dictionary<string, string> GetProgramTypeMappingBasicSearchColumns()
        {
            
            return this._iProgramTypeMappingRepository.GetProgramTypeMappingBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetProgramTypeMappingAdvanceSearchColumns()
        {
            
            return this._iProgramTypeMappingRepository.GetProgramTypeMappingAdvanceSearchColumns();
           
        }
        

		public virtual List<ProgramTypeMapping> GetProgramTypeMappingByProgramId(System.Int32? ProgramId)
		{
			return _iProgramTypeMappingRepository.GetProgramTypeMappingByProgramId(ProgramId);
		}

		public virtual List<ProgramTypeMapping> GetProgramTypeMappingByProgramTypeId(System.Int32? ProgramTypeId)
		{
			return _iProgramTypeMappingRepository.GetProgramTypeMappingByProgramTypeId(ProgramTypeId);
		}

		public ProgramTypeMapping GetProgramTypeMapping(System.Int32 ProgramTypeMappingId)
		{
			return _iProgramTypeMappingRepository.GetProgramTypeMapping(ProgramTypeMappingId);
		}

		public ProgramTypeMapping UpdateProgramTypeMapping(ProgramTypeMapping entity)
		{
			return _iProgramTypeMappingRepository.UpdateProgramTypeMapping(entity);
		}

		public bool DeleteProgramTypeMapping(System.Int32 ProgramTypeMappingId)
		{
			return _iProgramTypeMappingRepository.DeleteProgramTypeMapping(ProgramTypeMappingId);
		}

		public List<ProgramTypeMapping> GetAllProgramTypeMapping()
		{
			return _iProgramTypeMappingRepository.GetAllProgramTypeMapping();
		}

		public ProgramTypeMapping InsertProgramTypeMapping(ProgramTypeMapping entity)
		{
			 return _iProgramTypeMappingRepository.InsertProgramTypeMapping(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programtypemappingid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programtypemappingid))
            {
				ProgramTypeMapping programtypemapping = _iProgramTypeMappingRepository.GetProgramTypeMapping(programtypemappingid);
                if(programtypemapping!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(programtypemapping);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ProgramTypeMapping> programtypemappinglist = _iProgramTypeMappingRepository.GetAllProgramTypeMapping();
            if (programtypemappinglist != null && programtypemappinglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programtypemappinglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ProgramTypeMapping programtypemapping = new ProgramTypeMapping();
                PostOutput output = new PostOutput();
                programtypemapping.CopyFrom(Input);
                programtypemapping = _iProgramTypeMappingRepository.InsertProgramTypeMapping(programtypemapping);
                output.CopyFrom(programtypemapping);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramTypeMapping programtypemappinginput = new ProgramTypeMapping();
                ProgramTypeMapping programtypemappingoutput = new ProgramTypeMapping();
                PutOutput output = new PutOutput();
                programtypemappinginput.CopyFrom(Input);
                ProgramTypeMapping programtypemapping = _iProgramTypeMappingRepository.GetProgramTypeMapping(programtypemappinginput.ProgramTypeMappingId);
                if (programtypemapping!=null)
                {
                    programtypemappingoutput = _iProgramTypeMappingRepository.UpdateProgramTypeMapping(programtypemappinginput);
                    if(programtypemappingoutput!=null)
                    {
                        output.CopyFrom(programtypemappingoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programtypemappingid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programtypemappingid))
            {
				 bool IsDeleted = _iProgramTypeMappingRepository.DeleteProgramTypeMapping(programtypemappingid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
