﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ProgramType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ProgramTypeService : IProgramTypeService 
	{
		private IProgramTypeRepository _iProgramTypeRepository;
        
		public ProgramTypeService(IProgramTypeRepository iProgramTypeRepository)
		{
			this._iProgramTypeRepository = iProgramTypeRepository;
		}
        
        public Dictionary<string, string> GetProgramTypeBasicSearchColumns()
        {
            
            return this._iProgramTypeRepository.GetProgramTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetProgramTypeAdvanceSearchColumns()
        {
            
            return this._iProgramTypeRepository.GetProgramTypeAdvanceSearchColumns();
           
        }
        

		public ProgramType GetProgramType(System.Int32 ProgramTypeId)
		{
			return _iProgramTypeRepository.GetProgramType(ProgramTypeId);
		}

		public ProgramType UpdateProgramType(ProgramType entity)
		{
			return _iProgramTypeRepository.UpdateProgramType(entity);
		}

		public bool DeleteProgramType(System.Int32 ProgramTypeId)
		{
			return _iProgramTypeRepository.DeleteProgramType(ProgramTypeId);
		}

		public List<ProgramType> GetAllProgramType()
		{
			return _iProgramTypeRepository.GetAllProgramType();
		}

		public ProgramType InsertProgramType(ProgramType entity)
		{
			 return _iProgramTypeRepository.InsertProgramType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 programtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programtypeid))
            {
				ProgramType programtype = _iProgramTypeRepository.GetProgramType(programtypeid);
                if(programtype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(programtype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ProgramType> programtypelist = _iProgramTypeRepository.GetAllProgramType();
            if (programtypelist != null && programtypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(programtypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ProgramType programtype = new ProgramType();
                PostOutput output = new PostOutput();
                programtype.CopyFrom(Input);
                programtype = _iProgramTypeRepository.InsertProgramType(programtype);
                output.CopyFrom(programtype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ProgramType programtypeinput = new ProgramType();
                ProgramType programtypeoutput = new ProgramType();
                PutOutput output = new PutOutput();
                programtypeinput.CopyFrom(Input);
                ProgramType programtype = _iProgramTypeRepository.GetProgramType(programtypeinput.ProgramTypeId);
                if (programtype!=null)
                {
                    programtypeoutput = _iProgramTypeRepository.UpdateProgramType(programtypeinput);
                    if(programtypeoutput!=null)
                    {
                        output.CopyFrom(programtypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 programtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out programtypeid))
            {
				 bool IsDeleted = _iProgramTypeRepository.DeleteProgramType(programtypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
