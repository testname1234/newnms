﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RadioStation;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class RadioStationService : IRadioStationService 
	{
		private IRadioStationRepository _iRadioStationRepository;
        
		public RadioStationService(IRadioStationRepository iRadioStationRepository)
		{
			this._iRadioStationRepository = iRadioStationRepository;
		}
        
        public Dictionary<string, string> GetRadioStationBasicSearchColumns()
        {
            
            return this._iRadioStationRepository.GetRadioStationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRadioStationAdvanceSearchColumns()
        {
            
            return this._iRadioStationRepository.GetRadioStationAdvanceSearchColumns();
           
        }
        

		public RadioStation GetRadioStation(System.Int32 RadioStationId)
		{
			return _iRadioStationRepository.GetRadioStation(RadioStationId);
		}

		public RadioStation UpdateRadioStation(RadioStation entity)
		{
			return _iRadioStationRepository.UpdateRadioStation(entity);
		}

		public bool DeleteRadioStation(System.Int32 RadioStationId)
		{
			return _iRadioStationRepository.DeleteRadioStation(RadioStationId);
		}

		public List<RadioStation> GetAllRadioStation()
		{
			return _iRadioStationRepository.GetAllRadioStation();
		}

		public RadioStation InsertRadioStation(RadioStation entity)
		{
			 return _iRadioStationRepository.InsertRadioStation(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 radiostationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out radiostationid))
            {
				RadioStation radiostation = _iRadioStationRepository.GetRadioStation(radiostationid);
                if(radiostation!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(radiostation);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<RadioStation> radiostationlist = _iRadioStationRepository.GetAllRadioStation();
            if (radiostationlist != null && radiostationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(radiostationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                RadioStation radiostation = new RadioStation();
                PostOutput output = new PostOutput();
                radiostation.CopyFrom(Input);
                radiostation = _iRadioStationRepository.InsertRadioStation(radiostation);
                output.CopyFrom(radiostation);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                RadioStation radiostationinput = new RadioStation();
                RadioStation radiostationoutput = new RadioStation();
                PutOutput output = new PutOutput();
                radiostationinput.CopyFrom(Input);
                RadioStation radiostation = _iRadioStationRepository.GetRadioStation(radiostationinput.RadioStationId);
                if (radiostation!=null)
                {
                    radiostationoutput = _iRadioStationRepository.UpdateRadioStation(radiostationinput);
                    if(radiostationoutput!=null)
                    {
                        output.CopyFrom(radiostationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 radiostationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out radiostationid))
            {
				 bool IsDeleted = _iRadioStationRepository.DeleteRadioStation(radiostationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
