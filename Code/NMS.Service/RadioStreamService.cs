﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RadioStream;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class RadioStreamService : IRadioStreamService 
	{
		private IRadioStreamRepository _iRadioStreamRepository;
        
		public RadioStreamService(IRadioStreamRepository iRadioStreamRepository)
		{
			this._iRadioStreamRepository = iRadioStreamRepository;
		}
        
        public Dictionary<string, string> GetRadioStreamBasicSearchColumns()
        {
            
            return this._iRadioStreamRepository.GetRadioStreamBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRadioStreamAdvanceSearchColumns()
        {
            
            return this._iRadioStreamRepository.GetRadioStreamAdvanceSearchColumns();
           
        }
        

		public virtual List<RadioStream> GetRadioStreamByRadioStationId(System.Int32 RadioStationId)
		{
			return _iRadioStreamRepository.GetRadioStreamByRadioStationId(RadioStationId);
		}

        public virtual List<RadioStream> GetRadioStreamBetweenDates(System.DateTime fromDate, System.DateTime toDate)
        {
            return _iRadioStreamRepository.GetRadioStreamBetweenDates(fromDate, toDate);
        }

		public RadioStream GetRadioStream(System.Int32 RadioStreamId)
		{
			return _iRadioStreamRepository.GetRadioStream(RadioStreamId);
		}

		public RadioStream UpdateRadioStream(RadioStream entity)
		{
			return _iRadioStreamRepository.UpdateRadioStream(entity);
		}

		public bool DeleteRadioStream(System.Int32 RadioStreamId)
		{
			return _iRadioStreamRepository.DeleteRadioStream(RadioStreamId);
		}

		public List<RadioStream> GetAllRadioStream()
		{
			return _iRadioStreamRepository.GetAllRadioStream();
		}

		public RadioStream InsertRadioStream(RadioStream entity)
		{
			 return _iRadioStreamRepository.InsertRadioStream(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 radiostreamid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out radiostreamid))
            {
				RadioStream radiostream = _iRadioStreamRepository.GetRadioStream(radiostreamid);
                if(radiostream!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(radiostream);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<RadioStream> radiostreamlist = _iRadioStreamRepository.GetAllRadioStream();
            if (radiostreamlist != null && radiostreamlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(radiostreamlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                RadioStream radiostream = new RadioStream();
                PostOutput output = new PostOutput();
                radiostream.CopyFrom(Input);
                radiostream = _iRadioStreamRepository.InsertRadioStream(radiostream);
                output.CopyFrom(radiostream);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                RadioStream radiostreaminput = new RadioStream();
                RadioStream radiostreamoutput = new RadioStream();
                PutOutput output = new PutOutput();
                radiostreaminput.CopyFrom(Input);
                RadioStream radiostream = _iRadioStreamRepository.GetRadioStream(radiostreaminput.RadioStreamId);
                if (radiostream!=null)
                {
                    radiostreamoutput = _iRadioStreamRepository.UpdateRadioStream(radiostreaminput);
                    if(radiostreamoutput!=null)
                    {
                        output.CopyFrom(radiostreamoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 radiostreamid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out radiostreamid))
            {
				 bool IsDeleted = _iRadioStreamRepository.DeleteRadioStream(radiostreamid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public void MarkIsProcessed(int id)
        {
            _iRadioStreamRepository.MarkIsProcessed(id);
        }
	}
	
	
}
