﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RecordingClips;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class RecordingClipsService : IRecordingClipsService 
	{
		private IRecordingClipsRepository _iRecordingClipsRepository;
        
		public RecordingClipsService(IRecordingClipsRepository iRecordingClipsRepository)
		{
			this._iRecordingClipsRepository = iRecordingClipsRepository;
		}
        
        public Dictionary<string, string> GetRecordingClipsBasicSearchColumns()
        {
            
            return this._iRecordingClipsRepository.GetRecordingClipsBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRecordingClipsAdvanceSearchColumns()
        {
            
            return this._iRecordingClipsRepository.GetRecordingClipsAdvanceSearchColumns();
           
        }
        

		public virtual List<RecordingClips> GetRecordingClipsByCelebrityId(System.Int32? CelebrityId)
		{
			return _iRecordingClipsRepository.GetRecordingClipsByCelebrityId(CelebrityId);
		}

		public virtual List<RecordingClips> GetRecordingClipsByProgramRecordingId(System.Int32? ProgramRecordingId)
		{
			return _iRecordingClipsRepository.GetRecordingClipsByProgramRecordingId(ProgramRecordingId);
		}

        public virtual List<RecordingClips> GetRecordingClipsByProgramRecordingIdList(List<System.Int32> ProgramRecordingIds)
        {
            List<RecordingClips> ret = new List<RecordingClips>();
            foreach (var id in ProgramRecordingIds)
            {
                var data = _iRecordingClipsRepository.GetRecordingClipsByProgramRecordingId(id);
                if (data != null)
                {
                    ret.AddRange(data);
                }
            }
            return ret;
        }

        public RecordingClips GetRecordingClips(System.Int32 RecordingClipId)
		{
			return _iRecordingClipsRepository.GetRecordingClips(RecordingClipId);
		}

		public RecordingClips UpdateRecordingClips(RecordingClips entity)
		{
            if(entity.Status.HasValue && (entity.Status.Value == (int)Core.Enums.ProgramRecordingStatus.Scripted || entity.Status.Value == (int)Core.Enums.ProgramRecordingStatus.Clipped) && !entity.ParentId.HasValue)
            {
                // case where rejected clip is fixed and re updated by client , all others same clips will be fixed according to flow of status
                // this was handeled in client end on first time creation of clip
                if (entity.Status.Value == (int)Core.Enums.ProgramRecordingStatus.Scripted)
                {
                    RecordingClips otherLangs = new RecordingClips();
                    DataCopier.CopyFrom(otherLangs, entity);
                    otherLangs.Status = (int)Core.Enums.ProgramRecordingStatus.Clipped;
                    otherLangs.Script = string.Empty;
                    _iRecordingClipsRepository.UpdateRecordingClipsByParentId(otherLangs, entity.RecordingClipId);
                }
                else
                {
                    _iRecordingClipsRepository.UpdateRecordingClipsByParentId(entity, entity.RecordingClipId);
                }
                return _iRecordingClipsRepository.UpdateRecordingClips(entity);
            }
            else
            {
                return _iRecordingClipsRepository.UpdateRecordingClips(entity);
            }
		}

        public RecordingClips UpdateRecordingClipsRejectClip(RecordingClips entity)
        {
            List<RecordingClips> otherLangs = new List<RecordingClips>();
            if (entity.ParentId.HasValue)
            {
                otherLangs = _iRecordingClipsRepository.GetAllRecordingsByParentId(entity.ParentId.Value);
                otherLangs.Add(_iRecordingClipsRepository.GetRecordingClips(entity.ParentId.Value));
            }
            else
            {
                otherLangs = _iRecordingClipsRepository.GetAllRecordingsByParentId(entity.RecordingClipId);
                otherLangs.Add(_iRecordingClipsRepository.GetRecordingClips(entity.RecordingClipId));
            }
            //List <RecordingClips> otherLangs = _iRecordingClipsRepository.GetAllSameRecordings(entity);
            RecordingClips ret = new RecordingClips();

            foreach (var rec in otherLangs)
            {
                rec.Status = entity.Status;
                rec.LastUpdateDate = entity.LastUpdateDate;
                _iRecordingClipsRepository.UpdateRecordingClips(rec);
            }
            ret = otherLangs.Where(x => x.RecordingClipId == entity.RecordingClipId).FirstOrDefault();
            return ret;
        }

        public bool DeleteRecordingClips(System.Int32 RecordingClipId)
		{
			return _iRecordingClipsRepository.DeleteRecordingClips(RecordingClipId);
		}

		public List<RecordingClips> GetAllRecordingClips()
		{
			return _iRecordingClipsRepository.GetAllRecordingClips();
		}

		public RecordingClips InsertRecordingClips(RecordingClips entity)
		{
            return _iRecordingClipsRepository.InsertRecordingClips(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 recordingclipid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out recordingclipid))
            {
				RecordingClips recordingclips = _iRecordingClipsRepository.GetRecordingClips(recordingclipid);
                if(recordingclips!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(recordingclips);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<RecordingClips> recordingclipslist = _iRecordingClipsRepository.GetAllRecordingClips();
            if (recordingclipslist != null && recordingclipslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(recordingclipslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                RecordingClips recordingclips = new RecordingClips();
                PostOutput output = new PostOutput();
                recordingclips.CopyFrom(Input);
                recordingclips = _iRecordingClipsRepository.InsertRecordingClips(recordingclips);
                output.CopyFrom(recordingclips);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                RecordingClips recordingclipsinput = new RecordingClips();
                RecordingClips recordingclipsoutput = new RecordingClips();
                PutOutput output = new PutOutput();
                recordingclipsinput.CopyFrom(Input);
                RecordingClips recordingclips = _iRecordingClipsRepository.GetRecordingClips(recordingclipsinput.RecordingClipId);
                if (recordingclips!=null)
                {
                    recordingclipsoutput = _iRecordingClipsRepository.UpdateRecordingClips(recordingclipsinput);
                    if(recordingclipsoutput!=null)
                    {
                        output.CopyFrom(recordingclipsoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 recordingclipid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out recordingclipid))
            {
				 bool IsDeleted = _iRecordingClipsRepository.DeleteRecordingClips(recordingclipid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
