﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RecurringType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class RecurringTypeService : IRecurringTypeService 
	{
		private IRecurringTypeRepository _iRecurringTypeRepository;
        
		public RecurringTypeService(IRecurringTypeRepository iRecurringTypeRepository)
		{
			this._iRecurringTypeRepository = iRecurringTypeRepository;
		}
        
        public Dictionary<string, string> GetRecurringTypeBasicSearchColumns()
        {
            
            return this._iRecurringTypeRepository.GetRecurringTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRecurringTypeAdvanceSearchColumns()
        {
            
            return this._iRecurringTypeRepository.GetRecurringTypeAdvanceSearchColumns();
           
        }
        

		public RecurringType GetRecurringType(System.Int32 RecurringTypeId)
		{
			return _iRecurringTypeRepository.GetRecurringType(RecurringTypeId);
		}

		public RecurringType UpdateRecurringType(RecurringType entity)
		{
			return _iRecurringTypeRepository.UpdateRecurringType(entity);
		}

		public bool DeleteRecurringType(System.Int32 RecurringTypeId)
		{
			return _iRecurringTypeRepository.DeleteRecurringType(RecurringTypeId);
		}

		public List<RecurringType> GetAllRecurringType()
		{
			return _iRecurringTypeRepository.GetAllRecurringType();
		}

		public RecurringType InsertRecurringType(RecurringType entity)
		{
			 return _iRecurringTypeRepository.InsertRecurringType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 recurringtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out recurringtypeid))
            {
				RecurringType recurringtype = _iRecurringTypeRepository.GetRecurringType(recurringtypeid);
                if(recurringtype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(recurringtype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<RecurringType> recurringtypelist = _iRecurringTypeRepository.GetAllRecurringType();
            if (recurringtypelist != null && recurringtypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(recurringtypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                RecurringType recurringtype = new RecurringType();
                PostOutput output = new PostOutput();
                recurringtype.CopyFrom(Input);
                recurringtype = _iRecurringTypeRepository.InsertRecurringType(recurringtype);
                output.CopyFrom(recurringtype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                RecurringType recurringtypeinput = new RecurringType();
                RecurringType recurringtypeoutput = new RecurringType();
                PutOutput output = new PutOutput();
                recurringtypeinput.CopyFrom(Input);
                RecurringType recurringtype = _iRecurringTypeRepository.GetRecurringType(recurringtypeinput.RecurringTypeId);
                if (recurringtype!=null)
                {
                    recurringtypeoutput = _iRecurringTypeRepository.UpdateRecurringType(recurringtypeinput);
                    if(recurringtypeoutput!=null)
                    {
                        output.CopyFrom(recurringtypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 recurringtypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out recurringtypeid))
            {
				 bool IsDeleted = _iRecurringTypeRepository.DeleteRecurringType(recurringtypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
