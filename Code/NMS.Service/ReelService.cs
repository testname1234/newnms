﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Reel;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ReelService : IReelService 
	{
		private IReelRepository _iReelRepository;
        
		public ReelService(IReelRepository iReelRepository)
		{
			this._iReelRepository = iReelRepository;
		}
        
        public Dictionary<string, string> GetReelBasicSearchColumns()
        {
            
            return this._iReelRepository.GetReelBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetReelAdvanceSearchColumns()
        {
            
            return this._iReelRepository.GetReelAdvanceSearchColumns();
           
        }
        

		public virtual List<Reel> GetReelByStatusId(System.Int32 StatusId)
		{
			return _iReelRepository.GetReelByStatusId(StatusId);
		}

		public Reel GetReel(System.Int32 ReelId)
		{
			return _iReelRepository.GetReel(ReelId);
		}

		public Reel UpdateReel(Reel entity)
		{
			return _iReelRepository.UpdateReel(entity);
		}

		public bool DeleteReel(System.Int32 ReelId)
		{
			return _iReelRepository.DeleteReel(ReelId);
		}

		public List<Reel> GetAllReel()
		{
			return _iReelRepository.GetAllReel();
		}

		public Reel InsertReel(Reel entity)
		{
			 return _iReelRepository.InsertReel(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 reelid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out reelid))
            {
				Reel reel = _iReelRepository.GetReel(reelid);
                if(reel!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(reel);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Reel> reellist = _iReelRepository.GetAllReel();
            if (reellist != null && reellist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(reellist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Reel reel = new Reel();
                PostOutput output = new PostOutput();
                reel.CopyFrom(Input);
                reel = _iReelRepository.InsertReel(reel);
                output.CopyFrom(reel);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Reel reelinput = new Reel();
                Reel reeloutput = new Reel();
                PutOutput output = new PutOutput();
                reelinput.CopyFrom(Input);
                Reel reel = _iReelRepository.GetReel(reelinput.ReelId);
                if (reel!=null)
                {
                    reeloutput = _iReelRepository.UpdateReel(reelinput);
                    if(reeloutput!=null)
                    {
                        output.CopyFrom(reeloutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 reelid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out reelid))
            {
				 bool IsDeleted = _iReelRepository.DeleteReel(reelid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
