﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ReelStatus;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ReelStatusService : IReelStatusService 
	{
		private IReelStatusRepository _iReelStatusRepository;
        
		public ReelStatusService(IReelStatusRepository iReelStatusRepository)
		{
			this._iReelStatusRepository = iReelStatusRepository;
		}
        
        public Dictionary<string, string> GetReelStatusBasicSearchColumns()
        {
            
            return this._iReelStatusRepository.GetReelStatusBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetReelStatusAdvanceSearchColumns()
        {
            
            return this._iReelStatusRepository.GetReelStatusAdvanceSearchColumns();
           
        }
        

		public ReelStatus GetReelStatus(System.Int32 ReelStatusId)
		{
			return _iReelStatusRepository.GetReelStatus(ReelStatusId);
		}

		public ReelStatus UpdateReelStatus(ReelStatus entity)
		{
			return _iReelStatusRepository.UpdateReelStatus(entity);
		}

		public bool DeleteReelStatus(System.Int32 ReelStatusId)
		{
			return _iReelStatusRepository.DeleteReelStatus(ReelStatusId);
		}

		public List<ReelStatus> GetAllReelStatus()
		{
			return _iReelStatusRepository.GetAllReelStatus();
		}

		public ReelStatus InsertReelStatus(ReelStatus entity)
		{
			 return _iReelStatusRepository.InsertReelStatus(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 reelstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out reelstatusid))
            {
				ReelStatus reelstatus = _iReelStatusRepository.GetReelStatus(reelstatusid);
                if(reelstatus!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(reelstatus);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ReelStatus> reelstatuslist = _iReelStatusRepository.GetAllReelStatus();
            if (reelstatuslist != null && reelstatuslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(reelstatuslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ReelStatus reelstatus = new ReelStatus();
                PostOutput output = new PostOutput();
                reelstatus.CopyFrom(Input);
                reelstatus = _iReelStatusRepository.InsertReelStatus(reelstatus);
                output.CopyFrom(reelstatus);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ReelStatus reelstatusinput = new ReelStatus();
                ReelStatus reelstatusoutput = new ReelStatus();
                PutOutput output = new PutOutput();
                reelstatusinput.CopyFrom(Input);
                ReelStatus reelstatus = _iReelStatusRepository.GetReelStatus(reelstatusinput.ReelStatusId);
                if (reelstatus!=null)
                {
                    reelstatusoutput = _iReelStatusRepository.UpdateReelStatus(reelstatusinput);
                    if(reelstatusoutput!=null)
                    {
                        output.CopyFrom(reelstatusoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 reelstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out reelstatusid))
            {
				 bool IsDeleted = _iReelStatusRepository.DeleteReelStatus(reelstatusid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
