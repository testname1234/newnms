﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ResourceEditclipinfo;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ResourceEditclipinfoService : IResourceEditclipinfoService 
	{
		private IResourceEditclipinfoRepository _iResourceEditclipinfoRepository;
        
		public ResourceEditclipinfoService(IResourceEditclipinfoRepository iResourceEditclipinfoRepository)
		{
			this._iResourceEditclipinfoRepository = iResourceEditclipinfoRepository;
		}
        
        public Dictionary<string, string> GetResourceEditclipinfoBasicSearchColumns()
        {
            
            return this._iResourceEditclipinfoRepository.GetResourceEditclipinfoBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetResourceEditclipinfoAdvanceSearchColumns()
        {
            
            return this._iResourceEditclipinfoRepository.GetResourceEditclipinfoAdvanceSearchColumns();
           
        }
        

		public virtual List<ResourceEditclipinfo> GetResourceEditclipinfoByNewsResourceEditId(System.Int32? NewsResourceEditId)
		{
			return _iResourceEditclipinfoRepository.GetResourceEditclipinfoByNewsResourceEditId(NewsResourceEditId);
		}

		public ResourceEditclipinfo GetResourceEditclipinfo(System.Int32 ResourceEditclipinfoId)
		{
			return _iResourceEditclipinfoRepository.GetResourceEditclipinfo(ResourceEditclipinfoId);
		}

		public ResourceEditclipinfo UpdateResourceEditclipinfo(ResourceEditclipinfo entity)
		{
			return _iResourceEditclipinfoRepository.UpdateResourceEditclipinfo(entity);
		}

		public bool DeleteResourceEditclipinfo(System.Int32 ResourceEditclipinfoId)
		{
			return _iResourceEditclipinfoRepository.DeleteResourceEditclipinfo(ResourceEditclipinfoId);
		}

        public bool DeleteByNewsResourceEditId(System.Int32 NewsResourceEditId)
		{
			return _iResourceEditclipinfoRepository.DeleteByNewsResourceEditId(NewsResourceEditId);
		}
        

		public List<ResourceEditclipinfo> GetAllResourceEditclipinfo()
		{
			return _iResourceEditclipinfoRepository.GetAllResourceEditclipinfo();
		}

		public ResourceEditclipinfo InsertResourceEditclipinfo(ResourceEditclipinfo entity)
		{
			 return _iResourceEditclipinfoRepository.InsertResourceEditclipinfo(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourceeditclipinfoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourceeditclipinfoid))
            {
				ResourceEditclipinfo resourceeditclipinfo = _iResourceEditclipinfoRepository.GetResourceEditclipinfo(resourceeditclipinfoid);
                if(resourceeditclipinfo!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resourceeditclipinfo);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ResourceEditclipinfo> resourceeditclipinfolist = _iResourceEditclipinfoRepository.GetAllResourceEditclipinfo();
            if (resourceeditclipinfolist != null && resourceeditclipinfolist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourceeditclipinfolist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ResourceEditclipinfo resourceeditclipinfo = new ResourceEditclipinfo();
                PostOutput output = new PostOutput();
                resourceeditclipinfo.CopyFrom(Input);
                resourceeditclipinfo = _iResourceEditclipinfoRepository.InsertResourceEditclipinfo(resourceeditclipinfo);
                output.CopyFrom(resourceeditclipinfo);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ResourceEditclipinfo resourceeditclipinfoinput = new ResourceEditclipinfo();
                ResourceEditclipinfo resourceeditclipinfooutput = new ResourceEditclipinfo();
                PutOutput output = new PutOutput();
                resourceeditclipinfoinput.CopyFrom(Input);
                ResourceEditclipinfo resourceeditclipinfo = _iResourceEditclipinfoRepository.GetResourceEditclipinfo(resourceeditclipinfoinput.ResourceEditclipinfoId);
                if (resourceeditclipinfo!=null)
                {
                    resourceeditclipinfooutput = _iResourceEditclipinfoRepository.UpdateResourceEditclipinfo(resourceeditclipinfoinput);
                    if(resourceeditclipinfooutput!=null)
                    {
                        output.CopyFrom(resourceeditclipinfooutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourceeditclipinfoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out resourceeditclipinfoid))
            {
				 bool IsDeleted = _iResourceEditclipinfoRepository.DeleteResourceEditclipinfo(resourceeditclipinfoid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
