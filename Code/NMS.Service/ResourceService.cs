﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Resource;
using Validation;
using System.Linq;
using NMS.Core;
using System.Net;
using NMS.Core.Helper;
using System.IO;
using Newtonsoft.Json;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Models;
using System.Drawing;
using System.Collections.Specialized;
using NMS.Core.Entities.Mongo;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using MS.MediaIntegration.API;
using NMS.Core.Enums;
using System.Globalization;


namespace NMS.Service
{

    public class ResourceService : IResourceService
    {
        private IResourceRepository _iResourceRepository;
        private IMResourceRepository _iMResourceRepository;

        public ResourceService(IResourceRepository iResourceRepository, IMResourceRepository iMResourceRepository)
        {
            this._iResourceRepository = iResourceRepository;
            this._iMResourceRepository = iMResourceRepository;
        }

        public Dictionary<string, string> GetResourceBasicSearchColumns()
        {

            return this._iResourceRepository.GetResourceBasicSearchColumns();

        }

        public List<SearchColumn> GetResourceAdvanceSearchColumns()
        {

            return this._iResourceRepository.GetResourceAdvanceSearchColumns();

        }


        public virtual List<Resource> GetResourceByResourceTypeId(System.Int32 ResourceTypeId)
        {
            return _iResourceRepository.GetResourceByResourceTypeId(ResourceTypeId);
        }

        public Resource GetResource(System.Int32 ResourceId)
        {
            return _iResourceRepository.GetResource(ResourceId);
        }

        public Resource GetResourceByGuid(string ResourceGuid)
        {
            return _iResourceRepository.GetResourceBuGuid(ResourceGuid);
        }



        public Resource InsertResourceIfNotExists(Resource resource)
        {
            Resource res = _iResourceRepository.GetResourceBuGuid(resource.Guid);
            if (res == null)
            {
                res = _iResourceRepository.InsertResource(resource);
            }
            else
            {
                res = _iResourceRepository.UpdateResource(resource);
            }
            return res;
        }

        public void InsertIfNotExistsNoReturn(Resource resource)
        {
            if (_iResourceRepository.GetResourceBuGuid(resource.Guid) == null)
            {
                _iResourceRepository.InsertResourceNoReturn(resource);
            }
            else
            {
                _iResourceRepository.UpdateResourceNoReturn(resource);
            }
        }

        public Resource UpdateResource(Resource entity)
        {
            return _iResourceRepository.UpdateResource(entity);
        }

        public void UpdateResourceDuration(MResource entity)
        {
            _iMResourceRepository.UpdateResourceDuration(entity);
        }

        public bool DeleteResource(System.Int32 ResourceId)
        {
            return _iResourceRepository.DeleteResource(ResourceId);
        }

        public bool DeleteByResourceGuid(string ResourceId)
        {
            return _iMResourceRepository.DeleteByResourceGuid(ResourceId);
        }

        public MResource GetByGuid(string Guid)
        {
            return _iMResourceRepository.GetByGuid(Guid);
        }

        public List<Resource> GetAllResource()
        {
            return _iResourceRepository.GetAllResource();
        }

        public List<Resource> GetByUpdatedDate(DateTime UpdatedDate)
        {
            return _iResourceRepository.GetByUpdatedDate(UpdatedDate);
        }

        public DateTime GetLastResourceDate()
        {
            return _iResourceRepository.GetLastResourceDate();
        }

        public Resource InsertResource(Resource entity)
        {
            return _iResourceRepository.InsertResource(entity);
        }

        public bool InsertResource(MResource entity)
        {
            return _iMResourceRepository.InsertResource(entity);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 resourceid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out resourceid))
            {
                Resource resource = _iResourceRepository.GetResource(resourceid);
                if (resource != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(resource);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Resource> resourcelist = _iResourceRepository.GetAllResource();
            if (resourcelist != null && resourcelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(resourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Resource resource = new Resource();
                PostOutput output = new PostOutput();
                resource.CopyFrom(Input);
                resource = _iResourceRepository.InsertResource(resource);
                output.CopyFrom(resource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Resource resourceinput = new Resource();
                Resource resourceoutput = new Resource();
                PutOutput output = new PutOutput();
                resourceinput.CopyFrom(Input);
                Resource resource = _iResourceRepository.GetResource(resourceinput.ResourceId);
                if (resource != null)
                {
                    resourceoutput = _iResourceRepository.UpdateResource(resourceinput);
                    if (resourceoutput != null)
                    {
                        output.CopyFrom(resourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 resourceid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out resourceid))
            {
                bool IsDeleted = _iResourceRepository.DeleteResource(resourceid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<PostOutput> GetResourceGuidsFromMediaServer(List<PostInput> input, string rootPath)
        {
            List<NMS.Core.DataTransfer.Resource.PostOutput> lstOutput = new List<Core.DataTransfer.Resource.PostOutput>();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            foreach (var res in input)
            {
                res.Source = rootPath + "{guid}" + Path.GetExtension(res.FileName);
                MS.Core.DataTransfer.Resource.PostInput resInput = new MS.Core.DataTransfer.Resource.PostInput();
                resInput.CopyFrom(res);
                NMS.Core.DataTransfer.Resource.PostOutput NMSRes = new PostOutput();
                NMSRes.CopyFrom(MediaPostResource(resInput, AppSettings.ReporterFolder.ToString()));
                lstOutput.Add(NMSRes);

                //HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", res, null);
                //using (TextReader treader = new StreamReader(response.GetResponseStream()))
                //{
                //    string str = treader.ReadToEnd();
                //    lstOutput.Add(JsonConvert.DeserializeObject<NMS.Core.DataTransfer.Resource.PostOutput>(str));
                //}
            }
            return lstOutput;
        }

        public List<PostOutput> GetResourceGuidsFromMediaServer(List<PostInput> input, string rootPath, int? bucketId, string apiKey, string sourceTypeId, string sourceTypeName, string sourceId, string FolderSource)
        {
            List<NMS.Core.DataTransfer.Resource.PostOutput> lstOutput = new List<Core.DataTransfer.Resource.PostOutput>();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            foreach (var res in input)
            {
                res.Source = rootPath + "{guid}" + Path.GetExtension(res.FileName);
                MS.Core.DataTransfer.Resource.PostInput resInput = new MS.Core.DataTransfer.Resource.PostInput();
                resInput.BucketId = Convert.ToInt32(bucketId);
                resInput.ApiKey = Convert.ToString(apiKey);
                resInput.CopyFrom(res);
                resInput.SourceTypeId = sourceTypeId;
                resInput.SourceName = sourceTypeName;
                resInput.SourceId = sourceId;
                NMS.Core.DataTransfer.Resource.PostOutput NMSRes = new PostOutput();
                NMSRes.CopyFrom(MediaPostResource(resInput, FolderSource));
                lstOutput.Add(NMSRes);

                //HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", res, null);
                //using (TextReader treader = new StreamReader(response.GetResponseStream()))
                //{
                //    string str = treader.ReadToEnd();
                //    lstOutput.Add(JsonConvert.DeserializeObject<NMS.Core.DataTransfer.Resource.PostOutput>(str));
                //}
            }
            return lstOutput;
        }

        public PostOutput GetResourceFromMediaServerByGuid(string guid)
        {
            NMS.Core.DataTransfer.Resource.PostOutput lstOutput = new Core.DataTransfer.Resource.PostOutput();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

            HttpWebResponse response = requestHelper.GetRequest(NMS.Core.AppSettings.MediaServerUrl + "/GetResourceInfoByGuid?guid=" + guid.ToString(), null);
            using (TextReader treader = new StreamReader(response.GetResponseStream()))
            {
                string str = treader.ReadToEnd();
                DataTransfer<MS.Core.DataTransfer.Resource.GetOutput> MediaOutput = new DataTransfer<MS.Core.DataTransfer.Resource.GetOutput>();
                MediaOutput = JsonConvert.DeserializeObject<DataTransfer<MS.Core.DataTransfer.Resource.GetOutput>>(str);
                if (MediaOutput.IsSuccess)
                    lstOutput.CopyFrom(MediaOutput.Data);
            }
            return lstOutput;
        }

        public void UpdateResourceStatus(Guid guid, int resourceStatusId)
        {
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = requestHelper.GetRequest(NMS.Core.AppSettings.MediaServerUrl + "/UpdateResourceStatus?guid=" + guid.ToString() + "&statusId=" + resourceStatusId, null);
        }


        public MResource GetMResource(string id)
        {
            return _iMResourceRepository.GetById(id);
        }

        public MResource GetMResourceByGuid(string Guid)
        {
            return _iMResourceRepository.GetByGuid(Guid);
        }


        public bool DeleteFromMediaServer(Guid id)
        {
            List<NMS.Core.DataTransfer.Resource.PostOutput> lstOutput = new List<Core.DataTransfer.Resource.PostOutput>();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = requestHelper.GetRequest(NMS.Core.AppSettings.MediaServerUrl + "/DeleteResource/" + id.ToString(), null);
            if (response.StatusCode == HttpStatusCode.OK)
                return true;
            else return false;
        }


        public string GenerateSnapshotOnMediaServer(Guid guid)
        {
            List<NMS.Core.DataTransfer.Resource.PostOutput> lstOutput = new List<Core.DataTransfer.Resource.PostOutput>();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = requestHelper.GetRequest(NMS.Core.AppSettings.MediaServerUrl + "/GenerateSnapshot/" + guid.ToString(), null);
            string StatusVar = "";
            if (response.StatusCode == HttpStatusCode.OK)
                StatusVar = AppSettings.MediaServerUrl + "/getthumb/" + guid.ToString();
            if (response != null) response.Close();
            return StatusVar;
        }

        public List<MResource> GetResourceByMeta(string resourceMeta)
        {
            List<NMS.Core.DataTransfer.Resource.PostOutput> lstOutput = new List<Core.DataTransfer.Resource.PostOutput>();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = requestHelper.GetRequest(NMS.Core.AppSettings.MediaServerUrl + "/GetResourceByMeta?MetaString=" + resourceMeta, null);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (TextReader treader = new StreamReader(response.GetResponseStream()))
                {
                    string str = treader.ReadToEnd();
                    MS.Core.DataTransfer.Resource.ResourceOutput msResource = JsonConvert.DeserializeObject<MS.Core.DataTransfer.Resource.ResourceOutput>(str);
                    List<MResource> mListResources = new List<MResource>();
                    mListResources.CopyFrom(msResource.Resources);
                    return mListResources;
                }
            }
            else
                return null;
        }

        public string AddResourceMeta(MNews mongoNews)
        {
            List<NMS.Core.DataTransfer.Resource.PostOutput> lstOutput = new List<Core.DataTransfer.Resource.PostOutput>();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = null;

            #region add Resource Mata
            try
            {
                foreach (MResource mResource in mongoNews.Resources)
                {
                    if (mResource.IgnoreMeta == 0)
                    {
                        MS.Core.DataTransfer.Resource.PostInput resourceMetaInput = new MS.Core.DataTransfer.Resource.PostInput();
                        var Category = (mongoNews.Categories == null || mongoNews.Categories.Count == 0) ? null : mongoNews.Categories.Where(x => x.ParentId == null).FirstOrDefault();
                        var Location = (mongoNews.Locations == null || mongoNews.Locations.Count == 0) ? null : mongoNews.Locations.Where(x => x.ParentId == null).FirstOrDefault();
                        //string Location = mongoNews.Locations.Where(x => x.ParentId == null).FirstOrDefault().Location;
                        string caption = mongoNews.LanguageCode == "en" ? mongoNews.Title : mongoNews.TranslatedTitle;
                        resourceMetaInput.CopyFrom(mResource);
                        resourceMetaInput.Location = Location != null ? Location.Location : "";
                        resourceMetaInput.Category = Category != null ? Category.Category : "";
                        resourceMetaInput.Caption = caption;
                        resourceMetaInput.ResourceMeta = new List<KeyValuePair<string, string>>();
                        if (mongoNews.Tags != null && mongoNews.Tags.Count > 0)
                        {
                            mongoNews.Tags = mongoNews.Tags.OrderByDescending(x => x.Rank).Take(5).ToList();
                            foreach (MTag mTag in mongoNews.Tags)
                            {
                                resourceMetaInput.ResourceMeta.Add(new KeyValuePair<string, string>("Tag", mTag.Tag));
                            }
                        }
                        response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/AddResourceMeta", resourceMetaInput, null);
                        string status = string.Empty;
                        if (response.StatusCode == HttpStatusCode.OK)
                            status = "Success";
                        if (response != null) response.Close();
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                if (response != null) response.Close();
                throw ex;
            }
            #endregion

        }

        public void updateResourceMeta(ListResources input)
        {
            List<NMS.Core.DataTransfer.Resource.PostOutput> lstOutput = new List<Core.DataTransfer.Resource.PostOutput>();
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = null;
            string[] guids = input.ListGuids.Split(',').ToArray();
            string[] listTags = input.MetaTags.Split(',').ToArray();
            #region add Resource Mata
            try
            {
                foreach (string resourceGuid in guids)
                {
                    MS.Core.DataTransfer.Resource.PostInput resourceMetaInput = new MS.Core.DataTransfer.Resource.PostInput();
                    resourceMetaInput.Guid = resourceGuid;
                    resourceMetaInput.ResourceMeta = new List<KeyValuePair<string, string>>();
                    resourceMetaInput.Caption = input.Slug;
                    foreach (string mTag in listTags)
                    {
                        resourceMetaInput.ResourceMeta.Add(new KeyValuePair<string, string>("Tag", mTag));
                    }
                    resourceMetaInput.ResourceMeta.Add(new KeyValuePair<string, string>("Caption", input.Slug));
                    response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/AddResourceMeta", resourceMetaInput, null);
                    string status = string.Empty;
                    if (response.StatusCode == HttpStatusCode.OK)
                        status = "Success";
                    if (response != null) response.Close();

                }
            }
            catch (Exception ex)
            {
                if (response != null) response.Close();
                throw ex;
            }
            #endregion
        }


        public string GenerateCustomSnapshotOnMediaServer(CustomSnapshotInput input)
        {
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/GenerateCustomSnapshot", input, null);
            string returnVar = "";
            if (response.StatusCode == HttpStatusCode.OK)
            {
                returnVar = AppSettings.MediaServerUrl + "/getthumb/" + input.Id;
            }
            if (response != null)
                response.Close();
            return returnVar;
        }

        public Guid UploadImageOnMS(Image img)
        {
            MemoryStream memStream = new MemoryStream();
            img.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            memStream.SetLength(memStream.Length);
            return UploadFile(memStream.GetBuffer(), Guid.NewGuid().ToString() + ".jpg", (int)MS.Core.Enums.ResourceTypes.Image, "image/jpeg");
        }

        public Guid UploadImageOnMS(string imageUrl)
        {
            return UploadFile(File.ReadAllBytes(imageUrl), imageUrl, (int)MS.Core.Enums.ResourceTypes.Image, "image/jpeg");
        }

        public Guid UploadFile(byte[] data, string file, int resourceType, string mimeType, bool isHd = true, int bucketId = 0, string apikey = null, string caption = null, double? duration = null)
        {
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
            resource.ResourceStatusId = ((int)MS.Core.Enums.ResourceStatuses.Completed).ToString();
            resource.Source = file;
            resource.ResourceTypeId = ((int)resourceType).ToString();
            resource.IsFromExternalSource = false;
            resource.Caption = caption;
            resource.Duration = duration;
            if (bucketId > 0 && !string.IsNullOrEmpty(apikey))
            {
                resource.BucketId = bucketId;
                resource.ApiKey = apikey;
            }
            //HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", resource, null);

            var res = MediaPostResource(resource, "reporter");

            string ext = System.IO.Path.GetExtension(resource.Source).Trim('.');
            string fileName = System.IO.Path.GetFileName(resource.Source);
            try
            {
                MSApi MediaApi = new MSApi();
                var httpStatus = MediaApi.PostMedia(res.Guid.ToString(), isHd, fileName.Trim('"'), data, "file", mimeType, new NameValueCollection());
            }
            catch { }
            Console.WriteLine("ImageUploaded: {0}", file);
            return Guid.Parse(res.Guid);
        }


        public Resource MediaPostResource(MS.Core.DataTransfer.Resource.PostInput resource, string FolderSource)
        {

            MSApi MediaApi = new MSApi();
            string dir1 = "/" + FolderSource;
            string dir2 = "/" + FolderSource + "/" + DateTime.UtcNow.Year.ToString();
            string dir3 = "/" + FolderSource + "/" + DateTime.UtcNow.Year.ToString() + "/" + DateTime.UtcNow.ToString("MMMM");
            List<string> lstDir = new List<string>();
            lstDir.Add(dir1);
            lstDir.Add(dir2);
            lstDir.Add(dir3);

            if (resource.BucketId == 0)
            {
                foreach (string d in lstDir)
                {
                    var Result = MediaApi.CreateSubBucket(((int)NMSBucket.NMSBucket), NMS.Core.AppSettings.MediaServerApiKey, d);
                    if (!Result.IsSuccess)
                        throw new Exception(Result.Errors[0]);
                }

                string source = Path.GetFileName(resource.Source);
                if (source.Length > 100)
                {
                    string ext = "";

                    if (FolderSource == "Google News")
                        ext = ".jpg";

                    try
                    {
                        string Ext = source.Substring(source.LastIndexOf("."), 3);
                        resource.FilePath = dir3 + "/" + Guid.NewGuid() + ext;

                    }
                    catch (Exception ex)
                    {
                        if (resource.ResourceTypeId == "2")
                            resource.FilePath = dir3 + "/" + Guid.NewGuid() + ".mp4";
                        else
                            resource.FilePath = dir3 + "/" + Guid.NewGuid() + ".jpg";
                    }


                }
                else
                {
                    if (FolderSource == "Google News")
                        resource.FilePath = dir3 + "/" + source + ".jpg";

                    else
                        resource.FilePath = dir3 + "/" + source;
                }
                resource.BucketId = ((int)NMSBucket.NMSBucket);
                resource.ApiKey = NMS.Core.AppSettings.MediaServerApiKey;
            }
            Resource res = new Resource();
            res.CopyFrom(MediaApi.PostResource(resource));
            return res;
        }

        public Resource MediaPostResourceForNMSFileUpload(int bucketId, string apikey, string caption, string sourcePath, string filePath,int userID,string name)
        {
            MSApi MediaApi = new MSApi();
            MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
            resource.ResourceStatusId = ((int)MS.Core.Enums.ResourceStatuses.TempFile).ToString();
            resource.Source = sourcePath;
            resource.IsFromExternalSource = false;
            resource.Caption = caption;
            resource.FileName = System.IO.Path.GetFileName(sourcePath);
            resource.BucketId = bucketId;
            resource.ApiKey = apikey;
            resource.FilePath = filePath + "/" + resource.FileName;
            resource.SourceName = name;
            resource.SourceId = userID.ToString();
            resource.SourceTypeId = 7.ToString();

            Resource res = new Resource();
            res.CopyFrom(MediaApi.PostResource(resource));
            return res;
        }

        public void CreateSubBucket(int bucketId, string apikey, string filePath)
        {
            MSApi MediaApi = new MSApi();
            string[] paths = filePath.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            string str = string.Empty;
            foreach (var path in paths)
            {
                MediaApi.CreateSubBucket(bucketId, apikey, str + path);
                str += path + "/";
            }
        }

        public Resource MediaPostResourceForGoogleData(MS.Core.DataTransfer.Resource.PostInput resource, string FolderSource)
        {
            MSApi MediaApi = new MSApi();
            Guid g = Guid.NewGuid();
            //  resource.FilePath = dir3 + "/" + g.ToString();
            resource.FileName = g.ToString();
            resource.ApiKey = resource.ApiKey;
            Resource res = new Resource();
            res.CopyFrom(MediaApi.PostResource(resource));
            return res;
        }

        //public HttpStatusCode MediaPostMedia(string Guid,bool Ishd,string FileName,byte[] data,string Param,string ContentType)
        //{
        //    MSApi MediaApi = new MSApi();
        //    var httpStatus = MediaApi.PostMedia(Guid, Ishd, FileName.Trim('"'), data, Param, ContentType, new NameValueCollection());
        //    return httpStatus;
        //}

    }
}
