﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Role;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class RoleService : IRoleService 
	{
		private IRoleRepository _iRoleRepository;
        
		public RoleService(IRoleRepository iRoleRepository)
		{
			this._iRoleRepository = iRoleRepository;
		}
        
        public Dictionary<string, string> GetRoleBasicSearchColumns()
        {
            
            return this._iRoleRepository.GetRoleBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRoleAdvanceSearchColumns()
        {
            
            return this._iRoleRepository.GetRoleAdvanceSearchColumns();
           
        }
        

		public Role GetRole(System.Int32 RoleId)
		{
			return _iRoleRepository.GetRole(RoleId);
		}

		public Role UpdateRole(Role entity)
		{
			return _iRoleRepository.UpdateRole(entity);
		}

		public bool DeleteRole(System.Int32 RoleId)
		{
			return _iRoleRepository.DeleteRole(RoleId);
		}

		public List<Role> GetAllRole()
		{
			return _iRoleRepository.GetAllRole();
		}

		public Role InsertRole(Role entity)
		{
			 return _iRoleRepository.InsertRole(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 roleid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out roleid))
            {
				Role role = _iRoleRepository.GetRole(roleid);
                if(role!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(role);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Role> rolelist = _iRoleRepository.GetAllRole();
            if (rolelist != null && rolelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(rolelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Role role = new Role();
                PostOutput output = new PostOutput();
                role.CopyFrom(Input);
                role = _iRoleRepository.InsertRole(role);
                output.CopyFrom(role);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Role roleinput = new Role();
                Role roleoutput = new Role();
                PutOutput output = new PutOutput();
                roleinput.CopyFrom(Input);
                Role role = _iRoleRepository.GetRole(roleinput.RoleId);
                if (role!=null)
                {
                    roleoutput = _iRoleRepository.UpdateRole(roleinput);
                    if(roleoutput!=null)
                    {
                        output.CopyFrom(roleoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 roleid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out roleid))
            {
				 bool IsDeleted = _iRoleRepository.DeleteRole(roleid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
