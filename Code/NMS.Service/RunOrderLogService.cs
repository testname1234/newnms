﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RunOrderLog;
using Validation;
using System.Linq;
using NMS.Service;
using NMS.Core;

namespace NMS.Service
{
		
	public class RunOrderLogService : IRunOrderLogService 
	{
		private IRunOrderLogRepository _iRunOrderLogRepository;
        
		public RunOrderLogService(IRunOrderLogRepository iRunOrderLogRepository)
		{
			this._iRunOrderLogRepository = iRunOrderLogRepository;
		}
        
        public Dictionary<string, string> GetRunOrderLogBasicSearchColumns()
        {
            
            return this._iRunOrderLogRepository.GetRunOrderLogBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRunOrderLogAdvanceSearchColumns()
        {
            
            return this._iRunOrderLogRepository.GetRunOrderLogAdvanceSearchColumns();
           
        }
        

		public RunOrderLog GetRunOrderLog(System.Int32 RunOrderLogId)
		{
			return _iRunOrderLogRepository.GetRunOrderLog(RunOrderLogId);
		}

		public RunOrderLog UpdateRunOrderLog(RunOrderLog entity)
		{
			return _iRunOrderLogRepository.UpdateRunOrderLog(entity);
		}

		public bool DeleteRunOrderLog(System.Int32 RunOrderLogId)
		{
			return _iRunOrderLogRepository.DeleteRunOrderLog(RunOrderLogId);
		}

		public List<RunOrderLog> GetAllRunOrderLog()
		{
			return _iRunOrderLogRepository.GetAllRunOrderLog();
		}

		public RunOrderLog InsertRunOrderLog(RunOrderLog entity)
		{
			 return _iRunOrderLogRepository.InsertRunOrderLog(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 runorderlogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out runorderlogid))
            {
				RunOrderLog runorderlog = _iRunOrderLogRepository.GetRunOrderLog(runorderlogid);
                if(runorderlog!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(runorderlog);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<RunOrderLog> runorderloglist = _iRunOrderLogRepository.GetAllRunOrderLog();
            if (runorderloglist != null && runorderloglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(runorderloglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                RunOrderLog runorderlog = new RunOrderLog();
                PostOutput output = new PostOutput();
                runorderlog.CopyFrom(Input);
                runorderlog = _iRunOrderLogRepository.InsertRunOrderLog(runorderlog);
                output.CopyFrom(runorderlog);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                RunOrderLog runorderloginput = new RunOrderLog();
                RunOrderLog runorderlogoutput = new RunOrderLog();
                PutOutput output = new PutOutput();
                runorderloginput.CopyFrom(Input);
                RunOrderLog runorderlog = _iRunOrderLogRepository.GetRunOrderLog(runorderloginput.RunOrderLogId);
                if (runorderlog!=null)
                {
                    runorderlogoutput = _iRunOrderLogRepository.UpdateRunOrderLog(runorderloginput);
                    if(runorderlogoutput!=null)
                    {
                        output.CopyFrom(runorderlogoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 runorderlogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out runorderlogid))
            {
				 bool IsDeleted = _iRunOrderLogRepository.DeleteRunOrderLog(runorderlogid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
