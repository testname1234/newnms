﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RunOrderStories;
using Validation;
using System.Linq;
using NMS.Service;
using NMS.Core;

namespace NMS.Service
{
		
	public class RunOrderStoriesService : IRunOrderStoriesService 
	{
		private IRunOrderStoriesRepository _iRunOrderStoriesRepository;
        
		public RunOrderStoriesService(IRunOrderStoriesRepository iRunOrderStoriesRepository)
		{
			this._iRunOrderStoriesRepository = iRunOrderStoriesRepository;
		}
        
        public Dictionary<string, string> GetRunOrderStoriesBasicSearchColumns()
        {
            
            return this._iRunOrderStoriesRepository.GetRunOrderStoriesBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetRunOrderStoriesAdvanceSearchColumns()
        {
            
            return this._iRunOrderStoriesRepository.GetRunOrderStoriesAdvanceSearchColumns();
           
        }
        

		public virtual List<RunOrderStories> GetRunOrderStoriesByRunOrderLogId(System.Int32 RunOrderLogId)
		{
			return _iRunOrderStoriesRepository.GetRunOrderStoriesByRunOrderLogId(RunOrderLogId);
		}

		public RunOrderStories GetRunOrderStories(System.Int32 RunOrderStoryId)
		{
			return _iRunOrderStoriesRepository.GetRunOrderStories(RunOrderStoryId);
		}

		public RunOrderStories UpdateRunOrderStories(RunOrderStories entity)
		{
			return _iRunOrderStoriesRepository.UpdateRunOrderStories(entity);
		}

		public bool DeleteRunOrderStories(System.Int32 RunOrderStoryId)
		{
			return _iRunOrderStoriesRepository.DeleteRunOrderStories(RunOrderStoryId);
		}

		public List<RunOrderStories> GetAllRunOrderStories()
		{
			return _iRunOrderStoriesRepository.GetAllRunOrderStories();
		}

		public RunOrderStories InsertRunOrderStories(RunOrderStories entity)
		{
			 return _iRunOrderStoriesRepository.InsertRunOrderStories(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 runorderstoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out runorderstoryid))
            {
				RunOrderStories runorderstories = _iRunOrderStoriesRepository.GetRunOrderStories(runorderstoryid);
                if(runorderstories!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(runorderstories);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<RunOrderStories> runorderstorieslist = _iRunOrderStoriesRepository.GetAllRunOrderStories();
            if (runorderstorieslist != null && runorderstorieslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(runorderstorieslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                RunOrderStories runorderstories = new RunOrderStories();
                PostOutput output = new PostOutput();
                runorderstories.CopyFrom(Input);
                runorderstories = _iRunOrderStoriesRepository.InsertRunOrderStories(runorderstories);
                output.CopyFrom(runorderstories);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                RunOrderStories runorderstoriesinput = new RunOrderStories();
                RunOrderStories runorderstoriesoutput = new RunOrderStories();
                PutOutput output = new PutOutput();
                runorderstoriesinput.CopyFrom(Input);
                RunOrderStories runorderstories = _iRunOrderStoriesRepository.GetRunOrderStories(runorderstoriesinput.RunOrderStoryId);
                if (runorderstories!=null)
                {
                    runorderstoriesoutput = _iRunOrderStoriesRepository.UpdateRunOrderStories(runorderstoriesinput);
                    if(runorderstoriesoutput!=null)
                    {
                        output.CopyFrom(runorderstoriesoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 runorderstoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out runorderstoryid))
            {
				 bool IsDeleted = _iRunOrderStoriesRepository.DeleteRunOrderStories(runorderstoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
