﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScrapMaxDates;
using Validation;
using System.Linq;
using NMS.Core;
using System.Data;

namespace NMS.Service
{
		
	public class ScrapMaxDatesService : IScrapMaxDatesService 
	{
		private IScrapMaxDatesRepository _iScrapMaxDatesRepository;
        
		public ScrapMaxDatesService(IScrapMaxDatesRepository iScrapMaxDatesRepository)
		{
			this._iScrapMaxDatesRepository = iScrapMaxDatesRepository;
		}
        
        public Dictionary<string, string> GetScrapMaxDatesBasicSearchColumns()
        {
            
            return this._iScrapMaxDatesRepository.GetScrapMaxDatesBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetScrapMaxDatesAdvanceSearchColumns()
        {
            
            return this._iScrapMaxDatesRepository.GetScrapMaxDatesAdvanceSearchColumns();
           
        }

        public ScrapMaxDates GetScrapDateCreateIfNotExist(ScrapMaxDates ScrapMaxDates)
        {
            ScrapMaxDates entity = GetScrapMaxDatesBySource(ScrapMaxDates.SourceName);
            if ( entity != null)
            {
                return entity;
            }
            else
            {
                return InsertScrapMaxDates(ScrapMaxDates);
            }

        }

		public ScrapMaxDates GetScrapMaxDates(System.Int32 ScrapMaxDatesId)
		{
			return _iScrapMaxDatesRepository.GetScrapMaxDates(ScrapMaxDatesId);
		}

        public ScrapMaxDates GetScrapMaxDatesBySource(System.String Source)
        {
            return _iScrapMaxDatesRepository.GetScrapMaxDatesBySource(Source);
        }

		public ScrapMaxDates UpdateScrapMaxDates(ScrapMaxDates entity)
		{
			return _iScrapMaxDatesRepository.UpdateScrapMaxDates(entity);
		}

		public bool DeleteScrapMaxDates(System.Int32 ScrapMaxDatesId)
		{
			return _iScrapMaxDatesRepository.DeleteScrapMaxDates(ScrapMaxDatesId);
		}

		public List<ScrapMaxDates> GetAllScrapMaxDates()
		{
			return _iScrapMaxDatesRepository.GetAllScrapMaxDates();
		}

		public ScrapMaxDates InsertScrapMaxDates(ScrapMaxDates entity)
		{
			 return _iScrapMaxDatesRepository.InsertScrapMaxDates(entity);
		}

        public DataTable GetResult()
        {
            return _iScrapMaxDatesRepository.GetResult();
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 scrapmaxdatesid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out scrapmaxdatesid))
            {
				ScrapMaxDates scrapmaxdates = _iScrapMaxDatesRepository.GetScrapMaxDates(scrapmaxdatesid);
                if(scrapmaxdates!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(scrapmaxdates);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ScrapMaxDates> scrapmaxdateslist = _iScrapMaxDatesRepository.GetAllScrapMaxDates();
            if (scrapmaxdateslist != null && scrapmaxdateslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(scrapmaxdateslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ScrapMaxDates scrapmaxdates = new ScrapMaxDates();
                PostOutput output = new PostOutput();
                scrapmaxdates.CopyFrom(Input);
                scrapmaxdates = _iScrapMaxDatesRepository.InsertScrapMaxDates(scrapmaxdates);
                output.CopyFrom(scrapmaxdates);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ScrapMaxDates scrapmaxdatesinput = new ScrapMaxDates();
                ScrapMaxDates scrapmaxdatesoutput = new ScrapMaxDates();
                PutOutput output = new PutOutput();
                scrapmaxdatesinput.CopyFrom(Input);
                ScrapMaxDates scrapmaxdates = _iScrapMaxDatesRepository.GetScrapMaxDates(scrapmaxdatesinput.ScrapMaxDatesId);
                if (scrapmaxdates!=null)
                {
                    scrapmaxdatesoutput = _iScrapMaxDatesRepository.UpdateScrapMaxDates(scrapmaxdatesinput);
                    if(scrapmaxdatesoutput!=null)
                    {
                        output.CopyFrom(scrapmaxdatesoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 scrapmaxdatesid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out scrapmaxdatesid))
            {
				 bool IsDeleted = _iScrapMaxDatesRepository.DeleteScrapMaxDates(scrapmaxdatesid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
