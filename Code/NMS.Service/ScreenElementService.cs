﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScreenElement;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ScreenElementService : IScreenElementService 
	{
		private IScreenElementRepository _iScreenElementRepository;
        
		public ScreenElementService(IScreenElementRepository iScreenElementRepository)
		{
			this._iScreenElementRepository = iScreenElementRepository;
		}
        
        public Dictionary<string, string> GetScreenElementBasicSearchColumns()
        {
            
            return this._iScreenElementRepository.GetScreenElementBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetScreenElementAdvanceSearchColumns()
        {
            
            return this._iScreenElementRepository.GetScreenElementAdvanceSearchColumns();
           
        }
        

		public ScreenElement GetScreenElement(System.Int32 ScreenElementId)
		{
			return _iScreenElementRepository.GetScreenElement(ScreenElementId);
		}

		public ScreenElement UpdateScreenElement(ScreenElement entity)
		{
			return _iScreenElementRepository.UpdateScreenElement(entity);
		}

		public bool DeleteScreenElement(System.Int32 ScreenElementId)
		{
			return _iScreenElementRepository.DeleteScreenElement(ScreenElementId);
		}

		public List<ScreenElement> GetAllScreenElement()
		{
			return _iScreenElementRepository.GetAllScreenElement();
		}

		public ScreenElement InsertScreenElement(ScreenElement entity)
		{
			 return _iScreenElementRepository.InsertScreenElement(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 screenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out screenelementid))
            {
				ScreenElement screenelement = _iScreenElementRepository.GetScreenElement(screenelementid);
                if(screenelement!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(screenelement);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ScreenElement> screenelementlist = _iScreenElementRepository.GetAllScreenElement();
            if (screenelementlist != null && screenelementlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(screenelementlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ScreenElement screenelement = new ScreenElement();
                PostOutput output = new PostOutput();
                screenelement.CopyFrom(Input);
                screenelement = _iScreenElementRepository.InsertScreenElement(screenelement);
                output.CopyFrom(screenelement);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ScreenElement screenelementinput = new ScreenElement();
                ScreenElement screenelementoutput = new ScreenElement();
                PutOutput output = new PutOutput();
                screenelementinput.CopyFrom(Input);
                ScreenElement screenelement = _iScreenElementRepository.GetScreenElement(screenelementinput.ScreenElementId);
                if (screenelement!=null)
                {
                    screenelementoutput = _iScreenElementRepository.UpdateScreenElement(screenelementinput);
                    if(screenelementoutput!=null)
                    {
                        output.CopyFrom(screenelementoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 screenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out screenelementid))
            {
				 bool IsDeleted = _iScreenElementRepository.DeleteScreenElement(screenelementid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
