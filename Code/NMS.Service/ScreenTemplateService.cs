﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScreenTemplate;
using Validation;
using System.Linq;
using NMS.Core;
using System.Transactions;

namespace NMS.Service
{

    public class ScreenTemplateService : IScreenTemplateService
    {
        private IScreenTemplateRepository _iScreenTemplateRepository;
        private ITemplateScreenElementRepository _iTemplateScreenElementRepository;
        private IScreenTemplatekeyRepository _iScreenTemplatekeyRepository;

        public ScreenTemplateService(IScreenTemplateRepository iScreenTemplateRepository, ITemplateScreenElementRepository iTemplateScreenElementRepository, IScreenTemplatekeyRepository iScreenTemplatekeyRepository)
        {
            this._iScreenTemplateRepository = iScreenTemplateRepository;
            this._iTemplateScreenElementRepository = iTemplateScreenElementRepository;
            this._iScreenTemplatekeyRepository = iScreenTemplatekeyRepository;
        }

        public Dictionary<string, string> GetScreenTemplateBasicSearchColumns()
        {

            return this._iScreenTemplateRepository.GetScreenTemplateBasicSearchColumns();

        }

        public List<SearchColumn> GetScreenTemplateAdvanceSearchColumns()
        {

            return this._iScreenTemplateRepository.GetScreenTemplateAdvanceSearchColumns();

        }


        public virtual List<ScreenTemplate> GetScreenTemplateByProgramId(System.Int32 ProgramId)
        {
            return _iScreenTemplateRepository.GetScreenTemplateByProgramId(ProgramId);
        }

        public ScreenTemplate GetScreenTemplate(System.Int32 ScreenTemplateId)
        {
            return _iScreenTemplateRepository.GetScreenTemplate(ScreenTemplateId);
        }

        public ScreenTemplate UpdateScreenTemplate(ScreenTemplate entity)
        {
            return _iScreenTemplateRepository.UpdateScreenTemplate(entity);
        }

        public bool DeleteScreenTemplate(System.Int32 ScreenTemplateId)
        {
            return _iScreenTemplateRepository.DeleteScreenTemplate(ScreenTemplateId);
        }

        public List<ScreenTemplate> GetAllScreenTemplate()
        {
            return _iScreenTemplateRepository.GetAllScreenTemplate();
        }

        public ScreenTemplate InsertScreenTemplate(ScreenTemplate entity)
        {
            return _iScreenTemplateRepository.InsertScreenTemplate(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 screentemplateid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out screentemplateid))
            {
                ScreenTemplate screentemplate = _iScreenTemplateRepository.GetScreenTemplate(screentemplateid);
                if (screentemplate != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(screentemplate);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ScreenTemplate> screentemplatelist = _iScreenTemplateRepository.GetAllScreenTemplate();
            if (screentemplatelist != null && screentemplatelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(screentemplatelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ScreenTemplate screentemplate = new ScreenTemplate();
                PostOutput output = new PostOutput();
                screentemplate.CopyFrom(Input);
                screentemplate = _iScreenTemplateRepository.InsertScreenTemplate(screentemplate);
                output.CopyFrom(screentemplate);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ScreenTemplate screentemplateinput = new ScreenTemplate();
                ScreenTemplate screentemplateoutput = new ScreenTemplate();
                PutOutput output = new PutOutput();
                screentemplateinput.CopyFrom(Input);
                ScreenTemplate screentemplate = _iScreenTemplateRepository.GetScreenTemplate(screentemplateinput.ScreenTemplateId);
                if (screentemplate != null)
                {
                    screentemplateoutput = _iScreenTemplateRepository.UpdateScreenTemplate(screentemplateinput);
                    if (screentemplateoutput != null)
                    {
                        output.CopyFrom(screentemplateoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 screentemplateid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out screentemplateid))
            {
                bool IsDeleted = _iScreenTemplateRepository.DeleteScreenTemplate(screentemplateid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public void SyncScreenTemplate()
        {

            DateTime dtMax = _iScreenTemplateRepository.GetMaxLastUpdateDate();
            MMS.Integration.FPC.FPCAPI api = new MMS.Integration.FPC.FPCAPI();
            var transfer = api.GetScreenTemplateByLastUpdateDate(dtMax);
            TransactionOptions options = new TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            options.Timeout = new TimeSpan(0, 60, 0);

            if (transfer.IsSuccess && transfer.Data != null)
            {
                transfer.Data = transfer.Data.OrderBy(x => x.LastUpdateDate).ToList();
                foreach (var item in transfer.Data)
                {
                    //if (item.ProgramChannelMappingId == 2242 && item.IsVideoWall)
                    if (item.ProgramChannelMappingId == 5765 || item.ProgramChannelMappingId == 5766 ||item.ProgramChannelMappingId == 5767)
                    {                        
                        var screenTemplate = _iScreenTemplateRepository.GetScreenTemplate(item.ProgramScreenTemplateId);
                        if (screenTemplate != null)
                        {
                            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                            {
                                screenTemplate.IsActive = item.IsActive.HasValue ? item.IsActive.Value : true;
                                screenTemplate.Flashtemplateid = item.FlashTemplateId.Value;
                                screenTemplate.RatioTypeId = item.RatioTypeId;
                                screenTemplate.IsVideoWallTemplate = item.IsVideoWall;
                                screenTemplate.GroupId = item.GroupId;
                                screenTemplate.Name = item.Name;
                                if (item.ThumbGuid.HasValue)
                                    screenTemplate.ThumbGuid = item.ThumbGuid.Value;
                                if (item.BackgroundImageUrl.HasValue)
                                    screenTemplate.BackgroundImageUrl = item.BackgroundImageUrl;
                                screenTemplate.LastUpdateDate = item.LastUpdateDate.HasValue ? item.LastUpdateDate.Value : DateTime.UtcNow;
                                _iScreenTemplateRepository.UpdateScreenTemplate(screenTemplate);

                                _iTemplateScreenElementRepository.DeleteTemplateScreenElementByScreenTemplateID(screenTemplate.ScreenTemplateId);

                                _iScreenTemplatekeyRepository.DeleteScreenTemplatekeyByScreenTemplateID(screenTemplate.ScreenTemplateId);
                                foreach (var element in item.TemplateScreenElements)
                                {
                                    TemplateScreenElement _element = new TemplateScreenElement();
                                    element.ScreenTemplateId = screenTemplate.ScreenTemplateId;
                                    _element.CopyFrom(element);
                                    _element.Flashtemplatewindowid = element.FlashTemplateWindowId;
                                    _element.TemplateScreenElementId = 0;

                                    _iTemplateScreenElementRepository.InsertTemplateScreenElement(_element);
                                }

                                foreach (var key in item.ScreenTemplateKeys)
                                {
                                    ScreenTemplatekey _key = new ScreenTemplatekey();
                                    _key.CopyFrom(key);
                                    _key.ScreenTemplatekeyId = 0;
                                    _key.ScreenTemplateId = item.ProgramScreenTemplateId;
                                    _iScreenTemplatekeyRepository.InsertScreenTemplatekey(_key);
                                }
                                scope.Complete();
                                Console.WriteLine(item.Name + " updated");
                            }
                        }
                        else if(item.IsActive.HasValue && item.IsActive.Value)
                        {
                            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                            {
                                screenTemplate = new ScreenTemplate();
                                screenTemplate.CopyFrom(item);
                                screenTemplate.ProgramId = item.ProgramChannelMappingId;
                                screenTemplate.Flashtemplateid = !item.FlashTemplateId.HasValue ? 0 : item.FlashTemplateId.Value;
                                screenTemplate.CreatonDate = item.CreationDate.Value;
                                screenTemplate.ScreenTemplateId = item.ProgramScreenTemplateId;
                                screenTemplate.RatioTypeId = item.RatioTypeId;
                                screenTemplate.IsVideoWallTemplate = item.IsVideoWall;
                                screenTemplate.Html = "html";
                                _iScreenTemplateRepository.InsertScreenTemplate(screenTemplate);
                                foreach (var element in item.TemplateScreenElements)
                                {
                                    TemplateScreenElement _element = new TemplateScreenElement();
                                    element.ScreenTemplateId = screenTemplate.ScreenTemplateId;
                                    _element.CopyFrom(element);
                                    _element.Flashtemplatewindowid = element.FlashTemplateWindowId;
                                    _element.TemplateScreenElementId = 0;
                                    _iTemplateScreenElementRepository.InsertTemplateScreenElement(_element);
                                }

                                foreach (var key in item.ScreenTemplateKeys)
                                {
                                    ScreenTemplatekey _key = new ScreenTemplatekey();
                                    _key.CopyFrom(key);
                                    _key.ScreenTemplatekeyId = 0;
                                    _key.ScreenTemplateId = item.ProgramScreenTemplateId;
                                    _iScreenTemplatekeyRepository.InsertScreenTemplatekey(_key);
                                }
                                scope.Complete();
                                Console.WriteLine(item.Name + " inserted");
                            }
                        }
                    }
                }
            }
        }
    }
}
