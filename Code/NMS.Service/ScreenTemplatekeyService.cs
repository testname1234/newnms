﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ScreenTemplatekey;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class ScreenTemplatekeyService : IScreenTemplatekeyService
    {
        private IScreenTemplatekeyRepository _iScreenTemplatekeyRepository;

        public ScreenTemplatekeyService(IScreenTemplatekeyRepository iScreenTemplatekeyRepository)
        {
            this._iScreenTemplatekeyRepository = iScreenTemplatekeyRepository;
        }

        public Dictionary<string, string> GetScreenTemplatekeyBasicSearchColumns()
        {

            return this._iScreenTemplatekeyRepository.GetScreenTemplatekeyBasicSearchColumns();

        }

        public List<SearchColumn> GetScreenTemplatekeyAdvanceSearchColumns()
        {

            return this._iScreenTemplatekeyRepository.GetScreenTemplatekeyAdvanceSearchColumns();

        }


        public virtual List<ScreenTemplatekey> GetScreenTemplatekeyByScreenTemplateId(System.Int32? ScreenTemplateId)
        {
            return _iScreenTemplatekeyRepository.GetScreenTemplatekeyByScreenTemplateId(ScreenTemplateId);
        }

        public virtual List<ScreenTemplatekey> GetByScreenTemplateList(List<ScreenTemplate> ScreenTemplates)
        {
            List<ScreenTemplatekey> screenTemplatekeys = new List<ScreenTemplatekey>();
            foreach (ScreenTemplate screenTemplate in ScreenTemplates)
            {
                List<ScreenTemplatekey> lstkeys = _iScreenTemplatekeyRepository.GetScreenTemplatekeyByScreenTemplateId(screenTemplate.ScreenTemplateId);
                if (lstkeys != null)
                    screenTemplatekeys.AddRange(_iScreenTemplatekeyRepository.GetScreenTemplatekeyByScreenTemplateId(screenTemplate.ScreenTemplateId));
            }
            return screenTemplatekeys;
        }


        public ScreenTemplatekey GetScreenTemplatekey(System.Int32 ScreenTemplatekeyId)
        {
            return _iScreenTemplatekeyRepository.GetScreenTemplatekey(ScreenTemplatekeyId);
        }

        public ScreenTemplatekey UpdateScreenTemplatekey(ScreenTemplatekey entity)
        {
            return _iScreenTemplatekeyRepository.UpdateScreenTemplatekey(entity);
        }

        public bool DeleteScreenTemplatekey(System.Int32 ScreenTemplatekeyId)
        {
            return _iScreenTemplatekeyRepository.DeleteScreenTemplatekey(ScreenTemplatekeyId);
        }

        public List<ScreenTemplatekey> GetAllScreenTemplatekey()
        {
            return _iScreenTemplatekeyRepository.GetAllScreenTemplatekey();
        }

        public ScreenTemplatekey InsertScreenTemplatekey(ScreenTemplatekey entity)
        {
            return _iScreenTemplatekeyRepository.InsertScreenTemplatekey(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 screentemplatekeyid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out screentemplatekeyid))
            {
                ScreenTemplatekey screentemplatekey = _iScreenTemplatekeyRepository.GetScreenTemplatekey(screentemplatekeyid);
                if (screentemplatekey != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(screentemplatekey);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ScreenTemplatekey> screentemplatekeylist = _iScreenTemplatekeyRepository.GetAllScreenTemplatekey();
            if (screentemplatekeylist != null && screentemplatekeylist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(screentemplatekeylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ScreenTemplatekey screentemplatekey = new ScreenTemplatekey();
                PostOutput output = new PostOutput();
                screentemplatekey.CopyFrom(Input);
                screentemplatekey = _iScreenTemplatekeyRepository.InsertScreenTemplatekey(screentemplatekey);
                output.CopyFrom(screentemplatekey);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ScreenTemplatekey screentemplatekeyinput = new ScreenTemplatekey();
                ScreenTemplatekey screentemplatekeyoutput = new ScreenTemplatekey();
                PutOutput output = new PutOutput();
                screentemplatekeyinput.CopyFrom(Input);
                ScreenTemplatekey screentemplatekey = _iScreenTemplatekeyRepository.GetScreenTemplatekey(screentemplatekeyinput.ScreenTemplatekeyId);
                if (screentemplatekey != null)
                {
                    screentemplatekeyoutput = _iScreenTemplatekeyRepository.UpdateScreenTemplatekey(screentemplatekeyinput);
                    if (screentemplatekeyoutput != null)
                    {
                        output.CopyFrom(screentemplatekeyoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 screentemplatekeyid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out screentemplatekeyid))
            {
                bool IsDeleted = _iScreenTemplatekeyRepository.DeleteScreenTemplatekey(screentemplatekeyid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
    }


}
