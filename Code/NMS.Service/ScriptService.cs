﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Script;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class ScriptService : IScriptService 
	{
		private IScriptRepository _iScriptRepository;
        
		public ScriptService(IScriptRepository iScriptRepository)
		{
			this._iScriptRepository = iScriptRepository;
		}
        
        public Dictionary<string, string> GetScriptBasicSearchColumns()
        {
            
            return this._iScriptRepository.GetScriptBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetScriptAdvanceSearchColumns()
        {
            
            return this._iScriptRepository.GetScriptAdvanceSearchColumns();
           
        }
        

		public Script GetScript(System.Int32 ScriptId)
		{
			return _iScriptRepository.GetScript(ScriptId);
		}

		public Script UpdateScript(Script entity)
		{
			return _iScriptRepository.UpdateScript(entity);
		}

		public bool DeleteScript(System.Int32 ScriptId)
		{
			return _iScriptRepository.DeleteScript(ScriptId);
		}

		public List<Script> GetAllScript()
		{
			return _iScriptRepository.GetAllScript();
		}

		public Script InsertScript(Script entity)
		{
			 return _iScriptRepository.InsertScript(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 scriptid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out scriptid))
            {
				Script script = _iScriptRepository.GetScript(scriptid);
                if(script!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(script);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Script> scriptlist = _iScriptRepository.GetAllScript();
            if (scriptlist != null && scriptlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(scriptlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            Script script = new Script();
            PostOutput output = new PostOutput();
            script.CopyFrom(Input);
            script.CreationDate = DateTime.UtcNow;
            script.LastUpdateDate = script.CreationDate;
            script = _iScriptRepository.InsertScript(script);
            output.CopyFrom(script);
            transer.IsSuccess = true;
            transer.Data = output;
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Script scriptinput = new Script();
                Script scriptoutput = new Script();
                PutOutput output = new PutOutput();
                scriptinput.CopyFrom(Input);
                Script script = _iScriptRepository.GetScript(scriptinput.ScriptId);
                if (script!=null)
                {
                    scriptoutput = _iScriptRepository.UpdateScript(scriptinput);
                    if(scriptoutput!=null)
                    {
                        output.CopyFrom(scriptoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 scriptid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out scriptid))
            {
				 bool IsDeleted = _iScriptRepository.DeleteScript(scriptid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public bool MarkAsExecuted(int id)
         {
             return _iScriptRepository.MarkAsExecuted(id);
         }


         public List<GetOutput> GetAllUnExecutedScripts(string token)
         {
             List<GetOutput> output = new List<GetOutput>();
             var lst = _iScriptRepository.GetAllUnExecutedScripts(token);
             if (lst != null && lst.Count() > 0)
             {
                 output.CopyFrom(lst);
             }
             return output;
         }
    }
	
	
}
