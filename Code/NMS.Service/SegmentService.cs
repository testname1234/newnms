﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Segment;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SegmentService : ISegmentService 
	{
		private ISegmentRepository _iSegmentRepository;
        
		public SegmentService(ISegmentRepository iSegmentRepository)
		{
			this._iSegmentRepository = iSegmentRepository;
		}
        
        public Dictionary<string, string> GetSegmentBasicSearchColumns()
        {
            
            return this._iSegmentRepository.GetSegmentBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSegmentAdvanceSearchColumns()
        {
            
            return this._iSegmentRepository.GetSegmentAdvanceSearchColumns();
           
        }
        

		public virtual List<Segment> GetSegmentBySegmentTypeId(System.Int32 SegmentTypeId)
		{
			return _iSegmentRepository.GetSegmentBySegmentTypeId(SegmentTypeId);
		}

		public virtual List<Segment> GetSegmentByEpisodeId(System.Int32 EpisodeId)
		{
			return _iSegmentRepository.GetSegmentByEpisodeId(EpisodeId);
		}

		public Segment GetSegment(System.Int32 SegmentId)
		{
			return _iSegmentRepository.GetSegment(SegmentId);
		}

		public Segment UpdateSegment(Segment entity)
		{
			return _iSegmentRepository.UpdateSegment(entity);
		}

		public bool DeleteSegment(System.Int32 SegmentId)
		{
			return _iSegmentRepository.DeleteSegment(SegmentId);
		}

		public List<Segment> GetAllSegment()
		{
			return _iSegmentRepository.GetAllSegment();
		}

		public Segment InsertSegment(Segment entity)
		{
			 return _iSegmentRepository.InsertSegment(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 segmentid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out segmentid))
            {
				Segment segment = _iSegmentRepository.GetSegment(segmentid);
                if(segment!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(segment);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Segment> segmentlist = _iSegmentRepository.GetAllSegment();
            if (segmentlist != null && segmentlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(segmentlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Segment segment = new Segment();
                PostOutput output = new PostOutput();
                segment.CopyFrom(Input);
                segment = _iSegmentRepository.InsertSegment(segment);
                output.CopyFrom(segment);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Segment segmentinput = new Segment();
                Segment segmentoutput = new Segment();
                PutOutput output = new PutOutput();
                segmentinput.CopyFrom(Input);
                Segment segment = _iSegmentRepository.GetSegment(segmentinput.SegmentId);
                if (segment!=null)
                {
                    segmentoutput = _iSegmentRepository.UpdateSegment(segmentinput);
                    if(segmentoutput!=null)
                    {
                        output.CopyFrom(segmentoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 segmentid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out segmentid))
            {
				 bool IsDeleted = _iSegmentRepository.DeleteSegment(segmentid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
