﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SegmentStory;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SegmentStoryService : ISegmentStoryService 
	{
		private ISegmentStoryRepository _iSegmentStoryRepository;
        
		public SegmentStoryService(ISegmentStoryRepository iSegmentStoryRepository)
		{
			this._iSegmentStoryRepository = iSegmentStoryRepository;
		}
        
        public Dictionary<string, string> GetSegmentStoryBasicSearchColumns()
        {
            
            return this._iSegmentStoryRepository.GetSegmentStoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSegmentStoryAdvanceSearchColumns()
        {
            
            return this._iSegmentStoryRepository.GetSegmentStoryAdvanceSearchColumns();
           
        }
        

		public virtual List<SegmentStory> GetSegmentStoryBySegmentId(System.Int32 SegmentId)
		{
			return _iSegmentStoryRepository.GetSegmentStoryBySegmentId(SegmentId);
		}

		public virtual List<SegmentStory> GetSegmentStoryByStoryId(System.Int32 StoryId)
		{
			return _iSegmentStoryRepository.GetSegmentStoryByStoryId(StoryId);
		}

		public SegmentStory GetSegmentStory(System.Int32 SegmentStoryId)
		{
			return _iSegmentStoryRepository.GetSegmentStory(SegmentStoryId);
		}

		public SegmentStory UpdateSegmentStory(SegmentStory entity)
		{
			return _iSegmentStoryRepository.UpdateSegmentStory(entity);
		}

		public bool DeleteSegmentStory(System.Int32 SegmentStoryId)
		{
			return _iSegmentStoryRepository.DeleteSegmentStory(SegmentStoryId);
		}

		public List<SegmentStory> GetAllSegmentStory()
		{
			return _iSegmentStoryRepository.GetAllSegmentStory();
		}

		public SegmentStory InsertSegmentStory(SegmentStory entity)
		{
			 return _iSegmentStoryRepository.InsertSegmentStory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 segmentstoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out segmentstoryid))
            {
				SegmentStory segmentstory = _iSegmentStoryRepository.GetSegmentStory(segmentstoryid);
                if(segmentstory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(segmentstory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SegmentStory> segmentstorylist = _iSegmentStoryRepository.GetAllSegmentStory();
            if (segmentstorylist != null && segmentstorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(segmentstorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SegmentStory segmentstory = new SegmentStory();
                PostOutput output = new PostOutput();
                segmentstory.CopyFrom(Input);
                segmentstory = _iSegmentStoryRepository.InsertSegmentStory(segmentstory);
                output.CopyFrom(segmentstory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SegmentStory segmentstoryinput = new SegmentStory();
                SegmentStory segmentstoryoutput = new SegmentStory();
                PutOutput output = new PutOutput();
                segmentstoryinput.CopyFrom(Input);
                SegmentStory segmentstory = _iSegmentStoryRepository.GetSegmentStory(segmentstoryinput.SegmentStoryId);
                if (segmentstory!=null)
                {
                    segmentstoryoutput = _iSegmentStoryRepository.UpdateSegmentStory(segmentstoryinput);
                    if(segmentstoryoutput!=null)
                    {
                        output.CopyFrom(segmentstoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 segmentstoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out segmentstoryid))
            {
				 bool IsDeleted = _iSegmentStoryRepository.DeleteSegmentStory(segmentstoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
