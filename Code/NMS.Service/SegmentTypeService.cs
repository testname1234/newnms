﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SegmentType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SegmentTypeService : ISegmentTypeService 
	{
		private ISegmentTypeRepository _iSegmentTypeRepository;
        
		public SegmentTypeService(ISegmentTypeRepository iSegmentTypeRepository)
		{
			this._iSegmentTypeRepository = iSegmentTypeRepository;
		}
        
        public Dictionary<string, string> GetSegmentTypeBasicSearchColumns()
        {
            
            return this._iSegmentTypeRepository.GetSegmentTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSegmentTypeAdvanceSearchColumns()
        {
            
            return this._iSegmentTypeRepository.GetSegmentTypeAdvanceSearchColumns();
           
        }
        

		public SegmentType GetSegmentType(System.Int32 SegmentTypeId)
		{
			return _iSegmentTypeRepository.GetSegmentType(SegmentTypeId);
		}

		public SegmentType UpdateSegmentType(SegmentType entity)
		{
			return _iSegmentTypeRepository.UpdateSegmentType(entity);
		}

		public bool DeleteSegmentType(System.Int32 SegmentTypeId)
		{
			return _iSegmentTypeRepository.DeleteSegmentType(SegmentTypeId);
		}

		public List<SegmentType> GetAllSegmentType()
		{
			return _iSegmentTypeRepository.GetAllSegmentType();
		}

		public SegmentType InsertSegmentType(SegmentType entity)
		{
			 return _iSegmentTypeRepository.InsertSegmentType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 segmenttypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out segmenttypeid))
            {
				SegmentType segmenttype = _iSegmentTypeRepository.GetSegmentType(segmenttypeid);
                if(segmenttype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(segmenttype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SegmentType> segmenttypelist = _iSegmentTypeRepository.GetAllSegmentType();
            if (segmenttypelist != null && segmenttypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(segmenttypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SegmentType segmenttype = new SegmentType();
                PostOutput output = new PostOutput();
                segmenttype.CopyFrom(Input);
                segmenttype = _iSegmentTypeRepository.InsertSegmentType(segmenttype);
                output.CopyFrom(segmenttype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SegmentType segmenttypeinput = new SegmentType();
                SegmentType segmenttypeoutput = new SegmentType();
                PutOutput output = new PutOutput();
                segmenttypeinput.CopyFrom(Input);
                SegmentType segmenttype = _iSegmentTypeRepository.GetSegmentType(segmenttypeinput.SegmentTypeId);
                if (segmenttype!=null)
                {
                    segmenttypeoutput = _iSegmentTypeRepository.UpdateSegmentType(segmenttypeinput);
                    if(segmenttypeoutput!=null)
                    {
                        output.CopyFrom(segmenttypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 segmenttypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out segmenttypeid))
            {
				 bool IsDeleted = _iSegmentTypeRepository.DeleteSegmentType(segmenttypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
