﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotHeadline;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotHeadlineService : ISlotHeadlineService 
	{
		private ISlotHeadlineRepository _iSlotHeadlineRepository;
        
		public SlotHeadlineService(ISlotHeadlineRepository iSlotHeadlineRepository)
		{
			this._iSlotHeadlineRepository = iSlotHeadlineRepository;
		}
        
        public Dictionary<string, string> GetSlotHeadlineBasicSearchColumns()
        {
            
            return this._iSlotHeadlineRepository.GetSlotHeadlineBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSlotHeadlineAdvanceSearchColumns()
        {
            
            return this._iSlotHeadlineRepository.GetSlotHeadlineAdvanceSearchColumns();
           
        }
        

		public virtual List<SlotHeadline> GetSlotHeadlineBySlotId(System.Int32 SlotId)
		{
			return _iSlotHeadlineRepository.GetSlotHeadlineBySlotId(SlotId);
		}

		public SlotHeadline GetSlotHeadline(System.Int32 SlotHeadlineId)
		{
			return _iSlotHeadlineRepository.GetSlotHeadline(SlotHeadlineId);
		}

		public SlotHeadline UpdateSlotHeadline(SlotHeadline entity)
		{
			return _iSlotHeadlineRepository.UpdateSlotHeadline(entity);
		}

		public bool DeleteSlotHeadline(System.Int32 SlotHeadlineId)
		{
			return _iSlotHeadlineRepository.DeleteSlotHeadline(SlotHeadlineId);
		}

		public List<SlotHeadline> GetAllSlotHeadline()
		{
			return _iSlotHeadlineRepository.GetAllSlotHeadline();
		}

		public SlotHeadline InsertSlotHeadline(SlotHeadline entity)
		{
			 return _iSlotHeadlineRepository.InsertSlotHeadline(entity);
		}

        public bool DeleteSlotHeadlineBySlotId(int slotid)
        {
            return _iSlotHeadlineRepository.DeleteSlotHeadlineBySlotId(slotid);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slotheadlineid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotheadlineid))
            {
				SlotHeadline slotheadline = _iSlotHeadlineRepository.GetSlotHeadline(slotheadlineid);
                if(slotheadline!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slotheadline);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotHeadline> slotheadlinelist = _iSlotHeadlineRepository.GetAllSlotHeadline();
            if (slotheadlinelist != null && slotheadlinelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slotheadlinelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotHeadline slotheadline = new SlotHeadline();
                PostOutput output = new PostOutput();
                slotheadline.CopyFrom(Input);
                slotheadline = _iSlotHeadlineRepository.InsertSlotHeadline(slotheadline);
                output.CopyFrom(slotheadline);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotHeadline slotheadlineinput = new SlotHeadline();
                SlotHeadline slotheadlineoutput = new SlotHeadline();
                PutOutput output = new PutOutput();
                slotheadlineinput.CopyFrom(Input);
                SlotHeadline slotheadline = _iSlotHeadlineRepository.GetSlotHeadline(slotheadlineinput.SlotHeadlineId);
                if (slotheadline!=null)
                {
                    slotheadlineoutput = _iSlotHeadlineRepository.UpdateSlotHeadline(slotheadlineinput);
                    if(slotheadlineoutput!=null)
                    {
                        output.CopyFrom(slotheadlineoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slotheadlineid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotheadlineid))
            {
				 bool IsDeleted = _iSlotHeadlineRepository.DeleteSlotHeadline(slotheadlineid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
