﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplateCamera;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotScreenTemplateCameraService : ISlotScreenTemplateCameraService 
	{
		private ISlotScreenTemplateCameraRepository _iSlotScreenTemplateCameraRepository;
        private ISegmentRepository _iSegmentRepository;
        private ISlotRepository _iSlotRepository;
        private ISlotScreenTemplateRepository _iSlotScreenTemplateRepository;

        public SlotScreenTemplateCameraService(ISlotScreenTemplateCameraRepository iSlotScreenTemplateCameraRepository, ISegmentRepository iSegmentRepository, ISlotRepository iSlotRepository, ISlotScreenTemplateRepository iSlotScreenTemplateRepository)
		{
			this._iSlotScreenTemplateCameraRepository = iSlotScreenTemplateCameraRepository;
            this._iSegmentRepository = iSegmentRepository;
            this._iSlotRepository = iSlotRepository;
            this._iSlotScreenTemplateRepository = iSlotScreenTemplateRepository;
		}
        
        public Dictionary<string, string> GetSlotScreenTemplateCameraBasicSearchColumns()
        {
            
            return this._iSlotScreenTemplateCameraRepository.GetSlotScreenTemplateCameraBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSlotScreenTemplateCameraAdvanceSearchColumns()
        {
            
            return this._iSlotScreenTemplateCameraRepository.GetSlotScreenTemplateCameraAdvanceSearchColumns();
           
        }
        

		public virtual List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
		{
			return _iSlotScreenTemplateCameraRepository.GetSlotScreenTemplateCameraBySlotScreenTemplateId(SlotScreenTemplateId);
		}

		public virtual List<SlotScreenTemplateCamera> GetSlotScreenTemplateCameraByCameraTypeId(System.Int32 CameraTypeId)
		{
			return _iSlotScreenTemplateCameraRepository.GetSlotScreenTemplateCameraByCameraTypeId(CameraTypeId);
		}

		public SlotScreenTemplateCamera GetSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId)
		{
			return _iSlotScreenTemplateCameraRepository.GetSlotScreenTemplateCamera(SlotScreenTemplateCameraId);
		}

		public SlotScreenTemplateCamera UpdateSlotScreenTemplateCamera(SlotScreenTemplateCamera entity)
		{
			return _iSlotScreenTemplateCameraRepository.UpdateSlotScreenTemplateCamera(entity);
		}

		public bool DeleteSlotScreenTemplateCamera(System.Int32 SlotScreenTemplateCameraId)
		{
			return _iSlotScreenTemplateCameraRepository.DeleteSlotScreenTemplateCamera(SlotScreenTemplateCameraId);
		}

        public bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotScreenTemplateCameraRepository.DeleteSlotScreenTemplateCameraBySlotScreenTemplateId(SlotScreenTemplateId);
        }

		public List<SlotScreenTemplateCamera> GetAllSlotScreenTemplateCamera()
		{
			return _iSlotScreenTemplateCameraRepository.GetAllSlotScreenTemplateCamera();
		}

		public SlotScreenTemplateCamera InsertSlotScreenTemplateCamera(SlotScreenTemplateCamera entity)
		{
			 return _iSlotScreenTemplateCameraRepository.InsertSlotScreenTemplateCamera(entity);
		}

        public List<SlotScreenTemplateCamera> InsertSlotScreenTemplateCameraByEpisodeId(int EpisodeId)
        {
            List<SlotScreenTemplateCamera> lstSlotScreenTempalteCamera = new List<SlotScreenTemplateCamera>();
            List<Segment> sg = new List<Segment>();
            sg.AddRange(_iSegmentRepository.GetSegmentByEpisodeId(EpisodeId));

            List<Slot> slots = new List<Slot>();
            foreach (Segment s in sg)
            {
                if(_iSlotRepository.GetSlotBySegmentId(Convert.ToInt32(s.SegmentId)) !=null)
                {
                slots.AddRange(_iSlotRepository.GetSlotBySegmentId(Convert.ToInt32(s.SegmentId)));
                }
            }

            List<SlotScreenTemplate> slotScreenTemplates = new List<SlotScreenTemplate>();
            if (slots != null && slots.Count > 0)
            {

                foreach (Slot slt in slots)
                {
                    if (_iSlotScreenTemplateRepository.GetSlotScreenTemplateBySlotId(Convert.ToInt32(slt.SlotId)) != null)
                    {
                        slotScreenTemplates.AddRange(_iSlotScreenTemplateRepository.GetSlotScreenTemplateBySlotId(Convert.ToInt32(slt.SlotId)));
                    }
                }
            }
            if (slotScreenTemplates != null && slotScreenTemplates.Count > 0)
            {
                foreach (SlotScreenTemplate sltScreenTemplate in slotScreenTemplates)
                {
                    for (int i = 0; i <= 4; i++)
                    {
                        SlotScreenTemplateCamera sSCamera = new SlotScreenTemplateCamera();
                        sSCamera.SlotScreenTemplateId = sltScreenTemplate.SlotScreenTemplateId;
                        sSCamera.IsOn = true;
                        sSCamera.Name = "Camera 1";
                        sSCamera.CameraTypeId = 1;
                        sSCamera.CreationDate = Convert.ToDateTime("2014-07-14 15:09:40.453");
                        sSCamera.LastUpdateDate = Convert.ToDateTime("2014-07-14 15:09:40.453");
                        sSCamera = _iSlotScreenTemplateCameraRepository.InsertSlotScreenTemplateCamera(sSCamera);
                        lstSlotScreenTempalteCamera.Add(sSCamera);
                    }
                }
            }
            return lstSlotScreenTempalteCamera;
        }



        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slotscreentemplatecameraid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplatecameraid))
            {
				SlotScreenTemplateCamera slotscreentemplatecamera = _iSlotScreenTemplateCameraRepository.GetSlotScreenTemplateCamera(slotscreentemplatecameraid);
                if(slotscreentemplatecamera!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slotscreentemplatecamera);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotScreenTemplateCamera> slotscreentemplatecameralist = _iSlotScreenTemplateCameraRepository.GetAllSlotScreenTemplateCamera();
            if (slotscreentemplatecameralist != null && slotscreentemplatecameralist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slotscreentemplatecameralist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotScreenTemplateCamera slotscreentemplatecamera = new SlotScreenTemplateCamera();
                PostOutput output = new PostOutput();
                slotscreentemplatecamera.CopyFrom(Input);
                slotscreentemplatecamera = _iSlotScreenTemplateCameraRepository.InsertSlotScreenTemplateCamera(slotscreentemplatecamera);
                output.CopyFrom(slotscreentemplatecamera);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotScreenTemplateCamera slotscreentemplatecamerainput = new SlotScreenTemplateCamera();
                SlotScreenTemplateCamera slotscreentemplatecameraoutput = new SlotScreenTemplateCamera();
                PutOutput output = new PutOutput();
                slotscreentemplatecamerainput.CopyFrom(Input);
                SlotScreenTemplateCamera slotscreentemplatecamera = _iSlotScreenTemplateCameraRepository.GetSlotScreenTemplateCamera(slotscreentemplatecamerainput.SlotScreenTemplateCameraId);
                if (slotscreentemplatecamera!=null)
                {
                    slotscreentemplatecameraoutput = _iSlotScreenTemplateCameraRepository.UpdateSlotScreenTemplateCamera(slotscreentemplatecamerainput);
                    if(slotscreentemplatecameraoutput!=null)
                    {
                        output.CopyFrom(slotscreentemplatecameraoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slotscreentemplatecameraid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplatecameraid))
            {
				 bool IsDeleted = _iSlotScreenTemplateCameraRepository.DeleteSlotScreenTemplateCamera(slotscreentemplatecameraid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
