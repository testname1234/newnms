﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplateMic;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotScreenTemplateMicService : ISlotScreenTemplateMicService 
	{
		private ISlotScreenTemplateMicRepository _iSlotScreenTemplateMicRepository;
        private ISegmentRepository _iSegmentRepository;
        private ISlotRepository _iSlotRepository;
        private ISlotScreenTemplateRepository _iSlotScreenTemplateRepository;
        public SlotScreenTemplateMicService(ISlotScreenTemplateMicRepository iSlotScreenTemplateMicRepository, ISegmentRepository iSegmentRepository, ISlotRepository iSlotRepository, ISlotScreenTemplateRepository iSlotScreenTemplateRepository)
		{
			this._iSlotScreenTemplateMicRepository = iSlotScreenTemplateMicRepository;
            this._iSegmentRepository = iSegmentRepository;
            this._iSlotRepository = iSlotRepository;
            this._iSlotScreenTemplateRepository = iSlotScreenTemplateRepository;
		}
        
        public Dictionary<string, string> GetSlotScreenTemplateMicBasicSearchColumns()
        {
            return this._iSlotScreenTemplateMicRepository.GetSlotScreenTemplateMicBasicSearchColumns();  
        }
        
        public List<SearchColumn> GetSlotScreenTemplateMicAdvanceSearchColumns()
        {
            
            return this._iSlotScreenTemplateMicRepository.GetSlotScreenTemplateMicAdvanceSearchColumns();
           
        }
        

		public virtual List<SlotScreenTemplateMic> GetSlotScreenTemplateMicBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
		{
			return _iSlotScreenTemplateMicRepository.GetSlotScreenTemplateMicBySlotScreenTemplateId(SlotScreenTemplateId);
		}

		public SlotScreenTemplateMic GetSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId)
		{
			return _iSlotScreenTemplateMicRepository.GetSlotScreenTemplateMic(SlotScreenTemplateMicId);
		}

		public SlotScreenTemplateMic UpdateSlotScreenTemplateMic(SlotScreenTemplateMic entity)
		{
			return _iSlotScreenTemplateMicRepository.UpdateSlotScreenTemplateMic(entity);
		}

		public bool DeleteSlotScreenTemplateMic(System.Int32 SlotScreenTemplateMicId)
		{
			return _iSlotScreenTemplateMicRepository.DeleteSlotScreenTemplateMic(SlotScreenTemplateMicId);
		}

        public bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotScreenTemplateMicRepository.DeleteSlotScreenTemplateMicBySlotScreenTemplateId(SlotScreenTemplateId);
        }

		public List<SlotScreenTemplateMic> GetAllSlotScreenTemplateMic()
		{
			return _iSlotScreenTemplateMicRepository.GetAllSlotScreenTemplateMic();
		}

		public SlotScreenTemplateMic InsertSlotScreenTemplateMic(SlotScreenTemplateMic entity)
		{
			 return _iSlotScreenTemplateMicRepository.InsertSlotScreenTemplateMic(entity);
		}


        public List<SlotScreenTemplateMic> InsertSlotScreenTemplateMicByEpisodeId(int EpisodeId)
        {
            List<SlotScreenTemplateMic> lstSlotScreenTempalteMics = new List<SlotScreenTemplateMic>();
            List<Segment> sg = new List<Segment>();
            sg=_iSegmentRepository.GetSegmentByEpisodeId(EpisodeId);

            List<Slot> slots=new List<Slot>();
            foreach (Segment s in sg)
            {
                if (_iSlotRepository.GetSlotBySegmentId(Convert.ToInt32(s.SegmentId)) != null)
                {
                    slots.AddRange(_iSlotRepository.GetSlotBySegmentId(Convert.ToInt32(s.SegmentId)));
                }
            }

            List<SlotScreenTemplate> slotScreenTemplates = new List<SlotScreenTemplate>();
            if (slots != null && slots.Count > 0)
            {
                
                foreach (Slot slt in slots)
                {
                    if (_iSlotScreenTemplateRepository.GetSlotScreenTemplateBySlotId(Convert.ToInt32(slt.SlotId)) !=null)
                    {
                        slotScreenTemplates.AddRange(_iSlotScreenTemplateRepository.GetSlotScreenTemplateBySlotId(Convert.ToInt32(slt.SlotId)));
                    }
                }
            }
            if (slotScreenTemplates != null && slotScreenTemplates.Count > 0)
            {
                foreach (SlotScreenTemplate sltScreenTemplate in slotScreenTemplates)
                {
                    for (int i = 0; i <= 4; i++)
                    {
                        SlotScreenTemplateMic sSMics = new SlotScreenTemplateMic();
                        sSMics.SlotScreenTemplateId = sltScreenTemplate.SlotScreenTemplateId;
                        sSMics.IsOn = true;
                        sSMics.MicNo = 1;
                        sSMics.CreationDate = Convert.ToDateTime("2014-07-14 15:09:40.453");
                        sSMics.LastUpdateDate = Convert.ToDateTime("2014-07-14 15:09:40.453");
                        sSMics = _iSlotScreenTemplateMicRepository.InsertSlotScreenTemplateMic(sSMics);
                        lstSlotScreenTempalteMics.Add(sSMics);
                    }
                }
            }
            return lstSlotScreenTempalteMics;
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slotscreentemplatemicid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplatemicid))
            {
				SlotScreenTemplateMic slotscreentemplatemic = _iSlotScreenTemplateMicRepository.GetSlotScreenTemplateMic(slotscreentemplatemicid);
                if(slotscreentemplatemic!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slotscreentemplatemic);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotScreenTemplateMic> slotscreentemplatemiclist = _iSlotScreenTemplateMicRepository.GetAllSlotScreenTemplateMic();
            if (slotscreentemplatemiclist != null && slotscreentemplatemiclist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slotscreentemplatemiclist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotScreenTemplateMic slotscreentemplatemic = new SlotScreenTemplateMic();
                PostOutput output = new PostOutput();
                slotscreentemplatemic.CopyFrom(Input);
                slotscreentemplatemic = _iSlotScreenTemplateMicRepository.InsertSlotScreenTemplateMic(slotscreentemplatemic);
                output.CopyFrom(slotscreentemplatemic);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotScreenTemplateMic slotscreentemplatemicinput = new SlotScreenTemplateMic();
                SlotScreenTemplateMic slotscreentemplatemicoutput = new SlotScreenTemplateMic();
                PutOutput output = new PutOutput();
                slotscreentemplatemicinput.CopyFrom(Input);
                SlotScreenTemplateMic slotscreentemplatemic = _iSlotScreenTemplateMicRepository.GetSlotScreenTemplateMic(slotscreentemplatemicinput.SlotScreenTemplateMicId);
                if (slotscreentemplatemic!=null)
                {
                    slotscreentemplatemicoutput = _iSlotScreenTemplateMicRepository.UpdateSlotScreenTemplateMic(slotscreentemplatemicinput);
                    if(slotscreentemplatemicoutput!=null)
                    {
                        output.CopyFrom(slotscreentemplatemicoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slotscreentemplatemicid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplatemicid))
            {
				 bool IsDeleted = _iSlotScreenTemplateMicRepository.DeleteSlotScreenTemplateMic(slotscreentemplatemicid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
