﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplateResource;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotScreenTemplateResourceService : ISlotScreenTemplateResourceService 
	{
		private ISlotScreenTemplateResourceRepository _iSlotScreenTemplateResourceRepository;
        
		public SlotScreenTemplateResourceService(ISlotScreenTemplateResourceRepository iSlotScreenTemplateResourceRepository)
		{
			this._iSlotScreenTemplateResourceRepository = iSlotScreenTemplateResourceRepository;
		}
        
        public Dictionary<string, string> GetSlotScreenTemplateResourceBasicSearchColumns()
        {
            
            return this._iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSlotScreenTemplateResourceAdvanceSearchColumns()
        {
            
            return this._iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceAdvanceSearchColumns();
           
        }
        

		public virtual List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
		{
			return _iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceBySlotScreenTemplateId(SlotScreenTemplateId);
		}

		public SlotScreenTemplateResource GetSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId)
		{
			return _iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResource(SlotScreenTemplateResourceId);
		}

        public List<SlotScreenTemplateResource> GetSlotScreenTemplateResourceByEpisode(System.Int32 EpisodeId)
        {
            return _iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByEpisode(EpisodeId);
        }

		public SlotScreenTemplateResource UpdateSlotScreenTemplateResource(SlotScreenTemplateResource entity)
		{
			return _iSlotScreenTemplateResourceRepository.UpdateSlotScreenTemplateResource(entity);
		}

		public bool DeleteSlotScreenTemplateResource(System.Int32 SlotScreenTemplateResourceId)
		{
			return _iSlotScreenTemplateResourceRepository.DeleteSlotScreenTemplateResource(SlotScreenTemplateResourceId);
		}

		public List<SlotScreenTemplateResource> GetAllSlotScreenTemplateResource()
		{
			return _iSlotScreenTemplateResourceRepository.GetAllSlotScreenTemplateResource();
		}

		public SlotScreenTemplateResource InsertSlotScreenTemplateResource(SlotScreenTemplateResource entity)
		{
			 return _iSlotScreenTemplateResourceRepository.InsertSlotScreenTemplateResource(entity);
		}

        	public SlotScreenTemplateResource GetSlotScreenTemplateResourceByResourceGuid(string Guid)
		{
			 return _iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByResourceGuid(Guid);
		}
       


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slotscreentemplateresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplateresourceid))
            {
				SlotScreenTemplateResource slotscreentemplateresource = _iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResource(slotscreentemplateresourceid);
                if(slotscreentemplateresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slotscreentemplateresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotScreenTemplateResource> slotscreentemplateresourcelist = _iSlotScreenTemplateResourceRepository.GetAllSlotScreenTemplateResource();
            if (slotscreentemplateresourcelist != null && slotscreentemplateresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slotscreentemplateresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotScreenTemplateResource slotscreentemplateresource = new SlotScreenTemplateResource();
                PostOutput output = new PostOutput();
                slotscreentemplateresource.CopyFrom(Input);
                slotscreentemplateresource = _iSlotScreenTemplateResourceRepository.InsertSlotScreenTemplateResource(slotscreentemplateresource);
                output.CopyFrom(slotscreentemplateresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotScreenTemplateResource slotscreentemplateresourceinput = new SlotScreenTemplateResource();
                SlotScreenTemplateResource slotscreentemplateresourceoutput = new SlotScreenTemplateResource();
                PutOutput output = new PutOutput();
                slotscreentemplateresourceinput.CopyFrom(Input);
                SlotScreenTemplateResource slotscreentemplateresource = _iSlotScreenTemplateResourceRepository.GetSlotScreenTemplateResource(slotscreentemplateresourceinput.SlotScreenTemplateResourceId);
                if (slotscreentemplateresource!=null)
                {
                    slotscreentemplateresourceoutput = _iSlotScreenTemplateResourceRepository.UpdateSlotScreenTemplateResource(slotscreentemplateresourceinput);
                    if(slotscreentemplateresourceoutput!=null)
                    {
                        output.CopyFrom(slotscreentemplateresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slotscreentemplateresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplateresourceid))
            {
				 bool IsDeleted = _iSlotScreenTemplateResourceRepository.DeleteSlotScreenTemplateResource(slotscreentemplateresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public void DeleteAllBySlotScreenTemplateId(int slotScreenTemplateId)
         {
             _iSlotScreenTemplateResourceRepository.DeleteAllBySlotScreenTemplateId(slotScreenTemplateId);
         }
    }
	
	
}
