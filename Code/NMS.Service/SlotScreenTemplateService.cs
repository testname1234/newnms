﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplate;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Enums;
using NMS.Repository;

namespace NMS.Service
{

    public class SlotScreenTemplateService : ISlotScreenTemplateService
    {
        private ISlotScreenTemplateRepository _iSlotScreenTemplateRepository;
        private INotificationService _iNotificationService;
        private IUserRoleService _iUserRoleService;
        private IUserManagementService _iUserManagementService;
        private ITeamRepository _iTeamRepository;
        private ISlotRepository _iSlotRepository;

        public SlotScreenTemplateService(ISlotScreenTemplateRepository iSlotScreenTemplateRepository, INotificationService iNotificationService, IUserManagementService iUserManagementService, ITeamRepository iTeamRepository, ISlotRepository iSlotRepository)
        {
            this._iSlotScreenTemplateRepository = iSlotScreenTemplateRepository;
            this._iNotificationService = iNotificationService;
            this._iUserManagementService = iUserManagementService;
            this._iTeamRepository = iTeamRepository;
            this._iSlotRepository = iSlotRepository;
        }

        public Dictionary<string, string> GetSlotScreenTemplateBasicSearchColumns()
        {

            return this._iSlotScreenTemplateRepository.GetSlotScreenTemplateBasicSearchColumns();

        }

        public List<SearchColumn> GetSlotScreenTemplateAdvanceSearchColumns()
        {

            return this._iSlotScreenTemplateRepository.GetSlotScreenTemplateAdvanceSearchColumns();

        }

        public virtual List<SlotScreenTemplate> GetSlotScreenTemplateBySlotId(System.Int32 SlotId)
        {
            return _iSlotScreenTemplateRepository.GetSlotScreenTemplateBySlotId(SlotId);
        }

        public virtual List<SlotScreenTemplate> GetByEpisodeId(System.Int32 episodeId)
        {
            return _iSlotScreenTemplateRepository.GetByEpisodeId(episodeId);
        }

        public virtual int GetProgramIdBySlotScreenTemplateId(System.Int32 slotScreenTemplateId)
        {
            return _iSlotScreenTemplateRepository.GetProgramIdBySlotScreenTemplateId(slotScreenTemplateId);
        }

        public virtual List<SlotScreenTemplate> GetSlotScreenTemplateByScreenTemplateId(System.Int32 ScreenTemplateId)
        {
            return _iSlotScreenTemplateRepository.GetSlotScreenTemplateByScreenTemplateId(ScreenTemplateId);
        }

        public virtual List<SlotScreenTemplate> GetSlotScreenTemplateBySlotAndScreenTemplateId(System.Int32 ScreenTemplateId, System.Int32 SlotId)
        {
            return _iSlotScreenTemplateRepository.GetSlotScreenTemplateBySlotAndScreenTemplateId(ScreenTemplateId, SlotId);
        }

        public SlotScreenTemplate GetSlotScreenTemplate(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotScreenTemplateRepository.GetSlotScreenTemplate(SlotScreenTemplateId);
        }

        public List<SlotScreenTemplate> GetSlotScreenTemplateByParentId(int parentId)
        {
            return _iSlotScreenTemplateRepository.GetSlotScreenTemplateByParentId(parentId);
        }

        public SlotScreenTemplate UpdateSlotScreenTemplate(SlotScreenTemplate entity)
        {
            return _iSlotScreenTemplateRepository.UpdateSlotScreenTemplate(entity);
        }

        public SlotScreenTemplate UpdateSlotScreenTemplateSequence(SlotScreenTemplate entity)
        {
            return _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateSequence(entity);
        }

        public SlotScreenTemplate UpdateSlotScreenTemplateSequenceAndScript(SlotScreenTemplate entity)
        {
            return _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateSequenceAndScript(entity);
        }

        public SlotScreenTemplate UpdateSlotScreenTemplateThumb(SlotScreenTemplate entity)
        {
            return _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateThumb(entity);
        }

        public bool DeleteSlotScreenTemplate(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotScreenTemplateRepository.DeleteSlotScreenTemplate(SlotScreenTemplateId);
        }

        public bool DeleteSlotScreenTemplatebySlotId(System.Int32 SloId)
        {
            return _iSlotScreenTemplateRepository.DeleteSlotScreenTemplatebySlotId(SloId);
        }

        public List<SlotScreenTemplate> GetAllSlotScreenTemplate()
        {
            return _iSlotScreenTemplateRepository.GetAllSlotScreenTemplate();
        }

        public SlotScreenTemplate InsertSlotScreenTemplate(SlotScreenTemplate entity)
        {
            return _iSlotScreenTemplateRepository.InsertSlotScreenTemplate(entity);
        }

        public void MarkNotifications(SlotScreenTemplate template, int userId, int senderRole, int programId, NotificationTypes notificationType)
        {
            List<User> users = _iUserManagementService.GetAllUsers();
            List<Team> programTeam = _iTeamRepository.GetTeamByProgramId(programId);
            User sender = users.Where(x => x.UserId == userId).FirstOrDefault();

            if (sender != null)
            {
                Dictionary<string, string> arguments = new Dictionary<string, string>() { 
                        { NotificationArguments.SlotId.ToString(), template.SlotId.ToString() },
                        { NotificationArguments.SlotScreenTemplateId.ToString(), template.SlotScreenTemplateId.ToString() }
                    };

                Slot slot = _iSlotRepository.GetSlot(template.SlotId);
                if (slot != null)
                {
                    arguments.Add(NotificationArguments.StorySlug.ToString(), "\"" + slot.Title + "\"");
                }

                if (senderRole == (int)TeamRoles.NLE || senderRole == (int)TeamRoles.StoryWriter)
                {
                    var producers = from t1 in users.AsParallel()
                                    join t2 in programTeam.AsParallel() on t1.UserId equals t2.UserId
                                    where t2.WorkRoleId == (int)TeamRoles.Producer || t2.WorkRoleId == (int)TeamRoles.HeadlineProducer
                                    select new
                                    {
                                        t1.UserId,
                                        t1.Name
                                    };

                    if (producers != null && producers.Count() > 0)
                    {
                        foreach (var producer in producers)
                        {
                            _iNotificationService.Notify(sender.UserId, sender.Name, producer.UserId, notificationType, template.SlotId, template.SlotScreenTemplateId, arguments);
                        }
                    }
                }
                else if (senderRole == (int)TeamRoles.Producer || senderRole == (int)TeamRoles.HeadlineProducer)
                {
                    if (template.IsAssignedToNle == true)
                    {
                        var nonLinearEditors = from t1 in users.AsParallel()
                                               join t2 in programTeam.AsParallel() on t1.UserId equals t2.UserId
                                               where t2.WorkRoleId == (int)TeamRoles.NLE
                                               select new
                                               {
                                                   t1.UserId,
                                                   t1.Name
                                               };

                        if (nonLinearEditors != null && nonLinearEditors.Count() > 0)
                        {
                            foreach (var nle in nonLinearEditors)
                            {
                                _iNotificationService.Notify(sender.UserId, sender.Name, nle.UserId, notificationType, template.SlotId, template.SlotScreenTemplateId, arguments);
                            }
                        }
                    }
                    if (template.IsAssignedToStoryWriter == true)
                    {
                        var storyWriters = from t1 in users.AsParallel()
                                           join t2 in programTeam.AsParallel() on t1.UserId equals t2.UserId
                                           where t2.WorkRoleId == (int)TeamRoles.StoryWriter
                                           select new
                                           {
                                               t1.UserId,
                                               t1.Name
                                           };

                        if (storyWriters != null && storyWriters.Count() > 0)
                        {
                            foreach (var storyWriter in storyWriters)
                            {
                                _iNotificationService.Notify(sender.UserId, sender.Name, storyWriter.UserId, notificationType, template.SlotId, template.SlotScreenTemplateId, arguments);
                            }
                        }
                    }
                }
            }
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slotscreentemplateid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out slotscreentemplateid))
            {
                SlotScreenTemplate slotscreentemplate = _iSlotScreenTemplateRepository.GetSlotScreenTemplate(slotscreentemplateid);
                if (slotscreentemplate != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slotscreentemplate);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotScreenTemplate> slotscreentemplatelist = _iSlotScreenTemplateRepository.GetAllSlotScreenTemplate();
            if (slotscreentemplatelist != null && slotscreentemplatelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slotscreentemplatelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotScreenTemplate slotscreentemplate = new SlotScreenTemplate();
                PostOutput output = new PostOutput();
                slotscreentemplate.CopyFrom(Input);
                slotscreentemplate = _iSlotScreenTemplateRepository.InsertSlotScreenTemplate(slotscreentemplate);
                output.CopyFrom(slotscreentemplate);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotScreenTemplate slotscreentemplateinput = new SlotScreenTemplate();
                SlotScreenTemplate slotscreentemplateoutput = new SlotScreenTemplate();
                PutOutput output = new PutOutput();
                slotscreentemplateinput.CopyFrom(Input);
                SlotScreenTemplate slotscreentemplate = _iSlotScreenTemplateRepository.GetSlotScreenTemplate(slotscreentemplateinput.SlotScreenTemplateId);
                if (slotscreentemplate != null)
                {
                    slotscreentemplateoutput = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplate(slotscreentemplateinput);
                    if (slotscreentemplateoutput != null)
                    {
                        output.CopyFrom(slotscreentemplateoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slotscreentemplateid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out slotscreentemplateid))
            {
                bool IsDeleted = _iSlotScreenTemplateRepository.DeleteSlotScreenTemplate(slotscreentemplateid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public void UpdateSlotScreenScript(int slotScreenTemplateId, string script)
        {
            _iSlotScreenTemplateRepository.UpdateSlotScreenScript(slotScreenTemplateId, script);
        }

        public bool UpdateSlotScreenTemplateStatus(int slotScreenTemplateId, int? statusId, int? nleStatusId, int? storyWriterStatusId, int userId, int senderRoleId, int programId)
        {
            SlotScreenTemplate slotScreenTemplate = GetSlotScreenTemplate(slotScreenTemplateId);
            SlotTemplateScreenElementRepository tempElementRepo = new SlotTemplateScreenElementRepository();
            List<SlotTemplateScreenElement> tempLst = new List<SlotTemplateScreenElement>();
            if (tempElementRepo.GetSlotTemplateScreenElementBySlotScreenTemplateId(slotScreenTemplateId) != null)
            {
                tempLst = tempElementRepo.GetSlotTemplateScreenElementBySlotScreenTemplateId(slotScreenTemplateId).ToList();
                if (tempLst.Count > 0)
                {
                    List<SlotTemplateScreenElement> temp = tempLst.Where(x => x.ScreenElementId == 3 || x.ScreenElementId == 13).ToList();
                    if (temp.Count <= 0)
                    {
                        nleStatusId = storyWriterStatusId;
                    }
                }
            }
            if (slotScreenTemplate != null)
            {
                if (nleStatusId.HasValue)
                {
                    if (nleStatusId == (int)ScreenTemplateStatuses.ApprovalPending && slotScreenTemplate.NleStatusId == (int)ScreenTemplateStatuses.InProcess)
                    {
                        var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, nleStatusId, null);
                        if (flag)
                        {
                            MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskSubmitted);
                        }
                    }
                    else if (nleStatusId == (int)ScreenTemplateStatuses.Approved && slotScreenTemplate.NleStatusId == (int)ScreenTemplateStatuses.ApprovalPending)
                    {
                        if (slotScreenTemplate.StoryWriterStatusId == (int)ScreenTemplateStatuses.Approved)
                        {
                            var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, (int)NotificationTypes.TaskApproved, nleStatusId, null);
                            if (flag)
                            {
                                MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskApproved);
                            }
                        }
                        else
                        {
                            var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, nleStatusId, null);
                            if (flag)
                            {
                                MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskApproved);
                            }
                        }
                    }
                    else if (nleStatusId == (int)ScreenTemplateStatuses.InProcess && slotScreenTemplate.NleStatusId == (int)ScreenTemplateStatuses.ApprovalPending)
                    {
                        var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, nleStatusId, null);
                        if (flag)
                        {
                            MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskSubmitted);
                        }
                    }

                    else if (nleStatusId == (int)ScreenTemplateStatuses.InProcess && slotScreenTemplate.NleStatusId == (int)ScreenTemplateStatuses.Approved)
                    {
                        var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, nleStatusId, null);
                        if (flag)
                        {
                            MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskSubmitted);
                        }
                    }
                    else if (nleStatusId == (int)ScreenTemplateStatuses.Approved && (senderRoleId == (int)TeamRoles.Producer || senderRoleId == (int)TeamRoles.HeadlineProducer))
                    {
                        if (slotScreenTemplate.StoryWriterStatusId == (int)ScreenTemplateStatuses.Approved)
                        {
                            _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, (int)ScreenTemplateStatuses.Approved, nleStatusId, null);
                        }
                        else
                        {
                            _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, nleStatusId, null);
                        }
                    }
                }

                if (storyWriterStatusId.HasValue)
                {
                    if (storyWriterStatusId == (int)ScreenTemplateStatuses.ApprovalPending && slotScreenTemplate.StoryWriterStatusId == (int)ScreenTemplateStatuses.InProcess)
                    {
                        var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, null, storyWriterStatusId);
                        if (flag)
                        {
                            MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskSubmitted);
                        }
                    }
                    else if (storyWriterStatusId == (int)ScreenTemplateStatuses.Approved && slotScreenTemplate.StoryWriterStatusId == (int)ScreenTemplateStatuses.ApprovalPending)
                    {
                        if (slotScreenTemplate.NleStatusId == (int)ScreenTemplateStatuses.Approved)
                        {
                            var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, (int)ScreenTemplateStatuses.Approved, null, storyWriterStatusId);
                            if (flag)
                            {
                                MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskApproved);
                            }
                        }
                        else
                        {
                            var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, null, storyWriterStatusId);
                            if (flag)
                            {
                                MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskSubmitted);
                            }
                        }
                    }
                    else if (storyWriterStatusId == (int)ScreenTemplateStatuses.InProcess && slotScreenTemplate.StoryWriterStatusId == (int)ScreenTemplateStatuses.ApprovalPending)
                    {
                        var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, null, storyWriterStatusId);
                        if (flag)
                        {
                            MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskSubmitted);
                        }
                    }

                    else if (storyWriterStatusId == (int)ScreenTemplateStatuses.InProcess && slotScreenTemplate.StoryWriterStatusId == (int)ScreenTemplateStatuses.Approved)
                    {
                        var flag = _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, null, storyWriterStatusId);
                        if (flag)
                        {
                            MarkNotifications(slotScreenTemplate, userId, senderRoleId, programId, NotificationTypes.TaskSubmitted);
                        }
                    }
                    else if (storyWriterStatusId == (int)ScreenTemplateStatuses.Approved && (senderRoleId == (int)TeamRoles.Producer || senderRoleId == (int)TeamRoles.HeadlineProducer))
                    {
                        if (slotScreenTemplate.NleStatusId == (int)ScreenTemplateStatuses.Approved)
                        {
                            _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, (int)ScreenTemplateStatuses.Approved, null, storyWriterStatusId);
                        }
                        else
                        {
                            _iSlotScreenTemplateRepository.UpdateSlotScreenTemplateStatus(slotScreenTemplate.SlotScreenTemplateId, null, null, storyWriterStatusId);
                        }
                    }
                }
            }

            return true;
        }
    }
}
