﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotScreenTemplatekey;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotScreenTemplatekeyService : ISlotScreenTemplatekeyService 
	{
		private ISlotScreenTemplatekeyRepository _iSlotScreenTemplatekeyRepository;
        
		public SlotScreenTemplatekeyService(ISlotScreenTemplatekeyRepository iSlotScreenTemplatekeyRepository)
		{
			this._iSlotScreenTemplatekeyRepository = iSlotScreenTemplatekeyRepository;
		}
        
        public Dictionary<string, string> GetSlotScreenTemplatekeyBasicSearchColumns()
        {
            
            return this._iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekeyBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSlotScreenTemplatekeyAdvanceSearchColumns()
        {
            
            return this._iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekeyAdvanceSearchColumns();
           
        }
        

		public virtual List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyByScreenTemplatekeyId(System.Int32? ScreenTemplatekeyId)
		{
			return _iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekeyByScreenTemplatekeyId(ScreenTemplatekeyId);
		}

        public virtual List<SlotScreenTemplatekey> GetSlotScreenTemplatekeyBySlotScreenTemplateId(System.Int32? SlotScreenTemplateId)
        {
            return _iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekeyBySlotScreenTemplateId(SlotScreenTemplateId);
        }

		public SlotScreenTemplatekey GetSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId)
		{
			return _iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekey(SlotScreenTemplatekeyId);
		}

		public SlotScreenTemplatekey UpdateSlotScreenTemplatekey(SlotScreenTemplatekey entity)
		{
			return _iSlotScreenTemplatekeyRepository.UpdateSlotScreenTemplatekey(entity);
		}

		public bool DeleteSlotScreenTemplatekey(System.Int32 SlotScreenTemplatekeyId)
		{
			return _iSlotScreenTemplatekeyRepository.DeleteSlotScreenTemplatekey(SlotScreenTemplatekeyId);
		}

        public bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotScreenTemplatekeyRepository.DeleteBySlotScreenTemplateId(SlotScreenTemplateId);
        }

		public List<SlotScreenTemplatekey> GetAllSlotScreenTemplatekey()
		{
			return _iSlotScreenTemplatekeyRepository.GetAllSlotScreenTemplatekey();
		}

       

		public SlotScreenTemplatekey InsertSlotScreenTemplatekey(SlotScreenTemplatekey entity)
		{
			 return _iSlotScreenTemplatekeyRepository.InsertSlotScreenTemplatekey(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slotscreentemplatekeyid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplatekeyid))
            {
				SlotScreenTemplatekey slotscreentemplatekey = _iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekey(slotscreentemplatekeyid);
                if(slotscreentemplatekey!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slotscreentemplatekey);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotScreenTemplatekey> slotscreentemplatekeylist = _iSlotScreenTemplatekeyRepository.GetAllSlotScreenTemplatekey();
            if (slotscreentemplatekeylist != null && slotscreentemplatekeylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slotscreentemplatekeylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotScreenTemplatekey slotscreentemplatekey = new SlotScreenTemplatekey();
                PostOutput output = new PostOutput();
                slotscreentemplatekey.CopyFrom(Input);
                slotscreentemplatekey = _iSlotScreenTemplatekeyRepository.InsertSlotScreenTemplatekey(slotscreentemplatekey);
                output.CopyFrom(slotscreentemplatekey);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotScreenTemplatekey slotscreentemplatekeyinput = new SlotScreenTemplatekey();
                SlotScreenTemplatekey slotscreentemplatekeyoutput = new SlotScreenTemplatekey();
                PutOutput output = new PutOutput();
                slotscreentemplatekeyinput.CopyFrom(Input);
                SlotScreenTemplatekey slotscreentemplatekey = _iSlotScreenTemplatekeyRepository.GetSlotScreenTemplatekey(slotscreentemplatekeyinput.SlotScreenTemplatekeyId);
                if (slotscreentemplatekey!=null)
                {
                    slotscreentemplatekeyoutput = _iSlotScreenTemplatekeyRepository.UpdateSlotScreenTemplatekey(slotscreentemplatekeyinput);
                    if(slotscreentemplatekeyoutput!=null)
                    {
                        output.CopyFrom(slotscreentemplatekeyoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slotscreentemplatekeyid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slotscreentemplatekeyid))
            {
				 bool IsDeleted = _iSlotScreenTemplatekeyRepository.DeleteSlotScreenTemplatekey(slotscreentemplatekeyid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
