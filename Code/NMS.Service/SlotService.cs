﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Slot;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Enums;

namespace NMS.Service
{

    public class SlotService : ISlotService
    {
        private ISlotRepository _iSlotRepository;
        private ISlotScreenTemplateRepository _iSlotScreenTemplateRepository;
        private ITemplateScreenElementRepository _iTemplateScreenElementRepository;
        private ISlotTemplateScreenElementRepository _iSlotTemplateScreenElementRepository;
        private IScreenTemplateRepository _iScreenTemplateRepository;

        public SlotService(ISlotRepository iSlotRepository, ISlotScreenTemplateRepository iSlotScreenTemplateRepository, ITemplateScreenElementRepository iTemplateScreenElementRepository, ISlotTemplateScreenElementRepository iSlotTemplateScreenElementRepository, IScreenTemplateRepository iScreenTemplateRepository)
        {
            this._iSlotRepository = iSlotRepository;
            this._iSlotScreenTemplateRepository = iSlotScreenTemplateRepository;
            this._iTemplateScreenElementRepository = iTemplateScreenElementRepository;
            this._iSlotTemplateScreenElementRepository = iSlotTemplateScreenElementRepository;
            this._iScreenTemplateRepository = iScreenTemplateRepository;
        }

        public Dictionary<string, string> GetSlotBasicSearchColumns()
        {

            return this._iSlotRepository.GetSlotBasicSearchColumns();

        }

        public List<SearchColumn> GetSlotAdvanceSearchColumns()
        {

            return this._iSlotRepository.GetSlotAdvanceSearchColumns();

        }


        public virtual List<Slot> GetSlotBySlotTypeId(System.Int32 SlotTypeId)
        {
            return _iSlotRepository.GetSlotBySlotTypeId(SlotTypeId);
        }


        public virtual List<Slot> GetSlotByEpisode(System.Int32 EpisodeId)
        {
            return _iSlotRepository.GetSlotByEpisode(EpisodeId);
        }

        public virtual List<Slot> GetSlotBySegmentId(System.Int32 SegmentId)
        {
            return _iSlotRepository.GetSlotBySegmentId(SegmentId);
        }

        public Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, System.String NewsGuid)
        {
            return _iSlotRepository.GetSlotByEpisodeAndNews(EpisodeId, NewsGuid);
        }
        public Slot GetSlotByEpisodeAndNewsFileId(System.Int32 EpisodeId, System.Int32 NewsFileId)
        {
            return _iSlotRepository.GetSlotByEpisodeAndNewsFileId(EpisodeId, NewsFileId);
        }

        

        public Slot GetSlotByEpisodeAndNews(System.Int32 EpisodeId, int TickerId)
        {
            return _iSlotRepository.GetSlotByEpisodeAndNews(EpisodeId, TickerId);
        }

        public virtual List<Slot> GetSlotByCategoryId(System.Int32 CategoryId)
        {
            return _iSlotRepository.GetSlotByCategoryId(CategoryId);
        }

        public Slot GetSlot(System.Int32 SlotId)
        {
            return _iSlotRepository.GetSlot(SlotId);
        }

        public Slot UpdateSlot(Slot entity)
        {
            return _iSlotRepository.UpdateSlot(entity);
        }

        public bool DeleteSlot(System.Int32 SlotId)
        {
            return _iSlotRepository.DeleteSlot(SlotId);
        }

        public List<Slot> GetAllSlot()
        {
            return _iSlotRepository.GetAllSlot();
        }

        public Slot InsertSlot(Slot entity)
        {
            return _iSlotRepository.InsertSlot(entity);
        }


        public Slot InsertSlotBySegment(Slot entity)
        {
            if (_iSlotRepository.GetSlotBySegmentAndNews(entity.SegmentId, entity.NewsGuid, entity.TickerId) == null)
            {
                entity.SlotId = 0;
                Slot slot = _iSlotRepository.InsertSlot(entity);

                return slot;
            }
            else
            {
                return _iSlotRepository.UpdateSlotSequence(entity);
            }
        }

        public List<Slot> GetSlotsByEpisodeId(int EpisodeId)
        {
           return this._iSlotRepository.GetSlotsByEpisodeId(EpisodeId);
        }

        public bool InsertDefaultTemplates(Slot slot, int programId)
        {
            List<ScreenTemplate> tempScreenTemplates = _iScreenTemplateRepository.GetScreenTemplateByProgramId(programId);

            if (slot != null && tempScreenTemplates != null && tempScreenTemplates.Count == 1)
            {
                var tempScreenTemplate = tempScreenTemplates.First();
                SlotScreenTemplate screenTemplate = new SlotScreenTemplate();

                screenTemplate.CopyFrom(tempScreenTemplate);

                screenTemplate.SlotId = slot.SlotId;
                screenTemplate.SequenceNumber = 1;
                screenTemplate.ScreenTemplateId = tempScreenTemplate.ScreenTemplateId;
                screenTemplate.CreatonDate = DateTime.UtcNow;
                screenTemplate.LastUpdateDate = DateTime.UtcNow;
                screenTemplate.IsActive = true;

                if (screenTemplate.Name == null) screenTemplate.Name = "";

                var screenTemplateEntity = _iSlotScreenTemplateRepository.InsertSlotScreenTemplate(screenTemplate);

                if (screenTemplateEntity != null)
                {
                    List<TemplateScreenElement> templateScreenElements = _iTemplateScreenElementRepository.GetTemplateScreenElementsByProgramId(programId).Where(x => x.ScreenTemplateId == screenTemplate.ScreenTemplateId).ToList();

                    screenTemplateEntity.SlotTemplateScreenElements = new List<SlotTemplateScreenElement>();

                    foreach (TemplateScreenElement templateScreenElement in templateScreenElements)
                    {
                        SlotTemplateScreenElement slotTemplateScreenElement = new SlotTemplateScreenElement();
                        slotTemplateScreenElement.CopyFrom(templateScreenElement);
                        slotTemplateScreenElement.SlotScreenTemplateId = screenTemplateEntity.SlotScreenTemplateId;
                        slotTemplateScreenElement.ResourceGuid = templateScreenElement.ImageGuid;
                        slotTemplateScreenElement.CreationDate = DateTime.UtcNow;
                        slotTemplateScreenElement.LastUpdateDate = DateTime.UtcNow;

                        screenTemplateEntity.SlotTemplateScreenElements.Add(_iSlotTemplateScreenElementRepository.InsertSlotTemplateScreenElement(slotTemplateScreenElement));
                    }

                    slot.SlotScreenTemplates = new List<SlotScreenTemplate>();
                    slot.SlotScreenTemplates.Add(screenTemplateEntity);

                    return true;
                }
            }

            return false;
        }


        public Slot UpdateSlotSequence(Slot entity)
        {
            return _iSlotRepository.UpdateSlotSequence(entity);

        }



        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slotid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out slotid))
            {
                Slot slot = _iSlotRepository.GetSlot(slotid);
                if (slot != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slot);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Slot> slotlist = _iSlotRepository.GetAllSlot();
            if (slotlist != null && slotlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slotlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Slot slot = new Slot();
                PostOutput output = new PostOutput();
                slot.CopyFrom(Input);
                slot = _iSlotRepository.InsertSlot(slot);
                output.CopyFrom(slot);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Slot slotinput = new Slot();
                Slot slotoutput = new Slot();
                PutOutput output = new PutOutput();
                slotinput.CopyFrom(Input);
                Slot slot = _iSlotRepository.GetSlot(slotinput.SlotId);
                if (slot != null)
                {
                    slotoutput = _iSlotRepository.UpdateSlot(slotinput);
                    if (slotoutput != null)
                    {
                        output.CopyFrom(slotoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slotid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out slotid))
            {
                bool IsDeleted = _iSlotRepository.DeleteSlot(slotid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public int GetEpisodeId(int slotId)
        {
            return _iSlotRepository.GetEpisodeId(slotId);
        }
    }


}
