﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotTemplateScreenElementResource;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotTemplateScreenElementResourceService : ISlotTemplateScreenElementResourceService 
	{
		private ISlotTemplateScreenElementResourceRepository _iSlotTemplateScreenElementResourceRepository;
        
		public SlotTemplateScreenElementResourceService(ISlotTemplateScreenElementResourceRepository iSlotTemplateScreenElementResourceRepository)
		{
			this._iSlotTemplateScreenElementResourceRepository = iSlotTemplateScreenElementResourceRepository;
		}
        
        public Dictionary<string, string> GetSlotTemplateScreenElementResourceBasicSearchColumns()
        {
            
            return this._iSlotTemplateScreenElementResourceRepository.GetSlotTemplateScreenElementResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSlotTemplateScreenElementResourceAdvanceSearchColumns()
        {
            
            return this._iSlotTemplateScreenElementResourceRepository.GetSlotTemplateScreenElementResourceAdvanceSearchColumns();
           
        }
        

		public virtual List<SlotTemplateScreenElementResource> GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(System.Int32? SlotTemplateScreenElementId)
		{
			return _iSlotTemplateScreenElementResourceRepository.GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(SlotTemplateScreenElementId);
		}

        public virtual List<SlotTemplateScreenElementResource> GetBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotTemplateScreenElementResourceRepository.GetBySlotScreenTemplateId(SlotScreenTemplateId);
        }

		public SlotTemplateScreenElementResource GetSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId)
		{
			return _iSlotTemplateScreenElementResourceRepository.GetSlotTemplateScreenElementResource(SlotTemplateScreenElementResourceId);
		}

		public SlotTemplateScreenElementResource UpdateSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity)
		{
			return _iSlotTemplateScreenElementResourceRepository.UpdateSlotTemplateScreenElementResource(entity);
		}

		public bool DeleteSlotTemplateScreenElementResource(System.Int32 SlotTemplateScreenElementResourceId)
		{
			return _iSlotTemplateScreenElementResourceRepository.DeleteSlotTemplateScreenElementResource(SlotTemplateScreenElementResourceId);
		}

        public bool DeleteBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotTemplateScreenElementResourceRepository.DeleteBySlotScreenTemplateId(SlotScreenTemplateId);
        }

        public bool DeleteBySlotTemplateScreenElementId(System.Int32 SlotTemplateScreenElementId)
        {
            return _iSlotTemplateScreenElementResourceRepository.DeleteBySlotTemplateScreenElementId(SlotTemplateScreenElementId);
        }

		public List<SlotTemplateScreenElementResource> GetAllSlotTemplateScreenElementResource()
		{
			return _iSlotTemplateScreenElementResourceRepository.GetAllSlotTemplateScreenElementResource();
		}

		public SlotTemplateScreenElementResource InsertSlotTemplateScreenElementResource(SlotTemplateScreenElementResource entity)
		{
			 return _iSlotTemplateScreenElementResourceRepository.InsertSlotTemplateScreenElementResource(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slottemplatescreenelementresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slottemplatescreenelementresourceid))
            {
				SlotTemplateScreenElementResource slottemplatescreenelementresource = _iSlotTemplateScreenElementResourceRepository.GetSlotTemplateScreenElementResource(slottemplatescreenelementresourceid);
                if(slottemplatescreenelementresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slottemplatescreenelementresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotTemplateScreenElementResource> slottemplatescreenelementresourcelist = _iSlotTemplateScreenElementResourceRepository.GetAllSlotTemplateScreenElementResource();
            if (slottemplatescreenelementresourcelist != null && slottemplatescreenelementresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slottemplatescreenelementresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotTemplateScreenElementResource slottemplatescreenelementresource = new SlotTemplateScreenElementResource();
                PostOutput output = new PostOutput();
                slottemplatescreenelementresource.CopyFrom(Input);
                slottemplatescreenelementresource = _iSlotTemplateScreenElementResourceRepository.InsertSlotTemplateScreenElementResource(slottemplatescreenelementresource);
                output.CopyFrom(slottemplatescreenelementresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotTemplateScreenElementResource slottemplatescreenelementresourceinput = new SlotTemplateScreenElementResource();
                SlotTemplateScreenElementResource slottemplatescreenelementresourceoutput = new SlotTemplateScreenElementResource();
                PutOutput output = new PutOutput();
                slottemplatescreenelementresourceinput.CopyFrom(Input);
                SlotTemplateScreenElementResource slottemplatescreenelementresource = _iSlotTemplateScreenElementResourceRepository.GetSlotTemplateScreenElementResource(slottemplatescreenelementresourceinput.SlotTemplateScreenElementResourceId);
                if (slottemplatescreenelementresource!=null)
                {
                    slottemplatescreenelementresourceoutput = _iSlotTemplateScreenElementResourceRepository.UpdateSlotTemplateScreenElementResource(slottemplatescreenelementresourceinput);
                    if(slottemplatescreenelementresourceoutput!=null)
                    {
                        output.CopyFrom(slottemplatescreenelementresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slottemplatescreenelementresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slottemplatescreenelementresourceid))
            {
				 bool IsDeleted = _iSlotTemplateScreenElementResourceRepository.DeleteSlotTemplateScreenElementResource(slottemplatescreenelementresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
