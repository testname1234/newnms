﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotTemplateScreenElement;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotTemplateScreenElementService : ISlotTemplateScreenElementService 
	{
		private ISlotTemplateScreenElementRepository _iSlotTemplateScreenElementRepository;
        
		public SlotTemplateScreenElementService(ISlotTemplateScreenElementRepository iSlotTemplateScreenElementRepository)
		{
			this._iSlotTemplateScreenElementRepository = iSlotTemplateScreenElementRepository;
		}
        
        public Dictionary<string, string> GetSlotTemplateScreenElementBasicSearchColumns()
        {
            
            return this._iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElementBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSlotTemplateScreenElementAdvanceSearchColumns()
        {
            
            return this._iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElementAdvanceSearchColumns();
           
        }
        

		public virtual List<SlotTemplateScreenElement> GetSlotTemplateScreenElementBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
		{
			return _iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElementBySlotScreenTemplateId(SlotScreenTemplateId);
		}

        public virtual List<SlotTemplateScreenElement> GetBySlotScreenTemplateIdWithFilter(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotTemplateScreenElementRepository.GetBySlotScreenTemplateIdWithFilter(SlotScreenTemplateId);
        }

		public SlotTemplateScreenElement GetSlotTemplateScreenElement(System.Int32 SlotTemplateScreenElementId)
		{
			return _iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElement(SlotTemplateScreenElementId);
		}

		public SlotTemplateScreenElement UpdateSlotTemplateScreenElement(SlotTemplateScreenElement entity)
		{
			return _iSlotTemplateScreenElementRepository.UpdateSlotTemplateScreenElement(entity);
		}

        public SlotTemplateScreenElement UpdateSlotTemplateScreenElementResourceGuid(int slotTemplateScreenElementId, Guid? resourceGuid)
        {
            return _iSlotTemplateScreenElementRepository.UpdateSlotTemplateScreenElementResourceGuid(slotTemplateScreenElementId, resourceGuid);
        }

        public SlotTemplateScreenElement UpdateSlotScreenTemplateElementCelebrity(int slotTemplateScreenElementId, int celebrityId)
        {
            return _iSlotTemplateScreenElementRepository.UpdateSlotScreenTemplateElementCelebrity(slotTemplateScreenElementId, celebrityId);
        }

		public bool DeleteSlotTemplateScreenElement(System.Int32 SlotTemplateScreenElementId)
		{
			return _iSlotTemplateScreenElementRepository.DeleteSlotTemplateScreenElement(SlotTemplateScreenElementId);
		}

        public bool DeleteSlotTemplateScreenElementBySlotScreenTemplateId(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotTemplateScreenElementRepository.DeleteSlotTemplateScreenElementBySlotScreenTemplateId(SlotScreenTemplateId);
        }

        public bool DeleteSlotTemplateScreenElementBySlotId(System.Int32 SlotScreenTemplateId)
        {
            return _iSlotTemplateScreenElementRepository.DeleteSlotTemplateScreenElementBySlotId(SlotScreenTemplateId);
        }  

		public List<SlotTemplateScreenElement> GetAllSlotTemplateScreenElement()
		{
			return _iSlotTemplateScreenElementRepository.GetAllSlotTemplateScreenElement();
		}

		public SlotTemplateScreenElement InsertSlotTemplateScreenElement(SlotTemplateScreenElement entity)
		{
			 return _iSlotTemplateScreenElementRepository.InsertSlotTemplateScreenElement(entity);
		}

        public List<SlotTemplateScreenElement> GetSlotTemplateScreenElementByEpisodeId(System.Int32 EpisodeId)
        {
            return _iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElementByEpisodeId(EpisodeId);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slottemplatescreenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slottemplatescreenelementid))
            {
				SlotTemplateScreenElement slottemplatescreenelement = _iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElement(slottemplatescreenelementid);
                if(slottemplatescreenelement!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slottemplatescreenelement);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotTemplateScreenElement> slottemplatescreenelementlist = _iSlotTemplateScreenElementRepository.GetAllSlotTemplateScreenElement();
            if (slottemplatescreenelementlist != null && slottemplatescreenelementlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slottemplatescreenelementlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotTemplateScreenElement slottemplatescreenelement = new SlotTemplateScreenElement();
                PostOutput output = new PostOutput();
                slottemplatescreenelement.CopyFrom(Input);
                slottemplatescreenelement = _iSlotTemplateScreenElementRepository.InsertSlotTemplateScreenElement(slottemplatescreenelement);
                output.CopyFrom(slottemplatescreenelement);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotTemplateScreenElement slottemplatescreenelementinput = new SlotTemplateScreenElement();
                SlotTemplateScreenElement slottemplatescreenelementoutput = new SlotTemplateScreenElement();
                PutOutput output = new PutOutput();
                slottemplatescreenelementinput.CopyFrom(Input);
                SlotTemplateScreenElement slottemplatescreenelement = _iSlotTemplateScreenElementRepository.GetSlotTemplateScreenElement(slottemplatescreenelementinput.SlotTemplateScreenElementId);
                if (slottemplatescreenelement!=null)
                {
                    slottemplatescreenelementoutput = _iSlotTemplateScreenElementRepository.UpdateSlotTemplateScreenElement(slottemplatescreenelementinput);
                    if(slottemplatescreenelementoutput!=null)
                    {
                        output.CopyFrom(slottemplatescreenelementoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slottemplatescreenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slottemplatescreenelementid))
            {
				 bool IsDeleted = _iSlotTemplateScreenElementRepository.DeleteSlotTemplateScreenElement(slottemplatescreenelementid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
