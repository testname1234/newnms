﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SlotType;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SlotTypeService : ISlotTypeService 
	{
		private ISlotTypeRepository _iSlotTypeRepository;
        
		public SlotTypeService(ISlotTypeRepository iSlotTypeRepository)
		{
			this._iSlotTypeRepository = iSlotTypeRepository;
		}
        
        public Dictionary<string, string> GetSlotTypeBasicSearchColumns()
        {
            
            return this._iSlotTypeRepository.GetSlotTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSlotTypeAdvanceSearchColumns()
        {
            
            return this._iSlotTypeRepository.GetSlotTypeAdvanceSearchColumns();
           
        }
        

		public SlotType GetSlotType(System.Int32 SlotTypeId)
		{
			return _iSlotTypeRepository.GetSlotType(SlotTypeId);
		}

		public SlotType UpdateSlotType(SlotType entity)
		{
			return _iSlotTypeRepository.UpdateSlotType(entity);
		}

		public bool DeleteSlotType(System.Int32 SlotTypeId)
		{
			return _iSlotTypeRepository.DeleteSlotType(SlotTypeId);
		}

		public List<SlotType> GetAllSlotType()
		{
			return _iSlotTypeRepository.GetAllSlotType();
		}

		public SlotType InsertSlotType(SlotType entity)
		{
			 return _iSlotTypeRepository.InsertSlotType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slottypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slottypeid))
            {
				SlotType slottype = _iSlotTypeRepository.GetSlotType(slottypeid);
                if(slottype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slottype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SlotType> slottypelist = _iSlotTypeRepository.GetAllSlotType();
            if (slottypelist != null && slottypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(slottypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SlotType slottype = new SlotType();
                PostOutput output = new PostOutput();
                slottype.CopyFrom(Input);
                slottype = _iSlotTypeRepository.InsertSlotType(slottype);
                output.CopyFrom(slottype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SlotType slottypeinput = new SlotType();
                SlotType slottypeoutput = new SlotType();
                PutOutput output = new PutOutput();
                slottypeinput.CopyFrom(Input);
                SlotType slottype = _iSlotTypeRepository.GetSlotType(slottypeinput.SlotTypeId);
                if (slottype!=null)
                {
                    slottypeoutput = _iSlotTypeRepository.UpdateSlotType(slottypeinput);
                    if(slottypeoutput!=null)
                    {
                        output.CopyFrom(slottypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slottypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out slottypeid))
            {
				 bool IsDeleted = _iSlotTypeRepository.DeleteSlotType(slottypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
