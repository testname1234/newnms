﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Slug;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{

    public class SlugService : ISlugService
    {
        private ISlugRepository _iSlugRepository;

        public SlugService(ISlugRepository iSlugRepository)
        {
            this._iSlugRepository = iSlugRepository;
        }

        public Dictionary<string, string> GetSlugBasicSearchColumns()
        {

            return this._iSlugRepository.GetSlugBasicSearchColumns();

        }

        public List<SearchColumn> GetSlugAdvanceSearchColumns()
        {

            return this._iSlugRepository.GetSlugAdvanceSearchColumns();

        }

        public Slug GetBySlug(System.String Slug)
        {
            return _iSlugRepository.GetBySlug(Slug);
        }

        public Slug InsertIfNotExists(Slug entity)
        {
            Slug slug = _iSlugRepository.GetBySlug(entity.Slug);
            if (slug == null)
            {
                slug = _iSlugRepository.InsertSlug(entity);
            }
            return slug;
        }

        public Slug GetSlug(System.Int32 SlugId)
        {
            return _iSlugRepository.GetSlug(SlugId);
        }

        public Slug UpdateSlug(Slug entity)
        {
            return _iSlugRepository.UpdateSlug(entity);
        }

        public bool DeleteSlug(System.Int32 SlugId)
        {
            return _iSlugRepository.DeleteSlug(SlugId);
        }

        public List<Slug> GetAllSlug()
        {
            return _iSlugRepository.GetAllSlug();
        }

        public Slug InsertSlug(Slug entity)
        {
            return _iSlugRepository.InsertSlug(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 slugid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out slugid))
            {
                Slug slug = _iSlugRepository.GetSlug(slugid);
                if (slug != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(slug);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Slug> sluglist = _iSlugRepository.GetAllSlug();
            if (sluglist != null && sluglist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(sluglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Slug slug = new Slug();
                PostOutput output = new PostOutput();
                slug.CopyFrom(Input);
                slug = _iSlugRepository.InsertSlug(slug);
                output.CopyFrom(slug);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Slug sluginput = new Slug();
                Slug slugoutput = new Slug();
                PutOutput output = new PutOutput();
                sluginput.CopyFrom(Input);
                Slug slug = _iSlugRepository.GetSlug(sluginput.SlugId);
                if (slug != null)
                {
                    slugoutput = _iSlugRepository.UpdateSlug(sluginput);
                    if (slugoutput != null)
                    {
                        output.CopyFrom(slugoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 slugid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out slugid))
            {
                bool IsDeleted = _iSlugRepository.DeleteSlug(slugid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
    }


}
