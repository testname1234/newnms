﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SocialMediaAccount;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SocialMediaAccountService : ISocialMediaAccountService 
	{
		private ISocialMediaAccountRepository _iSocialMediaAccountRepository;
        
		public SocialMediaAccountService(ISocialMediaAccountRepository iSocialMediaAccountRepository)
		{
			this._iSocialMediaAccountRepository = iSocialMediaAccountRepository;
		}
        
        public Dictionary<string, string> GetSocialMediaAccountBasicSearchColumns()
        {
            
            return this._iSocialMediaAccountRepository.GetSocialMediaAccountBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSocialMediaAccountAdvanceSearchColumns()
        {
            
            return this._iSocialMediaAccountRepository.GetSocialMediaAccountAdvanceSearchColumns();
           
        }

        public void UpdateTwitterAccountIsFollowed(int SocialMediaAccountId, Boolean IsFollowed)
        {
            this._iSocialMediaAccountRepository.UpdateTwitterAccountIsFollowed(SocialMediaAccountId, IsFollowed);
        }

		public virtual List<SocialMediaAccount> GetSocialMediaAccountByCelebrityId(System.Int32? CelebrityId)
		{
			return _iSocialMediaAccountRepository.GetSocialMediaAccountByCelebrityId(CelebrityId);
		}

        public virtual List<SocialMediaAccount> GetByIsFollowed(System.Boolean IsFollowed)
        {
            return _iSocialMediaAccountRepository.GetByIsFollowed(IsFollowed);
        }

        public virtual List<SocialMediaAccount> GetBySocialMediaType(System.Int32 SocialMediaType)
        {
            return _iSocialMediaAccountRepository.GetBySocialMediaType(SocialMediaType);
        }

		public SocialMediaAccount GetSocialMediaAccount(System.Int32 SocialMediaId)
		{
			return _iSocialMediaAccountRepository.GetSocialMediaAccount(SocialMediaId);
		}

		public SocialMediaAccount UpdateSocialMediaAccount(SocialMediaAccount entity)
		{
			return _iSocialMediaAccountRepository.UpdateSocialMediaAccount(entity);
		}

		public bool DeleteSocialMediaAccount(System.Int32 SocialMediaId)
		{
			return _iSocialMediaAccountRepository.DeleteSocialMediaAccount(SocialMediaId);
		}

		public List<SocialMediaAccount> GetAllSocialMediaAccount()
		{
			return _iSocialMediaAccountRepository.GetAllSocialMediaAccount();
		}

		public SocialMediaAccount InsertSocialMediaAccount(SocialMediaAccount entity)
		{
			 return _iSocialMediaAccountRepository.InsertSocialMediaAccount(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 socialmediaid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out socialmediaid))
            {
				SocialMediaAccount socialmediaaccount = _iSocialMediaAccountRepository.GetSocialMediaAccount(socialmediaid);
                if(socialmediaaccount!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(socialmediaaccount);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SocialMediaAccount> socialmediaaccountlist = _iSocialMediaAccountRepository.GetAllSocialMediaAccount();
            if (socialmediaaccountlist != null && socialmediaaccountlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(socialmediaaccountlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SocialMediaAccount socialmediaaccount = new SocialMediaAccount();
                PostOutput output = new PostOutput();
                socialmediaaccount.CopyFrom(Input);
                socialmediaaccount = _iSocialMediaAccountRepository.InsertSocialMediaAccount(socialmediaaccount);
                output.CopyFrom(socialmediaaccount);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SocialMediaAccount socialmediaaccountinput = new SocialMediaAccount();
                SocialMediaAccount socialmediaaccountoutput = new SocialMediaAccount();
                PutOutput output = new PutOutput();
                socialmediaaccountinput.CopyFrom(Input);
                SocialMediaAccount socialmediaaccount = _iSocialMediaAccountRepository.GetSocialMediaAccount(socialmediaaccountinput.SocialMediaAccountId);
                if (socialmediaaccount!=null)
                {
                    socialmediaaccountoutput = _iSocialMediaAccountRepository.UpdateSocialMediaAccount(socialmediaaccountinput);
                    if(socialmediaaccountoutput!=null)
                    {
                        output.CopyFrom(socialmediaaccountoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 socialmediaid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out socialmediaid))
            {
				 bool IsDeleted = _iSocialMediaAccountRepository.DeleteSocialMediaAccount(socialmediaid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
