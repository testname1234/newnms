﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SocialMedia;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class SocialMediaService : ISocialMediaService 
	{
		private ISocialMediaRepository _iSocialMediaRepository;
        
		public SocialMediaService(ISocialMediaRepository iSocialMediaRepository)
		{
			this._iSocialMediaRepository = iSocialMediaRepository;
		}
        
        public Dictionary<string, string> GetSocialMediaBasicSearchColumns()
        {
            
            return this._iSocialMediaRepository.GetSocialMediaBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSocialMediaAdvanceSearchColumns()
        {
            
            return this._iSocialMediaRepository.GetSocialMediaAdvanceSearchColumns();
           
        }
        

		public SocialMedia GetSocialMedia(System.Int32 SocialMediaId)
		{
			return _iSocialMediaRepository.GetSocialMedia(SocialMediaId);
		}

		public SocialMedia UpdateSocialMedia(SocialMedia entity)
		{
			return _iSocialMediaRepository.UpdateSocialMedia(entity);
		}

		public bool DeleteSocialMedia(System.Int32 SocialMediaId)
		{
			return _iSocialMediaRepository.DeleteSocialMedia(SocialMediaId);
		}

		public List<SocialMedia> GetAllSocialMedia()
		{
			return _iSocialMediaRepository.GetAllSocialMedia();
		}

		public SocialMedia InsertSocialMedia(SocialMedia entity)
		{
			 return _iSocialMediaRepository.InsertSocialMedia(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 socialmediaid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out socialmediaid))
            {
				SocialMedia socialmedia = _iSocialMediaRepository.GetSocialMedia(socialmediaid);
                if(socialmedia!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(socialmedia);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SocialMedia> socialmedialist = _iSocialMediaRepository.GetAllSocialMedia();
            if (socialmedialist != null && socialmedialist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(socialmedialist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SocialMedia socialmedia = new SocialMedia();
                PostOutput output = new PostOutput();
                socialmedia.CopyFrom(Input);
                socialmedia = _iSocialMediaRepository.InsertSocialMedia(socialmedia);
                output.CopyFrom(socialmedia);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SocialMedia socialmediainput = new SocialMedia();
                SocialMedia socialmediaoutput = new SocialMedia();
                PutOutput output = new PutOutput();
                socialmediainput.CopyFrom(Input);
                SocialMedia socialmedia = _iSocialMediaRepository.GetSocialMedia(socialmediainput.SocialMediaId);
                if (socialmedia!=null)
                {
                    socialmediaoutput = _iSocialMediaRepository.UpdateSocialMedia(socialmediainput);
                    if(socialmediaoutput!=null)
                    {
                        output.CopyFrom(socialmediaoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 socialmediaid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out socialmediaid))
            {
				 bool IsDeleted = _iSocialMediaRepository.DeleteSocialMedia(socialmediaid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
