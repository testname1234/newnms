﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.SuggestedFlow;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class SuggestedFlowService : ISuggestedFlowService 
	{
		private ISuggestedFlowRepository _iSuggestedFlowRepository;
        
		public SuggestedFlowService(ISuggestedFlowRepository iSuggestedFlowRepository)
		{
			this._iSuggestedFlowRepository = iSuggestedFlowRepository;
		}
        
        public Dictionary<string, string> GetSuggestedFlowBasicSearchColumns()
        {
            
            return this._iSuggestedFlowRepository.GetSuggestedFlowBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetSuggestedFlowAdvanceSearchColumns()
        {
            
            return this._iSuggestedFlowRepository.GetSuggestedFlowAdvanceSearchColumns();
           
        }
        

		public SuggestedFlow GetSuggestedFlow(System.Int32 SuggestedFlowId)
		{
			return _iSuggestedFlowRepository.GetSuggestedFlow(SuggestedFlowId);
		}

		public SuggestedFlow UpdateSuggestedFlow(SuggestedFlow entity)
		{
			return _iSuggestedFlowRepository.UpdateSuggestedFlow(entity);
		}

		public bool DeleteSuggestedFlow(System.Int32 SuggestedFlowId)
		{
			return _iSuggestedFlowRepository.DeleteSuggestedFlow(SuggestedFlowId);
		}

		public List<SuggestedFlow> GetAllSuggestedFlow()
		{
			return _iSuggestedFlowRepository.GetAllSuggestedFlow();
		}

		public SuggestedFlow InsertSuggestedFlow(SuggestedFlow entity)
		{
            List<SuggestedFlow> suggestedFlows = GetSuggestedFlowByKey(entity.Key);

            if (suggestedFlows == null || (suggestedFlows != null && suggestedFlows.Count == 0))
            {
                entity.CreationDate = DateTime.UtcNow;
                entity.LastUpdatedDate = DateTime.UtcNow;
                entity.IsActive = true;

                return _iSuggestedFlowRepository.InsertSuggestedFlow(entity);
            }
            else
            {
                return null;
            }
		}

        public List<SuggestedFlow> GetSuggestedFlowByKey(string key)
        {
            return _iSuggestedFlowRepository.GetSuggestedFlowByKey(key);
        }

        public List<SuggestedFlow> GetSuggestedFlowsLikeKey(string Key, int ProgramId)
        {
            return _iSuggestedFlowRepository.GetSuggestedFlowsLikeKey(Key, ProgramId);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 suggestedflowid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out suggestedflowid))
            {
				SuggestedFlow suggestedflow = _iSuggestedFlowRepository.GetSuggestedFlow(suggestedflowid);
                if(suggestedflow!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(suggestedflow);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<SuggestedFlow> suggestedflowlist = _iSuggestedFlowRepository.GetAllSuggestedFlow();
            if (suggestedflowlist != null && suggestedflowlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(suggestedflowlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                SuggestedFlow suggestedflow = new SuggestedFlow();
                PostOutput output = new PostOutput();
                suggestedflow.CopyFrom(Input);
                suggestedflow = _iSuggestedFlowRepository.InsertSuggestedFlow(suggestedflow);
                output.CopyFrom(suggestedflow);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                SuggestedFlow suggestedflowinput = new SuggestedFlow();
                SuggestedFlow suggestedflowoutput = new SuggestedFlow();
                PutOutput output = new PutOutput();
                suggestedflowinput.CopyFrom(Input);
                SuggestedFlow suggestedflow = _iSuggestedFlowRepository.GetSuggestedFlow(suggestedflowinput.SuggestedFlowId);
                if (suggestedflow!=null)
                {
                    suggestedflowoutput = _iSuggestedFlowRepository.UpdateSuggestedFlow(suggestedflowinput);
                    if(suggestedflowoutput!=null)
                    {
                        output.CopyFrom(suggestedflowoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 suggestedflowid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out suggestedflowid))
            {
				 bool IsDeleted = _iSuggestedFlowRepository.DeleteSuggestedFlow(suggestedflowid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
