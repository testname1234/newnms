﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TagResource;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.Entities.Mongo;
using NMS.Core.DataInterfaces.Mongo;

namespace NMS.Service
{
		
	public class TagResourceService : ITagResourceService 
	{
		private ITagResourceRepository _iTagResourceRepository;
        private IMTagResourceRepository _iMTagResourceRepository;
        
		public TagResourceService(ITagResourceRepository iTagResourceRepository,IMTagResourceRepository iMTagResourceRepository)
		{
			this._iTagResourceRepository = iTagResourceRepository;
            this._iMTagResourceRepository = iMTagResourceRepository;
		}
        
        public Dictionary<string, string> GetTagResourceBasicSearchColumns()
        {
            
            return this._iTagResourceRepository.GetTagResourceBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTagResourceAdvanceSearchColumns()
        {
            
            return this._iTagResourceRepository.GetTagResourceAdvanceSearchColumns();
           
        }
        

		public virtual List<TagResource> GetTagResourceByTagId(System.Int32? TagId)
		{
			return _iTagResourceRepository.GetTagResourceByTagId(TagId);
		}

		public virtual List<TagResource> GetTagResourceByResourceId(System.Int32? ResourceId)
		{
			return _iTagResourceRepository.GetTagResourceByResourceId(ResourceId);
		}

		public TagResource GetTagResource(System.Int32 TagResourceid)
		{
			return _iTagResourceRepository.GetTagResource(TagResourceid);
		}

		public TagResource UpdateTagResource(TagResource entity)
		{
			return _iTagResourceRepository.UpdateTagResource(entity);
		}

		public bool DeleteTagResource(System.Int32 TagResourceid)
		{
			return _iTagResourceRepository.DeleteTagResource(TagResourceid);
		}

		public List<TagResource> GetAllTagResource()
		{
			return _iTagResourceRepository.GetAllTagResource();
		}

		public TagResource InsertTagResource(TagResource entity)
		{
			 return _iTagResourceRepository.InsertTagResource(entity);
		}

        public bool InsertTagResource(MTagResource entity)
        {
            return _iMTagResourceRepository.InsertTagResource(entity);
        }

        public bool CheckIfTagResourceExists(string tagId)
        {
            return _iMTagResourceRepository.CheckIfTagResourceExists(tagId);
        }

        public bool DeleteByTagGuid(string tagId)
        {
            return _iMTagResourceRepository.DeleteByTagGuid(tagId);
        }

        
        
        public bool InsertTagResourceNoReturn(TagResource entity)
        {
            return _iTagResourceRepository.InsertTagResourceNoReturn(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tagresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tagresourceid))
            {
				TagResource tagresource = _iTagResourceRepository.GetTagResource(tagresourceid);
                if(tagresource!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tagresource);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TagResource> tagresourcelist = _iTagResourceRepository.GetAllTagResource();
            if (tagresourcelist != null && tagresourcelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tagresourcelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TagResource tagresource = new TagResource();
                PostOutput output = new PostOutput();
                tagresource.CopyFrom(Input);
                tagresource = _iTagResourceRepository.InsertTagResource(tagresource);
                output.CopyFrom(tagresource);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TagResource tagresourceinput = new TagResource();
                TagResource tagresourceoutput = new TagResource();
                PutOutput output = new PutOutput();
                tagresourceinput.CopyFrom(Input);
                TagResource tagresource = _iTagResourceRepository.GetTagResource(tagresourceinput.TagResourceid);
                if (tagresource!=null)
                {
                    tagresourceoutput = _iTagResourceRepository.UpdateTagResource(tagresourceinput);
                    if(tagresourceoutput!=null)
                    {
                        output.CopyFrom(tagresourceoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tagresourceid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tagresourceid))
            {
				 bool IsDeleted = _iTagResourceRepository.DeleteTagResource(tagresourceid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
