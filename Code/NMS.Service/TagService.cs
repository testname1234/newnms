﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Tag;
using Validation;
using System.Linq;
using NMS.Core;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.Entities.Mongo;

namespace NMS.Service
{

    public class TagService : ITagService
    {
        private ITagRepository _iTagRepository;
        private IMTagRepository _iMTagRepository;

        public TagService(ITagRepository iTagRepository, IMTagRepository iMTagRepository)
        {
            this._iTagRepository = iTagRepository;
            this._iMTagRepository = iMTagRepository;
        }

        public Dictionary<string, string> GetTagBasicSearchColumns()
        {

            return this._iTagRepository.GetTagBasicSearchColumns();

        }

        public List<SearchColumn> GetTagAdvanceSearchColumns()
        {

            return this._iTagRepository.GetTagAdvanceSearchColumns();

        }


        public Tag GetTag(System.Int32 TagId)
        {
            return _iTagRepository.GetTag(TagId);
        }

        public Tag GetByGuid(System.String Guid)
        {
            return _iTagRepository.GetByGuid(Guid);
        }

        public virtual Tag InsertIfNotExists(Tag tag)
        {
            Tag tagnew = _iTagRepository.GetByGuid(tag.Guid);
            if (tagnew == null)
            {
                tagnew = _iTagRepository.InsertTag(tag);
            }
            else
            {
                tagnew = _iTagRepository.UpdateTag(tag);
            }
            return tagnew;
        }

        public virtual void InsertIfNotExistsNoReturn(Tag tag)
        {
            if (_iTagRepository.GetByGuid(tag.Guid) == null)
            {
                _iTagRepository.InsertTagNoReturn(tag);
            }
        }



        public Tag UpdateTag(Tag entity)
        {
            return _iTagRepository.UpdateTag(entity);
        }

        public bool DeleteTag(System.Int32 TagId)
        {
            return _iTagRepository.DeleteTag(TagId);
        }

        public List<Tag> GetAllTag()
        {
            return _iTagRepository.GetAllTag();
        }

        public Tag InsertTag(Tag entity)
        {
            return _iTagRepository.InsertTag(entity);
        }

        public bool InsertTag(MTag entity)
        {
            return _iMTagRepository.InsertTag(entity);
        }

        public bool CheckIfTagExists(string tag)
        {
            return _iMTagRepository.CheckIfTagExists(tag);
        }

        public bool DeleteByTagGuid(string tagGuid)
        {
            return _iMTagRepository.DeleteByTagGuid(tagGuid);
        }



        public bool InsertTagNoReturn(Tag entity)
        {
            return _iTagRepository.InsertTagNoReturn(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tagid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tagid))
            {
                Tag tag = _iTagRepository.GetTag(tagid);
                if (tag != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tag);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Tag> taglist = _iTagRepository.GetAllTag();
            if (taglist != null && taglist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(taglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Tag tag = new Tag();
                PostOutput output = new PostOutput();
                tag.CopyFrom(Input);
                tag = _iTagRepository.InsertTag(tag);
                output.CopyFrom(tag);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Tag taginput = new Tag();
                Tag tagoutput = new Tag();
                PutOutput output = new PutOutput();
                taginput.CopyFrom(Input);
                Tag tag = _iTagRepository.GetTag(taginput.TagId);
                if (tag != null)
                {
                    tagoutput = _iTagRepository.UpdateTag(taginput);
                    if (tagoutput != null)
                    {
                        output.CopyFrom(tagoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tagid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tagid))
            {
                bool IsDeleted = _iTagRepository.DeleteTag(tagid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public List<Tag> GetTagByTerm(string term)
        {
            return _iTagRepository.GetTagByTerm(term);
        }



    }
	
	
}
