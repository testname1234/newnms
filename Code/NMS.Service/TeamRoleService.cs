﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TeamRole;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class TeamRoleService : ITeamRoleService 
	{
		private ITeamRoleRepository _iTeamRoleRepository;
        
		public TeamRoleService(ITeamRoleRepository iTeamRoleRepository)
		{
			this._iTeamRoleRepository = iTeamRoleRepository;
		}
        
        public Dictionary<string, string> GetTeamRoleBasicSearchColumns()
        {
            
            return this._iTeamRoleRepository.GetTeamRoleBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTeamRoleAdvanceSearchColumns()
        {
            
            return this._iTeamRoleRepository.GetTeamRoleAdvanceSearchColumns();
           
        }
        

		public TeamRole GetTeamRole(System.Int32 TeamRoleId)
		{
			return _iTeamRoleRepository.GetTeamRole(TeamRoleId);
		}

		public TeamRole UpdateTeamRole(TeamRole entity)
		{
			return _iTeamRoleRepository.UpdateTeamRole(entity);
		}

		public bool DeleteTeamRole(System.Int32 TeamRoleId)
		{
			return _iTeamRoleRepository.DeleteTeamRole(TeamRoleId);
		}

		public List<TeamRole> GetAllTeamRole()
		{
			return _iTeamRoleRepository.GetAllTeamRole();
		}

		public TeamRole InsertTeamRole(TeamRole entity)
		{
			 return _iTeamRoleRepository.InsertTeamRole(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 teamroleid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out teamroleid))
            {
				TeamRole teamrole = _iTeamRoleRepository.GetTeamRole(teamroleid);
                if(teamrole!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(teamrole);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TeamRole> teamrolelist = _iTeamRoleRepository.GetAllTeamRole();
            if (teamrolelist != null && teamrolelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(teamrolelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TeamRole teamrole = new TeamRole();
                PostOutput output = new PostOutput();
                teamrole.CopyFrom(Input);
                teamrole = _iTeamRoleRepository.InsertTeamRole(teamrole);
                output.CopyFrom(teamrole);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TeamRole teamroleinput = new TeamRole();
                TeamRole teamroleoutput = new TeamRole();
                PutOutput output = new PutOutput();
                teamroleinput.CopyFrom(Input);
                TeamRole teamrole = _iTeamRoleRepository.GetTeamRole(teamroleinput.TeamRoleId);
                if (teamrole!=null)
                {
                    teamroleoutput = _iTeamRoleRepository.UpdateTeamRole(teamroleinput);
                    if(teamroleoutput!=null)
                    {
                        output.CopyFrom(teamroleoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 teamroleid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out teamroleid))
            {
				 bool IsDeleted = _iTeamRoleRepository.DeleteTeamRole(teamroleid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
