﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Team;
using NMS.Core;
using Validation;
using System.Linq;

namespace NMS.Service
{

    public class TeamService : ITeamService
    {
        private ITeamRepository _iTeamRepository;

        public TeamService(ITeamRepository iTeamRepository)
        {
            this._iTeamRepository = iTeamRepository;
        }

        public Dictionary<string, string> GetTeamBasicSearchColumns()
        {

            return this._iTeamRepository.GetTeamBasicSearchColumns();

        }

        public List<SearchColumn> GetTeamAdvanceSearchColumns()
        {

            return this._iTeamRepository.GetTeamAdvanceSearchColumns();

        }


        public virtual List<Team> GetTeamByProgramId(System.Int32 ProgramId)
        {
            return _iTeamRepository.GetTeamByProgramId(ProgramId);
        }

        public virtual List<Team> GetTeamByTeamRoleId(System.Int32 TeamRoleId)
        {
            return _iTeamRepository.GetTeamByTeamRoleId(TeamRoleId);
        }

        public Team GetTeam(System.Int32 TeamId)
        {
            return _iTeamRepository.GetTeam(TeamId);
        }

        public Team UpdateTeam(Team entity)
        {
            return _iTeamRepository.UpdateTeam(entity);
        }

        public bool DeleteTeam(System.Int32 TeamId)
        {
            return _iTeamRepository.DeleteTeam(TeamId);
        }

        public List<Team> GetAllTeam()
        {
            return _iTeamRepository.GetAllTeam();
        }

        public Team InsertTeam(Team entity)
        {
            return _iTeamRepository.InsertTeam(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 teamid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out teamid))
            {
                Team team = _iTeamRepository.GetTeam(teamid);
                if (team != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(team);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Team> teamlist = _iTeamRepository.GetAllTeam();
            if (teamlist != null && teamlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(teamlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Team team = new Team();
                PostOutput output = new PostOutput();
                team.CopyFrom(Input);
                team = _iTeamRepository.InsertTeam(team);
                output.CopyFrom(team);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Team teaminput = new Team();
                Team teamoutput = new Team();
                PutOutput output = new PutOutput();
                teaminput.CopyFrom(Input);
                Team team = _iTeamRepository.GetTeam(teaminput.TeamId);
                if (team != null)
                {
                    teamoutput = _iTeamRepository.UpdateTeam(teaminput);
                    if (teamoutput != null)
                    {
                        output.CopyFrom(teamoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 teamid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out teamid))
            {
                bool IsDeleted = _iTeamRepository.DeleteTeam(teamid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public bool GetTeamByUserWorkProgramId(int userId, int workRoleId, int programId)
        {
            return _iTeamRepository.GetTeamByUserWorkProgramId(userId, workRoleId, programId);
        }

        public bool DeleteFromTeam(int userId, int workRoleId, int programId)
        {
            return _iTeamRepository.DeleteFromTeam(userId, workRoleId, programId);
        }

        public List<Team> GetTeamByUserId(int userId)
        {
            return _iTeamRepository.GetTeamByUserId(userId);
        }

        public string InsertTeam(PostInput Input)
        {
            string transfer = string.Empty;
            if (!GetTeamByUserWorkProgramId(Convert.ToInt32(Input.UserId), Convert.ToInt32(Input.WorkRoleId), Convert.ToInt32(Input.ProgramId)))
            {
                IList<string> errors = Validator.Validate(Input);
                if (errors.Count == 0)
                {
                    Team team = new Team();
                    team.CopyFrom(Input);
                    team.CreationDate = DateTime.UtcNow;
                    team.LastUpdateDate = DateTime.UtcNow;
                    team.IsActive = true;
                    team = _iTeamRepository.InsertTeam(team);
                    transfer = "Data Inserted";
                }
                else
                {
                    transfer = "Validation Failed";
                }
            }
            else
            {
                transfer = "Record Already Present";
            }
            return transfer;
        }
    }


}
