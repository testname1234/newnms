﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TemplateScreenElement;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class TemplateScreenElementService : ITemplateScreenElementService 
	{
        private IProgramScreenElementRepository _iProgramScreenElementRepository;
        private IScreenElementRepository _iScreenElementRepository;
		private ITemplateScreenElementRepository _iTemplateScreenElementRepository;

        public TemplateScreenElementService(ITemplateScreenElementRepository iTemplateScreenElementRepository, IProgramScreenElementRepository iProgramScreenElementRepository, IScreenElementRepository iScreenElementRepository)
		{
			this._iTemplateScreenElementRepository = iTemplateScreenElementRepository;
            this._iProgramScreenElementRepository = iProgramScreenElementRepository;
            this._iScreenElementRepository = iScreenElementRepository;
		}
        
        public Dictionary<string, string> GetTemplateScreenElementBasicSearchColumns()
        {
            
            return this._iTemplateScreenElementRepository.GetTemplateScreenElementBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTemplateScreenElementAdvanceSearchColumns()
        {
            
            return this._iTemplateScreenElementRepository.GetTemplateScreenElementAdvanceSearchColumns();
           
        }
        

		public TemplateScreenElement GetTemplateScreenElement(System.Int32 TemplateScreenElementId)
		{
			return _iTemplateScreenElementRepository.GetTemplateScreenElement(TemplateScreenElementId);
		}

		public TemplateScreenElement UpdateTemplateScreenElement(TemplateScreenElement entity)
		{
			return _iTemplateScreenElementRepository.UpdateTemplateScreenElement(entity);
		}

		public bool DeleteTemplateScreenElement(System.Int32 TemplateScreenElementId)
		{
			return _iTemplateScreenElementRepository.DeleteTemplateScreenElement(TemplateScreenElementId);
		}

		public List<TemplateScreenElement> GetAllTemplateScreenElement()
		{
			return _iTemplateScreenElementRepository.GetAllTemplateScreenElement();
		}

		public TemplateScreenElement InsertTemplateScreenElement(TemplateScreenElement entity)
		{
			 return _iTemplateScreenElementRepository.InsertTemplateScreenElement(entity);
		}
		
		public virtual List<TemplateScreenElement> GetTemplateScreenElementByScreenTemplateId(System.Int32? ScreenTemplateId)
		{
			return _iTemplateScreenElementRepository.GetTemplateScreenElementByScreenTemplateId(ScreenTemplateId);
		}

        public virtual List<TemplateScreenElement> GetTemplateScreenElementByScreenElementId(System.Int32 ScreenElementId)
        {
            return _iTemplateScreenElementRepository.GetTemplateScreenElementByScreenElementId(ScreenElementId);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 templatescreenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out templatescreenelementid))
            {
				TemplateScreenElement templatescreenelement = _iTemplateScreenElementRepository.GetTemplateScreenElement(templatescreenelementid);
                if(templatescreenelement!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(templatescreenelement);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TemplateScreenElement> templatescreenelementlist = _iTemplateScreenElementRepository.GetAllTemplateScreenElement();
            if (templatescreenelementlist != null && templatescreenelementlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(templatescreenelementlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TemplateScreenElement templatescreenelement = new TemplateScreenElement();
                PostOutput output = new PostOutput();
                templatescreenelement.CopyFrom(Input);
                templatescreenelement = _iTemplateScreenElementRepository.InsertTemplateScreenElement(templatescreenelement);
                output.CopyFrom(templatescreenelement);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TemplateScreenElement templatescreenelementinput = new TemplateScreenElement();
                TemplateScreenElement templatescreenelementoutput = new TemplateScreenElement();
                PutOutput output = new PutOutput();
                templatescreenelementinput.CopyFrom(Input);
                TemplateScreenElement templatescreenelement = _iTemplateScreenElementRepository.GetTemplateScreenElement(templatescreenelementinput.TemplateScreenElementId);
                if (templatescreenelement!=null)
                {
                    templatescreenelementoutput = _iTemplateScreenElementRepository.UpdateTemplateScreenElement(templatescreenelementinput);
                    if(templatescreenelementoutput!=null)
                    {
                        output.CopyFrom(templatescreenelementoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 templatescreenelementid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out templatescreenelementid))
            {
				 bool IsDeleted = _iTemplateScreenElementRepository.DeleteTemplateScreenElement(templatescreenelementid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<TemplateScreenElement> GetTemplateScreenElementsByProgramId(int programId)
         {
             List<ProgramScreenElement> programScreenElements = _iProgramScreenElementRepository.GetProgramScreenElementByProgramId(programId);
             List<ScreenElement> screenElements = _iScreenElementRepository.GetAllScreenElement();
             List<TemplateScreenElement> templateScreenElements = new List<TemplateScreenElement>();
             templateScreenElements = _iTemplateScreenElementRepository.GetTemplateScreenElementsByProgramId(programId);
             if (programScreenElements == null)
                 programScreenElements = new List<ProgramScreenElement>();
             if (templateScreenElements != null)
             {
                 foreach (var element in templateScreenElements)
                 {
                     var ele = programScreenElements.Where(x => x.ScreenElementId == element.ScreenElementId).FirstOrDefault();
                     if (ele != null)
                     {
                         element.ProgramScreenElementId = ele.ProgramScreenElementId;
                         element.ImageGuid = ele.ImageGuid;
                     }
                     else
                     {
                         element.ImageGuid = screenElements.Where(x => x.ScreenElementId == element.ScreenElementId).First().ImageGuid.Value;
                     }
                 }
             }
             return templateScreenElements;
         }
    }
	
	
}
