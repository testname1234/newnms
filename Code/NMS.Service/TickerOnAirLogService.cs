﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TickerOnAirLog;
using Validation;
using System.Linq;

namespace NMS.Service
{
		
	public class TickerOnAirLogService : ITickerOnAirLogService 
	{
		private ITickerOnAirLogRepository _iTickerOnAirLogRepository;
        
		public TickerOnAirLogService(ITickerOnAirLogRepository iTickerOnAirLogRepository)
		{
			this._iTickerOnAirLogRepository = iTickerOnAirLogRepository;
		}
        
        public Dictionary<string, string> GetTickerOnAirLogBasicSearchColumns()
        {
            
            return this._iTickerOnAirLogRepository.GetTickerOnAirLogBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTickerOnAirLogAdvanceSearchColumns()
        {
            
            return this._iTickerOnAirLogRepository.GetTickerOnAirLogAdvanceSearchColumns();
           
        }
        

		public TickerOnAirLog GetTickerOnAirLog(System.Int32 TickerOnAirLogId)
		{
			return _iTickerOnAirLogRepository.GetTickerOnAirLog(TickerOnAirLogId);
		}

		public TickerOnAirLog UpdateTickerOnAirLog(TickerOnAirLog entity)
		{
			return _iTickerOnAirLogRepository.UpdateTickerOnAirLog(entity);
		}

		public bool DeleteTickerOnAirLog(System.Int32 TickerOnAirLogId)
		{
			return _iTickerOnAirLogRepository.DeleteTickerOnAirLog(TickerOnAirLogId);
		}

		public List<TickerOnAirLog> GetAllTickerOnAirLog()
		{
			return _iTickerOnAirLogRepository.GetAllTickerOnAirLog();
		}

		public TickerOnAirLog InsertTickerOnAirLog(TickerOnAirLog entity)
		{
			 return _iTickerOnAirLogRepository.InsertTickerOnAirLog(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickeronairlogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickeronairlogid))
            {
				TickerOnAirLog tickeronairlog = _iTickerOnAirLogRepository.GetTickerOnAirLog(tickeronairlogid);
                if(tickeronairlog!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tickeronairlog);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TickerOnAirLog> tickeronairloglist = _iTickerOnAirLogRepository.GetAllTickerOnAirLog();
            if (tickeronairloglist != null && tickeronairloglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickeronairloglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TickerOnAirLog tickeronairlog = new TickerOnAirLog();
                PostOutput output = new PostOutput();
                tickeronairlog.CopyFrom(Input);
                tickeronairlog = _iTickerOnAirLogRepository.InsertTickerOnAirLog(tickeronairlog);
                output.CopyFrom(tickeronairlog);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerOnAirLog tickeronairloginput = new TickerOnAirLog();
                TickerOnAirLog tickeronairlogoutput = new TickerOnAirLog();
                PutOutput output = new PutOutput();
                tickeronairloginput.CopyFrom(Input);
                TickerOnAirLog tickeronairlog = _iTickerOnAirLogRepository.GetTickerOnAirLog(tickeronairloginput.TickerOnAirLogId);
                if (tickeronairlog!=null)
                {
                    tickeronairlogoutput = _iTickerOnAirLogRepository.UpdateTickerOnAirLog(tickeronairloginput);
                    if(tickeronairlogoutput!=null)
                    {
                        output.CopyFrom(tickeronairlogoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickeronairlogid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickeronairlogid))
            {
				 bool IsDeleted = _iTickerOnAirLogRepository.DeleteTickerOnAirLog(tickeronairlogid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
