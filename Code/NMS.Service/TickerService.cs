﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Ticker;
using NMS.Core;
using Validation;
using System.Linq;
using NMS.Core.Enums;
using NMS.Integration;
using NMS.Integration.MCRTicker;
using System.Configuration;
using System.Web.Script.Serialization;

namespace NMS.Service
{

    public class TickerService : ITickerService
    {
        private ITickerRepository _iTickerRepository;
        private ITickerLineRepository _iTickerLineRepository;
        private ITickerCategoryRepository _iTickerCategoryRepository;
        private ICategoryRepository _iCategoryRepository;
        private ILocationRepository _iLocationRepository;
        private INewsTagRepository _iNewsTagRepository;
        private INewsFileOrganizationRepository _iNewsFileOrganizationRepository;
        private INewsFileRepository _iNewsFileRepository;
        private IKeyValueRepository _iKeyValueRepository;
        private ITagRepository _iTagRepository;
        private IOrganizationRepository _iOrganizationRepository;
        private IMcrTickerBroadcastedRepository _iMcrTickerBroadcastedRepository;
        private IMcrTickerHistoryRepository _iMcrTickerHistoryRepository;
        private IMcrTickerHistoryLogRepository _iMcrTickerHistoryLogRepository;
        private ITickerTranslationRepository _iTickerTranslationRepository;

        public TickerService(ITickerRepository iTickerRepository, ITickerLineRepository iTickerLineRepository, ITickerCategoryRepository iTickerCategoryRepository,
            ICategoryRepository iCategoryRepository, ILocationRepository iLocationRepository, INewsTagRepository iNewsTagRepository,
            INewsFileOrganizationRepository iNewsFileOrganizationRepository, INewsFileRepository iNewsFileRepository, IKeyValueRepository iKeyValueRepository,
            ITagRepository iTagRepository, IOrganizationRepository iOrganizationRepository, IMcrTickerBroadcastedRepository iMcrTickerBroadcastedRepository,
            IMcrTickerHistoryRepository iMcrTickerHistoryRepository, IMcrTickerHistoryLogRepository iMcrTickerHistoryLogRepository,
            ITickerTranslationRepository iTickerTranslationRepository)
        {
            this._iTickerRepository = iTickerRepository;
            this._iTickerLineRepository = iTickerLineRepository;
            this._iTickerCategoryRepository = iTickerCategoryRepository;
            this._iCategoryRepository = iCategoryRepository;
            this._iLocationRepository = iLocationRepository;
            this._iNewsTagRepository = iNewsTagRepository;
            this._iNewsFileOrganizationRepository = iNewsFileOrganizationRepository;
            this._iNewsFileRepository = iNewsFileRepository;
            this._iKeyValueRepository = iKeyValueRepository;
            this._iTagRepository = iTagRepository;
            this._iOrganizationRepository = iOrganizationRepository;
            this._iMcrTickerBroadcastedRepository = iMcrTickerBroadcastedRepository;
            this._iMcrTickerHistoryRepository = iMcrTickerHistoryRepository;
            this._iMcrTickerHistoryLogRepository = iMcrTickerHistoryLogRepository;
            this._iTickerTranslationRepository = iTickerTranslationRepository;
        }

        public Dictionary<string, string> GetTickerBasicSearchColumns()
        {

            return this._iTickerRepository.GetTickerBasicSearchColumns();

        }

        public List<SearchColumn> GetTickerAdvanceSearchColumns()
        {

            return this._iTickerRepository.GetTickerAdvanceSearchColumns();

        }


        public Ticker GetTicker(System.Int32 TickerId)
        {
            return _iTickerRepository.GetTicker(TickerId);
        }


        public Ticker UpdateTicker(Ticker entity)
        {
            return _iTickerRepository.UpdateTicker(entity);
        }

        public bool DeleteTicker(System.Int32 TickerId)
        {
            var ticker = GetTicker(TickerId);
            ticker.IsActive = false;
            ticker.LastUpdatedDate = DateTime.UtcNow;

            ticker = _iTickerRepository.UpdateTicker(ticker);
            return ticker.IsActive == false;
        }

        public List<Ticker> GetAllTicker()
        {
            return _iTickerRepository.GetAllTicker();
        }

        public Ticker InsertTicker(Ticker entity)
        {
            if (entity.CategoryId.HasValue)
            {
                Category category = _iCategoryRepository.GetCategory(entity.CategoryId.Value);

                if (category != null)
                {
                    bool isCategoryInserted = false;
                    if (category.ParentId.HasValue)
                    {
                        isCategoryInserted = _iTickerCategoryRepository.InsertTickerCategoryIfNotExist(category.ParentId.Value);
                        if (isCategoryInserted)
                            entity.CategoryId = category.ParentId.Value;
                    }
                    else
                    {
                        isCategoryInserted = _iTickerCategoryRepository.InsertTickerCategoryIfNotExist(category.CategoryId);
                        if (isCategoryInserted)
                            entity.CategoryId = category.CategoryId;
                    }
                }
            }
            entity.CreationDate = DateTime.UtcNow;
            entity.LastUpdatedDate = DateTime.UtcNow;
            entity.IsActive = true;
            CalculateTickerCreateDuration(entity);
            return _iTickerRepository.InsertTicker(entity);
        }

        public List<Ticker> GetTickers(string searchTerm, DateTime fromDate, DateTime toDate, int pageCount = 20, int pageIndex = 0, int? userId = null)
        {
            return _iTickerRepository.GetTickers(searchTerm, fromDate, toDate, pageCount, pageIndex, userId);
        }

        public List<Ticker> GetTickers(int? userId, DateTime? lastUpdateDate)
        {
            List<Ticker> tickers = _iTickerRepository.GetTickers(userId, lastUpdateDate);
            if (tickers != null)
            {
                foreach (var item in tickers)
                {
                    CalculateTickerCreateDuration(item);
                }
            }
            return tickers;
        }
        void CalculateTickerCreateDuration(Ticker ticker)
        {
            var currentDate = DateTime.UtcNow;
            TimeSpan ts = new TimeSpan();
            ts = currentDate - ticker.CreationDate.Value;
            ticker.CreatedDuration = ts.Minutes;
        }
        public List<Ticker> GetTickers(TickerStatuses tickerStatus, DateTime? lastUpdateDate)
        {
            List<Ticker> tickers = _iTickerRepository.GetTickers(tickerStatus, lastUpdateDate);
            FillTickers(tickers);
            return tickers;
        }

        public List<Ticker> GetTickersByTickerOnAirLog(DateTime? lastUpdateDate = null)
        {
            List<Ticker> tickers = _iTickerRepository.GetTickersByTickerOnAirLog(lastUpdateDate);
            FillTickers(tickers);
            return tickers;
        }

        public Ticker UpdateTickerStatus(int tickerId, TickerStatuses status, int? tickerTypeId)
        {
            Ticker ticker = GetTicker(tickerId);
            if (ticker != null)
            {
                return _iTickerRepository.UpdateTickerStatus(ticker, status, tickerTypeId);
            }
            return null;
        }

        public bool UpdateTicker(int tickerId, int severity, int repeatCount, int frequency)
        {
            return _iTickerRepository.UpdateTicker(tickerId, severity, repeatCount, frequency);
        }

        public bool SubmitTickersToMcrCategory(Ticker ticker, int seqId)
        {
            return _iTickerRepository.SubmitTickersToMcrCategory(ticker.TickerId, ticker.CategoryId.Value, seqId);
        }

        public bool SubmitTickersToMcrBreaking(Ticker ticker, int seqId)
        {
            return _iTickerRepository.SubmitTickersToMcrBreaking(ticker.TickerId, seqId);
        }

        public bool SubmitTickersToMcrLatest(Ticker ticker, int seqId)
        {
            return _iTickerRepository.SubmitTickersToMcrLatest(ticker.TickerId, seqId);
        }

        public bool FlushMcrTickerData(bool category = false, bool breaking = false, bool latest = false)
        {
            var flag = false;
            if (category)
                flag = _iTickerRepository.FlushMcrCategoryTickerData();
            if (breaking)
                flag = _iTickerRepository.FlushMcrBreakingTickerData();
            if (latest)
                flag = _iTickerRepository.FlushMcrLatestTickerData();

            return flag;
        }
        public bool DeleteMcrTickerData(int tickerId, int tickerTypeId)
        {
            return _iTickerRepository.DeleteMcrTickerData(tickerId, tickerTypeId);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickerid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickerid))
            {
                Ticker ticker = _iTickerRepository.GetTicker(tickerid);
                if (ticker != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(ticker);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Ticker> tickerlist = _iTickerRepository.GetAllTicker();
            if (tickerlist != null && tickerlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickerlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Ticker ticker = new Ticker();
                PostOutput output = new PostOutput();
                ticker.CopyFrom(Input);
                ticker = _iTickerRepository.InsertTicker(ticker);
                output.CopyFrom(ticker);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Ticker tickerinput = new Ticker();
                Ticker tickeroutput = new Ticker();
                PutOutput output = new PutOutput();
                tickerinput.CopyFrom(Input);
                Ticker ticker = _iTickerRepository.GetTicker(tickerinput.TickerId);
                if (ticker != null)
                {
                    tickeroutput = _iTickerRepository.UpdateTicker(tickerinput);
                    if (tickeroutput != null)
                    {
                        output.CopyFrom(tickeroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickerid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickerid))
            {
                bool IsDeleted = _iTickerRepository.DeleteTicker(tickerid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


        public bool UpdateTickerLastUpdateDate(DateTime? date, int? tickerId)
        {
            return _iTickerRepository.UpdateTickerLastUpdateDate(date, tickerId);
        }

        public void FillTickers(List<Ticker> tickers)
        {
            if (tickers != null && tickers.Count > 0)
            {
                foreach (Ticker ticker in tickers)
                {
                    FillTicker(ticker);
                }
            }
        }

        public void FillTicker(Ticker ticker)
        {
            ticker.TickerLines = new List<TickerLine>();
            ticker.TickerLines = _iTickerLineRepository.GetTickerLineByTickerId(ticker.TickerId);
        }

        public Ticker GetFilledTicker(int tickerId)
        {
            Ticker ticker = _iTickerRepository.GetTicker(tickerId);
            if (ticker != null)
            {
                FillTicker(ticker);
            }
            return ticker;
        }

        public List<Ticker> GetTickerByLastUpdateDate(DateTime dateTime)
        {
            List<Ticker> tickers = _iTickerRepository.GetTickerByLastUpdateDate(dateTime);
            if (tickers != null)
            {
                //tickers = tickers.Where(i => i.CategoryStatusId != (int)TickerStatuses.Pending).ToList();
            }
            FillTickers(tickers);
            return tickers;
        }


        public List<Ticker> GetFilteredTickers(List<TickerStatus> discardedStatus)
        {
            List<Ticker> tickers = _iTickerRepository.GetFilteredTickers(discardedStatus);
            FillTickers(tickers);
            return tickers;
        }


        public bool UpdateMcrTickerData(int TickerId, int TickerLineId, int TickerTypeId, bool isFromOnAir)
        {
            return _iTickerRepository.UpdateMcrTickerData(TickerId, TickerLineId, TickerTypeId, isFromOnAir);
        }


        public Ticker GetTickerLastRecord()
        {
            return _iTickerRepository.GetTickerLastRecord();
        }


        public bool UpdateTickerSequence(Ticker ticker, int Type)
        {
            return _iTickerRepository.UpdateTickerSequence(ticker, Type);
        }

        public Ticker GetTickerByNewsGuid(string NewsGuid)
        {
            return _iTickerRepository.GetTickerByNewsGuid(NewsGuid);
        }



        public List<Ticker> GetTickerByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
        {
            return _iTickerRepository.GetTickerByKeyValue(Key, Value, operand, SelectClause);
        }

        public Ticker InsertTicker(PostInput ticker, int userRoleId, DateTime dtNow)
        {
            INewsService _iNewsService = IoC.Resolve<INewsService>("NewsService");
            Dictionary<string, int> seqNumbers = _iTickerLineRepository.GetSequenceNumbers();
            TickerLine lastTicker = new TickerLine();

            if (ticker.TickerLines != null && ticker.TickerLines.Count > 0 && ticker.TickerLines.Where(x => !string.IsNullOrEmpty(x.Text)).Count() > 0)
            {
                Ticker entity = new Ticker();
                entity.CopyFrom(ticker);
                entity.CreationDate = dtNow;
                entity.LastUpdatedDate = dtNow;
                entity.IsActive = true;
                entity = InsertTicker(entity);

                if (!string.IsNullOrEmpty(entity.NewsGuid))
                {
                    _iNewsService.UpdateNewsStatistics(null, entity.NewsGuid, NewsStatisticType.NewsTickerCount);
                }

                if (entity != null)
                {
                    entity.TickerLines = new List<TickerLine>();

                    foreach (var tickerLine in ticker.TickerLines)
                    {
                        TickerLine tickerLineEntity = new TickerLine();
                        tickerLineEntity.CopyFrom(tickerLine);
                        tickerLineEntity.CreationDate = dtNow;
                        tickerLineEntity.LastUpdatedDate = dtNow;

                        if (userRoleId == (int)TeamRoles.TickerProducer)
                        {
                            tickerLineEntity.CategoryStatusId = (int)TickerStatuses.Approved;
                            tickerLineEntity.LatestStatusId = (int)TickerStatuses.Approved;

                            if (tickerLineEntity.Severity == (int)TickerSeverity.High)
                            {
                                tickerLineEntity.BreakingStatusId = (int)TickerStatuses.Approved;
                            }
                        }
                        else
                        {
                            tickerLineEntity.CategoryStatusId = (int)TickerStatuses.Pending;
                            tickerLineEntity.BreakingStatusId = (int)TickerStatuses.Pending;
                            tickerLineEntity.LatestStatusId = (int)TickerStatuses.Pending;
                        }

                        tickerLineEntity.BreakingSequenceId = ++seqNumbers["bSeq"];
                        tickerLineEntity.LatestSequenceId = ++seqNumbers["lSeq"];
                        tickerLineEntity.CategorySequenceId = ++seqNumbers["cSeq"];

                        tickerLineEntity.TickerId = entity.TickerId;
                        tickerLineEntity.IsActive = true;
                        TickerLine tempEntity = _iTickerLineRepository.InsertTickerLine(tickerLineEntity);
                        if (tempEntity != null)
                            entity.TickerLines.Add(tempEntity);

                    }
                    return entity;
                }
            }
            return null;
        }


        public List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds)
        {
            if (tickerCategoryIds.Count > 0)
            {
                return _iTickerRepository.GetTickersByCategoryIds(tickerCategoryIds);
            }
            else
            {
                return new List<Ticker>();
            }
        }
        public List<Ticker> GetTickersByCategoryIds(List<int> tickerCategoryIds, DateTime lastUpdateDate)
        {
            if (tickerCategoryIds.Count > 0)
            {
                return _iTickerRepository.GetTickersByCategoryIds(tickerCategoryIds, lastUpdateDate);
            }
            else
            {
                return new List<Ticker>();
            }
        }

        public Ticker CreateTicker(string text, int tickerCategoryId, int? newsFileId, int? userId, int userRoleId, bool isManual = false)
        {
            var creationDate = DateTime.UtcNow;
            if (userRoleId == (int)WorkRole.TickerProducerOneA || userRoleId == (int)WorkRole.TickerCopyWriterOneA)
            {
                if (newsFileId.HasValue && isManual)
                {
                    var news = _iNewsFileRepository.GetNewsFile(newsFileId.Value);
                    creationDate = news.CreationDate;
                }
                var ticker = _iTickerRepository.InsertTicker(new Ticker()
                {
                    Text = text,
                    CategoryId = tickerCategoryId,
                    NewsFileId = newsFileId,
                    StatusId = userRoleId == (int)WorkRole.TickerProducerOneA ? (int)TickersStatus.New : (int)TickersStatus.ApprovalPending,
                    UserId = userId,
                    IsActive = true,
                    CreationDate = creationDate,
                    LastUpdatedDate = DateTime.UtcNow,
                });
                ticker.SequenceId = _iTickerRepository.GetMaxSequenceId();
                if (newsFileId.HasValue && isManual)
                {
                    _iTickerRepository.UpdateCategorySequenceIdByCreationDate(tickerCategoryId, creationDate, ticker.SequenceId ?? 0);
                }
                return _iTickerRepository.UpdateTicker(ticker);
            }
            else
            {
                throw new Exception("User not aurthorized to create ticker.");
            }
            //return _iTickerRepository.CreateTicker(text, tickerCategoryId, newsFileId);
        }

        public bool SortTicker(int tickerId, int tickerCategoryId, int sequenceId, SortDirection direction)
        {
            return _iTickerRepository.SortTicker(tickerId, tickerCategoryId, sequenceId, direction);
        }

        public bool UpdateTickerbById(int tickerId, string text, int StatusId, int? userId, int userRoleId)
        {
           
            if (userRoleId == (int)WorkRole.EnglishTickerEditorial ||  userRoleId == (int)WorkRole.EnglishTickerWriter)
            {
                return _iTickerTranslationRepository.UpdateTranslation(tickerId, text, StatusId, LanguageCode.English, userId ?? 0);
            }
            else
            {
                return _iTickerRepository.UpdateTickerbById(tickerId, text, StatusId);
            }
        }


        public List<NMS.Core.DataTransfer.TickerCategory.GetOutput> GetAllTickerCategoriesGroupByParentCategory(int[] ParentCategoryIds)
        {
            List<TickerCategory> AllCategories = _iTickerCategoryRepository.GetAllTickerCategory();

            List<NMS.Core.DataTransfer.TickerCategory.GetOutput> list = new List<NMS.Core.DataTransfer.TickerCategory.GetOutput>();
            list.CopyFrom(AllCategories);


            List<NMS.Core.DataTransfer.TickerCategory.GetOutput> result = new List<NMS.Core.DataTransfer.TickerCategory.GetOutput>();
            for (int i = 0; i < ParentCategoryIds.Length; i++)
            {
                NMS.Core.DataTransfer.TickerCategory.GetOutput item = list.Where(x => x.ParentTickerCategoryId == ParentCategoryIds[i])
                    .GroupBy(x => x.ParentTickerCategoryId,
                                     (key, elements) =>
                                        new NMS.Core.DataTransfer.TickerCategory.GetOutput
                                        {
                                            TickerCategoryId = key.Value,
                                            Subcategories = elements.ToList()
                                        })
                            .FirstOrDefault();
                if (item != null)
                {
                    result.Add(item);
                }
                else
                {
                    NMS.Core.DataTransfer.TickerCategory.GetOutput item2 = list.Where(x => x.TickerCategoryId == ParentCategoryIds[i]).FirstOrDefault();
                    result.Add(item2);
                }

            }

            for (int i = 0; i < result.Count; i++)
            {
                if (ParentCategoryIds.Contains(result[i].TickerCategoryId))
                {
                    NMS.Core.DataTransfer.TickerCategory.GetOutput item = result.Where(x => x.TickerCategoryId == list[i].TickerCategoryId).FirstOrDefault();
                    NMS.Core.DataTransfer.TickerCategory.GetOutput listItem = list.Where(x => x.TickerCategoryId == list[i].TickerCategoryId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Name = listItem.Name;
                    }
                }
            }

            return result;
        }
        public List<NMS.Core.DataTransfer.TickerCategory.GetOutput> GetAllTickerCategoriesGroupByTabName(string tabName)
        {
            List<TickerCategory> AllCategories = _iTickerCategoryRepository.GetAllTickerCategory().Where(x => x.IsActive == true).ToList();//.Where(x => x.TabName == tabName).ToList();

            List<NMS.Core.DataTransfer.TickerCategory.GetOutput> list = new List<NMS.Core.DataTransfer.TickerCategory.GetOutput>();
            list.CopyFrom(AllCategories);

            List<NMS.Core.DataTransfer.TickerCategory.GetOutput> result = new List<NMS.Core.DataTransfer.TickerCategory.GetOutput>();
            foreach (var category in AllCategories.Where(x => x.TabName == tabName))
            {
                NMS.Core.DataTransfer.TickerCategory.GetOutput item = list.Where(x => x.ParentTickerCategoryId == category.TickerCategoryId)
                    .GroupBy(x => x.ParentTickerCategoryId,
                                     (key, elements) =>
                                        new NMS.Core.DataTransfer.TickerCategory.GetOutput
                                        {
                                            TickerCategoryId = key.Value,
                                            Subcategories = elements.ToList()
                                        })
                            .FirstOrDefault();
                if (item != null)
                {
                    result.Add(item);
                }
                else
                {
                    NMS.Core.DataTransfer.TickerCategory.GetOutput item2 = list.Where(x => x.TickerCategoryId == category.TickerCategoryId).FirstOrDefault();
                    result.Add(item2);
                }

            }

            foreach (var item in result)
            {
                //if (ParentCategoryIds.Contains(result[i].TickerCategoryId))
                //{
                if (item.ParentTickerCategoryId == null)
                {
                    // NMS.Core.DataTransfer.TickerCategory.GetOutput item = result.Where(x => x.TickerCategoryId == item.TickerCategoryId).FirstOrDefault();
                    NMS.Core.DataTransfer.TickerCategory.GetOutput listItem = list.Where(x => x.TickerCategoryId == item.TickerCategoryId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Name = listItem.Name;
                    }
                }
                //}
            }

            return result;
        }

        public bool MoveTickerByCategoryId(int TickerId, int TickerCategoryId)
        {
            return _iTickerRepository.MoveTickerByCategoryId(TickerId, TickerCategoryId);
        }


        public List<TickerCategory> GetAllCategoriesHavingTickers()
        {
            return _iTickerCategoryRepository.GetAllCategoriesHavingTickers();
        }

        public void pushTickersInCategories(bool isManual = false, bool isSync = true, bool isPush = true)
        {

            if (isManual || (ConfigurationManager.AppSettings["EnableTickerSerivce"] != null && ConfigurationManager.AppSettings["EnableTickerSerivce"] == "1"))
            {
                var lastSyncTimeStr = _iKeyValueRepository.GetValueByKey(Environment.MachineName + ":TickerSyncService");
                var lastSyncTime = string.IsNullOrWhiteSpace(lastSyncTimeStr) ? DateTime.UtcNow.AddMinutes(-61) : DateTime.ParseExact(lastSyncTimeStr, "yyyy-MM-dd HH:mm:ss", null);

                var currentDate = DateTime.UtcNow;
                var lastDateInterval = currentDate.AddHours(-1);
                if (isManual || currentDate.Minute == 3 || (currentDate - lastSyncTime).TotalMinutes > 60)
                {
                    var tickerCategories = (_iTickerCategoryRepository.GetAllTickerCategory() ?? new List<TickerCategory>()).Where(x => x.IsActive.HasValue && x.IsActive.Value);
                    MCRTickerService mcrTickerService = new MCRTickerService();
                    var mcrTickerCategories = mcrTickerService.GetMCRTickerCategories();
                    if (isPush)
                    {
                        #region MCR Tickers Deletion
                        mcrTickerService.DeleteAllTicker();
                        #endregion
                        #region Tickers Insertion in MCR

                        TickerHistory lstlog = new TickerHistory();
                        lstlog.lstTicker = new List<Ticker>();
                        lstlog.lstTickerhistory = new List<McrTickerHistory>();

                        var categorySize = new Dictionary<int, int>();
                        var categoryTickers = new Dictionary<int, int>();

                        foreach (var category in tickerCategories.Where(x => !x.ParentTickerCategoryId.HasValue || x.ParentTickerCategoryId.Value == 0))
                        {
                            categorySize.Add(category.TickerCategoryId, category.TickerSize);
                            categoryTickers.Add(category.TickerCategoryId, 0);
                        }
                        foreach (var category in tickerCategories.OrderBy(x => x.Priority ?? 0).ThenBy(x => x.TickerCategoryId))
                        {
                            var mcrTickerCategory = mcrTickerCategories.Where(x => x.MCRTickerCategoryId == category.McrTickerCategoryId).FirstOrDefault();
                            if (mcrTickerCategory == null) { continue; }
                            var tickers = _iTickerRepository.GetTickersForMCR(category.TickerCategoryId, category.TickerSize, true, true);
                            var historyTickers = new List<McrTickerHistory>();
                            tickers = tickers.Where(x => x.StatusId == (int)TickersStatus.Approved).OrderByDescending(x => x.SequenceId).ToList();
                            #region History Tickers is disabled
                            //if (tickers.Count < category.TickerSize && category.TicketCategoryTypeId != (int)TickerCategoryType.Entity
                            //    && category.TicketCategoryTypeId != (int)TickerCategoryType.Location
                            //    && category.TicketCategoryTypeId != (int)TickerCategoryType.Face
                            //    && category.TicketCategoryTypeId != (int)TickerCategoryType.Topic)
                            //{
                            //    var newTickerIds = tickers.Select(x => x.TickerId).ToList();
                            //    historyTickers = _iMcrTickerHistoryRepository.GetLastBroadcastedTickers(category.TickerCategoryId, category.TickerSize - tickers.Count, newTickerIds);
                            //} 
                            #endregion



                            var tickerBroadcasted = _iMcrTickerBroadcastedRepository.InsertMcrTickerBroadcasted(new McrTickerBroadcasted()
                            {
                                Broadcasted = tickers.Count,
                                CreationDate = DateTime.UtcNow,
                                Hour = DateTime.UtcNow.Hour,
                                Limit = category.TickerSize,
                                TickerCategoryId = category.TickerCategoryId
                            });
                            int priority = (tickers.Count + historyTickers.Count) - 1;
                            if (category.TicketCategoryTypeId == (int)TickerCategoryType.Entity
                               || category.TicketCategoryTypeId == (int)TickerCategoryType.Location
                               || category.TicketCategoryTypeId == (int)TickerCategoryType.Face
                               || category.TicketCategoryTypeId == (int)TickerCategoryType.Topic)
                            {
                                priority = Math.Min(((tickers.Count + historyTickers.Count) - 1), (categorySize[category.ParentTickerCategoryId.Value] - categoryTickers[category.ParentTickerCategoryId.Value]) - 1);
                            }
                            #region Current Tickers Insertion
                            foreach (var ticker in tickers)
                            {
                                if (category.TicketCategoryTypeId == (int)TickerCategoryType.Entity
                               || category.TicketCategoryTypeId == (int)TickerCategoryType.Location
                               || category.TicketCategoryTypeId == (int)TickerCategoryType.Face
                               || category.TicketCategoryTypeId == (int)TickerCategoryType.Topic)
                                {
                                    if (categoryTickers[category.ParentTickerCategoryId.Value] >= categorySize[category.ParentTickerCategoryId.Value])
                                    {
                                        continue;
                                    }
                                }
                                mcrTickerService.AddTicker(ticker.Text, mcrTickerCategory, priority, ticker.CreationDate?.ToLocalTime() ?? DateTime.Now, ticker.LastUpdatedDate?.ToLocalTime() ?? DateTime.Now);
                                priority--;

                                ticker.McrTickerCategoryId = mcrTickerCategory.MCRTickerCategoryId;
                                ticker.McrTickerCategoryName = mcrTickerCategory.MCRTickerCategoryName;
                                lstlog.lstTicker.Add(ticker);

                                _iMcrTickerHistoryRepository.InsertMcrTickerHistory(new McrTickerHistory()
                                {
                                    CreationDate = DateTime.UtcNow,
                                    McrTickerBroadcastedId = tickerBroadcasted.McrTickerBroadcastedId,
                                    Text = ticker.Text,
                                    TickerCategoryId = category.TickerCategoryId,
                                    TickerId = ticker.TickerId,
                                    Type = "New"
                                });
                                if (category.ParentTickerCategoryId.HasValue)
                                    categoryTickers[category.ParentTickerCategoryId.Value]++;
                            }

                            #endregion
                            #region History Tickers Insertion
                            var tickersFromTable = _iTickerRepository.GetByIds(historyTickers.Select(x => x.TickerId ?? 0).ToList());
                            foreach (var ticker in historyTickers.OrderByDescending(x => x.CreationDate))
                            {
                                var mainTicker = tickersFromTable.Where(x => x.TickerId == (ticker.TickerId ?? 0)).FirstOrDefault();
                                mcrTickerService.AddTicker(ticker.Text, mcrTickerCategory, priority, mainTicker.CreationDate?.ToLocalTime() ?? DateTime.Now, mainTicker.LastUpdatedDate?.ToLocalTime() ?? DateTime.Now);
                                priority--;

                                ticker.TickerCategoryId = mcrTickerCategory.MCRTickerCategoryId;
                                ticker.McrTickerCategoryName = mcrTickerCategory.MCRTickerCategoryName;
                                lstlog.lstTickerhistory.Add(ticker);

                                _iMcrTickerHistoryRepository.InsertMcrTickerHistory(new McrTickerHistory()
                                {
                                    CreationDate = DateTime.UtcNow,
                                    McrTickerBroadcastedId = tickerBroadcasted.McrTickerBroadcastedId,
                                    Text = ticker.Text,
                                    TickerCategoryId = category.TickerCategoryId,
                                    TickerId = ticker.TickerId,
                                    Type = "History"
                                });
                            }
                            #endregion

                            #region Mark Tickers as Obsolete By Date
                            if (category.HourPolicy > 0)
                            {
                                _iTickerRepository.MarkTickersDeleteByHour(category.HourPolicy ?? 0, category.TickerCategoryId);
                            }
                            #endregion
                            _iTickerRepository.MarkTickersUnapproved(category.TickerCategoryId);
                        }

                        try
                        {
                            string json = new JavaScriptSerializer().Serialize(lstlog);
                            McrTickerHistoryLog log = new McrTickerHistoryLog();
                            log.CreationDate = DateTime.UtcNow;
                            log.JsonObject = json;
                            _iMcrTickerHistoryLogRepository.InsertMcrTickerHistoryLog(log);
                        }
                        catch (Exception ex)
                        {
                            ExceptionLogger.Log(ex);
                        }

                        #endregion
                    }
                    if (isSync)
                    {
                        #region Commented
                        //#region Fresh Tickers Working
                        //var big30Category = tickerCategories.Where(x => x.Name == "Big 30").FirstOrDefault();
                        //var super40Category = tickerCategories.Where(x => x.Name == "Super 40").FirstOrDefault();
                        //var alertCategory = tickerCategories.Where(x => x.Name == "Alert").FirstOrDefault();

                        //var newsToInsertInFreshNews = _iNewsFileRepository.GetNewsForFreshTicker(lastDateInterval).OrderBy(x => x.CreationDate).ToList();

                        //if (big30Category != null)
                        //{
                        //    var newsForBig30 = newsToInsertInFreshNews.Take(30);
                        //    if (newsForBig30.Count() > 0)
                        //    {
                        //        var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(newsForBig30.Select(x => x.NewsFileId).ToList(), big30Category.TickerCategoryId);
                        //        var newsToInsert = newsForBig30.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                        //        foreach (var news in newsForBig30)
                        //        {
                        //            if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                        //            {
                        //                CreateTicker(news.Title, big30Category.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                        //            }
                        //            else
                        //            {
                        //                MoveNewsTickerToTop(news.NewsFileId, big30Category.TickerCategoryId);
                        //            }
                        //        }
                        //    }
                        //}
                        //if (super40Category != null)
                        //{
                        //    var newsForSuper40 = newsToInsertInFreshNews.Skip(30).Take(40);
                        //    if (newsForSuper40.Count() > 0)
                        //    {
                        //        var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(newsForSuper40.Select(x => x.NewsFileId).ToList(), super40Category.TickerCategoryId);
                        //        var newsToInsert = newsForSuper40.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                        //        foreach (var news in newsForSuper40)
                        //        {
                        //            if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                        //            {
                        //                CreateTicker(news.Title, super40Category.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                        //            }
                        //            else
                        //            {
                        //                MoveNewsTickerToTop(news.NewsFileId, super40Category.TickerCategoryId);
                        //            }
                        //        }
                        //    }
                        //}
                        //if (alertCategory != null)
                        //{
                        //    var newsForAlert = newsToInsertInFreshNews.Skip(70);
                        //    if (newsForAlert.Count() > 0)
                        //    {
                        //        var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(newsForAlert.Select(x => x.NewsFileId).ToList(), alertCategory.TickerCategoryId);
                        //        var newsToInsert = newsForAlert.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                        //        foreach (var news in newsForAlert)
                        //        {
                        //            if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                        //            {
                        //                CreateTicker(news.Title, alertCategory.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                        //            }
                        //            else
                        //            {
                        //                MoveNewsTickerToTop(news.NewsFileId, alertCategory.TickerCategoryId);
                        //            }
                        //        }
                        //    }
                        //}
                        //#endregion 
                        #endregion
                        #region Other Tickers Working
                        foreach (var category in tickerCategories)
                        {
                            #region Custom
                            if (category.TicketCategoryTypeId == (int)TickerCategoryType.Custom)
                            {
                                #region Top 20
                                if (category.Name == "Top 20")
                                {
                                    Dictionary<int, int> programsAndCount = new Dictionary<int, int>();
                                    Dictionary<int, int> newsFileIdAndSequenceId = new Dictionary<int, int>();
                                    programsAndCount.Add(2, 8);
                                    programsAndCount.Add(6107, 12);
                                    programsAndCount.Add(6108, 8);

                                    var newsRecentlyUpdated = _iNewsFileRepository.GetByProgramPriority(programsAndCount);
                                    var newsIds = new List<int>();

                                    foreach (var pair in programsAndCount)
                                    {
                                        var newsByProgramId = newsRecentlyUpdated.Where(x => x.ProgramId == pair.Key).OrderBy(x => x.SequenceNo).ToList();
                                        foreach (var news in newsByProgramId)
                                        {
                                            if (!newsIds.Contains(news.ParentId ?? 0) && newsIds.Count <= category.TickerSize) { newsIds.Add(news.ParentId ?? 0); }
                                        }

                                    }
                                    var parentNews = _iNewsFileRepository.GetNewsByIdsForTicker(newsIds);

                                    if (parentNews.Count > 0)
                                    {
                                        var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(parentNews.Select(x => x.NewsFileId).ToList(), category.TickerCategoryId);

                                        var newsToInsert = parentNews.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                                        foreach (var news in parentNews)
                                        {
                                            if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                                            {
                                                CreateTicker(news.Title, category.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                                            }
                                            else
                                            {
                                                MoveNewsTickerToTop(news.NewsFileId, category.TickerCategoryId);
                                            }

                                        }


                                    }


                                    int getMaxSeqNo = _iTickerRepository.GetMaxSequenceId();
                                    getMaxSeqNo = getMaxSeqNo + newsRecentlyUpdated.Count();

                                    foreach (var _news in newsRecentlyUpdated)
                                    {
                                        if (newsIds.Contains(_news.ParentId ?? 0))
                                            newsFileIdAndSequenceId.Add(_news.ParentId ?? 0, getMaxSeqNo);

                                        getMaxSeqNo = getMaxSeqNo - 1;
                                    }

                                    foreach (var _pair in newsFileIdAndSequenceId)
                                    {
                                        _iTickerRepository.UpdateTickerSequenceByNewsFileId(_pair.Key, category.TickerCategoryId, _pair.Value);
                                    }

                                }
                                #endregion
                            }
                            #endregion
                            #region Commented
                            //#region Location
                            //else if (category.TicketCategoryTypeId == (int)TickerCategoryType.Location)
                            //{
                            //    if (category.LocationId.HasValue && category.LocationId.Value > 0)
                            //    {
                            //        var locationIds = _iLocationRepository.GetAllChildLocations(category.LocationId.Value);
                            //        var newsRecentlyUpdated = _iNewsFileRepository.GetNewsByLocationIds(locationIds, lastDateInterval);
                            //        if (newsRecentlyUpdated.Count > 0)
                            //        {
                            //            var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(newsRecentlyUpdated.Select(x => x.NewsFileId).ToList(), category.TickerCategoryId);

                            //            var newsToInsert = newsRecentlyUpdated.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                            //            foreach (var news in newsRecentlyUpdated)
                            //            {
                            //                if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                            //                {
                            //                    CreateTicker(news.Title, category.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                            //                }
                            //                else
                            //                {
                            //                    MoveNewsTickerToTop(news.NewsFileId, category.TickerCategoryId);
                            //                }
                            //            }
                            //        }

                            //    }
                            //}
                            //#endregion
                            //#region Topic
                            //else if (category.TicketCategoryTypeId == (int)TickerCategoryType.Topic)
                            //{
                            //    if (!string.IsNullOrWhiteSpace(category.TagIds) || !string.IsNullOrWhiteSpace(category.EntityIds))
                            //    {
                            //        var tagIds = string.IsNullOrWhiteSpace(category.TagIds) ? new List<int>()
                            //            : (category.TagIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList());
                            //        var entityIds = string.IsNullOrWhiteSpace(category.EntityIds) ? new List<int>()
                            //            : (category.EntityIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList());

                            //        var newsRecentlyUpdated = _iNewsFileRepository.GetNewsByTagAndEntityIds(tagIds, entityIds, lastDateInterval);
                            //        if (newsRecentlyUpdated.Count > 0)
                            //        {
                            //            var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(newsRecentlyUpdated.Select(x => x.NewsFileId).ToList(), category.TickerCategoryId);

                            //            var newsToInsert = newsRecentlyUpdated.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                            //            foreach (var news in newsRecentlyUpdated)
                            //            {
                            //                if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                            //                {
                            //                    CreateTicker(news.Title, category.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                            //                }
                            //                else
                            //                {
                            //                    MoveNewsTickerToTop(news.NewsFileId, category.TickerCategoryId);
                            //                }
                            //            }
                            //        }
                            //    }
                            //}
                            //#endregion
                            //#region Face
                            //else if (category.TicketCategoryTypeId == (int)TickerCategoryType.Face)
                            //{
                            //    if (!string.IsNullOrWhiteSpace(category.TagIds))
                            //    {
                            //        var tagIds = string.IsNullOrWhiteSpace(category.TagIds) ? new List<int>()
                            //            : (category.TagIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList());

                            //        var newsRecentlyUpdated = _iNewsFileRepository.GetNewsByTagAndEntityIds(tagIds, new List<int>() { 0 }, lastDateInterval);
                            //        if (newsRecentlyUpdated.Count > 0)
                            //        {
                            //            var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(newsRecentlyUpdated.Select(x => x.NewsFileId).ToList(), category.TickerCategoryId);

                            //            var newsToInsert = newsRecentlyUpdated.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                            //            foreach (var news in newsRecentlyUpdated)
                            //            {
                            //                if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                            //                {
                            //                    CreateTicker(news.Title, category.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                            //                }
                            //                else
                            //                {
                            //                    MoveNewsTickerToTop(news.NewsFileId, category.TickerCategoryId);
                            //                }
                            //            }
                            //        }
                            //    }
                            //}
                            //#endregion
                            //#region Topic
                            //else if (category.TicketCategoryTypeId == (int)TickerCategoryType.Entity)
                            //{
                            //    if (!string.IsNullOrWhiteSpace(category.EntityIds))
                            //    {
                            //        var entityIds = string.IsNullOrWhiteSpace(category.EntityIds) ? new List<int>()
                            //            : (category.EntityIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList());

                            //        var newsRecentlyUpdated = _iNewsFileRepository.GetNewsByTagAndEntityIds(new List<int>(), entityIds, lastDateInterval);
                            //        if (newsRecentlyUpdated.Count > 0)
                            //        {
                            //            var newsIdsNotExistInCategory = _iTickerRepository.GetNotExistingNewsFileIds(newsRecentlyUpdated.Select(x => x.NewsFileId).ToList(), category.TickerCategoryId);

                            //            var newsToInsert = newsRecentlyUpdated.Where(x => newsIdsNotExistInCategory.Contains(x.NewsFileId));
                            //            foreach (var news in newsRecentlyUpdated)
                            //            {
                            //                if (newsToInsert.Select(x => x.NewsFileId).Contains(news.NewsFileId))
                            //                {
                            //                    CreateTicker(news.Title, category.TickerCategoryId, news.NewsFileId, 0, (int)WorkRole.TickerProducerOneA);
                            //                }
                            //                else
                            //                {
                            //                    MoveNewsTickerToTop(news.NewsFileId, category.TickerCategoryId);
                            //                }
                            //            }
                            //        }
                            //    }
                            //}
                            //#endregion 
                            #endregion

                        }
                        #endregion
                    }

                    _iKeyValueRepository.SetValueByKey(Environment.MachineName + ":TickerSyncService", currentDate.ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }
        }

        public class TickerHistory
        {
            public List<Ticker> lstTicker { get; set; }
            public List<McrTickerHistory> lstTickerhistory { get; set; }
        }

        private void MoveNewsTickerToTop(int newsFileId, int tickerCategoryId)
        {
            _iTickerRepository.MoveNewsTickerToTop(newsFileId, tickerCategoryId);
        }

        public TickerCategory InsertTickerCategory(TickerCategory input)
        {
            switch (input.TicketCategoryTypeId)
            {
                case (int)TickerCategoryType.Location:
                    input.ParentTickerCategoryId = 25;
                    break;
                case (int)TickerCategoryType.Topic:
                    input.ParentTickerCategoryId = 26;
                    break;
                case (int)TickerCategoryType.Face:
                    input.ParentTickerCategoryId = 12;
                    break;
                case (int)TickerCategoryType.Entity:
                    input.ParentTickerCategoryId = 2;
                    break;
                case (int)TickerCategoryType.Manual:
                    input.ParentTickerCategoryId = 1;
                    break;
                default:
                    break;
            }
            input.CreationDate = DateTime.UtcNow;
            input.LastUpdateDate = DateTime.UtcNow;
            input.IsActive = true;
            input.SequenceNumber = _iTickerCategoryRepository.GetMaxSequenceId();

            TickerCategory TCategory = _iTickerCategoryRepository.InsertTickerCategory(input);
            TCategory.Organizations = new List<Organization>();
            TCategory.Tags = new List<Tag>();
            TCategory.Locations = new List<Location>();
            List<string> EntityIds = TCategory.EntityIds.Split(',').ToList<string>();
            List<string> TagIds = TCategory.TagIds.Split(',').ToList<string>();
            if (!string.IsNullOrEmpty(TCategory.EntityIds))
            {
                for (int j = 0; j < EntityIds.Count; j++)
                {
                    Organization org = _iOrganizationRepository.GetOrganization(Convert.ToInt32(EntityIds[j]));
                    TCategory.Organizations.Add(org);
                }
            }
            if (!string.IsNullOrEmpty(TCategory.TagIds))
            {
                for (int k = 0; k < TagIds.Count; k++)
                {
                    Tag tag = _iTagRepository.GetTag(Convert.ToInt32(TagIds[k]));
                    TCategory.Tags.Add(tag);
                }
            }
            if (TCategory.LocationId != null && TCategory.LocationId.Value > 0)
            {
                Location location = _iLocationRepository.GetLocation(TCategory.LocationId.Value);
                TCategory.Locations.Add(location);
            }
            return TCategory;


        }

        public List<TickerCategory> GetAllTickerCategories()
        {
            MCRTickerService mcrTickerService = new MCRTickerService();
            var mcrTickerCategories = mcrTickerService.GetMCRTickerCategories();
            List<TickerCategory> TCategory = _iTickerCategoryRepository.GetAllTickerCategories();
            for (int i = 0; i < TCategory.Count; i++)
            {
                TCategory[i].McrTickerCategoryName = mcrTickerCategories.Where(x => x.MCRTickerCategoryId == TCategory[i].McrTickerCategoryId).FirstOrDefault()?.MCRTickerCategoryName ?? string.Empty;
                TCategory[i].Organizations = new List<Organization>();
                TCategory[i].Tags = new List<Tag>();
                TCategory[i].Locations = new List<Location>();
                List<string> EntityIds = TCategory[i].EntityIds.Split(',').ToList<string>();
                List<string> TagIds = TCategory[i].TagIds.Split(',').ToList<string>();
                if (!string.IsNullOrEmpty(TCategory[i].EntityIds))
                {
                    for (int j = 0; j < EntityIds.Count; j++)
                    {
                        Organization org = _iOrganizationRepository.GetOrganization(Convert.ToInt32(EntityIds[j]));
                        TCategory[i].Organizations.Add(org);
                    }
                }
                if (!string.IsNullOrEmpty(TCategory[i].TagIds))
                {
                    for (int k = 0; k < TagIds.Count; k++)
                    {
                        Tag tag = _iTagRepository.GetTag(Convert.ToInt32(TagIds[k]));
                        TCategory[i].Tags.Add(tag);
                    }
                }
                if (TCategory[i].LocationId != null && TCategory[i].LocationId.Value > 0)
                {
                    Location location = _iLocationRepository.GetLocation(TCategory[i].LocationId.Value);
                    TCategory[i].Locations.Add(location);
                }
            }

            return TCategory;
        }

        public TickerCategory updateTickerCategory(TickerCategory input)
        {
            TickerCategory tickerToUpdate = _iTickerCategoryRepository.GetTickerCategory(input.TickerCategoryId);
            switch (input.TicketCategoryTypeId)
            {
                case (int)TickerCategoryType.Location:
                    input.ParentTickerCategoryId = 25;
                    break;
                case (int)TickerCategoryType.Topic:
                    input.ParentTickerCategoryId = 26;
                    break;
                case (int)TickerCategoryType.Face:
                    input.ParentTickerCategoryId = 12;
                    break;
                case (int)TickerCategoryType.Entity:
                    input.ParentTickerCategoryId = 2;
                    break;
                case (int)TickerCategoryType.Manual:
                    input.ParentTickerCategoryId = 1;
                    break;
                default:
                    break;
            }
            input.LastUpdateDate = DateTime.UtcNow;
            input.CreationDate = tickerToUpdate.CreationDate;
            input.LastUpdateDate = DateTime.UtcNow;
            input.IsActive = true;

            TickerCategory TCategory = _iTickerCategoryRepository.UpdateTickerCategory(input);
            TCategory.Organizations = new List<Organization>();
            TCategory.Tags = new List<Tag>();
            TCategory.Locations = new List<Location>();
            List<string> EntityIds = TCategory.EntityIds.Split(',').ToList<string>();
            List<string> TagIds = TCategory.TagIds.Split(',').ToList<string>();
            if (!string.IsNullOrEmpty(TCategory.EntityIds))
            {
                for (int j = 0; j < EntityIds.Count; j++)
                {
                    Organization org = _iOrganizationRepository.GetOrganization(Convert.ToInt32(EntityIds[j]));
                    TCategory.Organizations.Add(org);
                }
            }
            if (!string.IsNullOrEmpty(TCategory.TagIds))
            {
                for (int k = 0; k < TagIds.Count; k++)
                {
                    Tag tag = _iTagRepository.GetTag(Convert.ToInt32(TagIds[k]));
                    TCategory.Tags.Add(tag);
                }
            }
            if (TCategory.LocationId != null && TCategory.LocationId.Value > 0)
            {
                Location location = _iLocationRepository.GetLocation(TCategory.LocationId.Value);
                TCategory.Locations.Add(location);
            }
            return TCategory;
        }

        public bool DeleteTickerCategory(TickerCategory input)
        {
            TickerCategory tickerToUpdate = _iTickerCategoryRepository.GetTickerCategory(input.TickerCategoryId);
            switch (input.TicketCategoryTypeId)
            {
                case (int)TickerCategoryType.Location:
                    input.ParentTickerCategoryId = 25;
                    break;
                case (int)TickerCategoryType.Topic:
                    input.ParentTickerCategoryId = 26;
                    break;
                case (int)TickerCategoryType.Face:
                    input.ParentTickerCategoryId = 12;
                    break;
                case (int)TickerCategoryType.Entity:
                    input.ParentTickerCategoryId = 2;
                    break;
                default:
                    break;
            }
            input.LastUpdateDate = DateTime.UtcNow;
            input.CreationDate = tickerToUpdate.CreationDate;
            input.IsActive = false;
            _iTickerCategoryRepository.UpdateTickerCategory(input);
            return true;
        }

        public void UpdateMCRTickerId(int tickerId, int mcrTickerId)
        {
            _iTickerRepository.UpdateMCRTickerId(tickerId, mcrTickerId);
        }

        public bool MarkTickerValid(int tickerId)
        {
            return _iTickerRepository.MarkTickerValid(tickerId);
        }

        public bool SubmitTickersByCateogryId(int categoryId)
        {
            var category = _iTickerCategoryRepository.GetTickerCategory(categoryId);
            var tickersToApprove = _iTickerRepository.GetTickersForMCR(categoryId, category.TickerSize, true, null);
            if (tickersToApprove.Count > 0)
            {
                var idsToApprove = tickersToApprove.Select(x => x.TickerId).ToList();
                return _iTickerRepository.ApproveTickers(idsToApprove);
            }
            return true;

        }
        public List<Ticker> GetTickersForMCR(int categoryId, int size, bool sortBySequence, bool? isApproved)
        {
            return _iTickerRepository.GetTickersForMCR(categoryId, size, sortBySequence, isApproved);
        }

        public List<TickerTranslation> GetTickerTranslations(List<int> tickerIds, LanguageCode languageCode)
        {
            return _iTickerTranslationRepository.GetTickerTranslations(tickerIds, languageCode);
        }

        public TickerTranslation GetTranslation(int tickerId, LanguageCode languageCode)
        {
            return _iTickerTranslationRepository.GetTranslation(tickerId, languageCode);
        }
    }
}


