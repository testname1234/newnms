﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TickerStatus;
using Validation;
using NMS.Core;
using System.Linq;

namespace NMS.Service
{
		
	public class TickerStatusService : ITickerStatusService 
	{
		private ITickerStatusRepository _iTickerStatusRepository;
        
		public TickerStatusService(ITickerStatusRepository iTickerStatusRepository)
		{
			this._iTickerStatusRepository = iTickerStatusRepository;
		}
        
        public Dictionary<string, string> GetTickerStatusBasicSearchColumns()
        {
            
            return this._iTickerStatusRepository.GetTickerStatusBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTickerStatusAdvanceSearchColumns()
        {
            
            return this._iTickerStatusRepository.GetTickerStatusAdvanceSearchColumns();
           
        }
        

		public TickerStatus GetTickerStatus(System.Int32 TickerStatusId)
		{
			return _iTickerStatusRepository.GetTickerStatus(TickerStatusId);
		}

		public TickerStatus UpdateTickerStatus(TickerStatus entity)
		{
			return _iTickerStatusRepository.UpdateTickerStatus(entity);
		}

		public bool DeleteTickerStatus(System.Int32 TickerStatusId)
		{
			return _iTickerStatusRepository.DeleteTickerStatus(TickerStatusId);
		}

		public List<TickerStatus> GetAllTickerStatus()
		{
			return _iTickerStatusRepository.GetAllTickerStatus();
		}

		public TickerStatus InsertTickerStatus(TickerStatus entity)
		{
			 return _iTickerStatusRepository.InsertTickerStatus(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickerstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickerstatusid))
            {
				TickerStatus tickerstatus = _iTickerStatusRepository.GetTickerStatus(tickerstatusid);
                if(tickerstatus!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tickerstatus);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TickerStatus> tickerstatuslist = _iTickerStatusRepository.GetAllTickerStatus();
            if (tickerstatuslist != null && tickerstatuslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickerstatuslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TickerStatus tickerstatus = new TickerStatus();
                PostOutput output = new PostOutput();
                tickerstatus.CopyFrom(Input);
                tickerstatus = _iTickerStatusRepository.InsertTickerStatus(tickerstatus);
                output.CopyFrom(tickerstatus);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerStatus tickerstatusinput = new TickerStatus();
                TickerStatus tickerstatusoutput = new TickerStatus();
                PutOutput output = new PutOutput();
                tickerstatusinput.CopyFrom(Input);
                TickerStatus tickerstatus = _iTickerStatusRepository.GetTickerStatus(tickerstatusinput.TickerStatusId);
                if (tickerstatus!=null)
                {
                    tickerstatusoutput = _iTickerStatusRepository.UpdateTickerStatus(tickerstatusinput);
                    if(tickerstatusoutput!=null)
                    {
                        output.CopyFrom(tickerstatusoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickerstatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickerstatusid))
            {
				 bool IsDeleted = _iTickerStatusRepository.DeleteTickerStatus(tickerstatusid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
