﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TranslatedScript;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class TranslatedScriptService : ITranslatedScriptService 
	{
		private ITranslatedScriptRepository _iTranslatedScriptRepository;
        
		public TranslatedScriptService(ITranslatedScriptRepository iTranslatedScriptRepository)
		{
			this._iTranslatedScriptRepository = iTranslatedScriptRepository;
		}
        
        public Dictionary<string, string> GetTranslatedScriptBasicSearchColumns()
        {
            
            return this._iTranslatedScriptRepository.GetTranslatedScriptBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTranslatedScriptAdvanceSearchColumns()
        {
            
            return this._iTranslatedScriptRepository.GetTranslatedScriptAdvanceSearchColumns();
           
        }
        

		public virtual List<TranslatedScript> GetTranslatedScriptByNewsFileId(System.Int32? NewsFileId)
		{
			return _iTranslatedScriptRepository.GetTranslatedScriptByNewsFileId(NewsFileId);
		}

		public TranslatedScript GetTranslatedScript(System.Int32 TranslatedScriptId)
		{
			return _iTranslatedScriptRepository.GetTranslatedScript(TranslatedScriptId);
		}

		public TranslatedScript UpdateTranslatedScript(TranslatedScript entity)
		{
			return _iTranslatedScriptRepository.UpdateTranslatedScript(entity);
		}

		public bool DeleteTranslatedScript(System.Int32 TranslatedScriptId)
		{
			return _iTranslatedScriptRepository.DeleteTranslatedScript(TranslatedScriptId);
		}

		public List<TranslatedScript> GetAllTranslatedScript()
		{
			return _iTranslatedScriptRepository.GetAllTranslatedScript();
		}

        public List<TranslatedScript> GetTranslatedScriptByLanguageId(int languageId)
        {
            return _iTranslatedScriptRepository.GetTranslatedScriptByLanguageId(languageId);
        }

        public TranslatedScript InsertTranslatedScript(TranslatedScript entity)
		{
			 return _iTranslatedScriptRepository.InsertTranslatedScript(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 translatedscriptid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out translatedscriptid))
            {
				TranslatedScript translatedscript = _iTranslatedScriptRepository.GetTranslatedScript(translatedscriptid);
                if(translatedscript!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(translatedscript);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TranslatedScript> translatedscriptlist = _iTranslatedScriptRepository.GetAllTranslatedScript();
            if (translatedscriptlist != null && translatedscriptlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(translatedscriptlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TranslatedScript translatedscript = new TranslatedScript();
                PostOutput output = new PostOutput();
                translatedscript.CopyFrom(Input);
                translatedscript = _iTranslatedScriptRepository.InsertTranslatedScript(translatedscript);
                output.CopyFrom(translatedscript);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TranslatedScript translatedscriptinput = new TranslatedScript();
                TranslatedScript translatedscriptoutput = new TranslatedScript();
                PutOutput output = new PutOutput();
                translatedscriptinput.CopyFrom(Input);
                TranslatedScript translatedscript = _iTranslatedScriptRepository.GetTranslatedScript(translatedscriptinput.TranslatedScriptId);
                if (translatedscript!=null)
                {
                    translatedscriptoutput = _iTranslatedScriptRepository.UpdateTranslatedScript(translatedscriptinput);
                    if(translatedscriptoutput!=null)
                    {
                        output.CopyFrom(translatedscriptoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 translatedscriptid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out translatedscriptid))
            {
				 bool IsDeleted = _iTranslatedScriptRepository.DeleteTranslatedScript(translatedscriptid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
