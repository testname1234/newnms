﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Net;
using System.Web.Configuration;
using System.IO;
using NMS.Core.IService;

namespace NMS.Service
{
    public class TwitterAPI : ITwitterAPI
    {
       // string oauth_token = "1710818149-T0tA7Y3l7Bvh6BvAzfFpwWBfr9tl6WE4gLT7c3N";
        //string oauth_token_secret = "uyCkFvxRLUAsL2Jc3J8n9m5lLpC155olQbjc0IORbMcgC";
       // string oauth_consumer_key = "3RTTUIIub0NOr7O20cf3Lnggd";
        //string oauth_consumer_secret = "ns0Vw7pcYz8QQRlcX93JGrnVfnMulI3MyreB1nD9QreEhNJtJo";

        private string oauth_token
        {
            get
            {
                return WebConfigurationManager.AppSettings["TwitterToken"];
            }
        }

        private string oauth_consumer_key
        {
            get
            {
                return WebConfigurationManager.AppSettings["TwitterConsumerKey"];
            }
        }

        private string oauth_token_secret
        {
            get
            {
                return WebConfigurationManager.AppSettings["TwitterTokenSecret"];
            }
        }

        private string oauth_consumer_secret
        {
            get
            {
                return WebConfigurationManager.AppSettings["TwitterConsumerSecret"];
            }
        }

        public Boolean PostTweet(string tweet, string type)
        {
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";
            var oauth_nonce = Convert.ToBase64String(
                                              new ASCIIEncoding().GetBytes(
                                                   DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                                              - new DateTime(1970, 1, 1, 0, 0, 0, 0,
                                                   DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
            var resource_url = "https://api.twitter.com/1.1/statuses/update.json";
            var status = tweet;

            var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&status={6}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_token,
                                        oauth_version,
                                        Uri.EscapeDataString(status)
                                        );

            baseString = string.Concat("POST&", Uri.EscapeDataString(resource_url),
                         "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                        "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                   "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                   "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                   "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),
                                    Uri.EscapeDataString(oauth_signature_method),
                                    Uri.EscapeDataString(oauth_timestamp),
                                    Uri.EscapeDataString(oauth_consumer_key),
                                    Uri.EscapeDataString(oauth_token),
                                    Uri.EscapeDataString(oauth_signature),
                                    Uri.EscapeDataString(oauth_version)
                            );

            var postBody = "status=" + Uri.EscapeDataString(status);

            ServicePointManager.Expect100Continue = false;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            using (Stream stream = request.GetRequestStream())
            {
                byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                stream.Write(content, 0, content.Length);
            }
            WebResponse response = request.GetResponse();

            return true;
        }
    }
}
