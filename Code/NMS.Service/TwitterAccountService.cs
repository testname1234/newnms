﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TwitterAccount;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class TwitterAccountService : ITwitterAccountService 
	{
		private ITwitterAccountRepository _iTwitterAccountRepository;
        
		public TwitterAccountService(ITwitterAccountRepository iTwitterAccountRepository)
		{
			this._iTwitterAccountRepository = iTwitterAccountRepository;
		}
        
        public Dictionary<string, string> GetTwitterAccountBasicSearchColumns()
        {
            
            return this._iTwitterAccountRepository.GetTwitterAccountBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTwitterAccountAdvanceSearchColumns()
        {
            
            return this._iTwitterAccountRepository.GetTwitterAccountAdvanceSearchColumns();
           
        }
        

		public virtual List<TwitterAccount> GetTwitterAccountByCelebrityId(System.Int32 CelebrityId)
		{
			return _iTwitterAccountRepository.GetTwitterAccountByCelebrityId(CelebrityId);
		}

		public TwitterAccount GetTwitterAccount(System.Int32 TwitterAccountId)
		{
			return _iTwitterAccountRepository.GetTwitterAccount(TwitterAccountId);
		}

		public TwitterAccount UpdateTwitterAccount(TwitterAccount entity)
		{
			return _iTwitterAccountRepository.UpdateTwitterAccount(entity);
		}

        public void UpdateTwitterAccountIsFollowed(int TwitterAccoutnId,bool Isfollowed)
        {
             _iTwitterAccountRepository.UpdateTwitterAccount(TwitterAccoutnId, Isfollowed);
        }

		public bool DeleteTwitterAccount(System.Int32 TwitterAccountId)
		{
			return _iTwitterAccountRepository.DeleteTwitterAccount(TwitterAccountId);
		}

		public List<TwitterAccount> GetAllTwitterAccount()
		{
			return _iTwitterAccountRepository.GetAllTwitterAccount();
		}

        public List<TwitterAccount> GetByIsFollowed(bool IsFollowed)
        {
            return _iTwitterAccountRepository.GetByIsFollowed(IsFollowed);
        }

		public TwitterAccount InsertTwitterAccount(TwitterAccount entity)
		{
			 return _iTwitterAccountRepository.InsertTwitterAccount(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 twitteraccountid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out twitteraccountid))
            {
				TwitterAccount twitteraccount = _iTwitterAccountRepository.GetTwitterAccount(twitteraccountid);
                if(twitteraccount!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(twitteraccount);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TwitterAccount> twitteraccountlist = _iTwitterAccountRepository.GetAllTwitterAccount();
            if (twitteraccountlist != null && twitteraccountlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(twitteraccountlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TwitterAccount twitteraccount = new TwitterAccount();
                PostOutput output = new PostOutput();
                twitteraccount.CopyFrom(Input);
                twitteraccount = _iTwitterAccountRepository.InsertTwitterAccount(twitteraccount);
                output.CopyFrom(twitteraccount);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TwitterAccount twitteraccountinput = new TwitterAccount();
                TwitterAccount twitteraccountoutput = new TwitterAccount();
                PutOutput output = new PutOutput();
                twitteraccountinput.CopyFrom(Input);
                TwitterAccount twitteraccount = _iTwitterAccountRepository.GetTwitterAccount(twitteraccountinput.TwitterAccountId);
                if (twitteraccount!=null)
                {
                    twitteraccountoutput = _iTwitterAccountRepository.UpdateTwitterAccount(twitteraccountinput);
                    if(twitteraccountoutput!=null)
                    {
                        output.CopyFrom(twitteraccountoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 twitteraccountid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out twitteraccountid))
            {
				 bool IsDeleted = _iTwitterAccountRepository.DeleteTwitterAccount(twitteraccountid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
