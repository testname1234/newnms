﻿using MS.MediaIntegration;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Service
{
    public class UserManagementService : IUserManagementService
    {
        private HttpWebRequestHelper helper = new HttpWebRequestHelper();

        public List<User> GetAllUsers()
        {
            List<User> users = new List<User>();
            NMS.Core.DataTransfer.DataTransfer<List<UserOutput>> transfer = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<UserOutput>>>(AppSettings.MMSUsermanagementUrl + "api/admin/ModuleUsers/" + AppSettings.ModuleId, null, true);

            if (transfer != null && transfer.IsSuccess == true && transfer.Data != null && transfer.Data.Count > 0)
            {
                foreach (var user in transfer.Data)
                {
                    User tempUser = new User();
                    tempUser.UserId = user.UserId;
                    tempUser.Name = user.Fullname;
                    tempUser.Login = user.LoginId;
                    tempUser.Password = user.Password;

                    users.Add(tempUser);
                }
            }

            return users;
        }

        public List<Team> GetAllUserWorkRoleMappings()
        {
            List<Team> userRoleMappings = new List<Team>();
            NMS.Core.DataTransfer.DataTransfer<List<UserWorkRoleOutput>> transfer = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<UserWorkRoleOutput>>>(AppSettings.MMSUsermanagementUrl + "api/admin/UserWorkRoles/" + AppSettings.ModuleId, null, true);

            if (transfer != null && transfer.IsSuccess == true && transfer.Data != null && transfer.Data.Count > 0)
            {
                foreach (var teamRole in transfer.Data)
                {
                    Team temp = new Team();
                    temp.UserId = teamRole.UserId;
                    temp.WorkRoleId = teamRole.WorkRoleId;

                    userRoleMappings.Add(temp);
                }
            }

            return userRoleMappings;
        }
    }
}