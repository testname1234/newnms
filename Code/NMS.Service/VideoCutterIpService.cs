﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.VideoCutterIp;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class VideoCutterIpService : IVideoCutterIpService 
	{
		private IVideoCutterIpRepository _iVideoCutterIpRepository;
        
		public VideoCutterIpService(IVideoCutterIpRepository iVideoCutterIpRepository)
		{
			this._iVideoCutterIpRepository = iVideoCutterIpRepository;
		}
        
        public Dictionary<string, string> GetVideoCutterIpBasicSearchColumns()
        {
            
            return this._iVideoCutterIpRepository.GetVideoCutterIpBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetVideoCutterIpAdvanceSearchColumns()
        {
            
            return this._iVideoCutterIpRepository.GetVideoCutterIpAdvanceSearchColumns();
           
        }
        

		public VideoCutterIp GetVideoCutterIp(System.Int32 VideoCutterIpId)
		{
			return _iVideoCutterIpRepository.GetVideoCutterIp(VideoCutterIpId);
		}

		public VideoCutterIp UpdateVideoCutterIp(VideoCutterIp entity)
		{
			return _iVideoCutterIpRepository.UpdateVideoCutterIp(entity);
		}

		public bool DeleteVideoCutterIp(System.Int32 VideoCutterIpId)
		{
			return _iVideoCutterIpRepository.DeleteVideoCutterIp(VideoCutterIpId);
		}

		public List<VideoCutterIp> GetAllVideoCutterIp()
		{
			return _iVideoCutterIpRepository.GetAllVideoCutterIp();
		}

		public VideoCutterIp InsertVideoCutterIp(VideoCutterIp entity)
		{
			 return _iVideoCutterIpRepository.InsertVideoCutterIp(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 videocutteripid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out videocutteripid))
            {
				VideoCutterIp videocutterip = _iVideoCutterIpRepository.GetVideoCutterIp(videocutteripid);
                if(videocutterip!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(videocutterip);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<VideoCutterIp> videocutteriplist = _iVideoCutterIpRepository.GetAllVideoCutterIp();
            if (videocutteriplist != null && videocutteriplist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(videocutteriplist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                VideoCutterIp videocutterip = new VideoCutterIp();
                PostOutput output = new PostOutput();
                videocutterip.CopyFrom(Input);
                videocutterip = _iVideoCutterIpRepository.InsertVideoCutterIp(videocutterip);
                output.CopyFrom(videocutterip);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                VideoCutterIp videocutteripinput = new VideoCutterIp();
                VideoCutterIp videocutteripoutput = new VideoCutterIp();
                PutOutput output = new PutOutput();
                videocutteripinput.CopyFrom(Input);
                VideoCutterIp videocutterip = _iVideoCutterIpRepository.GetVideoCutterIp(videocutteripinput.VideoCutterIpId);
                if (videocutterip!=null)
                {
                    videocutteripoutput = _iVideoCutterIpRepository.UpdateVideoCutterIp(videocutteripinput);
                    if(videocutteripoutput!=null)
                    {
                        output.CopyFrom(videocutteripoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 videocutteripid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out videocutteripid))
            {
				 bool IsDeleted = _iVideoCutterIpRepository.DeleteVideoCutterIp(videocutteripid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public List<VideoCutterIp> GetVideoCutterIpByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
         {
           return _iVideoCutterIpRepository.GetVideoCutterIpByKeyValue(Key, Value, operand, SelectClause);
         }
    }
	
	
}
