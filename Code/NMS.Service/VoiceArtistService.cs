﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.VoiceArtist;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class VoiceArtistService : IVoiceArtistService 
	{
		private IVoiceArtistRepository _iVoiceArtistRepository;
        
		public VoiceArtistService(IVoiceArtistRepository iVoiceArtistRepository)
		{
			this._iVoiceArtistRepository = iVoiceArtistRepository;
		}
        
        public Dictionary<string, string> GetVoiceArtistBasicSearchColumns()
        {
            
            return this._iVoiceArtistRepository.GetVoiceArtistBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetVoiceArtistAdvanceSearchColumns()
        {
            
            return this._iVoiceArtistRepository.GetVoiceArtistAdvanceSearchColumns();
           
        }
        

		public VoiceArtist GetVoiceArtist(System.Int32 VoiceArtistId)
		{
			return _iVoiceArtistRepository.GetVoiceArtist(VoiceArtistId);
		}

		public VoiceArtist UpdateVoiceArtist(VoiceArtist entity)
		{
			return _iVoiceArtistRepository.UpdateVoiceArtist(entity);
		}

		public bool DeleteVoiceArtist(System.Int32 VoiceArtistId)
		{
			return _iVoiceArtistRepository.DeleteVoiceArtist(VoiceArtistId);
		}

		public List<VoiceArtist> GetAllVoiceArtist()
		{
			return _iVoiceArtistRepository.GetAllVoiceArtist();
		}

		public VoiceArtist InsertVoiceArtist(VoiceArtist entity)
		{
			 return _iVoiceArtistRepository.InsertVoiceArtist(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 voiceartistid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out voiceartistid))
            {
				VoiceArtist voiceartist = _iVoiceArtistRepository.GetVoiceArtist(voiceartistid);
                if(voiceartist!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(voiceartist);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<VoiceArtist> voiceartistlist = _iVoiceArtistRepository.GetAllVoiceArtist();
            if (voiceartistlist != null && voiceartistlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(voiceartistlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                VoiceArtist voiceartist = new VoiceArtist();
                PostOutput output = new PostOutput();
                voiceartist.CopyFrom(Input);
                voiceartist = _iVoiceArtistRepository.InsertVoiceArtist(voiceartist);
                output.CopyFrom(voiceartist);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                VoiceArtist voiceartistinput = new VoiceArtist();
                VoiceArtist voiceartistoutput = new VoiceArtist();
                PutOutput output = new PutOutput();
                voiceartistinput.CopyFrom(Input);
                VoiceArtist voiceartist = _iVoiceArtistRepository.GetVoiceArtist(voiceartistinput.VoiceArtistId);
                if (voiceartist!=null)
                {
                    voiceartistoutput = _iVoiceArtistRepository.UpdateVoiceArtist(voiceartistinput);
                    if(voiceartistoutput!=null)
                    {
                        output.CopyFrom(voiceartistoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 voiceartistid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out voiceartistid))
            {
				 bool IsDeleted = _iVoiceArtistRepository.DeleteVoiceArtist(voiceartistid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
