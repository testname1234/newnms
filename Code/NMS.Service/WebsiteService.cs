﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Website;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class WebsiteService : IWebsiteService 
	{
		private IWebsiteRepository _iWebsiteRepository;
        
		public WebsiteService(IWebsiteRepository iWebsiteRepository)
		{
			this._iWebsiteRepository = iWebsiteRepository;
		}
        
        public Dictionary<string, string> GetWebsiteBasicSearchColumns()
        {
            
            return this._iWebsiteRepository.GetWebsiteBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetWebsiteAdvanceSearchColumns()
        {
            
            return this._iWebsiteRepository.GetWebsiteAdvanceSearchColumns();
           
        }
        

		public Website GetWebsite(System.Int32 WebsiteId)
		{
			return _iWebsiteRepository.GetWebsite(WebsiteId);
		}

		public Website UpdateWebsite(Website entity)
		{
			return _iWebsiteRepository.UpdateWebsite(entity);
		}

		public bool DeleteWebsite(System.Int32 WebsiteId)
		{
			return _iWebsiteRepository.DeleteWebsite(WebsiteId);
		}

		public List<Website> GetAllWebsite()
		{
			return _iWebsiteRepository.GetAllWebsite();
		}

		public Website InsertWebsite(Website entity)
		{
			 return _iWebsiteRepository.InsertWebsite(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 websiteid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out websiteid))
            {
				Website website = _iWebsiteRepository.GetWebsite(websiteid);
                if(website!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(website);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Website> websitelist = _iWebsiteRepository.GetAllWebsite();
            if (websitelist != null && websitelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(websitelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Website website = new Website();
                PostOutput output = new PostOutput();
                website.CopyFrom(Input);
                website = _iWebsiteRepository.InsertWebsite(website);
                output.CopyFrom(website);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Website websiteinput = new Website();
                Website websiteoutput = new Website();
                PutOutput output = new PutOutput();
                websiteinput.CopyFrom(Input);
                Website website = _iWebsiteRepository.GetWebsite(websiteinput.WebsiteId);
                if (website!=null)
                {
                    websiteoutput = _iWebsiteRepository.UpdateWebsite(websiteinput);
                    if(websiteoutput!=null)
                    {
                        output.CopyFrom(websiteoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 websiteid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out websiteid))
            {
				 bool IsDeleted = _iWebsiteRepository.DeleteWebsite(websiteid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
