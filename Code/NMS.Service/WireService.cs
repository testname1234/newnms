﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.Wire;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class WireService : IWireService 
	{
		private IWireRepository _iWireRepository;
        
		public WireService(IWireRepository iWireRepository)
		{
			this._iWireRepository = iWireRepository;
		}
        
        public Dictionary<string, string> GetWireBasicSearchColumns()
        {
            
            return this._iWireRepository.GetWireBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetWireAdvanceSearchColumns()
        {
            
            return this._iWireRepository.GetWireAdvanceSearchColumns();
           
        }
        

		public Wire GetWire(System.Int32 WireId)
		{
			return _iWireRepository.GetWire(WireId);
		}

		public Wire UpdateWire(Wire entity)
		{
			return _iWireRepository.UpdateWire(entity);
		}

		public bool DeleteWire(System.Int32 WireId)
		{
			return _iWireRepository.DeleteWire(WireId);
		}

		public List<Wire> GetAllWire()
		{
			return _iWireRepository.GetAllWire();
		}

		public Wire InsertWire(Wire entity)
		{
			 return _iWireRepository.InsertWire(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 wireid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out wireid))
            {
				Wire wire = _iWireRepository.GetWire(wireid);
                if(wire!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(wire);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Wire> wirelist = _iWireRepository.GetAllWire();
            if (wirelist != null && wirelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(wirelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Wire wire = new Wire();
                PostOutput output = new PostOutput();
                wire.CopyFrom(Input);
                wire = _iWireRepository.InsertWire(wire);
                output.CopyFrom(wire);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Wire wireinput = new Wire();
                Wire wireoutput = new Wire();
                PutOutput output = new PutOutput();
                wireinput.CopyFrom(Input);
                Wire wire = _iWireRepository.GetWire(wireinput.WireId);
                if (wire!=null)
                {
                    wireoutput = _iWireRepository.UpdateWire(wireinput);
                    if(wireoutput!=null)
                    {
                        output.CopyFrom(wireoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 wireid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out wireid))
            {
				 bool IsDeleted = _iWireRepository.DeleteWire(wireid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
