﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using NMS.Core.Entities;
using NMS.Core.DataInterfaces;
using NMS.Core.IService;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.WorkRoleFolder;
using Validation;
using System.Linq;
using NMS.Core;

namespace NMS.Service
{
		
	public class WorkRoleFolderService : IWorkRoleFolderService 
	{
		private IWorkRoleFolderRepository _iWorkRoleFolderRepository;
        
		public WorkRoleFolderService(IWorkRoleFolderRepository iWorkRoleFolderRepository)
		{
			this._iWorkRoleFolderRepository = iWorkRoleFolderRepository;
		}
        
        public Dictionary<string, string> GetWorkRoleFolderBasicSearchColumns()
        {
            
            return this._iWorkRoleFolderRepository.GetWorkRoleFolderBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetWorkRoleFolderAdvanceSearchColumns()
        {
            
            return this._iWorkRoleFolderRepository.GetWorkRoleFolderAdvanceSearchColumns();
           
        }
        

		public virtual List<WorkRoleFolder> GetWorkRoleFolderByFolderId(System.Int32 FolderId)
		{
			return _iWorkRoleFolderRepository.GetWorkRoleFolderByFolderId(FolderId);
		}

		public WorkRoleFolder GetWorkRoleFolder(System.Int32 WorkRoleFolderId)
		{
			return _iWorkRoleFolderRepository.GetWorkRoleFolder(WorkRoleFolderId);
		}

		public WorkRoleFolder UpdateWorkRoleFolder(WorkRoleFolder entity)
		{
			return _iWorkRoleFolderRepository.UpdateWorkRoleFolder(entity);
		}

		public bool DeleteWorkRoleFolder(System.Int32 WorkRoleFolderId)
		{
			return _iWorkRoleFolderRepository.DeleteWorkRoleFolder(WorkRoleFolderId);
		}

		public List<WorkRoleFolder> GetAllWorkRoleFolder()
		{
			return _iWorkRoleFolderRepository.GetAllWorkRoleFolder();
		}

		public WorkRoleFolder InsertWorkRoleFolder(WorkRoleFolder entity)
		{
			 return _iWorkRoleFolderRepository.InsertWorkRoleFolder(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 workrolefolderid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out workrolefolderid))
            {
				WorkRoleFolder workrolefolder = _iWorkRoleFolderRepository.GetWorkRoleFolder(workrolefolderid);
                if(workrolefolder!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(workrolefolder);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<WorkRoleFolder> workrolefolderlist = _iWorkRoleFolderRepository.GetAllWorkRoleFolder();
            if (workrolefolderlist != null && workrolefolderlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(workrolefolderlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                WorkRoleFolder workrolefolder = new WorkRoleFolder();
                PostOutput output = new PostOutput();
                workrolefolder.CopyFrom(Input);
                workrolefolder = _iWorkRoleFolderRepository.InsertWorkRoleFolder(workrolefolder);
                output.CopyFrom(workrolefolder);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                WorkRoleFolder workrolefolderinput = new WorkRoleFolder();
                WorkRoleFolder workrolefolderoutput = new WorkRoleFolder();
                PutOutput output = new PutOutput();
                workrolefolderinput.CopyFrom(Input);
                WorkRoleFolder workrolefolder = _iWorkRoleFolderRepository.GetWorkRoleFolder(workrolefolderinput.WorkRoleFolderId);
                if (workrolefolder!=null)
                {
                    workrolefolderoutput = _iWorkRoleFolderRepository.UpdateWorkRoleFolder(workrolefolderinput);
                    if(workrolefolderoutput!=null)
                    {
                        output.CopyFrom(workrolefolderoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 workrolefolderid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out workrolefolderid))
            {
				 bool IsDeleted = _iWorkRoleFolderRepository.DeleteWorkRoleFolder(workrolefolderid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
