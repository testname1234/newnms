﻿using NMS.Core;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.Service
{
    public static class Worker
    {
        private static IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
        private static ITickerService tickerService = IoC.Resolve<ITickerService>("TickerService");
        private static INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
        private static System.Timers.Timer myTimer;
        private static int interval;
        public static bool Start(int seconds)
        {

            interval = seconds;
            myTimer = new System.Timers.Timer(seconds * 1000);
            myTimer.Elapsed += MyTimer_Elapsed;
            myTimer.Start();
            return true;
        }

        private static void MyTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    int programId = programService.GetProgramIdByBroadcastedTime(DateTime.Now.Minute * 60, interval);
                    if (programId > 0)
                    {
                        fileService.UpdateProgramCountAndHistory(programId);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                }
            });
            Task.Factory.StartNew(() =>
            {
                try
                {
                    tickerService.pushTickersInCategories();
                }
                catch(Exception ex) {
                    ExceptionLogger.Log(ex);
                }
            });
        }
    }
}
