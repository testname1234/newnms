﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using Newtonsoft.Json;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;

namespace NMS.TestApp
{
    public class AnyThings
    {
        public void AddResources()
        {

            //string ResourceStr = "src=\"/content/images/template1.jpg\"";
            //ResourceStr += "src=\"/content/images/template2.jpg\"";
            //ResourceStr += "src=\"/content/images/template3.jpg\"";
            //ResourceStr += "src=\"/content/images/template4.jpg\"";

            string ResourceStr = "";
            
            MatchCollection collection = Regex.Matches(ResourceStr, "src=(\"|').*?(jpg|gif|png|mp4|flv|jpeg)", RegexOptions.IgnoreCase);
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            List<string> ResourceGuid = new List<string>();
            List<ScrapResource> ScrapResources = new List<ScrapResource>();
            if (collection.Count > 0)
            {
                foreach (var match in collection)
                {
                    string urlPrefix = "";
                    MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                    ScrapResource srapresource = new ScrapResource();
                    resource.ResourceStatusId = ((int)ResourceStatuses.Completed).ToString();
                    resource.Source = urlPrefix + match.ToString().Replace("src=\"", "").Replace("\"", "").Replace("src='", "").Replace("'", "");
                    if (!Regex.Match(resource.Source, ".*(mp4|flv)", RegexOptions.IgnoreCase).Success)
                    {
                        resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                    }
                    else
                    {
                        resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                    }
                    resource.IsFromExternalSource = true;

                    srapresource.Source = resource.Source;
                    srapresource.StatusId = Convert.ToInt32(resource.ResourceStatusId);
                    srapresource.DownloadedFilePath = resource.Source;

                    HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", resource, null);
                    using (TextReader treader = new StreamReader(response.GetResponseStream()))
                    {
                        string str = treader.ReadToEnd();
                        MS.Core.Entities.Resource res = JsonConvert.DeserializeObject<MS.Core.Entities.Resource>(str);
                        //ResourceGuid.Add(res.Guid.ToString());
                        srapresource.ResourceGuid = res.Guid;
                    }
                    response.Close();

                    ScrapResources.Add(srapresource);
                }
            }



            // Upload Images
            try
            {
                //ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                WebClient client = new WebClient();
                HttpWebRequestHelper proxy = new HttpWebRequestHelper();
                //IResourceService resourceService = IoC.Resolve<IResourceService>("MSResourceService");
                //var resources =  resourceService.GetScrapResourceByStatusId(ScrapResourceStatuses.Downloaded);

                if (ScrapResources != null && ScrapResources.Count > 0)
                {
                    int successfull = 0;
                    foreach (var res in ScrapResources)
                    {
                        try
                        {                            
                            string ext = System.IO.Path.GetExtension(res.Source).Trim('.');
                            string fileName = System.IO.Path.GetFileName(res.Source);
                            byte[] data = client.DownloadData(Environment.CurrentDirectory + res.DownloadedFilePath);
                            client.Dispose();
                            proxy.HttpUploadFile(NMS.Core.AppSettings.MediaServerUrl + "/PostMedia?Id=" + res.ResourceGuid.ToString() + "&IsHD=true", fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());

                            successfull++;
                        }
                        catch (Exception exp)
                        {

                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
        }


    }
}
