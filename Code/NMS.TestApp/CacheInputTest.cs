﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMS.CacheRepository;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.MongoRepository;

namespace NMS.TestApp
{
    public class CacheInputTest
    {

        public CacheInitialDataInput SetInputParamaters(DateTime AliveTime, DateTime ActivityTime, DateTime From, DateTime To, List<Filter> filters)
        {

            CacheInitialDataInput oCacheKeyObject = new CacheInitialDataInput();
            oCacheKeyObject.Data = null;
            oCacheKeyObject.ActivityTime = ActivityTime;
            oCacheKeyObject.AliveTime = AliveTime;
            oCacheKeyObject.Input = new CacheInputRequest { Filters = filters, From = From, To = To };

            return oCacheKeyObject;
        }

        public string GetCacheKeyNameFromCacheInputParamaters(CacheInputRequest obj)
        {
            string keyName = "FILTERSID_FROM_TO";
            List<Filter> objFilterList = obj.Filters.Where((x => x.FilterTypeId == 13 || x.FilterTypeId == 15)).OrderBy(x => x.FilterId).ToList<Filter>();
            //string filterIDs = string.Join(",", objFilterList.Select(x => x.FilterId.ToString()).ToArray());

            string filterIDs = string.Join(",", obj.Filters.Where((x => x.FilterTypeId == 13 || x.FilterTypeId == 15)).OrderBy(x => x.FilterId).Select(x => x.FilterId.ToString()).ToArray());

            keyName = keyName.Replace("FILTERSID", filterIDs);
            keyName = keyName.Replace("FROM", obj.From.ToShortDateString());
            keyName = keyName.Replace("TO", obj.To.ToShortDateString());

            return keyName;
        }

        public void Run()
        {
           
            string keyName = string.Empty;
            //CacheInitialDataInput obj = SetInputParamaters(DateTime.Now, DateTime.Now.AddDays(5),
            //                                            Convert.ToDateTime("06/12/2014"), Convert.ToDateTime("06/18/2014"),
             //                                           new List<int> { 12, 4, 67, 8, 9 });



            List<Filter> lstFilter = new List<Filter> { new Filter { FilterTypeId=7, FilterId =171}, new Filter { FilterTypeId=13, FilterId =81}, new Filter { FilterTypeId=15, FilterId =1},
                new Filter { FilterTypeId=15, FilterId =121}, new Filter { FilterTypeId=13, FilterId =11},new Filter { FilterTypeId=7, FilterId =11}};
            
            //NMS.CacheRepository.FilterCountRepository objRep = new CacheRepository.FilterCountRepository();
            //CacheInitialDataInput obj = objRep.SetInputParamaters(DateTime.Now, DateTime.Now.AddDays(5),
            //                                            Convert.ToDateTime("06/12/2014"), Convert.ToDateTime("06/18/2014"), lstFilter);
                                                       
            
            //keyName = GetCacheKeyNameFromCacheInputParamaters(obj.Input);
            //obj = null;
            Console.WriteLine(keyName);
           
            /*
            keyName = string.Empty;
            obj = SetInputParamaters(DateTime.Now, DateTime.Now.AddDays(5),
                                                        Convert.ToDateTime("06/1/2014"), Convert.ToDateTime("06/30/2014"),
                                                        new List<int> { 12, 4, 9 });

            keyName = GetCacheKeyNameFromCacheInputParamaters(obj.Input);
            Console.WriteLine(keyName);
             */
           
        }

        public void RunPolling()
        {

            INewsService service = IoC.Resolve<INewsService>("NewsService");
            service.ProcessProducerDataPolling();

        }


        public void TestBunchNewsGroup()
        {

            /*
            MBunchNewsRepository objRepo = new MBunchNewsRepository();
         //   objRepo.GetBunchNewsGroupByBunchId(System.DateTime.UtcNow.AddDays(-300));

            BunchCountRepository objBunch = new BunchCountRepository();
            objBunch.PollBunchCount();
            */

            IBunchCountService service = IoC.Resolve<IBunchCountService>("BunchCountService");
            service.PollBunchCount();
        }


        public void GetUserChatMessages (int userId)
        {
            IMessageService messageService = IoC.Resolve<IMessageService>("MessageService");
            List<Message> lstMessages = messageService.GetAllUserMessages(userId);

            List<Message> lstMessagesRecieved = messageService.GetAllUserPendingMessages(userId, new List<int> { 7, 8, 9, 10 });

            
            int x=0;
        }




        
        
    }
}
