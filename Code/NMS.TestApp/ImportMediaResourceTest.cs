﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Entities;
using MS.Core.Enums;
using MsMedia.Core.Entities;
using MsMedia.Repository;
using Newtonsoft.Json;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.ProcessThreads;
using NMS.Repository;
using NMS.Service;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

namespace NMS.TestApp
{
    [Serializable]
    public class ArchivalNews
    {
        public int ResourceId { get; set; }
        public int FilterTypeId { get; set; }
        public string Barcode { get; set; }
        public int LanguageCode { get; set; }
        public List<NMS.Core.Entities.Resource> ImageGuid { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<string> Categories { get; set; }
        public List<string> Tags { get; set; }
        public string Caption { get; set; }
        public string Location { get; set; }
        public DateTime? NewsDate { get; set; }
        public double? duration { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    //sqlMigrationRepository.BulkInsert<TagResource>(tagResources, SQLEntityName, ignoreColumnList);







    public class ImportMediaResourceTest
    {

        void BulkInsert<T>(List<T> obj, string collectionName, List<string> ignoreColumnList)
        {
            DataTable table = obj.ToDataTable(ignoreColumnList);

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy("", SqlBulkCopyOptions.KeepIdentity))
            {
                bulkCopy.DestinationTableName = "dbo." + collectionName;
                bulkCopy.WriteToServer(table);
            }
        }

        public void Execute()
        {
            //# region InterfaceObjects

            ResourceDetailRepository resourceDetailService = new ResourceDetailRepository();
            ImageDetailRepository imageDetailService = new ImageDetailRepository();
            RdCelebrityRepository rdcelebserv = new RdCelebrityRepository();
            RdEnvironmentRepository rdenvService = new RdEnvironmentRepository();
            RdEventRepository rdeventService = new RdEventRepository();
            RdKeywordRepository rdkeywordvService = new RdKeywordRepository();
            RdLocationRepository rdlocationService = new RdLocationRepository();
            RdMiscellaneousRepository rdmiscService = new RdMiscellaneousRepository();
            RdNatureRepository rdnatureService = new RdNatureRepository();
            RdSourceRepository rdsourceService = new RdSourceRepository();
            RdTechnologyRepository rdtechService = new RdTechnologyRepository();
            MsMedia.Repository.CelebrityRepository celebserv = new MsMedia.Repository.CelebrityRepository();
            CelebrityAgeGroupRepository celebagserv = new CelebrityAgeGroupRepository();
            CelebrityBodyLanguageRepository celebblserv = new CelebrityBodyLanguageRepository();
            CelebrityDepartmentRepository celebdeptserv = new CelebrityDepartmentRepository();
            CelebrityDesignationRepository celebdesgserv = new CelebrityDesignationRepository();
            CelebrityDressRepository celebdressserv = new CelebrityDressRepository();
            CelebrityExpressionRepository celebesserv = new CelebrityExpressionRepository();
            CelebrityFaceLookRepository celebflserv = new CelebrityFaceLookRepository();
            CelebrityHeadLookRepository celebhlserv = new CelebrityHeadLookRepository();
            CelebrityNativeRepository celebnsserv = new CelebrityNativeRepository();
            CelebrityPoliticalPartyRepository celebppserv = new CelebrityPoliticalPartyRepository();
            CelebrityProfessionRepository celebprofserv = new CelebrityProfessionRepository();
            CelebrityReligionRepository celebreligserv = new CelebrityReligionRepository();
            CelebritySectRepository celebsectserv = new CelebritySectRepository();
            CelebrityStatusRepository celebstatusserv = new CelebrityStatusRepository();
            KeywordRepository celebkeyserv = new KeywordRepository();
            LocationAreaRepository celebareaserv = new LocationAreaRepository();
            LocationBuildingRepository celebbuildserv = new LocationBuildingRepository();
            LocationBuildingTypeRepository celebbuildtypeserv = new LocationBuildingTypeRepository();
            LocationCountryRepository celebcountserv = new LocationCountryRepository();
            LocationCityRepository celebcityserv = new LocationCityRepository();
            LocationProvinceRepository celebprovserv = new LocationProvinceRepository();
            MiscellaneousRepository celebmiscserv = new MiscellaneousRepository();
            MsMedia.Repository.ResourceRepository resourceService = new MsMedia.Repository.ResourceRepository();
            EnvironmentDayPartRepository envdayService = new EnvironmentDayPartRepository();
            EnvironmentSeasonRepository envseasonService = new EnvironmentSeasonRepository(); ;
            EnvironmentWeatherRepository envweatherService = new EnvironmentWeatherRepository();
            EventDetailRepository eventdetailService = new EventDetailRepository();
            EventTypeRepository eventtypeservice = new EventTypeRepository();
            NatureRepository natureService = new NatureRepository();
            NatureTypeRepository naturetypeService = new NatureTypeRepository();
            SourceTypeRepository sourcetypeService = new SourceTypeRepository();
            SourceVersionRepository sourceVersionService = new SourceVersionRepository();
            TechnologyRepository techservice = new TechnologyRepository();
            TechnologyTypeRepository techtypeservice = new TechnologyTypeRepository();

            //# endregion 

            ImageDetailRepository idservice = new ImageDetailRepository();
            IdCelebrityRepository idCelebRepository = new IdCelebrityRepository(); 
            IdEnvironmentRepository ideservice = new IdEnvironmentRepository(); 
            IdEventRepository idEventRepo = new IdEventRepository(); 
            IdKeywordRepository idKeywordRepo = new IdKeywordRepository();
            IdLocationRepository idLocationRepo = new IdLocationRepository();  
            IdMiscellaneousRepository idMiscRepo = new IdMiscellaneousRepository(); 
            IdNatureRepository idNatureRepo = new IdNatureRepository(); 
            IdSourceRepository idSourceRepo = new IdSourceRepository(); 
            IdTechnologyRepository idTechRepo = new IdTechnologyRepository(); 



            INewsService newsService = IoC.Resolve<INewsService>("NewsService");
            List<ArchivalNews> lstNews = new List<ArchivalNews>();
            string connectoinstring = ConfigurationManager.ConnectionStrings["conn"].ToString();
            DateTime date = DateTime.Now.AddDays(-1);
            List<MsMedia.Core.Entities.Resource> resources = resourceService.GetResourcesAfterDate(date);
            if (resources != null)
            {
              
                foreach (MsMedia.Core.Entities.Resource resourceitem in resources)
                {
                    List<ResourceDetail> resourcedetail = resourceDetailService.GetAllResourceDetailbyResourceId(resourceitem.ResourceId, connectoinstring);

                    if (resourcedetail != null)
                    {
                        #region Videos
                        foreach (ResourceDetail detail in resourcedetail)
                        {
                            ArchivalNews news = new ArchivalNews();
                            news.Tags = new List<string>();
                            news.Categories = new List<string>();

                            RdCelebrity _rdCelebrity = rdcelebserv.GetRdCelebrityEntityByResourceDetailId(detail.ResourceDetailId);
                            RdEnvironment _rdEnvironment = rdenvService.GetRdEnvironmentEntityByResourceDetailId(detail.ResourceDetailId);
                            RdEvent _rdRdEvent = rdeventService.GetRdEventEntityByResourceDetailId(detail.ResourceDetailId);
                            RdKeyword _rdKeyword = rdkeywordvService.GetRdKeywordEntityByResourceDetailId(detail.ResourceDetailId);
                            List<RdLocation> _listrdLocation = rdlocationService.GetRdLocationByResourceDetailId(detail.ResourceDetailId);
                            RdLocation _rdLocation = null;
                            if (_listrdLocation != null)
                            {
                                _rdLocation = rdlocationService.GetRdLocationByResourceDetailId(detail.ResourceDetailId).LastOrDefault();
                            }

                            RdMiscellaneous _rdMiscellaneous = rdmiscService.GetRdMiscellaneousEntityByResourceDetailId(detail.ResourceDetailId);
                            RdNature _rdNature = rdnatureService.GetRdNatureEntityByResourceDetailId(detail.ResourceDetailId);
                            RdSource _rdSource = rdsourceService.GetRdSourceEntityByResourceDetailId(detail.ResourceDetailId);
                            RdTechnology _rdTechnology = rdtechService.GetRdTechnologyEntityByResourceDetailId(detail.ResourceDetailId);

                            MsMedia.Core.Entities.Celebrity celeb = null;
                            CelebrityAgeGroup _CelebrityAgeGroup = null;
                            CelebrityBodyLanguage _CelebrityBodyLanguage = null;
                            CelebrityDepartment _CelebrityDepartment = null;
                            CelebrityDesignation _CelebrityDesignation = null;
                            CelebrityDress _CelebrityDress = null;
                            CelebrityExpression _CelebrityExpression = null;
                            CelebrityFaceLook _CelebrityFaceLook = null;
                            CelebrityHeadLook _CelebrityHeadLook = null;
                            CelebrityNative _CelebrityNative = null;
                            CelebrityPoliticalParty _CelebrityPoliticalParty = null;
                            CelebrityProfession _CelebrityProfession = null;
                            CelebrityReligion _CelebrityReligion = null;
                            CelebritySect _CelebritySect = null;
                            CelebrityStatus _CelebrityStatus = null;

                            if (_rdCelebrity != null)
                            {
                                celeb = celebserv.GetCelebrity(Convert.ToInt32(_rdCelebrity.CelebrityId));

                                _CelebrityAgeGroup = celebagserv.GetCelebrityAgeGroup(Convert.ToInt32(_rdCelebrity.CelebrityAgeGroupId));
                                _CelebrityBodyLanguage = celebblserv.GetCelebrityBodyLanguage(Convert.ToInt32(_rdCelebrity.CelebrityBodyLanguageId));
                                _CelebrityDepartment = celebdeptserv.GetCelebrityDepartment(Convert.ToInt32(_rdCelebrity.CelebrityDepartmentId));
                                _CelebrityDesignation = celebdesgserv.GetCelebrityDesignation(Convert.ToInt32(_rdCelebrity.CelebrityDesignationId));
                                _CelebrityDress = celebdressserv.GetCelebrityDress(Convert.ToInt32(_rdCelebrity.CelebrityDressId));
                                _CelebrityExpression = celebesserv.GetCelebrityExpression(Convert.ToInt32(_rdCelebrity.CelebrityExpressionId));
                                _CelebrityFaceLook = celebflserv.GetCelebrityFaceLook(Convert.ToInt32(_rdCelebrity.CelebrityFaceLookId));
                                _CelebrityHeadLook = celebhlserv.GetCelebrityHeadLook(Convert.ToInt32(_rdCelebrity.CelebrityHeadLookId));
                                _CelebrityNative = celebnsserv.GetCelebrityNative(Convert.ToInt32(_rdCelebrity.CelebrityNativeId));
                                _CelebrityPoliticalParty = celebppserv.GetCelebrityPoliticalParty(Convert.ToInt32(_rdCelebrity.CelebrityPoliticalPartyId));
                                _CelebrityProfession = celebprofserv.GetCelebrityProfession(Convert.ToInt32(_rdCelebrity.CelebrityProfessionId));
                                _CelebrityReligion = celebreligserv.GetCelebrityReligion(Convert.ToInt32(_rdCelebrity.CelebrityReligionId));
                                _CelebritySect = celebsectserv.GetCelebritySect(Convert.ToInt32(_rdCelebrity.CelebritySectId));
                                _CelebrityStatus = celebstatusserv.GetCelebrityStatus(Convert.ToInt32(_rdCelebrity.CelebrityStatusId));

                                if (celeb != null)
                                {

                                    news.Tags.Add(celeb.Name);
                                }

                                if (_CelebrityBodyLanguage != null)
                                {
                                    news.Tags.Add(_CelebrityBodyLanguage.Name);
                                }

                                if (_CelebrityDress != null)
                                {
                                    news.Tags.Add(_CelebrityDress.Name);
                                }
                                if (_CelebrityExpression != null)
                                {

                                    news.Tags.Add(_CelebrityExpression.Name);
                                }
                                if (_CelebrityFaceLook != null)
                                {
                                    news.Tags.Add(_CelebrityFaceLook.Name);
                                }
                                if (_CelebrityHeadLook != null)
                                {
                                    news.Tags.Add(_CelebrityHeadLook.Name);
                                }
                                if (_CelebrityAgeGroup != null)
                                {
                                    news.Tags.Add(_CelebrityAgeGroup.Name);
                                }
                                if (_CelebrityDepartment != null)
                                {
                                    news.Tags.Add(_CelebrityDepartment.Name);
                                }
                                if (_CelebrityDesignation != null)
                                {
                                    news.Tags.Add(_CelebrityDesignation.Name);
                                }
                                if (_CelebrityNative != null)
                                {
                                    news.Tags.Add(_CelebrityNative.Name);
                                }
                                if (_CelebrityPoliticalParty != null)
                                {
                                    news.Tags.Add(_CelebrityPoliticalParty.Name);
                                }
                                if (_CelebrityProfession != null)
                                {
                                    news.Tags.Add(_CelebrityProfession.Name);
                                }
                                if (_CelebrityReligion != null)
                                {
                                    news.Tags.Add(_CelebrityReligion.Name);
                                }
                                if (_CelebritySect != null)
                                {
                                    news.Tags.Add(_CelebritySect.Name);
                                }
                                if (_CelebrityStatus != null)
                                {
                                    news.Tags.Add(_CelebrityStatus.Name);
                                }


                            }


                            Keyword _Keyword = null;
                            if (_rdKeyword != null)
                            {
                                _Keyword = celebkeyserv.GetKeyword(Convert.ToInt32(_rdKeyword.KeywordId));
                                if (_Keyword != null)
                                {
                                    news.Tags.Add(_Keyword.Name);
                                    news.Categories.Add(_Keyword.Name);
                                }

                            }

                            LocationArea _LocationArea = null;
                            LocationBuilding _LocationBuilding = null;
                            LocationBuildingType _LocationBuildingType = null;
                            LocationCity _LocationCity = null;
                            LocationCountry _LocationCountry = null;
                            LocationProvince _LocationProvince = null;
                            if (_rdLocation != null)
                            {
                                _LocationArea = celebareaserv.GetLocationArea(Convert.ToInt32(_rdLocation.LocationAreaId));
                                _LocationBuilding = celebbuildserv.GetLocationBuilding(Convert.ToInt32(_rdLocation.LocationBuildingId));
                                _LocationBuildingType = celebbuildtypeserv.GetLocationBuildingType(Convert.ToInt32(_rdLocation.LocationBuildingTypeId));
                                _LocationCountry = celebcountserv.GetLocationCountry(Convert.ToInt32(_rdLocation.LocationCountryId));
                                _LocationProvince = celebprovserv.GetLocationProvince(Convert.ToInt32(_rdLocation.LocationProvinceId));
                                _LocationCity = celebcityserv.GetLocationCity(Convert.ToInt32(_rdLocation.LocationCityId));



                                if (_LocationCountry != null)
                                {
                                    if (string.IsNullOrEmpty(_LocationCountry.Name))
                                    {
                                        news.Location = _LocationCountry.Name;
                                    }
                                }

                                if (_LocationProvince != null)
                                {
                                    if (string.IsNullOrEmpty(_LocationProvince.Name))
                                    {
                                        news.Location = _LocationProvince.Name;
                                    }
                                }

                                if (_LocationCity != null)
                                {
                                    if (string.IsNullOrEmpty(_LocationCity.Name))
                                    {
                                        news.Location = _LocationCity.Name;
                                    }
                                }

                                if (_LocationArea != null)
                                {
                                    if (string.IsNullOrEmpty(_LocationArea.Name))
                                    {
                                        news.Location = _LocationArea.Name;
                                    }
                                }

                                if (_LocationBuilding != null)
                                {
                                    if (string.IsNullOrEmpty(_LocationBuilding.Name))
                                    {
                                        news.Location = _LocationBuilding.Name;
                                    }
                                }
                            }

                            Miscellaneous _Miscellaneous = null;


                            if (_rdMiscellaneous != null)
                            {
                                _Miscellaneous = celebmiscserv.GetMiscellaneous(Convert.ToInt32(_rdMiscellaneous.MiscellaneousId));
                                if (_Miscellaneous != null)
                                {
                                    news.Tags.Add(_Miscellaneous.Name);
                                }

                            }

                            EnvironmentDayPart _Environment = null;
                            ////  EnvironmentSeason _EnvironmentSeason = null;
                            //   EnvironmentWeather  _EnvironmentWeather = null;

                            if (_rdEnvironment != null)
                            {
                                _Environment = envdayService.GetEnvironmentDayPart(Convert.ToInt32(_rdEnvironment.EnvironmentDayPartId));
                                if (_Environment != null)
                                {
                                    news.Tags.Add(_Environment.Name);
                                }
                            }

                            EventDetail _EventDetail = null;
                            EventType _EventType = null;
                            if (_rdRdEvent != null)
                            {
                                _EventDetail = eventdetailService.GetEventDetail(Convert.ToInt32(_rdRdEvent.EventDetailId));
                                if (_EventDetail != null)
                                {
                                    news.Tags.Add(_EventDetail.Name);
                                }
                                _EventType = eventtypeservice.GetEventType(Convert.ToInt32(_rdRdEvent.EventTypeId));
                                if (_EventType != null)
                                {
                                    news.Tags.Add(_EventType.Name);
                                }

                            }


                            Nature _Nature = null;
                            NatureType _NatureType = null;
                            if (_rdNature != null)
                            {
                                _Nature = natureService.GetNature(Convert.ToInt32(_rdNature.NatureId));
                                if (_Nature != null)
                                {
                                    news.Tags.Add(_Nature.Name);
                                }

                                _NatureType = naturetypeService.GetNatureType(Convert.ToInt32(_rdNature.NatureTypeId));

                                if (_NatureType != null)
                                {
                                    news.Tags.Add(_NatureType.Name);
                                }
                            }

                            SourceVersion _SourceVersion = null;
                            SourceType _SourceType = null;
                            if (_rdSource != null)
                            {
                                _SourceVersion = sourceVersionService.GetSourceVersion(Convert.ToInt32(_rdSource.SourceVersionId));

                                if (_SourceVersion != null)
                                {
                                    news.Tags.Add(_SourceVersion.Name);
                                }

                                _SourceType = sourcetypeService.GetSourceType(Convert.ToInt32(_rdSource.SourceTypeId));

                                if (_SourceType != null)
                                {
                                    news.Tags.Add(_SourceType.Name);
                                }
                            }

                            Technology _Technology = null;
                            TechnologyType _TechnologyType = null;
                            if (_rdTechnology != null)
                            {
                                _Technology = techservice.GetTechnology(Convert.ToInt32(_rdTechnology.TechnologyId));

                                if (_Technology != null)
                                {
                                    news.Tags.Add(_Technology.Name);
                                }

                                _TechnologyType = techtypeservice.GetTechnologyType(Convert.ToInt32(_rdTechnology.TechnologyTypeId));

                                if (_TechnologyType != null)
                                {
                                    news.Tags.Add(_TechnologyType.Name);
                                }

                            }

                            if (news.Categories.Count < 1)
                            {
                                news.Categories = new List<string>();
                                news.Categories.Add("Others");
                            }
                            if (!string.IsNullOrEmpty(detail.Slug))
                            {
                                news.Title = detail.Slug;
                            }
                            else
                            {
                                news.Title = celeb.Name;
                            }

                            news.ResourceId = detail.ResourceId;
                            news.ImageGuid = new List<Core.Entities.Resource>();
                            NMS.Core.Entities.Resource res = new Core.Entities.Resource();

                            res.CopyFrom(resourceitem);
                            news.ImageGuid.Add(res);
                            news.Description = detail.Description;
                            news.Barcode = detail.Barcode.ToString();
                            news.FilterTypeId = 0;
                            news.LanguageCode = 1;
                            news.NewsDate = detail.ResourceDate;
                            news.CreatedDate = detail.ResourceDate;
                            news.UpdatedDate = DateTime.UtcNow;
                            lstNews.Add(news);

                        }
                        #endregion
                    }
                    else
                    {
                        #region Images

                        List<ImageDetail> imagedetail = imageDetailService.GetAllImageDetailbyResourceId(resourceitem.ResourceId, connectoinstring);

                        if (imagedetail != null)
                        {
                            foreach (ImageDetail detail in imagedetail)
                            {
                                ArchivalNews news = new ArchivalNews();
                                news.Tags = new List<string>();
                                news.Categories = new List<string>();

                                IdCelebrity _rdCelebrity = idCelebRepository.GetIdCelebrityEntityByImageDetailId(detail.ImageDetailId);
                                IdEnvironment _rdEnvironment = ideservice.GetIdEnvironmentEntityByImageDetailId(detail.ImageDetailId);
                                IdEvent _rdRdEvent = idEventRepo.GetIdEventEntityByImageDetailId(detail.ImageDetailId);
                                IdKeyword _rdKeyword = idKeywordRepo.GetIdKeywordEntityByImageDetailId(detail.ImageDetailId);
                                List<IdLocation> _listrdLocation = idLocationRepo.GetIdLocationByImageDetailId(detail.ImageDetailId);
                                IdLocation _rdLocation = null;
                                if (_listrdLocation != null)
                                {
                                    _rdLocation = idLocationRepo.GetIdLocationByImageDetailId(detail.ImageDetailId).LastOrDefault();
                                }

                                IdMiscellaneous _rdMiscellaneous = idMiscRepo.GetIdMiscellaneousEntityByImageDetailId(detail.ImageDetailId);
                                IdNature _rdNature = idNatureRepo.GetIdNatureEntityByImageDetailId(detail.ImageDetailId);
                                IdSource _rdSource = idSourceRepo.GetIdSourceEntityByImageDetailId(detail.ImageDetailId);
                                IdTechnology _rdTechnology = idTechRepo.GetIdTechnologyEntityByImageDetailId(detail.ImageDetailId);

                                MsMedia.Core.Entities.Celebrity celeb = null;
                                CelebrityAgeGroup _CelebrityAgeGroup = null;
                                CelebrityBodyLanguage _CelebrityBodyLanguage = null;
                                CelebrityDepartment _CelebrityDepartment = null;
                                CelebrityDesignation _CelebrityDesignation = null;
                                CelebrityDress _CelebrityDress = null;
                                CelebrityExpression _CelebrityExpression = null;
                                CelebrityFaceLook _CelebrityFaceLook = null;
                                CelebrityHeadLook _CelebrityHeadLook = null;
                                CelebrityNative _CelebrityNative = null;
                                CelebrityPoliticalParty _CelebrityPoliticalParty = null;
                                CelebrityProfession _CelebrityProfession = null;
                                CelebrityReligion _CelebrityReligion = null;
                                CelebritySect _CelebritySect = null;
                                CelebrityStatus _CelebrityStatus = null;

                                if (_rdCelebrity != null)
                                {
                                    celeb = celebserv.GetCelebrity(Convert.ToInt32(_rdCelebrity.CelebrityId));

                                    _CelebrityAgeGroup = celebagserv.GetCelebrityAgeGroup(Convert.ToInt32(_rdCelebrity.CelebrityAgeGroupId));
                                    _CelebrityBodyLanguage = celebblserv.GetCelebrityBodyLanguage(Convert.ToInt32(_rdCelebrity.CelebrityBodyLanguageId));
                                    _CelebrityDepartment = celebdeptserv.GetCelebrityDepartment(Convert.ToInt32(_rdCelebrity.CelebrityDepartmentId));
                                    _CelebrityDesignation = celebdesgserv.GetCelebrityDesignation(Convert.ToInt32(_rdCelebrity.CelebrityDesignationId));
                                    _CelebrityDress = celebdressserv.GetCelebrityDress(Convert.ToInt32(_rdCelebrity.CelebrityDressId));
                                    _CelebrityExpression = celebesserv.GetCelebrityExpression(Convert.ToInt32(_rdCelebrity.CelebrityExpressionId));
                                    _CelebrityFaceLook = celebflserv.GetCelebrityFaceLook(Convert.ToInt32(_rdCelebrity.CelebrityFaceLookId));
                                    _CelebrityHeadLook = celebhlserv.GetCelebrityHeadLook(Convert.ToInt32(_rdCelebrity.CelebrityHeadLookId));
                                    _CelebrityNative = celebnsserv.GetCelebrityNative(Convert.ToInt32(_rdCelebrity.CelebrityNativeId));
                                    _CelebrityPoliticalParty = celebppserv.GetCelebrityPoliticalParty(Convert.ToInt32(_rdCelebrity.CelebrityPoliticalPartyId));
                                    _CelebrityProfession = celebprofserv.GetCelebrityProfession(Convert.ToInt32(_rdCelebrity.CelebrityProfessionId));
                                    _CelebrityReligion = celebreligserv.GetCelebrityReligion(Convert.ToInt32(_rdCelebrity.CelebrityReligionId));
                                    _CelebritySect = celebsectserv.GetCelebritySect(Convert.ToInt32(_rdCelebrity.CelebritySectId));
                                    _CelebrityStatus = celebstatusserv.GetCelebrityStatus(Convert.ToInt32(_rdCelebrity.CelebrityStatusId));

                                    if (celeb != null)
                                    {

                                        news.Tags.Add(celeb.Name);
                                    }

                                    if (_CelebrityBodyLanguage != null)
                                    {
                                        news.Tags.Add(_CelebrityBodyLanguage.Name);
                                    }

                                    if (_CelebrityDress != null)
                                    {
                                        news.Tags.Add(_CelebrityDress.Name);
                                    }
                                    if (_CelebrityExpression != null)
                                    {

                                        news.Tags.Add(_CelebrityExpression.Name);
                                    }
                                    if (_CelebrityFaceLook != null)
                                    {
                                        news.Tags.Add(_CelebrityFaceLook.Name);
                                    }
                                    if (_CelebrityHeadLook != null)
                                    {
                                        news.Tags.Add(_CelebrityHeadLook.Name);
                                    }
                                    if (_CelebrityAgeGroup != null)
                                    {
                                        news.Tags.Add(_CelebrityAgeGroup.Name);
                                    }
                                    if (_CelebrityDepartment != null)
                                    {
                                        news.Tags.Add(_CelebrityDepartment.Name);
                                    }
                                    if (_CelebrityDesignation != null)
                                    {
                                        news.Tags.Add(_CelebrityDesignation.Name);
                                    }
                                    if (_CelebrityNative != null)
                                    {
                                        news.Tags.Add(_CelebrityNative.Name);
                                    }
                                    if (_CelebrityPoliticalParty != null)
                                    {
                                        news.Tags.Add(_CelebrityPoliticalParty.Name);
                                    }
                                    if (_CelebrityProfession != null)
                                    {
                                        news.Tags.Add(_CelebrityProfession.Name);
                                    }
                                    if (_CelebrityReligion != null)
                                    {
                                        news.Tags.Add(_CelebrityReligion.Name);
                                    }
                                    if (_CelebritySect != null)
                                    {
                                        news.Tags.Add(_CelebritySect.Name);
                                    }
                                    if (_CelebrityStatus != null)
                                    {
                                        news.Tags.Add(_CelebrityStatus.Name);
                                    }


                                }


                                Keyword _Keyword = null;
                                if (_rdKeyword != null)
                                {
                                    _Keyword = celebkeyserv.GetKeyword(Convert.ToInt32(_rdKeyword.KeywordId));
                                    if (_Keyword != null)
                                    {
                                        news.Tags.Add(_Keyword.Name);
                                        news.Categories.Add(_Keyword.Name);
                                    }

                                }

                                LocationArea _LocationArea = null;
                                LocationBuilding _LocationBuilding = null;
                                LocationBuildingType _LocationBuildingType = null;
                                LocationCity _LocationCity = null;
                                LocationCountry _LocationCountry = null;
                                LocationProvince _LocationProvince = null;
                                if (_rdLocation != null)
                                {
                                    _LocationArea = celebareaserv.GetLocationArea(Convert.ToInt32(_rdLocation.LocationAreaId));
                                    _LocationBuilding = celebbuildserv.GetLocationBuilding(Convert.ToInt32(_rdLocation.LocationBuildingId));
                                    _LocationBuildingType = celebbuildtypeserv.GetLocationBuildingType(Convert.ToInt32(_rdLocation.LocationBuildingTypeId));
                                    _LocationCountry = celebcountserv.GetLocationCountry(Convert.ToInt32(_rdLocation.LocationCountryId));
                                    _LocationProvince = celebprovserv.GetLocationProvince(Convert.ToInt32(_rdLocation.LocationProvinceId));
                                    _LocationCity = celebcityserv.GetLocationCity(Convert.ToInt32(_rdLocation.LocationCityId));



                                    if (_LocationCountry != null)
                                    {
                                        if (string.IsNullOrEmpty(_LocationCountry.Name))
                                        {
                                            news.Location = _LocationCountry.Name;
                                        }
                                    }

                                    if (_LocationProvince != null)
                                    {
                                        if (string.IsNullOrEmpty(_LocationProvince.Name))
                                        {
                                            news.Location = _LocationProvince.Name;
                                        }
                                    }

                                    if (_LocationCity != null)
                                    {
                                        if (string.IsNullOrEmpty(_LocationCity.Name))
                                        {
                                            news.Location = _LocationCity.Name;
                                        }
                                    }

                                    if (_LocationArea != null)
                                    {
                                        if (string.IsNullOrEmpty(_LocationArea.Name))
                                        {
                                            news.Location = _LocationArea.Name;
                                        }
                                    }

                                    if (_LocationBuilding != null)
                                    {
                                        if (string.IsNullOrEmpty(_LocationBuilding.Name))
                                        {
                                            news.Location = _LocationBuilding.Name;
                                        }
                                    }
                                }

                                Miscellaneous _Miscellaneous = null;


                                if (_rdMiscellaneous != null)
                                {
                                    _Miscellaneous = celebmiscserv.GetMiscellaneous(Convert.ToInt32(_rdMiscellaneous.MiscellaneousId));
                                    if (_Miscellaneous != null)
                                    {
                                        news.Tags.Add(_Miscellaneous.Name);
                                    }

                                }

                                EnvironmentDayPart _Environment = null;
                                ////  EnvironmentSeason _EnvironmentSeason = null;
                                //   EnvironmentWeather  _EnvironmentWeather = null;

                                if (_rdEnvironment != null)
                                {
                                    _Environment = envdayService.GetEnvironmentDayPart(Convert.ToInt32(_rdEnvironment.EnvironmentDayPartId));
                                    if (_Environment != null)
                                    {
                                        news.Tags.Add(_Environment.Name);
                                    }
                                }

                                EventDetail _EventDetail = null;
                                EventType _EventType = null;
                                if (_rdRdEvent != null)
                                {
                                    _EventDetail = eventdetailService.GetEventDetail(Convert.ToInt32(_rdRdEvent.EventDetailId));
                                    if (_EventDetail != null)
                                    {
                                        news.Tags.Add(_EventDetail.Name);
                                    }
                                    _EventType = eventtypeservice.GetEventType(Convert.ToInt32(_rdRdEvent.EventTypeId));
                                    if (_EventType != null)
                                    {
                                        news.Tags.Add(_EventType.Name);
                                    }

                                }


                                Nature _Nature = null;
                                NatureType _NatureType = null;
                                if (_rdNature != null)
                                {
                                    _Nature = natureService.GetNature(Convert.ToInt32(_rdNature.NatureId));
                                    if (_Nature != null)
                                    {
                                        news.Tags.Add(_Nature.Name);
                                    }

                                    _NatureType = naturetypeService.GetNatureType(Convert.ToInt32(_rdNature.NatureTypeId));

                                    if (_NatureType != null)
                                    {
                                        news.Tags.Add(_NatureType.Name);
                                    }
                                }

                                SourceVersion _SourceVersion = null;
                                SourceType _SourceType = null;
                                if (_rdSource != null)
                                {
                                    _SourceVersion = sourceVersionService.GetSourceVersion(Convert.ToInt32(_rdSource.SourceVersionId));

                                    if (_SourceVersion != null)
                                    {
                                        news.Tags.Add(_SourceVersion.Name);
                                    }

                                    _SourceType = sourcetypeService.GetSourceType(Convert.ToInt32(_rdSource.SourceTypeId));

                                    if (_SourceType != null)
                                    {
                                        news.Tags.Add(_SourceType.Name);
                                    }
                                }

                                Technology _Technology = null;
                                TechnologyType _TechnologyType = null;
                                if (_rdTechnology != null)
                                {
                                    _Technology = techservice.GetTechnology(Convert.ToInt32(_rdTechnology.TechnologyId));

                                    if (_Technology != null)
                                    {
                                        news.Tags.Add(_Technology.Name);
                                    }

                                    _TechnologyType = techtypeservice.GetTechnologyType(Convert.ToInt32(_rdTechnology.TechnologyTypeId));

                                    if (_TechnologyType != null)
                                    {
                                        news.Tags.Add(_TechnologyType.Name);
                                    }

                                }

                                if (news.Categories.Count < 1)
                                {
                                    news.Categories = new List<string>();
                                    news.Categories.Add("Others");
                                }
                                if (!string.IsNullOrEmpty(detail.Slug))
                                {
                                    news.Title = detail.Slug;
                                }
                                else
                                {
                                    news.Title = celeb.Name;
                                }

                                news.ResourceId = detail.ResourceId;
                                news.ImageGuid = new List<Core.Entities.Resource>();
                                NMS.Core.Entities.Resource res = new Core.Entities.Resource();

                                res.CopyFrom(resourceitem);
                                news.ImageGuid.Add(res);
                                news.Description = detail.Description;
                                news.Barcode = detail.Barcode.ToString();
                                news.FilterTypeId = 0;
                                news.LanguageCode = 1;
                                news.NewsDate = detail.ResourceDate;
                                news.CreatedDate = detail.ResourceDate;
                                news.UpdatedDate = DateTime.UtcNow;
                                lstNews.Add(news);
                            }

                        }
                        #endregion
                    }

                }

            }

            try
            {
                var settings = ConfigurationManager.ConnectionStrings["conn"];

                var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(settings, false);
                settings.ConnectionString = "Data Source=10.1.51.173;Initial Catalog=NMS; user id=sa; password=Ax@ct!23";
                connectoinstring = ConfigurationManager.ConnectionStrings["conn"].ToString();



                RawNews news = new RawNews();
                if (lstNews != null)
                {
                    foreach (ArchivalNews archive in lstNews)
                    {
                        news.Title = archive.Title;
                        news.Description = archive.Description;
                        // news.FilterTypeId = "17";


                        news.ImageGuids = archive.ImageGuid;
                        news.LanguageCode = "en";
                        news.Source = "Media Archive";
                        //news.SourceType = FilterTypes.ArchivalSystem;
                        news.Categories = archive.Categories;
                        news.Location = archive.Location;

                        if (archive.NewsDate.HasValue)
                        {
                            news.PublishTime = (DateTime)archive.NewsDate;
                            news.UpdateTime = (DateTime)archive.NewsDate;
                            //news.PublishTime = DateTime.Now;
                            //news.UpdateTime =  DateTime.Now;
                        }
                        // news.Author = item["News_Author"];
                        news.NewsType = NewsTypes.Story;
                        //news.Url = item["URL"];
                        DateTime dt = DateTime.UtcNow;
                        try
                        {
                            InsertNewsStatus status = newsService.InsertRawNews(news);
                        }
                        catch(Exception ex)
                        { 
                        
                        
                        }

                        Console.WriteLine(DateTime.UtcNow.Subtract(dt).TotalSeconds);
                    }
                }

                //INewsService newsService = IoC.Resolve<INewsService>("NewsService");
                //ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
                //IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");

                //HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
                //string testTitle = "";
                //foreach (var _item in jo.Result)
                //{
                //    string NewsTitle = "";
                //    string NewsTitleOrg = "";
                //    string NewsDesc = "";
                //    string CelebrityName = "";
                //    RawNews news = new RawNews();
                //    dynamic item = null;
                //    item = (_item is JArray && _item.Count > 0) ? _item[0] : _item;
                //    if (item != null && ((item is JArray && item.Count > 0) || !(item is JArray)))
                //    {
                //        NewsTitleOrg = WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                //        NewsTitle = CelebrityName + WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                //        news.Title = NewsTitle.Trim();
                //        news.Source = arg.Source;
                //        news.SourceType = (FilterTypes)arg.SourceType;
                //        testTitle = NewsTitleOrg;

                //        if (news.Title.Length > 1 && !newsService.CheckIfNewsExists(news, out newsStatus))
                //        {
                //            news.Categories = new List<string>();

                //            news.Location = item["News_Location"];
                //            if ((item["News_Category"] is JArray))
                //            {
                //                foreach (var category in (item["News_Category"] as JArray))
                //                {
                //                    if (category.ToString().ToLower().Trim() != "home" && category.ToString().Trim() != string.Empty)
                //                        news.Categories.Add(category.ToString().Trim());
                //                }
                //            }
                //            else
                //            {
                //                if (item["News_Category"].ToString().ToLower().Trim() != "home" && item["News_Category"].ToString().Trim() != string.Empty)
                //                    news.Categories.Add(item["News_Category"].ToString().Trim());
                //            }

                //            news.Location = news.Location.Trim().Length > 30 ? "" : news.Location.Trim();

                //            HtmlDocument doc = new HtmlDocument();
                //            HtmlNodeCollection nodecollections = null;
                //            doc.LoadHtml(item["News_Body"].ToString());
                //            HtmlNodeCollection delteNodes = doc.DocumentNode.SelectNodes("//img[contains(@style ,'display:none')]");
                //            if (delteNodes != null && delteNodes.Count > 0)
                //            {
                //                foreach (HtmlNode htmlNode in delteNodes)
                //                {
                //                    htmlNode.Remove();
                //                }
                //            }

                //            HtmlNode VideoNode = doc.DocumentNode.SelectSingleNode("//div//script[1]");
                //            if (VideoNode != null)
                //            {
                //                MatchCollection videoCollection = Regex.Matches(VideoNode.InnerHtml, "http://((?!><).)*?(\\.mp4|\\.flv)", RegexOptions.IgnoreCase);
                //                if (videoCollection != null && videoCollection.Count > 0)
                //                {
                //                    HtmlNode AddNode = new HtmlNode(HtmlNodeType.Element, doc, 0);
                //                    VideoNode.InnerHtml = "<video src=\"" + videoCollection[0].Value + "\"/>";
                //                    doc.DocumentNode.AppendChild(AddNode);
                //                }
                //            }

                //            nodecollections = doc.DocumentNode.SelectNodes("//*[@src]");
                //            if (nodecollections != null && nodecollections.Count > 0)
                //            {
                //                news.ImageGuids = new List<Resource>();
                //                foreach (HtmlNode htmlNode in nodecollections)
                //                {
                //                    MatchCollection matchCollection = Regex.Matches(htmlNode.OuterHtml, RegExpressions.MediaExtraction, RegexOptions.IgnoreCase);

                //                    if (matchCollection.Count > 0)
                //                    {
                //                        foreach (Match match in matchCollection)
                //                        {
                //                            string urlPrefix = "";
                //                            if (!match.ToString().Contains("www.") && !match.ToString().Contains("http"))
                //                            {
                //                                Uri uri = new Uri(jo.Url.ToString());
                //                                urlPrefix = "http://" + uri.Host;
                //                            }
                //                            MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                //                            resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                //                            resource.Source = urlPrefix + match.ToString().Replace("src=\"", "").Replace("\"", "").Replace("src='", "").Replace("'", "");
                //                            if (!Regex.Match(resource.Source, RegExpressions.VideoExtraction, RegexOptions.IgnoreCase).Success)
                //                            {
                //                                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                //                            }
                //                            else
                //                            {
                //                                resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                //                            }
                //                            resource.IsFromExternalSource = true;

                //                            string resourceTitle = "";
                //                            if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["alt"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["alt"].Value))
                //                            {
                //                                resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["alt"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                //                            }
                //                            else if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["title"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["title"].Value))
                //                            {
                //                                resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["title"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                //                            }

                //                            DateTime dtNow = DateTime.Now;
                //                            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                //                            try
                //                            {
                //                                Resource _res = resourceService.MediaPostResource(resource, arg.Source);
                //                                news.ImageGuids.Add(_res);

                //                            }
                //                            catch (Exception ex)
                //                            {
                //                                logService.InsertSystemEventLog(NMS.Core.AppSettings.MediaServerUrl + "/PostResourceError ____" + DateTime.Now.Subtract(dtNow).TotalMilliseconds, resource.Source + "Error Message: " + ex.Message + "Error Strace: " + ex.StackTrace, EventCodes.Error);
                //                            }
                //                        }
                //                    }
                //                }
                //            }


                //            NewsDesc = Regex.Replace(item["News_Body"].ToString().Replace("\n", "").Replace("\r", "").Replace("\t", ""), RegExpressions.ScriptsRemoval, "", RegexOptions.Multiline);
                //            if (arg.Source == "Dawn News")
                //            {
                //                HtmlDocument docDawn = new HtmlDocument();
                //                doc.LoadHtml(item["News_Body"].ToString());
                //                HtmlNodeCollection removenodeCollection = docDawn.DocumentNode.SelectNodes("//a[@class='slideshow__prev'] | //a[@class='slideshow__next']");
                //                if (removenodeCollection != null && removenodeCollection.Count > 0)
                //                {
                //                    foreach (HtmlNode node in removenodeCollection)
                //                    {
                //                        node.Remove();
                //                    }
                //                    NewsDesc = doc.DocumentNode.InnerHtml;
                //                }
                //            }

                //            NewsDesc = WebUtility.HtmlDecode(Regex.Replace(NewsDesc, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                //            if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                //                news.PublishTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                //            if (item["News_Updated_Time"] != null && !String.IsNullOrEmpty(item["News_Updated_Time"].ToString()))
                //                news.UpdateTime = DateTime.Parse(item["News_Updated_Time"].ToString());
                //            else if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                //                news.UpdateTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                //            if (arg.Source == "Twitter")
                //            {
                //                news.Description = NewsTitleOrg.Trim();
                //            }
                //            else
                //            {

                //                news.Description = NewsDesc.Replace("Please turn on JavaScript. Media requires JavaScript to play.", "").Trim();
                //            }

                //            //news.Categories = 
                //            if (news.Categories.Count <= 0)
                //            {
                //                news.Categories.Add("Misc");
                //            }

                //            news.PublishTime = news.PublishTime.ToUniversalTime();
                //            news.UpdateTime = news.UpdateTime.ToUniversalTime();
                //            news.Author = item["News_Author"];
                //            news.LanguageCode = arg.Language;
                //            news.NewsType = NewsTypes.Story;
                //            news.Url = item["URL"];
                //            newsStatus = newsService.InsertRawNews(news);
                //            if (newsStatus == InsertNewsStatus.DiscrepancyCategory)
                //                DiscrepencyCatCount++;
                //            else if (newsStatus == InsertNewsStatus.DiscrepancyLocation)
                //                DiscrepencyLocCount++;
                //            else if (newsStatus == InsertNewsStatus.Inserted)
                //                Newscounter++;
                //            //output.Add(news);
                //        }
                //    }
                //}
            }
            catch (Exception exp)
            {
                //ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                //logService.InsertSystemEventLog(string.Format("ImportNewsThread: Error in {0} {1} {2}", arg.Source, testTitle, exp.Message), exp.StackTrace, EventCodes.Error);
                // return "Error";
            }
        }
    }
}