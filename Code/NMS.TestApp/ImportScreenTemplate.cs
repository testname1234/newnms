﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using MS.Core.Entities;
using MS.Core.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.MongoRepository;
using NMS.Repository;
using NMS.Service;
using NMS.Core;
using NMS.Core.Enums;

namespace NMS.TestApp
{
    public class ImportScreenTemplate
    {
        ScreenElementRepository screenElementRepository = new ScreenElementRepository();
        TemplateScreenElementRepository templateScreenElementRepository = new TemplateScreenElementRepository();
        ScreenTemplateRepository screenTemplateRepository = new ScreenTemplateRepository();
        MosItemRepository mositemRepository=new MosItemRepository();
        ScreenElementRepository screenelementRepository = new ScreenElementRepository();
        MosActiveItemRepository mosActiveItemRepository = new MosActiveItemRepository();
        CasperTemplateRepository casperTemplateRepository = new CasperTemplateRepository();
        CasperTemplateItemRepository casperTemplateItemRepository = new CasperTemplateItemRepository();
        CasperItemTransformationRepository casperItemTransformationRepository = new CasperItemTransformationRepository();


  

        public void SaveTemplate()
        {
           
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            string str = File.ReadAllText(@"C:\UploadToMS\json.txt");
            List<MMS.ScreenTemplateGenerator.ScreenTemplate> lst = new List<MMS.ScreenTemplateGenerator.ScreenTemplate>();
            str = str.Replace("http://localhost:62140/Images", "C:/UploadToMS");
            string[] arr = str.Split(new string[] { "@.@" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string st in arr)
            {
                lst.Add((MMS.ScreenTemplateGenerator.ScreenTemplate)serializer.Deserialize(st.Trim(), typeof(MMS.ScreenTemplateGenerator.ScreenTemplate)));
            }


            var count = casperTemplateRepository.GetCasperTemplateByTemplateName(lst[0].Name);
            string mosString = File.ReadAllText(@"C:\UploadToMS\"+lst[0].Name+".xml");

            mosString = mosString.Replace("</xml>", "");
            mosString = mosString.Replace("<MosItems>", "<MosItems><items>");
            mosString = mosString.Replace("</MosItems>", "</items></MosItems>");
            string testData = mosString;

            XmlSerializer serializers = new XmlSerializer(typeof(RootObject));

            TextReader reader = new StringReader(testData);

            RootObject result = (RootObject)serializers.Deserialize(reader);

            MosItem mosItem = new MosItem();
            mosItem.CopyFrom(result.items[0]);

            if (count == null)
            {
                CasperTemplate CasperTemp = new CasperTemplate();
                CasperTemp.Template = lst[0].Name;
                CasperTemp.ItemCount =Convert.ToInt32(lst[0].Elements.Count);
                var casperTemplate= casperTemplateRepository.InsertCasperTemplate(CasperTemp);
                
                for (int i = 0; i < lst.Count; i++)
                {     
                    ScreenTemplate screenTemplate = ConvertScreenTemplate(lst[i]);
                    screenTemplate.ProgramId = 1087;
                    screenTemplate.Html = arr[i].Trim();
                    screenTemplate.ScreenTemplateId = screenTemplateRepository.InsertScreenTemplate(screenTemplate).ScreenTemplateId;

                   
                    foreach (var ele in screenTemplate.Elements)
                    {

                        var screenElement = screenelementRepository.GetScreenElement(ele.ScreenElementId);
                        var screenElementName = screenElement.Name;

                        CasperTemplateItem CasperTemplateItem = new CasperTemplateItem();
                        CasperTemplateItem.CasperTemplateId = casperTemplate.CasperTemlateId;
                        CasperTemplateItem.ItemName = screenElement.Name;
                        CasperTemplateItem.CreationDate = ele.CreationDate;
                        CasperTemplateItem.Devicename = Convert.ToString(CasperImportScreenTemplate.DeviceName.ToDescription());
                        CasperTemplateItem.LastUpdateDate = ele.LastUpdateDate;
                        CasperTemplateItem.Type = Convert.ToString(CasperImportScreenTemplate.decklink.ToDescription());
                        CasperTemplateItem.Label = Convert.ToString(CasperImportScreenTemplate.decklink.ToDescription());
                        CasperTemplateItem.Name = Convert.ToString(CasperImportScreenTemplate.decklink);
                        CasperTemplateItem.Channel = Convert.ToString(CasperImportScreenTemplate.channel);
                        CasperTemplateItem = casperTemplateItemRepository.InsertCasperTemplateItem(CasperTemplateItem);

                        CasperItemTransformation casperItemTransformation = new CasperItemTransformation();
                        casperItemTransformation.Positionx = ele.Left;
                        casperItemTransformation.Positiony = ele.Top;
                        casperItemTransformation.Scalex = System.Math.Abs(ele.Right - ele.Left);
                        casperItemTransformation.Scaley = System.Math.Abs(ele.Top - ele.Bottom);
                        casperItemTransformation.CasperTemplateItemId = CasperTemplateItem.CasperTemplatItemId;
                        casperItemTransformation.Type = Convert.ToString(CasperImportScreenTemplate.Transformation.ToDescription());
                        casperItemTransformation.Label = Convert.ToString(CasperImportScreenTemplate.Transformation.ToDescription());
                        casperItemTransformation.Name = Convert.ToString(CasperImportScreenTemplate.Transformation.ToDescription());
                        casperItemTransformation = casperItemTransformationRepository.InsertCasperItemTransformation(casperItemTransformation);

                        CasperTemplateItem.CasperTransformationId = casperItemTransformation.CasperItemTransformationId;
                        CasperTemplateItem.Videolayer = casperItemTransformation.CasperItemTransformationId;
                        casperTemplateItemRepository.UpdateCasperTemplateItem(CasperTemplateItem);

                        var countOfTempalte = casperTemplateItemRepository.GetCasperTemplateItemByCasperTemplateIdAndType(casperTemplate.CasperTemlateId,"TEMPLATE");
                        if (countOfTempalte == null)
                        {
                            CasperTemplateItem.Type = Convert.ToString(CasperImportScreenTemplate.Template.ToDescription());
                            CasperTemplateItem.Label = Convert.ToString(CasperImportScreenTemplate.Template.ToDescription());
                            CasperTemplateItem.Name = lst[0].Name;
                            CasperTemplateItem.ItemName = Convert.ToString(CasperImportScreenTemplate.Template.ToDescription());
                            casperTemplateItemRepository.InsertCasperTemplateItem(CasperTemplateItem);
                        }


                        if (screenElementName == Convert.ToString(CasperImportScreenTemplate.Anchor.ToDescription()) || screenElementName == Convert.ToString(CasperImportScreenTemplate.Video.ToDescription()) || screenElementName == Convert.ToString(CasperImportScreenTemplate.Guest.ToDescription()))
                       {
                           MosActiveItem mosActiveItems = new MosActiveItem();

                           if (screenElementName == Convert.ToString(CasperImportScreenTemplate.Anchor.ToDescription()))
                           {
                               var mosActiveId = 63;
                               mosActiveItems = mosActiveItemRepository.GetMosActiveItem(mosActiveId);
                           }
                           else if (screenElementName == Convert.ToString(CasperImportScreenTemplate.Video.ToDescription()))
                           {
                               var mosActiveId = 60;
                               mosActiveItems = mosActiveItemRepository.GetMosActiveItem(mosActiveId);
                           }
                           else if (screenElementName == Convert.ToString(CasperImportScreenTemplate.Guest.ToDescription()))
                           {
                               var mosActiveId = 60;
                               mosActiveItems = mosActiveItemRepository.GetMosActiveItem(mosActiveId);
                           }

                           mosItem.CopyFrom(mosActiveItems);
                           mosItem.Positionx = ele.Left;
                           mosItem.Positiony = ele.Top;
                           mosItem.Scalex = System.Math.Abs(ele.Right - ele.Left);
                           mosItem.Scaley = System.Math.Abs(ele.Top - ele.Bottom);
                           mosItem.Timecode = ele.CreationDateStr;
                           mosItem.CreationDate = Convert.ToDateTime(ele.CreationDateStr);
                           mosItem.LastUpdateDate = Convert.ToDateTime(ele.LastUpdateDate);
                           var mositemId = mositemRepository.InsertMosItem(mosItem);
                           string CaspermosId = Convert.ToString(mositemId.CasperMosItemId);
                           ele.ScreenTemplateId = screenTemplate.ScreenTemplateId;
                          // ele.CasperMosItemId = Convert.ToInt32(CaspermosId);

                           templateScreenElementRepository.InsertTemplateScreenElement(ele);
                       }
                       else
                       {
                           mosItem.Positionx = ele.Left;
                           mosItem.Positiony = ele.Top;
                           mosItem.Scalex = System.Math.Abs(ele.Right - ele.Left);
                           mosItem.Scaley = System.Math.Abs(ele.Top - ele.Bottom);
                           mosItem.Timecode = ele.CreationDateStr;
                           mosItem.CreationDate = Convert.ToDateTime(ele.CreationDateStr);
                           mosItem.LastUpdateDate = Convert.ToDateTime(ele.LastUpdateDate);
                           var mositemId = mositemRepository.InsertMosItem(mosItem);
                           string CaspermosId = Convert.ToString(mositemId.CasperMosItemId);
                           ele.ScreenTemplateId = screenTemplate.ScreenTemplateId;
                          // ele.CasperMosItemId = Convert.ToInt32(CaspermosId);

                           templateScreenElementRepository.InsertTemplateScreenElement(ele);
                       }
                    }

                    Console.WriteLine("Template created successfully TemplateId:{0}, GroupId:{1}", screenTemplate.ScreenTemplateId, screenTemplate.GroupId);
                }
            }

        }

        public ScreenTemplate ConvertScreenTemplate(MMS.ScreenTemplateGenerator.ScreenTemplate template)
        {

            ResourceService resourceService = new ResourceService(null, null);

            List<ScreenElement> defaultElements = screenElementRepository.GetAllScreenElement();
            ScreenTemplate screenTemplate = new ScreenTemplate();
            screenTemplate.CreatonDate = DateTime.UtcNow;
            screenTemplate.LastUpdateDate = screenTemplate.CreatonDate;
            screenTemplate.IsActive = true;
            screenTemplate.Name = template.Name;
            screenTemplate.GroupId = "T_" + string.Join("_", template.Elements.Where(x => defaultElements.Where(y => y.ScreenElementId == x.ScreenElementId).First().AllowDisplay.Value).Select(x => x.ScreenElementId).OrderBy(x => x).Select(x => x.ToString()).ToArray());
            screenTemplate.BackgroundColor = template.Background.Color;
            if (template.Background.RepeatX && template.Background.RepeatY)
            screenTemplate.BackgroundRepeat = "repeat";
            else if (template.Background.RepeatX || template.Background.RepeatY)
            {
                screenTemplate.BackgroundRepeat = template.Background.RepeatX ? "repeat-x" : "repeat-y";
            }

            screenTemplate.Elements = new List<TemplateScreenElement>();
            foreach (var ele in template.Elements)
            {
                TemplateScreenElement element = new TemplateScreenElement();
                element.Bottom = ele.Top + ele.Height;// ele.Bottom;
                element.Top = ele.Top;
                element.Zindex = ele.Zindex;
                element.Right = ele.Left + ele.Width;// ele.Right;
                element.Left = ele.Left;
                element.CreationDate = screenTemplate.CreatonDate;
                element.LastUpdateDate = screenTemplate.CreatonDate;
                element.ScreenElementId = ele.ScreenElementId;
                element.IsActive = true;
                var de = defaultElements.Where(x => x.ScreenElementId == ele.ScreenElementId).First();
                if (!de.ImageGuid.HasValue)
                {
                    de.ImageGuid = resourceService.UploadImageOnMS(ele.ImageUrl);
                    de.LastUpdateDate = DateTime.UtcNow;
                    screenElementRepository.UpdateScreenElement(de);
                    element.ImageGuid = de.ImageGuid.Value;
                }
                screenTemplate.Elements.Add(element);
            }

            screenTemplate.BackgroundImageUrl = resourceService.UploadImageOnMS(template.Background.ImageUrl);
            MMS.ScreenTemplateGenerator.TemplateGenerator generator = new MMS.ScreenTemplateGenerator.TemplateGenerator();
            screenTemplate.ThumbGuid = resourceService.UploadImageOnMS(generator.Generate(template));
            return screenTemplate;
        }

    }


    public class Item
    {

        [XmlElement("casperMosItemId")]
        public int CasperMosItemId { get; set; }
        [XmlElement("type")]
        public string Type { get; set; }
        [XmlElement("label")]
        public string Label { get; set; }
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("channel")]
        public string Channel { get; set; }
        [XmlElement("videoLayer")]
        public int VideoLayer { get; set; }
        [XmlElement("delay")]
        public int Delay { get; set; }
        [XmlElement("duration")]
        public int Duration { get; set; }
        [XmlElement("allowGpi")]
        public bool AllowGpi { get; set; }
        [XmlElement("allowRemoteTriggering")]
        public bool AllowRemoteTriggering { get; set; }
        [XmlElement("remoteTriggerId")]
        public int RemoteTriggerId { get; set; }
        [XmlElement("flashLayer")]
        public int FlashLayer { get; set; }
        [XmlElement("invoke")]
        public int Invoke { get; set; }
        [XmlElement("useStoredData")]
        public bool UseStoredData { get; set; }
        [XmlElement("useuppercasedata")]
        public bool Useuppercasedata { get; set; }
        [XmlElement("color")]
        public string Color { get; set; }
        [XmlElement("transition")]
        public string Transition { get; set; }
        [XmlElement("transitionDuration")]
        public int TransitionDuration { get; set; }
        [XmlElement("tween")]
        public string Tween { get; set; }
        [XmlElement("direction")]
        public string Direction { get; set; }
        [XmlElement("seek")]
        public int Seek { get; set; }
        [XmlElement("length")]
        public int Length { get; set; }
        [XmlElement("loop")]
        public bool Loop { get; set; }
        [XmlElement("freezeonload")]
        public bool Freezeonload { get; set; }
        [XmlElement("triggeronnext")]
        public bool Triggeronnext { get; set; }
        [XmlElement("autoplay")]
        public bool Autoplay { get; set; }
        [XmlElement("timecode")]
        public DateTime Timecode { get; set; }
        [XmlElement("creationDate")]
        public DateTime CreationDate { get; set; }
        [XmlElement("lastUpdateDate")]
        public DateTime LastUpdateDate { get; set; }
        [XmlElement("isActive")]
        public bool IsActive { get; set; }
        [XmlElement("positionx")]
        public int Positionx { get; set; }
        [XmlElement("positiony")]
        public int Positiony { get; set; }
        [XmlElement("scalex")]
        public int Scalex { get; set; }
        [XmlElement("scaley")]
        public int Scaley { get; set; }
        [XmlElement("defer")]
        public bool Defer { get; set; }
        [XmlElement("device")]
        public int Device { get; set; }
        [XmlElement("format")]
        public string Format { get; set; }
        [XmlElement("showmask")]
        public bool Showmask { get; set; }
        [XmlElement("blur")]
        public int Blur { get; set; }
        [XmlElement("key")]
        public string Key { get; set; }
        [XmlElement("spread")]
        public int Spread { get; set; }
        [XmlElement("spill")]
        public int Spill { get; set; }
        [XmlElement("threshold")]
        public int Threshold { get; set; }

    }

    [XmlRoot("MosItems")]
    public class RootObject
    {
        [XmlElement("items")]
        public List<Item> items { get; set; }
    }
  
}
