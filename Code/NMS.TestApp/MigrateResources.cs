﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using Newtonsoft.Json;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Repository;

namespace NMS.TestApp
{
    public class MigrateResources
    {
        public void Migrate(string mediaServer1,string mediaServer2,string MediaFileFolderPath)
        {
            List<string> GuidCollection = File.ReadAllLines(MediaFileFolderPath).ToList();            
            WebClient client = new WebClient();
            HttpWebRequestHelper proxy = new HttpWebRequestHelper();
            foreach (string res in GuidCollection.Distinct())
            //foreach (string res in resourceIds)
            {
                try
                {
                    Resource resource = GetResource(new Guid(res), mediaServer1);

                    resource.ResourceId = 0;
                    resource.CreationDate = DateTime.UtcNow;
                    resource.LastUpdateDate = resource.CreationDate;

                    Resource resource1 = GetResource(new Guid(res), mediaServer2);
                    if (resource1 == null)
                    {
                        resource.ThumbUrl = null;
                        InsertResource(resource, mediaServer2);
                    }

                    string fileName = resource.FileName;
                    byte[] data = client.DownloadData("http://" + mediaServer1 + "/api/resource/getresource/" + res);
                    client.Dispose();
                    string mediaServer21 = mediaServer2 == "10.1.20.57" ? mediaServer2 + ":85" : mediaServer2;

                    proxy.HttpUploadFile("http://" + mediaServer21 + "/api/resource/PostMedia?Id=" + resource.Guid.ToString() + "&IsHD=true", fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());
                    Console.WriteLine(res);
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                }
            }
        }

        public void GenerateSnapshot(List<string> resourceIds, string mediaServer)
        {
            string MediaFileFolderPath = "C://UploadToMS/1.txt";
            List<string> GuidCollection = File.ReadAllLines(MediaFileFolderPath).ToList();  
            WebClient client = new WebClient();
            HttpWebRequestHelper proxy = new HttpWebRequestHelper();
            //foreach (string res in resourceIds)
            foreach (string res in GuidCollection)
            {
                try
                {
                    Resource resource = GetResource(new Guid(res), mediaServer);
                    resource.ThumbUrl = null;
                    resource.Source = "\\\\" + mediaServer + resource.HighResolutionFile;
                    UpdateResource(resource, mediaServer);
                    HttpWebResponse response =  proxy.GetRequest("http://" + mediaServer + "/api/resource/generatesnapshot/" + res, null);
                    response.Close();
                    Console.WriteLine(res);
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                }
            }
        }

        void UpdateResource(Resource entity, string serverIp)
        {

            string sql = @"Update Resource set  [Guid]=@Guid
							, [ResourceTypeId]=@ResourceTypeId
							, [HighResolutionFile]=@HighResolutionFile
							, [HighResolutionFormatId]=@HighResolutionFormatId
							, [LowResolutionFile]=@LowResolutionFile
							, [LowResolutionFormatId]=@LowResolutionFormatId
							, [FileName]=@FileName
							, [ResourceStatusId]=@ResourceStatusId
							, [ServerId]=@ServerId
							, [Source]=@Source
							, [ThumbUrl]=@ThumbUrl
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive 
							 where ResourceId=@ResourceId";
            SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId)
					, new SqlParameter("@HighResolutionFile",entity.HighResolutionFile ?? (object)DBNull.Value)
					, new SqlParameter("@HighResolutionFormatId",entity.HighResolutionFormatId ?? (object)DBNull.Value)
					, new SqlParameter("@LowResolutionFile",entity.LowResolutionFile ?? (object)DBNull.Value)
					, new SqlParameter("@LowResolutionFormatId",entity.LowResolutionFormatId ?? (object)DBNull.Value)
					, new SqlParameter("@FileName",entity.FileName ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceStatusId",entity.ResourceStatusId)
					, new SqlParameter("@ServerId",entity.ServerId ?? (object)DBNull.Value)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbUrl",entity.ThumbUrl ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@ResourceId",entity.ResourceId)};
            SqlHelper.ExecuteNonQuery("Data Source=" + serverIp + ";Initial Catalog=MMSMedia; user id=sa; password=" + ((serverIp == "10.1.20.24") ? "Axact123" : "Ax@ct!23"), CommandType.Text, sql, parameterArray);
        }

        void InsertResource(Resource entity, string serverIp)
        {
            Resource other = new Resource();
            other = entity;
            if (entity.IsTransient())
            {
                string sql = @"Insert into Resource ( [Guid]
				,[ResourceTypeId]
				,[HighResolutionFile]
				,[HighResolutionFormatId]
				,[LowResolutionFile]
				,[LowResolutionFormatId]
				,[FileName]
				,[ResourceStatusId]
				,[ServerId]
				,[Source]
				,[ThumbUrl]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive] )
				Values
				( @Guid
				, @ResourceTypeId
				, @HighResolutionFile
				, @HighResolutionFormatId
				, @LowResolutionFile
				, @LowResolutionFormatId
				, @FileName
				, @ResourceStatusId
				, @ServerId
				, @Source
				, @ThumbUrl
				, @CreationDate
				, @LastUpdateDate
				, @IsActive );
				Select scope_identity()";
                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@Guid",entity.Guid)
					, new SqlParameter("@ResourceTypeId",entity.ResourceTypeId)
					, new SqlParameter("@HighResolutionFile",entity.HighResolutionFile ?? (object)DBNull.Value)
					, new SqlParameter("@HighResolutionFormatId",entity.HighResolutionFormatId ?? (object)DBNull.Value)
					, new SqlParameter("@LowResolutionFile",entity.LowResolutionFile ?? (object)DBNull.Value)
					, new SqlParameter("@LowResolutionFormatId",entity.LowResolutionFormatId ?? (object)DBNull.Value)
					, new SqlParameter("@FileName",entity.FileName ?? (object)DBNull.Value)
					, new SqlParameter("@ResourceStatusId",entity.ResourceStatusId)
					, new SqlParameter("@ServerId",entity.ServerId ?? (object)DBNull.Value)
					, new SqlParameter("@Source",entity.Source ?? (object)DBNull.Value)
					, new SqlParameter("@ThumbUrl",entity.ThumbUrl ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@IsActive",entity.IsActive)};
                var identity = SqlHelper.ExecuteScalar("Data Source=" + ((serverIp == "10.1.20.57") ? "10.1.20.57\\Axact" : serverIp) + ";Initial Catalog=MMSMedia; user id=sa; password=" + ((serverIp == "10.1.20.57") ? "Axact123" : "Ax@ct!23"), CommandType.Text, sql, parameterArray);
                if (identity == DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
            }
        }

        Resource GetResource(Guid guid, string serverIp)
        {
            string sql = "select * from Resource with (nolock)  where Guid=@Guid ";
            SqlParameter parameter = new SqlParameter("@Guid", guid);
            DataSet ds = SqlHelper.ExecuteDataset("Data Source=" + ((serverIp == "10.1.20.57") ? "10.1.20.57\\Axact" : serverIp) + ";Initial Catalog=MMSMedia; user id=sa; password=" + ((serverIp == "10.1.20.57") ? "Axact123" : "Ax@ct!23"), CommandType.Text, sql, new SqlParameter[] { parameter });
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
            return ResourceFromDataRow(ds.Tables[0].Rows[0]);
        }

        Resource ResourceFromDataRow(DataRow dr)
        {
            if (dr == null) return null;
            Resource entity = new Resource();
            if (dr.Table.Columns.Contains("ResourceId"))
            {
                entity.ResourceId = (System.Int32)dr["ResourceId"];
            }
            if (dr.Table.Columns.Contains("Guid"))
            {
                entity.Guid = (System.Guid)dr["Guid"];
            }
            if (dr.Table.Columns.Contains("ResourceTypeId"))
            {
                entity.ResourceTypeId = (System.Int32)dr["ResourceTypeId"];
            }
            if (dr.Table.Columns.Contains("HighResolutionFile"))
            {
                entity.HighResolutionFile = dr["HighResolutionFile"].ToString();
            }
            if (dr.Table.Columns.Contains("HighResolutionFormatId"))
            {
                entity.HighResolutionFormatId = dr["HighResolutionFormatId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["HighResolutionFormatId"];
            }
            if (dr.Table.Columns.Contains("LowResolutionFile"))
            {
                entity.LowResolutionFile = dr["LowResolutionFile"].ToString();
            }
            if (dr.Table.Columns.Contains("LowResolutionFormatId"))
            {
                entity.LowResolutionFormatId = dr["LowResolutionFormatId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["LowResolutionFormatId"];
            }
            if (dr.Table.Columns.Contains("FileName"))
            {
                entity.FileName = dr["FileName"].ToString();
            }
            if (dr.Table.Columns.Contains("ResourceStatusId"))
            {
                entity.ResourceStatusId = (System.Int32)dr["ResourceStatusId"];
            }
            if (dr.Table.Columns.Contains("ServerId"))
            {
                entity.ServerId = dr["ServerId"] == DBNull.Value ? (System.Int32?)null : (System.Int32?)dr["ServerId"];
            }
            if (dr.Table.Columns.Contains("Source"))
            {
                entity.Source = dr["Source"].ToString();
            }
            if (dr.Table.Columns.Contains("ThumbUrl"))
            {
                entity.ThumbUrl = dr["ThumbUrl"].ToString();
            }
            if (dr.Table.Columns.Contains("CreationDate"))
            {
                entity.CreationDate = (System.DateTime)dr["CreationDate"];
            }
            if (dr.Table.Columns.Contains("LastUpdateDate"))
            {
                entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
            }
            if (dr.Table.Columns.Contains("IsActive"))
            {
                entity.IsActive = (System.Boolean)dr["IsActive"];
            }
            return entity;
        }
    }
}
