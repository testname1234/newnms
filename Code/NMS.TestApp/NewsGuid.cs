﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NMS.MongoRepository;
using NMS.Core.Entities.Mongo;
using NMS.Repository;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;

namespace NMS.TestApp
{
    class NewsGuid
    {
        public static string connectionstring
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["myconnlocal"].ToString();
            }
        }
        public DataTable GetResult()
        {
            DataTable table = new DataTable();
            string NewsId;
            string Guid;
            String BunchGuid;
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"select top 2 newsid,[guid],BunchGuid from news (nolock) where BunchGuid in 
                    (select BunchGuid from news (nolock) group by BunchGuid having count(*)>1) order by BunchGuid";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(table);
                    foreach (DataRow rcol in table.Rows)
                    {
                        NewsId = rcol["NewsId"].ToString();
                        Guid = rcol["Guid"].ToString();
                        BunchGuid = rcol["BunchGuid"].ToString();
                        Console.WriteLine(NewsId);
                        Console.WriteLine(Guid);
                        Console.WriteLine(BunchGuid);

                        MBunchRepository mrepo = new MBunchRepository();                        
                        MBunch mbunch = new MBunch();
                        MBunch mbunchExisting = mrepo.GetByGuid(BunchGuid);
                        mbunch.NewsCount = 1;
                        mbunch.CreationDate = mbunchExisting.CreationDate;
                        mbunch.LastUpdateDate = mbunchExisting.LastUpdateDate;                        
                        MBunch mbunchNew = mrepo.InsertBunch(mbunch);
                        string NewBunchGuid = mbunchNew._id;
                        //mbunchExisting._id = mbunchNew._id;
                        NewsRepository nmsrepo = new NewsRepository();
                        nmsrepo.UpdateNewsByBunchId(Convert.ToInt32(NewsId), NewBunchGuid);
                        //BunchRepository bunch = new BunchRepository();
                        //bunch.UpdateBunchByBunchId(Convert.ToInt32(NewsId), NewBunchGuid);
                        //Bunch bunch = new Bunch();
                        //bunch






                    }
                    Console.ReadLine();
                    return table;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
            }
        }
        void Main(string[] args)
        {
            Console.WriteLine(GetResult());
            Console.ReadLine();
        }
    }
}

