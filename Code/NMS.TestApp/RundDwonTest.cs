﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using CasparCGLib.Entities;
using ControlPanel.Core;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using MS.Core.Entities;
using MS.Core.Enums;
using Newtonsoft.Json;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Repository;
using NMS.Service;
using NMS.Core;

namespace NMS.TestApp
{
    public class RundDwonTest
    {


        string MosDeviceId = "bolnews";
        string MosDeviceIP = "10.1.20.46";
        string MosDevicePort = "10541";
        string NCID = "NMSRundownService";
        int messageId = 1;
        MOSClient mosClient = null;

        public MOS GetROCreate(string runDownId, string ProgramName, DateTime StartTime, String EndTime, List<Slot> slots)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROCreate ro = new ROCreate();
            ro.roID = runDownId;
            ro.roSlug = ProgramName;        //Episode Nam and Duration
            ro.roEdStart = StartTime.ToString();
            ro.roEdDur = EndTime; //00:59:00
            ro.roTrigger = "TIMED";
            ro.story = new System.Collections.Generic.List<Story>();

            //for (int i = 1; i <= slots.Count; i++)

            foreach (Slot slot in slots)
            {
                Story story = new Story();
                story.storyID = slot.SlotId.ToString();    //Slot pk
                story.storySlug = slot.Title;   //Slot Title
                story.storyNum = slot.SequnceNumber.ToString();
                ro.story.Add(story);

            }
            command.command = ro;

            return command;
        }

        public MOS GetStroySend(string storyID, string storyNumber, string runDownId, string storyTitle, List<string> StoryScripts)
        {
            MOS command = new MOS();
            command.mosID = MosDeviceId;
            command.ncsID = NCID;
            ROStorySend roStorySend = new ROStorySend();
            roStorySend.roID = runDownId;
            roStorySend.storyID = storyID;
            roStorySend.storySlug = storyTitle;
            roStorySend.storyNum = storyNumber;
            //roStorySend.storyBodies = new List<StoryBody>();
            List<StoryBody> storyBodies = new List<StoryBody>();
            int i = 0;
            foreach (string body in StoryScripts)
            {
                //storyBody.ParagraphWithInstructions = new Paragraph();
                //storyBody.ParagraphWithInstructions.ParagraphInstruction = "OC";
                StoryBody storyBody = new StoryBody();
                storyBody.ParagraphWithInstructions = "OC";
                storyBody.Paragraph = body;
                storyBodies.Add(storyBody);
                i++;
            }
            roStorySend.storyBodies = storyBodies;

            command.command = roStorySend;

            return command;
        }

        public MOS SendROCreate(out string message, string runDownId, string ProgramName, DateTime StartTime, String EndTime, List<Slot> slots)
        {
            message = string.Empty;
            MOS mosResponse = new MOS();
            try
            {
                if (mosClient != null && mosClient.IsConnected)
                {
                    MOS mos = GetROCreate(runDownId, ProgramName, StartTime, EndTime, slots);
                    mosResponse = mosClient.SendMOSCommand(mos, out message);
                }
                else
                {
                    message = "Error: Not Connected.";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return mosResponse;
        }

        public MOS SendROStorySend(out string message, string storyID, string storyNumber, string rundDownId, string storyTitle, List<string> StoryScript)
        {
            message = string.Empty;
            MOS mosResponse = new MOS();

            try
            {
                if (mosClient != null && mosClient.IsConnected)
                {
                    MOS mos = GetStroySend(storyID, storyNumber, rundDownId, storyTitle, StoryScript);
                    mosResponse = mosClient.SendMOSCommand(mos, out message);
                }
                else
                {
                    message = "Error: Not Connected.";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return mosResponse;
        }

        public string Execute(string argument)
        {
            string message = string.Empty;
            mosClient = new MOSClient(MosDeviceId, MosDeviceIP, int.Parse(MosDevicePort), NCID);
            ISlotService slotService = IoC.Resolve<ISlotService>("slotService");
            IMosActiveEpisodeService mosActiveEpisodeService = IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
            ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
            IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
            IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
            List<MosActiveEpisode> MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByStatus(null);//Inseert            
            if (MosActiveEpisodes != null && MosActiveEpisodes.Count > 0)
            {
                foreach (MosActiveEpisode mosEpisode in MosActiveEpisodes)
                {
                    if (mosEpisode.StatusCode == 1 || mosEpisode.StatusCode == 2)
                    {
                        List<Slot> slots = slotService.GetSlotByEpisode(Convert.ToInt32(mosEpisode.EpisodeId));
                        if (slots != null && slots.Count > 0)
                        {
                            Episode episode = episodeService.GetEpisode(Convert.ToInt32(mosEpisode.EpisodeId));
                            NMS.Core.Entities.Program program = programService.GetProgram(episode.ProgramId);


                            #region MOS Integration
                            MOS obj = null;
                            //if (mosClient.IsConnected)
                            //{
                            //}
                            //else
                            mosClient.Connect(out message);
                            System.Threading.Thread.Sleep(1000);
                            obj = mosClient.SendHeartBeat(out message);
                            System.Threading.Thread.Sleep(1000);
                            int Hour = Convert.ToInt32(episode.To.Subtract(episode.From).TotalHours);
                            int Minutes = Convert.ToInt32(episode.To.Subtract(episode.From).Minutes);
                            int Seconds = Convert.ToInt32(episode.To.Subtract(episode.From).Seconds);
                            obj = SendROCreate(out message, mosEpisode.MosActiveEpisodeId.ToString(), program.Name, episode.From, Hour.ToString() + ":" + Minutes.ToString() + ":" + Seconds.ToString(), slots);
                            foreach (Slot slot in slots)
                            {
                                List<string> scripts = new List<string>();
                                List<SlotScreenTemplate> slotScreenTemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slot.SlotId);
                                if (slotScreenTemplates != null && slotScreenTemplates.Count > 0)
                                {
                                    scripts = slotScreenTemplates.Select(x => x.Script).ToList();
                                    SendROStorySend(out message, slot.SlotId.ToString(), slot.SequnceNumber.ToString(), mosEpisode.MosActiveEpisodeId.ToString(), slot.Title, scripts);
                                }
                            }
                            #endregion
                        }
                    }
                    // mosEpisode.StatusCode = 3;
                    //  mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);                  
                }
                mosClient.Disconnect(out message);
            }
            return "";
        }

        public void GenerateRunDownMos()
        {
            IMosActiveEpisodeService mosActiveEpisodeService = NMS.Core.IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
            IMosActiveTransformationService mosActiveTransformationService = NMS.Core.IoC.Resolve<IMosActiveTransformationService>("MosActiveTransformationService");
            IMosActiveItemService mosActiveItemService = NMS.Core.IoC.Resolve<IMosActiveItemService>("MosActiveItemService");
            ISlotScreenTemplateResourceService slotScreenTemplateResourceRepository = NMS.Core.IoC.Resolve<ISlotScreenTemplateResourceService>("SlotScreenTemplateResourceService");
            IResourceService resourceService = NMS.Core.IoC.Resolve<IResourceService>("ResourceService");
            List<MosActiveEpisode> MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByStatus(null);//Inseert
            string remotePath = "C:\\MosRundDown\\";
            string remoteMediaPath = "D:\\Software\\CasparCG Server\\Server\\media\\";

            DataSet ds = new DataSet();
            if (MosActiveEpisodes != null && MosActiveEpisodes.Count > 0)
            {
                foreach (MosActiveEpisode mosEpisode in MosActiveEpisodes)
                {
                    if (mosEpisode.StatusCode == 1 || mosEpisode.StatusCode == 2)
                    {
                        List<MosActiveItem> MosActiveItems = mosActiveItemService.GetMosActiveItemWithRunDownByEpisodeId(mosEpisode.EpisodeId);
                        List<item> items = new List<item>();

                        if (MosActiveItems != null && MosActiveItems.Count > 0)
                        {

                            List<SlotScreenTemplateResource> slotScreenTemplateResources = slotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByEpisode(Convert.ToInt32(mosEpisode.EpisodeId));
                            //Download resource.
                            WebClient client = new WebClient();
                            string fileName = "";
                            try
                            {
                                int i = 1;
                                foreach (SlotScreenTemplateResource slotScreenTemplateResource in slotScreenTemplateResources)
                                {
                                    NMS.Core.Entities.Resource resource = resourceService.GetResourceByGuid(slotScreenTemplateResource.ResourceGuid.ToString());
                                    string ext = "";
                                    fileName = mosEpisode.EpisodeId + "_" + i;
                                    if (resource.ResourceTypeId == 1)
                                        ext = ".jpg";
                                    else if (resource.ResourceTypeId == 2)
                                        ext = ".mp4";
                                    else if (resource.ResourceTypeId == 3)
                                        ext = ".mp3";
                                    client.DownloadFile(NMS.Core.AppSettings.MediaServerUrl + "/getresource/" + slotScreenTemplateResource.ResourceGuid.ToString(), remoteMediaPath + fileName + ext);
                                    client.Dispose();
                                    i++;
                                }

                            }
                            catch (Exception ex)
                            {
                                if (client != null)
                                    client.Dispose();
                            }



                            MosActiveItems.Where(w => w.Type == "MOVIE").ToList().ForEach(s => s.Label = fileName);
                            MosActiveItems.Where(w => w.Type == "MOVIE").ToList().ForEach(s => s.Name = fileName);

                            items.CopyFrom(MosActiveItems);
                            using (TextWriter tw = new StreamWriter(remotePath + "TEMPLATE" + mosEpisode.EpisodeId + ".xml"))
                            {
                                XmlRootAttribute root = new XmlRootAttribute("items");
                                XmlSerializer xs = new XmlSerializer(items.GetType(), root);
                                xs.Serialize(tw, items);
                            }


                        }

                    }
                    mosEpisode.StatusCode = 3;
                    mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
                }
            }
        }

        private void CasperSendData(NetworkStream networkStream, string data)
        {
            byte[] outStream = System.Text.Encoding.BigEndianUnicode.GetBytes(data);
            networkStream.Write(outStream, 0, outStream.Length);
            networkStream.Flush();
        }

        public static string GetStringForMOSCommand<T>(T command, string RootArgument)
        {
            StringBuilder stringBuilder = new StringBuilder();

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.OmitXmlDeclaration = true;

            XmlSerializerNamespaces xmlEmptyNameSpace = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });

            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlWriterSettings);

            XmlRootAttribute root = new XmlRootAttribute(RootArgument);
            XmlSerializer xml = new XmlSerializer(typeof(T), root);
            xml.Serialize(xmlWriter, command, xmlEmptyNameSpace);

            return stringBuilder.ToString();
        }

        public string CasperDeviceIP = "10.1.20.26";
        public string CasperDevicePort = "7250";
        private const int ReceiveTimeout = 20000;
        private const int SendTimeout = 20000;

        public void SendCasperRundown()
        {

            IMosActiveEpisodeService mosActiveEpisodeService = NMS.Core.IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
            IMosActiveItemService mosActiveItemService = IoC.Resolve<IMosActiveItemService>("MosActiveItemService");
            ISlotScreenTemplateResourceService slotScreenTemplateResourceRepository = IoC.Resolve<ISlotScreenTemplateResourceService>("SlotScreenTemplateResourceService");
            IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
            List<MosActiveEpisode> MosActiveEpisodes = mosActiveEpisodeService.GetMosActiveEpisodeByStatus(null);//Inseert
            string remotePath = "C:\\MosRundDown\\";
            string remoteMediaPath = "C:\\media\\";

            DataSet ds = new DataSet();
            if (MosActiveEpisodes != null && MosActiveEpisodes.Count > 0)
            {
                foreach (MosActiveEpisode mosEpisode in MosActiveEpisodes)
                {
                    if (mosEpisode.StatusCode == 1 || mosEpisode.StatusCode == 2)
                    {
                        List<MosActiveItem> MosActiveItems = mosActiveItemService.GetMosActiveItemWithRunDownByEpisodeId(mosEpisode.EpisodeId);
                        List<item> items = new List<item>();

                        if (MosActiveItems != null && MosActiveItems.Count > 0)
                        {
                            List<SlotScreenTemplateResource> slotScreenTemplateResources = slotScreenTemplateResourceRepository.GetSlotScreenTemplateResourceByEpisode(Convert.ToInt32(mosEpisode.EpisodeId));
                            //Download resource.
                            WebClient client = new WebClient();
                            string fileName = "";
                            try
                            {
                                int i = 1;
                                foreach (SlotScreenTemplateResource slotScreenTemplateResource in slotScreenTemplateResources)
                                {
                                    MResource mResource = resourceService.GetMResourceByGuid(slotScreenTemplateResource.ResourceGuid.ToString());
                                    string ext = "";
                                    fileName = mosEpisode.EpisodeId + "_" + i;
                                    if (mResource.ResourceTypeId == 1)
                                        ext = ".jpg";
                                    else if (mResource.ResourceTypeId == 2)
                                        ext = ".mp4";
                                    else if (mResource.ResourceTypeId == 3)
                                        ext = ".mp3";
                                    client.DownloadFile(NMS.Core.AppSettings.MediaServerUrl + "/getresource/" + slotScreenTemplateResource.ResourceGuid.ToString(), remoteMediaPath + fileName + ext);
                                    client.Dispose();
                                    i++;
                                }

                            }
                            catch (Exception ex)
                            {
                                if (client != null)
                                    client.Dispose();
                            }



                            MosActiveItems.Where(w => w.Type == "MOVIE").ToList().ForEach(s => s.Label = fileName);
                            MosActiveItems.Where(w => w.Type == "MOVIE").ToList().ForEach(s => s.Name = fileName);

                            items.CopyFrom(MosActiveItems);
                            TcpClient clientCasper = new TcpClient(CasperDeviceIP, Convert.ToInt32(CasperDevicePort));
                            using (TextWriter tw = new StreamWriter(remotePath + "TEMPLATE" + mosEpisode.EpisodeId + ".xml"))
                            {
                                XmlRootAttribute root = new XmlRootAttribute("items");
                                XmlSerializer xs = new XmlSerializer(items.GetType(), root);
                                xs.Serialize(tw, items);

                                CasperSendData(clientCasper.GetStream(), GetStringForMOSCommand(items, "items"));
                            }
                            clientCasper.Close();
                        }
                    }
                    mosEpisode.StatusCode = 3;
                    mosActiveEpisodeService.UpdateMosActiveEpisode(mosEpisode);
                }
            }
        }

    }
}
