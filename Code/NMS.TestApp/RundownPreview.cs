﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FFMPEGLib;
using MS.Core.Enums;
using MS.Core.IService;
using MS.MediaIntegration.API;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.IService;
using NMS.Service;

namespace NMS.TestApp
{
    public class EpisodePreviewModel
    { 
        public EpisodePreviewModel()
        {
            this.SlotPreviews = new Dictionary<int, string>();
        }
       
        public int EpisodeId { get; set; }
        public Guid? EpisodePreview { get; set; }
        public Dictionary<int, string> SlotPreviews { get; set; }
    
    }

    class FilesOrder
    {
        public string FileName { get; set; }
        public int SequenceNumber { get; set; }
        public int ParentSequenceNumber { get; set; }
    }

   public class RundownPreview
    {

          public EpisodePreviewModel GetRunDownPreviewByEpisodeId(int episodeid)
         {

             int DefaultDuration = 5;
             IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
             ISegmentService segmentservice = IoC.Resolve<ISegmentService>("SegmentService");
             ISlotService slotService = IoC.Resolve<ISlotService>("SlotService");
             ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
             ISlotTemplateScreenElementService slotTemplateScreenElementService = IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
             MS.Core.IService.IServerService serverservice = IoC.Resolve<MS.Core.IService.IServerService>("ServerService");

             EpisodePreviewModel model = new EpisodePreviewModel();
             List<FilesOrder> downloadedFilePath = new List<FilesOrder>();
             List<Segment> segments = segmentservice.GetSegmentByEpisodeId(episodeid);
            
             model.EpisodeId = episodeid;
             
             if (segments != null)
             {
                 segments = segments.OrderBy(x => x.SequnceNumber).ToList();
                 Guid guid = Guid.NewGuid();
                 string folderPath = ConfigurationManager.AppSettings["TempContentLocation"].ToString() + guid.ToString();
                 if (!Directory.Exists(folderPath))
                 {
                     Directory.CreateDirectory(folderPath);
                 }
                 try
                 {
                     List<Slot> slots = new List<Slot>();
                     foreach (Segment segment in segments)
                     {
                         List<Slot> _slots = slotService.GetSlotBySegmentId(segment.SegmentId);

                         if (_slots != null)
                         {
                             _slots = _slots.OrderBy(x => x.SequnceNumber).ToList();

                             slots.AddRange(_slots);
                         }
                     }

                     Parallel.ForEach(slots, slot =>
                     //foreach (Slot slot in slots)
                     {
                         List<SlotScreenTemplate> slotscreentemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slot.SlotId);

                         if (slotscreentemplates != null)
                         {
                             slotscreentemplates = slotscreentemplates.Where(x => !x.ParentId.HasValue).OrderBy(x => x.SequenceNumber).ToList();
                             List<FilesOrder> slotFilePath = new List<FilesOrder>();
                            // Parallel.ForEach(slotscreentemplates, slotscreentemplate =>
                             foreach (SlotScreenTemplate slotscreentemplate in slotscreentemplates)
                             {

                                 bool flag = false;
                                 Dictionary<int, int> VideoDurations = new Dictionary<int, int>();
                                 Guid fileguid = Guid.NewGuid();
                                 try
                                 {
                                     System.Net.WebRequest request = System.Net.WebRequest.Create(ConfigurationManager.AppSettings["MsApi"].ToString() + "/api/Resource/getresource/" + slotscreentemplate.ThumbGuid.ToString());
                                     System.Net.WebResponse response = request.GetResponse();
                                     System.IO.Stream responseStream = response.GetResponseStream();
                                     using (Bitmap bitmap = new Bitmap(responseStream))
                                         bitmap.Save(folderPath + "\\" + fileguid.ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                     flag = true;
                                 }
                                 catch { }
                                 if (flag)
                                 {
                                     int Duration = (slotscreentemplate.Duration.HasValue && slotscreentemplate.Duration.Value > 0) ? slotscreentemplate.Duration.Value : DefaultDuration;

                                     List<SlotTemplateScreenElement> slotTemplateScreenElements = slotTemplateScreenElementService.GetSlotTemplateScreenElementBySlotScreenTemplateId(slotscreentemplate.SlotScreenTemplateId);
                                     if (slotTemplateScreenElements != null)
                                     {
                                         slotTemplateScreenElements = slotTemplateScreenElements.Where(a => a.ScreenElementId == 3).ToList();
                                         if (slotTemplateScreenElements.Count > 0)
                                         {
                                             foreach (var slotTemplateScreenElement in slotTemplateScreenElements)
                                             {
                                                 VideoDurations.Add(slotTemplateScreenElement.SlotTemplateScreenElementId, (int)Math.Floor(FFMPEGLib.FFMPEG.GetDuration(ConfigurationManager.AppSettings["MsApi"].ToString() + "/api/Resource/getresource/" + slotTemplateScreenElement.ResourceGuid.ToString())));
                                             }
                                             Duration = VideoDurations.Select(x => x.Value).Max() > Duration ? VideoDurations.Select(x => x.Value).Max() : Duration;
                                         }
                                         else
                                         {
                                             Duration = 5;
                                         }
                                     }


                                     int From = 0;
                                     string videopath = folderPath + "\\" + fileguid.ToString() + ".ts";

                                     //if (slotscreentemplate.ScreenTemplateId == 749)
                                     //{
                                     //    videopath = "F:\\MediaResources\\63cfafe5-7c3a-428b-8dc3-5f2c5eed91f3\\47a32312-efa4-457e-9831-f65843333388.ts";
                                     //}
                                     //else
                                     //{
                                     //   FFMPEG.CreateVideofromThumb(folderPath + "\\" + fileguid.ToString() + ".jpg", folderPath + "\\", fileguid.ToString() + ".ts", TimeSpan.FromSeconds(Duration));
                                     //}


                                     FFMPEG.CreateVideofromThumb(folderPath + "\\" + fileguid.ToString() + ".jpg", folderPath + "\\", fileguid.ToString() + ".ts", TimeSpan.FromSeconds(Duration));
                                    
                                     lock (slotFilePath)
                                         slotFilePath.Add(new FilesOrder() { FileName = videopath, SequenceNumber = slotscreentemplate.SequenceNumber.HasValue ? slotscreentemplate.SequenceNumber.Value : 1 });

                                     if (slotTemplateScreenElements != null)
                                     {
                                         slotTemplateScreenElements = slotTemplateScreenElements.Where(a => a.ScreenElementId == 3 || a.ScreenElementId == 13 || a.ScreenElementId == 8).ToList();

                                         if (slotTemplateScreenElements != null)
                                         {
                                             foreach (SlotTemplateScreenElement slotTemplateScreenElement in slotTemplateScreenElements)
                                             {

                                                 byte[] overlapdata = null;
                                                 using (WebClient client = new WebClient())
                                                 {
                                                     if (slotTemplateScreenElement.ResourceGuid != null)
                                                     {
                                                         overlapdata = client.DownloadData(ConfigurationManager.AppSettings["MsApi"].ToString() + "/api/Resource/getresource/" + slotTemplateScreenElement.ResourceGuid.ToString());
                                                     }
                                                 }

                                                 if (overlapdata.Length > 1200)
                                                 {
                                                     Guid embeddedVideoGuid = Guid.NewGuid();
                                                     Guid outputGuid = Guid.NewGuid();
                                                     string path = "";

                                                     if (slotTemplateScreenElement.ScreenElementId == 3)
                                                     {
                                                         path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".ts";
                                                         File.WriteAllBytes(path, overlapdata);
                                                      //   FFMPEG.EmbedVideo(videopath, path, folderPath + "\\" + outputGuid + ".ts", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(VideoDurations[slotTemplateScreenElement.SlotTemplateScreenElementId]));
                                                         FFMPEG.EmbedVideo(videopath, path, folderPath + "\\" + outputGuid + ".ts", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(10));
                                                     }
                                                     else if (slotTemplateScreenElement.ScreenElementId == 13 || slotTemplateScreenElement.ScreenElementId == 8)
                                                     {
                                                         path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".jpg";
                                                         File.WriteAllBytes(path, overlapdata);

                                                         using (System.IO.MemoryStream responseStream = new MemoryStream(overlapdata))
                                                         {
                                                             using (Bitmap bitmap = new Bitmap(responseStream))
                                                                 bitmap.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                         }
                                                         FFMPEG.EmbedImage(videopath, path, folderPath + "\\" + outputGuid + ".ts", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(10));
                                                         // FFMPEG.EmbedImage(videopath, path, folderPath + "\\" + outputGuid + ".ts", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(Duration));
                                                         
                                                     }
                                                     File.Delete(videopath);
                                                     File.Move(folderPath + "\\" + outputGuid + ".ts", videopath);
                                                 }
                                             }
                                         }
                                     }
                                 }
                             }
                             //);

                             if (slotFilePath != null && slotFilePath.Count > 0)
                             {
                                 Guid slotoutputGuid = Guid.NewGuid();
                                 string slotoutputpath = folderPath + "\\" + slotoutputGuid + ".mp4";
                                 slotFilePath = slotFilePath.OrderBy(x => x.SequenceNumber).ToList();
                                 FFMPEG.ConcatenateVideosForPreviewWithEncoding(slotFilePath.Select(x => x.FileName).ToList(), slotoutputpath);
                                 foreach (var path in slotFilePath)
                                 {
                                     path.ParentSequenceNumber = slot.SequnceNumber;
                                 }
                                 lock (downloadedFilePath)
                                 {
                                     downloadedFilePath.AddRange(slotFilePath);
                                 }

                                 HttpStatusCode code = HttpStatusCode.UseProxy;

                                 byte[] slotpreviewFile;
                                 using (WebClient client = new WebClient())
                                 {
                                     //slotpreviewFile = client.DownloadData(slotoutputpath);
                                     slotpreviewFile = File.ReadAllBytes(slotoutputpath);
                                     if (slotpreviewFile.Length > 1200)
                                     {
                                         MSApi api = new MSApi();
                                         string bucketpath = @"SlotPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";

                                         MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

                                         resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
                                         resource.ResourceTypeId = ((int)NMS.Core.Enums.ResourceTypes.Video).ToString();
                                         resource.CreationDate = DateTime.UtcNow.ToString();
                                         resource.LastUpdateDate = resource.CreationDate;
                                         resource.IsActive = "True";
                                         resource.BucketId = (int)NMSBucket.NMSBucket;
                                         resource.Source = slotoutputGuid + ".mp4";
                                         resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
                                         resource.IsPrivate = "True";

                                         MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);

                                         try { CreateBucket(bucketpath); }
                                         catch (Exception ex) { }

                                         code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", slotpreviewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());

                                         if (code == HttpStatusCode.OK)
                                         {
                                             Slot slott = slotService.GetSlot(slot.SlotId);
                                             slott.PreviewGuid = output.Guid;
                                             slotService.UpdateSlot(slott);
                                             model.SlotPreviews.Add(slot.SlotId, output.Guid.ToString());
                                         }

                                     }

                                 }

                             }

                         }
                     });


                     if (downloadedFilePath != null && downloadedFilePath.Count > 0)
                     {
                         Guid EpisodePreviewGuid = Guid.NewGuid();
                         string episodepreviewpath = folderPath + "\\" + EpisodePreviewGuid + ".mp4";
                         if (downloadedFilePath.Count > 1)
                         {
                             downloadedFilePath = downloadedFilePath.OrderBy(x => x.SequenceNumber).OrderBy(x => x.ParentSequenceNumber).ToList();
                             FFMPEG.ConcatenateVideosForPreviewWithEncoding(downloadedFilePath.Select(x => x.FileName).ToList(), episodepreviewpath);
                             HttpStatusCode code = HttpStatusCode.UseProxy;

                             byte[] previewFile;
                             using (WebClient client = new WebClient())
                             {
                                 previewFile = client.DownloadData(episodepreviewpath);
                                 if (previewFile.Length > 1200)
                                 {
                                     MSApi api = new MSApi();
                                     string bucketpath = @"RunDownPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";

                                     MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

                                     resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
                                     resource.ResourceTypeId = ((int)NMS.Core.Enums.ResourceTypes.Video).ToString();
                                     resource.CreationDate = DateTime.UtcNow.ToString();
                                     resource.LastUpdateDate = resource.CreationDate;
                                     resource.IsActive = "True";
                                     resource.BucketId = (int)NMSBucket.NMSBucket;
                                     resource.Source = EpisodePreviewGuid + ".mp4";
                                     resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
                                     resource.IsPrivate = "True";

                                     MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);

                                     try { CreateBucket(bucketpath); }
                                     catch (Exception ex) { }

                                     code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", previewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());
                                     if (code == HttpStatusCode.OK)
                                     {
                                         Episode episode = episodeService.GetEpisode(episodeid);
                                         episode.PreviewGuid = output.Guid;
                                         episodeService.UpdateEpisode(episode);
                                         Directory.Delete(folderPath, true);
                                         model.EpisodePreview = output.Guid;
                                         return model;
                                     }
                                 }

                             }

                         }
                         else
                         {
                             File.Move(downloadedFilePath[0].FileName, episodepreviewpath);
                             Episode episode = episodeService.GetEpisode(episodeid);
                             episode.PreviewGuid = new Guid(model.SlotPreviews.Select(x=>x.Value).First());
                             episodeService.UpdateEpisode(episode);
                             Directory.Delete(folderPath, true);
                             model.EpisodePreview = episode.PreviewGuid;
                             return model;
                         }
                     }
                 }
                 catch
                 {
                     if (Directory.Exists(folderPath))
                         Directory.Delete(folderPath, true);
                     throw;
                 }
             }
             return null;

         }

       //public EpisodePreviewModel GetRunDownPreviewByEpisodeId(int episodeid)
       //{

       //    IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
       //    ISegmentService segmentservice = IoC.Resolve<ISegmentService>("SegmentService");
       //    ISlotService slotService = IoC.Resolve<ISlotService>("SlotService");
       //    ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
       //    ISlotTemplateScreenElementService slotTemplateScreenElementService = IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
       //    IServerService serverservice = IoC.Resolve<IServerService>("ServerService");

       //    EpisodePreviewModel model = new EpisodePreviewModel();
           
       //    List<string> downloadedFilePath = new List<string>();
       //    List<string> slotFilePath;
       //    List<Segment> segments  =  segmentservice.GetSegmentByEpisodeId(episodeid);

       //    if (segments != null)
       //    {
       //        segments = segments.OrderBy(x => x.SequnceNumber).ToList();
       //        Guid guid = Guid.NewGuid();
       //        string folderPath = ConfigurationManager.AppSettings["ThumbTemplLocation"].ToString() + guid.ToString();
       //        if (!Directory.Exists(folderPath))
       //        {
       //            Directory.CreateDirectory(folderPath);
       //        }

       //        foreach (Segment segment in segments)
       //        {
       //            List<Slot> slots = slotService.GetSlotBySegmentId(segment.SegmentId);

       //            if (slots != null)
       //            {
       //                slots = slots.OrderBy(x => x.SequnceNumber).ToList();
                       
       //                foreach (Slot slot in slots)
       //                {
       //                    List<SlotScreenTemplate> slotscreentemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slot.SlotId);

       //                    if (slotscreentemplates != null)
       //                    {
       //                        slotscreentemplates = slotscreentemplates.OrderBy(x => x.SequenceNumber).ToList();
       //                        slotFilePath = new List<string>();
       //                        foreach (SlotScreenTemplate slotscreentemplate in slotscreentemplates)
       //                        {
       //                            byte[] data;
       //                            using (WebClient client = new WebClient())
       //                            {
       //                                data = client.DownloadData(ConfigurationManager.AppSettings["MsApi"].ToString() + "api/Resource/getresource/" + slotscreentemplate.ThumbGuid.ToString());
       //                            }

       //                            if (data.Length > 0)
       //                            {
       //                                Guid fileguid = Guid.NewGuid();

       //                                File.WriteAllBytes(folderPath + "\\" + fileguid.ToString() + ".jpg", data);
       //                                int Duration  = (slotscreentemplate.Duration.HasValue && slotscreentemplate.Duration.Value > 0) ? slotscreentemplate.Duration.Value : 10;
       //                                int From  = 0;
       //                                string videopath = folderPath + "\\" + fileguid.ToString() + ".ts";
       //                                FFMPEG.CreateVideofromThumb(folderPath + "\\" + fileguid.ToString() + ".jpg", folderPath + "\\", fileguid.ToString() + ".ts", TimeSpan.FromSeconds(Duration));
       //                                slotFilePath.Add(videopath);
       //                                downloadedFilePath.Add(videopath);

       //                                List<SlotTemplateScreenElement> slotTemplateScreenElements = slotTemplateScreenElementService.GetSlotTemplateScreenElementBySlotScreenTemplateId(slotscreentemplate.SlotScreenTemplateId);
                                       
       //                                if(slotTemplateScreenElements!=null)
       //                                {
       //                                    slotTemplateScreenElements = slotTemplateScreenElements.Where(a=>a.ScreenElementId == 3 || a.ScreenElementId == 13).ToList();

       //                                    if (slotTemplateScreenElements != null)
       //                                    {
       //                                        foreach (SlotTemplateScreenElement slotTemplateScreenElement in slotTemplateScreenElements)
       //                                        {

       //                                            byte[] overlapdata =null;
       //                                            using (WebClient client = new WebClient())
       //                                            {
       //                                                if (slotTemplateScreenElement.ResourceGuid != null)
       //                                                {
       //                                                    overlapdata = client.DownloadData(ConfigurationManager.AppSettings["MsApi"].ToString() + "api/Resource/getresource/" + slotTemplateScreenElement.ResourceGuid.ToString());
       //                                                }
       //                                            }

       //                                            if (overlapdata.Length > 1200)
       //                                            {
       //                                                Guid embeddedVideoGuid = Guid.NewGuid();
       //                                                string path = "";
                                                     
       //                                                if (slotTemplateScreenElement.ScreenElementId == 3)
       //                                                {
       //                                                    path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".ts";
       //                                                    File.WriteAllBytes(path, overlapdata);
       //                                                    FFMPEG.EmbedVideo(videopath, path, ConfigurationManager.AppSettings["ThumbTemplLocation"].ToString() + "\\" + embeddedVideoGuid + ".mp4", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(Duration));
       //                                                }
       //                                                else if (slotTemplateScreenElement.ScreenElementId == 13 || slotTemplateScreenElement.ScreenElementId == 8)
       //                                                {
       //                                                    path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".jpg";
       //                                                    File.WriteAllBytes(path, overlapdata);
       //                                                    FFMPEG.EmbedImage(videopath, path, ConfigurationManager.AppSettings["ThumbTemplLocation"].ToString() + "\\" + embeddedVideoGuid + ".mp4", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(Duration));
       //                                                }

       //                                            }
       //                                        }
       //                                    }
       //                                }
       //                            }
       //                        }

       //                        if (slotFilePath != null && slotFilePath.Count > 1)
       //                        {
       //                            Guid slotoutputGuid = Guid.NewGuid();
       //                            string slotoutputpath  = folderPath +"\\"+ slotoutputGuid +".mp4";
       //                            FFMPEG.ConcatenateVideosForPreview(slotFilePath, slotoutputpath);
       //                            HttpStatusCode code = HttpStatusCode.UseProxy;
                                  
       //                            byte[] slotpreviewFile;
       //                            using (WebClient client = new WebClient())
       //                            {
       //                                //slotpreviewFile = client.DownloadData(slotoutputpath);
       //                                slotpreviewFile = File.ReadAllBytes(slotoutputpath);
       //                                if (slotpreviewFile.Length > 1200)
       //                                {
       //                                    MSApi api = new MSApi();
       //                                    string bucketpath = @"SlotPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";
                                          
       //                                    MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

       //                                    resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
       //                                    resource.ResourceTypeId = ((int)ResourceTypes.Video).ToString();
       //                                    resource.CreationDate = DateTime.UtcNow.ToString();
       //                                    resource.LastUpdateDate = resource.CreationDate;
       //                                    resource.IsActive = "True";
       //                                    resource.ServerId = serverservice.GetAllServer().Where(a => a.IsDefault).First().ServerId.ToString();
       //                                    resource.BucketId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaServerBucketId"].ToString());
       //                                    resource.Source =  slotoutputGuid+".mp4";
       //                                    resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
       //                                    resource.SystemType = 5;
       //                                    resource.IsPrivate = "True";

       //                                    MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);
                                          
       //                                    try{ CreateBucket(bucketpath);} catch (Exception ex) {}
                                         
       //                                    code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", slotpreviewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());
                                          
       //                                    if (code == HttpStatusCode.OK)
       //                                    {
       //                                        Slot slott = slotService.GetSlot(slot.SlotId);
       //                                        slott.PreviewGuid = output.Guid;
       //                                        slotService.UpdateSlot(slott);
       //                                        model.SlotPreviews.Add(slot.SlotId, output.Guid.ToString());
       //                                    }

       //                                }

       //                            }

       //                        }
                               
       //                    }
       //                }
       //            }
       //        }


       //        if (downloadedFilePath != null && downloadedFilePath.Count > 1)
       //        {
       //            Guid EpisodePreviewGuid = Guid.NewGuid();
       //            string episodepreviewpath  = folderPath+"\\"+EpisodePreviewGuid+".mp4";
       //            FFMPEG.ConcatenateVideosForPreview(downloadedFilePath, episodepreviewpath);
       //            HttpStatusCode code = HttpStatusCode.UseProxy;
                   
       //            byte[] previewFile;
       //            using (WebClient client = new WebClient())
       //            {
       //                previewFile = client.DownloadData(episodepreviewpath);
       //                if (previewFile.Length > 1200)
       //                {
       //                    MSApi api = new MSApi();
       //                    string bucketpath = @"RunDownPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";

       //                    MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

       //                    resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
       //                    resource.ResourceTypeId = ((int)ResourceTypes.Video).ToString();
       //                    resource.CreationDate = DateTime.UtcNow.ToString();
       //                    resource.LastUpdateDate = resource.CreationDate;
       //                    resource.IsActive = "True";
       //                    resource.ServerId = serverservice.GetAllServer().Where(a => a.IsDefault).First().ServerId.ToString();
       //                    resource.BucketId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaServerBucketId"].ToString());
       //                    resource.Source = EpisodePreviewGuid + ".mp4";
       //                    resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
       //                    resource.SystemType = 5;
       //                    resource.IsPrivate = "True";

       //                    MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);

       //                    try { CreateBucket(bucketpath); } catch (Exception ex) { }
                          
       //                    code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", previewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());
       //                    if (code == HttpStatusCode.OK)
       //                    {
       //                        Episode episode  =  episodeService.GetEpisode(episodeid);
       //                        episode.PreviewGuid = output.Guid;
       //                        episodeService.UpdateEpisode(episode);
       //                      //  Directory.Delete(folderPath, true);
       //                        model.EpisodePreview = output.Guid;
       //                        return model;
       //                    }
       //                }

       //            }
                   
                  
       //        }
       //    }
       //    return null;
       //}

       public EpisodePreviewModel ffmpegCreaterundownPreview(int episodeid)
       {


           IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
           ISegmentService segmentservice = IoC.Resolve<ISegmentService>("SegmentService");
           ISlotService slotService = IoC.Resolve<ISlotService>("SlotService");
           ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
           ISlotTemplateScreenElementService slotTemplateScreenElementService = IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
           MS.Core.IService.IServerService serverservice = IoC.Resolve<MS.Core.IService.IServerService>("ServerService");

           EpisodePreviewModel model = new EpisodePreviewModel();

           List<string> downloadedFilePath = new List<string>();
           List<string> slotFilePath;
           List<Segment> segments = segmentservice.GetSegmentByEpisodeId(episodeid);

           if (segments != null)
           {
               segments = segments.OrderBy(x => x.SequnceNumber).ToList();
               Guid guid = Guid.NewGuid();
               string folderPath = ConfigurationManager.AppSettings["TempContentLocation"].ToString() + guid.ToString();
               if (!Directory.Exists(folderPath))
               {
                   Directory.CreateDirectory(folderPath);
               }

               foreach (Segment segment in segments)
               {
                   List<Slot> slots = slotService.GetSlotBySegmentId(segment.SegmentId);

                   if (slots != null)
                   {
                       slots = slots.OrderBy(x => x.SequnceNumber).ToList();

                       foreach (Slot slot in slots)
                       {
                           List<SlotScreenTemplate> slotscreentemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slot.SlotId);

                           if (slotscreentemplates != null)
                           {
                               slotscreentemplates = slotscreentemplates.OrderBy(x => x.SequenceNumber).ToList();
                               slotFilePath = new List<string>();
                               foreach (SlotScreenTemplate slotscreentemplate in slotscreentemplates)
                               {
                                   byte[] data;
                                   using (WebClient client = new WebClient())
                                   {
                                       data = client.DownloadData(ConfigurationManager.AppSettings["MsApi"].ToString() + "api/Resource/getresource/" + slotscreentemplate.ThumbGuid.ToString());
                                   }

                                   if (data.Length > 0)
                                   {
                                       Guid fileguid = Guid.NewGuid();

                                       File.WriteAllBytes(folderPath + "\\" + fileguid.ToString() + ".jpg", data);
                                       int Duration = (slotscreentemplate.Duration.HasValue && slotscreentemplate.Duration.Value > 0) ? slotscreentemplate.Duration.Value : 10;
                                       int From = 0;
                                       string videopath = folderPath + "\\" + fileguid.ToString() + ".ts";
                                       FFMPEG.CreateVideofromThumb(folderPath + "\\" + fileguid.ToString() + ".jpg", folderPath + "\\", fileguid.ToString() + ".ts", TimeSpan.FromSeconds(10));
                                       slotFilePath.Add(videopath);
                                       downloadedFilePath.Add(videopath);

                                       List<SlotTemplateScreenElement> slotTemplateScreenElements = slotTemplateScreenElementService.GetSlotTemplateScreenElementBySlotScreenTemplateId(slotscreentemplate.SlotScreenTemplateId);

                                       if (slotTemplateScreenElements != null)
                                       {
                                           slotTemplateScreenElements = slotTemplateScreenElements.Where(a => a.ScreenElementId == 3 || a.ScreenElementId == 13).ToList();

                                           if (slotTemplateScreenElements != null)
                                           {
                                               foreach (SlotTemplateScreenElement slotTemplateScreenElement in slotTemplateScreenElements)
                                               {

                                                   byte[] overlapdata = null;
                                                   using (WebClient client = new WebClient())
                                                   {
                                                       if (slotTemplateScreenElement.ResourceGuid != null)
                                                       {
                                                           overlapdata = client.DownloadData(ConfigurationManager.AppSettings["MsApi"].ToString() + "api/Resource/getresource/" + slotTemplateScreenElement.ResourceGuid.ToString());
                                                       }
                                                   }

                                                   if (overlapdata.Length > 1200)
                                                   {
                                                       Guid embeddedVideoGuid = Guid.NewGuid();
                                                       string path = "";

                                                       if (slotTemplateScreenElement.ScreenElementId == 3)
                                                       {
                                                           path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".ts";
                                                           File.WriteAllBytes(path, overlapdata);
                                                           FFMPEG.EmbedVideo(videopath, path, ConfigurationManager.AppSettings["ThumbTemplLocation"].ToString() + "\\" + embeddedVideoGuid + ".mp4", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(Duration));
                                                       }
                                                       else if (slotTemplateScreenElement.ScreenElementId == 13 || slotTemplateScreenElement.ScreenElementId == 8)
                                                       {
                                                           path = folderPath + "\\" + embeddedVideoGuid.ToString() + ".jpg";
                                                           File.WriteAllBytes(path, overlapdata);
                                                           FFMPEG.EmbedImage(videopath, path, ConfigurationManager.AppSettings["ThumbTemplLocation"].ToString() + "\\" + embeddedVideoGuid + ".mp4", slotTemplateScreenElement.Top, slotTemplateScreenElement.Left, slotTemplateScreenElement.Right, slotTemplateScreenElement.Bottom, TimeSpan.FromSeconds(From), TimeSpan.FromSeconds(Duration));
                                                       }

                                                   }
                                               }
                                           }
                                       }
                                   }
                               }

                               if (slotFilePath != null && slotFilePath.Count > 1)
                               {
                                   Guid slotoutputGuid = Guid.NewGuid();
                                   string slotoutputpath = folderPath + "\\" + slotoutputGuid + ".mp4";
                                   FFMPEG.ConcatenateVideosForPreview(slotFilePath, slotoutputpath);
                                   HttpStatusCode code = HttpStatusCode.UseProxy;

                                   byte[] slotpreviewFile;
                                   using (WebClient client = new WebClient())
                                   {
                                       //slotpreviewFile = client.DownloadData(slotoutputpath);
                                       slotpreviewFile = File.ReadAllBytes(slotoutputpath);
                                       if (slotpreviewFile.Length > 1200)
                                       {
                                           MSApi api = new MSApi();
                                           string bucketpath = @"SlotPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";

                                           MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

                                           resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
                                           resource.ResourceTypeId = ((int)NMS.Core.Enums.ResourceTypes.Video).ToString();
                                           resource.CreationDate = DateTime.UtcNow.ToString();
                                           resource.LastUpdateDate = resource.CreationDate;
                                           resource.IsActive = "True";
                                           resource.ServerId = serverservice.GetAllServer().Where(a => a.IsDefault).First().ServerId.ToString();
                                           resource.BucketId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaServerBucketId"].ToString());
                                           resource.Source = slotoutputGuid + ".mp4";
                                           resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
                                           resource.SystemType = 5;
                                           resource.IsPrivate = "True";

                                           MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);

                                           try { CreateBucket(bucketpath); }
                                           catch (Exception ex) { }

                                           code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", slotpreviewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());

                                           if (code == HttpStatusCode.OK)
                                           {
                                               Slot slott = slotService.GetSlot(slot.SlotId);
                                               slott.PreviewGuid = output.Guid;
                                               slotService.UpdateSlot(slott);
                                               model.SlotPreviews.Add(slot.SlotId, output.Guid.ToString());
                                           }

                                       }

                                   }

                               }

                           }
                       }
                   }
               }


               if (downloadedFilePath != null && downloadedFilePath.Count > 1)
               {
                   Guid EpisodePreviewGuid = Guid.NewGuid();
                   string episodepreviewpath = folderPath + "\\" + EpisodePreviewGuid + ".mp4";
                   FFMPEG.ConcatenateVideosForPreview(downloadedFilePath, episodepreviewpath);
                   HttpStatusCode code = HttpStatusCode.UseProxy;

                   byte[] previewFile;
                   using (WebClient client = new WebClient())
                   {
                       previewFile = client.DownloadData(episodepreviewpath);
                       if (previewFile.Length > 1200)
                       {
                           MSApi api = new MSApi();
                           string bucketpath = @"RunDownPreviews\" + DateTime.UtcNow.Year + "\\" + DateTime.UtcNow.Month + "\\" + DateTime.UtcNow.Day + "\\";

                           MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();

                           resource.ResourceStatusId = ((int)ResourceStatuses.Uploading).ToString();
                           resource.ResourceTypeId = ((int)NMS.Core.Enums.ResourceTypes.Video).ToString();
                           resource.CreationDate = DateTime.UtcNow.ToString();
                           resource.LastUpdateDate = resource.CreationDate;
                           resource.IsActive = "True";
                           resource.ServerId = serverservice.GetAllServer().Where(a => a.IsDefault).First().ServerId.ToString();
                           resource.BucketId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaServerBucketId"].ToString());
                           resource.Source = EpisodePreviewGuid + ".mp4";
                           resource.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString();
                           resource.SystemType = 5;
                           resource.IsPrivate = "True";

                           MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(resource);

                           try { CreateBucket(bucketpath); }
                           catch (Exception ex) { }

                           code = api.PostMedia(output.Guid.ToString(), true, output.Guid + ".mp4", previewFile, "", "video/mp4", new System.Collections.Specialized.NameValueCollection());
                           if (code == HttpStatusCode.OK)
                           {
                               Episode episode = episodeService.GetEpisode(episodeid);
                               episode.PreviewGuid = output.Guid;
                               episodeService.UpdateEpisode(episode);
                               //  Directory.Delete(folderPath, true);
                               model.EpisodePreview = output.Guid;
                               return model;
                           }
                       }

                   }


               }
           }
           return null;

       }

       //public Episode
       private void CreateBucket(string bucketpath)
       {
           MSApi api = new MSApi();
           string path = "";
           bool isExist = false;
           bucketpath = bucketpath.Substring(0, bucketpath.LastIndexOf("\\"));
           string[] folders = bucketpath.Split(new char[] { '\\' });
           foreach (string item in folders)
           {
               path += "\\" + item;
               api.CreateSubBucket(Convert.ToInt32(ConfigurationManager.AppSettings["MediaServerBucketId"].ToString()), ConfigurationManager.AppSettings["MediaServerAPIKey"].ToString(), path);
           }

       }



    }
}
