﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using HtmlAgilityPack;
using MS.Core.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Repository;
using NMS.Service;

namespace NMS.TestApp
{
    public class TestImportNewsThread
    {

        private class ImportNewsThreadArgument
        {
            public string FolderPath { get; set; }
            public int SourceType { get; set; }
            public string Source { get; set; }
            public string Language { get; set; }
        }

        public string TestNews()
        {
            ImportNewsThreadArgument arg = new ImportNewsThreadArgument();
            arg.FolderPath = "http://54.187.177.10//testnews//";
            arg.Source = "Urdu SAMAA News";
            arg.Language = "ur";
            arg.SourceType = 1;

            INewsService newsService = IoC.Resolve<INewsService>("NewsService");
            ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            try
            {
                Uri Uri = new System.Uri(arg.FolderPath);
                string host = Uri.Host;
                ScrapMaxDates ScrapMaxDate = new ScrapMaxDates();
                ScrapMaxDate.SourceName = arg.Source;
                ScrapMaxDate.MaxUpdateDate = DateTime.UtcNow.AddDays(-1);
                DateTime dtMaxScrapDate = newsService.GetMaxScrapDate(ScrapMaxDate);
                string path = arg.FolderPath;
                string ListRegularExp = RegExpressions.ListExp;
                string UrlExp = RegExpressions.UrlExp;
                WebClient wc = new WebClient();
                wc.Encoding = UTF8Encoding.UTF8;

                HttpWebRequestHelper WebHelper = new HttpWebRequestHelper();
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(wc.DownloadString(path));
                wc.Dispose();

                string[] HtmlArr = document.DocumentNode.InnerHtml.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                string RawUrl = "";
                string DateToCompare = "";
                string pattern1 = "$1";
                string pattern2 = "$2";
                foreach (string val in HtmlArr)
                {
                    RawUrl = "";
                    DateToCompare = Regex.Replace(val, ListRegularExp, pattern1, RegexOptions.IgnoreCase);
                    try
                    {
                        DateTime dtdate = new DateTime();
                        if (DateTime.TryParse(DateToCompare, out dtdate))
                        {
                            if (Convert.ToDateTime(DateToCompare) > dtMaxScrapDate)
                            {
                                RawUrl = Regex.Replace(val, UrlExp, pattern2, RegexOptions.IgnoreCase);
                            }
                            if (!String.IsNullOrEmpty(RawUrl))
                            {
                                ScrapMaxDate.MaxUpdateDate = dtdate;
                                dynamic jo = JsonConvert.DeserializeObject(wc.DownloadString("http://" + host + "//" + RawUrl));
                                wc.Dispose();

                                int Newscounter = 0;
                                int DiscrepencyCatCount = 0;
                                int DiscrepencyLocCount = 0;
                                InsertNewsStatus newsStatus = InsertNewsStatus.Inserted;
                                foreach (var _item in jo.Result)
                                {
                                    string NewsTitle = "";
                                    string NewsTitleOrg = "";
                                    string NewsDesc = "";
                                    string CelebrityName = "";
                                    RawNews news = new RawNews();
                                    dynamic item = null;
                                    item = (_item is JArray && _item.Count > 0) ? _item[0] : _item;
                                    if (item != null && ((item is JArray && item.Count > 0) || !(item is JArray)))
                                    {
                                        if (arg.Source == "Twitter")
                                        {
                                            Celebrity celebrity = celebrityService.GetBySocialMediaAccountUserName(item["Twitter_UserName"].ToString(),(int)SocialMediaType.Twitter);
                                            news.Categories.Add(celebrity.Category);
                                            news.Location = celebrity.Location;
                                            CelebrityName = celebrity.Name + ": ";
                                        }
                                        NewsTitleOrg = WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                                        NewsTitle = CelebrityName + WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                                        news.Title = NewsTitle.Trim();
                                        news.Source = arg.Source;
                                        news.SourceType = (FilterTypes)arg.SourceType;

                                        if (news.Title.Length > 1 && !newsService.CheckIfNewsExists(news, out newsStatus))
                                        {
                                            news.Categories = new List<string>();
                                            if (!(arg.Source == "Twitter"))
                                            {
                                                news.Location = item["News_Location"];
                                                if ((item["News_Category"] is JArray))
                                                {
                                                    foreach (var category in (item["News_Category"] as JArray))
                                                    {
                                                        if (category.ToString().ToLower().Trim() != "home")
                                                            news.Categories.Add(category.ToString().Trim());
                                                    }
                                                }
                                                else
                                                {
                                                    if (item["News_Category"].ToString().ToLower().Trim() != "home")
                                                        news.Categories.Add(item["News_Category"].ToString().Trim());
                                                }
                                            }
                                            news.Location = news.Location.Trim().Length > 30 ? "" : news.Location.Trim();

                                            HtmlDocument doc = new HtmlDocument();
                                            HtmlNodeCollection nodecollections = null;
                                            //if (item["News_Resources"] != null && item["News_Resources"].ToString().Lenght > 0)
                                            //{                                             
                                            //    doc.LoadHtml(item["News_Resources"].ToString());                                                
                                            //}
                                            //else
                                            //{                                                
                                            //collection = Regex.Matches(item["News_Body"].ToString(), RegExpressions.MediaExtraction, RegexOptions.IgnoreCase);                                                
                                            doc.LoadHtml(item["News_Body"].ToString());
                                            //}


                                            HtmlNodeCollection delteNodes = doc.DocumentNode.SelectNodes("//img[contains(@style ,'display:none')]");
                                            if (delteNodes != null && delteNodes.Count > 0)
                                            {
                                                foreach (HtmlNode htmlNode in delteNodes)
                                                {
                                                    htmlNode.Remove();
                                                }
                                            }

                                            //Code for Video Extraction                                            
                                            HtmlNode VideoNode = doc.DocumentNode.SelectSingleNode("//div//script[1]");
                                            if (VideoNode != null)
                                            {
                                                MatchCollection videoCollection = Regex.Matches(VideoNode.InnerHtml, "http://((?!><).)*?(\\.mp4|\\.flv)", RegexOptions.IgnoreCase);
                                                if (videoCollection != null && videoCollection.Count > 0)
                                                {
                                                    HtmlNode AddNode = new HtmlNode(HtmlNodeType.Element, doc, 0);
                                                    VideoNode.InnerHtml = "<video src=\"" + videoCollection[0].Value + "\"/>";
                                                    doc.DocumentNode.AppendChild(AddNode);
                                                }
                                            }


                                            nodecollections = doc.DocumentNode.SelectNodes("//*[@src]");

                                            try
                                            {
                                                if (nodecollections != null && nodecollections.Count > 0)
                                                {
                                                    news.ImageGuids = new List<Resource>();
                                                    foreach (HtmlNode htmlNode in nodecollections)
                                                    {
                                                        MatchCollection matchCollection = Regex.Matches(htmlNode.OuterHtml, RegExpressions.MediaExtraction, RegexOptions.IgnoreCase);
                                                        if (matchCollection.Count > 0)
                                                        {
                                                            
                                                            foreach (Match match in matchCollection)
                                                            {
                                                                string urlPrefix = "";
                                                                if (!match.ToString().Contains("www.") && !match.ToString().Contains("http"))
                                                                {
                                                                    Uri uri = new Uri(jo.Url.ToString());
                                                                    urlPrefix = "http://" + uri.Host;
                                                                }
                                                                MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                                                                resource.ResourceStatusId = ((int)ResourceStatuses.DownloadPending).ToString();
                                                                resource.Source = urlPrefix + match.ToString().Replace("src=\"", "").Replace("\"", "").Replace("src='", "").Replace("'", "");
                                                                if (!Regex.Match(resource.Source, RegExpressions.VideoExtraction, RegexOptions.IgnoreCase).Success)
                                                                // (resource.Source.Substring(resource.Source.LastIndexOf('.') + 1) != "mp4")
                                                                {
                                                                    resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                                                                }
                                                                else
                                                                {
                                                                    resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                                                                }
                                                                resource.IsFromExternalSource = true;

                                                                //Check Resource Alt Title
                                                                string resourceTitle = "";
                                                                if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["alt"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["alt"].Value))
                                                                {
                                                                    resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["alt"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                                                }
                                                                else if (htmlNode.Attributes != null && htmlNode.Attributes.Count > 0 && htmlNode.Attributes["title"] != null && !String.IsNullOrEmpty(htmlNode.Attributes["title"].Value))
                                                                {
                                                                    resourceTitle = WebUtility.HtmlDecode(Regex.Replace(htmlNode.Attributes["title"].Value, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase)).Trim();
                                                                }

                                                                //Resouce Additional Meta Population
                                                                //resource.Location = news.Location;
                                                                //resource.Category = news.Categories.Count > 0 ? news.Categories[0] : null;
                                                                //resource.Caption = news.Title;

                                                                HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", resource, null);
                                                                using (TextReader treader = new StreamReader(response.GetResponseStream()))
                                                                {
                                                                    string str = treader.ReadToEnd();
                                                                    MS.Core.Entities.Resource res = JsonConvert.DeserializeObject<MS.Core.Entities.Resource>(str);
                                                                    Resource _res = new Resource();
                                                                    _res.CopyFrom(res);
                                                                    _res.Caption = resourceTitle;
                                                                    news.ImageGuids.Add(_res);
                                                                }
                                                                response.Close();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {

                                                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                                                logService.InsertSystemEventLog(string.Format("ImportNewsThread: Error in {0} {1}", arg.Source, ex.Message), ex.StackTrace, EventCodes.Error);
                                            }

                                            NewsDesc = Regex.Replace(item["News_Body"].ToString().Replace("\n", "").Replace("\r", "").Replace("\t", ""), RegExpressions.ScriptsRemoval, "", RegexOptions.Multiline);
                                            if (arg.Source == "Dawn News")
                                            {
                                                HtmlDocument docDawn = new HtmlDocument();
                                                doc.LoadHtml(item["News_Body"].ToString());
                                                HtmlNodeCollection removenodeCollection = docDawn.DocumentNode.SelectNodes("//a[@class='slideshow__prev'] | //a[@class='slideshow__next']");
                                                if (removenodeCollection != null && removenodeCollection.Count > 0)
                                                {
                                                    foreach (HtmlNode node in removenodeCollection)
                                                    {
                                                        node.Remove();
                                                    }
                                                    NewsDesc = doc.DocumentNode.InnerHtml;
                                                }
                                            }

                                            NewsDesc = WebUtility.HtmlDecode(Regex.Replace(NewsDesc, RegExpressions.HtmlTagsFilteration, "", RegexOptions.IgnoreCase));
                                            //NewsTitle = WebUtility.HtmlDecode(Regex.Replace(item["News_Heading"].ToString(), "<.*?>", "", RegexOptions.IgnoreCase));
                                            // news.Title = item["News_Heading"];
                                            if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                                                news.PublishTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                                            if (item["News_Updated_Time"] != null && !String.IsNullOrEmpty(item["News_Updated_Time"].ToString()))
                                                news.UpdateTime = DateTime.Parse(item["News_Updated_Time"].ToString());
                                            else if (item["News_Publish_Time"] != null && !String.IsNullOrEmpty(item["News_Publish_Time"].ToString()))
                                                news.UpdateTime = DateTime.Parse(item["News_Publish_Time"].ToString());

                                            if (arg.Source == "Twitter")
                                            {
                                                news.Description = NewsTitleOrg.Trim();
                                            }
                                            else
                                            {
                                                news.Description = NewsDesc.Trim();
                                            }

                                            //news.Categories = 
                                            if (news.Categories.Count <= 0)
                                            {
                                                news.Categories.Add("Misc");
                                            }

                                            news.PublishTime = news.PublishTime.ToUniversalTime();
                                            news.UpdateTime = news.UpdateTime.ToUniversalTime();
                                            news.Author = item["News_Author"];
                                            news.LanguageCode = arg.Language;
                                            news.NewsType = NewsTypes.Story;
                                            news.Url = item["URL"];
                                            if(news.ImageGuids.Count>0)                                            
                                            newsStatus = newsService.InsertRawNews(news);
                                            if (newsStatus == InsertNewsStatus.DiscrepancyCategory)
                                                DiscrepencyCatCount++;
                                            else if (newsStatus == InsertNewsStatus.DiscrepancyLocation)
                                                DiscrepencyLocCount++;
                                            else if (newsStatus == InsertNewsStatus.Inserted)
                                                Newscounter++;
                                            //output.Add(news);
                                        }
                                    }
                                }
                                //Log News Count                                
                                if (Newscounter > 0 || (DiscrepencyCatCount + DiscrepencyLocCount) > 0)
                                {
                                    ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                                    string msg = string.Format("ImportNewsThread: {0}-> {1} of {2} ,DCC = {3},DLC = {4} News downloaded successfully", arg.Source, Newscounter, jo.Result.Count, DiscrepencyCatCount, DiscrepencyLocCount);
                                    logService.InsertSystemEventLog(msg, "", EventCodes.Log);
                                }

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                        logService.InsertSystemEventLog(string.Format("ImportNewsThread: Error in {0} {1}", arg.Source, ex.Message), ex.StackTrace, EventCodes.Error);
                    }

                }
                newsService.UpdateScrapDate(ScrapMaxDate);
                return "Successfull";

            }
            catch (Exception exp)
            {
                ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
                logService.InsertSystemEventLog(string.Format("ImportNewsThread: Error in {0} {1}", arg.Source, exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }


    }
}
