﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using NMS.Core.Entities;

namespace NMS.TestApp
{
    public class TickerEncoding
    {

        public void run()
        {

            NMS.Core.IService.ITickerService tickerService = NMS.Core.IoC.Resolve<NMS.Core.IService.ITickerService>("TickerService");
            NMS.Core.IService.ITickerLineService tickerLineService = NMS.Core.IoC.Resolve<NMS.Core.IService.ITickerLineService>("TickerLineService");
            NMS.Core.IService.ITickerCategoryService tickerCategoryService = NMS.Core.IoC.Resolve<NMS.Core.IService.ITickerCategoryService>("TickerCategoryService");

            string mergeString = string.Empty;
            List<TickerCategory> tickerCategories = new List<TickerCategory>();

            tickerCategories = tickerCategoryService.GetTickerCategory(DateTime.Now.AddYears(-1)).Take(3).ToList();
            List<Ticker> alltickers = tickerService.GetTickers(null, DateTime.Now.AddYears(-1)).ToList();


            foreach (var item in tickerCategories)
            {
                mergeString = string.Empty;
                List<Ticker> tickers = alltickers.Where(entity => entity.CategoryId == item.CategoryId).ToList();

                if (tickers != null && tickers.Count > 0)
                {
                    for (int i = 0; i < tickers.Count; i++)
                    {
                        Ticker ticker = tickers[i];
                        List<TickerLine> tickerLines = tickerLineService.GetTickerLineByTickerId(ticker.TickerId);
                        if (tickerLines != null && tickerLines.Count > 0)
                        {
                            Encoding enc = new UnicodeEncoding();
                            foreach (var line in tickerLines)
                            {
                                mergeString += enc.GetString(enc.GetBytes(string.Format("{0}{1}", line.Text, "##7")));

                            }
                            ticker.TickerLines = tickerLines;
                        }

                    }
                }

                string cmdText = @"INSERT INTO MCRAsimTickerRundown (TickerId,[Text],CategoryName,CategoryId,SequenceNumber,LanguageCode,CreationDate,TickerLineId)
                                   VALUES (@TickerId,@Text,@CategoryName,@CategoryId,@SequenceNumber,@LanguageCode,@CreationDate,@TickerLineId)";

                SqlParameter[] parameterArray = new SqlParameter[]{
					 new SqlParameter("@TickerId",1),
                     new SqlParameter("@Text",mergeString),
                     new SqlParameter("@CategoryName",item.Name),
                     new SqlParameter("@CategoryId",item.CategoryId),
                     new SqlParameter("@SequenceNumber",item.SequenceNumber),
                     new SqlParameter("@LanguageCode","ur"),
                     new SqlParameter("@CreationDate",DateTime.Now),
                     new SqlParameter("@TickerLineId",999)
                };
                var ex = NMS.Core.SqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["conn"].ToString(), CommandType.Text, cmdText, parameterArray);
            }
        }

    }
}
