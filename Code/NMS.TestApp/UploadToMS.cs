﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ControlPanel.Core.IService;
using MS.Core.Entities;
using MS.Core.Enums;
using MS.MediaIntegration.API;
using Newtonsoft.Json;
using NMS.Core;
using NMS.Core.Entities;
using NMS.Core.Helper;
using NMS.Core.IService;

namespace NMS.TestApp
{
    public class UploadToMS
    {
        public void Upload(string folderPath, bool IsNew = false)
        {

            string[] files = Directory.GetFiles(folderPath);


            string ResourceStr = "";

            HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
            List<string> ResourceGuid = new List<string>();
            List<ScrapResource> ScrapResources = new List<ScrapResource>();
            if (files.Length > 0)
            {
                foreach (var file in files)
                {
                    MS.Core.DataTransfer.Resource.PostInput resource = new MS.Core.DataTransfer.Resource.PostInput();
                    ScrapResource srapresource = new ScrapResource();
                    resource.ResourceStatusId = ((int)ResourceStatuses.Completed).ToString();
                    resource.Source = file;
                    if (Regex.Match(resource.Source, ".*(mp4|flv)", RegexOptions.IgnoreCase).Success)
                    {
                        resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Video).ToString();
                    }
                    else if (Regex.Match(resource.Source, ".*(mp3|wav)", RegexOptions.IgnoreCase).Success)
                    {
                        resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Audio).ToString();
                    }
                    else
                    {
                        resource.ResourceTypeId = ((int)MS.Core.Enums.ResourceTypes.Image).ToString();
                    }
                    resource.IsFromExternalSource = false;
                    srapresource.Source = resource.Source;
                    srapresource.StatusId = Convert.ToInt32(resource.ResourceStatusId);
                    srapresource.DownloadedFilePath = resource.Source;

                    if (IsNew)
                    {
                        MSApi MediaApi = new MSApi();
                        string dir1 = "/" + "Test Folder";
                        string dir2 = "/" + "Test Folder" + "/" + DateTime.UtcNow.Year.ToString();
                        string dir3 = "/" + "Test Folder" + "/" + DateTime.UtcNow.Year.ToString() + "/" + DateTime.UtcNow.Month.ToString("00");
                        List<string> lstDir = new List<string>();
                        lstDir.Add(dir1);
                        lstDir.Add(dir2);
                        lstDir.Add(dir3);

                        foreach (string d in lstDir)
                        {
                            var Result = MediaApi.CreateSubBucket(((int)NMS.Core.Enums.NMSBucket.NMSBucket), NMS.Core.AppSettings.MediaServerApiKey, d);
                        }
                        resource.FilePath = dir3 + "/" + Path.GetFileName(resource.Source);
                        resource.BucketId = ((int)NMS.Core.Enums.NMSBucket.NMSBucket);
                        resource.ApiKey = NMS.Core.AppSettings.MediaServerApiKey;
                        MS.Core.Entities.Resource res = new MS.Core.Entities.Resource();
                        res.CopyFrom(MediaApi.PostResource(resource));
                        srapresource.ResourceGuid = res.Guid;
                    }
                    else
                    {
                        HttpWebResponse response = requestHelper.PostRequest(NMS.Core.AppSettings.MediaServerUrl + "/PostResource", resource, null);
                        using (TextReader treader = new StreamReader(response.GetResponseStream()))
                        {
                            string str = treader.ReadToEnd();
                            MS.Core.Entities.Resource res = JsonConvert.DeserializeObject<MS.Core.Entities.Resource>(str);
                            //ResourceGuid.Add(res.Guid.ToString());
                            srapresource.ResourceGuid = res.Guid;
                        }
                        response.Close();
                    }
                    Console.WriteLine("Guid Recieved:{0} for {1}", srapresource.ResourceGuid, file);
                    ScrapResources.Add(srapresource);
                }
            }



            // Upload Images
            try
            {
                WebClient client = new WebClient();
                HttpWebRequestHelper proxy = new HttpWebRequestHelper();

                if (ScrapResources != null && ScrapResources.Count > 0)
                {
                    int successfull = 0;
                    foreach (var res in ScrapResources)
                    {
                        try
                        {
                            string ext = System.IO.Path.GetExtension(res.Source).Trim('.');
                            string fileName = System.IO.Path.GetFileName(res.Source);
                            byte[] data = File.ReadAllBytes(res.DownloadedFilePath);// client.DownloadData(res.DownloadedFilePath);
                            client.Dispose();
                            if (IsNew)
                            {
                                MSApi MediaApi = new MSApi();
                                MediaApi.PostMedia(res.ResourceGuid.ToString(), true, fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());
                            }
                            else
                            {
                                proxy.HttpUploadFile(NMS.Core.AppSettings.MediaServerUrl + "/PostMedia?Id=" + res.ResourceGuid.ToString() + "&IsHD=true", fileName.Trim('"'), data, "file", "image/jpeg", new NameValueCollection());
                            }
                            Console.WriteLine("ImageUploaded: {0}", res.DownloadedFilePath);
                            Console.WriteLine("Resource: {0}", res.ResourceGuid);
                            successfull++;
                        }
                        catch (Exception exp)
                        {
                            Console.WriteLine(exp.Message);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }


    }
}
