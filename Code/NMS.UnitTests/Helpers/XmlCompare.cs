﻿using Microsoft.XmlDiffPatch;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NMS.UnitTests.Helpers
{
    public class XmlCompare
    {
        public static bool CompareXml(string file1, string file2,string resultFile)
        {
            bool bIdentical = false;
            using (XmlReader reader1 = XmlReader.Create(new StringReader(file1)))
            {
                using (XmlReader reader2 = XmlReader.Create(new StringReader(file2)))
                {
                    string diffFile = resultFile;
                    StringBuilder differenceStringBuilder = new StringBuilder();

                    using (FileStream fs = new FileStream(diffFile, FileMode.Create))
                    {
                        using (XmlWriter diffGramWriter = XmlWriter.Create(fs))
                        {
                            XmlDiff xmldiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
                                                    XmlDiffOptions.IgnoreNamespaces | XmlDiffOptions.IgnoreWhitespace |
                                                    XmlDiffOptions.IgnorePrefixes);
                            bIdentical = xmldiff.Compare(file1, file2, false, diffGramWriter);
                            diffGramWriter.Close();
                        }
                    }
                    // cleaning up after we are done with the xml diff file
                    if (bIdentical)
                        File.Delete(diffFile);
                    return bIdentical;
                }
            }
        }  
    }
}
