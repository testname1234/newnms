﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NMS.UnitTests
{
    [TestClass]
    public class MediaServerTests
    {
        MockGenerator mock = new MockGenerator();

        [TestMethod]
        public void GetResourceOutputGuids()
        {

            var output = mock.GetResourceInfoByGuidIds();

        }

        [TestMethod]
        public void CropImage()
        {
            var output = mock.CropImage();

            if (output.IsSuccess)
                Assert.IsTrue(!string.IsNullOrEmpty(output.Data));
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void PostResourceAndMediaForProcessing()
        {
            var output = mock.PostResourceAndMediaForProcessing();

            if (!string.IsNullOrEmpty(output.Guid.ToString()))
                Assert.IsTrue(!string.IsNullOrEmpty(output.Guid.ToString()));
            else
                Assert.Fail();
        }

        [TestMethod]
        public void PostResourceAndMediaForProcessingFromMsApi()
        {
            var output = mock.PostResourceAndMediaFromMsApi();

            if (!string.IsNullOrEmpty(output.Guid.ToString()))
                Assert.IsTrue(!string.IsNullOrEmpty(output.Guid.ToString()));
            else
                Assert.Fail();
        }

        [TestMethod]
        public void PostResourceAndMedia()
        {
            var output = mock.PostResourceAndMedia();

            if (!string.IsNullOrEmpty(output.Guid.ToString()))
                Assert.IsTrue(!string.IsNullOrEmpty(output.Guid.ToString()));
            else
                Assert.Fail();
        }

        [TestMethod]
        public void PostResource()
        {
            var output = mock.PostResource();

            if(!string.IsNullOrEmpty(output.Guid.ToString()))
                Assert.IsTrue(!string.IsNullOrEmpty(output.Guid.ToString()));
            else
                Assert.Fail();
        }

        [TestMethod]
        public void ClipVideos()
        {
            mock.ClipVideos();

            Assert.IsTrue(1 == 1);
        }

        [TestMethod]
        public void GenerateCustomSnapShot()
        {
            mock.GenerateCustomSnapShot();
        }
    }
}
