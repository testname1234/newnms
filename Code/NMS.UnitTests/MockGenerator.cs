﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using MS.Core.DataTransfer.Resource.GET;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;

namespace NMS.UnitTests
{
    public class MockGenerator
    {
        string WebServerUrl = ConfigurationManager.AppSettings["WebServerUrl"];
        string MediaServerUrl = ConfigurationManager.AppSettings["MediaServerUrl"];

        private static List<Channel> _channels;
        public static List<Channel> Channels
        {
            get
            {
                if (_channels == null)
                {
                    IChannelService channelService = IoC.Resolve<IChannelService>("ChannelService");
                    return channelService.GetAllChannel();
                }
                return _channels;
            }
            set { _channels = value; }
        }

        private static List<ChannelVideo> _channelVideos;
        public static List<ChannelVideo> ChannelVideos
        {
            get
            {
                if (_channelVideos == null)
                {
                    IChannelVideoService channelVideosService = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
                    return channelVideosService.GetAllChannelVideo();
                }
                return _channelVideos;
            }
            set { _channelVideos = value; }
        }

        private static List<RadioStream> _radioStream;
        public static List<RadioStream> RadioStream
        {
            get
            {
                if (_radioStream == null)
                {
                    IRadioStreamService radioStreamService = IoC.Resolve<IRadioStreamService>("RadioStreamService");
                    return radioStreamService.GetAllRadioStream();
                }
                return _radioStream;
            }
            set { _radioStream = value; }
        }

        private static List<NewsPaperPage> _newsPaperPage;
        public static List<NewsPaperPage> NewsPaperPage
        {
            get
            {
                if (_newsPaperPage == null)
                {
                    INewsPaperPageService newsPaperPageService = IoC.Resolve<INewsPaperPageService>("NewsPaperPageService");
                    return newsPaperPageService.GetAllNewsPaperPage();
                }
                return _newsPaperPage;
            }
            set { _newsPaperPage = value; }
        }

        private static List<DailyNewsPaper> _dailyNewsPaperPage;
        public static List<DailyNewsPaper> DailyNewsPaperPage
        {
            get
            {
                if (_dailyNewsPaperPage == null)
                {
                    IDailyNewsPaperService dailyNewsPaperPageService = IoC.Resolve<IDailyNewsPaperService>("DailyNewsPaperService");
                    return dailyNewsPaperPageService.GetAllDailyNewsPaper();
                }
                return _dailyNewsPaperPage;
            }
            set { _dailyNewsPaperPage = value; }
        }

        private static List<Category> _categories;
        public static List<Category> Categories
        {
            get
            {
                if (_categories == null)
                {
                    ICategoryService categoryService = IoC.Resolve<ICategoryService>("CategoryService");
                    return categoryService.GetAllCategory().Where(x => x.IsApproved.HasValue && x.IsApproved.Value).ToList();
                }
                return _categories;
            }
            set { _categories = value; }
        }

        private static List<Location> _location;
        public static List<Location> Locations
        {
            get
            {
                if (_location == null)
                {
                    ILocationService locationService = IoC.Resolve<ILocationService>("LocationService");
                    return locationService.GetAllLocation().Where(x=>x.IsApproved.HasValue && x.IsApproved.Value).ToList();
                }
                return _location;
            }
            set { _location = value; }
        }

        private static List<FilterType> _filterTypes;
        public static List<FilterType> FilterTypes
        {
            get
            {
                if (_filterTypes == null)
                {
                    IFilterTypeService filterTypeService = IoC.Resolve<IFilterTypeService>("FilterTypeService");
                    return filterTypeService.GetAllFilterType();
                }
                return _filterTypes;
            }
            set { _filterTypes = value; }
        }

        private static List<Filter> _filters;
        public static List<Filter> Filters
        {
            get
            {
                if (_filters == null)
                {
                    IFilterService filterService = IoC.Resolve<IFilterService>("FilterService");
                    return filterService.GetAllFilter();
                }
                return _filters;
            }
            set { _filters = value; }
        }

        private static List<ScreenTemplate> _screenTemplate;
        public static List<ScreenTemplate> ScreenTemplates
        {
            get
            {
                if (_screenTemplate == null)
                {
                    IScreenTemplateService screenTemplateService = IoC.Resolve<IScreenTemplateService>("ScreenTemplateService");
                    return screenTemplateService.GetScreenTemplateByProgramId(1087);
                }
                return _screenTemplate;
            }
            set { _screenTemplate = value; }
        }

        private static List<Slot> _slots;
        public static List<Slot> Slots
        {
            get
            {
                if (_slots == null)
                {
                    ISlotService slotService = IoC.Resolve<ISlotService>("SlotService");
                    return slotService.GetAllSlot();
                }
                return _slots;
            }
            set { _slots = value; }
        }

        public NMS.Core.DataTransfer.News.PostInput GetMockReporterNews(int reporterID, NMS.Core.Enums.FilterTypes sourceType, bool insertResources = false, bool insertTags = false, bool insertResourceEdits = false, ResourceTypes resourceType = ResourceTypes.Video,int categoryId=-1)
        {
            NMS.Core.DataTransfer.News.PostInput input = new Core.DataTransfer.News.PostInput();
            input.CategoryIds = new List<int>();
            if (categoryId > 0)
                input.CategoryIds.Add(categoryId);
            else
                input.CategoryIds.AddRange(Faker.ArrayFaker.SelectFrom<int>(2, Categories.Select(x => x.CategoryId).ToArray()));
            input.Description = Faker.TextFaker.Sentences(4);
            //input.Description = "<div class=\"post-38202 post type-post status-publish format-standard has-post-thumbnail hentry category-karachi category-pakistan category-top-news block-content\"><p><span class=\"hover-effect delegate\"><span class=\"cover\" style=\"font-size:20px;\"><i></i><img class=\"image-border\" src=\"http://arynews.tv/en/wp-content/uploads/2014/07/police-encounter-890x395.jpg?1e801a\" alt=\"Four TTP terrorists killed in police encounter\"></span></span></p>";
            input.ReporterId = reporterID;
            input.Title = Faker.TextFaker.Sentence() + Faker.NumberFaker.Number(1, 2000);
            input.NewsDate = DateTime.UtcNow;
            input.FilterTypeId = (int)sourceType;
            input.LanguageCode = "en";
            input.NewsTypeId = (int)NMS.Core.Enums.NewsTypes.Package;
            input.LocationIds = new List<int>(Faker.ArrayFaker.SelectFrom<int>(1, Locations.Select(x => x.LocationId).ToArray()));
            if (insertResources)
            {
                input.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                for (int i = 0; i < Faker.NumberFaker.Number(1, 4); i++)
                {
                    NMS.Core.DataTransfer.Resource.PostInput res = new NMS.Core.DataTransfer.Resource.PostInput();
                    res.Guid = Guid.NewGuid().ToString();
                    res.ResourceTypeId = ((int)NMS.Core.Enums.ResourceTypes.Image).ToString();
                    input.Resources.Add(res);
                }
                input.ThumbnailUrl = input.Resources.First().Guid;
            }
            if (insertTags)
            {
                input.Tags = new List<Core.DataTransfer.Tag.PostInput>();
                input.Tags.Add(new NMS.Core.DataTransfer.Tag.PostInput() { Tag = "test" });
                input.Tags.Add(new NMS.Core.DataTransfer.Tag.PostInput() { Tag = "test news" });
            }
            if (insertResourceEdits)
            {
                if (resourceType == ResourceTypes.Video)
                {
                    var channelVideo = ChannelVideos.FirstOrDefault();
                    if (channelVideo != null)
                    {
                        input.ChannelId = channelVideo.ChannelId.Value;
                        input.ResourceEdit = new Core.Models.ResourceEdit();
                        input.ResourceEdit.Id = channelVideo.ChannelVideoId;
                        input.ResourceEdit.ResourceTypeId = (int)ResourceTypes.Video;
                        input.ResourceEdit.FileName = string.Format("Geo TV-{0}.mp4", Faker.NumberFaker.Number());

                        input.ResourceEdit.FromTos = new List<Core.Models.FromTo>();
                        input.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(5).TotalSeconds, To = TimeSpan.FromSeconds(10).TotalSeconds });
                        input.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(13).TotalSeconds, To = TimeSpan.FromSeconds(28).TotalSeconds });
                    }
                }
                else if (resourceType == ResourceTypes.Audio)
                {
                    var radioStream = RadioStream.FirstOrDefault();
                    if (radioStream != null)
                    {
                        input.RadioStationId = radioStream.RadioStationId;
                        input.ResourceEdit = new Core.Models.ResourceEdit();
                        input.ResourceEdit.Id = radioStream.RadioStreamId;
                        input.ResourceEdit.ResourceTypeId = (int)ResourceTypes.Audio;
                        input.ResourceEdit.FileName = string.Format("Radio-{0}.mp3", Faker.NumberFaker.Number());

                        input.ResourceEdit.FromTos = new List<Core.Models.FromTo>();
                        input.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(5).TotalSeconds, To = TimeSpan.FromSeconds(10).TotalSeconds });
                        input.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(13).TotalSeconds, To = TimeSpan.FromSeconds(28).TotalSeconds });
                    }
                }
                else if (resourceType == ResourceTypes.Image)
                {
                    var newspaperPage = NewsPaperPage.FirstOrDefault();
                    if (newspaperPage != null)
                    {
                        float top = 200;
                        float bottom = 800;
                        float right = 40000;
                        float left = 20000;

                        input.NewsPaperId = DailyNewsPaperPage.FirstOrDefault().NewsPaperId;
                        input.ResourceEdit = new Core.Models.ResourceEdit();
                        input.ResourceEdit.Id = newspaperPage.NewsPaperPageId;
                        input.ResourceEdit.ResourceTypeId = (int)ResourceTypes.Image;
                        input.ResourceEdit.FileName = string.Format("Dawn-{0}.jpg", Faker.NumberFaker.Number());

                        input.ResourceEdit.Top = top;
                        input.ResourceEdit.Bottom = bottom;
                        input.ResourceEdit.Right = right;
                        input.ResourceEdit.Left = left;

                    }
                }
            }
            return input;
        }

        public NMS.Core.DataTransfer.News.GetOutput GetSavedNews(int reporterID, NMS.Core.Enums.FilterTypes sourceType, bool insertResources = false, bool insertTags = false, bool insertResourceEdits = false, int categoryId = -1)
        {
            var input = GetMockReporterNews(reporterID, sourceType, insertResources, insertTags, insertResourceEdits, ResourceTypes.Video, categoryId);
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", input, null);
            if (output.IsSuccess)
                return output.Data;
            else throw new Exception("Could not add news");
        }

        public List<NMS.Core.DataTransfer.News.GetOutput> GetNewsWithUpdatesAndRelated(string id,string bunchId)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.News.GetOutput>>>(WebServerUrl + "api/news/GetNewsWithUpdates?id=" + id + "&bunchId=" + bunchId, null);
            if (output.IsSuccess)
                return output.Data;
            else throw new Exception("Could not get GetNewsWithUpdatesAndRelated");
        }

        public List<NMS.Core.DataTransfer.News.GetOutput> GetNewsByTerm(string term)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.News.GetOutput>>>(WebServerUrl + string.Format("api/News/GetNewsByTerm/{0}", term), null);
            return output.Data;
        }

        public NMS.Core.DataTransfer.DataTransfer<LoadInitialDataOutput> LoadInitialData(List<int> filterIDs, out LoadInitialDataInput _input)
        {
            LoadInitialDataInput input = new LoadInitialDataInput();
            List<NMS.Core.DataTransfer.Filter.GetOutput> filters = new List<Core.DataTransfer.Filter.GetOutput>();
            filters.CopyFrom(Filters.Where(x => filterIDs.Contains(x.FilterId)).ToList());
            input.Filters = filters;
            input.From = DateTime.UtcNow.AddDays(-1);
            input.To = DateTime.UtcNow;
            input.PageCount = 10;
            input.StartIndex = 0;
            input.Term = string.Empty;
            input.FilterCountLastUpdateDate = DateTime.UtcNow.AddDays(-1);
            input.UserId = 1;
            _input = input;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<LoadInitialDataOutput>>(WebServerUrl + "api/news/LoadInitialData", input, null);
        }


        public List<ScreenTemplate> MockGetScreenTemplatesByProgram()
        {
            List<ScreenTemplate> screenTemplates = new List<ScreenTemplate>();
            screenTemplates = ScreenTemplates;            
            return screenTemplates;
        }

        public List<Slot> MockGetSlots()
        {
            List<Slot> slots = new List<Slot>();
            slots = Slots;
            return slots;
        }

        public NMS.Core.DataTransfer.DataTransfer<LoadInitialDataOutput> ProducerPolling(LoadInitialDataInput input)
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<LoadInitialDataOutput>>(WebServerUrl + "api/news/ProducerPolling", input, null);
        }


        public List<NMS.Core.DataTransfer.Episode.GetOutput> GetSavedEpisodeMock()
        {            
            GetProgramEpisodeInput input = new GetProgramEpisodeInput();
            Program program = GetMockProgram();
            input.Date = GetTimeClosedToDayOfWeek((DayOfWeek)program.Schedule.WeekDayId);
            input.ProgramId = program.ProgramId;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>>(WebServerUrl + "api/program/GetProgramEpisode", input, null);
            if (output.IsSuccess)
                return output.Data;
            else throw new Exception("Could not Get Episode");
        }

        public Program GetMockProgram()
        {
            Program program = new Program();
            program.ChannelId = Channels.Where(x=>!x.IsOtherChannel).First().ChannelId;
            program.CreationDate = DateTime.UtcNow;
            program.LastUpdateDate = program.CreationDate.Value;
            program.IsActive = true;
            program.Name = Faker.NameFaker.Name() + " Show";
            IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
            program = programService.InsertProgram(program);

            ProgramSchedule schedule = new ProgramSchedule();
            schedule.WeekDayId = Faker.ArrayFaker.SelectFrom<int>(1, new int[] { 1, 2, 3, 4, 5, 6, 7 }).First();
            schedule.CreationDate = DateTime.UtcNow;
            schedule.LastUpdateDate = schedule.CreationDate;
            schedule.ProgramId = program.ProgramId;
            schedule.RecurringTypeId = 2;
            schedule.StartTime = TimeSpan.FromHours(20);
            schedule.EndTime = TimeSpan.FromHours(21);
            schedule.IsActive = true;

            program.Schedule = schedule;
            IProgramScheduleService programScheduleService = IoC.Resolve<IProgramScheduleService>("ProgramScheduleService");
            programScheduleService.InsertProgramSchedule(schedule);
            return program;
        }

        public SlotScreenTemplateCamera insertMockCamera(SlotScreenTemplateCamera camera)
        {
            ISlotScreenTemplateCameraService slotScreenTemplateCamera = IoC.Resolve<ISlotScreenTemplateCameraService>("SlotScreenTemplateCameraService");
            SlotScreenTemplateCamera submittedCamera = slotScreenTemplateCamera.InsertSlotScreenTemplateCamera(camera);
            return submittedCamera;
        }

        public SlotScreenTemplateMic insertMockMic(SlotScreenTemplateMic mic)
        {
            ISlotScreenTemplateMicService slotScreenTemplateMic = IoC.Resolve<ISlotScreenTemplateMicService>("SlotScreenTemplateMicService");
            SlotScreenTemplateMic submittedMic = slotScreenTemplateMic.InsertSlotScreenTemplateMic(mic);
            return submittedMic;
        }

        public DateTime GetTimeClosedToDayOfWeek(DayOfWeek dayOfWeek)
        {
            DateTime dt = DateTime.UtcNow;
            while (true)
            {
                if (dt.DayOfWeek == dayOfWeek)
                    return dt;
                dt = dt.AddDays(-1);
            }
        }

        internal NMS.Core.DataTransfer.News.PostInput ConvertNews(Core.DataTransfer.News.GetOutput getOutput)
        {
            var filter = Filters.Where(x => x.Name == getOutput.Source && getOutput.Filters.Any(y => y.FilterId == x.FilterId)).First();
            NMS.Core.DataTransfer.News.PostInput input = new Core.DataTransfer.News.PostInput();
            input.CategoryIds = getOutput.Categories.Select(x => x.CategoryId).ToList();
            input.Description = getOutput.Description;
            input.FilterTypeId = filter.FilterTypeId;
            input.LanguageCode = getOutput.LanguageCode;
            if (getOutput.Locations != null)
                input.LocationIds = getOutput.Locations.Select(x => x.LocationId).ToList();
            input.NewsDate = getOutput.PublishTime;
            input.NewsTypeId = getOutput.NewsTypeId;
            input.ReporterId = getOutput.ReporterId;
            input.ResourceEdit = getOutput.ResourceEdit;
            if (getOutput.Resources != null)
            {
                input.Resources = new List<Core.DataTransfer.Resource.PostInput>();
                input.Resources.CopyFrom(getOutput.Resources);
            }
            if (getOutput.Tags != null)
            {
                input.Tags = new List<Core.DataTransfer.Tag.PostInput>();
                input.Tags.CopyFrom(getOutput.Tags);
            }
            input.ThumbnailUrl = getOutput.ThumbnailUrl;
            input.Title = getOutput.Title;
            return input;
        }

        public bool ValidateNews(Core.DataTransfer.News.PostInput news, Core.DataTransfer.News.GetOutput getOutput)
        {
            bool flag = true;
            if (getOutput.ResourceEdit != null)
            {
                if (news.Resources == null)
                    news.Resources = new List<Core.DataTransfer.Resource.PostInput>() { new Core.DataTransfer.Resource.PostInput() };
                else news.Resources.Add(new Core.DataTransfer.Resource.PostInput());
            }
            if (news.Resources == null || getOutput.Resources == null)
            {
                if ((news.Resources == null || news.Resources.Count == 0) && (getOutput.Resources == null || getOutput.Resources.Count == 0))
                { }
                else flag = false;
            }
            else if (news.Resources.Count != getOutput.Resources.Count)
                flag = false;
            if (news.ReporterId > 0 && news.LocationIds.Count != getOutput.Locations.Count)
            {
                flag = false;
            }
            if (news.CategoryIds.Count != getOutput.Categories.Count)
            {
                flag = false;
            }
            if (news.ReporterId > 0)
            {
                List<Location> locations = new List<Location>();
                List<Category> categories = new List<Category>();

                categories = MockGenerator.Categories.Where(x => news.CategoryIds.Contains(x.CategoryId)).ToList();
                categories.AddRange(MockGenerator.Categories.Where(x => categories.Select(y => y.ParentId).Contains(x.CategoryId)).ToList());
                int count = 0;
                if (news.FilterTypeId == (int)NMS.Core.Enums.FilterTypes.Channel || news.FilterTypeId == (int)NMS.Core.Enums.FilterTypes.Radio || news.FilterTypeId == (int)NMS.Core.Enums.FilterTypes.NewsPaper)
                {
                    count += 2;
                    //count += MockGenerator.Filters.Where(x => x.FilterTypeId == news.FilterTypeId && x.Value.HasValue && news.ChannelId == x.Value.Value).Where(x => x.ParentId.HasValue).Count();
                }
                else
                {
                    if (news.NewsTypeId == (int)NewsTypes.Package)
                        count += 2;

                    locations = MockGenerator.Locations.Where(x => news.LocationIds.Contains(x.LocationId)).ToList();
                    locations.AddRange(MockGenerator.Locations.Where(x => locations.Select(y => y.ParentId).Contains(x.LocationId)).ToList());
                    count += MockGenerator.Filters.Where(x => x.FilterTypeId == news.FilterTypeId && x.Value.HasValue && locations.Select(y => y.LocationId).Contains(x.Value.Value)).Where(x => x.ParentId.HasValue).Count();
                }

                if (getOutput.Filters.Count != (locations.Count + categories.Count + 2 + count))
                    flag = false;
            }
            return flag;
        }

        public MS.Core.DataTransfer.DataTransfer<string> CropImage()
        {
            MS.Core.Models.PointInput point1 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point2 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point3 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point4 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point5 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point6 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point7 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point8 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point9 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point10 = new MS.Core.Models.PointInput();
            MS.Core.Models.PointInput point11 = new MS.Core.Models.PointInput();

            point1.X = 241;
            point1.Y = 104;
            point2.X = 240;
            point2.Y = 156;
            point3.X = 273;
            point3.Y = 193;
            point4.X = 287;
            point4.Y = 194;
            point5.X = 340;
            point5.Y = 150;
            point6.X = 346;
            point6.Y = 137;
            point7.X = 315;
            point7.Y = 102;
            point8.X = 291;
            point8.Y = 92;
            point9.X = 261;
            point9.Y = 99;
            point10.X = 256;
            point10.Y = 101;
            point11.X = 250;
            point11.Y = 103;

            List<MS.Core.Models.PointInput> points = new List<MS.Core.Models.PointInput>();
            points.Add(point1);
            points.Add(point2);
            points.Add(point3);
            points.Add(point4);
            points.Add(point5);
            points.Add(point6);
            points.Add(point7);
            points.Add(point8);
            points.Add(point9);
            points.Add(point10);
            points.Add(point11);

            MS.Core.Models.CropImageInput input = new MS.Core.Models.CropImageInput();
            input.Points = points;
            input.BucketId = 1;
            input.ApiKey = "H@);KDJ7Xd";
            input.ImageUrl = "http://localhost:21642/api/Resource/getresource/836cd06f-def3-463e-b508-0af2d9f137ce";

            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.PostRequest<MS.Core.DataTransfer.DataTransfer<string>>(MediaServerUrl + "/api/resource/CropImage", input, null);
        }

        public MS.Core.DataTransfer.DataTransfer<string> GenerateCustomSnapShot()
        {
            MS.Core.Models.CustomSnapshotInput input = new MS.Core.Models.CustomSnapshotInput();
            input.Id = "E2571E10-2565-4E79-8C31-A4671B82E894";
            input.FilePath = "\\\\10.3.18.21\\videos\\stream\\ARY News\\2015-02-17\\1_$2015-02-17 13-00-24.mp4";
            input.ResourceTypeId = 2;
            input.TimeSpan = 1169.396678;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            return helper.PostRequest<MS.Core.DataTransfer.DataTransfer<string>>(MediaServerUrl + "api/resource/GenerateCustomSnapshot", input, null);
        }

        public MS.Core.DataTransfer.Resource.PostOutput PostResourceAndMediaForProcessing()
        {

           // 35343d94-e119-48d4-a65b-b68c4d5802b4

            MS.Core.DataTransfer.Resource.PostInput input = new MS.Core.DataTransfer.Resource.PostInput();
            input.BucketId = 160;
            input.ApiKey = "R#o56!LVNb";
            input.FileName = "";
            input.ResourceTypeId = "1";
            input.FilePath = @"\\Google Media Bucket\2015\04\x2hkcyt_royal-blood-on-foo-fighters-headlining-glastonbury-2015_news2334475677009s89dsd98899e7wr";
            input.Source = "http://tune.pk/video/6001921/how-to-write-the-introduction-to-your-dissertation1234766787f809s985879459";
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<MS.Core.DataTransfer.Resource.PostOutput>(MediaServerUrl + "api/bucket/PostResource", input, null);

            //var file = new System.IO.FileStream("\\10.3.18.172\mms\Final\Force.com_Multitenancy_WP_101508.pdf", System.IO.FileMode.Open);
            WebClient wb = new WebClient();

            string filepath = @"F:\image.jpg";
            string downloadFileName = @"image.jpg";
            string ext = System.IO.Path.GetExtension(downloadFileName);
            wb.DownloadFile(filepath, downloadFileName);
            string resourceGuid = output.Guid.ToString();
            string requestUrl = MediaServerUrl + "api/resource/PostMediaForProcessing?id=" + resourceGuid;

            byte[] fileBytes = System.IO.File.ReadAllBytes(downloadFileName);
            string mimeType = MS.Core.Helper.ExtensionHelper.GetMimeType(ext);
            var output2 = helper.HttpUploadFile(requestUrl, downloadFileName, fileBytes, "File1", mimeType, new System.Collections.Specialized.NameValueCollection());

            return output;
        }

        public MS.Core.DataTransfer.Resource.PostOutput PostResourceAndMedia()
        {
            MS.Core.DataTransfer.Resource.PostInput input = new MS.Core.DataTransfer.Resource.PostInput();
            input.BucketId = 3;
            input.IsFromExternalSource = true;
            input.ApiKey = "H@);KDJ7Xd";
            input.FileName = "bg.jpg";
            input.Source = "abcd";
            //input.ThumbUrl = "http://s2.dmcdn.net/J4-vH.jpg";
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<MS.Core.DataTransfer.Resource.PostOutput>(MediaServerUrl + "api/bucket/PostResource", input, null);

            //var file = new System.IO.FileStream("\\10.3.18.172\mms\Final\Force.com_Multitenancy_WP_101508.pdf", System.IO.FileMode.Open);
            WebClient wb = new WebClient();

            //string filepath = @"C:\Users\hashamahmad\Desktop\bg.jpg";
            //string downloadFileName = "ftfile1.jpg";
            //string ext = System.IO.Path.GetExtension(downloadFileName);
            //wb.DownloadFile(filepath, downloadFileName);
            //string resourceGuid = output.Guid.ToString();
            //string requestUrl = MediaServerUrl + "api/resource/PostMedia?id=" + resourceGuid + "&IsHD=true";

            //byte[] fileBytes = System.IO.File.ReadAllBytes(downloadFileName);
            //string mimeType = MS.Core.Helper.ExtensionHelper.GetMimeType(ext);
            //var output2 = helper.HttpUploadFile(requestUrl, downloadFileName, fileBytes, "File1", mimeType, new System.Collections.Specialized.NameValueCollection());
           
            return output;
        }

        public MS.Core.DataTransfer.Resource.PostOutput PostResourceAndMediaFromMsApi()
        {
            MS.Core.DataTransfer.Resource.PostInput input = new MS.Core.DataTransfer.Resource.PostInput();
            input.BucketId = 1;
            input.ApiKey = "H@);KDJ7Xd";
            input.FileName = "54de0fd1282aa.jpg";
            input.FilePath = @"\Dawn News\2015\02\54de0fd1282aa.jpg";
            input.Source = "abcd";
            //HttpWebRequestHelper helper = new HttpWebRequestHelper();
            //var output = helper.PostRequest<MS.Core.DataTransfer.Resource.PostOutput>(MediaServerUrl + "api/bucket/PostResource", input, null);
            MS.MediaIntegration.API.MSApi msapi = new MS.MediaIntegration.API.MSApi();
            var output = msapi.PostResource(input);

            //var file = new System.IO.FileStream("\\10.3.18.172\mms\Final\Force.com_Multitenancy_WP_101508.pdf", System.IO.FileMode.Open);
            WebClient wb = new WebClient();

            string filepath = @"\\10.3.12.119\mms\NMS\Dawn News\2015\02\54de0fd1282aa.jpg";
            string downloadFileName = "ftfile.pdf";
            string ext = System.IO.Path.GetExtension(downloadFileName);
            wb.DownloadFile(filepath, downloadFileName);
            string resourceGuid = output.Guid.ToString();
            string requestUrl = MediaServerUrl + "api/resource/PostMedia?id=" + resourceGuid + "&IsHD=true";

            byte[] fileBytes = System.IO.File.ReadAllBytes(downloadFileName);
            string mimeType = MS.Core.Helper.ExtensionHelper.GetMimeType(ext);
            //var output2 = helper.HttpUploadFile(requestUrl, downloadFileName, fileBytes, "File1", mimeType, new System.Collections.Specialized.NameValueCollection());
            var output2 = msapi.PostMediaAndTranscode(resourceGuid, downloadFileName, fileBytes, "File1", mimeType, new System.Collections.Specialized.NameValueCollection());
            //var output2 = msapi.PostMedia(resourceGuid, true, downloadFileName, fileBytes, "File1", mimeType, new System.Collections.Specialized.NameValueCollection());

            return output;
        }


        public DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>> GetResourceInfoByGuidIds()
        {
            ResourceOutputGuids  OutputGuids = new ResourceOutputGuids();
            OutputGuids.Guids = new List<string>() { "5ECC2DF7-9D80-4996-AF0D-4E661EC129C6", "38A30CFA-2158-4C7F-83CA-1A71BE98D59D" };

            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>>>(MediaServerUrl + "api/resource/GetResourceInfoByGuidIds", OutputGuids, null);
            return output;
        }

        public MS.Core.DataTransfer.Resource.PostOutput PostResource()
        {
            MS.Core.DataTransfer.Resource.PostInput input = new MS.Core.DataTransfer.Resource.PostInput();
            input.BucketId = 165;
            input.ApiKey = "R#o56!LVNb";
            input.FileName = "14acc0db-aeab-4b0a-aad1-c9be4cafe8c11";
            input.FilePath = "/Google Media Bucket/2015/04/14acc0db-aeab-4b0a-aad1-c9be4cafe8c11";
            input.IsFromExternalSource = false;
            input.IsHd = false;
            input.ThumbUrl = "http://s2.dmcdn.net/KBwQp.jpg";
            input.Source = "http://www.dailymotion.com/video/x2n7ixb_mazahib-aur-aql-e-insani-kamran-ali-khan-episode-2-part-3_news";
            input.SystemType = 6;
            input.ResourceTypeId = "2";
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<MS.Core.DataTransfer.Resource.PostOutput>(MediaServerUrl + "api/bucket/PostResource", input, null);

            //var file = new System.IO.FileStream("\\10.3.18.172\mms\Final\Force.com_Multitenancy_WP_101508.pdf", System.IO.FileMode.Open);
            //WebClient wb = new WebClient();

            //string filepath = @"\\10.3.18.172\mms\Final\Force.com_Multitenancy_WP_101508.pdf";
            //string downloadFileName = "ftfile.pdf";
            //string ext = System.IO.Path.GetExtension(downloadFileName);
            //wb.DownloadFile(filepath, downloadFileName);
            //string resourceGuid = output.Guid.ToString();
            //string requestUrl = MediaServerUrl + "api/resource/PostMedia?id=" + resourceGuid + "&IsHD=true";

            //byte[] fileBytes = System.IO.File.ReadAllBytes(downloadFileName);
            //string mimeType = MS.Core.Helper.ExtensionHelper.GetMimeType(ext);
            //var output2 = helper.HttpUploadFile(requestUrl, downloadFileName, fileBytes, "File1", mimeType, new System.Collections.Specialized.NameValueCollection());

            //string thumbFilePath = @"\\10.3.12.119\mmsthumb\MediaArchieve\2015\02\0022382.jpg";
            //string thumbDownloadFileName = "thumb.jpg";
            //string thumbext = System.IO.Path.GetExtension(thumbDownloadFileName);
            //wb.DownloadFile(thumbFilePath, thumbDownloadFileName);
            //string thumbRequestUrl = MediaServerUrl + "api/resource/PostThumb?id=" + resourceGuid;

            //byte[] thumbFileBytes = System.IO.File.ReadAllBytes(thumbDownloadFileName);
            //string thumbMimeType = MS.Core.Helper.ExtensionHelper.GetMimeType(thumbext);
            //var output3 = helper.HttpUploadFile(thumbRequestUrl, thumbDownloadFileName, thumbFileBytes, "File2", thumbMimeType, new System.Collections.Specialized.NameValueCollection());

            return output;
        }

        public void ClipVideos()
        {
            MS.Core.Models.VideoClipInput input = new MS.Core.Models.VideoClipInput();
            MS.Core.Models.VideoEdits vEdit1 = new MS.Core.Models.VideoEdits();
            MS.Core.Models.VideoEdits vEdit2 = new MS.Core.Models.VideoEdits();
            List<MS.Core.Models.VideoEdits> vEdits = new List<MS.Core.Models.VideoEdits>();

            vEdit1.VideoGuid = "f74564fc-5eef-4216-bab4-06c91abee819";
            vEdit1.SequenceNumber = 1;
            vEdit1.From = 8.161;
            vEdit1.To = 17.156;

            vEdit2.VideoGuid = "b319357c-9a90-4aae-8bf1-d6ac74aa4061";
            vEdit2.SequenceNumber = 2;
            vEdit2.From = 8.161;
            vEdit2.To = 17.156;

            vEdits.Add(vEdit1);
            vEdits.Add(vEdit2);

            input.BucketId = 1;
            input.ApiKey = "H@);KDJ7Xd";
            input.Guid = "dc3c7c44-f3f2-487b-b4c9-3c64f4d0f4d1";
            input.VidEdits = vEdits;

            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<MS.Core.DataTransfer.Resource.PostOutput>(MediaServerUrl + "api/resource/ClipAndMergeVideos", input, null);
            //MS.MediaIntegration.API.MSApi api = new MS.MediaIntegration.API.MSApi();
            //var obj =  api.ClipAndMergeVideos(input);
        }
    }
}

