﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.ProcessThreads;
using NMS.UnitTests.Helpers;

namespace NMS.UnitTests
{
    [TestClass]
    public class PcrTest
    {
        [TestMethod]
        public void ValidatePCRMosGeneration()
        {
            var initialPath = ConfigurationManager.AppSettings["xmlpath"];
            int episodeId = 111;
            MosGeneration mosGenerator = new MosGeneration();
            mosGenerator.ProcessMosByEpisode(episodeId, "", "", "", "", "", false, true);
            Assert.IsTrue(XmlCompare.CompareXml(initialPath + "\\1.xml", initialPath + "\\4.xml", initialPath + "\\statistics.xml"));
        }
    }
}
