﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMS.Core.Helper;
using System.Linq;
using NMS.Core.Models;
using NMS.Core.Entities;
using System.Globalization;
using NMS.Core.DataTransfer.Program;
using NMS.Core.Enums;
using System.Threading;
using NMS.Core;

namespace NMS.UnitTests
{
    [TestClass]
    public class ProducerTests
    {
        MockGenerator mock = new MockGenerator();
        string WebServerUrl = ConfigurationManager.AppSettings["WebServerUrl"];

        [TestMethod]
        public void LoadInitialData()
        {
            AddChatMessage(1, 2);
            LoadInitialDataInput input = new LoadInitialDataInput();
            var output = mock.LoadInitialData(new List<int>() { 78 }, out input);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Channels.Count > 0 &&
                              output.Data.Programs.Count > 0 &&
                              output.Data.News.Count > 0 &&
                              output.Data.Filters.Count > 0 &&
                              output.Data.ScreenTemplates.Count > 0 &&
                              output.Data.ScreenElements.Count > 0 &&
                              output.Data.Messages.Count > 0
                              );
            else
                Assert.Fail(output.Errors[0]);

        }


        private void AddChatMessage(int UserIdFrom, int UserIdTo)
        {
            NMS.Core.IService.IMessageService MessageService = IoC.Resolve<NMS.Core.IService.IMessageService>("MessageService");

           
            Message entity = new Message { From = UserIdFrom, To = UserIdTo, Message = "Test Message goes here", CreationDate = DateTime.UtcNow, IsRecieved = true };
            entity.LastUpdateDate = entity.CreationDate;

            MessageService.InsertMessage(entity);

        }


        [TestMethod]
        public void GetProgramEpisodeByRunDownIDs()
        {   
            int id = 22;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>>(WebServerUrl + "api/program/GetProgramEpisodeByRundownId", id, null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Count > 0 && output.Data.First().Segments.Count > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void GetProgramEpisodeByRundownId()
        {
            DateTimeFormatInfo dateTimeFormat = new DateTimeFormatInfo();
            GetProgramEpisodeInput input = new GetProgramEpisodeInput();
            Program program = mock.GetMockProgram();
            input.Date = mock.GetTimeClosedToDayOfWeek((DayOfWeek)program.Schedule.WeekDayId);
            //input.Date = new DateTime(2014, 6, 27);
            input.ProgramId = 1087;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>>(WebServerUrl + "api/program/GetProgramEpisode", input, null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Count > 0 && output.Data.First().Segments.Count > 0);
            else
                Assert.Fail(output.Errors[0]);
        }


        [TestMethod]
        public void GetProgramFlow()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput>>(WebServerUrl + "api/program/GetProgramScreenFlow?programId=1087&Producerid=1", null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.ScreenTemplates.Count > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

       

        [TestMethod]
        public void GetBunch()
        {
            var news = mock.GetSavedNews(1, FilterTypes.FieldReporter);
            LoadInitialDataInput input = new LoadInitialDataInput();
            input.Filters = new List<NMS.Core.DataTransfer.Filter.GetOutput>() { new NMS.Core.DataTransfer.Filter.GetOutput() { FilterId = 78 } };
            input.From = DateTime.UtcNow.AddDays(-1);
            input.To = DateTime.UtcNow;
            input.PageCount = 10;
            input.StartIndex = 0;
            input.Term = string.Empty;
            input.BunchId = news.BunchGuid;
            input.FilterCountLastUpdateDate = DateTime.UtcNow.AddDays(-1);
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<List<Core.DataTransfer.News.GetOutput>>>(WebServerUrl + "api/news/getbunch", input, null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Count > 1);
            else
                Assert.Fail(output.Errors[0]);

        }

        [TestMethod]
        public void GetFilterCountTest()
        {
            var categoryId = MockGenerator.Categories.First().CategoryId;
            var parentFilterId = MockGenerator.Filters.Where(x => x.FilterTypeId == (int)FilterTypes.FieldReporter && !x.ParentId.HasValue).First().FilterId;
            var childFilterId = MockGenerator.Filters.Where(x => x.FilterTypeId == (int)FilterTypes.Category && x.Value == categoryId).First().FilterId;
            LoadInitialDataInput input = new LoadInitialDataInput();
            var output = mock.LoadInitialData(new List<int>() { parentFilterId, childFilterId }, out input);
            if (output.IsSuccess)
            {
                Thread.Sleep(TimeSpan.FromMinutes(1));
                var prevParentCount = (output.Data.FilterCounts != null && output.Data.FilterCounts.Where(x => x.FilterId == parentFilterId).Count() > 0) ? output.Data.FilterCounts.Where(x => x.FilterId == parentFilterId).First().Count : 0;
                var prevChildCount = (output.Data.FilterCounts != null && output.Data.FilterCounts.Where(x => x.FilterId == childFilterId).Count() > 0) ? output.Data.FilterCounts.Where(x => x.FilterId == childFilterId).First().Count : 0;
                var news = mock.GetSavedNews(1, FilterTypes.FieldReporter, false, false, false, categoryId);
                input.NewsLastUpdateDate = output.Data.News != null ? output.Data.News.Max(x => x.LastUpdateDate) : DateTime.UtcNow.AddHours(-1);
                input.StartIndex = output.Data.News != null ? output.Data.News.Count : 0;

                bool flag = false;

                for (int count = 0; ; count++)
                {
                    var pollingOutput = mock.ProducerPolling(input);
                    if (pollingOutput.IsSuccess)
                    {
                        if (flag || (pollingOutput.Data.News != null && pollingOutput.Data.News.Where(x => x.NewsId == news.NewsId).Count() > 0))
                        {
                            flag = true;
                            int newParentCount = pollingOutput.Data.FilterCounts.Where(x => x.FilterId == parentFilterId).First().Count;
                            int newChildCount =pollingOutput.Data.FilterCounts.Where(x => x.FilterId == childFilterId).First().Count;
                            if (pollingOutput.Data.FilterCounts != null && newParentCount > prevParentCount && newChildCount > prevChildCount)
                            {
                                Assert.IsTrue(true);
                                break;
                            }
                            //int count1 = pollingOutput.Data.FilterCounts.Sum(x => x.Count);
                            //int count2 = output.Data.FilterCounts.Sum(x => x.Count);
                            //var lst = pollingOutput.Data.FilterCounts.Where(x => output.Data.FilterCounts.Where(y => y.FilterId == x.FilterId).Count() > 0 && output.Data.FilterCounts.Where(y => y.FilterId == x.FilterId).First().Count != x.Count).ToList();
                            //if (pollingOutput.Data.FilterCounts != null && count1 > count2)
                            //{
                               
                            //}
                            else if (count > 3)
                            {
                                Assert.Fail("News didnt came");
                                break;
                            }
                            else Thread.Sleep(TimeSpan.FromSeconds(30));
                        }
                        else if (count > 3)
                        {
                            Assert.Fail("News didnt came");
                            break;
                        }
                        else Thread.Sleep(TimeSpan.FromSeconds(30));
                    }
                    else Assert.Fail(pollingOutput.Errors[0]);
                }
            }
            else Assert.Fail(output.Errors[0]);
        }
    }
}
