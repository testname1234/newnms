﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMS.Core.Helper;
using System.Linq;
using NMS.Core.Models;
using System.Net.Http;
using System.Net;
using NMS.Core.Enums;
using System.Threading;
using NMS.Core.Entities;
using System.Globalization;
using NMS.Core.DataTransfer.SlotScreenTemplate.POST;
using NMS.Core;

namespace NMS.UnitTests
{
    [TestClass]
    public class ReporterTests
    {
        MockGenerator mock = new MockGenerator();
        string WebServerUrl = ConfigurationManager.AppSettings["WebServerUrl"];
        string MediaServerUrl = ConfigurationManager.AppSettings["MediaServerUrl"];
        [TestMethod]
        public void AddNews()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.FieldReporter,true);
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
            {
                Assert.IsTrue(mock.ValidateNews(news, output.Data));
            }
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void AddNewsWithResources()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.FieldReporter, true, false);
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
                Assert.IsTrue(mock.ValidateNews(news, output.Data));
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void AddNewsWithTags()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.FieldReporter, false, true);
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
                Assert.IsTrue(mock.ValidateNews(news, output.Data));
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void AddNewsWithTagsAndResources()
        {

            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.FieldReporter, true, true);
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
                Assert.IsTrue(mock.ValidateNews(news, output.Data));
            else
                Assert.Fail(output.Errors[0]);

        }

        [TestMethod]
        public void AddNewsWithResourceEditsVideo()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            //var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.Channel, false, false, true);
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.Channel, false, false, true, NMS.Core.Enums.ResourceTypes.Video);
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
            {
                if (mock.ValidateNews(news, output.Data))
                {
                    bool flag = false;
                    int counter = 0;
                    while (counter < 60)
                    {
                        Thread.Sleep(1000);
                        HttpWebResponse response = helper.GetRequest(MediaServerUrl + "/getresource/" + output.Data.Resources.First().Guid, null);
                        if (response.ContentLength > 0)
                        {
                            flag = true;
                            break;
                        }
                        counter++;
                    }
                    if (!flag)
                        Assert.Fail("Resource:{0} could not be clipped", output.Data.Resources.First().Guid);
                    else Assert.IsTrue(true == true);
                }
                else
                    Assert.Fail("ValidateNews check failed");
            }
            else
                Assert.Fail(output.Errors[0]);

        }

        [TestMethod]
        public void AddNewsWithResourceEditsAudio()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            //var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.Channel, false, false, true);
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.Radio, false, false, true, NMS.Core.Enums.ResourceTypes.Audio);
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
            {
                if (mock.ValidateNews(news, output.Data))
                {
                    bool flag = false;
                    int counter = 0;
                    while (counter < 60)
                    {
                        Thread.Sleep(1000);
                        HttpWebResponse response = helper.GetRequest(MediaServerUrl + "/getresource/" + output.Data.Resources.First().Guid, null);
                        if (response.ContentLength > 0)
                        {
                            flag = true;
                            break;
                        }
                        counter++;
                    }
                    if (!flag)
                        Assert.Fail("Resource:{0} could not be clipped", output.Data.Resources.First().Guid);
                    else Assert.IsTrue(true == true);
                }
                else
                    Assert.Fail("ValidateNews check failed");
            }
            else
                Assert.Fail(output.Errors[0]);

        }

        [TestMethod]
        public void AddNewsWithResourceEditsImage()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            //var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.Channel, false, false, true);
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.NewsPaper, false, false, true, NMS.Core.Enums.ResourceTypes.Image);
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
            {
                if (mock.ValidateNews(news, output.Data))
                {
                    bool flag = false;
                    int counter = 0;
                    while (counter < 60)
                    {
                        Thread.Sleep(1000);
                        HttpWebResponse response = helper.GetRequest(MediaServerUrl + "/getresource/" + output.Data.Resources.First().Guid, null);
                        if (response.ContentLength > 0)
                        {
                            flag = true;
                            break;
                        }
                        counter++;
                    }
                    if (!flag)
                        Assert.Fail("Resource:{0} could not be clipped", output.Data.Resources.First().Guid);
                    else Assert.IsTrue(true == true);
                }
                else
                    Assert.Fail("ValidateNews check failed");
            }
            else
                Assert.Fail(output.Errors[0]);

        }

        [TestMethod]
        public void ChannelVideoMarkIsProcessed()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest(WebServerUrl + "api/resource/MarkIsProcessed?id=" + MockGenerator.ChannelVideos.First().ChannelVideoId + "&typeId=" + (int)ResourceTypes.Video, null);
            Assert.IsTrue(output.StatusCode == HttpStatusCode.OK);
        }

        [TestMethod]
        public void RadioMarkIsProcessed()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest(WebServerUrl + "api/resource/MarkIsProcessed?id=4&typeId=" + (int)ResourceTypes.Audio, null);
            Assert.IsTrue(output.StatusCode == HttpStatusCode.OK);
        }


        [TestMethod]
        public void GetAllMyNews()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.News.GetOutput>>>(WebServerUrl + string.Format("api/news/GetAllMyNews?rId={0}&pageCount={1}&startIndex={2}", 1, 10, 0), null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }


        [TestMethod]
        public void LoadChannelInitialData()
        {

            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<LoadChannelOutput>>(WebServerUrl + string.Format("api/Channel/LoadChannelInitialData?id={0}", MockGenerator.ChannelVideos.First().From.Value.ToString()), null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Channels.Count() > 0 && output.Data.Programs.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void GetAllCelebs()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>>>(WebServerUrl + "api/celebrity/GetCelebrityByTerm", null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void LoadRadioInitialData()
        {
            DateTime dt = MockGenerator.RadioStream.First().From;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<LoadRadioOutput>>(WebServerUrl + string.Format("api/Radio/LoadRadioInitialData?fromDate={0}&toDate={1}", dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"), dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ")), null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.RadioStations.Count() > 0 && output.Data.RadioStreams.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void RadioRefreshData()
        {
            DateTime dt = MockGenerator.RadioStream.First().From;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<LoadRadioOutput>>(WebServerUrl + string.Format("api/Radio/RefreshData?fromDate={0}&toDate={1}", dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"), dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ")), null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.RadioStreams.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void GetNewsByTerm()
        {
            var news = mock.GetMockReporterNews(1, NMS.Core.Enums.FilterTypes.FieldReporter);
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.News.GetOutput>>>(WebServerUrl + string.Format("api/News/GetNewsByTerm/{0}", news.Title.Substring(5, 15)), null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void NewsPaperLoadInitialData()
        {
            DateTime dt = MockGenerator.DailyNewsPaperPage.Last().Date;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<LoadNewsPaperOutput>>(WebServerUrl + string.Format("api/NewsPaper/LoadInitialData?fromDate={0}&toDate={1}", dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"), dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ")), null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.NewsPappers.Count() > 0 && output.Data.DailyNewsPapers.Count() > 0 && output.Data.NewsPaperPages.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void NewsPaperRefreshData()
        {
            DateTime dt = MockGenerator.DailyNewsPaperPage.First().Date;
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<LoadNewsPaperOutput>>(WebServerUrl + string.Format("api/NewsPaper/RefreshData?fromDate={0}&toDate={1}", dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"), dt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ")), null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.NewsPaperPages.Count() > 0);
            else
                Assert.Fail(output.Errors[0]);
        }


        [TestMethod]
        public void InsertUpdateSlots()
        {            
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            
            DateTimeFormatInfo dateTimeFormat = new DateTimeFormatInfo();
            GetProgramEpisodeInput input = new GetProgramEpisodeInput();
            
            input.Date = new DateTime(2014, 6, 27);
            input.ProgramId = 1087;            
            var outputepisode = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>>(WebServerUrl + "api/program/GetProgramEpisode", input, null);
            
            NMS.Core.DataTransfer.Slot.InputSlots inputslots = new Core.DataTransfer.Slot.InputSlots();
            List<NMS.Core.DataTransfer.Slot.PostInput> slots = new List<Core.DataTransfer.Slot.PostInput>();
            NMS.Core.DataTransfer.Slot.PostInput slot1 = new Core.DataTransfer.Slot.PostInput();
            var news1 = mock.GetSavedNews(1, FilterTypes.FieldReporter);
            var news2 = mock.GetSavedNews(1, FilterTypes.FieldReporter);
            

            slot1.CategoryId = news1.Categories[0].CategoryId.ToString();
            slot1.NewsGuid = news1._id;
            slot1.SequnceNumber = "1";
            slot1.SegmentId = outputepisode.Data[0].Segments[0].SegmentId.ToString();
            slot1.SlotTypeId = "1";

            NMS.Core.DataTransfer.Slot.PostInput slot2 = new Core.DataTransfer.Slot.PostInput();
            slot2.CategoryId = news2.Categories[0].CategoryId.ToString();
            slot2.NewsGuid = news2._id;
            slot2.SequnceNumber = "2";
            slot2.SegmentId = outputepisode.Data[0].Segments[0].SegmentId.ToString();
            slot2.SlotTypeId = "1";


            slots.Add(slot1);
            slots.Add(slot2);

            List<NMS.Core.DataTransfer.Slot.PostInput> lstSlots = new List<Core.DataTransfer.Slot.PostInput>();
            //lstSlots.CopyFrom(mock.MockGetSlots().Take(4));
           // lstSlots.CopyFrom(mock.MockGetSlots());
            slots.AddRange(lstSlots);

            inputslots.Slots = slots;

            var output = helper.PostRequest < NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>>>(WebServerUrl + "api/program/InsertUpdateSlots", inputslots, null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data[0].NewsGuid == news1._id && output.Data[0].SequnceNumber == 1 && output.Data[1].NewsGuid == news2._id && output.Data[1].SequnceNumber == 2);
            else
                Assert.Fail(output.Errors[0]);
        }


        [TestMethod]
        public void SaveSlotScreenTemplateFlow()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            SlotScreenTemplateFlow slotScreenTemplateFlow = new SlotScreenTemplateFlow();
            List<SlotScreenTemplate> SlotScreenTemplates = new List<SlotScreenTemplate>();
            slotScreenTemplateFlow.SlotScreenTemplates = new List<Core.DataTransfer.SlotScreenTemplate.PostInput>();
            //SlotScreenTemplate slotScreenTemplate1 = new SlotScreenTemplate();
            //SlotScreenTemplate slotScreenTemplate2 = new SlotScreenTemplate();
            //SlotScreenTemplate slotScreenTemplate3 = new SlotScreenTemplate();
            SlotScreenTemplates.CopyFrom(mock.MockGetScreenTemplatesByProgram().Take(3));
            SlotScreenTemplates[0].SlotId = 35;
            SlotScreenTemplates[0].SequenceNumber = 1;
            SlotScreenTemplates[1].SlotId = 36;
            SlotScreenTemplates[1].SequenceNumber = 2;
            SlotScreenTemplates[2].SlotId = 37;
            SlotScreenTemplates[2].SequenceNumber = 3;

            slotScreenTemplateFlow.SlotScreenTemplates.CopyFrom(SlotScreenTemplates);


            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>>>(WebServerUrl + "api/program/SaveSlotScreenTemplateFlow", slotScreenTemplateFlow, null);
        }

        [TestMethod]
        public void DeleteSlot()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();            
            //var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<bool>>(WebServerUrl + "api/program/DeleteSlot", id.ToString(),null);
            var output = helper.GetRequest <NMS.Core.DataTransfer.DataTransfer<bool>>(WebServerUrl + "api/program/DeleteSlot?Id=43", null);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data == true);
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void SaveSlotScreenElements()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            SlotScreenTemplateFlow slotScreenTemplateFlow = new SlotScreenTemplateFlow();
            List<SlotScreenTemplate> SlotScreenTemplates = new List<SlotScreenTemplate>();
            slotScreenTemplateFlow.SlotScreenTemplates = new List<Core.DataTransfer.SlotScreenTemplate.PostInput>();
            //SlotScreenTemplate slotScreenTemplate1 = new SlotScreenTemplate();
            //SlotScreenTemplate slotScreenTemplate2 = new SlotScreenTemplate();
            //SlotScreenTemplate slotScreenTemplate3 = new SlotScreenTemplate();
            SlotScreenTemplates.CopyFrom(mock.MockGetScreenTemplatesByProgram().Take(3));
            SlotScreenTemplates[0].SlotId = 35;
            SlotScreenTemplates[0].SequenceNumber = 1;
            SlotScreenTemplates[1].SlotId = 36;
            SlotScreenTemplates[1].SequenceNumber = 2;
            SlotScreenTemplates[2].SlotId = 37;
            SlotScreenTemplates[2].SequenceNumber = 3;

            slotScreenTemplateFlow.SlotScreenTemplates.CopyFrom(SlotScreenTemplates);


            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>>>(WebServerUrl + "api/program/SaveSlotScreenTemplateFlow", slotScreenTemplateFlow, null);
        }

        [TestMethod]
        public void AddChannelNewsNoChange()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var getEntity = mock.GetSavedNews(1, FilterTypes.FieldReporter);
            getEntity = mock.GetNewsByTerm(getEntity.Title.Substring(5, 15)).First();
            var news = mock.ConvertNews(getEntity);
            news.FilterTypeId = (int)FilterTypes.Channel;
            news.ReferenceNewsId = getEntity._id;
            var channelVideo = MockGenerator.ChannelVideos.FirstOrDefault();
            if (channelVideo != null)
            {
                news.ChannelId = channelVideo.ChannelId.Value;
                news.ResourceEdit = new Core.Models.ResourceEdit();
                news.ResourceEdit.Id = channelVideo.ChannelVideoId;
                news.ResourceEdit.ResourceTypeId = (int)ResourceTypes.Video;
                news.ResourceEdit.FileName = string.Format("Geo TV-{0}.mp4", Faker.NumberFaker.Number());

                news.ResourceEdit.FromTos = new List<Core.Models.FromTo>();
                news.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(5).TotalSeconds, To = TimeSpan.FromSeconds(10).TotalSeconds });
                news.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(13).TotalSeconds, To = TimeSpan.FromSeconds(28).TotalSeconds });
            }
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (!output.IsSuccess)
            {
                Assert.IsTrue(output.Errors[0] == "Duplicate Insertion attempted");
            }
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void AddChannelNewsChange()
        {
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            var getEntity = mock.GetSavedNews(1, FilterTypes.FieldReporter);
            getEntity = mock.GetNewsByTerm(getEntity.Title.Substring(5, 15)).First();
            var news = mock.ConvertNews(getEntity);
            news.FilterTypeId = (int)FilterTypes.Channel;
            news.Title = news.Title + Faker.NumberFaker.Number();
            news.ReferenceNewsId = getEntity._id;
            var channelVideo = MockGenerator.ChannelVideos.FirstOrDefault();
            if (channelVideo != null)
            {
                news.ChannelId = channelVideo.ChannelId.Value;
                news.ResourceEdit = new Core.Models.ResourceEdit();
                news.ResourceEdit.Id = channelVideo.ChannelVideoId;
                news.ResourceEdit.ResourceTypeId = (int)ResourceTypes.Video;
                news.ResourceEdit.FileName = string.Format("Geo TV-{0}.mp4", Faker.NumberFaker.Number());

                news.ResourceEdit.FromTos = new List<Core.Models.FromTo>();
                news.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(5).TotalSeconds, To = TimeSpan.FromSeconds(10).TotalSeconds });
                news.ResourceEdit.FromTos.Add(new Core.Models.FromTo() { From = TimeSpan.FromSeconds(13).TotalSeconds, To = TimeSpan.FromSeconds(28).TotalSeconds });
            }
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
            {
                if (mock.ValidateNews(news, output.Data))
                {
                    bool flag = false;
                    int counter = 0;
                    while (counter < 60)
                    {
                        Thread.Sleep(1000);
                        HttpWebResponse response = helper.GetRequest(MediaServerUrl + "/getresource/" + output.Data.Resources.Last().Guid, null);
                        if (response.ContentLength > 0)
                        {
                            flag = true;
                            break;
                        }
                        counter++;
                    }
                    if (!flag)
                        Assert.Fail("Resource:{0} could not be clipped", output.Data.Resources.First().Guid);
                    else Assert.IsTrue(true == true);
                }
                else
                    Assert.Fail("ValidateNews check failed");
            }
            else
                Assert.Fail(output.Errors[0]);
        }
    }
}
