﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using System.Net;
using System.IO;

using System.Threading.Tasks;

namespace NMS.UnitTests.UI_test
{
    public static class Browser
    {
        private static IWebDriver _WebBrowserchrome;
        private static IWebDriver _WebBrowserfirefox;
        private static IWebDriver _WebBrowserIE;
        private static IWebDriver _WebBrowser;
        private static int CurrentBrowser = 0;

        public static bool isLogin = false;

        private static IWebDriver GetRandomWebbrowser()
        {
            IWebDriver iwebdrv = null;
            Random ran = new Random();
            int number = 1;// ran.Next(3);
            switch (number)
            {
                case 4:

                    if (_WebBrowserIE == null)
                    {
                        _WebBrowserIE = new InternetExplorerDriver();

                    }
                    CurrentBrowser = 0;
                    iwebdrv = _WebBrowserIE;
                    break;
                case 1:
                    if (_WebBrowserchrome == null)
                    {
                        _WebBrowserchrome = new ChromeDriver();
                    

                    }
                    iwebdrv = _WebBrowserchrome;
                    CurrentBrowser = 1;
                    break;
                case 2:
                    if (_WebBrowserfirefox == null)
                    {
                        _WebBrowserfirefox = new FirefoxDriver();
                     

                    }
                    iwebdrv = _WebBrowserfirefox;
                    CurrentBrowser = 2;
                    break;
                default:
                    if (_WebBrowserchrome == null)
                    {
                        _WebBrowserchrome = new ChromeDriver();
                   

                    }

                    iwebdrv = _WebBrowserIE;
                    CurrentBrowser = 0;
                    break;
            }
            return iwebdrv;
        }

        private static void SetNewRandomWebbrowser()
        {
            _WebBrowser = GetRandomWebbrowser();
        }

        public static IWebDriver GetDriver()
        {
            SetNewRandomWebbrowser();
            return _WebBrowser;
        }

        public static string GetCurrentBrowserUrl()
        {
            string Url = string.Empty;

            switch (CurrentBrowser)
            {
                case 0:
                    Url = _WebBrowserIE.Url;
                    break;
                case 1:
                    Url = _WebBrowserchrome.Url;
                    break;

                case 2:
                    Url = _WebBrowserfirefox.Url;
                    break;

                default:
                    Url = _WebBrowserchrome.Url;
                    break;
            }

            return Url;

        }

    }
    }

