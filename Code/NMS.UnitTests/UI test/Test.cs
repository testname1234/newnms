﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;

using NMS;



namespace NMS.UnitTests.UI_test
{
    [TestClass]
    public class Test
    {
        MockGenerator t = new MockGenerator();
        
      
        
        [TestMethod]
        public void freportersubmitnews_test()
        {
            TestHelper tst = new TestHelper();
            Login(Browser.GetDriver(), ConfigurationManager.AppSettings["WebServerUrl"] + "reporter#/home", "freporter");
            Submit_freporter_news(Browser.GetDriver());
        }

        [TestMethod]
        public void login_test()
        {
            TestHelper tst = new TestHelper();
           
            foreach (string key in tst.userdictionary.Keys)
            {
                using (IWebDriver driver = new ChromeDriver())
                {
                    Login(driver, tst.userdictionary[key], key);
                }
            }
                
        }

        public void Login(IWebDriver driver, string url, string Id)
        {

            var LoginId = Id;
            var Password = ConfigurationManager.AppSettings["Password"];

            driver = Browser.GetDriver();
            driver.Url = ConfigurationManager.AppSettings["WebServerUrl"];

            IWebElement iwe = driver.FindElement(By.XPath("//input[@id='txtUserName']"));
            if (iwe != null)
            {
                iwe.SendKeys(LoginId);
                iwe = driver.FindElement(By.XPath("//input[@id='txtPassword']"));
                if (iwe != null)
                {
                    iwe.SendKeys(Password);
                    iwe = driver.FindElement(By.LinkText("Login"));
                    if (iwe != null)
                    {
                        iwe.Click();

                        System.Threading.Thread.Sleep(20000);

                        Assert.AreEqual(url,driver.Url);
                    }

                }

            }

        }

        public void Submit_freporter_news(IWebDriver driver)
        {
            driver = Browser.GetDriver();
            driver.Url = ConfigurationManager.AppSettings["WebServerUrl"] + "reporter#/home";
            System.Threading.Thread.Sleep(2000);
            IWebElement iwe = driver.FindElement(By.LinkText("Report News"));
            if (iwe != null)
            {
                iwe.Click();
                iwe = driver.FindElement(By.XPath("//div[@class='editor']//input[@type='text']"));
                if (iwe != null)
                {
                    iwe.SendKeys(Faker.StringFaker.AlphaNumeric(100));
                    iwe = driver.FindElement(By.XPath("//div[@class='jqte_editor']"));
                    
                    if (iwe != null)
                    {
                        iwe.Click();
                        IJavaScriptExecutor jsExecuter = (IJavaScriptExecutor)driver;
                        string script = "$($('textArea')[0]).val('This is test Description');$($('textArea')[0]).jqteVal('This is test Description');";
                        jsExecuter.ExecuteScript(script, new object[] { });
                        //iwe.SendKeys("This is test Description");
                        iwe = driver.FindElement(By.XPath("//input[contains(@class, 'ui-autocomplete-input') and contains(@class, 'notAdd')]"));
                       
                        if (iwe != null)
                        {

                            iwe.Click();
                            iwe.SendKeys("Politics");
                            System.Threading.Thread.Sleep(2000);
                            iwe = driver.FindElement(By.XPath("//li[@class='ui-menu-item']//a[@tabindex='-1']"));
                            if (iwe != null)
                            {
                                iwe.Click();
                                iwe = driver.FindElement(By.XPath("//input[contains(@class, 'ui-autocomplete-input') and contains(@class, 'notAdd') and contains(@tabindex, '3')]"));
                                if (iwe != null)
                                {     
                                    iwe.SendKeys("Karachi");
                                    System.Threading.Thread.Sleep(2000);
                                    iwe = driver.FindElement(By.XPath("//ul[contains(@class, 'ui-autocomplete') and contains(@class, 'ui-front') and contains(@class, 'ui-menu') and contains(@class, 'ui-widget') and contains(@class, 'ui-widget-content') and contains(@class, 'ui-corner-all')]//li[@class='ui-menu-item']//a[@tabindex='-1']"));
                                    if (iwe != null)
                                    {
                                        iwe.Click();
                                        iwe = driver.FindElement(By.XPath("//div[contains(@class, 'options') and contains(@class, 'dateTime')]"));
                                         
                                        
                                        if (iwe != null)
                                        {
                                            iwe.Click();
                                            iwe = driver.FindElement(By.XPath("//table[@class='ui-datepicker-calendar']"));
                                            
                                            System.Threading.Thread.Sleep(1500);
                                            iwe = driver.FindElement(By.LinkText("1"));
                                            if (iwe != null)
                                            {
                                                iwe.Click();
                                                iwe = driver.FindElement(By.XPath("//div[contains(@class, 'options') and contains(@class, 'dateTime')]"));
                                                
                                                //p[@class='news-title']
                                                if (iwe != null)
                                                {
                                                    iwe.Click();
                                                    iwe = driver.FindElement(By.XPath("//input[contains(@class, 'ui-autocomplete-input') and contains(@tabindex, '4')]"));

                                                   
                                                    
                                                    if (iwe != null)
                                                    {
                                                        iwe.SendKeys("test tag 1");
                                                        iwe.SendKeys(OpenQA.Selenium.Keys.Enter);
                                                        iwe.SendKeys("test tag 2");
                                                        iwe.SendKeys(OpenQA.Selenium.Keys.Enter);
                                                        iwe = driver.FindElement(By.XPath("//input[contains(@class, 'ReportNews') and contains(@type, 'button')]"));
                                                        if (iwe != null)
                                                        {
                                                            iwe.Click();
                                                        }
                                                    }
                                                }
                                                
                                            }
                                           
                                          
                                        }
                                            
                                         
                                        }
                                       
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
       
    }

