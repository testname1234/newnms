﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UnitTests.UI_test
{
   public class TestHelper
    {
       public  Dictionary<string, string> userdictionary = new Dictionary<string, string>();

       string webServerUrl = ConfigurationManager.AppSettings["WebServerUrl"];

       public TestHelper()
       {
           userdictionary.Add("producer",webServerUrl+ "producer#/home");
           userdictionary.Add("freporter", webServerUrl+"reporter#/home");
           //userdictionary.Add("preporter", "public-reporter#/home");

       }
       
    }
}
