﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMS.Core.Helper;
using NMS.Core.Models;
using System.Linq;
using NMS.Core.Enums;
using System.Net.Http;
using System.Net;

namespace NMS.UnitTests
{
    [TestClass]
    public class VerifierTest
    {
        MockGenerator mock = new MockGenerator();
        string WebServerUrl = ConfigurationManager.AppSettings["WebServerUrl"];

        [TestMethod]
        public void LoadVerifierInitialData()
        {
            LoadInitialDataInput input = new LoadInitialDataInput();
            var output = mock.LoadInitialData(new List<int>() { (int)NewsFilters.NotVerified },out input);
            if (output.IsSuccess)
                Assert.IsTrue(output.Data.News.Count > 0 && output.Data.News.Where(x => !x.Filters.Any(y => y.FilterId == (int)NewsFilters.NotVerified)).Count() == 0 && output.Data.Filters.Count > 0);
            else
                Assert.Fail(output.Errors[0]);
        }

        
        [TestMethod]
        public void UpdateNewsWithNoChangeMarkVerify()
        {
            LoadInitialDataInput input = new LoadInitialDataInput();
            var initialData = mock.LoadInitialData(new List<int>() { (int)NewsFilters.NotVerified },out input);
            HttpWebRequestHelper helper = new HttpWebRequestHelper();
            
            var getEntity = initialData.Data.News.First();
            getEntity = mock.GetNewsWithUpdatesAndRelated(getEntity._id, getEntity.BunchGuid).Where(x=>x._id==getEntity._id).First();
            var news = mock.ConvertNews(getEntity);
            news.MarkNewsVerified = new Core.DataTransfer.MarkVerifyNewsInput();
            news.MarkNewsVerified.NewsIds = new List<string>() { getEntity._id };
            news.MarkNewsVerified.IsVerified = true;
            news.MarkNewsVerified.bunchId = getEntity.BunchGuid;
            news.LinkedNewsId = getEntity._id;
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (!output.IsSuccess)
            {
                initialData = mock.LoadInitialData(new List<int>() { (int)NewsFilters.NotVerified },out input);
                Assert.IsTrue(!initialData.Data.News.Any(x=>x._id==getEntity._id) && output.Errors[0] == "Duplicate Insertion attempted");
            }
            else
                Assert.Fail("News Updated");
        }

        [TestMethod]
        public void UpdateNewsWithTitleChangeMarkVerify()
        {
            LoadInitialDataInput input = new LoadInitialDataInput();
            var initialData = mock.LoadInitialData(new List<int>() { (int)NewsFilters.NotVerified },out input);
            HttpWebRequestHelper helper = new HttpWebRequestHelper();

            var getEntity = initialData.Data.News.First();
            getEntity = mock.GetNewsWithUpdatesAndRelated(getEntity._id, getEntity.BunchGuid).Where(x => x._id == getEntity._id).First();
            var news = mock.ConvertNews(getEntity);
            news.Title = news.Title + Faker.NumberFaker.Number();
            news.MarkNewsVerified = new Core.DataTransfer.MarkVerifyNewsInput();
            news.MarkNewsVerified.NewsIds = new List<string>() { getEntity._id };
            news.MarkNewsVerified.IsVerified = true;
            news.MarkNewsVerified.bunchId = getEntity.BunchGuid;
            news.LinkedNewsId = getEntity._id;
            var output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.News.GetOutput>>(WebServerUrl + "api/news/Add", news, null);
            if (output.IsSuccess)
            {
                initialData = mock.LoadInitialData(new List<int>() { (int)NewsFilters.NotVerified },out input);
                Assert.IsTrue(!initialData.Data.News.Any(x => x._id == getEntity._id || x._id == output.Data._id));
            }
            else
                Assert.Fail(output.Errors[0]);
        }

        [TestMethod]
        public void MarkVerify()
        {
            LoadInitialDataInput input = new LoadInitialDataInput();
            var initialData = mock.LoadInitialData(new List<int>() { (int)NewsFilters.NotVerified },out input);
            HttpWebRequestHelper helper = new HttpWebRequestHelper();

            var getEntity = initialData.Data.News.First();
            NMS.Core.DataTransfer.MarkVerifyNewsInput MarkNewsVerified = new Core.DataTransfer.MarkVerifyNewsInput();
            MarkNewsVerified.NewsIds = new List<string>() { getEntity._id };
            MarkNewsVerified.IsVerified = true;
            MarkNewsVerified.bunchId = getEntity.BunchGuid;

            var output = helper.PostRequest(WebServerUrl + "api/news/MarkVerifyNews", MarkNewsVerified, null);
            Assert.IsTrue(output.StatusCode == HttpStatusCode.OK);
        }
    }
}
