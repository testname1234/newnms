﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.Helper;

namespace NMS.UserConsole.Repository.Media
{
    public class ImageRepository : IImageRepository
    {
        public byte[] GetImage(string imageId)
        {
            string filePath = "Media/Images/" + imageId + ".jpg";
            if (File.Exists(filePath))
            {
                return FileHelper.GetImageFromCache(filePath);
            }
            else
            {
                WebClient client = new WebClient();
                byte[] bytes = client.DownloadData("http://10.1.20.27/" + imageId + ".jpg");
                ThreadPool.QueueUserWorkItem(SaveFile, new SaveFileArg() { filePath = filePath, bytes = bytes });
                return bytes;
            }
        }

        public void SaveFile(Object threadContext)
        {
            var arg = (threadContext as SaveFileArg);
            FileHelper.SaveImageFromCache(arg.filePath, arg.bytes);
        }

        public class SaveFileArg
        {
            public string filePath { get; set; }
            public byte[] bytes { get; set; }
        }
    }
}
