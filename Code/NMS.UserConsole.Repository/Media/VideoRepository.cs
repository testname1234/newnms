﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.Helper;

namespace NMS.UserConsole.Repository.Media
{
    public class VideoRepository : IVideoRepository
    {
        public byte[] GetVideo(string videoId)
        {
            string filePath = "Media/Videos/" + videoId + ".mp4";
            if (File.Exists(filePath))
            {
                return VideoHelper.GetVideoFromCache(filePath);
            }
            else
            {
                WebClient client = new WebClient();
                byte[] bytes = client.DownloadData("http://10.1.20.27/" + videoId + ".mp4");
                ThreadPool.QueueUserWorkItem(SaveFile, new SaveFileArg() { filePath = filePath, bytes = bytes });
                return bytes;
            }
        }

        public void SaveFile(Object threadContext)
        {
            var arg = (threadContext as SaveFileArg);
            VideoHelper.SaveVideoFromCache(arg.filePath, arg.bytes);
        }

        public class SaveFileArg
        {
            public string filePath { get; set; }
            public byte[] bytes { get; set; }
        }
    }
}
