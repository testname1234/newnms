﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.View.Argument
{
    public class FileUploadArgument
    {
        public int BucketID { get; set; }
        public string ApiKey { get; set; }
        public string Name { get; set; }
    }
}
