﻿using Microsoft.Win32;
using NMS.UserConsole.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NMS.UserConsole.View.Controls
{
    /// <summary>
    /// Interaction logic for FileUpload.xaml
    /// </summary>
    public partial class FileUpload : UserControl
    {
        FileUploadViewModel vm = new FileUploadViewModel();

        public FileUpload()
        {
            InitializeComponent();
            vm.OnUploadCompleted += vm_OnUploadCompleted;
            this.DataContext = vm;
        }

        void vm_OnUploadCompleted(object sender, EventArgs e)
        {
            MessageBox.Show("File Upload Complete");
            btnStartUpload.IsEnabled = true;
            btnClear.IsEnabled = true;
            btnSelectFiles.IsEnabled = true;
        }

        private void btnSelectFiles_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                foreach (var file in dialog.FileNames)
                {
                    vm.AddFileItem(file);
                }
            }
        }

        private void btnStartUpload_Click_1(object sender, RoutedEventArgs e)
        {
            btnStartUpload.IsEnabled = false;
            btnClear.IsEnabled = false;
            btnSelectFiles.IsEnabled = false;
            vm.StartUpload();
        }

        private void btnClear_Click_1(object sender, RoutedEventArgs e)
        {
            vm.Items.Clear();
            vm.TotalProgress = 0;
        }
    }
}
