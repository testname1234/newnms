﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NMS.UserConsole.ViewModel;

namespace NMS.UserConsole.View.Controls
{
    /// <summary>
    /// Interaction logic for MarkerUC.xaml
    /// </summary>
    public partial class MarkerUC : UserControl
    {
        public Marker marker { get; set; }
        public bool IsX1 { get; set; }
        public MarkerUC(Marker _marker)
        {
            marker = _marker;
            IsX1 = false;
            InitializeComponent();
        }

        public void Rotate()
        {
            IsX1 = true;
            sclTransform.ScaleX = -1;
        }
    }
}
