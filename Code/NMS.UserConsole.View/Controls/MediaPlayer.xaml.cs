﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using FFMPEGLib;
using NMS.Core;
using NMS.Core.Helper;
using NMS.UserConsole.ViewModel;

namespace NMS.UserConsole.View.Controls
{
    /// <summary>
    /// Interaction logic for MediaPlayer.xaml
    /// </summary>
    public partial class MediaPlayer : UserControl
    {
        private DispatcherTimer timerVideoTime;
        private string VideoUrl;
        public int isPlay = 0;
        public int count = 0;
        public Uri tempUrl;
        private TimeSpan Duration;
        public MediaPlayer()
        {
            InitializeComponent();
            this.media.MediaFailed += media_MediaFailed;
            this.media.MediaOpened += media_MediaOpened;
            this.media.MediaEnded += media_MediaEnded;
            timerVideoTime = new DispatcherTimer();
            timeSlider.AddHandler(MouseLeftButtonUpEvent,
                      new MouseButtonEventHandler(timeSlider_MouseLeftButtonUp),
                      true);
        }

        public void OpenMedia(Marker marker, Video sourceVideo)
        {
            timerVideoTime.Stop();
            timeSlider.Value = 0;
            TimeSpan from = TimeSpan.FromMilliseconds(marker.XPercent * sourceVideo.Duration.TotalMilliseconds);
            TimeSpan to = TimeSpan.FromMilliseconds(marker.X1Percent * sourceVideo.Duration.TotalMilliseconds);
            Duration = to - from;
            VideoUrl = Directory.GetCurrentDirectory() + "/" + AppSettings.ClipsFolderPath + "/" + marker.VideoId + "-" + marker.XPercent + "-" + marker.X1Percent + "." + AppSettings.VideoFormat;
            if (!File.Exists(VideoUrl))
                FFMPEG.ClipVideo(sourceVideo.VideoUrl, from, to, VideoUrl);
            media.Source = new Uri(VideoUrl);
        }

        public void OpenMedia(Video sourceVideo)
        {
            media.Source = null;
            timerVideoTime.Stop();
            timeSlider.Value = 0;
            //if (sourceVideo.Duration.TotalMilliseconds == 0)
            //    Duration = TimeSpan.FromSeconds(FFMPEG.GetDuration(sourceVideo.VideoUrl));
            //else Duration = sourceVideo.Duration;
            media.Source = new Uri(sourceVideo.VideoUrl);
            tempUrl=new Uri(sourceVideo.VideoUrl);
        }
        private void PauseMedia(object sender, RoutedEventArgs e)
        {
            btnPlay.Visibility = Visibility.Visible;
            btnStop.Visibility = Visibility.Hidden;
            imgBorderpause.BorderBrush = Brushes.Green;
            imgBorderplay.BorderBrush = Brushes.Transparent;
            media.Pause();
        }
        private void PlayMedia(object sender, RoutedEventArgs e)
        {
            btnStop.Visibility = Visibility.Visible;
            btnPlay.Visibility = Visibility.Hidden;
            imgBorderplay.BorderBrush = Brushes.Green;
            imgBorderpause.BorderBrush = Brushes.Transparent;
            media.Play();
              
        }

        //private void ChangeMediaVolume(object sender, RoutedPropertyChangedEventArgs<double> args)
        //{
        //    media.Volume = (double)volumeSlider.Value;
        //}


        public void CloseMedia()
        {
            timerVideoTime.Stop();
            media.Stop();
            timeSlider.Value = 0;
            media.Source = null;
            btnPlay.Visibility = Visibility.Visible;
            btnStop.Visibility = Visibility.Visible;
           
        }

        void media_MediaEnded(object sender, RoutedEventArgs e)
        {
            timerVideoTime.Stop();
            timeSlider.Value = 0;
        }

        void media_MediaOpened(object sender, RoutedEventArgs e)
        {
            Duration = media.NaturalDuration.TimeSpan;
            timerVideoTime = new DispatcherTimer();
            timerVideoTime.Interval = TimeSpan.FromSeconds(1);
            timerVideoTime.Tick += new EventHandler(timer_Tick);
            timerVideoTime.Start();
        }

        private void timeSlider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Duration.TotalSeconds > 0)
            {
                media.Position = TimeSpan.FromSeconds((timeSlider.Value / 100) * Duration.TotalSeconds);
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (Duration.TotalSeconds > 0)
            {
                timeSlider.Value = (media.Position.TotalSeconds / Duration.TotalSeconds) * 100;
                lblEllapsedTime.Content = FormatHelper.FormatTimeSpan(media.Position) + " / " + FormatHelper.FormatTimeSpan(Duration);
            }
        }

        void media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            MessageBox.Show(e.ErrorException.Message);
        }
    }
}
