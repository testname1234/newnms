﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using FFMPEGLib;
using NMS.Core;
using NMS.Core.Helper;
using NMS.UserConsole.ViewModel;

namespace NMS.UserConsole.View.Controls
{
    /// <summary>
    /// Interaction logic for MediaPlayer.xaml
    /// </summary>
    public partial class MultiMediaPlayer : UserControl
    {
        private DispatcherTimer timerVideoTime;
        private string VideoUrl;
        private TimeSpan Duration;
        List<TimeSpan> VideoDurations;
        MediaElement currentMediaElement;
        public MultiMediaPlayer()
        {
            InitializeComponent();
            VideoDurations = new List<TimeSpan>();
            timerVideoTime = new DispatcherTimer();
            timeSlider.AddHandler(MouseLeftButtonUpEvent,
                      new MouseButtonEventHandler(timeSlider_MouseLeftButtonUp),
                      true);
        }

        public void OpenMedia(List<Video> sourceVideos)
        {
            VideoDurations = new List<TimeSpan>();
            gdMedia.Children.Clear();
            if (sourceVideos.Count > 0)
            {
                timerVideoTime.Stop();
                timeSlider.Value = 0;
                Duration = new TimeSpan(0);
                foreach (var video in sourceVideos)
                {
                    MediaElement element = new MediaElement();
                    element.LoadedBehavior = MediaState.Manual;
                    element.Source = new Uri(video.VideoUrl);
                    element.Visibility = Visibility.Hidden;
                    Duration =  Duration.Add(video.Duration);
                    VideoDurations.Add(video.Duration);
                    element.MediaEnded+=element_MediaEnded;
                    gdMedia.Children.Add(element);
                }
                (gdMedia.Children[0] as MediaElement).Visibility = Visibility.Visible;
                (gdMedia.Children[0] as MediaElement).Play();
                currentMediaElement = (gdMedia.Children[0] as MediaElement);
                timerVideoTime = new DispatcherTimer();
                timerVideoTime.Interval = TimeSpan.FromSeconds(1);
                timerVideoTime.Tick += new EventHandler(timer_Tick);
                timerVideoTime.Start();
            }
        }

        void element_MediaEnded(object sender, RoutedEventArgs e)
        {
            var index = gdMedia.Children.IndexOf(sender as UIElement);
            if (index < gdMedia.Children.Count - 1)
            {
                foreach (var child in gdMedia.Children)
                {
                    (child as UIElement).Visibility = Visibility.Hidden;
                }
                currentMediaElement = (gdMedia.Children[index + 1] as MediaElement);
                currentMediaElement.Visibility = Visibility.Visible;
                currentMediaElement.Position = new TimeSpan(0);
                currentMediaElement.Play();
            }
        }

        public void CloseMedia()
        {
            timerVideoTime.Stop();
            timeSlider.Value = 0;
            if (currentMediaElement != null)
                currentMediaElement.Source = null;
        }

        private void timeSlider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Duration.TotalSeconds > 0)
            {
                SetSeekValue(TimeSpan.FromSeconds((timeSlider.Value / 100) * Duration.TotalSeconds));
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (Duration.TotalSeconds > 0)
            {
                TimeSpan ts = GetSeekValue();
                timeSlider.Value = (ts.TotalSeconds / Duration.TotalSeconds) * 100;
                lblEllapsedTime.Content = FormatHelper.FormatTimeSpan(ts) + " / " + FormatHelper.FormatTimeSpan(Duration);
            }
        }

        private TimeSpan GetSeekValue()
        {
            if (currentMediaElement != null)
            {
                var index = gdMedia.Children.IndexOf(currentMediaElement);
                var lst = VideoDurations.Take(index).ToList();
                return TimeSpan.FromMilliseconds(lst.Select(x => x.TotalMilliseconds).Sum() + currentMediaElement.Position.TotalMilliseconds);
            }
            return new TimeSpan(0);
        }

        private void SetSeekValue(TimeSpan ts)
        {
            var lst = VideoDurations.Where(x => VideoDurations.Take(VideoDurations.IndexOf(x) + 1).Select(y => y.TotalMilliseconds).Sum() < ts.TotalMilliseconds).ToList();
            int index = 0;
            if (lst.Count == 0)
            {
                lst.Add(VideoDurations.First());
            }
            else
            {
                index = lst.Count;
            }

            (gdMedia.Children[index] as MediaElement).Position = TimeSpan.FromMilliseconds(ts.TotalMilliseconds - lst.Take(index).Select(x => x.TotalMilliseconds).Sum());
            if ((gdMedia.Children[index] as MediaElement).Visibility == Visibility.Hidden)
            {
                currentMediaElement.Pause();
                foreach (var child in gdMedia.Children)
                {
                    (child as UIElement).Visibility = Visibility.Hidden;
                }
                (gdMedia.Children[index] as MediaElement).Visibility = Visibility.Visible;
                (gdMedia.Children[index] as MediaElement).Play();
            }
            currentMediaElement = (gdMedia.Children[index] as MediaElement);
        }
    }
}
