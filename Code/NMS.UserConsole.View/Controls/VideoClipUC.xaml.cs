﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NMS.Core.Helper;
using NMS.UserConsole.ViewModel;

namespace NMS.UserConsole.View.Controls
{
    /// <summary>
    /// Interaction logic for VideoClipUC.xaml
    /// </summary>
    public partial class VideoClipUC : UserControl
    {
        public delegate void OnRemoveHandler(object sender,Marker marker);
        public event OnRemoveHandler OnRemove;
        public Marker marker;
        public VideoClipUC()
        {
            InitializeComponent();
            this.MouseEnter += VideoClipUC_MouseEnter;
            this.MouseLeave += VideoClipUC_MouseLeave;
            this.imgRemoveClip.MouseLeftButtonUp += imgRemoveClip_MouseLeftButtonUp;
        }

        void imgRemoveClip_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (OnRemove != null)
                OnRemove(this,marker);
            e.Handled = true;
        }

        void VideoClipUC_MouseLeave(object sender, MouseEventArgs e)
        {
            imgRemoveClip.Visibility = Visibility.Collapsed;
        }

        void VideoClipUC_MouseEnter(object sender, MouseEventArgs e)
        {
            imgRemoveClip.Visibility = Visibility.Visible;
        }

        public void SetUIElements(ImageSource source, TimeSpan duration,Marker _marker)
        {
            marker = _marker;
            if (this.marker.IsTransition)
            {
                this.Width = this.Width / 2;
            }
            imgThumb.Source = source;
            tbxDuration.Text = FormatHelper.FormatTimeSpan(duration);
        }
    }
}
