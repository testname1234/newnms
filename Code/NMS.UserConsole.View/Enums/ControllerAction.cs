﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.View.Enums
{
    public enum ControllerAction
    {

        PlayPause = 1,
        NextStory = 2,
        PrevStory = 3,
        RunorderTop = 4,
        ForwardBackward = 5,
    }
}
