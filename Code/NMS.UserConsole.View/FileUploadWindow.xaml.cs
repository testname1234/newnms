﻿using Newtonsoft.Json;
using NMS.UserConsole.View.Argument;
using NMS.UserConsole.ViewModel;
using NMS.UserConsole.ViewModel.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NMS.UserConsole.View
{
    /// <summary>
    /// Interaction logic for FileUploadWindow.xaml
    /// </summary>
    public partial class FileUploadWindow : Window
    {
        string MediaServerUrl { get; set; }
        public List<FileUploadArgument> UserBuckets { get; set; }
        public FileUploadArgument AssignedBucket { get; set; }
        FileUploadViewModel vm { get { return (fupload.DataContext as FileUploadViewModel); } }
        NetworkShareAccesser accessor = null;
        public FileUploadWindow(  List<FileUploadArgument> userBuckets,FileUploadArgument assignedBucket)
        {
            InitializeComponent();
            this.Closing += FileUploadWindow_Closing;
            try
            {
                var serverIP = ConfigurationManager.AppSettings["SANIp"].ToString();
                accessor = NetworkShareAccesser.Access(serverIP, serverIP + "\\muhammadchohan", "Axact@123");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

            if (userBuckets!=null)
            {
                UserBuckets = userBuckets;
                if (UserBuckets != null && UserBuckets.Count > 0)
                {
                    cmbUserBuckets.ItemsSource = UserBuckets;
                    cmbUserBuckets.SelectedIndex = 0;
                    FileUploadViewModel.BucketId = UserBuckets[0].BucketID;
                    FileUploadViewModel.ApiKey = UserBuckets[0].ApiKey;
                    FileUploadViewModel.FileUploadPath = UserBuckets[0].Name + "/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MMM") + "/" + DateTime.Now.ToString("dd");
                    lblUploadFolder.Content = FileUploadViewModel.FileUploadPath;
                }
            }
            if (assignedBucket!=null)
            {
                this.lblBucket.Visibility = this.cmbUserBuckets.Visibility = Visibility.Collapsed;
                AssignedBucket = assignedBucket;
                FileUploadViewModel.BucketId = AssignedBucket.BucketID;
                FileUploadViewModel.ApiKey = AssignedBucket.ApiKey;
                FileUploadViewModel.FileUploadPath = AssignedBucket.Name.Replace("\\", "/");
                lblUploadFolder.Content = FileUploadViewModel.FileUploadPath;
            }
        }

        void FileUploadWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                accessor.Dispose();
            }
            catch { }
        }

        private void tbxTitle_TextChanged(object sender, TextChangedEventArgs e)
        {
            vm.ChangeItemsTitle(tbxTitle.Text);
        }

        private void cmbUserBuckets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var bucket = (cmbUserBuckets.SelectedItem as FileUploadArgument);
            FileUploadViewModel.BucketId = bucket.BucketID;
            FileUploadViewModel.ApiKey = bucket.ApiKey;
            FileUploadViewModel.FileUploadPath = bucket.Name + "/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MMM") + "/" + DateTime.Now.ToString("dd");
            lblUploadFolder.Content = FileUploadViewModel.FileUploadPath;
        }
    }
}
