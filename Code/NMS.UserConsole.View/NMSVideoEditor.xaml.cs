﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using NMS.Core;
using NMS.Core.Helper;
using NMS.UserConsole.View.Controls;
using NMS.UserConsole.ViewModel;
using System.Configuration;
using NMS.Service;
using Newtonsoft.Json;
using Microsoft.Win32;
using System.Reflection;
using System.Diagnostics;
using NMS.UserConsole.View.Argument;

namespace NMS.UserConsole.View
{
    /// <summary>
    /// Interaction logic for VideoEditor.xaml
    /// </summary>
    /// 


    

    public partial class NMSVideoEditor : Window
    {
        public static TimeSpan TransitionDuration = TimeSpan.FromSeconds(3);
        private static double _canvasWidth;
        public static double CanvasWidth { get { return _canvasWidth; } set { _canvasWidth = value; Marker.TotalWidth = value; } }
        public MarkerUC MarkerInDragMode { get; set; }
        List<Marker> Markers = new List<Marker>();
        private NMSVideoEditorVM ViewModel { get { return (this.DataContext as NMSVideoEditorVM); } }
        private double SeekPositionPercentage { get; set; }
        public string totaltimeofselectedvid;
        Point dragStartPoint;
        ThreadingTasks threadingTasks;
        public string previewUrl = string.Empty;
        public string previewThumbUrl = string.Empty;
        public int SlotTemplateScreenElementId;
        public List<VideoEdits> videoEdits { get; set; }
        int UserID;
        string token;
        string objectState;
        int BuckerId;
        string ApiKey;
        double SeekPosition   
        {
            get
            { return ((double)rctSeeker.GetValue(Canvas.LeftProperty) / NMSVideoEditor.CanvasWidth) * 100; }
            set
            {
                SeekPositionPercentage = canMarkers.Width / ViewModel.SelectedVideo.Duration.TotalSeconds;
                rctSeeker.SetValue(Canvas.LeftProperty, (value * NMSVideoEditor.CanvasWidth) - (rctSeeker.Width / 2));
                TimeSpan ts =TimeSpan.FromSeconds((value) * ViewModel.SelectedVideo.Duration.TotalSeconds);
                lblEllapsedTime.Content = FormatHelper.FormatTimeSpan(ts) + " / " + FormatHelper.FormatTimeSpan(ViewModel.SelectedVideo.Duration);
                imgThumb.Source = ViewModel.SelectedVideo.GetVideoFrameThumb(value);
            }
        }

        public NMSVideoEditor(int _slotTemplateScreenElementId)
        { }
        FileUploadWindow fileUpload=null;
        public NMSVideoEditor()
        {
            InitializeComponent();
            try
            {
                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos"))
                {
                    Directory.Delete(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos", true);
                }
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos");
                //var arguments = new string[] { "nms:[{Guid:\"95c55123-96fd-4106-a52c-43ffb583d4d7\",ResourceTypeId:2,ResourceId:1},{Guid:\"d25b05b4-b943-434e-8eeb-6d264eca2a21\",ResourceTypeId:2,ResourceId:2},{Guid:\"25278fa8-135f-471d-a73b-3a172ed1fb7a\",ResourceTypeId:2,ResourceId:3}]&*&http://10.3.12.119/api/Resource/&*&6339&*&{ScreenTemplateId:684,StoryScreenTemplateId:2349,StoryId:1897,EpisodeId:1722,ThumbGuid:\"97687ce3-e8c7-4b68-81a2-292de16700e0\",BackgroundImageUrl:\"6f03c855-2cf0-400b-b41c-deafcd8da0a2\",Elements:[{TemplateScreenElementId:6339,SlotTemplateScreenElementId:6339,ScreenTemplateId:684,ScreenElementId:3,ImageGuid:\"bb515f48-317a-4863-bbf7-3411114a0a4d\",Top:0,Left:0,Bottom:341.28000000000003,Right:540,Zindex:0}]}&*&33&*&http://10.3.18.171/&*&9a518c75-82ba-2e90-85d7-5931dde0aec9" };
                var arguments = (Application.Current.Properties["arg"] as string[]);
                if (arguments != null && arguments.Count() > 0)
                {
                    arguments = arguments[0].Replace("*|*", " ").Replace("nms:", "").Split(new string[] { "&*&" }, StringSplitOptions.RemoveEmptyEntries);
                    if (arguments.Count() >= 7)
                    {
                        string resourcsJson = arguments[0];
                        string mediaServerUrl = arguments[1];
                        string slotTemplateScreenElementId = arguments[2];
                        objectState = arguments[3];
                        UserID = Convert.ToInt32(arguments[4]);
                        ConfigurationSettings.AppSettings["WebAppUrl"] = arguments[5];
                        token = arguments[6];
                        if (arguments.Count() == 9)
                        {
                            BuckerId = Convert.ToInt32(arguments[7]);
                            ApiKey = arguments[8];
                        }
                        else
                        {
                            BuckerId = 0;
                            ApiKey = null;
                        }
                        AppSettings._mediaServerUrlOverride = mediaServerUrl;
                        List<NMS.Core.Entities.Resource> resources = JsonConvert.DeserializeObject<List<NMS.Core.Entities.Resource>>(resourcsJson);
                        NMSVideoEditorVM editorVM = new NMSVideoEditorVM(resources);
                        this.DataContext = editorVM;

                        SlotTemplateScreenElementId = Convert.ToInt32(slotTemplateScreenElementId);
                        
                        
                    }
                    else if (arguments.Count() >= 4)
                    {
                        try
                        {
                            FileUploadViewModel.UserID = Convert.ToInt32(arguments[0]);
                            FileUploadViewModel.UserName = arguments[2];
                            List<FileUploadArgument> UserBuckets = null;
                            FileUploadArgument AssignedBucket = null;
                            if (arguments.Count() > 3)
                            {
                                string userBuckets = arguments[3];
                                UserBuckets = JsonConvert.DeserializeObject<List<FileUploadArgument>>(userBuckets);
                            }
                            if (arguments.Count() > 4)
                            {
                                string assignedBucket = arguments[4];
                                AssignedBucket = JsonConvert.DeserializeObject<FileUploadArgument>(assignedBucket);
                            }

                            fileUpload = new FileUploadWindow(UserBuckets, AssignedBucket);
                            fileUpload.Closed += fileUpload_Closed;
                            this.Hide();
                            fileUpload.Show();
                        }
                        catch (Exception exp)
                        {
                            MessageBox.Show("An error occurred: " + exp.Message + Environment.NewLine + exp.StackTrace);
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Parameters Count:->" + arguments.Count());
                        this.Close();
                    }
                }
                else
                {
             
               

                    Dictionary<string, string> param = new Dictionary<string, string>();
                    param.Add("VideoCutterVersion", Application.Current.Properties["version"] as string);

                    string webAppUrl = AppSettings.WebAppUrl;
                    
                    if (!RegistryOpps.Exist())
                    {
                        if (RegistryOpps.CreateProtocol())
                        {
                            HttpWebRequestHelper webHelper = new HttpWebRequestHelper();
                            var response = webHelper.GetRequest(webAppUrl + "api/user/RegisterUserIp", param, true);
                            response.Close();
                            MessageBox.Show("Protocol Registered Successfully");
                        }
                        else MessageBox.Show("UAC is required for current user");
                        this.Close();
                    }
                    else
                    {
                        HttpWebRequestHelper webHelper = new HttpWebRequestHelper();
                        var response = webHelper.GetRequest(webAppUrl + "api/user/RegisterUserIp", param, true);
                        response.Close();
                        //if (RegistryOpps.UpdateExePath())
                        //    MessageBox.Show("Protocol already registered");
                        //else MessageBox.Show("UAC is required for current user");
                    }
                    //this.Close();
                }
                this.Closed += NMSVideoEditor_Closed;
                this.SizeChanged += NMSVideoEditor_SizeChanged;
                this.Loaded += VideoEditor_Loaded;
                canMarkers.MouseLeftButtonUp += canMarkers_MouseLeftButtonUp;
                canMarkers.MouseMove += canMarkers_MouseMove;
                this.MouseMove += NMSVideoEditor_MouseMove;
                this.MouseLeftButtonUp += NMSVideoEditor_MouseLeftButtonUp;
                this.spnlRightSide.MouseLeftButtonUp += spnlRightSide_MouseLeftButtonUp;
                this.spnlClips.PreviewMouseLeftButtonDown += spnlClips_PreviewMouseLeftButtonDown;
                this.spnlClips.PreviewMouseMove += spnlClips_PreviewMouseMove;
                this.spnlClips.Drop += spnlClips_Drop;
                this.spnlClips.DragEnter += spnlClips_DragEnter;
                this.spnlTransitions.PreviewMouseLeftButtonDown += spnlClips_PreviewMouseLeftButtonDown;
                this.spnlTransitions.PreviewMouseMove += spnlClips_PreviewMouseMove;
                //this.imgClose.MouseLeftButtonUp += imgClose_MouseLeftButtonUp;
                threadingTasks = new ThreadingTasks();
                threadingTasks.OnCompleted += threadingTasks_OnCompleted;
                this.btnBacktoEditor.Click += btnBacktoEditor_Click;
            }
            catch (Exception exp)
            {
                MessageBox.Show("An error occurred: " + exp.Message + Environment.NewLine + exp.StackTrace);
                File.WriteAllText("c:\\log.txt", exp.Message + Environment.NewLine + exp.StackTrace);
                this.Close();
            }
        }

        void fileUpload_Closed(object sender, EventArgs e)
        {
            this.Close();
        }

       
        void NMSVideoEditor_Closed(object sender, EventArgs e)
        {
            if (fileUpload != null && fileUpload.IsVisible)
                fileUpload.Close();
            if (!string.IsNullOrEmpty((sender as NMSVideoEditor).previewUrl))
            {
                
            }
            else
            {
                HttpWebRequestHelper webHelper = new HttpWebRequestHelper();
                NMS.Core.DataTransfer.Script.PostInput script = new Core.DataTransfer.Script.PostInput();
                script.Script = "hideCutterLoader(true)";
                script.Token = token;
                script.UserId = UserID.ToString();
                webHelper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.Script.PostOutput>, NMS.Core.DataTransfer.Script.PostInput>(AppSettings.WebAppUrl + "api/Program/InsertVideoCutterScript", script, null);
            }
            
        }

        void btnBacktoEditor_Click(object sender, RoutedEventArgs e)
        {
            closeMediaPlayer();
        }

        void imgClose_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Hide();
        }

        void threadingTasks_OnCompleted(ThreadingTasks.ThreadingTaskNames task, object output)
        {
            switch (task)
            {
                case ThreadingTasks.ThreadingTaskNames.ClipMedia:
                    {
                        NMS.UserConsole.ViewModel.ThreadingTasks.ClipMediaAgument argument = output as NMS.UserConsole.ViewModel.ThreadingTasks.ClipMediaAgument;
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            gdPlayer.Visibility = Visibility.Visible;
                            btnBacktoEditor.Visibility = Visibility.Visible;
                            spnlRightSide.Opacity = 0.5;
                            gdPlayer.OpenMedia(new Video() { Duration = argument.To - argument.From, VideoUrl = argument.DestinationVideoUrl });
                        }));
                        Application.Current.Dispatcher.Invoke(new Action(() => { ViewModel.StopBusy(); }));
                        break;
                    }
                case ThreadingTasks.ThreadingTaskNames.CreatePreview:
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            //gdPlayer.Visibility = Visibility.Visible;
                            //btnBacktoEditor.Visibility = Visibility.Visible;
                            //spnlRightSide.Opacity = 0.5;
                            //gdPlayer.OpenMedia(output as Video);
                            videoEdits = ((output as object[])[1] as List<VideoEdits>);
                            previewUrl = ((output as object[])[0] as Video).VideoUrl;
                            previewThumbUrl = ((output as object[])[0] as Video).ThumbUrl;
                            ViewModel.StopBusy();
                            if (this.SlotTemplateScreenElementId > 0)
                            {
                                Application.Current.Dispatcher.Invoke(new Action(() => { ViewModel.StartBusy("Uploading..."); }));
                                NMS.UserConsole.ViewModel.ThreadingTasks.UploadVideoAgument inputArgs = new NMS.UserConsole.ViewModel.ThreadingTasks.UploadVideoAgument();
                                inputArgs.PreviewUrl = this.previewUrl;
                                inputArgs.previewThumbUrl = this.previewThumbUrl;
                                inputArgs.SlotTemplateScreenElementId = this.SlotTemplateScreenElementId;
                                inputArgs.VideoEdits = this.videoEdits;
                                inputArgs.Token = this.token;
                                inputArgs.UserID = this.UserID;
                                inputArgs.objectState = this.objectState;
                                inputArgs.BuckerId = this.BuckerId;
                                inputArgs.ApiKey = this.ApiKey;
                                inputArgs.Caption = tbxSlug.Text;
                                inputArgs.Duration = ((output as object[])[0] as Video).Duration.TotalSeconds;
                                ThreadPool.QueueUserWorkItem(new WaitCallback(threadingTasks.UploadVideo), inputArgs);
                            }
                            else
                            {
                                SaveFileDialog dialog = new SaveFileDialog();
                                dialog.Filter = "MPEG4 files (*.mp4)|*.mp4";
                                var result = dialog.ShowDialog();

                                if (result.HasValue && result.Value)
                                {
                                    if (File.Exists(dialog.FileName))
                                    {
                                        var res = dialog.OverwritePrompt ? MessageBoxResult.Yes : MessageBox.Show("File already exist, confirm replace?", "Replace file", MessageBoxButton.YesNo);
                                        if (res == MessageBoxResult.Yes)
                                        {
                                            File.Copy(((output as object[])[0] as Video).VideoUrl, dialog.FileName, true);
                                        }
                                    }
                                    else File.Copy(((output as object[])[0] as Video).VideoUrl, dialog.FileName, true);
                                }
                            }
                        }));
                        break;
                    }
                case ThreadingTasks.ThreadingTaskNames.CreateMultipleClips:
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            gdPlayer.Visibility = Visibility.Visible;
                            btnBacktoEditor.Visibility = Visibility.Visible;
                            spnlRightSide.Opacity = 0.5;
                            gdPlayer.OpenMedia(output as Video);
                        }));
                        Application.Current.Dispatcher.Invoke(new Action(() => { ViewModel.StopBusy(); }));
                        break;
                    }
                case ThreadingTasks.ThreadingTaskNames.UploadVideo:
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            ViewModel.StopBusy();
                            this.Close();
                        }));
                        break;
                    }
            }
        }

        private void AddTransitions()
        {
            Marker marker=new Marker(0,string.Empty);
            marker.X1Percent = TransitionDuration.TotalSeconds;
            marker.IsTransition=true;
            marker.TransitionImageUrl = new Uri("/Images/fadein.png", UriKind.Relative);
            VideoClipUC fadeInClip = new VideoClipUC();
            fadeInClip.SetUIElements(new BitmapImage(marker.TransitionImageUrl), TransitionDuration, marker);
            spnlTransitions.Children.Add(fadeInClip);

            Marker marker1 = new Marker(0, string.Empty);
            marker1.X1Percent = TransitionDuration.TotalSeconds;
            marker1.IsTransition = true;
            marker1.TransitionImageUrl = new Uri("/Images/slide.png", UriKind.Relative);
            VideoClipUC slideClip = new VideoClipUC();
            slideClip.SetUIElements(new BitmapImage(marker1.TransitionImageUrl), TransitionDuration, marker1);
            spnlTransitions.Children.Add(slideClip);
        }

        void spnlClips_DragEnter(object sender, DragEventArgs e)
        {
            if ((!e.Data.GetDataPresent(typeof(Marker)) && !e.Data.GetDataPresent(typeof(string))) ||
        sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }

        void spnlClips_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(Marker)))
            {
                Marker marker = e.Data.GetData(typeof(Marker)) as Marker;
                StackPanel stackPanel = sender as StackPanel;
                int indexTo = Markers.IndexOf((e.Source as VideoClipUC).marker);
                if (!marker.IsTransition)
                {
                    int indexFrom = Markers.IndexOf(marker);
                    Markers.RemoveAt(indexFrom);
                }
                Markers.Insert(indexTo, marker);
                UpdateTimeLine();
                dragStartPoint = new Point(0, 0);
            }
        }

        void spnlClips_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(null);
            Vector diff = dragStartPoint - mousePos;

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                StackPanel stackPanel = sender as StackPanel;
                VideoClipUC uiElement =
                    Helper.Helper.FindAnchestor<VideoClipUC>((DependencyObject)e.OriginalSource);

                DataObject dragData = new DataObject(typeof(Marker), uiElement.marker);
                DragDrop.DoDragDrop(uiElement, dragData, DragDropEffects.Move);
            } 
        }

        void spnlClips_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dragStartPoint = e.GetPosition(null);
        }

        void spnlRightSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            closeMediaPlayer();
        }

        void closeMediaPlayer()
        {
            gdPlayer.Visibility = Visibility.Collapsed;
            spnlRightSide.Opacity = 1;
            gdMultiPlayer.Visibility = Visibility.Collapsed;
            btnBacktoEditor.Visibility = Visibility.Collapsed;
            gdPlayer.CloseMedia();
            gdMultiPlayer.CloseMedia();
        }

        void NMSVideoEditor_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (MarkerInDragMode != null)
            {
                var x = (double)MarkerInDragMode.GetValue(Canvas.LeftProperty);
                if (MarkerInDragMode.IsX1)
                    MarkerInDragMode.marker.X1 = x;
                else MarkerInDragMode.marker.X = x;
                if (MarkerInDragMode.marker.X > MarkerInDragMode.marker.X1)
                {
                    var temp = MarkerInDragMode.marker.X1;
                    MarkerInDragMode.marker.X1 = MarkerInDragMode.marker.X;
                    MarkerInDragMode.marker.X = temp;
                }
                UpdateMarkerPositioning();
                UpdateTimeLine();
                MarkerInDragMode = null;
            }
        }

        void NMSVideoEditor_MouseMove(object sender, MouseEventArgs e)
        {
            if (MarkerInDragMode != null)
            {
                var lst = Markers.Where(x => x.VideoId == MarkerInDragMode.marker.VideoId).ToList();
                var position = e.GetPosition(canMarkers);
                if (lst.Where(x => x.IsMarkedAsClip && x.X < position.X && x.X1 > position.X && x != MarkerInDragMode.marker).Count() == 0)
                {
                    if (position.X >= 0 && position.X <= NMSVideoEditor.CanvasWidth)
                        MarkerInDragMode.SetValue(Canvas.LeftProperty, position.X - (MarkerInDragMode.IsX1 ? MarkerInDragMode.Width : 0));
                }
            }
        }

        void VideoEditor_Loaded(object sender, RoutedEventArgs e)
        {
            NMSVideoEditor.CanvasWidth = canMarkers.ActualWidth;
            if (ViewModel != null)
                ViewModel.OnVideoSelected += ViewModel_OnVideoSelected;
            Border border = new Border();
            border.SetValue(Canvas.LeftProperty, 5.0);
            border.SetValue(Canvas.TopProperty, 10.0);
            border.Height = 5;
            border.Width = NMSVideoEditor.CanvasWidth - 10;
            border.Background = new SolidColorBrush(Colors.White);
            canMarkers.Children.Add(border);
            AddTransitions();
        }

        void UpdateMarkerPositioning()
        {
            for (int i = 0; i < canMarkers.Children.Count; i++)
            {
                if (canMarkers.Children[i].GetType() != typeof(Ellipse) && canMarkers.Children[i].GetType() != typeof(Border))
                {
                    canMarkers.Children.RemoveAt(i);
                    i--;
                }
            }
            var lst = Markers.Where(x => x.VideoId == ViewModel.SelectedVideo.Id).ToList();
            foreach (var marker in lst)
            {
                PlaceMarker(marker, lst);
            }
        }

        void ViewModel_OnVideoSelected(Video video)
        {
            UpdateMarkerPositioning();
            imgThumb.Source = ViewModel.SelectedVideo.GetVideoFrameThumb(SeekPosition == 0 ? 1 : SeekPosition / 100);
            ucSelectedVideo.SetUIElements(imgThumb.Source, ViewModel.SelectedVideo.Size.ToString("00.00") + " MBs");
            lblEllapsedTime.Content = FormatHelper.FormatTimeSpan(TimeSpan.FromSeconds((SeekPosition > 0 ? SeekPosition : 0 / 100) * ViewModel.SelectedVideo.Duration.TotalSeconds)) + " / " + FormatHelper.FormatTimeSpan(ViewModel.SelectedVideo.Duration);
        }

        void NMSVideoEditor_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            NMSVideoEditor.CanvasWidth = canMarkers.ActualWidth;
            UpdateMarkerPositioning();
            rctSeeker.SetValue(Canvas.LeftProperty, (SeekPositionPercentage * NMSVideoEditor.CanvasWidth) - (rctSeeker.Width / 2));
            var br = canMarkers.Children.OfType<Border>().FirstOrDefault();
            if (br != null)
                br.Width = NMSVideoEditor.CanvasWidth - 10;
        }

        void canMarkers_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel!=null && ViewModel.SelectedVideo != null && ViewModel.SelectedVideo.Duration.TotalSeconds > 0)
            {
                if (e.OriginalSource is Canvas || e.OriginalSource is Ellipse || e.OriginalSource is Border)
                {
                    var lst = Markers.Where(x => x.VideoId == ViewModel.SelectedVideo.Id).ToList();

                    var position = e.GetPosition(sender as UIElement);
                    if (lst.Where(x => x.IsMarkedAsClip && x.X < position.X && x.X1 > position.X).Count() == 0)
                    {
                        Marker mark = new Marker(position.X, ViewModel.SelectedVideo.Id);
                        Markers.Add(mark);
                        PlaceMarker(mark, Markers.Where(x => x.VideoId == ViewModel.SelectedVideo.Id).ToList());
                    }
                }
            }
        }

       
        void canMarkers_MouseMove(object sender, MouseEventArgs e)
        {
            if (ViewModel!=null && ViewModel.SelectedVideo != null && ViewModel.SelectedVideo.Duration.TotalSeconds > 0)
            {
                var position = e.GetPosition(sender as UIElement);
                if (position.X >= 0 && position.X <= NMSVideoEditor.CanvasWidth)
                    SeekPosition = (position.X) / NMSVideoEditor.CanvasWidth;
            }
        }

        
        void PlaceMarker(Marker mark, List<Marker> markers)
        {
            MarkerUC marker = new MarkerUC(mark);
            marker.MouseLeftButtonDown += marker_MouseLeftButtonDown;
            marker.SetValue(Canvas.LeftProperty, (mark.X - (marker.Width)));
            marker.SetValue(Canvas.ZIndexProperty, 1);
            canMarkers.Children.Add(marker);
            if (mark.IsMarkedAsClip)
            {
                MarkerUC marker2 = new MarkerUC(mark);
                marker2.MouseLeftButtonDown += marker_MouseLeftButtonDown;
                marker2.SetValue(Canvas.LeftProperty, (mark.X1 - (marker.Width)));
                marker2.SetValue(Canvas.ZIndexProperty, 1);
                marker2.Rotate();
                canMarkers.Children.Add(marker2);
                Rectangle rect = new Rectangle();
                rect.Height = canMarkers.ActualHeight;
                rect.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0x65, 0x89, 0xC3));
                rect.Width = mark.X1 - mark.X;
                rect.SetValue(Canvas.LeftProperty, mark.X);
                rect.SetValue(Canvas.ZIndexProperty, -1);
                canMarkers.Children.Add(rect);
            }
            else
            {
                CheckForClipMarking(mark, markers);
            }
        }

        void marker_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MarkerUC uc = sender as MarkerUC;
            if (uc.marker.IsMarkedAsClip)
                MarkerInDragMode = uc;
        }

        private void CheckForClipMarking(Marker marker, List<Marker> markers)
        {
            var list = markers.OrderBy(x => x.X).ToList();
            var index = list.IndexOf(marker);
            Marker toMarker = null;
            double width = 0;
            double xPosition = 0;
            if (index > 0 && !list[index - 1].IsMarkedAsClip)
            {
                toMarker = list[index - 1];
                xPosition = toMarker.X;
                width = marker.X - toMarker.X;
            }
            else if (index < list.Count - 1 && !list[index + 1].IsMarkedAsClip)
            {
                toMarker = list[index + 1];
                xPosition = marker.X;
                width = toMarker.X - marker.X;
            }

            if (toMarker != null)
            {
                Rectangle rect = new Rectangle();
                rect.Height = canMarkers.ActualHeight;
                rect.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0x65, 0x89, 0xC3));
                rect.Width = width;
                rect.SetValue(Canvas.LeftProperty, xPosition);
                rect.SetValue(Canvas.ZIndexProperty, -1);
                var min = Math.Min(marker.X, toMarker.X);
                var max = Math.Max(marker.X, toMarker.X);
                marker.X = min;
                marker.X1 = max;
                Markers.Remove(toMarker);
                var to = canMarkers.Children.OfType<MarkerUC>().Where(x => (double)x.GetValue(Canvas.LeftProperty) == (marker.X1 - (new MarkerUC(null).Width))).FirstOrDefault();
                if (to != null)
                    to.Rotate();
                canMarkers.Children.Add(rect);
                UpdateTimeLine();
            }
        }

        private void UpdateTimeLine()
        {
            spnlClips.Children.Clear();
            var lst = Markers.Where(x => x.IsMarkedAsClip).ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                VideoClipUC videoClip = new VideoClipUC();
                videoClip.OnRemove += videoClip_OnRemove;
                if (!lst[i].IsTransition)
                {
                    videoClip.MouseLeftButtonUp += videoClip_MouseLeftButtonUp;
                    var video = ViewModel.Videos.Where(x => x.Id == lst[i].VideoId).First();
                    var percentFrom = lst[i].XPercent;
                    var percentTo = lst[i].X1Percent;
                    var from = TimeSpan.FromSeconds(video.Duration.TotalSeconds * percentFrom);
                    var to = TimeSpan.FromSeconds(video.Duration.TotalSeconds * percentTo);
                    videoClip.SetUIElements(video.GetVideoFrameThumb(percentFrom), to - from, lst[i]);
                }
                else
                {
                    videoClip.SetUIElements(new BitmapImage(lst[i].TransitionImageUrl), TransitionDuration, lst[i]);
                }
                spnlClips.Children.Add(videoClip);
            }
            UpdateDuration();
        }

        void videoClip_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var uc = (sender as VideoClipUC);
            if (uc != null)
            {
                ViewModel.StartBusy("Loading Clip...");
                NMS.UserConsole.ViewModel.ThreadingTasks.ClipMediaAgument argument = new ThreadingTasks.ClipMediaAgument();
                var video = ViewModel.Videos.Where(x => x.Id == uc.marker.VideoId).FirstOrDefault();
                argument.VideoUrl = video.VideoUrl;
                argument.From = TimeSpan.FromMilliseconds(uc.marker.XPercent * video.Duration.TotalMilliseconds);
                argument.To = TimeSpan.FromMilliseconds(uc.marker.X1Percent * video.Duration.TotalMilliseconds);
                argument.DestinationVideoUrl = Directory.GetCurrentDirectory() + "/" + AppSettings.ClipsFolderPath + "/" + uc.marker.VideoId + "-" + uc.marker.XPercent + "-" + uc.marker.X1Percent + "." + AppSettings.VideoFormat;
                if (!Directory.Exists(Directory.GetCurrentDirectory() + "/" + AppSettings.ClipsFolderPath))
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/" + AppSettings.ClipsFolderPath);
                ThreadPool.QueueUserWorkItem(new WaitCallback(threadingTasks.ClipMedia), argument);
            }
        }

        void UpdateDuration()
        {
            if (ViewModel != null)
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(Markers.Where(x => x.IsMarkedAsClip && !x.IsTransition).Sum(x => ((x.X1 - x.X) / NMSVideoEditor.CanvasWidth) * ViewModel.Videos.Where(y => y.Id == x.VideoId).First().Duration.TotalMilliseconds));
                tbxDuration.Text = FormatHelper.FormatTimeSpan(ts) + " / " + FormatHelper.FormatTimeSpan(ViewModel.Duration);
            }
        }

        void videoClip_OnRemove(object sender, Marker marker)
        {
            Markers.Remove(marker);
            if (marker.VideoId == ViewModel.SelectedVideo.Id)
                UpdateMarkerPositioning();
            spnlClips.Children.Remove(sender as UIElement);
            UpdateDuration();
        }

        private void btnPreviewVideo_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.StartBusy("Loading Preview...");
                NMS.UserConsole.ViewModel.ThreadingTasks.PreviewClipAgument argument = new ThreadingTasks.PreviewClipAgument();
                argument.Markers = Markers.Where(x => x.IsMarkedAsClip).ToList();
                argument.Videos = ViewModel.Videos.ToList();
                argument.TransitionDuration = TransitionDuration;
                ThreadPool.QueueUserWorkItem(new WaitCallback(threadingTasks.CreateMultipleClips), argument);
            }
        }

        private void btnCompileVideo_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                if (!string.IsNullOrEmpty(tbxSlug.Text))
                {
                    ViewModel.StartBusy("Compiling...");
                    NMS.UserConsole.ViewModel.ThreadingTasks.PreviewClipAgument argument = new ThreadingTasks.PreviewClipAgument();
                    argument.Markers = Markers.Where(x => x.IsMarkedAsClip).ToList();
                    argument.Videos = ViewModel.Videos.ToList();
                    argument.TransitionDuration = TransitionDuration;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(threadingTasks.CreatePreview), argument);
                }
                else
                {
                    MessageBox.Show("Enter slug for the compiled video");
                    tbxSlug.Focus();
                }
            }
        }

        private void btnBrowseVideo_Click(object sender, RoutedEventArgs e)
        {
            List<NMS.Core.Entities.Resource> resources = new List<Core.Entities.Resource>();
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            //dialog.Filter = "MPEG4 files (*.mp4)|*.mp4";
            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value && dialog.FileNames!=null)
            {
                SelectFiles(dialog.FileNames);
            }
        }

        public void SelectFiles(string[] fileNames)
        {
            List<NMS.Core.Entities.Resource> resources = new List<Core.Entities.Resource>();
            foreach (var file in fileNames)
            {
                var res = new NMS.Core.Entities.Resource();
                resources.Add(res);
                res.Guid = Guid.NewGuid().ToString();
                res.ResourceTypeId = (int)NMS.Core.Enums.ResourceTypes.Video;
                //res.FilePath = file;
                File.Copy(file, AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + res.Guid.ToString() + "." + AppSettings.VideoFormat);
            }
            if (resources.Count > 0)
            {
                Markers = new List<Marker>();
                UpdateTimeLine();
                NMSVideoEditorVM editorVM = new NMSVideoEditorVM(resources);
                this.DataContext = editorVM;
                ViewModel.OnVideoSelected += ViewModel_OnVideoSelected;
            }
        }

    }
    
}
