﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using Microsoft.DirectX.DirectInput;
using NMS.UserConsole.Controls;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Xceed.Wpf.Toolkit;
using BolUserConsole.Core.Entities;
using Awesomium.Windows.Controls;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using MOSProtocol.Lib.Enums;
using System.Threading;
using MarkupConverter;
using System.Windows.Markup;
using System.Windows.Forms;
using System.Windows.Threading;
using BolUserConsole.Repository;
using NMS.UserConsole.View.Enums;
using System.Reflection;

namespace NMS.UserConsole.View
{
    public partial class Prompter : Window
    {

        public ScaleTransform Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        public WebControl wbHost { get; set; }
        DispatcherTimer ActionTimer = null;
        System.Timers.Timer MappingTimer;
        public static int Index = 0;
        public RunOrder ro = null;
        public double ScrollPosition = 0;
        public bool Play = false;
        public DateTime LastPlayClick = DateTime.Now;
        public bool Backward = false;
        public DateTime LastBackwardClick = DateTime.Now;
        public DateTime LastNextStoryClick = DateTime.Now;
        public DateTime LastPrevStoryClick = DateTime.Now;
        public DateTime LastRunOrderTopClick = DateTime.Now;
        public Device JoyStick = null;
        public double ScrollSpeed = 100;
        public double CurrentFontSize = 80;
        public string CurrentFontFamily = "Times";
        public string CurrentActionToMap = "";

        //Cue Declaration
        bool captured = false;
        double y_shape1, y_canvas1, y_shape2, y_canvas2;     //x_shape, x_canvas, y_shape, y_canvas;

        UIElement source1 = null;
        UIElement source2 = null;

        public int CurrentStoryIndex
        {
            get
            {
                return Index;
            }
        }

        public RichTextBoxCustom p_rtb
        {
            get
            {
                return rtbPrompter;
            }
            set
            {
                rtbPrompter = value;
            }

        }

        public double step { get; set; }

        public MOSClient client;
        public string MOSId = null;
        public string Ip = null;
        public int NCSPort = 0;
        public string NCSId = null;
        public int TempCuePos = 0;
        public int LengthOfStories = 0;

        public Prompter(int s)
        {
            this.step = s;
            InitializeComponent();
            //InitDevices();
            //MappControllerInitially();
        }

        private void MappControllerInitially()
        {
            PrompterControllerActionRepository get = new PrompterControllerActionRepository();
            List<PrompterControllerAction> list= get.GetAllPrompterControllerAction();
            for (int i = 0; i < list.Count; i++)
            {
                if(list[i].PrompterControllerAction == (int)ControllerAction.PlayPause)
                {
                    PrompterAction.PlayPause=list[i].MappedValue;
                }
                if(list[i].PrompterControllerAction == (int)ControllerAction.NextStory)
                {
                    PrompterAction.NextStory=list[i].MappedValue;
                }
                if(list[i].PrompterControllerAction == (int)ControllerAction.PrevStory)
                {
                    PrompterAction.PrevStory=list[i].MappedValue;
                }
                if(list[i].PrompterControllerAction == (int)ControllerAction.RunorderTop)
                {
                    PrompterAction.RunorderTop=list[i].MappedValue;
                }
                if(list[i].PrompterControllerAction == (int)ControllerAction.ForwardBackward)
                {
                    PrompterAction.ForwardBackward = list[i].MappedValue;
                }
            }
            

        }

        public void ConnectHub()
        {
            string message = string.Empty;
            client = new MOSClient(MOSId, Ip, NCSPort, NCSId);
            client.Connect(out message);
        }

        public void LoadPrompter(RunOrder RO)
        {
            Play = false;
            ro = RO;
            LengthOfStories = ro.Stories.Count();
            upDatePrompterStory();
        }

        public void JumpToStory(int index)
        {
            Play = false;
            ScrollPosition = 0;
            Index = index;
            upDatePrompterStory();
        }

        private static FlowDocument SetRTF(string xamlString)
        {
            //xamlString = xamlString.Replace("11px","100px");
            //xamlString = xamlString.Replace("arial", "Jameel Noori Nastaleeq");
            StringReader stringReader = new StringReader(xamlString);
            System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
            Section sec = XamlReader.Load(xmlReader) as Section;
            FlowDocument doc = new FlowDocument();
            while (sec.Blocks.Count > 0)
            {
                var block = sec.Blocks.FirstBlock;
                sec.Blocks.Remove(block);
                doc.Blocks.Add(block);
            }
            return doc;
        }

        public void applyFontSizeOrFontFamily(int i, Nullable<double> FSize, string FFamily = null)
        {
            Index = i;
            CurrentFontFamily = FFamily;
            CurrentFontSize = (double)FSize;
            upDatePrompterStory();
        }

        public void upDatePrompterStory()
        {
            FlowDocument myFlowDoc = new FlowDocument();
            myFlowDoc.FontFamily = new FontFamily(CurrentFontFamily);
            myFlowDoc.FontSize = CurrentFontSize;
            Paragraph InstructionP;
            Paragraph ScriptP;
            Run InstructionR;
            Run ScriptR;

            if (ro != null)
            {
                if (ro.Stories != null)
                {
                    for (int i = Index; i < ro.Stories.Count; i++)
                    {
                        if (ro.Stories[i].IsSkipped == true)
                        {
                            Index = Index + 1;
                            continue;
                        }
                        if (ro.Stories[i].StoryItems != null)
                        {
                            for (int j = 0; j < ro.Stories[i].StoryItems.Count; j++)
                            {
                                InstructionP = new Paragraph();
                                ScriptP = new Paragraph();
                                InstructionR = new Run(ro.Stories[i].StoryItems[j].Instructions);
                                InstructionR.Background = Brushes.Yellow;
                                InstructionR.Foreground = Brushes.Blue;
                                ScriptR = new Run(ro.Stories[i].StoryItems[j].Detail);

                                InstructionP.Inlines.Add(InstructionR);
                                ScriptP.Inlines.Add(ScriptR);

                                ScriptP.LineHeight = 1;
                                myFlowDoc.Blocks.Add(InstructionP);


                                FlowDocument doc = new FlowDocument();
                                doc = SetRTF(HtmlToXamlConverter.ConvertHtmlToXaml(ro.Stories[i].StoryItems[j].Detail, false));

                                myFlowDoc.Blocks.Add(doc.Blocks.FirstBlock);

                            }

                            wbHost.ExecuteJavascript(string.Format("storyStarted({0});", ro.Stories[i].StoryId));
                            Thread oThread = new Thread(() => { NotifyHub(ro.Stories[i]); });
                            oThread.Start();
                            break;
                        }

                    }
                }
                rtbPrompter.Document = myFlowDoc;
                rtbPrompter.TextFormatter = new RtfFormatter();
                ScrollPosition = 0;
            }
        }

        private void NotifyHub(BolUserConsole.Core.Entities.Story story)
        {
            string message = string.Empty;

            try
            {
                if (client == null || !client.IsConnected)
                {
                    client = new MOSClient(MOSId, Ip, NCSPort, NCSId);
                    client.Connect(out message);
                }
                if (client != null
                    && client.IsConnected)
                {
                    MOS mos = new MOS();
                    mos.mosID = MOSId;
                    mos.ncsID = NCSId;
                    mos.messageID = 1;

                    Notification notification = new Notification();
                    notification.groupID = "pcr";
                    notification.roID = story.RoId.ToString();
                    notification.storyID = story.StoryId.ToString();
                    notification.storyStatus = StoryStatus.PLAY;

                    mos.command = notification;

                    client.SendMOSCommand(mos, out message);
                }
            }
            catch
            {
            }
        }

        private void InitDevices()
        {
            foreach (
                DeviceInstance di in
                Manager.GetDevices(
                    DeviceClass.GameControl,
                    EnumDevicesFlags.AttachedOnly))
            {
                JoyStick = new Device(di.InstanceGuid);
                JoyStick.Acquire();
                break;
            }



            ActionTimer = new DispatcherTimer();
            ActionTimer.Interval = TimeSpan.FromMilliseconds(1);
            ActionTimer.Tick += ActionTimer_Tick;
            ActionTimer.IsEnabled = true;

            MappingTimer = new System.Timers.Timer(1);
            MappingTimer.Elapsed += MappingTimer_Tick;
        }

        private void ActionTimer_Tick(object sender, EventArgs e)
        {
                ActionTimer.IsEnabled = false;
                UpdateJoystick();
                this.Dispatcher.Invoke((System.Action)(() =>
                {
                    if (Play)
                    {

                        if (!Backward && ScrollPosition < rtbPrompter.MaxHeight)
                        {
                            ScrollPosition += step;
                        }
                        else if (Backward && ScrollPosition >= 1)
                        {
                            ScrollPosition -= step;
                        }
                        else if (ScrollPosition <= 0)
                        {
                            ScrollPosition = step;
                        }
                        if (rtbPrompter.VerticalOffset + rtbPrompter.ViewportHeight == rtbPrompter.ExtentHeight)
                        {
                            if (Index <= ro.Stories.Count)
                            {
                                Play = false;
                                Index = Index + 1;
                                upDatePrompterStory();
                            }
                            else
                            {
                                rtbPrompter.Text = string.Empty;
                                Play = false;
                            }
                            ScrollPosition = step;
                        }
                        if (ScrollPosition > 0 && ScrollPosition <= rtbPrompter.MaxHeight)
                        {
                            rtbPrompter.ScrollToVerticalOffset(ScrollPosition);
                        }
                    }
                    ActionTimer.IsEnabled = true;
                }));
        }
        
        private void MappingTimer_Tick(object sender, EventArgs e)
        {
                 MappingTimer.Enabled = false;
                 MappJoystickController();
                 MappingTimer.Enabled = true;
        }

        private void UpdateJoystick()
        {
            JoystickState state = JoyStick.CurrentJoystickState;

            byte[] buttons = state.GetButtons();
            for (int i = 0; i < buttons.Length; i++)
            {
                if (buttons[i] != 0)
                {

                    if (i == PrompterAction.PlayPause)
                    {
                        if ((DateTime.Now - LastPlayClick).TotalMilliseconds > 500)
                           {
                                Play = !Play;
                                LastPlayClick = DateTime.Now;
                           }
                    
                    }
                    else if (i == PrompterAction.NextStory)
                    {
                        if ((DateTime.Now - LastNextStoryClick).TotalMilliseconds > 500)
                        {
                                NextStory();
                                LastNextStoryClick = DateTime.Now;                                                                 
                        }
                    }
                    else if (i == PrompterAction.PrevStory)
                    {
                        if ((DateTime.Now - LastPrevStoryClick).TotalMilliseconds > 500)
                        {
                                previousStory();
                                LastPrevStoryClick = DateTime.Now;
                        }
                        
                    }
                    else if (i == PrompterAction.RunorderTop)
                    {
                        if ((DateTime.Now - LastRunOrderTopClick).TotalMilliseconds > 500)
                        {
                                Play = false;
                                ScrollPosition = 0;
                                int indx= 0;
                                JumpToStory(indx);
                                LastRunOrderTopClick = DateTime.Now;
                        }

                    }
                    else if (i == PrompterAction.ForwardBackward)
                    {
                        if ((DateTime.Now - LastBackwardClick).TotalMilliseconds > 500)
                        {
                                Backward = !Backward;
                                LastBackwardClick = DateTime.Now;
                        }

                    }
                }
            }
        }

        public void NextStory()
        {
            int indx = Index + 1;
            if (indx < LengthOfStories) 
            {
                JumpToStory(indx);
            }
        }

        public void previousStory()
        {
            int indx = Index - 1;
            if (indx > 0)
            {
                JumpToStory(indx);
            }
        }

        public void  MappJoystickController() 
        {

            JoystickState state = JoyStick.CurrentJoystickState;
            byte[] buttons = state.GetButtons();
            for (int i = 0; i < buttons.Length; i++)
            {
                if (buttons[i] != 0)
                {
                    if(CurrentActionToMap == "PlayPause")
                    {
                        if ((DateTime.Now - LastPlayClick).TotalMilliseconds > 500)
                        {
                            checkDuplicationAndMap("PlayPause", i);
                            PrompterAction.PlayPause = i;
                            LastPlayClick = DateTime.Now;
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("mappingKeyCallBack('" + CurrentActionToMap + "');");
                            });
                        }
                    }
                    
                    if (CurrentActionToMap == "NextStory")
                    {
                        if ((DateTime.Now - LastNextStoryClick).TotalMilliseconds > 500)
                        {
                            checkDuplicationAndMap("NextStory", i);
                            PrompterAction.NextStory = i;
                            LastNextStoryClick = DateTime.Now;
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("mappingKeyCallBack('" + CurrentActionToMap + "');");
                            });
                        }
                        
                    }
                    if (CurrentActionToMap == "PrevStory")
                    {
                        if ((DateTime.Now - LastPrevStoryClick).TotalMilliseconds > 500)
                        {
                            checkDuplicationAndMap("PrevStory", i);
                            PrompterAction.PrevStory = i;
                            LastPrevStoryClick = DateTime.Now;
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("mappingKeyCallBack('" + CurrentActionToMap + "');");
                            });
                        }
                        
                    }
                    if (CurrentActionToMap == "RunorderTop")
                    {
                        if ((DateTime.Now - LastRunOrderTopClick).TotalMilliseconds > 500)
                        {
                            checkDuplicationAndMap("RunorderTop", i);
                            PrompterAction.RunorderTop = i;
                            LastRunOrderTopClick = DateTime.Now;
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("mappingKeyCallBack('" + CurrentActionToMap + "');");
                            });
                        }
                       
                    }

                    if (CurrentActionToMap == "ForwardBackward")
                    {
                        if ((DateTime.Now - LastBackwardClick).TotalMilliseconds > 500)
                        {
                            checkDuplicationAndMap("ForwardBackward", i);
                            PrompterAction.ForwardBackward = i;
                            LastBackwardClick = DateTime.Now;
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("mappingKeyCallBack('" + CurrentActionToMap + "');");
                            });
                        }
                        
                    } 
                }
            }
        
        }

        private void checkDuplicationAndMap(string name,int i)
        {
            foreach (FieldInfo field in typeof(PrompterAction).GetFields())
            {
                if (field.Name != name && i == (int)field.GetValue(null))
                {
                    DateTime date=new DateTime();
                    int n = int.Parse(date.ToString("MMss"));
                    field.SetValue(null,n);
                } 
                
            }
        }

        public void  MappingToggler(bool mapping) {
            if (mapping)
            {
                ActionTimer.IsEnabled = false;
                MappingTimer.Enabled = true;
            }
            else 
            {
                MappingTimer.Enabled = false;
                ActionTimer.IsEnabled = true;
                UpdateControllerActionToDb();

            }
           
             
        }

        private void UpdateControllerActionToDb()
        {
            PrompterControllerActionRepository updateAction = new PrompterControllerActionRepository();
            updateAction.MappControllerActionById((int)ControllerAction.PlayPause,  PrompterAction.PlayPause);
            updateAction.MappControllerActionById((int)ControllerAction.NextStory,   PrompterAction.NextStory);
            updateAction.MappControllerActionById((int)ControllerAction.PrevStory,    PrompterAction.PrevStory);
            updateAction.MappControllerActionById((int)ControllerAction.RunorderTop,   PrompterAction.RunorderTop);
            updateAction.MappControllerActionById((int)ControllerAction.ForwardBackward, PrompterAction.ForwardBackward);
        }

        private void RichTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        
        // joystick Actions
        public static class PrompterAction
        {
           public static int PlayPause= 0;
           public static int NextStory = 1;
           public static int PrevStory = 2;
           public static int RunorderTop = 3;
           public static int ForwardBackward= 4;
           
 
        }

        //Drag CUE
        private void shape_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            source1 = LeftArrow;
            source2 = RightArrow;

            // source1 = (UIElement)sender;
            // source2 = (UIElement)sender;

            System.Windows.Input.Mouse.Capture(source1);
            System.Windows.Input.Mouse.Capture(source2);

            captured = true;
            //x_shape = Canvas.GetLeft(source);
            //x_canvas = e.GetPosition(LayoutRoot).X;
            y_shape1 = Canvas.GetTop(source1);
            y_shape2 = Canvas.GetTop(source2);
            y_canvas1 = e.GetPosition(LayoutRoot1).Y;
            y_canvas2 = e.GetPosition(LayoutRoot2).Y;

        }

        private void shape_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (captured)
            {
                //  double x = e.GetPosition(LayoutRoot).X;
                double y1 = e.GetPosition(LayoutRoot1).Y;
                double y2 = e.GetPosition(LayoutRoot2).Y;

                //  x_shape += x - x_canvas;
                // Canvas.SetLeft(source, x_shape);
                // x_canvas = x;
                y_shape1 += y1 - y_canvas1;
                y_shape2 += y2 - y_canvas2;

                Canvas.SetTop(source1, y_shape1);
                Canvas.SetTop(source2, y_shape2);

                y_canvas1 = y1;
                y_canvas2 = y2;
            }
        }

        private void shape_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Input.Mouse.Capture(null);
            captured = false;
        }
    }
}

