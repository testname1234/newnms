﻿using System;
using System.Windows;
using WPFCSharpWebCam;

namespace NMS.UserConsole.View
{
    public partial class PrompterPreview : Window
    {
        public PrompterPreview()
        {
            InitializeComponent();
        }

        public WebCam webcam { get; set; }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            try
            {
                webcam = new WebCam();
                webcam.InitializeWebCam(ref imgVideo);
                webcam.Start();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
