﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace NMS.UserConsole.View
{
    public class RegistryOpps
    {
        public static string ApplicationName = "Test";

        static RegistryKey rk = Registry.ClassesRoot;
        public static bool Exist()
        {
            // Opening the registry key
            // Open a subKey as read-only
            RegistryKey sk1 = rk.OpenSubKey("nms");
            // If the RegistrySubKey doesn't exist -> (null)
            return !(sk1 == null);
        }

        public static bool CreateProtocol()
        {
            try
            {
                // Setting
                
                // I have to use CreateSubKey 
                // (create or open it if already exits), 
                // 'cause OpenSubKey open a subKey as read-only
                var protocol = rk.CreateSubKey("nms");
                RegistryKey sk1 = protocol.CreateSubKey("DefaultIcon");
                protocol.SetValue("", "URL:NMS UserConsole Protocol");
                protocol.SetValue("URL Protocol", "");
                sk1.SetValue("", "NMS.UserConsole.exe,1");
                var cm = protocol.CreateSubKey("shell").CreateSubKey("open").CreateSubKey("command");
                string path = AppDomain.CurrentDomain.BaseDirectory + "NMS.UserConsole.exe";
                cm.SetValue("", "\"" + path + "\"  \"%1\"");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool DeleteKey(string KeyName)
        {
            try
            {
                // Setting
                RegistryKey sk1 = rk.OpenSubKey("SOFTWARE").CreateSubKey(ApplicationName);

                // If the RegistrySubKey doesn't exists -> (true)
                if (sk1 == null)
                    return true;
                else
                    sk1.DeleteValue(KeyName);

                return true;
            }
            catch (Exception e)
            {
                // AAAAAAAAAAARGH, an error!
                return false;
            }
        }

        public static bool DeleteSubKeyTree()
        {
            try
            {
                // Setting
                RegistryKey sk1 = rk.OpenSubKey("SOFTWARE").OpenSubKey(ApplicationName);

                // If the RegistryKey exists, I delete it
                if (sk1 != null)
                    rk.OpenSubKey("SOFTWARE", true).DeleteSubKeyTree(ApplicationName);

                return true;
            }
            catch (Exception e)
            {
                // AAAAAAAAAAARGH, an error!
                return false;
            }
        }

        internal static bool UpdateExePath()
        {
            try
            {
                // Setting
                // I have to use CreateSubKey 
                // (create or open it if already exits), 
                // 'cause OpenSubKey open a subKey as read-only
                var protocol = rk.OpenSubKey("nms");
                var cm = protocol.OpenSubKey("shell").OpenSubKey("open").OpenSubKey("command", true);
                string path = AppDomain.CurrentDomain.BaseDirectory + "NMS.UserConsole.exe";
                cm.SetValue("", "\"" + path + "\"  \"%1\"");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
