﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace NMS.UserConsole.Controls
{
    public class RichTextBoxCustom : RichTextBox
    {
        public static readonly DependencyProperty CurrentFontFamilyProperty =
                DependencyProperty.Register("CurrentFontFamily",
                typeof(FontFamily), typeof
                (RichTextBoxCustom),
                new FrameworkPropertyMetadata(new FontFamily("Arial"),
                FrameworkPropertyMetadataOptions.None,
                new PropertyChangedCallback(OnCurrentFontChanged)));

        public FontFamily CurrentFontFamily
        {
            get
            {
                return (FontFamily)GetValue(CurrentFontFamilyProperty);
            }
            set
            {
                SetValue(CurrentFontFamilyProperty, value);
            }
        }

        private static void OnCurrentFontChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue != null)
            {
                RichTextBox rtb = (RichTextBox)obj;
                rtb.Selection.ApplyPropertyValue(System.Windows.Controls.RichTextBox.FontFamilyProperty, args.NewValue.ToString());
                rtb.Focus();
            }
        }


        public static readonly DependencyProperty CurrentFontSizeProperty =
                DependencyProperty.Register("CurrentFontSize",
                typeof(double), typeof
                (RichTextBoxCustom),
                new FrameworkPropertyMetadata(10.0,
                FrameworkPropertyMetadataOptions.None,
                new PropertyChangedCallback(OnCurrentFontSizeChanged)));

        public double CurrentFontSize
        {
            get
            {
                return (double)GetValue(CurrentFontSizeProperty);
            }
            set
            {
                SetValue(CurrentFontSizeProperty, value);
            }
        }

        private static void OnCurrentFontSizeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue != null)
            {
                RichTextBox rtb = (RichTextBox)obj;
                rtb.Selection.ApplyPropertyValue(System.Windows.Controls.RichTextBox.FontSizeProperty, (double)args.NewValue);
                rtb.Focus();
            }
        }

        public static readonly DependencyProperty CurrentForegroundProperty =
                DependencyProperty.Register("CurrentForeground",
                typeof(Color), typeof
                (RichTextBoxCustom),
                new FrameworkPropertyMetadata(Colors.Black,
                FrameworkPropertyMetadataOptions.None,
                new PropertyChangedCallback(OnCurrentForegroundChanged)));

        public Color CurrentForeground
        {
            get
            {
                return (Color)GetValue(CurrentForegroundProperty);
            }
            set
            {
                SetValue(CurrentForegroundProperty, value);
            }
        }

        private static void OnCurrentForegroundChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue != null)
            {
                RichTextBox rtb = (RichTextBox)obj;
                rtb.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush((Color)args.NewValue));
                rtb.Focus();
            }
        }

    }
}
