﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using NMS.Core;
using NMS.UserConsole.View.Controls;
using NMS.UserConsole.ViewModel;

namespace NMS.UserConsole.View
{
    /// <summary>
    /// Interaction logic for VideoEditor.xaml
    /// </summary>
    public partial class VideoEditor : Window
    {
        List<Marker> Markers = new List<Marker>();
        private TimeSpan? From;
        private TimeSpan? To;
        private DispatcherTimer timerVideoTime;
        private VideoEditorVM ViewModel { get { return (this.DataContext as VideoEditorVM); } }
        public double SeekPosition
        {
            get
            { return ((double)rctSeeker.GetValue(Canvas.LeftProperty) / canMarkers.ActualWidth) * 100; }
            set
            {
                rctSeeker.SetValue(Canvas.LeftProperty, value * canMarkers.ActualWidth);
                int x = Convert.ToInt32((ViewModel.SelectedVideo.Duration.TotalMilliseconds / 1000) * value);
                imgThumb.Source = new BitmapImage(new Uri(ViewModel.SelectedVideo.VideoFrameThumbsUrl + "/thumb" + (x == 0 ? 1 : x) + ".png"));
            }
        }
        public VideoEditor()
        {
            InitializeComponent();
            this.Loaded += VideoEditor_Loaded;
            //this.media.MediaFailed += media_MediaFailed;
            //this.media.MediaOpened += media_MediaOpened;
            //this.media.MediaEnded += media_MediaEnded;
            //this.media.SourceUpdated += media_SourceUpdated;
            //timeSlider.AddHandler(MouseLeftButtonUpEvent,
            //          new MouseButtonEventHandler(timeSlider_MouseLeftButtonUp),
            //          true);

            canMarkers.MouseLeftButtonUp += canMarkers_MouseLeftButtonUp;
            canMarkers.MouseMove += canMarkers_MouseMove;
            scrollVideoThumbs.ScrollChanged += scrollVideoThumbs_ScrollChanged;
        }

        void scrollVideoThumbs_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (ViewModel.SelectedVideo != null && ViewModel.SelectedVideo.Duration.TotalSeconds > 0)
            {
                double percentage = (e.HorizontalOffset / scrollVideoThumbs.ExtentWidth);
                SeekPosition = percentage;
            }
        }

        void VideoEditor_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.OnVideoSelected += ViewModel_OnVideoSelected;
        }

        void ViewModel_OnVideoSelected(Video video)
        {
            tblDuration.Text = video.Duration.Hours.ToString("00") + ":" + video.Duration.Minutes.ToString("00") + ":" + video.Duration.Seconds.ToString("00");
            for (int i = 0; i < canMarkers.Children.Count; i++)
            {
                if (canMarkers.Children[i].GetType() != typeof(Ellipse))
                {
                    canMarkers.Children.RemoveAt(i);
                    i--;
                }
            }
            var lst = Markers.Where(x => x.VideoId == video.Id).ToList();
            foreach (var marker in lst)
            {
                PlaceMarker(marker, lst);
            }
            imgThumb.Source = new BitmapImage(new Uri(ViewModel.SelectedVideo.VideoFrameThumbsUrl + "/thumb" + Convert.ToInt32(SeekPosition > 0 ? (SeekPosition / 100) * video.Duration.TotalSeconds : 1) + ".png"));
        }

        void canMarkers_MouseMove(object sender, MouseEventArgs e)
        {
            if (ViewModel.SelectedVideo != null && ViewModel.SelectedVideo.Duration.TotalSeconds > 0)
            {
                var position = e.GetPosition(sender as UIElement);
                scrollVideoThumbs.ScrollToHorizontalOffset((position.X / canMarkers.ActualWidth) * scrollVideoThumbs.ExtentWidth);
            }
        }

        void canMarkers_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel.SelectedVideo != null && ViewModel.SelectedVideo.Duration.TotalSeconds > 0)
            {
                if (e.OriginalSource is Canvas || e.OriginalSource is Ellipse)
                {   
                    var lst = Markers.Where(x => x.VideoId == ViewModel.SelectedVideo.Id).ToList();
                    
                    var position = e.GetPosition(sender as UIElement);
                    if (lst.Where(x => x.IsMarkedAsClip && x.X < position.X && x.X1 > position.X).Count() == 0)
                    {
                        Marker mark = new Marker(position.X, ViewModel.SelectedVideo.Id);
                        Markers.Add(mark);
                        PlaceMarker(mark, Markers.Where(x => x.VideoId == ViewModel.SelectedVideo.Id).ToList());
                    }
                }
            }
        }

        void PlaceMarker(Marker mark,List<Marker> markers)
        {
            MarkerUC marker = new MarkerUC(mark);
            marker.SetValue(Canvas.LeftProperty, (mark.X - (marker.Width / 2)));
            marker.SetValue(Canvas.TopProperty, 0.0);
            marker.SetValue(Canvas.ZIndexProperty, 1);
            canMarkers.Children.Add(marker);
            if (mark.IsMarkedAsClip)
            {
                MarkerUC marker2 = new MarkerUC(mark);
                marker2.SetValue(Canvas.LeftProperty, (mark.X1 - (marker.Width / 2)));
                marker2.SetValue(Canvas.TopProperty, 0.0);
                marker2.SetValue(Canvas.ZIndexProperty, 1);
                canMarkers.Children.Add(marker2);
                Rectangle rect = new Rectangle();
                rect.Height = canMarkers.ActualHeight;
                rect.Fill = new SolidColorBrush(Colors.AliceBlue);
                rect.Width = mark.X1 - mark.X;
                rect.SetValue(Canvas.LeftProperty, mark.X);
                rect.SetValue(Canvas.ZIndexProperty, -1);
                canMarkers.Children.Add(rect);
            }
            else
            {
                CheckForClipMarking(mark, markers);
            }
        }

        private void CheckForClipMarking(Marker marker, List<Marker> markers)
        {
            var list = markers.OrderBy(x => x.X).ToList();
            var index = list.IndexOf(marker);
            Marker toMarker = null;
            double width = 0;
            double xPosition = 0;
            if (index > 0 && !list[index - 1].IsMarkedAsClip)
            {
                toMarker = list[index - 1];
                xPosition = toMarker.X;
                width = marker.X - toMarker.X;
            }
            else if (index < list.Count - 1 && !list[index + 1].IsMarkedAsClip)
            {
                toMarker = list[index + 1];
                xPosition = marker.X;
                width = toMarker.X - marker.X;
            }

            if (toMarker != null)
            {
                Rectangle rect = new Rectangle();
                rect.Height = canMarkers.ActualHeight;
                rect.Fill = new SolidColorBrush(Colors.AliceBlue);
                rect.Width = width;
                rect.SetValue(Canvas.LeftProperty, xPosition);
                rect.SetValue(Canvas.ZIndexProperty, -1);
                var min =Math.Min(marker.X, toMarker.X);
                var max =Math.Max(marker.X, toMarker.X);
                marker.X = min;
                marker.X1 = max;
                Markers.Remove(toMarker);
                canMarkers.Children.Add(rect);
                UpdateTimeLine();
            }
        }

        private void UpdateTimeLine()
        {
            spnlTimeLine.Children.Clear();
            for (int i = 0; i < Markers.Count; i++)
            {
                Image img = new Image();
                img.Stretch = Stretch.Fill;
                var video = ViewModel.Videos.Where(x => x.Id == Markers[i].VideoId).First();
                int x1 = Convert.ToInt32((video.Duration.TotalMilliseconds / 1000) * Markers[i].X / canMarkers.ActualWidth);
                img.Source = new BitmapImage(new Uri(video.VideoFrameThumbsUrl + "/thumb" + (x1 == 0 ? 1 : x1) + ".png"));
                img.Width = (Markers[i].X1 - Markers[i].X) - 2;
                Rectangle rect = new Rectangle();
                rect.Width = 2;
                rect.Fill = new SolidColorBrush(Colors.DeepSkyBlue);
                spnlTimeLine.Children.Add(rect);
                spnlTimeLine.Children.Add(img);
            }
        }

        //void media_MediaEnded(object sender, RoutedEventArgs e)
        //{
        //    timerVideoTime.Stop();
        //    timeSlider.Value = 0;
        //}

        //void media_SourceUpdated(object sender, DataTransferEventArgs e)
        //{
        //    timerVideoTime.Stop();
        //    timeSlider.Value = 0;
        //}

        //void media_MediaOpened(object sender, RoutedEventArgs e)
        //{
        //    if (media.NaturalDuration.HasTimeSpan)
        //    {
        //        timerVideoTime = new DispatcherTimer();
        //        timerVideoTime.Interval = TimeSpan.FromSeconds(1);
        //        timerVideoTime.Tick += new EventHandler(timer_Tick);
        //        timerVideoTime.Start();
        //    }
        //}

        //private void timeSlider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    if (ViewModel.SelectedVideo!=null && ViewModel.SelectedVideo.Duration.TotalSeconds > 0)
        //    {
        //        media.Position = TimeSpan.FromSeconds((timeSlider.Value / 100) * ViewModel.SelectedVideo.Duration.TotalSeconds);
        //    }
        //}

        //void timer_Tick(object sender, EventArgs e)
        //{
        //    if (media.NaturalDuration.HasTimeSpan && media.NaturalDuration.TimeSpan.TotalSeconds > 0)
        //    {
        //        if (ViewModel.SelectedVideo.Duration.TotalSeconds > 0)
        //        {
        //            // Updating time slider
        //            timeSlider.Value = (media.Position.TotalSeconds /
        //                               ViewModel.SelectedVideo.Duration.TotalSeconds) * 100;
        //            tblEllapsedDuration.Text = media.Position.Hours.ToString("00") + ":" + media.Position.Minutes.ToString("00") + ":" + media.Position.Seconds.ToString("00");
        //        }
        //    }
        //    // Check if the movie finished calculate it's total time
        //}

        //void media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        //{
        //    MessageBox.Show(e.ErrorException.Message);
        //}
    }


}
