﻿using NMS.Service;
using NMS.UserConsole.ViewModel.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NMS.UserConsole.ViewModel
{
    public class FileItem : INotifyPropertyChanged
    {
        public ICommand PausePlayCommand { get; set; }
        public event EventHandler OnFileResume;
        public event EventHandler OnPostResourceComplete;

        public FileItem()
        {
            PausePlayCommand = new RelayCommand(ExecutePausePlayCommand);
            PauseResumeButtonText = "Pause";
            DestinationPath = FileUploadViewModel.FileUploadPath;
            IsReadyVisibility = System.Windows.Visibility.Collapsed;
            IsNotReadyVisibility = System.Windows.Visibility.Visible;
        }

        public bool IsPaused { get; set; }
        private bool _isCoping;
        public bool IsCoping
        {
            get { return _isCoping; }
            set
            {
                _isCoping = value;
                if (!_isCoping)
                    PauseResumeVisibility = System.Windows.Visibility.Collapsed;
                else PauseResumeVisibility = System.Windows.Visibility.Visible;


                if (_isCoping && !IsGettingGuid && !IsSubmittedToMediaServer)
                {
                    IsGettingGuid = true;
                    ThreadPool.QueueUserWorkItem(PostResource, this);
                }
            }
        }

        bool IsGettingGuid = false;
        bool ThumbGenerated = false;
        public bool IsSubmittedToMediaServer = false;

        public string DestinationPath { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }

        private string _pauseResumeButtonText;
        public string PauseResumeButtonText { get { return _pauseResumeButtonText; } set { _pauseResumeButtonText = value; NotifyPropertyChanged("PauseResumeButtonText"); } }

        private string _progressDisplay;
        public string ProgressDisplay { get { return _progressDisplay; } set { _progressDisplay = value; NotifyPropertyChanged("ProgressDisplay"); } }

        private double _progress;
        public double Progress { get { return _progress; } set { _progress = value; if (_progress == 100 && IsSubmittedToMediaServer && ThumbGenerated)IsReadyVisibility = System.Windows.Visibility.Visible; NotifyPropertyChanged("Progress"); } }

        private string _uploadedProgress;
        public string UploadedProgress { get { return _uploadedProgress; } set { _uploadedProgress = value; NotifyPropertyChanged("UploadedProgress"); } }

        private string _title;
        public string Title { get { return _title; } set { _title = value; NotifyPropertyChanged("Title"); } }

        private System.Windows.Visibility _pauseResumeVisibility;
        public System.Windows.Visibility PauseResumeVisibility { get { return _pauseResumeVisibility; } set { _pauseResumeVisibility = value; NotifyPropertyChanged("PauseResumeVisibility"); } }

        private System.Windows.Visibility _isReadyVisibility;
        public System.Windows.Visibility IsReadyVisibility
        {
            get { return _isReadyVisibility; }
            set
            {
                _isReadyVisibility = value; 
                NotifyPropertyChanged("IsReadyVisibility");
                IsNotReadyVisibility = (value == System.Windows.Visibility.Visible) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
            }
        }

        private System.Windows.Visibility _isNotReadyVisibility;
        public System.Windows.Visibility IsNotReadyVisibility { get { return _isNotReadyVisibility; } set { _isNotReadyVisibility = value; NotifyPropertyChanged("IsNotReadyVisibility"); } }

        private string _error;
        public string Error { get { return _error; } set { _error = value; NotifyPropertyChanged("Error"); } }

        public long UploadedLength { get; set; }

        public NMS.Core.Entities.Resource Resource { get; set; }
        private void PostResource(object state)
        {
            ResourceService resourceService = new ResourceService(null, null);
            var serverIP = ConfigurationManager.AppSettings["SANIp"].ToString();
            var serverPath = "\\\\" + serverIP + "\\mmstemp\\";
            Resource = resourceService.MediaPostResourceForNMSFileUpload(FileUploadViewModel.BucketId, FileUploadViewModel.ApiKey, Title, serverPath + this.DestinationPath + "/" + this.FileName, this.DestinationPath.Substring(this.DestinationPath.IndexOf("/")), FileUploadViewModel.UserID, FileUploadViewModel.UserName);
            IsGettingGuid = false;
            IsSubmittedToMediaServer = true;
            if (this.Progress == 100)
                GenerateThumb();
            if (_progress == 100 && IsSubmittedToMediaServer && ThumbGenerated) IsReadyVisibility = System.Windows.Visibility.Visible;
            if (OnPostResourceComplete != null)
                OnPostResourceComplete(null, null);
        }

        public void GenerateThumb()
        {
            if (!ThumbGenerated)
            {
                ResourceService resourceService = new ResourceService(null, null);
                var serverIP = ConfigurationManager.AppSettings["SANIp"].ToString();
                var serverPath = "\\\\" + serverIP + "\\mmstemp\\";
                resourceService.GenerateCustomSnapshotOnMediaServer(new Core.Models.CustomSnapshotInput() { Id = Resource.Guid, ResourceTypeId = Resource.ResourceTypeId, TimeSpan = 1, FilePath = serverPath + this.DestinationPath + "/" + this.FileName });
                ThumbGenerated = true;
            }
        }

        private void ExecutePausePlayCommand(object obj)
        {
            IsPaused = !IsPaused;
            if (!IsPaused)
            {
                PauseResumeButtonText = "Pause";
                OnFileResume(this, null);
            }
            else
                PauseResumeButtonText = "Resume";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
