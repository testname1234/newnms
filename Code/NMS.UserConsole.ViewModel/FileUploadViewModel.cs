﻿using NMS.Service;
using NMS.UserConsole.ViewModel.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace NMS.UserConsole.ViewModel
{
     public class FileUploadViewModel : INotifyPropertyChanged
    {
        CustomFileCopier copier = new CustomFileCopier();
        public ObservableCollection<FileItem> Items { get; set; }
        private double _progress;
        public double TotalProgress { get { return _progress; } set { _progress = value; NotifyPropertyChanged("TotalProgress"); } }
        public event EventHandler OnUploadCompleted;
        private string Title { get; set; }
        public static int UserID;
        public static string UserName;
        public static int BucketId;
        public static string ApiKey;
        public static string FileUploadPath;
        public FileUploadViewModel()
        {
            copier.OnProgressChanged += copier_OnProgressChanged;
            copier.OnComplete += copier_OnComplete;
            Items = new ObservableCollection<FileItem>();
        }

        public void ChangeItemsTitle(string title)
        {
            foreach (var item in Items)
            {
                if (item.Title == Title || string.IsNullOrEmpty(item.Title))
                    item.Title = title;
            }
            Title = title;
        }

        public void AddFileItem(string fileName)
        {
            FileItem item = new FileItem();
            item.FilePath = fileName;
            item.Title = this.Title;
            item.FileName = System.IO.Path.GetFileName(fileName);
            item.Progress = 0;
            item.ProgressDisplay = "0%";
            item.OnFileResume += item_OnFileResume;
            item.OnPostResourceComplete += item_OnPostResourceComplete;
            updateFileName(item);
            Items.Add(item);
        }

        private void updateFileName(FileItem item)
        {
            var serverIP = ConfigurationManager.AppSettings["SANIp"].ToString();
            if (File.Exists("\\\\" + serverIP + "\\mmstemp\\" + item.DestinationPath + "\\" + item.FileName))
            {
                item.FileName = System.IO.Path.GetFileNameWithoutExtension(item.FileName) + " - 1" + System.IO.Path.GetExtension(item.FileName);
                updateFileName(item);
            }
        }

        void item_OnPostResourceComplete(object sender, EventArgs e)
        {
            copier_OnComplete();
        }

        void item_OnFileResume(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(StartFileUpload, sender as FileItem);
        }

        public void StartUpload()
        {
            ThreadPool.QueueUserWorkItem(StartAsyncUpload);
        }

        private void StartAsyncUpload(object state)
        {
            var items = Items.Where(x => x.Progress != 100).ToList();
            if (items.Count > 0)
            {
                ResourceService service = new ResourceService(null, null);
                service.CreateSubBucket(FileUploadViewModel.BucketId, FileUploadViewModel.ApiKey, "/" + FileUploadViewModel.FileUploadPath.Substring(FileUploadViewModel.FileUploadPath.IndexOf("/")));
                var output = Parallel.ForEach(items, new ParallelOptions() { MaxDegreeOfParallelism = 3 }, (item) =>
                {
                    StartFileUpload(item);
                }).IsCompleted;
            }
        }

        private void StartFileUpload(object state)
        {
            var item = state as FileItem;
            item.IsCoping = true;
            var serverIP = ConfigurationManager.AppSettings["SANIp"].ToString();
            copier.Copy(item, "\\\\" + serverIP + "\\mmstemp\\" + item.DestinationPath + "\\" + item.FileName);
            if (item.IsSubmittedToMediaServer)
            {
                item.GenerateThumb();
            }
            item.IsCoping = false;
        }

        void copier_OnComplete()
        {
            if (Items.Where(x => x.Progress != 100).Count() == 0 && Items.Where(x => !x.IsSubmittedToMediaServer).Count() == 0)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    if (OnUploadCompleted != null)
                        OnUploadCompleted(this, null);
                }));
            }
        }

        void copier_OnProgressChanged(double Percentage, ref bool Cancel, long uploadedBytes, long totalBytes, FileItem item = null)
        {
            item.Progress = Math.Round((double)Percentage, 2);
            item.ProgressDisplay = item.Progress.ToString() + "%";
            item.UploadedProgress = GetSizeString(uploadedBytes) + " / " + GetSizeString(totalBytes);
            TotalProgress = (Items.Sum(x => x.Progress) / (Items.Count * 100)) * 100;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string GetSizeString(long length)
        {
            double size = Math.Round((double)(length / 1024) / 1024, 0);
            if (size >= 1024)
            {
                size = Math.Round((double)(size / 1024), 2);
                return size.ToString() + "GB";
            }
            if (size == 0) return Math.Round((double)(length / 1024), 0).ToString() + "KB";
            return size.ToString() + "MB";
        }
    }
}
