﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.ViewModel.Helper
{
    public delegate void ProgressChangeDelegate(double Percentage, ref bool Cancel, long uploadedBytes, long totalBytes, FileItem item = null);
    public delegate void Completedelegate();

    class CustomFileCopier
    {
        public void Copy(FileItem source, string destination)
        {
            try
            {
                var SourceFilePath = source.FilePath;
                var DestFilePath = destination;
                byte[] buffer = new byte[1024 * 1024]; // 1MB buffer
                bool cancelFlag = false;

                if (!File.Exists(SourceFilePath))
                {
                    throw new Exception("Source file does not exist");
                }
                
                using (FileStream _source = new FileStream(SourceFilePath, FileMode.Open, FileAccess.Read))
                {
                    long fileLength = _source.Length;
                    using (FileStream dest = new FileStream(DestFilePath, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        long totalBytes = (dest.Length == source.UploadedLength) ? source.UploadedLength : 0;
                        int currentBlockSize = 0;

                        _source.Position = totalBytes;
                        if (totalBytes > 0)
                            dest.Position = totalBytes;
                        while ((currentBlockSize = _source.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            totalBytes += currentBlockSize;
                            double persentage = (double)totalBytes * 100.0 / fileLength;


                            dest.Write(buffer, 0, currentBlockSize);
                            source.UploadedLength = totalBytes;

                            cancelFlag = false;
                            OnProgressChanged(persentage, ref cancelFlag, totalBytes, fileLength, source);

                            if (cancelFlag == true || source.IsPaused)
                            {
                                // Delete dest file here
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                source.Error = exp.Message;
            }
            OnComplete();
        }

        public void Copy(string source, string destination)
        {
            var SourceFilePath = source;
            var DestFilePath = destination;
            byte[] buffer = new byte[1024 * 1024]; // 1MB buffer
            bool cancelFlag = false;

            using (FileStream _source = new FileStream(SourceFilePath, FileMode.Open, FileAccess.Read))
            {
                long fileLength = _source.Length;
                using (FileStream dest = new FileStream(DestFilePath, FileMode.CreateNew, FileAccess.Write))
                {
                    long totalBytes = 0;
                    int currentBlockSize = 0;

                    while ((currentBlockSize = _source.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        totalBytes += currentBlockSize;
                        double persentage = (double)totalBytes * 100.0 / fileLength;

                        dest.Write(buffer, 0, currentBlockSize);

                        cancelFlag = false;
                        OnProgressChanged(persentage, ref cancelFlag, totalBytes, fileLength);

                        if (cancelFlag == true)
                        {
                            // Delete dest file here
                            break;
                        }
                    }
                }
            }

            OnComplete();
        }

        public event ProgressChangeDelegate OnProgressChanged;
        public event Completedelegate OnComplete;
    }
}
