﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.ViewModel
{
    public class Marker
    {
        public static double TotalWidth;
        public Marker(double x, string videoId)
        {
            X = X1 = x;
            VideoId = videoId;
            IsTransition = false;
        }
        public double X
        {
            get
            {
                return XPercent * TotalWidth;
            }
            set
            {
                XPercent = value / TotalWidth;
            }
        }
        public double XPercent { get; set; }
        public double X1
        {
            get
            {
                return X1Percent * TotalWidth;
            }
            set
            {
                X1Percent = value / TotalWidth;
            }
        }
        public double X1Percent { get; set; }
        public bool IsMarkedAsClip { get { return X != X1; } }
        public bool IsTransition { get; set; }
        public Uri TransitionImageUrl { get; set; }
        public string VideoId { get; set; }


        public FFMPEGLib.TransitionTypes TransitionType
        {
            get
            {
                if (TransitionImageUrl != null)
                {
                    if (TransitionImageUrl.OriginalString.Contains("fade"))
                        return FFMPEGLib.TransitionTypes.Fade;
                    if (TransitionImageUrl.OriginalString.Contains("slide"))
                        return FFMPEGLib.TransitionTypes.UnCoverLeft;
                }
                return FFMPEGLib.TransitionTypes.Fade;
            }
        }
    }
}
