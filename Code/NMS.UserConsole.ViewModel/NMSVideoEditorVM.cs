﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using FFMPEGLib;
using NMS.Core;
using NMS.UserConsole.ViewModel.Commands;

namespace NMS.UserConsole.ViewModel
{
    public class NMSVideoEditorVM : INotifyPropertyChanged
    {
        public static TimeSpan TimeLineSegmentDuration = TimeSpan.FromMilliseconds(500);
        public event NMS.UserConsole.ViewModel.Video.VideoSelectedHandler OnVideoSelected;
        public ObservableCollection<Video> Videos { get; set; }
        public TimeSpan Duration { get; set; }
        public Video SelectedVideo { get; set; }
        private bool _isBusy;
        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; OnPropertyChanged("IsBusy"); } }
        private string _busyMessage;
        public string BusyMessage { get { return _busyMessage; } set { _busyMessage = value; OnPropertyChanged("BusyMessage"); } }
        private double _totalWidth;
        ThreadingTasks threadingTasks;

        List<NMS.Core.Entities.Resource> resources = new List<Core.Entities.Resource>();

        public NMSVideoEditorVM(List<NMS.Core.Entities.Resource> _resources)
        {
            resources = _resources;
            Videos = new ObservableCollection<Video>();
            threadingTasks = new ThreadingTasks();
            threadingTasks.OnCompleted += threadingTasks_OnCompleted;
            Duration = TimeSpan.FromMinutes(new Random().Next(5, 10));
            PrepareSampleVideos();
        }

        void threadingTasks_OnCompleted(ThreadingTasks.ThreadingTaskNames taskName,object output)
        {
            switch (taskName)
            {
                case ThreadingTasks.ThreadingTaskNames.DownloadVideosAndCreateThumbs:
                    {
                        foreach (var video in (output as List<Video>))
                        {
                            video.OnVideoSelected += VideoEditorVM_OnVideoSelected;
                            Application.Current.Dispatcher.Invoke(new Action(() => { Videos.Add(video); }));
                        }
                        Application.Current.Dispatcher.Invoke(new Action(() => { VideoEditorVM_OnVideoSelected(Videos.First()); }));
                        break;
                    }
            }
            StopBusy();
        }

        public void StartBusy(string busyMessage)
        {
            IsBusy = true;
            BusyMessage = busyMessage;
        }

        public void StopBusy()
        {
            IsBusy = false;
        }

        private void PrepareSampleVideos()
        {
            StartBusy("Downloading Videos...");
            ThreadPool.QueueUserWorkItem(new WaitCallback(threadingTasks.DownloadVideosAndCreateThumbs), resources.Where(x => x.ResourceTypeId == (int)NMS.Core.Enums.ResourceTypes.Video).ToList());
        }

        public void VideoEditorVM_OnVideoSelected(Video video)
        {
            StartBusy("Loading Video...");
            if (SelectedVideo == null || SelectedVideo.Id != video.Id)
            {
                SelectedVideo = video;
                if (string.IsNullOrEmpty(SelectedVideo.VideoFrameSnapShotsUrl))
                {
                    string snapShotPath = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + SelectedVideo.Id + "/Snapshots/";
                    SelectedVideo.VideoFrameSnapShotsUrl = snapShotPath;

                    List<KeyValuePair<int, VideoThumb>> lst = new List<KeyValuePair<int, VideoThumb>>();
                    lst.AddRange(Directory.GetFiles(snapShotPath).Select(x => new KeyValuePair<int, VideoThumb>(Convert.ToInt32(Regex.Replace(x.Substring(x.LastIndexOf("/") + 1), "(snap)|(.png|.jpg|.gif)", "", RegexOptions.IgnoreCase)), new VideoThumb() { Url = x })));
                    foreach (var item in lst)
                    {
                        item.Value.Image = new BitmapImage(new Uri(item.Value.Url));
                        if (!SelectedVideo.VideoThumbs.ContainsKey(item.Key))
                            SelectedVideo.VideoThumbs.Add(item.Key, item.Value);
                    }
                }
                if (SelectedVideo.Duration.TotalSeconds == 0)
                {
                    SelectedVideo.Duration = TimeSpan.FromMilliseconds(FFMPEG.GetDurationInMilliSeconds(SelectedVideo.VideoUrl));
                }
                
                OnPropertyChanged("SelectedVideo");
                if (OnVideoSelected != null)
                    OnVideoSelected(SelectedVideo);
            }
            StopBusy();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                //ConsoleViewModel.notificationCount++;
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
