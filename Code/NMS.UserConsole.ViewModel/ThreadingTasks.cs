﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using FFMPEGLib;
using NMS.Core;
using NMS.UserConsole.View;
using NMS.Core.Helper;
using NMS.Service;
using Newtonsoft.Json;
using System.Configuration;

namespace NMS.UserConsole.ViewModel
{
    public class ThreadingTasks
    {
        public enum ThreadingTaskNames
        {
            DownloadVideosAndCreateThumbs, ClipMedia, CreatePreview, CreateMultipleClips,UploadVideo
        }
        public static int SnapshotImageCount = 200;
        private int subThreadTotalCount = -1;
        private int subThreadExecutedCount = 0;
        private List<object> subThreadResults = new List<object>();
        public delegate void OnCompletedHandler(ThreadingTaskNames task,object output);
        public event OnCompletedHandler OnCompleted;
        public void DownloadVideosAndCreateThumbs(object input)
        {
            subThreadResults = new List<object>();
            List<NMS.Core.Entities.Resource> resources = input as List<NMS.Core.Entities.Resource>;
            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Clips"))
            {
                Directory.Delete(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Clips", true);
            }
            
            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Clips");
            
            
            List<Video> videos = new List<Video>();
            subThreadTotalCount = resources.Count;
            for (int i = 0; i < resources.Count; i++)
            {
                try
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(CreateClip), resources[i]);
                }
                catch (Exception exp)
                {
                    File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/log.txt", Environment.NewLine + exp.Message + Environment.NewLine + exp.StackTrace);
                }
            }
        }

        private void CreateClip(object obj)
        {
            try
            {
                NMS.Core.Entities.Resource res = obj as NMS.Core.Entities.Resource;
                Video vid = new Video() { Id = res.Guid.ToString(), VideoUrl = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + res.Guid.ToString() + "." + AppSettings.VideoFormat };
                if (string.IsNullOrEmpty(res.FilePath))
                {
                    if (!File.Exists(vid.VideoUrl))
                    {
                        WebClient client = new WebClient();
                        client.DownloadFile(AppSettings.MediaServerUrl + "/getresource/" + res.Guid, vid.VideoUrl);
                        client.Dispose();
                    }
                }
                else
                {
                    vid.VideoUrl = res.FilePath;
                }
                vid.Duration = TimeSpan.FromMilliseconds(FFMPEG.GetDurationInMilliSeconds(vid.VideoUrl));
                if (vid.Duration.TotalSeconds > 0)
                {
                    FFMPEG.CreateSnapshots(vid.VideoUrl, 1 / (vid.Duration.TotalSeconds / SnapshotImageCount), AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + res.Guid.ToString() + "/Snapshots/");
                    vid.ThumbUrl = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + res.Guid.ToString() + "/Snapshots/snap1." + AppSettings.ImageFormat;
                    vid.VideoThumbs.Add(1, new VideoThumb() { Url = vid.ThumbUrl });
                    if (subThreadExecutionDone(vid))
                    {
                        if (OnCompleted != null)
                            OnCompleted(ThreadingTaskNames.DownloadVideosAndCreateThumbs, subThreadResults.Cast<Video>().ToList());
                    }
                }
                else
                {
                    subThreadTotalCount--;
                    if (subThreadExecutedCount >= subThreadTotalCount)
                        if (OnCompleted != null)
                            OnCompleted(ThreadingTaskNames.DownloadVideosAndCreateThumbs, subThreadResults.Cast<Video>().ToList());
                }
            }
            catch (Exception exp)
            {
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/log.txt", Environment.NewLine + exp.Message + Environment.NewLine + exp.StackTrace);
            }
           
        }

        private bool subThreadExecutionDone(object result)
        {
            subThreadResults.Add(result);
            subThreadExecutedCount++;
            return subThreadExecutedCount >= subThreadTotalCount;
        }

        public void ClipMedia(object input)
        {
            ClipMediaAgument argument = input as ClipMediaAgument;
            
            
            if (!File.Exists(argument.DestinationVideoUrl))
                FFMPEG.ClipVideo(argument.VideoUrl, argument.From, argument.To, argument.DestinationVideoUrl);

            if (OnCompleted != null)
                OnCompleted(ThreadingTaskNames.ClipMedia, input);
        }

        public void CreateMultipleClips(object input)
        {
            PreviewClipAgument argument = input as PreviewClipAgument;
            var clips = argument.Markers;
            List<VideoEdits> videoEdits = new List<VideoEdits>();
            List<string> clipUrls = new List<string>();
            for (int i = 0; i < clips.Count; i++)
            {
                if (clips[i].IsTransition)
                {
                    Video video1 = argument.Videos.Where(x => x.Id == clips[i - 1].VideoId).FirstOrDefault();
                    Video video2 = argument.Videos.Where(x => x.Id == clips[i + 1].VideoId).FirstOrDefault();
                    TimeSpan from1 = TimeSpan.FromMilliseconds(clips[i - 1].XPercent * video1.Duration.TotalMilliseconds);
                    TimeSpan to1 = TimeSpan.FromMilliseconds(clips[i - 1].X1Percent * video1.Duration.TotalMilliseconds);
                    TimeSpan from2 = TimeSpan.FromMilliseconds(clips[i + 1].XPercent * video2.Duration.TotalMilliseconds);
                    TimeSpan to2 = TimeSpan.FromMilliseconds(clips[i + 1].X1Percent * video2.Duration.TotalMilliseconds);
                    string transitionUrl = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/transition-" + i.ToString() + ".ts";
                    clipUrls.AddRange(FFMPEGLib.FFMPEG.BlendVideos(video1.VideoUrl, from1, to1, from2, to2, video2.VideoUrl, transitionUrl, to1 - from1, argument.TransitionDuration, clips[i].TransitionType, "ts"));
                    i++;
                }
                else
                {
                    if (clips.Count() == i + 1 || (clips.Count > i && !clips[i + 1].IsTransition))
                    {
                        Video video = argument.Videos.Where(x => x.Id == clips[i].VideoId).FirstOrDefault();
                        clipUrls.Add(CreateVideoFromMarker(clips[i], video));
                        videoEdits.Add(new VideoEdits() { VideoGuid = video.Id, SequenceNumber = i + 1, From = TimeSpan.FromMilliseconds(clips[i].XPercent * video.Duration.TotalMilliseconds).TotalSeconds, To = TimeSpan.FromMilliseconds(clips[i].X1Percent * video.Duration.TotalMilliseconds).TotalSeconds });
                    }
                }
            }
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/");

            string output = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/" + Guid.NewGuid().ToString() + "." + AppSettings.VideoFormat;
            if (clipUrls.Count == 1)
                File.Move(clipUrls[0], output);
            else
                FFMPEGLib.FFMPEG.ConcatenateVideosForPreviewWithEncoding(clipUrls, output);

            if (OnCompleted != null)
                OnCompleted(ThreadingTaskNames.CreateMultipleClips, new Video() { VideoUrl = output });
        }

        public void CreatePreview(object input)
        {
            List<VideoEdits> videoEdits = new List<VideoEdits>();
            PreviewClipAgument argument = input as PreviewClipAgument;
            var clips = argument.Markers;
            List<string> clipUrls = new List<string>();
            for (int i = 0; i < clips.Count; i++)
            {
                if (clips[i].IsTransition)
                {
                    Video video1 = argument.Videos.Where(x => x.Id == clips[i - 1].VideoId).FirstOrDefault();
                    Video video2 = argument.Videos.Where(x => x.Id == clips[i + 1].VideoId).FirstOrDefault();
                    TimeSpan from1 = TimeSpan.FromMilliseconds(clips[i - 1].XPercent * video1.Duration.TotalMilliseconds);
                    TimeSpan to1 = TimeSpan.FromMilliseconds(clips[i - 1].X1Percent * video1.Duration.TotalMilliseconds);
                    TimeSpan from2 = TimeSpan.FromMilliseconds(clips[i + 1].XPercent * video2.Duration.TotalMilliseconds);
                    TimeSpan to2 = TimeSpan.FromMilliseconds(clips[i + 1].X1Percent * video2.Duration.TotalMilliseconds);
                    string transitionUrl = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/transition-" + i.ToString() + ".ts";
                    clipUrls.AddRange(FFMPEGLib.FFMPEG.BlendVideos(video1.VideoUrl, from1, to1, from2, to2, video2.VideoUrl, transitionUrl, to1 - from1, argument.TransitionDuration, clips[i].TransitionType, "ts",true));
                    i++;
                }
                else
                {
                    if (clips.Count() == i + 1 || (clips.Count > i && !clips[i + 1].IsTransition))
                    {
                        Video video = argument.Videos.Where(x => x.Id == clips[i].VideoId).FirstOrDefault();
                        clipUrls.Add(CreateVideoFromMarker(clips[i], video));
                        videoEdits.Add(new VideoEdits() { VideoGuid = video.Id, SequenceNumber = i + 1, From = TimeSpan.FromMilliseconds(clips[i].XPercent * video.Duration.TotalMilliseconds).TotalSeconds, To = TimeSpan.FromMilliseconds(clips[i].X1Percent * video.Duration.TotalMilliseconds).TotalSeconds });
                    }
                }
            }
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/");

            string output = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/" + Guid.NewGuid().ToString() + "." + AppSettings.VideoFormat;
            if (clipUrls.Count == 1)
                File.Move(clipUrls[0], output);
            else
                FFMPEGLib.FFMPEG.ConcatenateVideosForPreviewWithEncoding(clipUrls, output);
                Video preview = new Video();
                preview.VideoUrl = output;
                string thumbname = Guid.NewGuid().ToString() + "." + AppSettings.ImageFormat;
                FFMPEG.CreateThumb(preview.VideoUrl, thumbname, AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/");
                preview.ThumbUrl = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/preview/" + thumbname;
                preview.Duration = TimeSpan.FromMilliseconds(FFMPEGLib.FFMPEG.GetDurationInMilliSeconds(preview.VideoUrl));

                if (OnCompleted != null)
                    OnCompleted(ThreadingTaskNames.CreatePreview, new object[] { preview, videoEdits });
        }

        private string CreateVideoFromMarker(Marker marker, Video sourceVideo, bool enableEncoding = false)
        {
            TimeSpan from = TimeSpan.FromMilliseconds(marker.XPercent * sourceVideo.Duration.TotalMilliseconds);
            TimeSpan to = TimeSpan.FromMilliseconds(marker.X1Percent * sourceVideo.Duration.TotalMilliseconds);
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/" + AppSettings.ClipsFolderPath))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/" + AppSettings.ClipsFolderPath);
            string VideoUrl = AppDomain.CurrentDomain.BaseDirectory + "/" + AppSettings.ClipsFolderPath + "/" + marker.VideoId + "-" + marker.XPercent + "-" + marker.X1Percent + Convert.ToInt32(enableEncoding) + ".ts";
            if (!File.Exists(VideoUrl))
                FFMPEGLib.FFMPEG.ClipVideoWith420pix(sourceVideo.VideoUrl, from, to, VideoUrl, enableEncoding);
            return VideoUrl;
        }

        public void UploadVideo(object input)
        {
            UploadVideoAgument inputArgs = input as UploadVideoAgument;
            string str = "editorClosed('" + inputArgs.PreviewUrl.Replace(System.AppDomain.CurrentDomain.BaseDirectory.Trim('\\'), "") + "','" + inputArgs.previewThumbUrl.Replace(System.AppDomain.CurrentDomain.BaseDirectory.Trim('\\'), "") + "'," + inputArgs.SlotTemplateScreenElementId + "," + inputArgs.objectState + ")";
            UploadArgs arg = new UploadArgs();
            arg.FilePath = inputArgs.PreviewUrl;
            arg.SlotTemplateScreenElementId = inputArgs.SlotTemplateScreenElementId;
            arg.VideoClip = new VideoClipInput();
            arg.VideoClip.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"];
            arg.VideoClip.BucketId = Convert.ToInt32(ConfigurationManager.AppSettings["BucketId"]);
            arg.VideoClip.VidEdits = inputArgs.VideoEdits;

            UploadFile(arg, inputArgs.Token, inputArgs.UserID, inputArgs.BuckerId, inputArgs.ApiKey, inputArgs.objectState, inputArgs.Caption, inputArgs.Duration);
            HttpWebRequestHelper webHelper = new HttpWebRequestHelper();
            NMS.Core.DataTransfer.Script.PostInput script = new Core.DataTransfer.Script.PostInput();
            script.Script = "hideCutterLoader(true)";
            script.Token = inputArgs.Token;
            script.UserId = inputArgs.UserID.ToString();
            webHelper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.Script.PostOutput>, NMS.Core.DataTransfer.Script.PostInput>(AppSettings.WebAppUrl + "api/Program/InsertVideoCutterScript", script, null);

            Thread.Sleep(5000);
            if (OnCompleted != null)
                OnCompleted(ThreadingTaskNames.UploadVideo, null);
        }

        void UploadFile(object _arg, string token, int UserID,int bucketId,string apiKey, string objectState,string caption,double duration)
        {
            try
            {
                UploadArgs arg = _arg as UploadArgs;
                ResourceService resourceService = new ResourceService(null, null);
                Guid guid = resourceService.UploadFile(File.ReadAllBytes(arg.FilePath), arg.FilePath, (int)NMS.Core.Enums.ResourceTypes.Video, "video/mpeg", false, bucketId, apiKey, caption, duration);
                HttpWebRequestHelper webHelper = new HttpWebRequestHelper();
                webHelper.GetRequest<NMS.Core.DataTransfer.DataTransfer<bool>>(AppSettings.WebAppUrl + "api/program/updatescreenelementguid?id=" + arg.SlotTemplateScreenElementId + "&guid=" + guid.ToString(), null);
                arg.VideoClip.Guid = guid.ToString();
                string str = JsonConvert.SerializeObject(arg.VideoClip);
                MS.MediaIntegration.API.MSApi msApi = new MS.MediaIntegration.API.MSApi();
                MS.Core.Models.VideoClipInput input = new MS.Core.Models.VideoClipInput();
                input.CopyFrom(arg.VideoClip);
                msApi.ClipAndMergeVideos(input);

                NMS.Core.DataTransfer.Script.PostInput script = new Core.DataTransfer.Script.PostInput();
                script.Script = "uploadCompleted('" + guid.ToString() + "'," + arg.SlotTemplateScreenElementId + "," + UserID + "," + objectState + "," + duration + ")";
                script.Token = token;
                script.UserId = UserID.ToString();
                webHelper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.Script.PostOutput>, NMS.Core.DataTransfer.Script.PostInput>(AppSettings.WebAppUrl + "api/Program/InsertVideoCutterScript", script, null);
            }
            catch (Exception exp)
            {

            }
        }

        public class UploadArgs
        {
            public int SlotTemplateScreenElementId { get; set; }
            public string FilePath { get; set; }

            public VideoClipInput VideoClip { get; set; }
        }

        public class ClipMediaAgument
        {
            public string VideoUrl { get; set; }
            public TimeSpan From { get; set; }
            public TimeSpan To { get; set; }
            public string DestinationVideoUrl { get; set; }
        }

        public class UploadVideoAgument
        {
            public string Token { get; set; }
            public int UserID { get; set; }
            public string PreviewUrl { get; set; }
            public string previewThumbUrl { get; set; }
            public List<VideoEdits> VideoEdits { get; set; }
            public int SlotTemplateScreenElementId { get; set; }

            public string objectState { get; set; }

            public int BuckerId { get; set; }

            public string ApiKey { get; set; }

            public string Caption { get; set; }

            public double Duration { get; set; }
        }

        public class PreviewClipAgument
        {
            public List<Marker> Markers { get; set; }
            public List<Video> Videos { get; set; }
            public TimeSpan TransitionDuration { get; set; }
        }
    }
}
