﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.ViewModel
{
    public class TimeLineSegment : INotifyPropertyChanged
    {
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
        public TimeSpan SourceFrom { get; set; }
        public TimeSpan SourceTo { get; set; }
        public string VideoUrl { get; set; }
        public TimeSpan TotalDuration { get; set; }
        private double _width;
        public double Width { get { return _width; } set { _width = value; OnPropertyChanged("Width"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                //ConsoleViewModel.notificationCount++;
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
