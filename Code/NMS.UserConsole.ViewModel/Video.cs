﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using NMS.Core;
using NMS.UserConsole.ViewModel.Commands;
using System.IO;

namespace NMS.UserConsole.ViewModel
{
    public class VideoThumb
    {
        public string Url { get; set; }
        public BitmapImage Image { get; set; }
    }
    public class Video : INotifyPropertyChanged
    {
        public delegate void VideoSelectedHandler(Video video);
        public event VideoSelectedHandler OnVideoSelected;
        public string ThumbUrl { get; set; }
        public string VideoUrl { get; set; }
        public string Id { get; set; }
        public TimeSpan Duration { get; set; }
        public string DurationStr { get { return NMS.Core.Helper.FormatHelper.FormatTimeSpan(Duration); } }
        public string VideoFrameThumbsUrl { get; set; }
        public string VideoFrameSnapShotsUrl { get; set; }
        public double Size { get; set; }
        public Dictionary<int, VideoThumb> VideoThumbs { get; set; }
        public int VideoThumbCounts { get; set; }
        public ICommand SelectVideoCommand { get; set; }

        public Video()
        {
            SelectVideoCommand = new RelayCommand(ExecuteSelectVideo);
            VideoThumbs = new Dictionary<int, VideoThumb>();
        }

        private void ExecuteSelectVideo(object obj)
        {
            if (obj != null)
                OnVideoSelected(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                //ConsoleViewModel.notificationCount++;
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private List<int> frameSnapshotsInPending = new List<int>();
        public System.Windows.Media.ImageSource GetVideoFrameThumb(double p)
        {
            try
            {
                int frame = 1;
                if (p > 0)
                    frame = Convert.ToInt32(p * ThreadingTasks.SnapshotImageCount);
                if (VideoThumbs.ContainsKey(frame))
                {
                    var thumb = VideoThumbs[frame];
                    if (thumb.Image == null)
                        thumb.Image = new BitmapImage(new Uri(thumb.Url));
                    return VideoThumbs[frame].Image;
                }
                else
                {
                    if (!frameSnapshotsInPending.Contains(frame))
                    {
                        frameSnapshotsInPending.Add(frame);
                        ThreadPool.QueueUserWorkItem(new WaitCallback(SomeLongTask), p);
                    }
                    return null;
                }
            }
            catch (Exception exp)
            {
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/log.txt", Environment.NewLine + exp.Message + Environment.NewLine + exp.StackTrace);
                return null;
            }
        }

        private void SomeLongTask(Object state)
        {
            double p = Convert.ToDouble(state);
            int frame = 1;
            if (p > 0)
                frame = Convert.ToInt32(p * ThreadingTasks.SnapshotImageCount);
            FFMPEGLib.FFMPEG.CreateSnapshot(this.VideoUrl, TimeSpan.FromMilliseconds(p * this.Duration.TotalMilliseconds), this.VideoFrameSnapShotsUrl, "snap" + frame + ".png");
            lock (frameSnapshotsInPending)
            {
                frameSnapshotsInPending.Remove(frame);
            }
            VideoThumbs.Add(frame, new VideoThumb() { Url = this.VideoFrameSnapShotsUrl + "/snap" + frame + ".png" });
        }
    }
}
