﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using NMS.UserConsole.ViewModel.Commands;

namespace NMS.UserConsole.ViewModel
{
    public class VideoClip : INotifyPropertyChanged
    {
       // public delegate void VideoSelectedHandler(Video video);
       // public event VideoSelectedHandler OnVideoSelected;
        public string ThumbUrl { get; set; }
        public string VideoUrl { get; set; }
        public string VideoId { get; set; }
        public TimeSpan Duration { get { return (To - From); } }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }

        public VideoClip()
        {
          //  SelectVideoCommand = new RelayCommand(ExecuteSelectVideo);
        }

        //private void ExecuteSelectVideo(object obj)
        //{
        //    if (obj != null)
        //        OnVideoSelected(this);
        //}

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                //ConsoleViewModel.notificationCount++;
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
