﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using FFMPEGLib;
using NMS.Core;
using NMS.UserConsole.ViewModel.Commands;

namespace NMS.UserConsole.ViewModel
{
    public class VideoEditorVM : INotifyPropertyChanged
    {
        public static TimeSpan TimeLineSegmentDuration = TimeSpan.FromMilliseconds(500);
        public event NMS.UserConsole.ViewModel.Video.VideoSelectedHandler OnVideoSelected;
        public List<Video> Videos { get; set; }
        public List<Video> VideoClips { get; set; }
        public TimeSpan Duration { get; set; }
        public Video SelectedVideo { get; set; }
        private double _totalWidth;

        public VideoEditorVM()
        {
            Videos = PrepareSampleVideos();
            Duration = TimeSpan.FromMinutes(new Random().Next(5, 10));
            VideoClips = new List<Video>();
        }
        private List<Video> PrepareSampleVideos()
        {
            List<Video> videos = new List<Video>();
            for (int i = 1; i < new Random().Next(5, 10); i++)
            {
                videos.Add(new Video() { Id = i.ToString(), VideoUrl = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + i.ToString() + "." + AppSettings.VideoFormat, ThumbUrl = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Images/" + i.ToString() + "." + AppSettings.ImageFormat});
                videos[i-1].OnVideoSelected += VideoEditorVM_OnVideoSelected;
            }
            return videos;
        }

        void VideoEditorVM_OnVideoSelected(Video video)
        {
            if (SelectedVideo == null || SelectedVideo.Id != video.Id)
            {
                SelectedVideo = video;
                //if (string.IsNullOrEmpty(SelectedVideo.VideoFrameThumbsUrl))
                //{
                    string thumbPath =AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + SelectedVideo.Id + "/Thumbs/";
                //    string snapShotPath = AppDomain.CurrentDomain.BaseDirectory + AppSettings.MediaFolderPath + "/Videos/" + SelectedVideo.Id + "/Snapshots/";
                //    FFMPEG.CreateThumbs(SelectedVideo.VideoUrl, 1, thumbPath);
                //    FFMPEG.CreateSnapshots(SelectedVideo.VideoUrl, 1, snapShotPath);
                //    SelectedVideo.VideoFrameThumbsUrl = thumbPath;
                //    SelectedVideo.VideoFrameSnapShotsUrl = snapShotPath;
                    List<KeyValuePair<int, VideoThumb>> lst = new List<KeyValuePair<int, VideoThumb>>();
                    lst.AddRange(Directory.GetFiles(thumbPath).Select(x => new KeyValuePair<int, VideoThumb>(Convert.ToInt32(Regex.Replace(x.Substring(x.LastIndexOf("/") + 1), "(snap)|(.png|.jpg|.gif)", "", RegexOptions.IgnoreCase)), new VideoThumb() { Url = x })));
                    foreach (var item in lst)
                    {
                        SelectedVideo.VideoThumbs.Add(item.Key, item.Value);
                    }
                //}
                if (SelectedVideo.Duration.TotalSeconds == 0)
                {
                    SelectedVideo.Duration = TimeSpan.FromMilliseconds(FFMPEG.GetDurationInMilliSeconds(SelectedVideo.VideoUrl));
                }
                OnPropertyChanged("SelectedVideo");
                if (OnVideoSelected != null)
                    OnVideoSelected(SelectedVideo);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                //ConsoleViewModel.notificationCount++;
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
