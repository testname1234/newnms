﻿using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using RazorEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;

namespace NMS.UserConsole.API
{
    public class BundlesController : ApiController
    {
        //public HttpResponseMessage Get()
        //{
        //    string bundlePath = "~/bundles/reporters/common/";
        //    var bundle = BundleTable.Bundles.GetBundleFor(bundlePath);
        //    var context = new BundleContext(new System.Web.HttpContextWrapper(new OwinContext()), BundleTable.Bundles, bundlePath);
        //    var response = bundle.GenerateBundleResponse(context);
        //    return new HttpResponseMessage()
        //    {
        //        Content = new StringContent(response.Content, Encoding.UTF8, "text/javascript"),
        //    };
        //    throw new NotImplementedException();
        //}


        public HttpResponseMessage Get()
        {
            string b = string.Empty;
            bool abc = false;
            string path = string.Empty;
            string Url = Request.RequestUri.ToString();
            string a = Url.Remove(0, Url.LastIndexOf("bundles") - 1);
            b = a.Replace("/bundles/", "");
           // string c = b.Replace("/", "");
            var response = new HttpResponseMessage(HttpStatusCode.OK);
          //  if (c.Contains("reporterteambcommon"))
          //  {
          //      c = c.Replace("reporterteambcommon", "common");
         //       abc = true;
         //   }
           // if (c.Contains("reportercommon"))
         //   {
          //      c = c.Replace("reportercommon", "common");
          //      abc = true;
          //  }
         //   if (abc)
         //   path = "D:\\Work\\BOL\\trunk\\Code\\NMS\\NMS.UserConsole" + a.Replace('/','\\') +c;

           // else
                path = "D:\\Work\\BOL\\trunk\\Code\\NMS\\NMS.UserConsole" + a.Replace('/', '\\');


            string viewpath = (path);
            var template = File.ReadAllText(viewpath);
            response.Content = new StringContent(template);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/javascript");
            return response;
        }
    }
}
