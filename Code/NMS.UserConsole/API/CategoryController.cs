﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NMS.Core.IController;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using Microsoft.Owin;
using System.Net.Http;

namespace NMS.UserConsole.API
{
    public class CategoryController : ApiController , ICategoryController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }
        
        [HttpGet]
        [ActionName("GetCategoryByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>> GetCategoryByTerm(string id = null)
        {
            try
            {
                if (id == null)
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>>>(AppSettings.WebAppUrl + "api/category/GetCategoryByTerm", null);
                else
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>>>(AppSettings.WebAppUrl + "api/category/GetCategoryByTerm?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }

        }
    }
}
