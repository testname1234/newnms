﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NMS.Core.IController;
using Microsoft.Owin;
using System.Net.Http;

namespace NMS.UserConsole.API
{
    public class CelebrityController : ApiController , ICelebrityController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();


        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }

        [HttpGet]
        [ActionName("GetCelebrityByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>> GetCelebrityByTerm(string term)
        {
            try
            {
                if (term == null || term == string.Empty)
                return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>>>(AppSettings.WebAppUrl + "api/Celebrity/GetCelebrityByTerm",null);

                else
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>>>(AppSettings.WebAppUrl + "api/Celebrity/GetCelebrityByTerm?term="+term, null);   
                
            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

    }

}
