﻿using NMS.Core;
using NMS.Core.IController;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NMS.Core.DataTransfer.ChannelVideo;
using Microsoft.Owin;
using System.Net.Http;


namespace NMS.UserConsole.API
{
    public class ChannelController : ApiController , IChannelController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();


        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }

        [HttpGet]
        [ActionName("LoadChannelInitialData")]
        public DataTransfer<LoadChannelOutput> LoadChannelInitialData(string id)
        {
            try
            {
                if (id == null || id == string.Empty)
                    return requestHelper.GetRequest<DataTransfer<LoadChannelOutput>>(AppSettings.WebAppUrl + "api/Channel/LoadChannelInitialData", null);

                else
                    return requestHelper.GetRequest<DataTransfer<LoadChannelOutput>>(AppSettings.WebAppUrl + "api/Channel/LoadChannelInitialData?id="+id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<LoadChannelOutput> output = new DataTransfer<LoadChannelOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("RefreshData")]
        public DataTransfer<LoadChannelOutput> RefreshData(string id)
        {
            try
            {
                if (id == null || id == string.Empty)
                    return requestHelper.GetRequest<DataTransfer<LoadChannelOutput>>(AppSettings.WebAppUrl + "api/Channel/RefreshData", null);

                else
                    return requestHelper.GetRequest<DataTransfer<LoadChannelOutput>>(AppSettings.WebAppUrl + "api/Channel/RefreshData?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<LoadChannelOutput> output = new DataTransfer<LoadChannelOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("GetChannelVideos")]
        public DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>> GetChannelVideos(GetChannelVideoInput input)
        {
            try
            {
                if (input == null)
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>>>(AppSettings.WebAppUrl + "api/Channel/GetChannelVideos", null);

                else
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>>>(AppSettings.WebAppUrl + "api/Channel/GetChannelVideos?input=" + input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("GetAll")]
        public DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>> GetAll()
        {
            try
            {
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>>>(AppSettings.WebAppUrl + "api/Channel/GetAll", null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("GetAllPrograms")]
        public DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> GetAllPrograms()
        {
            try
            {
                return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>>>(AppSettings.WebAppUrl + "api/Channel/GetAllPrograms", null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

    }
}
