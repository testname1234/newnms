﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using NMS.Core.IController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using System.Net.Http;


namespace NMS.UserConsole.API
{
    public class FilterController : ApiController, IFilterController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }


        [HttpGet]
        [ActionName("GetAllFilters")]
        public DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>> GetAllFilters()
        {
            try
            {
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>>>(AppSettings.WebAppUrl + "api/Filter/GetAllFilters", null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("GetAllFilterTypes")]
        public DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>> GetAllFilterTypes()
        {
            try
            {
                return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>>>(AppSettings.WebAppUrl + "api/Filter/GetAllFilterTypes", null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }
    }
}
