﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.UserConsole.Repository.Media;

namespace NMS.UserConsole.API
{
    public class ImageController : ApiController
    {
        public HttpResponseMessage Get(string imageId)
        {
            DateTime dt = DateTime.UtcNow;
            IImageRepository imageRepo = new ImageRepository();
            byte[] resourceByteArray = imageRepo.GetImage(imageId);

            MemoryStream dataStream = new MemoryStream(resourceByteArray);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(dataStream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            Console.WriteLine("Image Request Time: {0}ms", (DateTime.UtcNow - dt).TotalMilliseconds);
            return response;
        }
    }
}
