﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using NMS.Core;
using NMS.Core.IService;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.IController;
using NMS.Core.Models;
using System.Linq;
using System.Web.Http.Cors;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using System.Net.Http;
using System.Net;
using System.Web;
using Microsoft.Owin;
using NMS.Core.DataTransfer.News;


namespace NMS.UserConsole.API
{
    public class NewsController : ApiController, INewsController
    {
        #region Producer

        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }

        public NewsController()
        {
            
         
        }
       

        [HttpPost]
        [ActionName("LoadInitialData")]
        public DataTransfer<LoadInitialDataOutput> LoadInitialData(LoadInitialDataInput input)
        {
          
            try
            {
                return requestHelper.PostRequest<DataTransfer<LoadInitialDataOutput>, LoadInitialDataInput>(AppSettings.WebAppUrl + "api/News/LoadInitialData", input, null);
                
            }
            catch (Exception exp)
            {
                DataTransfer<LoadInitialDataOutput> output = new DataTransfer<LoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


        [HttpPost]
        [ActionName("ProducerPolling")]
        public DataTransfer<LoadInitialDataOutput> ProducerPolling(LoadInitialDataInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<LoadInitialDataOutput>, LoadInitialDataInput>(AppSettings.WebAppUrl + "api/News/ProducerPolling", input, null);
            }
            catch (Exception exp)
            {
                DataTransfer<LoadInitialDataOutput> output = new DataTransfer<LoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }

        }


        [HttpPost]
        [ActionName("GetMoreNews")]
        public DataTransfer<LoadInitialDataOutput> GetMoreNews(LoadInitialDataInput input)
        {
            try
            {
                //DateTime dt = DateTime.UtcNow;
                var res =  requestHelper.PostRequest<DataTransfer<LoadInitialDataOutput>, LoadInitialDataInput>(AppSettings.WebAppUrl + "api/News/GetMoreNews", input, null);
                //Console.WriteLine(DateTime.UtcNow.Subtract(dt).TotalSeconds + " - " + input.Term + " - " + res.Data.News != null ? res.Data.News.Count : 0);
                return res;
            }
            catch (Exception exp)
            {
                DataTransfer<LoadInitialDataOutput> output = new DataTransfer<LoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }

        }

        [HttpPost]
        [ActionName("UpdateThumbId")]
        public HttpResponseMessage UpdateThumbId(string id, string resguid)
        {
            throw new NotImplementedException();
        }


        [HttpPost]
        [ActionName("GetBunch")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetBunch(LoadInitialDataInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>, LoadInitialDataInput>(AppSettings.WebAppUrl + "api/News/GetBunch", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.News.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


#endregion

#region Reporter

        [HttpGet]
        [ActionName("GetNews")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> GetNews(string id)
        {
            try
            {
                if (id == null || id == string.Empty)
                    return requestHelper.GetRequest<DataTransfer<Core.DataTransfer.News.GetOutput>>(AppSettings.WebAppUrl + "api/News/GetNews", null);

                else
                    return requestHelper.GetRequest<DataTransfer<Core.DataTransfer.News.GetOutput>>(AppSettings.WebAppUrl + "api/News/GetNews?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<Core.DataTransfer.News.GetOutput> output = new DataTransfer<Core.DataTransfer.News.GetOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


        [HttpGet]
        [ActionName("GetNewsByTerm")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsByTerm(string id)
        {
            try
            {
                if (id == null || id == string.Empty)
                    return requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>>(AppSettings.WebAppUrl + "api/News/GetNewsByTerm", null);

                else
                    return requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>>(AppSettings.WebAppUrl + "api/News/GetNewsByTerm?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.News.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


        [HttpGet]
        [ActionName("GetAllMyNews")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetAllMyNews(int rId, int pageCount, int startIndex)
        {
            try
            {
                if (rId == null || pageCount == null || startIndex == null)
                    return requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>>(AppSettings.WebAppUrl + "api/News/GetAllMyNews", null);

                else
                    return requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>>(AppSettings.WebAppUrl + "api/News/GetAllMyNews?rId="+rId+"&pageCount="+pageCount+"&startIndex="+startIndex, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.News.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


        [HttpGet]
        [ActionName("ReporterLoadInitialData")]
        public DataTransfer<LoadInitialDataOutput> ReporterLoadInitialData()
        {
            try
            {
                return requestHelper.GetRequest<DataTransfer<LoadInitialDataOutput>>(AppSettings.WebAppUrl + "api/News/ReporterLoadInitialData", null);

            }
            catch (Exception exp)
            {
                DataTransfer<LoadInitialDataOutput> output = new DataTransfer<LoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("ReporterPolling")]
        public DataTransfer<LoadInitialDataOutput> ReporterPolling(LoadInitialDataInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<LoadInitialDataOutput>, LoadInitialDataInput>(AppSettings.WebAppUrl + "api/News/ReporterPolling", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<LoadInitialDataOutput> output = new DataTransfer<LoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }



        [HttpPost]
        [ActionName("Add")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> AddNews(NMS.Core.DataTransfer.News.PostInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<Core.DataTransfer.News.GetOutput>, NMS.Core.DataTransfer.News.PostInput>(AppSettings.WebAppUrl + "api/News/Add", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<Core.DataTransfer.News.GetOutput> output = new DataTransfer<Core.DataTransfer.News.GetOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


#endregion

#region Verification

        [HttpPost]
        [ActionName("InsertComment")]
        public DataTransfer<Core.DataTransfer.Comment.PostOutput> InsertComment(Core.DataTransfer.Comment.PostInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<Core.DataTransfer.Comment.PostOutput>, Core.DataTransfer.Comment.PostInput>(AppSettings.WebAppUrl + "api/News/InsertComment", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<Core.DataTransfer.Comment.PostOutput> output = new DataTransfer<Core.DataTransfer.Comment.PostOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("VerificationLoadInitialData")]
        public DataTransfer<LoadInitialDataOutput> VerificationLoadInitialData(LoadInitialDataInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<LoadInitialDataOutput>, LoadInitialDataInput>(AppSettings.WebAppUrl + "api/News/VerificationLoadInitialData", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<LoadInitialDataOutput> output = new DataTransfer<LoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }

        }


        [HttpPost]
        [ActionName("MarkVerifyNews")]
        public HttpResponseMessage MarkVerifyNews(MarkVerifyNewsInput input)
        {
            try
            {
                HttpWebResponse response = requestHelper.PostRequest(AppSettings.WebAppUrl + "api/News/MarkVerifyNews", input, null);
               return new HttpResponseMessage(response.StatusCode);
            }
            catch (Exception exp)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [ActionName("GetNewsWithUpdates")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsWithUpdatesAndRelated(string id, string bunchId)
        {
            try
            {
                if (id == null || bunchId == null || id == string.Empty || bunchId == string.Empty)
                    return requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>>(AppSettings.WebAppUrl + "api/News/GetNewsWithUpdates", null);

                else
                    return requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>>(AppSettings.WebAppUrl + "api/News/GetNewsWithUpdates?Id=" + id + "&bunchId=" + bunchId, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.News.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }

        }


#endregion

        [HttpPost]
        [ActionName("GetNewsByIDs")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsByIDs(NewsIdsModel model)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<List<Core.DataTransfer.News.GetOutput>>, NewsIdsModel>(AppSettings.WebAppUrl + "api/News/GetNewsByIDs", model, null);
            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.News.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("GetBunchWithCount")]
        public DataTransfer<GetNewsCustomOutput> GetBunchWithCount(LoadInitialDataInput input)
        {            
            try
            {
                return requestHelper.PostRequest<DataTransfer<GetNewsCustomOutput>, LoadInitialDataInput>(AppSettings.WebAppUrl + "api/News/GetBunchWithCount", input, null);
            }
            catch (Exception exp)
            {
                DataTransfer<GetNewsCustomOutput> output = new DataTransfer<GetNewsCustomOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }          
        }

    }
}