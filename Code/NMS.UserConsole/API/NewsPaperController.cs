﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using NMS.Core.IController;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using System.Net.Http;


namespace NMS.UserConsole.API
{
    public class NewsPaperController : ApiController, INewsPaperController
    {

       HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

      

       protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
       {
           base.Initialize(controllerContext);
           if (Request != null)
           {
               requestHelper.Request = Request.GetOwinContext().Request;
           }
       }


       [HttpGet]
       [ActionName("LoadInitialData")]
       public DataTransfer<LoadNewsPaperOutput> LoadInitialData(string fromDate, string toDate)
       {
           try
           {
               if (fromDate == null || fromDate == string.Empty || toDate == null || toDate == string.Empty)
                   return requestHelper.GetRequest<DataTransfer<LoadNewsPaperOutput>>(AppSettings.WebAppUrl + "api/News/LoadInitialData", null);

               else
                   return requestHelper.GetRequest<DataTransfer<LoadNewsPaperOutput>>(AppSettings.WebAppUrl + "api/News/LoadInitialData?fromDate=" + fromDate + "&toDate=" +toDate, null);

           }
           catch (Exception exp)
           {
               DataTransfer<LoadNewsPaperOutput> output = new DataTransfer<LoadNewsPaperOutput>();
               output.IsSuccess = false;
               output.Errors = new string[] { exp.Message };
               return output;
           }

       }

       [HttpGet]
       [ActionName("RefreshData")]
       public DataTransfer<LoadNewsPaperOutput> RefreshData(string fromDate, string toDate)
       {
           try
           {
               if (fromDate == null || fromDate == string.Empty || toDate == null || toDate == string.Empty)
                   return requestHelper.GetRequest<DataTransfer<LoadNewsPaperOutput>>(AppSettings.WebAppUrl + "api/News/RefreshData", null);

               else
                   return requestHelper.GetRequest<DataTransfer<LoadNewsPaperOutput>>(AppSettings.WebAppUrl + "api/News/RefreshData?fromDate=" + fromDate + "&toDate=" + toDate, null);

           }
           catch (Exception exp)
           {
               DataTransfer<LoadNewsPaperOutput> output = new DataTransfer<LoadNewsPaperOutput>();
               output.IsSuccess = false;
               output.Errors = new string[] { exp.Message };
               return output;
           }
       }



    }
}
