﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using NMS.Core.IController;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using System.Net.Http;


namespace NMS.UserConsole.API
{
    public class NotificationController: ApiController, INotificationController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }

        [HttpGet]
        [ActionName("MarkAsRead")]
        public Core.DataTransfer.DataTransfer<string> MarkAsRead(int nid, int uid)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                transfer = requestHelper.GetRequest<DataTransfer<string>>(AppSettings.WebAppUrl + "api/Notification/MarkAsRead?nid=" + nid.ToString() + "&uid=" + uid.ToString(), null);
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }
            return transfer;
        }
    }
}
