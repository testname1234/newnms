﻿using System;
using NMS.Core.IController;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NMS.Core.DataTransfer;
using NMS.Core.Models;
using NMS.Core;
using NMS.Core.Helper;
using NMS.Core.DataTransfer.Program;
using NMS.Core.DataTransfer.SlotScreenTemplate.POST;
using NMS.Service;
using NMS.Core.Entities;
using NMS.Core.IService;
using Microsoft.Owin;
using System.Net.Http;

namespace NMS.UserConsole.API
{
    public class ProgramController : ApiController , IProgramController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }

        [HttpPost]
        [ActionName("GetProgramEpisode")]
        public DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> GetProgramEpisode(GetProgramEpisodeInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>, GetProgramEpisodeInput>(AppSettings.WebAppUrl + "api/Program/GetProgramEpisode", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("GetProgramScreenFlow")]
        public DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput> GetProgramScreenFlow(string programId, string Producerid)
        {
            try
            {
                if (programId == null || Producerid == string.Empty || programId == string.Empty || Producerid == null)
                    return requestHelper.GetRequest<DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput>>(AppSettings.WebAppUrl + "api/program/GetProgramScreenFlow", null);

                else
                    return requestHelper.GetRequest<DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput>>(AppSettings.WebAppUrl + "api/program/GetProgramScreenFlow?programId=" + programId + "&Producerid="+Producerid, null);

            }
            catch (Exception exp)
            {
                DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput> output = new DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("GenerateScreenTemplateImage")]
        public DataTransfer<string> GenerateScreenTemplateImage(NMS.Core.DataTransfer.ScreenTemplate.PostInput input)
        {
            try
            {
                //return requestHelper.PostRequest<DataTransfer<string>, NMS.Core.DataTransfer.ScreenTemplate.PostInput>(AppSettings.WebAppUrl + "api/Program/GenerateScreenTemplateImage", input, null);
                DataTransfer<string> transfer = new DataTransfer<string>();
                ProgramService programService = new ProgramService(null, null, null, null, null, null, null, null,null,null,null,null,null);
                transfer.Data = programService.GenerateScreenTemplateImage(input);
                return transfer;
            }
            catch (Exception exp)
            {
                DataTransfer<string> output = new DataTransfer<string>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("PostNewsEpisode")]
        public DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>> PostNewsEpisode(NewsSequenceInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>>, NewsSequenceInput>(AppSettings.WebAppUrl + "api/Program/PostNewsEpisode", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }

        }

        [HttpPost]
        [ActionName("SaveSlotScreenTemplateFlow")]
        public DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> SaveSlotScreenTemplateFlow(SlotScreenTemplateFlow input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>>, SlotScreenTemplateFlow>(AppSettings.WebAppUrl + "api/Program/SaveSlotScreenTemplateFlow", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }

        }


        [HttpPost]  //Adeel
        [ActionName("InsertUpdateSlots")]
        public DataTransfer<List<Core.DataTransfer.Slot.GetOutput>> InsertUpdateSlots(Core.DataTransfer.Slot.InputSlots input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<List<Core.DataTransfer.Slot.GetOutput>>, Core.DataTransfer.Slot.InputSlots>(AppSettings.WebAppUrl + "api/Program/InsertUpdateSlots", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.Slot.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.Slot.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]  //Adeel
        [ActionName("DeleteSlot")]
        public DataTransfer<bool> DeleteSlot(int id)
        {
            try
            {
                return requestHelper.GetRequest<DataTransfer<bool>>(AppSettings.WebAppUrl + "api/Program/DeleteSlot?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<bool> output = new DataTransfer<bool>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]  //Adeel
        [ActionName("DeleteSlotScreenTemplate")]
        public DataTransfer<bool> DeleteSlotScreenTemplate(int id)
        {
            try
            {
                return requestHelper.GetRequest<DataTransfer<bool>>(AppSettings.WebAppUrl + "api/Program/DeleteSlotScreenTemplate?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<bool> output = new DataTransfer<bool>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("SaveSlotScreenElements")]
        public DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>> SaveSlotScreenTemplateFlow(Core.DataTransfer.SlotScreenTemplate.GetOutput input)
        {
            try
            {
                if (input.TemplateScreenElementImageUrl.ToLower().Contains("mediaresources"))
                {
                    ResourceService resourceService = new ResourceService(null, null);
                    string url = input.TemplateScreenElementImageUrl;
                    input.TemplateScreenElementImageGuid = resourceService.UploadImageOnMS(AppDomain.CurrentDomain.BaseDirectory + input.TemplateScreenElementImageUrl).ToString();
                }
                return requestHelper.PostRequest<DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>>, Core.DataTransfer.SlotScreenTemplate.GetOutput>(AppSettings.WebAppUrl + "api/Program/SaveSlotScreenElements", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("UpdateScreenElementGuid")]
        public DataTransfer<bool> UpdateScreenElementGuid(string id, string guid)
        {
            try
            {
                return requestHelper.GetRequest<DataTransfer<bool>>(AppSettings.WebAppUrl + "api/Program/UpdateScreenElementGuid?id=" + id + "&guid=" + guid, null);

            }
            catch (Exception exp)
            {
                DataTransfer<bool> output = new DataTransfer<bool>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("GetTemplateMicAndCamera")]
        public DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>> GetTemplateMicAndCamera(int id)
        {
            try
            {
                return requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>>>(AppSettings.WebAppUrl + "api/Program/GetTemplateMicAndCamera?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>> output = new DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("NLELoadInitialData")]
        public DataTransfer<NLELoadInitialDataOutput> NLELoadInitialData(NLELoadInitialDataInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<NLELoadInitialDataOutput>, NLELoadInitialDataInput>(AppSettings.WebAppUrl + "api/Program/NLELoadInitialData", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<NLELoadInitialDataOutput> output = new DataTransfer<NLELoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("NLEPolling")]
        public DataTransfer<NLELoadInitialDataOutput> NLEPolling(NLELoadInitialDataInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<NLELoadInitialDataOutput>, NLELoadInitialDataInput>(AppSettings.WebAppUrl + "api/Program/NLEPolling", input, null);

            }
            catch (Exception exp)
            {
                DataTransfer<NLELoadInitialDataOutput> output = new DataTransfer<NLELoadInitialDataOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpPost]
        [ActionName("NLESaveSlotScreenElements")]
        public DataTransfer<List<SlotScreenTemplate>> NLESaveSlotScreenElements(SaveScreenTemplateInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<List<SlotScreenTemplate>>, SaveScreenTemplateInput>(AppSettings.WebAppUrl + "api/Program/NLESaveSlotScreenElements", input, null);
            }
            catch (Exception exp)
            {
                DataTransfer<List<SlotScreenTemplate>> output = new DataTransfer<List<SlotScreenTemplate>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }

        [HttpGet]
        [ActionName("GetProgramEpisodeByRundownId")]
        public DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> GetProgramEpisode(int id)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Episode.GetOutput>>();
            try
            {
                return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>>(AppSettings.WebAppUrl + "api/Program/GetProgramEpisodeByRundownId/" + id, null);
            }
            catch(Exception e)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = e.Message;
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetProgram")]
        public DataTransfer<NMS.Core.DataTransfer.Program.GetOutput> GetProgram(string id)
        {
            DataTransfer<NMS.Core.DataTransfer.Program.GetOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.Program.GetOutput>();
            try
            {
                return requestHelper.GetRequest<DataTransfer<NMS.Core.DataTransfer.Program.GetOutput>>(AppSettings.WebAppUrl + "api/Program/GetProgram/" + id, null);
            }
            catch (Exception e)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = e.Message;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("SendMessage")]
        public DataTransfer<Core.DataTransfer.Message.PostOutput> SendMessage(Core.DataTransfer.Message.PostInput input)
        {
            DataTransfer<Core.DataTransfer.Message.PostOutput> transfer = new DataTransfer<Core.DataTransfer.Message.PostOutput>();
            try
            {
                transfer = requestHelper.PostRequest<DataTransfer<Core.DataTransfer.Message.PostOutput>>(AppSettings.WebAppUrl + "api/Program/SendMessage", input, null);
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetSlotScreenTemplateComments")]
        public DataTransfer<List<Core.DataTransfer.Message.GetOutput>> GetSlotScreenTemplateComments(int id)
        {
            DataTransfer<List<Core.DataTransfer.Message.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Message.GetOutput>>();
            try
            {
                transfer = requestHelper.GetRequest<DataTransfer<List<Core.DataTransfer.Message.GetOutput>>>(AppSettings.WebAppUrl + "api/Program/GetSlotScreenTemplateComments/" + id.ToString(), null);
            }
            catch (Exception e)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = e.Message;
            }
            return transfer;
        }


        public DataTransfer<bool> DeleteSlot(int id, string newsGuid, int episodeId)
        {
            throw new NotImplementedException();
        }


        public DataTransfer<bool> DeleteSlotScreenTemplate(int id, NewsStatisticsInput NewsStatisticsParam)
        {
            throw new NotImplementedException();
        }


        public DataTransfer<bool> DeleteSlot(int id, string newsGuid, int episodeId, int NewsBucketId)
        {
            throw new NotImplementedException();
        }
    }
}
