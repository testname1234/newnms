﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using NMS.Core.IController;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using System.Net.Http;


namespace NMS.UserConsole.API
{
    public class RadioController : ApiController, IRadioController
    {
       HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

       protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
       {
           base.Initialize(controllerContext);
           if (Request != null)
           {
               requestHelper.Request = Request.GetOwinContext().Request;
           }
       }

       [HttpGet]
       [ActionName("LoadRadioInitialData")]
       public DataTransfer<LoadRadioOutput> LoadRadioInitialData(string fromDate, string toDate)
       {
           try
           {
               if (fromDate == null || toDate == string.Empty || fromDate == string.Empty || toDate == null)
                   return requestHelper.GetRequest<DataTransfer<LoadRadioOutput>>(AppSettings.WebAppUrl + "api/Radio/LoadRadioInitialData", null);

               else
                   return requestHelper.GetRequest<DataTransfer<LoadRadioOutput>>(AppSettings.WebAppUrl + "api/Radio/LoadRadioInitialData?fromDate=" + fromDate + "&toDate=" + toDate, null);

           }
           catch (Exception exp)
           {
               DataTransfer<LoadRadioOutput> output = new DataTransfer<LoadRadioOutput>();
               output.IsSuccess = false;
               output.Errors = new string[] { exp.Message };
               return output;
           }

       }


       [HttpGet]
       [ActionName("RefreshData")]
       public DataTransfer<LoadRadioOutput> RefreshData(string fromDate, string toDate)
       {
           try
           {
               if (fromDate == null || toDate == string.Empty || fromDate == string.Empty || toDate == null)
                   return requestHelper.GetRequest<DataTransfer<LoadRadioOutput>>(AppSettings.WebAppUrl + "api/Radio/RefreshData", null);

               else
                   return requestHelper.GetRequest<DataTransfer<LoadRadioOutput>>(AppSettings.WebAppUrl + "api/Radio/RefreshData?fromDate=" + fromDate + "&toDate=" + toDate, null);

           }
           catch (Exception exp)
           {
               DataTransfer<LoadRadioOutput> output = new DataTransfer<LoadRadioOutput>();
               output.IsSuccess = false;
               output.Errors = new string[] { exp.Message };
               return output;
           }

       }

    }
}
