﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.Helper;
using NMS.Core.IController;
using NMS.Core.IService;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using System.Net.Http;


namespace NMS.UserConsole.API
{
    public class ResourceController : ApiController, IResourceController
    {
        //IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
        //IChannelVideoService channelVideoService = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
        //INewsPaperPageService newsPaperPageService = IoC.Resolve<INewsPaperPageService>("NewsPaperPageService");
        //IRadioStreamService radioStreamService = IoC.Resolve<IRadioStreamService>("RadioStreamService");
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }


        [HttpGet]
        [ActionName("GetResourceIds")]
        public DataTransfer<List<NMS.Core.DataTransfer.Resource.PostOutput>> GetResourceIds(string id, int? bucketId, string apiKey, string sourceTypeId, string sourceTypeName, string sourceId, string Source = "Reporter")
        {

            try
            {
                return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Resource.PostOutput>>>(AppSettings.WebAppUrl + "api/resource/GetResourceIds/" + id, null);
            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Resource.PostOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Resource.PostOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


        #region Delete

        [ActionName("DeleteResource")]
        public HttpResponseMessage DeleteResource(string Id)
        {
            try
            {
                return requestHelper.DeleteRequest<HttpResponseMessage>(AppSettings.WebAppUrl + "api/resource/DeleteResource/" + Id, null, null);
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        [ActionName("Delete")]
        public HttpResponseMessage Delete(string Id)
        {
            try
            {
                return requestHelper.DeleteRequest<HttpResponseMessage>(AppSettings.WebAppUrl + "api/resource/Delete/" + Id, null, null);
            }
            catch (Exception exp)
            {
                return null;
            }
        }


        #endregion

        [HttpGet]
        [ActionName("MarkIsProcessed")]
        public HttpResponseMessage MarkIsProcessed(int id, int typeId)
        {
            try
            {
                return requestHelper.GetRequest<HttpResponseMessage>(AppSettings.WebAppUrl + "api/resource/MarkIsProcessed?id=" + id + "&typeId=" + typeId, null);
            }
            catch (Exception exp)
            {
                return null;
            }

        }

        [HttpPost]
        [ActionName("SearchResource")]
        public MS.Core.DataTransfer.Resource.ResourceOutput SearchResource(string term, int pageSize, int pageNumber, int? resourceTypeId, int bucketId, string alldata)
        //public MS.Core.DataTransfer.Resource.ResourceOutput SearchResource(string term, int pageSize, int pageNumber, int resourceTypeId)
        {
            MS.Core.DataTransfer.Resource.ResourceOutput transfer = new MS.Core.DataTransfer.Resource.ResourceOutput();
            try
            {
                if (resourceTypeId != 0)
                    transfer = requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/searchresource?term=" + term + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&resourceTypeId=" + resourceTypeId + "&bucketId=" + bucketId + "&alldata=" + alldata, null);
                else
                    transfer = requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/searchresource?term=" + term + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&bucketId=" + bucketId + "&alldata=" + alldata, null);
            }
            catch (Exception exp)
            {

            }
            return transfer;
        }

        [HttpPost]
        [ActionName("CroppImage")]
        public DataTransfer<string> CroppImage(CroppedResource input)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                transfer = requestHelper.PostRequest<DataTransfer<string>, CroppedResource>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/CropImage", input, null);
            }
            catch (Exception exp)
            {
                transfer = new DataTransfer<string>();
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }
            return transfer;
        }

     
    }
}
