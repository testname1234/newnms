﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web;
using System.IO;
using RazorEngine;
using System.Net.Http.Headers;
using System.Web.Optimization;
using System.Configuration;

namespace NMS.UserConsole.API
{
    public class StaticController : ApiController
    {      
        [HttpGet]
        [ActionName("Index")]
        public HttpResponseMessage Index(string resource)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewpath = string.Empty;
            resource = resource.ToLower();

            viewpath = (ConfigurationManager.AppSettings["downloadplace"] + resource + ".cshtml");
            if (!File.Exists(viewpath))
            {
                viewpath = null;
            }

            if (!string.IsNullOrEmpty(viewpath))
            {
                var template = File.ReadAllText(viewpath);
                string ParsedView = Razor.Parse(template);
                response.Content = new StringContent(ParsedView);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            }

            return response;

        }


    }
}
