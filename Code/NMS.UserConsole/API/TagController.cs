﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Helper;
using NMS.Core.IController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using System.Net.Http;


namespace NMS.UserConsole.API
{
    public class TagController : ApiController, ITagController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();

        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }

        [HttpGet]
        [ActionName("GetTagByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>> GetTagByTerm(string id = null)
        {
            try
            {
                if (id == null || id == string.Empty)
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>>>(AppSettings.WebAppUrl + "api/Tag/GetTagByTerm", null);

                else
                    return requestHelper.GetRequest<DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>>>(AppSettings.WebAppUrl + "api/Tag/GetTagByTerm?id=" + id, null);

            }
            catch (Exception exp)
            {
                DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>> output = new DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }
    }
}
