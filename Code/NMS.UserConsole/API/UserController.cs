﻿using NMS.Core.Helper;
using NMS.Core.DataTransfer;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NMS.Core.IController;
using NMS.Core;
using NMS.Core.Models;
using Microsoft.Owin;
using System.Net.Http;



namespace NMS.UserConsole.API
{
    public class UserController : ApiController, IUserController
    {
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            if (Request != null)
            {
                requestHelper.Request = Request.GetOwinContext().Request;
            }
        }

        [HttpPost]
        [ActionName("Login")]
        public DataTransfer<Core.DataTransfer.User.PostOutput> Login(Core.DataTransfer.User.PostInput input)
        {
            try
            {
                return requestHelper.PostRequest<DataTransfer<Core.DataTransfer.User.PostOutput>, Core.DataTransfer.User.PostInput>(AppSettings.WebAppUrl + "api/user/login", input, null);
            }
            catch (Exception exp)
            {
                DataTransfer<Core.DataTransfer.User.PostOutput> output = new DataTransfer<Core.DataTransfer.User.PostOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


        [HttpPost]
        [ActionName("LoginMMS")]
        public DataTransfer<MMSLoadInitialDataLoginOutput> LoginMMS(NMS.Core.DataTransfer.User.PostInput input)
        {

            try
            {
                MMSLoadInitialDataLoginInput mmsInput = new MMSLoadInitialDataLoginInput();
                mmsInput.LoginId = input.Login;
                mmsInput.Password = input.Password;
                var dt = requestHelper.PostRequest<DataTransfer<MMSLoadInitialDataLoginOutput>, MMSLoadInitialDataLoginInput>(AppSettings.MMSUsermanagementUrl + "api/admin/Authenticate", mmsInput, null);
                return dt;
            }
            catch (Exception exp)
            {
                DataTransfer<MMSLoadInitialDataLoginOutput> output = new DataTransfer<MMSLoadInitialDataLoginOutput>();
                output.IsSuccess = false;
                output.Errors = new string[] { exp.Message };
                return output;
            }
        }


    }
}
