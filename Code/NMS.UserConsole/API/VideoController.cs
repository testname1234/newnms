﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NMS.Core.DataInterfaces;
using NMS.UserConsole.API;
using NMS.UserConsole.Repository.Media;



namespace NMS.UserConsole.API
{
    public class VideoController : ApiController
    {
        public HttpResponseMessage Get(string videoId)
        {
            DateTime dt = DateTime.UtcNow;
            IVideoRepository videoRepo = new VideoRepository();
            byte[] resourceByteArray = videoRepo.GetVideo(videoId);

            MemoryStream dataStream = new MemoryStream(resourceByteArray);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(dataStream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("video/mp4");
            Console.WriteLine("Video Request Time: {0}ms", (DateTime.UtcNow - dt).TotalMilliseconds);
            return response;
        }
    }
}
