﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace NMS.UserConsole.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/reporters/common/")
                .IncludeDirectory("~/Scripts/app/reporters/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/fieldreporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team A/fieldreporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/publicreporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team A/publicreporter", "*.js", searchSubdirectories: false));

            //bundles.Add(new ScriptBundle("~/bundles/reporter/teama/common/")
            //    .IncludeDirectory("~/Scripts/app/reporters/team A/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/channelreporter/")
              .IncludeDirectory("~/Scripts/app/reporters/team B/channelreporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/NewspaperReporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team B/NewspaperReporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/radioreporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team B/radioreporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/reporter/teamb/common/")
                .IncludeDirectory("~/Scripts/app/reporters/team B/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/producer/")
                .IncludeDirectory("~/Scripts/app/producer", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/Verification/")
                .IncludeDirectory("~/Scripts/app/Verification/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/FieldReporterVerification/")
                .IncludeDirectory("~/Scripts/app/Verification/FieldReporterVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/ChannelReporterVerification/")
                .IncludeDirectory("~/Scripts/app/Verification/ChannelReporterVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/RadioReporterVerification/")
               .IncludeDirectory("~/Scripts/app/Verification/RadioReporterVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/NewspaperVerification/")
            .IncludeDirectory("~/Scripts/app/Verification/NewspaperVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/models/")
                .IncludeDirectory("~/Scripts/app/models/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/managers/")
                .IncludeDirectory("~/Scripts/app/managers/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/common/")
                .IncludeDirectory("~/Scripts/app/common/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/mock/")
                .IncludeDirectory("~/Scripts/app/mock/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/controls/")
                .IncludeDirectory("~/Scripts/app/controls/", "*.js", searchSubdirectories: false));
        }

    }
}
