﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using Microsoft.Owin;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.Http;
using NMS.UserConsole.App_Start;
using System.Web;

namespace NMS.UserConsole
{
    public class WebAPIConfig
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            
            HttpConfiguration config = new HttpConfiguration();

            config.EnableCors();
            
           
            config.Routes.IgnoreRoute("resource", "{resource}.axd/{*pathInfo}");
    
            config.Routes.MapHttpRoute(
                name: "ControllerApi",
                routeTemplate: "api/{controller}",
                defaults: new { id = RouteParameter.Optional }
            );
            
            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ControllerAndAction",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );


            config.Routes.MapHttpRoute(
                name: "",
                routeTemplate: "{resource}/{*pathInfo}",
                defaults: new { controller = "static", Action = "Index", resource = @"\w+" }

                );


           // config.Routes.MapHttpRoute(
           //    name: "DefaultApi",
           //    routeTemplate: "api/{controller}/{id}",
           //    defaults: new { controller = "static", Action = "Index" }
           //);
            //config.Formatters.Remove(config.Formatters.XmlFormatter);
            //var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            //config.Formatters.Add(json);
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            
            appBuilder.UseFileServer(new FileServerOptions()
            {
                RequestPath = new PathString("/content"),
                EnableDirectoryBrowsing = true,
            });

            appBuilder.UseFileServer(new FileServerOptions()
            {
                RequestPath = new PathString("/scripts"),
                EnableDirectoryBrowsing = true,
            });

            appBuilder.UseFileServer(new FileServerOptions()
            {
                RequestPath = new PathString("/bundles"),
                EnableDirectoryBrowsing = true,
            });

            appBuilder.UseFileServer(new FileServerOptions()
            {
                RequestPath = new PathString("/tmpl"),
                EnableDirectoryBrowsing = true,
            });

            appBuilder.UseFileServer(new FileServerOptions()
            {
                RequestPath = new PathString("/media"),
                EnableDirectoryBrowsing = true,
            });

            appBuilder.UseFileServer(new FileServerOptions()
            {
                RequestPath = new PathString("/mediaresources"),
                EnableDirectoryBrowsing = true,
            });

            appBuilder.UseFileServer(new FileServerOptions()
            {
                RequestPath = new PathString("/images"),
                EnableDirectoryBrowsing = true,
            });

            appBuilder.UseWebApi(config);

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
        }
    }
}
