﻿using System;
using System.Collections.Generic;

namespace NMS.UserConsole.Entities
{
    public class InputSettings
    {
        public string MosId { get; set; }
        public int MosPort  { get; set; }
        public string NcsId { get; set; }
        public int NcsPort { get; set; }
        public string HubIp { get; set; }
       
    }
}
