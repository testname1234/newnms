﻿using System;
using System.Collections.Generic;

namespace NMS.UserConsole.Entities
{
    public class PostInput
    {
        public string RoId { get; set; }
        public string StoryId { get; set; }
        public string ItemId { get; set; }
        public string Path { get; set; }
        public string Port { get; set; }
        public string MosId { get; set; }
        public string MosPort { get; set; }
        public string NcsId { get; set; }
        public string NcsPort { get; set; }
        public string HubIp { get; set; }
        public int    ThreadDelayTime { get; set; }
        public double DeletionSpan { get; set; }
        public List<int> UpdatedSequence { get; set; }
        public string ProfileName { get; set; }
        public double Step { get; set; }
        public int SelectedFontSize { get; set; }
        public int SelectedFontStyleId { get; set; }
        public string ActionToMap { get; set; }
        public bool PlayPuaseToggle { get; set; }
        public int Tolerance { get; set; }
        public int SettingIdA { get; set; }
        public int SettingIdB { get; set; }
        public int SettingIdC { get; set; }
        public int SettingIdD { get; set; }
        public int SettingIdE { get; set; }
        
    }
}
