﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.Entities
{

    public class Profile
    {
        public int ProfileId{get; set;}
        public string Name	{get; set;}
        public int FontfamilyId	{get; set;}
        public double FontSize	{get; set;}
        public double ScrollSpeed{get; set;}
    }
}
