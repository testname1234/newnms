﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.Enums
{
    public enum ActionPerfomed
    {
        SKIP =1,
    	UNSKIP=5,
	    DELETE=3,
	    SEQUENCE_CHANGE=4,
        STARTED_CLEANUP=6,
    	STOPPED_CLEANUP=7
    }
}
