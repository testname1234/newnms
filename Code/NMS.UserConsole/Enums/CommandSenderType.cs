﻿using System;


namespace NMS.UserConsole.Enums
{
    public enum CommandSenderType
    {
        PcrUser=1,
        Teleprompter = 2,
        CasperClient = 3
    }
}
