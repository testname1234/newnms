﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.Enums
{
            public enum CommandType
            {
                //prompter
                GetAllRunDowns_Prompter = 1,
                PlayPauseToggle_Prompter = 2,
                //Pause_Prompter = 3,
                Skip_Prompter = 4,
                UnSkip_Prompter = 5,
                StartListening_Prompter = 6,
                StopListening_Prompter = 7,
                PromptON_Prompter = 8,
                PromptOFF_Prompter = 9,
                FlipText_Prompter = 10,
                JumpToStory_Prompter = 11,
                Delete_Prompter = 12,
                CurrentSelectedRunOrder_Prompter = 13,
                UpdateSequence_Prompter = 23,
                ConnectHub_Prompter = 24,
                EditText_Prompter = 25,
                EditText_Save = 26,
                StartCleanUp_Prompter=28,
                StopCleanUp_Prompter = 29,
                SetSpeed_Prompter = 30,
                CurrentFontSize_Prompter = 31,
                CurrentFontFamily_Prompter = 32,
                PreviewON_Prompter= 35,
                PreviewOFF_Prompter= 36,
                ResetCurrentRunOrder_Prompter = 37,
                MappingModeOn_Prompter = 38,
                MappingModeOFF_Prompter=39,
                MapActionToKey_Prompter = 40,
                NextStory_Prompter=41,
                PreviousStory_Prompter=42,
                DeleteProfile_Prompter=43,
                LoadToPrompter_Prompter=44,
                Tolerance_Prompter=45,
                BrowseXML_Prompter = 46,

                //casper
                Play_Caspar = 14,
                ClearGraphic_Caspar = 15,
                Connect_Caspar = 16,
                DisConnect_Caspar = 17,
                ListenRunOrder_Caspar = 18,
                LoadRunOrder_Caspar = 19,
                PlayItemWise_Caspar = 20,
                LoadInitialRunOrders_Caspar = 21,
                CurrentSelectedItem_Caspar=27,
                SetProfile_Prompter=33,
                NewProfile_Prompter=34,


                //pcrUser
                Notify_PcrUser = 22,


                // latest 45
            }

            public enum RunOrderType
            {
                New = 1,
                XmlRead = 2,
            }
}
