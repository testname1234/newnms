﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.Enums
{
    public enum ConfigurationSetting
    {
            MosId= 9,
            MosPort= 10,
            NcsId= 11,
            NcsPort= 12,
            IpAdress= 13,
            Tolerance= 14,
            AutoCheck= 15,
            AutoDelete= 16,
    }
}
