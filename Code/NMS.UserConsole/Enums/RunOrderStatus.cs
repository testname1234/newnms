﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMS.UserConsole.Enums
{
    public enum RunOrderStatus
    {
        NEW=1,
        OLD=2,
        UPDATED=3
    }
}
