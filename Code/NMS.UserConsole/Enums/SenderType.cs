﻿using System;


namespace NMS.UserConsole.Enums
{
    public enum SenderType
    {
        PcrUser=1,
        Teleprompter = 2,
        CasperClient = 3
    }
}
