﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BolUserConsole.Core.Entities;
using BolUserConsole.Repository;

namespace NMS.UserConsole
{
    public static class ExceptionLogger
    {
        public static void LogMessage(string message)
        {
            ExceptionLogRepository MessageToLog = new ExceptionLogRepository();
            ExceptionLog ExceptionToInsert = new ExceptionLog();
            ExceptionToInsert.Message = message;
            ExceptionToInsert.CreationDate = DateTime.UtcNow;
            MessageToLog.InsertExceptionLog(ExceptionToInsert);
        }

    }
}
