﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using NMS.Core;
using NMS.UserConsole.View;
using NMS.UserConsole.ViewModel;
using System.Configuration;
using System.IO;
using System.Net;
using Ionic.Zip;
using System.Reflection;
using System.ComponentModel;
using NMS.Core.Helper;
using System.Collections.Specialized;
using NMS.Service;
using System.Threading;
using Awesomium.Core;
using NMS.UserConsole.Entities;
using System.Windows.Interop;

namespace NMS.UserConsole
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NMSVideoEditor editor;
        //PrompterPreview preview;
        public string objectState;
        public bool popUpOpened = false;
        public MainWindow()
        {
            InitializeComponent();
            wbHost.ProcessCreated += wbHost_ProcessCreated;
            this.Closing += MainWindow_Closing;
            this.Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Title = this.Title + " - " + version;
            WebClient client = new WebClient();
            client.DownloadStringAsync(new Uri(AppSettings.UserConsoleApiUrl + "login#/index"));
            wbHost.Source = new Uri(AppSettings.UserConsoleApiUrl + "login#/index");
            this.WindowState = WindowState.Maximized;
        }

        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (editor != null && editor.IsInitialized)
            {
                editor.Close();
            }
            if (Directory.Exists(Directory.GetCurrentDirectory() + AppSettings.MediaFolderPath + "/Videos/preview/"))
                Directory.Delete(Directory.GetCurrentDirectory() + AppSettings.MediaFolderPath + "/Videos/preview/", true);

        }

        void wbHost_ProcessCreated(object sender, WebViewEventArgs e)
        {
            JSObject jsObject = wbHost.CreateGlobalJavascriptObject("app");
            jsObject.Bind("ShowVideoEditor", false, new JavascriptMethodEventHandler(ShowVideoEditor));
            jsObject.Bind("CheckForUpdate", false, new JavascriptMethodEventHandler(checkForUpdate));
            jsObject.Bind("OpenVideo", false, new JavascriptMethodEventHandler(openVideo));
        }

        void UploadFile(object _arg)
        {
            try
            {
                UploadArgs arg = _arg as UploadArgs;
                ResourceService resourceService = new ResourceService(null, null);
                Guid guid = resourceService.UploadFile(File.ReadAllBytes(arg.FilePath), arg.FilePath, (int)NMS.Core.Enums.ResourceTypes.Video, "video/mpeg");
                HttpWebRequestHelper webHelper = new HttpWebRequestHelper();
                webHelper.GetRequest(AppSettings.WebAppUrl + "api/program/updatescreenelementguid?id=" + arg.SlotTemplateScreenElementId + "&guid=" + guid.ToString(), null);
                arg.VideoClip.Guid = guid.ToString();
                string str = JsonConvert.SerializeObject(arg.VideoClip);
                MS.MediaIntegration.API.MSApi msApi = new MS.MediaIntegration.API.MSApi();
                MS.Core.Models.VideoClipInput input = new MS.Core.Models.VideoClipInput();
                input.CopyFrom(arg.VideoClip);
                msApi.ClipAndMergeVideos(input);
                Dispatcher.Invoke(() =>
                {
                    wbHost.ExecuteJavascript("uploadCompleted('" + guid.ToString() + "'," + arg.SlotTemplateScreenElementId + ",'" + objectState + "')");
                });
            }
            catch (Exception exp)
            {
 
            }
        }

        public void checkForUpdate(object obj, JavascriptMethodEventArgs jsMethodArgs)
        {
            var version = CheckForUpdate();
            if (version > 0)
                wbHost.Source = new Uri(AppSettings.UserConsoleApiUrl);
        }

        public static double version = 0.0;
        public static double CheckForUpdate()
        {
            double maxValue = 0;
            try
            {
                WebClient client = new WebClient();
                string str = client.DownloadString(ConfigurationManager.AppSettings["filepathurl"]);

                var match = System.Text.RegularExpressions.Regex.Match(str, "\">(.*?(zip).*?)<");
                string[] files = new string[] { match.Value.Replace("\"", "").Replace(">", "").Replace("<", "") };
                //string filepath = ConfigurationManager.AppSettings["filepath"];
                //string[] files = System.IO.Directory.GetFiles(filepath, "*.zip");
                List<int> Lstfoldernames = new List<int>();
                string filename = string.Empty;
               
                if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "/version.txt"))
                    maxValue = Convert.ToDouble(File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/version.txt"));
                foreach (var file in files)
                {
                    double currentValue = Convert.ToDouble(System.IO.Path.GetFileNameWithoutExtension(file));
                    if (maxValue < currentValue)
                    {
                        maxValue = currentValue;
                        filename = file;
                    }
                }

                if (!string.IsNullOrEmpty(filename))
                {
                    WebClient web = new WebClient();
                    web.DownloadFile(ConfigurationManager.AppSettings["filepathurl"]+"/"+filename,filename);


                    using (ZipFile zipFile = new ZipFile(System.IO.Path.GetFileName(filename)))
                    {
                        zipFile.ExtractAll(System.AppDomain.CurrentDomain.BaseDirectory, ExtractExistingFileAction.OverwriteSilently);
                    }
                    File.WriteAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/version.txt", maxValue.ToString());
                    version = maxValue;
                    return maxValue;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not get update Error: " + ex.Message);
            }
            version = maxValue;
            return maxValue;
        }

        public static string prevJson = string.Empty;

        public void ShowVideoEditor(object obj, JavascriptMethodEventArgs jsMethodArgs)
        {
            string resourcsJson = jsMethodArgs.Arguments[0];
            string mediaServerUrl = jsMethodArgs.Arguments[1];
            string slotTemplateScreenElementId = jsMethodArgs.Arguments[2];
            objectState = jsMethodArgs.Arguments[3];
            if (editor != null && editor.IsInitialized && prevJson == resourcsJson)
            {
                editor.Show();
            }
            else
            {
                prevJson = resourcsJson;
                AppSettings._mediaServerUrlOverride = mediaServerUrl;
                editor = new NMSVideoEditor(Convert.ToInt32(slotTemplateScreenElementId));
                editor.Closed += editor_Closed;
                List<NMS.Core.Entities.Resource> resources = JsonConvert.DeserializeObject<List<NMS.Core.Entities.Resource>>(resourcsJson);
                NMSVideoEditorVM editorVM = new NMSVideoEditorVM(resources);
                editor.DataContext = editorVM;
                editor.Show();
            }
        }

        void editor_Closed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty((sender as NMSVideoEditor).previewUrl))
            {
                wbHost.ExecuteJavascript("editorClosed('" + (sender as NMSVideoEditor).previewUrl.Replace(System.AppDomain.CurrentDomain.BaseDirectory.Trim('\\'), "") + "','" + (sender as NMSVideoEditor).previewThumbUrl.Replace(System.AppDomain.CurrentDomain.BaseDirectory.Trim('\\'), "") + "'," + (sender as NMSVideoEditor).SlotTemplateScreenElementId + ",'" + objectState + "')");
                UploadArgs arg = new UploadArgs();
                arg.FilePath = (sender as NMSVideoEditor).previewUrl;
                arg.SlotTemplateScreenElementId = (sender as NMSVideoEditor).SlotTemplateScreenElementId;
                arg.VideoClip = new VideoClipInput();
                arg.VideoClip.ApiKey = ConfigurationManager.AppSettings["MediaServerAPIKey"];
                arg.VideoClip.BucketId = Convert.ToInt32(ConfigurationManager.AppSettings["BucketId"]);
                arg.VideoClip.VidEdits = (sender as NMSVideoEditor).videoEdits;
                ThreadPool.QueueUserWorkItem(new WaitCallback(UploadFile), arg);
                editor = null;
            }
        }

        public class UploadArgs
        {
            public int SlotTemplateScreenElementId { get; set; }
            public string FilePath { get; set; }

            public VideoClipInput VideoClip { get; set; }
        }

        private bool isMaximized;
        private Rect normalBounds;
        private void MainWindow_OnStateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                isMaximized = true;

                normalBounds = RestoreBounds;

                Height = SystemParameters.WorkArea.Height;
                MaxHeight = Height;
                MinHeight = Height;

                Width = SystemParameters.WorkArea.Width;
                MaxWidth = Width;
                MinWidth = Width;
                Top = 0;
                Left = 0;
                SetMovable(false);
                
                if (editor != null && editor.IsInitialized)
                {
                    editor.Show();
                }

                if (popUpOpened)
                {
                    popPlayer.IsOpen = true;
                }
            }
            else if (WindowState == WindowState.Maximized && isMaximized)
            {
                WindowState = WindowState.Normal;
                isMaximized = false;

                MaxHeight = Double.PositiveInfinity;
                MinHeight = 0;

                Top = 0;
                Left = 0;
                Width = SystemParameters.WorkArea.Width;//normalBounds.Width;
                Height = SystemParameters.WorkArea.Height;//normalBounds.Height;
                SetMovable(true);

                if (editor != null && editor.IsInitialized)
                {
                    editor.Show();
                }

                if (popUpOpened)
                {
                    popPlayer.IsOpen = true;
                }
            }
            else if (WindowState == WindowState.Minimized)
            {
                editor.Hide();

                if (popUpOpened)
                {
                    popPlayer.IsOpen = false;
                }
            }
        }

        private void SetMovable(bool enable)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);

            if (enable)
                source.RemoveHook(WndProc);
            else
                source.AddHook(WndProc);
        }

        private static IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (msg)
            {
                case WM_SYSCOMMAND:
                    int command = wParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        handled = true;
                    break;
            }

            return IntPtr.Zero;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            popPlayer.IsOpen = true;
            gdPlayer.OpenMedia(new Video() { VideoUrl = "http://10.3.12.119/api/Resource/getresource/930d7d2f-d252-405a-9d9c-4384ce7698ba" });
        }

        public void openVideo(object obj, JavascriptMethodEventArgs jsMethodArgs)
        {
            popPlayer.IsOpen = true;
            popUpOpened = true;
            gdPlayer.OpenMedia(new Video() { VideoUrl = jsMethodArgs.Arguments[0].ToString() });
        }

        private void btnClosePopUp_Click_1(object sender, RoutedEventArgs e)
        {
            popUpOpened = false;
            popPlayer.IsOpen = false;
            gdPlayer.CloseMedia();
        }
    }
}
