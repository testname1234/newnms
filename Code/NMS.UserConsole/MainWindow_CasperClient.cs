﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using Awesomium.Core;
using CasparCGLib;
using CasparCGLib.Entities;
using NMS.UserConsole.Enums;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using Common;
using NMS.UserConsole.Entities;

namespace NMS.UserConsole
{
    public partial class MainWindow : Window
    {
        public string CasperClientXmlFileName = "casperRundown.xml";
        CasparCGServer cgServer = null;

        public List<ROCreate> CasparRunOrders = new List<ROCreate>();
        public List<CasparItem> CurrentCasperItems = null;
        public List<CasparItem> list = null;


        private void ExecuteCasperCommand(CommandType commandType, PostInput input)
        {
            switch (commandType)
            {

                case CommandType.CurrentSelectedItem_Caspar:
                    {
                         if (!string.IsNullOrEmpty(input.RoId))
                        {
                                CurrentCasperItems = new List<CasparItem>();
                                ROCreate CurrentRo= CasparRunOrders.Where(x => x.roID == input.RoId).FirstOrDefault();
                                if (CurrentRo != null)
                                {
                                    CurrentCasperItems = CurrentRo.casparItems;
                                    Dispatcher.Invoke(() =>
                                    {
                                        wbHost.ExecuteJavascript("currentRunOrderCallBack();");
                                    });
                                }
                                else {
                                    CurrentCasperItems = list;
                                }
                        }
                        break;
                    }

                case CommandType.LoadInitialRunOrders_Caspar:
                    {
                        CommandSenderType Type = CommandSenderType.CasperClient;
                        //ReadXMLFile(CasperClientXmlFileName, Type);
                        serializToJason_CallBackData(RunOrderType.XmlRead);
                        break;
                    }

                case CommandType.Play_Caspar:
                    {
                        if (cgServer != null && CurrentCasperItems !=null)
                        {
                            List<item> currentItems = new List<item>();
                            currentItems.CopyFrom(CurrentCasperItems);
                            cgServer.PlayRundown(currentItems);

                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("casperPlayCallBack(1);");
                            });
                        }

                        break;
                    }
                case CommandType.Connect_Caspar:
                    {
                        cInput.HubIp = input.HubIp;                      //as casper server Ip
                        cInput.NcsPort = int.Parse(input.NcsPort);        //as casper server port
                        
                        if (cgServer == null && !string.IsNullOrEmpty(cInput.HubIp) && cInput.NcsPort > 0)
                        {
                            cgServer = new CasparCGServer(cInput.HubIp, cInput.NcsPort);
                            Connection(CommandType.Connect_Caspar);

                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("casperConnectCallBack();");
                            });
                        }

                        break;
                    }
                case CommandType.DisConnect_Caspar:
                    {
                        if (cgServer != null && cgServer.IsConnected )
                        {
                            Connection(CommandType.DisConnect_Caspar);

                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("casperDisConnectCallBack();");
                            });
                        }

                        break;
                    }
                case CommandType.ListenRunOrder_Caspar:
                    {
                        cInput.MosId = input.MosId;            
                        cInput.MosPort = int.Parse(input.MosPort);

                        if (!string.IsNullOrEmpty(cInput.MosId) && cInput.MosPort > 0)
                        {
                            StartListening((cInput.MosPort));
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("casperListenRunOrderCallBack();");
                            });

                        }
                        break;
                    }
                case CommandType.LoadRunOrder_Caspar:
                    {
                        if (!string.IsNullOrEmpty(input.Path) && input.Path != "null")
                        {
                            LoadToClient(input.Path.ToString());
                        }

                        Dispatcher.Invoke(() =>
                        {
                            wbHost.ExecuteJavascript("casperLoadRunOrderCallBack();");
                        });

                        break;
                    }
                case CommandType.PlayItemWise_Caspar:
                    {
                        if (cgServer != null && CurrentCasperItems !=null)
                        {
                            Guid itemId = new Guid(input.ItemId);
                            CasparItem currentItemToPlay = new CasparItem();
                            //currentItemToPlay = CurrentCasperItems.Where(x => x.Id == itemId).FirstOrDefault();
                            item i = new item();
                            i.CopyFrom(currentItemToPlay);
                            cgServer.PlayItem(i);
                            
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("casperPlayItemWiseCallBack();");
                            });
                        }

                        break;
                    }

                case CommandType.ClearGraphic_Caspar:
                    {

                        if (cgServer != null && !string.IsNullOrEmpty(input.Path) && input.Path != "null")
                        {
                            string message = string.Empty;
                            cgServer.ClearChannel();

                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("casperClearCallBack();");
                            });
                        }
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
        }

        private void LoadToClient(string p)
        {
            ROCreate ro = new ROCreate();
            using (StreamReader reader = new StreamReader(p))
            {
                string xml = reader.ReadToEnd();
                list = Common.Helper.DeserializeFromXml<List<CasparItem>>(xml, "items");
                ro.roSlug = "New Runorder";
                ro.roID = Guid.NewGuid().ToString();
                ro.casparItems = list;
                if (list != null)
                {
                    CurrentCasperItems = ro.casparItems;
                }
            }
            serializToJason_CallBackData(RunOrderType.New,ro);
        }

        private void Connection(CommandType cmd)
        {
            if (cmd == CommandType.Connect_Caspar)
            {
                cgServer.Connect();
            }
            else if (cmd == CommandType.DisConnect_Caspar)
            {
                cgServer.Disconnect();
            }
            else { }

        }

        public void StartListening(int port)
        {
            mosServer = new MOSServer(cInput.MosId);
            mosServer.StartListening(port);
            mosServer.RunOrderReceived += ReceiveRunDown;
        }

        private MOS ReceiveRunDown(MOS obj)
        {
            MOS response = new MOS();
            response.mosID = obj.mosID;
            response.ncsID = obj.ncsID;
            response.messageID = obj.messageID;
            ROAck roAck = new ROAck();

            ROCreate ro = (ROCreate)obj.command;
            if (ro != null && ro.casparItems.Count > 0)
            {
                ROCreate item = CasparRunOrders.Where(x => x.roID == ro.roID).FirstOrDefault();
                if (item != null)
                {
                    CasparRunOrders.Remove(item);
                }

                CasparRunOrders.Add(ro);
                serializToJason_CallBackData(RunOrderType.New, ro);//update client if new runorder received

                roAck.roID = ro.roID;

                roAck.roStatus = "OK";
            }
            else
            {
                roAck.roStatus = "NACK";
            }
            response.command = roAck;
            return response;
        }

        private void serializToJason_CallBackData(RunOrderType cmd, ROCreate ro = null)
        {
            Dispatcher.Invoke(() =>
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (cmd == RunOrderType.New)
                {
                    string json = serializer.Serialize(ro);
                    wbHost.ExecuteJavascript("casperNewRunOrderRecieved([" + json + "]);");
                    //RunOrderXmlWriter<ROCreate>(CasperClientXmlFileName, CasparRunOrders);
                }
                else if (cmd == RunOrderType.XmlRead)
                {
                    var rundOrdersList = CasparRunOrders;
                    string json = serializer.Serialize(rundOrdersList);
                    wbHost.ExecuteJavascript("casperRunOrderRecievedFromXMl(" + json + ");");

                } else { }
            });
        }
    }


}
