﻿using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using MOSProtocol.Lib.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using BolUserConsole.Core.Entities;
using NMS.UserConsole.Enums;


namespace NMS.UserConsole
{
    public partial class MainWindow : Window
    {
        public static List<RunOrder> RunOrders = new List<RunOrder>();
        public static List<RunOrder> AllRunOrders = new List<RunOrder>();
        MOSServer mosServer;
        
        private MOS HeartBeatReceived(MOS mosCommand)
        {
            HeartBeat heartbeat = (HeartBeat)mosCommand.command;
            heartbeat.time = DateTime.UtcNow;
            mosCommand.command = heartbeat;
            return mosCommand;
        }
       
        private MOS RunOrderReceived(MOS mosCommand)
         {

                MOS response = new MOS();
                response.messageID = mosCommand.messageID;
                response.mosID = mosCommand.mosID;
                response.ncsID = mosCommand.ncsID;

                ROAck roAck = new ROAck();
                ROCreate roCreate = (ROCreate)mosCommand.command;
                List<MOSProtocol.Lib.Entities.Story> stories = (List<MOSProtocol.Lib.Entities.Story>)roCreate.story;

                RunOrder runOrder = new RunOrder();

               // added
                 int roCreateId=int.Parse(roCreate.roID);

                 RunOrder AlreadyExists_ro = AllRunOrders.Where(x => x.RoId == roCreateId).FirstOrDefault();

                 if (AllRunOrders.Count > 0)
                {
                    runOrder = AllRunOrders.Where(x => x.RoId == roCreateId).FirstOrDefault();
                }

                if (runOrder == null)
                {
                    runOrder = new RunOrder();
                }

                runOrder.RoId = roCreateId;
                runOrder.Slug = roCreate.roSlug;
                runOrder.StartTime = DateTimeOffset.Parse(roCreate.roEdStart).UtcDateTime;  //Convert.ToDateTime(roCreate.roEdStart);
                runOrder.Duration = DateTimeOffset.Parse(roCreate.roEdDur).UtcDateTime;

                roAck.roID = roCreate.roID;
                roAck.roStatus = "OK";

                if (stories != null)
                {
                    roAck.roAckStories = new List<ROAckStory>();

                    if (AlreadyExists_ro != null)
                    {
                        runOrder.Stories = runOrder.Stories;
                    }
                    else
                    {
                        runOrder.Stories = new List<BolUserConsole.Core.Entities.Story>();
                    }

                    for (int i = 0; i < stories.Count; i++)
                    {
                        //added
                        int stId=int.Parse(stories[i].storyID);

                        BolUserConsole.Core.Entities.Story AlreadyExists = runOrder.Stories.Where(x => x.StoryId == stId).FirstOrDefault();
                        BolUserConsole.Core.Entities.Story story = new BolUserConsole.Core.Entities.Story();

                        if (AlreadyExists != null)
                        {
                            story = AlreadyExists;
                        }


                        story.StoryId = stId;
                        story.RoId = roCreateId;
                        story.Slug = stories[i].storySlug;
                        story.SequenceId = Convert.ToInt32(stories[i].storyNum);

                        if (AlreadyExists == null)
                        {
                            runOrder.Stories.Add(story);
                        }

                        ROAckStory roAckStory = new ROAckStory();
                        roAckStory.storyID = stories[i].storyID;
                        if (AlreadyExists == null)
                        {
                            roAckStory.storyStatus = StoryStatus.NEW;
                        }
                        else
                        {
                            roAckStory.storyStatus = StoryStatus.UPDATED;
                        }
                        roAck.roAckStories.Add(roAckStory);

                    }
                }

                response.command = roAck;

                if (AlreadyExists_ro == null)
                {
                    AllRunOrders.Add(runOrder);
                }

                NewRunOrderAndStoriesNotification(RunOrderStatus.NEW,runOrder);

                return response;

        }
        private MOS StoryReceived(MOS mosCommand)
        {
            //Story return ack
            return mosCommand;
        }
        private MOS StoryInfoReceived(MOS mosCommand)
        {
            MOS response = new MOS();
            //Story Story Send..
            response.mosID = mosCommand.mosID;
            response.ncsID = mosCommand.ncsID;
            response.messageID = mosCommand.messageID;

            ROAck roAck = new ROAck();
            ROStorySend rostorySend = (ROStorySend)mosCommand.command;

            RunOrder runOrder = new RunOrder();

            //added 
            int roStorySendRoId = int.Parse(rostorySend.roID);


            BolUserConsole.Core.Entities.RunOrder AlreadyExists_ro = AllRunOrders.Where(x => x.RoId == roStorySendRoId).FirstOrDefault();

            if (AllRunOrders.Count > 0)
            {
                runOrder = AllRunOrders.Where(x => x.RoId == roStorySendRoId).FirstOrDefault();
            }

            if (runOrder == null)
            {
                runOrder = new RunOrder();
            }

            runOrder.RoId = roStorySendRoId;

            roAck.roID = rostorySend.roID;
            roAck.roStatus = "OK";

            roAck.roAckStories = new List<ROAckStory>();

            if (AllRunOrders.Count > 0)
            {
                runOrder.Stories = runOrder.Stories;
            }
            else
            {
                runOrder.Stories = new List<BolUserConsole.Core.Entities.Story>();
            }

            // addedd 
            int rostorySendStoryId = int.Parse(rostorySend.storyID);


            BolUserConsole.Core.Entities.Story AlreadyExists = runOrder.Stories.Where(x => x.StoryId == rostorySendStoryId).FirstOrDefault();

            BolUserConsole.Core.Entities.Story story = new BolUserConsole.Core.Entities.Story();
            if (AlreadyExists != null)
            {
                story = AlreadyExists;
            }

            story.StoryId = rostorySendStoryId;
            story.RoId = roStorySendRoId;
            story.Slug = rostorySend.storySlug;

            int AlreadyExists_Items;
            if (story.StoryItems == null)
            {
                AlreadyExists_Items = 0;
            }
            else
            {
                AlreadyExists_Items = 1;
            }

            if (story.StoryItems != null)
            {
                for (int i = 0; i < story.StoryItems.Count; i++)
                {
                    story.StoryItems.Remove(story.StoryItems[i]);
                }
            }

            story.StoryItems = new List<StoryItem>();
            for (int i = 0; i < rostorySend.storyBodies.Count; i++)
            {
                StoryItem storyItem = new StoryItem();
                storyItem.StoryItemId = int.Parse(string.Format("{0}{1}", rostorySend.storyID, i));
                storyItem.StoryId = rostorySendStoryId;
                storyItem.Instructions = rostorySend.storyBodies[i].ParagraphWithInstructions;
                storyItem.Detail = rostorySend.storyBodies[i].Paragraph;
                //story.SequenceId = i + 1;
                story.StoryItems.Add(storyItem);
            }

            if (AlreadyExists == null)
            {
                runOrder.Stories.Add(story);
            }

            ROAckStory roAckStory = new ROAckStory();
            roAckStory.storyID = rostorySend.storyID;

            if (AlreadyExists_Items == 0)
            {
                roAckStory.storyStatus = StoryStatus.NEW;
            }
            else
            {
                roAckStory.storyStatus = StoryStatus.UPDATED;
            }

            roAck.roAckStories.Add(roAckStory);

            response.command = roAck;

            if (AlreadyExists_ro == null)
            {
                AllRunOrders.Add(runOrder);
            }

            UpdatedStoriesAndNewStoryItemsNotification(runOrder);
            return response;

        }
        //private MOS StorySwapReceived(MOS mosCommand)
        //{
        //    MOS response = new MOS();
        //        response.messageID = mosCommand.messageID;
        //        response.mosID = mosCommand.mosID;
        //        response.ncsID = mosCommand.ncsID;

        //        ROAck roAck = new ROAck();
        //        ROStorySwap roStorySwap = (ROStorySwap)mosCommand.command;
        //        List<MOSProtocol.Lib.Entities.ROStorySwapStories> stories = (List<MOSProtocol.Lib.Entities.ROStorySwapStories>)roStorySwap.roStorySwapStories;

        //        RunOrder runOrder = new RunOrder();
        //        NMS.Core.Teleprompter.Entities.RunOrder AlreadyExists_ro = RunOrders.Where(x => x.ID == roStorySwap.roID).FirstOrDefault();


        //        if (RunOrders.Count > 0)
        //        {
        //            runOrder = RunOrders.Where(x => x.ID == roStorySwap.roID).FirstOrDefault();
        //        }

        //        if (runOrder == null)
        //        {
        //            runOrder = new RunOrder();
        //        }

        //        runOrder.ID = roStorySwap.roID;
        //        roAck.roID = roStorySwap.roID;
        //        roAck.roStatus = "OK";

        //        try
        //        {
        //            if (stories != null)
        //            {
        //                roAck.roAckStories = new List<ROAckStory>();

        //                NMS.Core.Teleprompter.Entities.Story FirstStory = runOrder.Stories.Where(x => x.ID == stories[0].storyID).FirstOrDefault();
        //                NMS.Core.Teleprompter.Entities.Story SecondStory = runOrder.Stories.Where(x => x.ID == stories[1].storyID).FirstOrDefault();

        //                double Var = FirstStory.SequenceId;
        //                FirstStory.SequenceId = SecondStory.SequenceId;
        //                SecondStory.SequenceId = Var;

        //            }
        //        }
        //        catch (Exception)
        //        {
        //            roAck.roStatus = "NACK";
        //        }


        //        response.command = roAck;

        //        if (AlreadyExists_ro ==null)
        //        {
        //            RunOrders.Add(runOrder);
        //        }

        //        return response;

        //}
        //private MOS StorySkipReceived(MOS mosCommand)
        //{
        //    MOS response = new MOS();

        //        response.messageID = mosCommand.messageID;
        //        response.mosID = mosCommand.mosID;
        //        response.ncsID = mosCommand.ncsID;

        //        ROAck roAck = new ROAck();
        //        ROStoryDelete roStoryDelete = (ROStoryDelete)mosCommand.command;
        //        roAck.roAckStories = new List<ROAckStory>();
        //        RunOrder runOrder = new RunOrder();
        //        NMS.Core.Teleprompter.Entities.RunOrder AlreadyExists_ro = RunOrders.Where(x => x.ID == roStoryDelete.roID).FirstOrDefault();

        //        if (RunOrders.Count > 0)
        //        {
        //            runOrder = RunOrders.Where(x => x.ID == roStoryDelete.roID).FirstOrDefault();
        //        }

        //        if (runOrder == null)
        //        {
        //            runOrder = new RunOrder();
        //        }

        //        runOrder.ID = roStoryDelete.roID;
        //        roAck.roStatus = "OK";

        //        if (RunOrders.Count > 0)
        //        {
        //            NMS.Core.Teleprompter.Entities.Story AlreadyExists = runOrder.Stories.Where(x => x.ID == roStoryDelete.storyID).FirstOrDefault();

        //            ROAckStory roAckStory = new ROAckStory();
        //            roAckStory.storyID = roStoryDelete.storyID;
        //            if (AlreadyExists == null)
        //            {
        //                roAckStory.storyStatus = StoryStatus.NACK;
        //            }
        //            else
        //            {
        //                runOrder.Stories.Remove(AlreadyExists);
        //                roAckStory.storyStatus = StoryStatus.DELETED;
        //            }

        //            roAck.roAckStories.Add(roAckStory);
        //        }

        //        response.command = roAck;

        //        if (AlreadyExists_ro ==null)
        //        {
        //            RunOrders.Add(runOrder);
        //        }

        //        return response;


        //}
        //private MOS StoryUnSkipReceived(MOS mosCommand)
        //{
        //    MOS response = new MOS();

        //        response.messageID = mosCommand.messageID;
        //        response.mosID = mosCommand.mosID;
        //        response.ncsID = mosCommand.ncsID;

        //        ROAck roAck = new ROAck();
        //        ROStoryInsert roStortyInsert = (ROStoryInsert)mosCommand.command;
        //        List<MOSProtocol.Lib.Entities.Story> stories = (List<MOSProtocol.Lib.Entities.Story>)roStortyInsert.story;

        //        RunOrder runOrder = new RunOrder();
        //        NMS.Core.Teleprompter.Entities.RunOrder AlreadyExists_ro = RunOrders.Where(x => x.ID == roStortyInsert.roID).FirstOrDefault();

        //        if (RunOrders.Count > 0)
        //        {
        //            runOrder = RunOrders.Where(x => x.ID == roStortyInsert.roID).FirstOrDefault();
        //        }

        //        if (runOrder == null)
        //        {
        //            runOrder = new RunOrder();
        //        }

        //        runOrder.ID = roStortyInsert.roID;
        //        roAck.roID = roStortyInsert.roID;
        //        roAck.roStatus = "OK";

        //        if (stories != null)
        //        {
        //            roAck.roAckStories = new List<ROAckStory>();
        //            if (RunOrders.Count > 0)
        //            {
        //                runOrder.Stories = runOrder.Stories;
        //            }
        //            else
        //            {
        //                runOrder.Stories = new List<NMS.Core.Teleprompter.Entities.Story>();
        //            }

        //            for (int i = 0; i < stories.Count; i++)
        //            {
        //                NMS.Core.Teleprompter.Entities.Story AlreadyExists = runOrder.Stories.Where(x => x.ID == stories[i].storyID).FirstOrDefault();
        //                NMS.Core.Teleprompter.Entities.Story story = new NMS.Core.Teleprompter.Entities.Story();

        //                if (AlreadyExists != null)
        //                {
        //                    story = AlreadyExists;
        //                }

        //                story.ID = stories[i].storyID;
        //                story.RoId = roStortyInsert.roID;
        //                story.Slug = stories[i].storySlug;
        //                story.SequenceId = Convert.ToDouble(roStortyInsert.storyID) - 0.001;
        //                if (AlreadyExists == null)
        //                {
        //                    runOrder.Stories.Add(story);
        //                }

        //                ROAckStory roAckStory = new ROAckStory();
        //                roAckStory.storyID = stories[i].storyID;
        //                if (AlreadyExists == null)
        //                {
        //                    roAckStory.storyStatus = StoryStatus.NEW;
        //                }
        //                else
        //                {
        //                    roAckStory.storyStatus = StoryStatus.UPDATED;
        //                }
        //                roAck.roAckStories.Add(roAckStory);

        //            }

        //        }

        //        response.command = roAck;

        //        if (AlreadyExists_ro == null)
        //        {
        //            RunOrders.Add(runOrder);
        //        }

        //        return response;

        //}
        //private MOS StoryMoveReceived(MOS mosCommand)
        //{
        //    MOS response = new MOS();
        //        response.messageID = mosCommand.messageID;
        //        response.mosID = mosCommand.mosID;
        //        response.ncsID = mosCommand.ncsID;

        //        ROAck roAck = new ROAck();
        //        ROStoryMove roStoryMove = (ROStoryMove)mosCommand.command;
        //        List<MOSProtocol.Lib.Entities.ROStoryMoveStories> stories = (List<MOSProtocol.Lib.Entities.ROStoryMoveStories>)roStoryMove.roStoryMoveStories;

        //        RunOrder runOrder = new RunOrder();
        //        NMS.Core.Teleprompter.Entities.RunOrder AlreadyExists_ro = RunOrders.Where(x => x.ID == roStoryMove.roID).FirstOrDefault();

        //        if (RunOrders.Count > 0)
        //        {
        //            runOrder = RunOrders.Where(x => x.ID == roStoryMove.roID).FirstOrDefault();
        //        }

        //        if (runOrder == null)
        //        {
        //            runOrder = new RunOrder();
        //        }

        //        roAck.roID = roStoryMove.roID;
        //        roAck.roStatus = "OK";

        //        try
        //        {
        //            if (stories != null)
        //            {
        //                roAck.roAckStories = new List<ROAckStory>();

        //                NMS.Core.Teleprompter.Entities.Story story = runOrder.Stories.Where(x => x.ID == stories[0].storyID).FirstOrDefault(); //4
        //                NMS.Core.Teleprompter.Entities.Story ReferenceStory = runOrder.Stories.Where(x => x.ID == stories[1].storyID).FirstOrDefault(); //1

        //                double Var = ReferenceStory.SequenceId;
        //                story.SequenceId = Var - 0.001;

        //            }
        //        }
        //        catch (Exception)
        //        {
        //            roAck.roStatus = "NACK";
        //        }


        //        response.command = roAck;

        //        if (AlreadyExists_ro    == null)
        //        {
        //            RunOrders.Add(runOrder);
        //        }

        //        return response;

        //}

        private void StartListening(string MOSID, int Port)
        {
            mosServer = new MOSServer(MOSID);
            mosServer.StartListening(Port);
            mosServer.HeartBeatReceived += HeartBeatReceived;
            mosServer.RunOrderReceived += RunOrderReceived;
            mosServer.StoryReceived += StoryReceived;
            mosServer.StoryInfoReceived += StoryInfoReceived;
            //mosServer.StorySwapReceived += StorySwapReceived;
            //mosServer.StorySkipReceived += StorySkipReceived;
            //mosServer.StoryUnSkipReceived += StoryUnSkipReceived;
            //mosServer.StoryMoveReceived += StoryMoveReceived;
        }
        private void StopListening()
        {

            mosServer.StopListening();
        }
    }
}
