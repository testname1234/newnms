﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using MOSProtocol.Lib;
using MOSProtocol.Lib.Entities;
using MOSProtocol.Lib.Enums;
using NMS.UserConsole.Entities;
using NMS.UserConsole.Enums;

namespace NMS.UserConsole
{
    public partial class MainWindow : Window
    {
        public InputSettings cInput = new InputSettings();

        private void StartListening()
        {
            if (!string.IsNullOrEmpty(cInput.MosId) && cInput.MosPort>0)
            {
              mosServer = new MOSServer(cInput.MosId);
              mosServer.StartListening(cInput.MosPort);
              mosServer.NotificationReceived += NotificationReceived;
            }
       
        }
  
        private MOS NotificationReceived(MOS MOSCommand)
        {
            Notification notification = (Notification)MOSCommand.command;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(notification);
            if (MOSCommand != null)
            {
                Dispatcher.Invoke(() =>
                {
                    wbHost.ExecuteJavascript("notificationCallBack(" + json + ");");
                });
            }

            return MOSCommand;
        }

        private void ExecutePcrUserCommand(CommandType commandType,PostInput input)
        { 
            switch(commandType)
            {
                case CommandType.Notify_PcrUser:
                    {
                        cInput.MosId = input.MosId;
                        cInput.MosPort = int.Parse(input.MosPort);
                        StartListening();
                        
                        break;
                    }
                    
                default:
                    break;
            
            }
        }
    }
}
