﻿using Awesomium.Core;
using System;
using System.Web.Script.Serialization;
using System.Windows;
using NMS.UserConsole.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Timers;
using System.Windows.Navigation;
using System.Management;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices.ComTypes;
using NMS.UserConsole.View;
using BolUserConsole.Core.Entities;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using MOSProtocol.Lib.Entities;
using NMS.UserConsole.Entities;
using System.Threading;
using BolUserConsole.Repository;
using MOSProtocol.Lib;
using BolUserConsole.Core;
using MOSProtocol.Lib.Enums;


namespace NMS.UserConsole
{
    public partial class MainWindow : Window
    {
        public Prompter prompter = null;
        public PrompterPreview PrompterP = null;
        public string TelePrompterXmlFileName = "runDown.xml";
        RunOrder ro = null;
        Thread DeleteRundownThread = null;
        public bool DeleteRundownThreadToggler = true;
        public int DeleteRundownThreadDelayTime = 1;
        public double RunDownDeletionTimeSpan = 1;
        public int CurrentUserId=0;
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        private MOSClient mosClient;
     
        private void flipTex()
        {
            if (prompter != null)
            {
                prompter.Scale.ScaleX = -prompter.Scale.ScaleX;
            }
        }

        private void InitPrompter()
        {
            var screens = System.Windows.Forms.Screen.AllScreens;
            if (screens.Count() > 0)
            {
                //var monitors = screens.Where(x => x.DeviceName.ToLower().Contains("display1"));
                var monitors = screens.Where(x => !x.Primary);
                System.Windows.Forms.Screen monitor = null;

                if (monitors != null && monitors.Count() == 1)
                {
                    monitor = monitors.FirstOrDefault();
                    prompter.Left = monitor.WorkingArea.Left;
                    prompter.Top = monitor.WorkingArea.Top;
                    prompter.Width = monitor.WorkingArea.Width;
                    prompter.Height = monitor.WorkingArea.Height;
                    prompter.Show();
                }
            }
        }

        private void ExecuteTeleprompterCommand(CommandType commandType, PostInput input)
        {
            switch (commandType)
            {
                case CommandType.GetAllRunDowns_Prompter:
                    {
                        if (!string.IsNullOrEmpty(input.RoId) && input.ThreadDelayTime>0)
                        {
                                CurrentUserId = input.ThreadDelayTime;
                                UserRepository CurrentUserToInsert = new UserRepository();
                                User exist= CurrentUserToInsert.GetUser(CurrentUserId);
                                User CurrentUser = new User();
                                CurrentUser.UserId = input.ThreadDelayTime;
                                CurrentUser.SessionId = input.RoId;
                                CurrentUser.LastLoginTime = DateTime.UtcNow;
                                if (exist != null)
                                {
                                    CurrentUserToInsert.UpdateUser(CurrentUser); 
                                }
                                else 
                                {
                                    CurrentUserToInsert.InsertUser(CurrentUser);
                                }
                                LoadInitialData();
                        }
                        else 
                        { 
                          Toaster("Failed Getting Data LogOut and Login Again",false);
                        }
                        break;
                    }

                case CommandType.CurrentSelectedRunOrder_Prompter:
                    {
                        if (!string.IsNullOrEmpty(input.RoId))
                        {
                            int RoId = int.Parse(input.RoId);
                            if (ro == null || RoId != ro.RoId)
                            {
                                ResetPrompter();
                                ro = new RunOrder();
                                ro = AllRunOrders.Where(x => x.RoId == RoId).FirstOrDefault();
                                ro.Stories = ro.Stories.OrderBy(x => x.SequenceId).ToList();
                                Dispatcher.Invoke(() =>
                                {
                                    wbHost.ExecuteJavascript("currentRunOrderCallBack();");
                                });
                            }
                        }

                        break;
                    }

                case CommandType.StartListening_Prompter:
                    {
                        cInput.MosId = input.MosId;
                        updateConfiguraitonSetting(input.SettingIdA,input.MosId);
                        cInput.MosPort = int.Parse(input.MosPort);
                        updateConfiguraitonSetting(input.SettingIdB,input.MosPort);
                        if (!string.IsNullOrEmpty(cInput.MosId) && cInput.MosPort > 0)
                        {
                            StartListening(cInput.MosId, cInput.MosPort);
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("startCallBack('" + "Success" + "');");
                            });
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("startCallBack('" + "Error" + "');");
                            });
                        }

                        break;
                    }
                case CommandType.ConnectHub_Prompter:
                    {
                        cInput.MosId = input.MosId;
                        updateConfiguraitonSetting(input.SettingIdA, input.MosId);
                        cInput.NcsId = input.NcsId;
                        updateConfiguraitonSetting(input.SettingIdB, input.NcsId);
                        cInput.NcsPort = int.Parse(input.NcsPort);
                        updateConfiguraitonSetting(input.SettingIdC, input.NcsPort);
                        cInput.HubIp = input.HubIp;
                        updateConfiguraitonSetting(input.SettingIdD, input.HubIp);

                        if (!string.IsNullOrEmpty(cInput.NcsId) && !string.IsNullOrEmpty(cInput.MosId) && !string.IsNullOrEmpty(cInput.HubIp) && cInput.NcsPort > 0)
                        {
                            if (prompter == null)
                            {
                                prompter = new Prompter(2);
                            }
                            prompter.MOSId = cInput.MosId;
                            prompter.NCSId = cInput.NcsId;
                            prompter.NCSPort = cInput.NcsPort;
                            prompter.Ip = cInput.HubIp;
                            prompter.ConnectHub();
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("hubConnectionCallBack("+"Success"+");");
                            });
                        }

                        break;
                    }
                case CommandType.StopListening_Prompter:
                    {
                        if (mosServer != null && mosServer.IsListening)
                        {
                            StopListening();
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("stopCallBack('" + "Success" + "');");
                            });
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("stopCallBack('" + "Error" + "');");
                            });
                                
                        }

                        break;
                    }
                case CommandType.BrowseXML_Prompter:
                    {
                        if (!string.IsNullOrEmpty(input.ActionToMap))
                        {
                            string FileName = input.ActionToMap;
                            string path = "C:/Users/asadhussain/Desktop/XML/" + FileName;
                            ReadXMLFile(path);
                        }
                        break;
                    }
                case CommandType.LoadToPrompter_Prompter:
                    {
                        if (input.RoId != null && prompter != null)
                        {
                            if (ro != null)
                            {
                                prompter.LoadPrompter(ro);
                                Dispatcher.Invoke(() =>
                                {
                                    wbHost.ExecuteJavascript("playCallBack('" + "Success" + "');");
                                });
                            }
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("playCallBack('" + "Error" + "');");
                                Toaster("Select Rundown and open Prompt window",false);
                            });
                        }
                        break;
                    }

                case CommandType.JumpToStory_Prompter:
                    {
                        if (input.RoId != null && prompter != null)
                        {
                            if (ro != null)
                            {
                                int index = 0;
                                bool ifSkipped=false;
                                int StId = int.Parse(input.StoryId);
                                for (int i = 0; i < ro.Stories.Count; i++)
                                {
                                    if (StId == ro.Stories[i].StoryId)
                                    {
                                        index = ro.Stories.IndexOf(ro.Stories[i]);
                                        ifSkipped = ro.Stories[i].IsSkipped;
                                    }
                                }

                                if (ifSkipped != true)
                                {
                                    prompter.JumpToStory(index);
                                    Dispatcher.Invoke(() =>
                                    {
                                        wbHost.ExecuteJavascript("storyJumpCallBack('" + "Success" + "');");
                                    });
                                    
                                }
                                else 
                                {
                                    Toaster("Unskip Story First",false);
                                }
                                  


                            }
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("storyJumpCallBack('" + "Error" + "');");
                                Toaster("Select Rundown and open Prompt window",false);
                            });
                        }
                        break;
                    }

                case CommandType.PlayPauseToggle_Prompter:
                    {
                        if (prompter != null)
                        {
                            if(input.PlayPuaseToggle && prompter.ro !=null)
                            {
                                prompter.Play = input.PlayPuaseToggle;
                                wbHost.ExecuteJavascript("playToggleCallBack ('" + "Success" + "');");
                            }
                            else if (!input.PlayPuaseToggle && prompter.ro != null)
                            {
                                prompter.Play = input.PlayPuaseToggle;
                                wbHost.ExecuteJavascript("playToggleCallBack ('" + "Success" + "');");
                            }
                            else 
                            {
                                wbHost.ExecuteJavascript("playToggleCallBack ('" + "Error" + "');");  
                            }
                        }
                        else
                        {

                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("playToggleCallBack ('" + "Error" + "');");
                            });
                        }
                        break;
                    }


                case CommandType.Skip_Prompter:
                    {
                        if (input.RoId != null && input.StoryId != null)
                        {
                            if (ro != null)
                            {
                                int StoryId = int.Parse(input.StoryId);
                                for (int i = 0; i < ro.Stories.Count; i++)
                                {
                                    if (StoryId == ro.Stories[i].RoId)
                                    {
                                        ro.Stories[i].IsSkipped = true;
                                    }
                                }
                                UpgradeUserActionToDB(CommandType.Skip_Prompter, null, StoryId);

                                Dispatcher.Invoke(() =>
                                {
                                    wbHost.ExecuteJavascript("skipCallBack(1);");
                                });
                            }
                        }
                        break;
                    }

                case CommandType.UnSkip_Prompter:
                    {
                        if (input.RoId != null && input.StoryId != null)
                        {
                            if (ro != null)
                            {
                                int StId=int.Parse(input.StoryId);
                                for (int i = 0; i < ro.Stories.Count; i++)
                                {
                                    if (StId == ro.Stories[i].StoryId)
                                    {
                                        ro.Stories[i].IsSkipped = false;
                                    }
                                }
                                UpgradeUserActionToDB(CommandType.UnSkip_Prompter, null, StId);
                                Dispatcher.Invoke(() =>
                                {
                                    wbHost.ExecuteJavascript("unSkipCallBack(1);");
                                });
                            }
                        }
                        break;
                    }

                case CommandType.PromptON_Prompter:
                    {
                        if (prompter == null)
                        {
                            if (prompter == null)
                            {
                                prompter = new Prompter(2);

                            }
                            InitPrompter();
                            prompter.Closing += ClosePrompter;
                            prompter.wbHost = wbHost;

                            //call back
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("promptON('" + "Success" + "');");
                            });
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("promptON('" + "Error" + "');");
                            });
                        }
                        break;
                    }
                case CommandType.PromptOFF_Prompter:
                    {
                        if (prompter != null)
                        {
                            prompter.Hide();
                            prompter = null;
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("promptOFF('" + "Success" + "');");
                            });
                        }
                        else
                        {

                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("promptOFF('" + "Error" + "');");
                            });
                        
                        }
                        break;
                    }
                case CommandType.PreviewON_Prompter:
                    {
                        if (PrompterP == null)
                        {
                            PrompterP = new PrompterPreview();
                            PrompterP.Show();
                            //call back
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("previewON('" + "Success" + "');");
                            });
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("previewON('" + "Error" + "');");
                            });
                        }
                        break;
                    }
                case CommandType.PreviewOFF_Prompter:
                    {
                        if (PrompterP != null)
                        {
                            PrompterP.Hide();
                            PrompterP = null;
                            //call back
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("previewOFF('" + "Success" + "');");
                            });
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("previewOFF('" + "Error" + "');");
                            });
                        }
                        break;
                    }


                case CommandType.UpdateSequence_Prompter:
                    {
                        if (input.UpdatedSequence != null)
                        {
                            
                            if (input.UpdatedSequence.Count == ro.Stories.Count)
                            {
                                for (int i = 0; i < input.UpdatedSequence.Count; i++)
                                {
                                    ro.Stories[i].SequenceId = input.UpdatedSequence[i];
                                    Dispatcher.Invoke(() =>
                                    {
                                        InsertUpdatedSequence(ro.Stories[i].StoryId, input.UpdatedSequence[i]);
                                    });
                                }
                            }
                            ro.Stories = ro.Stories.OrderBy(x => x.SequenceId).ToList();
                            UpgradeUserActionToDB(CommandType.UpdateSequence_Prompter, null, ro.Stories[0].StoryId);
                            Dispatcher.Invoke(() =>
                            {
                                wbHost.ExecuteJavascript("updatedSequenceCallBack('" + "Success" + "');");
                            });
                        }
                        break;
                    }

                case CommandType.Delete_Prompter:
                    {
                        //if (input.RoId != null && input.RoId != "null")
                        //{
                        //    int RoId = int.Parse(input.RoId);

                        //    RunOrder RoToRemove = RunOrders.Where(x => x.RoId == RoId).FirstOrDefault();
                        //    RunOrders.Remove(RoToRemove);
                        //    Dispatcher.Invoke(() =>
                        //    {
                        //        wbHost.ExecuteJavascript("deleteCallBack(1);");
                        //    });
                        //}
                        break;
                    }
                case CommandType.FlipText_Prompter:
                    {
                        if(prompter !=null){
                           flipTex();
                           Dispatcher.Invoke(() =>
                           {
                               wbHost.ExecuteJavascript("flipTextCallBack('"+"Success"+"');");
                           });
                        }
                        else
                        {
                            Toaster("prompt ON First",false);
                        }
                       
                        break;
                    }
                case CommandType.StartCleanUp_Prompter:
                    {
                        DeleteRundownThreadDelayTime = input.ThreadDelayTime;
                        RunDownDeletionTimeSpan = input.DeletionSpan;
                        if (DeleteRundownThread == null && DeleteRundownThreadDelayTime > 0 && RunDownDeletionTimeSpan > 0)
                        {
                            updateConfiguraitonSetting(input.SettingIdA, DeleteRundownThreadDelayTime.ToString());
                            updateConfiguraitonSetting(input.SettingIdB, RunDownDeletionTimeSpan.ToString());
                            DeleteRundownThreadToggler = true;
                            DeleteRundownThread = new Thread(DeletionOfOldRundowns);
                            DeleteRundownThread.Start();
                            UpgradeUserActionToDB(CommandType.StartCleanUp_Prompter,null,null);
                            wbHost.ExecuteJavascript("StartCleanUpCallBack('" + "Success" + "');");
                        }
                        else 
                        {
                            wbHost.ExecuteJavascript("StartCleanUpCallBack('" + "Error" + "');");
                        }
                        break;
                    }
                case CommandType.StopCleanUp_Prompter:
                    {
                        if (DeleteRundownThread != null)
                        {
                            DeleteRundownThreadToggler = false;
                            DeleteRundownThread = null;
                            UpgradeUserActionToDB(CommandType.StopCleanUp_Prompter, null, null);
                            wbHost.ExecuteJavascript("StopCleanUpCallBack('" + "Success" + "');");
                        }
                        else
                        {
                            wbHost.ExecuteJavascript("StopCleanUpCallBack('" + "Error" + "');");
                        }
                        break;
                    }
                case CommandType.SetSpeed_Prompter:
                    {
                        if (input.Step > 0 && prompter != null)
                        {
                            prompter.step = input.Step;
                            wbHost.ExecuteJavascript("setSpeedCallBack('" + "Success" + "');");
                        }
                        else
                        {
                            wbHost.ExecuteJavascript("setSpeedCallBack('" + "Error" + "');");
                        }
                        break;
                    }
                case CommandType.CurrentFontSize_Prompter:
                    {
                        if (input.SelectedFontSize > 0 && prompter != null)
                        {
                            int cIndex = prompter.CurrentStoryIndex;
                            string cFFamily = prompter.CurrentFontFamily;
                            prompter.applyFontSizeOrFontFamily(cIndex, input.SelectedFontSize, cFFamily);
                            wbHost.ExecuteJavascript("setfontSizeCallBack('" + "Success" + "');");
                        }
                        else
                        {
                            wbHost.ExecuteJavascript("setfontSizeCallBack('" + "Error" + "');");
                        }
                        break;
                    }
                case CommandType.CurrentFontFamily_Prompter:
                    {
                        if (input.SelectedFontStyleId !=null && input.SelectedFontStyleId>0 && prompter != null)
                        {
                            FontfamilyRepository cItemFfamily = new FontfamilyRepository();
                            int cIndex = prompter.CurrentStoryIndex;
                            double cFont = prompter.CurrentFontSize;
                            BolUserConsole.Core.Entities.Fontfamily cFontFfamily = cItemFfamily.GetFontfamily(input.SelectedFontStyleId);
                            prompter.applyFontSizeOrFontFamily(cIndex, cFont, cFontFfamily.Name);
                            wbHost.ExecuteJavascript("setfontfamilyCallBack('" + "Success" + "');");
                        }
                        else
                        {
                            wbHost.ExecuteJavascript("setfontfamilyCallBack('" + "Error" + "');");
                        }
                        break;
                    }
                case CommandType.SetProfile_Prompter:
                    {
                        if (!string.IsNullOrEmpty(input.RoId) && prompter != null)
                        {
                            ProfileRepository cItemProfile = new ProfileRepository();
                            FontfamilyRepository cItemFFamily = new FontfamilyRepository();
                            int cIndex = prompter.CurrentStoryIndex;
                            var id = int.Parse(input.RoId);
                            BolUserConsole.Core.Entities.Profile SProfile = cItemProfile.GetProfile(id);
                            BolUserConsole.Core.Entities.Fontfamily SProfileFfamily = cItemFFamily.GetFontfamily(SProfile.FontfamilyId);
                            prompter.applyFontSizeOrFontFamily(cIndex, SProfile.FontSize, SProfileFfamily.Name);
                            wbHost.ExecuteJavascript("setProfileCallBack('" + "Success" + "');");

                        }
                        else
                        {
                            
                            Toaster("Prompt ON first",false);
                        }

                        break;
                    }
                case CommandType.NewProfile_Prompter:
                    {
                        if(input.SelectedFontStyleId !=null)
                        {
                            BolUserConsole.Core.Entities.Profile NProfile = new BolUserConsole.Core.Entities.Profile();
                            NProfile.Name = input.ProfileName;
                            NProfile.FontSize = input.SelectedFontSize;
                            NProfile.FontfamilyId = input.SelectedFontStyleId;
                            NProfile.ScrollSpeed = input.Step;
                            ProfileRepository ProfileToInsert = new ProfileRepository();
                            BolUserConsole.Core.Entities.Profile IProfile=ProfileToInsert.InsertProfile(NProfile);
                            List<BolUserConsole.Core.Entities.Profile> InsertedProfile = new List<BolUserConsole.Core.Entities.Profile>();
                            InsertedProfile.Add(IProfile);
                            UpdateProfilesOnClients(InsertedProfile);
                            wbHost.ExecuteJavascript("newProfileAdderCallBack('" + "Success" + "');");
                        }

                        break;
                    }

                case CommandType.ResetCurrentRunOrder_Prompter:
                    {
                        if (prompter != null && ro!=null)
                        {
                            prompter.ro = null;
                            prompter.Play = false;
                            prompter.p_rtb.Text = string.Empty;
                            Prompter.Index = 0;
                            prompter.ScrollPosition = 0;
                            prompter.LoadPrompter(ro);
                            wbHost.ExecuteJavascript("resetRunOrderCallback('" + "Success" + "');");
                        }
                        else 
                        {
                            Toaster("Select RunOrder First",false);
                        }
                        break;
                    }
                case CommandType.MappingModeOn_Prompter:
                    {
                        if (prompter != null)
                        {
                            prompter.MappingToggler(true);
                            wbHost.ExecuteJavascript("onMappingModeCallBack('" + "Success" + "');");
                        }
                        else
                        {
                            wbHost.ExecuteJavascript("onMappingModeCallBack('" + "Error" + "');");
                        }
                        
                        break;
                    }
                case CommandType.MappingModeOFF_Prompter:
                    {
                        if(prompter != null)
                        {
                            prompter.MappingToggler(false);
                            wbHost.ExecuteJavascript("offMappingModeCallBack('" + "Success" + "');");
                        }
                        else
                        {
                            wbHost.ExecuteJavascript("offMappingModeCallBack('" + "Error" + "');");
              
                        }
                        break;
                    }
                case CommandType.MapActionToKey_Prompter:
                    {
                        if (prompter != null && !string.IsNullOrEmpty(input.ActionToMap))
                        {
                            prompter.CurrentActionToMap = input.ActionToMap;
                        }
                        else
                        {
                            Toaster("Unable to map PrompOn First",false);
                        }
                        break;
                    }
                case CommandType.NextStory_Prompter:
                    {
                        if (prompter != null && prompter.ro != null)
                        {
                            prompter.NextStory();
                        }
                        else
                        {
                            Toaster("Prompt On and Play Rundown first",false);                        
                        }
                        break;
                    }
                case CommandType.PreviousStory_Prompter:
                    {
                        if (prompter != null && prompter.ro != null)
                        {
                            prompter.previousStory();
                        }
                        else
                        {
                            Toaster("Prompt On and Play Rundown first",false);
                        }
                        break;
                    }
                case CommandType.Tolerance_Prompter:
                    {
                        if(input.Tolerance>0 && input.SettingIdA >0)
                        {
                            updateConfiguraitonSetting(input.SettingIdA, input.Tolerance.ToString());
                        }
                        
                        break;
                    }


                case CommandType.DeleteProfile_Prompter:
                      {
                          if(!string.IsNullOrEmpty(input.RoId))
                          {
                              int id= int.Parse(input.RoId);
                              if(id !=null && id >0)
                              {
                                  ProfileRepository ProfileToDelete = new ProfileRepository();
                                  bool b= ProfileToDelete.DeleteProfile(id);
                                  if(b)
                                  {
                                       Dispatcher.Invoke(() =>
                                    {
                                        wbHost.ExecuteJavascript("profileDeleteCallBack('"+"Success"+"');");
                                    });
                                  }
                                  else
                                  {
                                      Dispatcher.Invoke(() =>
                                      {
                                          wbHost.ExecuteJavascript("profileDeleteCallBack('" + "Error" + "');");
                                      });
                                  
                                  }
                                   
                              }
                          }
                          break;
                      }
                default:
                    {
                        Toaster("Switch Default Case", false);
                        break;
                    }
            }
        }

        private void InsertUpdatedSequence(int stId,int seqId)
        {
            StoryRepository updateSequence = new StoryRepository();
            updateSequence.UpdateSeqenceByStoryId(stId, seqId);
        }

        private void ResetPrompter()
        {
            if (prompter != null)
            {
                ro = null;
                prompter.Play = false;
                prompter.ro = null;
                prompter.p_rtb.Text = string.Empty;
                Prompter.Index = 0;
                prompter.ScrollPosition = 0;
            }
        }

        private void NewRunOrderAndStoriesNotification(RunOrderStatus status,BolUserConsole.Core.Entities.RunOrder ro = null )
        {
           
            //Fill in database   
            RunOrderRepository RundOrderRecieved = new RunOrderRepository();
            RundOrderRecieved.InsertRunOrder(ro);

            List<BolUserConsole.Core.Entities.Story> st = ro.Stories;
            StoryRepository StoryRecieved = new StoryRepository();
            for (int i = 0; i < st.Count; i++)
            {
                StoryRecieved.InsertStory(st[i]);
            }
            List<BolUserConsole.Core.Entities.RunOrder> NewRunOrder = new List<BolUserConsole.Core.Entities.RunOrder>();
            NewRunOrder.Add(ro);
            UpdateRunOdersOnClients(status, NewRunOrder);
        }

        private void UpdatedStoriesAndNewStoryItemsNotification(BolUserConsole.Core.Entities.RunOrder ro = null)
        {
             List<BolUserConsole.Core.Entities.Story> st = ro.Stories;

             StoryRepository StoryUpdates = new StoryRepository();
             for (int i = 0; i < st.Count; i++)
             {
                   StoryUpdates.UpdateStory(st[i]);
                   
             }
             StoryItemRepository StoryItemRecieved = new StoryItemRepository();
             for (int i = 0; i < st.Count; i++)
             {
                 if (st[i].StoryItems != null)
                 {
                     for (int j = 0; j < st[i].StoryItems.Count; j++)
                     {
                         StoryItemRecieved.InsertStoryItem(st[i].StoryItems[j]);
                     }
                 }
             
             }
             List<BolUserConsole.Core.Entities.RunOrder> NewRunOrder = new List<BolUserConsole.Core.Entities.RunOrder>();
             NewRunOrder.Add(ro);
             UpdateRunOdersOnClients(RunOrderStatus.UPDATED,NewRunOrder);
        }

        //private void ReadXMLFile(string fileName, CommandSenderType type)
        //{
        //    if (File.Exists(fileName))
        //    {

        //        using (StreamReader reader = new StreamReader(fileName))
        //        {
        //            string xml = reader.ReadToEnd();

        //            if (type == CommandSenderType.Teleprompter)
        //            {
        //                RunOrders = Common.Helper.DeserializeFromXml<List<RunOrder>>(xml);
        //            }
        //            else if (type == CommandSenderType.CasperClient)
        //            {
        //                CasparRunOrders = Common.Helper.DeserializeFromXml<List<ROCreate>>(xml);
        //            }
        //            else { }
        //        }
        //    }
        //}

        private void RunOrderXmlWriter<T>(string FileName, MOS rOrders)
        {
            XmlDocument xdocs = new XmlDocument();
            XmlSerializer serial = new XmlSerializer(rOrders.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                serial.Serialize(xmlStream, rOrders);
                xmlStream.Position = 0;
                xdocs.Load(xmlStream);
            }
            string xamlz = xdocs.InnerXml;
            System.IO.File.WriteAllText(FileName, xamlz, UTF8Encoding.Unicode);     //write file and save
        }

        void ClosePrompter(object sender, CancelEventArgs e)
        {
            if (prompter != null && prompter.IsInitialized)
            {
                prompter.Hide();
                prompter = null;
            }
            if (PrompterP != null && PrompterP.IsInitialized)
            {
                PrompterP.Hide();
                PrompterP = null;
            }
        }

        private void UpdateChanges(string xmlFileName)
        {
            //RunOrderXmlWriter<RunOrder>(xmlFileName, RunOrders);
        }

        public void Toaster(string msg, bool b)
        {
            Dispatcher.Invoke(() =>
            {
                wbHost.ExecuteJavascript("showToaster('" + msg+ "' , '"+b+"');");

            });
        }

        //private void DeleteFromXmlAndList()
        //{
        //    try
        //    {
        //        for (var i = 0; i < RunOrders.Count; i++)
        //        {
        //            TimeSpan Difference = new TimeSpan();
        //            DateTime StartTime =RunOrders[i].StartTime;
        //            DateTime CurrentTime = DateTime.Now;
        //            Difference = CurrentTime - StartTime;
        //            if (Difference.TotalHours > RunDownDeletionTimeSpan)
        //            {
        //                RunOrders.Remove(RunOrders[i]);
        //            }
        //        }
        //        UpdateChanges(TelePrompterXmlFileName);
        //        Thread.Sleep(DeleteRundownThreadDelayTime);        //variable from config
        //        if (DeleteRundownThreadToggler)                    // variable for thread stop
        //        {
        //            DeleteFromXmlAndList();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        // log errors
        //    }
        //}


        private void LoadInitialData() 
        {

            //Loading FontFamilies
            FontfamilyRepository LoadFontFamilies = new FontfamilyRepository();
            List<BolUserConsole.Core.Entities.Fontfamily> LoadFFamilies = new List<BolUserConsole.Core.Entities.Fontfamily>();
            LoadFFamilies.AddRange(LoadFontFamilies.GetAllFontfamily());
            string fontFamilies = serializer.Serialize(LoadFFamilies);
            wbHost.ExecuteJavascript("loadFontfamilyCallBack(" + fontFamilies + ");");

            //Load Settings
            ConfigSettingRepository LoadSettings = new ConfigSettingRepository();
            List<BolUserConsole.Core.Entities.ConfigSetting> AllSettings = new List<BolUserConsole.Core.Entities.ConfigSetting>();
            AllSettings.AddRange(LoadSettings.GetAllConfigSetting());
            string settings = serializer.Serialize(AllSettings);
            wbHost.ExecuteJavascript("loadSettingsCallBack(" + settings + ");");

            //Loading Profiles
            ProfileRepository LoadProfile = new ProfileRepository();
            List<BolUserConsole.Core.Entities.Profile> AllProfiles = new List<BolUserConsole.Core.Entities.Profile>(); 
            AllProfiles.AddRange(LoadProfile.GetAllProfile());
            UpdateProfilesOnClients(AllProfiles);

            //Loading Rundowns
            RunOrderRepository LoadRundOrders = new RunOrderRepository();
            List<RunOrder> runorderList= LoadRundOrders.GetAllRunOrder();
            StoryRepository LoadStories = new StoryRepository();
            List<BolUserConsole.Core.Entities.Story> storyList=LoadStories.GetAllStory();
            StoryItemRepository LoadStoryItems = new StoryItemRepository();
            List<BolUserConsole.Core.Entities.StoryItem> storyItemList=LoadStoryItems.GetAllStoryItem();

            if(runorderList !=null)
            {
                AllRunOrders.AddRange(runorderList);            
            }
            if (storyList!= null)
            {
                for (int i = 0; i < AllRunOrders.Count; i++)
                {
                    AllRunOrders[i].Stories = new List<BolUserConsole.Core.Entities.Story>();
                    for (int j = 0; j < storyList.Count; j++ )
                      {
                          if(AllRunOrders[i].RoId == storyList[j].RoId)
                          {
                             AllRunOrders[i].Stories.Add(storyList[j]);  
                          }
                      }
                }

            } 
            if (storyItemList!= null)
            {
                for (int i = 0; i < AllRunOrders.Count; i++)
                {
                    for (int j = 0; j < AllRunOrders[i].Stories.Count; j++)
                    {
                        AllRunOrders[i].Stories[j].StoryItems = new List<BolUserConsole.Core.Entities.StoryItem>();
                        for (int k = 0; k < storyItemList.Count; k++)
                        {
                            if (AllRunOrders[i].Stories[j].StoryId == storyItemList[k].StoryId)
                            {
                                AllRunOrders[i].Stories[j].StoryItems.Add(storyItemList[k]);
                            }
                        }
                    }
                }
            }

            UpdateRunOdersOnClients(RunOrderStatus.OLD, AllRunOrders);
        }

        //client Updates--------------
        private void UpdateProfilesOnClients(List<BolUserConsole.Core.Entities.Profile> p)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
             string profiles = serializer.Serialize(p);
             wbHost.ExecuteJavascript("loadProfileCallBack(" + profiles + ");");
        }

        private void UpdateRunOdersOnClients(RunOrderStatus status,List<BolUserConsole.Core.Entities.RunOrder> runOrderList)
         {
            this.Dispatcher.BeginInvoke(new Action(() => {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string json = serializer.Serialize(runOrderList);
                wbHost.ExecuteJavascript("recieveRundowns('" + json + "','" + status.ToString() + "');");      
            }));
            
        }

        //Function called by thread to delete rundown
        private void DeletionOfOldRundowns() 
        {
                        try
                        {
                            if (AllRunOrders.Count >0) 
                            {
                                for (var i = 0; i < AllRunOrders.Count; i++)
                                {
                                    TimeSpan Difference = new TimeSpan();
                                    DateTime StartTime = AllRunOrders[i].StartTime;
                                    DateTime CurrentTime = DateTime.Now;
                                    Difference = CurrentTime - StartTime;
                                    if (Difference.TotalHours > RunDownDeletionTimeSpan)
                                    {
                                        mosClient = new MOSClient("bolnews", "10.1.20.33", 10541, "ncsid");
                                        string message = string.Empty;
                                        mosClient.Connect(out message);
                                        if (mosClient.IsConnected)
                                        {
                                            MOS obj = SendRunOrder(out message, AllRunOrders[i]);
                                            if (obj != null)
                                            {
                                                int Roid = AllRunOrders[i].RoId;
                                                StoryItemRepository StoryItemsToRemove = new StoryItemRepository();

                                                for (int j = 0; j < AllRunOrders[i].Stories.Count; j++)
                                                {
                                                    StoryItemsToRemove.DeleteStoryItemByStoryId(AllRunOrders[i].Stories[j].StoryId);
                                                }

                                                UserActionRepository userActionToDelete = new UserActionRepository();
                                                userActionToDelete.DeleteUserActionByRoId(Roid);

                                                StoryRepository StoriesToRemove = new StoryRepository();
                                                for (int k = 0; k < AllRunOrders[i].Stories.Count; k++)
                                                {
                                                    StoriesToRemove.DeleteStoryByRoId(AllRunOrders[i].RoId);
                                                }
                                                RunOrderRepository RundownToRemove = new RunOrderRepository();
                                                RundownToRemove.DeleteRunOrder(AllRunOrders[i].RoId);
                                                AllRunOrders.Remove(AllRunOrders[i]);
                                                Dispatcher.Invoke(() =>
                                                {
                                                    wbHost.ExecuteJavascript("deleteCallBack('"+Roid+"');");
                                                });
                                            }
                                            else
                                            {
                                                Toaster("No Acknowledgment From server try later", false);
                                                DeleteRundownThreadToggler = false;
                                            }

                                        }
                                        else
                                        {
                                            Toaster("No older rundownExist", false);
                                            DeleteRundownThreadToggler = false;
                                        }
                                    }
                                }
                                
                            }
                            else
                            {
                                Toaster("No Rundowns Found in database", false);
                                DeleteRundownThreadToggler = false;
                            }
                            Thread.Sleep(DeleteRundownThreadDelayTime*60000);        //variable from config
                            if (DeleteRundownThreadToggler)                    // variable for thread stop
                            {
                                DeletionOfOldRundowns();
                            }

                        }
                        catch (Exception ex)
                        {
                            // log errors 
                        }
        
        }

        //Upgrade actions---------
        private void UpgradeUserActionToDB(CommandType c, int? roId, int? stId) 
        {
            UserActionRepository UpgradeAction = new UserActionRepository();
            UserAction ActionPerform = new UserAction();
            CommandType cmd=c;
            switch(cmd)
            {
                case CommandType.UpdateSequence_Prompter:
                    {
                            ActionPerform.UserId = CurrentUserId;
                            ActionPerform.ActionId = (int)ActionPerfomed.SEQUENCE_CHANGE;
                            ActionPerform.CreationDate = DateTime.UtcNow;
                            ActionPerform.RoId = ro.RoId;
                            ActionPerform.StoryId = stId;
                            UpgradeAction.InsertUserAction(ActionPerform);
 
                        break;
                    }
                case CommandType.Skip_Prompter:
                    {
                            ActionPerform.UserId = CurrentUserId;
                            ActionPerform.ActionId = (int)ActionPerfomed.SKIP;
                            ActionPerform.CreationDate = DateTime.UtcNow;
                            ActionPerform.RoId = ro.RoId;
                            ActionPerform.StoryId = stId;
                            UpgradeAction.InsertUserAction(ActionPerform);

                        break;
                    }
                case CommandType.UnSkip_Prompter:
                    {
                            ActionPerform.UserId = CurrentUserId;
                            ActionPerform.ActionId = (int)ActionPerfomed.UNSKIP;
                            ActionPerform.CreationDate = DateTime.UtcNow;
                            ActionPerform.RoId = ro.RoId;
                            ActionPerform.StoryId = stId;
                            UpgradeAction.InsertUserAction(ActionPerform);

                        break;
                    }
               
                default:
                   {
                        
                        break;
                   }
            }
        }

        // Runorder back to NMS-----
        public MOS SendRunOrder(out string message,RunOrder RunOrderToSend)
        {
            message = string.Empty;
            MOS mosResponse = new MOS();

            try
            {
                if (mosClient != null && mosClient.IsConnected)
                {
                    MOS mos = GetEntity(RunOrderToSend);
                    mosResponse = mosClient.SendMOSCommand(mos, out message);
                }
                else
                {
                    message = "Error: Not Connected.";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return mosResponse;
        }

        // Wrapping Entity in MosCommand called by SendRunOrder function
        public MOS GetEntity(RunOrder rToSend)
        {

            MOS command = new MOS();
            command.mosID = "bolnews";
            command.ncsID = "ncsid";
            UserActionRepository get = new UserActionRepository();
            List<UserAction> UserActions = new List<UserAction>();
            UserActions.AddRange(get.GetUserActionByRoId(rToSend.RoId));
            rToSend.UserActionsPerformed = new List<UserAction>();
            rToSend.UserActionsPerformed.AddRange(UserActions);

            //map to mos entity roToSend contains whole data to send;
            MOSRunOrder RunorderToSend = new MOSRunOrder();
            RunorderToSend.CopyFrom(rToSend);
            command.command = RunorderToSend;

            return command;
        }

        //Update configSettingsValue
        private void updateConfiguraitonSetting(int id,string value)
        {
            int settingId = id;
            ConfigSettingRepository update = new ConfigSettingRepository();
            switch ((ConfigurationSetting)settingId)
            {
                case ConfigurationSetting.MosId:
                    {
                        update.UpdateConfigSettingValueById(settingId, value);
                        break;
                    }
                case ConfigurationSetting.MosPort:
                    {
                        update.UpdateConfigSettingValueById(settingId, value);
                        break;
                    }
                case ConfigurationSetting.NcsId:
                    {
                        update.UpdateConfigSettingValueById(settingId, value);

                        break;
                    }
                case ConfigurationSetting.NcsPort:
                    {
                        update.UpdateConfigSettingValueById(settingId, value);

                        break;
                    }
                case ConfigurationSetting.IpAdress:
                    {
                        update.UpdateConfigSettingValueById(settingId, value);

                        break;
                    }
                case ConfigurationSetting.Tolerance:
                    {
                        
                        break;
                    }
                case ConfigurationSetting.AutoCheck:
                    {
                        update.UpdateConfigSettingValueById(settingId, value);

                        break;
                    }
                case ConfigurationSetting.AutoDelete:
                    {
                        update.UpdateConfigSettingValueById(settingId, value);

                        break;
                    }

                default:
                    {

                        break;
                    }
            }


        }

        private void ReadXMLFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    string xml = reader.ReadToEnd();
                    MOS LoadedRunOrder = Common.Helper.DeserializeFromXml<MOS>(xml);
                    //RunOrderReceived(LoadedRunOrder);
                    //StoryInfoReceived(LoadedRunOrder);
                }
            }
        }

    }
}

