﻿using System;
using Faker;
using System.Collections.Generic;
using NMS.Core.Teleprompter.Entities;

namespace NMS.UserConsole
{
    public class Mock
    {
        public List<RunOrder> GetMockRuOrders(int number = 1,int numberOfStories=1,int numberOfScriptsPerStory=1 )
        {
            List<RunOrder> list = new List<RunOrder>();

            for (int i = 0; i < number;i++ )
            {
                list.Add(GetMockRunOrder(numberOfStories, numberOfScriptsPerStory));
            }

            return list;
        }

        public RunOrder GetMockRunOrder(int numberOfStories=1,int numberOfScriptsPerStory=1)
        {
            RunOrder ro = new RunOrder();
            ro.ID = StringFaker.Numeric(2);
            ro.Slug = StringFaker.Alpha(100);
           // ro.StartTime = DateTimeFaker.DateTime(DateTime.Now.AddHours(1), DateTime.Now.AddHours(2)).ToUniversalTime().ToString();
            ro.Duration = "00:59:00";
            ro.Stories = new System.Collections.Generic.List<Story>();

            for (int i = 0; i < numberOfStories;i++)
            {
                ro.Stories.Add(GetMockStory(ro.ID, i));
                ro.Stories[i].StoryItems= new System.Collections.Generic.List<StoryItem>();

                for (int j = 0; j < numberOfScriptsPerStory; j++)
                {
                    ro.Stories[i].StoryItems.Add(GetMockStoryItem(ro.Stories[i].ID, j));
                }
            }

            return ro;
        }

        public Story GetMockStory(string runOrderId,int sequenceId=1)
        {
            Story story = new Story();
            story.RoId = runOrderId;
            story.ID = StringFaker.Numeric(2);
            story.Slug = StringFaker.Alpha(100);
            story.SequenceId = sequenceId;
            //story.


            return story;
        }

        public StoryItem GetMockStoryItem(string storyId, int sequenceId = 1)
        {
            StoryItem item = new StoryItem();
            item.Detail = StringFaker.Alpha(100);
            item.ID = StringFaker.Numeric(2); ;
            item.StoryId = storyId;
            item.Slug = StringFaker.Alpha(100);
            item.Instructions = StringFaker.Alpha(10);
            item.SequenceId = sequenceId;
            return item;
        }
    }
}
