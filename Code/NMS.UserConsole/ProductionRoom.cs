﻿using Awesomium.Core;
using NMS.UserConsole.Entities;
using NMS.UserConsole.Enums;
using System;
using System.Web.Script.Serialization;
using System.Windows;
using System.Linq;
using NMS.UserConsole.View;
using NMS.Core.Teleprompter.Entities;
using NMS.Core.Teleprompter;

namespace NMS.UserConsole
{
    public partial class MainWindow : Window
    {

        public void SendCommand(object obj, JavascriptMethodEventArgs jsMethodArgs)
        {
            try
            {
                int senderTypeId = int.Parse(jsMethodArgs.Arguments[0]);
                int commandTypeId = int.Parse(jsMethodArgs.Arguments[1]);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                PostInput input = serializer.Deserialize<PostInput>(jsMethodArgs.Arguments[2]);
                ExecuteCommand((CommandSenderType)senderTypeId, (CommandType)commandTypeId, input);
            }
            catch(Exception ex){
                ExceptionLogger.LogMessage(ex.Message);
            }
        }

        public void ExecuteCommand(CommandSenderType senderType, CommandType commandType, PostInput input)
        {
            switch (senderType)
            {
                case CommandSenderType.Teleprompter:
                    ExecuteTeleprompterCommand(commandType,input);
                    break;
                case CommandSenderType.CasperClient:
                    ExecuteCasperCommand(commandType, input);
                    break;
                case CommandSenderType.PcrUser:
                    ExecutePcrUserCommand(commandType, input);
                    break;
                default:
                    break;
            }
        }
    }
}
