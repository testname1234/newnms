﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.IController;

namespace NMS.Web.API
{
    public class CategoryController : ApiController, ICategoryController
    {
        ICategoryService categoryService = IoC.Resolve<ICategoryService>("CategoryService");

        [HttpGet]
        [ActionName("GetCategoryByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>> GetCategoryByTerm(string id = null)
        {
            if (id == null)
                id = string.Empty;
            DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>>();
            try
            {
                List<Category> categorys = categoryService.GetCategoryByTerm(id);
                transfer.Data = new List<Core.DataTransfer.Category.GetOutput>();
                if (categorys != null)
                    transfer.Data.CopyFrom(categorys);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetByDate")]
        public DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>> GetByDate(string LastUpdatedDate)
        {         
            DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Category.GetOutput>>();
            try
            {
                List<Category> categorys = categoryService.GetByDate(Convert.ToDateTime(LastUpdatedDate));
                transfer.Data = new List<Core.DataTransfer.Category.GetOutput>();
                if (categorys != null)
                    transfer.Data.CopyFrom(categorys);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }
    }
}
