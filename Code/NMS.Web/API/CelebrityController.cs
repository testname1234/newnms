﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using NMS.Core;
using NMS.Core.IController;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;
using System.Linq;
using NMS.Core.DataTransfer.Program;

namespace NMS.Web.API
{
    public class CelebrityController : ApiController , ICelebrityController
    {
        ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
        ICelebrityStatisticService celebrityStatisticService = IoC.Resolve<ICelebrityStatisticService>("CelebrityStatisticService");

        [HttpGet]
        [ActionName("GetCelebrityByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>> GetCelebrityByTerm(string Term = "")
        {
            DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Celebrity.GetOutput>>();
            try
            {
                List<Celebrity> celebrities = celebrityService.GetCelebrityByTerm(Term == null ? "" : Term);
                transfer.Data = new List<Core.DataTransfer.Celebrity.GetOutput>();

                if (celebrities != null && celebrities.Count > 0)
                {
                    foreach (Celebrity celebrity in celebrities)
                    {
                        List<CelebrityStatistic> celebrityStatistics = celebrityStatisticService.GetCelebrityStatisticByCelebrityId(celebrity.CelebrityId);
                        celebrity.CelebrityStatistics = celebrityStatistics;
                    }
                    transfer.Data.CopyFrom(celebrities);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }   
    }
}
