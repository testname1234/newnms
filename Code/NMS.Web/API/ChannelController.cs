﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.IController;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChannelVideo;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;

namespace NMS.Web.API
{
    public class ChannelController : ApiController , IChannelController
    {

        IChannelVideoService channelVideosService = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
        IChannelService channelService = IoC.Resolve<IChannelService>("ChannelService");
        IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");


        [HttpGet]
        [ActionName("LoadChannelInitialData")]
        public DataTransfer<LoadChannelOutput> LoadChannelInitialData(string id)
        {
            DataTransfer<LoadChannelOutput> transfer = new DataTransfer<LoadChannelOutput>();
            try
            {
              
                if (id != null)
                {
                    transfer.Data = new LoadChannelOutput();

                    List<Channel> channels = channelService.GetChannelsByOtherChannelBit(true);
                    if (channels != null && channels.Count > 0)
                    {
                        transfer.Data.Channels = new List<Core.DataTransfer.Channel.GetOutput>();
                        transfer.Data.Channels.CopyFrom(channels);
                    }

                    DateTime dt = new DateTime();
                    if (DateTime.TryParse(id, out dt))
                    {
                        // dt = dt.ToUniversalTime();
                        var from = dt.Date.AddHours(-5);
                        var to = dt.Date.AddDays(1).AddHours(-5).AddMilliseconds(-1);
                        List<ChannelVideo> channelVideos = channelVideosService.GetAllChannelVideosBetweenDates(from, to);
                        if (channelVideos != null && channelVideos.Count > 0)
                        {
                            transfer.Data.ChannelVideos = new List<Core.DataTransfer.ChannelVideo.GetOutput>();
                            transfer.Data.ChannelVideos.CopyFrom(channelVideos);
                        }
                    }
                    
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetTotalTime")]
        public DataTransfer<NMS.Core.DataTransfer.ChannelVideo.GetOutput> GetTotalTime(NMS.Core.DataTransfer.ChannelVideo.PostOutput input)
        {
            DataTransfer<NMS.Core.DataTransfer.ChannelVideo.GetOutput> transfer = new DataTransfer<GetOutput>();
            try
            {
                transfer.Data = new GetOutput();
                transfer.Data.Url = input.Url.Replace("http:", "").Replace("/", @"\");
                transfer.Data.DurationSeconds = channelVideosService.GetDuration(transfer.Data.Url);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("RefreshData")]
        public DataTransfer<LoadChannelOutput> RefreshData(string id,bool? isLive=false )
        {
            DataTransfer<LoadChannelOutput> transfer = new DataTransfer<LoadChannelOutput>();
            try
            {
                DateTime dt = new DateTime();
                if (id != null && DateTime.TryParse(id, out dt))
                {
                    transfer.Data = new LoadChannelOutput();
                    // dt = dt.ToUniversalTime();
                    var from = dt;
                    var to = dt.Date.AddDays(1).AddMilliseconds(-1);

                    List<ChannelVideo> channelVideos = new List<ChannelVideo>();
                    if (isLive == false)
                    {
                        channelVideos = channelVideosService.GetAllChannelVideosBetweenDates(from, to);
                    }
                    else
                    {
                        channelVideos = channelVideosService.GetAllChannelLiveTCS(from, to);
                    }

                    if (channelVideos != null && channelVideos.Count > 0)
                    {
                        transfer.Data.ChannelVideos = new List<Core.DataTransfer.ChannelVideo.GetOutput>();
                        transfer.Data.ChannelVideos.CopyFrom(channelVideos);
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetChannelVideos")]
        public DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>> GetChannelVideos(GetChannelVideoInput input)
        {
            DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.ChannelVideo.GetOutput>>();
            try
            {
                List<ChannelVideo> channelVideos = channelVideosService.GetAllChannelVideosBetweenDates(input.From,input.To);
                transfer.Data = new List<Core.DataTransfer.ChannelVideo.GetOutput>();
                if (channelVideos != null)
                    transfer.Data.CopyFrom(channelVideos);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetAll")]
        public DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>> GetAll()
        {
            try
            {
                return channelService.GetAll();
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Channel.GetOutput>>();
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
                return transfer;
            }
        }

         [HttpGet]
         [ActionName("GetAllPrograms")]
         public DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> GetAllPrograms()
         {
             try
             {
                 return programService.GetAll();
             }
             catch (Exception exp)
             {
                 ExceptionLogger.Log(exp);
                 DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>>();
                 transfer.IsSuccess = false;
                 transfer.Errors = new string[] { "An error occured" };
                 return transfer;
             }
             
         }
    }
}
