﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.IController;
using NMS.Core.Models;
using System.Threading;
using NMS.Core.Enums;
using NMS.Core.DataTransfer.EditorialComment;


namespace NMS.Web.API
{
      
    public class EditorialCommentController : ApiController
    {
        IEditorialCommentService EditorCommentservice = IoC.Resolve<IEditorialCommentService>("EditorialCommentService");

        [HttpPost]
        [ActionName("AddComment")]
        public DataTransfer<Core.DataTransfer.EditorialComment.GetOutput> AddComment(NMS.Core.DataTransfer.EditorialComment.PostInput input)
        {
            DataTransfer<Core.DataTransfer.EditorialComment.GetOutput> transfer = new DataTransfer<Core.DataTransfer.EditorialComment.GetOutput>();
            try
            {
                if (input != null)
                {
                    var obj = EditorCommentservice.AddComment(input);
                    EditorialComment edComment = new EditorialComment();
                    edComment.CopyFrom(obj);
                    transfer.Data = new Core.DataTransfer.EditorialComment.GetOutput();
                    transfer.Data.EditorialComments = new List<EditorialComment>();
                    if (input.SlotId != null && input.SlotId != 0)
                        edComment.SlotId = input.SlotId;

                    transfer.Data.EditorialComments.Add(edComment);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }
        

        [HttpGet]
        [ActionName("GetComment")]
        public DataTransfer<Core.DataTransfer.EditorialComment.GetOutput> GetComment(NMS.Core.DataTransfer.EditorialComment.PostInput input)
        {
            DataTransfer<Core.DataTransfer.EditorialComment.GetOutput> transfer = new DataTransfer<Core.DataTransfer.EditorialComment.GetOutput>();
            try
            {
                if (input != null)
                {
                    var obj = EditorCommentservice.GetComment(input);
                    transfer.Data.EditorialComments.CopyFrom(obj);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetCommentByTickerId")]
        public DataTransfer<Core.DataTransfer.EditorialComment.GetOutput> GetCommentByTickerId(int tickerId, int userRoleId)
        {
            DataTransfer<Core.DataTransfer.EditorialComment.GetOutput> transfer = new DataTransfer<Core.DataTransfer.EditorialComment.GetOutput>();
            try
            {
                if (tickerId > 0)
                {
                    var obj = EditorCommentservice.GetCommentByTickerId(tickerId, userRoleId);
                    transfer.Data = new GetOutput();
                    transfer.Data.EditorialComments = new List<EditorialComment>();
                    transfer.Data.EditorialComments.CopyFrom(obj);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

    }
}
