﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.IService;
using NMS.Core.IController;

namespace NMS.Web.API
{
    public class FilterController : ApiController, IFilterController
    {
        IFilterService filterService = IoC.Resolve<IFilterService>("FilterService");
        IFilterTypeService filterTypeService = IoC.Resolve<IFilterTypeService>("FilterTypeService");

        [HttpGet]
        [ActionName("GetAllFilters")]
        public DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>> GetAllFilters()
        {
            List<NMS.Core.DataTransfer.Filter.GetOutput> outputLst = new List<Core.DataTransfer.Filter.GetOutput>();
            DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>>();
            try
            {
                transfer.Data = filterService.GetAll().Data;
            }
            catch
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = "Exception occur.";
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetAllFilterTypes")]
        public DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>> GetAllFilterTypes()
        {
            List<NMS.Core.DataTransfer.FilterType.GetOutput> outputLst = new List<Core.DataTransfer.FilterType.GetOutput>();
            DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.FilterType.GetOutput>>();
            try
            {
                return filterTypeService.GetAll();
            }
            catch
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = "Exception occur.";
            }

            return transfer;
        }
    }
}
