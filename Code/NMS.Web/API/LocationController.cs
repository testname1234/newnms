﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.IController;

namespace NMS.Web.API
{
    public class LocationController : ApiController, ILocationController
    {
        ILocationService locationService = IoC.Resolve<ILocationService>("LocationService");

        [HttpGet]
        [ActionName("GetLocationByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Location.GetOutput>> GetLocationByTerm(string id = null)
        {
            if (id == null)
                id = string.Empty;
            DataTransfer<List<NMS.Core.DataTransfer.Location.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Location.GetOutput>>();
            try
            {
                List<Location> locations = locationService.GetLocationByTerm(id);
                transfer.Data = new List<Core.DataTransfer.Location.GetOutput>();
                if (locations != null)
                    transfer.Data.CopyFrom(locations);
            }
            catch(Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetByDate")]
        public DataTransfer<List<NMS.Core.DataTransfer.Location.GetOutput>> GetByDate(string LastUpdatedDate)
        {          
            DataTransfer<List<NMS.Core.DataTransfer.Location.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Location.GetOutput>>();
            try
            {                
                List<Location> locations = locationService.GetByDate(Convert.ToDateTime(LastUpdatedDate));
                transfer.Data = new List<Core.DataTransfer.Location.GetOutput>();
                if (locations != null)
                    transfer.Data.CopyFrom(locations);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }
    }
}
