﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataInterfaces;
using NMS.Core.Entities;
using NMS.Core.IController;
using NMS.Core.Models;
using NMS.Core.DataTransfer;
using NMS.Core.DataInterfaces.Mongo;
using NMS.Core.IService;
using NMS.Core.Entities.Mongo;
using NMS.Core.Helper;
using NMS.Core.DataTransfer.News;

namespace NMS.Web.API
{
    public class MosTestController : ApiController, IMosTestController
    {
        public MosTestController() { }
        public static List<Guid> ResourceTokens = new List<Guid>();

        IMosActiveItemService mosService = IoC.Resolve<IMosActiveItemService>("MosActiveItemService");



        #region MosItems

        [HttpGet]
        [ActionName("GetMosById")]
        public DataTransfer<List<Core.DataTransfer.MosActiveItem.GetOutput>> GetMosActiveItem(int id)
        {
            DataTransfer<List<Core.DataTransfer.MosActiveItem.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.MosActiveItem.GetOutput>>();
            try
            {
                transfer.Data = new List<Core.DataTransfer.MosActiveItem.GetOutput>();
                MosActiveItem mosAct = mosService.GetMosActiveItem(Convert.ToInt32(id));
                Core.DataTransfer.MosActiveItem.GetOutput _mosActive = new Core.DataTransfer.MosActiveItem.GetOutput();
                _mosActive.CopyFrom(mosAct);
                transfer.Data.Add(_mosActive);

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }






        #endregion
    }
}
