﻿using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.News;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Helper;
using NMS.Core.IController;
using NMS.Core.IService;
using NMS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using SolrManager;
using SolrManager.InputEntities;
using NMS.Core.Enums;
using System.Web;

namespace NMS.Web.API
{
    public class NewsController : ApiController, INewsController
    {
        public NewsController()
        {
        }

        public static List<Guid> ResourceTokens = new List<Guid>();

        private IFilterService filterService = IoC.Resolve<IFilterService>("FilterService");
        private IFilterTypeService filterTypeService = IoC.Resolve<IFilterTypeService>("FilterTypeService");
        private INewsService newsService = IoC.Resolve<INewsService>("NewsService");
        private IChannelService channelService = IoC.Resolve<IChannelService>("ChannelService");
        private IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
        private IProgramScheduleService programScheduleService = IoC.Resolve<IProgramScheduleService>("ProgramScheduleService");
        private IScreenTemplateService screenTemplateService = IoC.Resolve<IScreenTemplateService>("ScreenTemplateService");
        private IScreenElementService screenElementService = IoC.Resolve<IScreenElementService>("ScreenElementService");
        private IMessageService messageService = IoC.Resolve<IMessageService>("MessageService");
        private IEventInfoService eventInfoService = IoC.Resolve<IEventInfoService>("EventInfoService");
        private IAlertService alertService = IoC.Resolve<IAlertService>("AlertService");
        private INotificationService notificationService = IoC.Resolve<INotificationService>("NotificationService");
        private IUserManagementService userManagementService = IoC.Resolve<IUserManagementService>("UserManagementService");
        private IScriptService scriptService = IoC.Resolve<IScriptService>("ScriptService");
        private IProgramTypeMappingService programTypeMappingService = IoC.Resolve<IProgramTypeMappingService>("ProgramTypeMappingService");
        private IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
        private ITickerService tickerService = IoC.Resolve<ITickerService>("TickerService");
        private ITickerLineService tickerLineService = IoC.Resolve<ITickerLineService>("TickerLineService");
        private ITickerCategoryService tickerCategoryService = IoC.Resolve<ITickerCategoryService>("TickerCategoryService");
        private ITeamService teamService = IoC.Resolve<ITeamService>("TeamService");
        private IVideoCutterIpService videCutterIpService = IoC.Resolve<IVideoCutterIpService>("VideoCutterIpService");
        private ILocationService locationService = IoC.Resolve<ILocationService>("LocationService");
        private INewsBucketService newsBucketService = IoC.Resolve<INewsBucketService>("NewsBucketService");
        private IOnAirTickerService onAirTickerService = IoC.Resolve<IOnAirTickerService>("OnAirTickerService");
        private IOnAirTickerLineService onAirTickerLineService = IoC.Resolve<IOnAirTickerLineService>("OnAirTickerLineService");

        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();


        #region Producer

        [HttpPost]
        [ActionName("LoadBolNewsData")]
        public DataTransfer<BolNewsDataOutput> LoadInitialData(BolNewsDataInput input)
        {
            DataTransfer<BolNewsDataOutput> transfer = new DataTransfer<BolNewsDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new BolNewsDataOutput();
                    List<Filter> filters = new List<Filter>();
                    filters.CopyFrom(input.Filters);
                    List<MNews> news = newsService.GetNewsByFilterIds(filters, input.NewsLastUpdateDate, input.PageSize);

                    if (news != null && news.Count > 0)
                    {
                        transfer.Data.News = new List<Core.DataTransfer.News.GetOutput>();
                        transfer.Data.News.CopyFrom(news);
                    }
                    //transfer.Data.Filters = filterService.GetAll().Data;
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("LoadInitialDataNMS")]
        public DataTransfer<LoadInitialDataOutput> LoadInitialDataNMS(LoadInitialDataInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    DateTime dt = DateTime.UtcNow;
                    transfer.Data = new LoadInitialDataOutput();

                    List<Filter> filters = new List<Filter>();
                    if (input.Filters == null || input.Filters.Count == 0)
                    {
                        filters.Add(new Filter() { FilterTypeId = 16, FilterId = 78 });
                    }
                    else filters.CopyFrom(input.Filters);

                    List<Filter> discardedFilters = null;
                    if (input.FiltersToDiscard != null)
                    {
                        discardedFilters = new List<Filter>();
                        discardedFilters.CopyFrom(input.FiltersToDiscard);
                        discardedFilters.RemoveAll(x => filters.Any(y => y.FilterId == x.FilterId));
                    }


                    LoadNews loadNews = newsService.GetNewsAndBunchCached(filters, input.PageCount, input.StartIndex, input.From, input.To, input.Term, discardedFilters);

                    dt = DateTime.UtcNow;
                    if (loadNews.News != null && loadNews.News.Count > 0)
                    {
                        transfer.Data.News = loadNews.News;
                    }

                    if (loadNews.Bunchs != null && loadNews.Bunchs.Count > 0)
                    {
                        transfer.Data.Bunch = loadNews.Bunchs;
                    }

                    List<MFilterCount> filterCount = newsService.GetFilterCountByLastUpdateDate(filters, input.From, input.To, input.ProgramFilterIds);
                    System.Diagnostics.Debug.WriteLine("GetFilterCountByLastUpdateDate: {0}", DateTime.UtcNow.Subtract(dt));
                    dt = DateTime.UtcNow;
                    if (filterCount != null && filterCount.Count > 0)
                    {
                        transfer.Data.FilterCounts = new List<Core.DataTransfer.FilterCount.GetOutput>();
                        transfer.Data.FilterCounts.CopyFrom(filterCount);
                    }

                    transfer.Data.Term = input.Term;
                    
                    var _filters = filterService.GetAll().Data;
                    List<int> removeFilterTypes = new List<int>() { (int)FilterTypes.Package, (int)FilterTypes.Radio, (int)FilterTypes.Records, (int)FilterTypes.NewsPaper, (int)FilterTypes.PublicReporter, (int)FilterTypes.Program, (int)FilterTypes.PressRelease };
                    _filters.RemoveAll(x => removeFilterTypes.Contains(x.FilterTypeId));
                    transfer.Data.Filters = _filters;

                    if (transfer.Data.Programs != null)
                    {
                        transfer.Data.Filters.RemoveAll(x => !transfer.Data.Programs.Where(y => y.FilterId.HasValue).Select(z => z.FilterId.Value).Contains(x.FilterId) && x.FilterTypeId == (int)FilterTypes.Program && x.ParentId.HasValue);
                    }

                    List<TickerImage> tickerImages = new List<TickerImage>();

                    List<int> tickerCategoryIds = new List<int> { (int)TickerTypes.BreakingA };
                    transfer.Data.AllTickers = new List<NMS.Core.DataTransfer.Ticker.GetOutput>();
                    transfer.Data.AllTickers.CopyFrom(tickerService.GetTickersByCategoryIds(tickerCategoryIds));
                    
                    transfer.Data.users = new List<Core.DataTransfer.User.GetOutput>();
                    transfer.Data.users.CopyFrom(userManagementService.GetAllUsers());
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }


        [HttpPost]
        [ActionName("GetAllUsers")]
        public DataTransfer<LoadInitialDataOutput> GetAllUsers(LoadInitialDataInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new LoadInitialDataOutput();
                    transfer.Data.users = new List<Core.DataTransfer.User.GetOutput>();
                    transfer.Data.users.CopyFrom(userManagementService.GetAllUsers());
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("LoadInitialData")]
        public DataTransfer<LoadInitialDataOutput> LoadInitialData(LoadInitialDataInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    DateTime dt = DateTime.UtcNow;
                    transfer.Data = new LoadInitialDataOutput();

                    dt = DateTime.UtcNow;
                    List<Channel> channels = channelService.GetChannelsByUserId(input.UserId);
                    if (channels != null && channels.Count > 0)
                    {
                        transfer.Data.Channels = new List<Core.DataTransfer.Channel.GetOutput>();
                        transfer.Data.Channels.CopyFrom(channels);

                        List<Program> programs = new List<Program>();
                        List<ProgramSchedule> programSchedules = new List<ProgramSchedule>();
                        foreach (var channel in channels)
                        {
                            List<Program> _programs = programService.GetProgramByChannelIdAndUserId(channel.ChannelId, input.UserId);
                            if (_programs != null && _programs.Count > 0)
                            {
                                programs.AddRange(_programs);
                            }
                        }
                        System.Diagnostics.Debug.WriteLine("GetChannelsByUserId: {0}", DateTime.UtcNow.Subtract(dt));
                        dt = DateTime.UtcNow;
                        transfer.Data.Programs = new List<Core.DataTransfer.Program.GetOutput>();
                        transfer.Data.Programs.CopyFrom(programs);

                        List<ScreenTemplate> screenTemplate = screenTemplateService.GetAllScreenTemplate();
                        if (screenTemplate != null && screenTemplate.Count > 0)
                        {
                            transfer.Data.ScreenTemplates = new List<Core.DataTransfer.ScreenTemplate.GetOutput>();
                            transfer.Data.ScreenTemplates.CopyFrom(screenTemplate);
                        }

                        List<ScreenElement> screenElements = screenElementService.GetAllScreenElement().Where(x => x.AllowDisplay.HasValue && x.AllowDisplay.Value).ToList();
                        if (screenElements != null && screenElements.Count > 0)
                        {
                            transfer.Data.ScreenElements = new List<Core.DataTransfer.ScreenElement.GetOutput>();
                            transfer.Data.ScreenElements.CopyFrom(screenElements);
                        }
                        System.Diagnostics.Debug.WriteLine("GetAllScreenTemplate: {0}", DateTime.UtcNow.Subtract(dt));
                        dt = DateTime.UtcNow;
                        PopulateMessages(transfer, input.UserId);
                        PopulateUsers(transfer, input.UserId);
                        PopulateEvents(transfer);
                        PopulateAlerts(transfer, input.AlertsLastUpdateDate);
                        PopulateNotifications(transfer, input.UserId);
                        System.Diagnostics.Debug.WriteLine("Populate: {0}", DateTime.UtcNow.Subtract(dt));
                        dt = DateTime.UtcNow;
                        HttpWebRequestHelper helper = new HttpWebRequestHelper();
                        string MMSUsermanagementUrl = AppSettings.MMSUsermanagementUrl;

                        var output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<Metadata>>>(MMSUsermanagementUrl + "api/admin/MetaDataByUserId?id=" + input.UserId, null);
                        System.Diagnostics.Debug.WriteLine("MMSUsermanagementUrl: {0}", DateTime.UtcNow.Subtract(dt));
                    }
                    else
                    {
                        dt = DateTime.UtcNow;
                        PopulateUsers(transfer, input.UserId);
                        System.Diagnostics.Debug.WriteLine("PopulateUsers: {0}", DateTime.UtcNow.Subtract(dt));
                    }

                    List<Filter> filters = new List<Filter>();
                    if (input.Filters == null || input.Filters.Count == 0)
                    {
                        filters.Add(new Filter() { FilterTypeId = 16, FilterId = 78 });
                    }
                    else filters.CopyFrom(input.Filters);

                    if (filters.Any(x => x.FilterId == 78) && transfer.Data.Programs != null && transfer.Data.Programs.Where(x => x.FilterId.HasValue).Count() > 0)
                    {
                        foreach (var id in transfer.Data.Programs.Where(x => x.FilterId.HasValue).Select(x => x.FilterId))
                        {
                            filters.Add(new Filter { FilterTypeId = (int)FilterTypes.Program, FilterId = id.Value });
                        }
                    }

                    List<Filter> discardedFilters = null;
                    if (input.FiltersToDiscard != null)
                    {
                        discardedFilters = new List<Filter>();
                        discardedFilters.CopyFrom(input.FiltersToDiscard);
                        discardedFilters.RemoveAll(x => filters.Any(y => y.FilterId == x.FilterId));
                    }


                    LoadNews loadNews = newsService.GetNewsAndBunchCached(filters, input.PageCount, input.StartIndex, input.From, input.To, input.Term, discardedFilters);

                    dt = DateTime.UtcNow;
                    if (loadNews.News != null && loadNews.News.Count > 0)
                    {
                        transfer.Data.News = loadNews.News;
                    }

                    if (loadNews.Bunchs != null && loadNews.Bunchs.Count > 0)
                    {
                        transfer.Data.Bunch = loadNews.Bunchs;
                    }

                    List<MFilterCount> filterCount = newsService.GetFilterCountByLastUpdateDate(filters, input.From, input.To, input.ProgramFilterIds);
                    System.Diagnostics.Debug.WriteLine("GetFilterCountByLastUpdateDate: {0}", DateTime.UtcNow.Subtract(dt));
                    dt = DateTime.UtcNow;
                    if (filterCount != null && filterCount.Count > 0)
                    {
                        transfer.Data.FilterCounts = new List<Core.DataTransfer.FilterCount.GetOutput>();
                        transfer.Data.FilterCounts.CopyFrom(filterCount);
                    }

                    if (input.UserRole == (int)TeamRoles.TickerProducer || input.UserRole == (int)TeamRoles.HeadlineProducer)
                    {
                        PopulateOnAirTickers(transfer, input.UserId, input.UserRole);

                        if (input.UserRole != (int)TeamRoles.Producer)
                        {
                            PopulateLocations(transfer);
                        }

                        List<User> users = userManagementService.GetAllUsers();
                        if (users != null && users.Count > 0)
                        {
                            transfer.Data.users = new List<Core.DataTransfer.User.GetOutput>();
                            transfer.Data.users.CopyFrom(users);
                        }
                    }
                    System.Diagnostics.Debug.WriteLine("PopulateTickers: {0}", DateTime.UtcNow.Subtract(dt));

                    string VisitorsIPAddr = string.Empty;

                    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                        VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
                        VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;

                    List<VideoCutterIp> videoCutterIpRecord = videCutterIpService.GetVideoCutterIpByKeyValue("UserIP", VisitorsIPAddr, Operands.Equal, null);

                    if (videoCutterIpRecord != null && videoCutterIpRecord.Count > 0)
                        transfer.Data.IsVideoCutterInstalled = true;
                    else
                        transfer.Data.IsVideoCutterInstalled = false;

                    transfer.Data.Term = input.Term;
                    transfer.Data.Filters = filterService.GetAll().Data;
                    if (transfer.Data.Programs != null)
                    {
                        transfer.Data.Filters.RemoveAll(x => !transfer.Data.Programs.Where(y => y.FilterId.HasValue).Select(z => z.FilterId.Value).Contains(x.FilterId) && x.FilterTypeId == (int)FilterTypes.Program && x.ParentId.HasValue);
                    }
                    List<NewsBucket> newsbucketList = new List<NewsBucket>();
                    List<Team> listTeam = teamService.GetTeamByUserId(input.UserId);
                    if (listTeam != null)
                    {
                        listTeam = listTeam.GroupBy(x => x.ProgramId).Select(grp => grp.First()).ToList();
                        List<int> programIds = listTeam.Select(x => x.ProgramId.Value).ToList();
                        string ids = string.Join(",", programIds.ToArray());
                        newsbucketList = newsBucketService.GetNewsBucketByProgramIds(ids);
                    }

                    transfer.Data.NewsBuckets = new List<Core.DataTransfer.NewsBucket.GetOutput>();
                    transfer.Data.NewsBuckets.CopyFrom(newsbucketList);
                    System.Diagnostics.Debug.WriteLine("filterService: {0}", DateTime.UtcNow.Subtract(dt));

                    List<MS.Core.DataTransfer.UserFavourites.PostOutput> favourites = new List<MS.Core.DataTransfer.UserFavourites.PostOutput>();
                    favourites = requestHelper.GetRequest<List<MS.Core.DataTransfer.UserFavourites.PostOutput>>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/GetUserFavourite?UserId=" + input.UserId, null);
                    transfer.Data.UserFavourites = favourites;

                    List<TickerImage> tickerImages = new List<TickerImage>();
                    tickerImages = newsService.GetTickerImages(input.TickerImageLastUpdateDate);
                    transfer.Data.TickerImages = tickerImages;
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetUserFavourites")]
        public List<MS.Core.DataTransfer.UserFavourites.PostOutput> GetUserFavourites(int userId)
        {
            List<MS.Core.DataTransfer.UserFavourites.PostOutput> transfer = new List<MS.Core.DataTransfer.UserFavourites.PostOutput>();

            try
            {

                transfer = requestHelper.GetRequest<List<MS.Core.DataTransfer.UserFavourites.PostOutput>>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/GetUserFavourite?UserId=" + userId, null);
                return transfer;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                return transfer;
            }
        }


        [HttpPost]
        [ActionName("ProducerPolling")]
        public DataTransfer<LoadInitialDataOutput> ProducerPolling(LoadInitialDataInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new LoadInitialDataOutput();

                    List<Filter> filters = new List<Filter>();
                    filters.CopyFrom(input.Filters);

                    if (input.ProgramFilterIds != null && input.ProgramFilterIds.Count > 0)
                    {

                        filters.RemoveAll(x => x.FilterTypeId == (int)FilterTypes.Program && !x.ParentId.HasValue);
                        foreach (var id in input.ProgramFilterIds)
                        {
                            filters.Add(new Filter { FilterTypeId = (int)FilterTypes.Program, FilterId = id });
                        }
                    }
                    

                    List<Filter> discardedFilters = null;
                    if (input.FiltersToDiscard != null)
                    {
                        discardedFilters = new List<Filter>();
                        discardedFilters.CopyFrom(input.FiltersToDiscard);
                        discardedFilters.RemoveAll(x => filters.Any(y => y.FilterId == x.FilterId));
                    }

                    LoadNews loadNews = newsService.GetNewsAndBunchOptimized(filters, 10, 0, input.NewsLastUpdateDate, DateTime.UtcNow, input.Term, discardedFilters);

                    if (loadNews.Bunchs != null && loadNews.Bunchs.Count > 0)
                    {
                        transfer.Data.Bunch = loadNews.Bunchs;
                    }

                    if (loadNews.News != null && loadNews.News.Count > 0)
                    {
                        transfer.Data.News = loadNews.News;
                    }

                    if (input.NewsIds != null && input.NewsIds.Count > 0)
                    {
                        List<MNewsFilter> newsfilters = newsService.GetUpdatedNewsFilters(input.NewsFilterLastUpdateDate, newsIds: input.NewsIds);
                        if (newsfilters != null && newsfilters.Count > 0)
                        {
                            transfer.Data.NewsFilters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                            transfer.Data.NewsFilters.CopyFrom(newsfilters);
                        }
                    }

                    List<MFilterCount> filterCount = newsService.GetFilterCountByLastUpdateDate(filters, input.From, input.To, input.ProgramFilterIds);

                    if (filterCount != null && filterCount.Count > 0)
                    {
                        transfer.Data.FilterCounts = new List<Core.DataTransfer.FilterCount.GetOutput>();
                        transfer.Data.FilterCounts.CopyFrom(filterCount);
                    }

                    if (!string.IsNullOrEmpty(input.SlotScreenTemplateId))
                    {
                        int slotScreenTemplateId = Convert.ToInt32(input.SlotScreenTemplateId);
                        List<Message> chatMessages = messageService.GetMessages(slotScreenTemplateId, input.CommentsLastUpdate);
                        if (chatMessages != null && chatMessages.Count > 0)
                        {
                            transfer.Data.Messages = new List<Core.DataTransfer.Message.GetOutput>();
                            transfer.Data.Messages.CopyFrom(chatMessages);
                        }
                    }

                    if (input.UserRole == (int)TeamRoles.TickerProducer || input.UserRole == (int)TeamRoles.HeadlineProducer)
                    {

                        List<OnAirTicker> onAirTicker = new List<OnAirTicker>();

                        onAirTicker = onAirTickerService.GetLatestOnAirTicker(input.TickerLastUpdateDate);

                        if (onAirTicker != null && onAirTicker.Count > 0)
                        {
                            transfer.Data.Tickers = new List<Core.DataTransfer.OnAirTicker.GetOutput>();
                            transfer.Data.Tickers.CopyFrom(onAirTicker);
                        }

                        List<OnAirTickerLine> onAirTickerLines = new List<OnAirTickerLine>();
                        onAirTickerLines = onAirTickerLineService.GetLatestOnAirTickerLine(input.TickerLineLastUpdateDate);

                        if (onAirTickerLines != null && onAirTickerLines.Count > 0)
                        {
                            transfer.Data.TickerLines = new List<Core.DataTransfer.OnAirTickerLine.GetOutput>();
                            transfer.Data.TickerLines.CopyFrom(onAirTickerLines);
                        }
                    }

                    //MS.Core.DataTransfer.Resource.ResourceOutput Resources = new MS.Core.DataTransfer.Resource.ResourceOutput();
                    //Resources = requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/getresourcebydate?ResourceDate=" + input.FromStr + "&pageSize=1000&pageNumber=1", null);
                    //if (Resources != null)
                    transfer.Data.Resources = new MS.Core.DataTransfer.Resource.ResourceOutput();
                    transfer.Data.Resources.Resources = CacheManager.Resources.Resources;
                    if (transfer.Data.Resources.Resources != null && transfer.Data.Resources.Resources.Count > 0)
                        transfer.Data.Resources.Resources = transfer.Data.Resources.Resources.Where(x => x.CreationDate >= input.ResourceDate).OrderByDescending(x => x.CreationDate).ToList();

                    if (!string.IsNullOrEmpty(input.Token))
                        transfer.Data.Scripts = scriptService.GetAllUnExecutedScripts(input.Token);

                    PopulateEvents(transfer);
                    PopulateAlerts(transfer, input.AlertsLastUpdateDate);
                    PopulateNotificationsByLastUpdateDate(transfer, input.UserId, input.NotificationsLastUpdateDate);
                    transfer.Data.Term = input.Term;

                    string VisitorsIPAddr = string.Empty;

                    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                        VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
                        VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;

                    List<VideoCutterIp> videoCutterIpRecord = videCutterIpService.GetVideoCutterIpByKeyValue("UserIP", VisitorsIPAddr, Operands.Equal, null);


                    if (input.TickerImageLastUpdateDateStr != null)
                    {
                        List<TickerImage> tickerImages = new List<TickerImage>();
                        tickerImages = newsService.GetTickerImagesByLastUpdateDate(input.TickerImageLastUpdateDate);
                        transfer.Data.TickerImages = tickerImages;

                    }

                    if (videoCutterIpRecord != null && videoCutterIpRecord.Count > 0)
                        transfer.Data.IsVideoCutterInstalled = true;
                    else
                        transfer.Data.IsVideoCutterInstalled = false;

                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        


        [HttpPost]
        [ActionName("GetMoreNews")]
        public DataTransfer<LoadInitialDataOutput> GetMoreNews(LoadInitialDataInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new LoadInitialDataOutput();
                    List<Filter> filters = new List<Filter>();
                    filters.CopyFrom(input.Filters);

                    if (input.Filters.Count > 0)
                    {
                        if (filters.Count(x => x.FilterTypeId == 13 || x.FilterTypeId == 15 || x.FilterTypeId == 19 || x.FilterTypeId == 20 || x.FilterTypeId == 8) == filters.Count)
                            filters.Add(new Filter { FilterTypeId = (int)FilterTypes.AllNews, FilterId = 78 });

                        filters.RemoveAll(x => x.FilterTypeId == (int)FilterTypes.Program && !x.ParentId.HasValue);
                        if (input.ProgramFilterIds != null && input.ProgramFilterIds.Count > 0)
                        {
                            foreach (var id in input.ProgramFilterIds)
                            {
                                filters.Add(new Filter { FilterTypeId = (int)FilterTypes.Program, FilterId = id });
                            }
                        }

                        List<Filter> discardedFilters = null;
                        if (input.FiltersToDiscard != null)
                        {
                            discardedFilters = new List<Filter>();
                            discardedFilters.CopyFrom(input.FiltersToDiscard);
                            discardedFilters.RemoveAll(x => filters.Any(y => y.FilterId == x.FilterId));
                        }

                        LoadNews loadNews = null;
                        if (Math.Floor((DateTime.UtcNow - input.From).TotalDays) > 30)
                        {
                            input.IsArchivalSearch = true;
                        }

                        if (String.IsNullOrEmpty(input.Term))
                            //loadNews = newsService.GetNewsAndBunchWithArchival(filters, input.PageCount, input.StartIndex, input.From, input.To, input.Term, discardedFilters, input.IsArchivalSearch);
                            loadNews = newsService.GetNewsAndBunchOptimized(filters, input.PageCount, input.StartIndex, input.From, input.To, input.Term, discardedFilters, input.IsArchivalSearch);
                        else
                        {
                            loadNews = newsService.GetNewsAndBunchCache(filters, input.PageCount, input.StartIndex, input.FromStr, input.ToStr, input.Term, discardedFilters, IsArchivalSearch: input.IsArchivalSearch);
                        }

                        if (loadNews != null)
                        {
                            if (loadNews.News != null && loadNews.News.Count > 0)
                            {
                                transfer.Data.News = loadNews.News;
                            }

                            if (loadNews.Bunchs != null && loadNews.Bunchs.Count > 0)
                            {
                                transfer.Data.Bunch = loadNews.Bunchs;
                            }
                        }
                        transfer.Data.Term = input.Term;
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

       
        [HttpPost]
        [ActionName("UpdateThumbId")]
        public HttpResponseMessage UpdateThumbId(string id, string resguid)
        {
            try
            {
                if (newsService.UpdateThumbId(id, resguid))
                    return new HttpResponseMessage(HttpStatusCode.OK);
                else return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }

        [HttpPost]
        [ActionName("GetBunch")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetBunch(LoadInitialDataInput input)
        {
            DataTransfer<List<Core.DataTransfer.News.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
            try
            {
                if (input != null)
                {
                    List<Filter> filters = new List<Filter>();
                    filters.CopyFrom(input.Filters);

                    if (input.Filters.Count > 0)
                    {
                        if (filters.Count(x => x.FilterTypeId == 13 || x.FilterTypeId == 15 || x.FilterTypeId == 19 || x.FilterTypeId == 20 || x.FilterTypeId == 8) == filters.Count)
                            filters.Add(new Filter { FilterTypeId = (int)FilterTypes.AllNews, FilterId = 78 });
                        filters.RemoveAll(x => x.FilterTypeId == (int)FilterTypes.Program && !x.ParentId.HasValue);
                        if (input.ProgramFilterIds != null && input.ProgramFilterIds.Count > 0)
                        {
                            foreach (var id in input.ProgramFilterIds)
                            {
                                filters.Add(new Filter { FilterTypeId = (int)FilterTypes.Program, FilterId = id });
                            }
                        }
                    }

                    List<MNews> news = newsService.GetNewsWithBunch(input.BunchId, filters, input.PageCount, input.StartIndex, input.From, input.To);
                    if (news != null && news.Count > 0)
                    {
                        transfer.Data = new List<Core.DataTransfer.News.GetOutput>();

                        foreach (MNews mnews in news)
                        {
                            mnews.PublishTime = mnews.PublishTime.Date.Add(mnews.LastUpdateDate.TimeOfDay);
                        }

                        transfer.Data.CopyFrom(news);
                    }

                    //List<MFilterCount> filterCount = news.SelectMany(x => x.Filters.Select(y => y.FilterId)).Distinct().Select(x => new MFilterCount() { FilterId = x }).ToList();

                    //foreach (var filter in filterCount)
                    //{
                    //    filter.Count = news.Where(x => x.Filters.Select(y => y.FilterId).Contains(filter.FilterId)).Count();
                    //}

                    //if (filterCount != null && filterCount.Count > 0)
                    //{
                    //    transfer.Data.FilterCounts = new List<Core.DataTransfer.FilterCount.GetOutput>();
                    //    transfer.Data.FilterCounts.CopyFrom(filterCount);
                    //}


                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetBunchWithCount")]
        public DataTransfer<GetNewsCustomOutput> GetBunchWithCount(LoadInitialDataInput input)
        {
            DataTransfer<GetNewsCustomOutput> transfer = new DataTransfer<GetNewsCustomOutput>();
            try
            {
                if (input != null)
                {
                    List<Filter> filters = new List<Filter>();
                    filters.CopyFrom(input.Filters);

                    if (input.Filters.Count > 0)
                    {
                        if (filters.Count(x => x.FilterTypeId == 13 || x.FilterTypeId == 15 || x.FilterTypeId == 19 || x.FilterTypeId == 20 || x.FilterTypeId == 8) == filters.Count)
                            filters.Add(new Filter { FilterTypeId = (int)FilterTypes.AllNews, FilterId = 78 });
                        filters.RemoveAll(x => x.FilterTypeId == (int)FilterTypes.Program && !x.ParentId.HasValue);
                        if (input.ProgramFilterIds != null && input.ProgramFilterIds.Count > 0)
                        {
                            foreach (var id in input.ProgramFilterIds)
                            {
                                filters.Add(new Filter { FilterTypeId = (int)FilterTypes.Program, FilterId = id });
                            }
                        }
                    }

                    List<MNews> news = newsService.GetNewsWithBunch(input.BunchId, filters, input.PageCount, input.StartIndex, input.From, input.To);

                    List<MNewsFilter> lst = new List<MNewsFilter>();
                    lst = news.SelectMany(x => x.Filters).ToList();
                    List<MFilterCount> filtercount = lst.GroupBy(x => x.FilterId).Select(g => new MFilterCount { FilterId = g.Key, Count = g.Count() }).ToList();

                    transfer.Data = new GetNewsCustomOutput();
                    INewsUpdateHistoryService newsUpdateHistoryService = IoC.Resolve<INewsUpdateHistoryService>("NewsUpdateHistoryService");

                    if (news != null && news.Count > 0)
                    {
                        transfer.Data.News = new List<GetOutput>();
                        transfer.Data.FilterCounts = new List<MFilterCount>();
                        foreach (MNews mnews in news)
                        {
                            mnews.PublishTime = mnews.PublishTime.Date.Add(mnews.LastUpdateDate.TimeOfDay);
                        }
                        transfer.Data.News.CopyFrom(news);
                        for (int i = 0; i < transfer.Data.News.Count; i++)
                        {
                            transfer.Data.News[i].NewsPaperdescription = newsService.GetByNewsGuid(transfer.Data.News[i]._id).NewsPaperdescription;
                            List<NewsUpdateHistory> NewsUpdateHistories = newsUpdateHistoryService.GetByNewsGuid(transfer.Data.News[i]._id);
                            if (NewsUpdateHistories != null && NewsUpdateHistories.Count > 0)
                                transfer.Data.News[i].newsUpdateHistories = new List<Core.DataTransfer.NewsUpdateHistory.PostInput>();
                            transfer.Data.News[i].newsUpdateHistories.CopyFrom(NewsUpdateHistories);

                            Ticker tc = tickerService.GetTickerByNewsGuid(transfer.Data.News[i]._id);
                            if (tc != null && tc.TickerId > 0)
                            {
                                tc.TickerLines = new List<TickerLine>();
                                tc.TickerLines = tickerLineService.GetTickerLineByTickerId(tc.TickerId);
                                NMS.Core.DataTransfer.Ticker.PostInput tempTc = new Core.DataTransfer.Ticker.PostInput();
                                tempTc.CopyFrom(tc);
                                transfer.Data.News[i].Tickers = new List<NMS.Core.DataTransfer.Ticker.PostInput>();
                                transfer.Data.News[i].Tickers.Add(tempTc);
                            }
                        }

                        transfer.Data.FilterCounts = filtercount;
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }


        private void PopulateMessages(DataTransfer<LoadInitialDataOutput> transfer, int userId)
        {
            List<Message> chatMessages = messageService.GetAllUserMessages(userId);
            if (chatMessages != null && chatMessages.Count > 0)
            {
                transfer.Data.Messages = new List<Core.DataTransfer.Message.GetOutput>();
                transfer.Data.Messages.CopyFrom(chatMessages);
            }
        }

        private void PopulateUsers(DataTransfer<LoadInitialDataOutput> transfer, int userId)
        {
            List<User> users = userManagementService.GetAllUsers();
            List<Team> team = teamService.GetTeamByUserId(userId);

            if (users != null && team != null)
            {
                users = users.Where(x => team.Any(y => y.UserId == x.UserId)).ToList();

                if (users != null && users.Count > 0)
                {
                    foreach (User user in users)
                    {
                        user.UserRoles = team.Where(x => x.UserId == user.UserId).ToList();
                    }

                    transfer.Data.users = new List<Core.DataTransfer.User.GetOutput>();
                    transfer.Data.users.CopyFrom(users);
                }
            }
        }

        private void PopulateEvents(DataTransfer<LoadInitialDataOutput> transfer)
        {
            List<EventInfo> eventInfo = eventInfoService.GetUpcommingEvents();
            if (eventInfo != null && eventInfo.Count > 0)
            {
                transfer.Data.Events = new List<Core.DataTransfer.EventInfo.GetOutput>();
                transfer.Data.Events.CopyFrom(eventInfo);
            }
        }

        private void PopulateNotifications(DataTransfer<LoadInitialDataOutput> transfer, int userId)
        {
            List<Notification> notifications = notificationService.GetNotifications(userId);
            if (notifications != null && notifications.Count > 0)
            {
                transfer.Data.Notifications = new List<Core.DataTransfer.Notification.GetOutput>();
                transfer.Data.Notifications.CopyFrom(notifications);
            }
        }

        private void PopulateNotificationsByLastUpdateDate(DataTransfer<LoadInitialDataOutput> transfer, int userId, DateTime lastUpdateDate)
        {
            List<Notification> notifications = notificationService.GetNotifications(userId, lastUpdateDate);
            if (notifications != null && notifications.Count > 0)
            {
                transfer.Data.Notifications = new List<Core.DataTransfer.Notification.GetOutput>();
                transfer.Data.Notifications.CopyFrom(notifications);
            }
        }

        private void PopulateAlerts(DataTransfer<LoadInitialDataOutput> transfer, DateTime alertsLastUpdateDate)
        {
            List<Alert> alerts = alertService.GetLatestAlerts(alertsLastUpdateDate);
            if (alerts != null && alerts.Count > 0)
            {
                transfer.Data.Alerts = new List<Core.DataTransfer.Alert.GetOutput>();
                transfer.Data.Alerts.CopyFrom(alerts);
            }
        }

        private void PopulateOnAirTickers(DataTransfer<LoadInitialDataOutput> transfer, int userId, int userRole)
        {
            List<OnAirTicker> onAirTickers = new List<OnAirTicker>();
            onAirTickers = onAirTickerService.GetAllOnAirTicker();

            if (onAirTickers != null && onAirTickers.Count > 0)
            {
                onAirTickers = onAirTickerService.FillOnAirTicker(onAirTickers);
                transfer.Data.Tickers = new List<Core.DataTransfer.OnAirTicker.GetOutput>();
                transfer.Data.Tickers.CopyFrom(onAirTickers);
            }
        }

        private void PopulateLocations(DataTransfer<LoadInitialDataOutput> transfer)
        {
            List<Location> locations = locationService.GetAllLocation();
            if (locations != null && locations.Count > 0)
            {
                transfer.Data.Locations = new List<Core.DataTransfer.Location.GetOutput>();
                transfer.Data.Locations.CopyFrom(locations);
            }
        }

        #endregion Producer

        #region Reporter

        [HttpGet]
        [ActionName("GetNews")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> GetNews(string id)
        {
            DataTransfer<Core.DataTransfer.News.GetOutput> transfer = new DataTransfer<Core.DataTransfer.News.GetOutput>();
            try
            {
                MNews news = newsService.GetNewsById(id);
                if (news != null)
                {
                    transfer.Data = new Core.DataTransfer.News.GetOutput();
                    transfer.Data.CopyFrom(news);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetNewsByIDs")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsByIDs(NewsIdsModel model)
        {
            DataTransfer<List<Core.DataTransfer.News.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
            try
            {
                transfer.Data = new List<Core.DataTransfer.News.GetOutput>();
                foreach (string _id in model.ids)
                {
                    MNews news = newsService.GetNewsById(_id);
                    Core.DataTransfer.News.GetOutput _news = new Core.DataTransfer.News.GetOutput();
                    _news.CopyFrom(news);
                    transfer.Data.Add(_news);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetNewsByTerm")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsByTerm(string id)
        {
            DataTransfer<List<Core.DataTransfer.News.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
            try
            {
                //SolrCUD sc = new SolrCUD();
                //sc.Dump<SolarNews>(ConfigurationManager.AppSettings["solrUrl"].ToString());
                //SolrSearch ssearch = new SolrSearch();
                string queryString = "title:(*" + id + "*) OR description:(*" + id + "*)";
                SolrService<SolarNews> sService = new SolrService<SolarNews>();
                SolrManager.OutputEntities.SearchResult<SolarNews> searchResult = sService.Search(queryString, 10, 1, id);
                //SolrManager.OutputEntities.SearchResult<SolarNews> searchResult = ssearch.searchString<SolarNews>(id, 10, 1);
                List<MNews> lstNews = new List<MNews>();
                if (searchResult != null && searchResult.Results.Count > 0)
                {
                    //foreach (SolarNews sn in searchResult.Results)
                    //{
                    List<string> NewsList = searchResult.Results.Select(x => x._id).ToList();
                    lstNews = newsService.GetByIDs(NewsList);
                    //}
                }
                // List<MNews> news = newsService.GetNewsByTitle(id);
                if (searchResult != null && searchResult.Results.Count > 0)
                {
                    transfer.Data = new List<Core.DataTransfer.News.GetOutput>();
                    transfer.Data.CopyFrom(lstNews);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetAllMyNews")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetAllMyNews(int rId, int pageCount, int startIndex)
        {
            DataTransfer<List<Core.DataTransfer.News.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();

            try
            {
                List<Core.DataTransfer.News.GetOutput> news = new List<Core.DataTransfer.News.GetOutput>();
                news.CopyFrom(newsService.GetReportedNews(rId, pageCount, startIndex));
                transfer.Data = news;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = "Exception occur.";
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("ReporterLoadInitialData")]
        public DataTransfer<LoadInitialDataOutput> ReporterLoadInitialData(bool? withoutFolder)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                IProgramService programService=IoC.Resolve<IProgramService>("ProgramService");
                transfer.Data = new LoadInitialDataOutput();
                transfer.Data.Filters = filterService.GetAll().Data;
                
                
                {
                    transfer.Data.Programs = programService.GetAllProgramsForReporter(withoutFolder).Data;
                }
                

                MMS.Integration.UserManagement.UserManagement userManagement = new MMS.Integration.UserManagement.UserManagement();
                var dtUsers = userManagement.GetUsersByWorkRoleId(3);
                if (dtUsers.IsSuccess)
                {
                    transfer.Data.users = new List<Core.DataTransfer.User.GetOutput>();
                    foreach (var user in dtUsers.Data)
                    {
                        Core.DataTransfer.User.GetOutput u = new Core.DataTransfer.User.GetOutput();
                        u.UserId = user.UserId;
                        u.Name = user.Fullname;
                        transfer.Data.users.Add(u);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("ReportErrorFromMCR")]
        public DataTransfer<LoadInitialDataOutput> ReportErrorFromMCR(string error)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                var a = error;
                ExceptionLogger.LogErrorFromMCR(error);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("ReporterPolling")]
        public DataTransfer<LoadInitialDataOutput> ReporterPolling(LoadInitialDataInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new LoadInitialDataOutput();

                    List<MNewsFilter> newsfilters = newsService.GetUpdatedNewsFilters(input.NewsFilterLastUpdateDate, input.ReporterId);
                    if (newsfilters != null && newsfilters.Count > 0)
                    {
                        transfer.Data.NewsFilters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                        transfer.Data.NewsFilters.CopyFrom(newsfilters);
                    }

                    List<MComment> comments = newsService.GetCommentsByLastUpdateDate(input.CommentsLastUpdate, input.ReporterId);
                    if (comments != null && comments.Count > 0)
                    {
                        transfer.Data.Comments = new List<Core.DataTransfer.Comment.GetOutput>();
                        transfer.Data.Comments.CopyFrom(comments);
                    }
                    transfer.Data.Term = input.Term;
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("Add")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> AddNews(NMS.Core.DataTransfer.News.PostInput input)
        {
            DataTransfer<Core.DataTransfer.News.GetOutput> transfer = new DataTransfer<Core.DataTransfer.News.GetOutput>();
            try
            {
                if (input != null)
                {
                    MNews mnews = null;
                    if (String.IsNullOrEmpty(input.Guid))
                    {
                        mnews = newsService.InsertReportedNews(input);
                    }
                    else
                    {
                        mnews = newsService.EditNews(input);
                    }
                    if (mnews != null)
                    {
                        transfer.Data = new Core.DataTransfer.News.GetOutput();
                        transfer.Data.CopyFrom(mnews);
                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "Duplicate Insertion attempted" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        #endregion Reporter

        #region Verification

        [HttpPost]
        [ActionName("InsertComment")]
        public DataTransfer<Core.DataTransfer.Comment.PostOutput> InsertComment(Core.DataTransfer.Comment.PostInput input)
        {
            DataTransfer<Core.DataTransfer.Comment.PostOutput> transfer = new DataTransfer<Core.DataTransfer.Comment.PostOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new Core.DataTransfer.Comment.PostOutput();
                    MComment comment = new MComment();
                    comment.CopyFrom(input);
                    transfer.Data.CopyFrom(newsService.InsertComment(comment));
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("VerificationLoadInitialData")]
        public DataTransfer<LoadInitialDataOutput> VerificationLoadInitialData(LoadInitialDataInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new LoadInitialDataOutput();
                    List<Filter> filters = new List<Filter>();
                    filters.CopyFrom(input.Filters);
                    List<MNews> news = newsService.GetNewsByFilterIds(filters, input.PageCount, input.StartIndex, input.From, input.To, input.Term);
                    if (news != null && news.Count > 0)
                    {
                        transfer.Data.News = new List<Core.DataTransfer.News.GetOutput>();
                        transfer.Data.News.CopyFrom(news);
                    }

                    List<MNewsFilter> newsfilters = newsService.GetUpdatedNewsFilters(input.NewsFilterLastUpdateDate);
                    if (newsfilters != null && newsfilters.Count > 0)
                    {
                        transfer.Data.NewsFilters = new List<Core.DataTransfer.NewsFilter.GetOutput>();
                        transfer.Data.NewsFilters.CopyFrom(newsfilters);
                    }
                    transfer.Data.Filters = filterService.GetAll().Data;
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("MarkVerifyNews")]
        public HttpResponseMessage MarkVerifyNews(MarkVerifyNewsInput input)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                if (input != null)
                {
                    newsService.MarkVerifyNews(input.NewsIds, input.bunchId, input.IsVerified);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }

        [HttpGet]
        [ActionName("GetNewsWithUpdates")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetNewsWithUpdatesAndRelated(string id, string bunchId)
        {
            DataTransfer<List<Core.DataTransfer.News.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();
            try
            {
                List<MNews> news = newsService.GetNewsWithUpdatesAndRelatedById(id, bunchId);
                if (news != null)
                {
                    transfer.Data = new List<Core.DataTransfer.News.GetOutput>();
                    transfer.Data.CopyFrom(news);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        #endregion Verification

        #region MMS User Management Functions

        public DataTransfer<MMSUserConfigurationOutput> GetUserConfiguration(string SessionKey, int ModuleId)
        {
            string MMSUsermanagementUrl = AppSettings.MMSUsermanagementUrl;

            DataTransfer<MMSUserConfigurationOutput> transfer = new DataTransfer<MMSUserConfigurationOutput>();

            try
            {
                HttpWebRequestHelper helper = new HttpWebRequestHelper();

                MMSUserConfigurationInput mmsInput = new MMSUserConfigurationInput();
                mmsInput.ModuleId = ModuleId;
                mmsInput.SessionKey = SessionKey;

                DataTransfer<MMSUserConfigurationOutput> output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<MMSUserConfigurationOutput>>(AppSettings.MMSUsermanagementUrl + "api/admin/UserConfiguration", mmsInput, null);

                if (output != null && output.IsSuccess == true)
                {
                    transfer.Data = output.Data;
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { output.Errors[0].ToString() };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        private List<NMS.Core.DataTransfer.Filter.GetOutput> GetFiltersByUserConfiguration(List<NMS.Core.DataTransfer.Filter.GetOutput> filters, string SessionKey)
        {
            DataTransfer<MMSUserConfigurationOutput> transfer = GetUserConfiguration(SessionKey, 1);

            if (transfer.IsSuccess == true)
            {
                if (transfer.Data != null)
                {
                    if (transfer.Data.MetaData == null)
                        return filters;
                    else
                    {
                        // do the functionality here

                        List<MMSMetaData> metaData = transfer.Data.MetaData;
                        List<NMS.Core.DataTransfer.Filter.GetOutput> lstParentFilters = filters.Where(x => x.ParentId == null).ToList<NMS.Core.DataTransfer.Filter.GetOutput>();

                        //metaData.Select(x => x.MetaValue).ToList<int>();
                        //lstParentFilters.Where( x => x.FilterId ==

                        List<int> innerquery = metaData.Select(x => Convert.ToInt32(x.MetaValue)).ToList<int>();

                        //var obj = lstParentFilters.Where(x => x.FilterId == Convert.ToInt32(innerquery.Select(y => y).FirstOrDefault())).ToList();

                        List<NMS.Core.DataTransfer.Filter.GetOutput> lstAssigned = lstParentFilters.Where(x => innerquery.Contains(x.FilterId)).ToList<NMS.Core.DataTransfer.Filter.GetOutput>();

                        //delete all those keys except that is assign to user from lstParentFilters

                        foreach (NMS.Core.DataTransfer.Filter.GetOutput f in lstAssigned)
                        {
                            List<NMS.Core.DataTransfer.Filter.GetOutput> item = lstParentFilters.Where(x => x.FilterId == f.FilterId).ToList<NMS.Core.DataTransfer.Filter.GetOutput>();
                            if (item != null)
                                lstParentFilters.Remove(item[0]);
                        }

                        foreach (NMS.Core.DataTransfer.Filter.GetOutput f in lstParentFilters)
                        {
                            List<NMS.Core.DataTransfer.Filter.GetOutput> item = filters.Where(x => x.FilterId == f.FilterId).ToList<NMS.Core.DataTransfer.Filter.GetOutput>();
                            if (item != null)
                                filters.Remove(item[0]);
                        }

                        return filters;
                    }
                }
                else
                {
                    //means it have to show all the filters

                    return filters;
                }
            }
            else
            {
                //means IsSuccess == false
                throw new Exception(transfer.Errors[0]);
            }
        }

        #endregion MMS User Management Functions



        [HttpPost]
        [ActionName("JSErrorLogging")]
        public bool JSErrorLogging(string Msg, string Url, string Linenumber, string User)
        {
            bool isLogged = false;

            string Type = string.Empty;
            string ErrorMessage = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(Msg) && !string.IsNullOrEmpty(Url) && !string.IsNullOrEmpty(Linenumber))
                {

                    int index = Msg.LastIndexOf("TypeError");
                    if (index > 0)
                    {
                        ErrorMessage = Msg.Substring(0, index - 1);
                        Type = Msg.Substring(index);

                        string VisitorsIPAddr = string.Empty;
                        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                        {
                            VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();

                        }
                        else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
                        {
                            VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
                        }
                        isLogged = ExceptionLogger.LogJsError(ErrorMessage, Url, Linenumber, User, Type, VisitorsIPAddr);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                isLogged = false;
            }


            return isLogged;
        }

        [HttpGet]
        [ActionName("GetAllFilters")]
        public DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>> GetAllFilters()
        {


            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Filter.GetOutput>>();
            transfer.Data = new List<NMS.Core.DataTransfer.Filter.GetOutput>();

            try
            {
                List<NMS.Core.DataTransfer.Filter.GetOutput> list = filterService.GetAll().Data;

                if (list != null)
                {
                    transfer.Data.CopyFrom(list);

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("AddToNewsBucket")]
        public DataTransfer<Core.DataTransfer.NewsBucket.GetOutput> AddToNewsBucket(Core.DataTransfer.NewsBucket.InputNewsBucket input)
        {
            DataTransfer<Core.DataTransfer.NewsBucket.GetOutput> transfer = new DataTransfer<Core.DataTransfer.NewsBucket.GetOutput>();
            try
            {
                List<NewsBucket> listNewsBucket = new List<NewsBucket>();
                if (input.News != null && input.News.Count == 1)
                {
                    transfer.Data = new Core.DataTransfer.NewsBucket.GetOutput();
                    listNewsBucket.CopyFrom(input.News);
                    listNewsBucket[0].CreationDate = DateTime.UtcNow;
                    listNewsBucket[0].LastUpdateDate = DateTime.UtcNow;
                    listNewsBucket[0].IsActive = true;

                    transfer.Data.CopyFrom(newsBucketService.InsertNewsBucket(listNewsBucket[0]));
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }



        [HttpPost]
        [ActionName("UpdateNewsBucketSequence")]
        public DataTransfer<List<Core.DataTransfer.NewsBucket.GetOutput>> UpdateNewsBucketSequence(Core.DataTransfer.NewsBucket.InputNewsBucket input)
        {
            DataTransfer<List<Core.DataTransfer.NewsBucket.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.NewsBucket.GetOutput>>();
            try
            {
                List<NewsBucket> listNews = new List<NewsBucket>();
                if (input.News != null)
                {
                    transfer.Data = new List<Core.DataTransfer.NewsBucket.GetOutput>();
                    listNews.CopyFrom(input.News);
                    List<NewsBucket> tempList = new List<NewsBucket>();
                    foreach (var item in listNews)
                    {
                        tempList.Add(newsBucketService.UpdateSequence(item));
                    }
                    if (tempList != null)
                    {
                        transfer.Data.CopyFrom(tempList);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }
    }
}