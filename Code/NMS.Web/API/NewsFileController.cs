﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.IController;
using NMS.Core.Models;
using System.Threading;
using NMS.Core.Enums;
using NMS.Core.DataTransfer.NewsFile;
using NMS.Core.Helper;
using System.Web.Script.Serialization;
using NMS.Core.Helper.Notification;
using System.Configuration;

namespace NMS.Web.API
{
    public class NewsFileController : ApiController
    {
        private INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
        private IFolderService folderService = IoC.Resolve<IFolderService>("FolderService");
        ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
        ISlotTemplateScreenElementService slotTemplateScreenElementService = IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
        ISlotTemplateScreenElementResourceService slotTemplateScreenElementResourceService = IoC.Resolve<ISlotTemplateScreenElementResourceService>("SlotTemplateScreenElementResourceService");
        IMosActiveEpisodeService mosActiveEpisodeService = IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
        ISlotScreenTemplateMicService slotScreentTemplateMicService = IoC.Resolve<ISlotScreenTemplateMicService>("SlotScreenTemplateMicService");
        ISlotScreenTemplateCameraService slotScreentTemplateCameraService = IoC.Resolve<ISlotScreenTemplateCameraService>("SlotScreenTemplateCameraService");
        ISlotScreenTemplateResourceService slotScreenTemplateResourceService = IoC.Resolve<ISlotScreenTemplateResourceService>("SlotScreenTemplateResourceService");
        ISlotScreenTemplatekeyService slotScreenTemplatekeyService = IoC.Resolve<ISlotScreenTemplatekeyService>("SlotScreenTemplatekeyService");
        ISlotService slotService = IoC.Resolve<ISlotService>("SlotService");
        INewsService newsService = IoC.Resolve<INewsService>("NewsService");
        ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
        ISuggestedFlowService suggestedFlowService = IoC.Resolve<ISuggestedFlowService>("SuggestedFlowService");
        private IFileDetailService filedetailService = IoC.Resolve<IFileDetailService>("FileDetailService");
        private IUserManagementService userManagementService = IoC.Resolve<IUserManagementService>("UserManagementService");
        private IFilterService filterService = IoC.Resolve<IFilterService>("FilterService");
        INewsFileFilterService newsfilefilterservice = IoC.Resolve<INewsFileFilterService>("NewsFileFilterService");
        IFileCategoryService filecategoryservice = IoC.Resolve<IFileCategoryService>("FileCategoryService");
        IFileLocationService filelocationservice = IoC.Resolve<IFileLocationService>("FileLocationService");
        IFileTagService filetagservice = IoC.Resolve<IFileTagService>("FileTagService");
        IFileResourceService fileresourceservice = IoC.Resolve<IFileResourceService>("FileResourceService");
        IProgramService programservice = IoC.Resolve<IProgramService>("ProgramService");
        private ITickerService tickerService = IoC.Resolve<ITickerService>("TickerService");
        private ITickerCategoryService tickerCategoryService = IoC.Resolve<ITickerCategoryService>("TickerCategoryService");
        INewsFileFilterService filefilterservice = IoC.Resolve<INewsFileFilterService>("NewsFileFilterService");
        IFileFolderHistoryService filefolderhistoryservice = IoC.Resolve<IFileFolderHistoryService>("FileFolderHistoryService");
        IFileStatusHistoryService filestatushistoryservice = IoC.Resolve<IFileStatusHistoryService>("FileStatusHistoryService");
        IEditorialCommentService editorialCommentservice = IoC.Resolve<IEditorialCommentService>("EditorialCommentService");
        IEventTypeService eventtypeservice = IoC.Resolve<IEventTypeService>("EventTypeService");
        INewsFileHistoryService newsfilehistoryservice = IoC.Resolve<INewsFileHistoryService>("NewsFileHistoryService");
        ITranslatedScriptService translatedscriptservice = IoC.Resolve<ITranslatedScriptService>("TranslatedScriptService");

        string host = ConfigurationManager.AppSettings["rabbitmqhost"];

        //[HttpPost]
        //[ActionName("GetAllNewsFilesWithFilter")]
        //public DataTransfer<LoadInitialDataOutput> GetAllNewsFilesWithFilter(LoadInitialDataInput input)
        //{
        //    DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
        //    try
        //    {
        //        if (input != null)
        //        {
        //            transfer.Data = new LoadInitialDataOutput();
        //            List<Filter> filters = new List<Filter>();
        //            filters.CopyFrom(input.Filters);

        //            if (filters.Count == 0)
        //                filters.Add(new Filter() { FilterId = (int)NewsFilters.AllNews });


        //            if (input.From.Year == 1) { input.From = DateTime.UtcNow.AddMonths(-1); }
        //            if(input.To.Year == 1) { input.To = DateTime.UtcNow; }

        //            List<NewsFile> news = fileService.GetNewsFileWithFilterFromTo(filters,input.From,input.To);
        //            if (news != null)
        //            {   
        //                transfer.Data.NewsFile = new List<Core.DataTransfer.NewsFile.GetOutput>();
        //                transfer.Data.NewsFile.CopyFrom(news);
        //            }
        //        }
        //        else
        //        {
        //            transfer.IsSuccess = false;
        //            transfer.Errors = new string[] { "Invalid Input provided" };
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        transfer.Errors = new string[] { ex.Message };
        //        ExceptionLogger.Log(ex);
        //        transfer.IsSuccess = false;
        //    }
        //    return transfer;
        //}


        [HttpPost]
        [ActionName("CopyNewsFileWithFolderId")]
        public DataTransfer<LoadInitialDataOutput> CopyNewsFileWithFolderId(int NewsFileId, int FolderId, int CreatedBy, int? ProgramId = null)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                transfer.Data = new LoadInitialDataOutput();
                NewsFile newsfile = fileService.GetNewsFile(NewsFileId);
                if (newsfile != null)
                {
                    if (ProgramId != null && ProgramId != 0)
                    {
                        var Folder = folderService.GetAllFolder().Where(x => x.ProgramId == ProgramId).FirstOrDefault();
                        FolderId = Folder.FolderId;
                    }

                    var news = FillNewsFileObject(newsfile, FolderId);
                    news.CreatedBy = CreatedBy;
                    if (news.ProgramId == null)
                        news.ProgramId = ProgramId;

                    int sequenceNo = fileService.GetMaxSequenceNumByFolderId(FolderId);
                    if (sequenceNo >= 0)
                        news.SequenceNo = sequenceNo + 1;
                    else
                        news.SequenceNo = 0;

                    Core.DataTransfer.NewsFile.GetOutput submittednews = new Core.DataTransfer.NewsFile.GetOutput();
                    submittednews.CopyFrom(fileService.InsertNewsFile(news));
                    if (submittednews != null)
                    {
                        bool result = MakeDuplicate(newsfile, submittednews);

                        transfer.Data.NewsFile = new List<Core.DataTransfer.NewsFile.GetOutput>();
                        transfer.Data.NewsFile.Add(submittednews);
                    }
                }

                transfer.IsSuccess = true;
                return transfer;

            }
            catch (Exception ex)
            {
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateNewsFileSequenceNo")]
        public DataTransfer<LoadInitialDataOutput> UpdateNewsFileSequenceNo(NMS.Core.DataTransfer.NewsFile.GET.NMSPollingInput input)
        {
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                transfer.Data = new LoadInitialDataOutput();
                transfer.Data.NewsFile = new List<GetOutput>();

                for (int i = 0; i < input.SequenceList.Count(); i++)
                {
                    GetOutput nFileOutput = new GetOutput();
                    NewsFile nfile = fileService.UpdateNewsFileSequenceByNewsFileId(input.SequenceList[i], i);
                    List<FileDetail> filedetail = new List<FileDetail>();
                    filedetail = filedetailService.GetFileDetailByNewsFileId(nfile.NewsFileId);


                    if (!String.IsNullOrEmpty(nfile.Highlights))
                        nfile.IsTitle = true;
                    if (filedetail != null)
                    {
                        if (!String.IsNullOrEmpty(filedetail.Last().Text))
                            nfile.VoiceOver = true;
                    }

                    nFileOutput.CopyFrom(nfile);
                    transfer.Data.NewsFile.Add(nFileOutput);
                }
                transfer.IsSuccess = true;
                return transfer;

            }
            catch (Exception ex)
            {
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetTranslatedCommets")]
        public DataTransfer<List<EditorialComment>> GetTranslatedCommets(LoadInitialDataInput input)
        {

            DataTransfer<List<EditorialComment>> transfer = new DataTransfer<List<EditorialComment>>();
            try
            {
                transfer.Data = new List<EditorialComment>();
                if (input.NewsFileId != null && Convert.ToInt32(input.NewsFileId) != 0)
                {
                    List<EditorialComment> edComment = new List<EditorialComment>();
                    edComment = editorialCommentservice.GetCommentByNewsFileIdAndLanguageCode(Convert.ToInt32(input.NewsFileId));

                    if (edComment != null)
                    {
                        edComment = edComment.Where(x => x.LanguageCode == Convert.ToInt32(input.LanguageCode)).ToList() ?? new List<EditorialComment>();
                        transfer.Data = edComment;
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetAllNewsFiles")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> GetAllNewsFiles(NMS.Core.DataTransfer.NewsFile.GET.NMSPollingInput input)
        {

            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            List<NewsFile> news = new List<NewsFile>();
            try
            {
                //NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput list = fileService.GetNewsFiles(input);
                NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput list = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
                // news = fileService.GetNewsFileWithFilterFromTo(new List<Filter>() { new Filter() { FilterId = 78 } }, DateTime.UtcNow.AddDays(-7), false, string.Empty, input.PageOffset, input.PageSize, null);

                Program program = new Program();
                list.NewsFile = new List<GetOutput>();
                list.Programs = new List<Core.DataTransfer.Program.GetOutput>();
                // if (news != null && news.Count > 0)
                //  {
                if (input.WorkRoleId == (int)WorkRole.ControllerOutput || input.WorkRoleId == (int)WorkRole.CopyWriter || input.WorkRoleId == (int)WorkRole.Editorial)
                {
                    //news = news.Where(x => x.NewsStatus > 0).ToList();

                    if (input.FolderIds != null && input.FolderIds.Length > 0)
                    {
                        for (int i = 0; i <= input.FolderIds.Length - 1; i++)
                        {
                            var Folder = folderService.GetFolder(input.FolderIds[i]);

                            if (Folder != null)
                            {
                                if (Folder.ProgramId.HasValue)
                                {
                                    var programnews = fileService.GetNewsFileByProgramIdwithstatus(Folder.ProgramId.Value);
                                    if (programnews != null)
                                    {
                                        for (int x = 0; x <= programnews.Count - 1; x++)
                                        {
                                            List<FileDetail> filedetail = new List<FileDetail>();
                                            filedetail = filedetailService.GetFileDetailByNewsFileId(programnews[x].NewsFileId);

                                            if (!String.IsNullOrEmpty(programnews[x].Highlights))
                                                programnews[x].IsTitle = true;
                                            if (filedetail != null)
                                            {
                                                if (!String.IsNullOrEmpty(filedetail.Last().Text))
                                                    programnews[x].VoiceOver = true;
                                            }
                                            news.Add(programnews[x]);
                                        }
                                    }


                                    program = programservice.GetProgram(Folder.ProgramId.Value);

                                    if (program != null)
                                    {
                                        NMS.Core.DataTransfer.Program.GetOutput pgm = new Core.DataTransfer.Program.GetOutput();
                                        pgm.CopyFrom(program);
                                        list.Programs.Add(pgm);
                                    }
                                }
                            }

                        }

                    }
                }
                list.NewsFile.CopyFrom(news);
                //  }

                if (list != null)
                    transfer.Data = list;
            }
            catch (Exception ex)
            {
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetNewsFilesByProgram")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> GetNewsFilesByProgram(LoadInitialDataInput input)
        {

            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
            List<NewsFile> news = new List<NewsFile>();
            try
            {
                NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput list = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

                Program program = new Program();
                list.NewsFile = new List<GetOutput>();
                list.Programs = new List<Core.DataTransfer.Program.GetOutput>();

                if (input.ProgramId != null && input.ProgramId != "0")
                {
                    var programnews = fileService.GetNewsFileByProgramIdwithstatus(Convert.ToInt32(input.ProgramId));
                    list.NewsFile.CopyFrom(programnews);
                }


                if (list != null)
                    transfer.Data = list;
            }
            catch (Exception ex)
            {
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateProgramNewsFile")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> UpdateProgramNewsFile(NMS.Core.Models.ProgramNewsFileInput input)
        {

            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            try
            {
                if (input.Comments != null)
                {
                    NMS.Core.DataTransfer.EditorialComment.PostInput edt_comments = new NMS.Core.DataTransfer.EditorialComment.PostInput();
                    EditorialCommentController edtCommentController = new EditorialCommentController();

                    edt_comments.NewsFileId = Convert.ToString(input.NewsFileId);
                    edt_comments.EditorId = input.EditorId;
                    edt_comments.Comment = input.Comments;
                    if (input.LanguageCode != null)
                        edt_comments.LanguageCode = Convert.ToInt32(input.LanguageCode);

                    edtCommentController.AddComment(edt_comments);
                }
                else
                {
                    NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput list = new Core.DataTransfer.NewsFile.GET.NMSPollingOutput();
                    list.NewsFile = new List<GetOutput>();
                    list.FileDetail = new List<NMS.Core.DataTransfer.FileDetail.GetOutput>();

                    NewsFile newsEntity = fileService.GetNewsFile(input.NewsFileId);
                    newsEntity.Highlights = input.HighLights;
                    newsEntity.LastUpdateDate = DateTime.UtcNow;
                    list.NewsFile.CopyFrom(fileService.UpdateNewsFile(newsEntity));

                    var file_detail = filedetailService.GetFileDetailByNewsFileId(input.NewsFileId);
                    FileDetail fDetail = new FileDetail();
                    if (file_detail != null)

                    {
                        fDetail = file_detail.Last();
                        fDetail.Text = input.Description;
                        fDetail.LastUpdateDate = DateTime.UtcNow;
                        list.FileDetail.CopyFrom(filedetailService.UpdateFileDetail(fDetail));
                    }

                    else
                    {
                        fDetail.Text = input.Description;
                        fDetail.NewsFileId = input.NewsFileId;
                        fDetail.LastUpdateDate = DateTime.UtcNow;
                        fDetail.CreationDate = DateTime.UtcNow;
                        fDetail.Slug = newsEntity.Slug;
                        list.FileDetail.CopyFrom(filedetailService.InsertFileDetail(fDetail));
                    }




                    if (list != null)
                        transfer.Data = list;
                }
            }
            catch (Exception ex)
            {
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("Add")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> AddNews(NMS.Core.DataTransfer.News.PostInput input)
        {
            DataTransfer<Core.DataTransfer.News.GetOutput> transfer = new DataTransfer<Core.DataTransfer.News.GetOutput>();
            DataTransfer<Core.DataTransfer.NewsResourceEdit.GetOutput> transferResourceEdit = new DataTransfer<Core.DataTransfer.NewsResourceEdit.GetOutput>();
            try
            {
                if (input != null)
                {
                    NewsFile newsFile = null;
                    MNews news = null;
                    if (String.IsNullOrEmpty(input.Guid))
                    {

                        newsFile = fileService.InsertNews(input);

                        IChannelVideoService cS = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
                        if (input.ResourceEdit != null && input.ResourceEdit.Id > 0)
                        {
                            cS.MarkIsProcessed(input.ResourceEdit.Id);
                        }

                    }
                    if (newsFile != null)
                    {
                        news = fileService.ConvertNewsFileToMNews(newsFile);
                    }
                    if (news != null)
                    {
                        transfer.Data = new Core.DataTransfer.News.GetOutput();
                        transfer.Data.CopyFrom(news);
                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "Edit not supported" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }


        [HttpPost]
        [ActionName("AddNewsFileReturnNews")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> AddNewsFileReturnNews(NMS.Core.DataTransfer.News.PostInput input)
        {
            DataTransfer<Core.DataTransfer.News.GetOutput> transfer = new DataTransfer<Core.DataTransfer.News.GetOutput>();
            try
            {
                if (input != null)
                {
                    MNews news = null;
                    if (String.IsNullOrEmpty(input.Guid))
                    {
                        if (input.UserTypeId != 0 && input.UserTypeId == (int)NMS.Core.Enums.WorkRole.ControllerInput)
                        {
                            string MMSUsermanagementUrl = AppSettings.MMSUsermanagementUrl;
                            HttpWebRequestHelper helper = new HttpWebRequestHelper();
                            DataTransfer<List<Metadata>> output = helper.GetRequest<NMS.Core.DataTransfer.DataTransfer<List<Metadata>>>(MMSUsermanagementUrl + "api/admin/MetaDataByUserId?id=" + input.ReporterId, null, false);
                            if (output != null && output.Data != null)
                            {
                                var metaData = output.Data[0].MetaValue;
                                input.BureauId = Convert.ToInt32(metaData);
                            }

                        }
                        var newsfile = fileService.InsertNews(input);
                        if (newsfile != null)
                        {
                            news = fileService.ConvertNewsFileToMNews(newsfile);
                        }
                    }
                    if (news != null)
                    {
                        transfer.Data = new Core.DataTransfer.News.GetOutput();
                        transfer.Data.CopyFrom(news);
                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "Edit not supported" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("ReLoginUser")]
        public bool ReLoginUser(int UserId)
        {
            bool isSuccess = true;

            try
            {
                if (UserId != 0)
                {
                    RabbitMQNotification notif = new RabbitMQNotification(host);
                    var userId = UserId;
                    notif.PublishMessage(new RabbitWrapper<int> { Data = userId, EventName = "ReLoginUser" }, "user", "changed_" + userId);
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionLogger.Log(ex);
            }

            return isSuccess;
        }


        [HttpGet]
        [ActionName("GetAllMyNews")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> GetAllMyNews(int rId, int pageCount, int startIndex)
        {
            DataTransfer<List<Core.DataTransfer.News.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();

            try
            {
                List<Core.DataTransfer.News.GetOutput> news = new List<Core.DataTransfer.News.GetOutput>();
                var newsList = fileService.GetNewsByReporterId(rId, pageCount, startIndex);
                if(newsList != null)
                {
                    news.CopyFrom(newsList);
                }
                transfer.Data = news;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = "Exception occur.";
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("MyNewsPolling")]
        public DataTransfer<List<Core.DataTransfer.News.GetOutput>> MyNewsPolling(PollingObject obj)
        {
            DataTransfer<List<Core.DataTransfer.News.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.News.GetOutput>>();

            try
            {
                List<Core.DataTransfer.News.GetOutput> news = new List<Core.DataTransfer.News.GetOutput>();
                news.CopyFrom(fileService.MyNewsPolling(obj));
                transfer.Data = news;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = "Exception occur.";
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetNews")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> GetNews(string id)
        {
            DataTransfer<Core.DataTransfer.News.GetOutput> transfer = new DataTransfer<Core.DataTransfer.News.GetOutput>();
            try
            {
                MNews news = fileService.GetNews(id);
                if (news != null)
                {
                    transfer.Data = new Core.DataTransfer.News.GetOutput();
                    transfer.Data.CopyFrom(news);
                    transfer.Data.NewsFileId = Convert.ToInt32(news._id);

                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateNewsFileWithHistory")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.PostOutput> UpdateNewsFileWithHistory(NMS.Core.DataTransfer.News.PostInput postInput)
        {
            DataTransfer<NMS.Core.DataTransfer.NewsFile.PostOutput> transfer = new DataTransfer<Core.DataTransfer.NewsFile.PostOutput>();
            try
            {
                NewsFile file = new NewsFile();

                #region Mantain History

                NewsFileHistory history = fileService.InsertNewsHistory(postInput);

                #endregion

                #region UpdateNews
                file = fileService.UpdateNewsFileWithHistory(postInput);
                #endregion

                transfer.Data = new NMS.Core.DataTransfer.NewsFile.PostOutput();
                transfer.Data.CopyFrom(file);
                transfer.IsSuccess = true;

                return transfer;

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);

                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
                return transfer;
            }
        }


        [HttpGet]
        [ActionName("GetEventTypeByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.EventType.GetOutput>> GetEventTypeByTerm(string id = null)
        {
            DataTransfer<List<NMS.Core.DataTransfer.EventType.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.EventType.GetOutput>>();
            if (id == null)
                id = string.Empty;
            try
            {
                List<EventType> types = eventtypeservice.GetEventTypeByTerm(id.ToLower());
                transfer.Data = new List<Core.DataTransfer.EventType.GetOutput>();
                if (types != null)
                    transfer.Data.CopyFrom(types);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetRundownByEpisodeId")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> GetRundownByEpisodeId(int episodeId)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            try
            {
                NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput list = fileService.GetRundownByEpisodeId(episodeId);
                if (list != null)
                    transfer.Data = list;
            }
            catch (Exception ex)
            {
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("DeleteFileResouceByGuid")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> DeleteFileResouceByGuid(string guid)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            try
            {
                transfer.IsSuccess = fileService.DeleteFileResouceByGuid(guid);
            }
            catch (Exception ex)
            {
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertNewsFile")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.PostOutput> InsertNewsFile(NMS.Core.DataTransfer.NewsFile.PostInput input)
        {
            try
            {
                return fileService.Insert(input);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                DataTransfer<NMS.Core.DataTransfer.NewsFile.PostOutput> transfer = new DataTransfer<Core.DataTransfer.NewsFile.PostOutput>();
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
                return transfer;
            }
        }

        [HttpPost]
        [ActionName("UpdateNewsFileAction")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> UpdateNewsFileAction(NMS.Core.DataTransfer.NewsFile.PostInput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            try
            {
                NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput data = fileService.UpdateNewsFileAction(input);
                if (data != null)
                {
                    transfer.Data.CopyFrom(data);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateNewsFileSequence")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> UpdateNewsFileAction(string sequenceId)
        {

            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            try
            {
                if (sequenceId != null)
                {
                    var Ids = sequenceId.Split(',');
                    for (int i = 0; i < Ids.Count(); i++)
                    {
                        fileService.UpdateNewsFileSequence(Convert.ToInt32(Ids[i]), i);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateNewsFileStatus")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> UpdateNewsFileStatus(NMS.Core.DataTransfer.NewsFile.PostInput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            try
            {
                NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput data = fileService.UpdateNewsFileStatus(input);
                if (data != null)
                    transfer.Data.CopyFrom(data);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("MarkNewsFileStatus")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> MarkNewsFileStatus(NMS.Core.DataTransfer.NewsFile.PostInput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();

            try
            {
                TickerController controller = new TickerController();
                if (input.StatusId == null && input.IsVerified != null)
                    transfer.IsSuccess = fileService.MarkNewsVerificationStatus(input);
                else
                    transfer.IsSuccess = fileService.MarkNewsFilestatus(input);

                //if (transfer.IsSuccess && input.StatusId == ((int)NewsStatus.BreakingNews).ToString())
                //    controller.CreateTickerByNewsFileId(Convert.ToInt32(input.NewsFileId), Convert.ToInt32(input.CreatedBy));

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("MarkStatusSocialMediaNews")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> MarkStatusSocialMediaNews(int NewsFileId, bool Status)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();

            try
            {
                NewsFileFilter obj = new NewsFileFilter();
                List<NewsFileFilter> newsfilefilter = filefilterservice.GetNewsFileFilterByNewsFileId(NewsFileId);
                if (newsfilefilter != null)
                {
                    if (Status)
                    {
                        PostInput input = new PostInput();
                        input.NewsFileId = Convert.ToString(NewsFileId);
                        input.IsVerified = "true";
                        fileService.MarkNewsVerificationStatus(input);

                        NewsFileFilter filter = new NewsFileFilter();
                        filter.CreationDate = DateTime.UtcNow;
                        filter.LastUpdateDate = DateTime.UtcNow;
                        filter.NewsFileId = NewsFileId;
                        filter.IsActive = true;
                        filter.FilterId = (int)NewsFilters.AllNews;
                        obj = filefilterservice.InsertNewsFileFilter(filter);
                    }
                    else
                    {
                        RemoveNewsFile(NewsFileId);
                    }
                }

                transfer.IsSuccess = true;

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("PublishNews")]
        public DataTransfer<NMS.Core.DataTransfer.FileStatusHistory.PostOutput> PublishNews(NMS.Core.DataTransfer.NewsFile.PostInput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.FileStatusHistory.PostOutput> transfer = new DataTransfer<Core.DataTransfer.FileStatusHistory.PostOutput>();
            transfer.Data = new NMS.Core.DataTransfer.FileStatusHistory.PostOutput();

            try
            {
                FileStatusHistory data = fileService.PublishNews(input);
                if (data != null)
                    transfer.Data.CopyFrom(data);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateNewsFile")]
        public DataTransfer<NMS.Core.DataTransfer.FileDetail.PostOutput> UpdateNewsFile(NMS.Core.DataTransfer.FileDetail.PostOutput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.FileDetail.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.FileDetail.PostOutput>();
            transfer.Data = new NMS.Core.DataTransfer.FileDetail.PostOutput();

            try
            {
                NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput data = fileService.UpdateNewsFile(input);
                if (data != null)
                    transfer.Data.CopyFrom(data);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("MarkTitleVoiceOverStatus")]
        public DataTransfer<NMS.Core.DataTransfer.FileDetail.PostOutput> MarkTitleVoiceOverStatus(NMS.Core.DataTransfer.FileDetail.PostOutput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.FileDetail.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.FileDetail.PostOutput>();
            transfer.Data = new NMS.Core.DataTransfer.FileDetail.PostOutput();

            try
            {
                fileService.MarkTitleVoiceOverStatus(input);

                RabbitMQNotification notif = new RabbitMQNotification(host);
                notif.PublishMessage(new RabbitWrapper<int> { Data = input.NewsFileId, EventName = "TranslatedScriptUpdate" }, "TranslatedScriptUpdate", "changed_0");

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }



        [HttpPost]
        [ActionName("LoadInitialDataForWriters")]
        public DataTransfer<LoadInitialDataWriters> LoadInitialDataForWriters(LoadInitialDataInput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<LoadInitialDataWriters> transfer = new DataTransfer<LoadInitialDataWriters>();
            transfer.Data = new LoadInitialDataWriters();

            try
            {

                List<FileDetail> lst = new List<FileDetail>();
                lst = fileService.GetApprovedNewsfile(input.ProgramIdList);
                if (lst != null && lst.Count() > 0)
                {
                    transfer.Data.newsFiles = new List<FileDetail>();
                    transfer.Data.newsFiles.CopyFrom(lst);

                    List<TranslatedScript> _translatedScriptList = new List<TranslatedScript>();
                    _translatedScriptList = translatedscriptservice.GetTranslatedScriptByLanguageId(Convert.ToInt32(input.LanguageCode));
                    transfer.Data.translatedScrips = new List<TranslatedScript>();

                    if(_translatedScriptList !=null && _translatedScriptList.Count() > 0)
                       transfer.Data.translatedScrips.CopyFrom(_translatedScriptList);

                    transfer.Data.programDetails = new List<Program>();
                    for (int k = 0; k < input.ProgramIdList.Length; k++)
                    {
                        Program _prog = new Program();
                        _prog = programservice.GetProgram(input.ProgramIdList[k]);
                        if (_prog != null && _prog.ProgramId > 0)
                            transfer.Data.programDetails.Add(_prog);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("GetApprovedNewsfile")]
        public DataTransfer<List<FileDetail>> GetApprovedNewsfile(LoadInitialDataInput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<List<FileDetail>> transfer = new DataTransfer<List<FileDetail>>();
            transfer.Data =  new List<FileDetail>();

            try
            {
                List<FileDetail> lst = new List<FileDetail>();
                lst =  fileService.GetApprovedNewsfile(input.ProgramIdList);
                transfer.Data.CopyFrom(lst);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetTranslatedScripts")]
        public DataTransfer<List<TranslatedScript>> GetTranslatedScripts(int LanguageId)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<List<TranslatedScript>> transfer = new DataTransfer<List<TranslatedScript>>();
            transfer.Data = new List<TranslatedScript>();

            try
            {
                List<TranslatedScript> lst = new List<TranslatedScript>();
                lst = translatedscriptservice.GetTranslatedScriptByLanguageId(LanguageId);
                transfer.Data.CopyFrom(lst);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetTranslatedScriptById")]
        public DataTransfer<TranslatedScript> GetTranslatedScriptById(int translatedScriptId)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<TranslatedScript> transfer = new DataTransfer<TranslatedScript>();
            transfer.Data = new TranslatedScript();

            try
            {
                TranslatedScript lst = new TranslatedScript();
                lst = translatedscriptservice.GetTranslatedScript(translatedScriptId);
                transfer.Data.CopyFrom(lst);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpsertTranslatedScript")]
        public DataTransfer<TranslatedScript> UpsertTranslatedScript(TranslatedScript input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<TranslatedScript> transfer = new DataTransfer<TranslatedScript>();
            transfer.Data = new TranslatedScript();

            try
            {

                TranslatedScript _translatedScript = new TranslatedScript();
                if (input.TranslatedScriptId > 0)
                {
                    input.LastUpdateDate = DateTime.UtcNow;
                    _translatedScript = translatedscriptservice.UpdateTranslatedScript(input);
                }
                else
                {
                    _translatedScript = translatedscriptservice.InsertTranslatedScript(input);
                }

                RabbitMQNotification notif = new RabbitMQNotification(host);
                notif.PublishMessage(new RabbitWrapper<int> { Data = _translatedScript.TranslatedScriptId, EventName = "TranslatedScriptUpdate" }, "TranslatedScriptUpdate", "changed_1");

                transfer.Data.CopyFrom(_translatedScript);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateTranslatedStatus")]
        public DataTransfer<TranslatedScript> UpdateTranslatedStatus(TranslatedScript input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<TranslatedScript> transfer = new DataTransfer<TranslatedScript>();
            transfer.Data = new TranslatedScript();

            try
            {

                TranslatedScript _translatedScript = new TranslatedScript();

                _translatedScript = translatedscriptservice.GetTranslatedScript(input.TranslatedScriptId);
                _translatedScript.Status = input.Status;
                _translatedScript.LastUpdateDate = DateTime.UtcNow;
                _translatedScript = translatedscriptservice.UpdateTranslatedScript(_translatedScript);
                transfer.Data.CopyFrom(_translatedScript);

                EditorialComment edComment = new EditorialComment();
                edComment.Comment = input.Comment;
                edComment.NewsFileId = Convert.ToInt32(_translatedScript.NewsFileId);
                edComment.LanguageCode = _translatedScript.LanguageCode;

                editorialCommentservice.InsertEditorialComment(edComment);


                RabbitMQNotification notif = new RabbitMQNotification(host);                
                notif.PublishMessage(new RabbitWrapper<int> { Data = _translatedScript.TranslatedScriptId, EventName = "TranslatedScriptUpdate" }, "TranslatedScriptUpdate", "changed_1");

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        public DataTransfer<NMS.Core.DataTransfer.Folder.PostOutput> CreateRundownFolder(NMS.Core.DataTransfer.Folder.PostInput input)
        {
            DataTransfer<NMS.Core.DataTransfer.Folder.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.Folder.PostOutput>();
            try
            {
                transfer.Data = new Core.DataTransfer.Folder.PostOutput();
                Folder data = folderService.CreateRundownFolder(input);
                if (data != null)
                    transfer.Data.CopyFrom(data);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertUpdateNewsFile")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> InsertUpdateNewsFile(NMS.Core.DataTransfer.FileDetail.PostOutput input)
        {
            string Type = string.Empty;
            string ErrorMessage = string.Empty;
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput>();
            transfer.Data = new NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput();

            try
            {
                NMS.Core.DataTransfer.NewsFile.GET.NMSPollingOutput data = fileService.InsertUpdateNewsFile(input);
                if (data != null)
                {
                    transfer.Data.CopyFrom(data);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetNewsFileDetail")]
        public DataTransfer<Core.DataTransfer.FileDetail.GetOutput> GetNewsFileDetail(string newsFileId)
        {
            DataTransfer<Core.DataTransfer.FileDetail.GetOutput> transfer = new DataTransfer<Core.DataTransfer.FileDetail.GetOutput>();

            try
            {
                Core.DataTransfer.FileDetail.GetOutput fileDetail = new Core.DataTransfer.FileDetail.GetOutput();
                fileDetail.CopyFrom(filedetailService.GetFileDetailByNewsFileId(Convert.ToInt32(newsFileId)).Last());
                transfer.Data = fileDetail;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = "Exception occur.";
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("MarkNewsFileDeleted")]
        public DataTransfer<Core.DataTransfer.NewsFile.GetOutput> MarkNewsFileDeleted(string newsFileId)
        {
            DataTransfer<Core.DataTransfer.NewsFile.GetOutput> transfer = new DataTransfer<Core.DataTransfer.NewsFile.GetOutput>();

            try
            {
                NewsFile file = new NewsFile();
                Core.DataTransfer.NewsFile.GetOutput newsfile = new Core.DataTransfer.NewsFile.GetOutput();

                if (!string.IsNullOrEmpty(newsFileId))
                    file = fileService.GetNewsFile(Convert.ToInt32(newsFileId));

                if (file != null)
                    file.IsDeleted = true;
                file.LastUpdateDate = DateTime.UtcNow;

                file = fileService.UpdateNewsFile(file);
                newsfile.CopyFrom(file);
                transfer.Data = newsfile;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = "Exception occur.";
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("ProducerPolling")]
        public DataTransfer<LoadInitialDataOutput> ProducerPolling(LoadInitialDataInput input)
        {
            DateTime dtnow = DateTime.UtcNow;
            DataTransfer<LoadInitialDataOutput> transfer = new DataTransfer<LoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new LoadInitialDataOutput();
                    transfer.Data.ServerSyncTime = DateTime.UtcNow;
                    List<Filter> filters = new List<Filter>();
                    filters.CopyFrom(input.Filters);

                    if (input.ProgramFilterIds != null && input.ProgramFilterIds.Count > 0)
                    {

                        filters.RemoveAll(x => x.FilterTypeId == (int)FilterTypes.Program && !x.ParentId.HasValue);
                        foreach (var id in input.ProgramFilterIds)
                        {
                            filters.Add(new Filter { FilterTypeId = (int)FilterTypes.Program, FilterId = id });
                        }
                    }

                    List<Filter> discardedFilters = null;
                    if (input.FiltersToDiscard != null)
                    {
                        discardedFilters = new List<Filter>();
                        discardedFilters.CopyFrom(input.FiltersToDiscard);
                        discardedFilters.RemoveAll(x => filters.Any(y => y.FilterId == x.FilterId));
                    }

                    if (filters.Count == 0)
                        filters.Add(new Filter() { FilterId = 78 });

                    List<NewsFile> news = new List<NewsFile>();

                    if (input.NewsLastUpdateDate.Year > 1)
                    {
                        news = fileService.GetNewsFileWithFilterFromTo(filters, input.NewsLastUpdateDate, input.FolderIds, input.Term, !string.IsNullOrEmpty(input.PageOffset) ? Convert.ToInt32(input.PageOffset) : 0, !string.IsNullOrEmpty(input.PageSize) ? Convert.ToInt32(input.PageSize) : 50, null);
                    }
                    else if (input.From.Year > 1 && input.To.Year > 1)
                    {
                        news = fileService.GetNewsFileWithFilterFromTo(filters, input.From, input.FolderIds, input.Term, !string.IsNullOrEmpty(input.PageOffset) ? Convert.ToInt32(input.PageOffset) : 0, !string.IsNullOrEmpty(input.PageSize) ? Convert.ToInt32(input.PageSize) : 50, input.To.AddDays(1).AddMinutes(-1));

                    }
                    else if (input.NewsLastUpdateDate.Year <= 1 && input.From.Year <= 1 && input.To.Year <= 1)
                    {
                        input.From = DateTime.UtcNow.AddMonths(-1);
                        input.To = DateTime.UtcNow.Date.AddDays(1).AddMinutes(-1);
                        news = fileService.GetNewsFileWithFilterFromTo(filters, input.From, input.FolderIds, input.Term, !string.IsNullOrEmpty(input.PageOffset) ? Convert.ToInt32(input.PageOffset) : 0, !string.IsNullOrEmpty(input.PageSize) ? Convert.ToInt32(input.PageSize) : 50, input.To);
                    }

                    if (news != null)
                    {
                        //    DateTime dtNow = DateTime.UtcNow;
                        transfer.Data.NewsFile = new List<Core.DataTransfer.NewsFile.GetOutput>();
                        //    //update voiceover and istitle status for program related news
                        //    var _news = UpdateFileStatuses(news);
                        transfer.Data.NewsFile.CopyFrom(news);
                        //    System.Diagnostics.Debug.WriteLine("UpdateFileStatuses=>" + DateTime.UtcNow.Subtract(dtNow).TotalMilliseconds);
                    }

                    if (input.FilterLastUpdateDate != DateTime.MinValue)
                    {
                        var _filters = filterService.GetFilterByLastUpdateDate(input.FilterLastUpdateDate);

                        if (_filters != null)
                        {
                            List<int> removeFilterTypes = new List<int>() { (int)FilterTypes.Package, (int)FilterTypes.Radio, (int)FilterTypes.Records, (int)FilterTypes.NewsPaper, (int)FilterTypes.PublicReporter, (int)FilterTypes.Program, (int)FilterTypes.PressRelease };
                            _filters.RemoveAll(x => !x.IsApproved.HasValue || !x.IsApproved.Value || removeFilterTypes.Contains(x.FilterTypeId));
                            transfer.Data.Filters = new List<Core.DataTransfer.Filter.GetOutput>();
                            transfer.Data.Filters.CopyFrom(_filters);
                        }
                    }

                    if (input.Modules != null && input.Modules.Contains(Modules.Ticker) && input.NewsLastUpdateDate != DateTime.MinValue)
                    {
                        transfer.Data.TickerCategories = new List<Core.DataTransfer.TickerCategory.GetOutput>();
                        var tickerCategories = tickerCategoryService.GetTickerCategoryWithSubCategories(input.TickerCategories);
                        List<Ticker> tickers = tickerService.GetTickersByCategoryIds(tickerCategories.Select(x => x.TickerCategoryId).ToList(), input.NewsLastUpdateDate) ?? new List<Ticker>();
                        if (input.UserRole == (int)WorkRole.TickerProducerOneA)
                        {
                            tickers = tickers.OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                        }
                        else if (input.UserRole == (int)WorkRole.TickerCopyWriterOneA)
                        {
                            tickers = tickers.Where(x => x.StatusId == (int)TickersStatus.New || x.StatusId == (int)TickersStatus.ApprovalPending || x.StatusId == (int)TickersStatus.Rejected || x.StatusId == (int)TickersStatus.Approved).OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                        }
                        else if (input.UserRole == (int)WorkRole.TickerEditorial)
                        {
                            tickers = tickers.Where(x => x.StatusId == (int)TickersStatus.ApprovalPending).OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                        }
                        transfer.Data.AllTickers = new List<Core.DataTransfer.Ticker.GetOutput>();
                        transfer.Data.AllTickers.CopyFrom(tickers);

                    }

                    if (input.EpisodeIds != null && input.EpisodeIds.Length > 0)
                    {
                        List<NMS.Core.Entities.EditorialComment> editorialComments = new List<NMS.Core.Entities.EditorialComment>();
                        List<Slot> lst_slot = new List<Slot>();
                        for (int i = 0; i < input.EpisodeIds.Length; i++)
                        {
                            lst_slot = slotService.GetSlotByEpisode(input.EpisodeIds[i]);
                        }
                        if (lst_slot != null && lst_slot.Count() > 0)
                        {
                            foreach (var slot in lst_slot)
                            {
                                var comment = editorialCommentservice.GetCommentsBySlotIdAndLastUpdateDate(input.CommentLastUpdateDate, slot.SlotId);

                                if (comment != null && comment.Count > 0)
                                {
                                    for (int j = 0; j <= comment.Count - 1; j++)
                                    {
                                        editorialComments.Add(comment[j]);
                                    }
                                }

                            }

                            if (editorialComments != null && editorialComments.Count > 0)
                            {
                                transfer.Data.EditorialComments = new List<EditorialComment>();
                                transfer.Data.EditorialComments.CopyFrom(editorialComments);
                            }

                        }
                    }

                    if (input.IsBroadcastedcall)
                    {
                        List<NMS.Core.Entities.NewsFile> lstnewsfiles = new List<NMS.Core.Entities.NewsFile>();

                        DateTime dtfrom = new DateTime();
                        DateTime dtto = new DateTime();

                        if (input.NewsLastUpdateDate.Year <= 1)
                        {
                            dtfrom = DateTime.UtcNow.AddHours(-24);
                        }
                        else if (input.NewsLastUpdateDate.Year > 1)
                        {
                            dtfrom = input.NewsLastUpdateDate;
                        }

                        dtto = DateTime.UtcNow.Date.AddDays(1).AddMinutes(-1);
                        List<NewsFile> newsfiles = fileService.GetBroadcastedNewsFiles(dtfrom, dtto);

                        if (newsfiles != null && newsfiles.Count > 0)
                        {
                            transfer.Data.BroadcastedNewsFiles = new List<NewsFile>();
                            transfer.Data.BroadcastedNewsFiles = newsfiles;
                        }
                    }


                    #region Is Live


                    if (input.WorkRoleId == (int)WorkRole.ProgramManager)
                    {

                        if (input.ProgramIds != null && input.ProgramIds.Count > 0)
                        {
                            for(int i =0;i<=input.ProgramIds.Count -1; i++)
                            {
                                fileService.UpdateIsLiveBitNewsFile(input.ProgramIds[i]);
                            }
                            
                        }
                    }
                    #endregion




                    //LoadNews loadNews = newsService.GetNewsAndBunchOptimized(filters, 10, 0, input.NewsLastUpdateDate, DateTime.UtcNow, input.Term, discardedFilters);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }


        [HttpPost]
        [ActionName("GenerateRunDown")]
        public DataTransfer<bool> GenerateRunDown(Rundown runDown)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                if (runDown != null && runDown.NewsStatisticsParam.EpisodeId != null)
                {

                    List<SlotScreenTemplate> LstslotScreenTemplate = slotScreenTemplateService.GetByEpisodeId(runDown.NewsStatisticsParam.EpisodeId);
                    List<SlotTemplateScreenElement> LstslotTemplateScreenElements = slotTemplateScreenElementService.GetSlotTemplateScreenElementByEpisodeId(runDown.NewsStatisticsParam.EpisodeId);

                    bool IsPorgramValid = true;

                    //if (LstslotScreenTemplate != null && LstslotScreenTemplate.Count > 0)
                    //{
                    //    foreach (SlotScreenTemplate st in LstslotScreenTemplate)
                    //    {
                    //        if (!st.ParentId.HasValue)
                    //        {
                    //            var mediaElements = LstslotTemplateScreenElements.Where(x => (x.SlotScreenTemplateId == st.SlotScreenTemplateId) && (x.ScreenElementId == 3 || x.ScreenElementId == 13));
                    //            foreach (var element in mediaElements)
                    //            {
                    //                var resources = slotTemplateScreenElementResourceService.GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(element.SlotTemplateScreenElementId);
                    //                if (resources == null || resources.Count == 0)
                    //                {
                    //                    IsPorgramValid = false;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    if (IsPorgramValid)
                    {
                        MosActiveEpisode mosEpisode = new MosActiveEpisode();
                        mosEpisode.EpisodeId = runDown.NewsStatisticsParam.EpisodeId;
                        mosEpisode.StatusCode = 1;
                        mosEpisode = mosActiveEpisodeService.InsertIfNotExist(mosEpisode);
                        if (mosEpisode != null)
                        {
                            transfer.IsSuccess = true;
                            transfer.Data = true;
                        }
                        else
                        {
                            transfer.IsSuccess = false;
                            transfer.Errors = new string[] { "Episode Already Sent To Broadcast." };
                        }

                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "RUNDOWN INCOMPLETE: Please Assign Media on Selected Templates." };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("OrganizationTagsUpdate")]
        public DataTransfer<Core.DataTransfer.News.GetOutput> OrganizationTagsUpdate(NMS.Core.DataTransfer.News.PostInput input)
        {
            DataTransfer<Core.DataTransfer.News.GetOutput> transfer = new DataTransfer<Core.DataTransfer.News.GetOutput>();
            try
            {
                NewsFile file = new NewsFile();
                file = fileService.OrganizationTagsUpdate(input);

                transfer.Data = new Core.DataTransfer.News.GetOutput();
                transfer.Data.CopyFrom(file);
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("GetNewsFileById")]
        public DataTransfer<NMS.Core.DataTransfer.NewsFile.GetOutput> GetNewsFileById(NMS.Core.DataTransfer.NewsFile.PostInput input)
        {
            DataTransfer<NMS.Core.DataTransfer.NewsFile.GetOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.NewsFile.GetOutput>();
            try
            {
                transfer.Data = new Core.DataTransfer.NewsFile.GetOutput();
                transfer = fileService.Get(input.NewsFileId);

                DataTransfer<NMS.Core.DataTransfer.FileDetail.GetOutput> t2 = new DataTransfer<NMS.Core.DataTransfer.FileDetail.GetOutput>();
                t2 = GetNewsFileDetail(input.NewsFileId);
                transfer.Data.DescriptionText = t2.Data.Text;

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetNewsFilesDetailByFolderId")]
        public DataTransfer<List<NewsFile>> GetNewsFilesDetailByFolderId(int FolderId, int StoryCount)
        {
            DataTransfer<List<NewsFile>> transfer = new DataTransfer<List<NewsFile>>();
            try
            {
                transfer.Data = new List<NewsFile>();
                List<NewsFile> NewsFileList = fileService.GetNewsFilesByFolderId(FolderId, StoryCount);
                transfer.Data.CopyFrom(NewsFileList);
                transfer.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetNewsFileForTaggReport")]
        public DataTransfer<List<NewsFile>> GetNewsFileForTaggReport(DateTime? lstUpdate = null)
        {
            DataTransfer<List<NewsFile>> transfer = new DataTransfer<List<NewsFile>>();
            try
            {
                transfer.Data = new List<NewsFile>();
                List<NewsFile> NewsFileList = fileService.GetNewsFileForTaggReport(lstUpdate);

                transfer.Data.CopyFrom(NewsFileList);
                transfer.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetBroadcatedNewsFileDetail")]
        public DataTransfer<List<NewsFile>> GetBroadcatedNewsFileDetail(int ParentNewsId)
        {
            DataTransfer<List<NewsFile>> transfer = new DataTransfer<List<NewsFile>>();
            try
            {
                transfer.Data = new List<NewsFile>();
                List<NewsFile> NewsFileList = fileService.GetBroadcatedNewsFileDetail(ParentNewsId);

                transfer.Data.CopyFrom(NewsFileList);
                transfer.IsSuccess = true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.Errors = new string[] { ex.Message };
                transfer.IsSuccess = false;
            }
            return transfer;
        }


        public class SequenceIds
        {
            public string seqNo;
            public string Id;
        }


        public NewsFile FillNewsFileObject(NewsFile newsfile, int FolderId)
        {
            NewsFile news = new NewsFile();
            news.SequenceNo = newsfile.SequenceNo;
            news.Title = newsfile.Title;
            news.FolderId = FolderId;
            news.Source = newsfile.Source;
            news.Slug = newsfile.Slug;
            news.CreatedBy = newsfile.CreatedBy;
            news.StatusId = newsfile.StatusId;
            news.LocationId = newsfile.LocationId;
            news.CategoryId = newsfile.CategoryId;
            news.CreationDate = DateTime.UtcNow;
            news.LastUpdateDate = DateTime.UtcNow;
            news.NewsPaperDescription = newsfile.NewsPaperDescription;
            news.LanguageCode = newsfile.LanguageCode;
            news.PublishTime = newsfile.PublishTime;
            news.SourceTypeId = newsfile.SourceTypeId;
            news.SourceNewsUrl = newsfile.SourceNewsUrl;
            news.SourceFilterId = newsfile.SourceFilterId;

            if (newsfile.ParentId > 0)
                news.ParentId = newsfile.ParentId;
            else news.ParentId = newsfile.NewsFileId;

            news.SlugId = newsfile.SlugId;
            if (newsfile.ProgramId != null) { news.ProgramId = newsfile.ProgramId; }
            else { var f = folderService.GetFolder(FolderId); if (f != null) { news.ProgramId = f.ProgramId; } }
            news.ResourceGuid = newsfile.ResourceGuid;
            news.AssignedTo = newsfile.AssignedTo;
            news.EventType = newsfile.EventType;
            news.Coverage = newsfile.Coverage;
            news.ReporterBureauId = newsfile.ReporterBureauId;
            news.Highlights = newsfile.Highlights;
            news.SearchableText = newsfile.SearchableText;
            news.NewsStatus = newsfile.NewsStatus;
            news.IsVerified = newsfile.IsVerified;
            news.IsTagged = newsfile.IsTagged;


            return news;
        }

        public FileDetail FillFileDetailObj(FileDetail filedetailobj, int newsfileid)
        {
            FileDetail detail = new FileDetail();
            detail.CreationDate = DateTime.UtcNow;
            detail.LastUpdateDate = DateTime.UtcNow;
            detail.NewsFileId = newsfileid;
            detail.CreatedBy = filedetailobj.CreatedBy;
            detail.ReportedBy = filedetailobj.ReportedBy;
            detail.Slug = filedetailobj.Slug;
            detail.CgValues = filedetailobj.CgValues;
            detail.SlugId = filedetailobj.SlugId;
            detail.Title = filedetailobj.Title;
            detail.NewsDate = filedetailobj.NewsDate;

            return detail;
        }

        public NewsFileFilter FillNewsFileFilterObj(NewsFileFilter filter, int newsfileid)
        {
            NewsFileFilter newsfilter = new NewsFileFilter();
            newsfilter.FilterId = filter.FilterId;
            newsfilter.NewsFileId = newsfileid;
            newsfilter.IsActive = filter.IsActive;
            newsfilter.CreationDate = filter.CreationDate;
            newsfilter.LastUpdateDate = filter.LastUpdateDate;

            return newsfilter;
        }

        public FileCategory FillFileCategoryObj(FileCategory category, int newsfileid)
        {
            FileCategory filecat = new FileCategory();
            filecat.NewsFileId = newsfileid;
            filecat.CategoryId = category.CategoryId;
            filecat.CreationDate = DateTime.UtcNow;

            return filecat;
        }

        public FileLocation FillFileLocationObj(FileLocation loc, int newsfileid)
        {
            FileLocation filelocation = new FileLocation();
            filelocation.NewsFileId = newsfileid;
            filelocation.LocationId = loc.LocationId;
            filelocation.CreationDate = DateTime.UtcNow;

            return filelocation;
        }

        public FileTag FillFileTagObj(FileTag tag, int newsfileid)
        {
            FileTag filetag = new FileTag();
            filetag.NewsFileId = newsfileid;
            filetag.TagId = tag.TagId;
            filetag.Rank = tag.Rank;
            filetag.CreationDate = DateTime.UtcNow;
            filetag.LastUpdateDate = DateTime.UtcNow;

            return filetag;
        }

        public FileResource FillFileResourceObj(FileResource obj, int newsfileid)
        {
            FileResource resource = new FileResource();
            resource.NewsFileId = newsfileid;
            resource.ResourceTypeId = obj.ResourceTypeId;
            resource.Guid = obj.Guid;
            resource.Caption = obj.Caption;
            resource.Duration = obj.Duration;
            resource.CreationDate = obj.CreationDate;
            resource.UserId = obj.UserId;
            resource.LastUpdateDate = obj.LastUpdateDate;
            resource.IsActive = obj.IsActive;

            return resource;


        }

        public bool MakeDuplicate(NewsFile newsfile, Core.DataTransfer.NewsFile.GetOutput submittednews)
        {
            try
            {

                List<FileDetail> filedetail = filedetailService.GetFileDetailByNewsFileId(newsfile.NewsFileId);

                if (filedetail != null)
                {
                    for (int i = 0; i <= filedetail.Count - 1; i++)
                    {
                        var newsfiledetail = FillFileDetailObj(filedetail[i], submittednews.NewsFileId);
                        if (newsfiledetail != null)
                        {
                            filedetailService.InsertFileDetail(newsfiledetail);
                        }
                    }
                }

                //List<NewsFileFilter> newsfilefilter = newsfilefilterservice.GetNewsFileFilterByNewsFileId(newsfile.NewsFileId);
                //if (newsfilefilter != null)
                //{
                //    for (int i = 0; i <= newsfilefilter.Count - 1; i++)
                //    {
                //        var filefilter = FillNewsFileFilterObj(newsfilefilter[i], submittednews.NewsFileId);
                //        if (filefilter != null)
                //        {
                //            newsfilefilterservice.InsertNewsFileFilter(filefilter);
                //        }
                //    }
                //}

                //List<FileCategory> lstfilecategory = filecategoryservice.GetFileCategoryByNewsFileId(newsfile.NewsFileId);
                //if (lstfilecategory != null)
                //{
                //    for (int i = 0; i <= lstfilecategory.Count - 1; i++)
                //    {
                //        var filecategory = FillFileCategoryObj(lstfilecategory[i], submittednews.NewsFileId);
                //        if (filecategory != null)
                //        {
                //            filecategoryservice.InsertFileCategory(filecategory);
                //        }
                //    }
                //}

                //List<FileLocation> lstfilelocation = filelocationservice.GetFileLocationByNewsFileId(newsfile.NewsFileId);
                //if (lstfilelocation != null)
                //{
                //    for (int i = 0; i <= lstfilelocation.Count - 1; i++)
                //    {
                //        var filelocation = FillFileLocationObj(lstfilelocation[i], submittednews.NewsFileId);
                //        if (filelocation != null)
                //        {
                //            filelocationservice.InsertFileLocation(filelocation);
                //        }
                //    }
                //}


                //List<FileTag> lsttag = filetagservice.GetFileTagByNewsFileId(newsfile.NewsFileId);
                //if (lsttag != null)
                //{
                //    for (int i = 0; i <= lsttag.Count - 1; i++)
                //    {
                //        var filetag = FillFileTagObj(lsttag[i], submittednews.NewsFileId);
                //        if (filetag != null)
                //        {
                //            filetagservice.InsertFileTag(filetag);
                //        }
                //    }
                //}

                //List<FileResource> fileresource = fileresourceservice.GetFileResourceByNewsFileId(newsfile.NewsFileId);
                //if (fileresource != null)
                //{
                //    for (int i = 0; i <= fileresource.Count - 1; i++)
                //    {
                //        var resource = FillFileResourceObj(fileresource[i], submittednews.NewsFileId);
                //        if (fileresource != null)
                //        {
                //            fileresourceservice.InsertFileResource(resource);
                //        }
                //    }
                //}


                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<NewsFile> UpdateFileStatuses(List<NewsFile> news)
        {
            for (int i = 0; i <= news.Count - 1; i++)
            {
                if (!String.IsNullOrEmpty(news[i].Highlights))
                    news[i].IsTitle = true;
                List<FileDetail> _filedteail = new List<FileDetail>();
                _filedteail = filedetailService.GetFileDetailByNewsFileId(news[i].NewsFileId);
                if (_filedteail != null)
                {
                    if (!String.IsNullOrEmpty(_filedteail.Last().Text))
                        news[i].VoiceOver = true;
                }
            }
            return news;
        }

        public bool RemoveNewsFile(int newsfileid)
        {
            bool result = true;
            //List<NewsFileFilter> filefilter = filefilterservice.GetNewsFileFilterByNewsFileId(newsfileid);
            //List<FileCategory> filecategory = filecategoryservice.GetFileCategoryByNewsFileId(newsfileid);
            //List<FileLocation> filelocation = filelocationservice.GetFileLocationByNewsFileId(newsfileid);
            //List<FileResource> fileresource = fileresourceservice.GetFileResourceByNewsFileId(newsfileid);
            //List<FileTag> filetag = filetagservice.GetFileTagByNewsFileId(newsfileid);
            //List<FileFolderHistory> filefolderhistory = filefolderhistoryservice.GetFileFolderHistoryByNewsFileId(newsfileid);
            //List<FileStatusHistory> filestatushistory = filestatushistoryservice.GetFileStatusHistoryByNewsFileId(newsfileid);
            NewsFile newsfile = fileService.GetNewsFile(newsfileid);
            //  var file_detail = filedetailService.GetFileDetailByNewsFileId(newsfileid);

            try
            {
                //if (filefilter != null)
                //{
                //    for (int i = 0; i <= filefilter.Count - 1; i++)
                //    {
                //        filefilterservice.DeleteNewsFileFilter(filefilter[i].NewsFileFilterId);
                //    }
                //}

                //if (filecategory != null)
                //{
                //    for (int i = 0; i <= filecategory.Count - 1; i++)
                //    {
                //        filefilterservice.DeleteNewsFileFilter(filecategory[i].FileCategoryId);
                //    }
                //}

                //if (filelocation != null)
                //{
                //    for (int i = 0; i <= filelocation.Count - 1; i++)
                //    {
                //        filefilterservice.DeleteNewsFileFilter(filelocation[i].FileLocationId);
                //    }
                //}

                //if (fileresource != null)
                //{
                //    for (int i = 0; i <= fileresource.Count - 1; i++)
                //    {
                //        filefilterservice.DeleteNewsFileFilter(fileresource[i].FileResourceId);
                //    }
                //}

                //if (filetag != null)
                //{
                //    for (int i = 0; i <= filetag.Count - 1; i++)
                //    {
                //        filefilterservice.DeleteNewsFileFilter(filetag[i].FileTagId);
                //    }
                //}

                //if (filefolderhistory != null)
                //{
                //    for (int i = 0; i <= filefolderhistory.Count - 1; i++)
                //    {
                //        filefilterservice.DeleteNewsFileFilter(filefolderhistory[i].FileFolderHistoryId);
                //    }
                //}

                //if (filestatushistory != null)
                //{
                //    for (int i = 0; i <= filestatushistory.Count - 1; i++)
                //    {
                //        filefilterservice.DeleteNewsFileFilter(filestatushistory[i].FileStatusHistoryId);
                //    }
                //}

                //if (file_detail != null)
                //{
                //    for (int i = 0; i < file_detail.Count(); i++)
                //    {
                //        filedetailService.DeleteFileDetail(file_detail[i].FileDetailId);
                //    }
                //}
                if (newsfile != null)
                {
                    newsfile.IsDeleted = true;
                    newsfile.LastUpdateDate = DateTime.UtcNow;
                    fileService.UpdateNewsFile(newsfile);
                }

                return result;

            }
            catch (Exception ex)
            {
                result = false;
                return result;
            }
        }

    }
}

