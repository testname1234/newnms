﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.ChannelVideo;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Core.IController;

namespace NMS.Web.API
{
    public class NewsPaperController : ApiController, INewsPaperController
    {
        INewsPaperService newsPaperService = IoC.Resolve<INewsPaperService>("NewsPaperService");
        IDailyNewsPaperService dailyNewsPaperService = IoC.Resolve<IDailyNewsPaperService>("DailyNewsPaperService");
        INewsPaperPageService newsPaperPageService = IoC.Resolve<INewsPaperPageService>("NewsPaperPageService");
        INewsPaperPagesPartService newsPaperPagesPartService = IoC.Resolve<INewsPaperPagesPartService>("NewsPaperPagesPartService");
        INewsPaperPagesPartsDetailService newsPaperPagesPartsDetailService = IoC.Resolve<INewsPaperPagesPartsDetailService>("NewsPaperPagesPartsDetailService");

        [HttpGet]
        [ActionName("LoadInitialData")]
        public DataTransfer<LoadNewsPaperOutput> LoadInitialData(string fromDate, string toDate)
        {
            DataTransfer<LoadNewsPaperOutput> transfer = new DataTransfer<LoadNewsPaperOutput>();
            try
            {
                if (fromDate != null && toDate != null)
                {
                    transfer.Data = new LoadNewsPaperOutput();

                    List<NewsPaper> newsPapper = newsPaperService.GetAllNewsPaper();
                    if (newsPapper != null && newsPapper.Count > 0)
                    {
                        transfer.Data.NewsPappers = new List<Core.DataTransfer.NewsPaper.GetOutput>();
                        transfer.Data.NewsPappers.CopyFrom(newsPapper);
                    }
                    DateTime dtFrom = new DateTime();
                    DateTime dtto = new DateTime();
                    if (DateTime.TryParse(fromDate, out dtFrom) && DateTime.TryParse(toDate, out dtto))
                    {
                        // dt = dt.ToUniversalTime();
                        List<DailyNewsPaper> dailyNewsPapers = dailyNewsPaperService.GetDailyNewsPaperByDate(dtFrom.Date, dtto.Date);
                        if (dailyNewsPapers != null && dailyNewsPapers.Count > 0)
                        {
                            transfer.Data.DailyNewsPapers = new List<Core.DataTransfer.DailyNewsPaper.GetOutput>();
                            transfer.Data.DailyNewsPapers.CopyFrom(dailyNewsPapers);
                        }

                        List<NewsPaperPage> newsPaperPages = newsPaperPageService.GetNewsPaperPagesByDailyNewsPaperDate(dtFrom.Date, dtto.Date);
                        if (newsPaperPages != null && newsPaperPages.Count > 0)
                        {
                            transfer.Data.NewsPaperPages = new List<Core.DataTransfer.NewsPaperPage.GetOutput>();
                            transfer.Data.NewsPaperPages.CopyFrom(newsPaperPages);

                            List<NewsPaperPagesPart> newsPaperPagesParts = newsPaperPagesPartService.GetNewsPaperPagesPartsByDailyNewsPaperDate(dtFrom.Date, dtto.Date);
                            if (newsPaperPagesParts != null && newsPaperPagesParts.Count > 0)
                            {
                                List<NewsPaperPagesPartsDetail> newsPaperPagesPartsDetail = newsPaperPagesPartsDetailService.GetNewsPaperPagesPartsDetailByDailyNewsPaperDate(dtFrom.Date, dtto.Date);
                                for (int i = 0; i < transfer.Data.NewsPaperPages.Count; i++)
                                {
                                    List<NewsPaperPagesPart> listPageParts = newsPaperPagesParts.Where(x => x.NewsPaperPageId == transfer.Data.NewsPaperPages[i].NewsPaperPageId).ToList();
                                    transfer.Data.NewsPaperPages[i].NewsPaperPagesPart = new List<NMS.Core.DataTransfer.NewsPaperPagesPart.GetOutput>();
                                    transfer.Data.NewsPaperPages[i].NewsPaperPagesPart.CopyFrom(listPageParts);
                                    if (newsPaperPagesPartsDetail != null && newsPaperPagesPartsDetail.Count > 0)
                                    {
                                        for (int j = 0; j < transfer.Data.NewsPaperPages[i].NewsPaperPagesPart.Count; j++)
                                        {
                                            List<NewsPaperPagesPartsDetail> listPagePartDetail = newsPaperPagesPartsDetail.Where(x => x.NewsPaperPagesPartId == transfer.Data.NewsPaperPages[i].NewsPaperPagesPart[j].NewsPaperPagesPartId).ToList();
                                            transfer.Data.NewsPaperPages[i].NewsPaperPagesPart[j].NewsPaperPagesPartsDetail = new List<NMS.Core.DataTransfer.NewsPaperPagesPartsDetail.GetOutput>();
                                            transfer.Data.NewsPaperPages[i].NewsPaperPagesPart[j].NewsPaperPagesPartsDetail.CopyFrom(listPagePartDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("RefreshData")]
        public DataTransfer<LoadNewsPaperOutput> RefreshData(string fromDate, string toDate)
        {
            DataTransfer<LoadNewsPaperOutput> transfer = new DataTransfer<LoadNewsPaperOutput>();
            try
            {
                if (fromDate != null && toDate != null)
                {
                    transfer.Data = new LoadNewsPaperOutput();

                    DateTime dtFrom = new DateTime();
                    DateTime dtto = new DateTime();
                    if (DateTime.TryParse(fromDate, out dtFrom) && DateTime.TryParse(toDate, out dtto))
                    {
                        //  dt = dt.ToUniversalTime();
                        List<DailyNewsPaper> dailyNewsPapers = dailyNewsPaperService.GetDailyNewsPaperByDate(dtFrom.Date, dtto.Date);
                        if (dailyNewsPapers != null && dailyNewsPapers.Count > 0)
                        {
                            transfer.Data.DailyNewsPapers = new List<Core.DataTransfer.DailyNewsPaper.GetOutput>();
                            transfer.Data.DailyNewsPapers.CopyFrom(dailyNewsPapers);
                        }

                        List<NewsPaperPage> newsPaperPages = newsPaperPageService.GetNewsPaperPagesByDailyNewsPaperDate(dtFrom.Date, dtto.Date);
                        if (newsPaperPages != null && newsPaperPages.Count > 0)
                        {
                            transfer.Data.NewsPaperPages = new List<Core.DataTransfer.NewsPaperPage.GetOutput>();
                            transfer.Data.NewsPaperPages.CopyFrom(newsPaperPages);

                            List<NewsPaperPagesPart> newsPaperPagesParts = newsPaperPagesPartService.GetNewsPaperPagesPartsByDailyNewsPaperDate(dtFrom.Date, dtto.Date);
                            if (newsPaperPagesParts != null && newsPaperPagesParts.Count > 0)
                            {
                                List<NewsPaperPagesPartsDetail> newsPaperPagesPartsDetail = newsPaperPagesPartsDetailService.GetNewsPaperPagesPartsDetailByDailyNewsPaperDate(dtFrom.Date, dtto.Date);
                                for (int i = 0; i < transfer.Data.NewsPaperPages.Count; i++)
                                {
                                    List<NewsPaperPagesPart> listPageParts = newsPaperPagesParts.Where(x => x.NewsPaperPageId == transfer.Data.NewsPaperPages[i].NewsPaperPageId).ToList();
                                    transfer.Data.NewsPaperPages[i].NewsPaperPagesPart = new List<NMS.Core.DataTransfer.NewsPaperPagesPart.GetOutput>();
                                    transfer.Data.NewsPaperPages[i].NewsPaperPagesPart.CopyFrom(listPageParts);
                                    if (newsPaperPagesPartsDetail != null && newsPaperPagesPartsDetail.Count > 0)
                                    {
                                        for (int j = 0; j < transfer.Data.NewsPaperPages[i].NewsPaperPagesPart.Count; j++)
                                        {
                                            List<NewsPaperPagesPartsDetail> listPagePartDetail = newsPaperPagesPartsDetail.Where(x => x.NewsPaperPagesPartId == transfer.Data.NewsPaperPages[i].NewsPaperPagesPart[j].NewsPaperPagesPartId).ToList();
                                            transfer.Data.NewsPaperPages[i].NewsPaperPagesPart[j].NewsPaperPagesPartsDetail = new List<NMS.Core.DataTransfer.NewsPaperPagesPartsDetail.GetOutput>();
                                            transfer.Data.NewsPaperPages[i].NewsPaperPagesPart[j].NewsPaperPagesPartsDetail.CopyFrom(listPagePartDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }


        [HttpGet]
        [ActionName("GetNewspaperPageParts")]
        public DataTransfer<List<NMS.Core.DataTransfer.NewsPaperPagesPart.GetOutput>> GetNewspaperPageParts(int newsPaperPageId)
        {
            DataTransfer<List<NMS.Core.DataTransfer.NewsPaperPagesPart.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.NewsPaperPagesPart.GetOutput>>();
            try
            {
                transfer.Data = new List<NMS.Core.DataTransfer.NewsPaperPagesPart.GetOutput>();
                List<NewsPaperPagesPart> newsPaperPagesParts = newsPaperPagesPartService.GetNewsPaperPagesPartByNewsPaperPageId(newsPaperPageId);
                if (newsPaperPagesParts != null && newsPaperPagesParts.Count > 0)
                {
                    transfer.Data.CopyFrom(newsPaperPagesParts);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetNewspaperPagePartsDetail")]
        public DataTransfer<List<NMS.Core.DataTransfer.NewsPaperPagesPartsDetail.GetOutput>> GetNewspaperPagePartsDetail(int newsPaperPagesPartId)
        {
            DataTransfer<List<NMS.Core.DataTransfer.NewsPaperPagesPartsDetail.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.NewsPaperPagesPartsDetail.GetOutput>>();
            try
            {
                transfer.Data = new List<NMS.Core.DataTransfer.NewsPaperPagesPartsDetail.GetOutput>();
                List<NewsPaperPagesPartsDetail> newsPaperPagesPartsDetails = newsPaperPagesPartsDetailService.GetNewsPaperPagesPartsDetailByNewsPaperPagesPartId(newsPaperPagesPartId);
                if (newsPaperPagesPartsDetails != null && newsPaperPagesPartsDetails.Count > 0)
                {
                    transfer.Data.CopyFrom(newsPaperPagesPartsDetails);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetNewspaperDetailById")]
        public DataTransfer<List<NewsPaperData>> GetNewspaperDetailById(int dailyNewsPaperId)
        {
            DataTransfer<List<NewsPaperData>> transfer = new DataTransfer<List<NewsPaperData>>();
            try
            {
                List<NewsPaperPage> newsPaperPages = newsPaperPageService.GetNewsPaperPageByDailyNewsPaperId(dailyNewsPaperId);
                if (newsPaperPages != null)
                {
                    List<NewsPaperData> newsPaperDataList = new List<NewsPaperData>();
                    foreach (NewsPaperPage item in newsPaperPages)
                    {
                        NewsPaperData newsPaperData = new NewsPaperData();
                        newsPaperData.imageUrl = item.ImageGuid.ToString();

                        List<NewsPaperPagesPart> newsPaperPagesParts = newsPaperPagesPartService.GetNewsPaperPagesPartByNewsPaperPageId(item.NewsPaperPageId);
                        List<NewspaperDetail> npDetailList = new List<NewspaperDetail>();
                        foreach (NewsPaperPagesPart li in newsPaperPagesParts)
                        {
                            List<NewsPaperPagesPartsDetail> npPagesPartsDetail = newsPaperPagesPartsDetailService.GetNewsPaperPagesPartsDetailByNewsPaperPagesPartId(li.NewsPaperPagesPartId);
                            NewspaperDetail npDetail = new NewspaperDetail();
                            npDetail.newsPaperPagesPart = li;
                            npDetail.newsPaperPagesPartsDetail = npPagesPartsDetail;
                            npDetailList.Add(npDetail);
                        }
                        newsPaperData.newsPaperdetail = npDetailList;
                        newsPaperDataList.Add(newsPaperData);
                    }

                    transfer.Data = newsPaperDataList;
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }
    }

    public class NewsPaperData
    {
        public string imageUrl { get; set; }
        public List<NewspaperDetail> newsPaperdetail { get; set; }
    }

    public class NewspaperDetail
    {
        public NewsPaperPagesPart newsPaperPagesPart { get; set; }
        public List<NewsPaperPagesPartsDetail> newsPaperPagesPartsDetail { get; set; }
    }
}
