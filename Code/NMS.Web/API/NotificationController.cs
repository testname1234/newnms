﻿using NMS.Core.IController;
using NMS.Core;
using NMS.Core.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core.IService;

namespace NMS.Web.API
{
    public class NotificationController : ApiController, INotificationController
    {
        private INotificationService notificationService = IoC.Resolve<INotificationService>("NotificationService");

        [HttpGet]
        [ActionName("MarkAsRead")]
        public DataTransfer<string> MarkAsRead(int nid, int uid)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                notificationService.MarkAsRead(nid, uid);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }
    }
}
