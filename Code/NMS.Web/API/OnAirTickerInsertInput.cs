﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NMS.Core.Models
{
    public class OnAirTickerInsertInput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<NMS.Core.DataTransfer.OnAirTicker.PostInput> Tickers { get; set; }
    }
}
