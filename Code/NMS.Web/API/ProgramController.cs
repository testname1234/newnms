﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using NMS.Core;
using NMS.Core.IController;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;
using System.Linq;
using NMS.Core.DataTransfer.SlotScreenTemplate.POST;
using NMS.Core.DataTransfer.Slot;
using NMS.Core.Enums;
using NMS.Core.DataInterfaces.Mongo;
using System.Threading;
using System.Text;
using NMS.Core.Helper.Notification;
using System.Configuration;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace NMS.Web.API
{
    public class ProgramController : ApiController, IProgramController
    {
        IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");
        IProgramScheduleService programScheduleService = IoC.Resolve<IProgramScheduleService>("ProgramScheduleService");
        IScreenTemplateService screenTemplateService = IoC.Resolve<IScreenTemplateService>("ScreenTemplateService");
        IScreenTemplatekeyService screenTemplateKeyService = IoC.Resolve<IScreenTemplatekeyService>("ScreenTemplatekeyService");
        IScreenElementService screenElementService = IoC.Resolve<IScreenElementService>("ScreenElementService");
        ITemplateScreenElementService templateScreenElementService = IoC.Resolve<ITemplateScreenElementService>("TemplateScreenElementService");
        ISlotScreenTemplateService slotScreenTemplateService = IoC.Resolve<ISlotScreenTemplateService>("SlotScreenTemplateService");
        ISlotTemplateScreenElementService slotTemplateScreenElementService = IoC.Resolve<ISlotTemplateScreenElementService>("SlotTemplateScreenElementService");
        ISlotTemplateScreenElementResourceService slotTemplateScreenElementResourceService = IoC.Resolve<ISlotTemplateScreenElementResourceService>("SlotTemplateScreenElementResourceService");
        ISlotScreenTemplateMicService slotScreentTemplateMicService = IoC.Resolve<ISlotScreenTemplateMicService>("SlotScreenTemplateMicService");
        ISlotScreenTemplateCameraService slotScreentTemplateCameraService = IoC.Resolve<ISlotScreenTemplateCameraService>("SlotScreenTemplateCameraService");
        ISlotScreenTemplateResourceService slotScreenTemplateResourceService = IoC.Resolve<ISlotScreenTemplateResourceService>("SlotScreenTemplateResourceService");
        ISlotScreenTemplatekeyService slotScreenTemplatekeyService = IoC.Resolve<ISlotScreenTemplatekeyService>("SlotScreenTemplatekeyService");
        ISlotService slotService = IoC.Resolve<ISlotService>("SlotService");
        INewsService newsService = IoC.Resolve<INewsService>("NewsService");
        IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
        IMessageService messageService = IoC.Resolve<IMessageService>("MessageService");
        ISegmentService segmentService = IoC.Resolve<ISegmentService>("SegmentService");
        ITeamService teamService = IoC.Resolve<ITeamService>("TeamService");
        IMosActiveEpisodeService mosActiveEpisodeService = IoC.Resolve<IMosActiveEpisodeService>("MosActiveEpisodeService");
        INotificationService notificationService = IoC.Resolve<INotificationService>("NotificationService");
        IUserManagementService userManagementService = IoC.Resolve<IUserManagementService>("UserManagementService");
        IEpisodeService episodeService = IoC.Resolve<IEpisodeService>("EpisodeService");
        IMNewsRepository mNewsRepository = IoC.Resolve<IMNewsRepository>("MNewsRepository");
        IScriptService scriptService = IoC.Resolve<IScriptService>("ScriptService");
        IProgramSegmentService programSegmentService = IoC.Resolve<IProgramSegmentService>("ProgramSegmentService");
        ISegmentTypeService segmentTypeService = IoC.Resolve<ISegmentTypeService>("SegmentTypeService");
        IProgramConfigurationService programConfigurationService = IoC.Resolve<IProgramConfigurationService>("ProgramConfigurationService");
        ISuggestedFlowService suggestedFlowService = IoC.Resolve<ISuggestedFlowService>("SuggestedFlowService");
        ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");
        ICelebrityStatisticService celebrityStatisticService = IoC.Resolve<ICelebrityStatisticService>("CelebrityStatisticService");
        IProgramTypeService programTypeService = IoC.Resolve<IProgramTypeService>("ProgramTypeService");
        ITickerService tickerService = IoC.Resolve<ITickerService>("TickerService");
        IGuestService guestService = IoC.Resolve<IGuestService>("GuestService");
        ISlotHeadlineService slotheadlineservice = IoC.Resolve<ISlotHeadlineService>("SlotHeadlineService");

        private INewsBucketService newsBucketService = IoC.Resolve<INewsBucketService>("NewsBucketService");

        string host = ConfigurationManager.AppSettings["rabbitmqhost"];

        [HttpGet]
        [ActionName("GetAll")]
        public DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> GetAll()
        {
            try
            {
                return programService.GetAll();
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>>();
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
                return transfer;
            }
        }

        [HttpGet]
        [ActionName("GetTemplateMicAndCamera")]
        public DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> GetTemplateMicAndCamera(int id)
        {
            DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.SlotScreenTemplate.GetOutput>>();
            try
            {
                List<Segment> segments = segmentService.GetSegmentByEpisodeId(id);
                transfer.Data = new List<Core.DataTransfer.SlotScreenTemplate.GetOutput>();

                foreach (Segment seg in segments)
                {
                    List<Slot> slots = slotService.GetSlotBySegmentId(seg.SegmentId);
                    if (slots != null)
                    {
                        foreach (Slot selectedSlot in slots)
                        {
                            List<SlotScreenTemplate> slotScreenTemplates = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(selectedSlot.SlotId);

                            if (slotScreenTemplates != null)
                            {
                                foreach (SlotScreenTemplate template in slotScreenTemplates)
                                {
                                    template.SlotScreenTemplateMics = slotScreentTemplateMicService.GetSlotScreenTemplateMicBySlotScreenTemplateId(template.SlotScreenTemplateId);
                                    template.SlotScreenTemplateCameras = slotScreentTemplateCameraService.GetSlotScreenTemplateCameraBySlotScreenTemplateId(template.SlotScreenTemplateId);
                                    template.SlotScreenTemplateResources = slotScreenTemplateResourceService.GetSlotScreenTemplateResourceBySlotScreenTemplateId(template.SlotScreenTemplateId);
                                }
                            }

                            transfer.Data.CopyFrom(slotScreenTemplates);
                        }
                    }

                }



            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetProgramEpisode")]
        public DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> GetProgramEpisode(GetProgramEpisodeInput input)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Episode.GetOutput>>();
            try
            {
                if (input != null)
                {
                    List<Episode> episodes = new List<Episode>();

                    if (input.ProgramId > 0)
                    {
                        input.Date = input.Date.AddHours(5);
                        episodes = programService.GetProgramEpisode(input.ProgramId, input.Date);

                    }
                    else if (input.EpisodeId > 0)
                    {
                        Episode episode = episodeService.GetEpisode(input.EpisodeId);
                        if (episode != null)
                        {
                            programService.FillEpisode(episode);
                            episodes.Add(episode);
                        }
                    }
                    else if (input.SlotId > 0)
                    {
                        Episode episode = episodeService.GetEpisodeBySlotId(input.SlotId);
                        if (episode != null)
                        {
                            programService.FillEpisode(episode);
                            episodes.Add(episode);
                        }
                    }

                    transfer.Data = new List<Core.DataTransfer.Episode.GetOutput>();
                    transfer.Data.CopyFrom(episodes);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetRunDownPreviewByEpisodeId")]
        public DataTransfer<EpisodePreviewModel> GetRunDownPreviewByEpisodeId(int EpisodeId)
        {
            DataTransfer<EpisodePreviewModel> transfer = new DataTransfer<EpisodePreviewModel>();
            try
            {
                if (EpisodeId != null)
                {
                    EpisodePreviewModel episodepreviewmodel = programService.GetRunDownPreviewByEpisodeId(EpisodeId);
                    if (episodepreviewmodel != null)
                    {
                        transfer.Data = episodepreviewmodel;
                        transfer.IsSuccess = true;
                    }
                    else
                    {
                        transfer.Data = null;
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "Error in episode Generation" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Error in Preview" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetProgramEpisodeByRundownId")]
        public DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> GetProgramEpisode(int id)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Episode.GetOutput>>();
            try
            {
                if (id != null)
                {
                    Episode episodes = programService.GetProgramEpisodeByRundownId(id);
                    if (episodes != null)
                    {
                        transfer.Data = new List<Core.DataTransfer.Episode.GetOutput>();
                        List<Episode> ep = new List<Episode>();
                        ep.Add(episodes);
                        transfer.Data.CopyFrom(ep);
                    }
                    else
                    {
                        transfer.Data = null;
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetProgram")]
        public DataTransfer<NMS.Core.DataTransfer.Program.GetOutput> GetProgram(string id)
        {
            DataTransfer<NMS.Core.DataTransfer.Program.GetOutput> transfer = new DataTransfer<Core.DataTransfer.Program.GetOutput>();
            try
            {
                int programId = 0;
                if (int.TryParse(id, out programId))
                {
                    Program program = programService.GetProgram(programId);
                    transfer.Data = new Core.DataTransfer.Program.GetOutput();
                    transfer.Data.CopyFrom(program);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Program Id." };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("GetEpisodeDetailByProgram")]
        public DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> GetEpisodeDetailByProgram(EpisodeDetail detail)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Episode.GetOutput>>();
            try
            {
                if (detail.ProgramIds != null && detail.ProgramIds.Count > 0)
                {
                    if (detail.From.Year > 1 && detail.To.Year > 1)
                    {
                        List<NMS.Core.DataTransfer.Episode.GetOutput> lstepisode = episodeService.GetEpisodeDetailByProgram(detail.ProgramIds, detail.From, detail.To);
                        transfer.Data = new List<NMS.Core.DataTransfer.Episode.GetOutput>();
                        transfer.Data = lstepisode;
                    }
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetProgramByChannelId")]
        public DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> GetProgramByChannelId(int id)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Program.GetOutput>>();
            try
            {
                List<Program> program = programService.GetProgramByChannelId(id);
                transfer.Data = new List<Core.DataTransfer.Program.GetOutput>();
                transfer.Data.CopyFrom(program);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetProgramScreenFlow")]
        public DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput> GetProgramScreenFlow(string programId, string Producerid)
        {
            DataTransfer<NMS.Core.DataTransfer.Program.ProgramScreenOutput> transfer = new DataTransfer<Core.DataTransfer.Program.ProgramScreenOutput>();
            try
            {
                if (programId != null)
                {
                    transfer.Data = new Core.DataTransfer.Program.ProgramScreenOutput();
                    List<ScreenTemplate> screenTemplateWithKeys = new List<ScreenTemplate>();
                    List<ScreenTemplate> screenTemplate = screenTemplateService.GetScreenTemplateByProgramId(Convert.ToInt32(programId));
                    if (screenTemplate != null && screenTemplate.Count > 0)
                    {
                        foreach (ScreenTemplate st in screenTemplate)
                        {
                            List<ScreenTemplatekey> screenTemplatekey = screenTemplateKeyService.GetScreenTemplatekeyByScreenTemplateId(st.ScreenTemplateId);
                            if (screenTemplatekey != null && screenTemplatekey.Count > 0)
                            {
                                st.Keys = screenTemplatekey;
                            }

                        }
                        transfer.Data.ScreenTemplates = new List<Core.DataTransfer.ScreenTemplate.GetOutput>();
                        transfer.Data.ScreenTemplates.CopyFrom(screenTemplate);
                    }

                    List<TemplateScreenElement> templateElements = templateScreenElementService.GetTemplateScreenElementsByProgramId(Convert.ToInt32(programId));
                    if (templateElements != null && templateElements.Count > 0)
                    {
                        transfer.Data.ScreenElementIds = new List<int>();
                        transfer.Data.ScreenElementIds = templateElements.Select(x => x.ScreenElementId).Distinct().ToList<int>();

                        transfer.Data.TemplateScreenElements = new List<Core.DataTransfer.TemplateScreenElement.GetOutput>();
                        transfer.Data.TemplateScreenElements.CopyFrom(templateElements);
                    }

                    //List<ScreenTemplatekey> screenTemplatekey = screenTemplateKeyService.GetByScreenTemplateList(screenTemplate);
                    //if (screenTemplatekey != null && screenTemplatekey.Count > 0)
                    //{
                    //    transfer.Data.ScreenTemplatekeys = new List<Core.DataTransfer.ScreenTemplatekey.GetOutput>();
                    //    transfer.Data.ScreenTemplatekeys.CopyFrom(screenTemplatekey);
                    //}
                }
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = exp.Message;
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GenerateScreenTemplateImage")]
        public DataTransfer<string> GenerateScreenTemplateImage(NMS.Core.DataTransfer.ScreenTemplate.PostInput input)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                if (input != null)
                {
                    transfer.Data = programService.GenerateScreenTemplateImage(input);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertGuest")]
        public DataTransfer<List<NMS.Core.DataTransfer.Guest.PostOutput>> InsertGuest(GetProgramEpisodeInput input)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Guest.PostOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Guest.PostOutput>>();
            try
            {
                transfer.Data = new List<Core.DataTransfer.Guest.PostOutput>();
                if (input.Guests != null && input.Guests.Count() > 0)
                {
                    guestService.DeleteAllGuestBySlotId(Convert.ToInt32(input.SlotId));

                    for (int i = 0; i < input.Guests.Count(); i++)
                    {
                        Guest _entity = new Guest();
                        Core.DataTransfer.Guest.PostOutput guestOutput = new Core.DataTransfer.Guest.PostOutput();
                        _entity.CopyFrom(input.Guests[i]);
                        _entity.IsActive = true;
                        _entity.CreationDate = DateTime.UtcNow;
                        _entity = guestService.InsertGuest(_entity);

                        guestOutput.CopyFrom(_entity);
                        transfer.Data.Add(guestOutput);
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertUpdateSlots")]
        public DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>> InsertUpdateSlots(InputSlots input)
        {
            List<Slot> returnSlots = new List<Slot>();
            DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Slot.GetOutput>>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new List<Core.DataTransfer.Slot.GetOutput>();

                    List<Slot> lstSlots = new List<Slot>();
                    lstSlots.CopyFrom(input.Slots);

                    Episode episode = episodeService.GetEpisodeBySegmentId(lstSlots[0].SegmentId);

                    foreach (Slot slot in lstSlots)
                    {
                        if (slot.SlotId > 0)
                        {
                            var episodeId = slotService.GetEpisodeId(slot.SlotId);
                            if (episodeId != episode.EpisodeId)
                            {
                                slot.OldSlotId = slot.SlotId;
                                slot.SlotId = 0;
                            }
                        }
                        News news = null;


                        slot.SlotTypeId = (int)SlotTypes.News;
                        slot.UserId = input.UserId;
                        slot.CreationDate = DateTime.UtcNow;
                        slot.LastUpdateDate = slot.CreationDate;
                        slot.IsActive = true;


                        Slot tempSlot = new Slot();
                        Slot _tempslot = new Slot();
                        if (slot.TickerId.HasValue)
                            tempSlot = slotService.GetSlotByEpisodeAndNews(episode.EpisodeId, slot.TickerId.Value);
                        else
                        {
                            tempSlot = slotService.GetSlotByEpisodeAndNewsFileId(episode.EpisodeId, Convert.ToInt32(slot.NewsFileId));
                            _tempslot = slotService.GetSlot(slot.SlotId);
                        }


                        if (slot.SlotHeadlines != null && slot.SlotHeadlines.Count() > 0)
                        {
                            bool result = slotheadlineservice.DeleteSlotHeadlineBySlotId(slot.SlotId);
                            var slotheadlines = slot.SlotHeadlines;
                            slot.SlotHeadlines = new List<SlotHeadline>();

                            for (int i = 0; i < slotheadlines.Count(); i++)
                            {

                                SlotHeadline headline = new SlotHeadline();
                                headline.CreationDate = DateTime.UtcNow;
                                headline.LastUpdateDate = DateTime.UtcNow;
                                headline.IsActive = true;
                                headline.SlotId = slot.SlotId;
                                headline.LanguageCode = slot.LanguageCode;
                                headline.Headline = slotheadlines[i].Headline;
                                slot.SlotHeadlines.Add(slotheadlineservice.InsertSlotHeadline(headline));
                            }
                        }


                        if (tempSlot == null && _tempslot == null)
                        {
                            var _slot = slotService.InsertSlotBySegment(slot);
                            _slot.SlotHeadlines = new List<SlotHeadline>();
                            _slot.SlotHeadlines = slot.SlotHeadlines;

                            returnSlots.Add(_slot);

                            if (slot.OldSlotId > 0)
                            {
                                _slot.SlotScreenTemplates = programService.CopySlotInfo(slot.OldSlotId, _slot);
                            }

                            if (!slot.TickerId.HasValue)
                            {
                                NewsStatisticsThreadInfo newsStatisticsThreadInfo = new NewsStatisticsThreadInfo();
                                newsStatisticsThreadInfo.NewsGuid = slot.NewsGuid;
                                newsStatisticsThreadInfo.NewsStatisticsParam = input.NewsStatisticsParam;
                                newsStatisticsThreadInfo.StatisticType = NewsStatisticType.AddedToRundown;

                                // ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateNewsStatistics), (object)newsStatisticsThreadInfo);
                            }
                            try
                            {
                                var slotcount = slotService.GetSlotsByEpisodeId(episode.EpisodeId);
                                if (slotcount != null && slotcount.Count == 1)
                                {
                                    RabbitMQNotification notif = new RabbitMQNotification(host);
                                    InsertUpdateSlotNotification notifobj = new InsertUpdateSlotNotification();
                                    notifobj.SlotId = slotcount.FirstOrDefault().SlotId;
                                    notifobj.SegmentId = slotcount.FirstOrDefault().SegmentId;
                                    if (episode != null)
                                    {
                                        notifobj.EpisodeId = episode.EpisodeId;
                                        notifobj.ProgramId = episode.ProgramId;
                                    }

                                    notif.PublishMessage(new RabbitWrapper<InsertUpdateSlotNotification>
                                    {
                                        Data = notifobj,
                                        EventName = RabbitRoutingKey.ProgramEditorial.Events.Episode_FirstSlot
                                    },
                                    RabbitChannels.Program,
                                    RabbitRoutingKey.ProgramEditorial.Routes.Episode);
                                }

                            }
                            catch (Exception ex)
                            {
                                ExceptionLogger.Log(ex);
                            }
                        }
                        else
                        {
                            if (slot.SlotId != 0)
                            {
                                Slot _temp = new Slot();
                                _temp = slotService.UpdateSlotSequence(slot);
                                _temp.SlotHeadlines = new List<SlotHeadline>();
                                _temp.SlotHeadlines = slot.SlotHeadlines;
                                returnSlots.Add(_temp);
                            }

                        }

                    }
                    transfer.Data.CopyFrom(returnSlots);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
                try
                {
                    RabbitMQNotification notif = new RabbitMQNotification(host);
                    InsertUpdateSlotNotification obj = new InsertUpdateSlotNotification();

                    var episode = episodeService.GetEpisodeBySlotId(returnSlots.FirstOrDefault().SlotId);
                    obj.EpisodeId = episode.EpisodeId;
                    obj.ProgramId = episode.ProgramId;
                    notif.PublishMessage(new RabbitWrapper<InsertUpdateSlotNotification>
                    {
                        Data = obj,
                        EventName = RabbitRoutingKey.ProgramEditorial.Events.Episode_InfoChanged
                    },
                    RabbitChannels.Program,
                    RabbitRoutingKey.ProgramEditorial.Routes.Episode);
                }
                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("DeleteSlot")]
        public DataTransfer<bool> DeleteSlot(int slotId)
        {
            Episode episode = null;
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = false;
                if (slotId > 0)
                {
                    #region Slot Deletion
                    List<Guest> _guestList = new List<Guest>();
                    _guestList = guestService.GetAllGuestBySlotId(slotId);
                    slotheadlineservice.DeleteSlotHeadlineBySlotId(slotId);
                    if (_guestList != null)
                    {
                        for (int i = 0; i < _guestList.Count(); i++)
                        {
                            guestService.DeleteGuest(_guestList[i].GuestId);
                        }
                    }
                    episode = episodeService.GetEpisodeBySlotId(slotId);
                    slotService.DeleteSlot(slotId);


                    #endregion

                    //if (!string.IsNullOrEmpty(newsGuid) && newsGuid != "undefined")
                    //{
                    //    NewsStatisticsThreadInfo threadInfo = new NewsStatisticsThreadInfo();
                    //    threadInfo.NewsGuid = newsGuid;
                    //    threadInfo.EpisodeId = episodeId;

                    //    ThreadPool.QueueUserWorkItem(new WaitCallback(DeleteNewsStatistics), (object)threadInfo);
                    //}
                    //}
                    transfer.Data = true;
                }
                //if (NewsBucketId > 0)
                //{
                //    newsBucketService.DeleteNewsBucket(NewsBucketId);
                //    transfer.Data = true;
                //}
                if (!transfer.Data)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
                try
                {
                    RabbitMQNotification notif = new RabbitMQNotification(host);
                    InsertUpdateSlotNotification obj = new InsertUpdateSlotNotification();
                    obj.EpisodeId = episode.EpisodeId;
                    obj.ProgramId = episode.ProgramId;

                    notif.PublishMessage(new RabbitWrapper<InsertUpdateSlotNotification>
                    {
                        Data = obj,
                        EventName = RabbitRoutingKey.ProgramEditorial.Events.Episode_InfoChanged
                    },
                        RabbitChannels.Program,
                        RabbitRoutingKey.ProgramEditorial.Routes.Episode);
                }
                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("DeleteGuestById")]
        public DataTransfer<bool> DeleteGuestById(int guestId)
        {
            Episode episode = null;
            Guest guest = null;
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                guest = guestService.GetGuest(guestId);
                if (guest != null)
                {
                    episode = episodeService.GetEpisodeBySlotId(guest.SlotId);
                }
                transfer.Data = false;
                if (guestId > 0)
                {
                    #region Guest Deletion
                    guestService.DeleteGuest(guestId);
                    transfer.Data = true;
                    #endregion
                }
                try
                {
                    if (episode != null)
                    {
                        RabbitMQNotification notif = new RabbitMQNotification(host);
                        InsertUpdateSlotNotification obj = new InsertUpdateSlotNotification();
                        obj.EpisodeId = episode.EpisodeId;
                        obj.ProgramId = episode.ProgramId;

                        notif.PublishMessage(new RabbitWrapper<InsertUpdateSlotNotification>
                        {
                            Data = obj,
                            EventName = RabbitRoutingKey.ProgramEditorial.Events.Episode_InfoChanged
                        },
                            RabbitChannels.Program,
                            RabbitRoutingKey.ProgramEditorial.Routes.Episode);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                }

            }

            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("DeleteSlotHeadlineById")]
        public DataTransfer<bool> DeleteSlotHeadlineById(int Id)
        {
            SlotHeadline obj = null;
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = false;
                if (Id > 0)
                {
                    obj = slotheadlineservice.GetSlotHeadline(Id);

                    #region SlotHeadline Deletion

                    slotheadlineservice.DeleteSlotHeadline(Id);
                    transfer.Data = true;

                    #endregion

                }
                if (!transfer.Data)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
                try
                {
                    if (obj != null)
                    {
                        RabbitMQNotification notif = new RabbitMQNotification(host);
                        var episode = episodeService.GetEpisodeBySlotId(Convert.ToInt32(obj.SlotId));
                        notif.PublishMessage(new RabbitWrapper<int> { Data = episode.EpisodeId, EventName = "episodechanged" }, "program", "changed_" + episode.ProgramId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.Log(ex);
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetEpisodeStatusData")]
        public DataTransfer<List<EpisodeStatus>> GetEpisodeStatusData(string date)
        {
            DataTransfer<List<EpisodeStatus>> transfer = new DataTransfer<List<EpisodeStatus>>();
            try
            {
                List<EpisodeStatus> lst = GetEpisodeStatusDatatable(date);
                transfer.Data = new List<EpisodeStatus>();
                transfer.Data = lst;

                return transfer;
            }
            catch(Exception ex)
            {
                transfer.IsSuccess = false;
                return transfer;
            }
        }

        public List<EpisodeStatus> GetEpisodeStatusDatatable(string date)
        {
            List<EpisodeStatus> lststatus = new  List<EpisodeStatus>();

            if (!string.IsNullOrEmpty(date))
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
                conn.Open();
                string query = "select p.name as 'ProgramName', ep.programid,count(distinct sg.SegmentId) as 'TotalSegment',(select top(1) sum(s1.StoryCount) from segment s1 where s1.episodeid=sg.EpisodeId) as 'TotalStories',CONVERT(time,ep.[from]) as 'EpisodeTime' , count(distinct st.SlotId) as 'Stories',count(distinct sth.SlotId) as 'Headlines', count(distinct g.SlotId) as 'Guests', count(distinct ec.SlotId) as 'Feedback'from Episode ep left join program p on ep.ProgramId = p.ProgramId left join segment sg on sg.episodeid = ep.episodeid left join slot st on st.SegmentId = sg.SegmentId left join SlotHeadline sth on sth.SlotId = st.SlotId left join Guest g on g.SlotId = st.SlotId left join EditorialComment ec on ec.SlotId = st.SlotId where CONVERT(date, ep.[from]) = '"+date+"' and SegmentTypeId=2 group by ep.programid, sg.EpisodeId,ep.[from], p.Name";
                SqlCommand cmd = new SqlCommand(query, conn);
                DataTable dt = new DataTable();

                dt.Load(cmd.ExecuteReader());

               
                foreach (DataRow row in dt.Rows)
                {
                    EpisodeStatus status = new EpisodeStatus();
                    if (!string.IsNullOrEmpty(row["ProgramName"].ToString()))
                        status.ProgramName = row["ProgramName"].ToString();

                    if (!string.IsNullOrEmpty(row["programid"].ToString()))
                        status.ProgramId = (int)row["programid"];

                    if (!string.IsNullOrEmpty(row["TotalSegment"].ToString()))
                        status.TotalSegment = (int)row["TotalSegment"];

                    if (!string.IsNullOrEmpty(row["TotalStories"].ToString()))
                        status.TotalStories = (int)row["TotalStories"];

                    if (!string.IsNullOrEmpty(row["EpisodeTime"].ToString()))
                        status.EpisodeTime = row["EpisodeTime"].ToString();

                    if (!string.IsNullOrEmpty(row["Stories"].ToString()))
                        status.TotalStories = (int)row["Stories"];

                    if (!string.IsNullOrEmpty(row["Headlines"].ToString()))
                        status.Headlines = (int)row["Headlines"];

                    if (!string.IsNullOrEmpty(row["Guests"].ToString()))
                        status.Guests = (int)row["Guests"];

                    if (!string.IsNullOrEmpty(row["Feedback"].ToString()))
                        status.Feedback = (int)row["Feedback"];

                    lststatus.Add(status);
                }

                return lststatus;
            }
            return lststatus;
        }

        [HttpPost]
        [ActionName("SaveSlotScreenTemplateFlow")]
        public DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> SaveSlotScreenTemplateFlow(SlotScreenTemplateFlow input)
        {
            DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>>();
            try
            {
                if (input != null)
                {
                    DateTime dtNow = DateTime.UtcNow;
                    List<SlotScreenTemplate> slotScreenTemplatelist = new List<SlotScreenTemplate>();
                    transfer.Data = new List<Core.DataTransfer.SlotScreenTemplate.GetOutput>();

                    #region delete
                    //List<SlotScreenTemplate> LstSlotScreenTemplatesToDelete = new List<SlotScreenTemplate>();
                    //foreach (NMS.Core.DataTransfer.SlotScreenTemplate.PostInput slotScreenTemplateToDelete in input.SlotScreenTemplates)
                    //{
                    //    List<SlotScreenTemplate> slots = slotScreenTemplateService.GetSlotScreenTemplateBySlotId(slotScreenTemplateToDelete.SlotId);
                    //    if (slots != null)
                    //        LstSlotScreenTemplatesToDelete.AddRange(slots);
                    //}

                    //if (LstSlotScreenTemplatesToDelete != null)
                    //{
                    //    LstSlotScreenTemplatesToDelete = LstSlotScreenTemplatesToDelete.Where(x => !input.SlotScreenTemplates.Select(y => y.SlotScreenTemplateId).Contains(x.SlotScreenTemplateId)).ToList();
                    //    foreach (SlotScreenTemplate slotscreenTemplatetoDelete in LstSlotScreenTemplatesToDelete)
                    //    {
                    //        slotTemplateScreenElementService.DeleteSlotTemplateScreenElementBySlotScreenTemplateId(slotscreenTemplatetoDelete.SlotScreenTemplateId);                            
                    //        slotScreentTemplateMicService.DeleteBySlotScreenTemplateId(slotscreenTemplatetoDelete.SlotScreenTemplateId);
                    //        slotScreentTemplateCameraService.DeleteBySlotScreenTemplateId(slotscreenTemplatetoDelete.SlotScreenTemplateId);
                    //        slotScreenTemplateResourceService.DeleteAllBySlotScreenTemplateId(slotscreenTemplatetoDelete.SlotScreenTemplateId);
                    //        slotScreenTemplateService.DeleteSlotScreenTemplate(slotscreenTemplatetoDelete.SlotScreenTemplateId);

                    //    }
                    //}
                    #endregion

                    foreach (NMS.Core.DataTransfer.SlotScreenTemplate.PostInput slotScreenTemplate in input.SlotScreenTemplates)
                    {
                        SlotScreenTemplate slotScreenTemplateInsert = new SlotScreenTemplate();
                        List<SlotTemplateScreenElement> slottemplateScreenElements = new List<SlotTemplateScreenElement>();
                        if (slotScreenTemplate.SlotScreenTemplateId == 0)
                        {
                            ScreenTemplate screenTemplate = screenTemplateService.GetScreenTemplate(slotScreenTemplate.ScreenTemplateId);

                            screenTemplate.ParentId = slotScreenTemplate.ParentId;
                            slotScreenTemplateInsert.CopyFrom(screenTemplate);
                            slotScreenTemplateInsert.SlotId = slotScreenTemplate.SlotId;
                            slotScreenTemplateInsert.SequenceNumber = slotScreenTemplate.SequenceNumber;
                            slotScreenTemplateInsert.StatusId = (int)ScreenTemplateStatuses.InProcess;
                            slotScreenTemplateInsert.NleStatusId = (int)ScreenTemplateStatuses.InProcess;
                            slotScreenTemplateInsert.StoryWriterStatusId = (int)ScreenTemplateStatuses.InProcess;
                            slotScreenTemplateInsert.CreatonDate = dtNow;
                            slotScreenTemplateInsert.LastUpdateDate = dtNow;
                            slotScreenTemplateInsert.IsAssignedToNle = true;
                            slotScreenTemplateInsert.IsAssignedToStoryWriter = true;


                            if (slotScreenTemplateInsert.Name == null)
                            {
                                slotScreenTemplateInsert.Name = "";
                            }


                            var result = slotScreenTemplateService.InsertSlotScreenTemplate(slotScreenTemplateInsert);

                            if (result != null)
                            {
                                slotScreenTemplateService.MarkNotifications(result, input.UserId, input.UserRoleId, input.ProgramId, NotificationTypes.TaskAssigned);
                            }

                            List<TemplateScreenElement> templateScreenElements = templateScreenElementService.GetTemplateScreenElementsByProgramId(screenTemplate.ProgramId).Where(x => x.ScreenTemplateId == screenTemplate.ScreenTemplateId).ToList();
                            foreach (TemplateScreenElement templateScreenElement in templateScreenElements)
                            {
                                SlotTemplateScreenElement slotTemplateScreenElement = new SlotTemplateScreenElement();
                                slotTemplateScreenElement.CopyFrom(templateScreenElement);
                                slotTemplateScreenElement.SlotScreenTemplateId = result.SlotScreenTemplateId;
                                slotTemplateScreenElement.ResourceGuid = templateScreenElement.ImageGuid;
                                slotTemplateScreenElement.CreationDate = dtNow;
                                slotTemplateScreenElement.LastUpdateDate = dtNow;

                                slottemplateScreenElements.Add(slotTemplateScreenElementService.InsertSlotTemplateScreenElement(slotTemplateScreenElement));
                            }

                            result.SlotTemplateScreenElements = slottemplateScreenElements;
                            slotScreenTemplatelist.Add(result);
                        }
                        else
                        {
                            try
                            {
                                slotScreenTemplateInsert.CopyFrom(slotScreenTemplate);
                                var result = slotScreenTemplateService.UpdateSlotScreenTemplateSequence(slotScreenTemplateInsert);
                                List<SlotTemplateScreenElement> slotTemplateelements = slotTemplateScreenElementService.GetSlotTemplateScreenElementBySlotScreenTemplateId(result.SlotScreenTemplateId);
                                List<SlotScreenTemplateResource> slotScreenTemplateResources = slotScreenTemplateResourceService.GetSlotScreenTemplateResourceBySlotScreenTemplateId(result.SlotScreenTemplateId);
                                result.SlotScreenTemplatekeys = slotScreenTemplatekeyService.GetSlotScreenTemplatekeyBySlotScreenTemplateId(result.SlotScreenTemplateId);
                                result.SlotTemplateScreenElements = slotTemplateelements;
                                result.SlotScreenTemplateResources = slotScreenTemplateResources;

                                if (result.SlotTemplateScreenElements != null)
                                {
                                    foreach (var element in result.SlotTemplateScreenElements)
                                    {
                                        element.SlotTemplateScreenElementResources = slotTemplateScreenElementResourceService.GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(element.SlotTemplateScreenElementId);
                                    }
                                }

                                slotScreenTemplatelist.Add(result);
                            }
                            catch (Exception ex)
                            {
                                transfer.IsSuccess = false;
                                transfer.Errors = new string[1];
                                transfer.Errors[0] = ex.Message;
                            }
                        }
                    }
                    transfer.Data.CopyFrom(slotScreenTemplatelist);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = exp.Message;
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("DeleteSlotScreenTemplate")]
        public DataTransfer<bool> DeleteSlotScreenTemplate(int id, NewsStatisticsInput NewsStatisticsParam)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = false;
                if (id > 0)
                {
                    #region delete
                    slotTemplateScreenElementResourceService.DeleteBySlotScreenTemplateId(id);
                    slotTemplateScreenElementService.DeleteSlotTemplateScreenElementBySlotScreenTemplateId(id);
                    slotScreentTemplateMicService.DeleteBySlotScreenTemplateId(id);
                    slotScreentTemplateCameraService.DeleteBySlotScreenTemplateId(id);
                    slotScreenTemplateResourceService.DeleteAllBySlotScreenTemplateId(id);
                    slotScreenTemplatekeyService.DeleteBySlotScreenTemplateId(id);
                    notificationService.DeleteBySlotScreenTemplateId(id);
                    slotScreenTemplateService.DeleteSlotScreenTemplate(id);

                    #endregion

                    if (NewsStatisticsParam != null)
                        celebrityStatisticService.RefreshCelebrityStatistics(NewsStatisticsParam);

                    transfer.Data = true;
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("DeleteStoryScreenTemplateResource")]
        public DataTransfer<string> DeleteStoryScreenTemplateResource(string id)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    #region delete

                    transfer = slotTemplateScreenElementResourceService.Delete(id);

                    #endregion
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid input." };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("SaveSlotScreenElements")]
        public DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> SaveSlotScreenTemplateFlow(NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput input)
        {
            DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>>();
            try
            {
                if (input != null)
                {
                    DateTime dtNow = DateTime.UtcNow;
                    List<SlotScreenTemplate> slotScreenTemplatelist = new List<SlotScreenTemplate>();
                    List<int> celebrityIds = new List<int>();
                    transfer.Data = new List<Core.DataTransfer.SlotScreenTemplate.GetOutput>();

                    SlotScreenTemplate slotScreenTemplate = new SlotScreenTemplate();
                    slotScreenTemplate.CopyFrom(input);
                    List<SlotTemplateScreenElement> tempLst = new List<SlotTemplateScreenElement>();
                    slotScreenTemplate.IsAssignedToStoryWriter = true;
                    slotScreenTemplate.IsAssignedToNle = true;
                    if (slotTemplateScreenElementService.GetSlotTemplateScreenElementBySlotScreenTemplateId(input.SlotScreenTemplateId) != null)
                    {
                        tempLst = slotTemplateScreenElementService.GetSlotTemplateScreenElementBySlotScreenTemplateId(input.SlotScreenTemplateId).ToList();
                        if (tempLst.Count > 0)
                        {
                            List<SlotTemplateScreenElement> temp = tempLst.Where(x => x.ScreenElementId == 3 || x.ScreenElementId == 13).ToList();
                            if (temp != null && temp.Count <= 0)
                            {
                                slotScreenTemplate.IsAssignedToNle = false;
                            }
                        }
                    }
                    slotScreenTemplateService.UpdateSlotScreenTemplateSequenceAndScript(slotScreenTemplate);

                    foreach (NMS.Core.DataTransfer.SlotTemplateScreenElement.GetOutput slotTemplateScreenElement in input.SlotTemplateScreenElements)
                    {
                        //deleteing all resources of slottempletescreenelements

                        slotTemplateScreenElementService.UpdateSlotTemplateScreenElementResourceGuid(slotTemplateScreenElement.SlotTemplateScreenElementId, slotTemplateScreenElement.ResourceGuid);
                        if (slotTemplateScreenElement.CelebrityId.HasValue)
                        {
                            celebrityIds.Add(slotTemplateScreenElement.CelebrityId.Value);
                        }

                        slotTemplateScreenElementResourceService.DeleteBySlotTemplateScreenElementId(Convert.ToInt32(slotTemplateScreenElement.SlotTemplateScreenElementId));
                        if (slotTemplateScreenElement.SlotTemplateScreenElementResources != null && slotTemplateScreenElement.SlotTemplateScreenElementResources.Count > 0)
                        {
                            foreach (NMS.Core.DataTransfer.SlotTemplateScreenElementResource.GetOutput slotTemplateScreenElementResource in slotTemplateScreenElement.SlotTemplateScreenElementResources)
                            {
                                SlotTemplateScreenElementResource elementresource = new SlotTemplateScreenElementResource();
                                elementresource.SlotScreenTemplateId = slotTemplateScreenElementResource.SlotScreenTemplateId;
                                elementresource.SlotTemplateScreenElementId = slotTemplateScreenElementResource.SlotTemplateScreenElementId;
                                elementresource.ResourceGuid = slotTemplateScreenElementResource.ResourceGuid;
                                elementresource.CreationDate = DateTime.UtcNow;
                                elementresource.LastUpdateDate = elementresource.CreationDate;
                                elementresource.IsActive = true;
                                Core.DataTransfer.Resource.PostOutput ResInfo = resourceService.GetResourceFromMediaServerByGuid(slotTemplateScreenElementResource.ResourceGuid.ToString());
                                if (ResInfo != null)
                                {
                                    elementresource.ResourceTypeId = slotTemplateScreenElementResource.ResourceTypeId;
                                    elementresource.Duration = Convert.ToDecimal(ResInfo.Duration);
                                    elementresource.Caption = ResInfo.Caption;
                                }
                                slotTemplateScreenElementResourceService.InsertSlotTemplateScreenElementResource(elementresource);
                            }
                        }
                    }
                    if (input.SlotScreenTemplateResources != null && input.SlotScreenTemplateResources.Count > 0)
                    {
                        slotScreenTemplateResourceService.DeleteAllBySlotScreenTemplateId(slotScreenTemplate.SlotScreenTemplateId);
                        foreach (NMS.Core.DataTransfer.SlotScreenTemplateResource.GetOutput slotScreenTemplateResource in input.SlotScreenTemplateResources)
                        {
                            Core.DataTransfer.Resource.PostOutput ResInfo = resourceService.GetResourceFromMediaServerByGuid(slotScreenTemplateResource.ResourceGuid.ToString());
                            SlotScreenTemplateResource res = new SlotScreenTemplateResource();
                            res.CopyFrom(slotScreenTemplateResource);
                            res.CreationDate = dtNow;
                            if (ResInfo != null)
                            {
                                res.ResourceTypeId = ResInfo.ResourceTypeId;
                                res.Duration = Convert.ToDecimal(ResInfo.Duration);
                                res.Caption = ResInfo.Caption;
                            }
                            res.SlotScreenTemplateId = slotScreenTemplate.SlotScreenTemplateId;
                            slotScreenTemplateResourceService.InsertSlotScreenTemplateResource(res);
                        }
                    }

                    if (input.SlotScreenTemplatekeys != null && input.SlotScreenTemplatekeys.Count > 0)
                    {
                        slotScreenTemplatekeyService.DeleteBySlotScreenTemplateId(slotScreenTemplate.SlotScreenTemplateId);
                        foreach (NMS.Core.DataTransfer.SlotScreenTemplatekey.GetOutput slotScreenTemplatekey in input.SlotScreenTemplatekeys)
                        {
                            SlotScreenTemplatekey key = new SlotScreenTemplatekey();
                            key.CopyFrom(slotScreenTemplatekey);
                            key.CreationDate = dtNow;
                            key.SlotScreenTemplateId = slotScreenTemplate.SlotScreenTemplateId;
                            slotScreenTemplatekeyService.InsertSlotScreenTemplatekey(key);
                        }
                    }

                    if (string.IsNullOrEmpty(input.TemplateScreenElementImageGuid))
                    {
                        Guid tsguid = resourceService.UploadImageOnMS(AppDomain.CurrentDomain.BaseDirectory + input.TemplateScreenElementImageUrl);
                        slotScreenTemplate.ThumbGuid = tsguid;
                    }
                    else
                    {
                        slotScreenTemplate.ThumbGuid = new Guid(input.TemplateScreenElementImageGuid);
                    }

                    slotScreenTemplate.IsAssignedToNle = true;
                    slotScreenTemplate.IsAssignedToStoryWriter = true;
                    slotScreenTemplateService.UpdateSlotScreenTemplateThumb(slotScreenTemplate);
                    var entity = new Core.DataTransfer.SlotScreenTemplate.GetOutput();
                    entity.CopyFrom(slotScreenTemplate);
                    transfer.Data.Add(entity);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = exp.Message;
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertMediaWallTemplate")]
        public DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> InsertMediaWallTemplate(NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput input)
        {
            DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.SlotScreenTemplate.GetOutput>>();
            try
            {
                DateTime currentTime = DateTime.UtcNow;

                SlotScreenTemplate entity = new SlotScreenTemplate();
                List<SlotScreenTemplate> slotScreenTemplates = new List<SlotScreenTemplate>();
                List<SlotTemplateScreenElement> slottemplateScreenElements = new List<SlotTemplateScreenElement>();

                #region Delete Duplicate Templates

                List<SlotScreenTemplate> duplicateTemplates = slotScreenTemplateService.GetSlotScreenTemplateByParentId(input.ParentId.Value);
                if (duplicateTemplates != null && duplicateTemplates.Count > 0)
                {
                    for (int i = 0; i < duplicateTemplates.Count; i++)
                    {
                        slotTemplateScreenElementResourceService.DeleteBySlotScreenTemplateId(duplicateTemplates[i].SlotScreenTemplateId);
                        slotTemplateScreenElementService.DeleteSlotTemplateScreenElementBySlotScreenTemplateId(duplicateTemplates[i].SlotScreenTemplateId);
                        slotScreentTemplateMicService.DeleteBySlotScreenTemplateId(duplicateTemplates[i].SlotScreenTemplateId);
                        slotScreentTemplateCameraService.DeleteBySlotScreenTemplateId(duplicateTemplates[i].SlotScreenTemplateId);
                        slotScreenTemplateResourceService.DeleteAllBySlotScreenTemplateId(duplicateTemplates[i].SlotScreenTemplateId);
                        slotScreenTemplatekeyService.DeleteBySlotScreenTemplateId(duplicateTemplates[i].SlotScreenTemplateId);
                        notificationService.DeleteBySlotScreenTemplateId(duplicateTemplates[i].SlotScreenTemplateId);

                        slotScreenTemplateService.DeleteSlotScreenTemplate(duplicateTemplates[i].SlotScreenTemplateId);
                    }
                }
                #endregion

                ScreenTemplate screenTemplate = screenTemplateService.GetScreenTemplate(input.ScreenTemplateId);
                screenTemplate.ParentId = input.ParentId;
                entity.CopyFrom(screenTemplate);

                entity.SlotId = input.SlotId;
                entity.SequenceNumber = input.SequenceNumber;
                entity.Script = "";
                entity.Duration = input.Duration.HasValue ? input.Duration.Value : 0;
                entity.ScriptDuration = input.ScriptDuration.HasValue ? input.Duration.Value : 0;
                entity.VideoDuration = input.VideoDuration.HasValue ? input.VideoDuration.Value : 0;
                entity.IsAssignedToNle = input.IsAssignedToNle.HasValue ? input.IsAssignedToNle.Value : false;
                entity.IsAssignedToStoryWriter = input.IsAssignedToStoryWriter.HasValue ? input.IsAssignedToStoryWriter.Value : false;
                entity.Name = !string.IsNullOrEmpty(entity.Name) ? entity.Name : string.Empty;
                entity.StatusId = (int)ScreenTemplateStatuses.InProcess;
                entity.CreatonDate = currentTime;
                entity.LastUpdateDate = currentTime;
                entity.IsActive = true;

                if (string.IsNullOrEmpty(input.TemplateScreenElementImageGuid))
                {
                    Guid tsguid = resourceService.UploadImageOnMS(AppDomain.CurrentDomain.BaseDirectory + input.TemplateScreenElementImageUrl);
                    entity.ThumbGuid = tsguid;
                }
                else
                {
                    entity.ThumbGuid = new Guid(input.TemplateScreenElementImageGuid);
                }

                var slotScreenTemplate = slotScreenTemplateService.InsertSlotScreenTemplate(entity);

                if (slotScreenTemplate != null && input.ProgramId.HasValue)
                {
                    slotScreenTemplateService.MarkNotifications(slotScreenTemplate, input.UserId, input.UserRoleId, input.ProgramId.Value, NotificationTypes.TaskAssigned);
                }

                List<TemplateScreenElement> templateScreenElements = templateScreenElementService.GetTemplateScreenElementsByProgramId(screenTemplate.ProgramId).Where(x => x.ScreenTemplateId == screenTemplate.ScreenTemplateId).ToList();

                foreach (Core.DataTransfer.SlotTemplateScreenElement.GetOutput slotScreenTemplateElementOutputEntity in input.SlotTemplateScreenElements)
                {
                    SlotTemplateScreenElement temp = new SlotTemplateScreenElement();
                    TemplateScreenElement templateScreenElement = templateScreenElements.Where(x => x.TemplateScreenElementId == slotScreenTemplateElementOutputEntity.SlotTemplateScreenElementId).FirstOrDefault();
                    temp.CopyFrom(templateScreenElement);
                    temp.SlotScreenTemplateId = slotScreenTemplate.SlotScreenTemplateId;

                    temp.CreationDate = currentTime;
                    temp.LastUpdateDate = currentTime;
                    temp.IsActive = true;

                    var tempEntity = slotTemplateScreenElementService.InsertSlotTemplateScreenElement(temp);

                    if (tempEntity != null)
                    {
                        if (slotScreenTemplateElementOutputEntity.SlotTemplateScreenElementResources != null && slotScreenTemplateElementOutputEntity.SlotTemplateScreenElementResources.Count > 0)
                        {
                            tempEntity.SlotTemplateScreenElementResources = new List<SlotTemplateScreenElementResource>();

                            foreach (Core.DataTransfer.SlotTemplateScreenElementResource.GetOutput resEntity in slotScreenTemplateElementOutputEntity.SlotTemplateScreenElementResources)
                            {
                                Core.DataTransfer.Resource.PostOutput resourceInfo = resourceService.GetResourceFromMediaServerByGuid(resEntity.ResourceGuid.ToString());

                                SlotTemplateScreenElementResource resource = new SlotTemplateScreenElementResource();
                                resource.CopyFrom(resEntity);

                                resource.SlotScreenTemplateId = slotScreenTemplate.SlotScreenTemplateId;
                                resource.SlotTemplateScreenElementId = tempEntity.SlotTemplateScreenElementId;

                                resource.CreationDate = currentTime;
                                resource.LastUpdateDate = currentTime;
                                resource.IsActive = true;

                                if (resourceInfo != null)
                                {
                                    resource.ResourceTypeId = resourceInfo.ResourceTypeId;
                                    resource.Duration = Convert.ToDecimal(resourceInfo.Duration);
                                    resource.Caption = resourceInfo.Caption;
                                }

                                tempEntity.SlotTemplateScreenElementResources.Add(slotTemplateScreenElementResourceService.InsertSlotTemplateScreenElementResource(resource));
                            }
                        }

                        slottemplateScreenElements.Add(tempEntity);
                    }
                }


                slotScreenTemplate.SlotTemplateScreenElements = slottemplateScreenElements;
                slotScreenTemplates.Add(slotScreenTemplate);

                transfer.Data = new List<Core.DataTransfer.SlotScreenTemplate.GetOutput>();
                transfer.Data.CopyFrom(slotScreenTemplates);
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = exp.Message;
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("NLESaveSlotScreenElements")]
        public DataTransfer<List<SlotScreenTemplate>> NLESaveSlotScreenElements(SaveScreenTemplateInput input)
        {
            DataTransfer<List<SlotScreenTemplate>> transfer = new DataTransfer<List<SlotScreenTemplate>>();
            try
            {
                if (input != null)
                {
                    DateTime dtNow = DateTime.UtcNow;
                    transfer.Data = new List<SlotScreenTemplate>();
                    foreach (var template in input.Templates)
                    {
                        SlotScreenTemplate slotScreenTemplate = new SlotScreenTemplate();
                        slotScreenTemplate.CopyFrom(template);
                        slotScreenTemplate.SlotTemplateScreenElementResources = new List<SlotTemplateScreenElementResource>();
                        if (!string.IsNullOrEmpty(template.Script))
                            slotScreenTemplateService.UpdateSlotScreenScript(template.SlotScreenTemplateId, template.Script);

                        if (template.SlotScreenTemplateResources != null && template.SlotScreenTemplateResources.Count > 0)
                        {
                            slotScreenTemplateResourceService.DeleteAllBySlotScreenTemplateId(slotScreenTemplate.SlotScreenTemplateId);
                            foreach (NMS.Core.DataTransfer.SlotScreenTemplateResource.GetOutput slotScreenTemplateResource in template.SlotScreenTemplateResources)
                            {
                                SlotScreenTemplateResource res = new SlotScreenTemplateResource();
                                res.CopyFrom(slotScreenTemplateResource);
                                res.CreationDate = dtNow;
                                //res.SlotScreenTemplateId = slotScreenTemplate.SlotScreenTemplateId;
                                res.SlotScreenTemplateId = slotScreenTemplateResource.SlotScreenTemplateId;
                                res = slotScreenTemplateResourceService.InsertSlotScreenTemplateResource(res);
                                slotScreenTemplate.SlotScreenTemplateResources.Add(res);
                            }
                        }

                        if (template.SlotTemplateScreenElements != null && template.SlotTemplateScreenElements.Count > 0)
                        {
                            foreach (NMS.Core.DataTransfer.SlotTemplateScreenElement.GetOutput slotTemplateScreenElement in template.SlotTemplateScreenElements)
                            {
                                if (slotTemplateScreenElement.SlotTemplateScreenElementResources != null && slotTemplateScreenElement.SlotTemplateScreenElementResources.Count > 0)
                                {
                                    slotTemplateScreenElementResourceService.DeleteBySlotTemplateScreenElementId(slotTemplateScreenElement.SlotTemplateScreenElementId);
                                    foreach (NMS.Core.DataTransfer.SlotTemplateScreenElementResource.GetOutput slotTemplateScreenElementResource in slotTemplateScreenElement.SlotTemplateScreenElementResources)
                                    {
                                        SlotTemplateScreenElementResource res = new SlotTemplateScreenElementResource();
                                        res.CopyFrom(slotTemplateScreenElementResource);
                                        res.CreationDate = dtNow;
                                        res = slotTemplateScreenElementResourceService.InsertSlotTemplateScreenElementResource(res);
                                        slotScreenTemplate.SlotTemplateScreenElementResources.Add(res);
                                    }
                                }
                            }
                        }

                        transfer.Data.Add(slotScreenTemplate);
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = exp.Message;
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("UpdateScreenElementGuid")]
        public DataTransfer<bool> UpdateScreenElementGuid(string id, string guid)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                slotTemplateScreenElementService.UpdateSlotTemplateScreenElementResourceGuid(Convert.ToInt32(id), new Guid(guid));
                transfer.Data = true;
            }
            catch (Exception exp)
            {
                transfer.Data = false;
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = exp.Message;
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }
        [HttpPost]
        [ActionName("NLELoadInitialData")]
        public DataTransfer<NLELoadInitialDataOutput> NLELoadInitialData(NLELoadInitialDataInput input)
        {
            DataTransfer<NLELoadInitialDataOutput> transfer = new DataTransfer<NLELoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new NLELoadInitialDataOutput();

                    List<Episode> episodes = programService.GetProgramEpisodeByDate(input.UserId, (TeamRoles)input.TeamRoleId, input.Date, input.LastUpdateDate);
                    transfer.Data.Episodes = new List<Core.DataTransfer.Episode.GetOutput>();
                    transfer.Data.Episodes.CopyFrom(episodes);

                    List<Program> programs = programService.GetProgramByUserTeam(input.UserId);
                    transfer.Data.Programs = new List<Core.DataTransfer.Program.GetOutput>();
                    transfer.Data.Programs.CopyFrom(programs);

                    List<Message> chatMessages = messageService.GetAllUserMessages(input.UserId);
                    if (chatMessages != null && chatMessages.Count > 0)
                    {
                        transfer.Data.Messages = new List<Core.DataTransfer.Message.GetOutput>();
                        transfer.Data.Messages.CopyFrom(chatMessages);
                    }

                    List<User> users = userManagementService.GetAllUsers();
                    if (users != null && users.Count > 0)
                    {
                        transfer.Data.users = new List<Core.DataTransfer.User.GetOutput>();
                        transfer.Data.users.CopyFrom(users);
                    }

                    PopulateNotifications(transfer, input.UserId, input.NotificationsLastUpdateDate);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetSlotScreenTemplateComments")]
        public DataTransfer<List<Core.DataTransfer.Message.GetOutput>> GetSlotScreenTemplateComments(int id)
        {
            DataTransfer<List<Core.DataTransfer.Message.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Message.GetOutput>>();
            try
            {
                List<Message> chatMessages = messageService.GetAllUserPendingMessagesBySlotScreenTemplateId(id);
                if (chatMessages != null && chatMessages.Count > 0)
                {
                    transfer.Data = new List<Core.DataTransfer.Message.GetOutput>();
                    transfer.Data.CopyFrom(chatMessages);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetSlotScreenTemplate")]
        public DataTransfer<Core.DataTransfer.SlotScreenTemplate.GetOutput> GetSlotScreenTemplate(int id)
        {
            DataTransfer<Core.DataTransfer.SlotScreenTemplate.GetOutput> transfer = new DataTransfer<Core.DataTransfer.SlotScreenTemplate.GetOutput>();
            try
            {
                var template = slotScreenTemplateService.GetSlotScreenTemplate(id);
                if (template != null)
                {
                    transfer.Data = new Core.DataTransfer.SlotScreenTemplate.GetOutput();
                    List<SlotTemplateScreenElementResource> slotScreenTemplateResources = slotTemplateScreenElementResourceService.GetBySlotScreenTemplateId(template.SlotScreenTemplateId);
                    template.SlotTemplateScreenElementResources = slotScreenTemplateResources;
                    transfer.Data.CopyFrom(template);
                    transfer.Data.StoryWriterStatusId = template.StoryWriterStatusId;
                    transfer.Data.NleStatusId = template.NleStatusId;

                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetGuestEpisodeReport")]
        public DataTransfer<Core.DataTransfer.Guest.GetOutput> GetGuestEpisodeReport()
        {
            DataTransfer<Core.DataTransfer.Guest.GetOutput> transfer = new DataTransfer<Core.DataTransfer.Guest.GetOutput>();
            try
            {
                var guestreport = guestService.GetGuestEpisodeReport();
                transfer.Data = new Core.DataTransfer.Guest.GetOutput();
                transfer.Data.lstGuest = new List<Guest>();
                transfer.Data.lstGuest.CopyFrom(guestreport);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }



        [HttpPost]
        [ActionName("MarkSlotScreenTemplateStatus")]
        public DataTransfer<bool> MarkSlotScreenTemplateStatus(ScreenTemplateStatusInput input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();

            try
            {
                if (input != null)
                {
                    transfer.Data = slotScreenTemplateService.UpdateSlotScreenTemplateStatus(input.SlotScreenTemplateId, input.StatusId, input.NLEStatusId, input.StoryWriterStatusId, input.UserId, input.UserRoleId, input.ProgramId);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("NLEPolling")]
        public DataTransfer<NLELoadInitialDataOutput> NLEPolling(NLELoadInitialDataInput input)
        {
            DataTransfer<NLELoadInitialDataOutput> transfer = new DataTransfer<NLELoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new NLELoadInitialDataOutput();

                    List<Episode> episodes = programService.GetProgramEpisodeByDate(input.UserId, (TeamRoles)input.TeamRoleId, input.Date, input.LastUpdateDate);
                    transfer.Data.Episodes = new List<Core.DataTransfer.Episode.GetOutput>();
                    transfer.Data.Episodes.CopyFrom(episodes);

                    if (!string.IsNullOrEmpty(input.SlotScreenTemplateId))
                    {
                        int slotScreenTemplateId = Convert.ToInt32(input.SlotScreenTemplateId);
                        List<Message> chatMessages = messageService.GetMessages(slotScreenTemplateId, input.CommentsLastUpdate);
                        if (chatMessages != null && chatMessages.Count > 0)
                        {
                            transfer.Data.Messages = new List<Core.DataTransfer.Message.GetOutput>();
                            transfer.Data.Messages.CopyFrom(chatMessages);
                        }
                    }

                    PopulateNotifications(transfer, input.UserId, input.NotificationsLastUpdateDate);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("NLEGetMore")]
        public DataTransfer<NLELoadInitialDataOutput> NLEGetMore(NLELoadInitialDataInput input)
        {
            DataTransfer<NLELoadInitialDataOutput> transfer = new DataTransfer<NLELoadInitialDataOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new NLELoadInitialDataOutput();

                    List<Episode> episodes = programService.GetProgramEpisodeByDate(input.UserId, (TeamRoles)input.TeamRoleId, input.Date, input.LastUpdateDate);
                    transfer.Data.Episodes = new List<Core.DataTransfer.Episode.GetOutput>();
                    transfer.Data.Episodes.CopyFrom(episodes);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("SendMessage")]
        public DataTransfer<NMS.Core.DataTransfer.Message.PostOutput> SendMessage(NMS.Core.DataTransfer.Message.PostInput input)
        {
            DataTransfer<NMS.Core.DataTransfer.Message.PostOutput> transfer = new DataTransfer<Core.DataTransfer.Message.PostOutput>();
            try
            {
                if (input != null)
                {
                    transfer.Data = new Core.DataTransfer.Message.PostOutput();

                    Message message = new Message
                    {
                        From = Convert.ToInt32(input.From),
                        Message = input.Message,
                        IsRecieved = false,
                        CreationDate = DateTime.UtcNow,
                        LastUpdateDate = DateTime.UtcNow,
                        SlotScreenTemplateId = input.SlotScreenTemplateId
                    };

                    List<Team> teams = teamService.GetTeamByProgramId(input.ProgramId);

                    Message output = new Message();

                    if (teams != null && teams.Count > 0)
                    {
                        if (input.ToNLE)
                            teams = teams.Where(x => x.WorkRoleId == (int)TeamRoles.NLE).ToList();
                        else if (input.ToStoryWriter)
                            teams = teams.Where(x => x.WorkRoleId == (int)TeamRoles.StoryWriter).ToList();
                        else if (input.ToProducer)
                            teams = teams.Where(x => x.WorkRoleId == (int)TeamRoles.Producer).ToList();

                        foreach (Team team in teams)
                        {
                            message.To = team.UserId;
                            output = messageService.InsertMessage(message);
                        }

                        if (output != null)
                        {
                            transfer.Data.CopyFrom(output);
                        }
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid input." };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GenerateRunDown")]
        public DataTransfer<bool> GenerateRunDown(Rundown runDown)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                if (runDown != null && runDown.NewsStatisticsParam.EpisodeId != null)
                {

                    List<SlotScreenTemplate> LstslotScreenTemplate = slotScreenTemplateService.GetByEpisodeId(runDown.NewsStatisticsParam.EpisodeId);
                    List<SlotTemplateScreenElement> LstslotTemplateScreenElements = slotTemplateScreenElementService.GetSlotTemplateScreenElementByEpisodeId(runDown.NewsStatisticsParam.EpisodeId);

                    bool IsPorgramValid = true;

                    if (LstslotScreenTemplate != null && LstslotScreenTemplate.Count > 0)
                    {
                        foreach (SlotScreenTemplate st in LstslotScreenTemplate)
                        {
                            if (!st.ParentId.HasValue)
                            {
                                var mediaElements = LstslotTemplateScreenElements.Where(x => (x.SlotScreenTemplateId == st.SlotScreenTemplateId) && (x.ScreenElementId == 3 || x.ScreenElementId == 13));
                                foreach (var element in mediaElements)
                                {
                                    var resources = slotTemplateScreenElementResourceService.GetSlotTemplateScreenElementResourceBySlotTemplateScreenElementId(element.SlotTemplateScreenElementId);
                                    if (resources == null || resources.Count == 0)
                                    {
                                        IsPorgramValid = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (IsPorgramValid)
                    {
                        MosActiveEpisode mosEpisode = new MosActiveEpisode();
                        mosEpisode.EpisodeId = runDown.NewsStatisticsParam.EpisodeId;
                        mosEpisode.StatusCode = 1;
                        mosEpisode = mosActiveEpisodeService.InsertIfNotExist(mosEpisode);
                        if (mosEpisode != null)
                        {

                            slotScreentTemplateMicService.InsertSlotScreenTemplateMicByEpisodeId(Convert.ToInt32(mosEpisode.EpisodeId));
                            slotScreentTemplateCameraService.InsertSlotScreenTemplateCameraByEpisodeId(Convert.ToInt32(mosEpisode.EpisodeId));

                            List<Slot> slots = slotService.GetSlotByEpisode(runDown.NewsStatisticsParam.EpisodeId);

                            foreach (Slot slot in slots)
                            {
                                NewsStatisticsThreadInfo threadInfo = new NewsStatisticsThreadInfo();
                                threadInfo.NewsStatisticsParam = runDown.NewsStatisticsParam;
                                threadInfo.NewsGuid = slot.NewsGuid;
                                threadInfo.StatisticType = NewsStatisticType.OnAired;

                                ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateNewsStatistics), (object)threadInfo);
                            }

                            if (runDown.NewsStatisticsParam.CelebrityIds != null && runDown.NewsStatisticsParam.CelebrityIds.Count > 0)
                            {
                                for (int i = 0; i < runDown.NewsStatisticsParam.CelebrityIds.Count; i++)
                                {
                                    celebrityService.UpdateCelebrityStatistics(runDown.NewsStatisticsParam, runDown.NewsStatisticsParam.CelebrityIds[i], NewsStatisticType.OnAired);
                                }
                            }
                            if (runDown.NewsStatisticsParam.SuggestedFlowKeys != null && runDown.NewsStatisticsParam.SuggestedFlowKeys.Count > 0)
                            {
                                for (int i = 0; i < runDown.NewsStatisticsParam.SuggestedFlowKeys.Count; i++)
                                {
                                    if (!string.IsNullOrEmpty(runDown.NewsStatisticsParam.SuggestedFlowKeys[i]))
                                    {
                                        SuggestedFlow entity = new SuggestedFlow();

                                        entity.ProgramId = runDown.NewsStatisticsParam.ProgramId;
                                        entity.EpisodeId = runDown.NewsStatisticsParam.EpisodeId;
                                        entity.Key = runDown.NewsStatisticsParam.SuggestedFlowKeys[i];

                                        suggestedFlowService.InsertSuggestedFlow(entity);
                                    }
                                }
                            }

                            transfer.IsSuccess = true;
                            transfer.Data = true;
                        }
                        else
                        {
                            transfer.IsSuccess = false;
                            transfer.Errors = new string[] { "Episode Already Sent To Broadcast." };
                        }

                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "RUNDOWN INCOMPLETE: Please Assign Media on Selected Templates." };
                    }

                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertPrgramSchedule")]
        public DataTransfer<NMS.Core.DataTransfer.ProgramSchedule.PostOutput> InsertPrgramSchedule(NMS.Core.DataTransfer.ProgramSchedule.PostInput input)
        {
            DataTransfer<NMS.Core.DataTransfer.ProgramSchedule.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.ProgramSchedule.PostOutput>();
            try
            {
                input.CreationDate = DateTime.UtcNow.ToString();
                input.LastUpdateDate = input.CreationDate;
                input.IsActive = "true";

                transfer = programScheduleService.Insert(input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetProgramScheduleByProgramId")]
        public DataTransfer<List<NMS.Core.DataTransfer.ProgramSchedule.GetOutput>> GetProgramScheduleByProgramId(int programId)
        {
            DataTransfer<List<NMS.Core.DataTransfer.ProgramSchedule.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.ProgramSchedule.GetOutput>>();
            try
            {
                List<ProgramSchedule> programSchedules = programScheduleService.GetProgramScheduleByProgramId(programId);
                transfer.Data = new List<Core.DataTransfer.ProgramSchedule.GetOutput>();
                //foreach (ProgramSchedule ps in programSchedules)
                //{
                transfer.Data.CopyFrom(programSchedules);
                //}
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("DeleteProgramSchedule")]
        public DataTransfer<bool> DeleteProgramSchedule(int programScheduleId)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = programScheduleService.DeleteProgramSchedule(programScheduleId);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("AddProgram")]
        public DataTransfer<NMS.Core.DataTransfer.Program.PostOutput> AddProgram(NMS.Core.DataTransfer.Program.PostInput input)
        {
            DataTransfer<NMS.Core.DataTransfer.Program.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.Program.PostOutput>();
            List<NMS.Core.DataTransfer.SegmentType.GetOutput> SegmentTypes = segmentTypeService.GetAll().Data;

            try
            {
                MMS.Integration.FPC.FPCAPI api = new MMS.Integration.FPC.FPCAPI();
                MMS.Integration.FPC.DataTransfer.Program myProgram = new MMS.Integration.FPC.DataTransfer.Program();
                myProgram.ChannelId = Convert.ToInt32(input.ChannelId);
                myProgram.Name = input.Name;
                MMS.Integration.DataTransfer<MMS.Integration.FPC.DataTransfer.Program> returnedProgram = api.InsertProgram(myProgram);

                if (returnedProgram.IsSuccess)
                {
                    input.ProgramId = returnedProgram.Data.ProgramChannelMappingId.ToString();
                    input.CreationDate = DateTime.UtcNow.ToString();
                    input.LastUpdateDate = input.CreationDate;
                    input.IsActive = "true";
                    input.IsApproved = "false";

                    //Program prog = new Program();
                    //prog.CopyFrom(input);
                    //transfer.CopyFrom(programService.InsertProgram(prog));
                    transfer = programService.Insert(input);

                    if (input.startUpURL != null)
                    {
                        ProgramConfiguration programConfig1 = new ProgramConfiguration();
                        programConfig1.ProgramId = Convert.ToInt32(transfer.Data.ProgramId);
                        programConfig1.SegmentTypeId = SegmentTypes.Where(x => x.Segment == "StartUp").First().SegmentTypeId;
                        programConfig1.Url = input.startUpURL;
                        programConfigurationService.InsertProgramConfiguration(programConfig1);
                    }
                    if (input.creditsURL != null)
                    {
                        ProgramConfiguration programConfig2 = new ProgramConfiguration();
                        programConfig2.ProgramId = Convert.ToInt32(transfer.Data.ProgramId);
                        programConfig2.SegmentTypeId = SegmentTypes.Where(x => x.Segment == "Credits").First().SegmentTypeId;
                        programConfig2.Url = input.creditsURL;
                        programConfigurationService.InsertProgramConfiguration(programConfig2);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        private void PopulateNotifications(DataTransfer<NLELoadInitialDataOutput> transfer, int userId, DateTime lastUpdateDate)
        {
            List<Notification> notifications = notificationService.GetNotifications(userId, lastUpdateDate);
            if (notifications != null && notifications.Count > 0)
            {
                transfer.Data.Notifications = new List<Core.DataTransfer.Notification.GetOutput>();
                transfer.Data.Notifications.CopyFrom(notifications);
            }
        }

        [HttpGet]
        [ActionName("DeleteProgram")]
        public DataTransfer<bool> DeleteProgram(int programId)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = programService.DeleteProgram(programId);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertVideoCutterScript")]
        public DataTransfer<NMS.Core.DataTransfer.Script.PostOutput> InsertVideoCutterScript(NMS.Core.DataTransfer.Script.PostInput input)
        {
            try
            {
                return scriptService.Insert(input);
            }
            catch (Exception exp)
            {
                DataTransfer<NMS.Core.DataTransfer.Script.PostOutput> transfer = new DataTransfer<Core.DataTransfer.Script.PostOutput>();
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
                return transfer;
            }
        }

        [HttpPost]
        [ActionName("MarkAsExecuted")]
        public DataTransfer<bool> MarkAsExecuted(int id)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = scriptService.MarkAsExecuted(id);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetAllSegmentTypes")]
        public DataTransfer<List<NMS.Core.DataTransfer.SegmentType.GetOutput>> GetAllSegmentTypes()
        {
            try
            {
                return segmentTypeService.GetAll();
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                DataTransfer<List<NMS.Core.DataTransfer.SegmentType.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.SegmentType.GetOutput>>();
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
                return transfer;
            }
        }


        [HttpPost]
        [ActionName("InsertProgramSegment")]
        public DataTransfer<NMS.Core.DataTransfer.ProgramSegment.PostOutput> InsertProgramSegment(NMS.Core.DataTransfer.ProgramSegment.PostInput input)
        {
            DataTransfer<NMS.Core.DataTransfer.ProgramSegment.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.ProgramSegment.PostOutput>();
            try
            {
                input.CreationDate = DateTime.UtcNow.ToString();
                input.LastUpdatedDate = input.CreationDate;
                input.IsActive = "true";

                transfer = programSegmentService.Insert(input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertProgramSegments")]
        public DataTransfer<NMS.Core.DataTransfer.ProgramSegment.PostOutput> InsertProgramSegments(List<NMS.Core.DataTransfer.ProgramSegment.PostInput> input)
        {
            DataTransfer<NMS.Core.DataTransfer.ProgramSegment.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.ProgramSegment.PostOutput>();
            try
            {
                programSegmentService.DeleteProgramSegmentByProgramId(Convert.ToInt32(input[0].ProgramId));
                int i = 0;
                foreach (NMS.Core.DataTransfer.ProgramSegment.PostInput subInput in input)
                {
                    if (subInput.Name == "")
                    {
                        if (subInput.SegmentTypeId == "3") subInput.Name = "StartUp";
                        else if (subInput.SegmentTypeId == "4") subInput.Name = "Credits";
                        else subInput.Name = "Segment";
                    }
                    i++;
                    subInput.SequenceNumber = i.ToString();
                    subInput.ProgramSegmentId = null;
                    subInput.CreationDate = DateTime.UtcNow.ToString();
                    subInput.LastUpdatedDate = subInput.CreationDate;
                    subInput.IsActive = "true";
                    transfer = programSegmentService.Insert(subInput);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetProgramSegmentByProgramId")]
        public DataTransfer<List<NMS.Core.DataTransfer.ProgramSegment.GetOutput>> GetProgramSegmentByProgramId(int programId)
        {
            DataTransfer<List<NMS.Core.DataTransfer.ProgramSegment.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.ProgramSegment.GetOutput>>();
            try
            {
                List<ProgramSegment> programSegments = programSegmentService.GetProgramSegmentByProgramId(programId);
                foreach (ProgramSegment ps in programSegments)
                {
                    if (ps.Name == null) ps.Name = "";
                    if (ps.Duration == null) ps.Duration = 0;
                    if (ps.StoryCount == null) ps.StoryCount = 0;
                }
                transfer.Data = new List<Core.DataTransfer.ProgramSegment.GetOutput>();
                transfer.Data.CopyFrom(programSegments);

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("DeleteProgramSegment")]
        public DataTransfer<bool> DeleteProgramSegment(int programSegmentId)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = programSegmentService.DeleteProgramSegment(programSegmentId);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                //transfer.Data = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetSuggestedFlow")]
        public DataTransfer<List<List<string>>> GetSuggestedFlow(int programId, string key)
        {
            DataTransfer<List<List<string>>> transfer = new DataTransfer<List<List<string>>>();

            try
            {
                List<SuggestedFlow> suggestedFlows = suggestedFlowService.GetSuggestedFlowsLikeKey(key, programId);
                if (suggestedFlows != null && suggestedFlows.Count > 0)
                {
                    transfer.Data = new List<List<string>>();

                    foreach (SuggestedFlow suggestedFlow in suggestedFlows)
                    {
                        List<string> tempKey = suggestedFlow.Key.Split('_').ToList<string>();
                        transfer.Data.Add(tempKey);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateCelebrityStatistics")]
        public DataTransfer<bool> UpdateCelebrityStatistics(CelebrityStatisticInput input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            transfer.IsSuccess = false;

            if (input != null)
            {
                try
                {
                    slotTemplateScreenElementService.UpdateSlotScreenTemplateElementCelebrity(input.SlotTemplateScreenElementId, input.CelebrityId);
                    celebrityStatisticService.RefreshCelebrityStatistics(input.NewsStatisticsParam);

                    transfer.IsSuccess = true;
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }


        [HttpGet]
        [ActionName("GetAllProgramType")]
        public DataTransfer<List<NMS.Core.DataTransfer.ProgramType.GetOutput>> GetAllProgramType()
        {
            try
            {
                return programTypeService.GetAll();
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                DataTransfer<List<NMS.Core.DataTransfer.ProgramType.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.ProgramType.GetOutput>>();
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
                return transfer;
            }
        }

        [HttpPost]
        [ActionName("GetMoreHeadlines")]
        public DataTransfer<HeadlinesOutput> GetMoreHeadlines(HeadlinesInput input)
        {
            DataTransfer<HeadlinesOutput> transfer = new DataTransfer<HeadlinesOutput>();

            transfer.Data = new HeadlinesOutput();
            if (input != null)
            {
                try
                {
                    List<Episode> episodes = episodeService.GetEpisodesByProgramType(ProgramTypes.NewsBulletIn, input.SearchTerm, input.FromDate, input.ToDate, input.PageCount, input.PageIndex, input.UserId);

                    if (episodes != null && episodes.Count > 0)
                    {
                        for (int i = 0; i < episodes.Count; i++)
                        {
                            programService.FillEpisode(episodes[i]);
                        }

                        transfer.Data.Episodes = new List<Core.DataTransfer.Episode.GetOutput>();
                        transfer.Data.Episodes.CopyFrom(episodes);
                    }
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetUnicodeText")]
        public DataTransfer<string> GetUnicodeText(Arg arg)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();

            if (!string.IsNullOrEmpty(arg.Text))
            {
                try
                {
                    InPageToUnicodeConverter.InPageToUnicodeConverter converter = new InPageToUnicodeConverter.InPageToUnicodeConverter();
                    string convertedText = converter.Inpage2Unicode(arg.Text);
                    transfer.Data = convertedText.Trim();
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetProgramAnchor")]
        public DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>> GetProgramAnchor(int programId)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Celebrity.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Celebrity.GetOutput>>();
            try
            {
                List<Celebrity> celebrities = programService.GetProgramAnchor(programId);
                transfer.Data = new List<Core.DataTransfer.Celebrity.GetOutput>();

                if (celebrities != null && celebrities.Count > 0)
                {
                    foreach (Celebrity celebrity in celebrities)
                    {
                        List<CelebrityStatistic> celebrityStatistics = celebrityStatisticService.GetCelebrityStatisticByCelebrityId(celebrity.CelebrityId);
                        celebrity.CelebrityStatistics = celebrityStatistics;
                    }
                    transfer.Data.CopyFrom(celebrities);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        public class Arg
        {
            public string Text { get; set; }
        }

        private void UpdateNewsStatistics(object param)
        {
            NewsStatisticsThreadInfo threadInfo = param as NewsStatisticsThreadInfo;
            if (threadInfo != null)
            {
                newsService.UpdateNewsStatistics(threadInfo.NewsStatisticsParam, threadInfo.NewsGuid, threadInfo.StatisticType);
            }
        }

        private void DeleteNewsStatistics(object param)
        {
            NewsStatisticsThreadInfo threadInfo = param as NewsStatisticsThreadInfo;
            if (threadInfo != null)
            {
                newsService.DeleteNewsStatistics(threadInfo.NewsGuid, threadInfo.EpisodeId);
            }
        }

    }

    [Serializable]
    public class NewsStatisticsThreadInfo
    {
        public NewsStatisticsInput NewsStatisticsParam { get; set; }
        public string NewsGuid { get; set; }
        public int EpisodeId { get; set; }
        public NewsStatisticType StatisticType { get; set; }
    }


    public class EpisodeDetail
    {
        public List<int> ProgramIds { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class EpisodeStatus
    {
        [DataMember(EmitDefaultValue = false)]
        public string EpisodeTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalStories { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Headlines { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Guests { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Feedback { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProgramId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProgramName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalSegment { get; set; }
    }


}