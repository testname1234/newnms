﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.IController;
using NMS.Core.Helper;
using NMS.Core.Models;
using System.Web;
using NMS.Core.Helper.Notification;
using System.Configuration;

namespace NMS.Web.API
{
    public class ProgramRecordingController : ApiController, IProgramRecordingController
    {
        ICelebrityVoiceArtistService celebrityvoiceartistService = IoC.Resolve<ICelebrityVoiceArtistService>("CelebrityVoiceArtistService");
        IVoiceArtistService voiceartistService = IoC.Resolve<IVoiceArtistService>("VoiceArtistService");
        IProgramRecordingService programRecordingService = IoC.Resolve<IProgramRecordingService>("ProgramRecordingService");
        IRecordingClipsService recordingClipService = IoC.Resolve<IRecordingClipsService>("RecordingClipsService");
        ICelebrityService celebrityService = IoC.Resolve<ICelebrityService>("CelebrityService");

        string host = ConfigurationManager.AppSettings["rabbitmqhost"];



        [HttpPost]
        [ActionName("LoadInitialData")]
        public DataTransfer<LoadInitialDataProgramRecording> LoadInitialData(LoadInitialDataInput input)
        {

            DataTransfer<LoadInitialDataProgramRecording> transfer = new DataTransfer<LoadInitialDataProgramRecording>();
            try
            {

                if (input.ProgramIdList != null && input.ProgramIdList.Length > 0)
                {
                    transfer.Data = new LoadInitialDataProgramRecording();
                    transfer.Data.ProgramRecordings = new List<ProgramRecording>();
                    transfer.Data.RecordingClips = new List<RecordingClips>();
                    transfer.Data.Celebrity = new List<Celebrity>();
                    for (int i = 0; i < input.ProgramIdList.Length; i++)
                    {
                        var data = programRecordingService.GetProgramRecordingByProgramId(input.ProgramIdList[i]);
                        if (data != null)
                        {
                            transfer.Data.ProgramRecordings.AddRange(data);
                            List<RecordingClips> recording_clips = new List<RecordingClips>();
                            for (int k = 0; k < data.Count(); k++)
                            {
                                var rec_clip = recordingClipService.GetRecordingClipsByProgramRecordingId(data[k].ProgramRecordingId);
                                if(rec_clip != null)
                                  recording_clips.AddRange(rec_clip);

                            }
                            transfer.Data.RecordingClips.AddRange(recording_clips);
                        }
                    }
                    var celebrity = celebrityService.GetAllCelebrity();
                    if(celebrity !=null)
                      transfer.Data.Celebrity.AddRange(celebrity);

                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetAllProgramRecordedSegments")]
        public DataTransfer<List<ProgramRecording>> GetAllProgramRecordedSegments(LoadInitialDataInput input)
        {
            
            DataTransfer<List<ProgramRecording>> transfer = new DataTransfer<List<ProgramRecording>>();
            try
            {
                 
                if (input.ProgramIdList != null && input.ProgramIdList.Length > 0)
                {
                    transfer.Data = new List<ProgramRecording>();
                    for (int i = 0; i < input.ProgramIdList.Length; i++)
                    {
                        var data = programRecordingService.GetProgramRecordingByProgramId(input.ProgramIdList[i]);
                        if(data != null)
                        {
                            transfer.Data.AddRange(data);
                        }
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetAllRecordingClips")]
        public DataTransfer<List<RecordingClips>> GetAllRecordingClips(LoadInitialDataInput input)
        {

            DataTransfer<List<RecordingClips>> transfer = new DataTransfer<List<RecordingClips>>();
            try
            {

                if (input.ProgramRecordingId != null && Convert.ToInt32(input.ProgramRecordingId) > 0)
                {
                    transfer.Data = new List<RecordingClips>();
                    transfer.Data = recordingClipService.GetRecordingClipsByProgramRecordingId(Convert.ToInt32(input.ProgramRecordingId));
                }
                else if (input.ProgramRecordingIdList != null && input.ProgramRecordingIdList.Count > 0)
                {
                    transfer.Data = new List<RecordingClips>();
                    var ids = input.ProgramRecordingIdList.Select(s => int.Parse(s)).ToList();
                    transfer.Data = recordingClipService.GetRecordingClipsByProgramRecordingIdList(ids);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("GetGuestByProgramIdAndEpisodeDate")]
        public DataTransfer<List<Celebrity>> GetGuestByProgramIdAndEpisodeDate(LoadInitialDataInput input)
        {

            DataTransfer<List<Celebrity>> transfer = new DataTransfer<List<Celebrity>>();
            try
            {

                if (input.ProgramId != null && Convert.ToInt32(input.ProgramId) > 0)
                {
                    transfer.Data = new List<Celebrity>();
                    for (int i = 0; i < input.ProgramIdList.Length; i++)
                    {
                        transfer.Data.AddRange(celebrityService.GetAllCelebrity());
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("UpsertProgramRecodingClip")]
        public DataTransfer<RecordingClips> UpsertProgramRecodingClip(RecordingClips input)
        {

            DataTransfer<RecordingClips> transfer = new DataTransfer<RecordingClips>();
            try
            {
                transfer.Data = new RecordingClips();
                if (input != null && Convert.ToInt32(input.ProgramRecordingId) > 0 && input.RecordingClipId == 0)
                {
                    input.CreationDate = DateTime.UtcNow;
                    transfer.Data = recordingClipService.InsertRecordingClips(input);
                }
                else if (input != null && Convert.ToInt32(input.ProgramRecordingId) > 0 && input.RecordingClipId > 0)
                {
                    input.LastUpdateDate = DateTime.UtcNow;
                    if(input.Status.HasValue && input.Status.Value == (int)Core.Enums.ProgramRecordingStatus.ClipReject)
                    {
                        transfer.Data = recordingClipService.UpdateRecordingClipsRejectClip(input); // rejecting all clips with same recording clip
                    }
                    else
                    {
                        transfer.Data = recordingClipService.UpdateRecordingClips(input);
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

                RabbitMQNotification notif = new RabbitMQNotification(host);
                notif.PublishMessage(new RabbitWrapper<int> { Data = transfer.Data.RecordingClipId, EventName = "RecordingClipUpsert" }, "recording", "changed_1");
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("ProgramClipAudioDone")]
        public DataTransfer<RecordingClips> ProgramClipAudioDone(RecordingClips input)
        {

            DataTransfer<RecordingClips> transfer = new DataTransfer<RecordingClips>();
            try
            {
                transfer.Data = new RecordingClips();
                if (input != null && Convert.ToInt32(input.RecordingClipId) > 0)
                {
                    RecordingClips rC = new RecordingClips();
                    rC = recordingClipService.GetRecordingClips(input.RecordingClipId);
                    rC.LastUpdateDate = DateTime.UtcNow;
                    transfer.Data = recordingClipService.UpdateRecordingClips(rC);
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

                RabbitMQNotification notif = new RabbitMQNotification(host);
                notif.PublishMessage(new RabbitWrapper<int> { Data = transfer.Data.RecordingClipId, EventName = "RecordingClipUpsert" }, "recording", "changed_1");
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

    }
}
