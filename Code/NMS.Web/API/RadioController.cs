﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.RadioStation;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Core.IController;

namespace NMS.Web.API
{
    public class RadioController : ApiController, IRadioController
    {

        IRadioStationService radioStationService = IoC.Resolve<IRadioStationService>("RadioStationService");
        IRadioStreamService radioStreamService = IoC.Resolve<IRadioStreamService>("RadioStreamService");
        IProgramService programService = IoC.Resolve<IProgramService>("ProgramService");


        [HttpGet]
        [ActionName("LoadRadioInitialData")]
        public DataTransfer<LoadRadioOutput> LoadRadioInitialData(string fromDate, string toDate)
        {
            DataTransfer<LoadRadioOutput> transfer = new DataTransfer<LoadRadioOutput>();
            try
            {
                if (fromDate != null && toDate != null)
                {
                    transfer.Data = new LoadRadioOutput();

                    List<RadioStation> radioStations = radioStationService.GetAllRadioStation();
                    if (radioStations != null && radioStations.Count > 0)
                    {
                        transfer.Data.RadioStations = new List<Core.DataTransfer.RadioStation.GetOutput>();
                        transfer.Data.RadioStations.CopyFrom(radioStations);
                    }

                    DateTime dtFrom = new DateTime();
                    DateTime dtto = new DateTime();
                    if (DateTime.TryParse(fromDate, out dtFrom) && DateTime.TryParse(toDate, out dtto))
                    {
                        //dtFrom = dtFrom.ToUniversalTime();
                        //dtto = dtto.ToUniversalTime();
                        List<RadioStream> radioStreams = radioStreamService.GetRadioStreamBetweenDates(dtFrom.Date, dtto.Date.AddDays(1).AddMilliseconds(-1));
                        if (radioStreams != null && radioStreams.Count > 0)
                        {
                            transfer.Data.RadioStreams = new List<Core.DataTransfer.RadioStream.GetOutput>();
                            transfer.Data.RadioStreams.CopyFrom(radioStreams);
                        }
                    }

                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("RefreshData")]
        public DataTransfer<LoadRadioOutput> RefreshData(string fromDate, string toDate)
        {
            DataTransfer<LoadRadioOutput> transfer = new DataTransfer<LoadRadioOutput>();
            try
            {
                if (fromDate != null && toDate != null)
                {
                    transfer.Data = new LoadRadioOutput();
                    DateTime dtFrom = new DateTime();
                    DateTime dtto = new DateTime(); 



                    if (DateTime.TryParse(fromDate, out dtFrom) && DateTime.TryParse(toDate, out dtto))
                    {
                        //dtFrom = dtFrom.ToUniversalTime();
                        //dtto = dtto.ToUniversalTime();
                        
                        List<RadioStream> radioStreams = radioStreamService.GetRadioStreamBetweenDates(dtFrom.Date, dtto.Date.AddDays(1).AddMilliseconds(-1));
                        if (radioStreams != null && radioStreams.Count > 0)
                        {
                            transfer.Data.RadioStreams = new List<Core.DataTransfer.RadioStream.GetOutput>();
                            transfer.Data.RadioStreams.CopyFrom(radioStreams);
                        }
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

    }
}
