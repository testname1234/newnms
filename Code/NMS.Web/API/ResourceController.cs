﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;
using NMS.Core.DataTransfer;
using NMS.Core;
using NMS.Core.IService;
using NMS.Core.Entities;
using NMS.Core.Models;
using NMS.Core.Enums;
using NMS.Core.IController;
using NMS.Core.Helper;
using MS.Core.DataTransfer.Resource.GET;
using MS.Core.Models;

namespace NMS.Web.API
{
    public class ResourceController : ApiController, IResourceController
    {
        IResourceService resourceService = IoC.Resolve<IResourceService>("ResourceService");
        IChannelVideoService channelVideoService = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
        INewsPaperPageService newsPaperPageService = IoC.Resolve<INewsPaperPageService>("NewsPaperPageService");
        IRadioStreamService radioStreamService = IoC.Resolve<IRadioStreamService>("RadioStreamService");

        private readonly JavaScriptSerializer _js = new JavaScriptSerializer { MaxJsonLength = 41943040 };
        private readonly string _storageRoot = ConfigurationManager.AppSettings["TempContentLocation"];
        HttpWebRequestHelper requestHelper = new HttpWebRequestHelper();
        public bool _isReusable { get { return false; } }



        [HttpGet]
        [ActionName("GetResourceIds")]
        public DataTransfer<List<NMS.Core.DataTransfer.Resource.PostOutput>> GetResourceIds(string id, int? bucketId, string apiKey, string sourceTypeId, string sourceTypeName, string sourceId, string Source = "Reporter")
        {
            bucketId = null;
            DataTransfer<List<NMS.Core.DataTransfer.Resource.PostOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Resource.PostOutput>>();
            try
            {
                if (apiKey != null)
                {
                    apiKey = System.Net.WebUtility.HtmlDecode(apiKey);
                }
                transfer.Data = resourceService.GetResourceGuidsFromMediaServer(new List<NMS.Core.DataTransfer.Resource.PostInput>() { new NMS.Core.DataTransfer.Resource.PostInput() { FileName = id } }, _storageRoot, bucketId, apiKey, sourceTypeId, sourceTypeName, sourceId,Source);
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "error occurred" };
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("MediaPostResource")]
        public DataTransfer<NMS.Core.Entities.Resource> MediaPostResource(MS.Core.DataTransfer.Resource.PostInput resource)
        {
            DataTransfer<NMS.Core.Entities.Resource> transfer = new DataTransfer<NMS.Core.Entities.Resource>();
            try
            {
                resource.IsFromExternalSource = true;
                string FolderSource = "Google Media Bucket";

                transfer.Data = resourceService.MediaPostResourceForGoogleData(resource, FolderSource);
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "error occurred" };
                ExceptionLogger.Log(exp);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("GetAllResourceInfoByGuid")]
        public DataTransfer<DownloadPendingCount> GetAllResourceInfoByGuid(ResourceOutputGuids lisGuids)
        {
            DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>> result = new DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>>();

            DataTransfer<DownloadPendingCount> transfer = new DataTransfer<DownloadPendingCount>();
            try
            {
                int downloadCount = 0;
                int pendingCount = 0;
                result = requestHelper.PostRequest<DataTransfer<List<MS.Core.DataTransfer.Resource.GetOutput>>>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/GetResourceInfoByGuidIds", lisGuids, null);
                transfer.IsSuccess = true;
                for (int i = 0; i < result.Data.Count; i++)
                {
                    if (result.Data[i].ResourceStatusId != Convert.ToString(4) || result.Data[i].ResourceStatusId == null)
                    {
                        transfer.IsSuccess = false;
                        pendingCount = pendingCount + 1;
                        transfer.Data = null;
                    }
                    else
                    {
                        //transfer.IsSuccess = true;
                        transfer.Data = null;
                    }
                    downloadCount = downloadCount + 1;
                }
                transfer.Data = new DownloadPendingCount();
                transfer.Data.DownloadedCount = downloadCount;
                transfer.Data.pendingCount = pendingCount;

                return transfer;

            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                return transfer;
            }
        }

        #region Delete
        [ActionName("DeleteResource")]
        public HttpResponseMessage DeleteResource(string Id)
        {
            MResource resource = resourceService.GetMResource(Id);
            resourceService.DeleteFromMediaServer(new Guid(resource.Guid));
            return ControllerContext.Request.CreateResponse(HttpStatusCode.OK, "");
        }

        [ActionName("Delete")]
        public HttpResponseMessage Delete(string Id)
        {
            Guid guid = new Guid();
            if (Guid.TryParse(Id, out guid))
            {
                resourceService.DeleteFromMediaServer(guid);
            }

            //Delete from MS
            return ControllerContext.Request.CreateResponse(HttpStatusCode.OK, "");
        }

        #endregion

        [HttpGet]
        [ActionName("MarkIsProcessed")]
        public HttpResponseMessage MarkIsProcessed(int id, int typeId)
        {
            if (typeId == (int)ResourceTypes.Video)
                channelVideoService.MarkIsProcessed(id);
            if (typeId == (int)ResourceTypes.Image)
                newsPaperPageService.MarkIsProcessed(id);
            if (typeId == (int)ResourceTypes.Audio)
                radioStreamService.MarkIsProcessed(id);
            return ControllerContext.Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [ActionName("CroppImage")]
        public DataTransfer<string> CroppImage(CroppedResource input)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                transfer.IsSuccess = true;
                transfer = requestHelper.PostRequest<DataTransfer<string>>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/CropImage", input, null);
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("SearchResource")]
        public MS.Core.DataTransfer.Resource.ResourceOutput SearchResource(string term, int pageSize, int pageNumber, int? resourceTypeId, int bucketId, string alldata)
        {
            MS.Core.DataTransfer.Resource.ResourceOutput transfer = new MS.Core.DataTransfer.Resource.ResourceOutput();
            try
            {

                if (resourceTypeId != 0)
                    transfer = requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/searchresource?term=" + term + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&resourceTypeId=" + resourceTypeId + "&bucketId=" + (bucketId == 0 ? -1 : bucketId) + "&alldata=" + alldata, null);
                else
                    transfer = requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/searchresource?term=" + term + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&bucketId=" + (bucketId == 0 ? -1 : bucketId) + "&alldata=" + alldata, null);

            }
            catch (Exception exp)
            {

            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetResourceByDate")]
        public MS.Core.DataTransfer.Resource.ResourceOutput GetResourceByDate(string ResourceDate)
        {
            MS.Core.DataTransfer.Resource.ResourceOutput transfer = new MS.Core.DataTransfer.Resource.ResourceOutput();
            try
            {

                //if (resourceTypeId != 0)
                //    transfer = requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/getresourcebydate?ResourceDate=" + ResourceDate + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&resourceTypeId=" + resourceTypeId + "&bucketId=" + (bucketId == 0 ? -1 : bucketId) + "&alldata=" + alldata, null);
                //else
                //    transfer = requestHelper.GetRequest<MS.Core.DataTransfer.Resource.ResourceOutput>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/getresourcebydate?ResourceDate=" + ResourceDate + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&bucketId=" + (bucketId == 0 ? -1 : bucketId) + "&alldata=" + alldata, null);
                transfer.Resources = CacheManager.Resources.Resources;
                if (transfer.Resources != null && transfer.Resources.Count > 0)
                {
                    transfer.Count = transfer.Resources.Count;
                    transfer.Resources = transfer.Resources.Where(x => x.CreationDate >= Convert.ToDateTime(ResourceDate).ToUniversalTime()).OrderByDescending(x => x.CreationDate).ToList();
                }

            }
            catch (Exception exp)
            {

            }
            return transfer;
        }

        [HttpGet]
        [ActionName("SearchMetas")]
        public MS.Core.DataTransfer.Resource.ResourceOutput SearchMetas(string term, string Metas, int pageSize, int pageNumber, int? resourceTypeId = null, int? bucketid = null, Nullable<bool> alldata = true)
        {
            DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput> transfer = new DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput>();
            try
            {
                transfer = requestHelper.GetRequest<DataTransfer<MS.Core.DataTransfer.Resource.ResourceOutput>>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/SearchMetas?term=" + term + "&Metas=" + Metas + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&resourceTypeId=" + resourceTypeId + "&bucketId=" + (bucketid == 0 ? null : bucketid) + "&alldata=" + alldata, null);
                return transfer.Data;
            }
            catch (Exception exp)
            {
                transfer.Errors = new string[] { exp.Message };
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            }

            return transfer.Data;
        }

        [HttpPost]
        [ActionName("SearchCelebrity")]
        public DataTransfer<List<string>> SearchCelebrity(string term, int pageSize, int pageNumber)
        {
            DataTransfer<List<string>> transfer = new DataTransfer<List<string>>();

            try
            {

                transfer = requestHelper.GetRequest<DataTransfer<List<string>>>(ConfigurationManager.AppSettings["MediaServerUrl"] + "/SearchCelebrity?term=" + term + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber, null);
            }
            catch (Exception exp)
            {

            }
            return transfer;
        }


        [HttpPost]
        [ActionName("UpdateResources")]
        public DataTransfer<string> UpdateResources(ListResources input)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                transfer.IsSuccess = true;
                resourceService.updateResourceMeta(input);
            }
            catch (Exception exp)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }


        [HttpGet]
        [ActionName("GetAllBuckets")]
        public List<FolderModel> GetAllBuckets()
        {
            DataTransfer<List<FolderModel>> transfer = new DataTransfer<List<FolderModel>>();
            try
            {
                transfer = requestHelper.GetRequest<DataTransfer<List<FolderModel>>>(ConfigurationManager.AppSettings["MediaServerUrlBucket"] + "/GetAllBucket", null);
                return transfer.Data;
            }
            catch (Exception exp)
            {
                transfer.Errors = new string[] { exp.Message };
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exp));
            }

            return transfer.Data;
        }

    }
}