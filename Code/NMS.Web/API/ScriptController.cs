﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.IController;
using NMS.Core.Helper;
using NMS.Core.Models;

namespace NMS.Web.API
{
    public class ScriptController : ApiController, IScriptController
    {
        IScriptService scriptService = IoC.Resolve<IScriptService>("ScriptService");

        [HttpPost]
        [ActionName("Insert")]
        public DataTransfer<NMS.Core.DataTransfer.Script.PostOutput> Insert(NMS.Core.DataTransfer.Script.PostInput input)
        {
            try
            {
                 return scriptService.Insert(input);
            }
            catch (Exception exp)
            {
                DataTransfer<NMS.Core.DataTransfer.Script.PostOutput> transfer = new DataTransfer<Core.DataTransfer.Script.PostOutput>();
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
                return transfer;
            }
        }

        [HttpPost]
        [ActionName("MarkAsExecuted")]
        public DataTransfer<bool> MarkAsExecuted(int id)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = scriptService.MarkAsExecuted(id);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }
            return transfer;
        }
    }
}
