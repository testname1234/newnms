﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.IService;
using NMS.Core.IController;

namespace NMS.Web.API
{
    public class SlugController : ApiController, ISlugController
    {
        ISlugService slugService = IoC.Resolve<ISlugService>("SlugService");

        [HttpGet]
        [ActionName("InsertSlug")]
        public DataTransfer<List<NMS.Core.DataTransfer.Slug.GetOutput>> InsertSlug(string id = null)
        {
            if (id == null)
                id = string.Empty;
            DataTransfer<List<NMS.Core.DataTransfer.Slug.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Slug.GetOutput>>();
            try
            {
                Slug slug = new Slug();
                slug.Slug = id;
                slug = slugService.InsertIfNotExists(slug);
                transfer.Data = new List<Core.DataTransfer.Slug.GetOutput>();
                if (slug != null)
                    transfer.Data.CopyFrom(slug);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }
    }
}
