﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using NMS.Core.Helper;
using NMS.Core.Models;
using NMS.Core.IService;
using NMS.Core.IController;


namespace NMS.Web.API
{
    public class TagController : ApiController , ITagController
    {
        ITagService tagService = IoC.Resolve<ITagService>("TagService");
        IOrganizationService orgservice = IoC.Resolve<IOrganizationService>("OrganizationService");

        [HttpGet]
        [ActionName("GetTagByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>> GetTagByTerm(string id = null)
        {
            if (id == null)
                id = string.Empty;
            DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Tag.GetOutput>>();
            try
            {
                List<Tag> tags = tagService.GetTagByTerm(id.ToLower());
                transfer.Data = new List<Core.DataTransfer.Tag.GetOutput>();
                if (tags != null)
                {
                    tags = tags.OrderBy(x => x.Tag.Length).ToList();
                    transfer.Data.CopyFrom(tags);
                }
                    
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("LoadResourceAgainstTag")]
        public DataTransfer<List<Resource>> LoadResourceAgainstTag(string term,int type)
        {
            DataTransfer<List<Resource>> transfer = new DataTransfer<List<Resource>>();
            try
            {
                string mediaServer = AppSettings.MediaServerUrl;
                HttpWebRequestHelper helper = new HttpWebRequestHelper();
                string url = mediaServer + "/SearchResource?term="+term+ "&fromDate=&toDate=&pageSize=20&IsHDSelected=false&pageNumber=1&resourceTypeId="+type;
                NMS.Core.DataTransfer.Resource.GET.MediaServerResources output = helper.GetRequest<NMS.Core.DataTransfer.Resource.GET.MediaServerResources>(url, null,true);

                if (output != null && output.Resources.Count > 0)
                {
                    transfer.Data = output.Resources;
                }
            }
            catch(Exception ex)
            {
                ExceptionLogger.Log(ex);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetOrganizationByTerm")]
        public DataTransfer<List<NMS.Core.DataTransfer.Organization.GetOutput>> GetOrganizationByTerm(string id = null)
        {   
            DataTransfer<List<NMS.Core.DataTransfer.Organization.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Organization.GetOutput>>();
            if (id == null)
                id = string.Empty;
            try
            {
                List<Organization> org = orgservice.GetOrganizationByTerm(id.ToLower());
                transfer.Data = new List<Core.DataTransfer.Organization.GetOutput>();
                if (org != null)
                {
                    org = org.OrderBy(x => x.Name.Length).ToList();
                    transfer.Data.CopyFrom(org);
                }
                    
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }
    }


 
}
