﻿using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace NMS.Web.API
{
    public class TestController : ApiController
    {
        public DataTransfer<Filter> Get()
        {
            DataTransfer<Filter> transfer = new DataTransfer<Filter>();
            Filter filter = new Filter();
            filter.CreationDate = DateTime.UtcNow;
            transfer.Data = filter;
            return transfer;
        }
    }
}
