﻿using Newtonsoft.Json.Linq;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.Enums;
using NMS.Core.IController;
using NMS.Core.IService;
using NMS.Core.Models;
using NMS.Integration;
using NMS.Integration.MCRTicker;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web.Http;

namespace NMS.Web.API
{
    public class TickerController : ApiController, ITickerController
    {
        private ITickerService tickerService = IoC.Resolve<ITickerService>("TickerService");
        private ITickerLineService tickerLineService = IoC.Resolve<ITickerLineService>("TickerLineService");
        private ITickerCategoryService tickerCategoryService = IoC.Resolve<ITickerCategoryService>("TickerCategoryService");
        private INewsService newsService = IoC.Resolve<INewsService>("NewsService");
        private IUserManagementService userManagementService = IoC.Resolve<IUserManagementService>("UserManagementService");
        private IOnAirTickerService onAirTickerService = IoC.Resolve<IOnAirTickerService>("OnAirTickerService");
        private IOnAirTickerLineService onAirTickerLineService = IoC.Resolve<IOnAirTickerLineService>("OnAirTickerLineService");
        private IMcrBreakingTickerRundownService mcrBreakingService = IoC.Resolve<IMcrBreakingTickerRundownService>("McrBreakingTickerRundownService");
        private IMcrLatestTickerRundownService mcrLatestService = IoC.Resolve<IMcrLatestTickerRundownService>("McrLatestTickerRundownService");
        private IMcrCategoryTickerRundownService mcrCategoryService = IoC.Resolve<IMcrCategoryTickerRundownService>("McrCategoryTickerRundownService");
        private INewsFileService newsFileService = IoC.Resolve<INewsFileService>("NewsFileService");
        private IMcrTickerHistoryService mcrtickerhistoryservice = IoC.Resolve<IMcrTickerHistoryService>("McrTickerHistoryService");
        private IMcrTickerBroadcastedService mcrtickerbroadcastedservice = IoC.Resolve<IMcrTickerBroadcastedService>("McrTickerBroadcastedService");



        [HttpPost]
        [ActionName("CreateSegment")]
        public DataTransfer<Core.DataTransfer.OnAirTicker.GetOutput> CreateSegment(Core.DataTransfer.OnAirTicker.PostInput input)
        {
            DataTransfer<Core.DataTransfer.OnAirTicker.GetOutput> transfer = new DataTransfer<Core.DataTransfer.OnAirTicker.GetOutput>();
            transfer.Data = new Core.DataTransfer.OnAirTicker.GetOutput();

            try
            {
                if (input != null && !string.IsNullOrEmpty(input.TickerGroupName))
                {
                    OnAirTicker newOnAirTicker = new OnAirTicker();
                    newOnAirTicker.CopyFrom(input);

                    if (!string.IsNullOrEmpty(input.TickerId))
                    {
                        var existing = onAirTickerService.GetOnAirTickerByKeyValue("TickerId", input.TickerId, Operands.Equal);
                        if (existing != null && existing.Count == 1)
                        {
                            existing[0].TickerGroupName = input.TickerGroupName;
                            existing[0].LastUpdatedDate = DateTime.UtcNow;
                            newOnAirTicker = onAirTickerService.UpdateOnAirTicker(existing[0]);
                        }
                    }
                    else
                    {
                        newOnAirTicker = onAirTickerService.InsertOnAirTicker(newOnAirTicker);
                    }

                    if (newOnAirTicker != null && newOnAirTicker.TickerId > 0)
                    {
                        transfer.Data.CopyFrom(newOnAirTicker);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;

            }
            return transfer;
        }

        [HttpPost]
        [ActionName("InsertOnAirTickerLine")]
        public DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput> InsertOnAirTickerLine(NMS.Core.Models.OnAirTickerLineInsertInput input)
        {
            DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput> transfer = new DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput>();
            transfer.Data = new Core.DataTransfer.OnAirTickerLine.GetOutput();

            try
            {
                if (input.TickerLines != null)
                {

                    List<NMS.Core.DataTransfer.OnAirTickerLine.PostInput> temOnAirLines = new List<Core.DataTransfer.OnAirTickerLine.PostInput>();
                    temOnAirLines.CopyFrom(input.TickerLines);
                    OnAirTickerLine newOnAirTickerLine = new OnAirTickerLine();
                    if (temOnAirLines != null && temOnAirLines.Count > 0)
                    {
                        newOnAirTickerLine.CopyFrom(temOnAirLines[0]);
                        newOnAirTickerLine = onAirTickerLineService.InsertOnAirTickerLine(newOnAirTickerLine, input.TickerTypeId);
                    }

                    if (newOnAirTickerLine != null && newOnAirTickerLine.TickerLineId > 0)
                    {
                        List<OnAirTicker> list = new List<OnAirTicker>();
                        list = onAirTickerService.GetOnAirTickerByKeyValue("TickerTypeId", input.TickerTypeId.ToString(), Operands.Equal);
                        if (list != null && list.Count > 0)
                            list = list.OrderBy(i => i.SequenceId).ToList();

                        if (input.TickerTypeId == (int)TickerTypes.Breaking && list != null && list.Count > 0)
                        {
                            tickerLineService.FlushMcrTickerData(false, true, false);
                            foreach (var item in list)
                            {
                                OnAirTickerLine temp = new OnAirTickerLine();
                                temp.TickerId = item.TickerId;
                                onAirTickerLineService.SubmitOnAirTickerLineToMcrBreaking(temp);
                            }

                        }
                        else if (input.TickerTypeId == (int)TickerTypes.Latest && list != null && list.Count > 0)
                        {
                            tickerLineService.FlushMcrTickerData(false, false, true);
                            foreach (var item in list)
                            {
                                OnAirTickerLine temp = new OnAirTickerLine();
                                temp.TickerId = item.TickerId;
                                onAirTickerLineService.SubmitOnAirTickerLineToMcrLatest(temp);
                            }
                        }
                        else if (input.TickerTypeId == (int)TickerTypes.Category && list != null && list.Count > 0)
                        {
                            tickerLineService.FlushMcrTickerData(true, false, false);
                            foreach (var item in list)
                            {
                                OnAirTickerLine temp = new OnAirTickerLine();
                                temp.TickerId = item.TickerId;
                                onAirTickerLineService.SubmitOnAirTickerLineToMcrCategory(temp);

                            }
                        }
                    }
                    transfer.Data.CopyFrom(newOnAirTickerLine);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { ex.Message };
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("MarkOnAirTickerStatus")]
        public DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput> MarkOnAirTickerStatus(NMS.Core.Models.OnAirTickerLineInsertInput input)
        {

            DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput> transfer = new DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput>();
            try
            {
                if (input.TickerLines != null && input.TickerLines.Count > 0)
                {
                    int id = Convert.ToInt32(input.TickerLines[0].TickerLineId);
                    OnAirTickerLine tempObj = onAirTickerLineService.GetOnAirTickerLine(id);
                    if (tempObj != null)
                    {
                        tempObj.IsShow = false;
                        tempObj.LastUpdatedDate = DateTime.UtcNow;
                        tempObj = onAirTickerLineService.UpdateOnAirTickerLine(tempObj);
                        bool isDeleteFromMcr = false;
                        if (input.TickerTypeId == (int)TickerTypes.Breaking)
                        {
                            isDeleteFromMcr = onAirTickerLineService.DeleteOnAirTickerLineFromMCRBreaking(tempObj);
                        }
                        else if (input.TickerTypeId == (int)TickerTypes.Latest)
                        {
                            isDeleteFromMcr = onAirTickerLineService.DeleteOnAirTickerLineFromMCRLatest(tempObj);
                        }
                        else if (input.TickerTypeId == (int)TickerTypes.Category)
                        {
                            isDeleteFromMcr = onAirTickerLineService.DeleteOnAirTickerLineFromMCRCategory(tempObj);
                        }
                        if (isDeleteFromMcr)
                        {
                            transfer.Data = new Core.DataTransfer.OnAirTickerLine.GetOutput();
                            transfer.Data.CopyFrom(tempObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("DeleteGroup")]
        public DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput> DeleteGroup(NMS.Core.DataTransfer.OnAirTicker.PostInput input)
        {

            DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput> transfer = new DataTransfer<Core.DataTransfer.OnAirTickerLine.GetOutput>();
            try
            {
                if (input.TickerTypeId != null && input.TickerId != null)
                {
                    OnAirTickerLine temp = new OnAirTickerLine();
                    temp.TickerId = Convert.ToInt32(input.TickerId);
                    bool isDeleted = onAirTickerLineService.MigrateGroupToOnAiredTable(temp, Convert.ToInt32(input.TickerTypeId));
                    if (!isDeleted)
                        transfer.IsSuccess = false;

                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetMCRBreakingTickers")]
        public DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>> GetMCRBreakingTickers(NMS.Core.DataTransfer.McrBreakingTickerRundown.PostInput input)
        {
            DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>>();
            try
            {
                var list = mcrBreakingService.GetAllMcrBreakingTickerRundown();
                if (list != null)
                {
                    transfer.Data = new List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>();
                    transfer.Data.CopyFrom(list);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("ChangeTemplate")]
        public DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>> ChangeTemplate(string type)
        {
            DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>>();
            try
            {

                if (type != "-1")
                {
                    string ip_address = ConfigurationManager.AppSettings["TCPSeverIP"];
                    int port = Convert.ToInt32(ConfigurationManager.AppSettings["TCPServerPort"]);

                    JObject obj = new JObject();
                    obj["TemplateType"] = type;

                    string json = Convert.ToString(type);

                    TcpClient client = new TcpClient(ip_address, port);
                    Byte[] message = System.Text.Encoding.ASCII.GetBytes(json);
                    NetworkStream stream = client.GetStream();
                    stream.Write(message, 0, message.Length);
                    stream.Close();
                    client.Close();

                    transfer.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { ex.ToString() };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("PostToTwitter")]
        public DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>> PostToTwitter(NMS.Core.DataTransfer.McrBreakingTickerRundown.PostInput input)
        {
            DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.McrBreakingTickerRundown.GetOutput>>();
            try
            {
                string list = input.Text;
                if (list != null && list.Length > 0)
                {
                    ITwitterAPI tweet = IoC.Resolve<ITwitterAPI>("TwitterAPI");
                    transfer.IsSuccess = tweet.PostTweet(list, input.TickerType);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { ex.ToString() };
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetMCRLatestTickers")]
        public DataTransfer<List<Core.DataTransfer.McrLatestTickerRundown.GetOutput>> GetMCRBreakingTickers(NMS.Core.DataTransfer.McrLatestTickerRundown.PostInput input)
        {

            DataTransfer<List<Core.DataTransfer.McrLatestTickerRundown.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.McrLatestTickerRundown.GetOutput>>();
            try
            {
                var list = mcrLatestService.GetAllMcrLatestTickerRundown();
                if (list != null)
                {
                    transfer.Data = new List<Core.DataTransfer.McrLatestTickerRundown.GetOutput>();
                    transfer.Data.CopyFrom(list);
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetMCRCategoryTickers")]
        public DataTransfer<List<Core.DataTransfer.McrCategoryTickerRundown.GetOutput>> GetMCRCategoryTickers(NMS.Core.DataTransfer.McrCategoryTickerRundown.PostInput input)
        {

            DataTransfer<List<Core.DataTransfer.McrCategoryTickerRundown.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.McrCategoryTickerRundown.GetOutput>>();
            try
            {
                var list = mcrCategoryService.GetAllMcrCategoryTickerRundown();
                if (list != null)
                {
                    transfer.Data = new List<Core.DataTransfer.McrCategoryTickerRundown.GetOutput>();
                    transfer.Data.CopyFrom(list);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateTickerSequence")]
        public DataTransfer<List<Core.DataTransfer.OnAirTicker.GetOutput>> UpdateTickerSequence(NMS.Core.Models.OnAirTickerInsertInput input)
        {
            DataTransfer<List<Core.DataTransfer.OnAirTicker.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.OnAirTicker.GetOutput>>();
            try
            {
                List<OnAirTicker> returnList = new List<OnAirTicker>();
                if (input != null && input.Tickers.Count > 0)
                {
                    List<OnAirTicker> list = new List<OnAirTicker>();
                    list.CopyFrom(input.Tickers);
                    foreach (var item in list)
                    {
                        OnAirTicker tempObj = onAirTickerService.UpdateOnAirTickerSequenceNumber(item);
                        if (tempObj != null)
                            returnList.Add(tempObj);
                    }

                    if (returnList.Count > 0)
                    {
                        transfer.Data = new List<Core.DataTransfer.OnAirTicker.GetOutput>();
                        transfer.Data.CopyFrom(returnList);

                    }
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
            }
            return transfer;
        }
        [HttpPost]
        [ActionName("UpdateTickerLineSequence")]
        public DataTransfer<List<Core.DataTransfer.OnAirTickerLine.GetOutput>> UpdateTickerLineSequence(NMS.Core.Models.OnAirTickerLineInsertInput input)
        {
            DataTransfer<List<Core.DataTransfer.OnAirTickerLine.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.OnAirTickerLine.GetOutput>>();
            try
            {
                List<OnAirTickerLine> returnList = new List<OnAirTickerLine>();
                if (input.TickerLines != null && input.TickerLines.Count > 0)
                {
                    List<OnAirTickerLine> list = new List<OnAirTickerLine>();
                    list.CopyFrom(input.TickerLines);
                    foreach (var item in list)
                    {
                        OnAirTickerLine tempObj = onAirTickerLineService.UpdateOnAirTickerLineSequenceNumber(item);
                        if (tempObj != null)
                            returnList.Add(tempObj);
                    }

                    if (returnList.Count > 0)
                    {
                        transfer.Data = new List<Core.DataTransfer.OnAirTickerLine.GetOutput>();
                        transfer.Data.CopyFrom(returnList);

                    }
                }

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
            }
            return transfer;
        }
        [HttpPost]
        [ActionName("UpdateOnAirTickerToMCR")]
        public DataTransfer<TickerLineSubmitOutput> UpdateOnAirTickerToMCR(TickerLineSubmitInput input)
        {
            DataTransfer<TickerLineSubmitOutput> transfer = new DataTransfer<TickerLineSubmitOutput>();
            transfer.Data = new TickerLineSubmitOutput();

            try
            {
                List<OnAirTicker> onAirTickerList = onAirTickerService.GetOnAirTickerByKeyValue("TickerTypeId", input.TickerTypeId.ToString(), Operands.Equal);
                onAirTickerList = onAirTickerService.FillOnAirTicker(onAirTickerList);
                if (onAirTickerList != null && onAirTickerList.Count > 0)
                {

                    if (input.TickerTypeId == (int)TickerTypes.Category && onAirTickerList != null && onAirTickerList.Count > 0)
                    {
                        onAirTickerList = onAirTickerList.OrderBy(i => i.SequenceId).ToList();
                        tickerLineService.FlushMcrTickerData(true, false, false);
                        foreach (var item in onAirTickerList)
                        {
                            if (item.TickerLines != null && item.TickerLines.Count > 0)
                            {
                                OnAirTickerLine temp = new OnAirTickerLine();
                                temp.TickerId = item.TickerId;
                                onAirTickerLineService.SubmitOnAirTickerLineToMcrCategory(temp);
                            }

                        }
                    }

                    else if (input.TickerTypeId == (int)TickerTypes.Breaking && onAirTickerList != null && onAirTickerList.Count > 0)
                    {
                        onAirTickerList = onAirTickerList.OrderBy(i => i.SequenceId).ToList();
                        tickerLineService.FlushMcrTickerData(false, true, false);
                        foreach (var item in onAirTickerList)
                        {
                            if (item.TickerLines != null && item.TickerLines.Count > 0)
                            {
                                OnAirTickerLine temp = new OnAirTickerLine();
                                temp.TickerId = item.TickerId;
                                onAirTickerLineService.SubmitOnAirTickerLineToMcrBreaking(temp);
                            }
                        }
                    }

                    else if (input.TickerTypeId == (int)TickerTypes.Latest && onAirTickerList != null && onAirTickerList.Count > 0)
                    {
                        onAirTickerList = onAirTickerList.OrderBy(i => i.SequenceId).ToList();
                        tickerLineService.FlushMcrTickerData(false, false, true);
                        foreach (var item in onAirTickerList)
                        {
                            if (item.TickerLines != null && item.TickerLines.Count > 0)
                            {
                                OnAirTickerLine temp = new OnAirTickerLine();
                                temp.TickerId = item.TickerId;
                                onAirTickerLineService.SubmitOnAirTickerLineToMcrLatest(temp);
                            }
                        }
                    }



                }

            }
            catch (Exception)
            {
                transfer.IsSuccess = false;

            }
            return transfer;
        }

        /*Deprecated*/
        [HttpPost]
        [ActionName("GetMoreTickers")]
        public DataTransfer<TickerOutput> GetMoreTickers(TickerInput input)
        {
            DataTransfer<TickerOutput> transfer = new DataTransfer<TickerOutput>();

            if (input != null)
            {
                try
                {
                    transfer.Data = new TickerOutput();
                    List<TickerStatus> discardStatus = new List<TickerStatus>();
                    List<Ticker> tickers = tickerService.GetTickers(input.SearchTerm, input.FromDate, input.ToDate, input.PageCount, input.PageIndex, input.UserId);

                    if (tickers != null)
                    {
                        for (int i = 0; i < tickers.Count; i++)
                        {
                            List<TickerLine> tickerLines = tickerLineService.GetTickerLineByTickerId(tickers[i].TickerId);
                            if (tickerLines != null && tickerLines.Count > 0)
                            {
                                tickers[i].TickerLines = tickerLines;
                            }
                        }
                        transfer.Data.Tickers = new List<Core.DataTransfer.Ticker.GetOutput>();
                        transfer.Data.Tickers.CopyFrom(tickers);
                    }
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetCateogryTickers")]
        public DataTransfer<Core.DataTransfer.TickerCategory.GetOutput> GetCateogryTickers(TickerCategoryInput input)
        {
            DataTransfer<Core.DataTransfer.TickerCategory.GetOutput> transfer = new DataTransfer<Core.DataTransfer.TickerCategory.GetOutput>();

            if (input != null)
            {
                try
                {
                    transfer.Data = new Core.DataTransfer.TickerCategory.GetOutput();
                    List<TickerStatus> discardStatus = new List<TickerStatus>();
                    var tickerCategories = tickerCategoryService.GetTickerCategoryWithSubCategories(input.TickerCategoryId);
                    List<Ticker> tickers = tickerService.GetTickersByCategoryIds(tickerCategories.Select(x => x.TickerCategoryId).ToList()) ?? new List<Ticker>();
                    var tickerCategory = tickerCategories.Where(x => x.TickerCategoryId == input.TickerCategoryId).FirstOrDefault();

                    List<Ticker> categoryTickers = null;
                    //workRole
                    if (input.UserRole == (int)WorkRole.TickerProducerOneA || input.UserRole == (int)WorkRole.TickerAdmin)
                    {
                        categoryTickers = tickers.Where(x => (x.CategoryId ?? 0) == tickerCategory.TickerCategoryId).OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                    }
                    else if (input.UserRole == (int)WorkRole.TickerCopyWriterOneA)
                    {
                        categoryTickers = tickers.Where(x => (x.CategoryId ?? 0) == tickerCategory.TickerCategoryId && (x.StatusId == (int)TickersStatus.New || x.StatusId == (int)TickersStatus.ApprovalPending || x.StatusId == (int)TickersStatus.Rejected)).OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                    }
                    else if (input.UserRole == (int)WorkRole.EnglishTickerWriter)
                    {
                        categoryTickers = tickers.Where(x => (x.CategoryId ?? 0) == tickerCategory.TickerCategoryId && x.StatusId == (int)TickersStatus.Approved && x.IsApproved).OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                        var tickerIds = categoryTickers.Select(x => x.TickerId).ToList();
                        var tickerTranslations = tickerService.GetTickerTranslations(tickerIds, LanguageCode.English).ToList();
                        var verifiedTranslationsTickerIds = tickerTranslations.Where(x => x.StatusId == (int)TranslationStatus.Approved).Select(x => x.TickerId);

                        categoryTickers = categoryTickers.Where(x => !verifiedTranslationsTickerIds.Contains(x.TickerId)).ToList();

                        foreach (var ticker in categoryTickers)
                        {
                            var translation = tickerTranslations.Where(x => x.TickerId == ticker.TickerId).FirstOrDefault();
                            ticker.StatusId = translation?.StatusId ?? (int)TranslationStatus.New;
                            ticker.Translation = translation?.Text ?? string.Empty;
                        }
                    }
                    else if (input.UserRole == (int)WorkRole.TickerEditorial)
                    {
                        categoryTickers = tickers.Where(x => (x.CategoryId ?? 0) == tickerCategory.TickerCategoryId && (x.StatusId == (int)TickersStatus.ApprovalPending)).OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                    }
                    else if (input.UserRole == (int)WorkRole.EnglishTickerEditorial)
                    {
                        var tickerIds = tickers.Select(x => x.TickerId).ToList();
                        var unverifiedTranslations = tickerService.GetTickerTranslations(tickerIds, LanguageCode.English).Where(x => x.StatusId == (int)TranslationStatus.ApprovalPending);
                        var unverifiedTranslationsTickerIds = unverifiedTranslations.Select(x => x.TickerId);
                        categoryTickers = tickers.Where(x => unverifiedTranslationsTickerIds.Contains(x.TickerId)).ToList();
                        foreach (var ticker in categoryTickers)
                        {
                            var translation = unverifiedTranslations.Where(x => x.TickerId == ticker.TickerId).FirstOrDefault();
                            ticker.Translation = translation?.Text ?? string.Empty;
                            ticker.StatusId = translation?.StatusId ?? (int)TranslationStatus.ApprovalPending;
                        }
                    }
                    else if (input.UserRole == (int)WorkRole.EnglishTickerProducer)
                    {
                        categoryTickers = tickers.Where(x => (x.CategoryId ?? 0) == tickerCategory.TickerCategoryId && x.StatusId == (int)TickersStatus.Approved && x.IsApproved).OrderByDescending(x => x.SequenceId).ToList() ?? new List<Ticker>();
                        var tickerIds = categoryTickers.Select(x => x.TickerId).ToList();
                        var tickerTranslations = tickerService.GetTickerTranslations(tickerIds, LanguageCode.English).ToList();
                        var verifiedTranslationsTickerIds = tickerTranslations.Select(x => x.TickerId);

                       // categoryTickers = categoryTickers.Where(x => !verifiedTranslationsTickerIds.Contains(x.TickerId)).ToList();

                        foreach (var ticker in categoryTickers)
                        {
                            var translation = tickerTranslations.Where(x => x.TickerId == ticker.TickerId).FirstOrDefault();
                            ticker.StatusId = translation?.StatusId ?? (int)TranslationStatus.New;
                            ticker.Translation = translation?.Text ?? string.Empty;
                        }
                    }
                    var subCategories = tickerCategories.Where(x => (x.ParentTickerCategoryId ?? 0) == tickerCategory.TickerCategoryId).ToList() ?? new List<TickerCategory>();

                    transfer.Data.CopyFrom(tickerCategory);
                    if (categoryTickers.Count() > 0)
                    {
                        transfer.Data.Tickers.CopyFrom(categoryTickers);
                    }

                    if (subCategories.Count() > 0)
                    {
                        foreach (var cat in subCategories.OrderBy(x => x.SequenceNumber))
                        {
                            var subCat = new Core.DataTransfer.TickerCategory.GetOutput();
                            subCat.CopyFrom(cat);

                            List<Ticker> subTickers = null;
                            //workRole
                            if (input.UserRole == (int)WorkRole.TickerProducerOneA || input.UserRole == (int)WorkRole.TickerAdmin)
                            {
                                subTickers = tickers.Where(x => (x.CategoryId ?? 0) == cat.TickerCategoryId).OrderByDescending(x => x.SequenceId).ToList();
                            }
                            else if (input.UserRole == (int)WorkRole.TickerCopyWriterOneA)
                            {
                                subTickers = tickers.Where(x => (x.CategoryId ?? 0) == cat.TickerCategoryId && (x.StatusId == (int)TickersStatus.New || x.StatusId == (int)TickersStatus.ApprovalPending || x.StatusId == (int)TickersStatus.Rejected)).OrderByDescending(x => x.SequenceId).ToList();
                            }
                            else if (input.UserRole == (int)WorkRole.TickerEditorial)
                            {
                                subTickers = tickers.Where(x => (x.CategoryId ?? 0) == cat.TickerCategoryId && (x.StatusId == (int)TickersStatus.ApprovalPending)).OrderByDescending(x => x.SequenceId).ToList();
                            }
                            subCat.Tickers.CopyFrom(subTickers);
                            transfer.Data.Subcategories.Add(subCat);
                        }
                    }
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("CreateTicker")]
        public DataTransfer<Ticker> CreateTicker(TickerInput input)
        {
            DataTransfer<Ticker> transfer = new DataTransfer<Ticker>();

            if (input != null)
            {
                try
                {
                    transfer.Data = new Ticker();
                    var ticker = tickerService.CreateTicker(input.Text, input.TickerCategoryId, input.NewsFileId, input.UserId, input.UserRoleId, true);
                    transfer.Data.CopyFrom(ticker);
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("SortTicker")]
        public DataTransfer<bool> SortTicker(TickerInput input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();

            if (input != null)
            {
                try
                {
                    var isSuccess = tickerService.SortTicker(input.TickerId, input.TickerCategoryId, input.SequenceId, input.Direction);
                    transfer.Data.CopyFrom(isSuccess);
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("DeleteTicker")]
        public DataTransfer<bool> DeleteTicker(TickerInput input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();

            if (input != null)
            {
                try
                {
                    var isSuccess = tickerService.DeleteTicker(input.TickerId);
                    transfer.Data.CopyFrom(isSuccess);


                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("MarkTickerValid")]
        public DataTransfer<bool> MarkTickerValid(int tickerId)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();


            try
            {
                var isSuccess = tickerService.MarkTickerValid(tickerId);
                transfer.Data.CopyFrom(isSuccess);


            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }


            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateTicker")]
        public DataTransfer<Ticker> UpdateTickerbById(TickerInput input)
        {
            DataTransfer<Ticker> transfer = new DataTransfer<Ticker>();

            if (input != null)
            {
                try
                {
                    transfer.Data = new Ticker();
                    bool isSuccess = tickerService.UpdateTickerbById(input.TickerId, input.Text, input.StatusId, input.UserId, input.UserRoleId);
                    if (isSuccess)
                    {
                        var ticker = tickerService.Get(Convert.ToString(input.TickerId));
                        transfer.Data.CopyFrom(ticker.Data);
                        if (input.UserRoleId == (int)WorkRole.EnglishTickerEditorial || input.UserRoleId == (int)WorkRole.EnglishTickerWriter)
                        {
                            var translation = tickerService.GetTranslation(input.TickerId, LanguageCode.English);
                            transfer.Data.Translation = translation.Text;
                            transfer.Data.StatusId = translation.StatusId;
                        }
                        transfer.IsSuccess = isSuccess;


                    }
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("SubmitTickersByCateogryId")]
        public DataTransfer<bool> SubmitTickersByCateogryId(int categoryId)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();

            if (categoryId > 0)
            {
                try
                {
                    bool isSuccess = tickerService.SubmitTickersByCateogryId(categoryId);
                    transfer.Data = isSuccess;
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("CreateTickerByNewsFileId")]
        public DataTransfer<NMS.Core.DataTransfer.Ticker.GetOutput> CreateTickerByNewsFileId(int NewsFileId, int CreatedBy)
        {
            DataTransfer<NMS.Core.DataTransfer.Ticker.GetOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.Ticker.GetOutput>();

            if (NewsFileId > 0)
            {
                try
                {
                    transfer.Data = new NMS.Core.DataTransfer.Ticker.GetOutput();
                    var newsFile = newsFileService.GetNewsFile(NewsFileId);
                    if (newsFile != null)
                    {
                        var ticker = tickerService.CreateTicker(newsFile.Slug, (int)TickerTypes.BreakingA, NewsFileId, CreatedBy, (int)WorkRole.TickerProducerOneA);
                        transfer.Data.CopyFrom(ticker);
                    }
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetAllTickerCategoriesGroupByParentCategory")]
        public DataTransfer<List<NMS.Core.DataTransfer.TickerCategory.GetOutput>> GetAllTickerCategoriesGroupByParentCategory(int[] ParentCategoryIds)
        {
            DataTransfer<List<NMS.Core.DataTransfer.TickerCategory.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.TickerCategory.GetOutput>>();

            try
            {
                transfer.Data = new List<NMS.Core.DataTransfer.TickerCategory.GetOutput>();
                var ticker = tickerService.GetAllTickerCategoriesGroupByParentCategory(ParentCategoryIds);
                transfer.Data.CopyFrom(ticker);

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }


            return transfer;
        }

        [HttpPost]
        [ActionName("GetAllTickerCategoriesGroupByTabName")]
        public DataTransfer<List<NMS.Core.DataTransfer.TickerCategory.GetOutput>> GetAllTickerCategoriesGroupByTabName(string tabName)
        {
            DataTransfer<List<NMS.Core.DataTransfer.TickerCategory.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.TickerCategory.GetOutput>>();

            try
            {
                transfer.Data = new List<NMS.Core.DataTransfer.TickerCategory.GetOutput>();
                var ticker = tickerService.GetAllTickerCategoriesGroupByTabName(tabName);
                transfer.Data.CopyFrom(ticker);

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }


            return transfer;
        }

        [HttpPost]
        [ActionName("MoveTickerByCategoryId")]
        public DataTransfer<NMS.Core.DataTransfer.Ticker.GetOutput> MoveTickerByCategoryId(TickerInput input)
        {
            DataTransfer<NMS.Core.DataTransfer.Ticker.GetOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.Ticker.GetOutput>();

            if (input != null)
            {
                try
                {
                    var isSuccess = tickerService.MoveTickerByCategoryId(input.TickerId, input.TickerCategoryId);
                    if (isSuccess)
                    {
                        transfer = tickerService.Get(Convert.ToString(input.TickerId));
                    }
                }
                catch (Exception ex)
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = ex.Message;
                    ExceptionLogger.Log(ex);
                }
            }

            return transfer;
        }

        //get tickers

        [HttpGet]
        [ActionName("GetAllTickerCategories")]
        public DataTransfer<List<TickerCategory>> GetAllTickerCategories()
        {
            DataTransfer<List<TickerCategory>> transfer = new DataTransfer<List<TickerCategory>>();
            try
            {
                transfer.Data = new List<TickerCategory>();
                transfer.Data = tickerService.GetAllTickerCategories();
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }
        [HttpGet]
        [ActionName("GetAllMCRTickers")]
        public DataTransfer<List<MCRTickerCategory>> GetAllMCRTickers()
        {
            DataTransfer<List<MCRTickerCategory>> transfer = new DataTransfer<List<MCRTickerCategory>>();
            try
            {
                MCRTickerService mcrTickerService = new MCRTickerService();
                transfer.Data = new List<MCRTickerCategory>();
                transfer.Data = mcrTickerService.GetMCRTickerCategories().Where(x => !x.IsManual).ToList();
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("TriggerTickerSync")]
        public DataTransfer<bool> TriggerTickerSync()
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                var t1 = Task.Factory.StartNew(() =>
                {
                    tickerService.pushTickersInCategories(true, true, false);
                    transfer.Data = true;
                });
                t1.Wait();

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("TriggerMCRPush")]
        public DataTransfer<bool> TriggerMCRPush()
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                var t1 = Task.Factory.StartNew(() =>
                {
                    tickerService.pushTickersInCategories(true, false, true);
                    transfer.Data = true;
                });
                t1.Wait();

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpGet]
        [ActionName("GetTickerToRunReport")]
        public DataTransfer<List<Ticker>> GetTickerToRunReport()
        {
            DataTransfer<List<Ticker>> transfer = new DataTransfer<List<Ticker>>();
            try
            {
                List<Ticker> tickerlst = new List<Ticker>();
                var tickerCategories = (tickerCategoryService.GetAllTickerCategory() ?? new List<TickerCategory>()).Where(x => x.IsActive.HasValue && x.IsActive.Value);
                MCRTickerService mcrTickerService = new MCRTickerService();
                var mcrTickerCategories = mcrTickerService.GetMCRTickerCategories();

                var categorySize = new Dictionary<int, int>();
                var categoryTickers = new Dictionary<int, int>();
                foreach (var category in tickerCategories.Where(x => !x.ParentTickerCategoryId.HasValue || x.ParentTickerCategoryId.Value == 0))
                {
                    categorySize.Add(category.TickerCategoryId, category.TickerSize);
                    categoryTickers.Add(category.TickerCategoryId, 0);
                }
                foreach (var category in tickerCategories.OrderBy(x => x.Priority ?? 0).ThenBy(x => x.TickerCategoryId))
                {
                    var mcrTickerCategory = mcrTickerCategories.Where(x => x.MCRTickerCategoryId == category.McrTickerCategoryId).FirstOrDefault();
                    if (mcrTickerCategory == null) { continue; }
                    var tickers = tickerService.GetTickersForMCR(category.TickerCategoryId, category.TickerSize, true, true);
                    var historyTickers = new List<McrTickerHistory>();

                    tickers = tickers.Where(x => x.StatusId == (int)TickersStatus.Approved).OrderByDescending(x => x.SequenceId).ToList();

                    if (tickers.Count < category.TickerSize && category.TicketCategoryTypeId != (int)TickerCategoryType.Entity
                                && category.TicketCategoryTypeId != (int)TickerCategoryType.Location
                                && category.TicketCategoryTypeId != (int)TickerCategoryType.Face
                                && category.TicketCategoryTypeId != (int)TickerCategoryType.Topic)
                    {
                        var newTickerIds = tickers.Select(x => x.TickerId).ToList();
                        historyTickers = mcrtickerhistoryservice.GetLastBroadcastedTickers(category.TickerCategoryId, category.TickerSize - tickers.Count, newTickerIds);
                    }



                    #region Current Tickers Insertion
                    foreach (var ticker in tickers)
                    {
                        if (category.TicketCategoryTypeId == (int)TickerCategoryType.Entity
                             || category.TicketCategoryTypeId == (int)TickerCategoryType.Location
                             || category.TicketCategoryTypeId == (int)TickerCategoryType.Face
                             || category.TicketCategoryTypeId == (int)TickerCategoryType.Topic)
                        {
                            if (categoryTickers[category.ParentTickerCategoryId.Value] >= categorySize[category.ParentTickerCategoryId.Value])
                            {
                                continue;
                            }
                        }
                        tickerlst.Add(ticker);
                        categoryTickers[category.ParentTickerCategoryId.Value]++;
                    }
                    #endregion

                    #region History Tickers Insertion
                    foreach (var ticker in historyTickers.OrderByDescending(x => x.CreationDate))
                    {
                        var tick = tickerService.GetTicker(ticker.TickerId.Value);

                        if (tick != null)
                            tickerlst.Add(tick);
                    }
                    #endregion
                }
                transfer.Data = new List<Ticker>();
                transfer.Data = tickerlst;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetMcrTickerBroadcastedByDate")]
        public DataTransfer<List<McrTickerBroadcasted>> GetMcrTickerBroadcastedByDate(string From, string To)
        {
            DataTransfer<List<McrTickerBroadcasted>> transfer = new DataTransfer<List<McrTickerBroadcasted>>();
            try
            {
                DateTime dtfrom = new DateTime();
                DateTime dtto = new DateTime();
                if (string.IsNullOrEmpty(From) && string.IsNullOrEmpty(To))
                {
                    dtfrom = DateTime.UtcNow.AddHours(-24);
                    dtto = DateTime.UtcNow;
                }
                else
                {
                    dtfrom = Convert.ToDateTime(From.Trim());
                    dtto = Convert.ToDateTime(To.Trim());
                }

                var result = mcrtickerbroadcastedservice.GetMcrTickerBroadcastedByDate(dtfrom, dtto);
                transfer.Data = new List<McrTickerBroadcasted>();
                transfer.Data = result;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetMcrTickerHistoryByBroadcastedId")]
        public DataTransfer<List<McrTickerHistory>> GetMcrTickerHistoryByBroadcastedId(int Id)
        {
            DataTransfer<List<McrTickerHistory>> transfer = new DataTransfer<List<McrTickerHistory>>();
            try
            {
                var result = mcrtickerhistoryservice.GetMcrTickerHistoryByBroadcastedId(Id);
                transfer.Data = new List<McrTickerHistory>();
                transfer.Data = result;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("InsertTickerCategory")]
        public DataTransfer<TickerCategory> InsertTickerCategory(TickerCategory input)
        {
            DataTransfer<TickerCategory> transfer = new DataTransfer<TickerCategory>();
            try
            {
                transfer.Data = new TickerCategory();
                if (input.TickerCategoryId != 0 && input.TickerCategoryId > 0)
                    transfer.Data = tickerService.updateTickerCategory(input);
                else
                    transfer.Data = tickerService.InsertTickerCategory(input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }
        [HttpPost]
        [ActionName("updateTickerCategory")]
        public DataTransfer<NMS.Core.DataTransfer.TickerCategory.GetOutput> updateTickerCategory(TickerCategory input)
        {
            DataTransfer<NMS.Core.DataTransfer.TickerCategory.GetOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.TickerCategory.GetOutput>();
            try
            {
                transfer.Data = new NMS.Core.DataTransfer.TickerCategory.GetOutput();
                var ticker = tickerService.updateTickerCategory(input);
                transfer.Data.CopyFrom(ticker);

            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }


        [HttpPost]
        [ActionName("DeleteTickerCategory")]
        public DataTransfer<bool> DeleteTickerCategory(TickerCategory input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                var ticker = tickerService.DeleteTickerCategory(input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }
            return transfer;
        }



    }
}
