﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;
using NMS.Core.DataTransfer;
using NMS.Core.Entities;
using NMS.Core.IService;
using NMS.Core.IController;
using NMS.Core.Helper;
using NMS.Core.Models;
using System.Web;

namespace NMS.Web.API
{
    public class UserController : ApiController, IUserController
    {
        IUserManagementService userManagementService = IoC.Resolve<IUserManagementService>("UserManagementService");
        ITeamService teamService = IoC.Resolve<ITeamService>("TeamService");
        ITeamRoleService teamRoleService = IoC.Resolve<ITeamRoleService>("TeamRoleService");
        IVideoCutterIpService videCutterIp = IoC.Resolve<IVideoCutterIpService>("VideoCutterIpService");

        [HttpPost]
        [Obsolete("This method is deprecated.")]
        [ActionName("Login")]
        public DataTransfer<NMS.Core.DataTransfer.User.PostOutput> Login(NMS.Core.DataTransfer.User.PostInput input)
        {
            
            DataTransfer<NMS.Core.DataTransfer.User.PostOutput> transfer = new DataTransfer<NMS.Core.DataTransfer.User.PostOutput>();
            try
            {
                if (input != null)
                {
                    NMS.Core.DataTransfer.User.PostOutput user = new Core.DataTransfer.User.PostOutput();
                    if (user != null)
                        transfer.Data = user;
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "Login failed" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("LoginMMS")]
        public DataTransfer<MMSLoadInitialDataLoginOutput> LoginMMS(NMS.Core.DataTransfer.User.PostInput input)
        {
            string MMSUsermanagementUrl = AppSettings.MMSUsermanagementUrl;

            DataTransfer<MMSLoadInitialDataLoginOutput> transfer = new DataTransfer<MMSLoadInitialDataLoginOutput>();

            try
            {
                if (input != null)
                {

                    HttpWebRequestHelper helper = new HttpWebRequestHelper();

                    MMSLoadInitialDataLoginInput mmsInput = new MMSLoadInitialDataLoginInput();
                    mmsInput.LoginId = input.Login;
                    mmsInput.Password = input.Password;
                    DataTransfer<MMSLoadInitialDataLoginOutput> output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<MMSLoadInitialDataLoginOutput>>(MMSUsermanagementUrl + "api/admin/Authenticate", mmsInput, null);

                    if (output != null && output.IsSuccess == true)
                    {
                        output.Data.EncriptedName = SignalRChat.EncryptionHelper.Encrypt(output.Data.FullName);
                        output.Data.EncriptedUserId = SignalRChat.EncryptionHelper.Encrypt(output.Data.UserId.ToString());
                        transfer.Data = output.Data;
                        //Session["User"] = "";
                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { output.Errors[0].ToString() + "" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }



        [HttpGet]
        [ActionName("GetUsers")]
        public MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>> GetUsers()
        {
            MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>> transfer = new MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>>();
            MMS.Integration.UserManagement.UserManagement userManagement = new MMS.Integration.UserManagement.UserManagement();
            try
            {
                transfer = userManagement.UsersWithWorkRole(1);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("ChangePassword")]
        public MMS.Integration.DataTransfer<Core.DataTransfer.DataTransfer<User>> ChangePassword(string userId, string password)
        {
            MMS.Integration.DataTransfer<Core.DataTransfer.DataTransfer<User>> transfer = new MMS.Integration.DataTransfer<Core.DataTransfer.DataTransfer<User>>();
            string MMSUsermanagementUrl = AppSettings.MMSUsermanagementUrl;
            try
            {
                HttpWebRequestHelper helper = new HttpWebRequestHelper();
                transfer = helper.GetRequest<MMS.Integration.DataTransfer<Core.DataTransfer.DataTransfer<User>>>(MMSUsermanagementUrl + "api/admin/ChangeUserPassword?userId=" + userId + "&password=" + password, null);
                transfer.Data =  null;
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Data = null;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                //ExceptionLogger.Log(ex);
            }

            return transfer;
        }


        [HttpGet]
        [ActionName("GetUsersByProgram")]
        public DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> GetUsersByProgram(int programId)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> transfer = new DataTransfer<List<Core.DataTransfer.Team.GetOutput>>();

            try
            {
                List<Team> teams = teamService.GetTeamByProgramId(programId);
                transfer.Data = new List<Core.DataTransfer.Team.GetOutput>();
                if (teams != null)
                    foreach (Team t in teams)
                    {
                        NMS.Core.DataTransfer.Team.GetOutput entity = new Core.DataTransfer.Team.GetOutput();
                        entity.CopyFrom(t);
                        transfer.Data.Add(entity);
                    }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("DeleteFromTeam")]
        public DataTransfer<bool> DeleteFromTeam(NMS.Core.DataTransfer.Team.PostInput input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();
            try
            {
                transfer.Data = teamService.DeleteFromTeam(Convert.ToInt32(input.UserId), Convert.ToInt32(input.WorkRoleId), Convert.ToInt32(input.ProgramId));
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("AddUserToTeam")]
        public DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> AddUserToTeam(NMS.Core.DataTransfer.Team.PostInput input)
        {
            DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>>();

            try
            {
                string message = teamService.InsertTeam(input);
                transfer.Data = new List<Core.DataTransfer.Team.GetOutput>();
                List<Team> teams = teamService.GetTeamByProgramId(Convert.ToInt32(input.ProgramId));
                foreach (Team t in teams)
                {
                    NMS.Core.DataTransfer.Team.GetOutput entity = new Core.DataTransfer.Team.GetOutput();
                    entity.CopyFrom(t);
                    transfer.Data.Add(entity);
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }


        [HttpGet]
        [ActionName("GetTeamRole")]
        public DataTransfer<List<NMS.Core.DataTransfer.TeamRole.GetOutput>> GetTeamRole()
        {
            DataTransfer<List<NMS.Core.DataTransfer.TeamRole.GetOutput>> transfer = new DataTransfer<List<NMS.Core.DataTransfer.TeamRole.GetOutput>>();
            try
            {
                transfer = teamRoleService.GetAll();
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("RegisterUserIp")]

        public void RegisterUserIp(string VideoCutterVersion)
        {
            try
            {
       
                    string VisitorsIPAddr = string.Empty;
                    string Host = string.Empty;


                    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                        VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
                        VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;

                    VideoCutterIp entity = new VideoCutterIp();

                    IPHostEntry host = Dns.GetHostEntry(VisitorsIPAddr);
                    entity.UserIp = VisitorsIPAddr;
                    entity.Host = host.HostName;
                    entity.VideoCutterVersion = VideoCutterVersion;
                    entity.CreationDate = DateTime.UtcNow;
                    entity.LastUpdateDate = DateTime.UtcNow;

                    List<VideoCutterIp> videoCutterIpRecord = videCutterIp.GetVideoCutterIpByKeyValue("UserIP", VisitorsIPAddr, Operands.Equal, null);

                    if (videoCutterIpRecord == null)
                    {
                        videCutterIp.InsertVideoCutterIp(entity);
                    }
                    else if (videoCutterIpRecord != null && videoCutterIpRecord.Count == 1)
                    {
                        videoCutterIpRecord[0].VideoCutterVersion = VideoCutterVersion;
                        videoCutterIpRecord[0].LastUpdateDate = DateTime.UtcNow;
                        videCutterIp.UpdateVideoCutterIp(videoCutterIpRecord[0]);
                    }

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
            }
        }

        [HttpGet]
        [ActionName("GetUsersByWorkRoleId")]
        public MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>> GetUsersByWorkRoleId(int workRolesId)
        {
            MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>> transfer = new MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>>();
            MMS.Integration.UserManagement.UserManagement userManagement = new MMS.Integration.UserManagement.UserManagement();
            try
            {
                transfer = userManagement.GetUsersByWorkRoleId(workRolesId);
            }
            catch (Exception exp)
            {
                ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

    }
}
