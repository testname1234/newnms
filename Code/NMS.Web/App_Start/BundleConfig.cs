﻿using System.Web.Optimization;

namespace NMS.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/reporters/common/")
                .IncludeDirectory("~/Scripts/app/reporters/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/v3/common/")
               .IncludeDirectory("~/Scripts/app/v3/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/fieldreporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team A/fieldreporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/v3/")
               .IncludeDirectory("~/Scripts/app/v3", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/shared/")
             .IncludeDirectory("~/Scripts/app/shared", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/publicreporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team A/publicreporter", "*.js", searchSubdirectories: false));

            //bundles.Add(new ScriptBundle("~/bundles/reporter/teama/common/")
            //    .IncludeDirectory("~/Scripts/app/reporters/team A/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/channelreporter/")
              .IncludeDirectory("~/Scripts/app/reporters/team B/channelreporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/NewspaperReporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team B/NewspaperReporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/radioreporter/")
                .IncludeDirectory("~/Scripts/app/reporters/team B/radioreporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/reporter/teamb/common/")
                .IncludeDirectory("~/Scripts/app/reporters/team B/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/reporter/teamc/common/")
                .IncludeDirectory("~/Scripts/app/reporters/team C/common", "*.js", searchSubdirectories: false));

            
            bundles.Add(new ScriptBundle("~/bundles/courtreporter/")
             .IncludeDirectory("~/Scripts/app/reporters/team C/CourtReporter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/policereporter/")
         .IncludeDirectory("~/Scripts/app/reporters/team C/PoliceReporter", "*.js", searchSubdirectories: false));

     
            bundles.Add(new ScriptBundle("~/bundles/videoswitcher/")
                .IncludeDirectory("~/Scripts/app/mcruser/Videoswitcher", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/audiouser/")
             .IncludeDirectory("~/Scripts/app/mcruser/Audiouser", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/graphicuser/")
            .IncludeDirectory("~/Scripts/app/mcruser/Graphicuser", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/videoplayuser/")
            .IncludeDirectory("~/Scripts/app/mcruser/Videoplayuser", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/usermanagement/")
           .IncludeDirectory("~/Scripts/app/user", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/anchor/")
           .IncludeDirectory("~/Scripts/app/anchor", "*.js", searchSubdirectories: false));


            bundles.Add(new ScriptBundle("~/bundles/producer/")
                .IncludeDirectory("~/Scripts/app/producer", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/nmsBol/")
                .IncludeDirectory("~/Scripts/app/nmsBol", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/NewsFile/")
              .IncludeDirectory("~/Scripts/app/NewsFile", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/casper/")
                .IncludeDirectory("~/Scripts/app/CaperClient", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/videowall/")
                .IncludeDirectory("~/Scripts/app/VideoWall", "*.js", searchSubdirectories: false));
            
            bundles.Add(new ScriptBundle("~/bundles/teleprompter/")
                .IncludeDirectory("~/Scripts/app/teleprompter", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/Verification/")
                .IncludeDirectory("~/Scripts/app/Verification/common", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/FieldReporterVerification/")
                .IncludeDirectory("~/Scripts/app/Verification/FieldReporterVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/FReporterVerification/")
                .IncludeDirectory("~/Scripts/app/Verification/FReporterVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/ChannelReporterVerification/")
                .IncludeDirectory("~/Scripts/app/Verification/ChannelReporterVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/RadioReporterVerification/")
               .IncludeDirectory("~/Scripts/app/Verification/RadioReporterVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/NewspaperVerification/")
            .IncludeDirectory("~/Scripts/app/Verification/NewspaperVerification", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/models/")
                .IncludeDirectory("~/Scripts/app/models/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/managers/")
                .IncludeDirectory("~/Scripts/app/managers/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/common/")
                .IncludeDirectory("~/Scripts/app/common/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/mock/")
                .IncludeDirectory("~/Scripts/app/mock/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/controls/")
                .IncludeDirectory("~/Scripts/app/controls/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/bindcommon/")
                .IncludeDirectory("~/Scripts/app/bindCommon", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/libs/")
                   .Include("~/Scripts/lib/jquery-2.0.2.min.js")
                   .Include("~/Scripts/lib/jquery-ui.js")
                   .Include("~/Scripts/lib/jquery.validate.js")
                   .Include("~/Scripts/lib/jquery.mCustomScrollbar.min.js")
                   .Include("~/Scripts/lib/jquery-te-1.4.0.js")
                   .Include("~/Scripts/lib/jquery.UrduEditor.js")
                   .Include("~/Scripts/lib/bootstrap-multiselect.js")
                   .Include("~/Scripts/lib/bootstrap-3.1.1.min.js")
                   .Include("~/Scripts/lib/jquery.mockjson.js")
                   .Include("~/Scripts/lib/underscore.js")
                   .Include("~/Scripts/lib/TrafficCop.js")
                   .Include("~/Scripts/lib/toastr.min.js")
                   .Include("~/Scripts/lib/sammy.js")
                   .Include("~/Scripts/lib/sammy.title.js")
                   .Include("~/Scripts/lib/moment.js")
                   .Include("~/Scripts/lib/infuser.js")
                   .Include("~/Scripts/lib/knockout-3.2.0.min.js")
                   .Include("~/Scripts/lib/koExternalTemplateEngine.js")
                   .Include("~/Scripts/lib/knockout.validation.js")
                   .Include("~/Scripts/lib/knockout.dirtyFlag.js")
                   .Include("~/Scripts/lib/knockout.asyncCommand.js")
                   .Include("~/Scripts/lib/knockout.activity.js")
                   .Include("~/Scripts/lib/json2.js")
                   .Include("~/Scripts/lib/amplify.core.js")
                   .Include("~/Scripts/lib/amplify.store.js")
                   .Include("~/Scripts/lib/amplify.request.js")
                   .Include("~/Scripts/lib/bluimp/vendor/jquery.ui.widget.js")
                   .Include("~/Scripts/lib/bluimp/tmpl.js")
                   .Include("~/Scripts/lib/bluimp/load-image.js")
                   .Include("~/Scripts/lib/bluimp/canvas-to-blob.js")
                   .Include("~/Scripts/lib/bluimp/jquery.iframe-transport.js")
                   .Include("~/Scripts/lib/bluimp/jquery.fileupload.js")
                   .Include("~/Scripts/lib/bluimp/jquery.fileupload-fp.js")
                   .Include("~/Scripts/lib/bluimp/jquery.fileupload-ui.js")
                   .Include("~/Scripts/lib/DatePicker.js")
                   .Include("~/Scripts/lib/string.js")
                   .Include("~/Scripts/lib/cycle2.js")
                   .Include("~/Scripts/lib/jquery.bxslider.js")
                   .Include("~/Scripts/lib/XMLWriter-1.0.0.js")
                   .Include("~/Scripts/lib/jquery.ui-contextmenu.js")
                   .Include("~/Scripts/lib/require.js")
                   .Include("~/Scripts/lib/sockjs-0.3.js")
                   .Include("~/Scripts/lib/stomp.js")
               );

        }
    }
}