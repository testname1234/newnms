﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using MMS.Integration.UserManagement;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using NMS.Web.SignalRChat;
using NMS.Service;
using Owin;
using NMS.Core.Service;
using NMS.Core.IService;
using NMS.Core;
using NMS.Core.Entities;


namespace SignalRChat
{

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}

namespace NMS.Web
{
    public class GroupsAndUsers 
    {
        public List<Group> Groups { get; set; }
        public List<UserDetail> Users { get; set; }
    }


    public class ChatHub : Hub
    {
        #region Data Members

        static List<UserDetail> ConnectedUsers = new List<UserDetail>();
        static List<Group> chatGroups = new List<Group>();
        static List<MessageDetail> CurrentMessage = new List<MessageDetail>();
        public IChatMessageService chatServ = IoC.Resolve<IChatMessageService>("ChatMessageService");
        public IGroupService groupServ = IoC.Resolve<IGroupService>("GroupService");

        #endregion

        #region Methods

        public void Connect(string userName)
        {
            if (ConnectedUsers.Count ==0)
                AddUserstoCurrentUsers();
            //if (chatGroups == null || (chatGroups != null && chatGroups.Count == 0))
                RefreshAllGroupData();

            userName = userName.Replace(' ', '+');
            string encrypted = userName; 
            userName = EncryptionHelper.Decrypt(encrypted); 

            UserDetail thisUser = ConnectedUsers.Where(x => x.UserName == userName).FirstOrDefault();
            if (thisUser != null)
            {
                for (int j = 0; j < ConnectedUsers.Count; j++)
                {
                    ConnectedUsers[j].Messages = new List<ClientSideMessageEntity>();
                }

                GetMessagesOfThisUser(thisUser);
                List<Group> thisUserGroup = GetGroupsOfThisUserWithMessages(thisUser);
                thisUser.ConnectionId = Context.ConnectionId;
                var id = thisUser.ConnectionId;
                //ConnectedUsers.Add(new UserDetail { ConnectionId = id, UserName = userName });

                thisUser.Online = true;
                ConnectedUsers = ConnectedUsers.OrderBy(y => y.UserName).OrderByDescending(x => x.Online == true).ToList();
                List<UserDetail> toSendUserList = new List<UserDetail>();
                toSendUserList.AddRange(ConnectedUsers);
                toSendUserList.RemoveAll(j => j.UserId == thisUser.UserId);
                // send to caller
                Clients.Caller.onConnected(thisUser.UserId, userName, toSendUserList, thisUserGroup);

                // send to all except caller client
                Clients.AllExcept(id).onNewUserConnected(thisUser.UserId, thisUser.ConnectionId);

            }
            else
            {
                AddUserstoCurrentUsers();
            }

        }

        

        private List<Group> GetGroupsOfThisUserWithMessages(UserDetail thisUser)
        {
            List<Group> groupWithMsgs = new List<Group>();
            List<ChatMessage> allGroupMessages = new List<ChatMessage>();
            if (chatGroups != null)
            {
                foreach (Group gp in chatGroups)
                {
                    if (gp.UserIds.Contains(thisUser.UserId))
                    {
                        if (allGroupMessages==null || (allGroupMessages!=null && allGroupMessages.Count == 0))
                            allGroupMessages = chatServ.GetAllGroupMessages();
                        if (allGroupMessages != null)
                        {
                            List<ChatMessage> msgs = allGroupMessages.Where(x => x.To == gp.GroupId).ToList();
                            gp.Messages = new List<ClientSideMessageEntity>();
                            for (int x = 0; x < msgs.Count; x++)
                            {
                                string name = ConnectedUsers.Where(k => k.UserId == msgs[x].From).First().UserName;
                                gp.Messages.Add(new ClientSideMessageEntity() { from = name, msg = msgs[x].Message, dt = msgs[x].CreationDate });
                            }
                            gp.Messages = gp.Messages.OrderBy(l => l.dt).ToList();
                        }
                        groupWithMsgs.Add(gp);
                    }
                }
            }
            return groupWithMsgs;
        }


        private void GetMessagesOfThisUser(UserDetail user)
        {
            List<ChatMessage> allMessages = chatServ.GetChatMessagesForUser(user.UserId);

            if (allMessages != null && allMessages.Count > 0)
            {
                List<ChatMessage> messages = allMessages.Where(k => !k.IsSentToGroup.HasValue || (k.IsSentToGroup.HasValue && !k.IsSentToGroup.Value)).ToList();
                for (int i = 0; i < ConnectedUsers.Count; i++)
                {
                    List<ChatMessage> msgs = messages.Where(x=>x.From==ConnectedUsers[i].UserId || x.To==ConnectedUsers[i].UserId).ToList();
                    if (msgs != null && msgs.Count> 0)
                    {
                        for (int x = 0; x < msgs.Count; x++)
                        {
                            if (ConnectedUsers[i].UserId != user.UserId)
                            {
                                string name = ConnectedUsers.Where(k => k.UserId == msgs[x].From).First().UserName;
                                ConnectedUsers[i].Messages.Add(new ClientSideMessageEntity() { from = name, msg = msgs[x].Message , dt= msgs[x].CreationDate });
                            }
                            //else
                            //{
                            //    ConnectedUsers.Where(y => y.UserId == msgs[x].To).First().Messages.Add(new ClientSideMessageEntity() { from = user.UserName, msg = msgs[x].Message });
                            //}
                        }
                        ConnectedUsers[i].Messages = ConnectedUsers[i].Messages.OrderBy(l => l.dt).ToList();
                    }
                }
            }
        }


        public void AddUserstoCurrentUsers()
        {
            UserManagement userInt = new UserManagement();
            var userOut = userInt.UsersWithWorkRole(1);
            var users = userOut.Data;
            for (int i = 0; i < users.Count; i++)
            {
                var alreadyExist = ConnectedUsers.Where(x => x.UserId == users[i].UserId).FirstOrDefault();
                if (alreadyExist == null && users[i].Fullname.Length > 0)
                    ConnectedUsers.Add(new UserDetail { UserId = users[i].UserId, UserName = users[i].Fullname });
            }
        }



        public bool SendToGroup(int GroupId, int fromUserId, string message)
        {
            var fromUser = ConnectedUsers.Where(x => x.UserId == fromUserId).FirstOrDefault();
            message = message.Trim();

            if (GroupId != 0 && fromUser != null)
            {

                chatServ.InsertChatMessage(fromUser.UserId, GroupId, message, DateTime.Now, true, true);
                var group = chatGroups.Where(x => x.GroupId == GroupId).First();
                // send to 
                
                for (int i = 0; i < group.UserIds.Count; i++)
                {
                    var toUser = ConnectedUsers.Where(j => j.UserId == group.UserIds[i] && j.Online).FirstOrDefault();
                    if (toUser != null)
                        Clients.Client(toUser.ConnectionId).sendPrivateMessage(fromUser.UserId, fromUser.UserName, group, message, DateTime.Now);

                }
            }
            return true;

        }

        public GroupsAndUsers addGroup(string GroupName, List<int> Users, int creatorUserId)
        {
            //Console.WriteLine(GroupName);
            bool res = groupServ.AddNewGroupWithUserIds(GroupName, Users);
            if (res)
            {
                GroupsAndUsers resultant = new GroupsAndUsers();
                RefreshAllGroupData();
                var user = ConnectedUsers.Where(x => x.UserId == creatorUserId).First();
                resultant.Groups = GetGroupsOfThisUserWithMessages(user);
                resultant.Users = ConnectedUsers.Where(k => k.UserId != creatorUserId).OrderBy(y => y.UserName).OrderByDescending(j => j.Online == true).ToList();
                return resultant;
            }
            return null;
        }

        public GroupsAndUsers editGroup(int GroupId, List<int> Users, int creatorUserId)
        {
            var oldgroup = chatGroups.Where(x => x.GroupId == GroupId).First();
            bool res = groupServ.EditGroupWithUserIds(GroupId, Users , oldgroup.UserIds);
            if (res)
            {
                GroupsAndUsers resultant = new GroupsAndUsers();
                RefreshAllGroupData();
                var user = ConnectedUsers.Where(x => x.UserId == creatorUserId).First();
                resultant.Groups = GetGroupsOfThisUserWithMessages(user);
                resultant.Users = ConnectedUsers.Where(k => k.UserId != creatorUserId).OrderBy(y => y.UserName).OrderByDescending(j => j.Online == true).ToList();
                return resultant;
            }
            return null;
        }

        public GroupsAndUsers deleteGroup(int GroupId, int creatorUserId)
        {
            GroupsAndUsers resultant = new GroupsAndUsers();
            bool res = groupServ.DeleteGroupWithGroupUsers(GroupId);
            RefreshAllGroupData();
            var user = ConnectedUsers.Where(x => x.UserId == creatorUserId).First();
            resultant.Groups = GetGroupsOfThisUserWithMessages(user);
            resultant.Users = ConnectedUsers.Where(k => k.UserId != creatorUserId).OrderBy(y => y.UserName).OrderByDescending(j => j.Online == true).ToList();
            return resultant;
        }

        private void RefreshAllGroupData()
        {
            chatGroups = groupServ.GetAllGroupWithUserIds();
            if (chatGroups != null)
            {
                foreach (Group gp in chatGroups)
                {
                    if (gp.UserIds != null && gp.UserIds.Count > 0)
                    {
                        gp.Users = new List<User>();
                        for (int i = 0; i < gp.UserIds.Count; i++)
                        {
                            var user = ConnectedUsers.Where(x => x.UserId == gp.UserIds[i]).First();
                            gp.Users.Add(new User { UserId = user.UserId, Name = user.UserName });
                        }
                    }
                }
            }
        }

        public void SendMessageToAll(string userName, string message)
        {
            // store last 100 messages in cache
            AddMessageinCache(userName, message);

            // Broad cast message
            Clients.All.messageReceived(userName, message);
        }

        public void SendPrivateMessage(string toConnId, string message, int? fromUId=null)
        {
            if (fromUId.HasValue)
            {
                SendToGroup(Convert.ToInt32(toConnId), fromUId.Value, message);
            }
            else
            {
                message = message.Trim();
                string fromUserId = Context.ConnectionId;

                var toUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == toConnId);
                var fromUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == fromUserId);


                if (toUser != null && fromUser != null)
                {

                    chatServ.InsertChatMessage(fromUser.UserId, toUser.UserId, message, DateTime.Now, true);
                    // send to 
                    Clients.Client(toConnId).sendPrivateMessage(fromUser.UserId, fromUser.UserName, fromUser.ConnectionId, message, DateTime.Now);

                    // send to caller user
                    Clients.Caller.sendPrivateMessage(fromUser.UserId, fromUser.UserName, fromUser.ConnectionId, message, DateTime.Now);
                }
            }

        }

        

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                item.Online = false;

                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(item.UserId, item.ConnectionId, item.UserName);
            }
            return base.OnDisconnected(stopCalled);
        }

     
        #endregion

        #region private Messages

        private void AddMessageinCache(string userName, string message)
        {
            CurrentMessage.Add(new MessageDetail { UserName = userName, Message = message });

            if (CurrentMessage.Count > 100)
                CurrentMessage.RemoveAt(0);
        }

        #endregion
    }

}