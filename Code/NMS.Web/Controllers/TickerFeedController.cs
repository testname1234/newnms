﻿using NMS.Core;
using NMS.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using NMS.Core.DataTransfer;
using NMS.Core.DataTransfer.TickerCategory;

namespace NMS.Web.Controllers
{
    public class TickerFeedController : Controller
    {
        private ITickerService tickerService = IoC.Resolve<ITickerService>("TickerService");
        private ITickerCategoryService tickerCategoryService = IoC.Resolve<ITickerCategoryService>("TickerCategoryService");

        //
        // GET: /Ticker/

        public void Index()
        {
            List<Core.Entities.TickerCategory> AllCategories = tickerService.GetAllCategoriesHavingTickers();
            DataTransfer<List<Core.DataTransfer.Ticker.GetOutput>> AllTickers = tickerService.GetAll();
            AllTickers.Data = AllTickers.Data.Where(x => x.CategoryId !=null && x.StatusId == 3 && x.IsActive == true).ToList();

            XmlDocument doc = new XmlDocument();
            XmlElement tickerfeed = doc.CreateElement("tickerfeed");
            tickerfeed.SetAttribute("version", "2.4");

            for (int i = 0; i < AllCategories.Count; i++)
            {
                XmlElement playlist = doc.CreateElement("playlist");
                playlist.SetAttribute("type", "scrolling_carousel");
                playlist.SetAttribute("target", "carousel");
                playlist.SetAttribute("news", AllCategories[i].Name);

                List<Core.DataTransfer.Ticker.GetOutput> Tickers = AllTickers.Data.Where(x => x.CategoryId == AllCategories[i].TickerCategoryId).OrderByDescending(x=>x.CreationDate).Take(AllCategories[i].TickerSize).ToList();
                if (Tickers != null)
                {
                    for (int j = 0; j < Tickers.Count; j++)
                    {
                        XmlElement element = doc.CreateElement("element");
                        XmlElement template = doc.CreateElement("template");
                        template.InnerText = "grey_text";
                        XmlElement field = doc.CreateElement("field");
                        field.SetAttribute("name", "text");
                        field.InnerText = Tickers[j].Text;
                        element.AppendChild(template);
                        element.AppendChild(field);
                        playlist.AppendChild(element);
                    }
                }
                tickerfeed.AppendChild(playlist);
            }
            doc.AppendChild(tickerfeed);
            Response.Clear();
            // XmlSerializer xs = new XmlSerializer();
            //XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            //    namespaces.Add("image", "http://www.google.com/schemas/sitemap-image/1.1");
            doc.Save(Response.OutputStream);
            Response.ContentType = "text/xml";
            // xs.Serialize(Response.Output, doc, namespaces);
            Response.End();
        }
    }
}