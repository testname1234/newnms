﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NMS.Core.Entities;
using NMS.Core;
using NMS.MongoRepository;
using SolrManager;
using SolrManager.InputEntities;
using NMS.Core.Entities.Mongo;
using System.Web.Security;
using System.Web.SessionState;
using NMS.Service;

namespace NMS.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            SolrService<SolarNews> sService = new SolrService<SolarNews>();
            sService.Connect(ConfigurationManager.AppSettings["solrUrl"].ToString());
            Worker.Start(60);

            
            //sService.Clear();

            //MBunchFilterRepository mBunchFilterRep = new MBunchFilterRepository();
            //MNewsRepository mNewsRep = new MNewsRepository();
            ////List<MNews> mNews = mNewsRep.GetByUpdatedDate(DateTime.Now.AddDays(-5));
            //List<MBunchFilter> mBunchFilter = mBunchFilterRep.GetByUpdatedDate(DateTime.Now.AddDays(-5));

            //List<MNews> RawNews = mNewsRep.GetByBunchIDs(mBunchFilter.Select(x => x.BunchId).ToList());

            //List<string> NewsTitles = new List<string>();
            //List<string> NewsDescs = new List<string>();
            //List<string> NewsTags = new List<string>();
            //List<SolarNews> sNewsList = new List<SolarNews>();
            //foreach (MBunchFilter bf in mBunchFilter)
            //{
            //    SolarNews sNews = new SolarNews();
            //    List<MNews> mNews = RawNews.Where(x=>x.BunchGuid == bf.BunchId).ToList();
            //    NewsTitles = mNews.Select(x => String.IsNullOrEmpty(x.TranslatedTitle) ? x.Title : x.TranslatedTitle).ToList();
            //    NewsDescs = mNews.Select(x => String.IsNullOrEmpty(x.TranslatedDescription) ? x.Description : x.TranslatedDescription).ToList();
            //    NewsTags = mNews.SelectMany(x => x.Tags.Select(y=>y.Tag)).ToList();
            //    sNews.BunchId = bf.BunchId;
            //    //sNews.FilterId = bf.FilterId;
            //    sNews.LastUpdateDate = bf.LastUpdateDate;
            //    sNews.title = NewsTitles;
            //    sNews.Description = NewsDescs;
            //    sNews._id = bf._id;
            //    sNewsList.Add(sNews);
            //}
            //if (sNewsList != null && sNewsList.Count > 0)
            //{                
            // //   sService.AddRecords(sNewsList);
            //}

        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}