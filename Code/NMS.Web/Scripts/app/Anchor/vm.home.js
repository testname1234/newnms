﻿define('vm.anchor',
    [
        'manager',
        'datacontext',
        'control.mcrusers',
        'appdata',
        'underscore'

    ],
    function (manager, dc, Mcrusers, appdata, _) {

        var
            // Properties
            // ------------------------
            anchor = '';
        programName = ko.observable(''),
        mcrUser = new Mcrusers(),
        timelinebar = ko.observable();
        //computed Properties
        //---------------------------

        episode = function () {
            return dc.episodes.getObservableList()[0];

        },


        setTemplateValues = ko.computed({
            read: function () {
                var cEpisode = episode();
                var pName = programName();
                if (cEpisode && pName !== '') {
                    mcrUser.episode(episode());
                    mcrUser.programName(pName);
                }
            },
            deferevaluation: false
        }),


        // Methods
        // ------------------------
        count = 0,
        playpause = function () {
            if ((count % 2) == 0) {
                document.getElementById('myMarquee').stop();
                $('#myMarquee').stop();
                count = count + 1;
            }
            else {

                document.getElementById('myMarquee').start();
                count = count + 1;
            }
        };

        countforwardBackward = 0;
        forwardbackword = function () {

            if ((countforwardBackward % 2) == 0) {

                document.all.myMarquee.direction = "down";
                countforwardBackward = countforwardBackward + 1;

            }
            else {
                document.all.myMarquee.direction = "up";
                countforwardBackward = countforwardBackward + 1;
            }
        };

        var speedslow = 20;
        slow = function () {
            speedslow = speedslow - 4;
            if (speedslow < 0) {
            }
            else {
                document.getElementById('myMarquee').setAttribute('scrollamount', speedslow, 0);
            }
        }


        var speedfast = 4;
        fast = function () {
            speedfast = speedfast + 4;
            if (speedfast > 30) {

            }
            else {
                document.getElementById('myMarquee').setAttribute('scrollamount', speedfast, 0);
            }
        }

        activate = function (routeData, callback) {



            var requestObj = {
                ProgramId: 1087,
                DateStr: '2014-07-05T19:00:00.000Z'
            };

            $.when(manager.mcrusers.getEpisodeMicsCameras(152))
            .done(function (responseData) {

                $.when(manager.mcrusers.getProgramEpisodes(requestObj))
                    .done(function (responseData) {

                        var programId = responseData.Data[0].ProgramId;

                        $.when(manager.mcrusers.getProgram(programId))
                        .done(function (responseData) {
                            programName(responseData.Data.Name);
                        });
                    });

            });
        },
        canLeave = function () {
            return true;
        };

        return {
            activate: activate,
            canLeave: canLeave,
            anchor: anchor,
            mcrUser: mcrUser,
            slow: slow,
            fast: fast,
            forwardbackword: forwardbackword,
            playpause: playpause,
            timelinebar: timelinebar

        };
    });