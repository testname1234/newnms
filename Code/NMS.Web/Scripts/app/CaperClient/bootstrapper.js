﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager'
       
    ],
    function (config, binder, routeConfig, manager) {

        var
            run = function () {

               
                var views = config.views.casper;

             
                config.dataserviceInit();

                binder.bindPreLoginViews(views);

                routeConfig.register(views);

              
          amplify.subscribe(config.eventIds.onLogIn, function (data) {

                });
            };

        return {
            run: run
        };
    });
