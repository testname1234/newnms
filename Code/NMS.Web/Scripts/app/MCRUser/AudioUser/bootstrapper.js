﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager'
       
    ],
    function (config, binder, routeConfig, manager) {

        var
            run = function () {

                var views = config.views.audiouser;

          
                config.dataserviceInit();

                $.when(manager.news.loadInitialData())
                     .done(function (responseData) {
                         binder.bindPreLoginViews(views);
                     })
                     .fail(function (responseData) {
                     });


                routeConfig.register(views);

              
          amplify.subscribe(config.eventIds.onLogIn, function (data) {

                });
            };

        return {
            run: run
        };
    });
