﻿define('vm.mcrvideo',
    [
        'manager',
        'datacontext',
        'enum',
        'control.mcrusers',
        'moment',
        'config'

    ],
    function (manager, dc, e, Mcrusers, moment,config) {

        var
            // Properties
            // ------------------------
            videoSwitcherMainStory = '',
            programName = ko.observable(''),
            mcrUser = new Mcrusers(),
            newsDateTime = ko.observable(moment(new Date()).format('MMMM DD, YYYY')),
            programId = ko.observable(-1),
            currentEpisode = ko.observable(''),
            timelinebar = ko.observable(''),
            previousRoId = ko.observable(null),
            mosId   = ko.observable("VideoSwitcher"),
            mosPort = ko.observable(10541),

           //computed Properties
           //---------------------------


            // set currentEpisode and programName to control
            setPcrUserControl = ko.computed({
                read: function () {
                    var cEpisode = currentEpisode();
                    var pName = programName();
                    if (cEpisode && pName !== '') {
                        mcrUser.episode(cEpisode);
                        mcrUser.programName(pName);
                    }
                    return [];
                },
                deferevaluation: false
            }),


        // Methods
        // ------------------------

        getEpisodeByRoId = function (roId, storyId) {
            if (roId) {
                var obj = { Id: roId }
                $.when(manager.mcrusers.getProgramEpisodeByRoId(obj))
                .done(function (data) {
                    if (data.IsSuccess && data.Data) {
                        var getEpisode = dc.episodes.getLocalById(data.Data[0].EpisodeId);
                        if (getEpisode) {
                            getProgramName(getEpisode.id);
                            setEpisode(getEpisode, storyId);
                        }
                    }
                });
            }
        },

        setEpisode = function (data, sId) {
            currentEpisode(data);
            $.when(manager.mcrusers.getEpisodeMicsCameras(data.id))
            .done(function (data) {
                if (data.IsSuccess && data.Data) {
                    timelinebar(data.Data);
                    //timelinebar.valueHasMutated();
                    mcrUser.setCurrentStory(sId);
                }
            })
             .fail(function (ex) {
                 // errorMessage(true);
             });
        },

        
        getProgramName = function (id) {
            $.when(manager.mcrusers.getProgram(id))
                   .done(function (data) {
                       if (data.IsSuccess && data.Data) {
                           programName(data.Data.Name);
                       }
              });
        },
         
        saveSettings = function () {
            if (app)
            {
                if (mosId && mosPort && mosId() && mosPort()) {
                    mcrUser.saveSettings(mosId(), mosPort());
                } else { config.logger.error("enter id and port") }
            }
        },

        activate = function (routeData, callback) {
            //temp check
          //  var obj = { roID: 3, storyID: 1265 };
           // window.notificationCallBack(obj);
            //if (app) {
            //    mcrUser.activate(e.CommandType.Notify_PcrUser);
            //}
        },

        canLeave = function () {
            return true;
        };

        // callBacks From wpf---------------------
        //----------------------------------------


        window.notificationCallBack = function (d) {
            if (d && d.roID != previousRoId() || previousRoId() == null || !currentEpisode()) {
                previousRoId(d.roID);
                getEpisodeByRoId(d.roID, d.storyID)
            } else {
                mcrUser.setCurrentStory(d.storyID);
            }
        };


        return {
            activate: activate,
            canLeave: canLeave,
            videoSwitcherMainStory: videoSwitcherMainStory,
            mcrUser: mcrUser,
            e: e,
            newsDateTime: newsDateTime,
            currentEpisode: currentEpisode,
            timelinebar: timelinebar,
            mosId  :mosId, 
            mosPort: mosPort,
            saveSettings: saveSettings
        };
    });