﻿define('binder',
    [
        'jquery',
        'ko',
        'config',
        'enum',
        'presenter',
        'vm',
        'appdata',
        'manager',
        'datacontext',
        'model',
        'dataservice'
    ],
    function ($, ko, config, e, presenter, vm, appdata, manager, dc, model, dataservice) {
        var logger = config.logger;

        var
            ids = config.viewIds.production,

            bindPreLoginViews = function () {
                ko.applyBindings(vm.shell, getView(ids.shellTopNavView1));
                ko.applyBindings(vm.newsflow, getView(ids.newsFlow));
            },

            bindPostLoginViews = function () {

            },

            deleteExtraViews = function () {
            },

            bindStartUpEvents = function () {
                $(window).on('hashchange', function () {
                    appdata.currentHash = window.location.hash;
                });
                
            },

            getView = function (viewName) {
                return $(viewName).get(0);
            };

        return {
            bindPreLoginViews: bindPreLoginViews,
            bindPostLoginViews: bindPostLoginViews,
            bindStartUpEvents: bindStartUpEvents,
            deleteExtraViews: deleteExtraViews
        }
    });