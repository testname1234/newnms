﻿define('bootstrapper',
    [
        'enum',
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'vm',
        'presenter',
        'appdata',
        'helper',
        'model.user',
        'ko',
        'datacontext'
    ],
    function (e, config, binder, routeConfig, manager, timer, vm, presenter, appdata, helper, usermodel, ko, dc) {
        if ($.trim(helper.getCookie('user-id')) == '') {
            window.location.href = '/Login#/index';
        }
        var
            run = function () {
                config.dataserviceInit();
                appdata.extractUserInformation();
                binder.bindPreLoginViews();
                binder.bindStartUpEvents();
                routeConfig.register();
                presenter.toggleActivity(true);

                if ($.trim(helper.getCookie('user-id')) == '') {
                    window.location.href = '/Login#/index';
                }
                 vm.newsflow.OverLay(true);
                $.when(manager.newsfile.loadInitialData())
                    .done(function (responseData) {
                        vm.newsflow.init();
                        timer.start();
                        presenter.toggleActivity(false);
                        vm.newsflow.OverLay(false);
                    }).
                    fail(function () {
                        vm.newsflow.OverLay(true);
                    });
            };

        return {
            run: run
        }
    });