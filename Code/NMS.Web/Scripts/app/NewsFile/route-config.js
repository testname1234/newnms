﻿define('route-config',
    ['config', 'router', 'vm', 'appdata', 'enum'],
    function (config, router, vm, appdata, e) {
        var
            logger = config.logger,

            register = function () {
                var routeData;
                routeData = [

                     // Home routes
                     {
                         view: config.viewIds.production.newsFlow,
                         routes: [
                             {
                                 isDefault: true,
                                 route: config.hashes.production.newsFlow,
                                 title: 'Axact Media Solution',
                                 callback: vm.newsflow.activate,
                                 group: '.route-top'
                             }
                         ]
                     }];
               
                // Invalid routes
                routeData.push({
                    view: '',
                    route: /.*/,
                    title: '',
                    callback: function () {
                        logger.error(config.toasts.invalidRoute);
                    }
                });

                for (var i = 0; i < routeData.length; i++) {
                    router.register(routeData[i]);
                }

                // Crank up the router
                router.run();
            };

        return {
            register: register
        };
    });