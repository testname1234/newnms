﻿define('timer',
    ['manager', 'moment', 'config'],
    function (manager, moment, config) {
        var
            interval = config.pollingInterval,

            start = function () {
                setTimeout(function longPolling() {
                    $.when(manager.newsfile.newsFilePolling())
                    .done(function () {
                        setTimeout(longPolling, interval * 1000);
                    })
                    .fail(function () {
                        setTimeout(longPolling, interval * 1000);
                    });

                }, interval * 1000);


            };


        return {
            start: start
        };
    });