﻿define('vm',
    [
        'vm.shell',
        'vm.newsflow'
    ],

    function (shell,newsflow) {
        return {
            shell: shell,
            newsflow: newsflow
        };
    });