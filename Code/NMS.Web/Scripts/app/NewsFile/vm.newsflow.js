﻿define('vm.newsflow',
    [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'model.mapper',
        'model.reportnews',
        'helper'

    ],
    function (_, $, ko, dc, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, mapper, ReportNews, helper) {
        var logger = config.logger;

        var
            // Properties
            // ------------------------

            hashes = config.hashes.production,
            templates = config.templateNames,
            reportnewsView = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            reportnewsView1 = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            reportnewsView2 = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            isCreateFile = ko.observable(false),
            newFileTitle = ko.observable(''),
            newFileSlug = ko.observable(''),
            newsFileDescription = ko.observable(''),
            currentWindowSelected = ko.observable(''),
            currentSelectedMyFolder = ko.observable(12),
            currentSelectedRundownFolder = ko.observable(),
            currentEditableNewsFile = ko.observable(''),
            currentResource = ko.observable(''),
            isViewResource = ko.observable(false),
            currentNewsFile = ko.observable(),
            isCreatedStatus = ko.observable(false),
            isRundownFolderCreationVisible = ko.observable(false),
            isSec1ResourcesVisible = ko.observable(false),
            isSec2ResourcesVisible = ko.observable(false),
            viewFileDetailHistory = ko.observable(false),
            viewFileStatusHistory = ko.observable(false),
            currentRundownSelectedProgram = ko.observable(),
            currentProgramSelectedDate = ko.observable(),
            programEpisodeList = ko.observableArray([]),
            currentSelectedRundownEpisode = ko.observable(),
            superAdminUser = 1071,
            // Computed Properties
            // ------------------------

             allNewsFiles = ko.computed({
                 read: function () {
                     var arr = [];
                     var fileList = dc.newsFiles.getObservableList();
                     for (var i = 0, len = fileList.length; i < len; i++) {
                         var newsfile = fileList[i];
                         var newsFileHistoryList = dc.newsFileFolderHistory.getByNewsFileId(newsfile.id)();
                         var newsFileHistory;
                         if (newsFileHistoryList && newsFileHistoryList.length > 1) {
                             newsFileHistoryList = sortDesc(newsFileHistoryList);
                         }
                         newsFileHistory = newsFileHistoryList[0];
                         if (newsFileHistory && newsFileHistory.folderId() === e.NewsFile.AllNewsFile) {
                             arr.push(newsfile);
                         }
                     }
                     if (arr.length > 1)
                         arr = sortDesc(arr);
                     return arr;
                 },
                 deferEvaluation: true
             }),

             copyFolderList = ko.computed({
                 read: function () {
                     var arr = [];

                     var workfolderList = dc.workRoleFolders.getObservableList();
                     var folderList = dc.newsFolders.getObservableList();

                     for (var i = 0; i < workfolderList.length; i++) {
                         var workfolder = workfolderList[i];
                         for (var x = 0; x < folderList.length; x++) {
                             var folder = folderList[x];

                             if (currentNewsFile() && folder.id === workfolder.folderId && workfolder.allowCopy && folder.id !== currentNewsFile().folderId) {
                                 arr.push(folder);
                             }
                         }
                     }
                     if (typeof (currentSelectedRundownFolder()) != 'undefined' && currentSelectedRundownFolder() != null) {
                         var runDownFolder = dc.newsFolders.getLocalById(currentSelectedRundownFolder());
                         arr.push(runDownFolder);
                     }
                     return arr;
                 },
                 deferEvaluation: true
             }),
             folderAllowedStatus = ko.computed({
                 read: function () {
                     var arr = [];
                     var repoList = dc.workRoleFolders.getObservableList();
                     if (currentNewsFile()) {
                         var currentLocalObject = dc.newsFolders.getLocalById(currentNewsFile().folderId);
                         for (var i = 0; i < repoList.length; i++) {
                             if (currentLocalObject && currentLocalObject.id === repoList[i].folderId) {
                                 return repoList[i].colorList();
                             }
                         }
                     }
                     return arr;
                 },
                 deferEvaluation: true
             }),
             myFolderSelectList = ko.computed({
                 read: function () {

                     var repoList = dc.newsFolders.getObservableList();
                     repoList = _.filter(repoList, function (item) {
                         return (item.id !== e.NewsFile.AllNewsFile && !item.isRundown());
                     });

                     return repoList;
                 },
                 deferEvaluation: true
             }),
             rundownSelectList = ko.computed({
                 read: function () {

                     var repoList = dc.newsFolders.getObservableList();
                     repoList = _.filter(repoList, function (item) {
                         return (item.id !== e.NewsFile.AllNewsFile && item.isRundown());
                     });
                     if (!currentSelectedRundownFolder())
                         currentSelectedRundownFolder(repoList[0]);
                     return repoList;
                 },
                 deferEvaluation: true
             }),
             userProgramList = ko.computed({
                 read: function () {
                     var arr = dc.programs.getObservableList();
                     return arr;
                 },
                 deferEvaluation: true
             }),
             myFolderDataList = ko.computed({
                 read: function () {
                     var arr = [];

                     if (currentSelectedMyFolder()) {
                         var fileList = dc.newsFiles.getObservableList();
                         for (var i = 0, len = fileList.length; i < len; i++) {
                             var newsfile = fileList[i];
                             var newsFileHistoryList = _dc.newsFileFolderHistory.getByNewsFileId(newsfile.id)();
                             var newsFileHistory;
                             if (newsFileHistoryList && newsFileHistoryList.length > 1) {
                                 newsFileHistoryList = sortDesc(newsFileHistoryList);
                             }
                             newsFileHistory = newsFileHistoryList[0];
                             if (newsFileHistory && newsFileHistory.folderId() === currentSelectedMyFolder()) {
                                 arr.push(newsfile);
                             }
                         }
                     }
                     if (arr.length > 1)
                         arr = sortDesc(arr);

                     return arr;
                 },
                 deferEvaluation: true
             }),



             rundownDataList = ko.computed({
                 read: function () {
                     var arr = [];
                     if (currentSelectedRundownFolder()) {
                         var fileList = dc.newsFiles.getObservableList();
                         for (var i = 0, len = fileList.length; i < len; i++) {
                             var newsfile = fileList[i];
                             var newsFileHistoryList = _dc.newsFileFolderHistory.getByNewsFileId(newsfile.id)();
                             var newsFileHistory;
                             if (newsFileHistoryList && newsFileHistoryList.length > 1) {
                                 newsFileHistoryList = sortDesc(newsFileHistoryList);
                             }
                             newsFileHistory = newsFileHistoryList[0];
                             if (newsFileHistory && newsFileHistory.folderId() === currentSelectedRundownFolder()) {
                                 arr.push(newsfile);
                             }
                         }
                     }
                     if (arr.length > 1)
                         arr = sortDesc(arr);
                     return arr;
                 },
                 deferEvaluation: true
             }),
             isRundownSectionVisible = ko.computed({
                 read: function () {
                     var flag = dc.programs.getObservableList().length > 0;
                     return flag;
                 },
                 deferEvaluation: true
             }),
            // Methods
            // ------------------------
            createFile = function (data) {
                if (data == e.NewsFile.Bureau && !currentSelectedMyFolder()) {
                    config.logger.error("Please Select Folder!")
                }
                if (data == e.NewsFile.Rundown && !currentSelectedRundownFolder()) {
                    config.logger.error("Please Select Folder!")
                }
                else {
                    $(".newsFileOverlay").addClass('displayCreateFile');
                    isCreateFile(true);
                    var currentFolder;
                    if (data == e.NewsFile.Bureau && currentSelectedMyFolder()) {
                        currentFolder = _dc.newsFolders.getLocalById(currentSelectedMyFolder());
                        reportnewsView.selectedCategory({ id: currentFolder.categoryId, value: currentFolder.category() });
                        reportnewsView.selectedLocation({ id: currentFolder.locationId(), value: currentFolder.location() });
                    }
                    if (data == e.NewsFile.Bureau && currentSelectedRundownFolder()) {
                        currentFolder = _dc.newsFolders.getLocalById(currentSelectedRundownFolder());
                        reportnewsView.selectedCategory({ id: currentFolder.categoryId, value: currentFolder.category() });
                        reportnewsView.selectedLocation({ id: currentFolder.locationId(), value: currentFolder.location() });
                    }
                }
                currentWindowSelected(data);
            },

             reArrangeNewsFile = function (data) {
                 if (data && data.length > 0) {
                     var obj = null;
                     for (var i = 0; i < data.length; i++) {
                         if (obj)
                             obj = obj + "," + data[i];
                         else
                             obj = data[i];
                     }
                     var reqObj = {
                         sequenceIds: obj
                     }
                     $.when(manager.newsfile.UpdateNewsSequence(reqObj))
                   .done(function () {
                       config.logger.success('NewsFile Created Successfully !');
                   })
                   .fail(function () {
                       config.logger.success('Error Occurred');
                   });
                 }

             },

            closePopUp = function () {
                $(".newsFileOverlay").removeClass('displayCreateFile');
                isCreateFile(false);
            },
            closeResourceView = function () {
                $(".newsFileOverlay").removeClass('displayCreateFile');
                isViewResource(false);
            },
            openAttachment1 = function () {
                $('.editorUploader')[0].click();
            },
            openAttachment2 = function () {
                $('.editorUploader')[1].click();
            },
            viewSec2Resources = function () {

                isSec2ResourcesVisible(!isSec2ResourcesVisible());
            },
            viewSec1Resources = function () {
                isSec1ResourcesVisible(!isSec1ResourcesVisible());
            },
            hideSec2Resources = function () {
                isSec2ResourcesVisible(false);
            },
            hideSec1Resources = function () {
                isSec1ResourcesVisible(false);
            },
            sortDesc = function (data) {
                if (data) {
                    data = data.sort(function (entityA, entityB) {
                        return new Date(entityB.creationDate()) - new Date(entityA.creationDate());
                    });
                    return data
                }
            },
            submitNewFile = function () {
                if (newFileSlug()) {
                    var tempObj = {
                        FolderId: 9,
                        StatusId: 1,
                        Slug: newFileSlug(),
                        LocationId: parseInt([reportnewsView.selectedLocation().id]),
                        CategoryId: parseInt([reportnewsView.selectedCategory().id]),
                        Title: newFileTitle(),
                        LanguageCode: 'ur',
                        CreatedBy: appdata.currentUser().id
                    }
                    if (currentWindowSelected() == e.NewsFile.AllNewsFile)
                    { tempObj.FolderId = e.NewsFile.AllNewsFile; }

                    if (currentWindowSelected() == e.NewsFile.Bureau) {
                        tempObj.FolderId = currentSelectedMyFolder();
                    }

                    if (currentWindowSelected() == e.NewsFile.Rundown)
                        tempObj.FolderId = currentSelectedRundownFolder();

                    $.when(manager.newsfile.createNewsFile(tempObj))
                        .done(function () {
                            config.logger.success('NewsFile Created Successfully !');
                        })
                        .fail(function () {
                            config.logger.success('Error Occurred');
                        });
                    closePopUp();
                    clearData();

                }
                else {
                    config.logger.error("Please Enter Slug!");
                }
            },
            saveNewsFile = function () {
                var resource = [];
                for (var i = 0; i < reportnewsView.uploader().mappedResources().length; i++) {
                    var temp = {
                        NewsFileId: currentEditableNewsFile().id,
                        ResourceTypeId: reportnewsView.uploader().mappedResources()[i].type,
                        Guid: reportnewsView.uploader().mappedResources()[i].guid,
                        UserId: appdata.currentUser().id,
                    }
                    resource.push(temp);
                }
                var tempObj = {
                    Text: newsFileDescription(),
                    CreatedBy: appdata.currentUser().id,
                    NewsFileId: currentEditableNewsFile().id,
                    FileResource: resource,
                    StatusId: 1,
                    ReportedBy: appdata.currentUser().id,
                    NewsFolderId: currentEditableNewsFile().folderId
                }
                $.when(manager.newsfile.upadteNewsFile(tempObj))
                    .done(function () {
                        config.logger.success('NewsFile Created Successfully !');
                    })
                    .fail(function () {
                        config.logger.error('Error Occurred');
                    });
                reportnewsView.uploader().uploadedResources([]);
                reportnewsView.uploader().resources([]);
                newsFileDescription('');

            },
            clearEditorSection = function () {
                reportnewsView.uploader().uploadedResources([]);
                reportnewsView.uploader().resources([]);
                newsFileDescription('');
            },
            removeFolder = function (data) {
                currentFolders.remove(data);
            },
            publishFile = function () {
                var meta = helper.getCookie('Metas');
                var metaList = [];
                if (meta) {
                    metaList = JSON.parse(meta);
                }
                var temp = {
                    Metas: metaList,
                    NewsFileId: currentEditableNewsFile().id
                };
                $.when(manager.newsfile.publishNewsFile(temp))
                  .done(function () {
                      config.logger.success('NewsFile Published Successfully !');
                      isCreatedStatus(false);
                  })
                  .fail(function () {
                      config.logger.success('Error Occurred while publishing');
                  });
            },
            viewNewsFileHistory = function () {
                if (reportnewsView1.currentEditableNewsFile()) {
                    reportnewsView1.isNewsFileHistoryVisible(true);
                    $(".newsFileOverlay").addClass('displayCreateFile');
                    reportnewsView1.newsFileStatusHistory(_dc.newsFileStatusHistory.getByNewsFileId(reportnewsView1.currentEditableNewsFile().id)());
                    reportnewsView1.newsFolderStatusHistory(_dc.newsFileFolderHistory.getByNewsFileId(reportnewsView1.currentEditableNewsFile().id)());
                    reportnewsView1.newsFileDetailHistory(_dc.newsFileDetails.getByNewsFileId(reportnewsView1.currentEditableNewsFile().id)());
                }
            },
            closeNewsFileHistoryView = function () {
                reportnewsView1.newsFileStatusHistory([]);
                reportnewsView1.newsFolderStatusHistory();
                $(".newsFileOverlay").removeClass('displayCreateFile');
                reportnewsView1.isNewsFileHistoryVisible(false);
            },
            removeResource = function (data) {
                var arr1 = _.filter(reportnewsView1.uploader().uploadedResources(), function (upresource) {
                    return upresource.Guid === data.guid;
                });
                if (arr1 && arr1.length > 0) {
                    reportnewsView1.uploader().uploadedResources().remove(arr1[0]);
                    reportnewsView1.uploader().uploadedResources.valueHasMutated();
                    reportnewsView1.uploader().resources(reportnewsView1.uploader().uploadedResources());

                    var tempObj = {
                        guid: data.guid
                    };
                    $.when(manager.newsfile.delteNewsFileResource(tempObj))
                     .done(function () {
                         config.logger.success('File Resource Deleted Successfully !');
                     })
                     .fail(function () {
                         config.logger.success('Error Occurred');
                     });

                }



            },
            editNewsFile = function (data) {
                isCreatedStatus(false);
                var isAssigned = false;

                if (!reportnewsView1.isFull()) {
                    isAssigned = true;
                    reportnewsView1.uploader().resources([]);
                    reportnewsView1.uploader().uploadedResources([]);
                    reportnewsView1.isFull(true);
                    reportnewsView1.currentItem(data);
                    reportnewsView1.isEdited(true);
                    if (data.newsFileDetails && data.newsFileDetails().length > 0) {
                        reportnewsView1.newsFileDescription(data.newsFileDetails()[data.newsFileDetails().length - 1].text);
                    }

                    reportnewsView1.isEditedApply(false);
                    reportnewsView1.Slug(data.slug());
                    reportnewsView1.isDirty(false);
                    reportnewsView1.currentEditableNewsFile(data);
                    if (data.id && _dc.newsFileResources.getByNewsFileId(data.id)()) {
                        var res = _dc.newsFileResources.getByNewsFileId(data.id)();
                        var temp = [];
                        var resItem = mapper.resource.fromDto(res);
                        for (var i = 0; i < res.length; i++) {
                            var obj = {
                                Guid: res[i].guid,
                                ResourceTypeId: res[i].type
                            }
                            reportnewsView1.uploader().uploadedResources().push(obj);
                        }

                        reportnewsView1.uploader().uploadedResources.valueHasMutated();
                        reportnewsView1.uploader().resources(reportnewsView1.uploader().uploadedResources());

                    }
                }
                else {
                    if (!reportnewsView1.isDirty()) {
                        if (reportnewsView1.currentEditableNewsFile().id !== data.id) {
                            reportnewsView1.currentItem(data);
                            if (data.newsFileDetails && data.newsFileDetails().length > 0) {
                                reportnewsView1.newsFileDescription(data.newsFileDetails()[data.newsFileDetails().length - 1].text);
                            }
                            reportnewsView1.uploader().resources([]);
                            reportnewsView1.uploader().uploadedResources([]);
                            reportnewsView1.isEdited(true);
                            reportnewsView1.isEditedApply(false);
                            reportnewsView1.Slug(data.slug());
                            reportnewsView1.currentEditableNewsFile(data);
                            if (data.id && _dc.newsFileResources.getByNewsFileId(data.id)()) {
                                var res = _dc.newsFileResources.getByNewsFileId(data.id)();
                                var resItem = mapper.resource.fromDto(res);
                                for (var i = 0; i < res.length; i++) {
                                    var obj = {
                                        Guid: res[i].guid,
                                        ResourceTypeId: res[i].type
                                    }
                                    reportnewsView1.uploader().uploadedResources().push(obj);
                                }
                                reportnewsView1.uploader().uploadedResources.valueHasMutated();
                                reportnewsView1.uploader().resources(reportnewsView1.uploader().uploadedResources());

                            }
                        }
                        else {
                            config.logger.error('Already Selected in EditMode !');
                        }
                    }
                    else {
                        config.logger.error('Please Save or Cancel Existing File First');
                    }
                }
            },
            viewMediaArchivalResource = function () {
                $(".newsFileOverlay").addClass('displayCreateFile');
                reportnewsView1.isGetMediaArchivalData(true);
            },
            closeMediaArchivalView = function () {
                $(".newsFileOverlay").removeClass('displayCreateFile');
                reportnewsView1.isGetMediaArchivalData(false);
            },

             viewReportedMedia = function (data) {
                 debugger;

                 if (data.type == 1) {
                     reportnewsView1.isReportMediaTypeImage(true);
                     reportnewsView1.isReportMediaTypeVideo(false);
                     reportnewsView1.reportedMediaUrl(data.url());
                 }
                 else {
                     reportnewsView1.isReportMediaTypeImage(false);
                     reportnewsView1.isReportMediaTypeVideo(true);
                     reportnewsView1.reportedMediaUrl(data.url());
                 }

                 $(".newsFileOverlay").addClass('displayCreateFile');
                 reportnewsView1.isReportedMedia(true);

             },
            closeReportedMedia = function () {
                $(".newsFileOverlay").removeClass('displayCreateFile');
                reportnewsView1.isReportedMedia(false);
            },

            updateNewsFileAction = function (newsFileId, statusId, folderId) {
                manager.newsfile.updateNewsFileAction(newsFileId, statusId, folderId);
            },
            updateNewsFileStatus = function (newsFileId, statusId, createdBy) {
                if (newsFileId && statusId && createdBy)
                    manager.newsfile.updateNewsFileStatus(newsFileId, statusId, createdBy);
                else
                    config.logger.error("An error occurred!");
            },
            createRundownFolder = function () {
                if (currentSelectedRundownEpisode()) {
                    manager.newsfile.createRundownFolder(currentSelectedRundownEpisode());
                }
                else
                    config.logger.error("Please select an episode");

                closeRundownFolderCreation();
            },

            clearData = function () {
                newFileSlug('');
                newFileTitle('');
                reportnewsView.selectedCategory({});
                reportnewsView.selectedLocation({});

            },
            deleteResource = function (data) {
                console.log(data);
            },
            viewResource = function (data) {
                $(".newsFileOverlay").addClass('displayCreateFile');
                isViewResource(true);
                currentResource(data);
                console.log(data);
            },
            showFileDetailHistory = function () {
                viewFileStatusHistory(false);
                viewFileDetailHistory(true);


            },
            showFileStatusHistory = function () {
                viewFileStatusHistory(true);
                viewFileDetailHistory(false);
            },
            openRundownFolderCreation = function () {
                $(".newsFileOverlay").addClass('displayCreateFile');
                isRundownFolderCreationVisible(true);
            },
            closeRundownFolderCreation = function () {
                $(".newsFileOverlay").removeClass('displayCreateFile');
                isRundownFolderCreationVisible(false);
            },
            sendToBroadCast = function () {
                if (currentSelectedRundownFolder())
                    manager.newsfile.sendProgramToBroadcast(currentSelectedRundownFolder());
                else
                    config.logger.error("Please Select Rundown");
             },
            activate = function (routeData, callback) {
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
            },
            canLeave = function () {
                return true;
            },

            OverLay = function (isTrue) {
                if (isTrue) {
                    $('#contentviewer-overlay').show();
                    
                } else {
                    $('#contentviewer-overlay').hide();
                }
            },


            ReRender = function () {

                var elements = $(".layoutMain").find(".sliderDiv");
                var width = 0;
                for (var i = 0; i < elements.length ; i++) {
                    if (elements[i].style.display != "none") {
                        width = width + elements[i].offsetWidth;
                    }
                }
                width = $('.layoutMain').width() - width;
                var total = $('.layoutMain').width();
                var perc = width / total;
                if (perc > 0.1 && perc < 0.3)
                    width = '48%';
                else if (perc > 0.3 && perc < 0.5)
                    width = '72%';
                else if (perc > 0.5 && perc < 0.8)
                    width = '96%';
                else
                    width = '24.5%';
                $(DetailView).animate({
                    width: width,
                });
            },

            init = function () {
                reportnewsView1.subscribeEvents();
                reportnewsView2.subscribeEvents();
                currentProgramSelectedDate.subscribe(function (value) {
                    if (value) {
                        if (currentRundownSelectedProgram() && value) {
                            manager.newsfile.getProgramEpisodes(currentRundownSelectedProgram(), value, programEpisodeList);
                        }
                        else
                            config.logger.error("Please select a program");
                    }
                });


                $('.newsfilecontextmenu li').hover(function (ev) {
                    var obj = $(this).position();
                    $(this).find('ul').css({ "top": obj.top - 1, "left": obj.left + 100 })
                });

                $(".change").click(function () {
                    var obj = $(this).parents(".sliderDiv");
                    var percent = $(DetailView).width() / $('.layoutMain').width();
                    percent = percent.toFixed(2);

                    if (obj.width() == 5) {
                        obj.animate({
                            width: '24.5%',
                        });
                        $(this).css({ position: 'relative' });

                        var w = '24.5%';
                        if (percent == '0.96')
                            w = '72%';
                        else if (percent == '0.72')
                            w = '48%';
                        else if (percent == '0.48')
                            w = '24.5%';
                        $(DetailView).animate({
                            width: w,
                        });
                        this.style.backgroundColor = "transparent";

                    } else {
                        obj.animate({
                            width: '5px',
                        });
                        $(this).css({ position: 'absolute' });

                        var w = '24.5%';
                        if (percent == '0.25' || percent == '0.24')
                            w = '48%';
                        else if (percent == '0.48')
                            w = '72%';
                        else if (percent == '0.72')
                            w = '96%';

                        $(DetailView).animate({
                            width: w,
                        });
                        this.style.backgroundColor = "gray";
                    }

                });
                currentSelectedMyFolder(12);

                setTimeout(function () {
                    if (!isRundownSectionVisible()) {
                        ReRender();
                    }
                }, 1000)
            };
        function receivedParentNews(event) {
            if (event.data) {
                var item = JSON.parse(event.data);

                var arr1 = _.filter(reportnewsView1.uploader().uploadedResources(), function (upresource) {
                    return upresource.Guid === item.guid;
                });
                if (arr1 && arr1.length > 0) {
                    reportnewsView1.uploader().uploadedResources().remove(arr1[0]);
                    reportnewsView1.uploader().uploadedResources.valueHasMutated();
                    reportnewsView1.uploader().resources(reportnewsView1.uploader().uploadedResources());
                }
                else {
                    var obj = {
                        Guid: item.guid,
                        ResourceTypeId: item.type
                    }
                    reportnewsView1.uploader().uploadedResources().push(obj);
                    reportnewsView1.uploader().uploadedResources.valueHasMutated();
                    reportnewsView1.uploader().resources(reportnewsView1.uploader().uploadedResources());
                }

            }
        };

        addEventListener("message", receivedParentNews, false);

        return {
            init: init,
            activate: activate,
            myFolderSelectList: myFolderSelectList,
            allNewsFiles: allNewsFiles,
            rundownSelectList: rundownSelectList,
            createFile: createFile,
            isCreateFile: isCreateFile,
            closePopUp: closePopUp,
            submitNewFile: submitNewFile,
            newFileTitle: newFileTitle,
            newFileSlug: newFileSlug,
            newsFileDescription: newsFileDescription,
            openAttachment1: openAttachment1,
            saveNewsFile: saveNewsFile,
            removeFolder: removeFolder,
            templates: templates,
            activate: activate,
            reportnewsView: reportnewsView,
            createFile: createFile,
            e: e,
            editNewsFile: editNewsFile,
            deleteResource: deleteResource,
            viewResource: viewResource,
            isViewResource: isViewResource,
            currentResource: currentResource,
            closeResourceView: closeResourceView,
            closeResourceView: closeResourceView,
            copyFolderList: copyFolderList,
            updateNewsFileAction: updateNewsFileAction,
            updateNewsFileStatus: updateNewsFileStatus,
            currentNewsFile: currentNewsFile,
            publishFile: publishFile,
            isCreatedStatus: isCreatedStatus,
            currentSelectedMyFolder: currentSelectedMyFolder,
            currentSelectedRundownFolder: currentSelectedRundownFolder,
            myFolderDataList: myFolderDataList,
            rundownDataList: rundownDataList,
            isRundownFolderCreationVisible: isRundownFolderCreationVisible,
            openRundownFolderCreation: openRundownFolderCreation,
            closeRundownFolderCreation: closeRundownFolderCreation,
            userProgramList: userProgramList,
            currentRundownSelectedProgram: currentRundownSelectedProgram,
            reportnewsView1: reportnewsView1,
            reportnewsView2: reportnewsView2,
            createRundownFolder: createRundownFolder,
            currentRundownSelectedProgram: currentRundownSelectedProgram,
            programEpisodeList: programEpisodeList,
            currentProgramSelectedDate: currentProgramSelectedDate,
            currentSelectedRundownEpisode: currentSelectedRundownEpisode,
            folderAllowedStatus: folderAllowedStatus,
            isRundownSectionVisible: isRundownSectionVisible,
            ReRender: ReRender,
            openAttachment2: openAttachment2,
            isSec1ResourcesVisible: isSec1ResourcesVisible,
            isSec2ResourcesVisible: isSec2ResourcesVisible,
            viewSec1Resources: viewSec1Resources,
            viewSec2Resources: viewSec2Resources,
            hideSec2Resources: hideSec2Resources,
            hideSec1Resources: hideSec1Resources,
            viewMediaArchivalResource: viewMediaArchivalResource,
            config: config,
            closeMediaArchivalView: closeMediaArchivalView,
            removeResource: removeResource,
            closeNewsFileHistoryView: closeNewsFileHistoryView,
            viewNewsFileHistory: viewNewsFileHistory,
            showFileDetailHistory: showFileDetailHistory,
            showFileStatusHistory: showFileStatusHistory,
            viewFileStatusHistory: viewFileStatusHistory,
            viewFileDetailHistory: viewFileDetailHistory,
            reArrangeNewsFile: reArrangeNewsFile,
            viewReportedMedia: viewReportedMedia,
            closeReportedMedia: closeReportedMedia,
            OverLay: OverLay
        };
    });
