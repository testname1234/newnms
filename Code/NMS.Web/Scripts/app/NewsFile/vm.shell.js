﻿define('vm.shell',
     [
         'ko',
         'config',
         'router',
         'enum',
         'datacontext',
         'model',
         'manager',
         'appdata',
         'presenter',
         'utils',
         'model.user',
         'helper',

     ],
    function (ko, config, router, e, dc, model, manager, appdata, presenter, utils, usermodel, helper) {
        var logger = config.logger;

        var
            // Properties
            //-------------------

            menuHashes = config.hashes,
            templates = config.templateNames,
            sourceFilterControl = new model.SourceFilterControl(),
            generalFilterControl = new model.GeneralFilterControl(),
            generalFilterTickerControl = new model.GeneralFilterTickerControl(),
            calendarControl = new model.CalendarControl(),
            currentTopNews = ko.observable(new model.News()),
            currentTopNotification = ko.observable(),
            isStoriesChanged = ko.observable(false),
            currentPassword = ko.observable(''),
            newPassword = ko.observable(''),
            confirmPassword = ko.observable(''),

            isPasswordChanged = ko.observable(false),
            setTimeOutArrayNewsAlerts = [],
            setTimeOutArrayNotificationsAlerts = [],

            // Computed
            //-------------------

            // Methods
            //-------------------
            logOut = function () {
                window.location.href = '/login#/index';
                document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.locationId + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.newsTypeId + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.UserName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = "Password" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = "EncriptedName" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            },
             currentHash = ko.computed({
                 read: function () {
                     return router.currentHash();
                 },
                 deferEvaluation: true
             }),
            activate = function (routeData) {
            },
            init = function () {
            };

        return {
            currentHash: currentHash,
            activate: activate,
            menuHashes: menuHashes,
            templates: templates,
            init: init,
            appdata: appdata,
            presenter: presenter,
            logOut: logOut,
            config: config,
            router: router,
            e: e
        };
    });