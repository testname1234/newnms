﻿define('binder',
    [
        'jquery',
        'ko',
        'config',
        'enum',
        'presenter',
        'vm',
        'appdata',
        'manager',
        'datacontext',
        'model',
        'dataservice'
    ],
    function ($, ko, config, e, presenter, vm, appdata, manager, dc, model, dataservice) {
        var logger = config.logger;

        var
            ids = config.viewIds.production,

            bindPreLoginViews = function () {
                ko.applyBindings(vm.shell, getView(ids.shellTopNavView));
                ko.applyBindings(vm.contentviewer, getView(ids.contentViewer));

                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.FReporter) {
                    
                    ko.applyBindings(vm.shell, getView(ids.shellLeftView));
                    ko.applyBindings(vm.shell, getView(ids.shellRightView));
                    ko.applyBindings(vm.home, getView(ids.homeView));
                    ko.applyBindings(vm.home, getView(ids.filterPreference));
                    
                    //ko.applyBindings(vm.topicselection, getView(ids.topicSelectionView));
                    ko.applyBindings(vm.sequence, getView(ids.sequenceView));
                    ko.applyBindings(vm.production, getView(ids.programPreview));
                    ko.applyBindings(vm.report, getView(ids.reportView));
                    ko.applyBindings(vm.production, getView(ids.productionView));
                    ko.applyBindings(vm.home, getView(config.views.fieldreporter.reportNews.view));
                    ko.applyBindings(vm.home, getView(ids.XMLParserView));
                    ko.applyBindings(vm.topicselection, getView(ids.producerArchivalMedia));
                    //ko.applyBindings(vm.channelTickers, getView(ids.channelTickers));
                }
                else if (appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    ko.applyBindings(vm.shell, getView(ids.shellLeftView));
                    ko.applyBindings(vm.shell, getView(ids.shellRightView));
                    ko.applyBindings(vm.home, getView(ids.homeView));
                   // ko.applyBindings(vm.topicselection, getView(ids.topicSelectionView));
                    ko.applyBindings(vm.sequence, getView(ids.sequenceView));
                    ko.applyBindings(vm.production, getView(ids.programPreview));
                    ko.applyBindings(vm.headline, getView(ids.headlineView));
                    ko.applyBindings(vm.headline, getView(ids.myheadlineView));
                    ko.applyBindings(vm.report, getView(ids.reportView));
                    ko.applyBindings(vm.production, getView(ids.productionView));
                    ko.applyBindings(vm.home, getView(config.views.fieldreporter.reportNews.view));
                    //ko.applyBindings(vm.ticker, getView(ids.bureauTicker));

                }
                else if (appdata.currentUser().userType === e.UserType.TickerWriter) {
                    ko.applyBindings(vm.shell, getView(ids.shellLeftView));
                    ko.applyBindings(vm.home, getView(ids.homeView));
                    ko.applyBindings(vm.ticker, getView(ids.myTickerView));
                    ko.applyBindings(vm.shell, getView(ids.tickershellLeft));
                }
                else if (appdata.currentUser().userType === e.UserType.TickerProducer) {
                    ko.applyBindings(vm.shell, getView(ids.shellLeftView));
                    ko.applyBindings(vm.home, getView(ids.homeView));
                    ko.applyBindings(vm.home, getView(config.views.fieldreporter.reportNews.view));
                   // ko.applyBindings(vm.channelTickers, getView(ids.channelTickers));
                    ko.applyBindings(vm.tickerrundown, getView(ids.breakingTicker));
                    ko.applyBindings(vm.tickerrundown, getView(ids.alertTicker));
                    ko.applyBindings(vm.tickerrundown, getView(ids.categoryTicker));
                   // ko.applyBindings(vm.topicselection, getView(ids.topicSelectionView));

                }
                else if (appdata.currentUser().userType === e.UserType.StoryWriter || appdata.currentUser().userType === e.UserType.NLE) {
                    ko.applyBindings(vm.shell, getView(ids.shellLeftView));
                    ko.applyBindings(vm.home, getView(ids.pendingStoriesView));
                    ko.applyBindings(vm.production, getView(ids.productionView));
                    if (appdata.currentUser().userType === e.UserType.NLE)
                    {
                        ko.applyBindings(vm.uploadmedia, getView(ids.uploadMediaView));
                    }
                   
                }
                else if (appdata.currentUser().userType === e.UserType.TickerReporter) {
                    ko.applyBindings(vm.tickerreporter, getView(ids.tickerReporter));
                }
            },

            bindPostLoginViews = function () {
            },

            deleteExtraViews = function () {
            },

            bindStartUpEvents = function () {
                //try {
                //    window.onerror = function (msg, url, linenumber) {
                //        var req = {
                //            Msg: msg,
                //            Url: url,
                //            Linenumber: linenumber,
                //            User: appdata.currentUser().id.toString()
                //        }

                //        $.when(dataservice.jsErrorLogging(req))
                //         .done(function (responseData) {
                //             console.log(" ------Error Log Start------------");
                //             console.log("Error message: " + msg);
                //             console.log("URL: " + url);
                //             console.log("Line Number: " + linenumber);
                //             console.log("Application UserId: " + appdata.currentUser().id);
                //             console.log(" -------Error Log End-----------");
                //         })
                //         .fail(function (responseData) {
                //         });
                //        return true
                //    }
                //}
                //catch (ex) {
                //    console.log("Exception from error logging");
                //}

                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    appdata.currentProgram.subscribe(function () {
                        if (!appdata.currentProgram().isNullo) {
                          
                            appdata.currentEpisode(model.Episode.Nullo);
                            manager.production.getProgramEpisodes();
                            manager.production.getProgramScreenFlow();
                         
                        }
                    });
                    appdata.programSelectedDate.subscribe(function () {
                        appdata.currentEpisode(model.Episode.Nullo);
                        manager.production.getProgramEpisodes();
                        vm.sequence.greenNewsBucket([]);
                    });

                    window.uploadCompleted = function (imageGuid, storyTemplateScreenElementId, userId, args, duration) {
                        if (args && appdata.currentUser().id === userId) {
                            if (args.Elements) {
                                for (var i = 0; i < args.Elements.length; i++) {
                                    if (args.Elements[i].SlotTemplateScreenElementId == storyTemplateScreenElementId) {
                                        args.Elements[i].ImageGuid = imageGuid;
                                    }
                                }
                            }

                            var requestObj = {
                                ScreenTemplateId: args.ScreenTemplateId,
                                ThumbGuid: args.ThumbGuid,
                                BackgroundImageUrl: args.BackgroundImageUrl,
                                Elements: args.Elements
                            };

                            appdata._tempWpfArgs = args;

                            $.when(manager.production.generateScreenTemplate(requestObj))
                            .done(function (responseObj) {
                                var story = _.find(dc.stories.getByEpisodeId(appdata._tempWpfArgs.EpisodeId)(), function (storyObj) {
                                    return storyObj.storyId === appdata._tempWpfArgs.StoryId;
                                });

                                if (story) {
                                    var template = _.find(story.screenFlow().screenTemplates(), function (templateObj) {
                                        return templateObj.storyScreenTemplateId === appdata._tempWpfArgs.StoryScreenTemplateId;
                                    });

                                    if (template) {
                                        template.templateDisplayImage(responseObj.Data);
                                        for (var i = 0; i < template.templateElements().length; i++) {
                                            if (template.templateElements()[i].id == storyTemplateScreenElementId) {
                                                template.templateElements()[i].imageGuid = imageGuid;

                                                var tempResource = new model.Resource();

                                                tempResource.id = imageGuid;
                                                tempResource.guid = imageGuid;
                                                tempResource.type = e.ContentType.Video;
                                                tempResource.duration = duration;
                                                tempResource.creationDate = moment().toISOString();

                                                template.contentBucket.push(tempResource);
                                            }
                                        }
                                    }
                                    $('.videocutterOverlay').hide();
                                }
                            })
                            .fail(function (responseObj) {
                                logger.error(responseObj.Errors);
                            });
                        }
                    }

                    window.hideCutterLoader = function (bool) {
                        if (bool) {
                            appdata.showCutterImageLoader(false);
                        }
                    }
                }

                $(window).on('hashchange', function () {
                    appdata.currentHash = window.location.hash;
                });
            },

            getView = function (viewName) {
                return $(viewName).get(0);
            };

        return {
            bindPreLoginViews: bindPreLoginViews,
            bindPostLoginViews: bindPostLoginViews,
            bindStartUpEvents: bindStartUpEvents,
            deleteExtraViews: deleteExtraViews
        }
    });