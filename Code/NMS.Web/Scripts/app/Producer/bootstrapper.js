﻿define('bootstrapper',
    [
        'enum',
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'vm',
        'presenter',
        'appdata',
        'helper',
        'model.user',
        'ko',
        'datacontext'
    ],
    function (e, config, binder, routeConfig, manager, timer, vm, presenter, appdata, helper, usermodel, ko, dc) {
        if ($.trim(helper.getCookie('user-id')) == '') {
            window.location.href = '/Login#/index';
        }
        var
            run = function () {
                config.dataserviceInit();
                appdata.extractUserInformation();
                binder.bindPreLoginViews();
                binder.bindStartUpEvents();
                routeConfig.register();
                presenter.toggleActivity(true);

                if ($.trim(helper.getCookie('user-id')) == '') {
                    window.location.href = '/Login#/index';
                }

                if (appdata.currentUser().userType === e.UserType.NLE || appdata.currentUser().userType === e.UserType.StoryWriter) {
                    $.when(manager.production.loadInitialDataProductionTeam())
                            .done(function (responseData) {
                                binder.bindPostLoginViews();
                                vm.home.init();
                                vm.shell.init();
                                vm.production.init();
                                timer.startProductionTeam();

                                setTimeout(function () {
                                    presenter.toggleActivity(false);
                                }, 100);
                            })
                            .fail(function (responseData) {
                            });
                }
                else {
                    $.when(manager.news.loadInitialData())
                     .done(function (responseData) {
                         binder.bindPostLoginViews();

                         vm.shell.init();
                         vm.home.init();
                 
                         manager.production.getCelebrityByTerm();
                        
                         switch (appdata.currentUser().userType) {
                             case e.UserType.Producer:
                                 vm.production.init();
                                 vm.topicselection.init();
                                 manager.news.getAllFilters();
                                 manager.production.getMoreHeadlines();
                                 vm.topicselection.getResourceByDate();
                                 break;
                             case e.UserType.FReporter:
                                 vm.production.init();
                                 vm.topicselection.init();
                                 manager.news.getAllFilters();
                                 manager.production.getMoreHeadlines();
                                 break;
                             case e.UserType.HeadlineProducer:
                                 vm.headline.init();
                                 vm.production.init();
                                 vm.topicselection.init();
                                 manager.production.getMoreHeadlines();
                                 break;
                             case e.UserType.TickerWriter:
                                 vm.ticker.init();
                                 manager.ticker.getMoreTickers();
                                 vm.topicselection.init();
                                 break;
                             case e.UserType.TickerProducer:

                                 vm.shell.changeTickerViewId(window.location.hash);
                                 vm.tickerrundown.init();
                                 vm.ticker.init();

                                 setTimeout(function () {
                                     presenter.toggleActivity(false);
                                 }, 100);

                                 break;
                         }

                         timer.start();

                         //setTimeout(function () {
                         //    presenter.toggleActivity(false);
                         //}, 100);
                     })
                     .fail(function (responseData) {
                     });
                }

                $.when(manager.usermanagement.getUsersByWorkRoleId(e.UserType.CameraMan))
                           .done(function (responseData) {
                               dc.users.fillData(responseData.Data);
                               appdata.isCameramanFill(true);
                           })
                           .fail(function () {
                               config.logger.error("No Data Fetch");
                           });
                $.when(manager.usermanagement.getUsersByWorkRoleId(e.UserType.Producer))
                         .done(function (responseData) {
                             dc.users.fillData(responseData.Data);
                         })
                         .fail(function () {
                             config.logger.error("No Data Fetch");
                         });
                $.when(manager.usermanagement.getUsersByWorkRoleId(e.UserType.FReporter))
                     .done(function (responseData) {
                         dc.users.fillData(responseData.Data);
                     })
                     .fail(function () {
                         config.logger.error("No Data Fetch");
                     });
            };

        return {
            run: run
        }
    });