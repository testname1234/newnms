﻿define('route-config',
    ['config', 'router', 'vm', 'appdata', 'enum'],
    function (config, router, vm, appdata, e) {
        var
            logger = config.logger,

            register = function () {
                var routeData;
                
                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    routeData = [

                      // Home routes
                      {
                          view: config.viewIds.production.homeView,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.production.home,
                                  title: 'Axact Media Solution',
                                  callback: vm.home.activate,
                                  group: '.route-top'
                              }
                          ]
                      },
                      {
                          view: config.viewIds.production.newsFlow,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.production.newsFlow,
                                  title: 'Axact Media Solution',
                                  callback: vm.newsflow.activate,
                                  group: '.route-top'
                              }
                          ]
                      },
                      // channel Ticker
                      {
                          view: config.viewIds.production.channelTickers,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.production.channelTicker,
                                  title: 'Axact Media Solution',
                                  callback: vm.channelTickers.activate,
                                  group: '.route-top'
                              }
                          ]
                      },
                           // Home routes
                      {
                          view: config.viewIds.production.XMLParserView,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.production.XMLParser,
                                  title: 'Axact Media Solution',
                                  callback: vm.home.activate,
                                  group: '.route-top'
                              }
                          ]
                      },

                      //Filter route
                       {
                           view: config.viewIds.production.filterPreference,
                           routes: [
                               {
                                   isDefault: true,
                                   route: config.hashes.production.filterPreference,
                                   title: 'Axact Media Solution',
                                   callback: vm.home.activate,
                                   group: '.route-top'
                               }
                           ]
                       },

                      // Top Selection routes
                      {
                          view: config.viewIds.production.topicSelectionView,
                          routes:
                              [{
                                  route: config.hashes.production.topicSelection,
                                  title: 'Axact Media Solution',
                                  callback: vm.topicselection.activate,
                                  group: '.route-top'
                              }]
                      },

                      // Sequence routes
                      {
                          view: config.viewIds.production.sequenceView,
                          routes:
                              [{
                                  route: config.hashes.production.sequence,
                                  title: 'Axact Media Solution',
                                  callback: vm.sequence.activate,
                                  group: '.route-top'
                              }]
                      },
                      

                      // Reporter routes
                      {
                          view: config.views.fieldreporter.reportNews.view,
                          routes:
                              [{
                                  route: config.views.fieldreporter.reportNews.url,
                                  title: 'Axact Media Solution',
                                  callback: vm.home.activate,
                                  group: '.route-top'
                              }]
                      },
                        
                          // Sequence routes
                      
                        {
                            view: config.viewIds.production.producerArchivalMedia,
                            routes:
                                [{
                                    route: config.hashes.production.producerArchivalMedia,
                                    title: 'Axact Media Solution',
                                    callback: vm.topicselection.activate,
                                    group: '.route-top'
                                }]
                        }
                  
                    ];

                    if (appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                        // MyHeadline routes
                        routeData.push({
                            view: config.viewIds.production.myheadlineView,
                            routes:
                                [{
                                    route: config.hashes.production.myheadline,
                                    title: 'Axact Media Solution',
                                    callback: vm.headline.activate,
                                    group: '.route-top'
                                }]
                        });
                        // HeadlineUpdates routes
                        routeData.push({
                            view: config.viewIds.production.headlineView,
                            routes:
                                [{
                                    route: config.hashes.production.headlineUpdates,
                                    title: 'Axact Media Solution',
                                    callback: vm.headline.activate,
                                    group: '.route-top'
                                }]
                        });
                        routeData.push({
                            view: config.viewIds.production.bureauTicker,
                            routes:
                                [{
                                    route: config.hashes.production.bureauTicker,
                                    title: 'Axact Media Solution',
                                    callback: vm.headline.activate,
                                    group: '.route-top'
                                }]
                        });
                    }
                } else if (appdata.currentUser().userType === e.UserType.TickerProducer) {
                    routeData = [

                        // Home routes
                            {
                                view: config.viewIds.production.homeView,
                                routes: [
                                    {
                                        isDefault: true,
                                        route: config.hashes.production.home,
                                        title: 'Axact Media Solution',
                                        callback: vm.home.activate,
                                        group: '.route-top'
                                    }
                                ]
                            },
                            {
                                  view: config.viewIds.production.channelTickers,
                                  routes: [
                                      {
                                          isDefault: true,
                                          route: config.hashes.production.channelTicker,
                                          title: 'Axact Media Solution',
                                          callback: vm.channelTickers.activate,
                                          group: '.route-top'
                                      }
                                  ]
                            },
                            {
                                view: config.viewIds.production.alertTicker,
                                routes:
                                    [{
                                        route: config.hashes.production.alertTicker,
                                        title: 'Axact Media Solution',
                                        callback: vm.tickerrundown.activate,
                                        group: '.route-top'
                                    }]
                            },
                            {
                                view: config.viewIds.production.breakingTicker,
                                routes:
                                    [{
                                        route: config.hashes.production.breakingTicker,
                                        title: 'Axact Media Solution',
                                        callback: vm.tickerrundown.activate,
                                        group: '.route-top'
                                    }]
                            },
                            {
                                view: config.viewIds.production.categoryTicker,
                                routes:
                                    [{
                                        route: config.hashes.production.categoryTicker,
                                        title: 'Axact Media Solution',
                                        callback: vm.tickerrundown.activate,
                                        group: '.route-top'
                                    }]
                            },
                            // Top Selection routes
                            {
                                view: config.viewIds.production.topicSelectionView,
                                routes:
                                    [{
                                        route: config.hashes.production.topicSelection,
                                        title: 'Axact Media Solution',
                                        callback: vm.topicselection.activate,
                                        group: '.route-top'
                                    }]
                            },
                                // Reporter routes
                            {
                                view: config.views.fieldreporter.reportNews.view,
                                routes:
                                    [{
                                        route: config.views.fieldreporter.reportNews.url,
                                        title: 'Axact Media Solution',
                                        callback: vm.home.activate,
                                        group: '.route-top'
                                    }]
                            }
                    ];
                    
                }
                else if (appdata.currentUser().userType === e.UserType.TickerReporter) {
                    routeData = [
                       // Pending Stories routes
                       {
                           view: config.viewIds.production.tickerReporter,
                           routes: [
                               {
                                   isDefault: true,
                                   route: config.hashes.production.tickerReporter,
                                   title: 'Axact Media Solution',
                                   callback: vm.tickerreporter.activate,
                                   group: '.route-top'
                               }
                           ]
                       },
                    ];
                }
                else {
                    routeData = [
                        // Pending Stories routes
                        {
                            view: config.viewIds.production.pendingStoriesView,
                            routes: [
                                {
                                    isDefault: true,
                                    route: config.hashes.production.pendingStories,
                                    title: 'Axact Media Solution',
                                    callback: vm.home.activate,
                                    group: '.route-top'
                                }
                            ]
                        },
                        // Upload Media routes
                        {
                            view: config.viewIds.production.uploadMediaView,
                            routes: [
                                {
                                    isDefault: true,
                                    route: config.hashes.production.uploadMedia,
                                    title: 'Axact Media Solution',
                                    callback: vm.uploadmedia.activate,
                                    group: '.route-top'
                                }
                            ]
                        },
                    ];
                }

                // Production routes
                routeData.push({
                    view: config.viewIds.production.productionView,
                    routes:
                    [{
                        route: config.hashes.production.production,
                        title: 'Axact Media Solution',
                        callback: vm.production.activate,
                        group: '.route-top'
                    }]
                });
           
                // Report routes
                routeData.push({
                    view: config.viewIds.production.reportView,
                    routes:
                    [{
                        route: config.hashes.production.report,
                        title: 'Axact Media Solution',
                        callback: vm.report.activate,
                        group: '.route-top'
                    }]
                });

                // Invalid routes
                routeData.push({
                    view: '',
                    route: /.*/,
                    title: '',
                    callback: function () {
                        logger.error(config.toasts.invalidRoute);
                    }
                });

                for (var i = 0; i < routeData.length; i++) {
                    router.register(routeData[i]);
                }

                // Crank up the router
                router.run();
            };

        return {
            register: register
        };
    });