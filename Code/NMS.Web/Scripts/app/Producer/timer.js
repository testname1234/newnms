﻿define('timer',
    ['manager', 'moment', 'config'],
    function (manager, moment, config) {
        var
            interval = config.pollingInterval,

            start = function () {
                //setTimeout(function longPolling() {
                //    $.when(manager.news.producerPolling())
                //    .done(function () {
                //        setTimeout(longPolling, interval * 1000);
                //    })
                //    .fail(function () {
                //        setTimeout(longPolling, interval * 1000);
                //    });

                //}, interval * 1000);

            
            },

            startTicker = function () {
                //setTimeout(function longPollingTicker() {
                //    $.when(manager.ticker.tickerPolling())
                //    .done(function () {
                //        setTimeout(longPollingTicker, interval * 1000);
                //    })
                //    .fail(function () {
                //        setTimeout(longPollingTicker, interval * 1000);
                //    });

                //}, interval * 1000);
            },

            startProductionTeam = function () {
                //setTimeout(function longPollingProductionTeam() {
                //    $.when(manager.production.productionTeamPolling())
                //    .done(function () {
                //        setTimeout(longPollingProductionTeam, interval * 1000);
                //    })
                //    .fail(function () {
                //        setTimeout(longPollingProductionTeam, interval * 1000);
                //    });

                //}, interval * 1000);
            };

        return {
            start: start,
            startProductionTeam: startProductionTeam,
            startTicker: startTicker
        };
    });