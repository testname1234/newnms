﻿define('vm.channeltickers',
    ['config', 'appdata', 'datacontext', 'underscore', 'ko'],
    function (config, appdata, dc, _, ko) {

        var
            // Properties
            // ------------------------
            listOfImages = ko.observableArray([]),

            // Computed Properties
            // ------------------------
             TickerImages = ko.computed({
                 read: function () {
                     appdata.isTickerImageExist();
                     var tempArray = _.unique(dc.tickerImage.getObservableList(), false, function (item, k, v) {
                         return item.channelId;
                     });
                     var tempArr = [];
                     for (var i = 0; i < tempArray.length; i++) {
                         var arr = _.filter(dc.tickerImage.getObservableList(), function (tickerImageItem) {
                             return tickerImageItem.channelId === tempArray[i].channelId;
                         });
                         var tempObj = {
                             channelId: tempArray[i].channelId,
                             tickerImages: ko.observableArray(arr)
                         }
                         tempArr.push(tempObj)
                     }
                     if (listOfImages && listOfImages().length > 0) {
                         for (var j = 0; j < appdata.tickerImagesList().length; j++) {
                             var isExistingChannel = false;
                             for (var k = 0; k < listOfImages().length; k++) {

                                 if (listOfImages()[k].channelId == appdata.tickerImagesList()[j].channelId) {
                                     listOfImages()[k].tickerImages.push(appdata.tickerImagesList()[j]);
                                     listOfImages()[k].tickerImages.valueHasMutated();
                                     listOfImages.valueHasMutated();
                                     isExistingChannel = true;
                                 }
                                 listOfImages()[k].tickerImages(_.unique(listOfImages()[k].tickerImages()));
                                 if (listOfImages()[k].tickerImages().length > 20) {
                                     var maxDiff = listOfImages()[k].tickerImages().length - 20;
                                     for (var n = 0; n < maxDiff; n++) {
                                         listOfImages()[k].tickerImages.remove(listOfImages()[k].tickerImages()[n]);
                                         listOfImages()[k].tickerImages.valueHasMutated();
                                         listOfImages.valueHasMutated();
                                     }
                                 }
                                 listOfImages()[k].tickerImages(_.unique(listOfImages()[k].tickerImages()));
                             }
                             if (!isExistingChannel) {
                                 var arr = [];
                                 arr.push(appdata.tickerImagesList()[j]);
                                 var tempObj = {
                                     channelId: appdata.tickerImagesList()[j].channelId,
                                     tickerImages: ko.observableArray(arr)
                                 }
                                 listOfImages.push(tempObj);
                                 listOfImages.valueHasMutated();
                             }
                         }
                     }
                     else {
                         listOfImages(tempArr);
                     }
                     return tempArray;
                 },
                 deferEvaluation: false
             }),

            // Methods
            // ------------------------

            activate = function (routeData, callback) {
            },
            canLeave = function () {
                return true;
            };

        return {
            activate: activate,
            canLeave: canLeave,
            TickerImages: TickerImages,
            listOfImages: listOfImages
        };
    });