﻿define('vm.contentviewer',
	  [
          'ko',
          'enum',
          'model',
          'datacontext',
	      'appdata',
          'manager',
          'router',
          'config',
          'model.mapper',
          'helper'
	  ],
	  function (ko, e, model, dc, appdata, manager, router, config, mapper, helper) {
	      var

            // Properties
    		// ------------------------

            contentList = ko.observableArray([]),
            currentIndex = ko.observable(0),
            hashes = config.hashes.production,
            isVisible = ko.observable(false),
            twc = new model.TickerWriterControl(),
            tempTickerList = ko.observableArray([]),
            currentRepeatedCount = ko.observable(1),
            tickerLangCode = ko.observable('ur'),
            lasttickerCreated = ko.observable(''),
            mediaSelectionUrl=ko.observable(''),
            templates = config.templateNames,
            allowtickerTosave = ko.observable(false),
            tickersCount = ko.observable(0),
            tempListOfGoogleResource = ko.observableArray([]),
            currentPassword = ko.observable(''),
            newPassword = ko.observable(''),
            confirmPassword = ko.observable(''),
            isPasswordChanged = ko.observable(false),
            isCreateSegmentVisible = ko.observable(false),
            isTwitterVisible = ko.observable(false),
            isCreateNewsVisible = ko.observable(false),
            twitterMessage = ko.observable(''),
            isMediaViewVisibleExplorer = ko.observable(false),
            newTickerGroupName = ko.observable(''),
            editTickerSegmentId = ko.observable(0),

	      // Computed Properties
	      // ------------------------

           currentContent = ko.computed({
              read: function () {

                  if (contentList()[currentIndex()]) {
                      $.when(manager.production.searchResourceMetaByGuid(contentList()[currentIndex()]))
                      .done(function (responseObj) {

                      })
                      .fail(function (responseObj) {
                      });
                      return contentList()[currentIndex()];
                  }

              },
              deferEvaluation: true
          }),
          hasPrevious = ko.computed({
              read: function () {
                  return currentIndex() > 0;
              },
              deferEvaluation: true
          }),
          hasNext = ko.computed({
              read: function () {
                  return currentIndex() < contentList().length - 1;
              },
              deferEvaluation: true
          }),

          tweetLength = ko.computed({
              read: function () {
                  var t = twitterMessage();
                  return t.length + " out of 140 character(s)";
              },
              defferEvaluation: true
              }),

	      // Methods
	      // ------------------------

        toggleVisibility = function () {
            isVisible(!isVisible());
            if (!isVisible()) {
                contentList([]);
                currentIndex(null);
            }
        },



        setCurrentContent = function (resources, currentResource) {
            if (!isMediaViewVisibleExplorer()) {
                if (resources && resources.length > 0 && currentResource) {
                    if (typeof (app) != 'undefined' && app.OpenVideo && currentResource.type == e.ContentType.Video) {
                        app.OpenVideo(currentResource.url());
                    } else {
                        contentList(resources);
                        currentIndex(0);
                        for (var i = 0; i < resources.length; i++) {
                            if (resources[i].id === currentResource.id) {
                                currentIndex(i);
                                break;
                            }
                        }
                        toggleVisibility();
                    }
                }
            }
        },
        addCurrentResource = function (data) {
            var arr = _.filter(tempListOfGoogleResource(), function (item) {
                if (item.id == data.guid) {
                    return item;
                }
            });
            if (arr && arr.length > 0) {
                appdata.selectPopUpResource(arr[0]);
            }
            else {
                appdata.selectPopUpResource(data);
            }
            toggleVisibility();
        },
        deleteFavouriteResource = function (data) {
            if (data) {
                appdata.unmarkSelectedResource(data);
            }
        },
        sendTwitterMessage = function () {
            console.log(twitterMessage());
            var reqObj = {
                Text: twitterMessage(),
                TickerType:1
            };
	      $.when(manager.ticker.sendToTwitter(reqObj))
                        .done(function (responseObj) {
                            if (responseObj.IsSuccess) {
                                isTwitterVisible(false);
                            }
                         
                        })
                       .fail(function (responseObj) {
                         
                       });

        },
        markResourceFavourite = function (data) {
            if (data) {
                appdata.selectedFavouriteResource(data);
            }

        },
        closeMediaView = function () {
            $("#content-viewer-main").children('#contentviewer-overlay').removeClass('warningOverlay');
            isMediaViewVisibleExplorer(false);
        },
        setCurrentContentOfGoogle = function (resources, currentResource, isVideo) {
            if (!isMediaViewVisibleExplorer()) {
                if (resources && resources.length > 0 && currentResource) {
                    tempListOfGoogleResource(resources);
                    var tempResource = [];
                    if (isVideo) {
                        for (var j = 0; j < resources.length; j++) {
                            var tempObj = {
                                ResourceGuid: resources[j].id,
                                Slug: resources[j].title,
                                IsUploadedFromNle: false,
                                DirectUlr: resources[j].url,
                                DirectThumbUrl: resources[j].tbUrl,
                                ResourceTypeId: 2,
                                IsGoogleResource: true,
                                Duration: resources[j].duration,
                            };
                            var item = mapper.resource.fromDto(tempObj);
                            tempResource.push(item);
                        }
                        contentList(tempResource);
                        currentIndex(0);
                        for (var i = 0; i < tempResource.length; i++) {
                            if (tempResource[i].id === currentResource.id) {
                                currentIndex(i);
                                break;
                            }
                        }
                    }
                    else {
                        for (var j = 0; j < resources.length; j++) {
                            var tempObj = {
                                ResourceGuid: resources[j].imageId,
                                Slug: resources[j].title,
                                IsUploadedFromNle: false,
                                DirectUlr: resources[j].unescapedUrl,
                                DirectThumbUrl: resources[j].tbUrl,
                                ResourceTypeId: 1,
                                IsGoogleResource: true
                            };
                            var item = mapper.resource.fromDto(tempObj);
                            tempResource.push(item);
                        }
                        contentList(tempResource);
                        currentIndex(0);
                        for (var i = 0; i < tempResource.length; i++) {
                            if (tempResource[i].id === currentResource.imageId) {
                                currentIndex(i);
                                break;
                            }
                        }
                    }

                    toggleVisibility();
                }
            }
        },
        setCurrentAlertContent = function (data, type) {
            if (data) {
                if (typeof (app) != 'undefined' && app.OpenVideo && data && type == e.ContentType.Video) {
                    app.OpenVideo(data.url);
                } else {
                    currentIndex(0);
                    contentList([{ type: type, url: data.url }]);
                    toggleVisibility();
                }
            }
        },
        displayPrevious = function () {
            if (currentIndex() > 0)
                currentIndex(currentIndex() - 1);
        },
        displayNext = function () {
            if (currentIndex() < contentList().length - 1)
                currentIndex(currentIndex() + 1);
        },

        closedCreateNewsPopup = function () {
            isCreateNewsVisible(false);
            $("#content-viewer-main").children('#contentviewer-overlay').removeClass('warningOverlay');
            $('#createNewsPopup').removeClass('isNews');
        },

	     //Ticker Writer Methods

	    setCurrentTicker = function (currentTicker) {
	        if (currentTicker) {
	            resetTickerPopup();
	            twc.currentTicker(currentTicker);
	            twc.tickerLineList([]);
	            twc.manager = manager;
	            twc.currentTicker().severity(e.Severity.High);

	            appdata.istickerPopupVisible(true);
	            $('#RadioUr0').click();
	            var tickerLine = new model.TickerLine();
	            twc.tickerLineList.push(tickerLine);
	            twc.isEdit(false);
	            twc.isDisabled(false);
	        }
	    },
        addOrDeleteFromTickerList = function (data, isdeleted) {
            if (isdeleted) {
                for (var i = 0; i < tempTickerList().length; i++) {
                    if (tempTickerList()[i].Text == data[0].children[1].value) {
                        tempTickerList.remove(tempTickerList()[i]);
                        tickersCount(tickersCount() - 1);
                    }
                }
            }
            else {
                if (tempTickerList().length == 0)
                    tempTickerList.push({ Text: data[0].children[1].value, LanguageCode: tickerLangCode(), SequenceId: 1 });
                else
                    tempTickerList.push({ Text: data[0].children[1].value, LanguageCode: tickerLangCode(), SequenceId: tempTickerList().length + 1 });
            }
        },
        removeFromTickerList = function (event, ui) {

        },
        setTickerSeverity = function (data) {
            twc.currentTicker().severity(data);
        },
         setTickerFrequency = function (data) {
             twc.currentTicker().frequency(data);
         },
        setRepeatedCount = function () {
            twc.currentTicker().repeatCount(parseInt(currentRepeatedCount()));
        },

        downLoadFile = function (type) {
            var url = currentContent().url();
            if (type == 1) {
                if (url.indexOf("UserId") != -1) {
                    url += "&ishd=true";
                }
                else {
                    url += "?ishd=true";
                }

                

            }
            var link = document.createElement("a");
            link.download = url;
            link.href = url;
            link.click();
        },
          changePassword = function () {
              isPasswordChanged(true);
              $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
          },
            hidePasswordChange = function () {
                isPasswordChanged(false);
                $("#content-viewer-main").children('#contentviewer-overlay').removeClass('warningOverlay');
                clearCreadentials();
            },
             clearCreadentials = function () {
                 currentPassword('');
                 newPassword('');
                 confirmPassword('');
             },

            submitPassword = function () {
                if (currentPassword() == $.trim(helper.getCookie('Password'))) {
                    var reqObj = {
                        userId: $.trim(helper.getCookie('user-id')),
                        password: newPassword()
                    }
                    if (newPassword() == confirmPassword()) {
                        $.when(manager.production.changeUserPassword(reqObj))
                        .done(function (responseObj) {
                            if (responseObj.IsSuccess) {
                                document.cookie = "Password" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                                document.cookie = "Password" + "=" + $.trim(newPassword());
                                config.logger.success("Your Password Changed Successfully!");
                                clearCreadentials();
                            }
                            hidePasswordChange();
                        })
                       .fail(function (responseObj) {
                           hidePasswordChange();
                       });
                    }
                    else {
                        config.logger.success("Your Password Changed Successfully!");
                    }
                }
                else {
                    config.logger.error('Please Provide Valid Credentials');
                }

            },
            currentTickerType = ko.computed({
                read: function () {
                    if (router.currentHash() === hashes.breakingTicker)
                        return e.TickerType.Breaking.Value;
                    if (router.currentHash() === hashes.alertTicker)
                        return e.TickerType.Latest.Value;
                    if (router.currentHash() === hashes.categoryTicker)
                        return e.TickerType.Category.Value;
                }
            }),
            createSegment = function () {
                if (!newTickerGroupName().trim()) {
                    config.logger.error("Please write a segment name!");
                    return;
                }
                if (editTickerSegmentId() == 0) {
                    
                    var arr = _.filter(dc.tickers.getAllLocal(), function (item) {
                        return item.tickerGroupName() === newTickerGroupName();
                    });
                    if (arr && arr.length > 0) {
                        config.logger.error("Segment already exist!");
                        return;
                    }
                    manager.ticker.createSegment(newTickerGroupName(), currentTickerType());
                }
                else if (editTickerSegmentId() > 0) {
                    manager.ticker.createSegment(newTickerGroupName(), currentTickerType(), editTickerSegmentId());
                    editTickerSegmentId(0);
                }
                isCreateSegmentVisible(false);
            },
            init = function () {
                isCreateSegmentVisible.subscribe(function (value) {
                    if (!value) {
                        newTickerGroupName('');
                        $("#content-viewer-main").children('#contentviewer-overlay').removeClass('warningOverlay');
                    }
                    else {
                        $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                    }

                });
                isTwitterVisible.subscribe(function (value) {
                    if (!value) {
                        $("#content-viewer-main").children('#contentviewer-overlay').removeClass('warningOverlay');
                    }
                    else {
                        $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                    }

                });
                
            },
            resetTickerPopup = function () {
                //$(".eachrow").each(function () {
                //    $(this).children('input').val('');
                //    if ($(this).hasClass('sub')) {
                //        $(this).remove()
                //    }
                //});
                //tickersCount(0);
                //$('.popups #content-viewer .innercont .news-tickr .prioritysec ul li').each(function () {
                //    if ($(this).hasClass('active')) {
                //        $(this).removeClass('active');
                //    }
                //});
                //$('.popups #content-viewer .innercont .news-tickr .prioritysec ul li:first').addClass('active');
            };

	      function receivedResource(event) {
	          if (router.currentHash() != "#/production" && router.currentHash() != "#/topicselection" && !appdata.isNewsCreated()) {
	              if ($("#iframePopup").parent && $("#iframePopup").parent().parent && $("#iframePopup").parent().parent()[0] &&  !$("#iframePopup").parent().parent()[0].hasAttribute('Class'))
	              {
	                  $("#iframePopup").remove();
	              }
	              
	              var expResource = event.data;
	              isMediaViewVisibleExplorer(true);
	              if (expResource) {
	                      if (appdata.currentResource() && appdata.currentResource().guid == expResource.guid || appdata.currentResource().Guid == expResource.Guid) {
	                          appdata.currentResource('');
	                          appdata.currentResource(expResource);
	                      }
	                      else {
	                          appdata.currentResource(expResource);
	                      }

	              }	             
	          }
	          if (document.location.href.indexOf('isiframe') > -1) {
	              window.parent.postMessage(expResource, "*");
	          }
	      }
	      addEventListener("message", receivedResource, false);

	      init();
	      return {
	          e: e,
	          currentContent: currentContent,
	          toggleVisibility: toggleVisibility,
	          setCurrentContent: setCurrentContent,
	          displayNext: displayNext,
	          displayPrevious: displayPrevious,
	          hasNext: hasNext,
	          hasPrevious: hasPrevious,
	          isVisible: isVisible,
	          setCurrentAlertContent: setCurrentAlertContent,
	          currentIndex: currentIndex,
	          contentList: contentList,
	          appdata: appdata,
	          twc: twc,
	          setCurrentTicker: setCurrentTicker,
	          addOrDeleteFromTickerList: addOrDeleteFromTickerList,
	          removeFromTickerList: removeFromTickerList,
	          insertTicker: insertTicker,
	          setTickerSeverity: setTickerSeverity,
	          setRepeatedCount: setRepeatedCount,
	          currentRepeatedCount: currentRepeatedCount,
	          tempTickerList: tempTickerList,
	          tickerLangCode: tickerLangCode,
	          config: config,
	          router: router,
	          lasttickerCreated: lasttickerCreated,
	          tickersCount: tickersCount,
	          templates: templates,
	          setTickerFrequency: setTickerFrequency,
	          setCurrentContentOfGoogle: setCurrentContentOfGoogle,
	          addCurrentResource: addCurrentResource,
	          markResourceFavourite: markResourceFavourite,
	          deleteFavouriteResource: deleteFavouriteResource,
	          downLoadFile: downLoadFile,
	          changePassword: changePassword,
	          isPasswordChanged: isPasswordChanged,
	          hidePasswordChange: hidePasswordChange,
	          submitPassword: submitPassword,
	          currentPassword: currentPassword,
	          newPassword: newPassword,
	          confirmPassword: confirmPassword,
	          isCreateSegmentVisible: isCreateSegmentVisible,
	          newTickerGroupName: newTickerGroupName,
	          createSegment: createSegment,
	          editTickerSegmentId: editTickerSegmentId,
	          isMediaViewVisibleExplorer: isMediaViewVisibleExplorer,
	          closeMediaView: closeMediaView,
	          mediaSelectionUrl: mediaSelectionUrl,
	          isCreateNewsVisible: isCreateNewsVisible,
	          closedCreateNewsPopup: closedCreateNewsPopup,
	          isTwitterVisible: isTwitterVisible,
	          twitterMessage: twitterMessage,
	          sendTwitterMessage: sendTwitterMessage,
	          tweetLength: tweetLength
	     
	      };
	  });