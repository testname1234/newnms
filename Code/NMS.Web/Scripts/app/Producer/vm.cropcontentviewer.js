﻿define('vm.cropcontentviewer',
	  ['ko', 'enum', 'model', 'datacontext', 'ControllerScript', 'vm.production'],
	  function (ko, e, model, dc, controllerScript, production) {
	      var

            // Properties
    		// ------------------------

		  	contentViewerControlCrop = new model.ContentViewerControl(),
            breakingNews = ko.observable(),
	  	    resourceIndex = ko.observable(0),
            currentArchiveResource = ko.observable(new model.Resource()),
            isArchiveResource = false,
            isCropPopup = ko.observable(0),

	        // Methods
	        // ------------------------

	        toggleContentViewer = function () {
	            if (contentViewerControlCrop.isPopUpVisible()) {
	                alert();
	                contentViewerControlCrop.isPopUpVisible(false);
	            }
	            else
	                contentViewerControlCrop.isPopUpVisible(true);
	        },
           lasso3Cropp = function () {
               controllerScript.lasso3();
           },
	       lasso2Cropp = function () {
	           controllerScript.lasso2();
	       },
	       lasso1Cropp = function () {
	           controllerScript.lasso1();
	       },
	       resetCanvasCrop = function () {
	           production.resetCanvas();
	       },
           submitCropCoordinate = function ()
           {
               production.submitCoordinate();
           }


	      return {
	          e: e,
	          contentViewerControlCrop: contentViewerControlCrop,
	          toggleContentViewer: toggleContentViewer,
	          isCropPopup: isCropPopup,
	          lasso3Cropp: lasso3Cropp,
	          lasso2Cropp: lasso2Cropp,
	          lasso1Cropp: lasso1Cropp,
	          resetCanvasCrop: resetCanvasCrop,
	          submitCropCoordinate: submitCropCoordinate

	      };
	  });