﻿define('vm.headline',
    [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'vm.topicselection',
        'vm.production',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'vm.contentviewer'

    ],
	function (_, $, ko, dc, topicSelectionVM, productionVM, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, contentviewer) {
	    var
            //#region Properties

            hashes = config.hashes.production,
            isNewsCropVisible = ko.observable(false),
            calendarControl = model.CalendarControl(),
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            pageIndex = ko.observable(1),
            PageCount = ko.observable(30),
            totalHeadlines = ko.observable(0),
            headlines = ko.observableArray([]),
            templates = config.templateNames,

            //#endregion

            //#region Computed Properties

            isUserHeadlines = ko.computed({
                read: function () {
                    return router.currentHash() === config.hashes.production.myheadline;
                },
                deferEvaluation: true
            }),
            groupedHeadlines = ko.computed({
                read: function () {
                    totalHeadlines(0);
                    var headlinesArray = [];
                    appdata.arrangeNewsFlag();
                    if (headlines && headlines().length > 0) {
                        var currentGroup = '';

                        for (var i = 0; i < headlines().length; i++) {
                            var objectMap = {};
                            var episode = headlines()[i];
                            var stories = dc.stories.getByEpisodeId(episode.id)();

                            var tempStories = [];
                            if (searchKeywords() && searchKeywords().length > 0) {
                                for (var j = 0; j < stories.length; j++) {
                                    if (stories[j].news.title().toLowerCase().search(searchKeywords().toLowerCase()) !== -1) {
                                        tempStories.push(stories[j]);
                                    }
                                }
                            }
                            else
                                tempStories = stories;

                            var groupName = moment(headlines()[i].startTime()).format("LL hh:mm a");

                            if (groupName !== currentGroup && tempStories.length > 0) {
                                currentGroup = groupName;
                                objectMap.groupName = groupName;
                                objectMap.groupElements = tempStories;
                                objectMap.isReadOnly = isUserHeadlines();
                                totalHeadlines(totalHeadlines() + tempStories.length);
                                headlinesArray.push(objectMap);
                            }
                        }
                    }

                    return headlinesArray;
                },
                deferEvaluation: true
            }),

            //#endregion

            //#region Methods

            toggleHeadlineFilter = function (data) {
                manager.production.toggleHeadlineFilter(data);
                manager.production.setDataChangeTime();
            },
            switchView = function myfunction() {
                if (router.currentHash().indexOf('home') != -1) {
                    router.navigateTo(config.hashes.production.headlineUpdates);
                }
                else if (router.currentHash().indexOf('headlineUpdates') != -1) {
                    router.navigateTo(config.hashes.production.home);
                }
                else if (router.currentHash().indexOf('myheadline') != -1) {
                    router.navigateTo(config.hashes.production.home);
                }
            },
            loadNextPage = function () {
                presenter.toggleActivity(true);
                var requestObj;
                var pIndex = pageIndex() + 1;

                appdata.lpStartIndexHeadline = pIndex;

                $.when(manager.production.getMoreHeadlines())
                .done(function () {
                    pageIndex(pIndex);
                    presenter.toggleActivity(false);
                })
                .fail(function () {
                    presenter.toggleActivity(false);
                });
            },
            toggleStoryFilter = function (data) {
                manager.production.toggleStoryFilter(data);
                pageIndex(1);
                appdata.lpStartIndexHeadline = 0;
                manager.production.setDataChangeTime();
            },
            selectNews = function (data, story) {
                manager.news.selectNews(data, false, story);
            },
            setCalendarDate = function () {
                appdata.lpHeadlineFromDate = calendarControl.fromDate();
                appdata.lpHeadlineToDate = calendarControl.toDate();
                pageIndex(0);
                appdata.lpStartIndexHeadline = pageIndex();
                manager.production.setDataChangeTime();
                manager.production.getMoreHeadlines();
            },
            initializeHeadlineGenerator = function () {
                setTimeout(function headlinepolling() {
                    $.when(manager.production.getFilteredHeadlines())
                    .done(function (data) {
                        if (data) {
                            if (data.headlines) {
                                if (isUserHeadlines()) {
                                    var arr = data.headlines;
                                    var temp = [];
                                    for (var i = 0; i < arr.length; i++) {
                                        var objectMap = {};
                                        var episode = arr[i];
                                        var stories = dc.stories.getByEpisodeId(episode.id)();
                                        stories = _.filter(stories, function (item) {
                                            return item.userId === appdata.currentUser().id;
                                        });
                                        if (stories.length > 0) {
                                            temp.push(episode);
                                        }
                                    }
                                    headlines(temp);
                                }
                                else if (!isUserHeadlines()) {
                                    headlines(data.headlines);
                                }
                            }
                            //if (data.PageIndex)
                            //    pageIndex(0);
                        }
                        setTimeout(headlinepolling, config.displayPollingInterval * 1000);
                    })
                    .fail(function () {
                        setTimeout(headlinepolling, config.displayPollingInterval * 1000);
                    });
                }, config.displayPollingInterval * 1000);
            },
            setFullScreenMode = function () {
                var el = document.body;
                var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen
                || el.mozRequestFullScreen || el.msRequestFullScreen;
                if (requestMethod) {
                    requestMethod.call(el);
                } else if (typeof window.ActiveXObject !== "undefined") {
                    var wscript = new ActiveXObject("WScript.Shell");
                    if (wscript !== null) {
                        wscript.SendKeys("{F11}");
                    }
                }
            },

            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    appdata.searchHeadlineKeywords = value;
                    appdata.lpStartIndexHeadline = 0;
                    manager.production.setDataChangeTime();
                    manager.production.getMoreHeadlines();
                });
                appdata.currentEpisode.subscribe(function (value) {
                    manager.production.setDataChangeTime();
                });
            },
            activate = function (routeData, callback) {
            },
	        init = function () {
	            calendarControl.isNewsCropVisible(true);
	            calendarControl.currentOption('lastmonth');
	            appdata.headlineCalendarControlReference(calendarControl);
	            subscribeEvents();
	            initializeHeadlineGenerator();
	        };

	    //#endregion

	    return {
	        activate: activate,
	        init: init,
	        calendarControl: calendarControl,
	        templates: templates,
	        searchKeywords: searchKeywords,
	        isNewsCropVisible: isNewsCropVisible,
	        groupedHeadlines: groupedHeadlines,
	        selectNews: selectNews,
	        switchView: switchView,
	        loadNextPage: loadNextPage,
	        appdata: appdata,
	        toggleStoryFilter: toggleStoryFilter,
	        isUserHeadlines: isUserHeadlines,
	        totalHeadlines: totalHeadlines,
	        setCalendarDate: setCalendarDate,
	        setFullScreenMode: setFullScreenMode,
	        toggleHeadlineFilter: toggleHeadlineFilter,
	        utils: utils,
	        e: e,
	        hashes: hashes,
	        router: router
	    }
	});