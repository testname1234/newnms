﻿define('vm.home',
    [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'vm.topicselection',
        'vm.production',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'vm.contentviewer',
        'model.tickerwritercontrol',
        'model.mapper',
        'model.userfilter',
        'model.reportnews'
    ],
    function (_, $, ko, dc, topicSelectionVM, productionVM, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, contentviewer, tickerWriterControl, mapper, userfilter, ReportNews) {
        var logger = config.logger;

        var
            // Properties
            // ------------------------

            hashes = config.hashes.production,
            rightTmplName = 'producer.home',
            imageFooterTmplName = 'producer.home',
            contentFooterTmplName = 'producer.home',
            contentTmplName = 'producer.home',
            templates = config.templateNames,
            pageIndex = ko.observable(1),
            selectedNewsId = appdata.selectedNewsId,
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            sourceFilterControl = new model.SourceFilterControl(),
            calendarControl = new model.CalendarControl(),
            bunchArray = ko.observableArray(),
            pendingStoriesArray = ko.observableArray(),
            searchGroupName = ko.observable(),
            currentBunch = ko.observable(new model.Bunch()),
            tickerwritercontrol = new model.TickerWriterControl(),
            currentView = ko.observable('listView'),
            selectedAudioClip = ko.observable(),
            audioComments = ko.observable(),
            isUpdateScroll = ko.observable(false),
            isStoreScroll = ko.observable(false),
            viewBreakingNews = ko.observable(false),
            currentBreakingNews = ko.observable(),
            xmlParserObj = new model.XmlParser(),
            scrollPos = 0,
            reportnewsView = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            tickerReporterControl = new model.TickerReporterControl(),

            // Computed Properties
            // ------------------------

            newsCount = ko.computed({
                read: function () {
                    var allFilters = dc.filters.getObservableList();
                    var filter = _.filter(allFilters, function (obj) {
                        return obj.id === 78;
                    });

                    if (filter && filter.length)
                        return filter[0].newsCount(2);
                },
                deferEvaluation: true
            }),
            newsGroups = ko.computed({
                read: function () {
                    var newsBunchArray = bunch();
                    if (newsBunchArray.length > 0) {
                        var arr = [];
                        for (var i = 0; i < appdata.dateCategories().length; i++) {
                            var tempObj = {};
                            var parsedDate = moment(appdata.dateCategories()[i]).calendar();
                            var bunchArray = _.filter(newsBunchArray, function (obj) {
                                return moment(obj.lastUpdateDate()).format('l') == moment(appdata.dateCategories()[i]).format('l');
                            });
                            tempObj["groupName"] = parsedDate;
                            tempObj["bunch"] = bunchArray;
                            if (bunchArray.length > 0)
                                arr.push(tempObj);
                        }
                        return arr;
                    } else {
                        return [];
                    }
                },
                deferEvaluation: true
            }),
            bunch = ko.computed({
                read: function () {
                    var arr;
                    if (appdata.currentUser().userType == e.UserType.TickerWriter || appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerManager || appdata.currentUser().userType == e.UserType.HeadlineProducer || appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType == e.UserType.FReporter) {
                        arr = bunchArray();
                    }
                    else {
                        var arr = bunchArray().slice(3, pageIndex() * 5);
                    }

                    var currentGroup = '';
                    var groupArray = [];
                    for (var i = 0; i < arr.length; i++) {
                        var temp = moment(moment(arr[i].publishTime()).format('l')).calendar();
                        if (currentGroup !== temp) {
                            currentGroup = temp;
                            var tempGroup = {
                                groupName: currentGroup,
                                index: i
                            };
                            if (i > 0) {
                                groupArray.push(tempGroup);
                            }
                            else {
                                searchGroupName(tempGroup.groupName);
                            }
                        }
                    }
                    for (var i = 0; i < groupArray.length; i++) {
                        var tempObj = new model.Bunch();
                        tempObj.isGroupName = true;
                        tempObj.groupName = groupArray[i].groupName;
                        arr.splice(groupArray[i].index + i, 0, tempObj);
                    }
                    if (arr.length <= 0) {
                        searchGroupName('');
                    }
                    //if (parent.window && parent.window.document && $(parent.window.document.getElementById('createNewsPopup')).hasClass('isNews')) {
                    //    addToNewsBucket(arr[0].topNews());
                    //}
                    return arr;
                },
                deferEvaluation: true
            }),
            newsDragIsReadOnly = ko.computed({
                read: function () {
                    if (appdata.currentUser().userType === e.UserType.TickerProducer || appdata.currentUser().userType === e.UserType.TickerWriter) {
                        return true;
                    }
                    return false;
                },
                deferEvaluation: true
            }),
            headlines = ko.computed({
                read: function () {
                    if (bunchArray().length > 0) {
                        var arr = bunchArray();
                        arr = arr.slice(0, 3);

                        var tempObj = {};
                        var defaultNewsObj = new model.News();
                        defaultNewsObj.isNullo = true;

                        if (arr && arr.length > 0) {
                            if (arr[0] && arr[0].topNews()) {
                                arr[0].forceRefresh(!arr[0].forceRefresh());
                                tempObj["first"] = arr[0].topNews();
                            }
                            else
                                tempObj["first"] = defaultNewsObj;

                            if (arr[1] && arr[1].topNews()) {
                                arr[1].forceRefresh(!arr[1].forceRefresh());
                                tempObj["second"] = arr[1].topNews();
                            }
                            else
                                tempObj["second"] = defaultNewsObj;

                            if (arr[2] && arr[2].topNews()) {
                                arr[2].forceRefresh(!arr[2].forceRefresh());
                                tempObj["third"] = arr[2].topNews();
                            }
                            else
                                tempObj["third"] = defaultNewsObj;
                        } else {
                            tempObj["first"] = defaultNewsObj;
                            tempObj["second"] = defaultNewsObj;
                            tempObj["third"] = defaultNewsObj;
                        }

                        presenter.toggleActivity(false);

                        return tempObj;
                    } else {
                        presenter.toggleActivity(false);
                    }
                },
                deferEvaluation: true
            }),
            groupedPendingStories = ko.computed({
                read: function () {
                    lastUIRefreshDisplayTime = moment().toISOString();

                    var arr = [];
                    arr = pendingStoriesArray();

                    var timeCategories = utils.getTimeSlots(60);

                    var stories = [];

                    if (arr && arr.length > 0) {
                        for (var j = 0, len = arr.length; j < len; j++) {
                            var objectMap = {};
                            objectMap.GroupName = moment(arr[j].lastUpdateDate()).format("LL, hh:mm a");
                            objectMap.pendingStories = [];

                            var exist = utils.arrayIsContainObject(stories, objectMap, "GroupName");
                            if (!exist.flag) {
                                objectMap.pendingStories.push(arr[j]);
                                stories.push(objectMap);
                            }
                            else
                                stories[exist.objectIndex].pendingStories.push(arr[j]);
                        }
                    }

                    return stories;
                },
                deferEvaluation: true
            }),
            pendingStoryCount = ko.computed({
                read: function () {
                    var count = 0;
                    for (var i = 0; i < groupedPendingStories().length; i++) {
                        count += groupedPendingStories()[i].pendingStories.length;
                    }
                    return count;
                },
                deferEvaluation: true
            }),
            userPreferenceFilter = ko.computed({
                read: function () {
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && (arr[i].filterTypeId === e.NewsFilterType.Category || arr[i].filterTypeId < e.NewsFilterType.Category)) {
                                temparr.push(arr[i]);
                            }
                        }
                    }

                    return temparr;
                },
                deferEvaluation: true
            }),

            userCategorizedFilter = ko.computed({
                read: function () {
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && arr[i].filterTypeId === 13) {
                                arr[i].isVisible(true);
                                if (arr[i].children().length == 0) {
                                    arr[i].changeSign('');
                                }
                                temparr.push(arr[i]);
                            }
                        }
                    }
                    return temparr;
                },
                deferEvaluation: true
            }),

            userSourceFilter = ko.computed({
                read: function () {
                    appdata.isUserSourceFilterChange();
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && arr[i].filterTypeId != 13) {
                                arr[i].isVisible(true);
                                if (arr[i].children().length == 0) {
                                    arr[i].changeSign('');
                                }
                                temparr.push(arr[i]);
                            }
                        }
                    }
                    return temparr;
                },
                deferEvaluation: true
            }),

            lastUIRefreshDisplayTime = ko.observable(moment(appdata.lastUIRefreshTime).fromNow()),

            // Methods
            // ------------------------

            submitNews = function () {
              
                reportnewsView.tickerLines(tickerReporterControl.tickerLineList());
                reportnewsView.currentTicker(tickerReporterControl.currentTicker());
                if (tickerReporterControl.lastTicker && tickerReporterControl.lastTicker()) {
                    tickerReporterControl.tickerLineList.push({ Text: tickerReporterControl.lastTicker(), LanguageCode: "ur", SequenceId: tickerReporterControl.tickerLineList().length == 0 ? 1 : tickerReporterControl.tickerLineList().length + 1 });
                }
                reportnewsView.submit();
                tickerReporterControl.tickerLineList([])
               
            },

            setCurrentNews = function (data, isRelated) {
                if (data) {
                    if (isRelated)
                        topicSelectionVM.setCurrentNews(data);
                    else
                        topicSelectionVM.setCurrentNews(data.topNews());

                    router.navigateTo(config.hashes.production.topicSelection);
                    appdata.lastRoute(config.hashes.production.topicSelection);
                }

            },
            addToBucketNews = function () {
                var data = 1;
            },

            addToNewsBucket = function (data) {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    fillTicker(data);
                    return;
                }
                if (data.isUsed()) {
                    var temp;
                    var allNewsBucket = dc.newsBucketItems.getAllLocal();
                    for (var i = 0; i < allNewsBucket.length; i++) {
                        if (allNewsBucket[i].news.id === data.id) {
                            temp = allNewsBucket[i];
                            break;
                        }
                    }
                    if (temp)
                        manager.production.removeStoryOnServer(temp);
                }
                else
                    manager.news.addToNewsBucket(data);
            },
            setCurrentPendingStory = function (data, parentContext) {
                if (data) {
                    7
                    var arr = groupedPendingStories();
                    var sortedPending = [];
                    for (var i = 0; i < arr.length; i++) {
                        for (var j = 0; j < arr[i].pendingStories.length; j++) {
                            sortedPending.push(arr[i].pendingStories[j]);
                        }
                    }
                    var currentIndex = 0;
                    for (var i = 0; i < sortedPending.length; i++) {
                        pendingStory = sortedPending[i];
                        if (data.id === pendingStory.id) {
                            currentIndex = i;
                            break;
                        }
                    }
                    productionVM.setCurrentPendingStory(data, sortedPending, currentIndex);
                    router.navigateTo(config.hashes.production.production);
                }
            },
            initializeBunchGenerator = function () {
                setTimeout(function polling() {
                    $.when(manager.news.getFilteredBunches(calendarControl.currentOption()))
                    .done(function (data) {
                        if (data) {
                            if (data.PageIndex)
                                pageIndex(data.PageIndex);
                            if (data.Bunches)
                                bunchArray(data.Bunches);
                        }
                        if (lastUIRefreshDisplayTime)
                            lastUIRefreshDisplayTime(moment(appdata.lastUIRefreshTime).fromNow());

                        setTimeout(polling, config.displayPollingInterval * 1000);
                    })
                    .fail(function () {
                        if (lastUIRefreshDisplayTime)
                            lastUIRefreshDisplayTime(moment(appdata.lastUIRefreshTime).fromNow());

                        setTimeout(polling, config.displayPollingInterval * 1000);
                    });
                }, config.displayPollingInterval * 1000);
            },
            initializePendingStoryGenerator = function () {
                setTimeout(function pendingStoryPolling() {
                    $.when(manager.production.getFilteredPendingStories(calendarControl.currentOption()))
                    .done(function (data) {
                        if (data) {
                            if (data.PageIndex)
                                pageIndex(data.PageIndex);
                            if (data.pendingStories)
                                pendingStoriesArray(data.pendingStories);
                        }
                        setTimeout(pendingStoryPolling, config.displayPollingInterval * 1000);
                    })
                    .fail(function () {
                        setTimeout(pendingStoryPolling, config.displayPollingInterval * 1000);
                    });
                }, config.displayPollingInterval * 1000);
            },
            selectNews = function (data) {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    fillTicker(data);
                }
                else
                    addToNewsBucket(data);

                // manager.news.selectNews(data);
                // appdata.isStorySavedDirty(true);
            },
            fillTicker = function (data) {
                var reqObj = [];
                reqObj.news = ko.observable(data);
                var ticker = mapper.ticker.fromDto(reqObj);
                contentviewer.twc.tickerPopupInputVisible(false);
                contentviewer.twc.cssClass('showleftpanel');
                contentviewer.setCurrentTicker(ticker);
            },
            toggleStoryFilter = function (data) {
                manager.production.toggleStoryFilter(data);
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.setDataChangeTime();
            },
            toggleFilter = function (data, isExtraFilter) {
                for (var i = 0; i < dc.news.getAllLocal().length; i++) {
                    dc.news.getAllLocal()[i].searchTerm = null;
                }

                manager.news.toggleFilter(data, isExtraFilter);
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.getMoreNews();
                manager.news.setDataChangeTime();
            },
            resetFilters = function () {
                appdata.selectedCategoryFilters([]);
                appdata.selectedExtraFilters([]);
                appdata.selectedSourceFilters([]);
                sourceFilterControl.currentFilter(null);
                searchKeywords('');
                calendarControl.currentOption('lastmonth');

                appdata.lpFromDate = calendarControl.fromDate();
                appdata.lpToDate = calendarControl.toDate();

                $('.nav-step1 li a').removeClass('active-category-filter');  //Category
                $('.mainFilter.hideFilters').removeClass('newsActive'); //Source
                $('.navigationNews li ul li').removeClass('active-filter'); //Source Children
                $('.onoffswitch .onoffswitch-label').removeClass('active'); //Switches
                $('#top-rated-filter').removeClass('active'); //top Rated
                $('.calenderRight').find('.active').removeClass('active');
                $('.calenderRight').find('.lastMonth').addClass('active');

                appdata.selectedSourceFilters.push(78);
                appdata.isAllNewsFilterSelected(true);
                manager.news.setDataChangeTime();
            },
            setCurrentBunchFilters = function (data) {
                if (currentBunch().selectedFilters().indexOf(data) == -1)
                    currentBunch().selectedFilters.push(data);
                else {
                    var index = currentBunch().selectedFilters.indexOf(data);
                    currentBunch().selectedFilters.splice(index, 1);
                }
            },
            setCurrentBunch = function (data) {
                currentBunch(data);
                if (currentBunch().showfilters())
                    currentBunch().showfilters(false);
                else
                    currentBunch().showfilters(true);

                manager.news.getNewsBunch(currentBunch());
            },
            clickAttachment = function () {
                if (appdata.currentUser().userType == e.UserType.FReporter) {
                    $('.editorUploader')[1].click();
                }
                else {
                    $('.editorUploader').click();
                }
                
            },
            clickCameramen = function () {
                event.stopPropagation();
                $('.cameraman').toggleClass('active');
            },
            openMediaSelection = function () {
                contentviewer.mediaSelectionUrl(config.hashes.production.topicSelection + '?isiframe');
                $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                contentviewer.isMediaViewVisibleExplorer(true);
                appdata.isDefaultTab(!appdata.isDefaultTab());
                // document.getElementById('popupIframeFExp').contentWindow.myFunction();
            },
            loadNextPage = function () {
                presenter.toggleActivity(true);

                if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType == e.UserType.FReporter || appdata.currentUser().userType == e.UserType.HeadlineProducer || appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    $.when(manager.news.getMoreNews())
                    .done(function () {
                        var pIndex = pageIndex() + 1;
                        pageIndex(pIndex);
                    })
                    .fail(function () {
                        presenter.toggleActivity(false);
                    });
                } else {
                    $.when(manager.production.getMorePendingStories())
                    .done(function () {
                        var pIndex = pageIndex() + 1;
                        pageIndex(pIndex);
                    })
                    .fail(function () {
                    });
                }
            },
            switchViewTicker = function () {
                if (router.currentHash().indexOf('home') != -1) {
                    router.navigateTo(config.hashes.production.bureauTicker);
                } else if (router.currentHash().indexOf('bureauticker') != -1) {
                    router.navigateTo(config.hashes.production.home);
                }
            },
            switchView = function myfunction() {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    if (router.currentHash().indexOf('home') != -1) {
                        router.navigateTo(config.hashes.production.myTicker);
                    }
                } else {
                    if (router.currentHash().indexOf('home') != -1) {
                        //router.navigateTo(config.hashes.production.headlineUpdates);
                    } else if (router.currentHash().indexOf('headlineUpdates') != -1) {
                        router.navigateTo(config.hashes.production.home);
                    }
                }
            },
            setCalendarDate = function () {
                appdata.lpFromDate = calendarControl.fromDate();
                appdata.lpToDate = calendarControl.toDate();
                appdata.lpStartIndex = 0;

                if (appdata.currentUser().userType === e.UserType.NLE || appdata.currentUser().userType === e.UserType.StoryWriter) {
                    appdata.NLEfromDate(appdata.lpFromDate);
                    appdata.NLEtoDate(appdata.lpToDate);
                }
                manager.news.setDataChangeTime();
                manager.news.getMoreNews();
            },
            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    for (var i = 0; i < dc.news.getAllLocal().length; i++) {
                        dc.news.getAllLocal()[i].searchTerm = null;
                    }


                    appdata.searchKeywords = value;
                    appdata.lpStartIndex = 0;
                    manager.news.getMoreNews();
                    manager.news.setDataChangeTime();
                });
                pageIndex.subscribe(function (value) {
                    appdata.pageIndex = value;
                    manager.news.setDataChangeTime();
                });
                appdata.currentResource.subscribe(function (value) {
                    if (value && value != '') {

                        if (topicSelectionVM.isIframeVisible()) {
                            var resource = value;
                            window.parent.window.postMessage(value, "*");
                        }
                        else {
                            var value = JSON.parse(value);
                            if (value.Guid) {
                                var res = mapper.resource.fromDto(value);
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === res.guid;
                                });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === res.guid;
                                    });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                    }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(res);
                                    reportnewsView.uploader().uploadedResources().push(value);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = res.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                            }
                            else {
                                var obj = {
                                    Guid: value.guid,
                                    ResourceId: value.id,
                                    ResourceTypeId: value.type,
                                    IgnoreMeta: 1
                                }
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === value.guid;
                                });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === value.guid;
                                    });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                    }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(value);
                                    reportnewsView.uploader().uploadedResources().push(obj);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = value.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                            }
                        }
                    }
                });

            },
            displayAudioToolTip = function (data) {
                selectedAudioClip('../../wildlife.mp3');
                audioComments(data.title());
            },
            closeAudioToolTip = function () {
                selectedAudioClip('');
            },
            resetContentFilter = function () {
                $('#allNews').removeClass('active');
                $('#allNews').addClass('active');
                $('#news').removeClass('active');
                $('#packages').removeClass('active');
                $('#verified-filter').removeClass('active');
                $('#recommended-filter').removeClass('active');
                $('#top-rated-filter').removeClass('active');
                $('#top-executed-filter').removeClass('active');
                $('#most-recent-filter').removeClass('active');

                appdata.selectedExtraFilters([]);
                toggleFilter(-1, true);
            },
            searchEventNews = function (data) {
                if (data) {
                    if (data.isSelected()) {
                        data.isSelected(false);
                        appdata.searchKeywords = '';
                        manager.news.setDataChangeTime();
                    } else {
                        _.filter(sourceFilterControl.events(), function (obj) {
                            return obj.isSelected(false);
                        });
                        data.isSelected(true);
                        appdata.searchKeywords = data.searchTags;
                        manager.news.setDataChangeTime();
                    }
                }
            },
            setCurrenBreakingNews = function (data) {
                contentviewer.setCurrentAlertContent(data, 2);
            },
            setCurrentContent = function (data, options) {
                try {
                    if (data && options) {
                        if (options.isReportedContent) {
                            contentviewer.setCurrentContent(reportnewsView.uploader().mappedResources(), data);
                        }
                    }
                } catch (e) {
                    //console.log(e.message);
                }
            },
            hideBreakingNews = function () {
                viewBreakingNews(false);
                currentBreakingNews('');
            },
            setFullScreenMode = function () {
                var el = document.body;
                var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen
              || el.mozRequestFullScreen || el.msRequestFullScreen;
                if (requestMethod) {
                    requestMethod.call(el);
                } else if (typeof window.ActiveXObject !== "undefined") {
                    var wscript = new ActiveXObject("WScript.Shell");
                    if (wscript !== null) {
                        wscript.SendKeys("{F11}");
                    }
                }
            },
            storeScrollPosition = function () {
                appdata.producerScrollTop($('#home-view').children('div').children('.mCSB_container').css('top'));
            },
            setScrollPosition = function () {
                scrollPos = appdata.producerScrollTop();
                scrollPos = scrollPos.replace('px', '');
                $('#home-view').children('div').children('.mCSB_container').css('top', parseInt(scrollPos));
            },
            hideNextPrevNLE = function () {
                $(".prev").css({ 'display': 'none' });
                $(".next").css({ 'display': 'none' });
            },
            toggleArchivalSearch = function () {
                appdata.isArchival(!appdata.isArchival());
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.getMoreNews();
                manager.news.setDataChangeTime();
            },
            downLoadResource = function (Url) {
                var link = document.createElement("a");
                link.download = Url()
                link.href = Url();
                link.click();
            },
            submitMetaDataByUser = function () {
                var MetaTypeId = e.MetaTypeId.Filters;
                var result = dc.UserFilter.getAllLocal();
                var arry = [];
                for (var i = 0; i < result.length ; i++) {
                    var obj = {
                        MetaTypeId: MetaTypeId,
                        MetaName: result[i].name,
                        MetaValue: result[i].id,
                        UserId: appdata.currentUser().id,
                        isAllowed: result[i].isNotAllowed(),
                        isDiscarded: result[i].isDiscarded()
                    };
                    arry.push(obj);
                }

                var MetaData = { MetaData: arry };
                manager.news.submitMetaDataByUser(MetaData);
            },
            getAllFilters = function () {
                manager.news.getAllFilters();
            },

            editSelected = function (data) {
                router.navigateTo(config.views.fieldreporter.reportNews.url);
                data.topNews().isNewsEdit(true);
                reportnewsView.newsSelectedProduction(data.topNews());

            },
            activate = function (routeData, callback) {
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.TickerWriter || appdata.currentUser().userType === e.UserType.TickerProducer) {
                    setScrollPosition();
                }
            },
            canLeave = function () {
                if (appdata.currentHash != hashes.bureauTicker && appdata.currentHash != hashes.headlineUpdates && appdata.currentHash != hashes.home) {
                    if (appdata.isStorySavedDirty()) {
                        if (dc.stories.getByEpisodeId(appdata.currentEpisode().id)().length > 0) {
                            manager.production.proceedToSequence();
                            appdata.isStorySavedDirty(false);
                        }
                    }
                }
                storeScrollPosition();
                return true;
            },
            init = function () {

                sourceFilterControl.currentFilter(-1);
                sourceFilterControl.isPopulated(1);
                sourceFilterControl.showExtraFilters(true);
                calendarControl.currentOption('lastmonth');

                if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType == e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.TickerWriter || appdata.currentUser().userType === e.UserType.TickerProducer)
                    calendarControl.isNewsCropVisible(true);

                subscribeEvents();

                //toggleFilter(450, true);
                toggleFilter(-1, true);

                if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType == e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.TickerWriter || appdata.currentUser().userType === e.UserType.TickerProducer)
                    initializeBunchGenerator();
                else {
                    initializePendingStoryGenerator();
                    appdata.teamCalendarControlReference(calendarControl);
                }

                if (appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    switchView();
                }
            };

        function receivedParentData(event) {
            if (topicSelectionVM.isIframeVisible() && router.currentHash() != "#/production" && !appdata.isNewsCreated()) {
                var tempRes = event.data;
                var res = mapper.resource.fromDto(tempRes);
                reportnewsView.uploader().mappedResources().push(res);
                reportnewsView.uploader().uploadedResources().push(tempRes);
                reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = res.type;
                reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                reportnewsView.uploader().uploadedResources.valueHasMutated();
                reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
            }
        };

        addEventListener("message", receivedParentData, false);

        function receivedParentNews(event) {
            if (appdata.isNewsCreated()) {
                var data = event.data;
                var item = JSON.parse(data);
                var arr = _.filter(bunch(), function (bItem) {
                    return bItem.id === item.BunchGuid;
                });
                if (arr && arr.length > 0) {
                    addToNewsBucket(arr[0].topNews());
                }
            }
        };

        addEventListener("message", receivedParentNews, false);

        $("iframe").load(function() {
            var frameContents;
            frameContents = $("#popupIframeNewsAdd").contents();
            frameContents.find("#shell-top-nav-view").hide();
            frameContents.find("#main").find('.reportNews').find('.editingSection').children('.mainnewstmpl').children('.bodySection').find('.editor').find('.newEditor').find('.iframeFE').addClass('hideExp');
            frameContents.find("#main").find('.reportNews').find('.editingSection').children('.mainnewstmpl').children('.bodySection').find('.editor').find('.newEditor').find('.iframeFE').hide()
            
            
        });

        return {
        activate: activate,
        canLeave: canLeave,
        appdata: appdata,
        bunch: bunch,
        selectNews: selectNews,
        setCurrentNews: setCurrentNews,
        setCurrentPendingStory: setCurrentPendingStory,
        templates: templates,
        init: init,
        sourceFilterControl: sourceFilterControl,
        calendarControl: calendarControl,
        hashes: hashes,
        headlines: headlines,
        loadNextPage: loadNextPage,
        toggleFilter: toggleFilter,
        toggleStoryFilter: toggleStoryFilter,
        searchKeywords: searchKeywords,
        selectedNewsId: selectedNewsId,
        newsGroups: newsGroups,
        newsCount: newsCount,
        resetFilters: resetFilters,
        lastUIRefreshDisplayTime: lastUIRefreshDisplayTime,
        searchGroupName: searchGroupName,
        pendingStoryCount: pendingStoryCount,
        userPreferenceFilter: userPreferenceFilter,
        groupedPendingStories: groupedPendingStories,
        setCalendarDate: setCalendarDate,
        currentBunch: currentBunch,
        setCurrentBunch: setCurrentBunch,
        setCurrentBunchFilters: setCurrentBunchFilters,
        currentView: currentView,
        selectedAudioClip: selectedAudioClip,
        displayAudioToolTip: displayAudioToolTip,
        closeAudioToolTip: closeAudioToolTip,
        audioComments: audioComments,
        resetContentFilter: resetContentFilter,
        isUpdateScroll: isUpdateScroll,
        isStoreScroll: isStoreScroll,
        searchEventNews: searchEventNews,
        setCurrenBreakingNews: setCurrenBreakingNews,
        viewBreakingNews: viewBreakingNews,
        currentBreakingNews: currentBreakingNews,
        hideBreakingNews: hideBreakingNews,
        setFullScreenMode: setFullScreenMode,
        switchView: switchView,
        e: e,
        tickerwritercontrol: tickerwritercontrol,
        utils: utils,
        newsDragIsReadOnly: newsDragIsReadOnly,
        toggleArchivalSearch: toggleArchivalSearch,
        downLoadResource: downLoadResource,
        reportnewsView: reportnewsView,
        submitNews: submitNews,
        switchViewTicker: switchViewTicker,
        setCurrentContent: setCurrentContent,
        submitMetaDataByUser: submitMetaDataByUser,
        getAllFilters: getAllFilters,
        tickerReporterControl: tickerReporterControl,
        userSourceFilter: userSourceFilter,
        userCategorizedFilter: userCategorizedFilter,
        router: router,
        config: config,
        clickAttachment: clickAttachment,
        clickCameramen: clickCameramen,
        xmlParserObj: xmlParserObj,
        addToNewsBucket: addToNewsBucket,
        editSelected: editSelected,
        openMediaSelection: openMediaSelection,
        addToBucketNews: addToBucketNews
    };
});