﻿define('vm',
    [
        'vm.shell',
        'vm.home',
        'vm.topicselection',
        'vm.sequence',
        'vm.production',
        'vm.report',
        'vm.contentviewer',
        'vm.cropcontentviewer',
        'vm.headline',
        'vm.ticker',
        'vm.tickerrundown',
        'vm.uploadmedia',
        'vm.channeltickers',
        'vm.newsflow'
    ],

    function (shell, home, topicselection, sequence, production, report, contentviewer, cropcontentviewer, headline, ticker, tickerrundown, uploadmedia, channelTickers, newsflow) {
        return {
            shell: shell,
            home: home,
            topicselection: topicselection,
            sequence: sequence,
            production: production,
            report: report,
            contentviewer: contentviewer,
            cropcontentviewer: cropcontentviewer,
            headline: headline,
            ticker: ticker,
            tickerrundown: tickerrundown,
            uploadmedia: uploadmedia,
            channelTickers: channelTickers,
            newsflow: newsflow
        };
    });