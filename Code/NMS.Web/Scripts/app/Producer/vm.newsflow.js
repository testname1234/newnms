﻿define('vm.newsflow',
    [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'vm.topicselection',
        'vm.production',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'vm.contentviewer',
        'model.tickerwritercontrol',
        'model.mapper',
        'model.userfilter',
        'model.reportnews'
    ],
    function (_, $, ko, dc, topicSelectionVM, productionVM, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, contentviewer, tickerWriterControl, mapper, userfilter, ReportNews) {
        var logger = config.logger;

        var
            // Properties
            // ------------------------

            hashes = config.hashes.production,
            rightTmplName = 'producer.home',
            imageFooterTmplName = 'producer.home',
            contentFooterTmplName = 'producer.home',
            contentTmplName = 'producer.home',
            templates = config.templateNames,
            pageIndex = ko.observable(1),
            selectedNewsId = appdata.selectedNewsId,
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            sourceFilterControl = new model.SourceFilterControl(),
            calendarControl = new model.CalendarControl(),
            bunchArray = ko.observableArray(),
            pendingStoriesArray = ko.observableArray(),
            searchGroupName = ko.observable(),
            currentBunch = ko.observable(new model.Bunch()),
            tickerwritercontrol = new model.TickerWriterControl(),
            currentView = ko.observable('listView'),
            selectedAudioClip = ko.observable(),
            audioComments = ko.observable(),
            isUpdateScroll = ko.observable(false),
            isStoreScroll = ko.observable(false),
            viewBreakingNews = ko.observable(false),
            currentBreakingNews = ko.observable(),
            xmlParserObj = new model.XmlParser(),
            scrollPos = 0,
            reportnewsView = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            tickerReporterControl = new model.TickerReporterControl(),

            // Computed Properties
            // ------------------------

           
          
            // Methods
            // ------------------------

            submitNews = function () {
              
                reportnewsView.tickerLines(tickerReporterControl.tickerLineList());
                reportnewsView.currentTicker(tickerReporterControl.currentTicker());
                if (tickerReporterControl.lastTicker && tickerReporterControl.lastTicker()) {
                    tickerReporterControl.tickerLineList.push({ Text: tickerReporterControl.lastTicker(), LanguageCode: "ur", SequenceId: tickerReporterControl.tickerLineList().length == 0 ? 1 : tickerReporterControl.tickerLineList().length + 1 });
                }
                reportnewsView.submit();
                tickerReporterControl.tickerLineList([])
               
            },
            clickAttachment1 = function () {
               
                    $('.editorUploader')[1].click();
               
                
            },
            clickCameramen = function () {
                event.stopPropagation();
                $('.cameraman').toggleClass('active');
            },
            openMediaSelection1 = function () {
                contentviewer.mediaSelectionUrl(config.hashes.production.topicSelection + '?isiframe');
                $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                contentviewer.isMediaViewVisibleExplorer(true);
                appdata.isDefaultTab(!appdata.isDefaultTab());
                // document.getElementById('popupIframeFExp').contentWindow.myFunction();
            },
           
           
            subscribeEvents = function () {
                appdata.currentResource.subscribe(function (value) {
                    if (value && value != '') {

                        if (topicSelectionVM.isIframeVisible()) {
                            var resource = value;
                            window.parent.window.postMessage(value, "*");
                        }
                        else {
                            var value = JSON.parse(value);
                            if (value.Guid) {
                                var res = mapper.resource.fromDto(value);
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === res.guid;
                                });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === res.guid;
                                    });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                    }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(res);
                                    reportnewsView.uploader().uploadedResources().push(value);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = res.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                            }
                            else {
                                var obj = {
                                    Guid: value.guid,
                                    ResourceId: value.id,
                                    ResourceTypeId: value.type,
                                    IgnoreMeta: 1
                                }
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === value.guid;
                                });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === value.guid;
                                    });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                    }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(value);
                                    reportnewsView.uploader().uploadedResources().push(obj);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = value.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                            }
                        }
                    }
                });

            },
            displayAudioToolTip = function (data) {
                selectedAudioClip('../../wildlife.mp3');
                audioComments(data.title());
            },
            closeAudioToolTip = function () {
                selectedAudioClip('');
            },
            downLoadResource = function (Url) {
                var link = document.createElement("a");
                link.download = Url()
                link.href = Url();
                link.click();
            },
            activate = function (routeData, callback) {
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
                alert();
                
            },
            canLeave = function () {
                return true;
            },
            init = function () {
                subscribeEvents();
            };

       
            $(".change").click(function () {
                var obj = $(this).parent();
                if (obj.width() == 5) {
                    obj.animate({
                        width: '24.5%',
                    });
                    $(this).css({ position: 'relative' });
                    if (obj.next().length > 0)
                        obj.next().animate({
                            width: '24%',
                        });
                    else
                        obj.prev().animate({
                            width: '24%',
                        });
                    //obj.children('.insideDiv').show();
                } else {
                    obj.animate({
                        width: '5px',
                    });
                    $(this).css({ position: 'absolute' });
                    if (obj.next().length > 0 && obj.next().width() != 5) {
                        obj.next().animate({
                            width: '48%',
                        });
                        obj.next().children('.change').css({ position: 'relative' });
                    }
                    //else {
                    //    obj.prev().animate({
                    //        width: '48%',
                    //    });
                    //    obj.prev().children('.change').css({ position: 'relative' });
                    //}
                    //obj.children('.insideDiv').hide();
                }
                setTimeout(function () {
                    //checkForDetailViewWidth();
                }, 1000);

            });
       
        function checkForDetailViewWidth() {
            var dW = $(DetailView).width();
            var total = $(ContainerDiv).width();
            var width = 0;
            $(ContainerDiv).children('.sliderDiv').each(function () {
                width += $(this).outerWidth(true);
            });
            var whiteSpace = total - width;
            if (whiteSpace > 50) {
                dW += whiteSpace - 30;
                $(DetailView).animate({
                    width: dW,
                });
            } else if ($(DetailView).width() != '24.5%' || $(DetailView).width() != '48%') {
                $(DetailView).animate({
                    width: '24.5%',
                });
            }
        };

        function receivedParentData(event) {
            if (topicSelectionVM.isIframeVisible() && router.currentHash() != "#/production" && !appdata.isNewsCreated()) {
                var tempRes = event.data;
                var res = mapper.resource.fromDto(tempRes);
                reportnewsView.uploader().mappedResources().push(res);
                reportnewsView.uploader().uploadedResources().push(tempRes);
                reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = res.type;
                reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                reportnewsView.uploader().uploadedResources.valueHasMutated();
                reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
            }
        };

        addEventListener("message", receivedParentData, false);

        function receivedParentNews(event) {
            if (appdata.isNewsCreated()) {
                var data = event.data;
                var item = JSON.parse(data);
                var arr = _.filter(bunch(), function (bItem) {
                    return bItem.id === item.BunchGuid;
                });
                if (arr && arr.length > 0) {
                    addToNewsBucket(arr[0].topNews());
                }
            }
        };

        addEventListener("message", receivedParentNews, false);

        $("iframe").load(function() {
            var frameContents;
            frameContents = $("#popupIframeNewsAdd").contents();
            frameContents.find("#shell-top-nav-view").hide();
            frameContents.find("#main").find('.reportNews').find('.editingSection').children('.mainnewstmpl').children('.bodySection').find('.editor').find('.newEditor').find('.iframeFE').addClass('hideExp');
            frameContents.find("#main").find('.reportNews').find('.editingSection').children('.mainnewstmpl').children('.bodySection').find('.editor').find('.newEditor').find('.iframeFE').hide()
            
            
        });

        return {
        activate: activate,
        canLeave: canLeave,
        appdata: appdata,
        templates: templates,
        init: init,
        hashes: hashes,
        currentView: currentView,
        e: e,
        utils: utils,
        reportnewsView: reportnewsView,
        submitNews: submitNews,
        router: router,
        config: config,
        clickAttachment1: clickAttachment1,
        clickCameramen: clickCameramen,
        openMediaSelection1: openMediaSelection1,

    };
});