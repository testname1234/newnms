﻿define('vm.production',
    [
        'ko',
        'model',
        'datacontext',
        'appdata',
        'config',
        'enum',
        'model',
        'manager',
        'model.mapper',
        'vm.contentviewer',
        'messenger',
        'control.uploader',
        'utils',
        'ControllerScript',
        'router',
        'presenter',
        'config',
        'helper'
    ],
    function (ko, model, dc, appdata, config, e, model, manager, mapper, contentViewerVM, messenger, uploader, utils, controllerScript, router, presenter, config, helper) {
        var
            //#region Properties

            currentStep = ko.observable(e.ProgramDesignerStep.TemplateSelection),
            currentStoryIndex = ko.observable(0),
            currentPendingStory = ko.observable(new model.Story()),

            pendingStoryList = ko.observableArray([]),
            currentPendingStoryIndex = ko.observable(0),

            currentBunch = ko.observable(new model.Bunch()),
            currentGuest = ko.observable(new model.Guest()),
            templates = config.templateNames,
            views = config.viewIds.production,
            hashes = config.hashes.production,
            programInfoControl = new model.ProgramSelectionAndInfoControl(),
            sourceFilterControl = new model.SourceFilterControl(),
            pdc = new model.ProgramFlowDesignerControl(),
            sdc = new model.ScreenTemplateDesignerControl(),
            prc = new model.ProgramRundownControl(),
            ppc = new model.ProgramPreviewControl(),
            productionlp = new model.LeftPanel(),
            controlview = new model.ContentViewerControl(),
            calendarControl = new model.CalendarControl(),
            isMediaSelectionVisible = ko.observable(false),
            isSetTop = ko.observable(false),
            isIframeVisible = ko.observable(false),
            isContentBucketEnabled = ko.observable(true),
            lastSelectedBucketResource = ko.observable(),
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            searchKeywordsResource = ko.observable('').extend({ throttle: config.throttle }),
            isContentDraggingEnabled = ko.observable(true),
            isIframeDiv=ko.observable(true),
            selectedLanguage = 'en',
            textEditorBuffer = ko.observable(''),
            isTextEditorChangeAllowed = ko.observable(false),
            currentScript = ko.observable(''),
            isActive = ko.observable(false),
            contentUploader = new uploader(),
            commentBoxText = ko.observable(),
            showBucket = ko.observable(true),
            implementToAll = ko.observable(false),
            isChatPopUpVisible = ko.observable(false),
            isTemplateKeySet = ko.observable(false),
            hidePopup = ko.observable(false),
            isVideoTemplate = ko.observable(false),
            isImageTemplate = ko.observable(false),
            guestSearchKeywords = ko.observable(),
            isCheckBoxVisible = ko.observable(true),
            isAdvanceSearchVisible = ko.observable(false),
            isContentReportEnable=ko.observable(false),
            showAdvanceTags = ko.observable(false),
            searchTags = ko.observableArray([]),
            isResourceCrop = ko.observable(false),
            imageForCropping = ko.observable(),
            resouceSearchKeyword = ko.observable(),
            archivalResources = ko.observableArray([]),
            canvaswidth = ko.observable(500),
            canvasheight = ko.observable(),
            isVideoEditToolClick = ko.observable(false),
            pageNumber = ko.observable(1),
            pageSize = ko.observable(50),
            templateIds = ko.observableArray([]),
            canApplyJqteCycle = ko.observable(false),
            screentemplateKeys = ko.observableArray([]),
            archivalServerCount = ko.observable(0),
            lastResourceFilter = ko.observable(0),
            SelectedFaceAction = ko.observable(),
            currFaceAction = ko.observable(),
            btnAttribute = ko.observable({ text: "Preview", disabled: false }),
            logger = config.logger,
            isNleUploadedResources = ko.observable(false),
            isCropping = ko.observable(false),
            changeGuestFlag = ko.observable(false),
            onResourceCheckClick = ko.observable(false),
            isProducerUploadResource = ko.observable(false),
            suggestedKeywords = ko.observableArray([]),
            nleTempResources = ko.observableArray([]),
            mediaSearchGoogleList = ko.observableArray([]),
            GoogleSearchText = ko.observable("Enable Google Search"),
            isGoogleDuplicateResource = 0,
            e = e,
            googleSearchResult = [],
            start = 0,
            videosPageNo = 0,
            isdefaultMediaTemplateApplied = ko.observable(false),
            isMediaTemplateDuplicate = ko.observable(false),
            downloadsPending = ko.observable(false),
            duplicateMediaTemplateId = ko.observable(''),
            listOfExternalResourceGuids = [],
            isGoogleSearchEnabled = ko.observable(false),
            isDefaultState = ko.observable(false),
            isImplementToAll = ko.observable(false),
            isWarningBoxVisible = ko.observable(false),
            downloadCount = ko.observable(0),
            pendingCount = ko.observable(0),
            isAllSearchEnable = ko.observable(true),
            userFavouriteResources = ko.observableArray([]),
            isFavouriteSearchEnable = ko.observable(false),
            bufferRequestObject = {},
            noAjaxRequestIsInProgress = true,
            isSmallTemplateVisible = ko.observable(false),
            resourceFilterChange = ko.observable(false),
            FEResource = ko.observable(''),
            currentMetaSearchKeyword = ko.observable(),
            iframeSrc = ko.observable(''),


            //#endregion

            //#region Computed Properties

            storyCount = ko.computed({
                read: function () {
                    if (isActive()) {
                        return dc.stories.getByEpisodeId(appdata.currentEpisode().id)().length;
                    }
                },
                deferEvaluation: true
            }),

            checkTemplates = ko.computed({
                read: function () {
                    appdata.checkTemplates();
                    if (appdata.currentProgram && appdata.currentProgram().id) {
                        var arr = _.filter(_dc.screenTemplates.getObservableList(), function (tempTemplate) {
                            return tempTemplate.programId === appdata.currentProgram().id;
                        });
                        if (arr && arr.length == 1) {
                            isSmallTemplateVisible(true);
                        }
                    }

                },
                deferEvaluation: false
            }),


            templatefaceaction = ko.computed({
                read: function () {
                    var faceaction = currFaceAction();
                    sdc.currentTemplate().faceAction = faceaction;
                },
                deferEvaluation: true
            }),
            currentStory = ko.computed({
                read: function () {
                    if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                        appdata.arrangeNewsFlag();
                        var arr = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                        if (isActive() && arr.length > 0) {
                            arr = arr.sort(function (entity1, entity2) {
                                return entity1.sequenceId > entity2.sequenceId;
                            });

                            if (currentStoryIndex() >= arr.length) {
                                currentStoryIndex(0);
                            }
                            pdc.currentStory(arr[currentStoryIndex()]);

                            var bunch = dc.bunches.getLocalById(arr[currentStoryIndex()].news.bunchId);
                            if (bunch) {
                                currentBunch(bunch);
                            }

                            isDefaultState(false);

                            return arr[currentStoryIndex()];
                        } else {
                            isDefaultState(true);
                            return new model.Story();
                        }
                    } else {
                        return currentPendingStory();
                    }
                },
                deferEvaluation: true
            }),
            currentStoryIndexDisplay = ko.computed({
                read: function () {
                    if (storyCount() > 0) {
                        var index = currentStoryIndex() + 1;
                        return (index < storyCount()) ? index : storyCount();
                    } else {
                        return 0;
                    }
                },
                deferEvaluation: false
            }),
            isEditorDisable = ko.computed({
                read: function () {
                    return appdata.currentUser().userType == e.UserType.NLE
                },
                deferEvaluation: true
            }),
            programs = ko.computed({
                read: function () {
                    var allPrograms = dc.programs.getObservableList();
                    var arr = _.filter(allPrograms, function (program) {
                        return program.channelId() === appdata.currentChannel();
                    });
                    return arr;
                },
                deferEvaluation: true
            }),
            episodes = ko.computed({
                read: function () {
                    var arr = [];
                    var programEpisodes = dc.episodes.getByProgramId(appdata.currentProgram().id)();
                    for (var i = 0; i < programEpisodes.length; i++) {
                        var diffDays = utils.getDifferenceBetweenDates(appdata.programSelectedDate(), programEpisodes[i].startTime());
                        if (diffDays === 0) {
                            arr.push(programEpisodes[i]);
                        }
                    }
                    return arr;
                },
                deferEvaluation: true
            }),
            screenTemplates = ko.computed({
                read: function () {
                    return dc.screenTemplates.getObservableList();
                },
                deferEvaluation: true
            }),
            reportedContentCount = ko.computed({
                read: function () {
                    if (currentStory().news.resources && ko.isObservable(currentStory().news.resources) && currentStory().news.resources().length > 0)
                        return currentStory().news.resources().length;
                    else
                        return 0;
                },
                deferEvaluation: true
            }),
            totalGuestCount = ko.computed({
                read: function () {
                    return dc.guests.getObservableList().length;
                },
                deferEvaluation: true
            }),
            internalGuests = ko.computed({
                read: function () {
                    var arr = _.filter(dc.guests.getObservableList(), function (guest) {
                        return guest.guestType === e.GuestType.Internal;
                    });

                    if (guestSearchKeywords() && guestSearchKeywords().length > 0) {
                        var srch = utils.regExEscape(guestSearchKeywords().toLowerCase().trim());
                        arr = _.filter(arr, function (guest) {
                            return guest.name.toLowerCase().search(srch) !== -1;
                        });
                    }

                    return arr;
                },
                deferEvaluation: true
            }),
            externalGuests = ko.computed({
                read: function () {
                    var arr = _.filter(dc.guests.getObservableList(), function (guest) {
                        return guest.guestType === e.GuestType.External;
                    });

                    if (guestSearchKeywords() && guestSearchKeywords().length > 0) {
                        var srch = utils.regExEscape(guestSearchKeywords().toLowerCase().trim());
                        arr = _.filter(arr, function (guest) {
                            return guest.name.toLowerCase().search(srch) !== -1;
                        });
                    }

                    return arr;
                },
                deferEvaluation: true
            }),
            activeSideView = ko.computed({
                read: function () {
                    if (sourceFilterControl.currentFilter() === 'media')
                        return views.topicSelectionMediaView;
                    else if (sourceFilterControl.currentFilter() === 'guest')
                        return views.topicSelectionGuestsView;
                    else
                        return views.topicSelectionNewsFilterView;
                },
                deferEvaluation: true
            }),
            isAssignToTeamVisible = ko.computed({
                read: function () {
                    return appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer;
                },
                deferEvaluation: true
            }),
            isPreviousButtonVisible = ko.computed({
                read: function () {
                    return currentStep() < 3 && currentStoryIndex() > 0;
                },
                deferEvaluation: true
            }),
            isNextButtonVisible = ko.computed({
                read: function () {
                    return currentStep() < 3 && currentStoryIndex() < (storyCount() - 1);
                },
                deferEvaluation: true
            }),

            mediaArchivalResources = ko.computed({
                read: function () {
                    var arr = []
                    resourceFilterChange();
                    if (archivalResources() && archivalResources().length > 0) {
                        if (currentBunch().resourceFilter().length > 0) {
                            for (var i = 0, len = archivalResources().length ; i < len; i++) {
                                var item = archivalResources()[i];
                                if (item.type === currentBunch().resourceFilter()[0])
                                    arr.push(item)
                            }
                        }
                        else
                            arr = archivalResources();
                    }
                    return arr;
                },
                deferEvaluation: true
            }),


            // #endregion

            //#region Methods

            showloader = function (flag) {
                if (flag) {
                    $('.archiveSearch').show();
                }
                else {
                    $('.archiveSearch').hide();
                }
            },
            openFileExplorer = function () {
                iframeSrc('http://localhost:21642/FileManagementSystem.html?name=' + helper.getCookie('EncriptedUserId') + '&isNMS=1');
                isIframeVisible(true);
                isAllSearchEnable(false);
                isGoogleSearchEnabled(false);
                isAdvanceSearchVisible(false);
                isFavouriteSearchEnable(false);
            },
            addExplorerResource = function () {
                //var currentExpResource = JSON.parse($("#fExpResource").html());
                //console.log(currentExpResource);
            },
            playRundownUrl = function (data) {
                var arr = [];
                var tempObj = {
                    ResourceTypeId: e.ContentType.Video,
                    ResourceGuid: data(),
                    IsUploadedFromNle: false
                };
                var item = mapper.resource.fromDto(tempObj);
                arr.push(item);
                contentViewerVM.setCurrentContent(arr, arr[0]);
            },
            selectGuest = function (data) {
                if (data) {
                    currentGuest(data);
                }
            },
            addToContentBucket = function (data) {
                if (data) {
                    if (data.tbUrl) {
                        var tempObj = {
                            BucketId: config.googlebucketId,
                            ApiKey: config.googleApiKey,
                            ResourceTypeId: (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) ? 2 : 1,
                            FilePath: (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) ? data.url : data.unescapedUrl,
                            Source: data.url,
                            SystemType: 6,
                            IsFromExternalSource: true,
                            ThumbUrl: data.tbUrl
                        };
                        data.isSelected(true);
                        if (data.id) {
                            if (isGoogleDuplicateResource != data.id) {
                                isGoogleDuplicateResource = data.id;
                                $.when(manager.production.mediaPostResource(tempObj))
                                    .done(function (responseObj) {
                                        var item = mapper.resource.fromDto(responseObj.Data);
                                        item.directThumbUrl(data.tbUrl);
                                        item.isGoogleResource(true);
                                        data.guid = item.guid;
                                        listOfExternalResourceGuids.push(item.guid);
                                        lastSelectedBucketResource(item);
                                        checkInContentBucket(item);
                                        //console.log(responseObj);
                                    })
                                    .fail(function (responseObj) {
                                        logger.error("Server returned an error!");
                                    });
                            }
                        }
                        if (data.imageId) {
                            if (isGoogleDuplicateResource != data.imageId) {
                                isGoogleDuplicateResource = data.imageId;
                                $.when(manager.production.mediaPostResource(tempObj))
                                    .done(function (responseObj) {
                                        var item = mapper.resource.fromDto(responseObj.Data);
                                        item.directThumbUrl(data.tbUrl);
                                        item.isGoogleResource(true);
                                        data.guid = item.guid;
                                        listOfExternalResourceGuids.push(item.guid);
                                        lastSelectedBucketResource(item);
                                        checkInContentBucket(item);
                                        //console.log(responseObj);
                                    })
                                    .fail(function (responseObj) {
                                        logger.error("Server returned an error!");
                                    });
                            }
                        }
                    }
                    else {
                        lastSelectedBucketResource(data);
                        hidePopup(true);
                        var result = _.find(sdc.currentTemplate().contentBucket(), function (item) {
                            return item.id === data.id;
                        });

                        if (!result) {
                            sdc.currentTemplate().contentBucket.push(data);
                            sdc.currentTemplateElement().contentBucket(sdc.currentTemplate().contentBucket());

                            if (currentBunch().reportedContent().indexOf(data) > -1) {
                                var index = currentBunch().reportedContent().indexOf(data);
                                currentBunch().reportedContent()[index].isSelected(true);
                            }
                            else if (currentBunch().relatedNewsResources().indexOf(data) > -1) {
                                var index = currentBunch().relatedNewsResources().indexOf(data);
                                currentBunch().relatedNewsResources()[index].isSelected(true);
                            }
                            else {
                                var index = sdc.currentTemplate().contentBucket.indexOf(data);
                                data.isSelected(true);
                                sdc.currentTemplate().contentBucket()[index].isSelected(true);
                            }
                        }
                    }
                }
            },
            enableUserFavouriteSearch = function () {
                isFavouriteSearchEnable(true);
                isAllSearchEnable(false);
                isGoogleSearchEnabled(false);
                isAdvanceSearchVisible(false);
                isIframeVisible(false);
                filteredFavouriteSearch();
            },

            checkInContentBucket = function (data) {
                if (data) {
                    onResourceCheckClick(true);
                    hidePopup(true);
                    if (data.tbUrl) {
                        if (data.isSelected()) {
                            _.filter(sdc.currentTemplate().contentBucket(), function (item) {
                                if (item.guid === data.guid) {
                                    removeContentFromBuckets(item);
                                    sdc.currentTemplate().contentBucket.valueHasMutated();
                                }
                            });
                        }
                        else {
                            addToContentBucket(data);
                        }
                    }

                    else {
                        lastSelectedBucketResource(data);
                        var result = _.find(sdc.currentTemplate().contentBucket(), function (item) {
                            return item.id === data.id;
                        });
                        if (!result) {
                            sdc.currentTemplate().contentBucket.push(data);
                            sdc.currentTemplateElement().contentBucket(sdc.currentTemplate().contentBucket());
                            if (currentBunch().reportedContent().indexOf(data) > -1) {
                                var index = currentBunch().reportedContent().indexOf(data);
                                currentBunch().reportedContent()[index].isSelected(true);
                            }
                            else if (currentBunch().relatedNewsResources().indexOf(data) > -1) {
                                var index = currentBunch().relatedNewsResources().indexOf(data);
                                currentBunch().relatedNewsResources()[index].isSelected(true);
                            }
                            else {
                                var index = sdc.currentTemplate().contentBucket.indexOf(data);
                                sdc.currentTemplate().contentBucket()[index].isSelected(true);
                            }
                        }
                        else {
                            if (currentBunch().reportedContent().indexOf(data) > -1) {
                                var index = currentBunch().reportedContent().indexOf(data);
                                currentBunch().reportedContent()[index].isSelected(false);
                                data.isSelected(false);
                            }
                            else if (currentBunch().relatedNewsResources().indexOf(data) > -1) {
                                var index = currentBunch().relatedNewsResources().indexOf(data);
                                currentBunch().relatedNewsResources()[index].isSelected(false);
                                data.isSelected(false);
                            }
                            else {
                                var index = sdc.currentTemplate().contentBucket.indexOf(data);
                                sdc.currentTemplate().contentBucket()[index].isSelected(true);
                            }

                            if (sdc.currentTemplate().contentBucket().indexOf(data) > -1) {
                                sdc.currentTemplate().contentBucket().remove(data);
                                data.isSelected(false);
                                sdc.currentTemplate().contentBucket.valueHasMutated();
                            }
                        }
                    }
                }
            },
            onTemplateElementClick = function (data) {
                if (!isCropping()) {
                    searchKeywordsResource('');
                    if (appdata.currentUser().userType === e.UserType.NLE && !isVideoEditToolClick() && data.screenElementId != e.ScreenElementType.Anchor) {
                        openUploadDilogBox();
                    }
                    sdc.isGalleryVisible(true);
                    if (isVideoEditToolClick())
                    { }
                    else {
                        if (data) {
                            if (dc.news.getLocalById(currentStory().news.id)) {
                                currentStory().news.bunchTags(dc.news.getLocalById(currentStory().news.id).bunchTags());
                            }
                            contentUploader.uploadedResources([]);
                            if (data.screenElementId != e.ScreenElementType.Anchor) {
                                sdc.isGalleryVisible(true);
                            }
                            showBucket(true);
                            sdc.currentTemplateElement(data);

                            if (data.screenElementId === e.ScreenElementType.Anchor) {
                                sdc.isGalleryVisible(false);
                                return;
                            } else if (data.screenElementId === e.ScreenElementType.Video) {
                                toggleResourceFilter(e.ContentType.Video, true);
                                isVideoTemplate(true);
                                if (sdc.currentTemplateElement().contentBucket().length > 0 && sdc.currentTemplate().templateElements().length != 1) {
                                    sourceFilterControl.currentFilter('media');
                                    showBucket(true);
                                    sdc.currentTemplateElement(data);
                                    isMediaSelectionVisible(true);
                                    pdc.isVisible(false);
                                } else {
                                    sourceFilterControl.currentFilter('media');
                                    showBucket(true);
                                    if (sdc.currentTemplateElement().contentBucket().length <= 0 || sdc.currentTemplate().templateElements().length == 1) {
                                        if (!isNleUploadedResources()) {
                                            isMediaSelectionVisible(true);
                                            pdc.isVisible(false);
                                        }
                                    }

                                    sdc.currentTemplateElement(data);
                                }
                            }
                            else if (data.screenElementId === e.ScreenElementType.MediaWall) {
                                toggleResourceFilter(e.ContentType.Video, true);
                                isVideoTemplate(true);
                                if (sdc.currentTemplateElement().contentBucket().length > 0) {
                                    sourceFilterControl.currentFilter('media');
                                    showBucket(true);
                                    sdc.currentTemplateElement(data);
                                } else {
                                    sourceFilterControl.currentFilter('media');
                                    showBucket(true);
                                    if (sdc.currentTemplateElement().contentBucket().length <= 0) {
                                        isMediaSelectionVisible(true);
                                        pdc.isVisible(false);
                                    }

                                    sdc.currentTemplateElement(data);
                                }
                            }
                            else if (data.screenElementId === e.ScreenElementType.Graphic ||
                                data.screenElementId === e.ScreenElementType.Picture) {
                                if (data.screenElementId === e.ScreenElementType.Picture) {
                                    isImageTemplate(true);
                                    toggleResourceFilter(e.ContentType.Image, true);
                                }
                                sourceFilterControl.currentFilter('media');
                                showBucket(true);
                                if (sdc.currentTemplateElement().contentBucket().length <= 0 || sdc.currentTemplate().templateElements().length == 1) {
                                    pdc.isVisible(false);
                                    isMediaSelectionVisible(true);
                                }
                                sdc.currentTemplateElement(data);
                            } else if (data.screenElementId === e.ScreenElementType.Guest) {
                                sourceFilterControl.currentFilter('guest');
                                if (sdc.currentTemplateElement().contentBucket().length <= 0) {
                                    pdc.isVisible(false);
                                    isMediaSelectionVisible(true);
                                }
                                showBucket(false);
                                $('#bucket').css({ 'display': 'none' });
                                sdc.currentTemplateElement(data);
                            } else if (data.screenElementId === e.ScreenElementType.Logo) {
                                return;
                            } else if (data.screenElementId === e.ScreenElementType.Headline) {
                                return;
                            } else if (data.screenElementId === e.ScreenElementType.Ticker) {
                                return;
                            }
                            sdc.currentTemplate().contentBucket(sdc.currentTemplateElement().contentBucket());
                        }

                        if (isMediaSelectionVisible()) {
                            if (currentStory().news.bunchTags && currentStory().news.bunchTags().length > 0) {
                                searchOnSuggestedKeyword(currentStory().news.bunchTags()[0].Tag);
                            }
                            else {
                                resouceSearchKeyword('');
                                searchKeywordsResource('');
                            }
                        }
                    }
                    sdc.currentTemplate().contentBucket(sdc.currentTemplateElement().contentBucket());
                }
            },
            searchByTag = function (selectedTag) {
                searchOnSuggestedKeyword(selectedTag);
            },
            openVideoInEditor = function (data) {
                if (data) {
                    var arr = _.filter(sdc.currentTemplate().contentBucket(), function (item) {
                        return item.guid === sdc.currentTemplateElement().imageGuid && item.type === e.ContentType.Image;
                    });

                    if (arr && arr.length > 0) {
                        setForCropping(arr[0]);
                    }
                    else {
                        sdc.currentTemplateElement(data);
                        var videos = [];
                        isVideoEditToolClick(true);
                        setTimeout(function () { isVideoEditToolClick(false) }, 500);
                        for (var i = 0; i < sdc.currentTemplate().contentBucket().length > 0; i++) {
                            if (sdc.currentTemplate().contentBucket()[i].type === e.ContentType.Video) {
                                var tempObj = {};
                                var resourceGuid = sdc.currentTemplate().contentBucket()[i].guid || sdc.currentTemplate().contentBucket()[i].id;
                                tempObj['Guid'] = '"' + resourceGuid + '"';
                                tempObj['ResourceTypeId'] = e.ContentType.Video;
                                tempObj['ResourceId'] = i + 1;
                                videos.push(tempObj);
                            }
                        }
                        if (videos && videos.length > 0) {
                            var elements = [];

                            for (var i = 0; i < sdc.currentTemplate().templateElements().length; i++) {
                                elements.push(mapper.templateElement.toDto(sdc.currentTemplate().templateElements()[i]));
                                elements[i].ImageGuid = '\"' + elements[i].ImageGuid + '\"';
                            }

                            var args = {
                                ScreenTemplateId: sdc.currentTemplate().id,
                                StoryScreenTemplateId: sdc.currentTemplate().storyScreenTemplateId,
                                StoryId: currentStory().storyId,
                                EpisodeId: currentStory().episodeId,
                                ThumbGuid: '\"' + sdc.currentTemplate().thumbGuid() + '\"',
                                BackgroundImageUrl: '\"' + sdc.currentTemplate().templateBackground + '\"',
                                Elements: elements
                            };

                            if (!appdata.videoEditorToken)
                                appdata.videoEditorToken = utils.generateUid('-');

                            var argument = 'nms:' + JSON.stringify(videos) + '&*&' + config.MediaServerUrl + '&*&' + sdc.currentTemplateElement().storyTemplateScreenElementId + '&*&' + JSON.stringify(args) + '&*&' + appdata.currentUser().id + '&*&' + window.location.protocol + '//' + window.location.host + '/' + '&*&' + appdata.videoEditorToken + '&*&' + appdata.currentProgram().bucketId + '&*&' + appdata.currentProgram().apiKey;
                            $('#video-editor').attr('href', argument);
                            //$('.videocutterOverlay').show();
                            utils.simulateHumanClick(document.getElementById('video-editor'));
                            appdata.showCutterImageLoader(true);
                        }
                    }
                }
            },
            toggleRelatedTemplates = function (data, templateIndex) {
                if (data && !isNaN(templateIndex)) {
                    pdc.currentTemplate(data);
                    pdc.templateIndex = templateIndex;
                }
                pdc.showRelatedTemplate(!pdc.showRelatedTemplate());
            },
            toggleSelectedTemplate = function (data) {
                if (data) {
                    for (var i = 0; i < data.relatedTemplates().length; i++) {
                        data.relatedTemplates()[i].isSelected(false);
                    }
                    data.isSelected(!data.isSelected());
                }
            },
            onBucketResourceClick = function (data) {
                var index = sdc.currentTemplate().contentBucket.indexOf(data);
                for (var i = 0; i < sdc.currentTemplate().contentBucket().length; i++) {
                    sdc.currentTemplate().contentBucket()[i].isSelected(false);
                }
                if (index > -1) {
                    data.isSelected(true);
                    placeContentOnTemplateElement(sdc.currentTemplate().contentBucket()[index]);
                }
            },
            openUploadDilogBox = function (data) {
                $(".uploads").click();
                if (!$(".meidaFilesDtls").parents('li').hasClass('active')) {
                    $(".meidaFilesDtls").click();
                }
            },
            toggleContentSelection = function (data) {
                searchKeywordsResource('');

                if (isMediaSelectionVisible() == true && sourceFilterControl.currentFilter() == 'guest' && currentGuest()) {
                    if (currentGuest().imageGuid() != "") {
                        var guest = {
                            id: currentGuest().imageGuid()
                        };
                        placeContentOnTemplateElement(guest);
                        sdc.currentTemplateElement().guestId = currentGuest().id;
                        sdc.currentTemplate().isGuestSelected(true);

                        var requestObj = {
                            CelebrityId: currentGuest().id,
                            SlotTemplateScreenElementId: sdc.currentTemplateElement().id
                        }

                        manager.production.updateCelebrityStatistics(requestObj);
                    }

                    showBucket(true);
                    $('#bucket').css({ 'display': 'initial' });
                }
                if (!changeGuestFlag() && sdc.currentTemplate().contentBucket().length > 0) {
                    //placeContentOnTemplateElement(sdc.currentTemplate().contentBucket()[0]);
                }
                isMediaSelectionVisible(!isMediaSelectionVisible());
                pdc.isVisible(!pdc.isVisible());
                if (currentBunch().resourceFilter().indexOf(3) > -1) {
                    var index = currentBunch().resourceFilter.indexOf(3);
                    currentBunch().resourceFilter.splice(index, 1);
                }
                if (currentBunch().resourceFilter().indexOf(1) > -1) {
                    var index = currentBunch().resourceFilter.indexOf(1);
                    currentBunch().resourceFilter.splice(index, 1);
                }

                if (sdc.currentTemplateElement && sdc.currentTemplateElement().id && sdc.currentTemplate().contentBucket().length > 0) {
                    for (var i = 0; i < sdc.currentTemplate().contentBucket().length; i++) {
                        sdc.currentTemplate().contentBucket()[i].isSelected(false);
                    }
                    sdc.currentTemplate().contentBucket()[0].isSelected(true);
                    placeContentOnTemplateElement(sdc.currentTemplate().contentBucket()[0]);
                }
                RefreshTemplateStatus(false);
            },
            setGuestIdsForEpisode = function () {
                if (appdata.currentEpisode()) {
                    var currentTemplate = sdc.currentTemplate();
                    var currentTemplateElement = sdc.currentTemplateElement();

                    var guestId = currentTemplateElement.elementGuestId();

                    if (guestId) {
                        var episodeGuestIds = appdata.currentEpisode().episodeGuestIds;

                        if (episodeGuestIds().indexOf(guestId) == -1)
                            episodeGuestIds.push(guestId);
                        else {
                            var index = episodeGuestIds().indexOf(guestId);
                            episodeGuestIds.splice(index, 1);
                        }
                    }
                }
            },
            saveRelatedTemplate = function () {
                var template = _.find(pdc.currentTemplate().relatedTemplates(), function (temp) {
                    return temp.isSelected();
                });
                if (template) {
                    pdc.currentStory().screenFlow().screenTemplates()[pdc.templateIndex] = template;
                    pdc.currentStory().screenFlow().screenTemplates.valueHasMutated();
                    template.isSelected(false);
                }
                toggleRelatedTemplates();
            },
            displayTemplateKeyIcon = function () {
                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    if (dc.screenTemplates.getLocalById(sdc.currentTemplate().id)) {
                        if (dc.screenTemplates.getLocalById(sdc.currentTemplate().id).templateKeys && dc.screenTemplates.getLocalById(sdc.currentTemplate().id).templateKeys().length > 0) {
                            if (sdc.currentTemplate().templateKeys().length <= 0) {
                                var screenTemplate = dc.screenTemplates.getLocalById(sdc.currentTemplate().id);
                                var tempScreenTemplate = mapper.screenTemplate.getClone(screenTemplate);
                                sdc.currentTemplate().templateKeys(tempScreenTemplate.templateKeys());
                            }
                            sdc.isTemplateKeyVisible(true);
                        } else {
                            sdc.isTemplateKeyVisible(false);
                        }
                    }
                }
            },
            setCurrentTemplateForContentSelection = function (data, index) {
                if (data && !isNaN(index)) {
                    sdc.isMediaWallVisible(false);
                    sdc.currentTemplate(data);
                    sdc.currentTemplate().contentBucket([]);
                    sdc.templateIndex(index);
                    sdc.isTemplateSelected(true);
                    isTextEditorChangeAllowed(false);
                    textEditorBuffer(data.script());
                    $("#UN").click();
                    $('#script-editor').val(data.script());
                    $("#txtEditor1").html(data.script());
                    displayTemplateKeyIcon();
                    sdc.lastSelectedTemplateIndex = index;

                    manager.production.getSlotScreenTemplateComments(data.storyScreenTemplateId);
                    if (appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                        sdc.currentTemplate().isAssignToStoryWriter(true);
                    }
                    sdc.currentTemplateElement(sdc.currentTemplate().templateElements()[0]);
                    if (sdc.currentTemplate().contentBucket && sdc.currentTemplateElement && sdc.currentTemplateElement())
                        sdc.currentTemplate().contentBucket(sdc.currentTemplateElement().contentBucket());

                    for (var i = 0; i < sdc.currentTemplate().contentBucket().length; i++) {
                        if (sdc.currentTemplate().contentBucket()[i].guid == sdc.currentTemplateElement().imageGuid) {
                            sdc.currentTemplate().contentBucket()[i].isSelected(true);
                        }
                    }
                    sdc.currentTemplate().refreshTemplateStatus(false);
                    sdc.currentTemplate().refreshTemplateStatus(true);

                    if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType == e.UserType.HeadlineProducer) {
                        setTimeout(function () {
                            if (sdc.currentTemplate().name != "AnchorOnly") {
                                sdc.isGalleryVisible(true)
                                $(".viewAll").click();
                            }
                            else {
                                sdc.isGalleryVisible(false);
                            }
                        }, 500);
                    }
                }
            },
            showContentSelectionArea = function () {
                pdc.isVisible(false);
                isMediaSelectionVisible(true);
            },
            RefreshTemplateStatus = function (isRefreshFlow) {
                if (isRefreshFlow) {
                    var arr = _.filter(dc.stories.getObservableList(), function (item) {
                        return item.episodeId === appdata.currentEpisode().id;
                    });
                    for (var i = 0; i < arr.length; i++) {
                        for (var j = 0; j < arr[i].screenFlow().screenTemplates().length; j++) {
                            arr[i].screenFlow().screenTemplates()[j].refreshTemplateStatus(true);
                        }
                    }
                }
                else {
                    for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                        currentStory().screenFlow().screenTemplates()[i].refreshTemplateStatus(true);
                    }
                }
            },
            placeContentOnTemplateElement = function (data) {
                if (sdc.isResourceCommentBoxVisible()) {
                } else {
                    if (data && data.id) {
                        sdc.currentTemplateElement().imageGuid = data.guid || data.id;
                        var elements = [];

                        for (var i = 0; i < sdc.currentTemplate().templateElements().length; i++) {
                            elements.push(mapper.templateElement.toDto(sdc.currentTemplate().templateElements()[i]));
                        }

                        var requestObj = {
                            ScreenTemplateId: sdc.currentTemplate().id,
                            ThumbGuid: sdc.currentTemplate().thumbGuid(),
                            BackgroundImageUrl: sdc.currentTemplate().templateBackground,
                            Elements: elements
                        };

                        $.when(manager.production.generateScreenTemplate(requestObj))
                        .done(function (responseObj) {
                            sdc.currentTemplate().templateDisplayImage(responseObj.Data);
                            changeGuestFlag(false);
                            if (sdc.currentTemplateElement().screenElementId === e.ScreenElementType.Video) {
                                sdc.currentTemplate().videoDuration(data.duration);
                            }
                            else if (sdc.currentTemplateElement().screenElementId === e.ScreenElementType.MediaWall) {
                                sdc.currentTemplate().videoDuration(data.duration);
                            }

                            sdc.currentTemplate().isFreshImageGenerated = true;
                        })
                        .fail(function (responseObj) {
                            logger.error("Server returned an error!");
                        });
                    }
                }
            },
            changeStep = function (direction, storyNavFlag) {
                hideTabs();
                RefreshTemplateStatus(false);
                sdc.isMediaWallVisible(false);
                if (direction) {
                    switch (currentStep()) {
                        case e.ProgramDesignerStep.TemplateSelection:
                            saveStoryScreenFlowOnServer();
                            sdc.templateIndex(0);
                            currentStep(e.ProgramDesignerStep.ContentSelection);
                            if (currentStory().screenFlow().screenTemplates() && currentStory().screenFlow().screenTemplates().length > 0) {
                                setTimeout(function () {
                                    setCurrentTemplateForContentSelection(currentStory().screenFlow().screenTemplates()[0], 0);
                                }, 1000);
                            } else {
                                sdc.isTemplateSelected(false);
                            }
                            productionlp.isStoryBucketVisible(false);
                            //navigateStory(true, 0);
                            break;
                        case e.ProgramDesignerStep.ContentSelection:
                            for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                                currentStory().screenFlow().screenTemplates()[i].isAssignToNLE(true);
                                currentStory().screenFlow().screenTemplates()[i].isAssignToStoryWriter(true);
                                manager.production.saveStoryScreenTemplateOnServer(currentStory().screenFlow().screenTemplates()[i], currentStory().storyId);
                                appdata.isRefreshStoryStatus(!appdata.isRefreshStoryStatus());
                                RefreshTemplateStatus(true);
                            }
                            prc.computeRundownTime();
                            prc.isActive(true);
                            currentStep(e.ProgramDesignerStep.Rundown);

                            if (!sdc.isMediaWallVisible()) {
                                logger.success("Screen flow successfully saved!");
                            }
                            break;
                        case e.ProgramDesignerStep.Rundown:
                            currentStep(e.ProgramDesignerStep.Finished);
                            break;
                        default:
                            break;
                    }
                } else {
                    switch (currentStep()) {
                        case e.ProgramDesignerStep.ContentSelection:
                            if (!storyNavFlag)
                                navigateStory(true, 0);

                            currentStep(e.ProgramDesignerStep.TemplateSelection);
                            break;
                        case e.ProgramDesignerStep.Rundown:
                            navigateStory(true, 0);
                            prc.isActive(false);
                            currentStep(e.ProgramDesignerStep.ContentSelection);
                            setCurrentTemplateForContentSelection(currentStory().screenFlow().screenTemplates()[0], 0);
                            break;
                        case e.ProgramDesignerStep.Finished:
                            currentStep(e.ProgramDesignerStep.Rundown);
                            break;
                        default:
                            break;
                    }
                }
            },
            allowToSendRundown = function (isallowed) {
                if (isallowed) {
                    manager.production.sendProgramToBroadcast();
                }
                $("#content-viewer-main").children('#contentviewer-overlay').removeClass('warningOverlay');
                isWarningBoxVisible(false);
            },
            getAllGuidsList = function () {
            },
            sendToBroadCast = function () {
                var Guid = [];
                var arr = _.filter(dc.stories.getObservableList(), function (item) {
                    return item.episodeId === appdata.currentEpisode().id;
                });
                for (var i = 0; i < arr.length; i++) {
                    for (var j = 0; j < arr[i].screenFlow().screenTemplates().length; j++) {
                        for (var k = 0; k < arr[i].screenFlow().screenTemplates()[j].templateElements().length; k++) {
                            for (var m = 0; m < arr[i].screenFlow().screenTemplates()[j].templateElements()[k].contentBucket().length; m++) {
                                Guid.push(arr[i].screenFlow().screenTemplates()[j].templateElements()[k].contentBucket()[m].guid);
                            }
                        }
                    }
                }
                var lisGuids = {
                    Guids: Guid
                };
                if (Guid && Guid.length > 0) {
                    $.when(manager.production.getMediaResourceDownloadStatus(lisGuids))
                     .done(function (responseObj) {
                         if (!responseObj.IsSuccess) {
                             isWarningBoxVisible(true);
                             downloadCount(responseObj.Data.DownloadedCount);
                             pendingCount(responseObj.Data.pendingCount);
                             $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                         }
                         else if (responseObj.IsSuccess) {
                             downloadCount(responseObj.Data.DownloadedCount);
                             pendingCount(responseObj.Data.pendingCount);
                             manager.production.sendProgramToBroadcast();
                             allowToSendRundown(false);
                         }
                     })
                    .fail(function (responseObj) {
                        manager.production.sendProgramToBroadcast();
                    });
                }
                else {
                    manager.production.sendProgramToBroadcast();
                }
            },
            saveProductionTeamData = function () {
                if (appdata.currentUser().userType === e.UserType.NLE) {
                    saveScreenTemplateDataForNLE();
                    setTimeout(function () {
                        for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                            manager.production.saveProductionTeamData(currentStory().screenFlow().screenTemplates()[i], currentStory().storyId);
                        }
                    }, 1000);
                }
                else {
                    for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                        manager.production.saveProductionTeamData(currentStory().screenFlow().screenTemplates()[i], currentStory().storyId);
                    }
                }

                config.logger.success("Saved successfully!");
                router.navigateTo(config.hashes.production.pendingStories);
            },
            navigateStory = function (direction, storyIndex) {
                if (currentStep() === e.ProgramDesignerStep.TemplateSelection) {
                    saveStoryScreenFlowOnServer();
                } else if (currentStep() === e.ProgramDesignerStep.ContentSelection) {
                    for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                        manager.production.saveStoryScreenTemplateOnServer(currentStory().screenFlow().screenTemplates()[i], currentStory().storyId);
                    }
                }

                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    if (typeof (storyIndex) == 'undefined') {
                        if (direction && currentStoryIndex() < storyCount() - 1) {
                            currentStoryIndex(currentStoryIndex() + 1);
                        }

                        if (!direction && currentStoryIndex() > 0) {
                            currentStoryIndex(currentStoryIndex() - 1);
                        }
                        sdc.reset();
                        sdc.isTemplateCommentBoxVisible(true);

                        if (currentStory().screenFlow().screenTemplates() && currentStory().screenFlow().screenTemplates().length > 0) {
                            setTimeout(function () {
                                setCurrentTemplateForContentSelection(currentStory().screenFlow().screenTemplates()[0], 0);
                            }, 1000);
                        } else {
                            changeStep(false, true);
                        }

                        appdata.updateSuggestedFlowFlag(!appdata.updateSuggestedFlowFlag());
                    } else if (storyIndex >= 0 && storyIndex < storyCount() - 1) {
                        currentStoryIndex(storyIndex);
                    }
                } else if (appdata.currentUser().userType === e.UserType.NLE || appdata.currentUser().userType === e.UserType.StoryWriter) {
                    if (direction && currentPendingStoryIndex() < pendingStoryList().length - 1) {
                        currentPendingStoryIndex(currentPendingStoryIndex() + 1);
                    }
                    if (!direction && currentPendingStoryIndex() > 0) {
                        currentPendingStoryIndex(currentPendingStoryIndex() - 1);
                    }
                    currentPendingStory(pendingStoryList()[currentPendingStoryIndex()]);
                }
                RefreshTemplateStatus(false);
            },
            navigateToStory = function (story, storyScreenTemplateId) {
                if (story.id !== currentStory().id) {
                    var storiesArray = dc.stories.getByEpisodeId(story.episodeId)();
                    if (storiesArray && storiesArray.length > 0) {
                        storiesArray = storiesArray.sort(function (entity1, entity2) {
                            return entity1.sequenceId > entity2.sequenceId;
                        });
                        for (var i = 0; i < storiesArray.length; i++) {
                            if (storiesArray[i].id === story.id) {
                                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                                    currentStoryIndex(i);

                                    appdata.updateSuggestedFlowFlag(!appdata.updateSuggestedFlowFlag());
                                } else
                                    currentPendingStory(storiesArray[i]);
                                break;
                            }
                        }
                    }
                }

                for (var i = 0; i < story.screenFlow().screenTemplates().length; i++) {
                    if (story.screenFlow().screenTemplates()[i].storyScreenTemplateId === storyScreenTemplateId) {
                        currentStep(e.ProgramDesignerStep.ContentSelection);
                        setTimeout(function () {
                            setCurrentTemplateForContentSelection(currentStory().screenFlow().screenTemplates()[i], i);
                        }, 1000);
                        break;
                    }
                }

                if (router.currentHash() != config.hashes.production.production) {
                    router.navigateTo(config.hashes.production.production);
                }
                presenter.toggleActivity(false);
            },
            navigateToEpisode = function (story, episode, storyScreenTemplateId) {
                if (story.episodeId === appdata.currentEpisode().id) {
                    navigateToStory(story, storyScreenTemplateId);
                } else {
                    if (episode.programId !== appdata.currentProgram().id) {
                        var program = dc.programs.getLocalById(episode.programId);
                        if (program)
                            appdata.currentProgram(program);
                    }
                    appdata.programSelectedDate(moment(moment(episode.startTime()).format('l')).toISOString());
                    appdata.currentEpisode(episode);
                    navigateToStory(story, storyScreenTemplateId);
                }
            },
            updateCurrentTemplateStatus = function (status) {
                if (status == e.ScreenTemplateStatus.ApprovalPending) {
                    if (appdata.currentUser().userType == e.UserType.NLE)
                        sdc.currentTemplate().nleStatusId(e.ScreenTemplateStatus.ApprovalPending);
                    if (appdata.currentUser().userType == e.UserType.StoryWriter)
                        sdc.currentTemplate().storywriterStatusId(e.ScreenTemplateStatus.ApprovalPending);

                }
                if (status == e.ScreenTemplateStatus.Approved) {
                    sdc.currentTemplate().nleStatusId(e.ScreenTemplateStatus.Approved);
                    sdc.currentTemplate().storywriterStatusId(e.ScreenTemplateStatus.Approved);
                }

                if (status == e.ScreenTemplateStatus.InProcess) {
                    if (sdc.isNleChat())
                        sdc.currentTemplate().nleStatusId(e.ScreenTemplateStatus.InProcess);
                    if (sdc.isStorywriterChat())
                        sdc.currentTemplate().storywriterStatusId(e.ScreenTemplateStatus.InProcess);
                }

                var reqObj = {
                    SlotScreenTemplateId: sdc.currentTemplate().storyScreenTemplateId,
                    ProgramId: appdata.currentProgram().id,
                    UserId: appdata.currentUser().id,
                    UserRoleId: appdata.currentUser().userType,
                }

                if (appdata.currentUser().userType == e.UserType.NLE) {
                    reqObj.NLEStatusId = sdc.currentTemplate().nleStatusId();
                }
                if (appdata.currentUser().userType == e.UserType.StoryWriter) {
                    reqObj.StoryWriterStatusId = sdc.currentTemplate().storywriterStatusId();
                }
                if (appdata.currentUser().userType == e.UserType.Producer) {
                    if (sdc.isNleChat() && sdc.currentTemplate().nleStatusId() === e.ScreenTemplateStatus.InProcess) {
                        reqObj.NLEStatusId = sdc.currentTemplate().nleStatusId();
                    }
                    if (sdc.isStorywriterChat() && sdc.currentTemplate().storywriterStatusId() === e.ScreenTemplateStatus.InProcess) {
                        reqObj.StoryWriterStatusId = sdc.currentTemplate().storywriterStatusId();
                    }
                    if (sdc.currentTemplate().nleStatusId() === e.ScreenTemplateStatus.Approved && sdc.currentTemplate().storywriterStatusId() === e.ScreenTemplateStatus.Approved && !sdc.isStorywriterChat() && !sdc.isNleChat()) {
                        reqObj.NLEStatusId = sdc.currentTemplate().nleStatusId();
                        reqObj.StoryWriterStatusId = sdc.currentTemplate().storywriterStatusId();
                    }
                }
                manager.production.markTemplateStatus(reqObj);
                if (appdata.currentUser().userType == e.UserType.NLE || appdata.currentUser().userType == e.UserType.StoryWriter) {
                    saveProductionTeamData();
                }
                if ((sdc.isNleChat() || sdc.isStorywriterChat()) && (commentBoxText() && commentBoxText().length > 0)) {
                    sendComments();
                }
                appdata.isRefreshStoryStatus(!appdata.isRefreshStoryStatus());
            },
            toggleProgramPreview = function (data, event, showFullPreview, isRegenerateFlag) {
                ppc.reset();

                if (ppc.isVisible()) {
                    ppc.isVisible(false);
                    return;
                }

                var isStoryPreview = false;

                if (!isRegenerateFlag && !showFullPreview && data.previewGuid() && data.previewGuid().length > 0) {
                    ppc.programPreviewUrl(data.previewUrl())
                    ppc.isVisible(true);
                    return;
                }
                else if (!showFullPreview) {
                    isStoryPreview = true;
                }
                else {
                    var episode = appdata.currentEpisode();
                    if (!isRegenerateFlag && episode.previewGuid() && episode.previewGuid().length > 0) {
                        ppc.programPreviewUrl(episode.previewUrl())
                        ppc.isVisible(true);
                        return;
                    }
                }
                btnAttribute({ text: "Generating Preview", disabled: true });
                manager.production.generateEpisodePreview(btnAttribute, ppc, isStoryPreview, data);
            },
            setCurrentContent = function (data, options) {
                if (!onResourceCheckClick()) {
                    try {
                        if (data && options) {
                            if (options.isArchiveContent) {
                                contentViewerVM.setCurrentContent(archivalResources(), data);
                            } else if (options.isReportedContent) {
                                contentViewerVM.setCurrentContent(currentStory().news.resources(), data);
                            } else if (options.isRelatedContent) {
                                contentViewerVM.setCurrentContent(currentBunch().relatedNewsResources(), data);
                            }
                            else if (options.isContentBucket) {
                                contentViewerVM.setCurrentContent(sdc.currentTemplate().contentBucket(), data);
                            }
                            else if (options.isGoogleContent) {
                                var isVideo = (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) ? true : false;
                                contentViewerVM.setCurrentContentOfGoogle(mediaSearchGoogleList(), data, isVideo);
                            }
                            else if (options.isUserFavouriteResource) {
                                contentViewerVM.setCurrentContent(appdata.userFavourites(), data);
                            }
                        }
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
                else {
                    onResourceCheckClick(false);
                }
            },
            setCurrentPendingStory = function (data, pendingStoriesList, index) {
                if (data && pendingStoriesList && pendingStoriesList.length > 0) {
                    pendingStoryList(pendingStoriesList);
                    currentPendingStory(data);
                    currentPendingStoryIndex(index);

                    sdc.templateIndex(0);
                    setTimeout(function () {
                        setCurrentTemplateForContentSelection(data.screenFlow().screenTemplates()[0], 0);
                    }, 1000);
                }
            },

            setArchivalResource = function () {
                if (resouceSearchKeyword()) {
                    var requestObj = {
                        term: resouceSearchKeyword(),
                        pageSize: pageSize(),
                        pageNumber: pageNumber(),
                        bucketId: dc.programs.getLocalById(appdata.currentEpisode().programId).bucketId ? dc.programs.getLocalById(appdata.currentEpisode().programId).bucketId : 0,
                        alldata: isAllSearchEnable(),
                        resourceTypeId: currentBunch().resourceFilter && currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0
                    };
                    $.when(manager.production.searchContentArchive(requestObj, archivalServerCount, searchTags(), isAdvanceSearchVisible()))
                        .done(function (responseObj) {
                            archivalResources(responseObj);
                        })
                        .fail(function (responseObj) {
                        });
                }
            },
            removeScreenTemplate = function (data, ui) {
                if (data.storyScreenTemplateId) {
                    $.when(manager.production.deleteSlotscreentemplate(data.storyScreenTemplateId))
                      .done(function (responseData) {
                          if (responseData.Data) {
                              if (pdc.currentStory().screenFlow().screenTemplates().indexOf(data) > -1) {
                                  pdc.currentStory().screenFlow().screenTemplates().remove(data);
                                  pdc.currentStory().screenFlow().screenTemplates.valueHasMutated();

                                  if (pdc.currentStory().screenFlow().screenTemplates().length === 0) {
                                      pdc.currentStory().screenFlow().isDirty = false;
                                  } else {
                                      pdc.currentStory().screenFlow().isDirty = true;
                                  }
                                  if (!data.isMediaWallTemplate()) {
                                      logger.success("Template successfully deleted!");

                                      $(ui.currentTarget.parentElement).remove();
                                  }
                              }

                              appdata.updateSuggestedFlowFlag(!appdata.updateSuggestedFlowFlag());
                          }
                      })
                  .fail(function () {
                      logger.error("Unable to delete template due to some error!");
                  });
                } else {
                    if (pdc.currentStory().screenFlow().screenTemplates().indexOf(data) > -1) {
                        pdc.currentStory().screenFlow().screenTemplates().remove(data);
                        pdc.currentStory().screenFlow().screenTemplates.valueHasMutated();

                        if (pdc.currentStory().screenFlow().screenTemplates().length === 0) {
                            pdc.currentStory().screenFlow().isDirty = false;
                        } else {
                            pdc.currentStory().screenFlow().isDirty = true;
                        }

                        if (!data.isMediaWallTemplate()) {
                            logger.success("Template successfully deleted!");

                            $(ui.currentTarget.parentElement).remove();
                        }

                        appdata.updateSuggestedFlowFlag(!appdata.updateSuggestedFlowFlag());
                    }
                }
            },
            arrangeStoryScreenTemplates = function (arrayPositions) {
                if (arrayPositions.length === pdc.currentStory().screenFlow().screenTemplates().length) {
                    for (var i = 0; i < arrayPositions.length; i++) {
                        var template = _.find(pdc.currentStory().screenFlow().screenTemplates(), function (template) {
                            return template.uid() == arrayPositions[i];
                        });

                        if (template) {
                            template.sequenceId = i + 1;
                        } else {
                        }
                    }
                    pdc.currentStory().screenFlow().screenTemplates.valueHasMutated();
                    pdc.currentStory().screenFlow().isDirty = true;
                }
            },
            nleUploadResouces = function () {
                var resources = [];

                for (var i = 0; i < contentUploader.mappedResources().length; i++) {
                    var resource = contentUploader.mappedResources()[i];
                    var tempObj = {
                        SlotScreenTemplateResourceId: resource.id,
                        SlotScreenTemplateId: sdc.currentTemplate().storyScreenTemplateId,
                        ResourceGuid: resource.guid,
                        Comment: resource.comment(),
                        IsUploadedFromNle: true,
                        ResourceTypeId: resource.type
                    };
                    resources.push(tempObj);
                    nleTempResources(resources);
                }
                if (sdc.currentTemplateElement && sdc.currentTemplateElement().id) {
                    for (var j = 0; j < resources.length; j++) {
                        var item = mapper.resource.fromDto(resources[j]);
                        sdc.currentTemplateElement().contentBucket.push(item);
                    }
                }
                else {
                    for (var j = 0; j < resources.length; j++) {
                        var item = mapper.resource.fromDto(resources[j]);
                        sdc.currentTemplate().templateElements()[0].contentBucket.push(resources[j]);
                    }
                }
                onTemplateElementClick(sdc.currentTemplate().templateElements()[0]);
                setTimeout(function () {
                    sdc.isGalleryVisible(true);
                }, 300);
            },
            saveScreenTemplateDataForNLE = function () {
                $.when(manager.production.saveScreenTemplateDataForNLE(sdc.currentTemplate(), nleTempResources()))
                .done(function (responseData) {
                    if (responseData.Data) {
                        for (var i = 0; i < contentUploader.mappedResources().length; i++) {
                            var arr = _.filter(sdc.currentTemplate().contentBucket(), function (item) {
                                return item.guid === contentUploader.mappedResources()[i].guid;
                            });
                            if (sdc.currentTemplateElement && sdc.currentTemplateElement().id) {
                                onTemplateElementClick(sdc.currentTemplateElement());
                            }
                        }
                        if (responseData.Data[0].SlotTemplateScreenElementResources && responseData.Data[0].SlotTemplateScreenElementResources.length > 0) {
                            for (var k = 0; k < responseData.Data[0].SlotTemplateScreenElementResources.length; k++) {
                                _.filter(sdc.currentTemplateElement().contentBucket(), function (item) {
                                    if (item.guid === responseData.Data[0].SlotTemplateScreenElementResources[k].ResourceGuid) {
                                        item.storyTemplateScreenElementResourceId = responseData.Data[0].SlotTemplateScreenElementResources[k].SlotTemplateScreenElementResourceId;
                                    }
                                });
                            }
                        }
                    }
                })
                .fail(function () {
                });
            },

            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    guestSearchKeywords(value);
                });
                FEResource.subscribe(function (value) {
                    console.log(value);
                });


                currentMetaSearchKeyword.subscribe(function (value) {
                    noAjaxRequestIsInProgress = true;
                    pageNumber(1);
                    if (value) {
                        var programBucketId = 0;
                        var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;

                        var requestObj = getArchivalRequestObject(value, pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());

                        getArchivalDataFromServer(requestObj, false, false);
                    }
                    else {
                        bufferRequestObject = {};
                    }
                });

                searchKeywordsResource.subscribe(function (value) {

                    pageNumber(1);
                    noAjaxRequestIsInProgress = true;
                    getCelebrity(value, 10, pageNumber());
                    var programBucketId = 0;
                    var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;

                    if (isFavouriteSearchEnable()) {
                        filteredFavouriteSearch();
                    }
                    else if (isGoogleSearchEnabled()) {
                        getGoogleData(value);
                    }
                    else {

                        var requestObj = getArchivalRequestObject(value, pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                        getArchivalDataFromServer(requestObj, false, false);
                    }
                    if (!value.trim()) {
                        archivalServerCount(0);
                        archivalResources([]);
                        mediaSearchGoogleList([]);
                        googleSearchResult = [];
                        suggestedKeywords([]);
                        bufferRequestObject = {};
                        start = 0;
                    }
                });

                resourceFilterChange.subscribe(function (value) {
                    noAjaxRequestIsInProgress = true;
                    pageNumber(1);
                    if (isFavouriteSearchEnable()) {
                        filteredFavouriteSearch(searchKeywordsResource());
                    }
                    else if (isGoogleSearchEnabled()) {
                        getGoogleData(searchKeywordsResource());
                    }
                    else {
                        var programBucketId = dc.programs.getLocalById(dc.episodes.getLocalById(currentStory().episodeId).programId).bucketId ? dc.programs.getLocalById(dc.episodes.getLocalById(currentStory().episodeId).programId).bucketId : 0;
                        var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                        var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                        getArchivalDataFromServer(requestObj, false, false);
                    }
                });

                currentGuest.subscribe(function (value) {
                    changeGuestFlag(true);
                });

                appdata.isProducerUploadResource.subscribe(function (value) {
                    var resources = [];
                    console.log(appdata.uploadProducerResources());
                    for (var i = 0; i < appdata.uploadProducerResources().length; i++) {
                        if (sdc.currentTemplateElement && sdc.currentTemplateElement().id) {
                            var arr = _.filter(sdc.currentTemplate().contentBucket(), function (item) {
                                return item.guid == appdata.uploadProducerResources()[i].Guid;
                            });
                            if (arr && arr.length <= 0) {
                                var tempResource = mapper.resource.fromDto(appdata.uploadProducerResources()[i]);
                                sdc.currentTemplate().contentBucket.push(tempResource);
                                sdc.currentTemplate().contentBucket.valueHasMutated();
                            }
                        }
                    }
                    appdata.uploadProducerResources([]);
                    appdata.isProducerUploadResource(false);
                });
            },

            downLoadResource = function (Url) {
                var link = document.createElement("a");
                link.download = Url()
                link.href = Url();
                link.click();
            },
            removeContentFromBuckets = function (data) {
                if (sdc.currentTemplate().contentBucket()) {
                    if (data.isSelected)
                        data.isSelected(false);
                    for (var i = 0; i < sdc.currentTemplate().contentBucket().length; i++) {
                        if (sdc.currentTemplate().contentBucket()[i].id === data.id) {
                            var id = data.storyTemplateScreenElementResourceId;
                            $.when(manager.production.deleteSlotScreenTemplateResource(id))
                             .done(function (responseData) {
                                 if (responseData.IsSuccess)
                                     if (currentBunch().reportedContent().indexOf(data) > -1) {
                                         var index = currentBunch().reportedContent().indexOf(data);
                                         currentBunch().reportedContent()[index].isSelected(false);
                                     }
                                     else if (currentBunch().relatedNewsResources().indexOf(data) > -1) {
                                         var index = currentBunch().relatedNewsResources().indexOf(data);
                                         currentBunch().relatedNewsResources()[index].isSelected(false);
                                     }
                                     else {
                                         var index = sdc.currentTemplate().contentBucket.indexOf(data);
                                         sdc.currentTemplate().contentBucket()[index].isSelected(false);
                                     }
                                 removeFromExternalResourceList(data);
                                 sdc.removeContentFromBucket(data);
                             })
                          .fail(function (responseObj) {
                              if (currentBunch().reportedContent().indexOf(data) > -1) {
                                  var index = currentBunch().reportedContent().indexOf(data);
                                  currentBunch().reportedContent()[index].isSelected(false);
                              }
                              else if (currentBunch().relatedNewsResources().indexOf(data) > -1) {
                                  var index = currentBunch().relatedNewsResources().indexOf(data);
                                  currentBunch().relatedNewsResources()[index].isSelected(false);
                              }
                              else {
                                  var index = sdc.currentTemplate().contentBucket.indexOf(data);
                                  sdc.currentTemplate().contentBucket()[index].isSelected(false);
                              }
                              removeFromExternalResourceList(data);
                              sdc.removeContentFromBucket(data);
                          });
                        }
                    }
                }
                if (data.isGoogleResource()) {
                    removeSelectedGoogleResource(data);
                }
            },
            removeSelectedGoogleResource = function (data) {
                _.filter(mediaSearchGoogleList(), function (item) {
                    if (item.guid === data.guid) {
                        item.isSelected(false);
                        if ((item.id && item.id == isGoogleDuplicateResource) || (item.imageId && item.imageId == isGoogleDuplicateResource)) {
                            isGoogleDuplicateResource = 0;
                        }
                    }
                });
            },
            removeFromExternalResourceList = function (data) {
                for (var i = 0; i < listOfExternalResourceGuids.length; i++) {
                    if (listOfExternalResourceGuids[i] == data.guid) {
                        listOfExternalResourceGuids.remove(listOfExternalResourceGuids[i]);
                    }
                }
            },
            setFaceAction = function () {
                sdc.currentTemplate().faceAction = currFaceAction();
            },
            assignToNle = function (data) {
                sdc.currentTemplate().isAssignToNLE(data);
            },
            assignTostoryWriter = function (data) {
                sdc.currentTemplate().isAssignToStoryWriter(data);
            },
            isCheckedImplementForAll = function (data) {
                implementToAll(data);
            },
            saveCommentsForContentBucket = function () {
                sdc.isResourceCommentBoxVisible(false);
                if (isImplementToAll()) {
                    for (var i = 0; i < sdc.currentTemplate().contentBucket().length; i++) {
                        sdc.currentTemplate().contentBucket()[i].comment(sdc.currentResource().comment());
                    }
                }
                else {
                    for (var i = 0; i < sdc.currentTemplate().contentBucket().length; i++) {
                        if (sdc.currentTemplate().contentBucket()[i].id == sdc.currentBucketData().id) {
                            sdc.currentTemplate().contentBucket()[i].comment(sdc.currentResource().comment());
                        }
                    }
                }
            },
            saveStoryScreenFlowOnServer = function () {
                if (pdc.currentStory().screenFlow().isDirty) {
                    $.when(manager.production.saveStoryScreenFlowOnServer(currentStory()))
                    .done(function () {
                        pdc.currentStory().screenFlow().isDirty = false;
                        if (!sdc.isMediaWallVisible()) {
                            logger.success("Screen flow successfully saved!");
                        }
                    })
                    .fail(function () {
                        logger.error("Unable to save screen flow due to some error!");
                    });
                }
            },
            hideTabs = function () {
                $('#tab1').addClass('tabHide');
                $('#tab2').addClass('tabHide');
                $('.proHead').children('ul.tabs').children('li').removeClass('active');
                $('body').removeClass("scrollpadding");
                $('html').removeClass("scrolstart scrolstart1");
            },
            editProgramFlow = function () {
                if (sdc.currentTemplate()) {
                    for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                        manager.production.saveStoryScreenTemplateOnServer(currentStory().screenFlow().screenTemplates()[i], currentStory().storyId);
                    }

                    changeStep(false, true);
                }
            },
            setVideoForProduction = function (data, event) {
                isVideoEditToolClick(true);

                sdc.currentTemplateElement(data);

                var arr = _.filter(sdc.currentTemplateElement().contentBucket(), function (item) {
                    return item.guid === data.imageGuid;
                });

                if (data.screenElementId == e.ScreenElementType.Video) {
                    arr[0].type = e.ContentType.Video;
                }
                else if (data.screenElementId == e.ScreenElementType.Picture) {
                    arr[0].type = e.ContentType.Image;
                }
                contentViewerVM.setCurrentContent(arr, arr[0]);
                setTimeout(function () { isVideoEditToolClick(false) }, 500);
            },
            removeUploadedFile = function (data) {
                if (contentUploader.mappedResources().indexOf(data) > -1) {
                    var arr = _.filter(contentUploader.uploadedResources(), function (item) {
                        return item.Guid === data.guid;
                    });
                    if (arr && arr.length > 0) {
                        var list = contentUploader.uploadedResources();
                        var resources = contentUploader.resources();
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].Guid == arr[0].Guid) {
                                list.remove(list[i]);
                                for (var j = 0; j < resources.length; j++) {
                                    if (resources[j].Guid == arr.Guid) {
                                        resources.remove(resources[j]);
                                    }
                                }
                            }
                        }
                        contentUploader.uploadedResources(list);
                        $.when(manager.news.deletevideofile(data.guid))
                          .done(function (responseData) {
                          })
                          .fail(function () {
                          });
                    }
                }
            },
            getEpisodeFromServer = function (requestObj, storyScreenTemplateId, story) {
                $.when(manager.production.getProgramEpisodes(requestObj, storyScreenTemplateId))
                .done(function () {
                    try {
                        if (!story) {
                            story = dc.stories.getLocalById(requestObj.slotId);
                        }
                        if (story) {
                            episode = dc.episodes.getLocalById(story.episodeId);
                            if (episode) {
                                navigateToEpisode(story, episode, storyScreenTemplateId);
                            }
                        }
                    } catch (e) {
                        //console.log(e.message);
                    }
                    presenter.toggleActivity(false);
                })
                .fail(function () {
                    presenter.toggleActivity(false);
                });
            },
            navigateToTemplateDesigner = function (storyId, storyScreenTemplateId) {
                presenter.toggleActivity(true);
                if (storyId && storyScreenTemplateId) {
                    var story = _.find(dc.stories.getAllLocal(), function (tempStory) {
                        return tempStory.storyId === storyId;
                    });
                    if (story) {
                        var episode = dc.episodes.getLocalById(story.episodeId);
                        if (episode) {
                            navigateToEpisode(story, episode, storyScreenTemplateId);
                        } else {
                            var requestObj = { episodeId: story.episodeId };
                            getEpisodeFromServer(requestObj, storyScreenTemplateId, story);
                        }
                    } else {
                        var requestObj = { slotId: storyId };
                        getEpisodeFromServer(requestObj, storyScreenTemplateId);
                    }
                }
            },
            onPreviewThumbClick = function (story, index, isStory) {

                var arr = _.filter(_dc.screenTemplates.getObservableList(), function (tempTemplate) {
                    return tempTemplate.programId === appdata.currentProgram().id;
                });

                if (arr && arr.length == 1 && isStory) {
                    isStory = false;
                }

                var arr = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                if (arr.length > 0) {
                    arr = arr.sort(function (entity1, entity2) {
                        return entity1.sequenceId > entity2.sequenceId;
                    });
                    var storyindex = arr.indexOf(story);
                    currentStoryIndex(storyindex);

                    pdc.currentStory(arr[currentStoryIndex()]);
                    sdc.templateIndex(index);
                    currentStep(e.ProgramDesignerStep.TemplateSelection);
                    if (!isStory) {
                        if (currentStory().screenFlow().screenTemplates() && currentStory().screenFlow().screenTemplates().length > 0) {
                            currentStep(e.ProgramDesignerStep.ContentSelection);

                            setTimeout(function () {
                                setCurrentTemplateForContentSelection(currentStory().screenFlow().screenTemplates()[index], index);
                            }, 1000);
                        } else {
                            sdc.isTemplateSelected(false);
                        }
                    }
                }
            },
            onTemplatethumbClick = function (index) {
                if (currentStep() === e.ProgramDesignerStep.TemplateSelection) {
                    saveStoryScreenFlowOnServer();
                    sdc.templateIndex(index);
                    currentStep(e.ProgramDesignerStep.ContentSelection);
                    if (currentStory().screenFlow().screenTemplates() && currentStory().screenFlow().screenTemplates().length > 0) {
                        setTimeout(function () {
                            setCurrentTemplateForContentSelection(currentStory().screenFlow().screenTemplates()[index], index);
                        }, 1000);
                    } else {
                        sdc.isTemplateSelected(false);
                    }
                }
                else {
                    changeStep(false, true);
                    currentStep(e.ProgramDesignerStep.TemplateSelection);
                }
            },
            convertToUnicodeText = function () {
                $.when(manager.production.getUrduUnicodeText($('#txtEditor1').val()))
                .done(function (convertedText) {
                    if (convertedText && convertedText.length > 0) {
                        textEditorBuffer(convertedText);
                        $('#txtEditor1').val(convertedText);
                    }
                })
                .fail(function () {
                });
            },

            //#region Archival Methods
            toggleResourceFilter = function (data) {
                currentBunch().resourceFilter([]);
                currentBunch().resourceFilter.push(data);
                resourceFilterChange(!resourceFilterChange());
                //  setArchivalResource();
            },
            markResourceAsFavourite = function (data) {
                var arr = _.filter(appdata.userFavourites(), function (item) {
                    return (item.guid === data.guid);
                });
                if (arr && arr.length > 0) {
                    config.logger.error("Already Marked Favourite!");
                }
                else {
                    data.isMarkedFavourite(true);
                    appdata.userFavourites.push(data);
                    userFavouriteResources.push(data);
                    manager.production.addUserFavouriteResource(data);
                }
            },
            deleteResourceFromFavourite = function (data) {
                if (data && data.isMarkedFavourite() && userFavouriteResources().indexOf(data) != 0) {
                    var index = userFavouriteResources().indexOf(data);
                    data.isMarkedFavourite(false);
                    manager.production.deleteUserFavouriteResource(data);
                    userFavouriteResources().remove(userFavouriteResources()[index]);
                    userFavouriteResources.valueHasMutated();
                }
            },
            filteredFavouriteSearch = function () {
                if (searchKeywordsResource().trim().length > 0) {
                    var arr = [];
                    for (var i = 0, len = appdata.userFavourites().length ; i < len; i++) {
                        var item = appdata.userFavourites()[i];
                        if (item.slug && item.slug.toLowerCase().trim().indexOf(searchKeywordsResource().toLowerCase().trim()) != -1) {
                            arr.push(item);
                        }
                    }
                    if (currentBunch().resourceFilter().length > 0) {
                        for (var i = 0, len = arr.length; i < len; i++) {
                            if (arr[i].type !== currentBunch().resourceFilter()[0]) {
                                arr.splice(i, 1);
                            }
                        }
                    }
                    userFavouriteResources(arr);
                }
                else {
                    userFavouriteResources(appdata.userFavourites());
                }
            },
            showArchivalDataForSuggestedkeyword = function (keyword) {
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(keyword, pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            getGoogleLst = function (value) {
                if (value)
                    showloader(true);
                if (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) {
                    videosPageNo = videosPageNo + 1;
                    $.ajax({
                        url: config.GoogleVideosApiUrl + value + "&page=" + videosPageNo + "&limit=50",
                        dataType: 'jsonp',
                        success: function (results) {
                            showloader(false);
                            if (results && results.list && results.list.length > 0) {
                                googleSearchResult = results.list;
                                for (var k = 0; k < googleSearchResult.length; k++) {
                                    var resource = googleSearchResult[k];
                                    if (resource.duration) {
                                        var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                        tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                        resource.duration = tempDuration;
                                    }
                                    resource["tbUrl"] = resource.thumbnail_url;
                                    resource["isSelected"] = ko.observable(false),
                                    resource["guid"] = '';
                                    mediaSearchGoogleList.push(resource);
                                }
                            }
                        },
                        error: function () {
                            showloader(false);
                        }
                    });
                }
                else {
                    $.getJSON(config.GoogleImageApiUrl + "&q=" + value + "&start=" + start + "&rsz=8&safe=active&callback=?", function (results) {
                        showloader(false);
                        if (results && results.responseData && results.responseData.results) {
                            googleSearchResult.push(results.responseData.results);
                            if (googleSearchResult.length < 7) {
                                start = start + 8;
                                //console.log(googleSearchResult);
                                getGoogleLst(value);
                            } else {
                                for (var k = 0; k < googleSearchResult.length; k++) {
                                    for (var j = 0; j < googleSearchResult[k].length; j++) {
                                        var resource = googleSearchResult[k][j];
                                        if (resource.duration) {
                                            var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                            tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                            resource.duration = tempDuration;
                                        }
                                        resource["isSelected"] = ko.observable(false),
                                        resource["guid"] = '';
                                        resource["id"] = resource.imageId;
                                        mediaSearchGoogleList.push(resource);
                                    }
                                }
                            }
                        }

                    });
                }
            },
            getGoogleData = function (value) {
                googleSearchResult = [];
                mediaSearchGoogleList([]);
                start = 0;
                videosPageNo = 0;
                getGoogleLst(value);
            },
            searchOnSuggestedKeyword = function (keyword) {
                debugger
                showArchivalDataForSuggestedkeyword(keyword);
                searchKeywordsResource(keyword);
                setTimeout(function () {
                    suggestedKeywords([]);
                }, 600);
            },
            uploadFile = function () {
                openUploadDilogBox();
            },
            loadMoreArchivalData = function () {
                if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    var pageNo = pageNumber() + 1;

                    var term = searchKeywordsResource();

                    var programBucketId = 0;
                    var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                    var requestObj = getArchivalRequestObject(term, pageSize(), pageNo, programBucketId, resourceFiterId, isAllSearchEnable());
                    getArchivalDataFromServer(requestObj, true, false);

                    if (isGoogleSearchEnabled())
                        loadMoreGoogleData();
                }
            },
            loadMoreGoogleData = function () {
                showloader(true);
                presenter.toggleActivity(true);
                if (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) {
                    googleSearchResult = [];
                    videosPageNo = videosPageNo + 1;
                    $.ajax({
                        url: config.GoogleVideosApiUrl + searchKeywordsResource().trim() + "&page=" + videosPageNo + "&limit=10",
                        dataType: 'jsonp',
                        success: function (results) {
                            if (results && results.list && results.list.length > 0) {
                                googleSearchResult = results.list;
                                for (var k = 0; k < googleSearchResult.length; k++) {
                                    var resource = googleSearchResult[k];
                                    if (resource.duration) {
                                        var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                        tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                        resource.duration = tempDuration;
                                    }
                                    resource["tbUrl"] = resource.thumbnail_url;
                                    resource["isSelected"] = ko.observable(false),
                                    resource["guid"] = '';
                                    mediaSearchGoogleList.push(resource);
                                }
                                var arr = _.uniq(mediaSearchGoogleList());
                                mediaSearchGoogleList(arr);
                                $('#busyindicator').removeClass('margin-indicator');
                                presenter.toggleActivity(false);
                            }
                        },
                        error: function () {
                            showloader(false);
                            presenter.toggleActivity(true);
                        }
                    });
                }
                else {
                    googleSearchResult = [];
                    start = start + 8;
                    $.getJSON(config.GoogleImageApiUrl + "&q=" + searchKeywordsResource().trim() + "&start=" + start + "&rsz=8&safe=active&callback=?", function (results) {
                        if (results && results.responseData && results.responseData.results) {
                            if ((results.responseData.results)) {
                                googleSearchResult.push(results.responseData.results);
                                for (var k = 0; k < googleSearchResult.length; k++) {
                                    for (var j = 0; j < googleSearchResult[k].length; j++) {
                                        var resource = googleSearchResult[k][j];
                                        if (resource.duration) {
                                            var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                            tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                            resource.duration = tempDuration;
                                        }
                                        resource["tbUrl"] = resource.thumbnail_url;
                                        resource["isSelected"] = ko.observable(false),
                                        resource["guid"] = '';
                                        mediaSearchGoogleList.push(resource);
                                    }
                                }
                                var arr = _.uniq(mediaSearchGoogleList());
                                mediaSearchGoogleList(arr);
                            }
                        }
                    });
                }
            },
            enableGoogleSearch = function () {
                isGoogleSearchEnabled(true);
                getGoogleData(searchKeywordsResource());
                isAdvanceSearchVisible(false);
                isFavouriteSearchEnable(false);
                isIframeVisible(false);
            },
            enableTeamSearch = function () {
                isAllSearchEnable(false);
                isGoogleSearchEnabled(false);
                isAdvanceSearchVisible(false);
                isFavouriteSearchEnable(false);
                isIframeVisible(false);
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            enableAdvanceSearch = function () {
                isAllSearchEnable(false);
                isGoogleSearchEnabled(false);
                isFavouriteSearchEnable(false);
                isAdvanceSearchVisible(true);
                isIframeVisible(false);
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            enableUserFavouriteSearch = function () {
                isFavouriteSearchEnable(true);
                isAllSearchEnable(false);
                isGoogleSearchEnabled(false);
                filteredFavouriteSearch();
                isIframeVisible(false);
                //$.ajax({
                //    url: '/api/News/GetUserFavourites?UserId=' + appdata.currentUser().id,
                //    dataType: 'jsonp',
                //    type:'GET',
                //    success: function (results) {
                //        var tempResources = [];
                //        //for (var i = 0; i < responseData.Data.UserFavourites.length; i++) {
                //        //    var item = mapper.resource.fromDto(responseData.Data.UserFavourites[i]);
                //        //    item.isMarkedFavourite(true);
                //        //    tempResources.push(item);
                //        //}
                //        //appdata.userFavourites(tempResources);
                //    },
                //    error: function () {
                      
                //    }
                //});
            },
            enableAllSearch = function () {
                isAllSearchEnable(true);
                isGoogleSearchEnabled(false);
                isFavouriteSearchEnable(false);
                isAdvanceSearchVisible(false);
                isIframeVisible(false);
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            showloader = function (flag) {
                if (flag) {
                    $('.archiveSearch').show();
                }
                else {
                    $('.archiveSearch').hide();
                }
            },
            getCelebrity = function (term, pageSize, pageNumber) {
                if (term && term.length > 0) {

                    showloader(true);
                    var requestObj = { term: term, pageSize: pageSize, pageNumber: pageNumber };
                    $.when(manager.production.searchCelebrity(requestObj))
                   .done(function (responseObj) {
                       var obj = responseObj.Data;
                       suggestedKeywords(responseObj.Data);
                       showloader(false);
                   })
                   .fail(function (responseObj) {
                   });
                }
            },
            isNotSameRequest = function (newRequest, bufferedRequest) {
                if (!bufferedRequest)
                    return true;
                return JSON.stringify(newRequest) !== JSON.stringify(bufferedRequest)
            },

            // #endregion

            //#region Resource Cropper

            setForCropping = function (data) {
                if (data.type == 1) {
                    isCropping(true);
                    imageForCropping(data);
                    var tempImage = new Image();
                    tempImage.src = data.url();

                    tempImage.onload = function () {
                        var canvas = document.getElementById('myCanvas');
                        canvas.height = 520;
                        canvas.width = 930;
                    }

                    setTimeout(function () {
                        resetCanvas();
                        controllerScript.resetCanvas();
                        isResourceCrop(true);
                        controllerScript.Getimage(imageForCropping().url());
                        $('#lasso3').click();
                    }, 500);
                }
                else {
                    config.logger.error("Only Image Resource is Croppable!");
                }
            },
            lasso3 = function () {
                controllerScript.lasso3();
            },
            lasso2 = function () {
                controllerScript.lasso2();
            },
            lasso1 = function () {
                controllerScript.lasso1();
            },
            submitCoordinate = function () {
                if (controllerScript.sendpoints().length > 0) {
                    isCropping(false);
                    var sending = {
                        ImageUrl: imageForCropping().url(),
                        Points: controllerScript.sendpoints()[controllerScript.sendpoints().length - 1],
                        BucketId: config.bucketId,
                        ApiKey: config.apiKey
                    };
                    $.when(manager.production.croppImage(sending))
                     .done(function (responseData) {
                         if (responseData.Data && responseData.IsSuccess) {
                             var index = sdc.currentTemplate().contentBucket().indexOf(imageForCropping());
                             if (index > -1) {
                                 if (sdc.currentTemplate().contentBucket()[0].guid == sdc.currentTemplate().contentBucket()[index].guid) {
                                     sdc.currentTemplate().contentBucket()[index].croppedImageguid(responseData.Data);
                                     sdc.currentTemplate().contentBucket()[index].guid = responseData.Data;
                                     placeContentOnTemplateElement(sdc.currentTemplate().contentBucket()[index]);
                                 }
                                 else {
                                     sdc.currentTemplate().contentBucket()[index].croppedImageguid(responseData.Data);
                                     sdc.currentTemplate().contentBucket()[index].guid = responseData.Data;
                                 }
                             }
                             config.logger.success("Resource Cropped Successfully!");
                             resetCanvas();
                         }
                         else {
                             resetCanvas();
                             config.logger.error("Server Returned an Erorr!");
                         }
                         controllerScript.resetCanvas();
                         isResourceCrop(false);
                     });
                } else {
                    config.logger.error("Server Returned an Error!");
                    resetCanvas();
                }
            },
            resetCanvas = function () {
                controllerScript.resetCanvas();
                controllerScript.Getimage(config.pleaseWait);
                setTimeout(function () {
                    controllerScript.Getimage(imageForCropping().url());
                }, 5000);
                $('#lasso3').click();
            },
            hideCropper = function () {
                isResourceCrop(false);
                isCropping(false);
                sdc.isGalleryVisible(true);
            },

            //#endregion

            // #region Chat Conversation

            showChatConversation = function (direction) {
                if (direction) {

                    if (appdata.currentUser().userType == e.UserType.NLE) {
                        sdc.isProducerChat(true);
                    }
                    else { sdc.isNleChat(true); }
                }
                else {
                    sdc.isChatPopUpVisible(true);

                    if (appdata.currentUser().userType == e.UserType.StoryWriter) {
                        sdc.isProducerChat(true);
                    }
                    else { sdc.isStorywriterChat(true); }
                }
                $("#content-viewer-main").children('#contentviewer-overlay').addClass('showChatOverlay');
                sdc.isChatPopUpVisible(true);
            },
            hideChatConversation = function () {
                sdc.isChatPopUpVisible(!sdc.isChatPopUpVisible());
                sdc.isStorywriterChat(false);
                sdc.isNleChat(false);
                sdc.isProducerChat(false);
                $("#content-viewer-main").children('#contentviewer-overlay').removeClass('showChatOverlay');
            },
            sendComments = function () {
                if (commentBoxText() && commentBoxText().length > 0) {
                    //manager.production.sendComments(commentBoxText(), sdc.currentTemplate().storyScreenTemplateId);
                    var programId;
                    if (appdata.currentUser().userType === e.UserType.NLE || appdata.currentUser().userType === e.UserType.StoryWriter) {
                        programId = appdata.currentProgram().id;//dc.episodes.getLocalById(currentPendingStory().episodeId).programId;
                    }
                    else {
                        programId = appdata.currentProgram().id;
                    }
                    $.when(manager.production.sendComments(commentBoxText(), sdc.currentTemplate().storyScreenTemplateId, sdc.isNleChat(), sdc.isStorywriterChat(), sdc.isProducerChat(), programId))
                    .done(function () {
                    })
                    .fail(function () {
                    });
                    commentBoxText('');
                    hideChatConversation();
                    $("#content-viewer-main").children('#contentviewer-overlay').removeClass('showChatOverlay');
                } else {
                    logger.warning("Comment box is empty!");
                }
            },

            // #endregion

            //#region Media Wall
            setCurrentMediaWallForContentSelection = function (data, index) {
                if (data && !isNaN(index)) {
                    var arr = _.filter(currentStory().screenFlow().screenTemplates(), function (template) {
                        return (template.id === data.id && template.parentId == currentStory().screenFlow().screenTemplates()[sdc.lastSelectedTemplateIndex].storyScreenTemplateId);
                    });
                    if (arr && arr.length > 0) {
                        var available = _.filter(sdc.mediaWallTemplatePool(), function (template) {
                            return (template.id === arr[0].id);
                        });
                        if (available && available.length > 0) {
                            sdc.mediaWallTemplateIndex(sdc.mediaWallTemplatePool().indexOf(available[0]));
                        }
                        sdc.currentTemplate(arr[0]);
                        sdc.templateIndex(sdc.lastSelectedTemplateIndex);
                        sdc.isTemplateSelected(true);
                    }
                    else {
                        var tempMediaWallTemplate = mapper.screenTemplate.getClone(data);
                        var arr = [];
                        for (var i = 0; i < tempMediaWallTemplate.templateElements().length; i++) {
                            var item = mapper.templateElement.getClone(tempMediaWallTemplate.templateElements()[i]);
                            arr.push(item);
                            tempMediaWallTemplate.templateElements()[i].contentBucket([]);
                        }
                        tempMediaWallTemplate.templateElements([]);
                        tempMediaWallTemplate.templateElements(arr);
                        sdc.currentTemplate(tempMediaWallTemplate);
                        sdc.currentTemplateElement(tempMediaWallTemplate.templateElements()[0]);
                        sdc.templateIndex(sdc.lastSelectedTemplateIndex);
                        sdc.isTemplateSelected(true);
                        sdc.mediaWallTemplateIndex(index);
                    }
                }
            },
            downloadTemplateResource = function (data, event) {
                isVideoEditToolClick(true);
                sdc.currentTemplateElement(data);
                var arr = _.filter(sdc.currentTemplateElement().contentBucket(), function (item) {
                    if (item.guid === data.imageGuid);
                    return item;
                });
                downLoadResource(arr[0].url);
                setTimeout(function () { isVideoEditToolClick(false) }, 500);
            },

            toggleMediaWallDesignerVisibility = function () {
                sdc.currentTemplate().contentBucket([]);
                if (sdc.isMediaWallVisible()) {
                    sdc.currentTemplate(currentStory().screenFlow().screenTemplates()[sdc.lastSelectedTemplateIndex]);
                    sdc.templateIndex(sdc.lastSelectedTemplateIndex);
                    sdc.isTemplateSelected(true);
                } else {
                    sdc.populateMediaWallTemplates();

                    for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                        if (currentStory().screenFlow().screenTemplates()[i].parentId == sdc.currentTemplate().storyScreenTemplateId) {
                            setCurrentMediaWallForContentSelection(currentStory().screenFlow().screenTemplates()[i], i);
                        }
                    }

                    if (!sdc.currentTemplate().isMediaWallTemplate()) {
                        var tempMediaWallTemplate = mapper.screenTemplate.getClone(sdc.mediaWallTemplatePool()[0]);
                        var arr = [];
                        for (var i = 0; i < tempMediaWallTemplate.templateElements().length; i++) {
                            var item = mapper.templateElement.getClone(tempMediaWallTemplate.templateElements()[i]);
                            arr.push(item);
                            tempMediaWallTemplate.templateElements()[i].contentBucket([]);
                        }
                        tempMediaWallTemplate.templateElements([]);
                        tempMediaWallTemplate.templateElements(arr);
                        setCurrentMediaWallForContentSelection(tempMediaWallTemplate, 0);
                    }
                }
                sdc.isMediaWallVisible(!sdc.isMediaWallVisible());
            },
            saveMediaWallTemplate = function (tempScreenTemplate) {
                if (sdc.currentTemplate() && sdc.currentTemplate().isMediaWallTemplate()) {
                    sdc.currentTemplate().parentId = currentStory().screenFlow().screenTemplates()[sdc.lastSelectedTemplateIndex].storyScreenTemplateId;
                    sdc.currentTemplate().sequenceId = currentStory().screenFlow().screenTemplates()[sdc.lastSelectedTemplateIndex].sequenceId;
                    sdc.currentTemplate().isAssignToNLE(true);
                    sdc.currentTemplate().isAssignToStoryWriter(true);

                    $.when(manager.production.saveMediaWallTemplateOnServer(sdc.currentTemplate(), currentStory().storyId))
                    .done(function (responseData) {
                        for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                            if (currentStory().screenFlow().screenTemplates()[i].parentId == sdc.currentTemplate().parentId &&
                                currentStory().screenFlow().screenTemplates()[i].storyScreenTemplateId == sdc.currentTemplate().storyScreenTemplateId) {
                                currentStory().screenFlow().screenTemplates().remove(currentStory().screenFlow().screenTemplates()[i]);
                                currentStory().screenFlow().screenTemplates.valueHasMutated();
                            }
                        }

                        sdc.currentTemplate().storyScreenTemplateId = responseData.Data[0].SlotScreenTemplateId;

                        if (sdc.currentTemplate().templateElements && sdc.currentTemplate().templateElements().length > 0) {
                            var arr = [];
                            for (var j = 0; j < responseData.Data[0].SlotTemplateScreenElements.length; j++) {
                                var templateElement = responseData.Data[0].SlotTemplateScreenElements[j];
                                templateElement["ScreenTemplateId"] = responseData.Data[0].ScreenTemplateId;
                                var temp = mapper.templateElement.fromDto(templateElement);
                                arr.push(temp);
                            }
                            sdc.currentTemplate().templateElements(arr);
                        }

                        setTimeout(function () {
                            currentStory().screenFlow().screenTemplates().push(sdc.currentTemplate());
                            toggleMediaWallDesignerVisibility();
                        }, 1500);

                        logger.success("Media Wall saved successfully !")
                    })
                    .fail(function () {
                        toggleMediaWallDesignerVisibility();
                        logger.error("Unable to save media wall due to some error!");
                    });
                }
            },
            // #endregion

            //#region archival server hits 
            getArchivalRequestObject = function (term, pageSize, pageNumber, bucketId, resourceTypeId, alldata) {

                var requestObject = {
                    term: term ? term.trim() : term,
                    pageSize: pageSize,
                    pageNumber: pageNumber,
                    bucketId: dc.programs.getLocalById(dc.episodes.getLocalById(currentStory().episodeId).programId).bucketId,
                    resourceTypeId: resourceTypeId,
                    alldata: alldata
                };
                return requestObject;
            },
            getArchivalDataFromServer = function (requestObj, isFromGetMore, emptyExisting) {
                showloader(true);
                presenter.toggleActivity(true);
                if (isNotSameRequest(requestObj, bufferRequestObject) && noAjaxRequestIsInProgress) {
                    noAjaxRequestIsInProgress = false;
                    $.when(manager.production.searchContentArchive(requestObj, archivalServerCount, searchTags(), isAdvanceSearchVisible()))
                        .done(function (responseObj) {
                            noAjaxRequestIsInProgress = true;
                            var temparr = responseObj;

                            if (emptyExisting)
                                archivalResources([]);

                            if (isFromGetMore) {
                                for (var i = 0; i < responseObj.length; i++) {
                                    var resource = responseObj[i];
                                    archivalResources.push(resource);

                                }
                                pageNumber(requestObj.pageNumber);
                            }
                            else
                                archivalResources(temparr);

                            showloader(false);
                            presenter.toggleActivity(false);
                            bufferRequestObject = requestObj;

                        })
                        .fail(function (responseObj) {
                            showloader(false);
                            presenter.toggleActivity(false);
                        });
                }
                else {
                    showloader(false);
                    presenter.toggleActivity(false);
                }

            },



            //#endregion archival server hits  

            // #endregion

            activate = function (routeData, callback) {
                sdc.isMediaWallVisible(false);
                searchKeywordsResource('');
                var newsIds = [];
                $("#txtEditor1").html('');

                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                    for (var i = 0 ; i < stories.length; i++) {
                        newsIds.push(stories[i].news.id)
                    }

                    var arr = _.filter(_dc.screenTemplates.getObservableList(), function (tempTemplate) {
                        return tempTemplate.programId === appdata.currentProgram().id;
                    });

                    if (arr && arr.length == 1) {
                        onTemplatethumbClick(0);
                        setTimeout(function () {
                            onTemplatethumbClick(0);
                        }, 500);
                    }
                } else {
                    newsIds.push(currentPendingStory().news.id);
                    currentStep(e.ProgramDesignerStep.ContentSelection);
                }

                var requestObj = {
                    ids: newsIds
                };

                $.when(manager.news.getNewsByIds(requestObj))
                .done(function (responseObj) {
                    //var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                    //for (var i = 0; i < stories.length; i++) {
                    //    var tempNews = dc.news.getLocalById(stories[i].news.id)
                    //    if (tempNews) {
                    //        stories[i].news = dc.news.getLocalById(stories[i].news.id);
                    //    }
                    //}
                });

                isActive(true);
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });

                appdata.updateSuggestedFlowFlag(!appdata.updateSuggestedFlowFlag());

                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    if (!appdata.isNotifying) {
                        changeStep(false);
                    } else {
                        appdata.isNotifying = false;
                    }
                }

                $("#story-screen-templates").sortable('refresh');
            },
            canLeave = function () {
                isActive(false);
                if (sdc.isChatPopUpVisible()) {
                    hideChatConversation();
                }
                productionlp.isStoryBucketVisible(false);
                return true;
            },
            init = function () {
                sourceFilterControl.currentFilter(-1);
                sourceFilterControl.isPopulated(true);
                sourceFilterControl.isMediaSectionVisible(true);
                sourceFilterControl.isGuestSectionVisible(true);
                sourceFilterControl.isSourceFiltersVisible(false);
                sourceFilterControl.currentFilter('media');

                $('.navigationNews').children('li').addClass('hideFilters');

                if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    sdc.isMediaUploaderVisible(false);
                    sdc.isTemplateCommentBoxVisible(true);
                } else if (appdata.currentUser().userType == e.UserType.StoryWriter) {
                    sdc.isMediaUploaderVisible(false);
                    sdc.isTemplateCommentBoxVisible(true);
                } else if (appdata.currentUser().userType == e.UserType.NLE) {
                    sdc.isMediaUploaderVisible(true);
                    sdc.isTemplateCommentBoxVisible(true);
                }

                subscribeEvents();
                programInfoControl.currentPage(hashes.production);
                pdc.isVisible(true);

                textEditorBuffer.subscribe(function (value) {
                    sdc.currentTemplate().script(value);
                    RefreshTemplateStatus(false);
                });
                currFaceAction.subscribe(function (value) {
                    sdc.currentTemplate().faceAction = value;
                    SelectedFaceAction('');
                });

                appdata.updateSuggestedFlowFlag.subscribe(function () {
                    var key = pdc.getSuggestedFlowKey();

                    if (key) {
                        var requestObj = {
                            programId: appdata.currentProgram().id,
                            key: key
                        };

                        $.when(manager.production.getSuggestedFlow(requestObj))
                        .done(function (responseObj) {
                            if (responseObj && responseObj.IsSuccess && responseObj.Data) {
                                pdc.createSuggestedFlows(responseObj.Data);
                            } else {
                                pdc.createSuggestedFlows([]);
                            }
                        })
                        .fail(function (responseObj) {
                            pdc.createSuggestedFlows([]);
                        });
                    } else {
                        pdc.createSuggestedFlows([]);
                    }
                });

                appdata.currentNotificationAlertId.subscribe(function (currentNotification) {
                    var notification = dc.notifications.getLocalById(currentNotification.id);
                    if (notification) {
                        $.when(manager.production.getTaskDoneNLEOrStorywriter(notification.storyScreenTemplateId))
                        .done(function (responseObj) {
                            if (responseObj.Data.NleStatusId || responseObj.Data.StoryWriterStatusId) {
                                if (appdata.currentUser().userType == e.UserType.NLE || appdata.currentUser().userType == e.UserType.StoryWriter) {
                                    _.filter(currentStory().screenFlow().screenTemplates(), function (currentTemplate) {
                                        if (currentTemplate.storyScreenTemplateId == responseObj.Data.SlotScreenTemplateId) {
                                            if (appdata.currentUser().userType == e.UserType.NLE)
                                                currentTemplate.nleStatusId(responseObj.Data.NleStatusId);
                                            if (appdata.currentUser().userType == e.UserType.StoryWriter)
                                                currentTemplate.storywriterStatusId(responseObj.Data.StoryWriterStatusId);
                                        }
                                    });
                                }
                                else {
                                    _.filter(appdata.currentEpisode().programElements(), function (programElement) {
                                        for (var i = 0; i < programElement.stories().length; i++) {
                                            _.filter(programElement.stories()[i].screenFlow().screenTemplates(), function (currentTemplate) {
                                                if (currentTemplate.storyScreenTemplateId == responseObj.Data.SlotScreenTemplateId) {
                                                    if (responseObj.Data.NleStatusId)
                                                        currentTemplate.nleStatusId(responseObj.Data.NleStatusId);
                                                    if (responseObj.Data.StoryWriterStatusId)
                                                        currentTemplate.storywriterStatusId(responseObj.Data.StoryWriterStatusId);
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                            if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                                if (responseObj.Data.IsAssignedToStoryWriter) {
                                    //sdc.currentTemplate().script(responseObj.Data.Script);
                                    _.filter(appdata.currentEpisode().programElements(), function (programElement) {
                                        for (var i = 0; i < programElement.stories().length; i++) {
                                            _.filter(programElement.stories()[i].screenFlow().screenTemplates(), function (currentTemplate) {
                                                if (currentTemplate.storyScreenTemplateId == responseObj.Data.SlotScreenTemplateId) {
                                                    if (responseObj.Data.Script) {
                                                        currentTemplate.script(responseObj.Data.Script);
                                                        $("#txtEditor1").html(currentTemplate.script());
                                                    }

                                                    if (responseObj.Data.StatusId)
                                                        currentTemplate.statusId(responseObj.Data.StatusId);
                                                }
                                            });
                                        }
                                    });
                                }
                                if (responseObj.Data.IsAssignedToNle) {
                                    if (responseObj.Data.SlotTemplateScreenElementResources && responseObj.Data.SlotTemplateScreenElementResources.length > 0) {
                                        for (var j = 0; j < responseObj.Data.SlotTemplateScreenElementResources.length; j++) {
                                            var item = mapper.resource.fromDto(responseObj.Data.SlotTemplateScreenElementResources[j]);
                                            _.filter(appdata.currentEpisode().programElements(), function (programElement) {
                                                for (var i = 0; i < programElement.stories().length; i++) {
                                                    _.filter(programElement.stories()[i].screenFlow().screenTemplates(), function (currentTemplate) {
                                                        if (currentTemplate.storyScreenTemplateId == notification.storyScreenTemplateId) {
                                                            for (var k = 0; k < currentTemplate.templateElements().length; k++) {
                                                                if (currentTemplate.templateElements()[k].storyTemplateScreenElementId == item.storyTemplateScreenElementId) {
                                                                    var temparr = _.filter(currentTemplate.templateElements()[k].contentBucket(), function (resource) {
                                                                        if (resource.guid == item.guid)
                                                                            return resource;
                                                                    });
                                                                    if (temparr && temparr.length <= 0) {
                                                                        currentTemplate.templateElements()[k].contentBucket.push(item);
                                                                        currentTemplate.videoDuration(currentTemplate.videoDuration() + item.duration);
                                                                        isNleUploadedResources(true);
                                                                        if (sdc.currentTemplate().storyScreenTemplateId == notification.storyScreenTemplateId) {
                                                                            onTemplateElementClick(currentTemplate.templateElements()[k]);
                                                                            placeContentOnTemplateElement(currentTemplate.templateElements()[k].contentBucket()[0]);
                                                                        }
                                                                        isNleUploadedResources(false);
                                                                        isMediaSelectionVisible(false);
                                                                    }
                                                                }
                                                                else {
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                        setTimeout(function () {
                                            if (currentStep() == e.ProgramDesignerStep.Rundown) {
                                                for (var i = 0; i < currentStory().screenFlow().screenTemplates().length; i++) {
                                                    manager.production.saveStoryScreenTemplateOnServer(currentStory().screenFlow().screenTemplates()[i], currentStory().storyId);
                                                }
                                            }
                                        }, 1000);
                                    }
                                }
                            }
                        })
                        .fail(function (responseObj) {
                        });
                        appdata.isRefreshStoryStatus();
                    }

                });

                appdata.selectPopUpResource.subscribe(function (data) {
                    if (data && router.currentHash() === config.hashes.production.production)
                        addToContentBucket(data);
                });

                appdata.selectedFavouriteResource.subscribe(function (data) {
                    if (data && router.currentHash() === config.hashes.production.production) {
                        markResourceAsFavourite(data);
                        config.logger.success("Mark Favourite Successfully!");
                    }
                });

                appdata.unmarkSelectedResource.subscribe(function (data) {
                    if (data && router.currentHash() === config.hashes.production.production) {
                        deleteResourceFromFavourite(data);
                        config.logger.success("UnMark Successfully!");
                    }
                });
            };

        function receiveMessage(event) {
            if (router.currentHash() != "#/topicselection?isiframe" && router.currentHash() != "#/report-news" && !appdata.isNewsCreated()) {
                var expResource = JSON.parse(event.data);
                var tempObj = {
                    ResourceTypeId: expResource.ResourceType,
                    ResourceGuid: expResource.Guid,
                    IsUploadedFromNle: false
                };
                var item = mapper.resource.fromDto(tempObj);
                if (item) {
                    var arr = _.filter(sdc.currentTemplate().contentBucket(), function (resource) {
                        return resource.guid === item.guid;
                    });
                    if (arr && arr.length > 0) {
                        sdc.currentTemplate().contentBucket.remove(arr[0]);
                        sdc.currentTemplate().contentBucket.valueHasMutated();
                    }
                    else {
                        sdc.currentTemplate().contentBucket.push(item);
                    }
                }
            }
        }

        addEventListener("message", receiveMessage, false);

        return {
            init: init,
            activate: activate,
            canLeave: canLeave,
            screenTemplates: screenTemplates,
            appdata: appdata,
            templates: templates,
            isProducerUploadResource: isProducerUploadResource,
            programInfoControl: programInfoControl,
            pdc: pdc,
            sdc: sdc,
            prc: prc,
            ppc: ppc,
            calendarControl: calendarControl,
            currentStoryIndex: currentStoryIndex,
            currentStoryIndexDisplay: currentStoryIndexDisplay,
            currentStory: currentStory,
            currentBunch: currentBunch,
            storyCount: storyCount,
            hashes: hashes,
            views: views,
            toggleRelatedTemplates: toggleRelatedTemplates,
            toggleProgramPreview: toggleProgramPreview,
            saveRelatedTemplate: saveRelatedTemplate,
            currentStep: currentStep,
            changeStep: changeStep,
            navigateStory: navigateStory,
            toggleSelectedTemplate: toggleSelectedTemplate,
            setCurrentTemplateForContentSelection: setCurrentTemplateForContentSelection,
            onTemplateElementClick: onTemplateElementClick,
            showContentSelectionArea: showContentSelectionArea,
            sourceFilterControl: sourceFilterControl,
            isMediaSelectionVisible: isMediaSelectionVisible,
            setCurrentContent: setCurrentContent,
            setCurrentPendingStory: setCurrentPendingStory,
            activeSideView: activeSideView,
            searchKeywords: searchKeywords,
            internalGuests: internalGuests,
            externalGuests: externalGuests,
            totalGuestCount: totalGuestCount,
            reportedContentCount: reportedContentCount,
            toggleContentSelection: toggleContentSelection,
            isContentBucketEnabled: isContentBucketEnabled,
            isAssignToTeamVisible: isAssignToTeamVisible,
            isContentDraggingEnabled: isContentDraggingEnabled,
            addToContentBucket: addToContentBucket,
            placeContentOnTemplateElement: placeContentOnTemplateElement,
            currentGuest: currentGuest,
            selectGuest: selectGuest,
            programs: programs,
            episodes: episodes,
            textEditorBuffer: textEditorBuffer,
            isTextEditorChangeAllowed: isTextEditorChangeAllowed,
            e: e,
            saveProductionTeamData: saveProductionTeamData,
            contentUploader: contentUploader,
            sendComments: sendComments,
            commentBoxText: commentBoxText,
            sendToBroadCast: sendToBroadCast,
            toggleResourceFilter: toggleResourceFilter,
            removeScreenTemplate: removeScreenTemplate,
            arrangeStoryScreenTemplates: arrangeStoryScreenTemplates,
            saveScreenTemplateDataForNLE: saveScreenTemplateDataForNLE,
            removeContentFromBuckets: removeContentFromBuckets,
            assignToNle: assignToNle,
            assignTostoryWriter: assignTostoryWriter,
            showBucket: showBucket,
            isCheckedImplementForAll: isCheckedImplementForAll,
            saveCommentsForContentBucket: saveCommentsForContentBucket,
            isPreviousButtonVisible: isPreviousButtonVisible,
            isNextButtonVisible: isNextButtonVisible,
            isVideoTemplate: isVideoTemplate,
            isImageTemplate: isImageTemplate,
            editProgramFlow: editProgramFlow,
            checkInContentBucket: checkInContentBucket,
            isCheckBoxVisible: isCheckBoxVisible,
            isResourceCrop: isResourceCrop,
            setForCropping: setForCropping,
            hideCropper: hideCropper,
            lasso3: lasso3,
            lasso2: lasso2,
            lasso1: lasso1,
            submitCoordinate: submitCoordinate,
            resetCanvas: resetCanvas,
            searchKeywordsResource: searchKeywordsResource,
            archivalResources: archivalResources,
            controllerScript: controllerScript,
            canvaswidth: canvaswidth,
            canvasheight: canvasheight,
            mediaArchivalResources: mediaArchivalResources,
            onBucketResourceClick: onBucketResourceClick,
            removeUploadedFile: removeUploadedFile,
            navigateToTemplateDesigner: navigateToTemplateDesigner,
            pendingStoryList: pendingStoryList,
            currentPendingStoryIndex: currentPendingStoryIndex,
            setVideoForProduction: setVideoForProduction,
            openVideoInEditor: openVideoInEditor,
            loadMoreArchivalData: loadMoreArchivalData,
            btnAttribute: btnAttribute,
            archivalServerCount: archivalServerCount,
            utils: utils,
            onPreviewThumbClick: onPreviewThumbClick,
            currFaceAction: currFaceAction,
            setFaceAction: setFaceAction,
            SelectedFaceAction: SelectedFaceAction,
            setCurrentMediaWallForContentSelection: setCurrentMediaWallForContentSelection,
            toggleMediaWallDesignerVisibility: toggleMediaWallDesignerVisibility,
            saveMediaWallTemplate: saveMediaWallTemplate,
            playRundownUrl: playRundownUrl,
            downLoadResource: downLoadResource,
            isChatPopUpVisible: isChatPopUpVisible,
            showChatConversation: showChatConversation,
            hideChatConversation: hideChatConversation,
            onTemplatethumbClick: onTemplatethumbClick,
            suggestedKeywords: suggestedKeywords,
            showArchivalDataForSuggestedkeyword: showArchivalDataForSuggestedkeyword,
            resouceSearchKeyword: resouceSearchKeyword,
            searchByTag: searchByTag,
            nleUploadResouces: nleUploadResouces,
            searchOnSuggestedKeyword: searchOnSuggestedKeyword,
            isDefaultState: isDefaultState,
            isEditorDisable: isEditorDisable,
            productionlp: productionlp,
            mediaSearchGoogleList: mediaSearchGoogleList,
            isGoogleSearchEnabled: isGoogleSearchEnabled,
            GoogleSearchText: GoogleSearchText,
            isWarningBoxVisible: isWarningBoxVisible,
            allowToSendRundown: allowToSendRundown,
            downloadCount: downloadCount,
            pendingCount: pendingCount,
            isImplementToAll: isImplementToAll,
            router: router,
            downloadTemplateResource: downloadTemplateResource,
            enableGoogleSearch: enableGoogleSearch,
            enableTeamSearch: enableTeamSearch,
            enableAllSearch: enableAllSearch,
            enableUserFavouriteSearch: enableUserFavouriteSearch,
            isAllSearchEnable: isAllSearchEnable,
            userFavouriteResources: userFavouriteResources,
            isFavouriteSearchEnable: isFavouriteSearchEnable,
            searchTags: searchTags,
            isAdvanceSearchVisible: isAdvanceSearchVisible,
            enableAdvanceSearch: enableAdvanceSearch,
            currentMetaSearchKeyword: currentMetaSearchKeyword,
            convertToUnicodeText: convertToUnicodeText,
            uploadFile: uploadFile,
            currentScript: currentScript,
            config: config,
            updateCurrentTemplateStatus: updateCurrentTemplateStatus,
            isSmallTemplateVisible: isSmallTemplateVisible,
            openFileExplorer: openFileExplorer,
            iframeSrc: iframeSrc,
            isIframeVisible: isIframeVisible,
            addExplorerResource: addExplorerResource,
            FEResource: FEResource,
            isContentReportEnable: isContentReportEnable,
            isIframeDiv: isIframeDiv,
            isSetTop: isSetTop
        };
    });