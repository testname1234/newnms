﻿define('vm.sequence',
    ['underscore', 'ko', 'datacontext', 'appdata', 'config', 'model', 'manager', 'presenter', 'messenger', 'utils', 'enum', 'model.mapper', 'vm.contentviewer'],
    function (_, ko, dc, appdata, config, model, manager, presenter, messenger, utils, e, mapper, contentViewerVM) {
        var
            // Properties
            // ------------------------------

            logger = config.logger,
            templates = config.templateNames,
            hashes = config.hashes.production,
            programInfoControl = new model.ProgramSelectionAndInfoControl(),
            prc = new model.ProgramRundownControl(),
            calendarControl = new model.CalendarControl(),
            ppc = new model.ProgramPreviewControl(),
            btnAttribute = ko.observable({ text: "Preview", disabled: false }),
            isActive = ko.observable(false),
            greenNewsBucket = ko.observableArray(),
            blueNewsBucket = ko.observableArray(),
            isCartVisible = ko.observable(false),
            isPreviousProductionVisible = ko.observable(false),
            currentListView = ko.observable("Cart"),
            refresh = ko.observable(false),
            isFirstTime = ko.observable(true),
            isUserPushing = 0,
            currentAddedObject = null,
            // Computed Properties
            // ------------------------------

            stories_OBSOLETE = ko.computed({
                read: function () {
                    if (isActive()) {
                        var arr = greenNewsBucket();
                        arr = arr.sort(function (entity1, entity2) {
                            return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                        });
                        return arr;
                    }
                },
                deferEvaluation: true
            }),
            populateNewsBucketList = ko.computed({
                read: function () {
                    appdata.refreshNewsBucket();

                    var newsBucketList = dc.newsBucketItems.getObservableList();

                    var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                    stories = stories.sort(function (entity1, entity2) {
                        return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                    });

                    var greenArray = [];

                    if (stories.length > 0 && isUserPushing == 0) {
                        greenNewsBucket(stories);

                    }
                    else {
                        
                        var tempArray = [];
                        greenArray = _.filter(newsBucketList, function (item) {
                            return item.isGreen() == true || item.isRed() == true;
                        });

                        var rawArray = greenArray.concat(stories);
                        if (newsBucketList && newsBucketList.length > 0)
                            rawArray = rawArray.concat(greenNewsBucket());

                        if (currentAddedObject) {
                            rawArray.push(currentAddedObject);
                            currentAddedObject = null;
                        }
                        tempArray = rawArray;
                        tempArray = _.unique(tempArray, false, function (item, k, v) {
                            return item.news.id;
                        });
                        tempArray = tempArray.sort(function (storyA, storyB) {
                            return storyA.sequenceId - storyB.sequenceId;
                        });
                        greenNewsBucket(tempArray);
                    }
                    var bluebucket = [];

                    for (var i = 0, len = newsBucketList.length; i < len; i++) {
                        var flag = true;
                        for (var j = 0, len2 = stories.length; j < len2; j++) {
                            var story = stories[j];
                            if (stories[j].news.id === newsBucketList[i].news.id) {
                                flag = false;
                            }
                        }
                        if (flag && !newsBucketList[i].isRed() && !newsBucketList[i].isGreen())
                            bluebucket.push(newsBucketList[i]);
                    }

                    blueNewsBucket(bluebucket)
                }
            }).extend({ rateLimit: 500 }),

            previousProductionList = ko.computed({
                read: function () {
                    appdata.refreshNewsBucket();
                    var arr = dc.episodes.getObservableList();

                    arr = _.filter(arr, function (item) {
                        return item.id !== appdata.currentEpisode().id;
                    });
                    var storyTempArray = [];

                    var greenNewsId = [];

                    for (var i = 0; i < greenNewsBucket().length; i++) {
                        greenNewsId.push(greenNewsBucket()[i].news.id);
                    }

                    for (var i = 0, len = arr.length; i < len ; i++) {
                        var stories = dc.stories.getByEpisodeId(arr[i].id)();
                        for (var j = 0, len2 = stories.length; j < len2; j++) {
                            if (greenNewsId.indexOf(stories[j].news.id) === -1) {
                                stories[j].isRed(false);
                                stories[j].isGreen(false);
                                var storyDetails = [];
                                var progId = 0;
                                var progName = "";
                                var episodeDuration = "";
                                var episodeStrtTime = "";
                                var episodeEndTime = "";
                                var userName = dc.users.getLocalById(stories[j].userId).name;
                                _.filter(dc.episodes.getObservableList(), function (episode) {
                                    if (episode.id === stories[j].episodeId) {
                                        progId = episode.programId;
                                        episodeStrtTime = moment(episode.startTime()).format('MMMM Do YYYY, h:mm:ss a');
                                        episodeEndTime = moment(episode.endTime()).format('MMMM Do YYYY, h:mm:ss a');
                                    }
                                });
                                _.filter(dc.programs.getObservableList(), function (program) {
                                    if (program.id === progId) {
                                        progName = program.name;
                                    }
                                });

                                var isGroupStory = false;
                                storyDetails.push(userName);
                                storyDetails.push(progName);
                                storyDetails.push(stories[j].news.source());
                                storyDetails.push(stories[j].news.publishDisplayTime());
                                storyDetails.push(episodeStrtTime);
                                storyDetails.push(stories[j].contentDurationDisplay());
                                _.filter(storyTempArray, function (item) {
                                    if (item.news.id === stories[j].news.id) {
                                        stories[j]["storyDetails"] = storyDetails;
                                        appdata.isRefreshStoryStatus(!appdata.isRefreshStoryStatus());

                                        if (item.groupStories && item.groupStories.length > 0) {

                                            var tempArr = _.filter(item.groupStories, function (tempgroupStory) {
                                                if ((tempgroupStory.episodeStartTime() === stories[j].episodeStartTime())) {
                                                    return tempgroupStory;
                                                }
                                            });
                                            if (tempArr) {
                                                item.groupStories.push(stories[j]);
                                                var arr = _.uniq(item.groupStories);
                                                item.groupStories = arr;
                                            }
                                        }
                                        else {
                                            item.groupStories = [];
                                            item.groupStories.push(item);
                                            item.groupStories.push(stories[j]);
                                        }

                                        if (item.groupStories.length > 0) {
                                            if (item.storyDetails[6])
                                                item.storyDetails[6] = item.groupStories.length + " " + "Production";
                                            else
                                                item.storyDetails.push(item.groupStories.length + " " + "Production");
                                        }
                                        isGroupStory = true;
                                    }
                                });

                                if (!isGroupStory) {
                                    stories[j]["storyDetails"] = storyDetails;
                                    appdata.isRefreshStoryStatus(!appdata.isRefreshStoryStatus());
                                    storyTempArray.push(stories[j]);
                                }
                            }
                        }
                    }
                    return storyTempArray;
                },
                deferEvaluation: true
            }),
            toggleBlueArea = function (data) {

                if (data == 'previousProduction') {
                    isCartVisible(false);
                    isPreviousProductionVisible(true);
                }
                else if (data == 'cart') {
                    isCartVisible(true);
                    isPreviousProductionVisible(false);
                }
            },

            programs = ko.computed({
                read: function () {
                    var allPrograms = dc.programs.getObservableList();
                    var arr = _.filter(allPrograms, function (program) {
                        return program.channelId() === appdata.currentChannel();
                    });
                    return arr;
                },
                deferEvaluation: true
            }),
            episodes = ko.computed({
                read: function () {
                    var arr = [];
                    var programEpisodes = dc.episodes.getByProgramId(appdata.currentProgram().id)();
                    for (var i = 0; i < programEpisodes.length; i++) {
                        var diffDays = utils.getDifferenceBetweenDates(appdata.programSelectedDate(), programEpisodes[i].startTime());
                        if (diffDays === 0) {
                            arr.push(programEpisodes[i]);
                        }
                    }

                    if (arr[0]) {
                        appdata.currentEpisode(arr[0]);
                    }

                    return arr;
                },
                deferEvaluation: true
            }),

            // Methods
            // ------------------------------


            arrangeNewsBucket = function (arrayPositions) {

                for (var i = 0; i < arrayPositions.length; i++) {
                    var newsBucket = dc.newsBucketItems.getLocalById(arrayPositions[i]);
                    if (newsBucket) {
                        var newSeqId = i + 1;
                        if (newsBucket.sequenceId !== newSeqId) {
                            newsBucket.sequenceId = newSeqId;
                        }
                    }
                    else {
                        var programElements = dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                        var segmentArray = _.filter(programElements, function (programElement) {
                            return programElement.programElementType === e.ProgramElementType.Segment;
                        });
                        if (segmentArray.length > 0) {
                            segmentArray = segmentArray.sort(function (entity1, entity2) {
                                return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                            });
                            var story = dc.stories.getLocalById(arrayPositions[i]);
                            if (story) {
                                var newSeqId = i + 1;
                                if (story.sequenceId !== newSeqId) {
                                    var startIndex = 0;
                                    for (var j = 0; j < segmentArray.length; j++) {
                                        if (newSeqId > startIndex && newSeqId <= startIndex + segmentArray[j].storyCount) {
                                            if (story.programElementId !== segmentArray[j].id) {
                                                dc.stories.removeFromDictionary(story.id, story.programElementId, 'program-element');
                                                story.programElementId = segmentArray[j].id;
                                                dc.stories.addToDictionary(story, segmentArray[j].id, 'program-element');
                                            }
                                        }
                                        startIndex = startIndex + segmentArray[j].storyCount;
                                    }
                                }
                                story.sequenceId = newSeqId;
                            }
                            for (var k = 0; k < segmentArray.length; k++) {
                                segmentArray[k].stories.valueHasMutated();
                            }
                        }
                        appdata.arrangeNewsFlag(!appdata.arrangeNewsFlag());

                    }
                }
                updateSequenceOnServer();
            },
            arrangeStories = function (arrayPositions) {
                if (arrayPositions.length === dc.stories.getByEpisodeId(appdata.currentEpisode().id)().length) {

                    var programElements = dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                    var segmentArray = _.filter(programElements, function (programElement) {
                        return programElement.programElementType === e.ProgramElementType.Segment;
                    });

                    if (segmentArray.length > 0) {
                        segmentArray = segmentArray.sort(function (entity1, entity2) {
                            return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                        });

                        for (var i = 0; i < arrayPositions.length; i++) {
                            var story = dc.stories.getLocalById(arrayPositions[i]);
                            if (story) {
                                var newSeqId = i + 1;
                                if (story.sequenceId !== newSeqId) {
                                    var startIndex = 0;
                                    for (var j = 0; j < segmentArray.length; j++) {
                                        if (newSeqId > startIndex && newSeqId <= startIndex + segmentArray[j].storyCount) {
                                            if (story.programElementId !== segmentArray[j].id) {
                                                dc.stories.removeFromDictionary(story.id, story.programElementId, 'program-element');
                                                story.programElementId = segmentArray[j].id;
                                                dc.stories.addToDictionary(story, segmentArray[j].id, 'program-element');
                                            }
                                        }
                                        startIndex = startIndex + segmentArray[j].storyCount;
                                    }
                                }
                                story.sequenceId = newSeqId;
                            }
                        }
                        for (var i = 0; i < segmentArray.length; i++) {
                            segmentArray[i].stories.valueHasMutated();
                        }
                    }

                    appdata.arrangeNewsFlag(!appdata.arrangeNewsFlag());
                    updateSequenceOnServer();
                }
            },
            playRundownUrl = function (data) {
                var arr = [];
                var tempObj = {
                    ResourceTypeId: e.ContentType.Video,
                    ResourceGuid: data(),
                    IsUploadedFromNle: false
                };
                var item = mapper.resource.fromDto(tempObj);
                arr.push(item);
                contentViewerVM.setCurrentContent(arr, arr[0]);

            },
            removeById = function (e, ui) {

                var index = e.id;
                removeStory(e);
                $("#" + index).remove();
            },
            removeStory = function (story) {
                if (parseInt(story) > 0) {
                    story = dc.stories.getLocalById(parseInt(story));
                    manager.production.removeStoryOnServer(story);
                }
                else if (story) {
                    manager.production.removeStoryOnServer(story);
                }
            },
            toggleProgramPreview = function (data, event, showFullPreview, isRegenerateFlag) {
                return;
                ppc.reset();

                if (ppc.isVisible()) {
                    ppc.isVisible(false);
                    return;
                }

                var isStoryPreview = false;

                if (!isRegenerateFlag && !showFullPreview && data.previewGuid() && data.previewGuid().length > 0) {
                    ppc.programPreviewUrl(data.previewUrl())
                    ppc.isVisible(true);
                    return;
                }
                else if (!showFullPreview) {
                    isStoryPreview = true;
                }
                else {

                    var episode = appdata.currentEpisode();
                    if (!isRegenerateFlag && episode.previewGuid() && episode.previewGuid().length > 0) {
                        ppc.programPreviewUrl(episode.previewUrl())
                        ppc.isVisible(true);
                        return;
                    }
                }
                btnAttribute({ text: "Generating Preview", disabled: true });
                manager.production.generateEpisodePreview(btnAttribute, ppc, isStoryPreview, data);
            },
            updateNewsBucketSequence = function () {
                presenter.toggleActivity(true);

                $.when(manager.production.updateNewsBucketSequence())
                .done(function () {
                    presenter.toggleActivity(false);
                })
                .fail(function () {
                    presenter.toggleActivity(false);
                });
            },
            updateSequenceOnServer = function () {
                presenter.toggleActivity(true);

                $.when(manager.production.saveProgramDataOnServer())
                .done(function () {
                    presenter.toggleActivity(false);
                })
                .fail(function () {
                    presenter.toggleActivity(false);
                });
            },
            swapNewsBucket = function (data, dir) {
                if (data) {

                    isUserPushing = dir;
                    if (dir === -1) {
                        data.isRed(false);
                        data.isGreen(false);
                        var index = greenNewsBucket.indexOf(data);
                        greenNewsBucket.remove(greenNewsBucket()[index]);
                    }
                    else if (dir === 1) {

                        var maxCount = appdata.currentProgram().maxStoryCount > 0 ? appdata.currentProgram().maxStoryCount : appdata.currentProgramStoryCount();
                        for (var i = 0; i < greenNewsBucket().length; i++) {
                            if (data.news.id === greenNewsBucket()[i].news.id) {
                                logger.error("Story Already Exist!");
                                return;
                            }
                        }
                        var maxSeqId = _.max(greenNewsBucket(), function (item) {
                            return item.sequenceId;
                        });

                        data.sequenceId = maxSeqId.sequenceId + 1;

                        var isStoryObject = data instanceof model.Story;

                        data.isGreen(true);
                        currentAddedObject = data;

                        if (greenNewsBucket().length >= maxCount) {
                            data.isRed(true);
                            data.isGreen(false);

                        }
                        if (isStoryObject) {
                            data.isGreen(true)
                            currentAddedObject = data;
                            greenNewsBucket.push(data);

                        }
                    }
                    appdata.refreshNewsBucket(!appdata.refreshNewsBucket());
                }
            },
            proceedToProduction = function () {

                var flag = (greenNewsBucket() && greenNewsBucket().length > 0);
                var segmentsArray = dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                segmentsArray = _.filter(segmentsArray, function (tempObj) {
                    return tempObj.programElementType === e.ProgramElementType.Segment;
                });

                var storyCount = 0;
                var segmentIndex = 0;
                for (var i = 0; i < greenNewsBucket().length; i++) {
                    var item = greenNewsBucket()[i];
                    var story;
                    var canInsert = true;
                    if (greenNewsBucket()[i] instanceof model.NewsBucketItem) {
                        canInsert = true;
                        var maxCount = appdata.currentProgram().maxStoryCount > 0 ? appdata.currentProgram().maxStoryCount : appdata.currentProgramStoryCount();
                        var temp = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                        var isRed = false;
                        var isGreen = true;
                        if (temp.length >= maxCount) {
                            isRed = true;
                            isGreen = false;
                        }
                        story = {
                            Id: item.news.id,
                            EpisodeId: appdata.currentEpisode().id,
                            CategoryId: item.categoryId,
                            StoryId: item.storyId ? item.storyId : undefined,
                            ProgramElementId: segmentsArray[segmentIndex].id,
                            SequenceId: item.sequenceId,
                            PreviewGuid: item.previewGuid(),
                            CreationDateStr: item.lastUpdateDate,
                            LastUpdateDateStr: item.LastUpdateDateStr,
                            UserId: item.userId,
                            News: item.news,
                            TickerId: item.tickerId,
                            ScreenFlow: new model.ScreenFlow(),
                            IsRed: isRed,
                            IncludeInRundown: isGreen
                        }
                    }
                    else if (greenNewsBucket()[i] instanceof model.Story) {
                        var tempStory = dc.stories.getLocalById(item.id);
                        if (tempStory) {
                            if (tempStory.episodeId === appdata.currentEpisode().id) {
                                canInsert = false;
                            }
                            else {
                                story = mapper.story.toDto(tempStory);

                                story.Id = tempStory.news.id;
                                story.EpisodeId = appdata.currentEpisode().id;
                                story.ProgramElementId = segmentsArray[segmentIndex].id;

                                canInsert = true;
                            }
                        }
                    }

                    if (canInsert)
                        dc.stories.add(story, { addToObservable: true, groupByEpisodeId: true, groupByProgramElementId: true });

                    storyCount++;
                    if (storyCount >= segmentsArray[segmentIndex].storyCount) {
                        if (segmentIndex + 1 < segmentsArray.length) {
                            segmentIndex++;
                            storyCount = 0;
                        } else if (segmentIndex + 1 == segmentsArray.length) {
                            storyCount = 0;
                        }

                    }
                }
                manager.production.proceedToProduction(flag, greenNewsBucket());
            },
            activate = function (routeData, callback) {
                isActive(true);
                setTimeout(function () {
                    $("#sequence-news-list").sortable('refresh');


                }, 500);
                refresh(!refresh());
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
            },
            canLeave = function () {
                isActive(false);
                return true;
            },

            init = function () {
                programInfoControl.currentPage(hashes.sequence);
                prc.isSendToBroadCastVisible(false);
                prc.isActive(true);
                isCartVisible(true);

                currentListView.subscribe(function (value) {
                    //console.log(value);
                });
            };

        init();

        return {
            activate: activate,
            canLeave: canLeave,
            stories_OBSOLETE: stories_OBSOLETE,
            appdata: appdata,
            templates: templates,
            programInfoControl: programInfoControl,
            calendarControl: calendarControl,
            hashes: hashes,
            arrangeStories: arrangeStories,
            removeStory: removeStory,
            programs: programs,
            episodes: episodes,
            proceedToProduction: proceedToProduction,
            removeById: removeById,
            btnAttribute: btnAttribute,
            toggleProgramPreview: toggleProgramPreview,
            ppc: ppc,
            prc: prc,
            utils: utils,
            e: e,
            playRundownUrl: playRundownUrl,
            greenNewsBucket: greenNewsBucket,
            blueNewsBucket: blueNewsBucket,
            populateNewsBucketList: populateNewsBucketList,
            swapNewsBucket: swapNewsBucket,
            previousProductionList: previousProductionList,
            isCartVisible: isCartVisible,
            isPreviousProductionVisible: isPreviousProductionVisible,
            toggleBlueArea: toggleBlueArea,
            currentListView: currentListView,
            arrangeNewsBucket: arrangeNewsBucket
        };
    });